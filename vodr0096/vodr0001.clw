

   MEMBER('vodr0096.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


VCP_TATReport PROCEDURE                               !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::13:TAGFLAG         BYTE(0)
DASBRW::13:TAGMOUSE        BYTE(0)
DASBRW::13:TAGDISPSTATUS   BYTE(0)
DASBRW::13:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:VersionNumber    STRING(30)
tmp:Clipboard        ANY
StatusQueue          QUEUE,PRE(staque)
StatusMessage        STRING(60)
                     END
TempFilePath         CSTRING(255)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:LastDetailColumn STRING(2)
tmp:LastSummaryColumn STRING(2)
tmp:UserName         STRING(70)
tmp:HeadAccountNumber STRING(30)
tmp:ARCLocation      STRING(30)
Progress:Thermometer BYTE(0)
tmp:ARCAccount       BYTE(0)
tmp:ARCCompanyName   STRING(30)
tmp:ARCUser          BYTE(0)
TempFileQueue        QUEUE,PRE(tmpque)
AccountNumber        STRING(30)
CompanyName          STRING(30)
Count                LONG
FileName             STRING(255)
JobsBooked           LONG
JobsOpen             LONG
                     END
tmp:Tag              STRING(1)
tmp:AllAccounts      BYTE(0)
FilenameQueue        QUEUE,PRE(filque)
AccountNumber        STRING(30)
TempFilename         STRING(255)
RecordCount          LONG
                     END
TATFields            GROUP,PRE()
tmp:CourierTime      BYTE(0)
tmp:CourierTimeStartDate DATE
tmp:CourierTimeStartTime TIME
tmp:PUPInControl     BYTE(0)
tmp:PUPInControlStartDate DATE
tmp:PUPInControlStartTime TIME
tmp:PUPOutOfControl  BYTE(0)
tmp:PUPOutOfControlStartDate DATE
tmp:PUPOutOfControlStartTime TIME
tmp:WaitTime         BYTE(0)
tmp:WaitTimeStartDate DATE
tmp:WaitTimeStartTime TIME
tmp:CustomerCollection BYTE(0)
tmp:CustomerCollectionStartDate DATE
tmp:CustomerCollectionStartTime TIME
tmp:EstimateTime     BYTE(0)
tmp:EstimateTimeStartDate DATE
tmp:EstimateTimeStartTime TIME
tmp:EndOfRepairDate  DATE
tmp:EndOfRepairTime  TIME
tmp:EndOfCustomerPerspectiveDate DATE
tmp:EndOfCustomerPerspectiveTime TIME
                     END
TATTotals            GROUP,PRE()
tmp:TotalPUPInControl LONG
tmp:TotalCustomerCollection LONG
tmp:TotalCourierTime LONG
tmp:TotalPUPOutOfControl LONG
tmp:TotalWaitTime    LONG
tmp:TotalEstimateTime LONG
                     END
SummaryQueue         QUEUE,PRE(sumque)
AccountNumber        STRING(30)
OpenJobs             LONG
BookedJobs           LONG
CompletedJobs        LONG
                     END
tmp:CurrentRow       LONG
PerspectiveQueue     QUEUE,PRE(perque)
AccountNumber        STRING(30)
Type                 STRING(2)
Count                LONG
                     END
BRW12::View:Browse   VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:Tag                LIKE(tmp:Tag)                  !List box control field - type derived from local data
tmp:Tag_Icon           LONG                           !Entry's icon ID
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Excel                SIGNED !OLE Automation holder
excel:ProgramName    CString(255)
excel:ActiveWorkBook CString(20)
excel:Selected       CString(20)
excel:FileName       CString(255)
loc:Version          Cstring(30)
window               WINDOW('** Report Title **'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),ICON('Cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE,IMM
                       PANEL,AT(60,38,560,360),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,56,552,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(68,60,544,36),USE(?GroupTip),BOXED,TRN
                       END
                       STRING(@s255),AT(72,76,496,),USE(SRN:TipText),TRN
                       BUTTON,AT(576,64,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('VCP TAT Report Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,104,552,258),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Criteria'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Jobs Completed From'),AT(249,122),USE(?tmp:StartDate:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(337,122,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),REQ,UPR
                           BUTTON,AT(405,118),USE(?PopCalendar),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Jobs Completed To'),AT(249,144),USE(?tmp:EndDate:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(337,144,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Jobs Completed To'),TIP('Jobs Completed To'),REQ,UPR
                           CHECK('All Accounts'),AT(224,160),USE(tmp:AllAccounts),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Accounts'),TIP('All Accounts'),VALUE('1','0')
                           BUTTON,AT(405,140),USE(?PopCalendar:2),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT(''),AT(68,154),USE(?StatusText)
                           LIST,AT(224,172,240,184),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)J@s1@68L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON,AT(468,264),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(468,298),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON('&Rev tags'),AT(334,241,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(330,271,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(468,330),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                         END
                       END
                       PROMPT('Ensure Excel is NOT running before you begin!'),AT(238,378,204,12),USE(?Prompt4),CENTER,FONT(,,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                       BUTTON,AT(472,366),USE(?Print),TRN,FLAT,ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(544,366),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(72,376),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

!Progress Window (DBH: 22-03-2004)
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progresswindow WINDOW('Progress...'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH), |
         CENTER,IMM,ICON('cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,TIMER(1),GRAY,DOUBLE
       PANEL,AT(160,64,360,300),USE(?Panel5),FILL(0D6E7EFH)
       PANEL,AT(164,68,352,12),USE(?Panel1),FILL(09A6A7CH)
       LIST,AT(208,88,268,206),USE(?List1),VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH), |
           FORMAT('20L(2)|M'),FROM(StatusQueue),GRID(COLOR:White)
       PANEL,AT(164,84,352,246),USE(?Panel4),FILL(09A6A7CH)
       PROGRESS,USE(progress:thermometer),AT(206,314,268,12),RANGE(0,100)
       STRING(''),AT(259,300,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,080FFFFH,FONT:bold), |
           COLOR(09A6A7CH)
       STRING(''),AT(232,136,161,10),USE(?progress:pcttext),TRN,HIDE,CENTER,FONT('Arial',8,,)
       PANEL,AT(164,332,352,28),USE(?Panel2),FILL(09A6A7CH)
       PROMPT('Report Progress'),AT(168,70),USE(?WindowTitle2),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
       BUTTON,AT(444,332),USE(?ProgressCancel),TRN,FLAT,LEFT,ICON('Cancelp.jpg')
       BUTTON,AT(376,332),USE(?Button:OpenReportFolder),TRN,FLAT,HIDE,ICON('openrepp.jpg')
       BUTTON,AT(444,332),USE(?Finish),TRN,FLAT,HIDE,ICON('Finishp.jpg')
     END
local       Class
DrawBox                     Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
GetSummaryType              Procedure(Long func:Time),String
UpdateProgressWindow        Procedure(String  func:Text)
WriteLine                   Procedure()
WriteSummaryLineTotals      Procedure(Long  func:Row)
WriteSummarySectionTotal    Procedure(Long func:Row)
            End ! local       Class
!Export Files (DBH: 22-03-2004)
ExportFile    File,Driver('ASCII'),Pre(exp),Name(glo:ExportFile),Create,Bindable,Thread
Record                  Record
Line1                   String(2000)
                        End
                    End
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

BRW12                CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW12::Sort0:Locator StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
    Map
ExcelSetup          Procedure(Byte      func:Visible)

ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)

ExcelMakeSheet      Procedure()

ExcelSheetType      Procedure(String    func:Type)

ExcelHorizontal     Procedure(String    func:Direction)

ExcelVertical        Procedure(String    func:Direction)

ExcelCell   Procedure(String    func:Text,Byte  func:Bold)

ExcelFormatCell     Procedure(String    func:Format)

ExcelFormatRange    Procedure(String    func:Range,String   func:Format)

ExcelNewLine    Procedure(Long  func:Number)

ExcelMoveDown   Procedure()

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)

ExcelCellWidth          Procedure(Long  func:Width)

ExcelAutoFit            Procedure(String    func:Range)

ExcelGrayBox            Procedure(String    func:Range)

ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)

ExcelSelectRange        Procedure(String    func:Range)

ExcelFontSize           Procedure(Byte  func:Size)

ExcelSheetName          Procedure(String    func:Name)

ExcelSelectSheet    Procedure(String    func:SheetName)

ExcelAutoFilter         Procedure(String    func:Range)

ExcelDropAllSheets      Procedure()

ExcelDeleteSheet        Procedure(String    func:SheetName)

ExcelClose              Procedure()

ExcelSaveWorkBook       Procedure(String    func:Name)

ExcelFontColour         Procedure(String    func:Range,Long func:Colour)

ExcelWrapText           Procedure(String    func:Range,Byte func:True)

ExcelGetFilename        Procedure(Byte      func:DontAsk),Byte

ExcelGetDirectory       Procedure(),Byte

ExcelCurrentColumn      Procedure(),String

ExcelCurrentRow         Procedure(),String

ExcelPasteSpecial       Procedure(String    func:Range)

ExcelConvertFormula     Procedure(String    func:Formula),String

ExcelColumnLetter Procedure(Long  func:ColumnNumber),String

ExcelOpenDoc            Procedure(String    func:FileName)

ExcelFreeze             Procedure(String    func:Cell)
    End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::13:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW12.UpdateBuffer
   glo:Queue.Pointer = sub:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = sub:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:Tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:Tag = ''
  END
    Queue:Browse.tmp:Tag = tmp:Tag
  IF (tmp:Tag = '*')
    Queue:Browse.tmp:Tag_Icon = 2
  ELSE
    Queue:Browse.tmp:Tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW12.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW12::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = sub:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW12.Reset
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::13:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::13:QUEUE = glo:Queue
    ADD(DASBRW::13:QUEUE)
  END
  FREE(glo:Queue)
  BRW12.Reset
  LOOP
    NEXT(BRW12::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::13:QUEUE.Pointer = sub:Account_Number
     GET(DASBRW::13:QUEUE,DASBRW::13:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = sub:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASSHOWTAG Routine
   CASE DASBRW::13:TAGDISPSTATUS
   OF 0
      DASBRW::13:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::13:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::13:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW12.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DrawTitle     Routine     !Set Detail Title (DBH: 22-03-2004)
    E1.WriteToCell(excel:ProgramName, 'A1')
    E1.WriteToCell('Criteria','A3')
    E1.WriteToCell(tmp:VersionNumber,'D3')
    E1.WriteToCell('Completed Date From','A4')
    E1.WriteToCell(Format(tmp:StartDate,@d06),'B4')
    E1.WriteToCell('Completed Date To','A5')
    E1.WriteToCell(Format(tmp:EndDate,@d06),'B5')
    E1.WriteToCell('Created Date','A6')
    E1.WriteToCell(Format(Today(),@d06),'B6')

    E1.SetCellFontStyle('Bold','A1','D3')
    E1.SetCellFontStyle('Bold','A4','A6')

    local.DrawBox('A1','D1','A1','D1',color:Silver)
    local.DrawBox('A3','D3','A6','D6',color:Silver)

    E1.SetCellFontName('Tahoma','A1','D6')



getnextrecord2      routine !Progress Window Routines (DBH: 22-03-2004)
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress >= 100
        recordsprocessed        = 0
        percentprogress         = 0
        progress:thermometer    = 0
        recordsthiscycle        = 0
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    !0{prop:Text} = ?progress:pcttext{prop:text}
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                Yield()
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case Missive('Do you want to finish off building the excel document with the data you have compiled so far, or just quit now?','ServiceBase 3g',|
                       'mquest.jpg','\Cancel|Quit|/Finish') 
            Of 3 ! Finish Button
                tmp:Cancel = 2
            Of 2 ! Quit Button
                tmp:Cancel = 1
            Of 1 ! Cancel Button
        End ! Case Missive
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    display()
Reporting       Routine  !Do the Report (DBH: 22-03-2004)
Data
local:LocalPath              String(255)
local:CurrentAccount    String(30)
local:ReportStartDate   Date()
local:ReportStartTime   Time()

local:RecordCount       Long()
local:Desktop           Cstring(255)
Code
    !Set the temp folder for the csv files (DBH: 10-03-2004)
    If GetTempPathA(255,TempFilePath)
        If Sub(TempFilePath,-1,1) = '\'
            local:LocalPath = Clip(TempFilePath) & ''
        Else !If Sub(TempFilePath,-1,1) = '\'
            local:LocalPath = Clip(TempFilePath) & '\'
        End !If Sub(TempFilePath,-1,1) = '\'
    End

    !Set the folder for the excel file (DBH: 10-03-2004)
    excel:ProgramName = 'VCP TAT Report'
    ! Create Folder In My Documents
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))


    excel:FileName = Clip(local:Desktop) & '\' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12)

    If Exists(excel:FileName & '.xls')
        Remove(excel:FileName & '.xls')
        If Error()
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('Cannot get access to the selected document:'&|
                '|' & Clip(excel:FileName) & ''&|
                '|'&|
                '|Ensure the file is not in use and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If Error()
    End !If Exists(excel:FileName)

    ! Remove file name for Office 2007 Compatibility
    excel:FileName = Sub(excel:FileName,1,Len(Clip(excel:FileName)) - 4)
    Remove(Clip(excel:Filename) & '.xls')
    Remove(Clip(excel:Filename) & '.xlsx')

    If COMMAND('/DEBUG')
        Remove('c:\vcptat.log')
    End !If COMMAND('/DEBUG')

    !Open Program Window (DBH: 10-03-2004)
    recordspercycle         = 25
    recordsprocessed        = 0
    percentprogress         = 0
    progress:thermometer    = 0
    recordstoprocess        = 50
    Open(ProgressWindow)

    ?progress:userstring{prop:text} = 'Running...'
    ?progress:pcttext{prop:text} = '0% Completed'


    local:ReportStartDate = Today()
    local:ReportStartTime = Clock()

    If Command('/DEBUG')
        local.UpdateProgressWindow('========')
        local.UpdateProgressWindow('*** DEBUG MODE ***')
        local.UpdateProgressWindow('========')
        local.UpdateProgressWindow('')
    End !If Command('/DEBUG')

    local.UpdateProgressWindow('Report Started: ' & Format(local:ReportStartDate,@d6b) & ' ' & Format(local:ReportStartTime,@t1b))
    local.UpdateProgressWindow('')
    local.UpdateProgressWindow('Creating Export Files...')

    !_____________________________________________________________________

    !CREATE CSV FILES
    !_____________________________________________________________________

    local.UpdateProgressWindow('')

    local.UpdateProgressWindow('Counting "Open Jobs"')

    Access:JOBS.Clearkey(job:DateCompletedKey)
    job:Date_Completed = 0
    Set(job:DateCompletedKey,job:DateCompletedKey)
    Loop ! Begin Loop
        If Access:JOBS.Next()
            Break
        End ! If Access:JOBS.Next()
        If job:Date_Completed <> 0
            Break
        End ! If job:Date_Completed <> 0
        If job:Who_Booked <> 'WEB'
            Cycle
        End ! If job:Who_Booked <> 'WEB'
        If job:Date_Booked < tmp:StartDate Or job:Date_Booked > tmp:EndDate
            Cycle
        End ! If job:Date_Booked < tmp:StartDate Or job:Date_Booked > tmp:EndDate
        glo:Pointer = job:Account_Number
        Get(glo:Queue,glo:Pointer)
        If Error()
           Cycle
        End ! If Error()

        sumque:AccountNumber = job:Account_Number
        Get(SummaryQueue,sumque:AccountNumber)
        If Error()
            sumque:AccountNumber = job:Account_Number
            sumque:OpenJobs = 1
            sumque:BookedJobs = 0
            Add(SummaryQueue)
        Else ! If Error()
            sumque:OpenJobs += 1
            Put(SummaryQueue)
        End ! If Error()
    End ! Loop

    local.UpdateProgressWindow('Counting "Booked Jobs"')

    Access:JOBS.Clearkey(job:Date_Booked_Key)
    job:Date_Booked = tmp:StartDate
    Set(job:Date_Booked_Key,job:Date_Booked_Key)
    Loop ! Begin Loop
        If Access:JOBS.Next()
            Break
        End ! If Access:JOBS.Next()
        If job:Date_Booked > tmp:EndDate
            Break
        End ! If job:Date_Booked > tmp:EndDate

        If job:Who_Booked <> 'WEB'
            Cycle
        End ! If job:Who_Booked <> 'WEB'

        glo:Pointer = job:Account_Number
        Get(glo:Queue,glo:Pointer)
        If Error()
            Cycle
        End ! If Error()

        sumque:AccountNumber = job:Account_Number
        Get(SummaryQueue,sumque:AccountNumber)
        If Error()
            sumque:AccountNumber = job:Account_Number
            sumque:BookedJobs = 1
            sumque:OpenJobs = 0
            Add(SummaryQueue)
        Else ! If Error()
            sumque:BookedJobs += 1
            Put(SummaryQueue)
        End ! If Error()
    End ! Loop


    Loop x# = 1 To Records(glo:Queue)
        Get(glo:Queue,x#)
        filque:AccountNumber = glo:Pointer
        filque:TempFilename  = Clip(local:LocalPath) & Clip(glo:Pointer) & Clock() & '.CSV'
        Add(FileNameQueue)
        Remove(FileNameQueue)
        glo:ExportFile = filque:TempFilename
        Create(ExportFile)
    End ! Loop x# = 1 To Records(glo:Queue)

    Access:JOBS.Clearkey(job:DateCompletedKey)
    job:Date_Completed = tmp:StartDate
    Set(job:DateCompletedKey,job:DateCompletedKey)
    Loop ! Begin Loop
        If Access:JOBS.Next()
            Break
        End ! If Access:JOBS.Next()
        If job:Date_Completed > tmp:EndDate
            Break
        End ! If job:DateCompleted > tmp:EndDate
        If job:Who_Booked <> 'WEB'
            Cycle
        End ! If job:Who_Booked <> 'WEB'

        Do GetNextRecord2
        Do CancelCheck
        If tmp:Cancel
            Break
        End !If tmp:Cancel = 1

        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
            !Found
        Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
            !Error
            Cycle
        End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
            !Found
        Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
            !Error
            Cycle
        End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign

        ! Criteria____________________________________________________________________________________

        ! Has this account been selected? (DBH: 26/07/2006)
        If ~tmp:AllAccounts
            glo:Pointer = job:Account_Number
            Get(glo:Queue,glo:Pointer)
            If Error()
                Cycle
            End ! If Error()
        End ! If ~tmp:AllAccounts

        Do WorkOutTimes

        Local.WriteLine()

        filque:AccountNumber = job:Account_Number
        Get(FilenameQueue,filque:AccountNumber)
        If ~error()
            filque:RecordCount += 1
            Put(FileNamequeue)
        End ! If ~error()
        local:RecordCount += 1

        sumque:AccountNumber = job:Account_Number
        Get(SummaryQueue,sumque:AccountNumber)
        If Error()
            sumque:AccountNumber = job:Account_Number
            sumque:OpenJobs = 0
            sumque:BookedJobs = 0
            sumque:CompletedJobs = 1
            Add(SummaryQueue)
        Else ! If Error()
            sumque:CompletedJobs += 1
            Put(SummaryQueue)
        End ! If Error()

    End !Loop TRADEACC
    Close(ExportFile)

    !_____________________________________________________________________

    If tmp:Cancel = 0 Or tmp:Cancel = 2

        ?ProgressCancel{prop:Disable} = 1

        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Building Excel Document..')
    !_____________________________________________________________________

    !CREATE EXPORT DOCUMENT, CREATE SHEETS, COLUMNS and AUTOFILTER
    !_____________________________________________________________________

        local.UpdateProgressWindow('Creating Excel Sheets...')
        local.UpdateProgressWindow('')

        If E1.Init(0,0) = 0
            Case Missive('An error has occurred finding your Excel document.'&|
              '<13,10>'&|
              '<13,10>Please quit and try again.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Exit
        End !If E1.Init(0,0,1) = 0

        tmp:LastDetailColumn = 'X'

        E1.NewWorkBook()
!        E1.InsertWorksheet()
        E1.SaveAs(excel:FileName)!,oix:xlWorkbookDefault)
        E1.CloseWorkBook(3)
        !Sort(FileNameQueue,-filque:AccountNumber)
        Loop x# = 1 To Records(FilenameQueue)
            Get(FilenameQueue,x#)
            local.UpdateProgressWindow('Writing Account: ' & Clip(filque:AccountNumber))
            local.UpdateProgressWindow('Jobs Found: ' & Clip(filque:RecordCount))
            If filque:RecordCount = 0
                Cycle
            End ! If filque:RecordCount = 0
            SetClipboard('')
            E1.OpenWorkBook(filque:TempFilename)
            E1.Copy('A1',Clip(tmp:LastDetailColumn) & filque:RecordCount)
            tmp:Clipboard = ClipBoard()
            SetClipboard('Blank')
            E1.CloseWorkBook(3)

            ! Don't need the temporary csv any more (DBH: 11/09/2007)
!            Remove(filque:TempFilename)

            E1.OpenWorkBook(excel:FileName)
            E1.InsertWorkSheet()
            Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
            sub:Account_Number = filque:AccountNumber
            If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                !Found
            Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                !Error
            End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign

            E1.RenameWorksheet(sub:Company_Name)
            E1.SelectCells('A12')
            SetClipboard(tmp:Clipboard)
            E1.Paste()
            
            ! Detail Title
            E1.WriteToCell(excel:ProgramName            ,'A1')
            E1.WriteToCell('Completed Date From'        ,'A3')
            E1.WriteToCell(Format(tmp:StartDate,@d06)   ,'B3')
            E1.WriteToCell('Completed Date To'          ,'A4')
            E1.WriteToCell(Format(tmp:EndDate,@d06)     ,'B4')
            E1.WriteToCell('Date Created'               ,'A5')
            E1.WriteToCell(Format(Today(),@d06)         ,'B5')

            E1.SetCellFontStyle('Bold','A1')
            E1.SetCellFontStyle('Bold','B3','B5')

            local.DrawBox('A1','A1','A1','A1',color:Silver)
            local.DrawBox('A3','X3','A5','X5',color:Silver)

            E1.WriteToCell('In Report'          ,'E3')
            E1.WriteToCell(filque:RecordCount   ,'E4')
            E1.WriteToCell('Showing'            ,'F3')
            E1.WriteToCell('=SUBTOTAL(2,A12:A' & filque:RecordCount + 12,'F4')

            E1.WriteToCell('Jobs Booked In'                 ,'I9')
            E1.WriteToCell('Jobs Completed 705'             ,'K9')
            E1.WriteToCell('Customer Perspective 465'       ,'M9')
            E1.WriteToCell('VCP In Control'                 ,'O9')
            E1.WriteToCell('Customer Collection'            ,'Q9')
            E1.WriteToCell('Wait Time'                      ,'S9')
            E1.WriteToCell('Courier Time'                   ,'U9')
            E1.WriteToCell('RRC / ARC / 3rd Party Repair'   ,'W9')
            E1.WriteToCell('VCP'                            ,'O8')
            E1.WriteToCell('OUT OF CONTROL'                 ,'S8')
            local.DrawBox('I9','J9','I9','J9',color:Silver)
            local.DrawBox('K9','L9','K9','L9',color:Silver)
            local.DrawBox('M9','N9','M9','N9',color:Silver)
            local.DrawBox('O9','P9','O9','P9',color:Silver)
            local.DrawBox('Q9','R9','Q9','R9',color:Silver)
            local.DrawBox('S9','T9','S9','T9',color:Silver)
            local.DrawBox('U9','V9','U9','V9',color:Silver)
            local.DrawBox('W9','X9','W9','X9',color:Silver)
            local.DrawBox('O8','R8','O8','R8',color:Silver)
            local.DrawBox('S8','X8','S8','X8',color:Silver)
            local.DrawBox('A8','X8','A11','X11',color:Silver)

            ! Define Column Titles (DBH: 01/08/2006)
            E1.WriteToCell('VCP Job Number'         ,'A11')
            E1.WriteToCell('Branch Specific Number' ,'B11')
            E1.WriteToCell('Franchise Sent To'      ,'C11')
            E1.WriteToCell('Sub Account Number'     ,'D11')
            E1.WriteToCell('Sub Account Name'       ,'E11')
            E1.WriteToCell('Manufacturer'           ,'F11')
            E1.WriteToCell('Model Number'           ,'G11')
            E1.WriteToCell('Warranty'               ,'H11')
            E1.WriteToCell('Date'                   ,'I11')
            E1.WriteToCell('Time'                   ,'J11')
            E1.WriteToCell('Date'                   ,'K11')
            E1.WriteToCell('Time'                   ,'L11')
            E1.WriteToCell('Days'                   ,'M11')
            E1.WriteToCell('HH:MM'                  ,'N11')
            E1.WriteToCell('Days'                   ,'O11')
            E1.WriteToCell('HH:MM'                  ,'P11')
            E1.WriteToCell('Days'                   ,'Q11')
            E1.WriteToCell('HH:MM'                  ,'R11')
            E1.WriteToCell('Days'                   ,'S11')
            E1.WriteToCell('HH:MM'                  ,'T11')
            E1.WriteToCell('Days'                   ,'U11')
            E1.WriteToCell('HH:MM'                  ,'V11')
            E1.WriteToCell('Days'                   ,'W11')
            E1.WriteToCell('HH:MM'                  ,'X11')

            E1.SetCellFontName('Tahoma','A1',Clip(tmp:LastDetailColumn) & filque:RecordCount + 12)
            E1.SetCellFontSize(8,'A1',Clip(tmp:LastDetailColumn) & filque:RecordCount + 12)
            E1.SetCellFontStyle('Bold','A8',Clip(tmp:LastDetailColumn) & '11')
            E1.SetCellFontStyle('Bold','A9')
            E1.SetCellFontStyle('Bold','F9')
            E1.SetCellFontStyle('Bold','H9')
            local.DrawBox('A8','X8','A11','X11',color:Silver)

            E1.AutoFilter('A','X')
            E1.SetColumnWidth('A',Clip(tmp:LastDetailColumn))

            E1.SelectCells('B12')
            E1.FreezePanes()

            SetClipBoard('Blank')
!            E1.SaveAs(excel:FileName)
            E1.CloseWorkBook(3)
        End ! Loop x# = 1 To Records(TempFileQueue)

! ____________________________________________________________________________________
        ! Create Summary Worksheet (DBH: 01/08/2006)

        E1.OpenWorkBook(Excel:FileName)
        E1.InsertWorksheet()
        E1.RenameWorkSheet('Summary')

        Do DrawTitle

        ! Define Summary Column Titles (DBH: 01/08/2006)
        Do SummaryTitle

        E1.SelectCells('A11')

        tmp:LastSummaryColumn = 'AB'
        local:RecordCount = 11
        tmp:CurrentRow = 11
        Sort(SummaryQueue,Sumque:AccountNumber)
        Loop x# = 1 To Records(SummaryQueue)
            Get(SummaryQueue,x#)
            local:RecordCount += 2

            Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
            sub:Account_Number = sumque:AccountNumber
            If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                !Found
            Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                !Error
            End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign

            E1.WriteToCell(sub:Company_Name ,'A' & tmp:CurrentRow)
            E1.WriteToCell(sumque:CompletedJobs ,'B' & tmp:CurrentRow)
            E1.WriteToCell(sumque:BookedJobs    ,'C' & tmp:CurrentRow)
            E1.WriteToCell(sumque:OpenJobs  ,'D' & tmp:CurrentRow)

            Loop y# = 1 To Records(PerspectiveQueue)
                Get(PerspectiveQueue,y#)
                If perque:AccountNumber <> sumque:AccountNumber
                    Cycle
                End ! If perque:AccountNumber <> sumque:AccountNumber
                E1.WriteToCell(perque:Count,Clip(perque:Type) & Clip(tmp:CurrentRow))
            End ! Loop x# = 1 To Records(PerspectiveQueue)

            Local.WriteSummaryLineTotals(tmp:CurrentRow)
            tmp:CurrentRow += 2
        End ! Loop x# = 1 To Records(SummaryQueue)

        local.WriteSummarySectionTotal(tmp:CurrentRow - 2)

        E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'G11','G' & tmp:CurrentRow + 4)
        E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'J11','J' & tmp:CurrentRow + 4)
        E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'M11','M' & tmp:CurrentRow + 4)
        E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'P11','P' & tmp:CurrentRow + 4)
        E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'S11','S' & tmp:CurrentRow + 4)
        E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'V11','V' & tmp:CurrentRow + 4)
        E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'Y11','Y' & tmp:CurrentRow + 4)
        E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'AB11','AB' & tmp:CurrentRow + 4)

        local:RecordCount = tmp:CurrentRow + 4
        E1.SetCellFontName('Tahoma','A1',Clip(tmp:LastSummaryColumn) & local:RecordCount)
        E1.SetCellFontStyle('Bold','A9',Clip(tmp:LastSummaryColumn) & '10')
        E1.SetCellFontStyle('Bold','A' & local:RecordCount,Clip(tmp:LastSummaryColumn) & local:RecordCount)
        E1.SetCellFontSize(8,'A1',Clip(tmp:LastSummaryColumn) & local:RecordCount)
        E1.SetColumnWidth('A',Clip(tmp:LastSummaryColumn))
        E1.SetColumnWidth('E',,1)
        E1.SetColumnWidth('H',,1)
        E1.SetColumnWidth('K',,1)
        E1.SetColumnWidth('N',,1)
        E1.SetColumnWidth('Q',,1)
        E1.SetColumnWidth('T',,1)
        E1.SetColumnWidth('W',,1)
        E1.SetColumnWidth('Z',,1)
        E1.SelectCells('B11')
        E1.FreezePanes()
        
        E1.SelectWorksheet('Sheet1')
        E1.DeleteWorkSheet()
        E1.SelectWorksheet('Summary')
        E1.SelectCells('A11')
 !       E1.Save
        E1.CloseWorkBook(3)
        E1.Kill()

        local.UpdatePRogressWindow('Finishing Off Formatting..')


        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))

    Else!If tmp:Cancel = False
        staque:StatusMessage = '=========='
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

        staque:StatusMessage = 'Report CANCELLED: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b)
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

    End !If tmp:Cancel = False

    BHReturnDaysHoursMins(BHTimeDifference24Hr(local:ReportStartDate,Today(),local:ReportStartTime,Clock()),Days#,Hours#,Mins#)

    local.UpdateProgressWindow('Time To Finish: ' & Days# & ' Dys, ' & Format(Hours#,@n02) & ':' & Format(Mins#,@n02))
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))

    Do EndPrintRun
    ?progress:userstring{prop:text} = 'Finished...'

    Display()
    ?ProgressCancel{prop:Hide} = 1
    ?Finish{prop:Hide} = 0
    ?Button:OpenReportFolder{Prop:Hide} = 0
    Accept
        Case Field()
            Of ?Finish
                Case Event()
                    Of Event:Accepted
                        Break

                End !Case Event()
            Of ?Button:OpenReportFolder
                Case Event()
                Of Event:Accepted
                    RUN('EXPLORER.EXE ' & Clip(local:Desktop))
                End ! Case Event()
        End !Case Field()
    End !Accept
    Close(ProgressWindow)
    ?StatusText{prop:Text} = ''
    Post(Event:CloseWindow)
StartPUPInControl        Routine
    If tmp:CourierTime
        Do ResetCourierTime
    End ! If tmp:CourierTime
    If tmp:PUPOutOfControl
        Do ResetPUPOutOfControl
    End ! If tmp:PUPOutOfControl
    If tmp:WaitTime
        Do ResetWaitTime
    End ! If tmp:WaitTime
    If tmp:CustomerCollection
        Do ResetCustomerCollection
    End ! If tmp:CustomerCollection

    tmp:PUPInControl = 1
    tmp:PUPInControlStartDate = aus:DateChanged
    tmp:PUPInControlStartTime = aus:TimeChanged

    If Command('/DEBUG')
        LinePrint('In Control (Start): ' & Sub(aus:NewStatus,1,3) & ' - ' & Format(aus:DateChanged,@d06) & ' ' & Format(aus:TimeChanged,@t01),'c:\vcptat.log')
    End ! If Command('/DEBUG')


StartCourierTime        Routine
    If tmp:PUPInControl
        Do ResetPUPInControl
    End ! If tmp:PUPInControl
    If tmp:PUPOutOfControl
        Do ResetPUPOutOfControl
    End ! If tmp:PUPOutOfControl
    If tmp:WaitTime
        Do ResetWaitTime
    End ! If tmp:WaitTime
    If tmp:CustomerCollection
        Do ResetCustomerCollection
    End ! If tmp:CustomerCollection

    tmp:CourierTime = 1
    tmp:CourierTimeStartDate = aus:DateChanged
    tmp:CourierTimeStartTime = aus:TimeChanged

    If Command('/DEBUG')
        LinePrint('Courier Time (Start): ' & Sub(aus:NewStatus,1,3) & ' - ' & Format(aus:DateChanged,@d06) & ' ' & Format(aus:TimeChanged,@t01),'c:\vcptat.log')
    End ! If Command('/DEBUG')


StartPUPOutOfControl    Routine
    If tmp:PUPInControl
        Do ResetPUPInControl
    End ! If tmp:PUPInControl
    If tmp:CourierTime
        Do ResetCourierTime
    End ! If tmp:CourierTime
    If tmp:WaitTime
        Do ResetWaitTime
    End ! If tmp:WaitTime
    If tmp:CustomerCollection
        Do ResetCustomerCollection
    End ! If tmp:CustomerCollection

    tmp:PUPOutOfControl = 1
    tmp:PUPOutOfControlStartDate = aus:DateChanged
    tmp:PUPOutOfControlStartTime = aus:TimeChanged

    If Command('/DEBUG')
        LinePrint('Out Of Control (Start): ' & Sub(aus:NewStatus,1,3) & ' - ' & Format(aus:DateChanged,@d06) & ' ' & Format(aus:TimeChanged,@t01),'c:\vcptat.log')
    End ! If Command('/DEBUG')


StartWaitTime            Routine
    If tmp:PUPInControl
        Do ResetPUPInControl
    End ! If PUPInControl
    If tmp:CourierTime
        Do ResetCourierTime
    End ! If tmp:CourierTime
    If tmp:PUPOutOfControl
        Do ResetPUPOutOfControl
    End ! If tmp:PUPOutOfControl
    If tmp:CustomerCollection
        Do ResetCustomerCollection
    End ! If tmp:CustomerCollection

    tmp:WaitTime = 1
    tmp:WaitTimeStartDate = aus:DateChanged
    tmp:WaitTimeStartTime = aus:TimeChanged

    If Command('/DEBUG')
        LinePrint('Wait Time (Start): ' & Sub(aus:NewStatus,1,3) & ' - ' & Format(aus:DateChanged,@d06) & ' ' & Format(aus:TimeChanged,@t01),'c:\vcptat.log')
    End ! If Command('/DEBUG')

StartCustomerCollection    Routine
    If tmp:PUPInControl
        Do ResetPUPInControl
    End ! If PUPInControl
    If tmp:CourierTime
        Do ResetCourierTime
    End ! If tmp:CourierTime
    If tmp:PUPOutOfControl
        Do ResetPUPOutOfControl
    End ! If tmp:PUPOutOfControl
    If tmp:WaitTime
        Do ResetWaitTime
    End ! If tmp:WaitTime

    If Command('/DEBUG')
        LinePrint('Cust Collection (Start): ' & Sub(aus:NewStatus,1,3) & ' - ' & Format(aus:DateChanged,@d06) & ' ' & Format(aus:TimeChanged,@t01),'c:\vcptat.log')
    End ! If Command('/DEBUG')


    tmp:CustomerCollection = 1
    tmp:CustomerCollectionStartDate = aus:DateChanged
    tmp:CustomerCollectionStartTime = aus:TimeChanged

StartEstimateTime        Routine
    tmp:EstimateTime = 1
    tmp:EstimateTimeStartDate = aus:DateChanged
    tmp:EstimateTimeStartTime = aus:TimeChanged

    If Command('/DEBUG')
        LinePrint('Estimate Time (Start): ' & Sub(aus:NewStatus,1,3) & ' - ' & Format(aus:DateChanged,@d06) & ' ' & Format(aus:TimeChanged,@t01),'c:\vcptat.log')
    End ! If Command('/DEBUG')


StopAll                 ROutine
    If tmp:PUPInControl
        Do ResetPUPInControl
    End ! If PUPInControl
    If tmp:CourierTime
        Do ResetCourierTime
    End ! If tmp:CourierTime
    If tmp:PUPOutOfControl
        Do ResetPUPOutOfControl
    End ! If tmp:PUPOutOfControl
    If tmp:WaitTime
        Do ResetWaitTime
    End ! If tmp:WaitTime
    If tmp:CustomerCollection
        Do ResetCustomerCollection
    End ! If tmp:CustomerCollection

ResetPUPInControl        Routine
    tmp:PUPInControl = 0
    tmp:TotalPUPInControl += BHTimeDifference24Hr(tmp:PUPInControlStartDate, |
                                                aus:DateChanged, |
                                                tmp:PUPInControlStartTime, |
                                                aus:TimeChanged)

    If Command('/DEBUG')
        LinePrint('In Control (Stop): ' & Sub(aus:NewStatus,1,3) & ' - ' & Format(aus:DateChanged,@d06) & ' ' & Format(aus:TimeChanged,@t01),'c:\vcptat.log')
    End ! If Command('/DEBUG')

ResetCourierTime        Routine
    tmp:CourierTime = 0
    tmp:TotalCourierTime += BHTimeDifference24Hr(tmp:CourierTimeStartDate, |
                                                aus:DateChanged, |
                                                tmp:CourierTimeStartTime, |
                                                aus:TimeChanged)

    If Command('/DEBUG')
        LinePrint('Courier Time (Stop): ' & Sub(aus:NewStatus,1,3) & ' - ' & Format(aus:DateChanged,@d06) & ' ' & Format(aus:TimeChanged,@t01),'c:\vcptat.log')
    End ! If Command('/DEBUG')

ResetPUPOutOfControl    Routine
    tmp:PUPOutOfControl = 0
    tmp:TotalPUPOutOfControl += BHTimeDifference24Hr(tmp:PUPOutOfControlStartDate, |
                                                    aus:DateChanged, |
                                                    tmp:PUPOutOfControlStartTime,|
                                                    aus:TimeChanged)
    If Command('/DEBUG')
        LinePrint('Out Of Control (Stop): ' & Sub(aus:NewStatus,1,3) & ' - ' & Format(aus:DateChanged,@d06) & ' ' & Format(aus:TimeChanged,@t01),'c:\vcptat.log')
    End ! If Command('/DEBUG')

ResetWaitTime            Routine
    tmp:WaitTime = 0
    tmp:TotalWaitTime += BHTimeDifference24Hr(tmp:WaitTimeStartDate,|
                                                aus:DateChanged, |
                                                tmp:WaitTimeStartTime,|
                                                aus:TimeChanged)
    If Command('/DEBUG')
        LinePrint('Wait Time (Stop): ' & Sub(aus:NewStatus,1,3) & ' - ' & Format(aus:DateChanged,@d06) & ' ' & Format(aus:TimeChanged,@t01),'c:\vcptat.log')
    End ! If Command('/DEBUG')

ResetCustomerCollection    Routine
    tmp:CustomerCollection = 0
    tmp:TotalCustomerCollection += BHTimeDifference24Hr(tmp:CustomerCollectionStartDate, |
                                                        aus:DateChanged, |
                                                        tmp:CustomerCollectionStartTime,|
                                                        aus:TimeChanged)

    If Command('/DEBUG')
        LinePrint('Customer Collection (Stop): ' & Sub(aus:NewStatus,1,3) & ' - ' & Format(aus:DateChanged,@d06) & ' ' & Format(aus:TimeChanged,@t01),'c:\vcptat.log')
    End ! If Command('/DEBUG')

ResetEstimateTime            Routine
    tmp:EstimateTime = 0
    tmp:TotalEstimateTime += BHTimeDifference24Hr(tmp:EstimateTimeStartDate, |
                                                    aus:DateChanged, |
                                                    tmp:EstimateTimeStartTime, |
                                                    aus:TimeChanged)

    If Command('/DEBUG')
        LinePrint('Estimate Time (Stop): ' & Sub(aus:NewStatus,1,3) & ' - ' & Format(aus:DateChanged,@d06) & ' ' & Format(aus:TimeChanged,@t01),'c:\vcptat.log')
    End ! If Command('/DEBUG')
SummaryTitle        Routine
    E1.WriteToCell('Real Time Customer Perspective','A8')
    Local.DrawBox('A8','D8','A10','D10',color:Silver)

    E1.WriteToCell('1 Hour','F8')
    E1.WriteToCell('Completed','F10')
    E1.WriteToCell('%Satisfied','G10')
    Local.DrawBox('F8','G8','F10','G10',color:Silver)

    E1.WriteToCell('24 Hours','I8')
    E1.WriteToCell('Completed','I10')
    E1.WriteToCell('%Satisfied','J10')
    Local.DrawBox('I8','J8','I10','J10',color:Silver)

    E1.WriteToCell('48 Hours','L8')
    E1.WriteToCell('Completed','L0')
    E1.WriteToCell('%Satisfied','M10')
    Local.DrawBox('L8','M8','L10','M10',color:Silver)

    E1.WriteToCell('3 Days','O8')
    E1.WriteToCell('Completed','O0')
    E1.WriteToCell('%Satisfied','P10')
    Local.DrawBox('O8','P8','O10','P10',color:Silver)

    E1.WriteToCell('5 Days','R8')
    E1.WriteToCell('Completed','R')
    E1.WriteToCell('%Satisfied','S10')
    Local.DrawBox('R8','S8','R10','S10',color:Silver)

    E1.WriteToCell('7 Days','U8')
    E1.WriteToCell('Completed','U0')
    E1.WriteToCell('%Satisfied','V10')
    Local.DrawBox('U8','V8','U10','V10',color:Silver)

    E1.WriteToCell('9 Days','X8')
    E1.WriteToCell('Completed','X0')
    E1.WriteToCell('%Satisfied','Y10')
    Local.DrawBox('X8','Y8','X10','Y10',color:Silver)

    E1.WriteToCell('Over 9 Days','AA8')
    E1.WriteToCell('Completed','AA10')
    E1.WriteToCell('%Satisfied','AB10')
    Local.DrawBox('AA8','AB8','AA10','AB10',color:Silver)

    E1.WriteToCell('Account Name','A10')
    E1.WriteToCell('Jobs Completed Within Dates','B10')
    E1.WriteToCell('Jobs Booked Within Dates','C10')
    E1.WriteToCell('Open Jobs','D10')

    E1.SetCellFontStyle('Bold','A8','AB10')
WorkOutTimes        Routine
Data
local:ExchangeAttached      Byte(0)
local:EndOfCustomerPerspective  Byte(0)
local:EndOfRepair           Byte(0)
Code
    Clear(TATFields)
    Clear(TATTotals)
    tmp:TotalPUPInControl = 0
    tmp:TotalPUPInControl    = 0
    tmp:TotalCustomerCollection = 0
    tmp:TotalWaitTime        = 0
    tmp:TotalCourierTime     = 0
    tmp:TotalPUPOutOfControl = 0

    tmp:PUPInControlStartDate    = job:Date_Booked
    tmp:PUPInControlStartTime    = job:Time_Booked

    If Command('/DEBUG')
        LinePrint('[' & job:Ref_Number & ']','c:\vcptat.log')
        LinePrint('Job Booked: ' & Format(job:Date_Booked,@d06) & ' ' & Format(job:Time_Booked,@t01),'c:\vcptat.log')
    End ! If Command('/DEBUG')

    Access:AUDSTATS.Clearkey(aus:RefDateRecordKey)
    aus:RefNumber    = job:Ref_Number
    aus:DateChanged  = 0
    Set(aus:RefDateRecordKey,aus:RefDateRecordKey)
    Loop ! Begin Loop
        If Access:AUDSTATS.Next()
            Break
        End ! If Access:AUDSTATS.Next()
        If aus:RefNumber <> job:Ref_Number
            Break
        End ! If aus:RefNumber <> job:Ref_Number

        Case Sub(aus:NewStatus,1,3)
        Of '000'
!            tmp:PUPInControlStartDate = aus:DateChanged
!            tmp:PUPInControlStartTime = aus:TimeChanged
            Do StartPUPInControl

        Of '110' ! Despatched Exchange Unit
            If aus:Type = 'EXC'
                local:ExchangeAttached = 1
                Do StartWaitTime

                If ~local:EndOfRepair
                    tmp:EndOfRepairDate = aus:DateChanged
                    tmp:EndOfRepairTime = aus:TimeChanged
                    local:EndOfRepair = 1
                End ! If ~local:EndOfRepair
            End ! If aus:Type = 'EXC'
        Of '461' ! Despatched From PUP
            ! Start Courier Time
            If aus:Type = 'JOB' And ~local:ExchangeAttached And ~local:EndOfRepair
                Do StartCourierTime            
            End ! If aus:Type = 'JOB' And ~local:ExchangeAttached
        Of '462' ! Received From PUP
            ! Start Out Of Control Time
            If aus:Type = 'JOB' And ~local:ExchangeAttached And ~local:EndOfRepair
                Do StartPUPOutOfControl
            End ! If aus:Type = 'JOB' And ~local:ExchangeAttached
            
        Of '463' ! Send To PUP
            ! Start Wait Time
            If aus:Type = 'JOB' And ~local:ExchangeAttached
                Do StartWaitTime
            End ! If aus:Type = 'JOB' And ~local:ExchangeAttached
        Of '464' ! Despatched To PUP
            ! Start Courier Time
            If aus:Type = 'JOB' And ~local:ExchangeAttached
                Do StartCourierTime
            End ! If aus:Type = 'JOB' And ~local:ExchangeAttached

        Of '465' ! Received AT PUP
            ! Start Customer Collection
            If aus:Type = 'JOB' And ~local:ExchangeATtached
                Do StartCustomerCollection
            End ! If aus:Type = 'JOB' And ~local:ExchangeATtached
            ! If no exchange is attached, then count this as the "End Of Customer Perspective" (DBH: 13/09/2007)
            If job:Exchange_Unit_Number = 0
                If ~local:EndOfCustomerPerspective
                    tmp:EndOfCustomerPerspectiveDate = aus:DateChanged
                    tmp:EndOfCustomerPerspectiveTime = aus:TimeChanged
                    local:EndOfCustomerPerspective = 1
                End ! If ~local:EndOfCustomerPerspective
            End ! If job:Exchange_Unit_Number = 0

        Of '468' ! Exchange Despatched To PUP
            If aus:Type = 'EXC'
                Do StartCourierTime
            End ! If aus:Type = 'EXC'
        Of '469' ! Exchange Received At Pup
            If aus:Type = 'EXC'
                Do StartCustomerCollection
                If ~local:EndOfCustomerPerspective
                    tmp:EndOfCustomerPerspectiveDate = aus:DateChanged
                    tmp:EndOfCustomerPerspectiveTime = aus:TimeChanged
                    local:EndOfCustomerPerspective = 1
                End ! If ~local:EndOfCustomerPerspective
            End ! If aus:Type = 'EXC'
        Of '520' ! Estimate Sent
            ! Stop The Clock
            If ~local:EndOfRepair
                Do StartEstimateTime
            End ! If ~local:EndOfRepair
        Of '535' |! Estimate Accepted
        Orof '540' !Estimate Refust
            ! Start The Clock
            If ~local:EndOfRepair
                Do StartPUPOutOfControl
            End ! If ~local:EndOfRepair
        Of '705' |! Job Completed
        OrOf '710' !Job Completed (OBF)
            If aus:Type = 'JOB' And job:Exchange_Unit_Number = 0
                If ~local:EndOfRepair
                    tmp:EndOfRepairDate = aus:DateChanged
                    tmp:EndOfRepairTime = aus:TimeChanged
                    local:EndOfRepair = 1
                End ! If ~local:EndOfRepair
            End ! If aus:Type = 'JOB'
        Of '902' ! Despatched To Customer
            !End Of Customer Perspective
            Case aus:Type
            Of 'JOB'
            Of 'EXC'
            End ! Case aus:Type
            Do StopAll
        End ! Case Sub(aus:NewStatus,1,3)
    End ! Loop

    Do StopAll
    If Command('/DEBUG')
        LinePrint('PUP In Control Start: ' & Format(tmp:PUPInControlStartDate,@d06b) & ' ' & Format(tmp:PUPInControlStartTime,@t01b),'c:\vcptat.log')
        LinePrint('End Of Cust Per: ' & Format(tmp:EndOfCustomerPerspectiveDate,@d06b) & ' ' & Format(tmp:EndOfCustomerPerspectiveTime,@t01b),'c:\vcptat.log')
        LinePrint('End Of Report: ' & Format(tmp:EndOfRepairDate,@d06b) & ' ' & Format(tmp:EndOfRepairTime,@t01b),'c:\vcptat.log')
        LinePrint('Total In Control: ' & tmp:TotalPUPInControl,'c:\vcptat.log')
        LinePrint('Total Customer Collection: ' & tmp:TotalCustomerCollection,'c:\vcptat.log')
        LinePrint('Total Wait Time: ' & tmp:TotalWaitTime,'c:\vcptat.log')
        LinePrint('Total Courier Time: ' & tmp:TotalCourierTime,'c:\vcptat.log')
        LinePrint('Total Out Of Control: ' & tmp:TotalPUPOutOfControl,'c:\vcptat.log')
    End 

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('VCP_TATReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  Relate:WEBJOB.Open
  Access:TRADEACC.UseFile
  Access:JOBSE.UseFile
  Access:JOBS.UseFile
  Access:AUDSTATS.UseFile
  SELF.FilesOpened = True
  !Initialize Dates and save Head Account Information (DBH: 22-03-2004)
  
  tmp:StartDate = Deformat('1/' & Month(Today()) & '/' & Year(Today()),@d6)
  tmp:EndDate = Today()
  
  tmp:HeadAccountNumber   = GETINI('BOOKING','HeadAccount',,CLIP(Path()) & '\SB2KDEF.INI')
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = tmp:HeadAccountNumber
  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Found
      tmp:ARCLocation = tra:SiteLocation
      tmp:ARCCompanyName = tra:Company_Name
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  !Put in user name IF run from ServiceBase (DBH: 25-03-2004)
  pos# = Instring('%',COMMAND(),1,1)
  If pos#
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = Clip(Sub(COMMAND(),pos#+1,30))
      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Found
          tmp:UserName = Clip(use:Forename) & ' ' & Clip(use:Surname)
      Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Error
      End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  End !pos#
  BRW12.Init(?List,Queue:Browse.ViewPosition,BRW12::View:Browse,Queue:Browse,Relate:SUBTRACC,SELF)
  OPEN(window)
  SELF.Opened=True
  ! ================ Set Report Version =====================
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5001'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW12.Q &= Queue:Browse
  BRW12.AddSortOrder(,sub:Account_Number_Key)
  BRW12.AddLocator(BRW12::Sort0:Locator)
  BRW12::Sort0:Locator.Init(,sub:Account_Number,1,BRW12)
  BRW12.SetFilter('(Upper(sub:Region) <<> '''')')
  BIND('tmp:Tag',tmp:Tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW12.AddField(tmp:Tag,BRW12.Q.tmp:Tag)
  BRW12.AddField(sub:Account_Number,BRW12.Q.sub:Account_Number)
  BRW12.AddField(sub:Company_Name,BRW12.Q.sub:Company_Name)
  BRW12.AddField(sub:RecordNumber,BRW12.Q.sub:RecordNumber)
  IF ?tmp:AllAccounts{Prop:Checked} = True
    DISABLE(?List)
  END
  IF ?tmp:AllAccounts{Prop:Checked} = False
    ENABLE(?List)
  END
  BRW12.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tmp:AllAccounts
      IF ?tmp:AllAccounts{Prop:Checked} = True
        DISABLE(?List)
      END
      IF ?tmp:AllAccounts{Prop:Checked} = False
        ENABLE(?List)
      END
      ThisWindow.Reset
      If tmp:AllAccounts
          Post(Event:Accepted,?DasTagAll)
      End ! If tmp:AllAccounts
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Print
      ThisWindow.Update
      Do Reporting
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::13:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Local.DrawBox       Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
Code
    If func:BR = ''
        func:BR = func:TR
    End !If func:BR = ''

    If func:BL = ''
        func:BL = func:TL
    End !If func:BL = ''

    If func:Colour = 0
        func:Colour = oix:ColorWhite
    End !If func:Colour = ''
    E1.SetCellBackgroundColor(func:Colour,func:TL,func:BR)
    E1.SetCellBorders(func:BL,func:BR,oix:BorderEdgeBottom,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:TR,oix:BorderEdgeTop,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:BL,oix:BorderEdgeLeft,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TR,func:BR,oix:BorderEdgeRight,oix:LineStyleContinuous)
local.GetSummaryType        Procedure(Long func:Time)
Code
    If func:Time < 60
        !1 Hour
        Return 'F'
    Elsif func:Time >= 60 And func:Time < 1440
        !24 Hours
        Return 'I'
    Elsif func:Time >= 1440 And func:Time < 2880
        !48 Hours
        Return 'L'
    Elsif func:Time >= 2880 And func:Time < 4320
        !3 days
        Return 'O'
    Elsif func:Time >= 4320 And func:Time < 7200
        !5 Days
        Return 'R'
    Elsif func:Time >= 7200 And func:Time < 10080
        !7 Days
        Return 'U'
    Elsif func:Time >= 10080 And func:Time < 12960
        !9 Days
        Return 'X'
    Elsif func:Time >= 12960
        !Over 9 Days
        Return 'AA'
    End !If func:Time < 60
Local.WriteLine         Procedure()
Code
    filque:AccountNumber = job:Account_Number
    Get(FilenameQueue,filque:AccountNumber)
    If Error()
        Return
    End ! If Error()
    glo:ExportFile = Clip(filque:TempFilename)
    Open(ExportFile)

    ! VCP Job Number
    exp:Line1    = '"' & job:Ref_Number
    Access:TRADEACC.ClearKey(tra:Account_Number_Key)
    tra:Account_Number = wob:HeadAccountNumber
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        !Found
    Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign

    ! Branch Specific Number
    exp:Line1    = Clip(exp:Line1) & '","' & tra:BranchIdentification
    ! Franchise Sent To
    exp:Line1    = Clip(exp:Line1) & '","' & tra:Company_Name

    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = job:Account_Number
    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Found
    Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign

    ! Sub Account Number
    exp:Line1    = Clip(exp:Line1) & '","' & job:Account_Number
    ! Sub Account Name
    exp:Line1    = Clip(exp:Line1) & '","' & sub:Company_Name

    !Manufacturer
    exp:Line1    = Clip(exp:Line1) & '","' & job:Manufacturer
    !Model Number
    exp:Line1    = Clip(exp:Line1) & '","' & job:Model_Number
    !Warranty
    If job:Warranty_Job = 'YES'
        exp:Line1    = Clip(exp:Line1) & '","' & 'YES'
    Else ! If job:Warranty_Job = 'YES'
        exp:Line1    = Clip(exp:Line1) & '","' & 'NO'
    End ! If job:Warranty_Job = 'YES'
    !Jobs Booked In Date
    exp:Line1    = Clip(exp:Line1) & '","' & Format(job:Date_Booked,@d06b)
    !Jobs Booked In Time
    exp:Line1    = Clip(exp:Line1) & '","' & Format(job:Time_Booked,@t01b)
    !Jobs Completed Date
    exp:Line1    = Clip(exp:Line1) & '","' & Format(job:Date_Completed,@d06b)
    !Jobs Time Completed
    exp:Line1    = Clip(exp:Line1) & '","' & Format(job:Time_Completed,@t01b)
    !Customer Perspective Days 465
    !Customer Perspective Time 465
    Time# = BHTimeDifference24Hr(job:Date_Booked, |
                                        tmp:EndOfCustomerPerspectiveDate, |
                                        job:Time_Booked, |
                                        tmp:EndOfCustomerPerspectiveTime)
    Time# -= tmp:TotalEstimateTime

    BHReturnDaysHoursMins(Time#,D#,H#,M#)
    exp:Line1    = Clip(exp:Line1) & '","' & D#
    exp:Line1    = Clip(exp:Line1) & '","' & H# & ':' & M#

    ! Add to queue to show on summary (DBH: 13/09/2007)
    perque:AccountNumber = job:Account_Number
    perque:Type    = local.GetSummaryType(Time#)
    Get(PerspectiveQueue,perque:AccountNumber,perque:Type)
    If Error()
        perque:AccountNumber = job:Account_Number
        perque:Type = local.GetSummaryType(Time#)
        perque:Count = 1
        Add(PerspectiveQueue)
    Else ! If Error()
        perque:Count += 1
        Put(PerspectiveQueue)
    End ! If Error()

    !VCP In Control Days
    !VCP In Control Time
    Time# = tmp:TotalPUPInControl
    BHReturnDaysHoursMins(Time#,D#,H#,M#)
    exp:Line1    = Clip(exp:Line1) & '","' & D#
    exp:Line1    = Clip(exp:Line1) & '","' & H# & ':' & M#

    !Customer Collection Days
    !Customer Collection Time
    Time# = tmp:TotalCustomerCollection
    BHReturnDaysHoursMins(Time#,D#,H#,M#)
    exp:Line1    = Clip(exp:Line1) & '","' & D#
    exp:Line1    = Clip(exp:Line1) & '","' & H# & ':' & M#

    !Wait Time Days
    !Wait Time Time
    Time# = tmp:TotalWaitTime
    BHReturnDaysHoursMins(Time#,D#,H#,M#)
    exp:Line1    = Clip(exp:Line1) & '","' & D#
    exp:Line1    = Clip(exp:Line1) & '","' & H# & ':' & M#

    !Courier Movement Days
    !Courier Movement Time
    Time# = tmp:TotalCourierTime
    BHReturnDaysHoursMins(Time#,D#,H#,M#)
    exp:Line1    = Clip(exp:Line1) & '","' & D#
    exp:Line1    = Clip(exp:Line1) & '","' & H# & ':' & M#

    !RRC / ARC / 3rd Party Time Days
    !RRC / ARC / 3rd Party Time Time
    Time# = tmp:TotalPUPOutOfControl
    BHReturnDaysHoursMins(Time#,D#,H#,M#)
    exp:Line1    = Clip(exp:Line1) & '","' & D#
    exp:Line1    = Clip(exp:Line1) & '","' & H# & ':' & M#

    exp:Line1    = Clip(exp:Line1) & '"'
    Add(ExportFile)
    Close(ExportFile)
local.WriteSummaryLineTotals        Procedure(Long  func:Row)
Code
    !Complete Totals -  (DBH: 03-03-2004)
    E1.WriteToCell('=SUM(F' & func:Row,'F' & func:Row + 1)
    E1.WriteToCell('=SUM(F' & func:Row & ' + I' & func:Row,'I' & func:Row + 1)
    E1.WriteToCell('=SUM(F' & func:Row & ' + I' & func:Row & ' + L' & func:Row,'L' & func:Row + 1)
    E1.WriteToCell('=SUM(F' & func:Row & ' + I' & func:Row & ' + L' & func:Row & ' + O' & func:Row,'O' & func:Row + 1)
    E1.WriteToCell('=SUM(F' & func:Row & ' + I' & func:Row & ' + L' & func:Row & ' + O' & func:Row & ' + R' & func:Row,'R' & func:Row + 1)
    E1.WriteToCell('=SUM(F' & func:Row & ' + I' & func:Row & ' + L' & func:Row & ' + O' & func:Row & ' + R' & func:Row & ' + U' & func:Row,'U' & func:Row + 1)
    E1.WriteToCell('=SUM(F' & func:Row & ' + I' & func:Row & ' + L' & func:Row & ' + O' & func:Row & ' + R' & func:Row & ' + U' & func:Row & ' + X' & func:Row,'X' & func:Row + 1)
    E1.WriteToCell('=SUM(F' & func:Row & ' + I' & func:Row & ' + L' & func:Row & ' + O' & func:Row & ' + R' & func:Row & ' + U' & func:Row & ' + X' & func:Row & ' + AA' & func:Row,'AA' & func:Row + 1)

    E1.SetCellFontStyle('Bold','F' & func:Row + 1,'AB' & func:Row + 1)

    !Satisfied l
    E1.WriteToCell('=(F' & func:Row & '/B' & func:Row & ')* 100','G' & func:Row)
    E1.WriteToCell('=(I' & func:Row & '/B' & func:Row & ')* 100','J' & func:Row)
    E1.WriteToCell('=(L' & func:Row & '/B' & func:Row & ')* 100','M' & func:Row)
    E1.WriteToCell('=(O' & func:Row & '/B' & func:Row & ')* 100','P' & func:Row)
    E1.WriteToCell('=(R' & func:Row & '/B' & func:Row & ')* 100','S' & func:Row)
    E1.WriteToCell('=(U' & func:Row & '/B' & func:Row & ')* 100','V' & func:Row)
    E1.WriteToCell('=(X' & func:Row & '/B' & func:Row & ')* 100','Y' & func:Row)
    E1.WriteToCell('=(AA' & func:Row & '/B' & func:Row & ')* 100','AB' & func:Row)

    !Satisfied Totals
    E1.WriteToCell('=SUM(G' & func:Row,'G' & func:Row + 1)
    E1.WriteToCell('=SUM(G' & func:Row & ' + J' & func:Row,'J' & func:Row + 1)
    E1.WriteToCell('=SUM(G' & func:Row & ' + J' & func:Row & ' + M' & func:Row,'M' & func:Row + 1)
    E1.WriteToCell('=SUM(G' & func:Row & ' + J' & func:Row & ' + M' & func:Row & ' + P' & func:Row,'P' & func:Row + 1)
    E1.WriteToCell('=SUM(G' & func:Row & ' + J' & func:Row & ' + M' & func:Row & ' + P' & func:Row & ' + S' & func:Row,'S' & func:Row + 1)
    E1.WriteToCell('=SUM(G' & func:Row & ' + J' & func:Row & ' + M' & func:Row & ' + P' & func:Row & ' + S' & func:Row & ' + V' & func:Row,'V' & func:Row + 1)
    E1.WriteToCell('=SUM(G' & func:Row & ' + J' & func:Row & ' + M' & func:Row & ' + P' & func:Row & ' + S' & func:Row & ' + V' & func:Row & ' + Y' & func:Row,'Y' & func:Row + 1)
    E1.WriteToCell('=SUM(G' & func:Row & ' + J' & func:Row & ' + M' & func:Row & ' + P' & func:Row & ' + S' & func:Row & ' + V' & func:Row & ' + Y' & func:Row & ' + AB' & func:Row,'AB' & func:Row + 1)

    !Draw Box
    local.DrawBox('F' & func:Row + 1,'G' & func:Row + 1,'F' & func:Row + 1,'G' & func:Row + 1,color:Silver)
    local.DrawBox('I' & func:Row + 1,'J' & func:Row + 1,'I' & func:Row + 1,'J' & func:Row + 1,color:Silver)
    local.DrawBox('L' & func:Row + 1,'M' & func:Row + 1,'L' & func:Row + 1,'M' & func:Row + 1,color:Silver)
    local.DrawBox('O' & func:Row + 1,'P' & func:Row + 1,'O' & func:Row + 1,'P' & func:Row + 1,color:Silver)
    local.DrawBox('R' & func:Row + 1,'S' & func:Row + 1,'R' & func:Row + 1,'S' & func:Row + 1,color:Silver)
    local.DrawBox('U' & func:Row + 1,'V' & func:Row + 1,'U' & func:Row + 1,'V' & func:Row + 1,color:Silver)
    local.DrawBox('X' & func:Row + 1,'Y' & func:Row + 1,'X' & func:Row + 1,'Y' & func:Row + 1,color:Silver)
    local.DrawBox('AA' & func:Row + 1,'AB' & func:Row + 1,'AA' & func:Row + 1,'AB' & func:Row + 1,color:Silver)
local.WriteSummarySectionTotal  Procedure(Long func:Row)
local:FormulaString     String(255)
local:FormulaString2    String(255)
Code
    E1.WriteToCell('Sub Totals','A' & func:Row + 2)
    E1.WriteToCell('=SUM(B11' & ':B' & func:Row & ')','B' & func:Row + 2)
    E1.WriteToCell('=SUM(C11' & ':C' & func:Row & ')','C' & func:Row + 2)
    E1.WriteToCell('=SUM(D11' & ':D' & func:Row & ')','D' & func:Row + 2)

    E1.WriteToCell('Average','A' & func:Row + 3)

    local.DrawBox('F' & func:Row + 3,'G' & func:Row + 3,'F' & func:Row + 3,'G' & func:Row + 3,color:Silver)
    local.DrawBox('I' & func:Row + 3,'J' & func:Row + 3,'I' & func:Row + 3,'J' & func:Row + 3,color:Silver)
    local.DrawBox('L' & func:Row + 3,'M' & func:Row + 3,'L' & func:Row + 3,'M' & func:Row + 3,color:Silver)
    local.DrawBox('O' & func:Row + 3,'P' & func:Row + 3,'O' & func:Row + 3,'P' & func:Row + 3,color:Silver)
    local.DrawBox('R' & func:Row + 3,'S' & func:Row + 3,'R' & func:Row + 3,'S' & func:Row + 3,color:Silver)
    local.DrawBox('U' & func:Row + 3,'V' & func:Row + 3,'U' & func:Row + 3,'V' & func:Row + 3,color:Silver)
    local.DrawBox('X' & func:Row + 3,'Y' & func:Row + 3,'X' & func:Row + 3,'Y' & func:Row + 3,color:Silver)
    local.DrawBox('AA' & func:Row + 3,'AB' & func:Row + 3,'AA' & func:Row + 3,'AB' & func:Row + 3,color:Silver)

    E1.SetCellFontStyle('Bold','A' & func:Row + 2,'AB' & func:Row + 3)

    Loop column# = 6 to 27 By 3
        !Summary Totals Inc ARC
        local:FormulaString = Clip(E1.GetColumnAddressFromColumnNumber(column#)) & 12
        local:FormulaString2 = 'B11'
        Loop x# = (11 + 3) to (func:Row+1) By 2
            local:FormulaString = Clip(local:FormulaString) & ' + ' & Clip(E1.GetColumnAddressFromColumnNumber(column#)) & x#
            local:FormulaString2 = Clip(local:FormulaString2) & ' + B' & (x# - 1)
        End !Loop x# = 11 to func:Row Step 2
        E1.WriteToCell('=((' & Clip(local:FormulaString) & ')/(' & Clip(local:FormulaString2) & ')) * 100',Clip(E1.GetColumnAddressFromColumnNumber(column# + 1)) & func:Row + 3)

    End !x# = 7 to 37
    local.DrawBox('A' & func:Row + 2,'E' & func:Row + 2,'A' & func:Row + 2,'E' & func:Row + 2,color:Silver)
    local.DrawBox('A' & func:Row + 3,'E' & func:Row + 3,'A' & func:Row + 3,'E' & func:Row + 3,color:Silver)
Local.UpdateProgressWindow      Procedure(String    func:Text)
Code
    staque:StatusMessage = Clip(func:Text)
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))
    Display()
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
!Initialise
ExcelSetup          Procedure(Byte      func:Visible)
    Code
    Excel   = Create(0,Create:OLE)
    Excel{prop:Create} = 'Excel.Application'
    Excel{'ASYNC'}  = False
    Excel{'Application.DisplayAlerts'} = False

    Excel{'Application.ScreenUpdating'} = func:Visible
    Excel{'Application.Visible'} = func:Visible
    Excel{'Application.Calculation'} = 0FFFFEFD9h
    excel:ActiveWorkBook    = Excel{'ActiveWorkBook'}
    Yield()

!Make a WorkBook
ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    excel:ActiveWorkBook = Excel{'Application.Workbooks.Add()'}

    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Title")'} = func:Title
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Author")'} = func:Author
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Application Name")'} = func:AppName

    excel:Selected = Excel{'Sheets("Sheet2").Select'}
    Excel{prop:Release} = excel:Selected

    Excel{'ActiveWindow.SelectedSheets.Delete'}

    excel:Selected = Excel{'Sheets("Sheet1").Select'}
    Excel{prop:Release} = excel:Selected
    Yield()

ExcelMakeSheet      Procedure()
ActiveWorkBook  CString(20)
    Code
    ActiveWorkBook = Excel{'ActiveWorkBook'}

    Excel{ActiveWorkBook & '.Sheets("Sheet3").Select'}
    Excel{prop:Release} = ActiveWorkBook

    Excel{excel:ActiveWorkBook & '.Sheets.Add'}
    Yield()
!Select A Sheet
ExcelSelectSheet    Procedure(String    func:SheetName)
    Code
    Excel{'Sheets("' & Clip(func:SheetName) & '").Select'}
    Yield()
!Setup Sheet Type (P = Portrait, L = Lanscape)
ExcelSheetType      Procedure(String    func:Type)
    Code
    Case func:Type
        Of 'L'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 2
        Of 'P'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 1
    End !Case func:Type
    Excel{'ActiveSheet.PageSetup.FitToPagesWide'}  = 1
    Excel{'ActiveSheet.PageSetup.FitToPagesTall'}  = 9999
    Excel{'ActiveSheet.PageSetup.Order'}  = 2

    Yield()
ExcelHorizontal     Procedure(String    func:Direction)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Centre'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFF4h
        Of 'Left'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFDDh
        Of 'Right'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFC8h
    End !Case tmp:Direction
    Excel{prop:Release} = Selection
    Yield()
ExcelVertical   Procedure(String func:Direction)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Top'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFC0h
        Of 'Centre'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF4h
        Of 'Bottom'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF5h
    End ! Case func:Direction
    Excel{prop:Release} = Selection
    Yield()

ExcelCell   Procedure(String    func:Text,Byte    func:Bold)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    If func:Bold
        Excel{Selection & '.Font.Bold'} = True
    Else
        Excel{Selection & '.Font.Bold'} = False
    End !If func:Bold
    Excel{prop:Release} = Selection

    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Formula'}  = func:Text
    Excel{excel:Selected & '.Offset(0, 1).Select'}
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatCell     Procedure(String    func:Format)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.NumberFormat'} = func:Format
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatRange    Procedure(String    func:Range,String   func:Format)
Selection       Cstring(20)
    Code

    ExcelSelectRange(func:Range)
    Selection   = Excel{'Selection'}
    Excel{Selection & '.NumberFormat'} = func:Format
    Excel{prop:Release} = Selection
    Yield()

ExcelNewLine    Procedure(Long func:Number)
    Code
    Loop excelloop# = 1 to func:Number
        ExcelSelectRange('A' & (ExcelCurrentRow() + 1))
    End !Loop excelloop# = 1 to func:Number
    !excel:Selected = Excel{'ActiveCell'}
    !Excel{excel:Selected & '.Offset(0, -' & Excel{excel:Selected & '.Column'} - 1 & ').Select'}
    !Excel{excel:Selected & '.Offset(1, 0).Select'}
    !Excel{prop:Release} = excel:Selected
    Yield()

ExcelMoveDown   Procedure()
    Code
    ExcelSelectRange(ExcelCurrentColumn() & (ExcelCurrentRow() + 1))
    Yield()
!Set Column Width

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").ColumnWidth'} = func:Width
    Yield()
ExcelCellWidth          Procedure(Long  func:Width)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.ColumnWidth'} = func:Width
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelAutoFit            Procedure(String func:Range)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").Columns.AutoFit'}
    Yield()
!Set Gray Box

ExcelGrayBox            Procedure(String    func:Range)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Excel{Selection & '.Interior.ColorIndex'} = 15
    Excel{Selection & '.Interior.Pattern'} = 1
    Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(7).LineStyle'} = 1
    Excel{Selection & '.Borders(7).Weight'} = 2
    Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(10).LineStyle'} = 1
    Excel{Selection & '.Borders(10).Weight'} = 2
    Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(8).LineStyle'} = 1
    Excel{Selection & '.Borders(8).Weight'} = 2
    Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(9).LineStyle'} = 1
    Excel{Selection & '.Borders(9).Weight'} = 2
    Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    Excel{prop:Release} = Selection
    Yield()
ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)
Selection   Cstring(20)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Selection = Excel{'Selection'}
    If func:Colour
        Excel{Selection & '.Interior.ColorIndex'} = func:Colour
        Excel{Selection & '.Interior.Pattern'} = 1
        Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    End !If func:Colour
    If func:Left
        Excel{Selection & '.Borders(7).LineStyle'} = 1
        Excel{Selection & '.Borders(7).Weight'} = 2
        Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    End !If func:Left

    If func:Right
        Excel{Selection & '.Borders(10).LineStyle'} = 1
        Excel{Selection & '.Borders(10).Weight'} = 2
        Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    End !If func:Top

    If func:Top
        Excel{Selection & '.Borders(8).LineStyle'} = 1
        Excel{Selection & '.Borders(8).Weight'} = 2
        Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    End !If func:Right

    If func:Bottom
        Excel{Selection & '.Borders(9).LineStyle'} = 1
        Excel{Selection & '.Borders(9).Weight'} = 2
        Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    End !If func:Bottom
    Excel{prop:Release} = Selection
    Yield()
!Select a range of cells
ExcelSelectRange        Procedure(String    func:Range)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Yield()
!Change font size
ExcelFontSize           Procedure(Byte  func:Size)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Font.Size'}   = func:Size
    Excel{prop:Release} = excel:Selected
    Yield()
!Sheet Name
ExcelSheetName          Procedure(String    func:Name)
    Code
    Excel{'ActiveSheet.Name'} = func:Name
    Yield()
ExcelAutoFilter         Procedure(String    func:Range)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.AutoFilter'}
    Excel{prop:Release} = Selection
    Yield()
ExcelDropAllSheets      Procedure()
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    Loop While Excel{'WorkBooks.Count'} > 0
        excel:ActiveWorkBook = Excel{'ActiveWorkBook'}
        Excel{'ActiveWorkBook.Close(1)'}
        Excel{prop:Release} = excel:ActiveWorkBook
    End !Loop While Excel{'WorkBooks.Count'} > 0
    Yield()
ExcelClose              Procedure()
!xlCalculationAutomatic
    Code
    Excel{'Application.Calculation'}= 0FFFFEFF7h
    Excel{'Application.Quit'}
    Excel{prop:Deactivate}
    Destroy(Excel)
    Yield()
ExcelDeleteSheet        Procedure(String    func:SheetName)
    Code
    ExcelSelectSheet(func:SheetName)
    Excel{'ActiveWindow.SelectedSheets.Delete'}
    Yield()
ExcelSaveWorkBook       Procedure(String    func:Name)
    Code
    Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(func:Name)) & '")'}
    Excel{'Application.ActiveWorkBook.Close()'}
   Excel{'Application.Calculation'} = 0FFFFEFF7h
    Excel{'Application.Quit'}

    Excel{PROP:DEACTIVATE}
    YIELD()
ExcelFontColour         Procedure(String    func:Range,Long func:Colour)
    Code
    !16 = Gray
    ExcelSelectRange(func:Range)
    Excel{'Selection.Font.ColorIndex'} = func:Colour
    Yield()
ExcelWrapText           Procedure(String    func:Range,Byte func:True)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.WrapText'} = func:True
    Excel{prop:Release} = Selection
    Yield()
ExcelCurrentColumn      Procedure()
CurrentColumn   String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentColumn = Excel{excel:Selected & '.Column'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentColumn

ExcelCurrentRow         Procedure()
CurrentRow      String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentRow = Excel{excel:Selected & '.Row'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentRow

ExcelPasteSpecial       Procedure(String    func:Range)
Selection       CString(20)
    Code
    ExcelSelectRange(func:Range)
    Selection   = Excel{'ActiveCell'}
    Excel{Selection & '.PasteSpecial'}
    Excel{prop:Release} = Selection
    Yield()

ExcelConvertFormula     Procedure(String    func:Formula)
    Code
    Return Excel{'Application.ConvertFormula("' & Clip(func:Formula) & '",' & 0FFFFEFCAh & ',' & 1 & ')'}

ExcelGetFilename        Procedure(Byte  func:DontAsk)
sav:Path        CString(255)
func:Desktop     CString(255)
    Code

        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export'

        !Does the Export Folder already Exists?
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                Return Level:Fatal

            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        Error# = 0
        sav:Path = Path()
        SetPath(func:Desktop)

        func:Desktop = Clip(func:Desktop) & '\' & CLIP(Excel:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'

        If func:DontAsk = False
            IF NOT FILEDIALOG('Save Spreadsheet', func:Desktop, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
                Error# = 1
            End!IF NOT FILEDIALOG('Save Spreadsheet', tmp:Desktop, 'Microsoft Excel Workbook|*.XLS', |
        End !If func:DontAsk = True

        SetPath(sav:Path)

        If Error#
            Return Level:Fatal
        End !If Error#
        excel:FileName    = func:Desktop
        Return Level:Benign

ExcelGetDirectory       Procedure()
sav:Path        CString(255)
func:Desktop    CString(255)
    Code
        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export\'
        !Does the Export Folder already Exists?
        Error# = 0
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                If Not FileDialog('Save Spreadsheet To Folder', func:Desktop, ,FILE:KeepDir+ File:Save + File:NoError + File:LongName + File:Directory)
                    Return Level:Fatal
                End !+ File:LongName + File:Directory)
            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        excel:FileName  = func:Desktop
        Return Level:Benign

ExcelColumnLetter     Procedure(Long func:ColumnNumber)
local:Over26        Long()
Code
    local:Over26 = 0
    If func:ColumnNumber > 26
        Loop Until func:ColumnNumber <= 26
            local:Over26 += 1
            func:ColumnNumber -= 26
        End !Loop Until ColumnNumber <= 26.
    End !If func:ColumnNumber > 26

    If local:Over26 > 26
        Stop('ExcelColumnLetter Procedure Out Of Range!')
    End !If local:Over26 > 26
    If local:Over26 > 0
        Return Clip(CHR(local:Over26 + 64)) & Clip(CHR(func:ColumnNumber + 64))
    Else !If local:Over26 > 0
        Return Clip(CHR(func:ColumnNumber + 64))
    End !If local:Over26 > 0
ExcelOpenDoc        Procedure(String func:FileName)
Code
    Excel{'Workbooks.Open("' & Clip(func:FileName) & '")'}
ExcelFreeze         Procedure(String func:Cell)
Code
    Excel{'Range("' & Clip(func:Cell) & '").Select'}
    Excel{'ActiveWindow.FreezePanes'} = True

BRW12.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sub:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:Tag = ''
    ELSE
      tmp:Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Tag = '*')
    SELF.Q.tmp:Tag_Icon = 2
  ELSE
    SELF.Q.tmp:Tag_Icon = 1
  END


BRW12.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW12.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW12.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW12::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW12::RecordStatus=ReturnValue
  IF BRW12::RecordStatus NOT=Record:OK THEN RETURN BRW12::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sub:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::13:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW12::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW12::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW12::RecordStatus
  RETURN ReturnValue

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
