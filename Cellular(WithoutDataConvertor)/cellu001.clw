

   MEMBER('cellular.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELLU001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
LocalRequest         LONG
Debugfile            STRING(255)
tmp:VersionNumber    STRING(30)
Tmp:UseScheduledMinStock BYTE(0)
save_tra_id          USHORT,AUTO
save_sub_id          USHORT,AUTO
tmp:PopupString      CSTRING(255)
save_job_id          USHORT,AUTO
start_help_temp      STRING(255)
pos                  STRING(255)
loc:lastcount        LONG
loc:lasttime         LONG
FilesOpened          BYTE
save_ope_id          USHORT,AUTO
save_sup_id          USHORT,AUTO
CurrentTab           STRING(80)
FP::PopupString      STRING(4096)
FP::ReturnValues     QUEUE,PRE(FP:)
RVPtr                LONG
                     END
comp_name_temp       STRING(255)
AllowUserToExit      BYTE
Date_Temp            STRING(60)
Time_Temp            STRING(60)
SRCF::Version        STRING(512)
SRCF::Format         STRING(512)
CPCSExecutePopUp     BYTE
BackerDDEName        STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
FrameTitle           CSTRING(128)
OtherPgmHwnd         Hwnd !Handle=unsigned=hwnd
AppFrame             WINDOW('ServiceBase 2000 Cellular Progress System'),AT(0,0,680,429),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),ICON('Cellular3g.ico'),WALLPAPER('sbwall1.jpg'),CENTERED,GRAY,DOUBLE,IMM
                       MENUBAR
                         MENU('File'),USE(?File),FIRST
                           ITEM('&Print Setup ...'),USE(?PrintSetup),MSG('Setup printer'),STD(STD:PrintSetup)
                           ITEM,SEPARATOR
                           ITEM('&Re-Login'),USE(?ReLogin)
                           ITEM,SEPARATOR
                           ITEM('E&xit'),USE(?Exit),MSG('Exit this application'),STD(STD:Close)
                         END
                         MENU('&System Administration'),USE(?SystemAdministration)
                           MENU('System Defaults'),USE(?SystemDefaults)
                             ITEM('License Defaults'),USE(?LicenseDefaults)
                             MENU('General Defaults'),USE(?GeneralDefaults)
                               ITEM('Main Defaults'),USE(?MainDefaults)
                               ITEM('Estimate Defaults'),USE(?EstimateDefaults)
                               ITEM('Compulsory Fields'),USE(?CompulsoryFields)
                               ITEM('Standard Texts'),USE(?StandardTexts)
                               ITEM('Printing Defaults'),USE(?PrintingDefaults)
                               ITEM('R.R.C. Defaults'),USE(?RRCDefaults)
                               ITEM('Bastion Defaults'),USE(?BastionDefaults)
                               ITEM('Booking Defaults'),USE(?BookingDefaults)
                               ITEM('Browse Defaults'),USE(?BrowseDefaults)
                               MENU('Email Defaults'),USE(?EmailDefaults)
                                 ITEM('Server Defaults'),USE(?ServerDefaults)
                                 ITEM('Recipient Types'),USE(?RecipientTypes)
                               END
                               ITEM,SEPARATOR
                               ITEM('Stock Control Defaults'),USE(?StockControlDefaults)
                               ITEM('Retail Sales Defaults'),USE(?RetailSalesDefaults)
                             END
                             MENU('Import Defaults'),USE(?SystemAdminstrationSystemDefaultsImportDefaults),HIDE
                               ITEM('Siemens Defaults'),USE(?SiemensDefaults),HIDE
                               ITEM('Web Defaults'),USE(?WebDefaults)
                             END
                             ITEM('Unlock SBOnline Jobs'),USE(?UnlockSBOnlineJobs)
                           END
                           MENU('User Defaults'),USE(?UserDefaults)
                             ITEM('Access Levels'),USE(?accesslevels)
                             ITEM('System Users'),USE(?users)
                           END
                           MENU('Financial Defaults'),USE(?FinancialDefaults)
                             ITEM('Charge Rate Defaults'),USE(?ChargeRateDefaults),MSG('Browse Default Charges')
                             ITEM('Charge Types'),USE(?ChargeTypes)
                             ITEM('Repair Types'),USE(?RepairTypes)
                             ITEM('V.A.T. Codes'),USE(?VATCodes)
                             ITEM('Vetting Reasons'),USE(?BrowseVettingReasons)
                             MENU('Utilities'),USE(?SystemAdminstrationFinancialDefaultsUtilities)
                               ITEM('Reprice Jobs Routine'),USE(?RepriceJobsRoutine)
                             END
                           END
                           MENU('Stock Control Defaults'),USE(?SystemAdminstrationStockControlDefaults)
                             ITEM('Manufacturers'),USE(?manufacturers)
                             ITEM('Suppliers'),USE(?Suppliers)
                             ITEM('Site Locations'),USE(?SiteLocations)
                             ITEM('Internal Locations'),USE(?InternalLocations)
                             ITEM('Loan/Exchange Stock Type'),USE(?LoanExchangeStockType)
                             ITEM('Price Bands'),USE(?SystemAdministrationStockControlDefaultsPriceBa)
                           END
                           MENU('Trade Account Defaults'),USE(?TradeAccountDefaults)
                             ITEM('Browse Trade Header Accounts'),USE(?TradeAccounts),MSG('Browse Trade Accounts')
                             ITEM('Browse Trade Sub Accounts'),USE(?BrowseTradeSubAccounts)
                             ITEM('Browse Default Hubs'),USE(?BrowseDefaultHubs)
                             ITEM('Browse Default Regions'),USE(?BrowseDefaultRegions)
                             MENU('Trade Account Utilities'),USE(?TradeAccountUtilities)
                               ITEM('Auto-Change Courier'),USE(?AutoChangeCourier)
                               ITEM('WIP Report Statuses'),USE(?SystemAdministrationTradeAccountDefaultsTradeAc)
                             END
                           END
                           MENU('General Lookups'),USE(?Lookups)
                             ITEM('Accessories'),USE(?accessories)
                             ITEM('Action'),USE(?Action)
                             ITEM('Colours'),USE(?Colours)
                             ITEM('Couriers'),USE(?couriers)
                             ITEM('Estimate Rejection Reasons'),USE(?EstimateRejectionReasons)
                             ITEM('Job Turnaround Times'),USE(?JobTurnaroundTimes)
                             ITEM('Initial Transit Types'),USE(?InitialTransitTypes)
                             ITEM('Letters'),USE(?Letters)
                             ITEM('Mail Merge Letters'),USE(?MailMergeLetters)
                             ITEM('Networks'),USE(?Networks)
                             ITEM('OBF Rejection Reasons'),USE(?OBFRejectionReasons)
                             ITEM('Payment Types'),USE(?PaymentTypes)
                             ITEM('QA Failure Reasons'),USE(?QAFailureReasons)
                             ITEM('Return Types'),USE(?SystemAdministrationGeneralLookupsReturnTypes)
                             MENU('Status Types'),USE(?SystemAdminstrationGeneralLookupsStatusTypes)
                               ITEM('Status Types'),USE(?StatusTypes)
                               ITEM('Turnaround Time Utility'),USE(?TurnaroundTimeUtility)
                             END
                             ITEM('Suburbs'),USE(?BrowseSuburbs)
                             ITEM('Teams'),USE(?Teams)
                             ITEM('Third Party Repairers'),USE(?thirdPartyRepairers)
                             ITEM('Third Party Repair Types'),USE(?LookupThirdPartyRepairTypes)
                             ITEM('Unit Types'),USE(?UnitTypes)
                             ITEM('SMS Functionality'),USE(?SystemAdministrationGeneralLookupsSMSFunctional)
                           END
                         END
                         MENU('&Browse'),USE(?Browse)
                           ITEM('Contacts'),USE(?Contacts)
                           MENU('Loan Units'),USE(?BrowseLoanUnits)
                             ITEM('Browse Available Stock'),USE(?LoanUnits)
                             ITEM('Process Loan Sales'),USE(?RapidLoanTransfer)
                             ITEM('VCP Loan Audits'),USE(?BrowseLoanUnitsVCPLoanAudits)
                           END
                           MENU('Exchange Units'),USE(?BrowseExchangeUnits)
                             ITEM('By Available Stock'),USE(?ExchangeUnits)
                             ITEM('By Audit Number'),USE(?ByAuditNumber)
                             ITEM,SEPARATOR
                             ITEM('Rapid Exchange Transfer'),USE(?RapidExchangeTransfer)
                             ITEM('Rapid Exchange Transfer 48 Hour'),USE(?RapidExchangeTransfer48Hour)
                             ITEM,SEPARATOR
                             ITEM('Rapid Insert Wizard'),USE(?RapidInsertWizard)
                             ITEM('Minimum Stock Routine'),USE(?BrowseExchangeUnitsMinimum),HIDE
                           END
                           ITEM('I.M.E.I. Number To Model Correlation'),USE(?ESNToModelCorrelation)
                           ITEM('Bouncers'),USE(?Bouncers)
                           ITEM('Engineers Status'),USE(?EngineersStatus)
                         END
                         MENU('U&tilities'),USE(?Utilities)
                           ITEM('Internal E-mail'),USE(?InternalEmail)
                           ITEM('Users Logged In'),USE(?UsersLoggedIn)
                           ITEM('Clear Audit Trail'),USE(?ClearAuditTrail)
                           ITEM('Re-Submit For Despatch'),USE(?ReDespatch)
                           ITEM('Change Perceived Site Location'),USE(?ChangePerceivedSiteLocation)
                           ITEM('Release Locked Records'),USE(?UtilitiesReleaseLockedRecords),HIDE
                           ITEM('Resubmit XML Messages'),USE(?UtilitiesResubmitXMLMessages)
                         END
                         MENU('&Procedures'),USE(?Procedures)
                           ITEM('Warranty Process'),USE(?WarrantyProcess)
                           MENU('Invoicing'),USE(?Invoicing)
                             ITEM('Browse Invoices'),USE(?Invoices)
                             ITEM('Second Year Warranty Invoicing'),USE(?SecondYearWarrantyInvoicing)
                           END
                           MENU('Stock Control'),USE(?ProceduresStockControl)
                             ITEM('Goods Received Notes'),USE(?GoodsReceivedNotes)
                             ITEM('Minimum Stock Level'),USE(?MinimumStockLevel)
                             ITEM('Stock Requisitions'),USE(?StockRequisitions)
                             ITEM('Pending Orders'),USE(?PendingOrders)
                             ITEM,SEPARATOR
                             ITEM('Receive Orders'),USE(?Receiving)
                             ITEM('Returned Orders'),USE(?ProceduresStockControlReturnedOrders)
                             ITEM,SEPARATOR
                             ITEM('Replicate Main Store Stock'),USE(?ReplicateMainStoreStock)
                           END
                           MENU('Rapid Letter Printing'),USE(?ProceduresMailMerge)
                             ITEM('Standard Letter Printing'),USE(?RapidLetterPrinting)
                           END
                           MENU('Search Facilities'),USE(?SearchFacilities)
                             ITEM('Search Accessories'),USE(?SearchAccessories)
                           END
                         END
                         MENU('&Reports'),USE(?Reports)
                           ITEM('Status Report'),USE(?StatusReport)
                           MENU('Courier Reports'),USE(?CourierReports)
                             ITEM('Exchange Exceptions Report'),USE(?ExchangeExceptionsReport)
                             ITEM('Exchange Claims Report'),USE(?ExchangeClaimsReport)
                             ITEM('Courier Collection Report'),USE(?CourierCollectionReport),HIDE
                             ITEM('Special Delivery Report'),USE(?SpecialDeliveryReport)
                             ITEM('Exchange Unit Audit Report'),USE(?ExchangeUnitAuditReport)
                             ITEM('Overdue Loans Report'),USE(?OverdueLoansReport)
                           END
                           MENU('Stock Reports'),USE(?ReportsStockReports)
                             MENU('Stock Value Report'),USE(?StockValueReport)
                               ITEM('Summary'),USE(?stockValueReportSummary)
                               ITEM('Detailed'),USE(?StockValueReportDetailed)
                             END
                             ITEM('Stock Check Report'),USE(?StockCheckReport)
                             ITEM('Stock Usage Report'),USE(?StockUsageReport)
                             ITEM('Parts Not Received Report'),USE(?PartsNotReceivedReport)
                             ITEM('Stock Forecast Report'),USE(?StockForecastReport)
                             ITEM('Job Pending Parts Report'),USE(?JobPendingPartsReport),HIDE
                             ITEM('QA Failure Report'),USE(?QAFailureReport)
                             ITEM('Spares Received Report'),USE(?SparesReceivedReport),HIDE
                           END
                           MENU('Retail Reports'),USE(?RetailReports)
                             ITEM('Retail Sales Valuation Report'),USE(?RetailSalesValuationReport)
                           END
                           MENU('Income Reports'),USE(?ReportsIncomeReports)
                             ITEM('Income Report'),USE(?IncomeReport)
                             ITEM('Chargeable Income Report'),USE(?JobIncomeReport)
                             ITEM('Warranty Income Report'),USE(?WarrantyIncomeReport)
                           END
                           MENU('Status Reports'),USE(?ReportsStatusReports)
                             ITEM('Overdue Status Report'),USE(?OverdueStatusReport)
                             ITEM('Repair/Exchange Report'),USE(?RepairExchangeReport)
                             ITEM('Customer Enquiry Report'),USE(?CustomerEnquiryReport)
                             ITEM('Colour Report'),USE(?ColourReport)
                             ITEM('Loan Unit Report'),USE(?LoanUnitReport)
                           END
                         END
                         MENU('Retail Sales'),USE(?RetailSales)
                           ITEM('Insert Retail Sales'),USE(?InsertRetailSales)
                           ITEM('Pending Web Orders'),USE(?PendingWebOrders)
                           ITEM('Outstanding Stock Orders'),USE(?RetailSalesOutstandingStockOrders)
                           ITEM('Process Web Orders'),USE(?WebOrders)
                           ITEM('Adjusted Web Order Receipts'),USE(?RetailSalesAdjustedWebOrderReceipts)
                           ITEM,SEPARATOR
                           ITEM('Picking Reconciliation'),USE(?RetailSalesPickingReconciliation)
                           ITEM('Sales Awaiting Payment'),USE(?SalesAwaitingPayment)
                           ITEM('Despatch Procedure'),USE(?RetailSalesDespatchProcedure)
                           ITEM('Back Order Processing'),USE(?RetailBackOrderProcessing)
                           ITEM('Browse All Sales'),USE(?RetailSalesBrowseAllSales)
                         END
                         ITEM('About'),USE(?About)
                       END
                       STRING('SRN: 0000000'),AT(624,2),USE(?SRNNumber),FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                       IMAGE('logomain.jpg'),AT(218,4),USE(?Image2),HIDE,CENTERED
                       BUTTON,AT(632,318),USE(?Exit:2),TRN,FLAT,LEFT,ICON('exitverw.jpg'),STD(STD:Close)
                       GROUP('Standard Procedures'),AT(244,359,364,42),USE(?Group:StandardProcedures),BOXED,TRN,FONT(,,0101010H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                         BUTTON,AT(348,366),USE(?Browse_Jobs_Button),TRN,FLAT,FONT(,,,FONT:bold),ICON('jobprogw.jpg')
                         BUTTON,AT(436,366),USE(?Reports:2),FLAT,FONT(,,,FONT:bold),ICON('cusrepw.jpg')
                         BUTTON,AT(524,366),USE(?StockControl),TRN,FLAT,FONT(,,,FONT:bold),ICON('stoconw.jpg')
                         BUTTON,AT(256,366),USE(?ButtonRecVPJ),TRN,FLAT,ICON('RecVPJ.jpg')
                       END
                       BUTTON,AT(636,13),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpw.jpg')
                       GROUP('Rapid Procedures'),AT(68,314,540,42),USE(?Group:RapidProcedures),BOXED,TRN,FONT(,,0101010H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                         BUTTON,AT(76,322),USE(?Button7),TRN,FLAT,ICON('newjobw.jpg')
                         BUTTON,AT(168,322),USE(?Button8),TRN,FLAT,ICON('jobserw.jpg')
                         BUTTON,AT(256,322),USE(?Button:SingleIMEISearch),TRN,FLAT,ICON('sinimeiw.jpg')
                         BUTTON,AT(524,322),USE(?RapidProcedures),TRN,FLAT,FONT(,,,FONT:bold),ICON('menuw.jpg')
                         BUTTON,AT(436,322),USE(?Button:IMEIValidation),TRN,FLAT,ICON('validw.jpg')
                         BUTTON,AT(348,322),USE(?Button:GlobalIMEISearch),TRN,FLAT,ICON('gloimeiw.jpg')
                       END
                       IMAGE('passlong.jpg'),AT(76,364),USE(?Image1),CENTERED
                       PANEL,AT(84,370,128,14),USE(?Panel1),FILL(COLOR:White),BEVEL(-1,-1)
                       PROMPT('User:'),AT(76,394,147,10),USE(?UserName:Prompt),CENTER,FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                       PROMPT('Current Version:'),AT(76,404,147,10),USE(?CurrentVersion:Prompt),CENTER,FONT(,7,010101H,FONT:bold),COLOR(COLOR:White)
                       PROMPT('Location'),AT(88,372,120,10),USE(?UserLocation:Prompt),CENTER,FONT(,10,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                     END

TempFilePath         CSTRING(255)
!Shell Execute Variables
AssocFile    CString(255)
Operation    CString(255)
Param        CString(255) 
Dir          CString(255)
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
Login      Routine

    !message('At the start of the login routine')
    glo:select1 = 'N'

    Insert_Password
    If glo:select1 = 'Y'

        !TB13240 - entry freezing - add debugging code
        LinePrint('---Z',Debugfile)
        Halt

    Else

        !TB13240 - entry freezing - add debugging code
        LinePrint(glo:UserCode&'A',Debugfile)

        Access:USERS.Open
        Access:USERS.UseFile

        Access:DEFAULTS.Open
        Access:DEFAULTS.UseFile()

        Access:USERS.ClearKey(use:Password_Key)
        use:Password = glo:Password
        Access:USERS.Fetch(use:Password_Key)

        Set(DEFAULTS)
        Access:DEFAULTS.Next()

        ?UserName:Prompt{prop:Text} = 'User: ' & Capitalize(Clip(use:forename) & ' ' & Clip(use:surname))
        ?UserLocation:Prompt{Prop:Text} = use:Location

        !TB13240 - entry freezing - add debugging code
        LinePrint(glo:UserCode&'B',Debugfile)

        Do Security_Check
        Access:USERS.Close
        Access:DEFAULTS.Close

        !TB13240 - entry freezing - add debugging code
        LinePrint(glo:UserCode&'C',Debugfile)

        !TB13240 - entry freezing - add debugging code
        LinePrint(glo:UserCode&'D',Debugfile)

    End
    glo:select1 = ''
Log_out      Routine
    access:users.open
    access:users.usefile
    access:logged.open
    access:logged.usefile
    access:users.clearkey(use:password_key)
    use:password = glo:password
    if access:users.fetch(use:password_key) = Level:Benign
        access:logged.clearkey(log:user_code_key)
        log:user_code = use:user_code
        log:time      = glo:TimeLogged
        if access:logged.fetch(log:user_code_key) = Level:Benign
            delete(logged)
            If Error()
                Stop(Error())
            End !If Error()
        end !if
    end !if
    access:users.close
    access:logged.close
Security_Check      Routine
!!! - FILE - !!!
    Access:DEFAULTS.Open()
    Access:DEFAULTS.UseFile()

    Access:USERS_ALIAS.Open()
    Access:USERS_ALIAS.UseFile()
    Access:ACCAREAS_ALIAS.Open()
    Access:ACCAREAS_ALIAS.UseFile

    Set(DEFAULTS)
    Access:DEFAULTS.Next()

    If ~Instring('VODAC',def:User_Name,1,1)
        Destroy(?BastionDefaults)
    End

    if securitycheck('PRINT SETUP') = level:benign
        enable(?printsetup)
    else!if securitycheck('PRINT SETUP') = level:benign
        disable(?printsetup)
    end!if securitycheck('PRINT SETUP') = level:benign

    if securitycheck('RE-LOGIN') = level:benign
        enable(?relogin)
    else!if securitycheck('RE-LOGIN') = level:benign
        disable(?relogin)
    end!if securitycheck('RE-LOGIN') = level:benign


!!! - SYSTEM ADMINISTRATION - !!!

!!! - SYSTEM DEFAULTS - !!!

    if securitycheck('LICENSE DEFAULTS') = level:benign
        enable(?licensedefaults)
    else!if securitycheck('LICENSE DEFAULTS') = level:benign
        disable(?licensedefaults)
    end!if securitycheck('LICENSE DEFAULTS') = level:benign

    if securitycheck('GENERAL DEFAULTS') = level:benign
        enable(?generaldefaults)
    else!if securitycheck('GENERAL DEFAULTS') = level:benign
        disable(?generaldefaults)
    end!if securitycheck('GENERAL DEFAULTS') = level:benign

    If SecurityCheck('SIEMENS DEFAULTS') = level:benign
        Enable(?SiemensDefaults)
    Else!If SecurityCheck('SIEMENS DEFAULTS')
        Disable(?SIEMENSDEFAULTS)
    End!If SecurityCheck('SIEMENS DEFAULTS')


    If SecurityCheck('WEB DEFAULTS') = level:benign
        Enable(?WEBDEFAULTS)
    Else
        Disable(?WEBDEFAULTS)
    End

    If SecurityCheck('UNLOCK SBONLINE JOBS') = level:benign
        Enable(?UnlockSBOnlineJobs)
    Else
        Disable(?UnlockSBOnlineJobs)
    End

    
!!! - USER DEFAULTS - !!!

    if securitycheck('USERS - BROWSE') = level:benign
        enable(?users)
    else!if securitycheck('USERS - BROWSE') = level:benign
        disable(?users)
    end!if securitycheck('USERS - BROWSE') = level:benign

    if securitycheck('ACCESS LEVELS - BROWSE') = level:benign
        enable(?accesslevels)
    else!if securitycheck('ACCESS LEVELS - BROWSE') = level:benign
        disable(?accesslevels)
    end!if securitycheck('ACCESS LEVELS - BROWSE') = level:benign

!!! _ FINANCIAL DEFAULTS - !!!

    if securitycheck('CHARGE RATE DEFAULTS - BROWSE') = level:benign
        enable(?chargeratedefaults)
    else!if securitycheck('CHARGE RATE DEFAULTS - BROWSE') = level:benign
        disable(?chargeratedefaults)
    end!if securitycheck('CHARGE RATE DEFAULTS - BROWSE') = level:benign

    if securitycheck('CHARGE TYPES - BROWSE') = level:benign
        enable(?chargetypes)
    else!if securitycheck('CHARGE TYPES - BROWSE') = level:benign
        disable(?chargetypes)
    end!if securitycheck('CHARGE TYPES - BROWSE') = level:benign

    If SecurityCheck('REPAIR TYPES - BROWSE') = level:benign
        Enable(?REPAIRTYPES)
    Else
        Disable(?REPAIRTYPES)
    End

    if securitycheck('VAT CODES - BROWSE') = level:benign
        enable(?vatcodes)
    else!if securitycheck('VAT CODES - BROWSE') = level:benign
        disable(?vatcodes)
    end!if securitycheck('VAT CODES - BROWSE') = level:benign

    if securitycheck('REPAIR TYPES - BROWSE') = level:benign
        enable(?paymenttypes)
    else!if securitycheck('REPAIR TYPES - BROWSE') = level:benign
        disable(?paymenttypes)
    end!if securitycheck('REPAIR TYPES - BROWSE') = level:benign

    if securitycheck('REPRICE JOBS ROUTINE') = level:benign
        enable(?repricejobsroutine)
    else!if securitycheck('REPRICE JOBS ROUTINE') = level:benign
        disable(?repricejobsroutine)
    end!if securitycheck('REPRICE JOBS ROUTINE') = level:benign

    ! Inserting (DBH 25/06/2007) # 9061 - Add security level for Browse Vetting Reasons
    If SecurityCheck('VETTING REASONS - BROWSE') = Level:Benign
        Enable(?BrowseVettingReasons)
    Else ! If SecurityCheck('VETTING REASONS - BROWSE') = Level:Benign
        Disable(?BrowseVettingReasons)
    End ! If SecurityCheck('VETTING REASONS - BROWSE') = Level:Benign
    ! End (DBH 25/06/2007) #9061

!!! - STOCK CONTROL DEFAULTS - !!!
  
    if securitycheck('MANUFACTURERS - BROWSE') = level:benign
        enable(?manufacturers)
    else!if securitycheck('MANUFACTURERS - BROWSE') = level:benign
        disable(?manufacturers)
    end!if securitycheck('MANUFACTURERS - BROWSE') = level:benign

    if securitycheck('SITE LOCATIONS - BROWSE') = level:benign
        enable(?sitelocations)
    else!if securitycheck('SITE LOCATIONS - BROWSE') = level:benign
        disable(?sitelocations)
    end!if securitycheck('SITE LOCATIONS - BROWSE') = level:benign

    if securitycheck('SUPPLIERS - BROWSE') = level:benign
        enable(?suppliers)
    else!if securitycheck('SUPPLIERS - BROWSE') = level:benign
        disable(?suppliers)
    end!if securitycheck('SUPPLIERS - BROWSE') = level:benign

    if securitycheck('INTERNAL LOCATIONS - BROWSE') = level:benign
        enable(?internallocations)
    else!if securitycheck('INTERNAL LOCATIONS - BROWSE') = level:benign
        disable(?internallocations)
    end!if securitycheck('INTERNAL LOCATIONS - BROWSE') = level:benign

    if securitycheck('LOAN/EXCHANGE STOCK TYPE') = level:benign
        enable(?loanexchangestocktype)
    else!if securitycheck('LOAN/EXCHANGE STOCK TYPE') = level:benign
        disable(?loanexchangestocktype)
    end!if securitycheck('LOAN/EXCHANGE STOCK TYPE') = level:benign

!!! - TRADE ACCOUNT DEFAULTS - !!!

    if securitycheck('TRADE ACCOUNTS - BROWSE') = level:benign
        enable(?tradeaccounts)
        enable(?browsetradesubaccounts)
    else!if securitycheck('TRADE ACCOUNTS - BROWSE') = level:benign
        disable(?tradeaccounts)
        disable(?browsetradesubaccounts)
    end!if securitycheck('TRADE ACCOUNTS - BROWSE') = level:benign

    ! Inserting (DBH 25/04/2006) #7149 - Hide/Show default regions
    If SecurityCheck('DEFAULT REGIONS - BROWSE') = Level:Benign
        ?BrowseDefaultRegions{prop:Disable} = False
    Else ! If SecurityCheck('DEFAULT REGIONS - BROWSE') = Level:Benign
        ?BrowseDefaultRegions{prop:Disable} = True
    End ! If SecurityCheck('DEFAULT REGIONS - BROWSE') = Level:Benign
    ! End (DBH 25/04/2006) #7149

    if securitycheck('AUTO CHANGE COURIER') = level:benign 
        enable(?autochangecourier)
    else!if securitycheck('AUTO CHANGE COURIER') = level:benign
        disable(?autochangecourier)
    end!if securitycheck('AUTO CHANGE COURIER') = level:benign

!!! - GENERAL LOOKUPS - !!!

    If SecurityCheck('ACTIONS - BROWSE') = level:benign
        Enable(?ACTION)
    Else
        Disable(?ACTION)
    End

    if securitycheck('ACCESSORIES - BROWSE') = level:benign
        enable(?accessories)
    else!if securitycheck('ACCESSORIES - BROWSE') = level:benign
        disable(?accessories)
    end!if securitycheck('ACCESSORIES - BROWSE') = level:benign

    If SecurityCheck('COLOURS - BROWSE') = level:Benign
        Enable(?COLOURS)
    Else
        Disable(?COLOURS)
    End

    if securitycheck('COURIERS - BROWSE') = level:benign
        enable(?couriers)
    else!if securitycheck('COURIERS - BROWSE') = level:benign
        disable(?couriers)
    end!if securitycheck('COURIERS - BROWSE') = level:benign

    if securitycheck('JOB TURNAROUND TIMES - BROWSE') = level:benign
        enable(?jobturnaroundtimes)
    else!if securitycheck('JOB TURNAROUNDTIMES - BROWSE') = level:benign
        disable(?jobturnaroundtimes)
    end!if securitycheck('JOB TURNAROUNDTIMES - BROWSE') = level:benign

    if securitycheck('INITIAL TRANSIT TYPES - BROWSE') = level:benign
        enable(?initialtransittypes)
    else!if securitycheck('INITIAL TRANSIT TYPES - BROWSE') = level:benign
        disable(?initialtransittypes)
    end!if securitycheck('INITIAL TRANSIT TYPES - BROWSE') = level:benign

    if securitycheck('LETTERS - BROWSE') = level:benign
        enable(?letters)
    else!if securitycheck('LETTERS - BROWSE') = level:benign
        disable(?letters)
    end!if securitycheck('LETTERS - BROWSE') = level:benign

    If SecurityCheck('MAIL MERGE LETTERS - BROWSE') = level:benign
        Enable(?MAILMERGELETTERS)
    Else
        Disable(?MAILMERGELETTERS)
    End

    If SecurityCheck('MAIL MERGE LETTERS - BROWSE') = level:benign
        Enable(?MAILMERGELETTERS)
    Else
        Disable(?MAILMERGELETTERS)
    End

    If SecurityCheck('OBF REJECTION REASONS - BROWSE') = Level:Benign
        Enable(?OBFRejectionReasons)
    Else ! If SecurityCheck('OBF REJECTION REASONS - BROWSE') = Level:Benign
        Disable(?OBFRejectionReasons)
    End ! If SecurityCheck('OBF REJECTION REASONS - BROWSE') = Level:Benign

    if securitycheck('PAYMENT TYPES - BROWSE') = level:benign
        enable(?paymenttypes)
    else!if securitycheck('PAYMENT TYPES - BROWSE') = level:benign
        disable(?paymenttypes)
    end!if securitycheck('PAYMENT TYPES - BROWSE') = level:benign

    If SecurityCheck('QA FAILURE REASONS - BROWSE') = level:benign
        Enable(?QAFAILUREREASONS)
    Else
        Disable(?QAFAILUREREASONS)
    End

    If SecurityCheck('SUBURBS - BROWSE') = Level:Benign
        Enable(?BrowseSuburbs)
    Else ! If SecurityCheck('SUBURBS - BROWSE') = Level:Benign
        Disable(?BrowseSuburbs)
    End ! If SecurityCheck('SUBURBS - BROWSE') = Level:Benign


    if securitycheck('STATUS TYPES - BROWSE') = level:benign
        enable(?statustypes)
    else!if securitycheck('STATUS TYPES - BROWSE') = level:benign
        disable(?statustypes)
    end!if securitycheck('STATUS TYPES - BROWSE') = level:benign

    if securitycheck('TURNAROUND TIME UTILITY') = level:benign
        enable(?turnaroundtimeutility)
    else!if securitycheck('TURNAROUND TIME UTILITY') = level:benign
        disable(?turnaroundtimeutility)
    end!if securitycheck('TURNAROUND TIME UTILITY') = level:benign

    if securitycheck('CHANGE PERCEIVED SITE LOCATION') = level:benign
        enable(?ChangePerceivedSiteLocation)
    else!if securitycheck('TURNAROUND TIME UTILITY') = level:benign
        disable(?ChangePerceivedSiteLocation)
    end!if securitycheck('TURNAROUND TIME UTILITY') = level:benign

!    check_access('PROCESS CODES - BROWSE',x")
!    if x" = true
!        enable(?processcodes)
!    else
!        disable(?processcodes)
!    end

    if securitycheck('TEAMS - BROWSE') = level:benign
        enable(?teams)
    else!if securitycheck('TEAMS - BROWSE') = level:benign
        disable(?teams)
    end!if securitycheck('TEAMS - BROWSE') = level:benign

    if securitycheck('THIRD PARTY REPAIRERS - BROWSE') = level:benign
        enable(?thirdpartyrepairers)
    else!if securitycheck('THIRD PARTY REPAIRERS - BROWSE') = level:benign
        disable(?thirdpartyrepairers)
    end!if securitycheck('THIRD PARTY REPAIRERS - BROWSE') = level:benign

    if securitycheck('UNIT TYPES - BROWSE') = level:benign
        enable(?unittypes)
    else!if securitycheck('UNIT TYPES - BROWSE') = level:benign
        disable(?unittypes)
    end!if securitycheck('UNIT TYPES - BROWSE') = level:benign

!!! - BROWSE - !!!
    If SecurityCheck('BOUNCERS') = level:benign
        Enable(?BOUNCERS)
    Else
        Disable(?BOUNCERS)
    End

    if securitycheck('CONTACTS - BROWSE') = level:benign
        enable(?contacts)
    else!if securitycheck('CONTACTS - BROWSE') = level:benign
        disable(?contacts)
    end!if securitycheck('CONTACTS - BROWSE') = level:benign


    if securitycheck('LOAN UNITS - BROWSE') = level:benign
        enable(?loanunits)
    else!if securitycheck('LOAN UNITS - BROWSE') = level:benign
        disable(?loanunits)
    end!if securitycheck('LOAN UNITS - BROWSE') = level:benign

    if securitycheck('EXCHANGE UNITS - BROWSE') = level:benign
        enable(?exchangeunits)
    else!if securitycheck('EXCHANGE UNITS - BROWSE') = level:benign
        disable(?exchangeunits)
    end!if securitycheck('EXCHANGE UNITS - BROWSE') = level:benign

    if securitycheck('AUDIT NUMBERS - BROWSE') = level:benign
        enable(?byauditnumber)
    else!if securitycheck('AUDIT NUMBERS - BROWSE') = level:benign
        disable(?byauditnumber)
    end!if securitycheck('AUDIT NUMBERS - BROWSE') = level:benign

    If SecurityCheck('RAPID - EXCHANGE TRANSFER') = Level:Benign
        Enable(?RapidExchangeTransfer)
        Enable(?RapidExchangeTransfer48Hour)
    Else !If SecurityCheck('RAPID EXCHANGE TRANSFER') = Level:Benign
        Disable(?RapidExchangeTransfer)
        Disable(?RapidExchangeTransfer48Hour)
    End !If SecurityCheck('RAPID EXCHANGE TRANSFER') = Level:Benign

    if securitycheck('RAPID - EXCHANGE INSERT WIZARD') = level:benign
        enable(?rapidinsertwizard)
    else!if securitycheck('RAPID EXCHANGE INSERT WIZARD') = level:benign
        disable(?rapidinsertwizard)
    end!if securitycheck('RAPID EXCHANGE INSERT WIZARD') = level:benign

    if securitycheck('ESN TO MODEL CORRELATION') = level:benign
        enable(?esntomodelcorrelation)
    else!if securitycheck('ESN TO MODEL CORRELATION') = level:benign
        disable(?esntomodelcorrelation)
    end!if securitycheck('ESN TO MODEL CORRELATION') = level:benign

    if securitycheck('INVOICES - BROWSE') = level:benign
        enable(?invoices)
    else!if securitycheck('INVOICES - BROWSE') = level:benign
        disable(?invoices)
    end!if securitycheck('INVOICES - BROWSE') = level:benign

    if securitycheck('SECOND YEAR WARRANTY INVOICING') = level:benign
        enable(?SecondYearWarrantyInvoicing)
    else!if securitycheck('INVOICES - BROWSE') = level:benign
        disable(?SecondYearWarrantyInvoicing)
    end!if securitycheck('INVOICES - BROWSE') = level:benign

    If SecurityCheck('ENGINEERS STATUS') = level:benign
        Enable(?ENGINEERSSTATUS)
    Else
        Disable(?ENGINEERSSTATUS)
    End

!!! - UTILITIES - !!!

    if securitycheck('INTERNAL EMAIL - BROWSE') = level:benign
        enable(?internalemail)
    else!if securitycheck('INTERNAL EMAIL - BROWSE') = level:benign
        disable(?internalemail)
    end!if securitycheck('INTERNAL EMAIL - BROWSE') = level:benign
    If SecurityCheck('USERS LOGGED IN') = level:benign
        Enable(?USERSLOGGEDIN)
    Else
        Disable(?USERSLOGGEDIN)
    End

    if securitycheck('CLEAR AUDIT TRAIL') = level:benign
        enable(?clearaudittrail)
    else!if securitycheck('CLEAR AUDIT TRAIL') = level:benign
        disable(?clearaudittrail)
    end!if securitycheck('CLEAR AUDIT TRAIL') = level:benign

    if securitycheck('RE-SUBMIT FOR DESPATCH') = level:benign
        enable(?redespatch)
    else!if securitycheck('RE-SUBMIT FOR DESPATCH') = level:benign
        disable(?redespatch)
    end!if securitycheck('RE-SUBMIT FOR DESPATCH') = level:benign

    If SecurityCheck('RESUBMIT TO ORACLE') = Level:Benign Or SecurityCheck('RESUBMIT TO CID') = Level:Benign
        Enable(?UtilitiesResubmitXMLMessages)
    Else ! If SecurityCheck('RESUBMIT TO ORACLE') = Level:Benign Or SecurityCheck('RESUBMIT TO CID') = Level:Benign
        Disable(?UtilitiesResubmitXMLMessages)
    End ! If SecurityCheck('RESUBMIT TO ORACLE') = Level:Benign Or SecurityCheck('RESUBMIT TO CID') = Level:Benign


    if securitycheck('SMS FUNCTIONALITY') = level:benign
        enable(?SystemAdministrationGeneralLookupsSMSFunctional)
    else!if securitycheck('RE-SUBMIT FOR DESPATCH') = level:benign
        disable(?SystemAdministrationGeneralLookupsSMSFunctional)
    end!if securitycheck('RE-SUBMIT FOR DESPATCH') = level:benign

    if glo:WEbjob then
        hide(?SystemAdministrationStockControlDefaultsPriceBa)
    ELSE
        unhide(?SystemAdministrationStockControlDefaultsPriceBa)
    END

    !TB12740 - J - 17/06/14
    if securitycheck('WIP AUDIT STATUSES') = level:benign
        enable(?SystemAdministrationTradeAccountDefaultsTradeAc)
    else!if securitycheck() = level:benign
        disable(?SystemAdministrationTradeAccountDefaultsTradeAc)
    end!if securitycheck() = level:benign


!!! - PROCEDURES - !!!

!!! - WARRANTY PROCESS - !!!
! Deleting (DBH 30/05/2008) # 9792 -
!
!    if securitycheck('PENDING CLAIMS') = level:benign
!        enable(?pendingclaims)
!    else!if securitycheck('PENDING CLAIMS') = level:benign
!        disable(?pendingclaims)
!    end!if securitycheck('PENDING CLAIMS') = level:benign
!
!    if securitycheck('RECONCILE CLAIMS') = level:benign
!        enable(?reconcileclaims)
!    else!if securitycheck('RECONCILE CLAIMS') = level:benign
!        disable(?reconcileclaims)
!    end!if securitycheck('RECONCILE CLAIMS') = level:benign
! End (DBH 30/05/2008) #9792
    ! Inserting (DBH 30/05/2008) # 9792 - Combine warranty process into one
    If SecurityCheck('WARRANTY PROCESS') = Level:Benign
        Enable(?WarrantyProcess)
    Else ! If SecurityCheck('WARRANTY PROCESS') = Level:Benign
        Disable(?WarrantyProcess)
    End ! If SecurityCheck('WARRANTY PROCESS') = Level:Benign
    ! End (DBH 30/05/2008) #9792


!!! - IMPORT PROCEDURES - !!!

!!! - PARTS ORDERS - !!!
    if securitycheck('PENDING ORDERS') = level:benign
        enable(?pendingorders)
    else!if securitycheck('PENDING ORDERS') = level:benign
        disable(?pendingorders)
    end!if securitycheck('PENDING ORDERS') = level:benign

    if securitycheck('STOCK REQUISITIONS') = level:benign
        enable(?stockrequisitions)
    else!if securitycheck('PENDING ORDERS') = level:benign
        disable(?stockrequisitions)
    end!if securitycheck('PENDING ORDERS') = level:benign

    if securitycheck('PARTS ORDER RECEIVING') = level:benign
        enable(?receiving)
    else!if securitycheck('PARTS ORDER RECEIVING') = level:benign
        disable(?receiving)
    end!if securitycheck('PARTS ORDER RECEIVING') = level:benign

    if securitycheck('RETAIL BACK ORDER PROCESSING') = level:benign
        enable(?RetailBackOrderProcessing)
    else!if securitycheck('PARTS ORDER RECEIVING') = level:benign
        disable(?RetailBackOrderProcessing)
    end!if securitycheck('PARTS ORDER RECEIVING') = level:benign

    If SecurityCheck('RAPID - LETTER PRINTING') = level:benign
        Enable(?RAPIDLETTERPRINTING)
    Else
        Disable(?RAPIDLETTERPRINTING)
    End

!!! - STOCK CONTROL - !!!

    if securitycheck('MINIMUM STOCK LEVEL ROUTINE') = level:benign
        enable(?minimumstocklevel)
    else!if securitycheck('MINIMUM STOCK LEVEL ROUTINE') = level:benign
        disable(?minimumstocklevel)
    end!if securitycheck('MINIMUM STOCK LEVEL ROUTINE') = level:benign

    if securitycheck('REPLICATE MAIN STORE STOCK') = level:benign
        enable(?replicatemainstorestock)
    else!if securitycheck('REPLICATE MAIN STORE STOCK') = level:benign
        disable(?replicatemainstorestock)
    end!if securitycheck('REPLICATE MAIN STORE STOCK') = level:benign

!!! - VODAFONE EXCHANGE - !!!

!    check_access('VODA - IMPORT EXCHANGE FILE',x")
!    if x" = true
!        enable(?importexchangefile)
!    else
!        disable(?importexchangefile)
!    end
!
!    check_access('VODA - ALLOCATE EXCHANGE UNIT',x")
!    if x" = true
!        enable(?allocateexchangeunit)
!    else
!        disable(?allocateexchangeunit)
!    end
!
!    check_access('VODA - DESPATCH PROCEDURE',x")
!    if x" = true
!        enable(?despatchprocedure)
!    else
!        disable(?despatchprocedure)
!    end
!
!    check_access('VODA - QA FAILURE',x")
!    if x" = true
!        enable(?qafailure)
!    else
!        disable(?qafailure)
!    end
!
!    check_access('VODA - UPDATE VODAFONE',x")
!    if x" = true
!        enable(?updatevodafone)
!    else
!        disable(?updatevodafone)
!    end

!!! - REPORTS - !!!

!!! - REPORT WIZARDS - !!!

    if securitycheck('STATUS REPORT') = level:benign
        enable(?statusreport)
    else!if securitycheck('STATUS REPORT') = level:benign
        disable(?statusreport)
    end!if securitycheck('STATUS REPORT') = level:benign


!!! - COURIER REPORTS - !!!    

    If SecurityCheck('EXCHANGE UNIT AUDIT REPORT') = level:benign
        Enable(?EXCHANGEUNITAUDITREPORT)
    Else
        Disable(?EXCHANGEUNITAUDITREPORT)
    End

    If SecurityCheck('STOCK CHECK REPORT') = level:benign
        Enable(?STOCKCHECKREPORT)
    Else
        Disable(?STOCKCHECKREPORT)
    End

    If SecurityCheck('RETAIL SALES VALUATION REPORT') = level:benign
        Enable(?RETAILSALESVALUATIONREPORT)
    Else
        Disable(?RETAILSALESVALUATIONREPORT)
    End

    If SecurityCheck('REPAIR EXCHANGE REPORT') = level:benign
        Enable(?REPAIREXCHANGEREPORT)
    Else
        Disable(?REPAIREXCHANGEREPORT)
    End

    If SecurityCheck('CUSTOMER ENQUIRY REPORT') = level:benign
        Enable(?CUSTOMERENQUIRYREPORT)
    Else
        Disable(?CUSTOMERENQUIRYREPORT)
    End

    if securitycheck('EXCHANGE EXCEPTIONS REPORT') = level:benign
        enable(?exchangeexceptionsreport)
    else!if securitycheck('EXCHANGE EXCEPTIONS REPORT') = level:benign
        disable(?exchangeexceptionsreport)
    end!if securitycheck('EXCHANGE EXCEPTIONS REPORT') = level:benign

    if securitycheck('EXCHANGE CLAIMS REPORT') = level:benign
        enable(?exchangeclaimsreport)
    else!if securitycheck('EXCHANGE CLAIMS REPORT') = level:benign
        disable(?exchangeclaimsreport)
    end!if securitycheck('EXCHANGE CLAIMS REPORT') = level:benign

    if securitycheck('COURIER COLLECTION REPORT') = level:benign
        enable(?couriercollectionreport)
    else!if securitycheck('COURIER COLLECTION REPORT') = level:benign
        disable(?couriercollectionreport)
    end!if securitycheck('COURIER COLLECTION REPORT') = level:benign

    if securitycheck('SPECIAL DELIVERY REPORT') = level:benign
        enable(?specialdeliveryreport)
    else!if securitycheck('SPECIAL DELIVERY REPORT') = level:benign
        disable(?specialdeliveryreport)
    end!if securitycheck('SPECIAL DELIVERY REPORT') = level:benign

    if securitycheck('OVERDUE LOANS REPORT') = level:benign
        enable(?overdueloansreport)
    else!if securitycheck('OVERDUE LOANS REPORT') = level:benign
        disable(?overdueloansreport)
    end!if securitycheck('OVERDUE LOANS REPORT') = level:benign


!!! - STOCK REPORTS - !!!

    if securitycheck('STOCK VALUE REPORT') = level:benign
        enable(?stockvaluereport)
    else!if securitycheck('STOCK VALUE REPORT') = level:benign
        disable(?stockvaluereport)
    end!if securitycheck('STOCK VALUE REPORT') = level:benign

    if securitycheck('STOCK USAGE REPORT') = level:benign
        enable(?stockusagereport)
    else!if securitycheck('STOCK USAGE REPORT') = level:benign
        disable(?stockusagereport)
    end!if securitycheck('STOCK USAGE REPORT') = level:benign

    if securitycheck('PARTS NOT RECEIVED REPORT') = level:benign
        enable(?partsnotreceivedreport)
    else!if securitycheck('PARTS NOT RECEIVED REPORT') = level:benign
        disable(?partsnotreceivedreport)
    end!if securitycheck('PARTS NOT RECEIVED REPORT') = level:benign

    if securitycheck('STOCK FORECAST REPORT') = level:benign
        enable(?stockforecastreport)
    else!if securitycheck('STOCK FORECAST REPORT') = level:benign
        disable(?stockforecastreport)
    end!if securitycheck('STOCK FORECAST REPORT') = level:benign

    if securitycheck('JOB PENDING PARTS REPORT') = level:benign
        enable(?jobpendingpartsreport)
    else!if securitycheck('JOB PENDING PARTS REPORT') = level:benign
        disable(?jobpendingpartsreport)
    end!if securitycheck('JOB PENDING PARTS REPORT') = level:benign

!!! - THIRD PARTY SERVICE - !!!

!!! - INCOME REPORTS - !!!

    if securitycheck('INCOME REPORT') = level:benign
        enable(?incomereport)
    else!if securitycheck('INCOME REPORT') = level:benign
        disable(?incomereport)
    end!if securitycheck('INCOME REPORT') = level:benign

    if securitycheck('OVERDUE STATUS REPORT') = level:benign
        enable(?overduestatusreport)
    else!if securitycheck('OVERDUE STATUS REPORT') = level:benign
        disable(?overduestatusreport)
    end!if securitycheck('OVERDUE STATUS REPORT') = level:benign

    if securitycheck('QA FAILURE REPORT') = level:benign
        enable(?qafailurereport)
    else!if securitycheck('QA FAILURE REPORT') = level:benign
        disable(?qafailurereport)
    end!if securitycheck('QA FAILURE REPORT') = level:benign

!!! - RETAIL SALES - !!!

    If SecurityCheck('RETAIL SALES') = Level:benign
        Enable(?RetailSales)
    Else
        Disable(?RetailSales)
    End !If SecurityCheck('RETAIL SALES') = Level:benign

    If SecurityCheck('OUTSTANDING STOCK ORDERS') = Level:benign
        Enable(?RetailSalesOutstandingStockOrders)
    Else
        Disable(?RetailSalesOutstandingStockOrders)
    End !If SecurityCheck('RETAIL SALES') = Level:benign

    If SecurityCheck('PENDING WEB ORDERS') = Level:benign
        Enable(?PendingWebOrders)
    Else
        Disable(?PendingWebOrders)
    End !If SecurityCheck('RETAIL SALES') = Level:benign

    if glo:RelocateStore then
        if securitycheck('ARC STOCK CONTROL VIEW ONLY') = level:benign
            enable(?StockControl)
        else!if securitycheck('RE-LOGIN') = level:benign
            disable(?StockControl)
        end!if securitycheck('RE-LOGIN') = level:benign
    END


    Access:DEFAULTS.Close()
    Access:USERS_ALIAS.Close()
    Access:ACCAREAS_ALIAS.Close()

! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020562'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?SRNNumber
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
    ! Save the IP and Host of the logged in user (DBH: 13-04-2006)
    NetOptions(net:gethostiplist,glo:IPAddress)
    NetOptions(net:gethostname,glo:HostName)
    Loop x# = 1 To 30
        If Sub(glo:IPAddress,x#,1) = ' '
            glo:IPAddress = Sub(glo:IPAddress,1,x#-1)
            Break
        End ! If Sub(tmp:IPAddress,x#,1) = ' '
    End ! Loop x# = 1 To 30
  OPEN(AppFrame)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !TB13240 - entry freezing - add debugging code
  Debugfile = clip(path())&'\Login_Logs\Login_'&format(today(),@d12)&'_'&sub(format(clock(),@t02),1,2)&'00.log'
  
  Start_Screen
  If glo:select1 <> 'OK'
      Halt
  End!If glo:select1 <> 'OK'
  glo:select1 = ''
  Do Login
  
  !TB13240 - entry freezing - add debugging code
  LinePrint(glo:UserCode&'1',Debugfile)
  
  If Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
      ?ColourReport{prop:Text} = Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI')) & ' Report'
      ?Colours{prop:Text} = Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI')) & 's'
  End 
  
  !Debug - Make is a web job
  If COMMAND('WebJob') = 1
      glo:WebJob = 1
  End
  
  !Show the version number on the bottom of the screen - Useful for screen prints
  Access:DEFAULTV.Open()
  Access:DEFAULTV.UseFile()
  Set(DEFAULTV)
  Access:DEFAULTV.Next()
  tmp:VersionNumber   = '   (Ver: ' & Clip(defv:VersionNumber) & ')'
  ?CurrentVersion:Prompt{prop:Text} = 'Version: ' & Clip(defv:VersionNumber)
  Access:DEFAULTV.Close()
  
  !TB13240 - entry freezing - add debugging code
  LinePrint(glo:UserCode&'2',Debugfile)
  
  !Set Globals
  !TB13240 - this section is where everything was freezing
  !          not really needed here - head site location moved to booking defaults in sbc01app.dll
  !          it has already been set up - they can change it there
  !          just leave the glo:ARCAccountNumber getting
  glo:ARCAccountNumber    = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  !Access:TRADEACC.Open()
  !Access:TRADEACC.USeFile()
  !Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  !tra:Account_Number  = glo:ARCAccountNumber
  !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  !    !Found
  !    PUTINI('BOOKING','HeadSiteLocation',tra:SiteLocation,CLIP(PATH()) & '\SB2KDEF.INI')
  !Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  !    !Error
  !End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  !
  !Access:TRADEACC.Close()
  
  !TB13240 - entry freezing - add debugging code
  LinePrint(glo:UserCode&'3',Debugfile)
  
  If GETINI('SBDEF','HideCompanyLogos',,Clip(Path()) & '\SB2KDEF.INI') <> 1
      ?Image2{prop:Hide} = 0
  Else ! If GETINI('SBDEF','HideCompanyLogos',,Clip(Path()) & '\SB2KDEF.INI') = 1
      If GETINI('SBDEF','LogoPath',,Clip(Path()) & '\SB2KDEF.INI') <> ''
          ?Image2{prop:Text} = Clip(GETINI('SBDEF','LogoPath',,Clip(Path()) & '\SB2KDEF.INI'))
          ?Image2{prop:Hide} = 0
      End ! If GETINI('SBDEF','LogoPath',,Clip(Path()) & '\SB2KDEF.INI') <> ''
  End ! If GETINI('SBDEF','HideCompanyLogos',,Clip(Path()) & '\SB2KDEF.INI') = 1
  
  !TB13240 - entry freezing - add debugging code
  LinePrint(glo:UserCode&'4',Debugfile)
  
  !remove any expired prejobs
  ClearPreJobs
  
  !TB13240 - entry freezing - add debugging code
  LinePrint(glo:UserCode&'5',Debugfile)
  
      ! Save Window Name
   AddToLog('Window','Open','Main')
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
    Do log_out
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','Main')
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ChargeRateDefaults
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ChargeRateDefaults, Accepted)
      glo:select12 = 'STANDARD'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ChargeRateDefaults, Accepted)
    OF ?ClearAuditTrail
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearAuditTrail, Accepted)
      Glo:select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ClearAuditTrail, Accepted)
    OF ?Button7
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button7, Accepted)
          if (SecurityCheck('JOBS - INSERT'))  ! #11682 Check access. (Bryan: 07/09/2010)
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('You do not have access to this option.','ServiceBase',|
                             'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              Cycle
          End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button7, Accepted)
    OF ?Button8
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button8, Accepted)
      if (SecurityCheck('JOBS - CHANGE'))  ! #11682 Check access. (Bryan: 07/09/2010)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You do not have access to this option.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          Cycle
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button8, Accepted)
    OF ?Button:SingleIMEISearch
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:SingleIMEISearch, Accepted)
      if (SecurityCheck('JOBS - CHANGE'))  ! #11682 Check access. (Bryan: 07/09/2010)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You do not have access to this option.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          Cycle
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:SingleIMEISearch, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ReLogin
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReLogin, Accepted)
      Do Log_out
      Do Login
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReLogin, Accepted)
    OF ?LicenseDefaults
      ThisWindow.Update
      Defaults_Window
      ThisWindow.Reset
    OF ?MainDefaults
      ThisWindow.Update
      MainDefaults
      ThisWindow.Reset
    OF ?EstimateDefaults
      ThisWindow.Update
      EstimateDefaults
      ThisWindow.Reset
    OF ?CompulsoryFields
      ThisWindow.Update
      CompulsoryFields
      ThisWindow.Reset
    OF ?StandardTexts
      ThisWindow.Update
      StandardTexts
      ThisWindow.Reset
    OF ?PrintingDefaults
      ThisWindow.Update
      PrintingDefaults
      ThisWindow.Reset
    OF ?RRCDefaults
      ThisWindow.Update
      RRCDefaults
      ThisWindow.Reset
    OF ?BastionDefaults
      ThisWindow.Update
      BastionDefaults
      ThisWindow.Reset
    OF ?BookingDefaults
      ThisWindow.Update
      BookingDefaults
      ThisWindow.Reset
    OF ?BrowseDefaults
      ThisWindow.Update
      BrowseDefaults
      ThisWindow.Reset
    OF ?ServerDefaults
      ThisWindow.Update
      EmailDefaults
      ThisWindow.Reset
    OF ?RecipientTypes
      ThisWindow.Update
      BrowseRecipientTypes
      ThisWindow.Reset
    OF ?StockControlDefaults
      ThisWindow.Update
      StockDefaults
      ThisWindow.Reset
    OF ?RetailSalesDefaults
      ThisWindow.Update
      RetailSalesDefaults
      ThisWindow.Reset
    OF ?SiemensDefaults
      ThisWindow.Update
      eps_defaults
      ThisWindow.Reset
    OF ?WebDefaults
      ThisWindow.Update
      Browse_Web_Defaults
      ThisWindow.Reset
    OF ?UnlockSBOnlineJobs
      ThisWindow.Update
      UnlockSBOnlineJobs
      ThisWindow.Reset
    OF ?accesslevels
      ThisWindow.Update
      Browse_access_levels
      ThisWindow.Reset
    OF ?users
      ThisWindow.Update
      Browse_Users
      ThisWindow.Reset
    OF ?ChargeRateDefaults
      ThisWindow.Update
      Insert_Stardard_Charges
      ThisWindow.Reset
    OF ?ChargeTypes
      ThisWindow.Update
      Browse_Charge_Type
      ThisWindow.Reset
    OF ?RepairTypes
      ThisWindow.Update
      Browse_Default_Repair_Types
      ThisWindow.Reset
    OF ?VATCodes
      ThisWindow.Update
      Browse_vat_code
      ThisWindow.Reset
    OF ?BrowseVettingReasons
      ThisWindow.Update
      BrowseVettingReasons
      ThisWindow.Reset
    OF ?RepriceJobsRoutine
      ThisWindow.Update
      RepriceJobs
      ThisWindow.Reset
    OF ?manufacturers
      ThisWindow.Update
      BrowseMANUFACT
      ThisWindow.Reset
    OF ?Suppliers
      ThisWindow.Update
      Browse_Suppliers
      ThisWindow.Reset
    OF ?SiteLocations
      ThisWindow.Update
      Browse_Stock_Locations
      ThisWindow.Reset
    OF ?InternalLocations
      ThisWindow.Update
      Browse_Internal_Location
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?InternalLocations, Accepted)
      glo:select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?InternalLocations, Accepted)
    OF ?LoanExchangeStockType
      ThisWindow.Update
      Browse_Stock_Type
      ThisWindow.Reset
    OF ?SystemAdministrationStockControlDefaultsPriceBa
      ThisWindow.Update
      BrowsePriBand
      ThisWindow.Reset
    OF ?TradeAccounts
      ThisWindow.Update
      BrowseTRADEACC
      ThisWindow.Reset
    OF ?BrowseTradeSubAccounts
      ThisWindow.Update
      Browse_Sub_Accounts
      ThisWindow.Reset
    OF ?BrowseDefaultHubs
      ThisWindow.Update
      BrowseHubs
      ThisWindow.Reset
    OF ?BrowseDefaultRegions
      ThisWindow.Update
      BrowseRegions
      ThisWindow.Reset
    OF ?AutoChangeCourier
      ThisWindow.Update
      Auto_Change_Courier
      ThisWindow.Reset
    OF ?SystemAdministrationTradeAccountDefaultsTradeAc
      ThisWindow.Update
      WIPEXCLUDEDSTATUSES
      ThisWindow.Reset
    OF ?accessories
      ThisWindow.Update
      Browse_Default_Accessory
      ThisWindow.Reset
    OF ?Action
      ThisWindow.Update
      Browse_action
      ThisWindow.Reset
    OF ?Colours
      ThisWindow.Update
      Browse_Default_Colours
      ThisWindow.Reset
    OF ?couriers
      ThisWindow.Update
      Browse_Courier
      ThisWindow.Reset
    OF ?EstimateRejectionReasons
      ThisWindow.Update
      BrowseEstRejectionReasons
      ThisWindow.Reset
    OF ?JobTurnaroundTimes
      ThisWindow.Update
      Browse_Turnaround_Time
      ThisWindow.Reset
    OF ?InitialTransitTypes
      ThisWindow.Update
      Browse_Transit_Types
      ThisWindow.Reset
    OF ?Letters
      ThisWindow.Update
      Browse_Letters
      ThisWindow.Reset
    OF ?MailMergeLetters
      ThisWindow.Update
      BrowseLetters
      ThisWindow.Reset
    OF ?Networks
      ThisWindow.Update
      BrowseNetworks
      ThisWindow.Reset
    OF ?OBFRejectionReasons
      ThisWindow.Update
      BrowseOBFRejectionReasons
      ThisWindow.Reset
    OF ?PaymentTypes
      ThisWindow.Update
      Browse_Payment_Types
      ThisWindow.Reset
    OF ?QAFailureReasons
      ThisWindow.Update
      Browse_QA_Reason
      ThisWindow.Reset
    OF ?SystemAdministrationGeneralLookupsReturnTypes
      ThisWindow.Update
      BrowseReturnTypes
      ThisWindow.Reset
    OF ?StatusTypes
      ThisWindow.Update
      Browse_Status_Only
      ThisWindow.Reset
    OF ?TurnaroundTimeUtility
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TurnaroundTimeUtility, Accepted)
      Case Missive('Warning! This facility will reset the status turnaround times for each job.'&|
        '<13,10>This will take a long time to run and cannot be reversed.'&|
        '<13,10>'&|
        '<13,10>Are you sure?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
          
              Case Missive('You have selected to reset the status turnaround times for all jobs.'&|
                '<13,10>'&|
                '<13,10>Are you sure?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes') 
                  Of 2 ! Yes Button
                      TurnaroundTimeProcess()
                  Of 1 ! No Button
              End ! Case Missive
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TurnaroundTimeUtility, Accepted)
    OF ?BrowseSuburbs
      ThisWindow.Update
      BrowseSuburbs
      ThisWindow.Reset
    OF ?Teams
      ThisWindow.Update
      Browse_Teams
      ThisWindow.Reset
    OF ?thirdPartyRepairers
      ThisWindow.Update
      Browse_3rdParty
      ThisWindow.Reset
    OF ?LookupThirdPartyRepairTypes
      ThisWindow.Update
      BrowseThirdPartyRepairTypes
      ThisWindow.Reset
    OF ?UnitTypes
      ThisWindow.Update
      BrowseUNITTYPE
      ThisWindow.Reset
    OF ?SystemAdministrationGeneralLookupsSMSFunctional
      ThisWindow.Update
      Browse_SMSText
      ThisWindow.Reset
    OF ?Contacts
      ThisWindow.Update
      Browse_Contacts
      ThisWindow.Reset
    OF ?LoanUnits
      ThisWindow.Update
      Browse_Loan('')
      ThisWindow.Reset
    OF ?RapidLoanTransfer
      ThisWindow.Update
      Rapid_Loan_Transfer
      ThisWindow.Reset
    OF ?BrowseLoanUnitsVCPLoanAudits
      ThisWindow.Update
      BrowseVCPLoanAudits
      ThisWindow.Reset
    OF ?ExchangeUnits
      ThisWindow.Update
      Browse_Exchange('')
      ThisWindow.Reset
    OF ?ByAuditNumber
      ThisWindow.Update
      Browse_Exchange_By_Audit('')
      ThisWindow.Reset
    OF ?RapidExchangeTransfer
      ThisWindow.Update
      Rapid_Exchange_Transfer
      ThisWindow.Reset
    OF ?RapidExchangeTransfer48Hour
      ThisWindow.Update
      RapidExchangeTransfer48Hour
      ThisWindow.Reset
    OF ?RapidInsertWizard
      ThisWindow.Update
      Rapid_Exchange_Unit
      ThisWindow.Reset
    OF ?BrowseExchangeUnitsMinimum
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BrowseExchangeUnitsMinimum, Accepted)
      !Case Missive('Warning! This routine may take a long time to run.'&|
      !  '<13,10>'&|
      !  '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
      !               'mquest.jpg','\No|/Yes') 
      !    Of 2 ! Yes Button
      !    
      !        glo:File_Name   = 'F' & Format(Clock(),@n07) & '.TMP'
      !        MinimumExchangeProcess()
      !        BrowseExchangeMinStock
      !        Remove(glo:File_Name)
      !    Of 1 ! No Button
      !End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BrowseExchangeUnitsMinimum, Accepted)
    OF ?ESNToModelCorrelation
      ThisWindow.Update
      Browse_ESN_Model
      ThisWindow.Reset
    OF ?Bouncers
      ThisWindow.Update
      Browse_Bouncers
      ThisWindow.Reset
    OF ?EngineersStatus
      ThisWindow.Update
      Engineer_Status_Screen
      ThisWindow.Reset
    OF ?InternalEmail
      ThisWindow.Update
      Internal_Mail
      ThisWindow.Reset
    OF ?UsersLoggedIn
      ThisWindow.Update
      Browse_Logged_In
      ThisWindow.Reset
    OF ?ClearAuditTrail
      ThisWindow.Update
      Remove_Audit
      ThisWindow.Reset
    OF ?ReDespatch
      ThisWindow.Update
      Re_Despatch
      ThisWindow.Reset
    OF ?ChangePerceivedSiteLocation
      ThisWindow.Update
      Perceived_Site_Location
      ThisWindow.Reset
    OF ?UtilitiesReleaseLockedRecords
      ThisWindow.Update
      Unlock_Jobs
      ThisWindow.Reset
    OF ?UtilitiesResubmitXMLMessages
      ThisWindow.Update
      ResubmitMessages
      ThisWindow.Reset
    OF ?WarrantyProcess
      ThisWindow.Update
      WarrantyProcess
      ThisWindow.Reset
    OF ?Invoices
      ThisWindow.Update
      Browse_Invoice
      ThisWindow.Reset
    OF ?SecondYearWarrantyInvoicing
      ThisWindow.Update
      CreateSecondYearInvoice
      ThisWindow.Reset
    OF ?GoodsReceivedNotes
      ThisWindow.Update
      BrowseGoodsReceivedNotes
      ThisWindow.Reset
    OF ?MinimumStockLevel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MinimumStockLevel, Accepted)
      tmp:UseScheduledMinStock = GETINI('STOCK','UseScheduledMinStock',,CLIP(PATH()) & '\SB2KDEF.INI')
      IF tmp:UseScheduledMinStock = FALSE
        glo:File_Name   = 'S' & Format(CLock(),@n07) & '.TMP'
      ELSE
        glo:File_Name = GETINI('STOCK','ScheduledFileName',,CLIP(PATH()) & '\SB2KDEF.INI')
      END
      BrowseMinimumStock
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MinimumStockLevel, Accepted)
    OF ?StockRequisitions
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StockRequisitions, Accepted)
      If CreatePendingOrders()
          Browse_Pending_Orders
      Else !CreatePendingOrders
          Case Missive('There are no parts requested for ordering.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
      End !CreatePendingOrders
      Remove(Glo:File_Name)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StockRequisitions, Accepted)
    OF ?PendingOrders
      ThisWindow.Update
      BrowseStockRequisitions
      ThisWindow.Reset
    OF ?Receiving
      ThisWindow.Update
      Browse_Parts_Orders
      ThisWindow.Reset
    OF ?ProceduresStockControlReturnedOrders
      ThisWindow.Update
      BrowseReturningOrders
      ThisWindow.Reset
    OF ?ReplicateMainStoreStock
      ThisWindow.Update
      Pick_Other_Location('ALL',0)
      ThisWindow.Reset
    OF ?RapidLetterPrinting
      ThisWindow.Update
      RapidLetterPrint
      ThisWindow.Reset
    OF ?SearchAccessories
      ThisWindow.Update
      AccessoryJobMatch
      ThisWindow.Reset
    OF ?StatusReport
      ThisWindow.Update
      Status_Report_Criteria
      ThisWindow.Reset
    OF ?ExchangeExceptionsReport
      ThisWindow.Update
      Exchange_Exceptions_Criteria('E')
      ThisWindow.Reset
    OF ?ExchangeClaimsReport
      ThisWindow.Update
      Exchange_Exceptions_Criteria('C')
      ThisWindow.Reset
    OF ?CourierCollectionReport
      ThisWindow.Update
      Date_Range('Courier Collection Report')
      ThisWindow.Reset
    OF ?SpecialDeliveryReport
      ThisWindow.Update
      Special_Delivery_Criteria('SPECIAL DELIVERY')
      ThisWindow.Reset
    OF ?ExchangeUnitAuditReport
      ThisWindow.Update
      Exchange_Unit_Audit_Criteria
      ThisWindow.Reset
    OF ?OverdueLoansReport
      ThisWindow.Update
      Overdue_Loan_Criteria
      ThisWindow.Reset
    OF ?stockValueReportSummary
      ThisWindow.Update
      Stock_Value_Criteria
      ThisWindow.Reset
    OF ?StockValueReportDetailed
      ThisWindow.Update
      Stock_Value_Detailed_Criteria
      ThisWindow.Reset
    OF ?StockCheckReport
      ThisWindow.Update
      Stock_Check_Criteria(0)
      ThisWindow.Reset
    OF ?StockUsageReport
      ThisWindow.Update
      Stock_Usage_Criteria
      ThisWindow.Reset
    OF ?PartsNotReceivedReport
      ThisWindow.Update
      Parts_Not_Received_Criteria
      ThisWindow.Reset
    OF ?StockForecastReport
      ThisWindow.Update
      Stock_Forecast_Criteria
      ThisWindow.Reset
    OF ?JobPendingPartsReport
      ThisWindow.Update
      Job_Pending_Parts_Criteria
      ThisWindow.Reset
    OF ?QAFailureReport
      ThisWindow.Update
      Date_Range('QA Failure Report')
      ThisWindow.Reset
    OF ?SparesReceivedReport
      ThisWindow.Update
      SparesReceivedReportCriteria
      ThisWindow.Reset
    OF ?RetailSalesValuationReport
      ThisWindow.Update
      Retail_Sales_Valuation_Criteria
      ThisWindow.Reset
    OF ?IncomeReport
      ThisWindow.Update
      Income_Report_Criteria
      ThisWindow.Reset
    OF ?JobIncomeReport
      ThisWindow.Update
      JobIncomeReportCriteria
      ThisWindow.Reset
    OF ?WarrantyIncomeReport
      ThisWindow.Update
      WarrantyIncomeReportCriteria
      ThisWindow.Reset
    OF ?OverdueStatusReport
      ThisWindow.Update
      Overdue_status_criteria
      ThisWindow.Reset
    OF ?RepairExchangeReport
      ThisWindow.Update
      RepairExchangeReportCriteria
      ThisWindow.Reset
    OF ?CustomerEnquiryReport
      ThisWindow.Update
      CustomerEnquiryReportCriteria
      ThisWindow.Reset
    OF ?ColourReport
      ThisWindow.Update
      NetworkReportCriteria
      ThisWindow.Reset
    OF ?LoanUnitReport
      ThisWindow.Update
      LoanUnitReportCriteria
      ThisWindow.Reset
    OF ?InsertRetailSales
      ThisWindow.Update
      Update_Retail_Sales(0)
      ThisWindow.Reset
    OF ?PendingWebOrders
      ThisWindow.Update
      BrowsePendingWebOrders
      ThisWindow.Reset
    OF ?RetailSalesOutstandingStockOrders
      ThisWindow.Update
      BrowseOutstandingStockOrders
      ThisWindow.Reset
    OF ?WebOrders
      ThisWindow.Update
      BrowseWebOrders
      ThisWindow.Reset
    OF ?RetailSalesAdjustedWebOrderReceipts
      ThisWindow.Update
      AdjustedWebOrderReceipts
      ThisWindow.Reset
    OF ?RetailSalesPickingReconciliation
      ThisWindow.Update
      BrowsePickingReconciliation
      ThisWindow.Reset
    OF ?SalesAwaitingPayment
      ThisWindow.Update
      BrowseAwaitingPayment
      ThisWindow.Reset
    OF ?RetailSalesDespatchProcedure
      ThisWindow.Update
      Browse_To_Despatch
      ThisWindow.Reset
    OF ?RetailBackOrderProcessing
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RetailBackOrderProcessing, Accepted)
      Access:USERS.Open()
      Access:USERS.UseFile()
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = glo:Password
      Access:USERS.Tryfetch(use:Password_Key)
      
      ! Changing (DBH 22/09/2006) # 7963 - Make the temp file names more random
      !glo:File_Name   = 'M' & Clip(use:User_Code) & Format(Day(Today()),@n02) & '.TMP'
      !glo:File_Name2  = 'N' & Clip(use:User_Code) & Format(Day(Today()),@n02) & '.TMP'
      !glo:Tradetmp    = 'O' & Clip(use:User_Code) & Format(Day(Today()),@n02) & '.TMP'
      ! to (DBH 22/09/2006) # 7963
      glo:File_Name   = 'M' & Clip(use:User_Code) & Format(Day(Today()),@n02) & Clock() & '.TMP'
      glo:File_Name2  = 'N' & Clip(use:User_Code) & Format(Day(Today()),@n02) & Clock() & '.TMP'
      glo:Tradetmp    = 'O' & Clip(use:User_Code) & Format(Day(Today()),@n02) & Clock() & '.TMP'
      ! End (DBH 22/09/2006) #7963
      
      Access:USERS.Close()
      BackOrderProcessing()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RetailBackOrderProcessing, Accepted)
    OF ?RetailSalesBrowseAllSales
      ThisWindow.Update
      Browse_All_Retail_Sales
      ThisWindow.Reset
    OF ?About
      ThisWindow.Update
      AboutScreen
      ThisWindow.Reset
    OF ?Browse_Jobs_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse_Jobs_Button, Accepted)
      Access:USERS_ALIAS.Open()
      Access:USERS_ALIAS.UseFile()
      Access:ACCAREAS_ALIAS.Open()
      Access:ACCAREAS_ALIAS.UseFile()
      If SecurityCheck('JOB PROGRESS')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
      Else!if passed" = False
      
          Browse_Jobs
      
      End!if passed" = False
      Access:USERS_ALIAS.Close()
      Access:ACCAREAS_ALIAS.Close()
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse_Jobs_Button, Accepted)
    OF ?Reports:2
      ThisWindow.Update
      ExternalReports
      ThisWindow.Reset
    OF ?StockControl
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StockControl, Accepted)
      Access:USERS_ALIAS.Open()
      Access:USERS_ALIAS.UseFile()
      Access:ACCAREAS_ALIAS.Open()
      Access:ACCAREAS_ALIAS.UseFile()
      If SecurityCheck('STOCK CONTROL')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
      Else!if passed" = False
      
          Browse_Stock
      
      End!if passed" = False
      Access:USERS_ALIAS.Close()
      Access:ACCAREAS_ALIAS.Close()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StockControl, Accepted)
    OF ?ButtonRecVPJ
      ThisWindow.Update
      BrowsePreJob
      ThisWindow.Reset
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020562'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020562'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020562'&'0')
      ***
    OF ?Button7
      ThisWindow.Update
      CreateNewJob
      ThisWindow.Reset
    OF ?Button8
      ThisWindow.Update
      JobSearchWindow
      ThisWindow.Reset
    OF ?Button:SingleIMEISearch
      ThisWindow.Update
      SingleIMEISearchWindow
      ThisWindow.Reset
    OF ?RapidProcedures
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RapidProcedures, Accepted)
      Execute Popup('Single Job Booking|Multiple Job Booking|-|Job Allocation|Engineer Update|3rd Party Procedure|QA Procedure|' & |
          'Restock Loan/Exchange Units|Stock Procedures{{Return Orders|-|Stock Allocation|Stock Import|Stock Receive Procedure|-|Spare/Exchange/Loan Return Processing}|' & |
          'Audit Procedures{{Exchange Audit|Stock Audit|Work In Progress Audit}|-|Waybill Confirmation|Waybill Procedure|Create Sundry Waybill|-|' & |
          'Handover Confirmation|OBF Processing|ARC Warranty Claims|-|Automated Report Scheduler')
          Begin  ! Single Job Booking
              if (SecurityCheck('JOBS - INSERT'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              glo:Insert_Global = 'YES'
              Update_Jobs
          End ! Begin
          Begin ! Multiple Job Booking
              if (SecurityCheck('JOBS - INSERT'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              Multiple_Job_Booking
          End
          Begin ! Job Allocation
              if (SecurityCheck('RAPID - JOB ALLOCATION'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              RUN('CELRAPST.EXE %' & Clip(glo:Password))
          End
          Begin ! Engineers Update
              if (SecurityCheck('RAPID - ENGINEER UPDATE'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              If GetTempPathA(255,TempFilePath)
                  If Sub(Clip(TempFilePath),-1,1) = '\'
                      glo:FileName = Clip(TempFilePath) & 'RAPENG' & Clock() & '.TMP'
                  Else !If Sub(Clip(TempFilePath),-1,1) = '\'
                      glo:FileName = Clip(TempFilePath) & '\RAPENG' & Clock() & '.TMP'
                  End !If Sub(Clip(TempFilePath),-1,1) = '\'
              End
      
              Remove(glo:FileName)
      
              Update_Engineer_Job
      
              Remove(glo:FileName)
          End ! Begin
          Begin ! Rapid Third Party Despatch
              if (SecurityCheck('RAPID - THIRD PARTY DESPATCH'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              RUN('CELRAPTP.EXE %' & Clip(glo:Password))
          End
          Begin  ! QA
              if (SecurityCheck('RAPID - QA PROCEDURE'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              RUN('CELRAPQA.EXE %' & Clip(glo:Password))
          End
          Begin ! Restock Loan/Exchange
              if (SecurityCheck('RAPID - RESTOCK LOAN/EXCHANGE'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              RUN('CELRAPRS.EXE %' & Clip(glo:Password))
          End
          Begin ! Browse Return Orders
              if (SecurityCheck('BROWSE RETURN ORDERS'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              BrowseReturnOrders()
          END
          Begin ! Stock Allocation
              if (SecurityCheck('RAPID - STOCK ALLOCATION'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              RUN('CELRAPSN.EXE %' & Clip(glo:Password))
          End
          Begin ! Stock Import
              if (SecurityCheck('RAPID - STOCK IMPORT'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              StockImport
          End
          Begin ! Stock Receive
              if (SecurityCheck('RAPID - STOCK RECEIVE'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              ReceiveStock
          End
          Begin ! Browse Return Orders
              if (SecurityCheck('SPARE/EXCHANGE PROCESSING'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              ReturnProcessing()
          END
          Begin ! Exchange Audit
              if (SecurityCheck('RAPID - EXCHANGE AUDIT'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              BrowseExcAudit
          end
          Begin ! Stock Audit
              if (SecurityCheck('RAPID - STOCK AUDIT'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              BrowseAudit
          end
          Begin ! WIP Audit
              if (SecurityCheck('RAPID - WIP AUDIT'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              BrowseWIPAudit
          End
          Begin ! Waybill Confirmation
              if (SecurityCheck('RAPID - WAYBILL CONFIRMATION'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              If GetTempPathA(255,TempFilePath)
                  If Sub(TempFilePath,-1,1) = '\'
                      glo:FileName = Clip(TempFilePath) & 'WAYBILLCONF' & Clock() & '.TMP'
                  Else !If Sub(TempFilePath,-1,1) = '\'
                      glo:FileName = Clip(TempFilePath) & '\WAYBILLCONF' & Clock() & '.TMP'
                  End !If Sub(TempFilePath,-1,1) = '\'
              End
      
              WayBillConfirmation
      
              Remove(glo:FileName)
          End ! Begin
          Begin   !Waybill Procedure
              BrowseWaybConf
          END
          Begin
              if (SecurityCheck('RAPID - CREATE SUNDRY WAYBILL'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              CreateSundryWaybill
              Remove(Glo:FileName)
          End ! Begin
          Begin ! Hand Over Confirmation
              if (SecurityCheck('RAPID - HANDOVER CONFIRMATION'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              RUN('CELRAPHO.EXE %' & Clip(glo:Password))
          End
          Begin
              If SecurityCheck('OBF PROCESSING') = Level:Benign
                  OBFProcessing
              Else
                  Case Missive('You do not have access to this option.','ServiceBase 3g',|
                                 'mstop.jpg','/OK') 
                      Of 1 ! OK Button
                  End ! Case Missive
              End ! If SecurityCheck('OBF PROCESSING')
      
          End !Begin
          Begin ! ARC Warranty Claims
              if (SecurityCheck('RAPID - ARC WARRANTY CLAIMS'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              RRCWarrantyProcess
          End
          Begin ! Report Scheduler
              if (SecurityCheck('RAPID - REPORT SCHEDULER'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              RUN('CELRAPRC.EXE %' & Clip(glo:Password))
          End
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RapidProcedures, Accepted)
    OF ?Button:IMEIValidation
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:IMEIValidation, Accepted)
      AssocFile = Clip(GETINI('URL','IMEIValidation',,Clip(Path()) & '\SB2KDEF.INI'))
      Param = ''
      Dir = ''
      Operation = 'Open'
      ShellExecute(GetDesktopWindow(), Operation, AssocFile, Param, Dir, 5)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:IMEIValidation, Accepted)
    OF ?Button:GlobalIMEISearch
      ThisWindow.Update
      GlobalIMEISearchWindow
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F10Key
              Do Log_Out
              Do Login
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

CreatePendingOrders PROCEDURE                         !Generated from procedure template - Process

Progress:Thermometer BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Count            LONG
save_ope_id          USHORT,AUTO
Process:View         VIEW(SUPPLIER)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,148,60),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(4,4,140,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(4,30,140,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(44,40,56,16),USE(?Progress:Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          ProcessClass                     !Process Manager
ProgressMgr          StepStringClass                  !Progress Manager
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Count)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
      glo:file_name   = 'P' & Format(CLock(),@n07) & '.TMP'
  GlobalErrors.SetProcedureName('CreatePendingOrders')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ORDPEND.Open
  Relate:SUPPTEMP.Open
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','CreatePendingOrders')
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  ProgressMgr.Init(ScrollSort:AllowAlpha+ScrollSort:AllowNumeric,ScrollBy:RunTime)
  ThisProcess.Init(Process:View, Relate:SUPPLIER, ?Progress:PctText, Progress:Thermometer, ProgressMgr, sup:Company_Name)
  ThisProcess.CaseSensitiveValue = FALSE
  ThisProcess.AddSortOrder(sup:Company_Name_Key)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}='Create Pending Orders'
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(SUPPLIER,'QUICKSCAN=on')
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Init PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)

  CODE
  PARENT.Init(PC,R,PV)
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','CreatePendingOrders')


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ORDPEND.Close
    Relate:SUPPTEMP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','CreatePendingOrders')
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeRecord, (),BYTE)
      found# = 0
      save_ope_id = access:ordpend.savefile()
      access:ordpend.clearkey(ope:supplier_name_key)
      ope:supplier = sup:company_name
      set(ope:supplier_name_key,ope:supplier_name_key)
      loop
          if access:ordpend.next()
             break
          end !if
          if ope:supplier <> sup:company_name      |
              then break.  ! end if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if
          If ope:StockReqNumber = 0
              found# = 1
              Break
          End !If ope:StockReqNumber = 0
      end !loop
      access:ordpend.restorefile(save_ope_id)
      If found# = 1
          tmp:Count += 1
          If Access:SUPPTEMP.PrimeRecord() = Level:Benign
              suptmp:Account_Number   = sup:Account_Number
              suptmp:Company_Name     = sup:Company_Name
              suptmp:Minimum_Order_Value  = sup:Minimum_Order_Value
              If Access:SUPPTEMP.TryInsert() = Level:Benign
                  !Insert Successful
              Else !If Access:SUPPTEMP.TryInsert() = Level:Benign
                  !Insert Failed
              End !If Access:SUPPTEMP.TryInsert() = Level:Benign
          End !If Access:SUPPTEMP.PrimeRecord() = Level:Benign
      End!If found# = 1
  ReturnValue = PARENT.TakeRecord()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Start_Screen PROCEDURE                                !Generated from procedure template - Window

LocalRequest         LONG
OriginalRequest      LONG
LocalResponse        LONG
FilesOpened          BYTE
WindowOpened         LONG
WindowInitialized    LONG
ForceRefresh         LONG
ASCIIFileSize        LONG
ASCIIBytesThisRead   LONG
ASCIIBytesRead       LONG
ASCIIBytesThisCycle  LONG
ASCIIPercentProgress BYTE
FrameTitle           CSTRING(128)
OtherPgmHwnd         Hwnd !Handle=unsigned=hwnd
tmp:LicenseText      STRING(10000)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('ServiceBase 3g'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),ALRT(AltShiftF12),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,37,352,147),USE(?Panel5),FILL(COLOR:White),BEVEL(-1,-1)
                       PANEL,AT(164,188,352,178),USE(?Panel3),FILL(09A6A7CH)
                       TEXT,AT(168,192,344,170),USE(tmp:LicenseText),SKIP,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White)
                       PANEL,AT(164,370,352,28),USE(?Panel2),FILL(09A6A7CH)
                       BUTTON,AT(384,370),USE(?Ok),TRN,FLAT,LEFT,ICON('accept9p.jpg'),DEFAULT
                       BUTTON,AT(452,370),USE(?Cancel),TRN,FLAT,LEFT,ICON('declinep.jpg'),STD(STD:Close)
                       PANEL,AT(164,20,352,14),USE(?Panel4),FILL(09A6A7CH)
                       PROMPT('License Agreement'),AT(168,22),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       IMAGE('sbtitle.jpg'),AT(176,40),USE(?Image3),CENTERED
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Start_Screen')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel5
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  tmp:LicenseText = 'SOFTWARE LICENCE AGREEMENT'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>Copying, duplicating or otherwise distributing any part of this product without the'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>prior written consent of an authorised representative of the above is prohibited. '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>This Software is licensed subject to the following conditions. '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>LICENCE AGREEMENT. This is a legal agreement between you (either an '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>individual or an entity) and the manufacturer of the computer software  PC '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>Control Systems Ltd..  '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>If you operate this SOFTWARE  you are agreeing to be bound by the terms of '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>this agreement. If you do not agree to the terms of this agreement, promptly '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>return the SOFTWARE and the accompanying items (including written materials '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>and binders or other containers) to the place you obtained them from. '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>Discretionary compensation may be paid dependent upon the time elapsed since '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>the purchase date of the SOFTWARE in all events any form of compensation will '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>be at the sole discretion of and expressly set by PC Control Systems Ltd..'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>GRANT OF LICENCE.  This Licence Agreement permits you to use one copy '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>of the enclosed  software program ( the "SOFTWARE") on a single computer '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>if you purchased a single user version or on a network of computers if you '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>purchased a network user version. The SOFTWARE is in "use" on a computer '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>when it is loaded into temporary memory (i.e. RAM) or installed into permanent '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>memory (i.e. hard disk, CD-ROM, or other  storage device) of that computer.  '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>Network users are limited to the number of  simultaneous accesses to the '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>SOFTWARE  by the network. This is dependent upon the user version '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>purchased.  The user version is clearly stated on the original SOFTWARE '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>purchase invoice.  '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>COPYRIGHT:  The enclosed SOFTWARE and Documentation is protected by '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>copyright laws and international treaty provisions and are the proprietary '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>products of PC Control Systems Ltd. and its third party suppliers from whom '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>PC Control Systems Ltd. has licensed  portions of the Software.  Such suppliers '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>are expressly  understood  to be beneficiaries of the terms and provisions of '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>this  Agreement.  All rights that are not expressly  granted are reserved by '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>PC Control Systems Ltd. or its suppliers. You must treat the SOFTWARE like '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>any other copyrighted material (e.g. a book or musical recording) except that '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>you may either (a) make one copy of the SOFTWARE solely for backup '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>or archival purposes, or (b) transfer the SOFTWARE to a single hard disk '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>provided you keep the original solely for backup or archival purposes.  '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>You may not copy the written materials accompanying the SOFTWARE.'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>DUEL MEDIA SOFTWARE . If the SOFTWARE  package contains both 3.5" '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>and 5.25" or 3.5" and CD ROM disks, then you may use only the disks '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>appropriate for your single-user computer or your file server.  You may not use '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>the other disks on any other computer or loan, rent, lease, or transfer them to '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>another user except as part of the permanent transfer (as provided above) of all '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>SOFTWARE and written materials.'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>LIMITED WARRANTY.  PC Control Systems Ltd. warrants that (a) the '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>SOFTWARE will perform substantially in accordance with the accompanying '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>written materials for a period of ninety (90) days from the date of receipt, '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>and (b) any hardware accompanying the SOFTWARE will be free of defects in '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>materials and workmanship under normal use and service for a period of one '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>(1) year from the date of receipt. Any implied warranties on the '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>SOFTWARE and hardware are limited to ninety (90) days and one  '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>(1) year, respectively.  Some jurisdictions do not allow limitations '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>duration of an implied warranty, so the above limitation may not '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>apply to you.'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>CUSTOMER REMEDIES.  PC Control Systems Ltd. and its suppliers entire '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>liability and your exclusive remedy shall be, at PC Control Systems Ltd.  '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>option, either (a) return of the price paid , or (b) repair or replacement of '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>the SOFTWARE that does not meet PC Control Systems Ltd. Warranty and which '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>is returned to PC Control Systems Ltd. with a copy of your SOFTWARE '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>purchase invoice.  This Limited Warranty is void if failure of the SOFTWARE '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>has resulted from accident, abuse, or misapplication.  Any replacement '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>SOFTWARE will be warranted for the remainder of the original warranty period '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>or thirty (30) days, whichever is the longer.  '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>NO OTHER WARRANTIES.  To the maximum extent permitted by applicable '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>law, PC Control Systems Ltd. and its suppliers disclaim all other warranties, '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>either  express or implied, including, but not limited to implied warranties of '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>merchantability and fitness for a particular purpose, with regard to the '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>SOFTWARE, the accompanying written materials, and any accompanying '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>hardware.  This limited warranty gives you specific legal rights, and you may '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>also have other rights which vary from jurisdiction to jurisdiction.'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>NO LIABILITY FOR CONSEQUENTIAL DAMAGES. To the maximum extent permitted '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>by applicable law, in no event shall PC Control Systems Ltd.  or its suppliers '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>be liable for any damage  whatsoever ( including without  limitation, '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>damages for loss of business profits, business interruption, loss of business '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>information, or any other pecuniary loss)  arising  out of the use of or '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>inability to use this product,  even if PC Control Systems Ltd. has been advised  '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>of the possibility  of such damages.  Because some jurisdictions do not allow the '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>exclusion or limitation of liability for consequential or incidental damages, the '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>above limitation may not apply to you.'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>'
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>SUPPORT.   PC Control Systems Ltd.  will attempt to answer  your technical '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>support requests concerning the SOFTWARE; however, this service is offered '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>on a reasonable efforts basis only, and PC Control Systems Ltd. may not be '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>able to resolve every support request.  PC Control Systems Ltd. supports '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>the Software only if it is used under conditions and on operating systems '
  tmp:LicenseText = Clip(tmp:LicenseText) & '<13,10>for which the Software is designed. '
  PUTINI('STARTING','Run',True,)
  
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  glo:select1 = 'CANCEL'
      ! Save Window Name
   AddToLog('Window','Open','Start_Screen')
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','Start_Screen')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Ok
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Ok, Accepted)
      !If Today() > Deformat('10/3/2003',@d6)
      !    Message('System Error: 1701')
      !    Halt()
      !End !Today() > Deformat('28/02/2003',@d6)
      If GETINI('SB2000','FontsInstalled',,) = 1
          glo:select1 = 'OK'
      Else !GETINI('SB2000','FontsInstalled',,) = 1
          RUN('SBFONTS.EXE',1)
          If ~Error()
              PUTINI('SB2000','FontsInstalled','1',)
              BEEP(BEEP:SystemExclamation)  ;  YIELD()
              CASE MESSAGE('Your system has been updated. '&|
                      '||Please reboot your computer before running ServiceBase 3g '&|
                      'again.', |
                      'ServiceBase 3g', ICON:Exclamation, |
                       BUTTON:OK, BUTTON:OK, 0)
              Of BUTTON:OK
              End !CASE
              Halt()
          Else
              glo:select1 = 'OK'
          End !If ~Error()
      End !GETINI('SB2000','FontsInstalled',,) = 1
      
      
      
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Ok, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      If KeyCode() = AltShiftF12
        Message('ACCAREAS: ' & Format((Size(acc:record)/1024),@n6.2) & '<13,10>' & |
                  'ACCESDEF: ' & Format((Size(ACD:record)/1024),@n6.2) & '<13,10>' & |
                  'ACCESSOR: ' & Format((Size(ACR:record)/1024),@n6.2) & '<13,10>' & |
                  'ACTION:   ' & Format((Size(ACT:record)/1024),@n6.2) & '<13,10>' & |
                  'ALLLEVEL: ' & Format((Size(ALL:record)/1024),@n6.2) & '<13,10>' & |
                  'AUDIT:    ' & Format((Size(AUD:record)/1024),@n6.2) & '<13,10>' & |
                  'BOUNCER:  ' & Format((Size(BOU:record)/1024),@n6.2) & '<13,10>' & |
                  'CHARTYPE: ' & Format((Size(CHA:record)/1024),@n6.2) & '<13,10>' & |
                  'CITYSERV: ' & Format((Size(CIT:record)/1024),@n6.2) & '<13,10>' & |
                  'COLOUR:   ' & Format((Size(COL:record)/1024),@n6.2) & '<13,10>' & |
                  'COMMCAT:  ' & Format((Size(CMC:record)/1024),@n6.2) & '<13,10>' & |
                  'COMMONCP: ' & Format((Size(CCP:record)/1024),@n6.2) & '<13,10>' & |
                  'COMMONFA: ' & Format((Size(COM:record)/1024),@n6.2) & '<13,10>' & |
                  'COMMONWP: ' & Format((Size(CWP:record)/1024),@n6.2) & '<13,10>' & |
                  'CONSIGN:  ' & Format((Size(CNS:record)/1024),@n6.2) & '<13,10>' & |
                  'CONTACTS: ' & Format((Size(CON:record)/1024),@n6.2)& '<13,10>' & |
                  'CONTHIST: ' & Format((Size(CHT:record)/1024),@n6.2) & '<13,10>' & |
                  'COURIER:  ' & Format((Size(COU:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFAULTS: ' & Format((Size(DEF:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFCHRGE: ' & Format((Size(DEC:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFEDI:   ' & Format((Size(EDI:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFEDI2:  ' & Format((Size(ED2:record)/1024),@n6.2),'ServiceBase 2000')
      
      
        Message('DEFPRINT: ' & Format((Size(DEP:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFRAPID: ' & Format((Size(DER:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFSTOCK: ' & Format((Size(DST:record)/1024),@n6.2) & '<13,10>' & |
                  'DEFWEB:   ' & Format((Size(DEW:record)/1024),@n6.2) & '<13,10>' & |
                  'DESBATCH: ' & Format((Size(DBT:record)/1024),@n6.2) & '<13,10>' & |
                  'DISCOUNT: ' & Format((Size(DIS:record)/1024),@n6.2) & '<13,10>' & |
                  'EDIBATCH: ' & Format((Size(EBT:record)/1024),@n6.2) & '<13,10>' & |
                  'ESNMODEL: ' & Format((Size(ESN:record)/1024),@n6.2) & '<13,10>' & |
                  'ESTPARTS: ' & Format((Size(EPR:record)/1024),@n6.2) & '<13,10>' & |
                  'EXCAUDIT: ' & Format((Size(EXA:record)/1024),@n6.2) & '<13,10>' & |
                  'EXCCHRGE: ' & Format((Size(EXC:record)/1024),@n6.2) & '<13,10>' & |
                  'EXCHACC:  ' & Format((Size(XCA:record)/1024),@n6.2) & '<13,10>' & |
                  'EXCHANGE: ' & Format((Size(XCH:record)/1024),@n6.2) & '<13,10>' & |
                  'EXCHHIST: ' & Format((Size(EXH:record)/1024),@n6.2) & '<13,10>' & |
                  'INVOICE:  ' & Format((Size(INV:record)/1024),@n6.2) & '<13,10>' & |
                  'INVPARTS: ' & Format((Size(IVP:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBACC:   ' & Format((Size(JAC:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBBATCH: ' & Format((Size(JBT:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBEXACC: ' & Format((Size(JEA:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBEXHIS: ' & Format((Size(JXH:record)/1024),@n6.2) ,'ServiceBase 2000')
      
        Message('JOBLOHIS: ' & Format((Size(JLH:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBPAYMT: ' & Format((Size(JPT:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBS:     ' & Format((Size(JOB:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBSE:    ' & Format((Size(JOBE:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBTHIRD: ' & Format((Size(JOt:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBNOTES: ' & Format((Size(JBN:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBSTAGE: ' & Format((Size(JST:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBSVODA: ' & Format((Size(JVF:record)/1024),@n6.2) & '<13,10>' & |
                  'JOBVODAC: ' & Format((Size(JVA:record)/1024),@n6.2) & '<13,10>' & |
                  'LETTERS:  ' & Format((Size(LET:record)/1024),@n6.2) & '<13,10>' & |
                  'LOAN:     ' & Format((Size(LOA:record)/1024),@n6.2) & '<13,10>' & |
                  'LOANACC:  ' & Format((Size(LAC:record)/1024),@n6.2) & '<13,10>' & |
                  'LOANHIST: ' & Format((Size(LOH:record)/1024),@n6.2) & '<13,10>' & |
                  'LOCATION: ' & Format((Size(LOC:record)/1024),@n6.2) & '<13,10>' & |
                  'LOCINTER: ' & Format((Size(LOI:record)/1024),@n6.2) & '<13,10>' & |
                  'LOCSHELF: ' & Format((Size(LOS:record)/1024),@n6.2) & '<13,10>' & |
                  'LOGGED:   ' & Format((Size(LOG:record)/1024),@n6.2) & '<13,10>' & |
                  'MANFAULO: ' & Format((Size(MFO:record)/1024),@n6.2) & '<13,10>' & |
                  'MANFAULT: ' & Format((Size(MAF:record)/1024),@n6.2) & '<13,10>' & |
                  'MANFAUPA: ' & Format((Size(MAP:record)/1024),@n6.2) & '<13,10>' & |
                  'MANFPALO: ' & Format((Size(MFP:record)/1024),@n6.2) & '<13,10>' & |
                  'MANUFACT: ' & Format((Size(MAN:record)/1024),@n6.2) & '<13,10>' & |
                  'MERGE:    ' & Format((Size(MER:record)/1024),@n6.2) ,'ServiceBase 2000')
        Message('MERGELET: ' & Format((Size(MRG:record)/1024),@n6.2) & '<13,10>' & |
                  'MERGETXT: ' & Format((Size(MRT:record)/1024),@n6.2) & '<13,10>' & |
                  'MESSAGES: ' & Format((Size(MES:record)/1024),@n6.2) & '<13,10>' & |
                  'MODELCOL: ' & Format((Size(MOC:record)/1024),@n6.2) & '<13,10>' & |
                  'MODELNUM: ' & Format((Size(MOD:record)/1024),@n6.2) & '<13,10>' & |
                  'NEWFEAT:  ' & Format((Size(FEA:record)/1024),@n6.2) & '<13,10>' & |
                  'NOTESENG: ' & Format((Size(NOE:record)/1024),@n6.2) & '<13,10>' & |
                  'NOTESFAU: ' & Format((Size(NOF:record)/1024),@n6.2) & '<13,10>' & |
                  'NOTESINV: ' & Format((Size(NOI:record)/1024),@n6.2) & '<13,10>' & |
                  'ORDERS:   ' & Format((Size(ORD:record)/1024),@n6.2) & '<13,10>' & |
                  'ORDPARTS: ' & Format((Size(ORP:record)/1024),@n6.2) & '<13,10>' & |
                  'ORDPEND:  ' & Format((Size(OPE:record)/1024),@n6.2) & '<13,10>' & |
                  'PARTS:    ' & Format((Size(PAR:record)/1024),@n6.2) & '<13,10>' & |
                  'PAYTYPES: ' & Format((Size(PAY:record)/1024),@n6.2) & '<13,10>' & |
                  'PRIORITY: ' & Format((Size(PRI:record)/1024),@n6.2) & '<13,10>' & |
                  'PROCCODE: ' & Format((Size(PRO:record)/1024),@n6.2) & '<13,10>' & |
                  'PROINV:   ' & Format((Size(PRV:record)/1024),@n6.2) & '<13,10>' & |
                  'QAREASON: ' & Format((Size(QAR:record)/1024),@n6.2) & '<13,10>' & |
                  'QUERYREA: ' & Format((Size(QUE:record)/1024),@n6.2) & '<13,10>' & |
                  'REPAIRTY: ' & Format((Size(REP:record)/1024),@n6.2),'ServiceBase 2000')
        Message('REPTYDEF: ' & Format((Size(RTD:record)/1024),@n6.2) & '<13,10>' & |
                  'RETDESNO: ' & Format((Size(RdN:record)/1024),@n6.2) & '<13,10>' & |
                  'RETPAY:   ' & Format((Size(RTP:record)/1024),@n6.2) & '<13,10>' & |
                  'RETSALES: ' & Format((Size(RET:record)/1024),@n6.2) & '<13,10>' & |   
                  'RETSTOCK: ' & Format((Size(RES:record)/1024),@n6.2) & '<13,10>' & |
                  'SMSMAIL:  ' & Format((Size(SMS:record)/1024),@n6.2) & '<13,10>' & |
                  'SMSTEXT:  ' & Format((Size(SMT:record)/1024),@n6.2) & '<13,10>' & |
                  'STAHEAD:  ' & Format((Size(STH:record)/1024),@n6.2) & '<13,10>' & |
                  'STANTEXT: ' & Format((Size(STT:record)/1024),@n6.2) & '<13,10>' & |
                  'STATUS:   ' & Format((Size(STS:record)/1024),@n6.2) & '<13,10>' & |
                  'STDCHRGE: ' & Format((Size(STA:record)/1024),@n6.2) & '<13,10>' & |
                  'STOCK:    ' & Format((Size(STO:record)/1024),@n6.2) & '<13,10>' & |
                  'STOCKTYP: ' & Format((Size(STP:record)/1024),@n6.2) & '<13,10>' & |
                  'STOESN:   ' & Format((Size(STE:record)/1024),@n6.2) & '<13,10>' & |
                  'STOHIST:  ' & Format((Size(SHI:record)/1024),@n6.2) & '<13,10>' & |
                  'STOMODEL: ' & Format((Size(STM:record)/1024),@n6.2) & '<13,10>' & |
                  'SUBCHRGE: ' & Format((Size(SUC:record)/1024),@n6.2) & '<13,10>' & |
                  'SUBTRACC: ' & Format((Size(SUB:record)/1024),@n6.2) & '<13,10>' & |
                  'SUPPLIER: ' & Format((Size(SUP:record)/1024),@n6.2) & '<13,10>' & |
                  'TEAMS:    ' & Format((Size(TEA:record)/1024),@n6.2) & '<13,10>' & |
                  'TRACHAR:  ' & Format((Size(TCH:record)/1024),@n6.2) & '<13,10>' & |
                  'TRADEACC: ' & Format((Size(TRA:record)/1024),@n6.2) ,'ServiceBase 2000')
        Message('TRANTYPE: ' & Format((Size(TRT:record)/1024),@n6.2) & '<13,10>' & |
                  'TRDBATCH: ' & Format((Size(TRB:record)/1024),@n6.2) & '<13,10>' & |
                  'TRDMODEL: ' & Format((Size(TRM:record)/1024),@n6.2) & '<13,10>' & |
                  'TRDPARTY: ' & Format((Size(TRD:record)/1024),@n6.2) & '<13,10>' & |
                  'TRDSPEC:  ' & Format((Size(TSP:record)/1024),@n6.2) & '<13,10>' & |
                  'TURNARND: ' & Format((Size(TUR:record)/1024),@n6.2) & '<13,10>' & |
                  'UNITTYPE: ' & Format((Size(UNI:record)/1024),@n6.2) & '<13,10>' & |
                  'USELEVEL: ' & Format((Size(LEV:record)/1024),@n6.2) & '<13,10>' & |
                  'USERS:    ' & Format((Size(USE:record)/1024),@n6.2) & '<13,10>' & |
                  'USMASSIG: ' & Format((Size(USM:record)/1024),@n6.2) & '<13,10>' & |
                  'USUMASIG: ' & Format((Size(USU:record)/1024),@n6.2) & '<13,10>' & |
                  'VATCODE:  ' & Format((Size(VAT:record)/1024),@n6.2) & '<13,10>' & |
                  'WARPARTS: ' & Format((Size(WPR:record)/1024),@n6.2) & '<13,10>' & |
                  'WEBJOB:   ' & Format((Size(WOB:record)/1024),@n6.2),'ServiceBase 2000')
      End!If KeyCode() = ShiftAltF12
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:LoseFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(LoseFocus)
      !Error#    = SetWindowPos(0{Prop:Handle},-1,0,0,0,0,3)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(LoseFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

TurnaroundTimeProcess PROCEDURE                       !Generated from procedure template - Process

Progress:Thermometer BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Process:View         VIEW(JOBS)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,148,60),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(4,4,140,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(4,30,140,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(44,40,56,16),USE(?Progress:Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                    !Progress Manager
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('TurnaroundTimeProcess')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:JOBS.Open
  Relate:STATUS.Open
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','TurnaroundTimeProcess')
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisProcess.AddSortOrder(job:Ref_Number_Key)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}='Turnaround Time Process'
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(JOBS,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)

  CODE
  PARENT.Init(PC,R,PV)
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','TurnaroundTimeProcess')


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
    Relate:STATUS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','TurnaroundTimeProcess')
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
      access:status.clearkey(sts:ref_number_only_key)
      sts:ref_number  = Sub(job:current_status,1,3)
      IF access:status.tryfetch(sts:ref_number_only_key)   = Level:Benign
          If sts:use_turnaround_time  = 'YES'
              Turnaround_Routine(sts:turnaround_days,sts:turnaround_hours,end_date",end_time")
              job:status_end_date = end_date"
              job:status_end_time = end_time"
          Else!If sts:use_turnaround_time = 'YES'
              job:status_end_date = Deformat('1/1/2050',@d6)
              job:status_end_Time = Clock()           
          End!If sts:use_turnaround_time  = 'YES'
          access:jobs.update()
      End!IF access:status.tryfetch(sts:ref_number_only_key)   = Level:Benign
  ReturnValue = PARENT.TakeRecord()
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

do_not_delete PROCEDURE                               !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Caption'),AT(,,272,80),FONT('Arial',8,,FONT:regular),COLOR(COLOR:Teal),CENTER,GRAY,DOUBLE
                       PANEL,AT(4,52,264,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('do_not_delete')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCAREAS.Open
  Relate:ACCAREAS_ALIAS.Open
  Relate:ACCESDEF.Open
  Relate:ACTION.Open
  Relate:CITYSERV.Open
  Relate:COLOUR.Open
  Relate:COMMCAT.Open
  Relate:COMMONCP_ALIAS.Open
  Relate:COMMONFA_ALIAS.Open
  Relate:COMMONWP_ALIAS.Open
  Relate:CONSIGN.Open
  Relate:CONTACTS.Open
  Relate:COURIER_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:DEFAULTV.Open
  Relate:DEFEDI.Open
  Relate:DEFEDI2.Open
  Relate:DEFEPS.Open
  Relate:DEFPRINT.Open
  Relate:DEFRAPID.Open
  Relate:DEFSTOCK.Open
  Relate:DEFWEB.Open
  Relate:DESBATCH.Open
  Relate:DISCOUNT.Open
  Relate:EDIBATCH.Open
  Relate:EPSCSV.Open
  Relate:EPSIMP.Open
  Relate:ESNMODEL.Open
  Relate:EXCAUDIT.Open
  Relate:EXCCHRGE.Open
  Relate:EXPAUDIT.Open
  Relate:EXPBUS.Open
  Relate:EXPCITY.Open
  Relate:EXPGEN.Open
  Relate:EXPGENDM.Open
  Relate:EXPJOBS.Open
  Relate:EXPLABG.Open
  Relate:EXPPARTS.Open
  Relate:EXPSPARES.Open
  Relate:EXPWARPARTS.Open
  Relate:IMPCITY.Open
  Relate:INVOICE_ALIAS.Open
  Relate:INVPARTS.Open
  Relate:INVPATMP.Open
  Relate:JOBACTMP.Open
  Relate:JOBBATCH.Open
  Relate:JOBEXACC.Open
  Relate:JOBNOTES_ALIAS.Open
  Relate:JOBPAYMT.Open
  Relate:JOBS2_ALIAS.Open
  Relate:JOBSTMP.Open
  Relate:JOBSVODA.Open
  Relate:LABLGTMP.Open
  Relate:LETTERS.Open
  Relate:LOAN_ALIAS.Open
  Relate:LOCATION_ALIAS.Open
  Relate:LOG2TEMP.Open
  Relate:LOGASSST.Open
  Relate:LOGASSSTTEMP.Open
  Relate:LOGASSST_ALIAS.Open
  Relate:LOGCLSTE.Open
  Relate:LOGDEFLT.Open
  Relate:LOGGED.Open
  Relate:LOGRETRN.Open
  Relate:LOGSALCD.Open
  Relate:LOGSERST_ALIAS.Open
  Relate:LOGSTOCK_ALIAS.Open
  Relate:LOGSTOLC.Open
  Relate:LOGTEMP.Open
  Relate:MERGE.Open
  Relate:MERGELET.Open
  Relate:MESSAGES.Open
  Relate:NEWFEAT.Open
  Relate:NOTESENG.Open
  Relate:NOTESINV.Open
  Relate:ORDPARTS_ALIAS.Open
  Relate:ORDPEND_ALIAS.Open
  Relate:PARAMSS.Open
  Relate:PARTSTMP.Open
  Relate:PARTSTMP_ALIAS.Open
  Relate:PARTS_ALIAS.Open
  Relate:PAYTYPES.Open
  Relate:PROCCODE.Open
  Relate:PROINV.Open
  Relate:QAREASON.Open
  Relate:QUERYREA.Open
  Relate:RETDESNO.Open
  Relate:RETSALES_ALIAS.Open
  Relate:RETSTOCK_ALIAS.Open
  Relate:SMSMAIL.Open
  Relate:SMSText.Open
  Relate:STAHEAD.Open
  Relate:STANTEXT.Open
  Relate:STDCHRGE_ALIAS.Open
  Relate:STOCK_ALIAS.Open
  Relate:STOMODEL_ALIAS.Open
  Relate:SUPPTEMP.Open
  Relate:TRADETMP.Open
  Relate:TRANTYPE.Open
  Relate:TRDBATCH_ALIAS.Open
  Relate:TURNARND.Open
  Relate:UPDDATA.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Relate:VODAIMP.Open
  Relate:WARPARTS_ALIAS.Open
  Relate:WEBJOB.Open
  Relate:WPARTTMP.Open
  Relate:WPARTTMP_ALIAS.Open
  Relate:XREPACT.Open
  Access:ACCESSOR.UseFile
  Access:ALLLEVEL.UseFile
  Access:AUDIT.UseFile
  Access:BOUNCER.UseFile
  Access:CHARTYPE.UseFile
  Access:COMMONCP.UseFile
  Access:COMMONFA.UseFile
  Access:COMMONWP.UseFile
  Access:CONTHIST.UseFile
  Access:COURIER.UseFile
  Access:DEFCHRGE.UseFile
  Access:ESTPARTS.UseFile
  Access:EXCHACC.UseFile
  Access:EXCHANGE.UseFile
  Access:EXCHHIST.UseFile
  Access:INVOICE.UseFile
  Access:JOBACC.UseFile
  Access:JOBEXHIS.UseFile
  Access:JOBLOHIS.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBS.UseFile
  Access:JOBSTAGE.UseFile
  Access:JOBVODAC.UseFile
  Access:LOAN.UseFile
  Access:LOANACC.UseFile
  Access:LOANHIST.UseFile
  Access:LOCATION.UseFile
  Access:LOCINTER.UseFile
  Access:LOCSHELF.UseFile
  Access:MANFAULO.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAUPA.UseFile
  Access:MANFPALO.UseFile
  Access:MANUFACT.UseFile
  Access:MERGETXT.UseFile
  Access:MODELCOL.UseFile
  Access:MODELNUM.UseFile
  Access:NOTESFAU.UseFile
  Access:ORDERS.UseFile
  Access:ORDPARTS.UseFile
  Access:ORDPEND.UseFile
  Access:PARTS.UseFile
  Access:PRIORITY.UseFile
  Access:REPAIRTY.UseFile
  Access:REPTYDEF.UseFile
  Access:RETPAY.UseFile
  Access:RETSALES.UseFile
  Access:RETSTOCK.UseFile
  Access:STATUS.UseFile
  Access:STDCHRGE.UseFile
  Access:STOCK.UseFile
  Access:STOCKTYP.UseFile
  Access:STOESN.UseFile
  Access:STOHIST.UseFile
  Access:STOMODEL.UseFile
  Access:SUBCHRGE.UseFile
  Access:SUBTRACC.UseFile
  Access:SUPPLIER.UseFile
  Access:TEAMS.UseFile
  Access:TRACHAR.UseFile
  Access:TRACHRGE.UseFile
  Access:TRADEACC.UseFile
  Access:TRDBATCH.UseFile
  Access:TRDMODEL.UseFile
  Access:TRDPARTY.UseFile
  Access:TRDSPEC.UseFile
  Access:UNITTYPE.UseFile
  Access:USELEVEL.UseFile
  Access:USERS.UseFile
  Access:USMASSIG.UseFile
  Access:USUASSIG.UseFile
  Access:WARPARTS.UseFile
  Access:EXCHANGE_ALIAS.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:LOGSERST.UseFile
  Access:LOGSTHIS.UseFile
  Access:LOGSTOCK.UseFile
  Access:LOGSTHII.UseFile
  Access:LOGRTHIS.UseFile
  Access:LOGSTLOC.UseFile
  Access:PRODCODE.UseFile
  Access:LOGDESNO.UseFile
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','do_not_delete')
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS.Close
    Relate:ACCAREAS_ALIAS.Close
    Relate:ACCESDEF.Close
    Relate:ACTION.Close
    Relate:CITYSERV.Close
    Relate:COLOUR.Close
    Relate:COMMCAT.Close
    Relate:COMMONCP_ALIAS.Close
    Relate:COMMONFA_ALIAS.Close
    Relate:COMMONWP_ALIAS.Close
    Relate:CONSIGN.Close
    Relate:CONTACTS.Close
    Relate:COURIER_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:DEFAULTV.Close
    Relate:DEFEDI.Close
    Relate:DEFEDI2.Close
    Relate:DEFEPS.Close
    Relate:DEFPRINT.Close
    Relate:DEFRAPID.Close
    Relate:DEFSTOCK.Close
    Relate:DEFWEB.Close
    Relate:DESBATCH.Close
    Relate:DISCOUNT.Close
    Relate:EDIBATCH.Close
    Relate:EPSCSV.Close
    Relate:EPSIMP.Close
    Relate:ESNMODEL.Close
    Relate:EXCAUDIT.Close
    Relate:EXCCHRGE.Close
    Relate:EXPAUDIT.Close
    Relate:EXPBUS.Close
    Relate:EXPCITY.Close
    Relate:EXPGEN.Close
    Relate:EXPGENDM.Close
    Relate:EXPJOBS.Close
    Relate:EXPLABG.Close
    Relate:EXPPARTS.Close
    Relate:EXPSPARES.Close
    Relate:EXPWARPARTS.Close
    Relate:IMPCITY.Close
    Relate:INVOICE_ALIAS.Close
    Relate:INVPARTS.Close
    Relate:INVPATMP.Close
    Relate:JOBACTMP.Close
    Relate:JOBBATCH.Close
    Relate:JOBEXACC.Close
    Relate:JOBNOTES_ALIAS.Close
    Relate:JOBPAYMT.Close
    Relate:JOBS2_ALIAS.Close
    Relate:JOBSTMP.Close
    Relate:JOBSVODA.Close
    Relate:LABLGTMP.Close
    Relate:LETTERS.Close
    Relate:LOAN_ALIAS.Close
    Relate:LOCATION_ALIAS.Close
    Relate:LOG2TEMP.Close
    Relate:LOGASSST.Close
    Relate:LOGASSSTTEMP.Close
    Relate:LOGASSST_ALIAS.Close
    Relate:LOGCLSTE.Close
    Relate:LOGDEFLT.Close
    Relate:LOGGED.Close
    Relate:LOGRETRN.Close
    Relate:LOGSALCD.Close
    Relate:LOGSERST_ALIAS.Close
    Relate:LOGSTOCK_ALIAS.Close
    Relate:LOGSTOLC.Close
    Relate:LOGTEMP.Close
    Relate:MERGE.Close
    Relate:MERGELET.Close
    Relate:MESSAGES.Close
    Relate:NEWFEAT.Close
    Relate:NOTESENG.Close
    Relate:NOTESINV.Close
    Relate:ORDPARTS_ALIAS.Close
    Relate:ORDPEND_ALIAS.Close
    Relate:PARAMSS.Close
    Relate:PARTSTMP.Close
    Relate:PARTSTMP_ALIAS.Close
    Relate:PARTS_ALIAS.Close
    Relate:PAYTYPES.Close
    Relate:PROCCODE.Close
    Relate:PROINV.Close
    Relate:QAREASON.Close
    Relate:QUERYREA.Close
    Relate:RETDESNO.Close
    Relate:RETSALES_ALIAS.Close
    Relate:RETSTOCK_ALIAS.Close
    Relate:SMSMAIL.Close
    Relate:SMSText.Close
    Relate:STAHEAD.Close
    Relate:STANTEXT.Close
    Relate:STDCHRGE_ALIAS.Close
    Relate:STOCK_ALIAS.Close
    Relate:STOMODEL_ALIAS.Close
    Relate:SUPPTEMP.Close
    Relate:TRADETMP.Close
    Relate:TRANTYPE.Close
    Relate:TRDBATCH_ALIAS.Close
    Relate:TURNARND.Close
    Relate:UPDDATA.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
    Relate:VODAIMP.Close
    Relate:WARPARTS_ALIAS.Close
    Relate:WEBJOB.Close
    Relate:WPARTTMP.Close
    Relate:WPARTTMP_ALIAS.Close
    Relate:XREPACT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','do_not_delete')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

ClearPreJobs         PROCEDURE                        ! Declare Procedure
KillerList           QUEUE,PRE()
RecordNumber         LONG
                     END
Count                LONG
VPortalDays          LONG
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:PREJOB.Open
!kill off any overdue jobs - done once a day
    if getIni('VPORTAL','LAST',0,clip(path())&'\SB2KDEF.INI') < today() then
        
        !vodacom portal days
        VPortalDays   = GetIni('VPORTAL','DAYS','',CLIP(PATH())&'\SB2KDEF.INI')

        if VportalDays > 0 then

            PutIni('VPORTAL','LAST',today(),clip(path())&'\SB2KDEF.INI')

            !create the killer list
            Access:prejob.clearkey(PRE:KeyJobRef_Number)
            PRE:JobRef_number = 0
            set(PRE:KeyJobRef_Number,PRE:KeyJobRef_Number)
            Loop
                if access:prejob.next() then break.
                if PRE:JobRef_number <> 0 then break.

                if today() - PRE:Date_booked > VportalDays then
                    KillerList.RecordNumber = PRE:RefNumber
                    Add(KillerList)
                END !if overdue
            END
        
            !now kill them -  or rather mark then as expirified
            Loop Count = 1 to records(KillerList)

                get(Killerlist,count)

                Access:Prejob.clearkey(PRE:KeyRefNumber)
                PRE:RefNumber = Killerlist.RecordNumber
                If access:prejob.fetch(PRE:KeyRefNumber) = level:Benign
                    PRE:JobRef_number = -1
                    Access:Prejob.update()
                END !if fetch
            END !loop through killer list
        END !if number of "portal" days is more than zero
    END !if was done before today
   Relate:PREJOB.Close
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
