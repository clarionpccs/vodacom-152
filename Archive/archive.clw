   PROGRAM



   INCLUDE('ABERROR.INC'),ONCE
   INCLUDE('ABFILE.INC'),ONCE
   INCLUDE('ABFUZZY.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('EQUATES.CLW'),ONCE
   INCLUDE('ERRORS.CLW'),ONCE
   INCLUDE('KEYCODES.CLW'),ONCE
   INCLUDE('FE.INC'),ONCE

!* * * * Line Print Template Generated Code * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

OFSTRUCT    GROUP,TYPE                                                                  
cBytes       BYTE                                                                       ! Specifies the length, in bytes, of the structure
cFixedDisk   BYTE                                                                       ! Specifies whether the file is on a hard (fixed) disk. This member is nonzero if the file is on a hard disk
nErrCode     SIGNED                                                                     ! Specifies the MS-DOS error code if the OpenFile function failed
Reserved1    SIGNED                                                                     ! Reserved, Don't use
Reserved2    SIGNED                                                                     ! Reserved, Don't use
szPathName   BYTE,DIM(128)                                                              ! Specifies the path and filename of the file
            END

LF                  CSTRING(CHR(10))                                                    ! Line Feed
FF                  CSTRING(CHR(12))                                                    ! Form Feed
CR                  CSTRING(CHR(13))                                                    ! Carriage Return

lpszFilename        CSTRING(144)                                                        ! File Name
fnAttribute         SIGNED                                                              ! Attribute
hf                  SIGNED                                                              ! File Handle
hpvBuffer           STRING(500)                                                        ! String To Write
cbBuffer            SIGNED                                                              ! Characters to Write
BytesWritten        SIGNED                                                              ! Characters Written

Succeeded           EQUATE(0)                                                           ! Function Succeeded
OpenError           EQUATE(1)                                                           ! Can Not Open File or Device
WriteError          EQUATE(2)                                                           ! Can not Write to File or Device
CloseError          EQUATE(3)                                                           ! Can not Close File or Device
SeekError           EQUATE(4)                                                           ! Can not Seek File or Device

ATTR_Normal         EQUATE(0)                                                           ! File Attribute Normal
ATTR_ReadOnly       EQUATE(1)                                                           ! File Attribute Read Only
ATTR_Hidden         EQUATE(2)                                                           ! File Attribute Hidden
ATTR_System         EQUATE(3)                                                           ! File Attribute System

OF_READ             EQUATE(0000h)                                                       ! Opens the file for reading only
OF_WRITE            EQUATE(0001h)                                                       ! Opens the file for writing only
OF_READWRITE        EQUATE(0002h)                                                       ! Opens the file for reading and writing
OF_SHARE_COMPAT     EQUATE(0000h)                                                       ! Opens the file with compatibility mode, allowing any program on a given machine to open the file any number of times.
OF_SHARE_EXCLUSIVE  EQUATE(0010h)                                                       ! Opens the file with exclusive mode, denying other programs both read and write access to the file
OF_SHARE_DENY_WRITE EQUATE(0020h)                                                       ! Opens the file and denies other programs write access to the file
OF_SHARE_DENY_READ  EQUATE(0030h)                                                       ! Opens the file and denies other programs read access to the file
OF_SHARE_DENY_NONE  EQUATE(0040h)                                                       ! Opens the file without denying other programs read or write access to the file
OF_PARSE            EQUATE(0100h)                                                       ! Fills the OFSTRUCT structure but carries out no other action
OF_DELETE           EQUATE(0200h)                                                       ! Deletes the file
OF_VERIFY           EQUATE(0400h)                                                       ! Compares the time and date in the OF_STRUCT with the time and date of the specified file
OF_SEARCH           EQUATE(0400h)                                                       ! Windows searches in directories even when the file name includes a full path
OF_CANCEL           EQUATE(0800h)                                                       ! Adds a Cancel button to the OF_PROMPT dialog box. Pressing the Cancel button directs OpenFile to return a file-not-found error message
OF_CREATE           EQUATE(1000h)                                                       ! Creates a new file. If the file already exists, it is truncated to zero length
OF_PROMPT           EQUATE(2000h)                                                       ! Displays a dialog box if the requested file does not exist.
OF_EXIST            EQUATE(4000h)                                                       ! Opens the file, and then closes it. This value is used to test for file existence
OF_REOPEN           EQUATE(8000h)                                                       ! Opens the file using information in the reopen buffer

OF_STRUCT           LIKE(OFSTRUCT)                                                      ! Will Be loaded with File information. See OFSTRUCT above

!* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 


   MAP
     MODULE('Windows API')
SystemParametersInfo PROCEDURE (LONG uAction, LONG uParam, *? lpvParam, LONG fuWinIni),LONG,RAW,PROC,PASCAL,DLL(TRUE),NAME('SystemParametersInfoA')
     END
     MODULE('ARCHIBC.CLW')
DctInit     PROCEDURE
DctKill     PROCEDURE
     END
     MODULE('FileExplorerDLL.Lib')
fe_ClassVersion        PROCEDURE(byte Flag=0),string,name('fe_ClassVersion'),DLL(dll_mode)
feDispose              PROCEDURE(),name('feDispose'),DLL(dll_mode)
fe_OnlineStatus        PROCEDURE(byte pOption=0),byte,name('fe_OnlineStatus'),DLL(dll_mode)
fe_OnlineStatusConnectionName PROCEDURE(byte pOption=0),string,name('fe_OnlineStatusConnectionName'),DLL(dll_mode)
fe_StartIE             PROCEDURE(string pURL),name('fe_StartIE'),DLL(dll_mode)
fe_DebugString         PROCEDURE(string DbgMessage),name('fe_DebugString'),DLL(dll_mode)
fe_GetRegValue         PROCEDURE(string ParentFolder, string NameOfKey, string NameOfValue),string,name('fe_GetRegValue'),DLL(dll_mode)
fe_GetOSVersionInfo    PROCEDURE(),string,name('fe_GetOSVersionInfo'),DLL(dll_mode)
fe_GetIEVersionInfo    PROCEDURE(byte pTranslate=0),string,name('fe_GetIEVersionInfo'),DLL(dll_mode)
fe_ShellExecute        PROCEDURE(string pParm1),name('fe_ShellExecute'),DLL(dll_mode)
fe_SetFocus            PROCEDURE(long pHandle),name('fe_SetFocus'),DLL(dll_mode)
fe_MCIPlay             PROCEDURE(string pFileName),byte,proc,name('fe_MCIPlay'),DLL(dll_mode)
fe_MCIStop             PROCEDURE(<string pFileName>),byte,proc,name('fe_MCIStop'),DLL(dll_mode)
     END
!--- Application Global and Exported Procedure Definitions --------------------------------------------
     MODULE('ARCHI001.CLW')
Main                   PROCEDURE   !Version Number: 5005
     END
     MODULE('ARCHI003.CLW')
HelpBrowser            PROCEDURE(String)   !Doesn't need a SRN, as this *IS* the help screen
     END
     MODULE('ARCHI004.CLW')
Missive                FUNCTION(String,String,String,String),byte   !
     END
         Module('winapi')
           DBGGetExitCodeProcess(unsigned, ulong),signed,pascal,NAME('GetExitCodeProcess')
           DBGCreateProcess(ulong, ulong, ulong, ulong, signed, ulong, ulong, ulong, ulong, ulong ),signed,raw,pascal,name('CreateProcessA')
           DBGSleep(ulong),pascal,raw,name('Sleep'),NAME('Sleep')
         End
         MODULE('Windows API Call')
           BringWindowToTop(LONG),BYTE,RAW,PASCAL,NAME('BringWindowToTop'),PROC
         END
         Include('DDE.CLW')
     
     !* * * * Line Print Template Generated Code (Start) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
     
          LinePrint(STRING StringToPrint,<STRING DeviceName>,<BYTE CRLF>),BYTE,PROC          ! Declare LinePrint Function
          DeleteFile(STRING FileToDelete),BYTE,PROC                                          ! Declare DeleteFile Function
            MODULE('')                                                                       ! MODULE Start
             OpenFile(*CSTRING,*OFSTRUCT,SIGNED),SIGNED,PASCAL,RAW                           ! Open/Create/Delete File
             _llseek(SIGNED,LONG,SIGNED),LONG,PASCAL                                         ! Control File Pointer
             _lwrite(SIGNED,*STRING,UNSIGNED),UNSIGNED,PASCAL,RAW                           ! Write to File
             _lclose(SIGNED),SIGNED,PASCAL                                                   ! Close File
            END                                                                              ! Terminate Module
     
     !* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
     
     !--------------------------------------------------------------------------
     ! -----------Tintools-------------
     !--------------------------------------------------------------------------
      MODULE('archi_VW.CLW')    !REPORT VIEWER
         TINCALENDARSTYLE1(<LONG>),LONG
         TINCALENDARSTYLE2(<STRING>,<LONG>),LONG
         TINCALENDARSTYLE3(<STRING>,<LONG>),LONG
         TINCALENDARSTYLE4(<STRING>,<LONG>),LONG
         TINCALENDARmonyear(<LONG>),LONG
         TINCALENDARmonth(<LONG>),LONG
      END
     !--------------------------------------------------------------------------
     ! -----------Tintools-------------
     !--------------------------------------------------------------------------
   END

file:JOBSE2        STRING(255)
file:JOBS          STRING(255)
file:JOBSCONS      STRING(255)
file:JOBACCNO      STRING(255)
file:JOBOUTFL      STRING(255)
file:WEBJOB        STRING(255)
file:LOCATLOG      STRING(255)
file:JOBSENG       STRING(255)
file:AUDSTATS      STRING(255)
file:JOBSE         STRING(255)
file:JOBTHIRD      STRING(255)
file:CONTHIST      STRING(255)
file:WARPARTS      STRING(255)
file:AUDIT         STRING(255)
file:ESTPARTS      STRING(255)
file:JOBSTAGE      STRING(255)
file:JOBEXHIS      STRING(255)
file:JOBLOHIS      STRING(255)
file:PARTS         STRING(255)
file:JOBACC        STRING(255)
file:DEFAULTV      STRING(255)
file:JOBSOBF       STRING(255)
file:JOBSWARR      STRING(255)
file:JOBSTAMP      STRING(255)
file:SMSMAIL       STRING(255)
file:AUDIT2        STRING(255)
file:JOBSE3        STRING(255)
SilentRunning        BYTE(0)                         !Set true when application is running in silent mode

    Map
BHStripReplace           Procedure(String func:String,String func:Strip,String func:Replace),String
BHStripNonAlphaNum           Procedure(String func:String,String func:Replace),String
BHReturnDaysHoursMins       Procedure(Long func:TotalMins,*Long func:Days,*Long func:Hours,*Long func:Mins)
BHTimeDifference24Hr        Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime),Long
BHTimeDifference24Hr5Days   Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime),Long
BHTimeDifference24Hr5DaysMonday   Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime,String func:MondayStart),Long
BHGetFileFromPath           Procedure(String f:Path),String
BHGetPathFromFile           Procedure(String f:Path),String
BHGetFileNoExtension        Procedure(String fullPath),String
BHAddBackSlash              Procedure(STRING fullPath),STRING
BHRunDOS                    Procedure(String f:Command,<Byte f:Wait>,<Byte f:NoTimeOut>),Long
    End
ARCJOBS              FILE,DRIVER('Btrieve'),NAME(file:JOBS),PRE(ajob),BINDABLE,CREATE,THREAD
Ref_Number_Key           KEY(ajob:Ref_Number),NOCASE,PRIMARY
Model_Unit_Key           KEY(ajob:Model_Number,ajob:Unit_Type),DUP,NOCASE
EngCompKey               KEY(ajob:Engineer,ajob:Completed,ajob:Ref_Number),DUP,NOCASE
EngWorkKey               KEY(ajob:Engineer,ajob:Workshop,ajob:Ref_Number),DUP,NOCASE
Surname_Key              KEY(ajob:Surname),DUP,NOCASE
MobileNumberKey          KEY(ajob:Mobile_Number),DUP,NOCASE
ESN_Key                  KEY(ajob:ESN),DUP,NOCASE
MSN_Key                  KEY(ajob:MSN),DUP,NOCASE
AccountNumberKey         KEY(ajob:Account_Number,ajob:Ref_Number),DUP,NOCASE
AccOrdNoKey              KEY(ajob:Account_Number,ajob:Order_Number),DUP,NOCASE
Model_Number_Key         KEY(ajob:Model_Number),DUP,NOCASE
Engineer_Key             KEY(ajob:Engineer,ajob:Model_Number),DUP,NOCASE
Date_Booked_Key          KEY(ajob:date_booked),DUP,NOCASE
DateCompletedKey         KEY(ajob:Date_Completed),DUP,NOCASE
ModelCompKey             KEY(ajob:Model_Number,ajob:Date_Completed),DUP,NOCASE
By_Status                KEY(ajob:Current_Status,ajob:Ref_Number),DUP,NOCASE
StatusLocKey             KEY(ajob:Current_Status,ajob:Location,ajob:Ref_Number),DUP,NOCASE
Location_Key             KEY(ajob:Location,ajob:Ref_Number),DUP,NOCASE
Third_Party_Key          KEY(ajob:Third_Party_Site,ajob:Third_Party_Printed,ajob:Ref_Number),DUP,NOCASE
ThirdEsnKey              KEY(ajob:Third_Party_Site,ajob:Third_Party_Printed,ajob:ESN),DUP,NOCASE
ThirdMsnKey              KEY(ajob:Third_Party_Site,ajob:Third_Party_Printed,ajob:MSN),DUP,NOCASE
PriorityTypeKey          KEY(ajob:Job_Priority,ajob:Ref_Number),DUP,NOCASE
Unit_Type_Key            KEY(ajob:Unit_Type),DUP,NOCASE
EDI_Key                  KEY(ajob:Manufacturer,ajob:EDI,ajob:EDI_Batch_Number,ajob:Ref_Number),DUP,NOCASE
InvoiceNumberKey         KEY(ajob:Invoice_Number),DUP,NOCASE
WarInvoiceNoKey          KEY(ajob:Invoice_Number_Warranty),DUP,NOCASE
Batch_Number_Key         KEY(ajob:Batch_Number,ajob:Ref_Number),DUP,NOCASE
Batch_Status_Key         KEY(ajob:Batch_Number,ajob:Current_Status,ajob:Ref_Number),DUP,NOCASE
BatchModelNoKey          KEY(ajob:Batch_Number,ajob:Model_Number,ajob:Ref_Number),DUP,NOCASE
BatchInvoicedKey         KEY(ajob:Batch_Number,ajob:Invoice_Number,ajob:Ref_Number),DUP,NOCASE
BatchCompKey             KEY(ajob:Batch_Number,ajob:Completed,ajob:Ref_Number),DUP,NOCASE
ChaInvoiceKey            KEY(ajob:Chargeable_Job,ajob:Account_Number,ajob:Invoice_Number,ajob:Ref_Number),DUP,NOCASE
InvoiceExceptKey         KEY(ajob:Invoice_Exception,ajob:Ref_Number),DUP,NOCASE
ConsignmentNoKey         KEY(ajob:Consignment_Number),DUP,NOCASE
InConsignKey             KEY(ajob:Incoming_Consignment_Number),DUP,NOCASE
ReadyToDespKey           KEY(ajob:Despatched,ajob:Ref_Number),DUP,NOCASE
ReadyToTradeKey          KEY(ajob:Despatched,ajob:Account_Number,ajob:Ref_Number),DUP,NOCASE
ReadyToCouKey            KEY(ajob:Despatched,ajob:Current_Courier,ajob:Ref_Number),DUP,NOCASE
ReadyToAllKey            KEY(ajob:Despatched,ajob:Account_Number,ajob:Current_Courier,ajob:Ref_Number),DUP,NOCASE
DespJobNumberKey         KEY(ajob:Despatch_Number,ajob:Ref_Number),DUP,NOCASE
DateDespatchKey          KEY(ajob:Courier,ajob:Date_Despatched,ajob:Ref_Number),DUP,NOCASE
DateDespLoaKey           KEY(ajob:Loan_Courier,ajob:Loan_Despatched,ajob:Ref_Number),DUP,NOCASE
DateDespExcKey           KEY(ajob:Exchange_Courier,ajob:Exchange_Despatched,ajob:Ref_Number),DUP,NOCASE
ChaRepTypeKey            KEY(ajob:Repair_Type),DUP,NOCASE
WarRepTypeKey            KEY(ajob:Repair_Type_Warranty),DUP,NOCASE
ChaTypeKey               KEY(ajob:Charge_Type),DUP,NOCASE
WarChaTypeKey            KEY(ajob:Warranty_Charge_Type),DUP,NOCASE
Bouncer_Key              KEY(ajob:Bouncer,ajob:Ref_Number),DUP,NOCASE
EngDateCompKey           KEY(ajob:Engineer,ajob:Date_Completed),DUP,NOCASE
ExcStatusKey             KEY(ajob:Exchange_Status,ajob:Ref_Number),DUP,NOCASE
LoanStatusKey            KEY(ajob:Loan_Status,ajob:Ref_Number),DUP,NOCASE
ExchangeLocKey           KEY(ajob:Exchange_Status,ajob:Location,ajob:Ref_Number),DUP,NOCASE
LoanLocKey               KEY(ajob:Loan_Status,ajob:Location,ajob:Ref_Number),DUP,NOCASE
BatchJobKey              KEY(ajob:InvoiceAccount,ajob:InvoiceBatch,ajob:Ref_Number),DUP,NOCASE
BatchStatusKey           KEY(ajob:InvoiceAccount,ajob:InvoiceBatch,ajob:InvoiceStatus,ajob:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Batch_Number                LONG
Internal_Status             STRING(10)
Auto_Search                 STRING(30)
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Cancelled                   STRING(3)
Bouncer                     STRING(8)
Bouncer_Type                STRING(3)
Web_Type                    STRING(3)
Warranty_Job                STRING(3)
Chargeable_Job              STRING(3)
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(20)
MSN                         STRING(20)
ProductCode                 STRING(30)
Unit_Type                   STRING(30)
Colour                      STRING(30)
Location_Type               STRING(10)
Phone_Lock                  STRING(30)
Workshop                    STRING(3)
Location                    STRING(30)
Authority_Number            STRING(30)
Insurance_Reference_Number  STRING(30)
DOP                         DATE
Insurance                   STRING(3)
Insurance_Type              STRING(30)
Transit_Type                STRING(30)
Physical_Damage             STRING(3)
Intermittent_Fault          STRING(3)
Loan_Status                 STRING(30)
Exchange_Status             STRING(30)
Job_Priority                STRING(30)
Charge_Type                 STRING(30)
Warranty_Charge_Type        STRING(30)
Current_Status              STRING(30)
Account_Number              STRING(15)
Trade_Account_Name          STRING(30)
Department_Name             STRING(30)
Order_Number                STRING(30)
POP                         STRING(3)
In_Repair                   STRING(3)
Date_In_Repair              DATE
Time_In_Repair              TIME
On_Test                     STRING(3)
Date_On_Test                DATE
Time_On_Test                TIME
Estimate_Ready              STRING(3)
QA_Passed                   STRING(3)
Date_QA_Passed              DATE
Time_QA_Passed              TIME
QA_Rejected                 STRING(3)
Date_QA_Rejected            DATE
Time_QA_Rejected            TIME
QA_Second_Passed            STRING(3)
Date_QA_Second_Passed       DATE
Time_QA_Second_Passed       TIME
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Mobile_Number               STRING(15)
Postcode_Collection         STRING(10)
Company_Name_Collection     STRING(30)
Address_Line1_Collection    STRING(30)
Address_Line2_Collection    STRING(30)
Address_Line3_Collection    STRING(30)
Telephone_Collection        STRING(15)
Postcode_Delivery           STRING(10)
Address_Line1_Delivery      STRING(30)
Company_Name_Delivery       STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Date_Completed              DATE
Time_Completed              TIME
Completed                   STRING(3)
Paid                        STRING(3)
Paid_Warranty               STRING(3)
Date_Paid                   DATE
Repair_Type                 STRING(30)
Repair_Type_Warranty        STRING(30)
Engineer                    STRING(3)
Ignore_Chargeable_Charges   STRING(3)
Ignore_Warranty_Charges     STRING(3)
Ignore_Estimate_Charges     STRING(3)
Courier_Cost                REAL
Advance_Payment             REAL
Labour_Cost                 REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier_Cost_Estimate       REAL
Labour_Cost_Estimate        REAL
Parts_Cost_Estimate         REAL
Sub_Total_Estimate          REAL
Courier_Cost_Warranty       REAL
Labour_Cost_Warranty        REAL
Parts_Cost_Warranty         REAL
Sub_Total_Warranty          REAL
Loan_Issued_Date            DATE
Loan_Unit_Number            LONG
Loan_accessory              STRING(3)
Loan_User                   STRING(3)
Loan_Courier                STRING(30)
Loan_Consignment_Number     STRING(30)
Loan_Despatched             DATE
Loan_Despatched_User        STRING(3)
Loan_Despatch_Number        LONG
LoaService                  STRING(1)
Exchange_Unit_Number        LONG
Exchange_Authorised         STRING(3)
Loan_Authorised             STRING(3)
Exchange_Accessory          STRING(3)
Exchange_Issued_Date        DATE
Exchange_User               STRING(3)
Exchange_Courier            STRING(30)
Exchange_Consignment_Number STRING(30)
Exchange_Despatched         DATE
Exchange_Despatched_User    STRING(3)
Exchange_Despatch_Number    LONG
ExcService                  STRING(1)
Date_Despatched             DATE
Despatch_Number             LONG
Despatch_User               STRING(3)
Courier                     STRING(30)
Consignment_Number          STRING(30)
Incoming_Courier            STRING(30)
Incoming_Consignment_Number STRING(30)
Incoming_Date               DATE
Despatched                  STRING(3)
Despatch_Type               STRING(3)
Current_Courier             STRING(30)
JobService                  STRING(1)
Last_Repair_Days            REAL
Third_Party_Site            STRING(30)
Estimate                    STRING(3)
Estimate_If_Over            REAL
Estimate_Accepted           STRING(3)
Estimate_Rejected           STRING(3)
Third_Party_Printed         STRING(3)
ThirdPartyDateDesp          DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(255)
Fault_Code11                STRING(255)
Fault_Code12                STRING(255)
PreviousStatus              STRING(30)
StatusUser                  STRING(3)
Status_End_Date             DATE
Status_End_Time             TIME
Turnaround_End_Date         DATE
Turnaround_End_Time         TIME
Turnaround_Time             STRING(30)
Special_Instructions        STRING(30)
InvoiceAccount              STRING(15)
InvoiceStatus               STRING(3)
InvoiceBatch                LONG
InvoiceQuery                STRING(255)
EDI                         STRING(3)
EDI_Batch_Number            REAL
Invoice_Exception           STRING(3)
Invoice_Failure_Reason      STRING(80)
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Date_Warranty       DATE
Invoice_Courier_Cost        REAL
Invoice_Labour_Cost         REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
Invoice_Number_Warranty     LONG
WInvoice_Courier_Cost       REAL
WInvoice_Labour_Cost        REAL
WInvoice_Parts_Cost         REAL
WInvoice_Sub_Total          REAL
                         END
                     END                       

JOBSE3               FILE,DRIVER('Btrieve'),OEM,NAME('JOBSE3.DAT'),PRE(jobe3),CREATE,BINDABLE,THREAD
KeyRecordNumber          KEY(jobe3:RecordNumber),NOCASE,PRIMARY
KeyRefNumber             KEY(jobe3:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
LoyaltyStatus               STRING(250)
Exchange_IntLocation        STRING(30)
SpecificNeeds               BYTE
SN_Software_Installed       BYTE
FaultCode21                 STRING(30)
FaultCode22                 STRING(30)
FaultCode23                 STRING(30)
FaultCode24                 STRING(30)
FaultCode25                 STRING(30)
FaultCode26                 STRING(30)
FaultCode27                 STRING(30)
FaultCode28                 STRING(30)
FaultCode29                 STRING(30)
FaultCode30                 STRING(30)
                         END
                     END                       

ARCJOBSTAMP          FILE,DRIVER('Btrieve'),OEM,NAME(file:JOBSTAMP),PRE(ajos),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ajos:RecordNumber),NOCASE,PRIMARY
JOBSRefNumberKey         KEY(ajos:JOBSRefNumber),DUP,NOCASE
DateTimeKey              KEY(ajos:DateStamp,ajos:TimeStamp),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
JOBSRefNumber               LONG
DateStamp                   DATE
TimeStamp                   TIME
                         END
                     END                       

JOBSTAMP             FILE,DRIVER('Btrieve'),OEM,NAME('JOBSTAMP.DAT'),PRE(jos),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jos:RecordNumber),NOCASE,PRIMARY
JOBSRefNumberKey         KEY(jos:JOBSRefNumber),DUP,NOCASE
DateTimeKey              KEY(jos:DateStamp,jos:TimeStamp),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
JOBSRefNumber               LONG
DateStamp                   DATE
TimeStamp                   TIME
                         END
                     END                       

ARCDEFAULTV          FILE,DRIVER('Btrieve'),OEM,NAME(file:DEFAULTV),PRE(adefv),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(adefv:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
VersionNumber               STRING(30)
NagDate                     DATE
ReUpdate                    BYTE
                         END
                     END                       

ARCJOBSWARR          FILE,DRIVER('Btrieve'),OEM,NAME(file:JOBSWARR),PRE(ajow),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ajow:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(ajow:RefNumber),DUP,NOCASE
StatusManFirstKey        KEY(ajow:Status,ajow:Manufacturer,ajow:FirstSecondYear,ajow:RefNumber),DUP,NOCASE
StatusManKey             KEY(ajow:Status,ajow:Manufacturer,ajow:RefNumber),DUP,NOCASE
ClaimStatusManKey        KEY(ajow:Status,ajow:Manufacturer,ajow:ClaimSubmitted,ajow:RefNumber),DUP,NOCASE
ClaimStatusManFirstKey   KEY(ajow:Status,ajow:Manufacturer,ajow:FirstSecondYear,ajow:ClaimSubmitted,ajow:RefNumber),DUP,NOCASE
RRCStatusKey             KEY(ajow:BranchID,ajow:RepairedAt,ajow:RRCStatus,ajow:RefNumber),DUP,NOCASE
RRCStatusManKey          KEY(ajow:BranchID,ajow:RepairedAt,ajow:RRCStatus,ajow:Manufacturer,ajow:RefNumber),DUP,NOCASE
RRCReconciledManKey      KEY(ajow:BranchID,ajow:RepairedAt,ajow:RRCStatus,ajow:Manufacturer,ajow:RRCDateReconciled,ajow:RefNumber),DUP,NOCASE
RRCReconciledKey         KEY(ajow:BranchID,ajow:RepairedAt,ajow:RRCStatus,ajow:RRCDateReconciled,ajow:RefNumber),DUP,NOCASE
RepairedRRCStatusKey     KEY(ajow:RepairedAt,ajow:RRCStatus,ajow:RefNumber),DUP,NOCASE
RepairedRRCStatusManKey  KEY(ajow:RepairedAt,ajow:RRCStatus,ajow:Manufacturer,ajow:RefNumber),DUP,NOCASE
RepairedRRCReconciledKey KEY(ajow:RepairedAt,ajow:RRCStatus,ajow:DateReconciled,ajow:RefNumber),DUP,NOCASE
RepairedRRCReconManKey   KEY(ajow:RepairedAt,ajow:RRCStatus,ajow:Manufacturer,ajow:RRCDateReconciled,ajow:RefNumber),DUP,NOCASE
SubmittedRepairedBranchKey KEY(ajow:BranchID,ajow:RepairedAt,ajow:ClaimSubmitted,ajow:RefNumber),DUP,NOCASE
SubmittedBranchKey       KEY(ajow:BranchID,ajow:ClaimSubmitted,ajow:RefNumber),DUP,NOCASE
ClaimSubmittedKey        KEY(ajow:ClaimSubmitted,ajow:RefNumber),DUP,NOCASE
RepairedRRCAccManKey     KEY(ajow:BranchID,ajow:RepairedAt,ajow:RRCStatus,ajow:Manufacturer,ajow:DateAccepted,ajow:RefNumber),DUP,NOCASE
AcceptedBranchKey        KEY(ajow:BranchID,ajow:DateAccepted,ajow:RefNumber),DUP,NOCASE
DateAcceptedKey          KEY(ajow:DateAccepted,ajow:RefNumber),DUP,NOCASE
RejectedBranchKey        KEY(ajow:BranchID,ajow:DateRejected,ajow:RefNumber),DUP,NOCASE
RejectedKey              KEY(ajow:DateRejected,ajow:RefNumber),DUP,NOCASE
FinalRejectionBranchKey  KEY(ajow:BranchID,ajow:DateFinalRejection,ajow:RefNumber),DUP,NOCASE
FinalRejectionKey        KEY(ajow:DateFinalRejection,ajow:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
BranchID                    STRING(2)
RepairedAt                  STRING(3)
Manufacturer                STRING(30)
FirstSecondYear             BYTE
Status                      STRING(3)
Submitted                   LONG
FromApproved                BYTE
ClaimSubmitted              DATE
DateAccepted                DATE
DateReconciled              DATE
RRCDateReconciled           DATE
RRCStatus                   STRING(3)
DateRejected                DATE
DateFinalRejection          DATE
Orig_Sub_Date               DATE
                         END
                     END                       

JOBSWARR             FILE,DRIVER('Btrieve'),OEM,NAME('JOBSWARR.DAT'),PRE(jow),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jow:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jow:RefNumber),DUP,NOCASE
StatusManFirstKey        KEY(jow:Status,jow:Manufacturer,jow:FirstSecondYear,jow:RefNumber),DUP,NOCASE
StatusManKey             KEY(jow:Status,jow:Manufacturer,jow:RefNumber),DUP,NOCASE
ClaimStatusManKey        KEY(jow:Status,jow:Manufacturer,jow:ClaimSubmitted,jow:RefNumber),DUP,NOCASE
ClaimStatusManFirstKey   KEY(jow:Status,jow:Manufacturer,jow:FirstSecondYear,jow:ClaimSubmitted,jow:RefNumber),DUP,NOCASE
RRCStatusKey             KEY(jow:BranchID,jow:RepairedAt,jow:RRCStatus,jow:RefNumber),DUP,NOCASE
RRCStatusManKey          KEY(jow:BranchID,jow:RepairedAt,jow:RRCStatus,jow:Manufacturer,jow:RefNumber),DUP,NOCASE
RRCReconciledManKey      KEY(jow:BranchID,jow:RepairedAt,jow:RRCStatus,jow:Manufacturer,jow:RRCDateReconciled,jow:RefNumber),DUP,NOCASE
RRCReconciledKey         KEY(jow:BranchID,jow:RepairedAt,jow:RRCStatus,jow:RRCDateReconciled,jow:RefNumber),DUP,NOCASE
RepairedRRCStatusKey     KEY(jow:RepairedAt,jow:RRCStatus,jow:RefNumber),DUP,NOCASE
RepairedRRCStatusManKey  KEY(jow:RepairedAt,jow:RRCStatus,jow:Manufacturer,jow:RefNumber),DUP,NOCASE
RepairedRRCReconciledKey KEY(jow:RepairedAt,jow:RRCStatus,jow:DateReconciled,jow:RefNumber),DUP,NOCASE
RepairedRRCReconManKey   KEY(jow:RepairedAt,jow:RRCStatus,jow:Manufacturer,jow:RRCDateReconciled,jow:RefNumber),DUP,NOCASE
SubmittedRepairedBranchKey KEY(jow:BranchID,jow:RepairedAt,jow:ClaimSubmitted,jow:RefNumber),DUP,NOCASE
SubmittedBranchKey       KEY(jow:BranchID,jow:ClaimSubmitted,jow:RefNumber),DUP,NOCASE
ClaimSubmittedKey        KEY(jow:ClaimSubmitted,jow:RefNumber),DUP,NOCASE
RepairedRRCAccManKey     KEY(jow:BranchID,jow:RepairedAt,jow:RRCStatus,jow:Manufacturer,jow:DateAccepted,jow:RefNumber),DUP,NOCASE
AcceptedBranchKey        KEY(jow:BranchID,jow:DateAccepted,jow:RefNumber),DUP,NOCASE
DateAcceptedKey          KEY(jow:DateAccepted,jow:RefNumber),DUP,NOCASE
RejectedBranchKey        KEY(jow:BranchID,jow:DateRejected,jow:RefNumber),DUP,NOCASE
RejectedKey              KEY(jow:DateRejected,jow:RefNumber),DUP,NOCASE
FinalRejectionBranchKey  KEY(jow:BranchID,jow:DateFinalRejection,jow:RefNumber),DUP,NOCASE
FinalRejectionKey        KEY(jow:DateFinalRejection,jow:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
BranchID                    STRING(2)
RepairedAt                  STRING(3)
Manufacturer                STRING(30)
FirstSecondYear             BYTE
Status                      STRING(3)
Submitted                   LONG
FromApproved                BYTE
ClaimSubmitted              DATE
DateAccepted                DATE
DateReconciled              DATE
RRCDateReconciled           DATE
RRCStatus                   STRING(3)
DateRejected                DATE
DateFinalRejection          DATE
Orig_Sub_Date               DATE
                         END
                     END                       

DEFAULTV             FILE,DRIVER('Btrieve'),OEM,NAME('DEFAULTV.DAT'),PRE(defv),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(defv:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
VersionNumber               STRING(30)
NagDate                     DATE
ReUpdate                    BYTE
                         END
                     END                       

ARCJOBSE             FILE,DRIVER('Btrieve'),OEM,NAME(file:JOBSE),PRE(ajobe),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ajobe:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(ajobe:RefNumber),DUP,NOCASE
WarrStatusDateKey        KEY(ajobe:WarrantyStatusDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
HubRepair                   BYTE
Network                     STRING(30)
POPConfirmed                BYTE
HubRepairDate               DATE
HubRepairTime               TIME
ClaimValue                  REAL
HandlingFee                 REAL
ExchangeRate                REAL
InvoiceClaimValue           REAL
InvoiceHandlingFee          REAL
InvoiceExchangeRate         REAL
BoxESN                      STRING(20)
ValidPOP                    STRING(3)
ReturnDate                  DATE
TalkTime                    REAL
OriginalPackaging           BYTE
OriginalBattery             BYTE
OriginalCharger             BYTE
OriginalAntenna             BYTE
OriginalManuals             BYTE
PhysicalDamage              BYTE
OriginalDealer              CSTRING(255)
BranchOfReturn              STRING(30)
COverwriteRepairType        BYTE
WOverwriteRepairType        BYTE
LabourAdjustment            REAL
PartsAdjustment             REAL
SubTotalAdjustment          REAL
IgnoreClaimCosts            BYTE
RRCCLabourCost              REAL
RRCCPartsCost               REAL
RRCCPartsSale               REAL
RRCCSubTotal                REAL
InvRRCCLabourCost           REAL
InvRRCCPartsCost            REAL
InvRRCCPartsSale            REAL
InvRRCCSubTotal             REAL
RRCWLabourCost              REAL
RRCWPartsCost               REAL
RRCWPartsSale               REAL
RRCWSubTotal                REAL
InvRRCWLabourCost           REAL
InvRRCWPartsCost            REAL
InvRRCWPartsSale            REAL
InvRRCWSubTotal             REAL
ARC3rdPartyCost             REAL
InvARC3rdPartCost           REAL
WebJob                      BYTE
RRCELabourCost              REAL
RRCEPartsCost               REAL
RRCESubTotal                REAL
IgnoreRRCChaCosts           REAL
IgnoreRRCWarCosts           REAL
IgnoreRRCEstCosts           REAL
OBFvalidated                BYTE
OBFvalidateDate             DATE
OBFvalidateTime             TIME
DespatchType                STRING(3)
Despatched                  STRING(3)
WarrantyClaimStatus         STRING(30)
WarrantyStatusDate          DATE
InSecurityPackNo            STRING(30)
JobSecurityPackNo           STRING(30)
ExcSecurityPackNo           STRING(30)
LoaSecurityPackNo           STRING(30)
ExceedWarrantyRepairLimit   BYTE
BouncerClaim                BYTE
Sub_Sub_Account             STRING(15)
ConfirmClaimAdjustment      BYTE
ARC3rdPartyVAT              REAL
ARC3rdPartyInvoiceNumber    STRING(30)
ExchangeAdjustment          REAL
ExchangedATRRC              BYTE
EndUserTelNo                STRING(15)
ClaimColour                 BYTE
ARC3rdPartyMarkup           REAL
Ignore3rdPartyCosts         BYTE
POPType                     STRING(30)
OBFProcessed                BYTE
LoanReplacementValue        REAL
PendingClaimColour          BYTE
AccessoryNotes              STRING(255)
ClaimPartsCost              REAL
InvClaimPartsCost           REAL
Booking48HourOption         BYTE
Engineer48HourOption        BYTE
ExcReplcamentCharge         BYTE
SecondExchangeNumber        LONG
SecondExchangeStatus        STRING(30)
VatNumber                   STRING(30)
ExchangeProductCode         STRING(30)
SecondExcProdCode           STRING(30)
ARC3rdPartyInvoiceDate      DATE
ARC3rdPartyWaybillNo        STRING(30)
ARC3rdPartyRepairType       STRING(30)
ARC3rdPartyRejectedReason   STRING(255)
ARC3rdPartyRejectedAmount   REAL
VSACustomer                 BYTE
HandsetReplacmentValue      REAL
SecondHandsetRepValue       REAL
t                           STRING(20)
                         END
                     END                       

ARCAUDSTATS          FILE,DRIVER('Btrieve'),OEM,NAME(file:AUDSTATS),PRE(aaus),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(aaus:RecordNumber),NOCASE,PRIMARY
DateChangedKey           KEY(aaus:RefNumber,aaus:Type,aaus:DateChanged),DUP,NOCASE
NewStatusKey             KEY(aaus:RefNumber,aaus:Type,aaus:NewStatus),DUP,NOCASE
StatusTimeKey            KEY(aaus:RefNumber,aaus:Type,aaus:DateChanged,aaus:TimeChanged),DUP,NOCASE
StatusDateKey            KEY(aaus:RefNumber,aaus:DateChanged,aaus:TimeChanged),DUP,NOCASE
StatusDateRecordKey      KEY(aaus:RefNumber,aaus:DateChanged,aaus:TimeChanged,aaus:RecordNumber),DUP,NOCASE
StatusTypeRecordKey      KEY(aaus:RefNumber,aaus:Type,aaus:DateChanged,aaus:TimeChanged,aaus:RecordNumber),DUP,NOCASE
RefRecordNumberKey       KEY(aaus:RefNumber,aaus:RecordNumber),DUP,NOCASE
RefDateRecordKey         KEY(aaus:RefNumber,aaus:DateChanged,aaus:RecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
Type                        STRING(3)
DateChanged                 DATE
TimeChanged                 TIME
OldStatus                   STRING(30)
NewStatus                   STRING(30)
UserCode                    STRING(3)
                         END
                     END                       

ARCJOBSTAGE          FILE,DRIVER('Btrieve'),NAME(file:JOBSTAGE),PRE(ajst),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(ajst:Ref_Number,-ajst:Date),DUP,NOCASE
Job_Stage_Key            KEY(ajst:Ref_Number,ajst:Job_Stage),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
Job_Stage                   STRING(30)
Date                        DATE
Time                        TIME
User                        STRING(3)
                         END
                     END                       

SMSMAIL              FILE,DRIVER('Btrieve'),OEM,NAME('SMSMAIL.DAT'),PRE(sms),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(sms:RecordNumber),NOCASE,PRIMARY
SMSSentKey               KEY(sms:SendToSMS,sms:SMSSent),DUP,NOCASE
EmailSentKey             KEY(sms:SendToEmail,sms:EmailSent),DUP,NOCASE
SMSSBUpdateKey           KEY(sms:SMSSent,sms:SBUpdated),DUP,NOCASE
EmailSBUpdateKey         KEY(sms:EmailSent,sms:SBUpdated),DUP,NOCASE
DateTimeInsertedKey      KEY(sms:RefNumber,sms:DateInserted,sms:TimeInserted),DUP,NOCASE
KeyMSISDN_DateDec        KEY(sms:MSISDN,-sms:DateSMSSent),DUP,NOCASE
KeyRefDateTimeDec        KEY(sms:RefNumber,-sms:DateInserted,-sms:TimeInserted),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
MSISDN                      STRING(12)
MSG                         STRING(255)
EmailAddress                STRING(255)
SendToSMS                   STRING(1)
SendToEmail                 STRING(1)
SMSSent                     STRING(1)
DateInserted                DATE
TimeInserted                TIME
EmailSent                   STRING(1)
DateSMSSent                 DATE
TimeSMSSent                 TIME
DateEmailSent               DATE
TimeEmailSent               TIME
PathToEstimate              STRING(255)
SBUpdated                   BYTE
SMSType                     STRING(1)
EmailSubject                STRING(255)
EmailAddress2               STRING(255)
EmailAddress3               STRING(255)
                         END
                     END                       

JOBSTAGE             FILE,DRIVER('Btrieve'),NAME('JOBSTAGE.DAT'),PRE(jst),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(jst:Ref_Number,-jst:Date),DUP,NOCASE
Job_Stage_Key            KEY(jst:Ref_Number,jst:Job_Stage),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
Job_Stage                   STRING(30)
Date                        DATE
Time                        TIME
User                        STRING(3)
                         END
                     END                       

ARCESTPARTS          FILE,DRIVER('Btrieve'),NAME(file:ESTPARTS),PRE(aepr),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(aepr:Ref_Number,aepr:Part_Number),DUP,NOCASE
record_number_key        KEY(aepr:Record_Number),NOCASE,PRIMARY
Part_Ref_Number2_Key     KEY(aepr:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(aepr:Date_Ordered),DUP,NOCASE
Part_Ref_Number_Key      KEY(aepr:Ref_Number,aepr:Part_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(aepr:Ref_Number,aepr:Order_Number),DUP,NOCASE
Supplier_Key             KEY(aepr:Supplier),DUP,NOCASE
Order_Part_Key           KEY(aepr:Ref_Number,aepr:Order_Part_Number),DUP,NOCASE
PartAllocatedKey         KEY(aepr:PartAllocated,aepr:Part_Number),DUP,NOCASE
StatusKey                KEY(aepr:Status,aepr:Part_Number),DUP,NOCASE
AllocatedStatusKey       KEY(aepr:PartAllocated,aepr:Status,aepr:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Record_Number               LONG
Adjustment                  STRING(3)
Part_Ref_Number             LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Quantity                    LONG
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          LONG
Order_Number                LONG
Date_Received               DATE
Order_Part_Number           LONG
Status_Date                 DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
Fault_Codes_Checked         STRING(3)
InvoicePart                 LONG
Credit                      STRING(3)
Requested                   BYTE
UsedOnRepair                BYTE
PartAllocated               BYTE
Status                      STRING(3)
AveragePurchaseCost         REAL
InWarrantyMarkup            LONG
OutWarrantyMarkup           LONG
RRCPurchaseCost             REAL
RRCSaleCost                 REAL
RRCInWarrantyMarkup         LONG
RRCOutWarrantyMarkup        LONG
RRCAveragePurchaseCost      REAL
                         END
                     END                       

ESTPARTS             FILE,DRIVER('Btrieve'),NAME('ESTPARTS.DAT'),PRE(epr),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(epr:Ref_Number,epr:Part_Number),DUP,NOCASE
record_number_key        KEY(epr:Record_Number),NOCASE,PRIMARY
Part_Ref_Number2_Key     KEY(epr:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(epr:Date_Ordered),DUP,NOCASE
Part_Ref_Number_Key      KEY(epr:Ref_Number,epr:Part_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(epr:Ref_Number,epr:Order_Number),DUP,NOCASE
Supplier_Key             KEY(epr:Supplier),DUP,NOCASE
Order_Part_Key           KEY(epr:Ref_Number,epr:Order_Part_Number),DUP,NOCASE
PartAllocatedKey         KEY(epr:PartAllocated,epr:Part_Number),DUP,NOCASE
StatusKey                KEY(epr:Status,epr:Part_Number),DUP,NOCASE
AllocatedStatusKey       KEY(epr:PartAllocated,epr:Status,epr:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Record_Number               LONG
Adjustment                  STRING(3)
Part_Ref_Number             LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Quantity                    LONG
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          LONG
Order_Number                LONG
Date_Received               DATE
Order_Part_Number           LONG
Status_Date                 DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
Fault_Codes_Checked         STRING(3)
InvoicePart                 LONG
Credit                      STRING(3)
Requested                   BYTE
UsedOnRepair                BYTE
PartAllocated               BYTE
Status                      STRING(3)
AveragePurchaseCost         REAL
InWarrantyMarkup            LONG
OutWarrantyMarkup           LONG
RRCPurchaseCost             REAL
RRCSaleCost                 REAL
RRCInWarrantyMarkup         LONG
RRCOutWarrantyMarkup        LONG
RRCAveragePurchaseCost      REAL
                         END
                     END                       

ARCJOBSE3            FILE,DRIVER('Btrieve'),OEM,NAME(file:JOBSE3),PRE(ajobe3),CREATE,BINDABLE,THREAD
KeyRecordNumber          KEY(ajobe3:RecordNumber),NOCASE,PRIMARY
KeyRefNumber             KEY(ajobe3:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
LoyaltyStatus               STRING(250)
Exchange_IntLocation        STRING(30)
SpecificNeeds               BYTE
SN_Software_Installed       BYTE
FaultCode21                 STRING(30)
FaultCode22                 STRING(30)
FaultCode23                 STRING(30)
FaultCode24                 STRING(30)
FaultCode25                 STRING(30)
FaultCode26                 STRING(30)
FaultCode27                 STRING(30)
FaultCode28                 STRING(30)
FaultCode29                 STRING(30)
FaultCode30                 STRING(30)
                         END
                     END                       

ARCAUDIT             FILE,DRIVER('Btrieve'),NAME(file:AUDIT),PRE(aaud),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(aaud:Ref_Number,-aaud:Date,-aaud:Time,aaud:Action),DUP,NOCASE
Action_Key               KEY(aaud:Ref_Number,aaud:Action,-aaud:Date),DUP,NOCASE
User_Key                 KEY(aaud:Ref_Number,aaud:User,-aaud:Date),DUP,NOCASE
Record_Number_Key        KEY(aaud:record_number),NOCASE,PRIMARY
ActionOnlyKey            KEY(aaud:Action,aaud:Date),DUP,NOCASE
TypeRefKey               KEY(aaud:Ref_Number,aaud:Type,-aaud:Date,-aaud:Time,aaud:Action),DUP,NOCASE
TypeActionKey            KEY(aaud:Ref_Number,aaud:Type,aaud:Action,-aaud:Date),DUP,NOCASE
TypeUserKey              KEY(aaud:Ref_Number,aaud:Type,aaud:User,-aaud:Date),DUP,NOCASE
DateActionJobKey         KEY(aaud:Date,aaud:Action,aaud:Ref_Number),DUP,NOCASE
DateJobKey               KEY(aaud:Date,aaud:Ref_Number),DUP,NOCASE
DateTypeJobKey           KEY(aaud:Date,aaud:Type,aaud:Ref_Number),DUP,NOCASE
DateTypeActionKey        KEY(aaud:Date,aaud:Type,aaud:Action,aaud:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
record_number               REAL
Ref_Number                  REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Action                      STRING(80)
Type                        STRING(3)
                         END
                     END                       

AUDIT                FILE,DRIVER('Btrieve'),NAME('AUDIT.DAT'),PRE(aud),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(aud:Ref_Number,-aud:Date,-aud:Time,aud:Action),DUP,NOCASE
Action_Key               KEY(aud:Ref_Number,aud:Action,-aud:Date),DUP,NOCASE
User_Key                 KEY(aud:Ref_Number,aud:User,-aud:Date),DUP,NOCASE
Record_Number_Key        KEY(aud:record_number),NOCASE,PRIMARY
ActionOnlyKey            KEY(aud:Action,aud:Date),DUP,NOCASE
TypeRefKey               KEY(aud:Ref_Number,aud:Type,-aud:Date,-aud:Time,aud:Action),DUP,NOCASE
TypeActionKey            KEY(aud:Ref_Number,aud:Type,aud:Action,-aud:Date),DUP,NOCASE
TypeUserKey              KEY(aud:Ref_Number,aud:Type,aud:User,-aud:Date),DUP,NOCASE
DateActionJobKey         KEY(aud:Date,aud:Action,aud:Ref_Number),DUP,NOCASE
DateJobKey               KEY(aud:Date,aud:Ref_Number),DUP,NOCASE
DateTypeJobKey           KEY(aud:Date,aud:Type,aud:Ref_Number),DUP,NOCASE
DateTypeActionKey        KEY(aud:Date,aud:Type,aud:Action,aud:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
record_number               REAL
Ref_Number                  REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Action                      STRING(80)
Type                        STRING(3)
                         END
                     END                       

ARCCONTHIST          FILE,DRIVER('Btrieve'),NAME(file:CONTHIST),PRE(acht),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(acht:Ref_Number,-acht:Date,-acht:Time,acht:Action),DUP,NOCASE
Action_Key               KEY(acht:Ref_Number,acht:Action,-acht:Date),DUP,NOCASE
User_Key                 KEY(acht:Ref_Number,acht:User,-acht:Date),DUP,NOCASE
Record_Number_Key        KEY(acht:Record_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  LONG
Date                        DATE
Time                        TIME
User                        STRING(3)
Action                      STRING(80)
Notes                       STRING(2000)
SystemHistory               BYTE
SN_StickyNote               STRING(1)
SN_Completed                STRING(1)
SN_EngAlloc                 STRING(1)
SN_EngUpdate                STRING(1)
SN_CustService              STRING(1)
SN_WaybillConf              STRING(1)
SN_Despatch                 STRING(1)
                         END
                     END                       

CONTHIST             FILE,DRIVER('Btrieve'),NAME('CONTHIST.DAT'),PRE(cht),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(cht:Ref_Number,-cht:Date,-cht:Time,cht:Action),DUP,NOCASE
Action_Key               KEY(cht:Ref_Number,cht:Action,-cht:Date),DUP,NOCASE
User_Key                 KEY(cht:Ref_Number,cht:User,-cht:Date),DUP,NOCASE
Record_Number_Key        KEY(cht:Record_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  LONG
Date                        DATE
Time                        TIME
User                        STRING(3)
Action                      STRING(80)
Notes                       STRING(2000)
SystemHistory               BYTE
SN_StickyNote               STRING(1)
SN_Completed                STRING(1)
SN_EngAlloc                 STRING(1)
SN_EngUpdate                STRING(1)
SN_CustService              STRING(1)
SN_WaybillConf              STRING(1)
SN_Despatch                 STRING(1)
                         END
                     END                       

ARCJOBTHIRD          FILE,DRIVER('Btrieve'),OEM,NAME(file:JOBTHIRD),PRE(ajot),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ajot:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(ajot:RefNumber),DUP,NOCASE
OutIMEIKey               KEY(ajot:OutIMEI),DUP,NOCASE
InIMEIKEy                KEY(ajot:InIMEI),DUP,NOCASE
OutDateKey               KEY(ajot:RefNumber,ajot:DateOut,ajot:RecordNumber),DUP,NOCASE
ThirdPartyKey            KEY(ajot:ThirdPartyNumber),DUP,NOCASE
OriginalIMEIKey          KEY(ajot:OriginalIMEI),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
OriginalIMEI                STRING(30)
OutIMEI                     STRING(30)
InIMEI                      STRING(30)
DateOut                     DATE
DateDespatched              DATE
DateIn                      DATE
ThirdPartyNumber            LONG
OriginalMSN                 STRING(30)
OutMSN                      STRING(30)
InMSN                       STRING(30)
                         END
                     END                       

JOBTHIRD             FILE,DRIVER('Btrieve'),OEM,NAME('JOBTHIRD.DAT'),PRE(jot),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jot:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jot:RefNumber),DUP,NOCASE
OutIMEIKey               KEY(jot:OutIMEI),DUP,NOCASE
InIMEIKEy                KEY(jot:InIMEI),DUP,NOCASE
OutDateKey               KEY(jot:RefNumber,jot:DateOut,jot:RecordNumber),DUP,NOCASE
ThirdPartyKey            KEY(jot:ThirdPartyNumber),DUP,NOCASE
OriginalIMEIKey          KEY(jot:OriginalIMEI),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
OriginalIMEI                STRING(30)
OutIMEI                     STRING(30)
InIMEI                      STRING(30)
DateOut                     DATE
DateDespatched              DATE
DateIn                      DATE
ThirdPartyNumber            LONG
OriginalMSN                 STRING(30)
OutMSN                      STRING(30)
InMSN                       STRING(30)
                         END
                     END                       

ARCJOBSOBF           FILE,DRIVER('Btrieve'),OEM,NAME(file:JOBSOBF),PRE(ajof),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ajof:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(ajof:RefNumber),DUP,NOCASE
StatusRefNumberKey       KEY(ajof:Status,ajof:RefNumber),DUP,NOCASE
StatusIMEINumberKey      KEY(ajof:Status,ajof:IMEINumber),DUP,NOCASE
HeadAccountCompletedKey  KEY(ajof:HeadAccountNumber,ajof:DateCompleted,ajof:RefNumber),DUP,NOCASE
HeadAccountProcessedKey  KEY(ajof:HeadAccountNumber,ajof:DateProcessed,ajof:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
IMEINumber                  STRING(30)
Status                      BYTE
Replacement                 BYTE
StoreReferenceNumber        STRING(30)
RNumber                     STRING(30)
RejectionReason             STRING(255)
ReplacementIMEI             STRING(30)
LAccountNumber              STRING(30)
UserCode                    STRING(3)
HeadAccountNumber           STRING(30)
DateCompleted               DATE
TimeCompleted               TIME
DateProcessed               DATE
TimeProcessed               TIME
                         END
                     END                       

AUDSTATS             FILE,DRIVER('Btrieve'),OEM,NAME('AUDSTATS.DAT'),PRE(aus),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(aus:RecordNumber),NOCASE,PRIMARY
DateChangedKey           KEY(aus:RefNumber,aus:Type,aus:DateChanged),DUP,NOCASE
NewStatusKey             KEY(aus:RefNumber,aus:Type,aus:NewStatus),DUP,NOCASE
StatusTimeKey            KEY(aus:RefNumber,aus:Type,aus:DateChanged,aus:TimeChanged),DUP,NOCASE
StatusDateKey            KEY(aus:RefNumber,aus:DateChanged,aus:TimeChanged),DUP,NOCASE
StatusDateRecordKey      KEY(aus:RefNumber,aus:DateChanged,aus:TimeChanged,aus:RecordNumber),DUP,NOCASE
StatusTypeRecordKey      KEY(aus:RefNumber,aus:Type,aus:DateChanged,aus:TimeChanged,aus:RecordNumber),DUP,NOCASE
RefRecordNumberKey       KEY(aus:RefNumber,aus:RecordNumber),DUP,NOCASE
RefDateRecordKey         KEY(aus:RefNumber,aus:DateChanged,aus:RecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
Type                        STRING(3)
DateChanged                 DATE
TimeChanged                 TIME
OldStatus                   STRING(30)
NewStatus                   STRING(30)
UserCode                    STRING(3)
                         END
                     END                       

ARCJOBSENG           FILE,DRIVER('Btrieve'),OEM,NAME(file:JOBSENG),PRE(ajoe),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ajoe:RecordNumber),NOCASE,PRIMARY
UserCodeKey              KEY(ajoe:JobNumber,ajoe:UserCode,ajoe:DateAllocated),DUP,NOCASE
UserCodeOnlyKey          KEY(ajoe:UserCode,ajoe:DateAllocated),DUP,NOCASE
AllocatedKey             KEY(ajoe:JobNumber,ajoe:AllocatedBy,ajoe:DateAllocated),DUP,NOCASE
JobNumberKey             KEY(ajoe:JobNumber,-ajoe:DateAllocated,-ajoe:TimeAllocated),DUP,NOCASE
UserCodeStatusKey        KEY(ajoe:UserCode,ajoe:StatusDate,ajoe:Status),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
JobNumber                   LONG
UserCode                    STRING(3)
DateAllocated               DATE
TimeAllocated               STRING(20)
AllocatedBy                 STRING(3)
EngSkillLevel               LONG
JobSkillLevel               STRING(30)
Status                      STRING(30)
StatusDate                  DATE
StatusTime                  TIME
                         END
                     END                       

JOBSENG              FILE,DRIVER('Btrieve'),OEM,NAME('JOBSENG.DAT'),PRE(joe),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(joe:RecordNumber),NOCASE,PRIMARY
UserCodeKey              KEY(joe:JobNumber,joe:UserCode,joe:DateAllocated),DUP,NOCASE
UserCodeOnlyKey          KEY(joe:UserCode,joe:DateAllocated),DUP,NOCASE
AllocatedKey             KEY(joe:JobNumber,joe:AllocatedBy,joe:DateAllocated),DUP,NOCASE
JobNumberKey             KEY(joe:JobNumber,-joe:DateAllocated,-joe:TimeAllocated),DUP,NOCASE
UserCodeStatusKey        KEY(joe:UserCode,joe:StatusDate,joe:Status),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
JobNumber                   LONG
UserCode                    STRING(3)
DateAllocated               DATE
TimeAllocated               STRING(20)
AllocatedBy                 STRING(3)
EngSkillLevel               LONG
JobSkillLevel               STRING(30)
Status                      STRING(30)
StatusDate                  DATE
StatusTime                  TIME
                         END
                     END                       

JOBEXHIS             FILE,DRIVER('Btrieve'),NAME('JOBEXHIS.DAT'),PRE(jxh),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(jxh:Ref_Number,jxh:Date,jxh:Time),DUP,NOCASE
record_number_key        KEY(jxh:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Loan_Unit_Number            REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Status                      STRING(60)
                         END
                     END                       

ARCLOCATLOG          FILE,DRIVER('Btrieve'),OEM,NAME(file:LOCATLOG),PRE(alot),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(alot:RecordNumber),NOCASE,PRIMARY
DateKey                  KEY(alot:RefNumber,alot:TheDate),DUP,NOCASE
NewLocationKey           KEY(alot:RefNumber,alot:NewLocation),DUP,NOCASE
DateNewLocationKey       KEY(alot:NewLocation,alot:TheDate,alot:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
TheDate                     DATE
TheTime                     TIME
UserCode                    STRING(30)
PreviousLocation            STRING(30)
NewLocation                 STRING(30)
Exchange                    STRING(1)
                         END
                     END                       

ARCAUDIT2            FILE,DRIVER('Btrieve'),NAME(file:AUDIT2),PRE(aaud2),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(aaud2:RecordNumber),NOCASE,PRIMARY
AUDRecordNumberKey       KEY(aaud2:AUDRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
AUDRecordNumber             LONG
Notes                       STRING(255)
                         END
                     END                       

AUDIT2               FILE,DRIVER('Btrieve'),NAME('AUDIT2.DAT'),PRE(aud2),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(aud2:RecordNumber),NOCASE,PRIMARY
AUDRecordNumberKey       KEY(aud2:AUDRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
AUDRecordNumber             LONG
Notes                       STRING(255)
                         END
                     END                       

LOCATLOG             FILE,DRIVER('Btrieve'),OEM,NAME('LOCATLOG.DAT'),PRE(lot),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(lot:RecordNumber),NOCASE,PRIMARY
DateKey                  KEY(lot:RefNumber,lot:TheDate),DUP,NOCASE
NewLocationKey           KEY(lot:RefNumber,lot:NewLocation),DUP,NOCASE
DateNewLocationKey       KEY(lot:NewLocation,lot:TheDate,lot:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
TheDate                     DATE
TheTime                     TIME
UserCode                    STRING(30)
PreviousLocation            STRING(30)
NewLocation                 STRING(30)
Exchange                    STRING(1)
                         END
                     END                       

JOBSOBF              FILE,DRIVER('Btrieve'),OEM,NAME('JOBSOBF.DAT'),PRE(jof),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jof:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jof:RefNumber),DUP,NOCASE
StatusRefNumberKey       KEY(jof:Status,jof:RefNumber),DUP,NOCASE
StatusIMEINumberKey      KEY(jof:Status,jof:IMEINumber),DUP,NOCASE
HeadAccountCompletedKey  KEY(jof:HeadAccountNumber,jof:DateCompleted,jof:RefNumber),DUP,NOCASE
HeadAccountProcessedKey  KEY(jof:HeadAccountNumber,jof:DateProcessed,jof:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
IMEINumber                  STRING(30)
Status                      BYTE
Replacement                 BYTE
StoreReferenceNumber        STRING(30)
RNumber                     STRING(30)
RejectionReason             STRING(255)
ReplacementIMEI             STRING(30)
LAccountNumber              STRING(30)
UserCode                    STRING(3)
HeadAccountNumber           STRING(30)
DateCompleted               DATE
TimeCompleted               TIME
DateProcessed               DATE
TimeProcessed               TIME
                         END
                     END                       

ARCJOBLOHIS          FILE,DRIVER('Btrieve'),NAME(file:JOBLOHIS),PRE(ajlh),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(ajlh:Ref_Number,ajlh:Date,ajlh:Time),DUP,NOCASE
record_number_key        KEY(ajlh:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Loan_Unit_Number            REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Status                      STRING(60)
                         END
                     END                       

JOBLOHIS             FILE,DRIVER('Btrieve'),NAME('JOBLOHIS.DAT'),PRE(jlh),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(jlh:Ref_Number,jlh:Date,jlh:Time),DUP,NOCASE
record_number_key        KEY(jlh:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Loan_Unit_Number            REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Status                      STRING(60)
                         END
                     END                       

ARCJOBACC            FILE,DRIVER('Btrieve'),NAME(file:JOBACC),PRE(ajac),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(ajac:Ref_Number,ajac:Accessory),NOCASE,PRIMARY
DamagedKey               KEY(ajac:Ref_Number,ajac:Damaged,ajac:Accessory),DUP,NOCASE
DamagedPirateKey         KEY(ajac:Ref_Number,ajac:Damaged,ajac:Pirate,ajac:Accessory),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
Accessory                   STRING(30)
Damaged                     BYTE
Pirate                      BYTE
Attached                    BYTE
                         END
                     END                       

ARCJOBEXHIS          FILE,DRIVER('Btrieve'),NAME(file:JOBEXHIS),PRE(ajxh),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(ajxh:Ref_Number,ajxh:Date,ajxh:Time),DUP,NOCASE
record_number_key        KEY(ajxh:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Loan_Unit_Number            REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Status                      STRING(60)
                         END
                     END                       

ARCJOBOUTFL          FILE,DRIVER('Btrieve'),OEM,NAME(file:JOBOUTFL),PRE(ajoo),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ajoo:RecordNumber),NOCASE,PRIMARY
JobNumberKey             KEY(ajoo:JobNumber,ajoo:FaultCode),DUP,NOCASE
LevelKey                 KEY(ajoo:JobNumber,ajoo:Level),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
JobNumber                   LONG
FaultCode                   STRING(30)
Description                 STRING(255)
Level                       LONG
                         END
                     END                       

ARCWEBJOB            FILE,DRIVER('Btrieve'),OEM,NAME(file:WEBJOB),PRE(awob),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(awob:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(awob:RefNumber),DUP,NOCASE
HeadJobNumberKey         KEY(awob:HeadAccountNumber,awob:JobNumber),DUP,NOCASE
HeadOrderNumberKey       KEY(awob:HeadAccountNumber,awob:OrderNumber,awob:RefNumber),DUP,NOCASE
HeadMobileNumberKey      KEY(awob:HeadAccountNumber,awob:MobileNumber,awob:RefNumber),DUP,NOCASE
HeadModelNumberKey       KEY(awob:HeadAccountNumber,awob:ModelNumber,awob:RefNumber),DUP,NOCASE
HeadIMEINumberKey        KEY(awob:HeadAccountNumber,awob:IMEINumber,awob:RefNumber),DUP,NOCASE
HeadMSNKey               KEY(awob:HeadAccountNumber,awob:MSN),DUP,NOCASE
HeadEDIKey               KEY(awob:HeadAccountNumber,awob:EDI,awob:Manufacturer,awob:JobNumber),DUP,NOCASE
ValidationIMEIKey        KEY(awob:HeadAccountNumber,awob:Validation,awob:IMEINumber),DUP,NOCASE
HeadSubKey               KEY(awob:HeadAccountNumber,awob:SubAcountNumber,awob:RefNumber),DUP,NOCASE
HeadRefNumberKey         KEY(awob:HeadAccountNumber,awob:RefNumber),DUP,NOCASE
OracleNumberKey          KEY(awob:OracleExportNumber),DUP,NOCASE
HeadCurrentStatusKey     KEY(awob:HeadAccountNumber,awob:Current_Status,awob:RefNumber),DUP,NOCASE
HeadExchangeStatus       KEY(awob:HeadAccountNumber,awob:Exchange_Status,awob:RefNumber),DUP,NOCASE
HeadLoanStatusKey        KEY(awob:HeadAccountNumber,awob:Loan_Status,awob:RefNumber),DUP,NOCASE
ExcWayBillNoKey          KEY(awob:ExcWayBillNumber),DUP,NOCASE
LoaWayBillNoKey          KEY(awob:LoaWayBillNumber),DUP,NOCASE
JobWayBillNoKey          KEY(awob:JobWayBillNumber),DUP,NOCASE
RRCWInvoiceNumberKey     KEY(awob:RRCWInvoiceNumber,awob:RefNumber),DUP,NOCASE
DateBookedKey            KEY(awob:HeadAccountNumber,awob:DateBooked),DUP,NOCASE
DateCompletedKey         KEY(awob:HeadAccountNumber,awob:DateCompleted),DUP,NOCASE
CompletedKey             KEY(awob:HeadAccountNumber,awob:Completed),DUP,NOCASE
DespatchKey              KEY(awob:HeadAccountNumber,awob:ReadyToDespatch,awob:RefNumber),DUP,NOCASE
DespatchSubKey           KEY(awob:HeadAccountNumber,awob:ReadyToDespatch,awob:SubAcountNumber,awob:RefNumber),DUP,NOCASE
DespatchCourierKey       KEY(awob:HeadAccountNumber,awob:ReadyToDespatch,awob:DespatchCourier,awob:RefNumber),DUP,NOCASE
DespatchSubCourierKey    KEY(awob:HeadAccountNumber,awob:ReadyToDespatch,awob:SubAcountNumber,awob:DespatchCourier,awob:RefNumber),DUP,NOCASE
EDIKey                   KEY(awob:HeadAccountNumber,awob:EDI,awob:Manufacturer,awob:RefNumber),DUP,NOCASE
EDIRefNumberKey          KEY(awob:HeadAccountNumber,awob:EDI,awob:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
JobNumber                   LONG
RefNumber                   LONG
HeadAccountNumber           STRING(30)
SubAcountNumber             STRING(30)
OrderNumber                 STRING(30)
IMEINumber                  STRING(30)
MSN                         STRING(30)
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
MobileNumber                STRING(30)
EDI                         STRING(3)
Validation                  STRING(3)
OracleExportNumber          LONG
Current_Status              STRING(30)
Exchange_Status             STRING(30)
Loan_Status                 STRING(30)
Current_Status_Date         DATE
Exchange_Status_Date        DATE
Loan_Status_Date            DATE
ExcWayBillNumber            LONG
LoaWayBillNumber            LONG
JobWayBillNumber            LONG
DateJobDespatched           DATE
RRCEngineer                 STRING(3)
ReconciledMarker            BYTE
RRCWInvoiceNumber           LONG
DateBooked                  DATE
TimeBooked                  TIME
DateCompleted               DATE
TimeCompleted               TIME
Completed                   STRING(3)
ReadyToDespatch             BYTE
DespatchCourier             STRING(30)
FaultCode13                 STRING(30)
FaultCode14                 STRING(30)
FaultCode15                 STRING(30)
FaultCode16                 STRING(30)
FaultCode17                 STRING(30)
FaultCode18                 STRING(30)
FaultCode19                 STRING(30)
FaultCode20                 STRING(30)
                         END
                     END                       

WEBJOB               FILE,DRIVER('Btrieve'),OEM,NAME('WEBJOB.DAT'),PRE(wob),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(wob:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(wob:RefNumber),DUP,NOCASE
HeadJobNumberKey         KEY(wob:HeadAccountNumber,wob:JobNumber),DUP,NOCASE
HeadOrderNumberKey       KEY(wob:HeadAccountNumber,wob:OrderNumber,wob:RefNumber),DUP,NOCASE
HeadMobileNumberKey      KEY(wob:HeadAccountNumber,wob:MobileNumber,wob:RefNumber),DUP,NOCASE
HeadModelNumberKey       KEY(wob:HeadAccountNumber,wob:ModelNumber,wob:RefNumber),DUP,NOCASE
HeadIMEINumberKey        KEY(wob:HeadAccountNumber,wob:IMEINumber,wob:RefNumber),DUP,NOCASE
HeadMSNKey               KEY(wob:HeadAccountNumber,wob:MSN),DUP,NOCASE
HeadEDIKey               KEY(wob:HeadAccountNumber,wob:EDI,wob:Manufacturer,wob:JobNumber),DUP,NOCASE
ValidationIMEIKey        KEY(wob:HeadAccountNumber,wob:Validation,wob:IMEINumber),DUP,NOCASE
HeadSubKey               KEY(wob:HeadAccountNumber,wob:SubAcountNumber,wob:RefNumber),DUP,NOCASE
HeadRefNumberKey         KEY(wob:HeadAccountNumber,wob:RefNumber),DUP,NOCASE
OracleNumberKey          KEY(wob:OracleExportNumber),DUP,NOCASE
HeadCurrentStatusKey     KEY(wob:HeadAccountNumber,wob:Current_Status,wob:RefNumber),DUP,NOCASE
HeadExchangeStatus       KEY(wob:HeadAccountNumber,wob:Exchange_Status,wob:RefNumber),DUP,NOCASE
HeadLoanStatusKey        KEY(wob:HeadAccountNumber,wob:Loan_Status,wob:RefNumber),DUP,NOCASE
ExcWayBillNoKey          KEY(wob:ExcWayBillNumber),DUP,NOCASE
LoaWayBillNoKey          KEY(wob:LoaWayBillNumber),DUP,NOCASE
JobWayBillNoKey          KEY(wob:JobWayBillNumber),DUP,NOCASE
RRCWInvoiceNumberKey     KEY(wob:RRCWInvoiceNumber,wob:RefNumber),DUP,NOCASE
DateBookedKey            KEY(wob:HeadAccountNumber,wob:DateBooked),DUP,NOCASE
DateCompletedKey         KEY(wob:HeadAccountNumber,wob:DateCompleted),DUP,NOCASE
CompletedKey             KEY(wob:HeadAccountNumber,wob:Completed),DUP,NOCASE
DespatchKey              KEY(wob:HeadAccountNumber,wob:ReadyToDespatch,wob:RefNumber),DUP,NOCASE
DespatchSubKey           KEY(wob:HeadAccountNumber,wob:ReadyToDespatch,wob:SubAcountNumber,wob:RefNumber),DUP,NOCASE
DespatchCourierKey       KEY(wob:HeadAccountNumber,wob:ReadyToDespatch,wob:DespatchCourier,wob:RefNumber),DUP,NOCASE
DespatchSubCourierKey    KEY(wob:HeadAccountNumber,wob:ReadyToDespatch,wob:SubAcountNumber,wob:DespatchCourier,wob:RefNumber),DUP,NOCASE
EDIKey                   KEY(wob:HeadAccountNumber,wob:EDI,wob:Manufacturer,wob:RefNumber),DUP,NOCASE
EDIRefNumberKey          KEY(wob:HeadAccountNumber,wob:EDI,wob:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
JobNumber                   LONG
RefNumber                   LONG
HeadAccountNumber           STRING(30)
SubAcountNumber             STRING(30)
OrderNumber                 STRING(30)
IMEINumber                  STRING(30)
MSN                         STRING(30)
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
MobileNumber                STRING(30)
EDI                         STRING(3)
Validation                  STRING(3)
OracleExportNumber          LONG
Current_Status              STRING(30)
Exchange_Status             STRING(30)
Loan_Status                 STRING(30)
Current_Status_Date         DATE
Exchange_Status_Date        DATE
Loan_Status_Date            DATE
ExcWayBillNumber            LONG
LoaWayBillNumber            LONG
JobWayBillNumber            LONG
DateJobDespatched           DATE
RRCEngineer                 STRING(3)
ReconciledMarker            BYTE
RRCWInvoiceNumber           LONG
DateBooked                  DATE
TimeBooked                  TIME
DateCompleted               DATE
TimeCompleted               TIME
Completed                   STRING(3)
ReadyToDespatch             BYTE
DespatchCourier             STRING(30)
FaultCode13                 STRING(30)
FaultCode14                 STRING(30)
FaultCode15                 STRING(30)
FaultCode16                 STRING(30)
FaultCode17                 STRING(30)
FaultCode18                 STRING(30)
FaultCode19                 STRING(30)
FaultCode20                 STRING(30)
                         END
                     END                       

JOBOUTFL             FILE,DRIVER('Btrieve'),OEM,NAME('JOBOUTFL.DAT'),PRE(joo),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(joo:RecordNumber),NOCASE,PRIMARY
JobNumberKey             KEY(joo:JobNumber,joo:FaultCode),DUP,NOCASE
LevelKey                 KEY(joo:JobNumber,joo:Level),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
JobNumber                   LONG
FaultCode                   STRING(30)
Description                 STRING(255)
Level                       LONG
                         END
                     END                       

ARCJOBACCNO          FILE,DRIVER('Btrieve'),OEM,NAME(file:JOBACCNO),PRE(ajoa),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ajoa:RecordNumber),NOCASE,PRIMARY
AccessoryNumberKey       KEY(ajoa:RefNumber,ajoa:AccessoryNumber),DUP,NOCASE
AccessoryNoOnlyKey       KEY(ajoa:AccessoryNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
AccessoryNumber             STRING(30)
                         END
                     END                       

JOBACCNO             FILE,DRIVER('Btrieve'),OEM,NAME('JOBACCNO.DAT'),PRE(joa),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(joa:RecordNumber),NOCASE,PRIMARY
AccessoryNumberKey       KEY(joa:RefNumber,joa:AccessoryNumber),DUP,NOCASE
AccessoryNoOnlyKey       KEY(joa:AccessoryNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
AccessoryNumber             STRING(30)
                         END
                     END                       

ARCJOBSCONS          FILE,DRIVER('Btrieve'),OEM,NAME(file:JOBSCONS),PRE(ajoc),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ajoc:RecordNumber),NOCASE,PRIMARY
DateKey                  KEY(ajoc:RefNumber,ajoc:TheDate,ajoc:TheTime),DUP,NOCASE
ConsignmentNumberKey     KEY(ajoc:ConsignmentNumber,ajoc:RefNumber),DUP,NOCASE
DespatchFromDateKey      KEY(ajoc:DespatchFrom,ajoc:TheDate,ajoc:RefNumber),DUP,NOCASE
DateOnlyKey              KEY(ajoc:TheDate,ajoc:RefNumber),DUP,NOCASE
CourierKey               KEY(ajoc:Courier,ajoc:RefNumber),DUP,NOCASE
DespatchFromCourierKey   KEY(ajoc:DespatchFrom,ajoc:Courier,ajoc:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
TheDate                     DATE
TheTime                     TIME
UserCode                    STRING(3)
DespatchFrom                STRING(30)
DespatchTo                  STRING(30)
Courier                     STRING(30)
ConsignmentNumber           STRING(30)
DespatchType                STRING(3)
                         END
                     END                       

ARCSMSMAIL           FILE,DRIVER('Btrieve'),OEM,NAME(file:SMSMAIL),PRE(arcsms),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(arcsms:RecordNumber),NOCASE,PRIMARY
SMSSentKey               KEY(arcsms:SendToSMS,arcsms:SMSSent),DUP,NOCASE
EmailSentKey             KEY(arcsms:SendToEmail,arcsms:EmailSent),DUP,NOCASE
SMSSBUpdateKey           KEY(arcsms:SMSSent,arcsms:SBUpdated),DUP,NOCASE
EmailSBUpdateKey         KEY(arcsms:EmailSent,arcsms:SBUpdated),DUP,NOCASE
DateTimeInsertedKey      KEY(arcsms:RefNumber,arcsms:DateInserted,arcsms:TimeInserted),DUP,NOCASE
KeyMSISDN_DateDec        KEY(arcsms:MSISDN,-arcsms:DateSMSSent),DUP,NOCASE
KeyRefDateTimeDec        KEY(arcsms:RefNumber,-arcsms:DateInserted,-arcsms:TimeInserted),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
MSISDN                      STRING(12)
MSG                         STRING(255)
EmailAddress                STRING(255)
SendToSMS                   STRING(1)
SendToEmail                 STRING(1)
SMSSent                     STRING(1)
DateInserted                DATE
TimeInserted                TIME
EmailSent                   STRING(1)
DateSMSSent                 DATE
TimeSMSSent                 TIME
DateEmailSent               DATE
TimeEmailSent               TIME
PathToEstimate              STRING(255)
SBUpdated                   BYTE
SMSType                     STRING(1)
EmailSubject                STRING(255)
EmailAddress2               STRING(255)
EmailAddress3               STRING(255)
                         END
                     END                       

ARCWARPARTS          FILE,DRIVER('Btrieve'),NAME(file:WARPARTS),PRE(awpr),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(awpr:Ref_Number,awpr:Part_Number),DUP,NOCASE
RecordNumberKey          KEY(awpr:Record_Number),NOCASE,PRIMARY
PartRefNoKey             KEY(awpr:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(awpr:Date_Ordered),DUP,NOCASE
RefPartRefNoKey          KEY(awpr:Ref_Number,awpr:Part_Ref_Number),DUP,NOCASE
PendingRefNoKey          KEY(awpr:Ref_Number,awpr:Pending_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(awpr:Ref_Number,awpr:Order_Number),DUP,NOCASE
Supplier_Key             KEY(awpr:Supplier),DUP,NOCASE
Order_Part_Key           KEY(awpr:Ref_Number,awpr:Order_Part_Number),DUP,NOCASE
SuppDateOrdKey           KEY(awpr:Supplier,awpr:Date_Ordered),DUP,NOCASE
SuppDateRecKey           KEY(awpr:Supplier,awpr:Date_Received),DUP,NOCASE
RequestedKey             KEY(awpr:Requested,awpr:Part_Number),DUP,NOCASE
WebOrderKey              KEY(awpr:WebOrder,awpr:Part_Number),DUP,NOCASE
PartAllocatedKey         KEY(awpr:PartAllocated,awpr:Part_Number),DUP,NOCASE
StatusKey                KEY(awpr:Status,awpr:Part_Number),DUP,NOCASE
AllocatedStatusKey       KEY(awpr:PartAllocated,awpr:Status,awpr:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Record_Number               LONG
Adjustment                  STRING(3)
Part_Ref_Number             LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          LONG
Order_Number                LONG
Order_Part_Number           REAL
Date_Received               DATE
Status_Date                 DATE
Main_Part                   STRING(3)
Fault_Codes_Checked         STRING(3)
InvoicePart                 LONG
Credit                      STRING(3)
Requested                   BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
WebOrder                    BYTE
PartAllocated               BYTE
Status                      STRING(3)
CostAdjustment              REAL
AveragePurchaseCost         REAL
InWarrantyMarkup            LONG
OutWarrantyMarkup           LONG
RRCPurchaseCost             REAL
RRCSaleCost                 REAL
RRCInWarrantyMarkup         LONG
RRCOutWarrantyMarkup        LONG
RRCAveragePurchaseCost      REAL
ExchangeUnit                BYTE
SecondExchangeUnit          BYTE
Correction                  BYTE
                         END
                     END                       

WARPARTS             FILE,DRIVER('Btrieve'),NAME('WARPARTS.DAT'),PRE(wpr),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(wpr:Ref_Number,wpr:Part_Number),DUP,NOCASE
RecordNumberKey          KEY(wpr:Record_Number),NOCASE,PRIMARY
PartRefNoKey             KEY(wpr:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(wpr:Date_Ordered),DUP,NOCASE
RefPartRefNoKey          KEY(wpr:Ref_Number,wpr:Part_Ref_Number),DUP,NOCASE
PendingRefNoKey          KEY(wpr:Ref_Number,wpr:Pending_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(wpr:Ref_Number,wpr:Order_Number),DUP,NOCASE
Supplier_Key             KEY(wpr:Supplier),DUP,NOCASE
Order_Part_Key           KEY(wpr:Ref_Number,wpr:Order_Part_Number),DUP,NOCASE
SuppDateOrdKey           KEY(wpr:Supplier,wpr:Date_Ordered),DUP,NOCASE
SuppDateRecKey           KEY(wpr:Supplier,wpr:Date_Received),DUP,NOCASE
RequestedKey             KEY(wpr:Requested,wpr:Part_Number),DUP,NOCASE
WebOrderKey              KEY(wpr:WebOrder,wpr:Part_Number),DUP,NOCASE
PartAllocatedKey         KEY(wpr:PartAllocated,wpr:Part_Number),DUP,NOCASE
StatusKey                KEY(wpr:Status,wpr:Part_Number),DUP,NOCASE
AllocatedStatusKey       KEY(wpr:PartAllocated,wpr:Status,wpr:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Record_Number               LONG
Adjustment                  STRING(3)
Part_Ref_Number             LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          LONG
Order_Number                LONG
Order_Part_Number           REAL
Date_Received               DATE
Status_Date                 DATE
Main_Part                   STRING(3)
Fault_Codes_Checked         STRING(3)
InvoicePart                 LONG
Credit                      STRING(3)
Requested                   BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
WebOrder                    BYTE
PartAllocated               BYTE
Status                      STRING(3)
CostAdjustment              REAL
AveragePurchaseCost         REAL
InWarrantyMarkup            LONG
OutWarrantyMarkup           LONG
RRCPurchaseCost             REAL
RRCSaleCost                 REAL
RRCInWarrantyMarkup         LONG
RRCOutWarrantyMarkup        LONG
RRCAveragePurchaseCost      REAL
ExchangeUnit                BYTE
SecondExchangeUnit          BYTE
Correction                  BYTE
                         END
                     END                       

JOBACC               FILE,DRIVER('Btrieve'),NAME('JOBACC.DAT'),PRE(jac),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(jac:Ref_Number,jac:Accessory),NOCASE,PRIMARY
DamagedKey               KEY(jac:Ref_Number,jac:Damaged,jac:Accessory),DUP,NOCASE
DamagedPirateKey         KEY(jac:Ref_Number,jac:Damaged,jac:Pirate,jac:Accessory),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
Accessory                   STRING(30)
Damaged                     BYTE
Pirate                      BYTE
Attached                    BYTE
                         END
                     END                       

JOBSCONS             FILE,DRIVER('Btrieve'),OEM,NAME('JOBSCONS.DAT'),PRE(joc),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(joc:RecordNumber),NOCASE,PRIMARY
DateKey                  KEY(joc:RefNumber,joc:TheDate,joc:TheTime),DUP,NOCASE
ConsignmentNumberKey     KEY(joc:ConsignmentNumber,joc:RefNumber),DUP,NOCASE
DespatchFromDateKey      KEY(joc:DespatchFrom,joc:TheDate,joc:RefNumber),DUP,NOCASE
DateOnlyKey              KEY(joc:TheDate,joc:RefNumber),DUP,NOCASE
CourierKey               KEY(joc:Courier,joc:RefNumber),DUP,NOCASE
DespatchFromCourierKey   KEY(joc:DespatchFrom,joc:Courier,joc:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
TheDate                     DATE
TheTime                     TIME
UserCode                    STRING(3)
DespatchFrom                STRING(30)
DespatchTo                  STRING(30)
Courier                     STRING(30)
ConsignmentNumber           STRING(30)
DespatchType                STRING(3)
                         END
                     END                       

JOBS                 FILE,DRIVER('Btrieve'),NAME('JOBS.DAT'),PRE(job),BINDABLE,CREATE,THREAD
Ref_Number_Key           KEY(job:Ref_Number),NOCASE,PRIMARY
Model_Unit_Key           KEY(job:Model_Number,job:Unit_Type),DUP,NOCASE
EngCompKey               KEY(job:Engineer,job:Completed,job:Ref_Number),DUP,NOCASE
EngWorkKey               KEY(job:Engineer,job:Workshop,job:Ref_Number),DUP,NOCASE
Surname_Key              KEY(job:Surname),DUP,NOCASE
MobileNumberKey          KEY(job:Mobile_Number),DUP,NOCASE
ESN_Key                  KEY(job:ESN),DUP,NOCASE
MSN_Key                  KEY(job:MSN),DUP,NOCASE
AccountNumberKey         KEY(job:Account_Number,job:Ref_Number),DUP,NOCASE
AccOrdNoKey              KEY(job:Account_Number,job:Order_Number),DUP,NOCASE
Model_Number_Key         KEY(job:Model_Number),DUP,NOCASE
Engineer_Key             KEY(job:Engineer,job:Model_Number),DUP,NOCASE
Date_Booked_Key          KEY(job:date_booked),DUP,NOCASE
DateCompletedKey         KEY(job:Date_Completed),DUP,NOCASE
ModelCompKey             KEY(job:Model_Number,job:Date_Completed),DUP,NOCASE
By_Status                KEY(job:Current_Status,job:Ref_Number),DUP,NOCASE
StatusLocKey             KEY(job:Current_Status,job:Location,job:Ref_Number),DUP,NOCASE
Location_Key             KEY(job:Location,job:Ref_Number),DUP,NOCASE
Third_Party_Key          KEY(job:Third_Party_Site,job:Third_Party_Printed,job:Ref_Number),DUP,NOCASE
ThirdEsnKey              KEY(job:Third_Party_Site,job:Third_Party_Printed,job:ESN),DUP,NOCASE
ThirdMsnKey              KEY(job:Third_Party_Site,job:Third_Party_Printed,job:MSN),DUP,NOCASE
PriorityTypeKey          KEY(job:Job_Priority,job:Ref_Number),DUP,NOCASE
Unit_Type_Key            KEY(job:Unit_Type),DUP,NOCASE
EDI_Key                  KEY(job:Manufacturer,job:EDI,job:EDI_Batch_Number,job:Ref_Number),DUP,NOCASE
InvoiceNumberKey         KEY(job:Invoice_Number),DUP,NOCASE
WarInvoiceNoKey          KEY(job:Invoice_Number_Warranty),DUP,NOCASE
Batch_Number_Key         KEY(job:Batch_Number,job:Ref_Number),DUP,NOCASE
Batch_Status_Key         KEY(job:Batch_Number,job:Current_Status,job:Ref_Number),DUP,NOCASE
BatchModelNoKey          KEY(job:Batch_Number,job:Model_Number,job:Ref_Number),DUP,NOCASE
BatchInvoicedKey         KEY(job:Batch_Number,job:Invoice_Number,job:Ref_Number),DUP,NOCASE
BatchCompKey             KEY(job:Batch_Number,job:Completed,job:Ref_Number),DUP,NOCASE
ChaInvoiceKey            KEY(job:Chargeable_Job,job:Account_Number,job:Invoice_Number,job:Ref_Number),DUP,NOCASE
InvoiceExceptKey         KEY(job:Invoice_Exception,job:Ref_Number),DUP,NOCASE
ConsignmentNoKey         KEY(job:Consignment_Number),DUP,NOCASE
InConsignKey             KEY(job:Incoming_Consignment_Number),DUP,NOCASE
ReadyToDespKey           KEY(job:Despatched,job:Ref_Number),DUP,NOCASE
ReadyToTradeKey          KEY(job:Despatched,job:Account_Number,job:Ref_Number),DUP,NOCASE
ReadyToCouKey            KEY(job:Despatched,job:Current_Courier,job:Ref_Number),DUP,NOCASE
ReadyToAllKey            KEY(job:Despatched,job:Account_Number,job:Current_Courier,job:Ref_Number),DUP,NOCASE
DespJobNumberKey         KEY(job:Despatch_Number,job:Ref_Number),DUP,NOCASE
DateDespatchKey          KEY(job:Courier,job:Date_Despatched,job:Ref_Number),DUP,NOCASE
DateDespLoaKey           KEY(job:Loan_Courier,job:Loan_Despatched,job:Ref_Number),DUP,NOCASE
DateDespExcKey           KEY(job:Exchange_Courier,job:Exchange_Despatched,job:Ref_Number),DUP,NOCASE
ChaRepTypeKey            KEY(job:Repair_Type),DUP,NOCASE
WarRepTypeKey            KEY(job:Repair_Type_Warranty),DUP,NOCASE
ChaTypeKey               KEY(job:Charge_Type),DUP,NOCASE
WarChaTypeKey            KEY(job:Warranty_Charge_Type),DUP,NOCASE
Bouncer_Key              KEY(job:Bouncer,job:Ref_Number),DUP,NOCASE
EngDateCompKey           KEY(job:Engineer,job:Date_Completed),DUP,NOCASE
ExcStatusKey             KEY(job:Exchange_Status,job:Ref_Number),DUP,NOCASE
LoanStatusKey            KEY(job:Loan_Status,job:Ref_Number),DUP,NOCASE
ExchangeLocKey           KEY(job:Exchange_Status,job:Location,job:Ref_Number),DUP,NOCASE
LoanLocKey               KEY(job:Loan_Status,job:Location,job:Ref_Number),DUP,NOCASE
BatchJobKey              KEY(job:InvoiceAccount,job:InvoiceBatch,job:Ref_Number),DUP,NOCASE
BatchStatusKey           KEY(job:InvoiceAccount,job:InvoiceBatch,job:InvoiceStatus,job:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Batch_Number                LONG
Internal_Status             STRING(10)
Auto_Search                 STRING(30)
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Cancelled                   STRING(3)
Bouncer                     STRING(8)
Bouncer_Type                STRING(3)
Web_Type                    STRING(3)
Warranty_Job                STRING(3)
Chargeable_Job              STRING(3)
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(20)
MSN                         STRING(20)
ProductCode                 STRING(30)
Unit_Type                   STRING(30)
Colour                      STRING(30)
Location_Type               STRING(10)
Phone_Lock                  STRING(30)
Workshop                    STRING(3)
Location                    STRING(30)
Authority_Number            STRING(30)
Insurance_Reference_Number  STRING(30)
DOP                         DATE
Insurance                   STRING(3)
Insurance_Type              STRING(30)
Transit_Type                STRING(30)
Physical_Damage             STRING(3)
Intermittent_Fault          STRING(3)
Loan_Status                 STRING(30)
Exchange_Status             STRING(30)
Job_Priority                STRING(30)
Charge_Type                 STRING(30)
Warranty_Charge_Type        STRING(30)
Current_Status              STRING(30)
Account_Number              STRING(15)
Trade_Account_Name          STRING(30)
Department_Name             STRING(30)
Order_Number                STRING(30)
POP                         STRING(3)
In_Repair                   STRING(3)
Date_In_Repair              DATE
Time_In_Repair              TIME
On_Test                     STRING(3)
Date_On_Test                DATE
Time_On_Test                TIME
Estimate_Ready              STRING(3)
QA_Passed                   STRING(3)
Date_QA_Passed              DATE
Time_QA_Passed              TIME
QA_Rejected                 STRING(3)
Date_QA_Rejected            DATE
Time_QA_Rejected            TIME
QA_Second_Passed            STRING(3)
Date_QA_Second_Passed       DATE
Time_QA_Second_Passed       TIME
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Mobile_Number               STRING(15)
Postcode_Collection         STRING(10)
Company_Name_Collection     STRING(30)
Address_Line1_Collection    STRING(30)
Address_Line2_Collection    STRING(30)
Address_Line3_Collection    STRING(30)
Telephone_Collection        STRING(15)
Postcode_Delivery           STRING(10)
Address_Line1_Delivery      STRING(30)
Company_Name_Delivery       STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Date_Completed              DATE
Time_Completed              TIME
Completed                   STRING(3)
Paid                        STRING(3)
Paid_Warranty               STRING(3)
Date_Paid                   DATE
Repair_Type                 STRING(30)
Repair_Type_Warranty        STRING(30)
Engineer                    STRING(3)
Ignore_Chargeable_Charges   STRING(3)
Ignore_Warranty_Charges     STRING(3)
Ignore_Estimate_Charges     STRING(3)
Courier_Cost                REAL
Advance_Payment             REAL
Labour_Cost                 REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier_Cost_Estimate       REAL
Labour_Cost_Estimate        REAL
Parts_Cost_Estimate         REAL
Sub_Total_Estimate          REAL
Courier_Cost_Warranty       REAL
Labour_Cost_Warranty        REAL
Parts_Cost_Warranty         REAL
Sub_Total_Warranty          REAL
Loan_Issued_Date            DATE
Loan_Unit_Number            LONG
Loan_accessory              STRING(3)
Loan_User                   STRING(3)
Loan_Courier                STRING(30)
Loan_Consignment_Number     STRING(30)
Loan_Despatched             DATE
Loan_Despatched_User        STRING(3)
Loan_Despatch_Number        LONG
LoaService                  STRING(1)
Exchange_Unit_Number        LONG
Exchange_Authorised         STRING(3)
Loan_Authorised             STRING(3)
Exchange_Accessory          STRING(3)
Exchange_Issued_Date        DATE
Exchange_User               STRING(3)
Exchange_Courier            STRING(30)
Exchange_Consignment_Number STRING(30)
Exchange_Despatched         DATE
Exchange_Despatched_User    STRING(3)
Exchange_Despatch_Number    LONG
ExcService                  STRING(1)
Date_Despatched             DATE
Despatch_Number             LONG
Despatch_User               STRING(3)
Courier                     STRING(30)
Consignment_Number          STRING(30)
Incoming_Courier            STRING(30)
Incoming_Consignment_Number STRING(30)
Incoming_Date               DATE
Despatched                  STRING(3)
Despatch_Type               STRING(3)
Current_Courier             STRING(30)
JobService                  STRING(1)
Last_Repair_Days            REAL
Third_Party_Site            STRING(30)
Estimate                    STRING(3)
Estimate_If_Over            REAL
Estimate_Accepted           STRING(3)
Estimate_Rejected           STRING(3)
Third_Party_Printed         STRING(3)
ThirdPartyDateDesp          DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(255)
Fault_Code11                STRING(255)
Fault_Code12                STRING(255)
PreviousStatus              STRING(30)
StatusUser                  STRING(3)
Status_End_Date             DATE
Status_End_Time             TIME
Turnaround_End_Date         DATE
Turnaround_End_Time         TIME
Turnaround_Time             STRING(30)
Special_Instructions        STRING(30)
InvoiceAccount              STRING(15)
InvoiceStatus               STRING(3)
InvoiceBatch                LONG
InvoiceQuery                STRING(255)
EDI                         STRING(3)
EDI_Batch_Number            REAL
Invoice_Exception           STRING(3)
Invoice_Failure_Reason      STRING(80)
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Date_Warranty       DATE
Invoice_Courier_Cost        REAL
Invoice_Labour_Cost         REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
Invoice_Number_Warranty     LONG
WInvoice_Courier_Cost       REAL
WInvoice_Labour_Cost        REAL
WInvoice_Parts_Cost         REAL
WInvoice_Sub_Total          REAL
                         END
                     END                       

ARCJOBSE2            FILE,DRIVER('Btrieve'),OEM,NAME(file:JOBSE2),PRE(ajobe2),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ajobe2:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(ajobe2:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
IDNumber                    STRING(13)
InPendingDate               DATE
Contract                    BYTE
Prepaid                     BYTE
WarrantyRefNo               STRING(30)
SIDBookingName              STRING(60)
XAntenna                    BYTE
XLens                       BYTE
XFCover                     BYTE
XBCover                     BYTE
XKeypad                     BYTE
XBattery                    BYTE
XCharger                    BYTE
XLCD                        BYTE
XSimReader                  BYTE
XSystemConnector            BYTE
XNone                       BYTE
XNotes                      STRING(255)
ExchangeTerms               BYTE
WaybillNoFromPUP            LONG
WaybillNoToPUP              LONG
SMSNotification             BYTE
EmailNotification           BYTE
SMSAlertNumber              STRING(30)
EmailAlertAddress           STRING(255)
DateReceivedAtPUP           DATE
TimeReceivedAtPUP           TIME
DateDespatchFromPUP         DATE
TimeDespatchFromPUP         TIME
LoanIDNumber                STRING(13)
CourierWaybillNumber        STRING(30)
HubCustomer                 STRING(30)
HubCollection               STRING(30)
HubDelivery                 STRING(30)
WLabourPaid                 REAL
WPartsPaid                  REAL
WOtherCosts                 REAL
WSubTotal                   REAL
WVAT                        REAL
WTotal                      REAL
WarrantyReason              STRING(255)
WInvoiceRefNo               STRING(255)
SMSDate                     DATE
JobDiscountAmnt             REAL
InvDiscountAmnt             REAL
POPConfirmed                BYTE
ThirdPartyHandlingFee       REAL
InvThirdPartyHandlingFee    REAL
                         END
                     END                       

JOBSE2               FILE,DRIVER('Btrieve'),OEM,NAME('JOBSE2.DAT'),PRE(jobe2),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jobe2:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jobe2:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
IDNumber                    STRING(13)
InPendingDate               DATE
Contract                    BYTE
Prepaid                     BYTE
WarrantyRefNo               STRING(30)
SIDBookingName              STRING(60)
XAntenna                    BYTE
XLens                       BYTE
XFCover                     BYTE
XBCover                     BYTE
XKeypad                     BYTE
XBattery                    BYTE
XCharger                    BYTE
XLCD                        BYTE
XSimReader                  BYTE
XSystemConnector            BYTE
XNone                       BYTE
XNotes                      STRING(255)
ExchangeTerms               BYTE
WaybillNoFromPUP            LONG
WaybillNoToPUP              LONG
SMSNotification             BYTE
EmailNotification           BYTE
SMSAlertNumber              STRING(30)
EmailAlertAddress           STRING(255)
DateReceivedAtPUP           DATE
TimeReceivedAtPUP           TIME
DateDespatchFromPUP         DATE
TimeDespatchFromPUP         TIME
LoanIDNumber                STRING(13)
CourierWaybillNumber        STRING(30)
HubCustomer                 STRING(30)
HubCollection               STRING(30)
HubDelivery                 STRING(30)
WLabourPaid                 REAL
WPartsPaid                  REAL
WOtherCosts                 REAL
WSubTotal                   REAL
WVAT                        REAL
WTotal                      REAL
WarrantyReason              STRING(255)
WInvoiceRefNo               STRING(255)
SMSDate                     DATE
JobDiscountAmnt             REAL
InvDiscountAmnt             REAL
POPConfirmed                BYTE
ThirdPartyHandlingFee       REAL
InvThirdPartyHandlingFee    REAL
                         END
                     END                       

JOBSE                FILE,DRIVER('Btrieve'),OEM,NAME('JOBSE.DAT'),PRE(jobe),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jobe:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jobe:RefNumber),DUP,NOCASE
WarrStatusDateKey        KEY(jobe:WarrantyStatusDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
HubRepair                   BYTE
Network                     STRING(30)
POPConfirmed                BYTE
HubRepairDate               DATE
HubRepairTime               TIME
ClaimValue                  REAL
HandlingFee                 REAL
ExchangeRate                REAL
InvoiceClaimValue           REAL
InvoiceHandlingFee          REAL
InvoiceExchangeRate         REAL
BoxESN                      STRING(20)
ValidPOP                    STRING(3)
ReturnDate                  DATE
TalkTime                    REAL
OriginalPackaging           BYTE
OriginalBattery             BYTE
OriginalCharger             BYTE
OriginalAntenna             BYTE
OriginalManuals             BYTE
PhysicalDamage              BYTE
OriginalDealer              CSTRING(255)
BranchOfReturn              STRING(30)
COverwriteRepairType        BYTE
WOverwriteRepairType        BYTE
LabourAdjustment            REAL
PartsAdjustment             REAL
SubTotalAdjustment          REAL
IgnoreClaimCosts            BYTE
RRCCLabourCost              REAL
RRCCPartsCost               REAL
RRCCPartsSale               REAL
RRCCSubTotal                REAL
InvRRCCLabourCost           REAL
InvRRCCPartsCost            REAL
InvRRCCPartsSale            REAL
InvRRCCSubTotal             REAL
RRCWLabourCost              REAL
RRCWPartsCost               REAL
RRCWPartsSale               REAL
RRCWSubTotal                REAL
InvRRCWLabourCost           REAL
InvRRCWPartsCost            REAL
InvRRCWPartsSale            REAL
InvRRCWSubTotal             REAL
ARC3rdPartyCost             REAL
InvARC3rdPartCost           REAL
WebJob                      BYTE
RRCELabourCost              REAL
RRCEPartsCost               REAL
RRCESubTotal                REAL
IgnoreRRCChaCosts           REAL
IgnoreRRCWarCosts           REAL
IgnoreRRCEstCosts           REAL
OBFvalidated                BYTE
OBFvalidateDate             DATE
OBFvalidateTime             TIME
DespatchType                STRING(3)
Despatched                  STRING(3)
WarrantyClaimStatus         STRING(30)
WarrantyStatusDate          DATE
InSecurityPackNo            STRING(30)
JobSecurityPackNo           STRING(30)
ExcSecurityPackNo           STRING(30)
LoaSecurityPackNo           STRING(30)
ExceedWarrantyRepairLimit   BYTE
BouncerClaim                BYTE
Sub_Sub_Account             STRING(15)
ConfirmClaimAdjustment      BYTE
ARC3rdPartyVAT              REAL
ARC3rdPartyInvoiceNumber    STRING(30)
ExchangeAdjustment          REAL
ExchangedATRRC              BYTE
EndUserTelNo                STRING(15)
ClaimColour                 BYTE
ARC3rdPartyMarkup           REAL
Ignore3rdPartyCosts         BYTE
POPType                     STRING(30)
OBFProcessed                BYTE
LoanReplacementValue        REAL
PendingClaimColour          BYTE
AccessoryNotes              STRING(255)
ClaimPartsCost              REAL
InvClaimPartsCost           REAL
Booking48HourOption         BYTE
Engineer48HourOption        BYTE
ExcReplcamentCharge         BYTE
SecondExchangeNumber        LONG
SecondExchangeStatus        STRING(30)
VatNumber                   STRING(30)
ExchangeProductCode         STRING(30)
SecondExcProdCode           STRING(30)
ARC3rdPartyInvoiceDate      DATE
ARC3rdPartyWaybillNo        STRING(30)
ARC3rdPartyRepairType       STRING(30)
ARC3rdPartyRejectedReason   STRING(255)
ARC3rdPartyRejectedAmount   REAL
VSACustomer                 BYTE
HandsetReplacmentValue      REAL
SecondHandsetRepValue       REAL
t                           STRING(20)
                         END
                     END                       

ARCPARTS             FILE,DRIVER('Btrieve'),NAME(file:PARTS),PRE(apar),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(apar:Ref_Number,apar:Part_Number),DUP,NOCASE
recordnumberkey          KEY(apar:Record_Number),NOCASE,PRIMARY
PartRefNoKey             KEY(apar:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(apar:Date_Ordered),DUP,NOCASE
RefPartRefNoKey          KEY(apar:Ref_Number,apar:Part_Ref_Number),DUP,NOCASE
PendingRefNoKey          KEY(apar:Ref_Number,apar:Pending_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(apar:Ref_Number,apar:Order_Number),DUP,NOCASE
Supplier_Key             KEY(apar:Supplier),DUP,NOCASE
Order_Part_Key           KEY(apar:Ref_Number,apar:Order_Part_Number),DUP,NOCASE
SuppDateOrdKey           KEY(apar:Supplier,apar:Date_Ordered),DUP,NOCASE
SuppDateRecKey           KEY(apar:Supplier,apar:Date_Received),DUP,NOCASE
RequestedKey             KEY(apar:Requested,apar:Part_Number),DUP,NOCASE
WebOrderKey              KEY(apar:WebOrder,apar:Part_Number),DUP,NOCASE
PartAllocatedKey         KEY(apar:PartAllocated,apar:Part_Number),DUP,NOCASE
StatusKey                KEY(apar:Status,apar:Part_Number),DUP,NOCASE
AllocatedStatusKey       KEY(apar:PartAllocated,apar:Status,apar:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Record_Number               LONG
Adjustment                  STRING(3)
Part_Ref_Number             LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          LONG
Order_Number                LONG
Order_Part_Number           LONG
Date_Received               DATE
Status_Date                 DATE
Fault_Codes_Checked         STRING(3)
InvoicePart                 LONG
Credit                      STRING(3)
Requested                   BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
WebOrder                    BYTE
PartAllocated               BYTE
Status                      STRING(3)
AveragePurchaseCost         REAL
InWarrantyMarkup            LONG
OutWarrantyMarkup           LONG
RRCPurchaseCost             REAL
RRCSaleCost                 REAL
RRCInWarrantyMarkup         LONG
RRCOutWarrantyMarkup        LONG
RRCAveragePurchaseCost      REAL
ExchangeUnit                BYTE
SecondExchangeUnit          BYTE
Correction                  BYTE
                         END
                     END                       

PARTS                FILE,DRIVER('Btrieve'),NAME('PARTS.DAT'),PRE(par),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(par:Ref_Number,par:Part_Number),DUP,NOCASE
recordnumberkey          KEY(par:Record_Number),NOCASE,PRIMARY
PartRefNoKey             KEY(par:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(par:Date_Ordered),DUP,NOCASE
RefPartRefNoKey          KEY(par:Ref_Number,par:Part_Ref_Number),DUP,NOCASE
PendingRefNoKey          KEY(par:Ref_Number,par:Pending_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(par:Ref_Number,par:Order_Number),DUP,NOCASE
Supplier_Key             KEY(par:Supplier),DUP,NOCASE
Order_Part_Key           KEY(par:Ref_Number,par:Order_Part_Number),DUP,NOCASE
SuppDateOrdKey           KEY(par:Supplier,par:Date_Ordered),DUP,NOCASE
SuppDateRecKey           KEY(par:Supplier,par:Date_Received),DUP,NOCASE
RequestedKey             KEY(par:Requested,par:Part_Number),DUP,NOCASE
WebOrderKey              KEY(par:WebOrder,par:Part_Number),DUP,NOCASE
PartAllocatedKey         KEY(par:PartAllocated,par:Part_Number),DUP,NOCASE
StatusKey                KEY(par:Status,par:Part_Number),DUP,NOCASE
AllocatedStatusKey       KEY(par:PartAllocated,par:Status,par:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Record_Number               LONG
Adjustment                  STRING(3)
Part_Ref_Number             LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          LONG
Order_Number                LONG
Order_Part_Number           LONG
Date_Received               DATE
Status_Date                 DATE
Fault_Codes_Checked         STRING(3)
InvoicePart                 LONG
Credit                      STRING(3)
Requested                   BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
WebOrder                    BYTE
PartAllocated               BYTE
Status                      STRING(3)
AveragePurchaseCost         REAL
InWarrantyMarkup            LONG
OutWarrantyMarkup           LONG
RRCPurchaseCost             REAL
RRCSaleCost                 REAL
RRCInWarrantyMarkup         LONG
RRCOutWarrantyMarkup        LONG
RRCAveragePurchaseCost      REAL
ExchangeUnit                BYTE
SecondExchangeUnit          BYTE
Correction                  BYTE
                         END
                     END                       



Channel     Long
!--------------------------------------------------------------------------
! -----------Tintools-------------
!--------------------------------------------------------------------------
TinToolsViewRecord  EQUATE(15)
TinToolsCopyRecord  EQUATE(16)
!--------------------------------------------------------------------------
! -----------Tintools-------------
!--------------------------------------------------------------------------
Access:ARCJOBS       &FileManager
Relate:ARCJOBS       &RelationManager
Access:JOBSE3        &FileManager
Relate:JOBSE3        &RelationManager
Access:ARCJOBSTAMP   &FileManager
Relate:ARCJOBSTAMP   &RelationManager
Access:JOBSTAMP      &FileManager
Relate:JOBSTAMP      &RelationManager
Access:ARCDEFAULTV   &FileManager
Relate:ARCDEFAULTV   &RelationManager
Access:ARCJOBSWARR   &FileManager
Relate:ARCJOBSWARR   &RelationManager
Access:JOBSWARR      &FileManager
Relate:JOBSWARR      &RelationManager
Access:DEFAULTV      &FileManager
Relate:DEFAULTV      &RelationManager
Access:ARCJOBSE      &FileManager
Relate:ARCJOBSE      &RelationManager
Access:ARCAUDSTATS   &FileManager
Relate:ARCAUDSTATS   &RelationManager
Access:ARCJOBSTAGE   &FileManager
Relate:ARCJOBSTAGE   &RelationManager
Access:SMSMAIL       &FileManager
Relate:SMSMAIL       &RelationManager
Access:JOBSTAGE      &FileManager
Relate:JOBSTAGE      &RelationManager
Access:ARCESTPARTS   &FileManager
Relate:ARCESTPARTS   &RelationManager
Access:ESTPARTS      &FileManager
Relate:ESTPARTS      &RelationManager
Access:ARCJOBSE3     &FileManager
Relate:ARCJOBSE3     &RelationManager
Access:ARCAUDIT      &FileManager
Relate:ARCAUDIT      &RelationManager
Access:AUDIT         &FileManager
Relate:AUDIT         &RelationManager
Access:ARCCONTHIST   &FileManager
Relate:ARCCONTHIST   &RelationManager
Access:CONTHIST      &FileManager
Relate:CONTHIST      &RelationManager
Access:ARCJOBTHIRD   &FileManager
Relate:ARCJOBTHIRD   &RelationManager
Access:JOBTHIRD      &FileManager
Relate:JOBTHIRD      &RelationManager
Access:ARCJOBSOBF    &FileManager
Relate:ARCJOBSOBF    &RelationManager
Access:AUDSTATS      &FileManager
Relate:AUDSTATS      &RelationManager
Access:ARCJOBSENG    &FileManager
Relate:ARCJOBSENG    &RelationManager
Access:JOBSENG       &FileManager
Relate:JOBSENG       &RelationManager
Access:JOBEXHIS      &FileManager
Relate:JOBEXHIS      &RelationManager
Access:ARCLOCATLOG   &FileManager
Relate:ARCLOCATLOG   &RelationManager
Access:ARCAUDIT2     &FileManager
Relate:ARCAUDIT2     &RelationManager
Access:AUDIT2        &FileManager
Relate:AUDIT2        &RelationManager
Access:LOCATLOG      &FileManager
Relate:LOCATLOG      &RelationManager
Access:JOBSOBF       &FileManager
Relate:JOBSOBF       &RelationManager
Access:ARCJOBLOHIS   &FileManager
Relate:ARCJOBLOHIS   &RelationManager
Access:JOBLOHIS      &FileManager
Relate:JOBLOHIS      &RelationManager
Access:ARCJOBACC     &FileManager
Relate:ARCJOBACC     &RelationManager
Access:ARCJOBEXHIS   &FileManager
Relate:ARCJOBEXHIS   &RelationManager
Access:ARCJOBOUTFL   &FileManager
Relate:ARCJOBOUTFL   &RelationManager
Access:ARCWEBJOB     &FileManager
Relate:ARCWEBJOB     &RelationManager
Access:WEBJOB        &FileManager
Relate:WEBJOB        &RelationManager
Access:JOBOUTFL      &FileManager
Relate:JOBOUTFL      &RelationManager
Access:ARCJOBACCNO   &FileManager
Relate:ARCJOBACCNO   &RelationManager
Access:JOBACCNO      &FileManager
Relate:JOBACCNO      &RelationManager
Access:ARCJOBSCONS   &FileManager
Relate:ARCJOBSCONS   &RelationManager
Access:ARCSMSMAIL    &FileManager
Relate:ARCSMSMAIL    &RelationManager
Access:ARCWARPARTS   &FileManager
Relate:ARCWARPARTS   &RelationManager
Access:WARPARTS      &FileManager
Relate:WARPARTS      &RelationManager
Access:JOBACC        &FileManager
Relate:JOBACC        &RelationManager
Access:JOBSCONS      &FileManager
Relate:JOBSCONS      &RelationManager
Access:JOBS          &FileManager
Relate:JOBS          &RelationManager
Access:ARCJOBSE2     &FileManager
Relate:ARCJOBSE2     &RelationManager
Access:JOBSE2        &FileManager
Relate:JOBSE2        &RelationManager
Access:JOBSE         &FileManager
Relate:JOBSE         &RelationManager
Access:ARCPARTS      &FileManager
Relate:ARCPARTS      &RelationManager
Access:PARTS         &FileManager
Relate:PARTS         &RelationManager
FuzzyMatcher         FuzzyClass
GlobalErrors         ErrorClass
INIMgr               INIClass
GlobalRequest        BYTE(0),THREAD
GlobalResponse       BYTE(0),THREAD
VCRRequest           LONG(0),THREAD
lCurrentFDSetting    LONG
lAdjFDSetting        LONG

  CODE
  GlobalErrors.Init
  DctInit
  FuzzyMatcher.Init
  FuzzyMatcher.SetOption(MatchOption:NoCase, 1)
  FuzzyMatcher.SetOption(MatchOption:WordOnly, 0)
  INIMgr.Init('archive.INI')
      omit('***',ClarionetUsed=0)
      If ~clarionetserver:Active()
      ***
      IF (INSTRING(Upper('ARCHIVE'),Upper(DDEQUERY()),1,1))
      !look for running instances of this program
  !    IF Channel <> 0 ! and if found
          Channel = DDECLIENT('ARCHIVE')
          DDEEXECUTE(Channel,'INFRONT') ! tell the running instance to take focus
          DDECLOSE(Channel)
          RETURN ! then get out
      END
      omit('***',ClarionetUsed=0)
      End !If clarionetserver:Active()
      ***
  SystemParametersInfo (38, 0, lCurrentFDSetting, 0)
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 0, lAdjFDSetting, 3)
  END
  Main
  INIMgr.Update
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 1, lAdjFDSetting, 3)
  END
  INIMgr.Kill
  FuzzyMatcher.Kill
  DctKill
  GlobalErrors.Kill
    
BHStripReplace            Procedure(String func:String,String func:Strip,String func:Replace)
Code
    STR_LEN#  = LEN(func:String)
    STR_POS#  = 1
    StripLength#    = Len(func:Strip)

    func:String = UPPER(SUB(func:String,STR_POS#,1)) & SUB(func:String,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

       !Exception for space

        IF SUB(func:String,STR_POS#,StripLength#) = func:Strip
            If func:Replace <> ''
                func:String = SUB(func:String,1,STR_POS#-1) & func:Replace & SUB(func:String,STR_POS#+StripLength#,STR_LEN#-1)

            Else !If func:Replace <> ''
                func:String = SUB(func:String,1,STR_POS#-1) &  SUB(func:String,STR_POS#+StripLength#,STR_LEN#-1)

            End !If func:Replace <> ''

        End
    End
    RETURN(func:String)
BHStripNonAlphaNum      Procedure(String func:String,String func:Replace)
Code
    STR_LEN#  = LEN(func:String)
    STR_POS#  = 1

    func:String = UPPER(SUB(func:String,STR_POS#,1)) & SUB(func:String,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

       !Exception for space

     IF VAL(SUB(func:string,STR_POS#,1)) < 32 Or VAL(SUB(func:string,STR_POS#,1)) > 126 Or |
        VAL(SUB(func:string,STR_POS#,1)) = 34 Or VAL(SUB(func:string,STR_POS#,1)) = 44
           func:String = SUB(func:String,1,STR_POS#-1) & func:Replace & SUB(func:String,STR_POS#+1,STR_LEN#-1)
        End
    End
    RETURN(func:String)
BHReturnDaysHoursMins       Procedure(Long func:TotalMins,*Long func:Days,*Long func:Hours,*Long func:Mins)
Code
    func:Hours = 0
    func:Days = 0
    func:Mins = func:TotalMins
    If func:Totalmins >= 60
        Loop Until func:Mins < 60
            func:Mins -= 60
            func:Hours += 1
            If func:Hours > 23
                func:Days += 1
                func:Hours = 0
            End !If func:Hours > 23
        End !Loop Until local:MinutesLeft < 60
    End !If func:Minutes > 60
BHTimeDifference24Hr        Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime)
local:StartTime             Time()
local:EndTime               Time()
local:TotalTime             Long(0)

local:1Time  String(4)
local:1a     Long()
local:1b     Long()
local:1c     Long()
local:1d     Long()

local:2Time  String(4)
local:2a     Long()
local:2b     Long()
local:2c     Long()
local:2d     Long()

local:Totala    Long()
Local:Totalb    Long()
local:totalc    Long()
local:Totald    Long()

local:TimeDifference    Long()

Code
    If func:EndDate < func:StartDate
        Return 0
    End !If func:EndDate < func:StartDate

    If func:EndDate = func:StartDate
        If func:EndTime < func:StartTime
            Return 0
        End !If func:EndTime < func:StartTime
    End !If func:EndDate = func:StartDate
    Loop x# = func:StartDate to func:EndDate
        local:StartTime   = Deformat('00:00:00',@t4)
        local:EndTime     = Deformat('23:59:59',@t4)

        local:1a = 0
        local:1b = 0
        local:1c = 4
        local:1d = 2

        local:2a = 0
        local:2b = 0
        local:2c = 0
        local:2d = 0

        If x# = func:StartDate
            local:StartTime = func:StartTime
            local:2Time = Format(local:StartTime,@t2)
            local:2a = Sub(local:2Time,4,1)
            local:2b = Sub(local:2Time,3,1)
            local:2c = Sub(local:2time,2,1)
            local:2d = Sub(local:2time,1,1)

        End !If x# = func:StartDate
        If x# = func:EndDate
            local:EndTime = func:EndTime
            local:1Time = Format(local:EndTime,@t2)

            local:1a = Sub(local:1Time,4,1)
            local:1b = Sub(local:1Time,3,1)
            local:1c = Sub(local:1time,2,1)
            local:1d = Sub(local:1time,1,1)
        End !If x# = func:EndDate

        If local:1a < local:2a

            local:1a += 10

            If local:1b > 0
                local:1b -= 1
            Else !If local:1b > 0
                If local:1c > 0
                    local:1c -= 1
                Else !If local:1c > 0
                    local:1d -= 1
                    local:1c += 9
                End !If local:1c > 0
                local:1b += 5
            End !If local:1b > 0
        End !If local:1a > local:2a

        local:Totala = local:1a - local:2a


        If local:1b < local:2b
            If local:1c > 0
                local:1c -= 1
            Else !If local:1c > 0
                local:1d -= 1
                local:1c += 9
            End !If local:1c > 0
            local:1b += 6

        End !If local:1b => local:2b

        local:Totalb = local:1b - local:2b


        If local:1c < local:2c
            local:1d -= 1
            local:1c += 10

        End !If local:1c < local:2c

        local:Totalc = local:1c - local:2c
        local:totald = local:1d - local:2d

        local:TimeDifference = ((local:Totald & local:Totalc) * 60) + (local:Totalb & local:Totala)

        local:TotalTime += (local:TimeDifference)

    End !Loop x# = func:StartDate to func:EndDate
    Return INT(local:TotalTime)

BHTimeDifference24Hr5Days    Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime)
local:StartTime             Time()
local:EndTime               Time()
local:TotalTime             Long(0)

local:1Time  String(4)
local:1a     Long()
local:1b     Long()
local:1c     Long()
local:1d     Long()

local:2Time  String(4)
local:2a     Long()
local:2b     Long()
local:2c     Long()
local:2d     Long()

local:Totala    Long()
Local:Totalb    Long()
local:totalc    Long()
local:Totald    Long()

local:TimeDifference    Long()

Code
    If func:EndDate < func:StartDate
        Return 0
    End !If func:EndDate < func:StartDate

    If func:EndDate = func:StartDate
        If func:EndTime < func:StartTime
            Return 0
        End !If func:EndTime < func:StartTime
    End !If func:EndDate = func:StartDate
    Loop x# = func:StartDate to func:EndDate
        local:StartTime   = Deformat('00:00:00',@t4)
        local:EndTime     = Deformat('23:59:59',@t4)

        If (x# % 7) = 0
            Cycle
        End
        If (x# % 7) = 6
            Cycle
        End

        local:1a = 0
        local:1b = 0
        local:1c = 4
        local:1d = 2

        local:2a = 0
        local:2b = 0
        local:2c = 0
        local:2d = 0

        If x# = func:StartDate
            local:StartTime = func:StartTime
            local:2Time = Format(local:StartTime,@t2)
            local:2a = Sub(local:2Time,4,1)
            local:2b = Sub(local:2Time,3,1)
            local:2c = Sub(local:2time,2,1)
            local:2d = Sub(local:2time,1,1)

        End !If x# = func:StartDate
        If x# = func:EndDate
            local:EndTime = func:EndTime
            local:1Time = Format(local:EndTime,@t2)

            local:1a = Sub(local:1Time,4,1)
            local:1b = Sub(local:1Time,3,1)
            local:1c = Sub(local:1time,2,1)
            local:1d = Sub(local:1time,1,1)
        End !If x# = func:EndDate

        If local:1a < local:2a

            local:1a += 10

            If local:1b > 0
                local:1b -= 1
            Else !If local:1b > 0
                If local:1c > 0
                    local:1c -= 1
                Else !If local:1c > 0
                    local:1d -= 1
                    local:1c += 9
                End !If local:1c > 0
                local:1b += 5
            End !If local:1b > 0
        End !If local:1a > local:2a

        local:Totala = local:1a - local:2a


        If local:1b < local:2b
            If local:1c > 0
                local:1c -= 1
            Else !If local:1c > 0
                local:1d -= 1
                local:1c += 9
            End !If local:1c > 0
            local:1b += 6

        End !If local:1b => local:2b

        local:Totalb = local:1b - local:2b


        If local:1c < local:2c
            local:1d -= 1
            local:1c += 10

        End !If local:1c < local:2c

        local:Totalc = local:1c - local:2c
        local:totald = local:1d - local:2d

        local:TimeDifference = ((local:Totald & local:Totalc) * 60) + (local:Totalb & local:Totala)

        local:TotalTime += (local:TimeDifference)

    End !Loop x# = func:StartDate to func:EndDate
    Return INT(local:TotalTime)

BHTimeDifference24Hr5DaysMonday    Procedure(Date func:StartDate, Date func:EndDate, Time func:StartTime, Time func:EndTime,String func:MondayStart)
local:StartTime             Time()
local:EndTime               Time()
local:TotalTime             Long(0)

local:1Time  String(4)
local:1a     Long()
local:1b     Long()
local:1c     Long()
local:1d     Long()

local:2Time  String(4)
local:2a     Long()
local:2b     Long()
local:2c     Long()
local:2d     Long()

local:Totala    Long()
Local:Totalb    Long()
local:totalc    Long()
local:Totald    Long()

local:TimeDifference    Long()

Code
    If func:EndDate < func:StartDate
        Return 0
    End !If func:EndDate < func:StartDate

    If func:EndDate = func:StartDate
        If func:EndTime < func:StartTime
            Return 0
        End !If func:EndTime < func:StartTime
    End !If func:EndDate = func:StartDate
    Loop x# = func:StartDate to func:EndDate
        If (x# % 7) = 1
            local:StartTime   = Deformat(Clip(func:MondayStart),@t4)
            local:2Time = Format(local:StartTime,@t2)
            local:2a = Sub(local:2Time,4,1)
            local:2b = Sub(local:2Time,3,1)
            local:2c = Sub(local:2time,2,1)
            local:2d = Sub(local:2time,1,1)
        Else !If (x# % 7) = 1
            local:StartTime   = Deformat('00:00:00',@t4)
            local:2a = 0
            local:2b = 0
            local:2c = 0
            local:2d = 0
        End !If (x# % 7) = 1

        local:EndTime     = Deformat('23:59:59',@t4)

        If (x# % 7) = 0
            Cycle
        End
        If (x# % 7) = 6
            Cycle
        End

        local:1a = 0
        local:1b = 0
        local:1c = 4
        local:1d = 2



        If x# = func:StartDate
            local:StartTime = func:StartTime
            local:2Time = Format(local:StartTime,@t2)
            local:2a = Sub(local:2Time,4,1)
            local:2b = Sub(local:2Time,3,1)
            local:2c = Sub(local:2time,2,1)
            local:2d = Sub(local:2time,1,1)

        End !If x# = func:StartDate
        If x# = func:EndDate
            local:EndTime = func:EndTime
            local:1Time = Format(local:EndTime,@t2)

            local:1a = Sub(local:1Time,4,1)
            local:1b = Sub(local:1Time,3,1)
            local:1c = Sub(local:1time,2,1)
            local:1d = Sub(local:1time,1,1)
        End !If x# = func:EndDate

        If local:1a < local:2a

            local:1a += 10

            If local:1b > 0
                local:1b -= 1
            Else !If local:1b > 0
                If local:1c > 0
                    local:1c -= 1
                Else !If local:1c > 0
                    local:1d -= 1
                    local:1c += 9
                End !If local:1c > 0
                local:1b += 5
            End !If local:1b > 0
        End !If local:1a > local:2a

        local:Totala = local:1a - local:2a


        If local:1b < local:2b
            If local:1c > 0
                local:1c -= 1
            Else !If local:1c > 0
                local:1d -= 1
                local:1c += 9
            End !If local:1c > 0
            local:1b += 6

        End !If local:1b => local:2b

        local:Totalb = local:1b - local:2b


        If local:1c < local:2c
            local:1d -= 1
            local:1c += 10

        End !If local:1c < local:2c

        local:Totalc = local:1c - local:2c
        local:totald = local:1d - local:2d

        local:TimeDifference = ((local:Totald & local:Totalc) * 60) + (local:Totalb & local:Totala)

        local:TotalTime += (local:TimeDifference)

    End !Loop x# = func:StartDate to func:EndDate
    Return INT(local:TotalTime)
BHGetFileFromPath       Procedure(String f:Path)
Code
     Loop x# = Len(f:Path) To 1 By -1
        If Sub(f:Path,x#,1) = '\'
            Return Clip(Sub(f:Path,x# + 1,255))
            Break
        End ! If Sub(f:Path,x#,1) = '\'
    End ! Loop x# = Len(f:Path) To 1 By -1

BHGetPathFromFile       Procedure(String f:Path)
Code
    Loop x# = Len(f:Path) To 1 By -1
        If Sub(f:Path,x#,1) = '\'
            Return Clip(Sub(f:Path,1,x#))
            Break
        End ! If Sub(f:Path,x#) = '\'
    End ! Loop x# = 1 To Len(f:Path) To 1 By -1

BHGetFileNoExtension    PROCEDURE(STRING fullPath)
count LONG()
locFilename                                             STRING(255)
    code
        locFilename = BHGetFileFromPath(fullPath)
        LOOP count = LEN(CLIP(locFilename)) TO 1 BY -1
            IF (SUB(locFilename,count,1) = '.')
                locFilename = CLIP(SUB(locFilename,1,count - 1))
                BREAK
            END
        END
        RETURN CLIP(locFilename)

BHAddBackSlash              Procedure(STRING fullPath)
ReturnPath  STRING(255)
    code
        ReturnPath = fullPath
        IF (SUB(CLIP(fullPath),-1,1) <> '\')
            ReturnPath = CLIP(fullPath) & '\'
        END
        RETURN CLIP(ReturnPath)

BHRunDOS                Procedure(String f:Command,<f:Wait>,<f:NoTimeOut>)
NORMAL_PRIORITY_CLASS             equate(00000020h)
CREATE_NO_WINDOW                  equate(08000000h)

!STILL_ACTIVE          equate(259)

ProcessExitCode       ulong
CommandLine           cstring(1024)

StartUpInfo           group
Cb                      ulong
lpReserved              ulong
lpDesktop               ulong
lpTitle                 ulong
dwX                     ulong
dwY                     ulong
dwXSize                 ulong
dwYSize                 ulong
dwXCountChars           ulong
dwYCountChars           ulong
dwFillAttribute         ulong
dwFlags                 ulong
wShowWindow             signed
cbReserved2             signed
lpReserved2             ulong
hStdInput               unsigned
hStdOutput              unsigned
hStdError               unsigned
                      end

ProcessInformation    group
hProcess                unsigned
hThread                 unsigned
dwProcessId             ulong
dwThreadId              ulong
                      end
Code
    CommandLine = clip(f:Command)
    StartUpInfo.Cb = size(StartUpInfo)

    ReturnCode# = DBGCreateProcess(0,                                             |
                                address(CommandLine),                          |
                                0,                                             |
                                0,                                             |
                                0,                                             |
                                BOR(NORMAL_PRIORITY_CLASS, CREATE_NO_WINDOW),  |
                                0,                                             |
                                0,                                             |
                                address(StartUpInfo),                          |
                                address(ProcessInformation))

    if ReturnCode# = 0  then
        ! Error Return
        return false
    end

    timeout# = clock() + (f:Wait * 100)
    setcursor(cursor:wait)

    if f:Wait > 0 or f:NoTimeOut > 0 then
        loop
            DBGSleep(100)
            ! check for when process is finished
            ReturnCode# = DBGGetExitCodeProcess(ProcessInformation.hProcess, address(ProcessExitCode))
            if (ProcessExitCode <> 259)  ! 259 = Still Active
                break
            end
            if (f:NoTimeOut <> 1)
                if (clock() < timeout#)
                    break
                end
            end
        end
    end
    setcursor
    return(true)

!* * * * Line Print Template Generated Code (Start) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

LinePrint FUNCTION(STRING StringToPrint,<STRING DeviceName>,<BYTE CRLF>)                ! LinePrint Function

   CODE                                                                                 ! Fuction Code Starts Here
    IF OMITTED(2)                                                                       ! If Device Name is Omitted
       IF SUB(PRINTER{07B29H},1,2) = '\\' 
         lpszFilename = SUB(PRINTER{07B29H},1,LEN(CLIP(PRINTER{07B29H})))               ! Use Default Device
       ELSE
         lpszFilename = SUB(PRINTER{07B29H},1,LEN(CLIP(PRINTER{07B29H})) - 1)           ! Use Default Device
       END 
    ELSE                                                                                ! Otherwise
       lpszFilename = CLIP(DeviceName)                                                  ! Use passed Device Name
    END                                                                                 ! Terminate IF

    IF (OMITTED(3) OR CRLF = True) AND CLIP(StringToPrint) <> FF                        ! If CRLF parameter is set or omitted and user did not pass FF
        hpvBuffer = StringToPrint & CR & LF                                             ! Print String with CR & LF
    ELSE                                                                                ! Otherwise
       hpvBuffer = StringToPrint                                                        ! Print Text As Is
    END                                                                                 ! Terminate IF

    cbBuffer = LEN(CLIP(hpvBuffer))                                                     ! Check Length of the Data to be Printed

     hf = OpenFile(lpszFilename,OF_STRUCT,OF_WRITE)                                     ! Open file and obtain file handle
     IF hf = -1                                                                         ! If File does not exist
      hf = OpenFile(lpszFilename,OF_STRUCT,OF_CREATE)                                   ! Create file and obtain file handle
      IF hf = -1 THEN RETURN(OpenError).                                                ! If Error then return OpenError
     END                                                                                ! Terminate IF

    IF SUB(lpszFilename,1,3) <> 'COM' AND |                                             ! If user prints to a file
       SUB(lpszFilename,1,3) <> 'LPT' AND |                                            
       SUB(lpszFilename,1,2) <> '\\'
       IF _llseek(hf,0,2) = -1 THEN RETURN(4).                                          ! Set file pointer to the end of the file (Append lines)
    END                                                                                 ! Terminate IF

    BytesWritten = _lwrite(hf,hpvBuffer,cbBuffer)                                       ! Write to the file
    IF BytesWritten < cbBuffer THEN RETURN(WriteError).                                 ! IF Writing to a device or file is not possible, return Write Error
    IF _lclose(hf) THEN RETURN(CloseError) ELSE RETURN(Succeeded).                      ! If error in closing device or a file, return CloseError otherwise return Succeeded


DeleteFile FUNCTION(STRING FileToDelete)                                                ! DeleteFile Function

    CODE                                                                                ! Function Code Starts Here
    lpszFilename = CLIP(FileToDelete)                                                   ! Put file name in the buffer
    hf = OpenFile(lpszFilename,OF_STRUCT,OF_DELETE)                                     ! and delete it
     IF hf = -1 THEN RETURN(OpenError) ELSE RETURN(Succeeded).                          ! If error, return OpenError otherwise Return Succeeded

!* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 


