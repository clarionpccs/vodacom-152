

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER206.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER010.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER017.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER120.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER184.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER208.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER210.INC'),ONCE        !Req'd for module callout resolution
                     END


FormStockAllocation  PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locStockAllocationType STRING(30)                          !
FilesOpened     Long
STOCKALL::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('FormStockAllocation')
  loc:formname = 'FormStockAllocation_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormStockAllocation',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormStockAllocation','')
    p_web._DivHeader('FormStockAllocation',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormStockAllocation',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockAllocation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockAllocation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormStockAllocation',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockAllocation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormStockAllocation',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormStockAllocation',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(STOCKALL)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOCKALL)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SSV('RapidLocation',RapidLocation(p_web.GSV('Default:SiteLocation')))
  IF (p_web.GSV('RapidLocation') <> 1)
        p_web.SSV('locStockAllocationType','WEB')
        p_web.SSV('title','Parts On Order')
    ELSE
        p_web.SSV('title','Rapid Stock Allocation')
  END
  p_web.SSV('RecordSelected',0)
  p_web.SetValue('FormStockAllocation_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('RapidLocation') = 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locStockAllocationType',locStockAllocationType)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locStockAllocationType')
    locStockAllocationType = p_web.GetValue('locStockAllocationType')
    p_web.SetSessionValue('locStockAllocationType',locStockAllocationType)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormStockAllocation_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
      p_web.site.SaveButton.TextValue = 'Close'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locStockAllocationType = p_web.RestoreValue('locStockAllocationType')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'FormBrowseStock'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormStockAllocation_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormStockAllocation_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormStockAllocation_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormStockAllocation" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormStockAllocation" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormStockAllocation" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate(p_web.GSV('title')) <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate(p_web.GSV('title'),0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket
    do SendPacket
  If (p_web.GSV('RapidLocation') = 1)
    Do theKey
    do SendPacket
  End

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormStockAllocation">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormStockAllocation" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormStockAllocation')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GSV('RapidLocation') = 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Criteria') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Click On Line Above To View Options Below') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormStockAllocation')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormStockAllocation'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormStockAllocation_brwStockAllocation_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormStockAllocation_brwStockAllocation_embedded_div')&''');'
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('RapidLocation') = 1
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GSV('RapidLocation') = 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormStockAllocation')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GSV('RapidLocation') = 1
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GSV('RapidLocation') = 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Criteria') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormStockAllocation_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locStockAllocationType
      do Value::locStockAllocationType
      do Comment::locStockAllocationType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormStockAllocation_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    If DoesUserHaveAccess(p_web,'STOCK ALLOCATION REFRESH')
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::linkRefreshList
      do Comment::linkRefreshList
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwStockAllocation
      do Comment::brwStockAllocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Click On Line Above To View Options Below') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormStockAllocation_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Click On Line Above To View Options Below')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Click On Line Above To View Options Below')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Click On Line Above To View Options Below')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Click On Line Above To View Options Below')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnProcessPicking
      do Comment::btnProcessPicking
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnPicked
      do Comment::btnPicked
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnAllocatePart
      do Comment::btnAllocatePart
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnRestockExchange
      do Comment::btnRestockExchange
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnReturnPart
      do Comment::btnReturnPart
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnAutoFulfilRequest
      do Comment::btnAutoFulfilRequest
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locStockAllocationType  Routine
  p_web._DivHeader('FormStockAllocation_' & p_web._nocolon('locStockAllocationType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Browse Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locStockAllocationType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locStockAllocationType',p_web.GetValue('NewValue'))
    locStockAllocationType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locStockAllocationType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locStockAllocationType',p_web.GetValue('Value'))
    locStockAllocationType = p_web.GetValue('Value')
  End
  do Value::locStockAllocationType
  do SendAlert
  do Value::brwStockAllocation  !1
  do Value::btnAllocatePart  !1
  do Value::btnAutoFulfilRequest  !1
  do Value::btnPicked  !1
  do Value::btnProcessPicking  !1
  do Value::btnRestockExchange  !1
  do Value::btnReturnPart  !1

Value::locStockAllocationType  Routine
  p_web._DivHeader('FormStockAllocation_' & p_web._nocolon('locStockAllocationType') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locStockAllocationType
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locStockAllocationType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locStockAllocationType') = 'ALL'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locStockAllocationType'',''formstockallocation_locstockallocationtype_value'',1,'''&clip('ALL')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locStockAllocationType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locStockAllocationType',clip('ALL'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locStockAllocationType_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('All') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locStockAllocationType') = ' '
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locStockAllocationType'',''formstockallocation_locstockallocationtype_value'',1,'''&clip(' ')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locStockAllocationType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locStockAllocationType',clip(' '),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locStockAllocationType_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Awaiting Processing') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locStockAllocationType') = 'PRO'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locStockAllocationType'',''formstockallocation_locstockallocationtype_value'',1,'''&clip('PRO')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locStockAllocationType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locStockAllocationType',clip('PRO'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locStockAllocationType_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Picking In Progress') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locStockAllocationType') = 'PIK'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locStockAllocationType'',''formstockallocation_locstockallocationtype_value'',1,'''&clip('PIK')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locStockAllocationType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locStockAllocationType',clip('PIK'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locStockAllocationType_4') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Picked') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locStockAllocationType') = 'RET'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locStockAllocationType'',''formstockallocation_locstockallocationtype_value'',1,'''&clip('RET')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locStockAllocationType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locStockAllocationType',clip('RET'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locStockAllocationType_5') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Awaiting Return') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locStockAllocationType') = 'WEB'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locStockAllocationType'',''formstockallocation_locstockallocationtype_value'',1,'''&clip('WEB')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locStockAllocationType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locStockAllocationType',clip('WEB'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locStockAllocationType_6') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Parts On Order') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAllocation_' & p_web._nocolon('locStockAllocationType') & '_value')

Comment::locStockAllocationType  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAllocation_' & p_web._nocolon('locStockAllocationType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::linkRefreshList  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('linkRefreshList',p_web.GetValue('NewValue'))
    do Value::linkRefreshList
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::linkRefreshList  Routine
  p_web._DivHeader('FormStockAllocation_' & p_web._nocolon('linkRefreshList') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('rebuildStockAllocation()')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.CreateHyperLink(p_web.Translate('Refresh List'),'#',,,loc:javascript,,) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::linkRefreshList  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAllocation_' & p_web._nocolon('linkRefreshList') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::brwStockAllocation  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwStockAllocation',p_web.GetValue('NewValue'))
    do Value::brwStockAllocation
  Else
    p_web.StoreValue('stl:RecordNumber')
  End
    p_web.SSV('RecordSelected',1)
    Access:STOCKALL.ClearKey(stl:RecordNumberKey)
    stl:RecordNumber = p_web.GSV('stl:RecordNumber')
    IF (Access:STOCKALL.TryFetch(stl:RecordNumberKey) = Level:Benign)
        p_web.SSV('stl:PartNumber',stl:PartNumber)
    END
  
  do SendAlert
  do Value::btnAllocatePart  !1
  do Value::btnAutoFulfilRequest  !1
  do Value::btnPicked  !1
  do Value::btnProcessPicking  !1
  do Value::btnRestockExchange  !1
  do Value::btnReturnPart  !1

Value::brwStockAllocation  Routine
  loc:extra = ''
  ! --- BROWSE ---  brwStockAllocation --
  p_web.SetValue('brwStockAllocation:NoForm',1)
  p_web.SetValue('brwStockAllocation:FormName',loc:formname)
  p_web.SetValue('brwStockAllocation:parentIs','Form')
  p_web.SetValue('_parentProc','FormStockAllocation')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormStockAllocation_brwStockAllocation_embedded_div')&'"><!-- Net:brwStockAllocation --></div><13,10>'
    p_web._DivHeader('FormStockAllocation_' & lower('brwStockAllocation') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormStockAllocation_' & lower('brwStockAllocation') & '_value')
  else
    packet = clip(packet) & '<!-- Net:brwStockAllocation --><13,10>'
  end
  do SendPacket

Comment::brwStockAllocation  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAllocation_' & p_web._nocolon('brwStockAllocation') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnProcessPicking  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnProcessPicking',p_web.GetValue('NewValue'))
    do Value::btnProcessPicking
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::brwStockAllocation  !1

Value::btnProcessPicking  Routine
  p_web._DivHeader('FormStockAllocation_' & p_web._nocolon('btnProcessPicking') & '_value',Choose(p_web.GSV('locStockAllocationType') = 'WEB' OR p_web.GSV('RecordSelected') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locStockAllocationType') = 'WEB' OR p_web.GSV('RecordSelected') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnProcessPicking'',''formstockallocation_btnprocesspicking_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnProcessPicking','Process Picking','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('frmChangeStockStatus?' &'STA=PRO')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAllocation_' & p_web._nocolon('btnProcessPicking') & '_value')

Comment::btnProcessPicking  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAllocation_' & p_web._nocolon('btnProcessPicking') & '_comment',Choose(p_web.GSV('locStockAllocationType') = 'WEB' OR p_web.GSV('RecordSelected') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locStockAllocationType') = 'WEB' OR p_web.GSV('RecordSelected') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnPicked  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnPicked',p_web.GetValue('NewValue'))
    do Value::btnPicked
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::brwStockAllocation  !1

Value::btnPicked  Routine
  p_web._DivHeader('FormStockAllocation_' & p_web._nocolon('btnPicked') & '_value',Choose(p_web.GSV('locStockAllocationType') = 'WEB' OR p_web.GSV('RecordSelected') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locStockAllocationType') = 'WEB' OR p_web.GSV('RecordSelected') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnPicked'',''formstockallocation_btnpicked_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnPicked','Picked','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('frmChangeStockStatus?' &'STA=PIK')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAllocation_' & p_web._nocolon('btnPicked') & '_value')

Comment::btnPicked  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAllocation_' & p_web._nocolon('btnPicked') & '_comment',Choose(p_web.GSV('locStockAllocationType') = 'WEB' OR p_web.GSV('RecordSelected') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locStockAllocationType') = 'WEB' OR p_web.GSV('RecordSelected') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnAllocatePart  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnAllocatePart',p_web.GetValue('NewValue'))
    do Value::btnAllocatePart
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::brwStockAllocation  !1

Value::btnAllocatePart  Routine
  p_web._DivHeader('FormStockAllocation_' & p_web._nocolon('btnAllocatePart') & '_value',Choose(p_web.GSV('locStockAllocationType') = 'WEB' OR p_web.GSV('RecordSelected') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locStockAllocationType') = 'WEB' OR p_web.GSV('RecordSelected') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnAllocatePart'',''formstockallocation_btnallocatepart_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnAllocatePart','Allocate Part','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('frmChangeStockStatus?' &'STA=ALL')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAllocation_' & p_web._nocolon('btnAllocatePart') & '_value')

Comment::btnAllocatePart  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAllocation_' & p_web._nocolon('btnAllocatePart') & '_comment',Choose(p_web.GSV('locStockAllocationType') = 'WEB' OR p_web.GSV('RecordSelected') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locStockAllocationType') = 'WEB' OR p_web.GSV('RecordSelected') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnRestockExchange  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnRestockExchange',p_web.GetValue('NewValue'))
    do Value::btnRestockExchange
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::brwStockAllocation  !1

Value::btnRestockExchange  Routine
  p_web._DivHeader('FormStockAllocation_' & p_web._nocolon('btnRestockExchange') & '_value',Choose(p_web.GSV('locStockAllocationType') = 'WEB' OR p_web.GSV('RecordSelected') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locStockAllocationType') = 'WEB' OR p_web.GSV('RecordSelected') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnRestockExchange'',''formstockallocation_btnrestockexchange_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnRestockExchange','Restock/Allocate Exchange','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PickExchangeUnit?' &'FromURL=StockAllocation&ReturnURL=FormStockAllocation&Warning=' & p_web.GSV('stl:PartNumber'))) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAllocation_' & p_web._nocolon('btnRestockExchange') & '_value')

Comment::btnRestockExchange  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAllocation_' & p_web._nocolon('btnRestockExchange') & '_comment',Choose(p_web.GSV('locStockAllocationType') = 'WEB' OR p_web.GSV('RecordSelected') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locStockAllocationType') = 'WEB' OR p_web.GSV('RecordSelected') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnReturnPart  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnReturnPart',p_web.GetValue('NewValue'))
    do Value::btnReturnPart
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::brwStockAllocation  !1

Value::btnReturnPart  Routine
  p_web._DivHeader('FormStockAllocation_' & p_web._nocolon('btnReturnPart') & '_value',Choose(p_web.GSV('locStockAllocationType') = 'WEB' OR p_web.GSV('RecordSelected') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locStockAllocationType') = 'WEB' OR p_web.GSV('RecordSelected') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnReturnPart'',''formstockallocation_btnreturnpart_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnReturnPart','Return Part','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormDeletePart?' &'FromURL=StockAllocation&ReturnURL=FormStockAllocation')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAllocation_' & p_web._nocolon('btnReturnPart') & '_value')

Comment::btnReturnPart  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAllocation_' & p_web._nocolon('btnReturnPart') & '_comment',Choose(p_web.GSV('locStockAllocationType') = 'WEB' OR p_web.GSV('RecordSelected') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locStockAllocationType') = 'WEB' OR p_web.GSV('RecordSelected') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnAutoFulfilRequest  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnAutoFulfilRequest',p_web.GetValue('NewValue'))
    do Value::btnAutoFulfilRequest
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::brwStockAllocation  !1

Value::btnAutoFulfilRequest  Routine
  p_web._DivHeader('FormStockAllocation_' & p_web._nocolon('btnAutoFulfilRequest') & '_value',Choose(p_web.GSV('locStockAllocationType') <> 'WEB','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locStockAllocationType') <> 'WEB')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('autoFulFilRequests()')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnAutoFulfilRequest','Auto Fulfil Requests','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAllocation_' & p_web._nocolon('btnAutoFulfilRequest') & '_value')

Comment::btnAutoFulfilRequest  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAllocation_' & p_web._nocolon('btnAutoFulfilRequest') & '_comment',Choose(p_web.GSV('locStockAllocationType') <> 'WEB','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locStockAllocationType') <> 'WEB'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormStockAllocation_locStockAllocationType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locStockAllocationType
      else
        do Value::locStockAllocationType
      end
  of lower('FormStockAllocation_brwStockAllocation_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::brwStockAllocation
      else
        do Value::brwStockAllocation
      end
  of lower('FormStockAllocation_btnProcessPicking_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnProcessPicking
      else
        do Value::btnProcessPicking
      end
  of lower('FormStockAllocation_btnPicked_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnPicked
      else
        do Value::btnPicked
      end
  of lower('FormStockAllocation_btnAllocatePart_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnAllocatePart
      else
        do Value::btnAllocatePart
      end
  of lower('FormStockAllocation_btnRestockExchange_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnRestockExchange
      else
        do Value::btnRestockExchange
      end
  of lower('FormStockAllocation_btnReturnPart_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnReturnPart
      else
        do Value::btnReturnPart
      end
  of lower('FormStockAllocation_btnAutoFulfilRequest_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnAutoFulfilRequest
      else
        do Value::btnAutoFulfilRequest
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormStockAllocation_form:ready_',1)
  p_web.SetSessionValue('FormStockAllocation_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormStockAllocation',0)

PreCopy  Routine
  p_web.SetValue('FormStockAllocation_form:ready_',1)
  p_web.SetSessionValue('FormStockAllocation_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormStockAllocation',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormStockAllocation_form:ready_',1)
  p_web.SetSessionValue('FormStockAllocation_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormStockAllocation:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormStockAllocation_form:ready_',1)
  p_web.SetSessionValue('FormStockAllocation_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormStockAllocation:Primed',0)
  p_web.setsessionvalue('showtab_FormStockAllocation',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('RapidLocation') = 1
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormStockAllocation_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormStockAllocation_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
  If p_web.GSV('RapidLocation') = 1
    loc:InvalidTab += 1
  End
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormStockAllocation:Primed',0)
  p_web.StoreValue('locStockAllocationType')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
theKey  Routine
  packet = clip(packet) & |
    '<<style><13,10>'&|
    '#key ul {{<13,10>'&|
    '  display: table;<13,10>'&|
    '  width: 100%;<13,10>'&|
    '}<13,10>'&|
    '#key li {{<13,10>'&|
    '  display: table-cell;<13,10>'&|
    '}<13,10>'&|
    '#key {{<13,10>'&|
    '  font-weight: bold;<13,10>'&|
    '}<13,10>'&|
    '<</style><13,10>'&|
    '<13,10>'&|
    '<13,10>'&|
    '<<div id="key"><13,10>'&|
    '<<span id="key">Key:<</span><13,10>'&|
    '<<ul><13,10>'&|
    '<<li><<img width="16" height="16" src="/images/bullet_purple.png" /> - On Order<</li><13,10>'&|
    '<<li><<img width="16" height="16" src="/images/bullet_black.png" /> - Awaiting Processing<</li><13,10>'&|
    '<<li><<img width="16" height="16" src="/images/bullet_pink.png" /> - Awaiting Picking<</li><13,10>'&|
    '<<li><<img width="16" height="16" src="/images/bullet_blue.png" /> - Picked<</li><13,10>'&|
    '<<li><<img width="16" height="16" src="/images/bullet_red.png" /> - Awaiting Return<</li><13,10>'&|
    '<</ul><13,10>'&|
    '<</div><13,10>'&|
    ''
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locStockAllocationType',locStockAllocationType) ! STRING(30)
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locStockAllocationType = p_web.GSV('locStockAllocationType') ! STRING(30)
