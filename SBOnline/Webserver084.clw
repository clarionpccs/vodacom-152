

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER084.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ForceTransitType     PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !

  CODE
    !Initial Transit Type
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    If (def:Force_Initial_Transit_Type ='B' and func:Type = 'B') Or |
        (def:Force_Initial_Transit_Type <> 'I' and func:Type = 'C')
        Return Level:Fatal
    End
    Return Level:Benign
