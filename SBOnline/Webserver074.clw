

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER074.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
CustClassificationFields PROCEDURE  (LONG refNumber,*STRING lifeTimeValue,*STRING averageSpend,*STRING loyaltyStatus,*DATE upgradeDate,*STRING IDNumber) ! Declare Procedure

  CODE
    vod.CustomerClassificationFields(refNumber,|
        lifeTimeValue,|
        averageSpend,|
        loyaltyStatus,|
        upgradeDate,|
        IDNumber)
