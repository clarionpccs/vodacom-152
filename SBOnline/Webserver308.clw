

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER308.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER048.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER303.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER310.INC'),ONCE        !Req'd for module callout resolution
                     END


FormStockAuditProcess PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locStockType         STRING(30)                            !
locIMEINumber        STRING(30)                            !
locInStock           LONG                                  !
locCounted           LONG                                  !
locVariance          LONG                                  !
locStatus            STRING(60)                            !
locBrowseType        STRING(1)                             !
locManufacturer      STRING(30)                            !
locScanPart          STRING(30)                            !
                    MAP
CompleteStockType	PROCEDURE()
LoanAuditProcessIMEI    PROCEDURE()
ScanningStockPart       PROCEDURE()
ScanningPartFinish      PROCEDURE()
UpdateCounted		PROCEDURE(LONG pNumber)
                    END ! MAP
FilesOpened     Long
STOAUDIT::State  USHORT
GENSHORT::State  USHORT
STOCK::State  USHORT
STMASAUD::State  USHORT
MANUFACT::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('FormStockAuditProcess')
  loc:formname = 'FormStockAuditProcess_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormStockAuditProcess',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormStockAuditProcess','')
    p_web._DivHeader('FormStockAuditProcess',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormStockAuditProcess',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockAuditProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockAuditProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormStockAuditProcess',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockAuditProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormStockAuditProcess',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormStockAuditProcess',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(STOAUDIT)
  p_web._OpenFile(GENSHORT)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(STMASAUD)
  p_web._OpenFile(MANUFACT)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOAUDIT)
  p_Web._CloseFile(GENSHORT)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(STMASAUD)
  p_Web._CloseFile(MANUFACT)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormStockAuditProcess_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'locManufacturer'
    p_web.setsessionvalue('showtab_FormStockAuditProcess',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MANUFACT)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.sto:Description')
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locStatus',locStatus)
  p_web.SetSessionValue('locManufacturer',locManufacturer)
  p_web.SetSessionValue('sto:Description',sto:Description)
  p_web.SetSessionValue('sto:Part_Number',sto:Part_Number)
  p_web.SetSessionValue('sto:Shelf_Location',sto:Shelf_Location)
  p_web.SetSessionValue('sto:Second_Location',sto:Second_Location)
  p_web.SetSessionValue('stoa:Original_Level',stoa:Original_Level)
  p_web.SetSessionValue('locCounted',locCounted)
  p_web.SetSessionValue('locVariance',locVariance)
  p_web.SetSessionValue('locScanPart',locScanPart)
  p_web.SetSessionValue('locBrowseType',locBrowseType)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locStatus')
    locStatus = p_web.GetValue('locStatus')
    p_web.SetSessionValue('locStatus',locStatus)
  End
  if p_web.IfExistsValue('locManufacturer')
    locManufacturer = p_web.GetValue('locManufacturer')
    p_web.SetSessionValue('locManufacturer',locManufacturer)
  End
  if p_web.IfExistsValue('sto:Description')
    sto:Description = p_web.GetValue('sto:Description')
    p_web.SetSessionValue('sto:Description',sto:Description)
  End
  if p_web.IfExistsValue('sto:Part_Number')
    sto:Part_Number = p_web.GetValue('sto:Part_Number')
    p_web.SetSessionValue('sto:Part_Number',sto:Part_Number)
  End
  if p_web.IfExistsValue('sto:Shelf_Location')
    sto:Shelf_Location = p_web.GetValue('sto:Shelf_Location')
    p_web.SetSessionValue('sto:Shelf_Location',sto:Shelf_Location)
  End
  if p_web.IfExistsValue('sto:Second_Location')
    sto:Second_Location = p_web.GetValue('sto:Second_Location')
    p_web.SetSessionValue('sto:Second_Location',sto:Second_Location)
  End
  if p_web.IfExistsValue('stoa:Original_Level')
    stoa:Original_Level = p_web.GetValue('stoa:Original_Level')
    p_web.SetSessionValue('stoa:Original_Level',stoa:Original_Level)
  End
  if p_web.IfExistsValue('locCounted')
    locCounted = p_web.GetValue('locCounted')
    p_web.SetSessionValue('locCounted',locCounted)
  End
  if p_web.IfExistsValue('locVariance')
    locVariance = p_web.GetValue('locVariance')
    p_web.SetSessionValue('locVariance',locVariance)
  End
  if p_web.IfExistsValue('locScanPart')
    locScanPart = p_web.GetValue('locScanPart')
    p_web.SetSessionValue('locScanPart',locScanPart)
  End
  if p_web.IfExistsValue('locBrowseType')
    locBrowseType = p_web.GetValue('locBrowseType')
    p_web.SetSessionValue('locBrowseType',locBrowseType)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormStockAuditProcess_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    IF (p_web.IfExistsValue('stom:Audit_No'))
        Access:STMASAUD.ClearKey(stom:AutoIncrement_Key)
        stom:Audit_No = p_web.GetValue('stom:Audit_No')
        IF (Access:STMASAUD.TryFetch(stom:AutoIncrement_Key) = Level:Benign)
            p_web.FileToSessionQueue(STMASAUD)
        ELSE ! IF
        END ! IF
    END ! IF
    
    IF (p_web.IfExistsValue('AuditPart'))
        p_web.StoreValue('AuditPart')
        Access:STOAUDIT.ClearKey(stoa:Internal_AutoNumber_Key)
        stoa:Internal_AutoNumber = p_web.GetValue('stoa:Internal_AutoNumber')
        IF (Access:STOAUDIT.TryFetch(stoa:Internal_AutoNumber_Key))
        END !I F
        p_web.FileToSessionQueue(STOAUDIT)
        
        Access:STOCK.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = p_web.GSV('stoa:Stock_Ref_No')
        IF (Access:STOCK.TryFetch(sto:Ref_Number_Key))
        END ! IF
        p_web.FileToSessionQueue(STOCK)
    ELSE
        p_web.DeleteSessionValue('AuditPart')
        p_web.SSV('sto:Part_Number','')
        p_web.SSV('sto:Description','')
        p_web.SSV('sto:Shelf_Location','')
        p_web.SSV('sto:Second_Location','')
  
        p_web.SSV('locCounted','')
        p_web.SSV('locVariance','')
        p_web.SSV('stoa:Original_Level','')
    END ! IF
    
    UpdateCounted(99) ! Clear Count
    
    
    
    IF (NOT(INLIST(p_web.GSV('locBrowseType'),'Y','N')))
        p_web.SSV('locBrowseType','N')
    END ! IF
    
    p_web.SSV('Hide:FinishScan',1)
    p_web.SSV('Hide:CompleteAudit',0)
    p_web.SSV('Hide:ContinueAudit',0)
    p_web.SSV('Hide:SuspendAudit',0)
    p_web.SSV('Hide:StockUpdateButtons',1)
    p_web.SSV('ReadOnly:ScanningField',1)
    
    IF (p_web.GSV('stom:Audit_User') <> p_web.GSV('BookingUserCode'))
        p_web.SSV('locStatus','AUDIT JOINED')
        p_web.SSV('Hide:ContinueAudit',1)
        p_web.SSV('Hide:SuspendAudit',1)
        p_web.SSV('Hide:FinishScan',0)
        p_web.SSV('Hide:CompleteAudit',1)
        p_web.SSV('ReadOnly:ScanningField',0)
    ELSE
        CASE p_web.GSV('stom:Send')
        OF 'S'
            p_web.SSV('locStatus','AUDIT SUSPENDED')
            p_web.SSV('Hide:ContinueAudit',0)
            p_web.SSV('Hide:SuspendAudit',1)
  		
        OF 'T'
            p_web.SSV('locStatus','TEMP COMPLETED')
            p_web.SSV('Hide:ContinueAudit',1)
            p_web.SSV('Hide:SuspendAudit',1)
            p_web.SSV('Hide:StockUpdateButtons',0)
        ELSE
            p_web.SSV('locStatus','AUDIT IN PROGRESS')
            p_web.SSV('Hide:ContinueAudit',1)
            p_web.SSV('Hide:SuspendAudit',0)
            p_web.SSV('Hide:StockUpdateButtons',0)
            p_web.SSV('ReadOnly:ScanningField',0)
        END ! CASE
    END ! IF
  
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locStatus = p_web.RestoreValue('locStatus')
 locManufacturer = p_web.RestoreValue('locManufacturer')
 locCounted = p_web.RestoreValue('locCounted')
 locVariance = p_web.RestoreValue('locVariance')
 locScanPart = p_web.RestoreValue('locScanPart')
 locBrowseType = p_web.RestoreValue('locBrowseType')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormStockAuditProcess')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormStockAuditProcess_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormStockAuditProcess_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormStockAuditProcess_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'FormStockAudits'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormStockAuditProcess" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormStockAuditProcess" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormStockAuditProcess" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Stock Audit Procedure') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Stock Audit Procedure',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormStockAuditProcess">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormStockAuditProcess" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormStockAuditProcess')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Stock Audit Procedure') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Actions') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormStockAuditProcess')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormStockAuditProcess'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='MANUFACT'
        If p_web.GSV('stom:CompleteType') = 'S'
            p_web.SetValue('SelectField',clip(loc:formname) & '.locScanPart')
        End
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locManufacturer')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormStockAuditProcess')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
    do SendPacket
    Do spacer
    do SendPacket
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Stock Audit Procedure') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormStockAuditProcess_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Stock Audit Procedure')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Stock Audit Procedure')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Stock Audit Procedure')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Stock Audit Procedure')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locStatus
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('stom:CompleteType') <> 'S'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locManufacturer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('stom:CompleteType') <> 'S'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Description
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sto:Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('stom:CompleteType') <> 'S'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Part_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Part_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sto:Part_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('stom:CompleteType') <> 'S'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Shelf_Location
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Shelf_Location
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sto:Shelf_Location
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('stom:CompleteType') <> 'S'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Second_Location
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Second_Location
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sto:Second_Location
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('stom:CompleteType') <> 'S'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locInStock
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locInStock
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locInStock
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('stom:CompleteType') <> 'S'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCounted
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCounted
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCounted
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('stom:CompleteType') <> 'S'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locVariance
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locVariance
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locVariance
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('stom:CompleteType') <> 'S' AND p_web.GSV('AuditPart') = 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btn1
      do Comment::btn1
    end
    do SendPacket
    If p_web.GSV('stom:CompleteType') <> 'S' AND p_web.GSV('AuditPart') = 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btn2
      do Comment::btn2
    end
    do SendPacket
    If p_web.GSV('stom:CompleteType') <> 'S' AND p_web.GSV('AuditPart') = 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btn3
      do Comment::btn3
    end
    do SendPacket
    If p_web.GSV('stom:CompleteType') <> 'S' AND p_web.GSV('AuditPart') = 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btn4
      do Comment::btn4
    end
    do SendPacket
    If p_web.GSV('stom:CompleteType') <> 'S' AND p_web.GSV('AuditPart') = 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btn5
      do Comment::btn5
    end
    do SendPacket
    If p_web.GSV('stom:CompleteType') <> 'S' AND p_web.GSV('AuditPart') = 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btn6
      do Comment::btn6
    end
    do SendPacket
    If p_web.GSV('stom:CompleteType') <> 'S' AND p_web.GSV('AuditPart') = 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btn7
      do Comment::btn7
    end
    do SendPacket
    If p_web.GSV('stom:CompleteType') <> 'S' AND p_web.GSV('AuditPart') = 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btn8
      do Comment::btn8
    end
    do SendPacket
    If p_web.GSV('stom:CompleteType') <> 'S' AND p_web.GSV('AuditPart') = 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btn9
      do Comment::btn9
    end
    do SendPacket
    If p_web.GSV('stom:CompleteType') <> 'S' AND p_web.GSV('AuditPart') = 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btn0
      do Comment::btn0
    end
    do SendPacket
    If p_web.GSV('stom:CompleteType') <> 'S' AND p_web.GSV('AuditPart') = 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnX
      do Comment::btnX
    end
    do SendPacket
    If p_web.GSV('stom:CompleteType') <> 'S' AND p_web.GSV('AuditPart') = 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnAccept
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnAccept
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('stom:CompleteType') = 'S'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locScanPart
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locScanPart
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locScanPart
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('stom:CompleteType') = 'S'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::btnProceed
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnProceed
      do Comment::btnProceed
    end
    do SendPacket
    If p_web.GSV('stom:CompleteType') = 'S'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnCancel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnCancel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::___line
      do Comment::___line
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtAuditPart
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::txtAuditPart
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locBrowseType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locBrowseType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locBrowseType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwAudit
      do Comment::brwAudit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Actions') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormStockAuditProcess_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnCompleteAudit
      do Comment::btnCompleteAudit
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnSuspendAudit
      do Comment::btnSuspendAudit
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnContinueAudit
      do Comment::btnContinueAudit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locStatus  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('locStatus') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locStatus',p_web.GetValue('NewValue'))
    locStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locStatus
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locStatus',p_web.GetValue('Value'))
    locStatus = p_web.GetValue('Value')
  End

Value::locStatus  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('locStatus') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locStatus
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold LargeText')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locStatus'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('locStatus') & '_value')

Comment::locStatus  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('locStatus') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locManufacturer  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('locManufacturer') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Manufacturer')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('locManufacturer') & '_prompt')

Validate::locManufacturer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locManufacturer',p_web.GetValue('NewValue'))
    locManufacturer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locManufacturer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locManufacturer',p_web.GetValue('Value'))
    locManufacturer = p_web.GetValue('Value')
  End
    locManufacturer = Upper(locManufacturer)
    p_web.SetSessionValue('locManufacturer',locManufacturer)
  p_Web.SetValue('lookupfield','locManufacturer')
  do AfterLookup
  do Value::locManufacturer
  do SendAlert
  do Comment::locManufacturer

Value::locManufacturer  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('locManufacturer') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locManufacturer
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('stom:Send') = 'S','readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('stom:Send') = 'S'
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('locManufacturer')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locManufacturer'',''formstockauditprocess_locmanufacturer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locManufacturer')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','locManufacturer',p_web.GetSessionValueFormat('locManufacturer'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('SelectManufacturers?LookupField=locManufacturer&Tab=1&ForeignField=man:Manufacturer&_sort=man:Manufacturer&Refresh=sort&LookupFrom=FormStockAuditProcess&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('locManufacturer') & '_value')

Comment::locManufacturer  Routine
      loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('locManufacturer') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('locManufacturer') & '_comment')

Prompt::sto:Description  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('sto:Description') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Description:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Description  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Description',p_web.GetValue('NewValue'))
    sto:Description = p_web.GetValue('NewValue') !FieldType= STRING Field = sto:Description
    do Value::sto:Description
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Description',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sto:Description = p_web.GetValue('Value')
  End

Value::sto:Description  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('sto:Description') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:Description
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Description'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::sto:Description  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('sto:Description') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sto:Part_Number  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('sto:Part_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Part Number:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Part_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Part_Number',p_web.GetValue('NewValue'))
    sto:Part_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = sto:Part_Number
    do Value::sto:Part_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Part_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sto:Part_Number = p_web.GetValue('Value')
  End

Value::sto:Part_Number  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('sto:Part_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:Part_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Part_Number'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('sto:Part_Number') & '_value')

Comment::sto:Part_Number  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('sto:Part_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sto:Shelf_Location  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('sto:Shelf_Location') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Shelf Location:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Shelf_Location  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Shelf_Location',p_web.GetValue('NewValue'))
    sto:Shelf_Location = p_web.GetValue('NewValue') !FieldType= STRING Field = sto:Shelf_Location
    do Value::sto:Shelf_Location
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Shelf_Location',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sto:Shelf_Location = p_web.GetValue('Value')
  End

Value::sto:Shelf_Location  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('sto:Shelf_Location') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:Shelf_Location
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Shelf_Location'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::sto:Shelf_Location  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('sto:Shelf_Location') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sto:Second_Location  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('sto:Second_Location') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('2nd Location:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Second_Location  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Second_Location',p_web.GetValue('NewValue'))
    sto:Second_Location = p_web.GetValue('NewValue') !FieldType= STRING Field = sto:Second_Location
    do Value::sto:Second_Location
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Second_Location',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sto:Second_Location = p_web.GetValue('Value')
  End

Value::sto:Second_Location  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('sto:Second_Location') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:Second_Location
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Second_Location'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::sto:Second_Location  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('sto:Second_Location') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locInStock  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('locInStock') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('In Stock:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locInStock  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locInStock',p_web.GetValue('NewValue'))
    stoa:Original_Level = p_web.GetValue('NewValue') !FieldType= LONG Field = stoa:Original_Level
    do Value::locInStock
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('stoa:Original_Level',p_web.dFormat(p_web.GetValue('Value'),'@n-14'))
    stoa:Original_Level = p_web.GetValue('Value')
  End

Value::locInStock  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('locInStock') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- stoa:Original_Level
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold LargeText')&'">' & p_web._jsok(p_web.GetSessionValueFormat('stoa:Original_Level'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('locInStock') & '_value')

Comment::locInStock  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('locInStock') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locCounted  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('locCounted') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Counted:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locCounted  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCounted',p_web.GetValue('NewValue'))
    locCounted = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCounted
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCounted',p_web.GetValue('Value'))
    locCounted = p_web.GetValue('Value')
  End

Value::locCounted  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('locCounted') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locCounted
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('bold LargeText')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locCounted'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('locCounted') & '_value')

Comment::locCounted  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('locCounted') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locVariance  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('locVariance') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Variance:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locVariance  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locVariance',p_web.GetValue('NewValue'))
    locVariance = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locVariance
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locVariance',p_web.GetValue('Value'))
    locVariance = p_web.GetValue('Value')
  End

Value::locVariance  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('locVariance') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locVariance
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('red bold LargeText')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locVariance'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('locVariance') & '_value')

Comment::locVariance  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('locVariance') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btn1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btn1',p_web.GetValue('NewValue'))
    do Value::btn1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  UpdateCounted(1)    
  do SendAlert
  do Value::locCounted  !1
  do Value::locInStock  !1
  do Value::locVariance  !1

Value::btn1  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btn1') & '_value',Choose(p_web.GSV('stom:Send') = 'S','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('stom:Send') = 'S')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btn1'',''formstockauditprocess_btn1_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btn1','1','button-flat',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('btn1') & '_value')

Comment::btn1  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btn1') & '_comment',Choose(p_web.GSV('stom:Send') = 'S','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('stom:Send') = 'S'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btn2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btn2',p_web.GetValue('NewValue'))
    do Value::btn2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  UpdateCounted(2)    
  do SendAlert
  do Value::locCounted  !1
  do Value::locInStock  !1
  do Value::locVariance  !1

Value::btn2  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btn2') & '_value',Choose(p_web.GSV('stom:Send') = 'S','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('stom:Send') = 'S')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btn2'',''formstockauditprocess_btn2_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btn2','2','button-flat',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('btn2') & '_value')

Comment::btn2  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btn2') & '_comment',Choose(p_web.GSV('stom:Send') = 'S','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('stom:Send') = 'S'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btn3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btn3',p_web.GetValue('NewValue'))
    do Value::btn3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  UpdateCounted(3)    
  do SendAlert
  do Value::locCounted  !1
  do Value::locInStock  !1
  do Value::locVariance  !1

Value::btn3  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btn3') & '_value',Choose(p_web.GSV('stom:Send') = 'S','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('stom:Send') = 'S')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btn3'',''formstockauditprocess_btn3_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btn3','3','button-flat',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('btn3') & '_value')

Comment::btn3  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btn3') & '_comment',Choose(p_web.GSV('stom:Send') = 'S','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('stom:Send') = 'S'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btn4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btn4',p_web.GetValue('NewValue'))
    do Value::btn4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  UpdateCounted(4)    
  do SendAlert
  do Value::locCounted  !1
  do Value::locVariance  !1
  do Value::locInStock  !1

Value::btn4  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btn4') & '_value',Choose(p_web.GSV('stom:Send') = 'S','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('stom:Send') = 'S')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btn4'',''formstockauditprocess_btn4_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btn4','4','button-flat',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('btn4') & '_value')

Comment::btn4  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btn4') & '_comment',Choose(p_web.GSV('stom:Send') = 'S','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('stom:Send') = 'S'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btn5  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btn5',p_web.GetValue('NewValue'))
    do Value::btn5
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  UpdateCounted(5)    
  do SendAlert
  do Value::locInStock  !1
  do Value::locCounted  !1
  do Value::locVariance  !1

Value::btn5  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btn5') & '_value',Choose(p_web.GSV('stom:Send') = 'S','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('stom:Send') = 'S')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btn5'',''formstockauditprocess_btn5_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btn5','5','button-flat',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('btn5') & '_value')

Comment::btn5  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btn5') & '_comment',Choose(p_web.GSV('stom:Send') = 'S','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('stom:Send') = 'S'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btn6  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btn6',p_web.GetValue('NewValue'))
    do Value::btn6
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  UpdateCounted(6)    
  do SendAlert
  do Value::locCounted  !1
  do Value::locInStock  !1
  do Value::locVariance  !1

Value::btn6  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btn6') & '_value',Choose(p_web.GSV('stom:Send') = 'S','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('stom:Send') = 'S')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btn6'',''formstockauditprocess_btn6_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btn6','6','button-flat',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('btn6') & '_value')

Comment::btn6  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btn6') & '_comment',Choose(p_web.GSV('stom:Send') = 'S','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('stom:Send') = 'S'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btn7  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btn7',p_web.GetValue('NewValue'))
    do Value::btn7
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  UpdateCounted(7)    
  do SendAlert
  do Value::locCounted  !1
  do Value::locInStock  !1
  do Value::locVariance  !1

Value::btn7  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btn7') & '_value',Choose(p_web.GSV('stom:Send') = 'S','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('stom:Send') = 'S')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btn7'',''formstockauditprocess_btn7_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btn7','7','button-flat',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('btn7') & '_value')

Comment::btn7  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btn7') & '_comment',Choose(p_web.GSV('stom:Send') = 'S','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('stom:Send') = 'S'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btn8  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btn8',p_web.GetValue('NewValue'))
    do Value::btn8
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  UpdateCounted(8)    
  do SendAlert
  do Value::locCounted  !1
  do Value::locInStock  !1
  do Value::locVariance  !1

Value::btn8  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btn8') & '_value',Choose(p_web.GSV('stom:Send') = 'S','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('stom:Send') = 'S')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btn8'',''formstockauditprocess_btn8_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btn8','8','button-flat',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('btn8') & '_value')

Comment::btn8  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btn8') & '_comment',Choose(p_web.GSV('stom:Send') = 'S','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('stom:Send') = 'S'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btn9  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btn9',p_web.GetValue('NewValue'))
    do Value::btn9
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  UpdateCounted(0)    
  do SendAlert
  do Value::locCounted  !1
  do Value::locInStock  !1
  do Value::locVariance  !1

Value::btn9  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btn9') & '_value',Choose(p_web.GSV('stom:Send') = 'S','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('stom:Send') = 'S')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btn9'',''formstockauditprocess_btn9_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btn9','9','button-flat',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('btn9') & '_value')

Comment::btn9  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btn9') & '_comment',Choose(p_web.GSV('stom:Send') = 'S','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('stom:Send') = 'S'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btn0  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btn0',p_web.GetValue('NewValue'))
    do Value::btn0
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  UpdateCounted(0)    
  do SendAlert
  do Value::locCounted  !1
  do Value::locInStock  !1
  do Value::locVariance  !1

Value::btn0  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btn0') & '_value',Choose(p_web.GSV('stom:Send') = 'S','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('stom:Send') = 'S')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btn0'',''formstockauditprocess_btn0_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btn0','0','button-flat',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('btn0') & '_value')

Comment::btn0  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btn0') & '_comment',Choose(p_web.GSV('stom:Send') = 'S','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('stom:Send') = 'S'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnX  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnX',p_web.GetValue('NewValue'))
    do Value::btnX
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  UpdateCounted(99)    
  do SendAlert
  do Value::locCounted  !1
  do Value::locInStock  !1
  do Value::locVariance  !1

Value::btnX  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btnX') & '_value',Choose(p_web.GSV('stom:Send') = 'S','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('stom:Send') = 'S')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnX'',''formstockauditprocess_btnx_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnX','Clear','button-flat',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('btnX') & '_value')

Comment::btnX  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btnX') & '_comment',Choose(p_web.GSV('stom:Send') = 'S','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('stom:Send') = 'S'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnAccept  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnAccept',p_web.GetValue('NewValue'))
    do Value::btnAccept
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnAccept  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btnAccept') & '_value',Choose(p_web.GSV('stom:Send') = 'S','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('stom:Send') = 'S')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnAccept','Accept Values','button-flat',loc:formname,,,'window.open('''& p_web._MakeURL(clip('AuditProcess?' &'ProcessType=StockAuditNextPart&okURL=FormStockAuditProcess&errorURL=FormSTockAuditProcess')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('btnAccept') & '_value')

Comment::btnAccept  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btnAccept') & '_comment',Choose(p_web.GSV('stom:Send') = 'S','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('stom:Send') = 'S'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locScanPart  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('locScanPart') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Stock Part Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locScanPart  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locScanPart',p_web.GetValue('NewValue'))
    locScanPart = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locScanPart
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locScanPart',p_web.GetValue('Value'))
    locScanPart = p_web.GetValue('Value')
  End
    locScanPart = Upper(locScanPart)
    p_web.SetSessionValue('locScanPart',locScanPart)
  ScanningStockPart()
  do Value::locScanPart
  do SendAlert
  do Value::btnCancel  !1
  do Value::btnProceed  !1
  do Value::brwAudit  !1

Value::locScanPart  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('locScanPart') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locScanPart
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ScanningField') = 1 OR p_web.GSV('ProceedScan') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:ScanningField') = 1 OR p_web.GSV('ProceedScan') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('locScanPart')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locScanPart'',''formstockauditprocess_locscanpart_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locScanPart')&''',2);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locScanPart',p_web.GetSessionValueFormat('locScanPart'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('locScanPart') & '_value')

Comment::locScanPart  Routine
      loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('locScanPart') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::btnProceed  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btnProceed') & '_prompt',Choose(p_web.GSV('ProceedScan') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('ProceedScan') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::btnProceed  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnProceed',p_web.GetValue('NewValue'))
    do Value::btnProceed
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ScanningPartFinish()
  do SendAlert
  do Value::locScanPart  !1
  do Value::btnProceed  !1
  do Value::btnCancel  !1
  do Value::brwAudit  !1

Value::btnProceed  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btnProceed') & '_value',Choose(p_web.GSV('ProceedScan') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('ProceedScan') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnProceed'',''formstockauditprocess_btnproceed_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnProceed','Proceed','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('btnProceed') & '_value')

Comment::btnProceed  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btnProceed') & '_comment',Choose(p_web.GSV('ProceedScan') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('ProceedScan') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnCancel  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnCancel',p_web.GetValue('NewValue'))
    do Value::btnCancel
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    p_web.SSV('locScanPart','')
    p_web.SSV('ProceedScan',0)
  do SendAlert
  do Value::locScanPart  !1
  do Value::btnProceed  !1
  do Value::btnCancel  !1

Value::btnCancel  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btnCancel') & '_value',Choose(p_web.GSV('ProceedScan') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('ProceedScan') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnCancel'',''formstockauditprocess_btncancel_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnCancel','Cancel','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('btnCancel') & '_value')

Comment::btnCancel  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btnCancel') & '_comment',Choose(p_web.GSV('ProceedScan') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('ProceedScan') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::___line  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('___line',p_web.GetValue('NewValue'))
    do Value::___line
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::___line  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('___line') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::___line  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('___line') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::txtAuditPart  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtAuditPart',p_web.GetValue('NewValue'))
    do Value::txtAuditPart
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtAuditPart  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('txtAuditPart') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web.Translate('Select "Audit Part" to begin Auditing A Part',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::txtAuditPart  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('txtAuditPart') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locBrowseType  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('locBrowseType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locBrowseType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locBrowseType',p_web.GetValue('NewValue'))
    locBrowseType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locBrowseType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locBrowseType',p_web.GetValue('Value'))
    locBrowseType = p_web.GetValue('Value')
  End
  do Value::locBrowseType
  do SendAlert
  do Value::brwAudit  !1

Value::locBrowseType  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('locBrowseType') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locBrowseType
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locBrowseType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locBrowseType') = 'N'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locBrowseType'',''formstockauditprocess_locbrowsetype_value'',1,'''&clip('N')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locBrowseType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locBrowseType',clip('N'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locBrowseType_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Awaiting Audit') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locBrowseType') = 'Y'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locBrowseType'',''formstockauditprocess_locbrowsetype_value'',1,'''&clip('Y')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locBrowseType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locBrowseType',clip('Y'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locBrowseType_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Audited Parts') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('locBrowseType') & '_value')

Comment::locBrowseType  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('locBrowseType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::brwAudit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwAudit',p_web.GetValue('NewValue'))
    do Value::brwAudit
  Else
    p_web.StoreValue('stoa:Internal_AutoNumber')
  End
  do SendAlert
  do Value::sto:Part_Number  !1

Value::brwAudit  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseAuditedStockUnits --
  p_web.SetValue('BrowseAuditedStockUnits:NoForm',1)
  p_web.SetValue('BrowseAuditedStockUnits:FormName',loc:formname)
  p_web.SetValue('BrowseAuditedStockUnits:parentIs','Form')
  p_web.SetValue('_parentProc','FormStockAuditProcess')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormStockAuditProcess_BrowseAuditedStockUnits_embedded_div')&'"><!-- Net:BrowseAuditedStockUnits --></div><13,10>'
    p_web._DivHeader('FormStockAuditProcess_' & lower('BrowseAuditedStockUnits') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormStockAuditProcess_' & lower('BrowseAuditedStockUnits') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseAuditedStockUnits --><13,10>'
  end
  do SendPacket

Comment::brwAudit  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('brwAudit') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnCompleteAudit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnCompleteAudit',p_web.GetValue('NewValue'))
    do Value::btnCompleteAudit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnCompleteAudit  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btnCompleteAudit') & '_value',Choose(p_web.GSV('Hide:CompleteAudit') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CompleteAudit') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('stockCompleteAudit()')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnCompleteAudit','Complete Audit','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('btnCompleteAudit') & '_value')

Comment::btnCompleteAudit  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btnCompleteAudit') & '_comment',Choose(p_web.GSV('Hide:CompleteAudit') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CompleteAudit') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnSuspendAudit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnSuspendAudit',p_web.GetValue('NewValue'))
    do Value::btnSuspendAudit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnSuspendAudit  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btnSuspendAudit') & '_value',Choose(p_web.GSV('Hide:SuspendAudit') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:SuspendAudit') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnSuspendAudit','Suspend Audit','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('AuditProcess?' &'ProcessType=StockSuspendAudit&okURL=FormStockAudits&errorURL=FormStockAuditProcess')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('btnSuspendAudit') & '_value')

Comment::btnSuspendAudit  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btnSuspendAudit') & '_comment',Choose(p_web.GSV('Hide:SuspendAudit') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:SuspendAudit') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnContinueAudit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnContinueAudit',p_web.GetValue('NewValue'))
    do Value::btnContinueAudit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    Access:STMASAUD.ClearKey(stom:AutoIncrement_Key)
    stom:Audit_No = p_web.GSV('stom:Audit_No')
    IF (Access:STMASAUD.TryFetch(stom:AutoIncrement_Key) = Level:Benign)
        p_web.SessionQueueToFile(STMASAUD)
        stom:Send = ''
        Access:STMASAUD.TryUpdate()
        p_web.SSV('stom:Send','')
        p_web.SSV('locStatus','AUDIT IN PROGRESS')
        p_web.SSV('Hide:SuspendAudit',0)        
        p_web.SSV('Hide:ContinueAudit',1)
        IF (p_web.GSV('stom:CompleteType') = 'S')
            p_web.SSV('ReadOnly:ScanningField',0)
            p_web.SSV('Hide:StockUpdateButtons',0)
        END ! IF
    ELSE ! IF
    END ! IF
  				
  do SendAlert
  do Value::btnCompleteAudit  !1
  do Value::btnContinueAudit  !1
  do Value::btnSuspendAudit  !1
  do Value::brwAudit  !1
  do Value::locStatus  !1
  do Value::locScanPart  !1
  do Prompt::locManufacturer
  do Value::locManufacturer  !1
  do Value::btn0  !1
  do Value::btn1  !1
  do Value::btn2  !1
  do Value::btn3  !1
  do Value::btn4  !1
  do Value::btn5  !1
  do Value::btn6  !1
  do Value::btn7  !1
  do Value::btn8  !1
  do Value::btn9  !1
  do Value::btnX  !1
  do Value::btnAccept  !1

Value::btnContinueAudit  Routine
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btnContinueAudit') & '_value',Choose(p_web.GSV('Hide:ContinueAudit') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ContinueAudit') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnContinueAudit'',''formstockauditprocess_btncontinueaudit_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnContinueAudit','Continue Audit','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockAuditProcess_' & p_web._nocolon('btnContinueAudit') & '_value')

Comment::btnContinueAudit  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockAuditProcess_' & p_web._nocolon('btnContinueAudit') & '_comment',Choose(p_web.GSV('Hide:ContinueAudit') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ContinueAudit') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormStockAuditProcess_locManufacturer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locManufacturer
      else
        do Value::locManufacturer
      end
  of lower('FormStockAuditProcess_btn1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btn1
      else
        do Value::btn1
      end
  of lower('FormStockAuditProcess_btn2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btn2
      else
        do Value::btn2
      end
  of lower('FormStockAuditProcess_btn3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btn3
      else
        do Value::btn3
      end
  of lower('FormStockAuditProcess_btn4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btn4
      else
        do Value::btn4
      end
  of lower('FormStockAuditProcess_btn5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btn5
      else
        do Value::btn5
      end
  of lower('FormStockAuditProcess_btn6_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btn6
      else
        do Value::btn6
      end
  of lower('FormStockAuditProcess_btn7_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btn7
      else
        do Value::btn7
      end
  of lower('FormStockAuditProcess_btn8_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btn8
      else
        do Value::btn8
      end
  of lower('FormStockAuditProcess_btn9_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btn9
      else
        do Value::btn9
      end
  of lower('FormStockAuditProcess_btn0_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btn0
      else
        do Value::btn0
      end
  of lower('FormStockAuditProcess_btnX_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnX
      else
        do Value::btnX
      end
  of lower('FormStockAuditProcess_locScanPart_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locScanPart
      else
        do Value::locScanPart
      end
  of lower('FormStockAuditProcess_btnProceed_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnProceed
      else
        do Value::btnProceed
      end
  of lower('FormStockAuditProcess_btnCancel_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnCancel
      else
        do Value::btnCancel
      end
  of lower('FormStockAuditProcess_locBrowseType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locBrowseType
      else
        do Value::locBrowseType
      end
  of lower('FormStockAuditProcess_BrowseAuditedStockUnits_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::brwAudit
      else
        do Value::brwAudit
      end
  of lower('FormStockAuditProcess_btnContinueAudit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnContinueAudit
      else
        do Value::btnContinueAudit
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormStockAuditProcess_form:ready_',1)
  p_web.SetSessionValue('FormStockAuditProcess_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormStockAuditProcess',0)

PreCopy  Routine
  p_web.SetValue('FormStockAuditProcess_form:ready_',1)
  p_web.SetSessionValue('FormStockAuditProcess_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormStockAuditProcess',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormStockAuditProcess_form:ready_',1)
  p_web.SetSessionValue('FormStockAuditProcess_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormStockAuditProcess:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormStockAuditProcess_form:ready_',1)
  p_web.SetSessionValue('FormStockAuditProcess_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormStockAuditProcess:Primed',0)
  p_web.setsessionvalue('showtab_FormStockAuditProcess',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormStockAuditProcess_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormStockAuditProcess_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
    If p_web.GSV('stom:CompleteType') <> 'S'
          locManufacturer = Upper(locManufacturer)
          p_web.SetSessionValue('locManufacturer',locManufacturer)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('stom:CompleteType') = 'S'
          locScanPart = Upper(locScanPart)
          p_web.SetSessionValue('locScanPart',locScanPart)
        If loc:Invalid <> '' then exit.
    End
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormStockAuditProcess:Primed',0)
  p_web.StoreValue('locStatus')
  p_web.StoreValue('locManufacturer')
  p_web.StoreValue('sto:Description')
  p_web.StoreValue('sto:Part_Number')
  p_web.StoreValue('sto:Shelf_Location')
  p_web.StoreValue('sto:Second_Location')
  p_web.StoreValue('stoa:Original_Level')
  p_web.StoreValue('locCounted')
  p_web.StoreValue('locVariance')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locScanPart')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locBrowseType')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
spacer  Routine
  packet = clip(packet) & |
    '<<br/><13,10>'&|
    '<<br/><13,10>'&|
    '<<br/><13,10>'&|
    '<<br/><13,10>'&|
    ''
CompleteStockType	PROCEDURE()
	CODE
		Access:LOANALC.ClearKey(lac1:Locate_Key)
		lac1:Audit_Number = p_web.GSV('lmf:Audit_Number')
		lac1:Location = p_web.GSV('BookingSiteLocation')
		lac1:Stock_Type = p_web.GSV('locStockType')
		IF (Access:LOANALC.TryFetch(lac1:Locate_Key) = Level:Benign)
			IF (lac1:Confirmed = 0)
				lac1:Confirmed = 1
				Access:LOANALC.TryUpdate()
				p_web.SSV('locStatus','STOCK TYPE AUDIT COMPLETE')
			ELSE ! IF
				loc:alert = 'This stock type has already been completed. You cannot complete it again.'
				loc:invalid = 'btnCompleteStockType'
				RETURN
			END ! IF
		ELSE ! IF
			loc:alert = 'Stock Type ' & p_web.GSV('locStockType') & ' is not currently being audited. Therefore you cannot complete it.'
			loc:invalid = 'btnCompleteStockType'
			RETURN
		END ! IF
LoanAuditProcessIMEI	PROCEDURE()
	CODE
		IF (p_web.GSV('locIMEINumber') = '')
			RETURN
		END ! IF
		Access:LOANAUI.ClearKey(lau:Locate_IMEI_Key)
		lau:Audit_Number = p_web.GSV('lmf:Audit_Number')
		lau:Site_Location = p_web.GSV('BookingSiteLocation')
		lau:IMEI_Number = p_web.GSV('locIMEINumber')
		IF (Access:LOANAUI.TryFetch(lau:Locate_IMEI_Key) = Level:Benign)
			IF (lau:Stock_Type <> p_web.GSV('locStockType'))
				loc:alert = 'This unit exists in a different loan/stock type. Please ensure it is in the correct location.'
				loc:Invalid = 'locIMEINumber'
				RETURN
			ELSE ! IF
				IF (lau:Confirmed = 1)
					loc:alert = 'The selected I.M.E.I. Number has already been scanned.'
					loc:Invalid = 'locIMEINumber'
				RETURN
				ELSE ! IF
					lau:Confirmed = 1
					p_web.SSV('locCounted',p_web.GSV('locCounted') + 1)
					Access:LOANAUI.TryUpdate()
					p_web.SSV('locIMEINumber','')
				END ! IF
			END ! IF
		ELSE ! IF
 
			loc:alert = 'This I.M.E.I. number is not recognized in the exchange or loan databases..'
				loc:Invalid = 'locIMEINumber'
				RETURN
		END ! IF


ScanningPartFinish 	PROCEDURE()
	CODE
		IF (p_web.GSV('locScanPart') = '')
			RETURN
		END ! IF
	
		Access:STOCK.ClearKey(sto:Location_Key)
		sto:Location = p_web.GSV('BookingSiteLocation')
		sto:Part_Number = p_web.GSV('locScanPart')
		IF (Access:STOCK.TryFetch(sto:Location_Key))
			! ERror checking should of already been done
			RETURN
		END ! IF
		
		Access:STOAUDIT.ClearKey(stoa:Lock_Down_Key)
		stoa:Audit_Ref_No = p_web.GSV('stom:Audit_No')
		stoa:Stock_Ref_No = sto:Ref_Number
		IF (Access:STOAUDIT.TryFetch(stoa:Lock_Down_Key))
			! ERror checking should of already been done
			RETURN
		END ! IF
		
		stoa:New_Level += 1
		stoa:Confirmed = 'Y'
		Access:STOAUDIT.TryUpdate()
		
		IF (stoa:New_Level = stoa:Original_Level)
			Access:GENSHORT.ClearKey(gens:Lock_Down_Key)
			gens:Audit_No = stoa:Audit_Ref_No
			gens:Stock_Ref_No = sto:Ref_Number
			IF (Access:GENSHORT.TryFetch(gens:Lock_Down_Key) = Level:Benign)
				Access:GENSHORT.DeleteREcord(0)
			ELSE ! IF
			END ! IF
		ELSE  !IF
			Access:GENSHORT.ClearKey(gens:Lock_Down_Key)
			gens:Audit_No = stoa:Audit_Ref_No
			gens:Stock_Ref_No = sto:Ref_Number
			IF (Access:GENSHORT.TryFetch(gens:Lock_Down_Key) = Level:Benign)
				
			ELSE ! IF
				IF (Access:GENSHORT.PrimeRecord() = Level:Benign)
					gens:Audit_No = stoa:Audit_Ref_No
					gens:Stock_Ref_No = sto:Ref_Number
					IF (Access:GENSHORT.TryInsert())
						Access:GENSHORT.CancelAutoInc()
					END ! IF
				END ! IF
			END ! IF
			gens:Stock_Qty	 = stoa:New_Level - stoa:Original_Level
			Access:GENSHORT.TryUpdate()
		END ! IF
		
        p_web.SSV('ProceedScan',0)
        p_web.SSV('locScanPart','')
ScanningStockPart		PROCEDURE()
userOK		LONG(0)
	CODE
		p_web.SSV('ProceedScan',0)
	
		IF (p_web.GSV('locScanPart') = '')
			RETURN
		END ! IF
		
		Access:STOCK.ClearKey(sto:Location_Key)
		sto:Location = p_web.GSV('BookingSiteLocation')
		sto:Part_Number = p_web.GSV('locScanPart')
		IF (Access:STOCK.TryFetch(sto:Location_Key))
			loc:Alert = 'Stock record not found for this Part number at this location.'
			loc:invalid = 'locScanPart'
			RETURN
		END ! IF
		
		Access:STOAUDIT.ClearKey(stoa:Lock_Down_Key)
		stoa:Audit_Ref_No = p_web.GSV('stom:Audit_No')
		stoa:Stock_Ref_No = sto:Ref_Number
		IF (Access:STOAUDIT.TryFetch(stoa:Lock_Down_Key))
		    loc:Alert = 'This part number does not appear in this audit.'
			loc:invalid = 'locScanPart'
			RETURN
		END ! IF
		
		! Does user have permissin to scan this item?
		userOk = 2
		Access:STOAUUSE.ClearKey(stou:KeyTypeNoPart)
		stou:AuditType = 'S'
		stou:Audit_No = p_web.GSV('stom:Audit_No')
		stou:StockPartNumber = p_web.GSV('locScanPart')
		SET(stou:KeyTypeNoPart,stou:KeyTypeNoPart)
		LOOP UNTIL Access:STOAUUSE.Next() <> Level:Benign
			IF (stou:AuditType <> 'S' OR |
				stou:Audit_No <> p_web.GSV('stom:Audit_No') OR |
				stou:StockPartNumber <> p_web.GSV('locScanPart'))
				BREAK
			END ! IF
			
			IF (stou:User_Code = p_web.GSV('BookingUserCode'))
				userOK = 1
				BREAK
			ELSE
				userOK = 0
			END ! 
			
		END ! LOOP
		
		CASE userOK
		OF 0
			loc:Alert = 'Another user has begun the Audit on this Part Number already. Click PROCEED to continue.'
			loc:invalid = 'locScanPart'
			p_web.SSV('ProceedScan',1)
			RETURN
		OF 1
			! User already permitted
			ScanningPartFinish()
		OF 2
			! No user permite this one
			IF (Access:STOAUUSE.PrimeRecord() = Level:Benign)
				STOU:AuditType       = 'S'
				STOU:Audit_No        = p_web.GSV('stom:Audit_No')
				STOU:User_code       = p_web.GSV('BookingUserCode')
				STOU:Current         = 'Y'
				STOU:StockPartNumber = p_web.GSV('locScanPart')				
				IF (Access:STOAUUSE.TryInsert())
					Access:STOAUUSE.CancelAutoInc()
				END ! IF
				ScanningPartFinish()
			END ! IF
		END ! CASE
UpdateCounted		PROCEDURE(LONG pNumber)
	CODE
	IF (pNumber = 99)
		! Cancel
		p_web.SSV('locCounted','')
	ELSE ! IF
		p_web.SSV('locCounted',p_web.GSV('locCounted') & pNumber)
		p_web.SSV('locVariance',p_web.GSV('locCounted') - p_web.GSV('stoa:Original_Level'))
	END !I F
		
		
		
		
		
		
