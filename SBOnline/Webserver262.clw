

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER262.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER013.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER263.INC'),ONCE        !Req'd for module callout resolution
                     END


RapEngUpdBrowseJobs  PROCEDURE  (NetWebServerWorker p_web)
locJobNumber         STRING(60)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(SBO_GenericFile)
                      Project(sbogen:RecordNumber)
                      Join(JOBS,'(job:Ref_Number = sbogen:Long1)'),INNER      !Compile errors? Make sure you've got a key for both files in the relation:  <=> JOBS
                        Project(job:date_booked)
                        Project(job:time_booked)
                        Project(job:Current_Status)
                      END
                      Join(WEBJOB,'(wob:RefNumber = sbogen:Long1)'),INNER      !Compile errors? Make sure you've got a key for both files in the relation:  <=> WEBJOB
                        Project(wob:RecordNumber)
                      END
                      Join(JOBNOTES,'(jbn:RefNumber = sbogen:Long1)')      !Compile errors? Make sure you've got a key for both files in the relation:  <=> JOBNOTES
                        Project(jbn:Fault_Description)
                      END
                      Join(JOBSE,'(jobe:RefNumber = sbogen:Long1)'),INNER      !Compile errors? Make sure you've got a key for both files in the relation:  <=> JOBSE
                        Project(jobe:SkillLevel)
                      END
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
TagFile::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('RapEngUpdBrowseJobs')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('RapEngUpdBrowseJobs:NoForm')
      loc:NoForm = p_web.GetValue('RapEngUpdBrowseJobs:NoForm')
      loc:FormName = p_web.GetValue('RapEngUpdBrowseJobs:FormName')
    else
      loc:FormName = 'RapEngUpdBrowseJobs_frm'
    End
    p_web.SSV('RapEngUpdBrowseJobs:NoForm',loc:NoForm)
    p_web.SSV('RapEngUpdBrowseJobs:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('RapEngUpdBrowseJobs:NoForm')
    loc:FormName = p_web.GSV('RapEngUpdBrowseJobs:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('RapEngUpdBrowseJobs') & '_' & lower(loc:parent)
  else
    loc:divname = lower('RapEngUpdBrowseJobs')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(SBO_GenericFile,sbogen:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'TAG:TAGGED') then p_web.SetValue('RapEngUpdBrowseJobs_sort','7')
    ElsIf (loc:vorder = 'LOCJOBNUMBER') then p_web.SetValue('RapEngUpdBrowseJobs_sort','1')
    ElsIf (loc:vorder = 'SBOGEN:LONG1') then p_web.SetValue('RapEngUpdBrowseJobs_sort','1')
    ElsIf (loc:vorder = 'JOB:DATE_BOOKED') then p_web.SetValue('RapEngUpdBrowseJobs_sort','2')
    ElsIf (loc:vorder = 'JOB:TIME_BOOKED') then p_web.SetValue('RapEngUpdBrowseJobs_sort','3')
    ElsIf (loc:vorder = 'JBN:FAULT_DESCRIPTION') then p_web.SetValue('RapEngUpdBrowseJobs_sort','4')
    ElsIf (loc:vorder = 'JOB:CURRENT_STATUS') then p_web.SetValue('RapEngUpdBrowseJobs_sort','5')
    ElsIf (loc:vorder = 'JOBE:SKILLLEVEL') then p_web.SetValue('RapEngUpdBrowseJobs_sort','6')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('RapEngUpdBrowseJobs:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('RapEngUpdBrowseJobs:LookupFrom','LookupFrom')
    p_web.StoreValue('RapEngUpdBrowseJobs:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('RapEngUpdBrowseJobs:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('RapEngUpdBrowseJobs:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  ! Save Window Name
  AddToLog('NetWebBrowse',p_web.RequestData.DataString,'RapEngUpdBrowseJobs',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 18
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('RapEngUpdBrowseJobs_sort',net:DontEvaluate)
  p_web.SetSessionValue('RapEngUpdBrowseJobs_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 7
    loc:vorder = Choose(Loc:SortDirection=1,'tag:tagged','-tag:tagged')
    Loc:LocateField = 'tag:tagged'
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'sbogen:Long1','-sbogen:Long1')
    Loc:LocateField = 'locJobNumber'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'job:date_booked','-job:date_booked')
    Loc:LocateField = 'job:date_booked'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'job:time_booked','-job:time_booked')
    Loc:LocateField = 'job:time_booked'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(jbn:Fault_Description)','-UPPER(jbn:Fault_Description)')
    Loc:LocateField = 'jbn:Fault_Description'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(job:Current_Status)','-UPPER(job:Current_Status)')
    Loc:LocateField = 'job:Current_Status'
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'jobe:SkillLevel','-jobe:SkillLevel')
    Loc:LocateField = 'jobe:SkillLevel'
  of 8
    Loc:LocateField = ''
  end
  if loc:vorder = ''
    loc:nobuffer = 1
    Loc:LocateField = 'sbogen:Long1'
    loc:sortheader = 'Job No'
    loc:vorder = '+sbogen:SessionID,+sbogen:Long1'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('tag:tagged')
    loc:SortHeader = p_web.Translate('')
    p_web.SetSessionValue('RapEngUpdBrowseJobs_LocatorPic','@n3')
  Of upper('locJobNumber')
    loc:SortHeader = p_web.Translate('Job No')
    p_web.SetSessionValue('RapEngUpdBrowseJobs_LocatorPic','@s60')
  Of upper('job:date_booked')
    loc:SortHeader = p_web.Translate('Date Allocated')
    p_web.SetSessionValue('RapEngUpdBrowseJobs_LocatorPic','@d6b')
  Of upper('job:time_booked')
    loc:SortHeader = p_web.Translate('Time Allocated')
    p_web.SetSessionValue('RapEngUpdBrowseJobs_LocatorPic','@t1')
  Of upper('jbn:Fault_Description')
    loc:SortHeader = p_web.Translate('Reported Fault')
    p_web.SetSessionValue('RapEngUpdBrowseJobs_LocatorPic','@s255')
  Of upper('job:Current_Status')
    loc:SortHeader = p_web.Translate('Job Status')
    p_web.SetSessionValue('RapEngUpdBrowseJobs_LocatorPic','@s30')
  Of upper('jobe:SkillLevel')
    loc:SortHeader = p_web.Translate('Skill Level')
    p_web.SetSessionValue('RapEngUpdBrowseJobs_LocatorPic','@s8')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('RapEngUpdBrowseJobs:LookupFrom')
  End!Else
  loc:formaction = 'RapEngUpdBrowseJobs'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="RapEngUpdBrowseJobs:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="RapEngUpdBrowseJobs:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('RapEngUpdBrowseJobs:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="SBO_GenericFile"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="sbogen:RecordNumberKey"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'RapEngUpdBrowseJobs',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2RapEngUpdBrowseJobs',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="RapEngUpdBrowseJobs.locate(''Locator2RapEngUpdBrowseJobs'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'RapEngUpdBrowseJobs.cl(''RapEngUpdBrowseJobs'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="RapEngUpdBrowseJobs_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="RapEngUpdBrowseJobs_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'7','RapEngUpdBrowseJobs','','Click here to sort by tagged',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by tagged')&'">'&p_web.Translate('')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','RapEngUpdBrowseJobs','Job No','Click here to sort by Job No',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Job No')&'">'&p_web.Translate('Job No')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','RapEngUpdBrowseJobs','Date Allocated','Click here to sort by date booked',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by date booked')&'">'&p_web.Translate('Date Allocated')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','RapEngUpdBrowseJobs','Time Allocated','Click here to sort by time booked',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by time booked')&'">'&p_web.Translate('Time Allocated')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','RapEngUpdBrowseJobs','Reported Fault','Click here to sort by Fault Description',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Fault Description')&'">'&p_web.Translate('Reported Fault')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','RapEngUpdBrowseJobs','Job Status','Click here to sort by Current Status',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Current Status')&'">'&p_web.Translate('Job Status')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'6','RapEngUpdBrowseJobs','Skill Level','Click here to sort by Skill Level',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Skill Level')&'">'&p_web.Translate('Skill Level')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  ViewJob
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'job:date_booked' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,18,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('sbogen:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and SBO_GenericFile{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'sbogen:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sbogen:RecordNumber'),p_web.GetValue('sbogen:RecordNumber'),p_web.GetSessionValue('sbogen:RecordNumber'))
      loc:FilterWas = 'sbogen:SessionID = ' & p_web.SessionID
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'RapEngUpdBrowseJobs',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('RapEngUpdBrowseJobs_Filter')
    p_web.SetSessionValue('RapEngUpdBrowseJobs_FirstValue','')
    p_web.SetSessionValue('RapEngUpdBrowseJobs_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,SBO_GenericFile,sbogen:RecordNumberKey,loc:PageRows,'RapEngUpdBrowseJobs',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If SBO_GenericFile{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(SBO_GenericFile,loc:firstvalue)
              Reset(ThisView,SBO_GenericFile)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If SBO_GenericFile{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(SBO_GenericFile,loc:lastvalue)
            Reset(ThisView,SBO_GenericFile)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sbogen:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'RapEngUpdBrowseJobs.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'RapEngUpdBrowseJobs.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'RapEngUpdBrowseJobs.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'RapEngUpdBrowseJobs.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'RapEngUpdBrowseJobs',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('RapEngUpdBrowseJobs_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('RapEngUpdBrowseJobs_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1RapEngUpdBrowseJobs',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="RapEngUpdBrowseJobs.locate(''Locator1RapEngUpdBrowseJobs'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'RapEngUpdBrowseJobs.cl(''RapEngUpdBrowseJobs'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('RapEngUpdBrowseJobs_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('RapEngUpdBrowseJobs_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'RapEngUpdBrowseJobs.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'RapEngUpdBrowseJobs.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'RapEngUpdBrowseJobs.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'RapEngUpdBrowseJobs.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    !    Access:JOBS.ClearKey(job:Ref_Number_Key)
    !    job:Ref_Number = sbogen:Long1
    !    IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
    !    END ! IF
    !    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    !    wob:RefNumber = job:Ref_Number
    !    IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
    !    END ! IF
    
    locJobNumber = job:Ref_Number & '-' & p_web.GSV('BookingBranchID') & wob:JobNumber    
    Access:TAGFILE.Clearkey(tag:keyTagged)
    tag:sessionID    = p_web.SessionID
    tag:taggedValue    = sbogen:Long1
    if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
        ! Found
    else ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
        ! Error
        tag:Tagged = 0
    end ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
    loc:field = sbogen:RecordNumber
    p_web._thisrow = p_web._nocolon('sbogen:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('RapEngUpdBrowseJobs:LookupField')) = sbogen:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((sbogen:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="RapEngUpdBrowseJobs.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If SBO_GenericFile{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(SBO_GenericFile)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If SBO_GenericFile{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(SBO_GenericFile)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','sbogen:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="RapEngUpdBrowseJobs.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','sbogen:RecordNumber',clip(loc:field),,'checked',,,'onclick="RapEngUpdBrowseJobs.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::tag:tagged
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locJobNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job:date_booked
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job:time_booked
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif job:Current_Status[1:3] = '606'
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            else
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            end
          end ! loc:eip = 0
          do value::jbn:Fault_Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job:Current_Status
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::jobe:SkillLevel
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::ViewJob
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="RapEngUpdBrowseJobs.omv(this);" onMouseOut="RapEngUpdBrowseJobs.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var RapEngUpdBrowseJobs=new browseTable(''RapEngUpdBrowseJobs'','''&clip(loc:formname)&''','''&p_web._jsok('sbogen:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('sbogen:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'RapEngUpdBrowseJobs.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'RapEngUpdBrowseJobs.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2RapEngUpdBrowseJobs')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1RapEngUpdBrowseJobs')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1RapEngUpdBrowseJobs')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2RapEngUpdBrowseJobs')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(SBO_GenericFile)
  p_web._CloseFile(JOBS)
  p_web._CloseFile(WEBJOB)
  p_web._CloseFile(JOBNOTES)
  p_web._CloseFile(JOBSE)
  p_web._CloseFile(TagFile)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(SBO_GenericFile)
  Bind(sbogen:Record)
  Clear(sbogen:Record)
  NetWebSetSessionPics(p_web,SBO_GenericFile)
  p_web._OpenFile(JOBS)
  Bind(job:Record)
  NetWebSetSessionPics(p_web,JOBS)
  p_web._OpenFile(WEBJOB)
  Bind(wob:Record)
  NetWebSetSessionPics(p_web,WEBJOB)
  p_web._OpenFile(JOBNOTES)
  Bind(jbn:Record)
  NetWebSetSessionPics(p_web,JOBNOTES)
  p_web._OpenFile(JOBSE)
  Bind(jobe:Record)
  NetWebSetSessionPics(p_web,JOBSE)
  p_web._OpenFile(TagFile)
  Bind(tag:Record)
  NetWebSetSessionPics(p_web,TagFile)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('sbogen:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
      ! Save Window Name
      IF (loc:alert <> '')
          AddToLog('Alert',loc:alert,'RapEngUpdBrowseJobs',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
      END ! IF
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(SBO_GenericFile)
  Case upper(p_web.GetValue('_EIPClm'))
  of upper('tag:tagged')
    do Validate::tag:tagged
  of upper('ViewJob')
    do Validate::ViewJob
  End
  p_web._CloseFile(SBO_GenericFile)
! ----------------------------------------------------------------------------------------
Validate::tag:tagged  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  sbogen:RecordNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  !#Bryan: Added embed to set extra key components for multi part keys
  loc:result = p_web._GetFile(SBO_GenericFile,sbogen:RecordNumberKey)
  p_web.FileToSessionQueue(SBO_GenericFile)
  loc:was = tag:tagged
  tag:tagged = Choose(p_web.GetValue('value') = 1,1,0)
  Access:TAGFILE.Clearkey(tag:keyTagged)
  tag:sessionID    = p_web.SessionID
  tag:taggedValue    = sbogen:Long1
  if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
      ! Found
      tag:tagged = Choose(p_web.getValue('value') = 1,true,false)
      access:TAGFILE.tryUpdate()
  else ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
      ! Error
      if (Access:TAGFILE.PrimeRecord() = Level:Benign)
          tag:sessionID    = p_web.sessionID
          tag:taggedValue    = sbogen:Long1
          tag:tagged = Choose(p_web.getValue('value') = 1,true,false)
          if (Access:TAGFILE.TryInsert() = Level:Benign)
              ! Inserted
          else ! if (Access:TAGFILE.TryInsert() = Level:Benign)
              ! Error
          end ! if (Access:TAGFILE.TryInsert() = Level:Benign)
      end ! if (Access:TAGFILE.PrimeRecord() = Level:Benign)
  end ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
  ! Validation goes here, if fails set loc:invalid & loc:alert
  do CheckForDuplicate
  If loc:invalid = '' ! save record
      p_web._updatefile(TagFile)
      p_web.FileToSessionQueue(TagFile)
  End
  do SendAlert
  if loc:alert <> ''
    tag:tagged = loc:was
    do Value::tag:tagged
  End
  ! updating other browse cells goes here.
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::tag:tagged   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('tag:tagged_'&sbogen:RecordNumber,,net:crc)
      loc:extra = Choose(tag:tagged = 1,'checked','')
      packet = clip(packet) & p_web.CreateInput('checkbox','inp'&p_web.crc32('tag:tagged'&sbogen:RecordNumber),clip(1),,loc:extra & ' ' & loc:disabled,,,'onclick="RapEngUpdBrowseJobs.eip(this,'''&p_web._jsok('tag:tagged')&''','''&p_web._jsok(loc:viewstate)&''');"',,) & '<13,10>'
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locJobNumber   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locJobNumber_'&sbogen:RecordNumber,,net:crc)
      loc:total[1] = loc:total[1] + (locJobNumber)
      packet = clip(packet) & p_web._jsok(Left(Format(locJobNumber,'@s60')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::job:date_booked   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job:date_booked_'&sbogen:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job:date_booked,'@d6b')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::job:time_booked   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job:time_booked_'&sbogen:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job:time_booked,'@t1')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::jbn:Fault_Description   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    elsif job:Current_Status[1:3] = '606'
      packet = clip(packet) & p_web._DivHeader('jbn:Fault_Description_'&sbogen:RecordNumber,,net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok('QA REQUIRED ON THIS UNIT',0))
    else
      packet = clip(packet) & p_web._DivHeader('jbn:Fault_Description_'&sbogen:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(jbn:Fault_Description,'@s255')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::job:Current_Status   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job:Current_Status_'&sbogen:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job:Current_Status,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::jobe:SkillLevel   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('jobe:SkillLevel_'&sbogen:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(jobe:SkillLevel,'@s8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
Validate::ViewJob  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  sbogen:RecordNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  !#Bryan: Added embed to set extra key components for multi part keys
  loc:result = p_web._GetFile(SBO_GenericFile,sbogen:RecordNumberKey)
  p_web.FileToSessionQueue(SBO_GenericFile)
  do CheckForDuplicate
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::ViewJob   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('ViewJob_'&sbogen:RecordNumber,,net:crc)
      loc:disabled = ''
      packet = clip(packet) & p_web.CreateButton('button','ViewJob','View Job','button-flat',loc:formname,,,'window.open('''& p_web._MakeURL(clip('RapEngUpdViewJob')  & '&' & p_web._noColon('sbogen:RecordNumber')&'='& p_web.escape(sbogen:RecordNumber) & '' ) & ''','''&clip('_self')&''')',,loc:disabled,,,,,) & '<13,10>' !2
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(TagFile)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(TagFile)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = sbogen:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('sbogen:RecordNumber',sbogen:RecordNumber)


MakeFooter  Routine
  TableQueue.Kind = Net:RowFooter
  If records(TableQueue) > 0
    packet = clip(packet) & '<tr>'
    If(loc:SelectionMethod  = Net:Radio)
      packet = clip(packet) & '<td width="1">&#160;</td>' ! first column is the select column
    End
      If loc:skip = 0
        loc:class = ' class="'&clip('BrowseFooter')&'"'
        packet = clip(packet) & '<td'&clip(loc:class)&'>&#160;</td>'
      End
      loc:skip = Choose(loc:skip = 0,0,loc:skip-1)
      If loc:skip = 0
        loc:class = 'BrowseFooter'
        If loc:class[1] = ' ' then loc:class = clip('BrowseFooter') & loc:class.
        If loc:class <> '' then loc:class = ' class="'&clip(loc:class)&'"'.
          loc:skip = 1
        packet = clip(packet) & '<td'&clip(loc:class)&'>' & loc:RowNumber &'</td>'
      End
      loc:skip = Choose(loc:skip = 0,0,loc:skip-1)
      If loc:skip = 0
        loc:class = ' class="'&clip('BrowseFooter')&'"'
        packet = clip(packet) & '<td'&clip(loc:class)&'>&#160;</td>'
      End
      loc:skip = Choose(loc:skip = 0,0,loc:skip-1)
      If loc:skip = 0
        loc:class = ' class="'&clip('BrowseFooter')&'"'
        packet = clip(packet) & '<td'&clip(loc:class)&'>&#160;</td>'
      End
      loc:skip = Choose(loc:skip = 0,0,loc:skip-1)
      If loc:skip = 0
        loc:class = ' class="'&clip('BrowseFooter')&'"'
        packet = clip(packet) & '<td'&clip(loc:class)&'>&#160;</td>'
      End
      loc:skip = Choose(loc:skip = 0,0,loc:skip-1)
      If loc:skip = 0
        loc:class = ' class="'&clip('BrowseFooter')&'"'
        packet = clip(packet) & '<td'&clip(loc:class)&'>&#160;</td>'
      End
      loc:skip = Choose(loc:skip = 0,0,loc:skip-1)
      If loc:skip = 0
        loc:class = ' class="'&clip('BrowseFooter')&'"'
        packet = clip(packet) & '<td'&clip(loc:class)&'>&#160;</td>'
      End
      loc:skip = Choose(loc:skip = 0,0,loc:skip-1)
      If loc:skip = 0
        loc:class = ' class="'&clip('BrowseFooter')&'"'
        packet = clip(packet) & '<td'&clip(loc:class)&'>&#160;</td>'
      End
      loc:skip = Choose(loc:skip = 0,0,loc:skip-1)
    packet = clip(packet) & '</tr>'
    TableQueue.Kind = Net:RowFooter
  End
  do AddPacket

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('sbogen:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('sbogen:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('sbogen:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
