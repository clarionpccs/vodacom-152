

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER246.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER010.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER120.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER239.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER247.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER251.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER254.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER255.INC'),ONCE        !Req'd for module callout resolution
                     END


WaybillGeneration    PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
RestrictMessage      STRING(100)                           !
RestrictPassword     STRING(20)                            !
locWaybillJobNNumber STRING(30)                            !
locWaybillIMEINumber STRING(30)                            !
locWaybillSecurityPackNumber STRING(30)                    !
                    MAP
AddJobToWaybill         PROCEDURE(LONG pIgnoreAccessories)
ForceWaybills           PROCEDURE(),LONG,PROC
ProcessCounters         PROCEDURE()
ProcessJob              PROCEDURE()
                    END ! MAP
FilesOpened     Long
WAYBPRO_Alias::State  USHORT
JOBACC::State  USHORT
JOBS::State  USHORT
WAYBPRO::State  USHORT
WAYBAWT::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('WaybillGeneration')
  loc:formname = 'WaybillGeneration_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'WaybillGeneration',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('WaybillGeneration','')
    p_web._DivHeader('WaybillGeneration',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferWaybillGeneration',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWaybillGeneration',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWaybillGeneration',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_WaybillGeneration',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWaybillGeneration',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_WaybillGeneration',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'WaybillGeneration',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
ResetVariables      ROUTINE
    p_web.SSV('ProcessJob',0)
    p_web.SSV('AccessoryMismatch',0)
    p_web.SSV('locWaybillJobNNumber','')
    p_web.SSV('locWaybillIMEINumber','')
    p_web.SSV('locWaybillSecurityPackNumber','')
    ClearTaggingFile(p_web)
Refresh:ProcessJob_FAIL     ROUTINE
    DO ResetVariables
    do Value::locWaybillIMEINumber  !1
    do Value::locWaybillJobNNumber  !1
    

Refresh:ProcessJob_PASS     ROUTINE
    do Value::locWaybillIMEINumber  !1
    do Value::locWaybillJobNNumber  !1
    do Value::tagAccessories  !1
    do Value::locWaybillSecurityPackNumber  !1
    do Value::btnCancelProcess  !1
    do Value::btnProcessJob  !1
    do Value::txtValidatedAccessories  !1
    DO Value::btnTagAll
    DO Value::btnUnTagAll
    do Value::btnValidateAccessories  !1
    DO Value::btnGenerateNRWaybill
    DO Value::btnGenerateOBFWaybill
    
Refresh:CancelProcess       ROUTINE
    DO ResetVariables
    do Value::locWaybillIMEINumber  !1
    do Value::locWaybillJobNNumber  !1
    do Value::tagAccessories  !1
    do Value::locWaybillSecurityPackNumber  !1
    do Value::btnCancelProcess  !1
    do Value::btnProcessJob  !1
    do Value::txtValidatedAccessories  !1
    do Value::btnValidateAccessories  !1
    DO Value::btnTagAll
    DO Value::btnUnTagAll
    do Value::btnAcceptMismatch  !1
    do Value::_txtMismatch  !1    
    
Refresh:ValidateAccessories_FAIL    ROUTINE
    do Value::btnAcceptMismatch  !1
    do Value::_txtMismatch  !1

Refresh:ValidateAccessories_PASS    ROUTINE
    DO Value::brwAwaitingProcessing  !1
    DO Value::brwPRocessed  !1
    DO Value::brwPageProcessed  !1
    
    DO ResetVariables
    
    do Value::locWaybillIMEINumber  !1
    do Value::locWaybillJobNNumber  !1
    do Value::tagAccessories  !1
    do Value::locWaybillSecurityPackNumber  !1
    do Value::btnCancelProcess  !1
    do Value::btnProcessJob  !1
    do Value::txtValidatedAccessories  !1
    DO Value::btnTagAll
    DO Value::btnUnTagAll
    DO Value::btnGenerateNRWaybill
    DO Value::btnGenerateOBFWaybill

    
Refresh:AcceptMismatch      ROUTINE
    DO Value::brwAwaitingProcessing  !1
    DO Value::brwPRocessed  !1
    DO Value::brwPageProcessed  !1

    DO ResetVariables
    
    do Value::locWaybillIMEINumber  !1
    do Value::locWaybillJobNNumber  !1
    do Value::tagAccessories  !1
    do Value::locWaybillSecurityPackNumber  !1
    do Value::btnCancelProcess  !1
    do Value::btnProcessJob  !1
    do Value::txtValidatedAccessories  !1
    do Value::btnValidateAccessories  !1
    do Value::btnAcceptMismatch  !1
    do Value::_txtMismatch  !1    
    DO Value::btnTagAll
    DO Value::btnUnTagAll
    

Refresh:RejectMismatch      ROUTINE
    DO ResetVariables
    do Value::locWaybillIMEINumber  !1
    do Value::locWaybillJobNNumber  !1
    do Value::tagAccessories  !1
    do Value::locWaybillSecurityPackNumber  !1
    do Value::btnCancelProcess  !1
    do Value::btnProcessJob  !1
    do Value::txtValidatedAccessories  !1
    do Value::btnValidateAccessories  !1
    DO Value::btnTagAll
    DO Value::btnUnTagAll
    do Value::btnAcceptMismatch  !1
    do Value::_txtMismatch  !1    
DeleteSessionValues ROUTINE ! Auto generated by Bryan's Template
    ! Local Variables
    ! p_web.DeleteSessionValue('Ans') ! Excluded
    p_web.DeleteSessionValue('RestrictMessage')
    p_web.DeleteSessionValue('RestrictPassword')
    p_web.DeleteSessionValue('locWaybillJobNNumber')
    p_web.DeleteSessionValue('locWaybillIMEINumber')
    p_web.DeleteSessionValue('locWaybillSecurityPackNumber')

    ! Other Variables
    p_web.DeleteSessionValue('ProcessJob')
    p_web.DeleteSessionValue('AccessoryMismatch')
OpenFiles  ROUTINE
  p_web._OpenFile(WAYBPRO_Alias)
  p_web._OpenFile(JOBACC)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(WAYBPRO)
  p_web._OpenFile(WAYBAWT)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(WAYBPRO_Alias)
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WAYBPRO)
  p_Web._CloseFile(WAYBAWT)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('WaybillGeneration_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locWaybillJobNNumber',locWaybillJobNNumber)
  p_web.SetSessionValue('locWaybillIMEINumber',locWaybillIMEINumber)
  p_web.SetSessionValue('RestrictMessage',RestrictMessage)
  p_web.SetSessionValue('locWaybillSecurityPackNumber',locWaybillSecurityPackNumber)
  p_web.SetSessionValue('RestrictPassword',RestrictPassword)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locWaybillJobNNumber')
    locWaybillJobNNumber = p_web.GetValue('locWaybillJobNNumber')
    p_web.SetSessionValue('locWaybillJobNNumber',locWaybillJobNNumber)
  End
  if p_web.IfExistsValue('locWaybillIMEINumber')
    locWaybillIMEINumber = p_web.GetValue('locWaybillIMEINumber')
    p_web.SetSessionValue('locWaybillIMEINumber',locWaybillIMEINumber)
  End
  if p_web.IfExistsValue('RestrictMessage')
    RestrictMessage = p_web.GetValue('RestrictMessage')
    p_web.SetSessionValue('RestrictMessage',RestrictMessage)
  End
  if p_web.IfExistsValue('locWaybillSecurityPackNumber')
    locWaybillSecurityPackNumber = p_web.GetValue('locWaybillSecurityPackNumber')
    p_web.SetSessionValue('locWaybillSecurityPackNumber',locWaybillSecurityPackNumber)
  End
  if p_web.IfExistsValue('RestrictPassword')
    RestrictPassword = p_web.GetValue('RestrictPassword')
    p_web.SetSessionValue('RestrictPassword',RestrictPassword)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('WaybillGeneration_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    DO DeleteSessionValues
    
    ClearTaggingFile(p_web)
    
    IF (DoesUserHaveAccess(p_web,'RAPID - WAYBILL GENERATION') = FALSE)
        CreateScript(p_web,packet,'alert("You do not have access to this option")')
        CreateScript(p_web,packet,'window.open("IndexPage","_self")')
        DO SendPacket
        EXIT
    END ! IF
    
    ProcessCounters()
  
    IF (ForceWaybills())
        CreateScript(p_web,packet,'alert("10 NR/48E or OBF Jobs Have Been Processed.\n\nYou must create the relevant Waybill before you can continue.")')
        DO SendPacket
    END ! IF
    
  !      !TB13195 - re-introducing the access levels for printing less than ten copies
  !      Restrict# = 0
  !      RestrictMessage = ''
  !      RestrictPassword= ''
  !      p_web.ssv('RestrictPassword','')
  !  
  !      if (DoesUserHaveAccess(p_web,'GENERATE NR/48E WAYBILL') = FALSE)
  !          Restrict# += 1
  !          p_web.ssv('RESTRICTNR48','1')
  !          RestrictMessage = 'You do not have access to print less than ten NR/48E jobs on a waybill. If you intend to do this you must enter a qualifying password below.'
  !      ELSE  
  !          p_web.ssv('RESTRICTNR48','0')
  !      END    
  !      if (DoesUserHaveAccess(p_web,'GENERATE OBF WAYBILL') = FALSE)
  !          Restrict# += 1
  !          p_web.ssv('RESTRICTOBF','1')
  !          RestrictMessage = 'You do not have access to print less than ten OBF jobs on a waybill. If you intend to do this you must enter a qualifying password below.'
  !      ELSE  
  !          p_web.ssv('RESTRICTOBF','0')
  !      END
  !      if restrict# = 2 then
  !          RestrictMessage = 'You do not have access to print less than ten jobs on a waybill. If you intend to do this you must enter a qualifying password below.'
  !      end
      p_web.site.CancelButton.TextValue = 'Close'
      p_web.site.CancelButton.Image = 'images/psave.png'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locWaybillJobNNumber = p_web.RestoreValue('locWaybillJobNNumber')
 locWaybillIMEINumber = p_web.RestoreValue('locWaybillIMEINumber')
 RestrictMessage = p_web.RestoreValue('RestrictMessage')
 locWaybillSecurityPackNumber = p_web.RestoreValue('locWaybillSecurityPackNumber')
 RestrictPassword = p_web.RestoreValue('RestrictPassword')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'IndexPage'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('WaybillGeneration_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('WaybillGeneration_ChainTo')
    loc:formaction = p_web.GetSessionValue('WaybillGeneration_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="WaybillGeneration" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="WaybillGeneration" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="WaybillGeneration" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Waybill Generation') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Waybill Generation',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_WaybillGeneration">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_WaybillGeneration" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_WaybillGeneration')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Please Process Up To 10 Jobs Per Waybill') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Actions') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_WaybillGeneration')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_WaybillGeneration'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('WaybillGeneration_WaybillBrowseAwaitingProcessing_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('WaybillGeneration_WaybillProcessedPage_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('WaybillGeneration_WaybillBrowseProcessed_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('WaybillGeneration_TagBookedAccessories_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_WaybillGeneration')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Please Process Up To 10 Jobs Per Waybill') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_WaybillGeneration_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Please Process Up To 10 Jobs Per Waybill')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Please Process Up To 10 Jobs Per Waybill')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Please Process Up To 10 Jobs Per Waybill')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Please Process Up To 10 Jobs Per Waybill')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwAwaitingProcessing
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwPageProcessed
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwProcessed
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Actions') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_WaybillGeneration_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtProcessJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::txtCreateWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWaybillJobNNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWaybillJobNNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnGenerateOBFWaybill
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnGenerateNRWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWaybillIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWaybillIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::RestrictMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWaybillSecurityPackNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWaybillSecurityPackNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::RestrictPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::RestrictPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnProcessJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_WaybillGeneration_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::txtValidatedAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::tagAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hiddenNextToBrowse
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnTagAll
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnUnTagAll
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnValidateAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::_txtMismatch
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="4">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::_txtMismatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnAcceptMismatch
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnCancelProcess
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::brwAwaitingProcessing  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwAwaitingProcessing',p_web.GetValue('NewValue'))
    do Value::brwAwaitingProcessing
  Else
    p_web.StoreValue('wya:WAYBAWTID')
  End

Value::brwAwaitingProcessing  Routine
  loc:extra = ''
  ! --- BROWSE ---  WaybillBrowseAwaitingProcessing --
  p_web.SetValue('WaybillBrowseAwaitingProcessing:NoForm',1)
  p_web.SetValue('WaybillBrowseAwaitingProcessing:FormName',loc:formname)
  p_web.SetValue('WaybillBrowseAwaitingProcessing:parentIs','Form')
  p_web.SetValue('_parentProc','WaybillGeneration')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('WaybillGeneration_WaybillBrowseAwaitingProcessing_embedded_div')&'"><!-- Net:WaybillBrowseAwaitingProcessing --></div><13,10>'
    p_web._DivHeader('WaybillGeneration_' & lower('WaybillBrowseAwaitingProcessing') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('WaybillGeneration_' & lower('WaybillBrowseAwaitingProcessing') & '_value')
  else
    packet = clip(packet) & '<!-- Net:WaybillBrowseAwaitingProcessing --><13,10>'
  end
  do SendPacket


Validate::brwPageProcessed  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwPageProcessed',p_web.GetValue('NewValue'))
    do Value::brwPageProcessed
  End

Value::brwPageProcessed  Routine
  loc:extra = ''
  ! --- BROWSE ---  WaybillProcessedPage --
  p_web.SetValue('WaybillProcessedPage:NoForm',1)
  p_web.SetValue('WaybillProcessedPage:FormName',loc:formname)
  p_web.SetValue('WaybillProcessedPage:parentIs','Form')
  p_web.SetValue('_parentProc','WaybillGeneration')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('WaybillGeneration_WaybillProcessedPage_embedded_div')&'"><!-- Net:WaybillProcessedPage --></div><13,10>'
    p_web._DivHeader('WaybillGeneration_' & lower('WaybillProcessedPage') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('WaybillGeneration_' & lower('WaybillProcessedPage') & '_value')
  else
    packet = clip(packet) & '<!-- Net:WaybillProcessedPage --><13,10>'
  end
  do SendPacket


Validate::brwProcessed  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwProcessed',p_web.GetValue('NewValue'))
    do Value::brwProcessed
  Else
    p_web.StoreValue('wyp:WAYBPROID')
  End

Value::brwProcessed  Routine
  loc:extra = ''
  ! --- BROWSE ---  WaybillBrowseProcessed --
  p_web.SetValue('WaybillBrowseProcessed:NoForm',1)
  p_web.SetValue('WaybillBrowseProcessed:FormName',loc:formname)
  p_web.SetValue('WaybillBrowseProcessed:parentIs','Form')
  p_web.SetValue('_parentProc','WaybillGeneration')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('WaybillGeneration_WaybillBrowseProcessed_embedded_div')&'"><!-- Net:WaybillBrowseProcessed --></div><13,10>'
    p_web._DivHeader('WaybillGeneration_' & lower('WaybillBrowseProcessed') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('WaybillGeneration_' & lower('WaybillBrowseProcessed') & '_value')
  else
    packet = clip(packet) & '<!-- Net:WaybillBrowseProcessed --><13,10>'
  end
  do SendPacket


Validate::txtProcessJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtProcessJob',p_web.GetValue('NewValue'))
    do Value::txtProcessJob
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtProcessJob  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('txtProcessJob') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('bold')&'">' & p_web.Translate('Process Job',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::txtCreateWaybill  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtCreateWaybill',p_web.GetValue('NewValue'))
    do Value::txtCreateWaybill
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtCreateWaybill  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('txtCreateWaybill') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('bold')&'">' & p_web.Translate('Create Waybill',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locWaybillJobNNumber  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('locWaybillJobNNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locWaybillJobNNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWaybillJobNNumber',p_web.GetValue('NewValue'))
    locWaybillJobNNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWaybillJobNNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locWaybillJobNNumber',p_web.GetValue('Value'))
    locWaybillJobNNumber = p_web.GetValue('Value')
  End
  If locWaybillJobNNumber = ''
    loc:Invalid = 'locWaybillJobNNumber'
    loc:alert = p_web.translate('Job Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::locWaybillJobNNumber
  do SendAlert

Value::locWaybillJobNNumber  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('locWaybillJobNNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locWaybillJobNNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ProcessJob') = 1 OR p_web.GSV('ForceWaybill') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ProcessJob') = 1 OR p_web.GSV('ForceWaybill') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locWaybillJobNNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locWaybillJobNNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWaybillJobNNumber'',''waybillgeneration_locwaybilljobnnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locWaybillJobNNumber',p_web.GetSessionValueFormat('locWaybillJobNNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillGeneration_' & p_web._nocolon('locWaybillJobNNumber') & '_value')


Validate::btnGenerateOBFWaybill  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnGenerateOBFWaybill',p_web.GetValue('NewValue'))
    do Value::btnGenerateOBFWaybill
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnGenerateOBFWaybill  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('btnGenerateOBFWaybill') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('generateWaybill(''OBF'',' & p_web.GSV('ForceOBF') & ')')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnGenerateOBFWaybill','Generate OBF Waybill','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnGenerateNRWaybill  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnGenerateNRWaybill',p_web.GetValue('NewValue'))
    do Value::btnGenerateNRWaybill
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnGenerateNRWaybill  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('btnGenerateNRWaybill') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('generateWaybill(''NR'',' & p_web.GSV('ForceNR') & ')')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnGenerateNRWaybill','Generate NR/48E Waybill','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Prompt::locWaybillIMEINumber  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('locWaybillIMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('I.M.E.I. Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locWaybillIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWaybillIMEINumber',p_web.GetValue('NewValue'))
    locWaybillIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWaybillIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locWaybillIMEINumber',p_web.GetValue('Value'))
    locWaybillIMEINumber = p_web.GetValue('Value')
  End
  If locWaybillIMEINumber = ''
    loc:Invalid = 'locWaybillIMEINumber'
    loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::locWaybillIMEINumber
  do SendAlert

Value::locWaybillIMEINumber  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('locWaybillIMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locWaybillIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ProcessJob') = 1 OR p_web.GSV('ForceWaybill') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ProcessJob') = 1 OR p_web.GSV('ForceWaybill') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locWaybillIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locWaybillIMEINumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWaybillIMEINumber'',''waybillgeneration_locwaybillimeinumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locWaybillIMEINumber',p_web.GetSessionValueFormat('locWaybillIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillGeneration_' & p_web._nocolon('locWaybillIMEINumber') & '_value')


Validate::RestrictMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('RestrictMessage',p_web.GetValue('NewValue'))
    RestrictMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::RestrictMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('RestrictMessage',p_web.GetValue('Value'))
    RestrictMessage = p_web.GetValue('Value')
  End

Value::RestrictMessage  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('RestrictMessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- RestrictMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('redbold')&'">' & p_web.Translate(RestrictMessage,) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locWaybillSecurityPackNumber  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('locWaybillSecurityPackNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Security Pack Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locWaybillSecurityPackNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWaybillSecurityPackNumber',p_web.GetValue('NewValue'))
    locWaybillSecurityPackNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWaybillSecurityPackNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locWaybillSecurityPackNumber',p_web.GetValue('Value'))
    locWaybillSecurityPackNumber = p_web.GetValue('Value')
  End
  do Value::locWaybillSecurityPackNumber
  do SendAlert

Value::locWaybillSecurityPackNumber  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('locWaybillSecurityPackNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locWaybillSecurityPackNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ProcessJob') = 1 OR p_web.GSV('ForceWaybill') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ProcessJob') = 1 OR p_web.GSV('ForceWaybill') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('locWaybillSecurityPackNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWaybillSecurityPackNumber'',''waybillgeneration_locwaybillsecuritypacknumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locWaybillSecurityPackNumber',p_web.GetSessionValueFormat('locWaybillSecurityPackNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillGeneration_' & p_web._nocolon('locWaybillSecurityPackNumber') & '_value')


Prompt::RestrictPassword  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('RestrictPassword') & '_prompt',Choose(RestrictMessage = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Master Password')
  If RestrictMessage = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::RestrictPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('RestrictPassword',p_web.GetValue('NewValue'))
    RestrictPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::RestrictPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('RestrictPassword',p_web.GetValue('Value'))
    RestrictPassword = p_web.GetValue('Value')
  End
    RestrictPassword = Upper(RestrictPassword)
    p_web.SetSessionValue('RestrictPassword',RestrictPassword)
  !Check restrict Password here
  do Value::RestrictPassword
  do SendAlert

Value::RestrictPassword  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('RestrictPassword') & '_value',Choose(RestrictMessage = '','hdiv','adiv'))
  loc:extra = ''
  If Not (RestrictMessage = '')
  ! --- STRING --- RestrictPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('RestrictPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''RestrictPassword'',''waybillgeneration_restrictpassword_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','RestrictPassword',p_web.GetSessionValueFormat('RestrictPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillGeneration_' & p_web._nocolon('RestrictPassword') & '_value')


Validate::btnProcessJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnProcessJob',p_web.GetValue('NewValue'))
    do Value::btnProcessJob
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    IF (p_web.GSV('locWaybillJobNNumber') = '' OR p_web.GSV('locWaybillIMEINumber') = '')
          loc:alert = 'Job Number AND IMEI Number Required.'
        p_web.SSV('ProcessJob',0)
        
        DO Refresh:ProcessJob_FAIL
          !DO RefreshProcessFields
    ELSE
        ProcessJob()       
        DO Refresh:ProcessJob_PASS
        !DO RefreshProcessFields        
    END ! IF    
  do SendAlert
  do Value::btnProcessJob  !1

Value::btnProcessJob  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('btnProcessJob') & '_value',Choose(p_web.GSV('ProcessJob') = 1 OR p_web.GSV('ForceWaybill') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('ProcessJob') = 1 OR p_web.GSV('ForceWaybill') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnProcessJob'',''waybillgeneration_btnprocessjob_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnProcessJob','Process Job','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillGeneration_' & p_web._nocolon('btnProcessJob') & '_value')


Validate::txtValidatedAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtValidatedAccessories',p_web.GetValue('NewValue'))
    do Value::txtValidatedAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtValidatedAccessories  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('txtValidatedAccessories') & '_value',Choose(p_web.GSV('ProcessJob') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('ProcessJob') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('FormPrompt')&'">' & p_web.Translate('Select Accessories To Send To ARC',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Validate::tagAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tagAccessories',p_web.GetValue('NewValue'))
    do Value::tagAccessories
  Else
    p_web.StoreValue('acd:Accessory')
  End

Value::tagAccessories  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('ProcessJob') = 0,1,0))
  ! --- BROWSE ---  TagBookedAccessories --
  p_web.SetValue('TagBookedAccessories:NoForm',1)
  p_web.SetValue('TagBookedAccessories:FormName',loc:formname)
  p_web.SetValue('TagBookedAccessories:parentIs','Form')
  p_web.SetValue('_parentProc','WaybillGeneration')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('WaybillGeneration_TagBookedAccessories_embedded_div')&'"><!-- Net:TagBookedAccessories --></div><13,10>'
    p_web._DivHeader('WaybillGeneration_' & lower('TagBookedAccessories') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('WaybillGeneration_' & lower('TagBookedAccessories') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagBookedAccessories --><13,10>'
  end
  do SendPacket


Validate::hiddenNextToBrowse  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hiddenNextToBrowse',p_web.GetValue('NewValue'))
    do Value::hiddenNextToBrowse
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hiddenNextToBrowse  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('hiddenNextToBrowse') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::btnTagAll  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnTagAll',p_web.GetValue('NewValue'))
    do Value::btnTagAll
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    ClearTaggingFile(p_web)
    
    Access:JOBACC.ClearKey(jac:Ref_Number_Key)
    jac:Ref_Number = p_web.GSV('job:Ref_Number')
    SET(jac:Ref_Number_Key,jac:Ref_Number_Key)
    LOOP UNTIL Access:JOBACC.Next() <> Level:Benign
        IF (jac:Ref_Number <> p_web.GSV('job:Ref_Number'))
            BREAK
        END ! IF
        
        Access:Tagging.ClearKey(tgg:SessionIDKey)
        tgg:SessionID = p_web.SessionID
        tgg:TaggedValue = jac:Accessory
        IF (Access:Tagging.TryFetch(tgg:SessionIDKey) = Level:Benign)
            IF (tgg:Tagged = 0)
                tgg:Tagged = 1
                Access:Tagging.TryUpdate()
            END ! IF (tgg:Tagged = 0)
        ELSE ! IF (Access:Tagging.TryFetch(tgg:SessionIDKey) = Level:Benign)
            IF (Access:Tagging.PrimeRecord() = Level:Benign)
                tgg:SessionID = p_web.SessionID
                tgg:TaggedValue = jac:Accessory
                tgg:Tagged = 1
                IF (Access:Tagging.TryInsert())
                    Access:Tagging.CancelAutoInc()
                END ! IF
            END ! IF
        END ! IF (Access:Tagging.TryFetch(tgg:SessionIDKey) = Level:Benign)
        
    END ! LOOp
  do SendAlert
  do Value::tagAccessories  !1

Value::btnTagAll  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('btnTagAll') & '_value',Choose(p_web.GSV('ProcessJob') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('ProcessJob') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnTagAll'',''waybillgeneration_btntagall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnTagAll','Tag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillGeneration_' & p_web._nocolon('btnTagAll') & '_value')


Validate::btnUnTagAll  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnUnTagAll',p_web.GetValue('NewValue'))
    do Value::btnUnTagAll
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    ClearTaggingFile(p_web)  
  do SendAlert
  do Value::tagAccessories  !1

Value::btnUnTagAll  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('btnUnTagAll') & '_value',Choose(p_web.GSV('ProcessJob') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('ProcessJob') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnUnTagAll'',''waybillgeneration_btnuntagall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnUnTagAll','UnTag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillGeneration_' & p_web._nocolon('btnUnTagAll') & '_value')


Validate::btnValidateAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnValidateAccessories',p_web.GetValue('NewValue'))
    do Value::btnValidateAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    p_web.SSV('AccessoryMismatch',0)
    Access:JOBACC.ClearKey(jac:Ref_Number_Key)
    jac:Ref_Number = p_web.GSV('job:Ref_Number')
    SET(jac:Ref_Number_Key,jac:Ref_Number_Key)
    LOOP UNTIL Access:JOBACC.Next() <> Level:Benign
        IF (jac:Ref_Number <> p_web.GSV('job:Ref_Number'))
            BREAK
        END 
        Access:Tagging.ClearKey(tgg:SessionIDKey)
        tgg:SessionID = p_web.SessionID
        tgg:TaggedValue = jac:Accessory
        IF (Access:Tagging.TryFetch(tgg:SessionIDKey))
            p_web.SSV('AccessoryMismatch',1)
            jac:Attached = 0            
        ELSE
            IF (tgg:Tagged <> 1)
                p_web.SSV('AccessoryMismatch',1)
                jac:Attached = 0
            ELSE
                jac:Attached = 1
            END ! IF
            
        END ! IF
        Access:JOBACC.TryUpdate()
    END ! LOOP    
    
    IF (p_web.GSV('AccessoryMismatch') = 0)
        AddJobToWaybill(0)
        DO Refresh:ValidateAccessories_PASS
    ELSE
        DO Refresh:ValidateAccessories_FAIL
            
    END ! IF
    
  
    
  do SendAlert
  do Value::btnValidateAccessories  !1

Value::btnValidateAccessories  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('btnValidateAccessories') & '_value',Choose(p_web.GSV('ProcessJob') = 0 OR (p_web.GSV('AccessoryMismatch') = 1 AND p_web.GSV('ProcessJob') = 1),'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('ProcessJob') = 0 OR (p_web.GSV('AccessoryMismatch') = 1 AND p_web.GSV('ProcessJob') = 1))
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnValidateAccessories'',''waybillgeneration_btnvalidateaccessories_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnValidateAccessories','Validate Accessories','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillGeneration_' & p_web._nocolon('btnValidateAccessories') & '_value')


Prompt::_txtMismatch  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('_txtMismatch') & '_prompt',Choose(p_web.GSV('AccessoryMismatch') <> 1 OR p_web.GSV('ProcessJob') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('AccessoryMismatch') <> 1 OR p_web.GSV('ProcessJob') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::_txtMismatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('_txtMismatch',p_web.GetValue('NewValue'))
    do Value::_txtMismatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::_txtMismatch  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('_txtMismatch') & '_value',Choose(p_web.GSV('AccessoryMismatch') <> 1 OR p_web.GSV('ProcessJob') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('AccessoryMismatch') <> 1 OR p_web.GSV('ProcessJob') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('Red Bold')&'">' & p_web.Translate('Please confirm that only the tagged accessory(s) will be sent to the ARC.',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Validate::btnAcceptMismatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnAcceptMismatch',p_web.GetValue('NewValue'))
    do Value::btnAcceptMismatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    AddJobToWaybill(0)  
    DO Refresh:AcceptMismatch
  do SendAlert
  do Value::btnAcceptMismatch  !1

Value::btnAcceptMismatch  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('btnAcceptMismatch') & '_value',Choose(p_web.GSV('AccessoryMismatch') <> 1 OR p_web.GSV('ProcessJob') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('AccessoryMismatch') <> 1 OR p_web.GSV('ProcessJob') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnAcceptMismatch'',''waybillgeneration_btnacceptmismatch_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnAcceptMismatch','Accept','button-entryfield',loc:formname,,,,loc:javascript,0,'images/psave.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillGeneration_' & p_web._nocolon('btnAcceptMismatch') & '_value')


Validate::btnCancelProcess  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnCancelProcess',p_web.GetValue('NewValue'))
    do Value::btnCancelProcess
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    DO Refresh:CancelProcess  
  do SendAlert
  do Value::btnCancelProcess  !1

Value::btnCancelProcess  Routine
  p_web._DivHeader('WaybillGeneration_' & p_web._nocolon('btnCancelProcess') & '_value',Choose(p_web.GSV('ProcessJob') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('ProcessJob') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnCancelProcess'',''waybillgeneration_btncancelprocess_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnCancelProcess','Cancel Process','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillGeneration_' & p_web._nocolon('btnCancelProcess') & '_value')


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('WaybillGeneration_locWaybillJobNNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWaybillJobNNumber
      else
        do Value::locWaybillJobNNumber
      end
  of lower('WaybillGeneration_locWaybillIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWaybillIMEINumber
      else
        do Value::locWaybillIMEINumber
      end
  of lower('WaybillGeneration_locWaybillSecurityPackNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWaybillSecurityPackNumber
      else
        do Value::locWaybillSecurityPackNumber
      end
  of lower('WaybillGeneration_RestrictPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::RestrictPassword
      else
        do Value::RestrictPassword
      end
  of lower('WaybillGeneration_btnProcessJob_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnProcessJob
      else
        do Value::btnProcessJob
      end
  of lower('WaybillGeneration_btnTagAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnTagAll
      else
        do Value::btnTagAll
      end
  of lower('WaybillGeneration_btnUnTagAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnUnTagAll
      else
        do Value::btnUnTagAll
      end
  of lower('WaybillGeneration_btnValidateAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnValidateAccessories
      else
        do Value::btnValidateAccessories
      end
  of lower('WaybillGeneration_btnAcceptMismatch_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnAcceptMismatch
      else
        do Value::btnAcceptMismatch
      end
  of lower('WaybillGeneration_btnCancelProcess_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnCancelProcess
      else
        do Value::btnCancelProcess
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('WaybillGeneration_form:ready_',1)
  p_web.SetSessionValue('WaybillGeneration_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_WaybillGeneration',0)

PreCopy  Routine
  p_web.SetValue('WaybillGeneration_form:ready_',1)
  p_web.SetSessionValue('WaybillGeneration_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_WaybillGeneration',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('WaybillGeneration_form:ready_',1)
  p_web.SetSessionValue('WaybillGeneration_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('WaybillGeneration:Primed',0)

PreDelete       Routine
  p_web.SetValue('WaybillGeneration_form:ready_',1)
  p_web.SetSessionValue('WaybillGeneration_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('WaybillGeneration:Primed',0)
  p_web.setsessionvalue('showtab_WaybillGeneration',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('WaybillGeneration_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('WaybillGeneration_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
        If locWaybillJobNNumber = ''
          loc:Invalid = 'locWaybillJobNNumber'
          loc:alert = p_web.translate('Job Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
        If locWaybillIMEINumber = ''
          loc:Invalid = 'locWaybillIMEINumber'
          loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      If not (RestrictMessage = '')
          RestrictPassword = Upper(RestrictPassword)
          p_web.SetSessionValue('RestrictPassword',RestrictPassword)
        If loc:Invalid <> '' then exit.
      End
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('WaybillGeneration:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locWaybillJobNNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locWaybillIMEINumber')
  p_web.StoreValue('RestrictMessage')
  p_web.StoreValue('locWaybillSecurityPackNumber')
  p_web.StoreValue('RestrictPassword')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('RestrictMessage',RestrictMessage) ! STRING(100)
     p_web.SSV('RestrictPassword',RestrictPassword) ! STRING(20)
     p_web.SSV('locWaybillJobNNumber',locWaybillJobNNumber) ! STRING(30)
     p_web.SSV('locWaybillIMEINumber',locWaybillIMEINumber) ! STRING(30)
     p_web.SSV('locWaybillSecurityPackNumber',locWaybillSecurityPackNumber) ! STRING(30)
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     RestrictMessage = p_web.GSV('RestrictMessage') ! STRING(100)
     RestrictPassword = p_web.GSV('RestrictPassword') ! STRING(20)
     locWaybillJobNNumber = p_web.GSV('locWaybillJobNNumber') ! STRING(30)
     locWaybillIMEINumber = p_web.GSV('locWaybillIMEINumber') ! STRING(30)
     locWaybillSecurityPackNumber = p_web.GSV('locWaybillSecurityPackNumber') ! STRING(30)
AddJobToWaybill     PROCEDURE(LONG pIgnoreAccessories)
locRetainedAccessories  STRING(255)
locSentAccessories      STRING(255)
locAuditNotes           STRING(255)
locAction               STRING(255)
    CODE
        IF (p_web.GSV('wya:WAYBAWTID') < 1)
            ! #13536 Double check for errors (DBH: 20/10/2015)
            p_web.Message('Alert','An error occurred retrieving the waybill details. Please try again.',p_web._MessageClass,Net:Send)
            RETURN
        END ! IF
        
        IF (pIgnoreAccessories <> 1)
            IF (p_web.GSV('job:Ref_Number') < 1)
                ! #13536 Double check for errors (DBH: 20/10/2015)
                p_web.Message('Alert','An error occurred retrieving the jobs details. Please try again.',p_web._MessageClass,Net:Send)
                RETURN
            END ! IF
            
            Access:JOBACC.ClearKey(jac:Ref_Number_Key)
            jac:Ref_Number = p_web.GSV('job:Ref_Number')
            SET(jac:Ref_Number_Key,jac:Ref_Number_Key)
            LOOP UNTIL Access:JOBACC.Next() <> Level:Benign
                IF (jac:Ref_Number <> p_web.GSV('job:Ref_Number'))
                    BREAK
                END ! IF
                IF (jac:Attached = FALSE)
                    locRetainedAccessories = CLIP(locRetainedAccessories) & '<13,10>' & CLIP(jac:Accessory)
                ELSE
                    locSentAccessories = CLIP(locSentAccessories) & '<13,10>' & CLIP(jac:Accessory)
                END ! IF
                
            END ! LOOP
            
            locAuditNotes = ''
            locAction = 'WAYBILL GENERATION ACCESSORY VALIDATION'
            IF (locRetainedAccessories <> '')
                locAuditNotes = 'ACCESSORIES RETAINED:' & CLIP(locRetainedAccessories)
                locAuditNotes = CLIP(locAuditNotes) & '<13,10,13,10>ACCESSORIES SENT TO ARC:' & CLIP(locSentAccessories)
                locAction = CLIP(locAction) & ' (MISMATCH)'
            END ! IF
            
            AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB',CLIP(locAction),CLIP(locAuditNotes))
            
            
        END !IF
        
        IF (Access:WAYBPRO.PrimeRecord() = Level:Benign)
            wyp:AccountNumber = p_web.GSV('Default:AccountNumber')
            wyp:JobNumber = p_web.GSV('wya:JobNumber')
            wyp:ModelNumber = p_web.GSV('wya:ModelNumber')
            wyp:IMEINumber = p_web.GSV('wya:IMEINumber')
            wyp:SecurityPackNumber = p_web.GSV('locWaybillSecurityPackNumber')
            IF (Access:WAYBPRO.TryInsert())
            ELSE
                Access:WAYBAWT.ClearKey(wya:WAYBAWTIDKey)
                wya:WAYBAWTID = p_web.GSV('wya:WAYBAWTID')
                IF (Access:WAYBAWT.TryFetch(wya:WAYBAWTIDKey) = Level:Benign)
                    Access:WAYBAWT.DeleteRecord(0)
                ELSE
                    AddToLog('Debug','Job: ' & CLIP(wyp:JobNumber) & ' - ' & ERROR(),'WAYBAWT Deleting Error',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
                END ! IF

                DO ResetVariables
                
                ProcessCounters() 
                
                !DO RefreshProcessFields
                
                IF (ForceWaybills())
                    
                    !DO RefreshActions

                END ! IF

                
            END ! IF
            
        END ! IF
ForceWaybills       PROCEDURE()!,LONG
retValue                LONG(0)
totalWB                 EQUATE(10)  !Should Be 10
    CODE
        p_web.SSV('ForceWaybill',0)
        p_web.SSV('ForceOBF',0)
        p_web.SSV('ForceNR',0)

        IF (p_web.GSV('locWaybillNR') >= totalWB)
            p_web.SSV('ForceNR',1)
            retValue = 1
        END
        IF (p_web.GSV('locWaybillOBF') >= totalWB)
            p_web.SSV('ForceOBF',1)
            retValue = 1
        END !I F
        
        IF (retValue = 1)
            p_web.SSV('ForceWaybill',1)
            loc:alert = '10 NR/48E or OBF Jobs Have Been Processed.<13,10>You must create the relevant Waybill before you can continue.'
        END ! IF
        
        RETURN retValue
        
ProcessCounters     PROCEDURE()
    CODE
        p_web.SSV('locWaybillOBF',0)
        p_web.SSV('locWaybillNR',0)

        Access:WAYBPRO_ALIAS.clearkey(wyp_ali:AccountJobNumberKey)
        wyp_ali:AccountNumber = p_web.GSV('Default:AccountNumber')
        Set(wyp_ali:AccountJobNumberKey,wyp_ali:AccountJobNumberKey)
        loop
            If Access:WAYBPRO_ALIAS.next() then
                break
            End
            If wyp_ali:AccountNumber <> p_web.GSV('Default:AccountNumber') then
                break
            End
        !now check whic counter to add this jb to

            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = wyp_ali:JobNumber
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !added by Paul 20/04/2010 - log no 10546
            !check the job type
            !open the jobse record
                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber = job:Ref_Number
                If Access:Jobse.fetch(jobe:RefNumberKey) = level:benign then
                    If jobe:OBFvalidated = 1 then
                        p_web.SSV('locWaybillOBF',p_web.GSV('locWaybillOBF') + 1)

                    Else
                        p_web.SSV('locWaybillNR',p_web.GSV('locWaybillNR') + 1)
                    End
                End

            Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !Error
            End !If

        End
ProcessJob          PROCEDURE()
    CODE
        p_web.SSV('ProcessJob',0)
        
        Access:WAYBAWT.ClearKey(wya:AccountJobNumberKey)
        wya:AccountNumber = p_web.GSV('Default:AccountNumber')
        wya:JobNumber = p_web.GSV('locWaybillJobNNumber')
        IF (Access:WAYBAWT.TryFetch(wya:AccountJobNumberKey))
            loc:Alert = 'Cannot find selected Job Number'
            RETURN
        END ! IF
        
        IF (wya:IMEINumber <> p_web.GSV('locWaybillIMEINumber'))
            loc:Alert = 'Cannot find the selected Job And IMEI Number'
            RETURN
        END ! IF
        
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = wya:JobNumber
        IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
            loc:Alert = 'Cannot find the selected Job Number'
            RETURN
        END ! IF
        
        Access:WAYBPRO.ClearKey(wyp:AccountJobNumberKey)
        wyp:AccountNumber =  p_web.GSV('Default:AccountNumber')
        wyp:JobNumber = p_web.GSV('locWaybillJobNNumber')
        IF (Access:WAYBPRO.Fetch(wyp:AccountJobNumberKey) = Level:Benign)
            loc:Alert = 'Error: Selected job has already been processed'
            RETURN
        END ! IF
        
        p_web.FileToSessionQueue(JOBS)
        p_web.FileToSessionQueue(WAYBAWT)
        
        p_web.SSV('ProcessJob',1)
        
        IF (p_web.GSV('job:Exchange_Unit_Number') > 0)
            ! EXchange Unit Attached, skip the accessory validation process
            AddJobToWaybill(1)
            DO Refresh:AcceptMismatch
        END ! IF
