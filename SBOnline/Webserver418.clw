

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER418.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER260.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER419.INC'),ONCE        !Req'd for module callout resolution
                     END


HandoverConfirmation PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
BlankIMEI            BYTE                                  !
BlankJob             BYTE                                  !
NextFocus            STRING(20)                            !
TempAddingString     STRING(60)                            !
NewAccessories       STRING(300)                           !
NewEngineerName      STRING(50)                            !
SendingResult        STRING(255)                           !
NewEngineer          STRING(3)                             !User code
NewJobRef            STRING(10)                            !
NewIMEI              STRING(20)                            !
FilterLocation       STRING(30)                            !
FilesOpened     Long
JOBS::State  USHORT
JOBACC::State  USHORT
HANDOVER::State  USHORT
HANDOJOB::State  USHORT
AUDIT::State  USHORT
WEBJOB::State  USHORT
EXCHANGE::State  USHORT
TRADEACC::State  USHORT
TagFile::State  USHORT
DEFAULT2::State  USHORT
DEFAULTS::State  USHORT
USERS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('HandoverConfirmation')
  loc:formname = 'HandoverConfirmation_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'HandoverConfirmation',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('HandoverConfirmation','')
    p_web._DivHeader('HandoverConfirmation',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferHandoverConfirmation',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferHandoverConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferHandoverConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_HandoverConfirmation',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferHandoverConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_HandoverConfirmation',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'HandoverConfirmation',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
HandoverConfirm     ROUTINE
    
    Do HandoverConfirmDetails       !so I can use EXIT
    
    !Set the CompletedJobs in tagfile to show in list
    CASE SendingResult[1:5] 
    of 'EMPTY'
        !do nothing
    OF 'ERROR' 
        !dont update the completed jobs list ...
        !Show the message
        p_web.Message('Alert',clip(SendingResult),p_web._MessageClass,Net:Send)

        !a few resets
        if BlankJob THEN
            p_web.ssv('NewJobRef','')
            NewJobRef = ''
        END
        if BlankIMEI THEN
            p_web.ssv('NewIMEI','')
            NewIMEI = ''
        END
        
    ELSE
        
        p_web.Message('Alert',clip(SendingResult),p_web._MessageClass,Net:Send)
        
        !after the message - add to the completed list for display
        Access:Tagfile.PrimeRecord()
        tag:sessionID = p_web.SessionID
        tag:taggedValue = clip(p_web.gsv('newJobRef'))
        tag:tagged = 1
        access:Tagfile.insert()

        !Write to the files for the report
        if p_web.gsv('HandoverGenerated') <> 'YES' THEN
            Access:Handover.primerecord()
            han:UserCode            = p_web.GSV('NewEngineer')    
            han:RRCAccountNumber    = p_web.gsv('tra:Account_Number')
            han:HandoverType        = p_web.gsv('HandoverType')  
            Access:handover.update()
            
            p_web.ssv('HandoverGenerated','YES')
            p_web.ssv('han:RecordNumber',han:RecordNumber)
            p_web.ssv('HandoverRecordNo',han:RecordNumber)
           
        END     !if record not already made
        
        !add the handojob record for this job
        !accessories from jobacc where ref_number = ref_number
        NewAccessories   = ''

        Access:JOBACC.clearkey(jac:Ref_Number_Key)
        jac:Ref_Number = p_web.gsv('NewJobRef')
        set(jac:Ref_Number_Key,jac:Ref_Number_Key)
        LOOP
            if access:jobacc.next() then break.
            if jac:Ref_Number <> p_web.gsv('NewJobRef') then break.
            if jac:Damaged then 
                TempAddingString = 'DAMAGED '&clip(jac:Accessory)
            ELSE
                TempAddingString = jac:Accessory
            END
            if clip(NewAccessories) = '' THEN
                NewAccessories = TempAddingString
            ELSE
                NewAccessories = clip(NewAccessories)&', '&clip(TempAddingString)
            END        
        END !loop through jobacc        
        
        !add the record
        Access:handojob.primerecord()
        haj:RefNumber       = p_web.gsv('han:RecordNumber')
        haj:JobNumber       = p_web.gsv('NewJobRef')
        haj:IMEINumber      = p_web.gsv('NewIMEI')
        haj:ModelNumber     = p_web.gsv('NewModelNumber')
        haj:Accessories     = NewAccessories
        Access:handojob.update()
        
        !a few resets
        p_web.ssv('NewJobRef','')
        NewJobRef = ''
        p_web.ssv('NewIMEI','')
        NewIMEI = ''
        
        
    END !case sendingresult 

    EXIT
    
HandoverConfirmDetails      ROUTINE
    
    SendingResult = ''
    BlankIMEI = 0
    BlankJob  = 0
    
    if p_web.gsv('NewIMEI') = '' THEN
        SendingResult = 'EMPTY - You must provide a new IMEI'
        EXIT
    END
    
    if p_web.gsv('NewJobRef') = '' THEN
        SendingResult = 'EMPTY - You must provide a new job number'
        EXIT
    END
    
    if p_web.gsv('NewEngineer') = '' THEN
        SendingResult = 'ERROR - You must provide a new user'
        EXIT
    END

    Access:JOBS.Clearkey(job:Ref_Number_Key)
    job:Ref_Number  = p_web.gsv('NewJobRef')
    If Access:JOBS.Tryfetch(job:Ref_Number_Key) THEN
        SendingResult = 'ERROR - Unable to find the selected job number: '&p_web.gsv('NewJobRef')    
        BlankJob = TRUE   
        BlankIMEI = TRUE    !I would disagree, but they want both to blank J - 18/09/2013        
        EXIT        
    End !if job fetch failed

    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) THEN
        SendingResult = 'ERROR - Unable to find the selected jobse record: '&p_web.gsv('NewJobRef')
        BlankJob = TRUE
        BlankIMEI = TRUE    !I would disagree, but they want both to blank J - 18/09/2013
        EXIT
    END
      
    Access:WEBJOB.Clearkey(wob:RefNumberKey)
    wob:RefNumber   = job:Ref_Number
    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) THEN
        SendingResult = 'ERROR - Unable to find the selected Webjob record: '&p_web.gsv('NewJobRef')
        BlankJob = TRUE
        BlankIMEI = TRUE    !I would disagree, but they want both to blank J - 18/09/2013
        EXIT
    END
        
    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number  = wob:HeadAccountNumber
    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) THEN
        SendingResult = 'ERROR - Unable to find the trade account for job number: '&p_web.gsv('NewJobRef')
        BlankJob = TRUE
        BlankIMEI = TRUE    !I would disagree, but they want both to blank J - 18/09/2013
        EXIT
    END
        
    if tra:SiteLocation <> p_web.gsv('FilterLocation') THEN
        SendingResult = 'ERROR - Job Number not available: '&p_web.gsv('NewJobRef')
        BlankJob  = TRUE
        BlankIMEI = TRUE        
        EXIT
    END
    
    p_web.ssv('tra:Account_Number',tra:Account_Number)
    
    if p_web.gsv('HandoverType') = 'JOB' THEN
        If p_web.gsv('NewIMEI') <> job:ESN
            SendingResult = 'ERROR - The entered IMEI Number does not match the IMEI Number on the selected job.'            
!            BlankJob  = TRUE
             BlankIMEI = TRUE        
            EXIT        
        END !IMEI DOES NOT MATCH
        If jobe:HubRepair = True
            sendingResult = 'ERROR - The selected job is in ARC Control.'
            BlankJob  = TRUE
            BlankIMEI = TRUE        
            Exit    
        END !if hubrepair
        p_web.ssv('NewModelNumber',job:Model_Number)
    ELSE
        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number  = job:Exchange_Unit_Number
        If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) 
            !SendingResult = 'ERROR - the unit '&job:Exchange_Unit_Number&' cannot be found in the exchange table.'
            SendingResult = 'ERROR -The entered IMEI Number does not match the Exchange IMEI Number on the selected job.'
            BlankIMEI = TRUE        
            EXIT
        END
        If xch:ESN <> p_web.gsv('NewIMEI') THEN
            !SendingResult = 'ERROR - the entered IMEI number does not match the exchange unit on the selected job'
            SendingResult = 'ERROR -The entered IMEI Number does not match the Exchange IMEI Number on the selected job.'
            BlankIMEI = TRUE        
            EXIT
        END
        p_web.ssv('NewModelNumber',xch:Model_Number)
    END
    
    !if we get here there are no errors!
    !all this does is to create an audit record
    If p_web.gsv('HandoverType') = 'JOB'    
        AddToAudit(p_web,p_web.gsv('NewJobRef'),'JOB','JOB HAND-OVER CONFIRMED','USER: ' & Clip(p_web.gsv('NewEngineerName')))
    ELSE
        AddToAudit(p_web,p_web.gsv('NewJobRef'),'EXC','EXCHANGE HAND-OVER CONFIRMED','USER: ' & Clip(p_web.gsv('NewEngineerName')))
    END ! !IF
    
!    If Access:AUDIT.PrimeRecord() = Level:Benign
!        aud:Notes       = 'USER: ' & Clip(p_web.gsv('NewEngineerName'))
!        aud:Ref_Number  = p_web.gsv('NewJobRef')
!        aud:Date        = Today()
!        aud:Time        = Clock()
!        aud:Type        = p_web.gsv('HandoverType') 
!        aud:User        = p_web.GSV('BookingUserCode')  
!        If p_web.gsv('HandoverType') = 'JOB'
!            aud:Action  = 'JOB HAND-OVER CONFIRMED'
!        Else ! If func:Type = 'JOB'
!            aud:Action  = 'EXCHANGE HAND-OVER CONFIRMED'
!        End ! If func:Type = 'JOB'
!
!        If Access:AUDIT.TryInsert()
!            ! Insert Failed
!            Access:AUDIT.CancelAutoInc()
!        End 
!    End !If Access:AUDIT.PrimeRecord() = Level:Benign
    
    SendingResult = 'Successfully Updated'      !was 'SUCESSFULLY UPDATED'
    
    EXIT
    
OpenFiles  ROUTINE
  p_web._OpenFile(JOBS)
  p_web._OpenFile(JOBACC)
  p_web._OpenFile(HANDOVER)
  p_web._OpenFile(HANDOJOB)
  p_web._OpenFile(AUDIT)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(TagFile)
  p_web._OpenFile(DEFAULT2)
  p_web._OpenFile(DEFAULTS)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(HANDOVER)
  p_Web._CloseFile(HANDOJOB)
  p_Web._CloseFile(AUDIT)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(DEFAULT2)
  p_Web._CloseFile(DEFAULTS)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  !set up the screen and the defaults
  !some things are done every time - a few defaults are only done the first time ...
  
    IF (p_web.IfExistsValue('HandOverType'))
        p_web.StoreValue('HandOverType')
    END ! IF
    IF (p_web.IfExistsValue('HandoverGenerated'))
        p_web.StoreValue('HandoverGenerated')
    END ! IF
        
  !read files needed
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  access:users.clearkey(use:User_Code_Key)
  use:User_Code = p_web.GSV('BookingUserCode')    
  access:users.fetch(use:User_Code_Key)
  
  !set up the filter used for checking the location of jobs being handed over
  p_web.ssv('FilterLocation',use:Location)!
  FilterLocation = use:location
  
  !preset new Engineer to current user
  NewEngineer = Use:user_Code
  p_web.ssv('NewEngineer',NewEngineer)
  !set up the new engineer's name
  NewEngineerName = clip(use:Forename)&' '&clip(use:Surname)
  p_Web.ssv('NewEngineerName',NewEngineerName)
  
  
  !set variables used by SelectEngineer when called
  p_web.ssv('ReturnURL','HandoverConfirmation')
  p_web.ssv('FilterByEngineer','NO')
  
  
  NextFocus = 'NewJobRef'
  
  !changing to using selectEngineers 
  p_web.SSV('filter:Engineer','Upper(use:Active) = <39>YES<39> And Upper(use:Location) = Upper(<39>' & Clip(use:location) & '<39>)') !And Upper(use:User_Type) = <39>ENGINEER<39>')
  p_web.SetValue('HandoverConfirmation_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  !finishing code here
  !clear all sessionvariables as we cancel the form
  p_web.ssv('SendingResult','')
  p_web.ssv('NewEngineer','')
  p_web.ssv('FilterLocation','')
  p_web.ssv('NewJobRef','')
  p_web.ssv('NewEngineer','')
  p_web.ssv('NewEngineerName','')
  p_web.ssv('FilterByEngineer','')
  
  !and the local variables
  SendingResult = ''
  NewEngineer = ''
  NewEngineerName = ''
  NewJobRef = ''
  FilterLocation = ''
  
  !clear out the tag file used to hold jobs updated
  LOOP
      Access:TagFile.clearkey(tag:keyTagged)
      tag:sessionID = p_web.SessionID
      set(tag:keyTagged,tag:keyTagged)
      if Access:TagFile.next() then break.
      if tag:sessionID <> p_web.SessionID then break.
      Access:TagFile.DeleteRecord(0)
  END !loop

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'NewEngineer'
    p_web.setsessionvalue('showtab_HandoverConfirmation',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(USERS)
      !set up the new engineer's name
          Access:users.ClearKey(use:User_Code_Key)
          use:User_Code = p_web.gsv('NewEngineer')
          if access:users.fetch(use:User_Code_Key) THEN
              use:Forename = 'ENGINEER NOT '
              use:Surname  = 'FOUND '
          !ELSE
              !leave them as found
              !use:Forename & use:Surname  
          END
          NewEngineerName = clip(use:Forename)&' '&clip(use:Surname)
          p_Web.ssv('NewEngineerName',NewEngineerName)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.NewEngineerName')
  End
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('NewEngineer',NewEngineer)
  p_web.SetSessionValue('NewEngineerName',NewEngineerName)
  p_web.SetSessionValue('NewJobRef',NewJobRef)
  p_web.SetSessionValue('NewIMEI',NewIMEI)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('NewEngineer')
    NewEngineer = p_web.GetValue('NewEngineer')
    p_web.SetSessionValue('NewEngineer',NewEngineer)
  End
  if p_web.IfExistsValue('NewEngineerName')
    NewEngineerName = p_web.GetValue('NewEngineerName')
    p_web.SetSessionValue('NewEngineerName',NewEngineerName)
  End
  if p_web.IfExistsValue('NewJobRef')
    NewJobRef = p_web.GetValue('NewJobRef')
    p_web.SetSessionValue('NewJobRef',NewJobRef)
  End
  if p_web.IfExistsValue('NewIMEI')
    NewIMEI = p_web.GetValue('NewIMEI')
    p_web.SetSessionValue('NewIMEI',NewIMEI)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('HandoverConfirmation_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 NewEngineer = p_web.RestoreValue('NewEngineer')
 NewEngineerName = p_web.RestoreValue('NewEngineerName')
 NewJobRef = p_web.RestoreValue('NewJobRef')
 NewIMEI = p_web.RestoreValue('NewIMEI')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferHandoverConfirmation')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('HandoverConfirmation_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('HandoverConfirmation_ChainTo')
    loc:formaction = p_web.GetSessionValue('HandoverConfirmation_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'SubMenuHandovers'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="HandoverConfirmation" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="HandoverConfirmation" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="HandoverConfirmation" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Handover Routine') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Handover Routine',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_HandoverConfirmation">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_HandoverConfirmation" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_HandoverConfirmation')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate(p_web.gsv('HandoverType')&' Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Completed Jobs') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_HandoverConfirmation')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_HandoverConfirmation'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('HandoverConfirmation_BrowseTagFile_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='USERS'
            p_web.SetValue('SelectField',clip(loc:formname) & '.NewJobRef')
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.NewEngineer')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_HandoverConfirmation')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate(p_web.gsv('HandoverType')&' Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_HandoverConfirmation_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate(p_web.gsv('HandoverType')&' Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate(p_web.gsv('HandoverType')&' Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate(p_web.gsv('HandoverType')&' Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate(p_web.gsv('HandoverType')&' Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::NewEngineer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::NewEngineer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::NewEngineer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::NewEngineerName
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::NewEngineerName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::NewEngineerName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::NewJobRef
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::NewJobRef
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::NewJobRef
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::NewIMEI
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::NewIMEI
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::NewIMEI
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Completed Jobs') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_HandoverConfirmation_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Completed Jobs')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Completed Jobs')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Completed Jobs')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Completed Jobs')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseTagFile
      do Comment::BrowseTagFile
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_HandoverConfirmation_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::HandoverReport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::HandoverReport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::NewEngineer  Routine
  p_web._DivHeader('HandoverConfirmation_' & p_web._nocolon('NewEngineer') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('User')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::NewEngineer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('NewEngineer',p_web.GetValue('NewValue'))
    NewEngineer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::NewEngineer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('NewEngineer',p_web.GetValue('Value'))
    NewEngineer = p_web.GetValue('Value')
  End
      p_web.ssv('NewEngineer',NewEngineer)
  p_Web.SetValue('lookupfield','NewEngineer')
  do AfterLookup
  do Value::NewEngineer
  do SendAlert
  do Comment::NewEngineer
  do Prompt::NewEngineerName
  do Value::NewEngineerName  !1

Value::NewEngineer  Routine
  p_web._DivHeader('HandoverConfirmation_' & p_web._nocolon('NewEngineer') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- NewEngineer
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('NewEngineer')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''NewEngineer'',''handoverconfirmation_newengineer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('NewEngineer')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','NewEngineer',p_web.GetSessionValueFormat('NewEngineer'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,'Use button to select engineer') & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('SelectEngineers?LookupField=NewEngineer&Tab=2&ForeignField=use:User_Code&_sort=&Refresh=sort&LookupFrom=HandoverConfirmation&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('HandoverConfirmation_' & p_web._nocolon('NewEngineer') & '_value')

Comment::NewEngineer  Routine
      loc:comment = ''
  p_web._DivHeader('HandoverConfirmation_' & p_web._nocolon('NewEngineer') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('HandoverConfirmation_' & p_web._nocolon('NewEngineer') & '_comment')

Prompt::NewEngineerName  Routine
  p_web._DivHeader('HandoverConfirmation_' & p_web._nocolon('NewEngineerName') & '_prompt',Choose(p_web.gsv('NewEngineerName') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('User Name')
  If p_web.gsv('NewEngineerName') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('HandoverConfirmation_' & p_web._nocolon('NewEngineerName') & '_prompt')

Validate::NewEngineerName  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('NewEngineerName',p_web.GetValue('NewValue'))
    NewEngineerName = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::NewEngineerName
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('NewEngineerName',p_web.GetValue('Value'))
    NewEngineerName = p_web.GetValue('Value')
  End

Value::NewEngineerName  Routine
  p_web._DivHeader('HandoverConfirmation_' & p_web._nocolon('NewEngineerName') & '_value',Choose(p_web.gsv('NewEngineerName') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.gsv('NewEngineerName') = '')
  ! --- DISPLAY --- NewEngineerName
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.Translate(NewEngineerName,) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('HandoverConfirmation_' & p_web._nocolon('NewEngineerName') & '_value')

Comment::NewEngineerName  Routine
    loc:comment = ''
  p_web._DivHeader('HandoverConfirmation_' & p_web._nocolon('NewEngineerName') & '_comment',Choose(p_web.gsv('NewEngineerName') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.gsv('NewEngineerName') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::NewJobRef  Routine
  p_web._DivHeader('HandoverConfirmation_' & p_web._nocolon('NewJobRef') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Reference Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::NewJobRef  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('NewJobRef',p_web.GetValue('NewValue'))
    NewJobRef = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::NewJobRef
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('NewJobRef',p_web.GetValue('Value'))
    NewJobRef = p_web.GetValue('Value')
  End
      p_web.ssv('NewJobRef',NewJobRef)
      do HandoverConfirm 
  do Value::NewJobRef
  do SendAlert
  do Value::BrowseTagFile  !1

Value::NewJobRef  Routine
  p_web._DivHeader('HandoverConfirmation_' & p_web._nocolon('NewJobRef') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- NewJobRef
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('NewJobRef')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''NewJobRef'',''handoverconfirmation_newjobref_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('NewJobRef')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','NewJobRef',p_web.GetSessionValueFormat('NewJobRef'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,'Enter job number') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('HandoverConfirmation_' & p_web._nocolon('NewJobRef') & '_value')

Comment::NewJobRef  Routine
    loc:comment = p_web.Translate('Enter Job Number and press [TAB]')
  p_web._DivHeader('HandoverConfirmation_' & p_web._nocolon('NewJobRef') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::NewIMEI  Routine
  p_web._DivHeader('HandoverConfirmation_' & p_web._nocolon('NewIMEI') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('IMEI')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::NewIMEI  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('NewIMEI',p_web.GetValue('NewValue'))
    NewIMEI = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::NewIMEI
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('NewIMEI',p_web.GetValue('Value'))
    NewIMEI = p_web.GetValue('Value')
  End
      p_web.ssv('NewIMEI',NewIMEI)
      do HandoverConfirm
      
  do Value::NewIMEI
  do SendAlert
  do Value::BrowseTagFile  !1
  do Value::NewJobRef  !1
  do Value::HandoverReport  !1

Value::NewIMEI  Routine
  p_web._DivHeader('HandoverConfirmation_' & p_web._nocolon('NewIMEI') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- NewIMEI
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('NewIMEI')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''NewIMEI'',''handoverconfirmation_newimei_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon(NextFocus)&''',2);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','NewIMEI',p_web.GetSessionValueFormat('NewIMEI'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,'Enter IMEI') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('HandoverConfirmation_' & p_web._nocolon('NewIMEI') & '_value')

Comment::NewIMEI  Routine
    loc:comment = p_web.Translate('Enter relevant IMEI and press [TAB]')
  p_web._DivHeader('HandoverConfirmation_' & p_web._nocolon('NewIMEI') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::BrowseTagFile  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseTagFile',p_web.GetValue('NewValue'))
    do Value::BrowseTagFile
  Else
    p_web.StoreValue('tag:sessionID')
  End

Value::BrowseTagFile  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseTagFile --
  p_web.SetValue('BrowseTagFile:NoForm',1)
  p_web.SetValue('BrowseTagFile:FormName',loc:formname)
  p_web.SetValue('BrowseTagFile:parentIs','Form')
  p_web.SetValue('_parentProc','HandoverConfirmation')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('HandoverConfirmation_BrowseTagFile_embedded_div')&'"><!-- Net:BrowseTagFile --></div><13,10>'
    p_web._DivHeader('HandoverConfirmation_' & lower('BrowseTagFile') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('HandoverConfirmation_' & lower('BrowseTagFile') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseTagFile --><13,10>'
  end
  do SendPacket

Comment::BrowseTagFile  Routine
    loc:comment = ''
  p_web._DivHeader('HandoverConfirmation_' & p_web._nocolon('BrowseTagFile') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::HandoverReport  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('HandoverReport',p_web.GetValue('NewValue'))
    do Value::HandoverReport
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      UpdateUniqueBitText(p_web)
      !
  do Value::HandoverReport
  do SendAlert

Value::HandoverReport  Routine
  p_web._DivHeader('HandoverConfirmation_' & p_web._nocolon('HandoverReport') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''HandoverReport'',''handoverconfirmation_handoverreport_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','HandoverReport','Handover Report','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('HandoverReport?' &'var=' & RANDOM(1,1000))) & ''','''&clip('_blank')&''')',loc:javascript,0,'/images/printer.png',,,,'Print the current session')

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('HandoverConfirmation_' & p_web._nocolon('HandoverReport') & '_value')

Comment::HandoverReport  Routine
    loc:comment = ''
  p_web._DivHeader('HandoverConfirmation_' & p_web._nocolon('HandoverReport') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('HandoverConfirmation_NewEngineer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::NewEngineer
      else
        do Value::NewEngineer
      end
  of lower('HandoverConfirmation_NewEngineer_comment')
    do Comment::NewEngineer
  of lower('HandoverConfirmation_NewJobRef_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::NewJobRef
      else
        do Value::NewJobRef
      end
  of lower('HandoverConfirmation_NewIMEI_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::NewIMEI
      else
        do Value::NewIMEI
      end
  of lower('HandoverConfirmation_HandoverReport_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::HandoverReport
      else
        do Value::HandoverReport
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('HandoverConfirmation_form:ready_',1)
  p_web.SetSessionValue('HandoverConfirmation_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_HandoverConfirmation',0)

PreCopy  Routine
  p_web.SetValue('HandoverConfirmation_form:ready_',1)
  p_web.SetSessionValue('HandoverConfirmation_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_HandoverConfirmation',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('HandoverConfirmation_form:ready_',1)
  p_web.SetSessionValue('HandoverConfirmation_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('HandoverConfirmation:Primed',0)

PreDelete       Routine
  p_web.SetValue('HandoverConfirmation_form:ready_',1)
  p_web.SetSessionValue('HandoverConfirmation_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('HandoverConfirmation:Primed',0)
  p_web.setsessionvalue('showtab_HandoverConfirmation',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('HandoverConfirmation_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('HandoverConfirmation_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('HandoverConfirmation:Primed',0)
  p_web.StoreValue('NewEngineer')
  p_web.StoreValue('NewEngineerName')
  p_web.StoreValue('NewJobRef')
  p_web.StoreValue('NewIMEI')
  p_web.StoreValue('')
  p_web.StoreValue('')
