

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER318.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER303.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseExchangeAudits PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(EXCHAMF)
                      Project(emf:Audit_Number)
                      Project(emf:Audit_Number)
                      Project(emf:Date)
                      Project(emf:User)
                      Project(emf:Status)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
  GlobalErrors.SetProcedureName('BrowseExchangeAudits')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseExchangeAudits:NoForm')
      loc:NoForm = p_web.GetValue('BrowseExchangeAudits:NoForm')
      loc:FormName = p_web.GetValue('BrowseExchangeAudits:FormName')
    else
      loc:FormName = 'BrowseExchangeAudits_frm'
    End
    p_web.SSV('BrowseExchangeAudits:NoForm',loc:NoForm)
    p_web.SSV('BrowseExchangeAudits:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseExchangeAudits:NoForm')
    loc:FormName = p_web.GSV('BrowseExchangeAudits:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseExchangeAudits') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseExchangeAudits')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('adiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(EXCHAMF,emf:Audit_Number_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'EMF:AUDIT_NUMBER') then p_web.SetValue('BrowseExchangeAudits_sort','1')
    ElsIf (loc:vorder = 'EMF:DATE') then p_web.SetValue('BrowseExchangeAudits_sort','2')
    ElsIf (loc:vorder = 'EMF:USER') then p_web.SetValue('BrowseExchangeAudits_sort','4')
    ElsIf (loc:vorder = 'EMF:STATUS') then p_web.SetValue('BrowseExchangeAudits_sort','5')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseExchangeAudits:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseExchangeAudits:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseExchangeAudits:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseExchangeAudits:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseExchangeAudits:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  ! Save Window Name
  AddToLog('NetWebBrowse',p_web.RequestData.DataString,'BrowseExchangeAudits',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseExchangeAudits_sort',net:DontEvaluate)
  If loc:vordernumber = 0
    loc:vordernumber = 1
  End
  p_web.SetSessionValue('BrowseExchangeAudits_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'emf:Audit_Number','-emf:Audit_Number')
    Loc:LocateField = 'emf:Audit_Number'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'emf:Date','-emf:Date')
    Loc:LocateField = 'emf:Date'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(emf:User)','-UPPER(emf:User)')
    Loc:LocateField = 'emf:User'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(emf:Status)','-UPPER(emf:Status)')
    Loc:LocateField = 'emf:Status'
  of 6
    Loc:LocateField = ''
  of 7
    Loc:LocateField = ''
  of 8
    Loc:LocateField = ''
  end
  if loc:vorder = ''
    loc:vorder = '+emf:Audit_Number'
  end
  If False ! add range fields to sort order
  ElsIf (p_web.GSV('locExchangeAuditType') = 1)
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('emf:Audit_Number')
    loc:SortHeader = p_web.Translate('Audit Number')
    p_web.SetSessionValue('BrowseExchangeAudits_LocatorPic','@n_8')
  Of upper('emf:Date')
    loc:SortHeader = p_web.Translate('Date')
    p_web.SetSessionValue('BrowseExchangeAudits_LocatorPic','@d06b')
  Of upper('emf:User')
    loc:SortHeader = p_web.Translate('User Code')
    p_web.SetSessionValue('BrowseExchangeAudits_LocatorPic','@s3')
  Of upper('emf:Status')
    loc:SortHeader = p_web.Translate('Status')
    p_web.SetSessionValue('BrowseExchangeAudits_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseExchangeAudits:LookupFrom')
  End!Else
  loc:formaction = 'BrowseExchangeAudits'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseExchangeAudits:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseExchangeAudits:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseExchangeAudits:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="EXCHAMF"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="emf:Audit_Number_Key"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseExchangeAudits',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseExchangeAudits',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseExchangeAudits.locate(''Locator2BrowseExchangeAudits'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseExchangeAudits.cl(''BrowseExchangeAudits'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseExchangeAudits_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseExchangeAudits_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseExchangeAudits','Audit Number','Click here to sort by Audit Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Audit Number')&'">'&p_web.Translate('Audit Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseExchangeAudits','Date','Click here to sort by Date',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Date')&'">'&p_web.Translate('Date')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowseExchangeAudits','User Code','Click here to sort by User Code',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by User Code')&'">'&p_web.Translate('User Code')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','BrowseExchangeAudits','Status','Click here to sort by Status',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Status')&'">'&p_web.Translate('Status')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  If (p_web.GSV('locExchangeAuditType') <> 1) AND  true
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  JoinAudit
        do AddPacket
        loc:columns += 1
  End ! Field condition
  If (p_web.GSV('locExchangeAuditType') <> 1) AND  true
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  ContinueAudit
        do AddPacket
        loc:columns += 1
  End ! Field condition
  If (p_web.GSV('locExchangeAuditType') = 1) AND  true
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  ReprintAudit
        do AddPacket
        loc:columns += 1
  End ! Field condition
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'emf:Date' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('emf:audit_number',lower(Thisview{prop:order}),1,1) = 0 !and EXCHAMF{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'emf:Audit_Number'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('emf:Audit_Number'),p_web.GetValue('emf:Audit_Number'),p_web.GetSessionValue('emf:Audit_Number'))
  If False  ! Generate Filter
  ElsIf (p_web.GSV('locExchangeAuditType') = 1)
      loc:FilterWas = 'emf:Complete_Flag = 1 AND UPPER(emf:Site_Location) = UPPER(''' & p_web.GSV('BookingSiteLocation') & ''')'
  Else
        loc:FilterWas = 'emf:Complete_Flag = 0 AND UPPER(emf:Site_Location) = UPPER(''' & p_web.GSV('BookingSiteLocation') & ''')'
  End
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseExchangeAudits',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseExchangeAudits_Filter')
    p_web.SetSessionValue('BrowseExchangeAudits_FirstValue','')
    p_web.SetSessionValue('BrowseExchangeAudits_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,EXCHAMF,emf:Audit_Number_Key,loc:PageRows,'BrowseExchangeAudits',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If EXCHAMF{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(EXCHAMF,loc:firstvalue)
              Reset(ThisView,EXCHAMF)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If EXCHAMF{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(EXCHAMF,loc:lastvalue)
            Reset(ThisView,EXCHAMF)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(emf:Audit_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseExchangeAudits.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseExchangeAudits.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseExchangeAudits.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseExchangeAudits.last();',,loc:nextdisabled)
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseExchangeAudits',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseExchangeAudits_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseExchangeAudits_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseExchangeAudits',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseExchangeAudits.locate(''Locator1BrowseExchangeAudits'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseExchangeAudits.cl(''BrowseExchangeAudits'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseExchangeAudits_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseExchangeAudits_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseExchangeAudits.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseExchangeAudits.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseExchangeAudits.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseExchangeAudits.last();',,loc:nextdisabled)
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = emf:Audit_Number
    p_web._thisrow = p_web._nocolon('emf:Audit_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseExchangeAudits:LookupField')) = emf:Audit_Number and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((emf:Audit_Number = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseExchangeAudits.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If EXCHAMF{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(EXCHAMF)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If EXCHAMF{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(EXCHAMF)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','emf:Audit_Number',clip(loc:field),,loc:checked,,,'onclick="BrowseExchangeAudits.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','emf:Audit_Number',clip(loc:field),,'checked',,,'onclick="BrowseExchangeAudits.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::emf:Audit_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::emf:Date
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::emf:User
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::emf:Status
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      If (p_web.GSV('locExchangeAuditType') <> 1) AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::JoinAudit
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      If (p_web.GSV('locExchangeAuditType') <> 1) AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::ContinueAudit
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      If (p_web.GSV('locExchangeAuditType') = 1) AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::ReprintAudit
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseExchangeAudits.omv(this);" onMouseOut="BrowseExchangeAudits.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseExchangeAudits=new browseTable(''BrowseExchangeAudits'','''&clip(loc:formname)&''','''&p_web._jsok('emf:Audit_Number',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('emf:Audit_Number')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseExchangeAudits.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseExchangeAudits.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseExchangeAudits')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseExchangeAudits')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseExchangeAudits')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseExchangeAudits')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(EXCHAMF)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(EXCHAMF)
  Bind(emf:Record)
  Clear(emf:Record)
  NetWebSetSessionPics(p_web,EXCHAMF)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('emf:Audit_Number',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
      ! Save Window Name
      IF (loc:alert <> '')
          AddToLog('Alert',loc:alert,'BrowseExchangeAudits',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
      END ! IF
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(EXCHAMF)
  Case upper(p_web.GetValue('_EIPClm'))
  of upper('JoinAudit')
    do Validate::JoinAudit
  of upper('ContinueAudit')
    do Validate::ContinueAudit
  of upper('ReprintAudit')
    do Validate::ReprintAudit
  End
  p_web._CloseFile(EXCHAMF)
! ----------------------------------------------------------------------------------------
value::emf:Audit_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('emf:Audit_Number_'&emf:Audit_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(emf:Audit_Number,'@n_8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::emf:Date   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('emf:Date_'&emf:Audit_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(emf:Date,'@d06b')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::emf:User   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('emf:User_'&emf:Audit_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(emf:User,'@s3')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::emf:Status   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('emf:Status_'&emf:Audit_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(emf:Status,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
Validate::JoinAudit  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  emf:Audit_Number = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  !#Bryan: Added embed to set extra key components for multi part keys
  loc:result = p_web._GetFile(EXCHAMF,emf:Audit_Number_Key)
  p_web.FileToSessionQueue(EXCHAMF)
  do CheckForDuplicate
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::JoinAudit   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('locExchangeAuditType') <> 1)
    if false
    else
      packet = clip(packet) & p_web._DivHeader('JoinAudit_'&emf:Audit_Number,,net:crc)
      loc:Disabled = Choose(emf:User = p_web.GSV('BookingUserCode'),1,0)
      If '_self'='_self' and Loc:Disabled = 0
        Loc:Disabled = -1
      End
      ! the button is set as a "submit" button, and this must have a URL
      packet = clip(packet) & p_web.CreateButton('submit','JoinAudit','Join Audit','button-flat',loc:formname,p_web._MakeURL(clip('AuditProcess?ProcessType=ExchangeContinueAudit&okURL=FormExchangeAuditProcess&errorURL=FormExchangeAudits')  & '&' & p_web._noColon('emf:Audit_Number')&'='& p_web.escape(emf:Audit_Number) & '&PressedButton='&clip('JoinAudit')),clip('_self'),,,loc:disabled,,,,,) & '<13,10>' !4
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
Validate::ContinueAudit  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  emf:Audit_Number = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  !#Bryan: Added embed to set extra key components for multi part keys
  loc:result = p_web._GetFile(EXCHAMF,emf:Audit_Number_Key)
  p_web.FileToSessionQueue(EXCHAMF)
  do CheckForDuplicate
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::ContinueAudit   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('locExchangeAuditType') <> 1)
    if false
    else
      packet = clip(packet) & p_web._DivHeader('ContinueAudit_'&emf:Audit_Number,,net:crc)
      loc:Disabled = Choose(emf:User <> p_web.GSV('BookingUserCode'),1,0)
      If '_self'='_self' and Loc:Disabled = 0
        Loc:Disabled = -1
      End
      ! the button is set as a "submit" button, and this must have a URL
      packet = clip(packet) & p_web.CreateButton('submit','ContinueAudit','Continue Audit','button-flat',loc:formname,p_web._MakeURL(clip('AuditProcess?ProcessType=ExchangeContinueAudit&okURL=FormExchangeAuditProcess&errorURL=FormExchangeAudits')  & '&' & p_web._noColon('emf:Audit_Number')&'='& p_web.escape(emf:Audit_Number) & '&PressedButton='&clip('ContinueAudit')),clip('_self'),,,loc:disabled,,,,,) & '<13,10>' !4
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
Validate::ReprintAudit  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  emf:Audit_Number = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  !#Bryan: Added embed to set extra key components for multi part keys
  loc:result = p_web._GetFile(EXCHAMF,emf:Audit_Number_Key)
  p_web.FileToSessionQueue(EXCHAMF)
  do CheckForDuplicate
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::ReprintAudit   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('locExchangeAuditType') = 1)
    if false
    else
      packet = clip(packet) & p_web._DivHeader('ReprintAudit_'&emf:Audit_Number,,net:crc)
      loc:disabled = ''
      packet = clip(packet) & p_web.CreateButton('button','ReprintAudit','Reprint Audit','button-flat',loc:formname,,,clip('exchangeAuditReprintOption(' & emf:Audit_Number & ')'),,loc:disabled,,,,,) & '<13,10>' !3
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = emf:Audit_Number

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('emf:Audit_Number',emf:Audit_Number)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('emf:Audit_Number',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('emf:Audit_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('emf:Audit_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
