

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER086.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ForcePostcode        PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !

  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Postcode
    If (def:ForcePostcode = 'B' and func:Type = 'B') Or |
        (def:ForcePostcode <> 'I' and func:Type = 'C')
        Return Level:Fatal
    End!If def:ForcePostcode = 'B''
    Return Level:Benign
