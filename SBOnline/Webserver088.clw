

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER088.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ForceNetwork         PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !

  CODE
    If (GETINI('COMPULSORY','Network',,CLIP(PATH())&'\SB2KDEF.INI') = 'B' and func:Type = 'B') Or |
        (GETINI('COMPULSORY','Network',,CLIP(PATH())&'\SB2KDEF.INI') <> 'I' and func:Type = 'C')
        Return 1
    Else !If GETINI('COMPULSORY','Network',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        Return 0
    End !If GETINI('COMPULSORY','Network',,CLIP(PATH())&'\SB2KDEF.INI') = 1
