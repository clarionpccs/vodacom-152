

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER040.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER038.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER042.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ConvertEstimateParts PROCEDURE  (NetwebServerWorker p_web, *STRING pError)!,LONG ! Declare Procedure
RetValue                    LONG(Level:Benign)
locAddToStockAllocation     LONG(0)
locCreateWebOrder           LONG
locPartNumber               LONG  
locQuantity                 LONG  
locNewPartRecordNumber      LONG
FilesOpened     BYTE(0)

  CODE
        !region ProcessedCode
        DO OpenFiles

        Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
        epr:Part_Number = p_web.GSV('job:Ref_Number')
        SET(epr:Part_Number_Key,epr:Part_Number_Key)
        LOOP UNTIL Access:ESTPARTS.Next() <> Level:Benign
            IF (epr:Part_Number <> p_web.GSV('job:Ref_Number'))
                BREAK
            END ! IF
            
            Access:STOCK.ClearKey(sto:Ref_Number_Key)
            sto:Ref_Number  = epr:Part_Ref_Number
            IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                !region ExchangePart
            
                IF (sto:ExchangeUnit = 'YES')
                    ! This is an exchange unit
                    ! Check for existing unit then exchange part and partNumber
                    Access:PARTS.ClearKey(par:Part_Number_Key)
                    par:Ref_Number = p_web.GSV('job:Ref_Number')
                    par:Part_Number = 'EXCH'
                    IF (Access:PARTS.TryFetch(par:Part_Number_Key) = Level:Benign)
                        ! Found an existing EXCH part entry
                    ELSE ! IF
                        Access:USERS.ClearKey(use:User_Code_Key)
                        use:User_Code = p_web.GSV('job:Engineer')
                        IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
                            ! Find the "exchange stock part" in this user's location
                            Access:STOCK.ClearKey(sto:Location_Key)
                            sto:Location = use:Location
                            sto:Part_Number = 'EXCH'
                            IF (Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign)
                                ! Insert chargeable exchange part entry
                                IF (Access:PARTS.PrimeRecord() = Level:Benign)
                                    par:Part_Ref_Number      = sto:Ref_Number
                                    par:ref_number            = p_web.GSV('job:ref_number')
                                    par:adjustment            = 'YES'
                                    par:part_number           = 'EXCH'
                                    par:description           = Clip(p_web.GSV('job:Manufacturer')) & ' EXCHANGE UNIT'
                                    par:quantity              = 1
                                    par:warranty_part         = 'NO'
                                    par:exclude_from_order    = 'YES'
                                    par:PartAllocated         = 0
                                    par:Status                = 'REQ'
                                    If sto:Assign_Fault_Codes = 'YES'
                                       !Try and get the fault codes. This key should get the only record
                                       Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                                       stm:Ref_Number  = sto:Ref_Number
                                       stm:Part_Number = sto:Part_Number
                                       stm:Location    = sto:Location
                                       stm:Description = sto:Description
                                       If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                                           !Found
                                           par:Fault_Code1  = stm:FaultCode1
                                           par:Fault_Code2  = stm:FaultCode2
                                           par:Fault_Code3  = stm:FaultCode3
                                           par:Fault_Code4  = stm:FaultCode4
                                           par:Fault_Code5  = stm:FaultCode5
                                           par:Fault_Code6  = stm:FaultCode6
                                           par:Fault_Code7  = stm:FaultCode7
                                           par:Fault_Code8  = stm:FaultCode8
                                           par:Fault_Code9  = stm:FaultCode9
                                           par:Fault_Code10 = stm:FaultCode10
                                           par:Fault_Code11 = stm:FaultCode11
                                           par:Fault_Code12 = stm:FaultCode12
                                       Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                                           !Error
                                           !Assert(0,'<13,10>Fetch Error<13,10>')
                                       End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                                    End !If sto:Assign_Fault_Codes = 'YES'                                    
                                    IF (Access:PARTS.TryInsert())
                                        Access:PARTS.CancelAutoInc()
                                    END ! IF
                                END ! IF
                            ELSE ! IF
                            END ! IF
                        ELSE ! IF
                        END ! IF
                        
                        GetStatus(108,0,'EXC',p_web)
                    END ! IF
                    
                    ! Remove the tick so that if the job is made "Unaccepted"
                    ! then you won't have to remove these parts to stock twice
                    IF (epr:UsedOnRepair)
                        epr:UsedOnRepair = 0
                        Access:ESTPARTS.TryUpdate()
                    END ! IF (epr:UsedOnRepair)
                    
                    CYCLE ! Don't carry on
                    
                END ! IF (sto:ExchangeUnit = 'YES')
                !endregion
            END ! IF
            
            locNewPartRecordNumber = 0
            
            IF (Access:PARTS.PrimeRecord() = Level:Benign)
                par:Ref_Number = p_web.GSV('job:Ref_Number')
                par:Adjustment = epr:Adjustment
                par:Part_Number = epr:Part_Number
                par:Description = epr:Description
                par:Supplier = epr:Supplier
                par:Purchase_Cost = epr:Purchase_Cost
                par:Sale_Cost    = epr:Sale_Cost
                par:Retail_Cost  = epr:Retail_Cost
                par:Quantity        = epr:Quantity
                par:Exclude_From_Order  = epr:Exclude_From_Order
                par:Part_Ref_Number  = epr:Part_Ref_Number
                IF (epr:UsedOnRepair)
                    par:PartAllocated    = epr:PartAllocated
                ELSE ! IF (epr:UsedOn)
                    par:PartAllocated   = 0
                    Access:STOCK.ClearKey(sto:Ref_Number_Key)
                    sto:Ref_Number = par:Part_Ref_Number
                    IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                        ! Found stock item
                        IF (sto:Sundry_Item = 'YES')
                            par:PartAllocated = 1
                            par:Date_Ordered = TODAY()
                        ELSE ! IF (sto:Sundry_Item = 'YES')
                            IF (par:Quantity <= sto:Quantity_Stock)
                                ! There are enough items in stock to decrement
                                par:PartAllocated = 0
                                sto:Quantity_Stock -= par:Quantity
                                IF (sto:Quantity_Stock < 0)
                                    sto:Quantity_Stock = 0
                                END ! IF
                                
                                IF (Access:STOCK.TryUpdate())
                                    pError = 'Unable to Decrement Stock Item.'
                                    RetValue = Level:Fatal
                                    
                                    ! undo adding part
                                    Access:PARTS.CancelAutoInc()
                                    BREAK
                                END ! IF (Access:STOCK.TryUpdate())
                                
                                locAddToStockAllocation = 1
                                
                                If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                    'DEC', | ! Transaction_Type
                                    par:Despatch_Note_Number, | ! Depatch_Note_Number
                                    p_web.GSV('job:Ref_Number'), | ! Job_Number
                                    0, | ! Sales_Number
                                    par:Quantity, | ! Quantity
                                    sto:Purchase_Cost, | ! Purchase_Cost
                                    sto:Sale_Cost, | ! Sale_Cost
                                    sto:Retail_Cost, | ! Retail_Cost
                                    'STOCK DECREMENTED', | ! Notes
                                    '', | !Information
                                    p_web.GSV('BookingUserCode'), |
                                    sto:Quantity_Stock) ! Information
                                    ! Added OK
                                Else ! AddToStockHistory
                                    ! Error
                                End ! AddToStockHistory                                
                                
                            ELSE ! IF (par:Quantity <= sto:Quantity_Stock)
                                ! Insufficient Items in STock
                                CreateWebOrder(p_web.GSV('BookingAccount'),par:Part_Number,par:Description,par:Quantity - sto:Quantity_Stock,par:Retail_Cost)
                                
                                IF (sto:Quantity_Stock > 0)
                                    ! There is some stock, use that first
                                    ! and setup to order the rest
                                     locCreateWebOrder = 1
                                     locPartNumber = par:Part_Ref_Number
                                     locQuantity = par:Quantity - sto:Quantity_Stock
                                     par:Quantity = sto:Quantity_Stock
                                     par:Date_Ordered = TODAY()
                                     locAddToStockAllocation = 0
                                ELSE ! IF (sto:Quantit)
                                    par:WebOrder = 1
                                    locAddToStockAllocation = 2
                                END ! IF (sto:Quantit)
                            END ! IF (par:Quantity <= sto:Quantity_Stock)
                        END ! IF (sto:Sundry_Item = 'YES')
                    ELSE ! IF
                    END ! IF
                END ! IF (epr:UsedOn)
                
                par:Fault_Code1  = epr:Fault_Code1
                par:Fault_Code2  = epr:Fault_Code2
                par:Fault_Code3  = epr:Fault_Code3
                par:Fault_Code4  = epr:Fault_Code4
                par:Fault_Code5  = epr:Fault_Code5
                par:Fault_Code6  = epr:Fault_Code6
                par:Fault_Code7  = epr:Fault_Code7
                par:Fault_Code8  = epr:Fault_Code8
                par:Fault_Code9  = epr:Fault_Code9
                par:Fault_Code10  = epr:Fault_Code10
                par:Fault_Code11  = epr:Fault_Code11
                par:Fault_Code12  = epr:Fault_Code12
                par:Fault_Codes_Checked = epr:Fault_Codes_Checked
                par:RRCPurchaseCost        = epr:RRCPurchaseCost
                par:RRCSaleCost            = epr:RRCSaleCost
                par:InWarrantyMarkUp       = epr:InWarrantyMarkUp
                par:OutWarrantyMarkUp      = epr:OutWarrantyMarkUp
                par:RRCAveragePurchaseCost = epr:RRCAveragePurchaseCost
                par:AveragePurchaseCost    = epr:AveragePurchaseCost
                
                IF (Access:PARTS.TryInsert())
                    Access:PARTS.CancelAutoInc()
                    pError = 'Failed to insert chargeable part.'
                    RetValue = Level:Benign
                    BREAK
                END ! IF
                
                locNewPartRecordNumber = par:Record_Number ! Save in case we need to delete it later
                
                
                IF (locCreateWebOrder = 1)
                    Access:STOCK.ClearKey(sto:Ref_Number_Key)
                    sto:Ref_Number = locPartNumber
                    IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                        sto:Quantity_Stock = 0 ! Had already done this above?
                        IF (Access:STOCK.TryUpdate())
                            pError = 'Unable to update Stock Part.'
                            RetValue = Level:Fatal
                            
                            ! Failed, so delete the new part added
                            Access:PARTS.ClearKey(par:recordnumberkey)
                            par:Record_Number = locNewPartRecordNumber
                            IF (Access:PARTS.TryFetch(par:recordnumberkey) = Level:Benign)
                                Access:PARTS.DeleteRecord(0)
                            END ! IF
                            BREAK
                            
                        END ! IF
                        ! Add part to stock allocation list
                        CASE locAddToStockAllocation
                        OF 1!
                            AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'',p_web.GSV('job:Engineer'),p_web)
                        OF 2
                            AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'WEB',p_web.GSV('job:Engineer'),p_web)
                        END ! CASE
                        
                        ! Create new part for incoming order
                        Access:PARTS_ALIAS.ClearKey(par_ali:RefPartRefNoKey)
                        par_ali:Ref_Number = p_web.GSV('job:Ref_Number')
                        par_ali:Part_Ref_Number = locPartNumber
                        IF (Access:PARTS_ALIAS.TryFetch(par_ali:RefPartRefNoKey) = Level:Benign)
                            IF (Access:PARTS_ALIAS.PrimeRecord() = Level:Benign)
                                par:ref_number           = par_ali:ref_number
                                par:adjustment           = par_ali:adjustment
                                par:part_ref_number      = par_ali:part_ref_number

                                par:part_number     = par_ali:part_number
                                par:description     = par_ali:description
                                par:supplier        = par_ali:supplier
                                par:purchase_cost   = par_ali:purchase_cost
                                par:sale_cost       = par_ali:sale_cost
                                par:retail_cost     = par_ali:retail_cost
                                par:quantity             = locQuantity
                                par:warranty_part        = 'NO'
                                par:exclude_from_order   = 'NO'
                                par:despatch_note_number = ''
                                par:date_ordered         = ''
                                par:date_received        = ''
                                par:pending_ref_number   = ''
                                par:order_number         = ''
                                par:fault_code1    = par_ali:fault_code1
                                par:fault_code2    = par_ali:fault_code2
                                par:fault_code3    = par_ali:fault_code3
                                par:fault_code4    = par_ali:fault_code4
                                par:fault_code5    = par_ali:fault_code5
                                par:fault_code6    = par_ali:fault_code6
                                par:fault_code7    = par_ali:fault_code7
                                par:fault_code8    = par_ali:fault_code8
                                par:fault_code9    = par_ali:fault_code9
                                par:fault_code10   = par_ali:fault_code10
                                par:fault_code11   = par_ali:fault_code11
                                par:fault_code12   = par_ali:fault_code12
                                par:WebOrder = 1                                                            
                                IF (Access:PARTS.TryInsert())
                                    Access:PARTS.CancelAutoInc()
                                    
                                END ! IF
                                AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'WEB',p_web.GSV('job:Engineer'),p_web) 
                            END ! IF

                        ELSE ! IF
                        END ! IF
                        
                    ELSE ! IF
                    END ! IF
                ELSE ! IF (locCreateWebOrder = 1)
                END ! IF (locCreateWebOrder = 1)
            END ! IF
            ! Remove the tick so that if the job is made "Unaccepted"
            ! then you won't have to remove these parts to stock twice
            IF (epr:UsedOnRepair)
                epr:UsedOnRepair = 0
                Access:ESTPARTS.TryUpdate()
            END ! IF (epr:UsedOnRepair)            
            
        END ! LOOP


        DO CloseFiles
        
        RETURN RetValue
        
        !endregion

!!region CodeRefactor to add possiblities of errors
!        do openFiles
!
!    tmp:AddToStockAllocation = 0
!
!    access:estparts.clearkey(epr:part_number_key)
!    epr:ref_number  = p_web.GSV('job:ref_number')
!    set(epr:part_number_key,epr:part_number_key)
!    loop
!        if access:estparts.next()
!           break
!        end !if
!        if epr:ref_number  <> p_web.GSV('job:ref_number')      |
!            then break.  ! end if
!        yldcnt# += 1
!        if yldcnt# > 25
!           yield() ; yldcnt# = 0
!        end !if
!
!        Access:STOCK.ClearKey(sto:ref_number_key)
!        sto:ref_number = epr:Part_Ref_Number
!        if not Access:STOCK.Fetch(sto:ref_number_key)
!            if sto:ExchangeUnit = 'YES'
!
!                !Check for existing unit - then exchange
!                !chargeable parts and partnumber = EXCH?
!                access:parts.clearkey(par:Part_Number_Key)
!                par:Ref_Number = p_web.GSV('job:ref_number')
!                par:Part_Number = 'EXCH'
!                if access:parts.fetch(par:Part_NUmber_Key) = level:benign
!                    !found an existing Exchange unit
!                ELSE
!
!                    Access:USERS.Clearkey(use:User_Code_Key)
!                    use:User_Code   = p_web.GSV('job:Engineer')
!                    If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!                        !Found
!                        Access:STOCK.Clearkey(sto:Location_Key)
!                        sto:Location    = use:Location
!                        sto:Part_Number = 'EXCH'
!                        If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
!                            access:location.clearkey(loc:Location_Key)
!                            loc:Location = use:location
!                            access:location.fetch(loc:Location_Key)
!                            !changed 19/11 alway allocate at once
!                            !change back 20/11
!!                            if loc:UseRapidStock then
!                                !Found
!                                Local.AllocateExchangePart('CHA',0)
!!                            ELSE
!!                                ExchangeUnitNumber# = p_web.GSV('job:Exchange_Unit_Number')
!!                                ViewExchangeUnit()
!!                                Access:JOBS.TryUpdate()
!!                                ! Inserting (DBH 16/09/2008) # 10253 - Update Date/Time Stamp
!!                                UpdateDateTimeStamp(p_web.GSV('job:Ref_Number'))
!!                                ! End (DBH 16/09/2008) #10253
!!                                If p_web.GSV('job:Exchange_Unit_Number') <> ExchangeUnitNumber#
!!                                    Local.AllocateExchangePart('CHA',1)
!!                                End !If p_web.GSV('job:Exchange_Unit_Number <> ExchangeUnitNumber#
!!                            END !if loc:useRapidStock
!                        Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
!                            !Error
!                        End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
!                    Else ! If Access:USERS.Tryfetch(use:User_Code_KEy) = Level:Benign
!                        !Error
!                    End !If Access:USERS.Tryfetch(use:User_Code_KEy) = Level:Benign
!
!                    p_web.SSV('GetStatus:Type','EXC')
!                    p_web.SSV('GetStatus:StatusNumber',108)
!                    getStatus(108,0,'EXC',p_web)
!                End !If exchang unit already existed
!
!                !Remove the tick so that if the job is made "Unaccpeted"
!                !then you won't have to remove these parts to stock twice
!                If epr:UsedOnRepair
!                    epr:UsedOnRepair = 0
!                    Access:ESTPARTS.Update()
!                End !If epr:UsedOnRepair
!                cycle
!            end
!        end
!
!        get(parts,0)
!        glo:select1 = ''
!        if access:parts.primerecord() = level:benign
!            par:ref_number           = p_web.GSV('job:ref_number')
!            par:adjustment           = epr:adjustment
!            par:part_number     = epr:part_number
!            par:description     = epr:description
!            par:supplier        = epr:supplier
!            par:purchase_cost   = epr:purchase_cost
!            par:sale_cost       = epr:sale_cost
!            par:retail_cost     = epr:retail_cost
!            par:quantity             = epr:quantity
!            par:exclude_from_order   = epr:exclude_from_order
!            par:part_ref_number      = epr:part_ref_number
!            !If not used, then mark to be allocated
!            If epr:UsedOnRepair
!                par:PartAllocated       = epr:PartAllocated
!            Else !If epr:UsedOnRepair
!                par:PartAllocated       = 0
!                Access:STOCK.Clearkey(sto:Ref_Number_Key)
!                sto:Ref_Number  = par:Part_Ref_Number
!                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                    !Found
!                    IF (sto:Sundry_Item = 'YES') ! ! #11837 Don't decrement a sundry item. (Bryan: 08/12/2010)
!                        par:PartAllocated = 1
!                        par:Date_Ordered = TODAY()
!                    ELSE
!                        If par:Quantity <= sto:Quantity_Stock
!                            !If its not a Rapid site, turn part allocated off
!!                        If RapidLocation(sto:Location)
!                            par:PartAllocated = 0
!!                        End !If RapidLocation(sto:Location) = Level:Benign
!                            sto:Quantity_Stock  -= par:Quantity
!                            If sto:Quantity_Stock < 0
!                                sto:Quantity_Stock = 0
!                            End !If sto:Quantity_Stock < 0
!
!                            tmp:AddToStockAllocation = 1
!
!                            If Access:STOCK.Update() = Level:Benign
!                                If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
!                                    'DEC', | ! Transaction_Type
!                                    par:Despatch_Note_Number, | ! Depatch_Note_Number
!                                    p_web.GSV('job:Ref_Number'), | ! Job_Number
!                                    0, | ! Sales_Number
!                                    par:Quantity, | ! Quantity
!                                    sto:Purchase_Cost, | ! Purchase_Cost
!                                    sto:Sale_Cost, | ! Sale_Cost
!                                    sto:Retail_Cost, | ! Retail_Cost
!                                    'STOCK DECREMENTED', | ! Notes
!                                    '', | !Information
!                                    p_web.GSV('BookingUserCode'), |
!                                    sto:Quantity_Stock) ! Information
!                                    ! Added OK
!                                Else ! AddToStockHistory
!                                    ! Error
!                                End ! AddToStockHistory
!                            End! If Access:STOCK.Update() = Level:Benign
!                        Else !If par:Quantity < sto:Quantity
!
!                            CreateWebOrder(p_web.GSV('BookingAccount'),par:Part_Number,par:Description,par:Quantity - sto:Quantity_Stock,par:Retail_Cost)
!                            If sto:Quantity_Stock > 0
!                                locCreateWebOrder = 1
!                                locPartNumber = par:Part_Ref_Number
!                                locQuantity = par:Quantity - sto:Quantity_Stock
!                                !glo:Select1 = 'NEW PENDING WEB'
!                                !glo:Select2 = par:Part_Ref_Number
!                                !glo:Select3 = par:Quantity - sto:Quantity_Stock
!                                !glo:Select4 =
!                                par:Quantity    = sto:Quantity_Stock
!                                par:Date_Ordered    = Today()
!                                tmp:AddToStockAllocation = 0
!                            Else !If sto:Quantity_Stock > 0
!                                par:WebOrder    = 1
!                                tmp:AddToStockAllocation = 2
!                            End !If sto:Quantity_Stock > 0
!                        End !If par:Quantity > sto:Quantity
!                    END
!                    
!                Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                    !Error
!                End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!
!            End !If epr:UsedOnRepair
!
!            par:Fault_Code1         = epr:Fault_Code1
!            par:Fault_Code2         = epr:Fault_Code2
!            par:Fault_Code3         = epr:Fault_Code3
!            par:Fault_Code4         = epr:Fault_Code4
!            par:Fault_Code5         = epr:Fault_Code5
!            par:Fault_Code6         = epr:Fault_Code6
!            par:Fault_Code7         = epr:Fault_Code7
!            par:Fault_Code8         = epr:Fault_Code8
!            par:Fault_Code9         = epr:Fault_Code9
!            par:Fault_Code10        = epr:Fault_Code10
!            par:Fault_Code11        = epr:Fault_Code11
!            par:Fault_Code12        = epr:Fault_Code12
!            par:Fault_Codes_Checked = epr:Fault_Codes_Checked
!            !Neil
!            par:RRCPurchaseCost        = epr:RRCPurchaseCost
!            par:RRCSaleCost            = epr:RRCSaleCost
!            par:InWarrantyMarkUp       = epr:InWarrantyMarkUp
!            par:OutWarrantyMarkUp      = epr:OutWarrantyMarkUp
!            par:RRCAveragePurchaseCost = epr:RRCAveragePurchaseCost
!            par:AveragePurchaseCost    = epr:AveragePurchaseCost
!
!            access:parts.insert()
!
!            !Add part to stock allocation list - L873 (DBH: 11-08-2003)
!            Case tmp:AddToStockAllocation
!            Of 1
!                p_web.SSV('AddToStockAllocation:Type','CHA')
!                p_web.SSV('AddToStockAllocation:Status','')
!                p_web.SSV('AddToStockAllocation:Qty',par:Quantity)
!                AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'',p_web.GSV('job:Engineer'),p_web)
!            Of 2
!                p_web.SSV('AddToStockAllocation:Type','CHA')
!                p_web.SSV('AddToStockAllocation:Status','WEB')
!                p_web.SSV('AddToStockAllocation:Qty',par:Quantity)
!                AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'WEB',p_web.GSV('job:Engineer'),p_web)
!            End !If tmp:AddToStockAllocation
!
!            If (locCreateWebOrder = 1)
!                access:stock.clearkey(sto:ref_number_key)
!                sto:ref_number = locPartNumber
!                If access:stock.fetch(sto:ref_number_key)
!                Else!If access:stock.fetch(sto:ref_number_key)
!                    sto:quantity_stock     = 0
!                    access:stock.update()
!
!                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
!                                         'DEC', | ! Transaction_Type
!                                         par:Despatch_Note_Number, | ! Depatch_Note_Number
!                                         p_web.GSV('job:Ref_Number'), | ! Job_Number
!                                         0, | ! Sales_Number
!                                         sto:Quantity_stock, | ! Quantity
!                                         par:Purchase_Cost, | ! Purchase_Cost
!                                         par:Sale_Cost, | ! Sale_Cost
!                                         par:Retail_Cost, | ! Retail_Cost
!                                         'STOCK DECREMENTED', | ! Notes
!                                         '', |
!                                         p_web.GSV('BookingUserCode'), |
!                                         sto:Quantity_Stock) ! Information
!                        ! Added OK
!                    Else ! AddToStockHistory
!                        ! Error
!                    End ! AddToStockHistory
!
!                    access:parts_alias.clearkey(par_ali:RefPartRefNoKey)
!                    par_ali:ref_number      = p_web.GSV('job:ref_number')
!                    par_ali:part_ref_number = par:part_Ref_Number
!                    If access:parts_alias.fetch(par_ali:RefPartRefNoKey) = Level:Benign
!                        par:ref_number           = par_ali:ref_number
!                        par:adjustment           = par_ali:adjustment
!                        par:part_ref_number      = par_ali:part_ref_number
!
!                        par:part_number     = par_ali:part_number
!                        par:description     = par_ali:description
!                        par:supplier        = par_ali:supplier
!                        par:purchase_cost   = par_ali:purchase_cost
!                        par:sale_cost       = par_ali:sale_cost
!                        par:retail_cost     = par_ali:retail_cost
!                        par:quantity             = locQuantity
!                        par:warranty_part        = 'NO'
!                        par:exclude_from_order   = 'NO'
!                        par:despatch_note_number = ''
!                        par:date_ordered         = ''
!                        par:date_received        = ''
!                        par:pending_ref_number   = ''
!                        par:order_number         = ''
!                        par:fault_code1    = par_ali:fault_code1
!                        par:fault_code2    = par_ali:fault_code2
!                        par:fault_code3    = par_ali:fault_code3
!                        par:fault_code4    = par_ali:fault_code4
!                        par:fault_code5    = par_ali:fault_code5
!                        par:fault_code6    = par_ali:fault_code6
!                        par:fault_code7    = par_ali:fault_code7
!                        par:fault_code8    = par_ali:fault_code8
!                        par:fault_code9    = par_ali:fault_code9
!                        par:fault_code10   = par_ali:fault_code10
!                        par:fault_code11   = par_ali:fault_code11
!                        par:fault_code12   = par_ali:fault_code12
!                        par:WebOrder = 1
!                        access:parts.insert()
!                        p_web.SSV('AddToStockAllocation:Type','CHA')
!                        p_web.SSV('AddToStockAllocation:Status','WEB')
!                        p_web.SSV('AddToStockAllocation:Qty',par:Quantity)
!                        AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'WEB',p_web.GSV('job:Engineer'),p_web) 
!                    end !If access:parts_alias.clearkey(par_ali:part_ref_number_key) = Level:Benign
!
!                end !if access:stock.fetch(sto:ref_number_key = Level:Benign
!            End !If glo:Select1 = 'NEW PENDING WEB'
!            !End
!        end!if access:parts.primerecord() = level:benign
!
!        !Remove the tick so that if the job is made "Unaccpeted"
!        !then you won't have to remove these parts to stock twice
!        If epr:UsedOnRepair
!            epr:UsedOnRepair = 0
!            Access:ESTPARTS.Update()
!        End !If epr:UsedOnRepair
!    end !loop
!
!    do closeFiles
!!endregion
!--------------------------------------
OpenFiles  ROUTINE
  Access:PARTS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS_ALIAS.Open                                  ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS_ALIAS.UseFile                               ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOMODEL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOMODEL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ESTPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ESTPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:USERS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:USERS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:PARTS.Close
     Access:PARTS_ALIAS.Close
     Access:STOMODEL.Close
     Access:STOCK.Close
     Access:ESTPARTS.Close
     Access:USERS.Close
     FilesOpened = False
  END
!Local.AllocateExchangePart      Procedure(String    func:Type,Byte  func:Allocated)
!Code
!        Case func:Type
!            Of 'WAR'
!                get(warparts,0)
!                if access:warparts.primerecord() = level:benign
!                    wpr:PArt_Ref_Number      = sto:Ref_Number
!                    wpr:ref_number            = p_web.GSV('job:ref_number')
!                    wpr:adjustment            = 'YES'
!                    wpr:part_number           = 'EXCH'
!                    wpr:description           = Clip(p_web.GSV('job:Manufacturer')) & ' EXCHANGE UNIT'
!                    wpr:quantity              = 1
!                    wpr:warranty_part         = 'NO'
!                    wpr:exclude_from_order    = 'YES'
!                    wpr:PartAllocated         = func:Allocated !was func:Allocated but see VP116 - must always be allocated straight off!
!                                                  !changed back from '1' 20/11 - original system reinstalled
!                    if func:Allocated = 0 then
!                        wpr:Status            = 'REQ'
!                    ELSE
!                        WPR:Status            = 'PIK'
!                    END
!                    If sto:Assign_Fault_Codes = 'YES'
!                        !Try and get the fault codes. This key should get the only record
!                        Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
!                        stm:Ref_Number  = sto:Ref_Number
!                        stm:Part_Number = sto:Part_Number
!                        stm:Location    = sto:Location
!                        stm:Description = sto:Description
!                        If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
!                            !Found
!                            wpr:Fault_Code1  = stm:FaultCode1
!                            wpr:Fault_Code2  = stm:FaultCode2
!                            wpr:Fault_Code3  = stm:FaultCode3
!                            wpr:Fault_Code4  = stm:FaultCode4
!                            wpr:Fault_Code5  = stm:FaultCode5
!                            wpr:Fault_Code6  = stm:FaultCode6
!                            wpr:Fault_Code7  = stm:FaultCode7
!                            wpr:Fault_Code8  = stm:FaultCode8
!                            wpr:Fault_Code9  = stm:FaultCode9
!                            wpr:Fault_Code10 = stm:FaultCode10
!                            wpr:Fault_Code11 = stm:FaultCode11
!                            wpr:Fault_Code12 = stm:FaultCode12
!                        Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
!                        !Error
!                        !Assert(0,'<13,10>Fetch Error<13,10>')
!                        End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
!                    end !If sto:Assign_Fault_Codes = 'YES'
!                    if access:warparts.insert()
!                        access:warparts.cancelautoinc()
!                    end
!                End
!
!            Of 'CHA'
!                get(parts,0)
!                if access:parts.primerecord() = level:benign
!                    par:PArt_Ref_Number      = sto:Ref_Number
!                    par:ref_number            = p_web.GSV('job:ref_number')
!                    par:adjustment            = 'YES'
!                    par:part_number           = 'EXCH'
!                    par:description           = Clip(p_web.GSV('job:Manufacturer')) & ' EXCHANGE UNIT'
!                    par:quantity              = 1
!                    par:warranty_part         = 'NO'
!                    par:exclude_from_order    = 'YES'
!                    par:PartAllocated         = func:Allocated
!                    !see above for confusion
!                    if func:allocated = 0 then
!                        par:Status            = 'REQ'
!                    ELSE
!                        par:status            = 'PIK'
!                    END
!                    If sto:Assign_Fault_Codes = 'YES'
!                       !Try and get the fault codes. This key should get the only record
!                       Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
!                       stm:Ref_Number  = sto:Ref_Number
!                       stm:Part_Number = sto:Part_Number
!                       stm:Location    = sto:Location
!                       stm:Description = sto:Description
!                       If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
!                           !Found
!                           par:Fault_Code1  = stm:FaultCode1
!                           par:Fault_Code2  = stm:FaultCode2
!                           par:Fault_Code3  = stm:FaultCode3
!                           par:Fault_Code4  = stm:FaultCode4
!                           par:Fault_Code5  = stm:FaultCode5
!                           par:Fault_Code6  = stm:FaultCode6
!                           par:Fault_Code7  = stm:FaultCode7
!                           par:Fault_Code8  = stm:FaultCode8
!                           par:Fault_Code9  = stm:FaultCode9
!                           par:Fault_Code10 = stm:FaultCode10
!                           par:Fault_Code11 = stm:FaultCode11
!                           par:Fault_Code12 = stm:FaultCode12
!                       Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
!                           !Error
!                           !Assert(0,'<13,10>Fetch Error<13,10>')
!                       End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
!                    End !If sto:Assign_Fault_Codes = 'YES'
!                    if access:parts.insert()
!                        access:parts.cancelautoinc()
!                    end
!                End !If access:Prime
!        End !Case func:Type
