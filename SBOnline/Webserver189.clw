

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER189.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
PushPack             PROCEDURE  (NetWebServerWorker p_web, string p_packet) ! Declare Procedure
packetlen   long

  CODE
  packetlen = len(clip(p_packet))
  if packetlen > 0
    p_web.ParseHTML(p_packet, 1, packetlen, NET:NoHeader)
    p_packet = ''
    packetlen = 0
  end
