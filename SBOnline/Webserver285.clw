

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER285.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER013.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER280.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER286.INC'),ONCE        !Req'd for module callout resolution
                     END


JobSearch            PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locJobPassword       STRING(30)                            !
locSearchJobNumber   STRING(20)                            !
locPasswordMessage   STRING(100)                           !
tmp:ExchangeStatusDate STRING(20)                          !
tmp:ExchangeIMEI     STRING(20)                            !
tmp:ExchangeModel    STRING(40)                            !
tmp:LoanStatusDate   STRING(20)                            !
tmp:LoanImei         STRING(20)                            !
tmp:LoanModel        STRING(40)                            !
Title:password       STRING(20)                            !
Title:Job            STRING(20)                            !
Title:Exchange       STRING(20)                            !
Title:Loan           STRING(20)                            !
FilesOpened     Long
EXCHANGE::State  USHORT
AUDSTATS::State  USHORT
USERS::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
WEBJOB::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('JobSearch')
  loc:formname = 'JobSearch_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'JobSearch',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('JobSearch','Change')
    p_web._DivHeader('JobSearch',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferJobSearch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobSearch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobSearch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_JobSearch',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobSearch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_JobSearch',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'JobSearch',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
DeleteSessionValues ROUTINE ! Auto generated by Bryan's Template
    ! Local Variables
    p_web.DeleteSessionValue('Ans')
    p_web.DeleteSessionValue('locJobPassword')
    p_web.DeleteSessionValue('locSearchJobNumber')
    p_web.DeleteSessionValue('locPasswordMessage')
    p_web.DeleteSessionValue('tmp:ExchangeStatusDate')
    p_web.DeleteSessionValue('tmp:ExchangeIMEI')
    p_web.DeleteSessionValue('tmp:ExchangeModel')
    p_web.DeleteSessionValue('tmp:LoanStatusDate')
    p_web.DeleteSessionValue('tmp:LoanImei')
    p_web.DeleteSessionValue('tmp:LoanModel')
    p_web.DeleteSessionValue('Title:password')
    p_web.DeleteSessionValue('Title:Job')
    p_web.DeleteSessionValue('Title:Exchange')
    p_web.DeleteSessionValue('Title:Loan')

    ! Other Variables
RefreshJobDetails       ROUTINE
    DO Prompt::job:Account_Number
    DO VAlue::job:Account_Number
    DO Prompt::job:Model_Number
    DO Value::job:Model_Number
    DO Prompt::job:Date_Booked
    DO VAlue::job:Date_Booked
    DO Prompt::job:Current_Status
    DO Value::job:Current_Status
    DO Prompt::job:Location
    DO Value::job:Location
    Do Prompt::Job:ESN
    Do Value::Job:ESN

ShowHidePaymentDetails      ROUTINE
DATA
locPaymentStringRRC EQUATE('PAYMENT TYPES')
locPaymentStringARC EQUATE('PAYMENT DETAILS')
locPaymentString CSTRING(30)
CODE
    
    IF (p_web.GSV('locJobFound') = 1)
        paymentFail# = 0
        
        IF (p_web.GSV('BookingSite') = 'RRC')
            locPaymentString = locPaymentStringRRC
        ELSE
            locPaymentString = locPaymentStringARC
        END
    
        IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),locPaymentString) = 0)
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = p_web.GSV('locSearchJobNumber')
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                IF (job:Loan_Unit_Number = 0)
                    IF (job:Warranty_Job = 'YES' AND job:Chargeable_Job <> 'YES')
                        paymentFail# = 1
                    END
                END
            END
            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                IF (wob:HeadAccountNumber <> p_web.GSV('BookingAccount'))
                    ! #13531 Do not show payment screen for different RRC (DBH: 13/05/2015)
                    paymentFail# = 1
                END ! IF
            END ! IF
            
        ELSE
            paymentFail# = 1
        END
        IF (paymentFail# = 1)
            p_web.SSV('Hide:PaymentDetails',1)
            p_web.SSV('Comment:PaymentDetails','')
        ELSE
            p_web.SSV('Hide:PaymentDetails',0)
        END
    END
    
LookupLoanExchangeDetsails  ROUTINE
    
!TB13095 - adding loan and exchange details when a job is found    
!Use Audstats for the dates
    
    tmp:LoanStatusDate = ''
    Access:AUDSTATS.ClearKey(aus:DateChangedKey)
    aus:RefNumber   = job:Ref_Number
    aus:Type        = 'LOA'
    aus:DateChanged = Today()
    Set(aus:DateChangedKey,aus:DateChangedKey)
    Loop
        If Access:AUDSTATS.PREVIOUS() then BREAK.
        If aus:RefNumber   <> job:Ref_Number      |
        Or aus:Type        <> 'LOA'      |
            Then Break.  ! End If
        if aus:NewStatus = '101 NOT ISSUED' then break.     !don't show the date if it has not been issued
        tmp:LoanStatusDate  =  format(aus:DateChanged,@d06)
        Break
    End !Loop
    
    tmp:ExchangeStatusDate = ''
    Access:AUDSTATS.ClearKey(aus:DateChangedKey)
    aus:RefNumber   = job:Ref_Number
    aus:Type        = 'EXC'
    aus:DateChanged = Today()
    Set(aus:DateChangedKey,aus:DateChangedKey)
    Loop
        If Access:AUDSTATS.PREVIOUS()
           Break
        End !If
        If aus:RefNumber   <> job:Ref_Number      |
        Or aus:Type        <> 'EXC'      |
            Then Break.  ! End If
        if aus:NewStatus = '101 NOT ISSUED' then break.     !don't show the date if it has not been issued
        tmp:ExchangeStatusDate  =  format(aus:DateChanged,@d06)
        Break
    End !Loop

!for the text look up the relevant tables
    tmp:ExchangeIMEI  = ''
    tmp:ExchangeModel = ''
    If job:Exchange_Unit_Number <> ''
        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number  = job:Exchange_Unit_Number
        If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            !Found
            tmp:ExchangeIMEI = xch:ESN
            tmp:ExchangeModel = xch:Model_Number
        Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            !Error
        End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
    End !job:Exchange_Unit_Number <> ''
    
    tmp:LoanImei = ''
    tmp:LoanModel = ''
    If job:Loan_Unit_Number <> ''
        Access:LOAN.Clearkey(loa:Ref_Number_Key)
        loa:Ref_Number  = job:Loan_Unit_Number
        If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
            !Found
            tmp:LoanImei    = loa:ESN
            tmp:LoanModel   = loa:Model_Number
        Else ! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
            !Error
        End !If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
    End !job:Loan_Unit_Number <> ''
    
    EXIT
OpenFiles  ROUTINE
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(AUDSTATS)
  p_web._OpenFile(USERS)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(WEBJOB)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(AUDSTATS)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WEBJOB)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  ! Clear Fields
  DO DeleteSessionValues
  p_web.SSV('Hide:PaymentDetails',1)
  p_web.SSV('Hide:ViewJobButton',1)
  p_web.SSV('comment:locSearchJobNumber','Enter Job Number and press [TAB]')
  p_web.SSV('comment:locJobPassword','Enter Password and press [TAB]')
  p_web.SSV('locJobFound',0)
  p_web.SSV('locPasswordRequired',0)
  
  p_web.site.CancelButton.TextValue = 'Close'
  
  ClearUpdateJobVariables(p_web)
  p_web.SetValue('JobSearch_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  DO DeleteSessionValues

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:date_booked')
    p_web.SetPicture('job:date_booked','@d06')
  End
  p_web.SetSessionPicture('job:date_booked','@d06')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locSearchJobNumber',locSearchJobNumber)
  p_web.SetSessionValue('locPasswordMessage',locPasswordMessage)
  p_web.SetSessionValue('locJobPassword',locJobPassword)
  p_web.SetSessionValue('job:Account_Number',job:Account_Number)
  p_web.SetSessionValue('job:ESN',job:ESN)
  p_web.SetSessionValue('job:date_booked',job:date_booked)
  p_web.SetSessionValue('job:Current_Status',job:Current_Status)
  p_web.SetSessionValue('job:Location',job:Location)
  p_web.SetSessionValue('tmp:ExchangeModel',tmp:ExchangeModel)
  p_web.SetSessionValue('tmp:ExchangeIMEI',tmp:ExchangeIMEI)
  p_web.SetSessionValue('tmp:ExchangeStatusDate',tmp:ExchangeStatusDate)
  p_web.SetSessionValue('job:Exchange_Status',job:Exchange_Status)
  p_web.SetSessionValue('tmp:LoanModel',tmp:LoanModel)
  p_web.SetSessionValue('tmp:LoanImei',tmp:LoanImei)
  p_web.SetSessionValue('tmp:LoanStatusDate',tmp:LoanStatusDate)
  p_web.SetSessionValue('job:Loan_Status',job:Loan_Status)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locSearchJobNumber')
    locSearchJobNumber = p_web.GetValue('locSearchJobNumber')
    p_web.SetSessionValue('locSearchJobNumber',locSearchJobNumber)
  End
  if p_web.IfExistsValue('locPasswordMessage')
    locPasswordMessage = p_web.GetValue('locPasswordMessage')
    p_web.SetSessionValue('locPasswordMessage',locPasswordMessage)
  End
  if p_web.IfExistsValue('locJobPassword')
    locJobPassword = p_web.GetValue('locJobPassword')
    p_web.SetSessionValue('locJobPassword',locJobPassword)
  End
  if p_web.IfExistsValue('job:Account_Number')
    job:Account_Number = p_web.GetValue('job:Account_Number')
    p_web.SetSessionValue('job:Account_Number',job:Account_Number)
  End
  if p_web.IfExistsValue('job:ESN')
    job:ESN = p_web.GetValue('job:ESN')
    p_web.SetSessionValue('job:ESN',job:ESN)
  End
  if p_web.IfExistsValue('job:date_booked')
    job:date_booked = p_web.dformat(clip(p_web.GetValue('job:date_booked')),'@d06')
    p_web.SetSessionValue('job:date_booked',job:date_booked)
  End
  if p_web.IfExistsValue('job:Current_Status')
    job:Current_Status = p_web.GetValue('job:Current_Status')
    p_web.SetSessionValue('job:Current_Status',job:Current_Status)
  End
  if p_web.IfExistsValue('job:Location')
    job:Location = p_web.GetValue('job:Location')
    p_web.SetSessionValue('job:Location',job:Location)
  End
  if p_web.IfExistsValue('tmp:ExchangeModel')
    tmp:ExchangeModel = p_web.GetValue('tmp:ExchangeModel')
    p_web.SetSessionValue('tmp:ExchangeModel',tmp:ExchangeModel)
  End
  if p_web.IfExistsValue('tmp:ExchangeIMEI')
    tmp:ExchangeIMEI = p_web.GetValue('tmp:ExchangeIMEI')
    p_web.SetSessionValue('tmp:ExchangeIMEI',tmp:ExchangeIMEI)
  End
  if p_web.IfExistsValue('tmp:ExchangeStatusDate')
    tmp:ExchangeStatusDate = p_web.GetValue('tmp:ExchangeStatusDate')
    p_web.SetSessionValue('tmp:ExchangeStatusDate',tmp:ExchangeStatusDate)
  End
  if p_web.IfExistsValue('job:Exchange_Status')
    job:Exchange_Status = p_web.GetValue('job:Exchange_Status')
    p_web.SetSessionValue('job:Exchange_Status',job:Exchange_Status)
  End
  if p_web.IfExistsValue('tmp:LoanModel')
    tmp:LoanModel = p_web.GetValue('tmp:LoanModel')
    p_web.SetSessionValue('tmp:LoanModel',tmp:LoanModel)
  End
  if p_web.IfExistsValue('tmp:LoanImei')
    tmp:LoanImei = p_web.GetValue('tmp:LoanImei')
    p_web.SetSessionValue('tmp:LoanImei',tmp:LoanImei)
  End
  if p_web.IfExistsValue('tmp:LoanStatusDate')
    tmp:LoanStatusDate = p_web.GetValue('tmp:LoanStatusDate')
    p_web.SetSessionValue('tmp:LoanStatusDate',tmp:LoanStatusDate)
  End
  if p_web.IfExistsValue('job:Loan_Status')
    job:Loan_Status = p_web.GetValue('job:Loan_Status')
    p_web.SetSessionValue('job:Loan_Status',job:Loan_Status)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('JobSearch_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locSearchJobNumber = p_web.RestoreValue('locSearchJobNumber')
 locPasswordMessage = p_web.RestoreValue('locPasswordMessage')
 locJobPassword = p_web.RestoreValue('locJobPassword')
 tmp:ExchangeModel = p_web.RestoreValue('tmp:ExchangeModel')
 tmp:ExchangeIMEI = p_web.RestoreValue('tmp:ExchangeIMEI')
 tmp:ExchangeStatusDate = p_web.RestoreValue('tmp:ExchangeStatusDate')
 tmp:LoanModel = p_web.RestoreValue('tmp:LoanModel')
 tmp:LoanImei = p_web.RestoreValue('tmp:LoanImei')
 tmp:LoanStatusDate = p_web.RestoreValue('tmp:LoanStatusDate')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferJobSearch')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('JobSearch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('JobSearch_ChainTo')
    loc:formaction = p_web.GetSessionValue('JobSearch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="JobSearch" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="JobSearch" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="JobSearch" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Job Progress') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Job Progress',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_JobSearch">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_JobSearch" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_JobSearch')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Enter Job Number') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_JobSearch')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_JobSearch'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locSearchJobNumber')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_JobSearch')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Enter Job Number') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobSearch_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Number')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Number')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Number')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Number')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSearchJobNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSearchJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locSearchJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobSearch_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPasswordMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locPasswordMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locJobPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobSearch_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Account_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Account_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Account_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Model_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Model_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Model_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:ESN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:date_booked
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:date_booked
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:date_booked
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Current_Status
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Current_Status
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Current_Status
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Location
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Location
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Location
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::HozLine1
      do Comment::HozLine1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::TitleExchange
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::TitleExchange
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::TitleExchange
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ExchangeModel
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ExchangeModel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ExchangeModel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ExchangeIMEI
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ExchangeIMEI
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ExchangeIMEI
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ExchangeStatusDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ExchangeStatusDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ExchangeStatusDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Exchange_Status
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Exchange_Status
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Exchange_Status
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::HozLine2
      do Comment::HozLine2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::TitleLoan
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::TitleLoan
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::TitleLoan
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:LoanModel
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:LoanModel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:LoanModel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:LoanImei
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:LoanImei
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:LoanImei
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:LoanStatusDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:LoanStatusDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:LoanStatusDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Loan_Status
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Loan_Status
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Loan_Status
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::button:OpenJob
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button:OpenJob
      do Comment::button:OpenJob
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonViewJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonViewJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
    If p_web.GSV('PH') > 2
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonPaymentDetails
      do Comment::buttonPaymentDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnCustomerServices
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnCustomerServices
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locSearchJobNumber  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('locSearchJobNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locSearchJobNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSearchJobNumber',p_web.GetValue('NewValue'))
    locSearchJobNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSearchJobNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locSearchJobNumber',p_web.GetValue('Value'))
    locSearchJobNumber = p_web.GetValue('Value')
  End
  If locSearchJobNumber = ''
    loc:Invalid = 'locSearchJobNumber'
    loc:alert = p_web.translate('Job Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  ! Validate job
      p_web.SSV('Hide:ViewJobButton',1)
      fail# = 0
      Access:WEBJOB.Clearkey(wob:RefNumberKey)
      wob:RefNumber    =  p_web.GSV('locSearchJobNumber')
      if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
          ! Found
          p_web.SSV('locSearchRecordNumber',wob:RecordNumber)
  !          if (p_web.GSV('BookingSite') = 'RRC')
  !              if( wob:HeadAccountNumber <> p_web.GSV('BookingAccount'))
  !                    !p_web.SSV('comment:locSearchJobNumber','Error: The selected job was not booked at this site')
  !                    p_web.Message('Alert','Error: The selected job was not booked at this site',p_web._MessageClass,Net:Send)
  !                  fail# = 1
  !              end ! if( wob:HeadAccountNumber <> p_web.GSV('BookingAccount'))
  !          end ! if (p_web.GSV('BookingSite') = 'RRC')
      else ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
          ! Error
            !p_web.SSV('comment:locSearchJobNumber','Error: Cannot find selected Job Number')
            !p_web.Message('Alert','Error: Cannot find selected Job Number',p_web._MessageClass,Net:Send)
            loc:alert = 'Error: Cannot find selected Job Number'
          fail# = 1
      end ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
  
      if (fail# = 1)
          p_web.SSV('locJobFound',0)
          P_web.SSV('locSearchRecordNumber','')
          p_web.SSV('Hide:PaymentDetails',1)
          Title:password  = ''
          Title:Job       = ''
          Title:Exchange  = ''
          Title:Loan      = ''
          
          
      else ! if (fail# = 1)
        
          p_web.SSV('comment:locSearchJobNumber','Job Found')
  
          p_web.SSV('locPasswordRequired',0)
          p_web.SSV('locJobFound',1)
          p_web.SSV('Hide:ViewJobButton',1)
  
          Title:password  = ''
          Title:Job       = 'JOB DETAILS'
          Title:Exchange  = 'EXCHANGE DETAILS'
          Title:Loan      = 'LOAN DETAILS'
          
          
          
          Access:JOBS.ClearKey(job:Ref_Number_Key)
          job:Ref_Number = wob:RefNumber
          IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
          END
          p_web.FileToSessionQueue(JOBS)
        
          Access:JOBSE.ClearKey(jobe:RefNumberKey)
          jobe:RefNumber = job:Ref_Number
          IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
          END
            p_web.FileToSessionQueue(JOBSE)
            
            p_web.FileToSessionQueue(WEBJOB)
        
        
        
  !          IF (p_web.GSV('BookingSite') = 'RRC')
  !            
  !              IF (Inlist(job:Location,p_web.GSV('Default:RRCLocation'), |
  !                              p_web.GSV('Default:DespatchToCustomer'), |
  !                              p_web.GSV('Default:InTransitPUP'), |
  !                              p_web.GSV('Default:PUPLocation')) > 0)
  !                  SentToHub(p_web)
  !                  ! Job is RRC and at RRC
  !                  IF (job:Warranty_Job = 'YES' AND wob:EDI <> 'XXX')
  !                      IF (job:Date_Completed <> '' AND p_web.GSV('SentToHub') = 0)
  !                          IF (wob:EDI = 'NO' OR wob:EDI = 'YES' OR wob:EDI = 'PAY' | 
  !                                          OR wob:EDI = 'APP')
  !                              ! Job should be disabled
  !                          ELSE
  !                              p_web.SSV('locJobFound',0)
  !                              p_web.SSV('locPasswordRequired',1)
  !                            
  !                              !p_web.SSV('comment:locSearchJobNumber','Job Found: Password required.')
  !                              p_web.Message('Alert','Job Found: Password required.',p_web._MessageClass,Net:Send)
  !  
  !                              p_web.SSV('locPasswordRequired',1)
  !                              p_web.SSV('locJobFound',0)
  !                              p_web.SSV('Hide:ViewJobButton',0)
  !                              p_web.SSV('locPasswordMessage','The selected warranty job has been rejected. Enter password to EDIT the job, or click "View Job" to view the job.')
  !                          END
  !                      END
  !                  END
  !              END
  !          END ! IF (p_web.GSV('BookingSite') = 'RRC')
          
          Do LookupLoanExchangeDetsails
       
          DO ShowHidePaymentDetails
          
          
        
      end ! if (fail# = 0)
      DO RefreshJobDetails
  do Value::locSearchJobNumber
  do SendAlert
  do Comment::locSearchJobNumber
  do Value::button:OpenJob  !1
  do Value::buttonPaymentDetails  !1
  do Comment::buttonPaymentDetails
  do Prompt::locJobPassword
  do Value::locJobPassword  !1
  do Comment::locJobPassword
  do Value::buttonViewJob  !1
  do Value::locPasswordMessage  !1
  do Prompt::tmp:ExchangeIMEI
  do Value::tmp:ExchangeIMEI  !1
  do Prompt::tmp:ExchangeStatusDate
  do Value::tmp:ExchangeStatusDate  !1
  do Prompt::tmp:LoanImei
  do Value::tmp:LoanImei  !1
  do Prompt::tmp:LoanStatusDate
  do Value::tmp:LoanStatusDate  !1
  do Prompt::job:Exchange_Status
  do Value::job:Exchange_Status  !1
  do Prompt::job:Loan_Status
  do Value::job:Loan_Status  !1
  do Prompt::tmp:LoanImei
  do Value::tmp:LoanImei  !1
  do Prompt::TitleExchange
  do Value::TitleExchange  !1
  do Prompt::tmp:ExchangeModel
  do Value::tmp:ExchangeModel  !1
  do Value::HozLine2  !1
  do Value::HozLine1  !1
  do Prompt::tmp:LoanModel
  do Value::tmp:LoanModel  !1
  do Prompt::TitleLoan
  do Value::TitleLoan  !1
  do Value::btnCustomerServices  !1

Value::locSearchJobNumber  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('locSearchJobNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locSearchJobNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locSearchJobNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locSearchJobNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locSearchJobNumber'',''jobsearch_locsearchjobnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSearchJobNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locSearchJobNumber',p_web.GetSessionValueFormat('locSearchJobNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('locSearchJobNumber') & '_value')

Comment::locSearchJobNumber  Routine
    loc:comment = p_web.Translate(p_web.GSV('comment:locSearchJobNumber'))
  p_web._DivHeader('JobSearch_' & p_web._nocolon('locSearchJobNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('locSearchJobNumber') & '_comment')

Validate::locPasswordMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPasswordMessage',p_web.GetValue('NewValue'))
    locPasswordMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPasswordMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPasswordMessage',p_web.GetValue('Value'))
    locPasswordMessage = p_web.GetValue('Value')
  End

Value::locPasswordMessage  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('locPasswordMessage') & '_value',Choose(p_web.GSV('locPasswordMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locPasswordMessage') = '')
  ! --- DISPLAY --- locPasswordMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locPasswordMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('locPasswordMessage') & '_value')

Comment::locPasswordMessage  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('locPasswordMessage') & '_comment',Choose(p_web.GSV('locPasswordMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locPasswordMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locJobPassword  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('locJobPassword') & '_prompt',Choose(p_web.GSV('locPasswordRequired') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Enter Password')
  If p_web.GSV('locPasswordRequired') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('locJobPassword') & '_prompt')

Validate::locJobPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobPassword',p_web.GetValue('NewValue'))
    locJobPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobPassword',p_web.GetValue('Value'))
    locJobPassword = p_web.GetValue('Value')
  End
    locJobPassword = Upper(locJobPassword)
    p_web.SetSessionValue('locJobPassword',locJobPassword)
  ! Check password
  p_web.SSV('locJobFound',0)
  Access:USERS.ClearKey(use:password_key)
  use:password = p_web.GSV('locJobPassword')
    IF (Access:USERS.tryfetch(use:password_key) = Level:Benign)
        Access:ACCAREAS.ClearKey(acc:Access_level_key)
        acc:User_Level = use:User_Level
        acc:Access_Area = 'EDIT REJECTED WARRANTY JOB'
        IF (Access:ACCAREAS.TryFetch(acc:Access_level_key) = Level:Benign)
            p_web.SSV('locJobFound',1)    
            p_web.SSV('comment:locJobPassword','Password accepted')
          
            DO ShowHidePaymentDetails
        ELSE
            p_web.SSV('comment:locJobPassword','User does not have access to edit job.')
        END
          
    ELSE
        p_web.SSV('comment:locJobPassword','Invalid password')
    END
    
    DO RefreshJobDetails
    
  do Value::locJobPassword
  do SendAlert
  do Value::buttonPaymentDetails  !1
  do Value::button:OpenJob  !1
  do Comment::locJobPassword
  do Value::locSearchJobNumber  !1

Value::locJobPassword  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('locJobPassword') & '_value',Choose(p_web.GSV('locPasswordRequired') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locPasswordRequired') <> 1)
  ! --- STRING --- locJobPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('locJobFound') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('locJobFound') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('locJobPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobPassword'',''jobsearch_locjobpassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobPassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locJobPassword',p_web.GetSessionValueFormat('locJobPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('locJobPassword') & '_value')

Comment::locJobPassword  Routine
    loc:comment = p_web.Translate(p_web.GSV('comment:locJobPassword'))
  p_web._DivHeader('JobSearch_' & p_web._nocolon('locJobPassword') & '_comment',Choose(p_web.GSV('locPasswordRequired') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locPasswordRequired') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('locJobPassword') & '_comment')

Prompt::job:Account_Number  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:Account_Number') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Account Number')
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Account_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Account_Number',p_web.GetValue('NewValue'))
    job:Account_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Account_Number
    do Value::job:Account_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Account_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Account_Number = p_web.GetValue('Value')
  End

Value::job:Account_Number  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:Account_Number') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- job:Account_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Account_Number'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::job:Account_Number  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:Account_Number') & '_comment',Choose(p_web.GSV('locJobFound') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Model_Number  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:Model_Number') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Model Number')
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Model_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Model_Number',p_web.GetValue('NewValue'))
    do Value::job:Model_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::job:Model_Number  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:Model_Number') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('job:Model_Number') & ' - ' & p_web.GSV('job:Unit_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::job:Model_Number  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:Model_Number') & '_comment',Choose(p_web.GSV('locJobFound') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:ESN  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:ESN') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('IMEI Number')
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:ESN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:ESN',p_web.GetValue('NewValue'))
    job:ESN = p_web.GetValue('NewValue') !FieldType= STRING Field = job:ESN
    do Value::job:ESN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:ESN',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    job:ESN = p_web.GetValue('Value')
  End

Value::job:ESN  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:ESN') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- job:ESN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(job:esn,) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::job:ESN  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:ESN') & '_comment',Choose(p_web.GSV('locJobFound') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:date_booked  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:date_booked') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Date Booked')
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:date_booked  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:date_booked',p_web.GetValue('NewValue'))
    job:date_booked = p_web.GetValue('NewValue') !FieldType= DATE Field = job:date_booked
    do Value::job:date_booked
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:date_booked',p_web.dFormat(p_web.GetValue('Value'),'@d06'))
    job:date_booked = p_web.Dformat(p_web.GetValue('Value'),'@d06') !
  End

Value::job:date_booked  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:date_booked') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- job:date_booked
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(format(p_web.GetSessionValue('job:date_booked'),'@d06')) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::job:date_booked  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:date_booked') & '_comment',Choose(p_web.GSV('locJobFound') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Current_Status  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:Current_Status') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Current Status')
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Current_Status  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Current_Status',p_web.GetValue('NewValue'))
    job:Current_Status = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Current_Status
    do Value::job:Current_Status
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Current_Status',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Current_Status = p_web.GetValue('Value')
  End

Value::job:Current_Status  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:Current_Status') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- job:Current_Status
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Current_Status'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::job:Current_Status  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:Current_Status') & '_comment',Choose(p_web.GSV('locJobFound') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Location  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:Location') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Location')
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Location  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Location',p_web.GetValue('NewValue'))
    job:Location = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Location
    do Value::job:Location
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Location',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Location = p_web.GetValue('Value')
  End

Value::job:Location  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:Location') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- job:Location
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Location'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::job:Location  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:Location') & '_comment',Choose(p_web.GSV('locJobFound') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::HozLine1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('HozLine1',p_web.GetValue('NewValue'))
    do Value::HozLine1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::HozLine1  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('HozLine1') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv',''))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('HozLine1') & '_value')

Comment::HozLine1  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('HozLine1') & '_comment',Choose(p_web.GSV('locJobFound') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::TitleExchange  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('TitleExchange') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(Title:Exchange)
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('TitleExchange') & '_prompt')

Validate::TitleExchange  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('TitleExchange',p_web.GetValue('NewValue'))
    do Value::TitleExchange
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::TitleExchange  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('TitleExchange') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.Translate(' ',0) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('TitleExchange') & '_value')

Comment::TitleExchange  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('TitleExchange') & '_comment',Choose(p_web.GSV('locJobFound') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ExchangeModel  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('tmp:ExchangeModel') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Model Number')
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('tmp:ExchangeModel') & '_prompt')

Validate::tmp:ExchangeModel  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ExchangeModel',p_web.GetValue('NewValue'))
    tmp:ExchangeModel = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ExchangeModel
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ExchangeModel',p_web.GetValue('Value'))
    tmp:ExchangeModel = p_web.GetValue('Value')
  End

Value::tmp:ExchangeModel  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('tmp:ExchangeModel') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- tmp:ExchangeModel
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(tmp:ExchangeModel,) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('tmp:ExchangeModel') & '_value')

Comment::tmp:ExchangeModel  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('tmp:ExchangeModel') & '_comment',Choose(p_web.GSV('locJobFound') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ExchangeIMEI  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('tmp:ExchangeIMEI') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('IMEI Number')
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('tmp:ExchangeIMEI') & '_prompt')

Validate::tmp:ExchangeIMEI  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ExchangeIMEI',p_web.GetValue('NewValue'))
    tmp:ExchangeIMEI = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ExchangeIMEI
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ExchangeIMEI',p_web.GetValue('Value'))
    tmp:ExchangeIMEI = p_web.GetValue('Value')
  End

Value::tmp:ExchangeIMEI  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('tmp:ExchangeIMEI') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- tmp:ExchangeIMEI
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(tmp:ExchangeIMEI,) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('tmp:ExchangeIMEI') & '_value')

Comment::tmp:ExchangeIMEI  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('tmp:ExchangeIMEI') & '_comment',Choose(p_web.GSV('locJobFound') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ExchangeStatusDate  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('tmp:ExchangeStatusDate') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Date Allocated')
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('tmp:ExchangeStatusDate') & '_prompt')

Validate::tmp:ExchangeStatusDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ExchangeStatusDate',p_web.GetValue('NewValue'))
    tmp:ExchangeStatusDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ExchangeStatusDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ExchangeStatusDate',p_web.GetValue('Value'))
    tmp:ExchangeStatusDate = p_web.GetValue('Value')
  End

Value::tmp:ExchangeStatusDate  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('tmp:ExchangeStatusDate') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- tmp:ExchangeStatusDate
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(tmp:ExchangeStatusDate,) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('tmp:ExchangeStatusDate') & '_value')

Comment::tmp:ExchangeStatusDate  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('tmp:ExchangeStatusDate') & '_comment',Choose(p_web.GSV('locJobFound') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Exchange_Status  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:Exchange_Status') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Current Status')
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('job:Exchange_Status') & '_prompt')

Validate::job:Exchange_Status  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Exchange_Status',p_web.GetValue('NewValue'))
    job:Exchange_Status = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Exchange_Status
    do Value::job:Exchange_Status
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Exchange_Status',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Exchange_Status = p_web.GetValue('Value')
  End

Value::job:Exchange_Status  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:Exchange_Status') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- job:Exchange_Status
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(job:exchange_status,) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('job:Exchange_Status') & '_value')

Comment::job:Exchange_Status  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:Exchange_Status') & '_comment',Choose(p_web.GSV('locJobFound') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::HozLine2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('HozLine2',p_web.GetValue('NewValue'))
    do Value::HozLine2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::HozLine2  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('HozLine2') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv',''))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('HozLine2') & '_value')

Comment::HozLine2  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('HozLine2') & '_comment',Choose(p_web.GSV('locJobFound') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::TitleLoan  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('TitleLoan') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(Title:Loan)
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('TitleLoan') & '_prompt')

Validate::TitleLoan  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('TitleLoan',p_web.GetValue('NewValue'))
    do Value::TitleLoan
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::TitleLoan  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('TitleLoan') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.Translate(' ',) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('TitleLoan') & '_value')

Comment::TitleLoan  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('TitleLoan') & '_comment',Choose(p_web.GSV('locJobFound') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:LoanModel  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('tmp:LoanModel') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Model Number')
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('tmp:LoanModel') & '_prompt')

Validate::tmp:LoanModel  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:LoanModel',p_web.GetValue('NewValue'))
    tmp:LoanModel = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:LoanModel
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:LoanModel',p_web.GetValue('Value'))
    tmp:LoanModel = p_web.GetValue('Value')
  End

Value::tmp:LoanModel  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('tmp:LoanModel') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- tmp:LoanModel
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(tmp:LoanModel,) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('tmp:LoanModel') & '_value')

Comment::tmp:LoanModel  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('tmp:LoanModel') & '_comment',Choose(p_web.GSV('locJobFound') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:LoanImei  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('tmp:LoanImei') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('IMEI Number')
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('tmp:LoanImei') & '_prompt')

Validate::tmp:LoanImei  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:LoanImei',p_web.GetValue('NewValue'))
    tmp:LoanImei = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:LoanImei
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:LoanImei',p_web.GetValue('Value'))
    tmp:LoanImei = p_web.GetValue('Value')
  End

Value::tmp:LoanImei  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('tmp:LoanImei') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- tmp:LoanImei
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(tmp:LoanImei,) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('tmp:LoanImei') & '_value')

Comment::tmp:LoanImei  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('tmp:LoanImei') & '_comment',Choose(p_web.GSV('locJobFound') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:LoanStatusDate  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('tmp:LoanStatusDate') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Date Allocated')
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('tmp:LoanStatusDate') & '_prompt')

Validate::tmp:LoanStatusDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:LoanStatusDate',p_web.GetValue('NewValue'))
    tmp:LoanStatusDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:LoanStatusDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:LoanStatusDate',p_web.GetValue('Value'))
    tmp:LoanStatusDate = p_web.GetValue('Value')
  End

Value::tmp:LoanStatusDate  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('tmp:LoanStatusDate') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- tmp:LoanStatusDate
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(tmp:LoanStatusDate,) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('tmp:LoanStatusDate') & '_value')

Comment::tmp:LoanStatusDate  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('tmp:LoanStatusDate') & '_comment',Choose(p_web.GSV('locJobFound') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Loan_Status  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:Loan_Status') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Current Status')
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('job:Loan_Status') & '_prompt')

Validate::job:Loan_Status  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Loan_Status',p_web.GetValue('NewValue'))
    job:Loan_Status = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Loan_Status
    do Value::job:Loan_Status
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Loan_Status',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Loan_Status = p_web.GetValue('Value')
  End

Value::job:Loan_Status  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:Loan_Status') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- job:Loan_Status
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(job:Loan_Status,) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('job:Loan_Status') & '_value')

Comment::job:Loan_Status  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('job:Loan_Status') & '_comment',Choose(p_web.GSV('locJobFound') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::button:OpenJob  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('button:OpenJob') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::button:OpenJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:OpenJob',p_web.GetValue('NewValue'))
    do Value::button:OpenJob
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::button:OpenJob  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('button:OpenJob') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','OpenJob','Edit Job','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ViewJob?&wob__RecordNumber=' & p_web.GSV('locSearchRecordNumber') & '&Change_btn=Change&ViewJobReturnURL=JobSearch')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/zoom.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('button:OpenJob') & '_value')

Comment::button:OpenJob  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('button:OpenJob') & '_comment',Choose(p_web.GSV('locJobFound') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonViewJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonViewJob',p_web.GetValue('NewValue'))
    do Value::buttonViewJob
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonViewJob  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('buttonViewJob') & '_value',Choose(p_web.GSV('Hide:ViewJobButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ViewJobButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ViewJob','View Job','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ViewJob?' &'wob__RecordNumber=' & p_web.GSV('locSearchRecordNumber') & '&Change_btn=Change&ViewJobReturnURL=JobSearch&ViewOnly=1')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/zoom.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('buttonViewJob') & '_value')

Comment::buttonViewJob  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('buttonViewJob') & '_comment',Choose(p_web.GSV('Hide:ViewJobButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ViewJobButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPaymentDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPaymentDetails',p_web.GetValue('NewValue'))
    do Value::buttonPaymentDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonPaymentDetails  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('buttonPaymentDetails') & '_value',Choose(p_web.GSV('locJobFound') = 0 Or p_web.GSV('Hide:PaymentDetails') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0 Or p_web.GSV('Hide:PaymentDetails') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PaymentDetails','Payment Details','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('DisplayBrowsePayments?DisplayBrowsePaymentsReturnURL=JobSearch')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/money.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('buttonPaymentDetails') & '_value')

Comment::buttonPaymentDetails  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PaymentDetails'))
  p_web._DivHeader('JobSearch_' & p_web._nocolon('buttonPaymentDetails') & '_comment',Choose(p_web.GSV('locJobFound') = 0 Or p_web.GSV('Hide:PaymentDetails') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0 Or p_web.GSV('Hide:PaymentDetails') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('buttonPaymentDetails') & '_comment')

Validate::btnCustomerServices  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnCustomerServices',p_web.GetValue('NewValue'))
    do Value::btnCustomerServices
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnCustomerServices  Routine
  p_web._DivHeader('JobSearch_' & p_web._nocolon('btnCustomerServices') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnCustomerServices','Customer Services','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('CustomerServicesForm?' &'wob__RecordNumber=' & p_web.GSV('locSearchRecordNumber') & '&Amend=0&FirstTime=1&CustServReturnURL=JobSearch&Change_btn=Change')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobSearch_' & p_web._nocolon('btnCustomerServices') & '_value')

Comment::btnCustomerServices  Routine
    loc:comment = ''
  p_web._DivHeader('JobSearch_' & p_web._nocolon('btnCustomerServices') & '_comment',Choose(p_web.GSV('locJobFound') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locJobFound') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('JobSearch_locSearchJobNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSearchJobNumber
      else
        do Value::locSearchJobNumber
      end
  of lower('JobSearch_locJobPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobPassword
      else
        do Value::locJobPassword
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('JobSearch_form:ready_',1)
  p_web.SetSessionValue('JobSearch_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_JobSearch',0)

PreCopy  Routine
  p_web.SetValue('JobSearch_form:ready_',1)
  p_web.SetSessionValue('JobSearch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_JobSearch',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('JobSearch_form:ready_',1)
  p_web.SetSessionValue('JobSearch_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('JobSearch:Primed',0)

PreDelete       Routine
  p_web.SetValue('JobSearch_form:ready_',1)
  p_web.SetSessionValue('JobSearch_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('JobSearch:Primed',0)
  p_web.setsessionvalue('showtab_JobSearch',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('JobSearch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('JobSearch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
        If locSearchJobNumber = ''
          loc:Invalid = 'locSearchJobNumber'
          loc:alert = p_web.translate('Job Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
  ! tab = 3
    loc:InvalidTab += 1
      If not (p_web.GSV('locPasswordRequired') <> 1)
          locJobPassword = Upper(locJobPassword)
          p_web.SetSessionValue('locJobPassword',locJobPassword)
        If loc:Invalid <> '' then exit.
      End
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('JobSearch:Primed',0)
  p_web.StoreValue('locSearchJobNumber')
  p_web.StoreValue('locPasswordMessage')
  p_web.StoreValue('locJobPassword')
  p_web.StoreValue('job:Account_Number')
  p_web.StoreValue('')
  p_web.StoreValue('job:ESN')
  p_web.StoreValue('job:date_booked')
  p_web.StoreValue('job:Current_Status')
  p_web.StoreValue('job:Location')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ExchangeModel')
  p_web.StoreValue('tmp:ExchangeIMEI')
  p_web.StoreValue('tmp:ExchangeStatusDate')
  p_web.StoreValue('job:Exchange_Status')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:LoanModel')
  p_web.StoreValue('tmp:LoanImei')
  p_web.StoreValue('tmp:LoanStatusDate')
  p_web.StoreValue('job:Loan_Status')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
