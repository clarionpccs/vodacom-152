

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER203.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! Report the JOBS File
!!! </summary>
JobIncomeReport PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
Loan_Issued          STRING(3)                             !
rrc_flag             BYTE                                  !
AddressGroup         GROUP,PRE(address)                    !
CompanyName          STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
Postcode             STRING(30)                            !
TelephoneNumber      STRING(30)                            !
FaxNumber            STRING(30)                            !
EmailAddress         STRING(255)                           !
                     END                                   !
tmp:printedby        STRING(60)                            !
tmp:TelephoneNumber  STRING(20)                            !
tmp:FaxNumber        STRING(20)                            !
tmp:StartDate        DATE                                  !
tmp:EndDate          DATE                                  !
tmp:CChargeType      STRING(30)                            !
tmp:WChargeType      STRING(30)                            !
tmp:All              BYTE                                  !
ReportGroup          GROUP,PRE(report)                     !
InvoiceNumber        STRING(16)                            !
InvoiceDate          STRING(30)                            !
JobNumber            STRING(16)                            !
CompanyName          STRING(30)                            !
User                 STRING(3)                             !
Repair               STRING(3)                             !
Exchanged            STRING(3)                             !
ARCCharge            REAL                                  !
HandlingFee          REAL                                  !
LostLoanCharge       REAL                                  !
Labour               REAL                                  !
Parts                REAL                                  !
PartsSelling         REAL                                  !
ARCMarkup            REAL                                  !
Vat                  REAL                                  !
Total                REAL                                  !
CashAmount           REAL                                  !
CreditAmount         REAL                                  !
AmountPaid           REAL                                  !
AmountOutstanding    REAL                                  !
                     END                                   !
TotalGroup           GROUP,PRE(total)                      !
Lines                LONG                                  !
HandlingFee          REAL                                  !
LostLoanCharge       REAL                                  !
ARCCharge            REAL                                  !
Labour               REAL                                  !
Parts                REAL                                  !
PartsSelling         REAL                                  !
ARCMarkup            REAL                                  !
VAT                  REAL                                  !
Total                REAL                                  !
CashAmount           REAL                                  !
CreditAmount         REAL                                  !
AmountPaid           REAL                                  !
AmountOutstanding    REAL                                  !
                     END                                   !
NumberQueue          QUEUE,PRE(number)                     !
RecordNumber         LONG                                  !
RecordNumber2        LONG                                  !
                     END                                   !
tmp:DateRange        STRING(60)                            !
tmp:ReportOrder      BYTE                                  !
tmp:AllChargeTypes   BYTE                                  !
tmp:ARCVatTotal      REAL                                  !
tmp:ARCLocation      STRING(30)                            !
ShowEmpty            BYTE                                  !
TempFilePath         CSTRING(255)                          !
tmp:FixedPrice       BYTE                                  !
tmp:LoopCount        LONG                                  !
Process:View         VIEW(JOBS)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Engineer)
                       PROJECT(job:Ref_Number)
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Report JOBS'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

report               REPORT('Status Report'),AT(458,2010,10937,5698),PRE(RPT),PAPER(PAPER:A4),LANDSCAPE,FONT('Arial', |
  10),THOUS
                       HEADER,AT(427,323,10823,1375),USE(?unnamed:2)
                         STRING(@s30),AT(94,52),USE(address:CompanyName),FONT(,12,,FONT:bold),TRN
                         STRING(@s30),AT(104,260,3531,198),USE(address:AddressLine1),FONT(,8),TRN
                         STRING('Date Printed:'),AT(7667,729),USE(?RunPrompt),FONT(,8),TRN
                         STRING(@s30),AT(104,365,3531,198),USE(address:AddressLine2),FONT(,8),TRN
                         STRING(@s30),AT(104,469,3531,198),USE(address:AddressLine3),FONT(,8),TRN
                         STRING(@s30),AT(104,573),USE(address:Postcode),FONT(,8),TRN
                         STRING('Printed By:'),AT(7667,573,625,208),USE(?String67),FONT(,8),TRN
                         STRING(@s60),AT(8656,573),USE(tmp:printedby),FONT(,8,,FONT:bold),TRN
                         STRING(@s60),AT(8656,417),USE(tmp:DateRange),FONT(,8,,FONT:bold),TRN
                         STRING('Date Range:'),AT(7667,417,625,208),USE(?String67:2),FONT(,8),TRN
                         STRING('Tel: '),AT(104,729),USE(?String15),FONT(,9),TRN
                         STRING(@s30),AT(573,729),USE(address:TelephoneNumber),FONT(,8),TRN
                         STRING('Fax:'),AT(104,833),USE(?String16),FONT(,9),TRN
                         STRING(@s30),AT(573,833),USE(address:FaxNumber),FONT(,8),TRN
                         STRING(@s255),AT(573,938,3531,198),USE(address:EmailAddress),FONT(,8),TRN
                         STRING('Email:'),AT(104,938),USE(?String16:2),FONT(,9),TRN
                         STRING('Page:'),AT(7667,885),USE(?PagePrompt),FONT(,8),TRN
                         STRING('<<-- Date Stamp -->'),AT(8667,708),USE(?ReportDateStamp),FONT(,8,,FONT:bold),TRN
                         STRING(@N3),AT(8667,885),USE(ReportPageNumber),FONT(,8,,FONT:bold),TRN
                       END
DETAIL                 DETAIL,AT(10,10,10875,167),USE(?DetailBand)
                         STRING(@n10.2),AT(9750,0),USE(qIncomeReportRep.AmountPaid),FONT(,7),RIGHT,TRN
                         STRING(@n10.2),AT(10260,0,521,167),USE(qIncomeReportRep.Outstanding),FONT(,7),RIGHT,TRN
                         STRING(@n10.2),AT(5000,0),USE(qIncomeReportRep.LostLoanCharge),FONT('Arial',7,COLOR:Black, |
  FONT:regular,CHARSET:ANSI),RIGHT,TRN
                         STRING(@n-10.2),AT(7146,0),USE(qIncomeReportRep.ARCMarkup),FONT('Arial',7,COLOR:Black,FONT:regular, |
  CHARSET:ANSI),RIGHT,TRN
                         STRING(@s3),AT(3625,0),USE(qIncomeReportRep.Exchanged),FONT('Arial',7,COLOR:Black,FONT:regular, |
  CHARSET:ANSI),TRN
                         STRING(@s3),AT(3333,0),USE(qIncomeReportRep.Repair),FONT('Arial',7,COLOR:Black,FONT:regular, |
  CHARSET:ANSI),TRN
                         STRING(@n10.2),AT(4479,0),USE(qIncomeReportRep.ARCCharge),FONT('Arial',7,COLOR:Black,FONT:regular, |
  CHARSET:ANSI),RIGHT,TRN
                         STRING(@n-10.2),AT(5552,0),USE(qIncomeReportRep.PartsCost),FONT(,7),RIGHT,TRN
                         STRING(@n10.2),AT(7667,0),USE(qIncomeReportRep.Vat),FONT(,7),RIGHT,TRN
                         STRING(@n10.2),AT(8177,0),USE(qIncomeReportRep.Total),FONT(,7),RIGHT,TRN
                         STRING(@n10.2),AT(8698,0),USE(qIncomeReportRep.CashAmount),FONT(,7),RIGHT,TRN
                         STRING(@n10.2),AT(9208,0),USE(qIncomeReportRep.CreditAmount),FONT(,7),RIGHT,TRN
                         STRING(@s15),AT(156,0),USE(qIncomeReportRep.InvoiceNumber),FONT('Arial',7,,FONT:regular,CHARSET:ANSI), |
  LEFT,TRN
                         STRING(@s15),AT(1490,0),USE(qIncomeReportRep.RefNumber),FONT('Arial',7,,FONT:regular,CHARSET:ANSI), |
  LEFT,TRN
                         STRING(@s12),AT(2292,0),USE(job:Account_Number),FONT('Arial',7,,FONT:regular,CHARSET:ANSI), |
  LEFT,TRN
                         STRING(@s3),AT(3000,0),USE(job:Engineer),FONT(,7),TRN
                         STRING(@n10.2),AT(3948,0),USE(qIncomeReportRep.HandlingFee),FONT(,7),RIGHT,TRN
                         STRING(@n-10.2),AT(6615,0),USE(qIncomeReportRep.Labour),FONT(,7,COLOR:Black),RIGHT,TRN
                         STRING(@n-10.2),AT(6104,0),USE(qIncomeReportRep.PartsSelling),FONT(,7),RIGHT,TRN
                         STRING(@s10),AT(948,0),USE(qIncomeReportRep.InvoiceDate),FONT('Arial',7,,FONT:regular,CHARSET:ANSI), |
  LEFT,TRN
                       END
TOTALDETAIL            DETAIL,AT(10,10,10875,167),USE(?TotalDetail)
                         LINE,AT(198,52,10635,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Total Lines:'),AT(260,156),USE(?String68),FONT(,7,,FONT:bold),TRN
                         STRING(@s8),AT(885,156),USE(total:Lines),FONT(,7,,FONT:bold)
                         STRING(@n12.2),AT(4406,156),USE(total:ARCCharge),FONT('Arial',7,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  RIGHT,TRN
                         STRING(@n12.2),AT(3875,156),USE(total:HandlingFee),FONT(,7,,FONT:bold),RIGHT,TRN
                         STRING(@n-14.2),AT(5927,156),USE(total:PartsSelling),FONT(,7,,FONT:bold),RIGHT,TRN
                         STRING(@n-14.2),AT(6438,156),USE(total:Labour),FONT(,7,COLOR:Black,FONT:bold),RIGHT,TRN
                         STRING(@n12.2),AT(7594,156),USE(total:VAT),FONT(,7,,FONT:bold),RIGHT,TRN
                         STRING(@n12.2),AT(8104,156),USE(total:Total),FONT(,7,,FONT:bold),RIGHT,TRN
                         STRING(@n12.2),AT(8625,156),USE(total:CashAmount),FONT(,7,,FONT:bold),RIGHT,TRN
                         STRING(@n12.2),AT(9135,156),USE(total:CreditAmount),FONT(,7,,FONT:bold),RIGHT,TRN
                         STRING(@n12.2),AT(9677,156),USE(total:AmountPaid),FONT(,7,,FONT:bold),RIGHT,TRN
                         STRING(@n12.2),AT(10104,156,677,156),USE(total:AmountOutstanding),FONT(,7,,FONT:bold),RIGHT, |
  TRN
                         STRING(@n12.2),AT(4927,156),USE(total:LostLoanCharge),FONT('Arial',7,COLOR:Black,FONT:bold, |
  CHARSET:ANSI),RIGHT,TRN
                         STRING(@n-14.2),AT(6969,156),USE(total:ARCMarkup),FONT('Arial',7,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  RIGHT,TRN
                         STRING(@n-14.2),AT(5375,156),USE(total:Parts,,?total:Parts:2),FONT(,7,,FONT:bold),RIGHT,TRN
                       END
                       FORM,AT(406,313,10969,7448),USE(?unnamed)
                         IMAGE('Rlistlan.gif'),AT(104,0,10938,7448),USE(?Image1)
                         STRING('CHARGEABLE INCOME REPORT'),AT(4146,125),USE(?ReportTitle),FONT(,12,,FONT:bold),TRN
                         STRING('Inv No'),AT(208,1406),USE(?InvoiceNumberText),FONT(,7,COLOR:Black,FONT:bold),TRN
                         STRING('Account No'),AT(2354,1406),USE(?String26:4),FONT(,7,COLOR:Black,FONT:bold),TRN
                         STRING('Coll By'),AT(2948,1406),USE(?String26:5),FONT(,7,COLOR:Black,FONT:bold),TRN
                         STRING('Repair'),AT(3333,1406),USE(?String69),FONT('Arial',7,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Exch'),AT(3688,1406),USE(?String73),FONT('Arial',7,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Hndlng Fee'),AT(4031,1406),USE(?String26:7),FONT(,7,COLOR:Black,FONT:bold),TRN
                         STRING('Lost Loan'),AT(5260,1510),USE(?String77),FONT('Arial',7,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Labour'),AT(6875,1510),USE(?String26:8),FONT(,7,COLOR:Black,FONT:bold),TRN
                         STRING('RRC Parts'),AT(6042,1354),USE(?String26:9),FONT(,7,COLOR:Black,FONT:bold),TRN
                         STRING('Outstndng'),AT(10344,1510),USE(?AmountOutstanding),FONT(,7,,FONT:bold),TRN
                         STRING('RRC'),AT(8385,1354),USE(?String81:4),FONT(,7,,FONT:bold),TRN
                         STRING('RRC'),AT(6927,1354),USE(?String81:2),FONT(,7,,FONT:bold),TRN
                         STRING('/Exchange'),AT(4042,1510),USE(?String80),FONT('Arial',7),TRN
                         STRING('Cost'),AT(5917,1510),USE(?String26:2),FONT(,7,COLOR:Black,FONT:bold),TRN
                         STRING('Selling'),AT(6365,1510),USE(?String26:3),FONT(,7,COLOR:Black,FONT:bold),TRN
                         STRING('Parts M/Up'),AT(7344,1510),USE(?String74),FONT('Arial',7,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Amount'),AT(10208,1354),USE(?AmountPaid:2),FONT(,7,,FONT:bold),TRN
                         STRING('Job No'),AT(1552,1406),USE(?JobNumberText),FONT(,7,COLOR:Black,FONT:bold),TRN
                         STRING('Inv Date'),AT(1010,1406),USE(?InvoiceDateText),FONT(,7,COLOR:Black,FONT:bold),TRN
                         STRING('Paid'),AT(10052,1510),USE(?AmountPaid),FONT(,7,,FONT:bold),TRN
                         STRING('Credit Crd'),AT(9417,1406),USE(?CreditCard),FONT(,7,COLOR:Black,FONT:bold),TRN
                         STRING('RRC'),AT(5417,1354),USE(?String81),FONT(,7,,FONT:bold),TRN
                         STRING('ARC Charge'),AT(4625,1406),USE(?String70),FONT('Arial',7,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('ARC'),AT(7448,1354),USE(?String85),FONT(,7,,FONT:bold),TRN
                         STRING('RRC'),AT(7917,1354),USE(?String81:3),FONT(,7,,FONT:bold),TRN
                         STRING('Cash/Chqe'),AT(8844,1406),USE(?CashCheque),FONT(,7,COLOR:Black,FONT:bold),TRN
                         STRING('Total'),AT(8365,1510,260,156),USE(?String26:11),FONT(,7,COLOR:Black,FONT:bold),TRN
                         STRING('V.A.T.'),AT(7917,1510),USE(?String26:10),FONT(,7,COLOR:Black,FONT:bold),TRN
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR3               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
EndReport              PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Next                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.EndReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
    PRINT(rpt:TotalDetail)  
  ReturnValue = PARENT.EndReport()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('JobIncomeReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:AUDIT.SetOpenRelated()
  Relate:AUDIT.Open                                        ! File AUDIT used by this procedure, so make sure it's RelationManager is open
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:JOBPAYMT.Open                                     ! File JOBPAYMT used by this procedure, so make sure it's RelationManager is open
  Relate:PAYTYPES.Open                                     ! File PAYTYPES used by this procedure, so make sure it's RelationManager is open
  Relate:SUBTRACC_ALIAS.Open                               ! File SUBTRACC_ALIAS used by this procedure, so make sure it's RelationManager is open
  Relate:TRADEACC_ALIAS.Open                               ! File TRADEACC_ALIAS used by this procedure, so make sure it's RelationManager is open
  Relate:VATCODE.Open                                      ! File VATCODE used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:AUDIT2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PARTS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOCATLOG.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:INVOICE.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('JobIncomeReport',ProgressWindow)           ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, RECORDS(qIncomeReportRep))
  ThisReport.AddSortOrder()
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:JOBPAYMT.Close
    Relate:PAYTYPES.Close
    Relate:SUBTRACC_ALIAS.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('JobIncomeReport',ProgressWindow)        ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'JobIncomeReport',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Next PROCEDURE

ReturnValue          BYTE,AUTO

Progress BYTE,AUTO
  CODE
      ThisReport.RecordsProcessed+=1
      GET(qIncomeReportRep,ThisReport.RecordsProcessed)
      IF ERRORCODE() THEN
         ReturnValue = Level:Notify
      ELSE
         ReturnValue = Level:Benign
      END
      IF ReturnValue = Level:Notify
          IF ThisReport.RecordsProcessed>RECORDS(qIncomeReportRep)
             SELF.Response = RequestCompleted
             POST(EVENT:CloseWindow)
             RETURN Level:Notify
          ELSE
             SELF.Response = RequestCancelled
             POST(EVENT:CloseWindow)
             RETURN Level:Fatal
          END
      ELSE
         Progress = ThisReport.RecordsProcessed / ThisReport.RecordsToProcess*100
         IF Progress > 100 THEN Progress = 100.
         IF Progress <> Progress:Thermometer
           Progress:Thermometer = Progress
           DISPLAY()
         END
      END
      RETURN Level:Benign
  ReturnValue = PARENT.Next()
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
        Set(DEFAULTS)
        Access:DEFAULTS.Next()
  
        tmp:StartDate   = p_web.GSV('locStartDate')
        tmp:EndDate     = p_web.GSV('locEndDate')
  !        tmp:All         = func:All
  !        tmp:AllChargeTypes  = func:AllChargeTypes
  !        tmp:ReportOrder = func:ReportOrder
  
        tmp:DateRange   = Format(tmp:StartDate,@d6) & ' to ' & Format(tmp:EndDate,@d6)
  
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = p_web.GSV('BookingAccount')
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    !Found
            address:CompanyName     = tra:Company_Name
            address:AddressLine1    = tra:Address_Line1
            address:AddressLine2    = tra:Address_Line2
            address:AddressLine3    = tra:Address_Line3
            address:Postcode        = tra:Postcode
            address:TelephoneNumber = tra:Telephone_Number
            address:FaxNumber       = tra:Fax_Number
            address:EmailAddress    = tra:EmailAddress
        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    !Error
        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = p_web.GSV('BookingUserCode')
        IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            tmp:printedby = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
        END ! IF
        
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.Init(report, loc:pdfname,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,report{PROPPRINT:Paper},PAPER:USER), CHOOSE(report{PROP:Thous}=True,PROP:Thous,CHOOSE(report{PROP:MM}=True,PROP:MM,CHOOSE(report{PROP:Points}=True,PROP:Points,0))), report{PROPPRINT:PaperWidth}, report{PROPPRINT:PaperHeight}, report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetTitle('Income Report')            !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR3.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetViewerPrefs(PDFXTR3:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@d06)
  END
  IF ReturnValue = Level:Benign
    report$?ReportPageNumber{PROP:PageNo} = True
  END
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'JobIncomeReport',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR3.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
        IF (qIncomeReportRep.SessionID = p_web.SessionID)    
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = qIncomeReportRep.RefNumber
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                PRINT(rpt:Detail)
                
                total:Lines             += 1
                total:ARCCharge         += qIncomeReportRep.ARCCharge
                total:HandlingFee       += qIncomeReportRep.HandlingFee
                total:LostLoanCharge    += qIncomeReportRep.LostLoanCharge
                total:Labour            += qIncomeReportRep.Labour
                total:Parts             += qIncomeReportRep.PartsCost
                total:PartsSelling      += qIncomeReportRep.PartsSelling
                total:ARCMarkup         += qIncomeReportRep.ARCMarkup
                total:VAT               += qIncomeReportRep.Vat
                total:Total             += qIncomeReportRep.Total
                total:CashAmount        += qIncomeReportRep.CashAmount
                total:CreditAmount      += qIncomeReportRep.CreditAmount
                total:AmountPaid        += qIncomeReportRep.AmountPaid
                total:AmountOutstanding += qIncomeReportRep.Outstanding   
                
            END ! IF
            
            
            
        END ! IF
        
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR3:rtn = PDFXTR3.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR3:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

