

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER012.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
WOBEDIStatus         PROCEDURE  (fStatus)                  ! Declare Procedure

  CODE
    Case fStatus
    Of 'NO'
        Return 'PENDING'
    Of 'YES'
        Return 'SUBMITTED'
    Of 'APP'
        Return 'APPROVED'
    Of 'PAY'
        Return 'PAID'
    Of 'EXC'
        Return 'REJECTED'
    Of 'EXM'
        Return 'REJECTED'
    Of 'AAJ'
        Return 'ACCEPTED REJECTION'
    Of 'REJ'
        Return 'FINAL REJECTION'
    End !func:Status
    Return ''
