

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER519.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ClearTaggingFile     PROCEDURE  (NetWebServerWorker p_web, LONG pForceDelete=0) ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
        DO OpenFiles
        STREAM(Tagging)

        Access:Tagging.Clearkey(tgg:SessionIDKey)
        tgg:SessionID    = p_web.sessionID
        set(tgg:SessionIDKey,tgg:SessionIDKey)
        loop
            if (Access:Tagging.Next())
                Break
            end ! if (Access:TAGFILE.Next())
            if (tgg:SessionID    <> p_web.sessionID)
                Break
            end ! if (tag:sessionID    <> p_web.sessionID)
            IF (pForceDelete)
                ! #13575 Sometimes we do need to delete the tag entries (DBH: 28/07/2015)
                Access:Tagging.DeleteRecord(0)
            ELSE
                tgg:Tagged = 0
                Access:Tagging.TryUpdate()
            END ! IF
            
        end ! loop
        FLUSH(Tagging)

        DO CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
