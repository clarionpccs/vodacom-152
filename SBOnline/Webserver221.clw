

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER221.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER082.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER211.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER213.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER222.INC'),ONCE        !Req'd for module callout resolution
                     END


FormBrowseOldPartNumber PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
SBO_OutParts::State  USHORT
STOPARTS::State  USHORT
STOCK::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormBrowseOldPartNumber')
  loc:formname = 'FormBrowseOldPartNumber_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormBrowseOldPartNumber',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormBrowseOldPartNumber','')
      do SendPacket
      Do ShowWait
      do SendPacket
      Do CloseWait
      do SendPacket
    p_web._DivHeader('FormBrowseOldPartNumber',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormBrowseOldPartNumber',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseOldPartNumber',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseOldPartNumber',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormBrowseOldPartNumber',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseOldPartNumber',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormBrowseOldPartNumber',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormBrowseOldPartNumber',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
BuildPartsList      ROUTINE
    IF (p_web.GSV('locOldPartNumber') = '')
        EXIT
    END
    
    ! Clear file
    Access:SBO_OutParts.ClearKey(sout:PartNumberKey)
    sout:SessionID = p_web.SessionID
    SET(sout:PartNumberKey,sout:PartNumberKey)
    LOOP UNTIL Access:SBO_OutParts.Next()
        IF (sout:SessionID <> p_web.SessionID)
            BREAK
        END
        Delete(SBO_OutParts)
    END
    
    
    Access:STOPARTS.Clearkey(spt:LocationOldPartKey)
    spt:Location = p_web.GSV('Default:SiteLocation')
    spt:OldPartNumber = p_web.GSV('locOldPartNumber')
    Set(spt:LocationOldPartKey,spt:LocationOldPartKey)
    Loop ! Begin Loop
        If Access:STOPARTS.Next()
            Break
        End ! If Access:STOPARTS.Next()
        If spt:Location <> p_web.GSV('Default:SiteLocation')
            Break
        End ! If spt:Location <> f:Location
        If spt:OldPartNumber <> p_web.GSV('locOldPartNumber')
            Break
        End ! If spt:OldPartNumber <> f:PartNumber
        Access:STOCK.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = spt:STOCKRefNumber
        If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
        !Found
        Else ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
        !Error
            Cycle
        End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
        
        IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
            sout:SessionID = p_web.SessionID
            sout:LineType = 'S'
            sout:PartNumber = sto:Part_Number
            sout:Description = sto:Description
            sout:Quantity = sto:Ref_Number ! Use to get part number later
            IF (Access:SBO_OutParts.TryInsert())
                Access:SBO_OutParts.CancelAutoInc()
            END
        END

    End ! Loop
    Access:STOCK.ClearKey(sto:Location_Key)
    sto:Part_Number = p_web.GSV('locOldPartNumber')
    sto:Location = p_web.GSV('Default:SiteLocation')
    If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
    !Found
        IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
            sout:SessionID = p_web.SessionID
            sout:LineType = 'S'
            sout:PartNumber = sto:Part_Number
            sout:Description = sto:Description
            sout:Quantity = sto:Ref_Number ! Use to get part number later
            IF (Access:SBO_OutParts.TryInsert())
                Access:SBO_OutParts.CancelAutoInc()
            END
        END
    Else ! If Access:STOCK.TryFetch(sto:Part_Number_Key) = Level:Benign
    !Error
    End ! If Access:STOCK.TryFetch(sto:Part_Number_Key) = Level:Benign

OpenFiles  ROUTINE
  p_web._OpenFile(SBO_OutParts)
  p_web._OpenFile(STOPARTS)
  p_web._OpenFile(STOCK)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(SBO_OutParts)
  p_Web._CloseFile(STOPARTS)
  p_Web._CloseFile(STOCK)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormBrowseOldPartNumber_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormBrowseOldPartNumber_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
      p_web.site.SaveButton.TextValue = 'Close'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'FormBrowseStock'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormBrowseOldPartNumber_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormBrowseOldPartNumber_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormBrowseOldPartNumber_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormBrowseOldPartNumber" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormBrowseOldPartNumber" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormBrowseOldPartNumber" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('"Old" Part Number Search') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('"Old" Part Number Search',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormBrowseOldPartNumber">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormBrowseOldPartNumber" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormBrowseOldPartNumber')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Highlight Part Above And Select Option Below') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormBrowseOldPartNumber')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormBrowseOldPartNumber'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormBrowseOldPartNumber_BrowseOldPartNumber_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormBrowseOldPartNumber_BrowseOldPartNumber_embedded_div')&''');'
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormBrowseOldPartNumber')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormBrowseOldPartNumber_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseOldPartNumber
      do Comment::BrowseOldPartNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Highlight Part Above And Select Option Below') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormBrowseOldPartNumber_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Highlight Part Above And Select Option Below')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Highlight Part Above And Select Option Below')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Highlight Part Above And Select Option Below')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Highlight Part Above And Select Option Below')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnAddStock
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnAddStock
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnDepleteStock
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnDepleteStock
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnOrderStock
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnOrderStock
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::BrowseOldPartNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseOldPartNumber',p_web.GetValue('NewValue'))
    do Value::BrowseOldPartNumber
  Else
    p_web.StoreValue('sout:RecordNumber')
  End
  p_web.SSV('BrowseID',sout:Quantity)
  do SendAlert
  do Value::btnAddStock  !1
  do Value::btnDepleteStock  !1
  do Value::btnOrderStock  !1

Value::BrowseOldPartNumber  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseOldPartNumber --
  p_web.SetValue('BrowseOldPartNumber:NoForm',1)
  p_web.SetValue('BrowseOldPartNumber:FormName',loc:formname)
  p_web.SetValue('BrowseOldPartNumber:parentIs','Form')
  p_web.SetValue('_parentProc','FormBrowseOldPartNumber')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormBrowseOldPartNumber_BrowseOldPartNumber_embedded_div')&'"><!-- Net:BrowseOldPartNumber --></div><13,10>'
    p_web._DivHeader('FormBrowseOldPartNumber_' & lower('BrowseOldPartNumber') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormBrowseOldPartNumber_' & lower('BrowseOldPartNumber') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseOldPartNumber --><13,10>'
  end
  do SendPacket

Comment::BrowseOldPartNumber  Routine
    loc:comment = ''
  p_web._DivHeader('FormBrowseOldPartNumber_' & p_web._nocolon('BrowseOldPartNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnAddStock  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnAddStock',p_web.GetValue('NewValue'))
    do Value::btnAddStock
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnAddStock  Routine
  p_web._DivHeader('FormBrowseOldPartNumber_' & p_web._nocolon('btnAddStock') & '_value',Choose(p_web.GSV('BrowseID') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('BrowseID') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnAddStock','Add Stock','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormAddStock?' &'ReturnURL=FormBrowseOldPartNumber')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseOldPartNumber_' & p_web._nocolon('btnAddStock') & '_value')

Comment::btnAddStock  Routine
    loc:comment = ''
  p_web._DivHeader('FormBrowseOldPartNumber_' & p_web._nocolon('btnAddStock') & '_comment',Choose(p_web.GSV('BrowseID') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('BrowseID') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnDepleteStock  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnDepleteStock',p_web.GetValue('NewValue'))
    do Value::btnDepleteStock
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnDepleteStock  Routine
  p_web._DivHeader('FormBrowseOldPartNumber_' & p_web._nocolon('btnDepleteStock') & '_value',Choose(p_web.GSV('BrowseID') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('BrowseID') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnDepleteSTock','Deplete Stock','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormDepleteStock?' &'ReturnURL=FormBrowseOldPartNumber')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseOldPartNumber_' & p_web._nocolon('btnDepleteStock') & '_value')

Comment::btnDepleteStock  Routine
    loc:comment = ''
  p_web._DivHeader('FormBrowseOldPartNumber_' & p_web._nocolon('btnDepleteStock') & '_comment',Choose(p_web.GSV('BrowseID') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('BrowseID') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnOrderStock  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnOrderStock',p_web.GetValue('NewValue'))
    do Value::btnOrderStock
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnOrderStock  Routine
  p_web._DivHeader('FormBrowseOldPartNumber_' & p_web._nocolon('btnOrderStock') & '_value',Choose(p_web.GSV('BrowseID') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('BrowseID') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnOrderStock','Order Stock','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormStockOrder?' &'ReturnURL=FormBrowseOldPartNumber')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseOldPartNumber_' & p_web._nocolon('btnOrderStock') & '_value')

Comment::btnOrderStock  Routine
    loc:comment = ''
  p_web._DivHeader('FormBrowseOldPartNumber_' & p_web._nocolon('btnOrderStock') & '_comment',Choose(p_web.GSV('BrowseID') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('BrowseID') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormBrowseOldPartNumber_BrowseOldPartNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::BrowseOldPartNumber
      else
        do Value::BrowseOldPartNumber
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormBrowseOldPartNumber_form:ready_',1)
  p_web.SetSessionValue('FormBrowseOldPartNumber_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormBrowseOldPartNumber',0)

PreCopy  Routine
  p_web.SetValue('FormBrowseOldPartNumber_form:ready_',1)
  p_web.SetSessionValue('FormBrowseOldPartNumber_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormBrowseOldPartNumber',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormBrowseOldPartNumber_form:ready_',1)
  p_web.SetSessionValue('FormBrowseOldPartNumber_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormBrowseOldPartNumber:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormBrowseOldPartNumber_form:ready_',1)
  p_web.SetSessionValue('FormBrowseOldPartNumber_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormBrowseOldPartNumber:Primed',0)
  p_web.setsessionvalue('showtab_FormBrowseOldPartNumber',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormBrowseOldPartNumber_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormBrowseOldPartNumber_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormBrowseOldPartNumber:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
ShowWait  Routine
  packet = clip(packet) & |
    '<<div id="waitframe" class="nt-process"><13,10>'&|
    '<<p class="nt-process-text">Please wait while ServiceBase searches for matching records<<BR /><<br/><13,10>'&|
    'This may take several seconds. . . <<BR /><<BR/><13,10>'&|
    '<<img src="images/search_progress_bar.gif" width="226" height="26" /><13,10>'&|
    '<</p><13,10>'&|
    '<<BR/><<BR/><13,10>'&|
    '<<p><13,10>'&|
    '<<a href="FormBrowseStock">Cancel Process<</a><13,10>'&|
    '<</p><13,10>'&|
    '<</div><13,10>'&|
    ''
  DO SendPacket
  p_web.SSV('BrowseID','')
  DO BuildPartsList
CloseWait  Routine
  packet = clip(packet) & |
    '<<script type="text/javascript"><13,10>'&|
    'document.getElementById("waitframe").style.display="none";<13,10>'&|
    '<</script><13,10>'&|
    ''
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
