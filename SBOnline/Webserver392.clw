

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER392.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER391.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseAllExchangeUnits PROCEDURE  (NetWebServerWorker p_web)
locStatus            STRING(60)                            !
BookingSiteLocation         STRING(30)
StockType                   STRING(30)
ModelNumber                 STRING(30)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(EXCHANGE)
                      Project(xch:Ref_Number)
                      Project(xch:ESN)
                      Project(xch:Ref_Number)
                      Project(xch:Model_Number)
                      Project(xch:MSN)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseAllExchangeUnits')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseAllExchangeUnits:NoForm')
      loc:NoForm = p_web.GetValue('BrowseAllExchangeUnits:NoForm')
      loc:FormName = p_web.GetValue('BrowseAllExchangeUnits:FormName')
    else
      loc:FormName = 'BrowseAllExchangeUnits_frm'
    End
    p_web.SSV('BrowseAllExchangeUnits:NoForm',loc:NoForm)
    p_web.SSV('BrowseAllExchangeUnits:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseAllExchangeUnits:NoForm')
    loc:FormName = p_web.GSV('BrowseAllExchangeUnits:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseAllExchangeUnits') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseAllExchangeUnits')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(EXCHANGE,xch:Ref_Number_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'XCH:ESN') then p_web.SetValue('BrowseAllExchangeUnits_sort','1')
    ElsIf (loc:vorder = 'XCH:REF_NUMBER') then p_web.SetValue('BrowseAllExchangeUnits_sort','2')
    ElsIf (loc:vorder = 'XCH:MODEL_NUMBER') then p_web.SetValue('BrowseAllExchangeUnits_sort','3')
    ElsIf (loc:vorder = 'XCH:MSN') then p_web.SetValue('BrowseAllExchangeUnits_sort','5')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseAllExchangeUnits:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseAllExchangeUnits:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseAllExchangeUnits:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseAllExchangeUnits:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseAllExchangeUnits:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  ! Save Window Name
  AddToLog('NetWebBrowse',p_web.RequestData.DataString,'BrowseAllExchangeUnits',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    BookingSiteLocation = p_web.GSV('BookingSiteLocation')
    StockType = p_web.GSV('locStockType')
    ModelNumber = p_web.GSV('locModelNumber')
  
    BIND('BookingSiteLocation',BookingSiteLocation)
    BIND('StockType',StockType)
    BIND('ModelNumber',ModelNumber)  
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseAllExchangeUnits_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseAllExchangeUnits_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(xch:ESN)','-UPPER(xch:ESN)')
    Loc:LocateField = 'xch:ESN'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'xch:Ref_Number','-xch:Ref_Number')
    Loc:LocateField = 'xch:Ref_Number'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(xch:Model_Number)','-UPPER(xch:Model_Number)')
    Loc:LocateField = 'xch:Model_Number'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(xch:MSN)','-UPPER(xch:MSN)')
    Loc:LocateField = 'xch:MSN'
  of 8
    Loc:LocateField = ''
  end
  if loc:vorder = ''
    Loc:LocateField = 'xch:ESN'
    loc:sortheader = 'IMEI Number'
    loc:vorder = '+UPPER(xch:ESN)'
  end
  If False ! add range fields to sort order
  ElsIf (p_web.GSV('locExchangeAvailable') = 1 AND p_web.GSV('locStockType') = '' AND p_web.GSV('locModelNumber') = '')
  ElsIf (p_web.GSV('locExchangeAvailable') = 0 AND p_web.GSV('locStockType') = '' AND p_web.GSV('locModelNumber') = '')
  ElsIf (p_web.GSV('locExchangeAvailable') = 1 AND p_web.GSV('locStockType') <> '' AND p_web.GSV('locModelNumber') = '')
  ElsIf (p_web.GSV('locExchangeAvailable') = 0 AND p_web.GSV('locStockType') <> '' AND p_web.GSV('locModelNumber') = '')
  ElsIf (p_web.GSV('locExchangeAvailable') = 1 AND p_web.GSV('locStockType') = '' AND p_web.GSV('locModelNumber') <> '')
  ElsIf (p_web.GSV('locExchangeAvailable') = 0 AND p_web.GSV('locStockType') = '' AND p_web.GSV('locModelNumber') <> '')
  ElsIf (p_web.GSV('locExchangeAvailable') = 1 AND p_web.GSV('locStockType') <> '' AND p_web.GSV('locModelNumber') <> '')
  ElsIf (p_web.GSV('locExchangeAvailable') = 0 AND p_web.GSV('locStockType') <> '' AND p_web.GSV('locModelNumber') <> '')
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('xch:ESN')
    loc:SortHeader = p_web.Translate('I.M.E.I. Number')
    p_web.SetSessionValue('BrowseAllExchangeUnits_LocatorPic','@s30')
  Of upper('xch:Ref_Number')
    loc:SortHeader = p_web.Translate('Unit Number')
    p_web.SetSessionValue('BrowseAllExchangeUnits_LocatorPic','@s8')
  Of upper('xch:Model_Number')
    loc:SortHeader = p_web.Translate('Model Number')
    p_web.SetSessionValue('BrowseAllExchangeUnits_LocatorPic','@s30')
  Of upper('xch:MSN')
    loc:SortHeader = p_web.Translate('M.S.N.')
    p_web.SetSessionValue('BrowseAllExchangeUnits_LocatorPic','@s30')
  Of upper('locStatus')
    loc:SortHeader = p_web.Translate('Status')
    p_web.SetSessionValue('BrowseAllExchangeUnits_LocatorPic','@s60')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseAllExchangeUnits:LookupFrom')
  End!Else
  loc:formaction = 'BrowseAllExchangeUnits'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseAllExchangeUnits:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseAllExchangeUnits:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
    do SendPacket
    Do showwait
    do SendPacket
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseAllExchangeUnits:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="EXCHANGE"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="xch:Ref_Number_Key"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAllExchangeUnits',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator fixedtd')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseAllExchangeUnits',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator fixedtd',,'onchange="BrowseAllExchangeUnits.locate(''Locator2BrowseAllExchangeUnits'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseAllExchangeUnits.cl(''BrowseAllExchangeUnits'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseAllExchangeUnits_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseAllExchangeUnits_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseAllExchangeUnits','I.M.E.I. Number','Click here to sort by E.S.N. / I.M.E.I.',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by E.S.N. / I.M.E.I.')&'">'&p_web.Translate('I.M.E.I. Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseAllExchangeUnits','Unit Number','Click here to sort by Unit Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Unit Number')&'">'&p_web.Translate('Unit Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseAllExchangeUnits','Model Number','Click here to sort by Model Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Model Number')&'">'&p_web.Translate('Model Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','BrowseAllExchangeUnits','M.S.N.','Click here to sort by M.S.N.',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by M.S.N.')&'">'&p_web.Translate('M.S.N.')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&p_web.Translate('Status')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  History
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('xch:ref_number',lower(Thisview{prop:order}),1,1) = 0 !and EXCHANGE{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'xch:Ref_Number'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('xch:Ref_Number'),p_web.GetValue('xch:Ref_Number'),p_web.GetSessionValue('xch:Ref_Number'))
  If False  ! Generate Filter
  ElsIf (p_web.GSV('locExchangeAvailable') = 1 AND p_web.GSV('locStockType') = '' AND p_web.GSV('locModelNumber') = '')
      loc:FilterWas = 'UPPER(xch:Location) = UPPER(''' & p_web.GSV('BookingSiteLocation') & ''')'
  ElsIf (p_web.GSV('locExchangeAvailable') = 0 AND p_web.GSV('locStockType') = '' AND p_web.GSV('locModelNumber') = '')
      loc:FilterWas = 'UPPER(xch:Available) = ''AVL'' AND UPPER(xch:Location) = UPPER(''' & p_web.GSV('BookingSiteLocation') & ''')'
  ElsIf (p_web.GSV('locExchangeAvailable') = 1 AND p_web.GSV('locStockType') <> '' AND p_web.GSV('locModelNumber') = '')
      loc:FilterWas = 'UPPER(xch:Location) = UPPER(''' & p_web.GSV('BookingSiteLocation') & ''') AND UPPER(xch:Stock_Type) = UPPER(''' & p_web.GSV('locStockType') & ''')'
  ElsIf (p_web.GSV('locExchangeAvailable') = 0 AND p_web.GSV('locStockType') <> '' AND p_web.GSV('locModelNumber') = '')
      loc:FilterWas = 'UPPER(xch:Location) = UPPER(''' & p_web.GSV('BookingSiteLocation') & ''') AND UPPER(xch:Stock_Type) = UPPER(''' & p_web.GSV('locStockType') & ''') AND UPPER(xch:Available) = ''AVL'''
  ElsIf (p_web.GSV('locExchangeAvailable') = 1 AND p_web.GSV('locStockType') = '' AND p_web.GSV('locModelNumber') <> '')
      loc:FilterWas = 'UPPER(xch:Location) = UPPER(''' & p_web.GSV('BookingSiteLocation') & ''') AND UPPER(xch:Model_Number) = UPPER(''' & p_web.GSV('locModelNumber') & ''')'
  ElsIf (p_web.GSV('locExchangeAvailable') = 0 AND p_web.GSV('locStockType') = '' AND p_web.GSV('locModelNumber') <> '')
      loc:FilterWas = 'UPPER(xch:Location) = UPPER(''' & p_web.GSV('BookingSiteLocation') & ''') AND UPPER(xch:Available) = ''AVL'' AND UPPER(xch:Model_Number) = UPPER(''' & p_web.GSV('locModelNumber') & ''')'
  ElsIf (p_web.GSV('locExchangeAvailable') = 1 AND p_web.GSV('locStockType') <> '' AND p_web.GSV('locModelNumber') <> '')
      loc:FilterWas = 'UPPER(xch:Location) = UPPER(''' & p_web.GSV('BookingSiteLocation') & ''') AND UPPER(xch:Stock_Type) = UPPER(''' & p_web.GSV('locStockType') & ''') AND UPPER(xch:Model_Number) = UPPER(''' & p_web.GSV('locModelNumber') & ''')'
  ElsIf (p_web.GSV('locExchangeAvailable') = 0 AND p_web.GSV('locStockType') <> '' AND p_web.GSV('locModelNumber') <> '')
      loc:FilterWas = 'UPPER(xch:Location) = UPPER(''' & p_web.GSV('BookingSiteLocation') & ''') AND UPPER(xch:Stock_Type) = UPPER(''' & p_web.GSV('locStockType') & ''') AND UPPER(xch:Available) = ''AVL'' AND UPPER(xch:Model_Number) = UPPER(''' & p_web.GSV('locModelNumber') & ''')'
  Else
        loc:FilterWas = 'UPPER(xch:Available) = ''AVL'' AND UPPER(xch:Location) = UPPER(''' & p_web.GSV('BookingSiteLocation') & ''')'
  End
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAllExchangeUnits',Net:Both)
  ! Handcoded filter
    ThisView{prop:Filter} = 'UPPER(xch:Available) = ''AVL'' AND UPPER(xch:Location) = BookingSiteLocation'
  
    IF (p_web.GSV('locExchangeAvailable') = 1 AND p_web.GSV('locStockType') = '' AND p_web.GSV('locModelNumber') = '')
        ThisView{prop:Filter} = 'UPPER(xch:Location) = BookingSiteLocation'
    ElsIf (p_web.GSV('locExchangeAvailable') = 0 AND p_web.GSV('locStockType') = '' AND p_web.GSV('locModelNumber') = '')
        ThisView{prop:Filter} = 'UPPER(xch:Available) = ''AVL'' AND UPPER(xch:Location) = BookingSiteLocation'
    ElsIf (p_web.GSV('locExchangeAvailable') = 1 AND p_web.GSV('locStockType') <> '' AND p_web.GSV('locModelNumber') = '')
        ThisView{prop:Filter} = 'UPPER(xch:Location) = BookingSiteLocation AND UPPER(xch:Stock_Type) = StockType'
    ElsIf (p_web.GSV('locExchangeAvailable') = 0 AND p_web.GSV('locStockType') <> '' AND p_web.GSV('locModelNumber') = '')
        ThisView{prop:Filter} = 'UPPER(xch:Location) = BookingSiteLocation AND UPPER(xch:Stock_Type) = StockType AND UPPER(xch:Available) = ''AVL'''
    ElsIf (p_web.GSV('locExchangeAvailable') = 1 AND p_web.GSV('locStockType') = '' AND p_web.GSV('locModelNumber') <> '')
        ThisView{prop:Filter} = 'UPPER(xch:Location) = BookingSiteLocation AND UPPER(xch:Model_Number) = ModelNumber'
    ElsIf (p_web.GSV('locExchangeAvailable') = 0 AND p_web.GSV('locStockType') = '' AND p_web.GSV('locModelNumber') <> '')
        ThisView{prop:Filter} = 'UPPER(xch:Location) = BookingSiteLocation AND UPPER(xch:Available) = ''AVL'' AND UPPER(xch:Model_Number) = ModelNumber'
    ElsIf (p_web.GSV('locExchangeAvailable') = 1 AND p_web.GSV('locStockType') <> '' AND p_web.GSV('locModelNumber') <> '')
        ThisView{prop:Filter} = 'UPPER(xch:Location) = BookingSiteLocation AND UPPER(xch:Stock_Type) = StockType AND UPPER(xch:Model_Number) = ModelNumber'
    ElsIf (p_web.GSV('locExchangeAvailable') = 0 AND p_web.GSV('locStockType') <> '' AND p_web.GSV('locModelNumber') <> '')
        ThisView{prop:Filter} = 'UPPER(xch:Location) = BookingSiteLocation AND UPPER(xch:Stock_Type) = StockType AND UPPER(xch:Available) = ''AVL'' AND UPPER(xch:Model_Number) = ModelNumber'
    END ! IF
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseAllExchangeUnits_Filter')
    p_web.SetSessionValue('BrowseAllExchangeUnits_FirstValue','')
    p_web.SetSessionValue('BrowseAllExchangeUnits_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,EXCHANGE,xch:Ref_Number_Key,loc:PageRows,'BrowseAllExchangeUnits',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If EXCHANGE{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(EXCHANGE,loc:firstvalue)
              Reset(ThisView,EXCHANGE)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If EXCHANGE{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(EXCHANGE,loc:lastvalue)
            Reset(ThisView,EXCHANGE)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(xch:Ref_Number)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseAllExchangeUnits.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseAllExchangeUnits.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseAllExchangeUnits.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseAllExchangeUnits.last();',,loc:nextdisabled)
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseAllExchangeUnits',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseAllExchangeUnits_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseAllExchangeUnits_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator fixedtd')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseAllExchangeUnits',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator fixedtd',,'onchange="BrowseAllExchangeUnits.locate(''Locator1BrowseAllExchangeUnits'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseAllExchangeUnits.cl(''BrowseAllExchangeUnits'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseAllExchangeUnits_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseAllExchangeUnits_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseAllExchangeUnits.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseAllExchangeUnits.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseAllExchangeUnits.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseAllExchangeUnits.last();',,loc:nextdisabled)
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    locStatus = VodacomClass.GetExchangeStatus(xch:Available,xch:Job_Number)
    loc:field = xch:Ref_Number
    p_web._thisrow = p_web._nocolon('xch:Ref_Number')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseAllExchangeUnits:LookupField')) = xch:Ref_Number and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((xch:Ref_Number = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseAllExchangeUnits.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If EXCHANGE{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(EXCHANGE)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If EXCHANGE{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(EXCHANGE)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','xch:Ref_Number',clip(loc:field),,loc:checked,,,'onclick="BrowseAllExchangeUnits.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','xch:Ref_Number',clip(loc:field),,'checked',,,'onclick="BrowseAllExchangeUnits.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::xch:ESN
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::xch:Ref_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::xch:Model_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::xch:MSN
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locStatus
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::History
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseAllExchangeUnits.omv(this);" onMouseOut="BrowseAllExchangeUnits.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseAllExchangeUnits=new browseTable(''BrowseAllExchangeUnits'','''&clip(loc:formname)&''','''&p_web._jsok('xch:Ref_Number',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('xch:Ref_Number')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseAllExchangeUnits.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseAllExchangeUnits.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseAllExchangeUnits')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseAllExchangeUnits')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseAllExchangeUnits')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseAllExchangeUnits')
          end
        End
      End
    End
      do SendPacket
      Do hidewait
      do SendPacket
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(EXCHANGE)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(EXCHANGE)
  Bind(xch:Record)
  Clear(xch:Record)
  NetWebSetSessionPics(p_web,EXCHANGE)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('xch:Ref_Number',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
      ! Save Window Name
      IF (loc:alert <> '')
          AddToLog('Alert',loc:alert,'BrowseAllExchangeUnits',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
      END ! IF
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(EXCHANGE)
  Case upper(p_web.GetValue('_EIPClm'))
  of upper('History')
    do Validate::History
  End
  p_web._CloseFile(EXCHANGE)
! ----------------------------------------------------------------------------------------
value::xch:ESN   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('xch:ESN_'&xch:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(xch:ESN,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::xch:Ref_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('xch:Ref_Number_'&xch:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(xch:Ref_Number,'@s8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::xch:Model_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('xch:Model_Number_'&xch:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(xch:Model_Number,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::xch:MSN   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('xch:MSN_'&xch:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(xch:MSN,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locStatus   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locStatus_'&xch:Ref_Number,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locStatus,'@s60')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
Validate::History  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  xch:Ref_Number = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  !#Bryan: Added embed to set extra key components for multi part keys
  loc:result = p_web._GetFile(EXCHANGE,xch:Ref_Number_Key)
  p_web.FileToSessionQueue(EXCHANGE)
  do CheckForDuplicate
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::History   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('History_'&xch:Ref_Number,,net:crc)
      loc:disabled = ''
      packet = clip(packet) & p_web.CreateButton('button','History','History','button-flat',loc:formname,,,'window.open('''& p_web._MakeURL(clip('BrowseExchangeHistory')  & '&' & p_web._noColon('xch:Ref_Number')&'='& p_web.escape(xch:Ref_Number) & '' ) & ''','''&clip('_self')&''')',,loc:disabled,,,,,) & '<13,10>' !2
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = xch:Ref_Number

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('xch:Ref_Number',xch:Ref_Number)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('xch:Ref_Number',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('xch:Ref_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('xch:Ref_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
showwait  Routine
  packet = clip(packet) & |
    '<<script><13,10>'&|
    'document.getElementById("_busy").style.cssText="visibility:visible";<13,10>'&|
    '<</script><13,10>'&|
    ''
hidewait  Routine
  packet = clip(packet) & |
    '<<script><13,10>'&|
    'document.getElementById("_busy").style.cssText="visibility:hidden";<13,10>'&|
    '<</script><13,10>'&|
    ''
