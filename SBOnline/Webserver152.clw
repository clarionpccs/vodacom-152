

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER152.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
AddEmailSMS          PROCEDURE  (Long f:JobNumber, String f:AccountNumber, String f:MessageType, String f:SMSEmail, String f:MobileNumber, String f:EmailAddress, Real f:Cost, String f:EstimatePath) ! Declare Procedure
save_tra_id          USHORT,AUTO                           !
save_sub_id          USHORT,AUTO                           !
tmp:TelephoneNumber  STRING(30)                            !
tmp:CompanyName      STRING(30)                            !Trade Company Name
FilesOpened     BYTE(0)

  CODE
    VodacomClass.AddEmailSMS(f:JobNumber, f:AccountNumber, f:MessageType, f:SMSEmail,|
    f:MobileNumber, f:EmailAddress, f:Cost, f:EstimatePath)

!    Do OpenFiles
!
!    INCLUDE('AddEmailSMS.inc')
!
!    Do CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SMSMAIL.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SMSMAIL.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:SUBTRACC.Close
     Access:SMSMAIL.Close
     Access:TRADEACC.Close
     FilesOpened = False
  END
