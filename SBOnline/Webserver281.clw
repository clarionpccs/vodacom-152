

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER281.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER139.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER185.INC'),ONCE        !Req'd for module callout resolution
                     END


FormPayments         PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locTotalBalance      REAL                                  !
FilesOpened     Long
CONTHIST::State  USHORT
JOBPAYMT_ALIAS::State  USHORT
PAYTYPES::State  USHORT
JOBPAYMT::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
jpt:Payment_Type_OptionView   View(PAYTYPES)
                          Project(pay:Payment_Type)
                        End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormPayments')
  loc:formname = 'FormPayments_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormPayments',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormPayments','')
    p_web._DivHeader('FormPayments',clip('fdiv') & ' ' & clip('FormContent'))
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormPayments',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormPayments',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormPayments',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormPayments',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If loc:act = InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormPayments',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormPayments',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormPayments',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
DeleteSessionValues ROUTINE
    p_web.DeleteSessionValue('PassedType')        
CreditCardBit       ROUTINE
    p_web.SSV('Hide:CreditCard',1)
! #12274 Don't want to see credit card bits (Bryan: 05/09/2011)    
!    IF (p_web.GSV('jpt:Payment_Type') <> '')
!        Access:PAYTYPES.ClearKey(pay:Payment_Type_Key)
!        pay:Payment_Type = p_web.GSV('jpt:Payment_Type')
!        IF (Access:PAYTYPES.TryFetch(pay:Payment_Type_Key) = Level:Benign)
!            IF (pay:Credit_Card = 'YES')
!                p_web.SSV('Hide:CreditCard',0)
!            ELSE
!                p_web.SSV('Hide:CreditCard',1)
!            END
!        END
!    END
    
OpenFiles  ROUTINE
  p_web._OpenFile(CONTHIST)
  p_web._OpenFile(JOBPAYMT_ALIAS)
  p_web._OpenFile(PAYTYPES)
  p_web._OpenFile(JOBPAYMT)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(CONTHIST)
  p_Web._CloseFile(JOBPAYMT_ALIAS)
  p_Web._CloseFile(PAYTYPES)
  p_Web._CloseFile(JOBPAYMT)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormPayments_form:inited_',1)
  p_web.SetValue('UpdateFile','JOBPAYMT')
  p_web.SetValue('UpdateKey','jpt:Record_Number_Key')
  p_web.SetValue('IDField','jpt:Record_Number')
  do RestoreMem

CancelForm  Routine
  Do DeleteSessionValues
  IF p_web.GetSessionValue('FormPayments:Primed') = 1
    p_web._deleteFile(JOBPAYMT)
    p_web.SetSessionValue('FormPayments:Primed',0)
  End

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','JOBPAYMT')
  p_web.SetValue('UpdateKey','jpt:Record_Number_Key')
  If p_web.IfExistsValue('jpt:Date')
    p_web.SetPicture('jpt:Date','@d06b')
  End
  p_web.SetSessionPicture('jpt:Date','@d06b')
  If p_web.IfExistsValue('jpt:Credit_Card_Number')
    p_web.SetPicture('jpt:Credit_Card_Number','@s20')
  End
  p_web.SetSessionPicture('jpt:Credit_Card_Number','@s20')
  If p_web.IfExistsValue('jpt:Expiry_Date')
    p_web.SetPicture('jpt:Expiry_Date','@p##/##p')
  End
  p_web.SetSessionPicture('jpt:Expiry_Date','@p##/##p')
  If p_web.IfExistsValue('jpt:Issue_Number')
    p_web.SetPicture('jpt:Issue_Number','@s5')
  End
  p_web.SetSessionPicture('jpt:Issue_Number','@s5')
  If p_web.IfExistsValue('jpt:Amount')
    p_web.SetPicture('jpt:Amount','@n-14.2')
  End
  p_web.SetSessionPicture('jpt:Amount','@n-14.2')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=File

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormPayments_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    DO CreditCardBit 
    p_web.SSV('Title:JobPayments','')
    IF (p_web.IfExistsValue('PassedType'))
        p_web.StoreValue('PassedType')
        p_web.SSV('Title:JobPayments',': ' & p_web.GSV('PassedType'))
    END
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'DisplayBrowsePayments'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormPayments_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormPayments_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormPayments_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'DisplayBrowsePayments'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  packet = clip(packet) & '<input type="hidden" name="JOBPAYMT__FileAction" value="'&p_web.getSessionValue('JOBPAYMT:FileAction')&'" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="file" value="JOBPAYMT" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateFile" value="JOBPAYMT" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateKey" value="jpt:Record_Number_Key" ></input><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormPayments" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormPayments" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormPayments" ></input><13,10>'
  end

  do SendPacket
  packet = clip(packet) & p_web.CreateInput('hidden','jpt:Record_Number',p_web._jsok(p_web.getSessionValue('jpt:Record_Number'))) & '<13,10>'
  If p_web.Translate('Job Payments' & p_web.GSV('Title:JobPayments')) <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Job Payments' & p_web.GSV('Title:JobPayments'),0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormPayments">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormPayments" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormPayments')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('General') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormPayments')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormPayments'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.jpt:Date')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormPayments')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('General') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormPayments_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jpt:Date
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jpt:Date
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jpt:Date
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jpt:Payment_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jpt:Payment_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jpt:Payment_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jpt:Credit_Card_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jpt:Credit_Card_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jpt:Credit_Card_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jpt:Expiry_Date
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jpt:Expiry_Date
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jpt:Expiry_Date
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jpt:Issue_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jpt:Issue_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jpt:Issue_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jpt:Amount
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jpt:Amount
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jpt:Amount
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormPayments_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonViewCosts
      do Comment::buttonViewCosts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::jpt:Date  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Date') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jpt:Date  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jpt:Date',p_web.GetValue('NewValue'))
    jpt:Date = p_web.GetValue('NewValue') !FieldType= DATE Field = jpt:Date
    do Value::jpt:Date
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jpt:Date',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    jpt:Date = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  If jpt:Date = ''
    loc:Invalid = 'jpt:Date'
    loc:alert = p_web.translate('Date') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    jpt:Date = Upper(jpt:Date)
    p_web.SetSessionValue('jpt:Date',jpt:Date)
  do Value::jpt:Date
  do SendAlert

Value::jpt:Date  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Date') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jpt:Date
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('jpt:Date')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If jpt:Date = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jpt:Date'',''formpayments_jpt:date_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jpt:Date',p_web.GetSessionValue('jpt:Date'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
  !Handcode Date Lookup Button
      packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar(jpt__Date,''dd/mm/yyyy'',this); Date.disabled=true;sv(''...'',''formpayments_pickdate_value'',1,FieldValue(this,1));nextFocus(FormPayments_frm,'''',0);" value="Select Date" name="Date" type="button">...</button>'
      do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Date') & '_value')

Comment::jpt:Date  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Date') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jpt:Payment_Type  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Payment_Type') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Payment Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jpt:Payment_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jpt:Payment_Type',p_web.GetValue('NewValue'))
    jpt:Payment_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = jpt:Payment_Type
    do Value::jpt:Payment_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jpt:Payment_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jpt:Payment_Type = p_web.GetValue('Value')
  End
  If jpt:Payment_Type = ''
    loc:Invalid = 'jpt:Payment_Type'
    loc:alert = p_web.translate('Payment Type') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  DO CreditCardBit
  do Value::jpt:Payment_Type
  do SendAlert
  do Prompt::jpt:Credit_Card_Number
  do Value::jpt:Credit_Card_Number  !1
  do Comment::jpt:Credit_Card_Number
  do Prompt::jpt:Expiry_Date
  do Value::jpt:Expiry_Date  !1
  do Comment::jpt:Expiry_Date
  do Prompt::jpt:Issue_Number
  do Value::jpt:Issue_Number  !1
  do Comment::jpt:Issue_Number

Value::jpt:Payment_Type  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Payment_Type') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('jpt:Payment_Type')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If jpt:Payment_Type = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jpt:Payment_Type'',''formpayments_jpt:payment_type_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jpt:Payment_Type')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('jpt:Payment_Type',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('jpt:Payment_Type') = 0
    p_web.SetSessionValue('jpt:Payment_Type','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('jpt:Payment_Type')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(CONTHIST)
  bind(cht:Record)
  p_web._OpenFile(JOBPAYMT_ALIAS)
  bind(jpt_ali:Record)
  p_web._OpenFile(PAYTYPES)
  bind(pay:Record)
  p_web._OpenFile(JOBPAYMT)
  bind(jpt:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(jpt:Payment_Type_OptionView)
  jpt:Payment_Type_OptionView{prop:order} = 'UPPER(pay:Payment_Type)'
  Set(jpt:Payment_Type_OptionView)
  Loop
    Next(jpt:Payment_Type_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('jpt:Payment_Type') = 0
      p_web.SetSessionValue('jpt:Payment_Type',pay:Payment_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,pay:Payment_Type,choose(pay:Payment_Type = p_web.getsessionvalue('jpt:Payment_Type')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(jpt:Payment_Type_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(CONTHIST)
  p_Web._CloseFile(JOBPAYMT_ALIAS)
  p_Web._CloseFile(PAYTYPES)
  p_Web._CloseFile(JOBPAYMT)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Payment_Type') & '_value')

Comment::jpt:Payment_Type  Routine
    loc:comment = ''
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Payment_Type') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jpt:Credit_Card_Number  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Credit_Card_Number') & '_prompt',Choose(p_web.GSV('Hide:CreditCard') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Credit Card Number')
  If p_web.GSV('Hide:CreditCard') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Credit_Card_Number') & '_prompt')

Validate::jpt:Credit_Card_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jpt:Credit_Card_Number',p_web.GetValue('NewValue'))
    jpt:Credit_Card_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = jpt:Credit_Card_Number
    do Value::jpt:Credit_Card_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jpt:Credit_Card_Number',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    jpt:Credit_Card_Number = p_web.GetValue('Value')
  End
  If jpt:Credit_Card_Number = ''
    loc:Invalid = 'jpt:Credit_Card_Number'
    loc:alert = p_web.translate('Credit Card Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    jpt:Credit_Card_Number = Upper(jpt:Credit_Card_Number)
    p_web.SetSessionValue('jpt:Credit_Card_Number',jpt:Credit_Card_Number)
  do Value::jpt:Credit_Card_Number
  do SendAlert

Value::jpt:Credit_Card_Number  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Credit_Card_Number') & '_value',Choose(p_web.GSV('Hide:CreditCard') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CreditCard') = 1)
  ! --- STRING --- jpt:Credit_Card_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('jpt:Credit_Card_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If jpt:Credit_Card_Number = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jpt:Credit_Card_Number'',''formpayments_jpt:credit_card_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jpt:Credit_Card_Number',p_web.GetSessionValueFormat('jpt:Credit_Card_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s20'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Credit_Card_Number') & '_value')

Comment::jpt:Credit_Card_Number  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Credit_Card_Number') & '_comment',Choose(p_web.GSV('Hide:CreditCard') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CreditCard') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Credit_Card_Number') & '_comment')

Prompt::jpt:Expiry_Date  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Expiry_Date') & '_prompt',Choose(p_web.GSV('Hide:CreditCard') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Expiry Date')
  If p_web.GSV('Hide:CreditCard') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Expiry_Date') & '_prompt')

Validate::jpt:Expiry_Date  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jpt:Expiry_Date',p_web.GetValue('NewValue'))
    jpt:Expiry_Date = p_web.GetValue('NewValue') !FieldType= STRING Field = jpt:Expiry_Date
    do Value::jpt:Expiry_Date
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jpt:Expiry_Date',p_web.dFormat(p_web.GetValue('Value'),'@p##/##p'))
    jpt:Expiry_Date = p_web.Dformat(p_web.GetValue('Value'),'@p##/##p') !
  End
  If jpt:Expiry_Date = ''
    loc:Invalid = 'jpt:Expiry_Date'
    loc:alert = p_web.translate('Expiry Date') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::jpt:Expiry_Date
  do SendAlert

Value::jpt:Expiry_Date  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Expiry_Date') & '_value',Choose(p_web.GSV('Hide:CreditCard') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CreditCard') = 1)
  ! --- STRING --- jpt:Expiry_Date
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('jpt:Expiry_Date')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If jpt:Expiry_Date = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jpt:Expiry_Date'',''formpayments_jpt:expiry_date_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jpt:Expiry_Date',p_web.GetSessionValue('jpt:Expiry_Date'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@p##/##p',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Expiry_Date') & '_value')

Comment::jpt:Expiry_Date  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Expiry_Date') & '_comment',Choose(p_web.GSV('Hide:CreditCard') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CreditCard') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Expiry_Date') & '_comment')

Prompt::jpt:Issue_Number  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Issue_Number') & '_prompt',Choose(p_web.GSV('Hide:CreditCard') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Issue Number')
  If p_web.GSV('Hide:CreditCard') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Issue_Number') & '_prompt')

Validate::jpt:Issue_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jpt:Issue_Number',p_web.GetValue('NewValue'))
    jpt:Issue_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = jpt:Issue_Number
    do Value::jpt:Issue_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jpt:Issue_Number',p_web.dFormat(p_web.GetValue('Value'),'@s5'))
    jpt:Issue_Number = p_web.GetValue('Value')
  End
  do Value::jpt:Issue_Number
  do SendAlert

Value::jpt:Issue_Number  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Issue_Number') & '_value',Choose(p_web.GSV('Hide:CreditCard') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CreditCard') = 1)
  ! --- STRING --- jpt:Issue_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('jpt:Issue_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jpt:Issue_Number'',''formpayments_jpt:issue_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jpt:Issue_Number',p_web.GetSessionValueFormat('jpt:Issue_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s5'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Issue_Number') & '_value')

Comment::jpt:Issue_Number  Routine
      loc:comment = ''
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Issue_Number') & '_comment',Choose(p_web.GSV('Hide:CreditCard') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CreditCard') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Issue_Number') & '_comment')

Prompt::jpt:Amount  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Amount') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Payment')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jpt:Amount  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jpt:Amount',p_web.GetValue('NewValue'))
    jpt:Amount = p_web.GetValue('NewValue') !FieldType= REAL Field = jpt:Amount
    do Value::jpt:Amount
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jpt:Amount',p_web.dFormat(p_web.GetValue('Value'),'@n-14.2'))
    jpt:Amount = p_web.Dformat(p_web.GetValue('Value'),'@n-14.2') !
  End
  If jpt:Amount = ''
    loc:Invalid = 'jpt:Amount'
    loc:alert = p_web.translate('Payment') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::jpt:Amount
  do SendAlert

Value::jpt:Amount  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Amount') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jpt:Amount
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('jpt:Amount')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If jpt:Amount = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jpt:Amount'',''formpayments_jpt:amount_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jpt:Amount',p_web.GetSessionValue('jpt:Amount'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n-14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPayments_' & p_web._nocolon('jpt:Amount') & '_value')

Comment::jpt:Amount  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormPayments_' & p_web._nocolon('jpt:Amount') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonViewCosts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonViewCosts',p_web.GetValue('NewValue'))
    do Value::buttonViewCosts
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonViewCosts  Routine
  p_web._DivHeader('FormPayments_' & p_web._nocolon('buttonViewCosts') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ViewCosts','Job Costs','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ViewCosts?ViewCostsReturnURL=FormPayments')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/money.png',,,,)

  do SendPacket
  p_web._DivFooter()

Comment::buttonViewCosts  Routine
    loc:comment = ''
  p_web._DivHeader('FormPayments_' & p_web._nocolon('buttonViewCosts') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormPayments_jpt:Date_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jpt:Date
      else
        do Value::jpt:Date
      end
  of lower('FormPayments_jpt:Payment_Type_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jpt:Payment_Type
      else
        do Value::jpt:Payment_Type
      end
  of lower('FormPayments_jpt:Credit_Card_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jpt:Credit_Card_Number
      else
        do Value::jpt:Credit_Card_Number
      end
  of lower('FormPayments_jpt:Expiry_Date_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jpt:Expiry_Date
      else
        do Value::jpt:Expiry_Date
      end
  of lower('FormPayments_jpt:Issue_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jpt:Issue_Number
      else
        do Value::jpt:Issue_Number
      end
  of lower('FormPayments_jpt:Amount_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jpt:Amount
      else
        do Value::jpt:Amount
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormPayments_form:ready_',1)
  p_web.SetSessionValue('FormPayments_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormPayments',0)
  jpt:Ref_Number = p_web.GSV('job:Ref_Number')
  p_web.SetSessionValue('jpt:Ref_Number',jpt:Ref_Number)
  jpt:Date = Today()
  p_web.SetSessionValue('jpt:Date',jpt:Date)
  jpt:User_Code = p_web.GSV('BookingUserCode')
  p_web.SetSessionValue('jpt:User_Code',jpt:User_Code)
  !region Fill In Amount
    p_web.SSV('locTotalBalance',0)
    IF (p_web.GSV('PassedType') = 'REFUND')
        jpt:Amount = 0
        Access:JOBPAYMT_ALIAS.ClearKey(jpt_ali:Loan_Deposit_Key)
        jpt_ali:Ref_Number = p_web.GSV('locSearchJobNumber')
        jpt_ali:Loan_Deposit = 1
        SET(jpt_ali:Loan_Deposit_Key,jpt_ali:Loan_Deposit_Key)
        LOOP UNTIL Access:JOBPAYMT_ALIAS.Next()
            IF (jpt_ali:Ref_Number <> p_web.GSV('locSearchJobNumber'))
                BREAK
            END 
            IF (jpt_ali:Loan_Deposit <> 1)
                BREAK
            END
            jpt:Amount += jpt_ali:Amount   
        END
    ELSIF (p_web.GSV('PassedType') = 'DEPOSIT')
        jpt:Amount = 0
    ELSE        
        IF (IsJobInvoiced(p_web.GSV('job:Invoice_Number'),p_web) = TRUE)
            TotalPrice(p_web,'I',vat$,tot$,bal$)
            jpt:Amount = bal$
            p_web.SSV('locTotalBalance',bal$)
        ELSE
            TotalPrice(p_web,'C',vat$,tot$,bal$)
            jpt:Amount = bal$
            p_web.SSV('locTotalBalance',bal$)
        END !IF
    END
  
    ! p_web.SSV('jpt:Amount',jpt:Amount)
  
  
  !endregion  

PreCopy  Routine
  p_web.SetValue('FormPayments_form:ready_',1)
  p_web.SetSessionValue('FormPayments_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormPayments',0)
  p_web._PreCopyRecord(JOBPAYMT,jpt:Record_Number_Key)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormPayments_form:ready_',1)
  p_web.SetSessionValue('FormPayments_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormPayments:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormPayments_form:ready_',1)
  ! Delete: Add To Audit
  !  p_web.SSV('AddToAudit:Type','JOB')
  !  p_web.SSV('AddToAudit:Action','PAYMENT DETAIL DELETED')
  p_web.SSV('AddToAudit:Notes','PAYMENT AMOUNT: ' & Format(jpt:Amount,@n14.2) &|
      '<13,10>PAYMENT TYPE: ' & Clip(jpt:Payment_Type))
  IF (jpt:Credit_Card_Number <> '')
      p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & '<13,10>CREDIT CARD NO: ' & Clip(jpt:Credit_Card_Number) & |
          '<13,10>EXPIRY DATE: ' & Clip(jpt:Expiry_Date) & |
          '<13,10>ISSUE NO: ' & Clip(jpt:Issue_Number))
  END
  AddToAudit(p_web,jpt:Ref_Number,'JOB','PAYMENT DETAIL DELETED',p_web.GSV('AddToAudit:Notes'))
  p_web.SetSessionValue('FormPayments_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormPayments:Primed',0)
  p_web.setsessionvalue('showtab_FormPayments',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord
    IF (loc:Invalid <> '')
        EXIT
    END
    CASE p_web.GSV('PassedType')
    OF 'DEPOSIT'
        p_web.SSV('AddToAudit:Action','LOAN DESPOSIT')
    OF 'REFUND'
        IF (jpt:Amount > 0)
            jpt:Amount = -(jpt:Amount)
        END
      
        IF (Access:CONTHIST.PrimeRecord() = Level:Benign)
            cht:Ref_Number = p_web.GSV('job:Ref_Number')
            cht:Date = TODAY()
            cht:Time = CLOCK()
            cht:User = p_web.GSV('BookingUsercode')
            cht:Action = 'REFUND MADE ON LOAN UNIT'
            cht:Notes = 'AMOUNT TAKEN: ' & FORMAT(jpt:Amount,@n14.2) & | 
                            '<13,10>PAYMENT TYPE: ' & CLIP(jpt:Payment_Type) & '<13,10>'
            IF (Access:CONTHIST.TryUpdate())
            END
          
        END
        p_web.SSV('AddToAudit:Action','REFUND ISSED')
    ELSE
        p_web.SSV('AddToAudit:Action','PAYMENT RECEIVED')
    END  
    p_web.SSV('AddToAudit:Type','JOB')
    p_web.SSV('AddToAudit:Notes','PAYMENT TYPE: ' & clip(jpt:Payment_Type) & | 
                    '<13,10>AMOUNT: ' & FORMAT(jpt:Amount,@n14.2))
    AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB',p_web.GSV('AddToAudit:Action'),p_web.GSV('AddToAudit:Notes'))
  Do DeleteSessionValues

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord
  Do DeleteSessionValues

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormPayments_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  DATA
locAmount       REAL()
locBalance      REAL()
  CODE
      IF (p_web.GSV('PassedType') = '') 
          locAmount = DEFORMAT(p_web.GSV('jpt:Amount')) ! #12500 Deal with formatting issue (DBH: 19/06/2012)
          locBalance = DEFORMAT(p_web.GSV('locTotalBalance'))
          IF (locAmount > locBalance)
              loc:Invalid = 'jpt:Amount'
              loc:Alert = 'This amount exceeds the current outstanding balance.'
              EXIT
          END
      END
  p_web.DeleteSessionValue('FormPayments_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  If Loc:Invalid <> '' then exit.
        If jpt:Date = ''
          loc:Invalid = 'jpt:Date'
          loc:alert = p_web.translate('Date') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          jpt:Date = Upper(jpt:Date)
          p_web.SetSessionValue('jpt:Date',jpt:Date)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
        If jpt:Payment_Type = ''
          loc:Invalid = 'jpt:Payment_Type'
          loc:alert = p_web.translate('Payment Type') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
      If not (p_web.GSV('Hide:CreditCard') = 1)
        If jpt:Credit_Card_Number = ''
          loc:Invalid = 'jpt:Credit_Card_Number'
          loc:alert = p_web.translate('Credit Card Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          jpt:Credit_Card_Number = Upper(jpt:Credit_Card_Number)
          p_web.SetSessionValue('jpt:Credit_Card_Number',jpt:Credit_Card_Number)
        If loc:Invalid <> '' then exit.
      End
  If Loc:Invalid <> '' then exit.
      If not (p_web.GSV('Hide:CreditCard') = 1)
        If jpt:Expiry_Date = ''
          loc:Invalid = 'jpt:Expiry_Date'
          loc:alert = p_web.translate('Expiry Date') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
  If Loc:Invalid <> '' then exit.
        If jpt:Amount = ''
          loc:Invalid = 'jpt:Amount'
          loc:alert = p_web.translate('Payment') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostInsert      Routine

PostCopy        Routine
  p_web.SetSessionValue('FormPayments:Primed',0)

PostUpdate      Routine
  p_web.SetSessionValue('FormPayments:Primed',0)
  p_web.StoreValue('')

PostDelete      Routine
