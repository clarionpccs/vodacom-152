

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER005.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER070.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER256.INC'),ONCE        !Req'd for module callout resolution
                     END


LoginForm            PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
Loc:Login            STRING(20)                            !
Loc:Password         STRING(20)                            !
Loc:Remember         LONG                                  !
loc:BranchID         STRING(30)                            !
tmp:LicenseText      STRING(10000)                         !
locNewPassword       STRING(30)                            !
locConfirmNewPassword STRING(30)                           !
FilesOpened     Long
USERS_ALIAS::State  USHORT
SUBTRACC::State  USHORT
TRADEACC::State  USHORT
USERS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('LoginForm')
  loc:formname = 'LoginForm_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    IF (p_web.GSV('BookingUserCode') <> '' AND p_web.GSV('BookingUSerCode') <> 'V0D')
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'LoginForm',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
    p_web.FormReady('LoginForm','')
    p_web._DivHeader('LoginForm',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferLoginForm',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferLoginForm',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferLoginForm',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_LoginForm',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferLoginForm',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_LoginForm',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'LoginForm',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(USERS_ALIAS)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS_ALIAS)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('LoginForm_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('loc:BranchID',loc:BranchID)
  p_web.SetSessionValue('Loc:Password',Loc:Password)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('loc:BranchID')
    loc:BranchID = p_web.GetValue('loc:BranchID')
    p_web.SetSessionValue('loc:BranchID',loc:BranchID)
  End
  if p_web.IfExistsValue('Loc:Password')
    Loc:Password = p_web.GetValue('Loc:Password')
    p_web.SetSessionValue('Loc:Password',Loc:Password)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('LoginForm_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    p_web.DeleteSession()
    p_web.NewSession()
    p_web.SetSessionLoggedIn(0)  
    
    p_web.SSV('NewPasswordRequired','')
    p_web.SSV('UserMobileRequired','')
    p_web.SSV('locCurrentUser','')
    p_web.SSV('loc:Password','')
    p_web.SSV('loc:BranchID','')
    p_web.SSV('BookingName','')
    p_web.SSV('BookingAccount','')    
      p_web.site.SaveButton.TextValue = 'Login'
      p_web.site.CancelButton.TextValue = 'Reset'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 loc:BranchID = p_web.RestoreValue('loc:BranchID')
 Loc:Password = p_web.RestoreValue('Loc:Password')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'IndexPage'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('LoginForm_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('LoginForm_ChainTo')
    loc:formaction = p_web.GetSessionValue('LoginForm_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'LoginForm'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  loc:formaction = 'IndexPage'
  loc:formactiontarget = '_top'  
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="LoginForm" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="LoginForm" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="LoginForm" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Login') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Login',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket
    do SendPacket
    Do license
    do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_LoginForm">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_LoginForm" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_LoginForm')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('ServiceBase Online Login') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_LoginForm')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_LoginForm'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.loc:BranchID')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_LoginForm')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('ServiceBase Online Login') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_LoginForm_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('ServiceBase Online Login')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('ServiceBase Online Login')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('ServiceBase Online Login')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('ServiceBase Online Login')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentreSmall')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::loc:BranchID
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::loc:BranchID
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::Loc:Password
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Loc:Password
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::loc:BranchID  Routine
  p_web._DivHeader('LoginForm_' & p_web._nocolon('loc:BranchID') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Branch ID')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::loc:BranchID  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('loc:BranchID',p_web.GetValue('NewValue'))
    loc:BranchID = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::loc:BranchID
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('loc:BranchID',p_web.GetValue('Value'))
    loc:BranchID = p_web.GetValue('Value')
  End
  If loc:BranchID = ''
    loc:Invalid = 'loc:BranchID'
    loc:alert = p_web.translate('Branch ID') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    loc:BranchID = Upper(loc:BranchID)
    p_web.SetSessionValue('loc:BranchID',loc:BranchID)
  do Value::loc:BranchID
  do SendAlert

Value::loc:BranchID  Routine
  p_web._DivHeader('LoginForm_' & p_web._nocolon('loc:BranchID') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- loc:BranchID
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('loc:BranchID')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If loc:BranchID = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''loc:BranchID'',''loginform_loc:branchid_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon(loc:Password)&''',2);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','loc:BranchID',p_web.GetSessionValueFormat('loc:BranchID'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('LoginForm_' & p_web._nocolon('loc:BranchID') & '_value')


Prompt::Loc:Password  Routine
  p_web._DivHeader('LoginForm_' & p_web._nocolon('Loc:Password') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Password')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::Loc:Password  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Loc:Password',p_web.GetValue('NewValue'))
    Loc:Password = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::Loc:Password
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('Loc:Password',p_web.GetValue('Value'))
    Loc:Password = p_web.GetValue('Value')
  End
  If Loc:Password = ''
    loc:Invalid = 'Loc:Password'
    loc:alert = p_web.translate('Password') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    Loc:Password = Upper(Loc:Password)
    p_web.SetSessionValue('Loc:Password',Loc:Password)
  do Value::Loc:Password
  do SendAlert

Value::Loc:Password  Routine
  p_web._DivHeader('LoginForm_' & p_web._nocolon('Loc:Password') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- Loc:Password
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('Loc:Password')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If Loc:Password = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''Loc:Password'',''loginform_loc:password_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','Loc:Password',p_web.GetSessionValueFormat('Loc:Password'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('LoginForm_' & p_web._nocolon('Loc:Password') & '_value')


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('LoginForm_loc:BranchID_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::loc:BranchID
      else
        do Value::loc:BranchID
      end
  of lower('LoginForm_Loc:Password_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::Loc:Password
      else
        do Value::Loc:Password
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('LoginForm_form:ready_',1)
  p_web.SetSessionValue('LoginForm_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_LoginForm',0)

PreCopy  Routine
  p_web.SetValue('LoginForm_form:ready_',1)
  p_web.SetSessionValue('LoginForm_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_LoginForm',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('LoginForm_form:ready_',1)
  p_web.SetSessionValue('LoginForm_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('LoginForm:Primed',0)

PreDelete       Routine
  p_web.SetValue('LoginForm_form:ready_',1)
  p_web.SetSessionValue('LoginForm_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('LoginForm:Primed',0)
  p_web.setsessionvalue('showtab_LoginForm',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord
    ! Validation
    DuplicateTabCheckDelete(p_web,'','')
    
    !region Licence Check
    
    Relate:DEFAULT2.Open()
    SET(DEFAULT2,0)
    Access:DEFAULT2.Next()
    p_web.SSV('Licence:ExpiryDate',de2:PLE)
    Relate:DEFAULT2.Close()
    
    IF (p_web.GSV('Licence:ExpiryDate') > 0)
        IF (p_web.GSV('Licence:ExpiryDate') < TODAY())
            loc:alert = 'Licence expired!\n\nYou can no longer use ServiceBase.'
            loc:invalid = 'loc:Login'
            EXIT
        END
            
        IF (p_web.GSV('Licence:ExpiryDate') < TODAY() + 14)
            CASE (p_web.GSV('Licence:ExpiryDate') - TODAY())
            OF 0 ! No days left
                loc:alert = 'Licence Warning!\n\nYour ServiceBase licence is due to expire today.'
            OF 1 ! 1 day left
                loc:alert = 'Licence Warning!\n\nYour ServiceBase licence is due to expire tomorrow.'
            ELSE
                loc:alert = 'Licence Warning!\n\nYour ServiceBase licence is due to expire in ' & p_web.GSV('Licence:ExpiryDate') - TODAY() & ' days.'
            END
        END ! IF
            
    END     
    !endregion    
    IF (loc:Invalid)
        EXIT
    END
    p_web.SSV('NextURL','IndexPage')
    
    p_web.SSV('NewPasswordRequired','')
    p_web.SSV('UserMobileRequired','')
    error# = FALSE
    
    Access:USERS.Clearkey(use:Password_Key)
    use:Password = UPPER(p_web.GSV('loc:Password'))
    IF (Access:USERS.Tryfetch(use:Password_Key) = Level:Benign)
        IF (use:Active <> 'YES')
            loc:Invalid = 'loc:Password'
            loc:Alert = 'Entered password is no longer active'
            p_web.SSV('loc:Password','')
            EXIT
        END
        
        Access:TRADEACC.Clearkey(tra:SiteLocationKey)
        tra:SiteLocation = use:Location
        IF (Access:TRADEACC.Tryfetch(tra:SiteLocationKey) = Level:Benign)
            IF (tra:Account_Number <> p_web.GSV('loc:BranchID'))
                loc:Invalid = 'loc:BranchID'
                loc:Alert = 'Branch ID And Password Do Not Match'
                p_web.SSV('loc:Password','')
                EXIT
            END ! IF
            IF (tra:RemoteRepairCentre <> 1 OR tra:Stop_Account = 'YES')
                loc:invalid = 'loc:BranchID'
                loc:Alert = 'The selected Branch is not configured to be a Remote Repair Centre'
                p_web.SSV('loc:Password','')
                EXIT
            END ! IF
            
            ! Renew Password?
            IF (use:RenewPassword > 0)
                IF (use:RenewPassword = '')
                    use:RenewPassword = TODAY()
                    Access:USERS.TryUpdate()
                ELSE ! IF
                    IF ((use:PasswordLastChanged + use:RenewPassword) < TODAY() OR |
                                    use:PasswordLastChanged > TODAY())
                        p_web.SSV('NextURL','FormNewPassword')
                        p_web.SSV('NewPasswordRequired',1)
                        error# = TRUE
                    END !IF
                END ! IF
            END ! IF
  
            ! Mobile?
            If (use:MobileNumber = '')
                p_web.SSV('UserMobileRequired',1)
                error# = TRUE
            END
  
            p_web.SSV('LoginDetails1','Site: ' & clip(tra:Company_Name))
            p_web.SSV('LoginDetails2','User: ' & clip(use:forename) & ' ' & clip(use:Surname))
        ELSE ! IF
            loc:Invalid = 'loc:Password'
            loc:Alert = 'Incorrect Password'
            EXIT
        END ! IF
    ELSE ! IF 
        loc:Invalid = 'loc:Password'
        loc:Alert = 'Incorrect Password'
        EXIT
    END ! IF
    
    IF (error# = FALSE)
        ClearJobVariables(p_web)
    
        p_web.SSV('VersionNumber',glo:VersionNumber)
    
        ! Bodge because this was never replaced when the default switched where
        ! added to the trade account
        p_web.SSV('PH',9)
        
        SetLoginDefaults(p_web)
    END ! IF
  
    p_web.ValidateLogin()
    
    
  

ValidateDelete  Routine
  p_web.DeleteSessionValue('LoginForm_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('LoginForm_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
        If loc:BranchID = ''
          loc:Invalid = 'loc:BranchID'
          loc:alert = p_web.translate('Branch ID') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          loc:BranchID = Upper(loc:BranchID)
          p_web.SetSessionValue('loc:BranchID',loc:BranchID)
        If loc:Invalid <> '' then exit.
        If Loc:Password = ''
          loc:Invalid = 'Loc:Password'
          loc:alert = p_web.translate('Password') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          Loc:Password = Upper(Loc:Password)
          p_web.SetSessionValue('Loc:Password',Loc:Password)
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('LoginForm:Primed',0)
  p_web.StoreValue('loc:BranchID')
  p_web.StoreValue('Loc:Password')
license  Routine
  packet = clip(packet) & |
    '<<div align="center" class="nt-licence-text"><13,10>'&|
    '<<iframe id="waitframe" src="la.htm"><</iframe><13,10>'&|
    '<</div><13,10>'&|
    ''
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('Loc:Login',Loc:Login) ! STRING(20)
     p_web.SSV('Loc:Password',Loc:Password) ! STRING(20)
     p_web.SSV('Loc:Remember',Loc:Remember) ! LONG
     p_web.SSV('loc:BranchID',loc:BranchID) ! STRING(30)
     p_web.SSV('tmp:LicenseText',tmp:LicenseText) ! STRING(10000)
     p_web.SSV('locNewPassword',locNewPassword) ! STRING(30)
     p_web.SSV('locConfirmNewPassword',locConfirmNewPassword) ! STRING(30)
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     Loc:Login = p_web.GSV('Loc:Login') ! STRING(20)
     Loc:Password = p_web.GSV('Loc:Password') ! STRING(20)
     Loc:Remember = p_web.GSV('Loc:Remember') ! LONG
     loc:BranchID = p_web.GSV('loc:BranchID') ! STRING(30)
     tmp:LicenseText = p_web.GSV('tmp:LicenseText') ! STRING(10000)
     locNewPassword = p_web.GSV('locNewPassword') ! STRING(30)
     locConfirmNewPassword = p_web.GSV('locConfirmNewPassword') ! STRING(30)
