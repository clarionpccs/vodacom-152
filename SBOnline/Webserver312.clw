

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER312.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER303.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER313.INC'),ONCE        !Req'd for module callout resolution
                     END


FormLoanAuditProcess PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locStockType         STRING(30)                            !
locIMEINumber        STRING(30)                            !
locInStock           LONG                                  !
locCounted           LONG                                  !
locVariance          LONG                                  !
locStatus            STRING(60)                            !
locBrowseType        LONG                                  !
                    MAP
CompleteStockType	PROCEDURE()
LoanAuditProcessIMEI    PROCEDURE()
                    END ! MAP
FilesOpened     Long
LOANALC::State  USHORT
STOCKTYP::State  USHORT
LOANAMF::State  USHORT
LOANAUI::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
locStockType_OptionView   View(STOCKTYP)
                          Project(stp:Stock_Type)
                        End
  CODE
  GlobalErrors.SetProcedureName('FormLoanAuditProcess')
  loc:formname = 'FormLoanAuditProcess_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormLoanAuditProcess',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormLoanAuditProcess','')
    p_web._DivHeader('FormLoanAuditProcess',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormLoanAuditProcess',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormLoanAuditProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormLoanAuditProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormLoanAuditProcess',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormLoanAuditProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormLoanAuditProcess',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormLoanAuditProcess',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(LOANALC)
  p_web._OpenFile(STOCKTYP)
  p_web._OpenFile(LOANAMF)
  p_web._OpenFile(LOANAUI)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(LOANALC)
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(LOANAMF)
  p_Web._CloseFile(LOANAUI)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormLoanAuditProcess_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('lmf:Status',lmf:Status)
  p_web.SetSessionValue('locInStock',locInStock)
  p_web.SetSessionValue('locCounted',locCounted)
  p_web.SetSessionValue('locVariance',locVariance)
  p_web.SetSessionValue('locStockType',locStockType)
  p_web.SetSessionValue('locStatus',locStatus)
  p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  p_web.SetSessionValue('locBrowseType',locBrowseType)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('lmf:Status')
    lmf:Status = p_web.GetValue('lmf:Status')
    p_web.SetSessionValue('lmf:Status',lmf:Status)
  End
  if p_web.IfExistsValue('locInStock')
    locInStock = p_web.GetValue('locInStock')
    p_web.SetSessionValue('locInStock',locInStock)
  End
  if p_web.IfExistsValue('locCounted')
    locCounted = p_web.GetValue('locCounted')
    p_web.SetSessionValue('locCounted',locCounted)
  End
  if p_web.IfExistsValue('locVariance')
    locVariance = p_web.GetValue('locVariance')
    p_web.SetSessionValue('locVariance',locVariance)
  End
  if p_web.IfExistsValue('locStockType')
    locStockType = p_web.GetValue('locStockType')
    p_web.SetSessionValue('locStockType',locStockType)
  End
  if p_web.IfExistsValue('locStatus')
    locStatus = p_web.GetValue('locStatus')
    p_web.SetSessionValue('locStatus',locStatus)
  End
  if p_web.IfExistsValue('locIMEINumber')
    locIMEINumber = p_web.GetValue('locIMEINumber')
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  End
  if p_web.IfExistsValue('locBrowseType')
    locBrowseType = p_web.GetValue('locBrowseType')
    p_web.SetSessionValue('locBrowseType',locBrowseType)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormLoanAuditProcess_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    p_web.SSV('ReadOnly:IMEINumber',0)
    IF (p_web.IfExistsValue('setprogress'))
        ! This is a bit of a bodge because refreshing fields doesn't work well when there's a dropdown
        CASE p_web.GetValue('setprogress')
        OF 'inprogress'
            Access:LOANAMF.ClearKey(lmf:Audit_Number_Key)
            lmf:Audit_Number = p_web.GSV('lmf:Audit_Number')
            IF (Access:LOANAMF.TryFetch(lmf:Audit_Number_Key) = Level:Benign)
                p_web.SessionQueueToFile(LOANAMF)
                lmf:Status = 'AUDIT IN PROGRESS'
                Access:LOANAMF.TryUpdate()
  			
                p_web.SSV('lmf:Status',lmf:Status)	
                p_web.FileToSessionQueue(LOANAMF)
  		
            END ! IF
        OF 'startaudit'
            IF (Access:LOANALC.PrimeRecord() = Level:Benign)
                lac1:Audit_Number = p_web.GSV('lmf:Audit_Number')
                lac1:Location = p_web.GSV('BookingSiteLocation')
                lac1:Stock_Type = p_web.GSV('locStockType')
                lac1:Confirmed = 0
                IF (Access:LOANALC.TryInsert())
                    Access:LOANALC.CancelAutoInc()
                END ! IF
                p_web.SSV('locStatus',p_web.GSV('locStockType') & ': PRELIMINARY AUDIT')
            END ! IF
        OF 'startauditno'
            p_web.SSV('locStatus',p_web.GSV('locStockType') & ': VIEW ONLY')
            p_web.SSV('ReadOnly:IMEINumber',1)
        END ! IF
    END ! IF
    
    IF (p_web.GSV('lmf:Status') = 'SUSPENDED')
        p_web.SSV('Hide:SuspendAudit',1)
        p_web.SSV('Hide:ContinueAudit',0)
        p_web.SSV('Hide:CompleteStockType',1)
    ELSE ! IF
        p_web.SSV('Hide:SuspendAudit',0)
        p_web.SSV('Hide:ContinueAudit',1)
        p_web.SSV('Hide:CompleteStockType',0)
    END ! IF  
  
    IF (p_web.GSV('locBrowseType') = '')
        p_web.SSV('locBrowseType',0)
    END ! IF
    
    p_web.SSV('locVariance',p_web.GSV('locCounted') - p_web.GSV('locInStock'))
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locInStock = p_web.RestoreValue('locInStock')
 locCounted = p_web.RestoreValue('locCounted')
 locVariance = p_web.RestoreValue('locVariance')
 locStockType = p_web.RestoreValue('locStockType')
 locStatus = p_web.RestoreValue('locStatus')
 locIMEINumber = p_web.RestoreValue('locIMEINumber')
 locBrowseType = p_web.RestoreValue('locBrowseType')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormLoanAuditProcess')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormLoanAuditProcess_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormLoanAuditProcess_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormLoanAuditProcess_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'FormLoanAudits'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormLoanAuditProcess" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormLoanAuditProcess" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormLoanAuditProcess" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Loan Audit Procedure') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Loan Audit Procedure',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormLoanAuditProcess">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormLoanAuditProcess" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormLoanAuditProcess')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Loan Audit Procedure') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Actions') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormLoanAuditProcess')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormLoanAuditProcess'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locStockType')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormLoanAuditProcess')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
    do SendPacket
    Do spacer
    do SendPacket
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Loan Audit Procedure') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormLoanAuditProcess_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Loan Audit Procedure')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Loan Audit Procedure')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Loan Audit Procedure')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Loan Audit Procedure')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::lmf:Status
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::lmf:Status
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::lmf:Status
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locInStock
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locInStock
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locInStock
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCounted
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCounted
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCounted
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locVariance
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locVariance
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locVariance
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locStockType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locStockType
      do Comment::locStockType
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnStartStockType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnStartStockType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locStatus
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnCompleteStockType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnCompleteStockType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::___line
      do Comment::___line
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& ' rowspan="4">'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locBrowseType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&' rowspan="4">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locBrowseType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&' rowspan="4">'
      loc:columncounter += 1
      do SendPacket
      do Comment::locBrowseType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwAudit
      do Comment::brwAudit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Actions') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormLoanAuditProcess_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnCompleteAudit
      do Comment::btnCompleteAudit
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnSuspendAudit
      do Comment::btnSuspendAudit
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnContinueAudit
      do Comment::btnContinueAudit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::lmf:Status  Routine
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('lmf:Status') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::lmf:Status  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('lmf:Status',p_web.GetValue('NewValue'))
    lmf:Status = p_web.GetValue('NewValue') !FieldType= STRING Field = lmf:Status
    do Value::lmf:Status
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('lmf:Status',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    lmf:Status = p_web.GetValue('Value')
  End

Value::lmf:Status  Routine
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('lmf:Status') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- lmf:Status
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold LargeText')&'">' & p_web._jsok(p_web.GetSessionValueFormat('lmf:Status'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormLoanAuditProcess_' & p_web._nocolon('lmf:Status') & '_value')

Comment::lmf:Status  Routine
    loc:comment = ''
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('lmf:Status') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locInStock  Routine
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('locInStock') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('In Stock:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locInStock  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locInStock',p_web.GetValue('NewValue'))
    locInStock = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locInStock
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locInStock',p_web.GetValue('Value'))
    locInStock = p_web.GetValue('Value')
  End

Value::locInStock  Routine
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('locInStock') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locInStock
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('bold LargeText')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locInStock'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormLoanAuditProcess_' & p_web._nocolon('locInStock') & '_value')

Comment::locInStock  Routine
    loc:comment = ''
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('locInStock') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locCounted  Routine
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('locCounted') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Counted:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locCounted  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCounted',p_web.GetValue('NewValue'))
    locCounted = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCounted
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCounted',p_web.GetValue('Value'))
    locCounted = p_web.GetValue('Value')
  End

Value::locCounted  Routine
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('locCounted') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locCounted
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('bold LargeText')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locCounted'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormLoanAuditProcess_' & p_web._nocolon('locCounted') & '_value')

Comment::locCounted  Routine
    loc:comment = ''
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('locCounted') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locVariance  Routine
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('locVariance') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Variance:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locVariance  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locVariance',p_web.GetValue('NewValue'))
    locVariance = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locVariance
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locVariance',p_web.GetValue('Value'))
    locVariance = p_web.GetValue('Value')
  End

Value::locVariance  Routine
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('locVariance') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locVariance
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('bold LargeText')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locVariance'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormLoanAuditProcess_' & p_web._nocolon('locVariance') & '_value')

Comment::locVariance  Routine
    loc:comment = ''
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('locVariance') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locStockType  Routine
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('locStockType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Stock Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locStockType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locStockType',p_web.GetValue('NewValue'))
    locStockType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locStockType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locStockType',p_web.GetValue('Value'))
    locStockType = p_web.GetValue('Value')
  End
    locStockType = Upper(locStockType)
    p_web.SetSessionValue('locStockType',locStockType)
  do Value::locStockType
  do SendAlert

Value::locStockType  Routine
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('locStockType') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locStockType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locStockType'',''formloanauditprocess_locstocktype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('lmf:Status') <> 'AUDIT IN PROGRESS','disabled','')
  packet = clip(packet) & p_web.CreateSelect('locStockType',loc:fieldclass,loc:readonly,,,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locStockType') = 0
    p_web.SetSessionValue('locStockType','')
  end
    packet = clip(packet) & p_web.CreateOption('','',choose('' = p_web.getsessionvalue('locStockType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(LOANALC)
  bind(lac1:Record)
  p_web._OpenFile(STOCKTYP)
  bind(stp:Record)
  p_web._OpenFile(LOANAMF)
  bind(lmf:Record)
  p_web._OpenFile(LOANAUI)
  bind(lau:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locStockType_OptionView)
  locStockType_OptionView{prop:filter} = 'UPPER(stp:Use_Loan) = ''YES'''
  locStockType_OptionView{prop:order} = 'UPPER(stp:Stock_Type)'
  Set(locStockType_OptionView)
  Loop
    Next(locStockType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locStockType') = 0
      p_web.SetSessionValue('locStockType',stp:Stock_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,stp:Stock_Type,choose(stp:Stock_Type = p_web.getsessionvalue('locStockType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locStockType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(LOANALC)
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(LOANAMF)
  p_Web._CloseFile(LOANAUI)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormLoanAuditProcess_' & p_web._nocolon('locStockType') & '_value')

Comment::locStockType  Routine
    loc:comment = ''
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('locStockType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnStartStockType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnStartStockType',p_web.GetValue('NewValue'))
    do Value::btnStartStockType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnStartStockType  Routine
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('btnStartStockType') & '_value',Choose(p_web.GSV('lmf:Status') <> 'AUDIT IN PROGRESS','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('lmf:Status') <> 'AUDIT IN PROGRESS')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnStartStockType','Start Stock Type','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('AuditProcess?' &'ProcessType=LoanAuditPickStockType&okURL=FormLoanAuditProcess&errorURL=FormLoanAuditProcess')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::btnStartStockType  Routine
    loc:comment = ''
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('btnStartStockType') & '_comment',Choose(p_web.GSV('lmf:Status') <> 'AUDIT IN PROGRESS','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('lmf:Status') <> 'AUDIT IN PROGRESS'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locStatus  Routine
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('locStatus') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locStatus',p_web.GetValue('NewValue'))
    locStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locStatus
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locStatus',p_web.GetValue('Value'))
    locStatus = p_web.GetValue('Value')
  End

Value::locStatus  Routine
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('locStatus') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locStatus
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('bold LargeText blue')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locStatus'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormLoanAuditProcess_' & p_web._nocolon('locStatus') & '_value')

Comment::locStatus  Routine
    loc:comment = ''
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('locStatus') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locIMEINumber  Routine
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('locIMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('I.M.E.I. Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('NewValue'))
    locIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('Value'))
    locIMEINumber = p_web.GetValue('Value')
  End
  LoanAuditProcessIMEI()
  do Value::locIMEINumber
  do SendAlert
  do Value::locCounted  !1
  do Value::brwAudit  !1
  do Value::locInStock  !1
  do Value::locVariance  !1

Value::locIMEINumber  Routine
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('locIMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('lmf:Status') <> 'AUDIT IN PROGRESS' OR p_web.GSV('ReadOnly:IMEINumber') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('lmf:Status') <> 'AUDIT IN PROGRESS' OR p_web.GSV('ReadOnly:IMEINumber') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('locIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locIMEINumber'',''formloanauditprocess_locimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locIMEINumber',p_web.GetSessionValueFormat('locIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormLoanAuditProcess_' & p_web._nocolon('locIMEINumber') & '_value')

Comment::locIMEINumber  Routine
      loc:comment = ''
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('locIMEINumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnCompleteStockType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnCompleteStockType',p_web.GetValue('NewValue'))
    do Value::btnCompleteStockType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    CompleteStockType()  
  do SendAlert
  do Value::locStatus  !1
  do Value::btnCompleteStockType  !1
  do Value::brwAudit  !1

Value::btnCompleteStockType  Routine
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('btnCompleteStockType') & '_value',Choose(p_web.GSV('Hide:CompleteStockType') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CompleteStockType') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnCompleteStockType'',''formloanauditprocess_btncompletestocktype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnCompleteStockType','Complete Stock Type','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormLoanAuditProcess_' & p_web._nocolon('btnCompleteStockType') & '_value')

Comment::btnCompleteStockType  Routine
    loc:comment = ''
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('btnCompleteStockType') & '_comment',Choose(p_web.GSV('Hide:CompleteStockType') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CompleteStockType') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::___line  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('___line',p_web.GetValue('NewValue'))
    do Value::___line
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::___line  Routine
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('___line') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::___line  Routine
    loc:comment = ''
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('___line') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locBrowseType  Routine
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('locBrowseType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locBrowseType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locBrowseType',p_web.GetValue('NewValue'))
    locBrowseType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locBrowseType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locBrowseType',p_web.GetValue('Value'))
    locBrowseType = p_web.GetValue('Value')
  End
  do Value::locBrowseType
  do SendAlert
  do Value::brwAudit  !1

Value::locBrowseType  Routine
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('locBrowseType') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locBrowseType
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locBrowseType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locBrowseType') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locBrowseType'',''formloanauditprocess_locbrowsetype_value'',1,'''&clip(0)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locBrowseType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locBrowseType',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locBrowseType_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Awaiting Audit') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locBrowseType') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locBrowseType'',''formloanauditprocess_locbrowsetype_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locBrowseType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locBrowseType',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locBrowseType_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Audited Units') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormLoanAuditProcess_' & p_web._nocolon('locBrowseType') & '_value')

Comment::locBrowseType  Routine
    loc:comment = ''
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('locBrowseType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::brwAudit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwAudit',p_web.GetValue('NewValue'))
    do Value::brwAudit
  Else
    p_web.StoreValue('lau:Internal_No')
  End

Value::brwAudit  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseAuditedLoanUnits --
  p_web.SetValue('BrowseAuditedLoanUnits:NoForm',1)
  p_web.SetValue('BrowseAuditedLoanUnits:FormName',loc:formname)
  p_web.SetValue('BrowseAuditedLoanUnits:parentIs','Form')
  p_web.SetValue('_parentProc','FormLoanAuditProcess')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormLoanAuditProcess_BrowseAuditedLoanUnits_embedded_div')&'"><!-- Net:BrowseAuditedLoanUnits --></div><13,10>'
    p_web._DivHeader('FormLoanAuditProcess_' & lower('BrowseAuditedLoanUnits') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormLoanAuditProcess_' & lower('BrowseAuditedLoanUnits') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseAuditedLoanUnits --><13,10>'
  end
  do SendPacket

Comment::brwAudit  Routine
    loc:comment = ''
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('brwAudit') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnCompleteAudit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnCompleteAudit',p_web.GetValue('NewValue'))
    do Value::btnCompleteAudit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnCompleteAudit  Routine
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('btnCompleteAudit') & '_value',Choose(p_web.GSV('Hide:CompleteAudit') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CompleteAudit') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('loanAuditCompleteConfirm()')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnCompleteAudit','Complete Audit','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormLoanAuditProcess_' & p_web._nocolon('btnCompleteAudit') & '_value')

Comment::btnCompleteAudit  Routine
    loc:comment = ''
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('btnCompleteAudit') & '_comment',Choose(p_web.GSV('Hide:CompleteAudit') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CompleteAudit') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnSuspendAudit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnSuspendAudit',p_web.GetValue('NewValue'))
    do Value::btnSuspendAudit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::btnCompleteAudit  !1
  do Value::btnCompleteStockType  !1
  do Value::btnContinueAudit  !1
  do Value::btnSuspendAudit  !1
  do Value::locIMEINumber  !1
  do Value::locStockType  !1

Value::btnSuspendAudit  Routine
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('btnSuspendAudit') & '_value',Choose(p_web.GSV('Hide:SuspendAudit') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:SuspendAudit') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnSuspendAudit'',''formloanauditprocess_btnsuspendaudit_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnSuspendAudit','Suspend Audit','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('AuditProcess?' &'ProcessType=LoanSuspendAudit&okURL=FormLoanAudits&errorURL=FormLoanAuditProcess')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormLoanAuditProcess_' & p_web._nocolon('btnSuspendAudit') & '_value')

Comment::btnSuspendAudit  Routine
    loc:comment = ''
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('btnSuspendAudit') & '_comment',Choose(p_web.GSV('Hide:SuspendAudit') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:SuspendAudit') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnContinueAudit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnContinueAudit',p_web.GetValue('NewValue'))
    do Value::btnContinueAudit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::btnCompleteAudit  !1
  do Value::btnCompleteStockType  !1
  do Value::btnContinueAudit  !1
  do Value::btnSuspendAudit  !1
  do Value::locIMEINumber  !1
  do Value::locStockType  !1
  do Value::lmf:Status  !1
  do Value::brwAudit  !1

Value::btnContinueAudit  Routine
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('btnContinueAudit') & '_value',Choose(p_web.GSV('Hide:ContinueAudit') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ContinueAudit') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnContinueAudit'',''formloanauditprocess_btncontinueaudit_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnContinueAudit','Continue Audit','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormLoanAuditProcess?' &'setprogress=inprogress')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormLoanAuditProcess_' & p_web._nocolon('btnContinueAudit') & '_value')

Comment::btnContinueAudit  Routine
    loc:comment = ''
  p_web._DivHeader('FormLoanAuditProcess_' & p_web._nocolon('btnContinueAudit') & '_comment',Choose(p_web.GSV('Hide:ContinueAudit') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ContinueAudit') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormLoanAuditProcess_locStockType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locStockType
      else
        do Value::locStockType
      end
  of lower('FormLoanAuditProcess_locIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIMEINumber
      else
        do Value::locIMEINumber
      end
  of lower('FormLoanAuditProcess_btnCompleteStockType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnCompleteStockType
      else
        do Value::btnCompleteStockType
      end
  of lower('FormLoanAuditProcess_locBrowseType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locBrowseType
      else
        do Value::locBrowseType
      end
  of lower('FormLoanAuditProcess_btnSuspendAudit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnSuspendAudit
      else
        do Value::btnSuspendAudit
      end
  of lower('FormLoanAuditProcess_btnContinueAudit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnContinueAudit
      else
        do Value::btnContinueAudit
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormLoanAuditProcess_form:ready_',1)
  p_web.SetSessionValue('FormLoanAuditProcess_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormLoanAuditProcess',0)

PreCopy  Routine
  p_web.SetValue('FormLoanAuditProcess_form:ready_',1)
  p_web.SetSessionValue('FormLoanAuditProcess_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormLoanAuditProcess',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormLoanAuditProcess_form:ready_',1)
  p_web.SetSessionValue('FormLoanAuditProcess_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormLoanAuditProcess:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormLoanAuditProcess_form:ready_',1)
  p_web.SetSessionValue('FormLoanAuditProcess_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormLoanAuditProcess:Primed',0)
  p_web.setsessionvalue('showtab_FormLoanAuditProcess',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormLoanAuditProcess_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormLoanAuditProcess_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
          locStockType = Upper(locStockType)
          p_web.SetSessionValue('locStockType',locStockType)
        If loc:Invalid <> '' then exit.
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormLoanAuditProcess:Primed',0)
  p_web.StoreValue('lmf:Status')
  p_web.StoreValue('locInStock')
  p_web.StoreValue('locCounted')
  p_web.StoreValue('locVariance')
  p_web.StoreValue('locStockType')
  p_web.StoreValue('')
  p_web.StoreValue('locStatus')
  p_web.StoreValue('locIMEINumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locBrowseType')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
spacer  Routine
  packet = clip(packet) & |
    '<<br/><13,10>'&|
    '<<br/><13,10>'&|
    '<<br/><13,10>'&|
    '<<br/><13,10>'&|
    ''
CompleteStockType	PROCEDURE()
	CODE
		Access:LOANALC.ClearKey(lac1:Locate_Key)
		lac1:Audit_Number = p_web.GSV('lmf:Audit_Number')
		lac1:Location = p_web.GSV('BookingSiteLocation')
		lac1:Stock_Type = p_web.GSV('locStockType')
		IF (Access:LOANALC.TryFetch(lac1:Locate_Key) = Level:Benign)
			IF (lac1:Confirmed = 0)
				lac1:Confirmed = 1
				Access:LOANALC.TryUpdate()
				p_web.SSV('locStatus','STOCK TYPE AUDIT COMPLETE')
			ELSE ! IF
				loc:alert = 'This stock type has already been completed. You cannot complete it again.'
				loc:invalid = 'btnCompleteStockType'
				RETURN
			END ! IF
		ELSE ! IF
			loc:alert = 'Stock Type ' & p_web.GSV('locStockType') & ' is not currently being audited. Therefore you cannot complete it.'
			loc:invalid = 'btnCompleteStockType'
			RETURN
		END ! IF
LoanAuditProcessIMEI	PROCEDURE()
	CODE
		IF (p_web.GSV('locIMEINumber') = '')
			RETURN
		END ! IF
		Access:LOANAUI.ClearKey(lau:Locate_IMEI_Key)
		lau:Audit_Number = p_web.GSV('lmf:Audit_Number')
		lau:Site_Location = p_web.GSV('BookingSiteLocation')
		lau:IMEI_Number = p_web.GSV('locIMEINumber')
		IF (Access:LOANAUI.TryFetch(lau:Locate_IMEI_Key) = Level:Benign)
			IF (lau:Stock_Type <> p_web.GSV('locStockType'))
				loc:alert = 'This unit exists in a different loan/stock type. Please ensure it is in the correct location.'
				loc:Invalid = 'locIMEINumber'
				RETURN
			ELSE ! IF
				IF (lau:Confirmed = 1)
					loc:alert = 'The selected I.M.E.I. Number has already been scanned.'
					loc:Invalid = 'locIMEINumber'
				RETURN
				ELSE ! IF
					lau:Confirmed = 1
					p_web.SSV('locCounted',p_web.GSV('locCounted') + 1)
					Access:LOANAUI.TryUpdate()
					p_web.SSV('locIMEINumber','')
				END ! IF
			END ! IF
		ELSE ! IF
 
			loc:alert = 'This I.M.E.I. number is not recognized in the exchange or loan databases..'
				loc:Invalid = 'locIMEINumber'
				RETURN
		END ! IF

        p_web.SSV('locVariance',p_web.GSV('locCounted') - p_web.GSV('locInStock'))
        p_web.SSV('BrowseAuditedLoanUnits_FirstValue','')

