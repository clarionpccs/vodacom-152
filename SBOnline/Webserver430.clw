

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER430.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER010.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER024.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER079.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER157.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER202.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER431.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER432.INC'),ONCE        !Req'd for module callout resolution
                     END


QAProcedure          PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locJobNumber         LONG                                  !
locSerialNumber      STRING(30)                            !
locQAType            STRING(10)                            !
locModelNumber       STRING(30)                            !
locHandsetType       STRING(30)                            !
locDateBooked        LONG                                  !
                    MAP
AllocatePreviousEngineer        PROCEDURE()
LookupJob               PROCEDURE()
FailRepairExchangeLoan  PROCEDURE(STRING pType),LONG
PassExchangeLoan        PROCEDURE(STRING pType),LONG
PassRepair              PROCEDURE(),LONG
RestoreAndSaveFilesFromSessionQ PROCEDURE(),LONG
UpdateJob               PROCEDURE()
UpdateJobChildren       PROCEDURE(LONG pJobNumber)
                    END !MAP
FilesOpened     Long
BOUNCER::State  USHORT
MANUFACT::State  USHORT
JOBNOTES::State  USHORT
JOBSE2::State  USHORT
EXCHHIST::State  USHORT
LOANHIST::State  USHORT
JOBSENG::State  USHORT
USERS::State  USHORT
SUBTRACC::State  USHORT
TRADEACC::State  USHORT
LOAN::State  USHORT
EXCHANGE::State  USHORT
WEBJOB::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
    MAP
trace	PROCEDURE(STRING pText)	
    END
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('QAProcedure')
  loc:formname = 'QAProcedure_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'QAProcedure',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('QAProcedure','')
    p_web._DivHeader('QAProcedure',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferQAProcedure',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferQAProcedure',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferQAProcedure',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_QAProcedure',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferQAProcedure',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_QAProcedure',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'QAProcedure',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(BOUNCER)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(JOBNOTES)
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(EXCHHIST)
  p_web._OpenFile(LOANHIST)
  p_web._OpenFile(JOBSENG)
  p_web._OpenFile(USERS)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(BOUNCER)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(JOBNOTES)
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(LOANHIST)
  p_Web._CloseFile(JOBSENG)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('QAProcedure_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('locDateBooked')
    p_web.SetPicture('locDateBooked','@d06b')
  End
  p_web.SetSessionPicture('locDateBooked','@d06b')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locQAType',locQAType)
  p_web.SetSessionValue('locJobNumber',locJobNumber)
  p_web.SetSessionValue('locSerialNumber',locSerialNumber)
  p_web.SetSessionValue('locModelNumber',locModelNumber)
  p_web.SetSessionValue('locHandsetType',locHandsetType)
  p_web.SetSessionValue('locDateBooked',locDateBooked)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locQAType')
    locQAType = p_web.GetValue('locQAType')
    p_web.SetSessionValue('locQAType',locQAType)
  End
  if p_web.IfExistsValue('locJobNumber')
    locJobNumber = p_web.GetValue('locJobNumber')
    p_web.SetSessionValue('locJobNumber',locJobNumber)
  End
  if p_web.IfExistsValue('locSerialNumber')
    locSerialNumber = p_web.GetValue('locSerialNumber')
    p_web.SetSessionValue('locSerialNumber',locSerialNumber)
  End
  if p_web.IfExistsValue('locModelNumber')
    locModelNumber = p_web.GetValue('locModelNumber')
    p_web.SetSessionValue('locModelNumber',locModelNumber)
  End
  if p_web.IfExistsValue('locHandsetType')
    locHandsetType = p_web.GetValue('locHandsetType')
    p_web.SetSessionValue('locHandsetType',locHandsetType)
  End
  if p_web.IfExistsValue('locDateBooked')
    locDateBooked = p_web.dformat(clip(p_web.GetValue('locDateBooked')),'@d06b')
    p_web.SetSessionValue('locDateBooked',locDateBooked)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('QAProcedure_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    IF (p_web.IfExistsValue('firsttime'))
        IF (p_web.GetValue('firsttime') = 1)
            ! Clear Vars
            p_web.SSV('locJobNumber','')
            p_web.SSV('locSerialNumber','')
            p_web.SSV('locQAType','')
            p_web.SSV('locModelNumber','')
            p_web.SSV('locHandsetType','')
            p_web.SSV('locDateBooked','')
            p_web.SSV('FailReason','')
            p_web.SSV('FailNotes','')
            ClearTagFile(p_web)
            ClearSBOGenericFile(p_web)
            p_web.SSV('QAJobsUpdated','')
        END ! IF
    END ! IF
    IF (p_web.IfExistsValue('clear'))
        IF (p_web.GetValue('clear') = 1)
            ! Move onto next job.
            p_web.SSV('locJobNumber','')
            p_web.SSV('locSerialNumber','')
            p_web.SSV('locModelNumber','')
            p_web.SSV('locHandsetType','')
            p_web.SSV('locDateBooked','')
            p_web.SSV('FailReason','')
            p_web.SSV('FailNotes','')            
            ClearTagFile(p_web)            
        END ! IF
    END ! IF
    
    ! Pass a return URL because this is called from different placed
    IF (p_web.IfExistsValue('QAProcReturnURL'))
        p_web.StoreValue('QAProcReturnURL')
    END !I F    
    
    IF (p_web.IfExistsValue('upd'))
        IF (p_web.GetValue('upd') = 1)
            IF (p_web.GSV('FailReason') <> '')
                UpdateJob()
                loc:invalid = TRUE
                EXIT
            END ! IF
        END ! IF
    END ! IF
    
      p_web.site.CancelButton.TextValue = 'Close'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locQAType = p_web.RestoreValue('locQAType')
 locJobNumber = p_web.RestoreValue('locJobNumber')
 locSerialNumber = p_web.RestoreValue('locSerialNumber')
 locModelNumber = p_web.RestoreValue('locModelNumber')
 locHandsetType = p_web.RestoreValue('locHandsetType')
 locDateBooked = p_web.RestoreValue('locDateBooked')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferQAProcedure')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('QAProcedure_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('QAProcedure_ChainTo')
    loc:formaction = p_web.GetSessionValue('QAProcedure_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('QAProcReturnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="QAProcedure" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="QAProcedure" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="QAProcedure" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('QA Procedure') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('QA Procedure',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_QAProcedure">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_QAProcedure" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_QAProcedure')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Select QA Type') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Enter Job Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Jobs Updated This Session') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_QAProcedure')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_QAProcedure'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_QAProcedure')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Select QA Type') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_QAProcedure_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select QA Type')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Select QA Type')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select QA Type')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select QA Type')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locQAType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locQAType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&250&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locQAType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::_hidden
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&250&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::_hidden
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Enter Job Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_QAProcedure_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Job Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&250&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::_hidden2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&250&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::_hidden2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSerialNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSerialNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&250&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locSerialNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locModelNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&250&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locHandsetType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locHandsetType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&250&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locHandsetType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locDateBooked
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locDateBooked
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&250&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locDateBooked
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnFaultCodes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&250&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnFaultCodes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnQAPass
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&250&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnQAPass
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnQAFail
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&250&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnQAFail
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Jobs Updated This Session') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_QAProcedure_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Updated This Session')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Updated This Session')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Updated This Session')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Updated This Session')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::QAJobsUpdated
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&250&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::QAJobsUpdated
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::_hidden3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&250&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::_hidden3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locQAType  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('locQAType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('QA Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('QAProcedure_' & p_web._nocolon('locQAType') & '_prompt')

Validate::locQAType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locQAType',p_web.GetValue('NewValue'))
    locQAType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locQAType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locQAType',p_web.GetValue('Value'))
    locQAType = p_web.GetValue('Value')
  End
    DO Prompt::locJobNumber
    DO Prompt::locSerialNumber
    DO Prompt::locModelNumber
    DO Prompt::locHandsetType
    DO Prompt::locDateBooked
    DO Value::locJobNumber
    DO Value::locSerialNumber
    DO Value::locModelNumber
    DO Value::locHandsetType
    DO Value::locDateBooked
    DO Comment::locJobNumber
    DO Comment::locSerialNumber
    DO Comment::locModelNumber
    DO Comment::locHandsetType
    DO Comment::locDateBooked  
  do Value::locQAType
  do SendAlert
  do Prompt::locQAType

Value::locQAType  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('locQAType') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locQAType
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locQAType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(p_web.GSV('locQAType') <> '','disabled','')
    if p_web.GetSessionValue('locQAType') = 'MANUAL'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locQAType'',''qaprocedure_locqatype_value'',1,'''&clip('MANUAL')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locQAType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locQAType',clip('MANUAL'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locQAType_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Manual') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(p_web.GSV('locQAType') <> '','disabled','')
    if p_web.GetSessionValue('locQAType') = 'ELECTRONIC'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locQAType'',''qaprocedure_locqatype_value'',1,'''&clip('ELECTRONIC')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locQAType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locQAType',clip('ELECTRONIC'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locQAType_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Electronic') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('QAProcedure_' & p_web._nocolon('locQAType') & '_value')

Comment::locQAType  Routine
    loc:comment = ''
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('locQAType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::_hidden  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('_hidden',p_web.GetValue('NewValue'))
    do Value::_hidden
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::_hidden  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('_hidden') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::_hidden  Routine
    loc:comment = ''
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('_hidden') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locJobNumber  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('locJobNumber') & '_prompt',Choose(p_web.GSV('locQAType') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Job Number')
  If p_web.GSV('locQAType') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locJobNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobNumber',p_web.GetValue('NewValue'))
    locJobNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobNumber',p_web.GetValue('Value'))
    locJobNumber = p_web.GetValue('Value')
  End
  If locJobNumber = ''
    loc:Invalid = 'locJobNumber'
    loc:alert = p_web.translate('Job Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    LookupJob()  
  do Value::locJobNumber
  do SendAlert

Value::locJobNumber  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('locJobNumber') & '_value',Choose(p_web.GSV('locQAType') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locQAType') = '')
  ! --- STRING --- locJobNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locJobNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locJobNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobNumber'',''qaprocedure_locjobnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locJobNumber',p_web.GetSessionValueFormat('locJobNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('QAProcedure_' & p_web._nocolon('locJobNumber') & '_value')

Comment::locJobNumber  Routine
    loc:comment = p_web.Translate('Enter Value and press [TAB]')
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('locJobNumber') & '_comment',Choose(p_web.GSV('locQAType') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locQAType') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::_hidden2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('_hidden2',p_web.GetValue('NewValue'))
    do Value::_hidden2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::_hidden2  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('_hidden2') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::_hidden2  Routine
    loc:comment = ''
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('_hidden2') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locSerialNumber  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('locSerialNumber') & '_prompt',Choose(p_web.GSV('locQAType') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Serial Number')
  If p_web.GSV('locQAType') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locSerialNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSerialNumber',p_web.GetValue('NewValue'))
    locSerialNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSerialNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locSerialNumber',p_web.GetValue('Value'))
    locSerialNumber = p_web.GetValue('Value')
  End
  If locSerialNumber = ''
    loc:Invalid = 'locSerialNumber'
    loc:alert = p_web.translate('Serial Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    LookupJob()  
  do Value::locSerialNumber
  do SendAlert

Value::locSerialNumber  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('locSerialNumber') & '_value',Choose(p_web.GSV('locQAType') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locQAType') = '')
  ! --- STRING --- locSerialNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locSerialNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locSerialNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locSerialNumber'',''qaprocedure_locserialnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSerialNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locSerialNumber',p_web.GetSessionValueFormat('locSerialNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('QAProcedure_' & p_web._nocolon('locSerialNumber') & '_value')

Comment::locSerialNumber  Routine
    loc:comment = p_web.Translate('Enter Value and press [TAB]')
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('locSerialNumber') & '_comment',Choose(p_web.GSV('locQAType') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locQAType') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locModelNumber  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('locModelNumber') & '_prompt',Choose(p_web.GSV('locModelNumber') = '' OR p_web.GSV('locQAType') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Model Number:')
  If p_web.GSV('locModelNumber') = '' OR p_web.GSV('locQAType') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locModelNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locModelNumber',p_web.GetValue('NewValue'))
    locModelNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locModelNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locModelNumber',p_web.GetValue('Value'))
    locModelNumber = p_web.GetValue('Value')
  End

Value::locModelNumber  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('locModelNumber') & '_value',Choose(p_web.GSV('locModelNumber') = '' OR p_web.GSV('locQAType') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locModelNumber') = '' OR p_web.GSV('locQAType') = '')
  ! --- DISPLAY --- locModelNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locModelNumber'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locModelNumber  Routine
    loc:comment = ''
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('locModelNumber') & '_comment',Choose(p_web.GSV('locModelNumber') = '' OR p_web.GSV('locQAType') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locModelNumber') = '' OR p_web.GSV('locQAType') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locHandsetType  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('locHandsetType') & '_prompt',Choose(p_web.GSV('locModelNumber') = '' OR p_web.GSV('locQAType') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Handset Type:')
  If p_web.GSV('locModelNumber') = '' OR p_web.GSV('locQAType') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locHandsetType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locHandsetType',p_web.GetValue('NewValue'))
    locHandsetType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locHandsetType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locHandsetType',p_web.GetValue('Value'))
    locHandsetType = p_web.GetValue('Value')
  End

Value::locHandsetType  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('locHandsetType') & '_value',Choose(p_web.GSV('locModelNumber') = '' OR p_web.GSV('locQAType') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locModelNumber') = '' OR p_web.GSV('locQAType') = '')
  ! --- DISPLAY --- locHandsetType
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locHandsetType'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locHandsetType  Routine
    loc:comment = ''
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('locHandsetType') & '_comment',Choose(p_web.GSV('locModelNumber') = '' OR p_web.GSV('locQAType') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locModelNumber') = '' OR p_web.GSV('locQAType') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locDateBooked  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('locDateBooked') & '_prompt',Choose(p_web.GSV('locModelNumber') = '' OR p_web.GSV('locQAType') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Date Booked:')
  If p_web.GSV('locModelNumber') = '' OR p_web.GSV('locQAType') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locDateBooked  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locDateBooked',p_web.GetValue('NewValue'))
    locDateBooked = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locDateBooked
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locDateBooked',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    locDateBooked = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End

Value::locDateBooked  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('locDateBooked') & '_value',Choose(p_web.GSV('locModelNumber') = '' OR p_web.GSV('locQAType') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locModelNumber') = '' OR p_web.GSV('locQAType') = '')
  ! --- DISPLAY --- locDateBooked
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(format(p_web.GetSessionValue('locDateBooked'),'@d06b')) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locDateBooked  Routine
    loc:comment = ''
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('locDateBooked') & '_comment',Choose(p_web.GSV('locModelNumber') = '' OR p_web.GSV('locQAType') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locModelNumber') = '' OR p_web.GSV('locQAType') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnFaultCodes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnFaultCodes',p_web.GetValue('NewValue'))
    do Value::btnFaultCodes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnFaultCodes  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('btnFaultCodes') & '_value',Choose(p_web.GSV('locModelNumber') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locModelNumber') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnFaultCodes','Edit Fault Codes','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('JobFaultCodes')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::btnFaultCodes  Routine
    loc:comment = ''
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('btnFaultCodes') & '_comment',Choose(p_web.GSV('locModelNumber') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locModelNumber') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnQAPass  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnQAPass',p_web.GetValue('NewValue'))
    do Value::btnQAPass
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnQAPass  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('btnQAPass') & '_value',Choose(p_web.GSV('locModelNumber') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locModelNumber') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnQAPass','QA Pass','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('QAPass?' &'firsttime=1')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/psave.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::btnQAPass  Routine
    loc:comment = ''
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('btnQAPass') & '_comment',Choose(p_web.GSV('locModelNumber') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locModelNumber') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnQAFail  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnQAFail',p_web.GetValue('NewValue'))
    do Value::btnQAFail
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnQAFail  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('btnQAFail') & '_value',Choose(p_web.GSV('locModelNumber') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locModelNumber') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnQAFail','QA Fail','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('QA_FailReason')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/pcancel.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::btnQAFail  Routine
    loc:comment = ''
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('btnQAFail') & '_comment',Choose(p_web.GSV('locModelNumber') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locModelNumber') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::QAJobsUpdated  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('QAJobsUpdated',p_web.GetValue('NewValue'))
    do Value::QAJobsUpdated
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::QAJobsUpdated  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('QAJobsUpdated') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('listbox')&'">' & p_web.Translate(p_web.GSV('QAJobsUpdated'),1) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::QAJobsUpdated  Routine
    loc:comment = ''
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('QAJobsUpdated') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::_hidden3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('_hidden3',p_web.GetValue('NewValue'))
    do Value::_hidden3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::_hidden3  Routine
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('_hidden3') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::_hidden3  Routine
    loc:comment = ''
  p_web._DivHeader('QAProcedure_' & p_web._nocolon('_hidden3') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('QAProcedure_locQAType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locQAType
      else
        do Value::locQAType
      end
  of lower('QAProcedure_locJobNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobNumber
      else
        do Value::locJobNumber
      end
  of lower('QAProcedure_locSerialNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSerialNumber
      else
        do Value::locSerialNumber
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('QAProcedure_form:ready_',1)
  p_web.SetSessionValue('QAProcedure_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_QAProcedure',0)

PreCopy  Routine
  p_web.SetValue('QAProcedure_form:ready_',1)
  p_web.SetSessionValue('QAProcedure_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_QAProcedure',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('QAProcedure_form:ready_',1)
  p_web.SetSessionValue('QAProcedure_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('QAProcedure:Primed',0)

PreDelete       Routine
  p_web.SetValue('QAProcedure_form:ready_',1)
  p_web.SetSessionValue('QAProcedure_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('QAProcedure:Primed',0)
  p_web.setsessionvalue('showtab_QAProcedure',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('QAProcedure_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('QAProcedure_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
      If not (p_web.GSV('locQAType') = '')
        If locJobNumber = ''
          loc:Invalid = 'locJobNumber'
          loc:alert = p_web.translate('Job Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('locQAType') = '')
        If locSerialNumber = ''
          loc:Invalid = 'locSerialNumber'
          loc:alert = p_web.translate('Serial Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('QAProcedure:Primed',0)
  p_web.StoreValue('locQAType')
  p_web.StoreValue('')
  p_web.StoreValue('locJobNumber')
  p_web.StoreValue('')
  p_web.StoreValue('locSerialNumber')
  p_web.StoreValue('locModelNumber')
  p_web.StoreValue('locHandsetType')
  p_web.StoreValue('locDateBooked')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locJobNumber',locJobNumber) ! LONG
     p_web.SSV('locSerialNumber',locSerialNumber) ! STRING(30)
     p_web.SSV('locQAType',locQAType) ! STRING(10)
     p_web.SSV('locModelNumber',locModelNumber) ! STRING(30)
     p_web.SSV('locHandsetType',locHandsetType) ! STRING(30)
     p_web.SSV('locDateBooked',locDateBooked) ! LONG
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locJobNumber = p_web.GSV('locJobNumber') ! LONG
     locSerialNumber = p_web.GSV('locSerialNumber') ! STRING(30)
     locQAType = p_web.GSV('locQAType') ! STRING(10)
     locModelNumber = p_web.GSV('locModelNumber') ! STRING(30)
     locHandsetType = p_web.GSV('locHandsetType') ! STRING(30)
     locDateBooked = p_web.GSV('locDateBooked') ! LONG
AllocatePreviousEngineer PROCEDURE()
allocationAllowed	LONG
existingLocation	STRING(30)
engCount	 LONG
previousEngineer STRING(3)
	CODE
		allocationAllowed = 0
		Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
		sub:Account_Number = p_web.GSV('job:Account_Number')
		IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
		
		ELSE ! IF
			Access:TRADEACC.ClearKey(tra:Account_Number_Key)
			tra:Account_Number = sub:Main_Account_Number
			IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
				IF (tra:AllocateQAEng)
					allocationAllowed = 1
				END ! IF
			ELSE ! IF
			END !IF
		END !IF

		IF (allocationAllowed)
			existingLocation = ''
			Access:USERS.ClearKey(use:User_Code_Key)
			use:User_Code = p_web.GSV('job:Engineer')
			IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
				existingLocation = use:Location ! Store engineer's current location
			END !IF

			! Allocate job to previous engineer
			engCount = 0
			Access:JOBSENG.ClearKey(joe:JobNumberKey)
			joe:JobNumber = p_web.GSV('job:Ref_Number')
			SET(joe:JobNumberKey,joe:JobNumberKey)
			LOOP UNTIL Access:JOBSENG.Next() <> Level:Benign
				IF (joe:JobNumber <> p_web.GSV('job:Ref_Number'))
					BREAK
				END ! IF
				engCount += 1
				IF (engCount = 2)
					previousEngineer = joe:UserCode
					BREAK
				END ! IF
			END ! LOOP

			Access:USERS.ClearKey(use:User_Code_Key)
			use:User_Code = previousEngineer
			IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
				IF (use:Location = existingLocation)
					job:Engineer = previousEngineer

					IF (Access:JOBS.TryUpdate() = Level:Benign)
						AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','RAPID ENGINEER UPDATE: JOB ALLOCATED','FIELDS UPDATED:<13,10>ENGINEER: ' & CLIP(previousEngineer) & |
                              '<13,10>SKILL LEVEL: ' & Clip(use:SkillLevel))

						IF (Access:JOBSENG.PrimeRecord() = Level:Benign)
							joe:JobNumber = job:Ref_Number
							joe:UserCode = job:Engineer
							joe:DateAllocated = TODAY()
							joe:TimeAllocated = CLOCK()
							joe:AllocatedBy = p_web.GSV('BookingUserCode')
							joe:EngSkillLevel = use:SkillLevel
							joe:JobSkillLevel = p_web.GSV('jobe:SkillLevel')
							IF (Access:JOBSENG.TryInsert())
								Access:JOBSENG.CancelAutoInc()
							END ! IF
						END !IF
					END ! IF (Access:JOBS.TryUpdate() = Level:Benign)
				END ! IF
			ELSE ! IF
			END !IF

		END ! IF (allocationAllowed)
FailRepairExchangeLoan	PROCEDURE(STRING pType)!,LONG
unitDescription	CSTRING(15)
unitNumber	LONG
unitAvailable STRING(3)
auditType                   STRING(3)

	CODE
		CASE p_web.GSV('locQAType')
		OF 'MANUAL'
			GetStatus(615,1,pType,p_web) ! Failed QA

			IF (pType = 'EXC' AND p_web.GSV('jobe:Engineer48HourOption') = 3)
				! It's a 7 day TAT and failed QA Exchange. Add extra info
				GetStatus(126,1,pType,p_web) ! Failed QA				
			END ! IF (pType = 'EXC' AND p_web.GSV('jobe:Engineer48HourOption') = 3)
		ELSE
			GetStatus(625,1,pType,p_web) ! Failed E QA
		END ! CASE

		auditType = pType ! Set Type for Audit Entry

		CASE pType
		OF 'JOB'
			p_web.SSV('job:QA_Rejeceted', 'YES')
			p_web.SSV('job:Date_QA_Rejected', TODAY())
			p_web.SSV('job:Time_QA_Rejected', CLOCK())

			IF (p_web.GSV('job:Date_Completed') > 0)
				GetStatus(626,1,'JOB',p_web) ! ARC QA FAILED
				p_web.SSV('job:Location', 'DESPATCHED')
			ELSE ! IF
				AllocatePreviousEngineer()
			END ! IF
		OF 'LOA'
			unitDescription = 'LOAN' ! Set Description For Audit Entry
			unitAvailable = 'QAF' ! Set Loan/Exchange Availablility

		OF 'EXC' OROF '2NE'
			unitAvailable = 'QAF' ! Set Loan/Exchange Availablility

			unitDescription = 'EXCHANGE'  ! Set Description For Audit Entry
			auditType = 'EXC' ! Set Type for Audit Entry
		END!  CASE



		p_web.SSV('jobe:DespatchType', '')

		IF (p_web.GSV('jbn:Engineers_Notes') = '')
			p_web.SSV('jbn:Engineers_Notes', p_web.GSV('FailNotes'))
		ELSE ! IF
			p_web.SSV('jbn:Engineers_Notes', p_web.GSV('jbn:Engineers_Notes') & '; ' & p_web.GSV('FailNotes'))
		END ! IF

		IF (RestoreAndSaveFilesFromSessionQ())
            loc:alert = 'ERROR: Unable to save job details. Please try again.'
			RETURN Level:Fatal
		ELSE ! IF
			IF (pType = 'JOB')
                AddToAudit(p_web,p_web.GSV('job:Ref_Number'),pType,'QA REJECTION '&p_web.gsv('locQAType')&': '&p_web.gsv('FailReason'),'QA FAILURE REASON: ' & clip(p_web.gsv('FailNotes')))    			

                IF (p_web.GSV('job:Date_Completed') > 0)
					! This means it's an ARC job that has now failed at the RRC
                    AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','ARC QA FAILED: ' & p_web.GSV('FailReason'), p_web.GSV('FailNotes'))

                    CreateScript(p_web,packet,'window.open("PageProcess?ProcessType=QANewJob&alrt=1","_self")')
                END ! IF (pType = 'JOB' AND p_web.GSV('job:Date_Completed') > 0)		

			ELSE ! IF (pType = 'JOB')
				AddToAudit(p_web, |
							p_web.GSV('job:Ref_Number'), |
							pType, |
							'RAPID QA UPDATE: ' & p_web.gsv('locQAType') & ' QA FAILED (' & auditType & ')', |
							unitDescription & ' UNIT NUMBER: ' & unitNumber & '<13,10>QA FAILURE REASON: ' & p_web.gsv('FailReason') & '<13,10>' & clip(p_web.gsv('FailNotes')))    

				IF (pType = 'EXC' AND p_web.GSV('jobe:Engineer48HourOption') = 3)
					! It's a 7 day TAT and failed QA Exchange. Add extra info
					AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','EXCHANGE QA FAILED: ' & p_web.GSV('FailReason'),p_web.GSV('FailNotes'))

                    ! Book a new job.
                    CreateScript(p_web,packet,'window.open("PageProcess?ProcessType=QANewJob&alrt=1&e=1","_self")')
                    
				END ! IF (pType = 'EXC' AND p_web.GSV('jobe:Engineer48HourOption') = 3)		

			END ! IF (pType = 'JOB')

		END ! IF

		! Update relevant unit details and histories
        CASE pType
        OF 'LOA'
            Access:LOAN.ClearKey(loa:Ref_Number_Key)
            loa:Ref_Number = unitNumber
            IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
                IF (unitAvailable <> '')
					! Available not updated for every occurance
                    loa:Available = unitAvailable
                    loa:StatusChangeDate = TODAY()
                    Access:LOAN.TryUpdate()
                END ! IF
                IF (Access:LOANHIST.PrimeRecord() = Level:Benign)
                    loh:Ref_Number = loa:Ref_Number
                    loh:Date = TODAY()
                    loh:Time = CLOCK()
                    loh:User = p_web.GSV('BookingUserCode')
                    loh:Status = 'RAPID QA UPDATE: QA FAILED - ' & p_web.GSV('FailReason')
                    IF (Access:LOANHIST.TryInsert())
                        Access:LOANHIST.CancelAutoInc()
                    END ! IF
                END !IF
            ELSE ! IF
            END !IF
        OF 'EXC' OROF '2NE'
            Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
            xch:Ref_Number = unitNumber
            IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                IF (unitAvailable <> '')
					! Available not updated for every occurance
                    xch:Available = unitAvailable
                    xch:StatusChangeDate = TODAY()
                    Access:EXCHANGE.TryUpdate()
                END ! IF

                IF (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                    exh:Ref_Number = xch:Ref_Number
                    exh:Date = TODAY()
                    exh:Time = CLOCK()
                    exh:User = p_web.GSV('BookingUserCode')
                    loh:Status = 'RAPID QA UPDATE: QA FAILED - ' & p_web.GSV('FailReason')
                    IF (Access:EXCHHIST.TryInsert())
                        Access:EXCHHIST.CancelAutoInc()
                    END ! IF
                END !IF
            ELSE ! IF
            END !IF
        END!  CASE
        
        
        
        RETURN Level:Benign
LookupJob		PROCEDURE()
errorText		STRING(255)
	CODE
		IF (p_web.GSV('locJobNumber') = '' OR |
			p_web.GSV('locSerialNumber') = '')
			RETURN
		END ! IF
		
        DO ClearUnitFields
		
        LOOP 1 TIMES
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = p_web.GSV('locJobNumber')
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                loc:alert = 'ERROR: Job ' & p_web.GSV('locJobNumber') & ' not found. Please check and try again.'
                loc:invalid = 'locJobNumber'
                BREAK
            END ! IF
			
            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
                loc:alert = 'ERROR: Webjob Record not found for job number ' & p_web.GSV('locJobNumber') & '. Please check and try again.'
                loc:invalid = 'locJobNumber'
                BREAK
            END ! IF
			
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
				
            ELSE ! IF
            END ! IF
            
            Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
            jobe2:RefNumber = job:Ref_Number
            IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
			
            ELSE ! IF
            END ! IF
		
            Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
            jbn:RefNumber = job:Ref_Number
            IF (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
			
            ELSE ! IF
            END ! IF
			
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = wob:HeadAccountNumber
            IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
                loc:alert = 'ERROR: TradeAcc Record not found for job number ' & p_web.GSV('locJobNumber') & '. Please check and try again.'
                loc:invalid = 'locJobNumber'
                BREAK
            END ! IF
			
            IF (p_web.GSV('locSerialNumber') = job:ESN OR |
                p_web.GSV('locSerialNumber') = job:MSN)
				
                IF (job:Workshop <> 'YES')
                    loc:alert = 'ERROR: Job ' & p_web.GSV('locJobNumber') & ' cannot be updated as it is not in the workshop.'
                    loc:invalid = 'locJobNumber'
                    BREAK
                END ! IF
				
                IF ((job:Location <> p_web.GSV('Default:RRCLocation')) OR |
                    jobe:HubRepair= 1 OR |
                    wob:HeadAccountNumber <> p_web.GSV('BookingAccount'))
                    loc:alert = 'ERROR: Job ' & p_web.GSV('locJobNumber') & ' is out of RRC Control.'
                    loc:invalid = 'locJobNumber'
                    BREAK
                END ! IF
				
                IF (VodacomClass.JobSentToHub(job:Ref_Number) AND jobe:HubRepair = 0)
					! Sent to hub, but has now come back, so is allowed to be completed
                ELSE ! IF
                    IF (job:Date_Completed > 0)
                        loc:alert = 'ERROR: Job ' & p_web.GSV('locJobNumber') & ' cannot be updated as it is completed.'
                        loc:invalid = 'locJobNumber'                        
                        BREAK
                    END ! IF
                END ! IF
				
				! Success
                p_web.SSV('locModelNumber',CLIP(job:Model_Number) & ' - ' & CLIP(job:Manufacturer))
                IF (job:Third_Party_Site <> '')
                    p_web.SSV('locHandsetType','Third Party Repair')
                ELSE ! IF
                    p_web.SSV('locHandsetType','Repair')
                END ! IF
                p_web.SSV('locDateBooked',job:Date_Booked)
                BREAK
            ELSE ! IF
                p_web.SSV('locDateBooked',job:Date_Booked)
				
                IF (job:Exchange_Unit_Number > 0)
                    Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                    xch:Ref_Number = job:Exchange_Unit_Number
                    IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                        IF (xch:ESN = p_web.GSV('locSerialNumber') OR |
                            xch:MSN = p_web.GSV('locSerialNumber'))
                            p_web.SSV('locModelNumber',CLIP(xch:Model_Number) & ' - ' & CLIP(xch:Manufacturer))
                            p_web.SSV('locHandsetType','Exchange')
                            BREAK
                        END ! IF
                    ELSE ! IF
                    END ! IF
                END ! IF
				
                IF (job:Loan_Unit_Number > 0)
                    Access:LOAN.ClearKey(loa:Ref_Number_Key)
                    loa:Ref_Number = job:Loan_Unit_Number
                    IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
                        IF (loa:ESN = p_web.GSV('locSerialNumber') OR |
                            loa:MSN = p_web.GSV('locSerialNumber'))
                            p_web.SSV('locModelNumber',CLIP(loa:Model_Number) & ' - ' & CLIP(loa:Manufacturer))
                            p_web.SSV('locHandsetType','Loan')
                            BREAK
                        END ! IF
                    ELSE ! IF
                    END ! IF
                END ! IF
				
                IF (jobe:SecondExchangeNumber > 0)
                    Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                    xch:Ref_Number = jobe:SecondExchangeNumber
                    IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                        IF (xch:ESN = p_web.GSV('locSerialNumber') OR |
                            xch:MSN = p_web.GSV('locSerialNumber'))
                            p_web.SSV('locModelNumber',CLIP(xch:Model_Number) & ' - ' & CLIP(xch:Manufacturer))
                            p_web.SSV('locHandsetType','2nd Exchange')
                            BREAK
                        END ! IF						
                    ELSE ! IF
                    END ! IF
                END ! IF
			
                loc:alert = 'ERROR: The selected serial number does not exist on Job ' & p_web.GSV('locJobNumber')
                loc:invalid = 'locSerialNumber'
                BREAK
            END ! IF
			
        END ! IF
        
        IF (loc:invalid = '')
            ! Check if Manufacturer is actually setup to QA Loan/Exchange Units
            Access:MANUFACT.ClearKey(man:Manufacturer_Key)
            man:Manufacturer = job:Manufacturer
            IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
            
                CASE p_web.GSV('locHandsetType')
                OF 'Repair' OROF 'Third Party Repair'
                    IF (~man:UseQA)
                        loc:alert = 'ERROR: The Manufacturer of this job is not set-up to QA Loan/Exchange Units.'
                        loc:invalid = 'locJobNumber'   

                        DO ClearUnitFields
                    END ! IF
                ELSE ! CASE
                    IF (~man:QALoanExchange)
                        loc:alert = 'ERROR: The Manufacturer of this job is not set-up to QA Loan/Exchange Units.'
                        loc:invalid = 'locJobNumber'
                        DO ClearUnitFields                    
                    END ! IF
                        
                END ! IF
                    
            END ! CASE
            
        END ! IF
        
		
		! Refresh Fields
		DO Value::locJobNumber
        DO Value::locSerialNumber
        DO Prompt::locModelNumber
        DO Prompt::locHandsetType
        DO Prompt::locDateBooked
		DO Value::locModelNumber
		DO Value::locHandsetType
        DO Value::locDateBooked
        DO Value::btnFaultCodes
        DO Value::btnQAPass
        DO Value::btnQAFail
        
        IF (loc:invalid = '')
            
            trace('QAProcedure: Job Qs Loaded')
            
            p_web.FileToSessionQueue(JOBS)
            p_web.FileToSessionQueue(JOBSE)
            p_web.FileToSessionQueue(JOBSE2)
            p_web.FileToSessionQueue(JOBNOTES)
            p_web.FileToSessionQueue(WEBJOB)
        END ! IF

ClearUnitFields     ROUTINE
    p_web.SSV('locModelNumber','')
    p_web.SSV('locHandsetType','')
    p_web.SSV('locDateBooked','')
PassExchangeLoan    PROCEDURE(STRING pType)!,LONG
unitDescription	CSTRING(15)
unitNumber	LONG
statusNumber LONG
unitAvailable STRING(3)
auditStatus STRING(255)
    CODE
        trace('QAProcedure: PassExchangeLoan = ' & pType)
        CASE pType
        OF 'LOA' 
            unitDescription = 'LOAN' ! Set description for use in audit trail entry
            unitNumber = p_web.GSV('job:Loan_Unit_Number') ! Get unit no to lookup later

			! Job is ready for despatch for loan unit
            p_web.SSV('jobe:Despatched', 'REA')
            p_web.SSV('jobe:DespatchType', 'LOA')
            p_web.SSV('wob:ReadyToDespatch', 1)
            p_web.SSV('wob:DespatchCourier', p_web.GSV('job:Loan_Courier'))				
			
            CASE p_web.GSV('locQAType')
            OF 'MANUAL'
                statusNumber = 105 ! Despatch Loan Unit
                unitAvailable = '' ! Do not update Loan Unit's Availablility
                auditStatus = 'QA PASS ON JOB NO: ' & p_web.GSV('job:Ref_Number') ! Loan Audit Entry				
            OF 'ELECTRONIC'
                statusNumber = 620 ! Electronic QA Passed
                unitAvailable = 'QA2'
                auditStatus = 'RAPID QA UPDATE: ' & p_web.GSV('locQAType') &' QA PASSED' ! Loan Audit Entry			
            END!  CASE

        OF 'EXC'
            unitDescription = 'EXCHANGE' ! Set description for use in audit trail entry
            unitNumber = p_web.GSV('job:Exchange_Unit_Number') ! Get unit no to lookup later

			! Job is ready for despatch of exchange unit (not sure if right for electronic QA, but thats what it does)
            p_web.SSV('jobe:Despatched', 'REA')
            p_web.SSV('jobe:DespatchType', 'EXC')
            p_web.SSV('wob:ReadyToDespatch', 1)
            p_web.SSV('wob:DespatchCourier', p_web.GSV('job:Exchange_Courier'))

            CASE p_web.GSV('locQAType')
            OF 'MANUAL'
                statusNumber = 110 ! Despatch Exchange Unit
                unitAvailable = '' ! Do not update Exchange Unit's Availablility
                auditStatus = 'QA PASS ON JOB NO: ' & p_web.GSV('job:Ref_Number') ! Exchange Audit Entry				
            OF 'ELECTRONIC'
                statusNumber = 620 ! Electronic QA Passed
                unitAvailable = 'QA2'
                auditStatus = 'RAPID QA UPDATE: ' & p_web.GSV('locQAType') &' QA PASSED' ! Exchange Audit Entry			
            END!  CASE

        OF '2NE'
            unitDescription = '2ND EXCHANGE' ! Set description for use in audit trail entry
            unitNumber = p_web.GSV('jobe:SecondExchangeNumber') ! Get unit no to lookup later

            CASE p_web.GSV('locQAType')
            OF 'MANUAL'
                statusNumber = 708	! Return To Stock
                unitAvailable = '' ! Do not update Exchange Unit's Availablility
                auditStatus = 'QA PASS ON JOB NO: ' & p_web.GSV('job:Ref_Number') ! Exchange Audit Entry			
            OF 'ELECTRONIC'
                statusNumber = 620 ! Electronic QA Passed
                unitAvailable = 'QA2'
                auditStatus = 'RAPID QA UPDATE: ' & p_web.GSV('locQAType') &' QA PASSED' ! Exchange Audit Entry			
            END!  CASE

        END!  CASE
        
        trace('QAProcedure: Set Status = ' & statusNumber)

		GetStatus(statusNumber,1,pType,p_web) ! Set job/exchange/2nd exchange status

		IF (RestoreAndSaveFilesFromSessionQ()) ! Save Job
            loc:alert = 'ERROR: Unable to save job details. Please try again.'
            RETURN Level:Fatal
		ELSE ! IF
			! Add audit entry
			AddToAudit(p_web, |
						p_web.GSV('job:Ref_Number'), |
						pType, |
						'RAPID QA UPDATE: ' & p_web.gsv('locQAType') & ': ' & 'QA PASSED (' & pType & ')', |
						unitDescription & ' UNIT NUMBER: ' & unitNumber)
		END ! IF

		! Update relevant unit details and histories
        CASE pType
        OF 'LOA'
            Access:LOAN.ClearKey(loa:Ref_Number_Key)
            loa:Ref_Number = unitNumber
            IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
                IF (unitAvailable <> '')
					! Available not updated for every occurance
                    loa:Available = unitAvailable
                    loa:StatusChangeDate = TODAY()
                    Access:LOAN.TryUpdate()
                END ! IF
                IF (Access:LOANHIST.PrimeRecord() = Level:Benign)
                    loh:Ref_Number = loa:Ref_Number
                    loh:Date = TODAY()
                    loh:Time = CLOCK()
                    loh:User = p_web.GSV('BookingUserCode')
                    loh:Status = 'RAPID QA UPDATE: ' & p_web.gsv('locQAType') & ' QA PASSED'
                    IF (Access:LOANHIST.TryInsert())
                        Access:LOANHIST.CancelAutoInc()
                    END ! IF
                END !IF
            ELSE ! IF
            END !IF
        OF 'EXC' OROF '2NE'
            Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
            xch:Ref_Number = unitNumber
            IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                IF (unitAvailable <> '')
					! Available not updated for every occurance
                    xch:Available = unitAvailable
                    xch:StatusChangeDate = TODAY()
                    Access:EXCHANGE.TryUpdate()
                END ! IF

                IF (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                    exh:Ref_Number = xch:Ref_Number
                    exh:Date = TODAY()
                    exh:Time = CLOCK()
                    exh:User = p_web.GSV('BookingUserCode')
                    exh:Status = CLIP(auditStatus)
                    IF (Access:EXCHHIST.TryInsert())
                        Access:EXCHHIST.CancelAutoInc()
                    END ! IF
                END !IF
            ELSE ! IF
            END !IF
        END!  CASE
        
        RETURN Level:Benign
PassRepair          PROCEDURE()!,LONG
RetValue                LONG(Level:Benign)
locPaid                 LONG
recs                    LONG
i                       LONG
    CODE
        trace('QAProcedure: locQAType = ' & p_web.GSV('locQAType'))
        
        
		CASE p_web.GSV('locQAType')
		OF 'ELECTRONIC'
			GetStatus(620,1,'JOB',p_web)
			
		OF 'MANUAL'
            LOOP 1 TIMES
			! Do completion field check
                p_web.SSV('locCompleteRepair',CompulsoryFieldCheck(p_web,'C'))
			
                IF (p_web.GSV('locCompleteRepair') <> '')
				! Failed completion check
!                loc:alert = 'ERROR: The repair cannot be completed before of the following reasons:\n\n' & BHStripReplace(p_web.GSV('locCompleteRepair'),'<13,10>','\n') & '\nPlease amend or fail this job.'
                ! loc:alert isn't long enough for all the text returned.
                    CreateScript(p_web,packet,'alert("ERROR: The repair cannot be completed before of the following reasons:\n\n' & BHStripReplace(p_web.GSV('locCompleteRepair'),'<13,10>','\n') & '")')
                ! redirect to QA Failure
                    p_web.SSV('FailReason','')
                    p_web.SSV('FailNotes','')
                    CreateScript(p_web,packet,'window.open("QA_FailReason","_self")')
                    DO SendPacket
                    RetValue = Level:Fatal
                    BREAK
                END ! IF
			
                GetStatus(705,1,'JOB',p_web)
			
                IF (~p_web.GSV('job:Date_Completed') > 0)
                    p_web.SSV('job:Date_Completed',TODAY())
                    p_web.SSV('job:Time_Completed',CLOCK())
                    p_web.SSV('job:QA_Passed','YES')
                    p_web.SSV('job:Date_QA_Passed',TODAY())
                    p_web.SSV('job:Time_QA_Passed',CLOCK())
                    p_web.SSV('wob:DateCompleted',p_web.GSV('job:Date_Completed'))
                    p_web.SSV('wob:TimeCompleted',p_web.GSV('job:Time_Completed'))
                    p_web.SSV('wob:Completed','YES')
                    p_web.SSV('job:Completed','YES')
                    trace('QAProcedure: job:Date_Completed = ' & p_web.GSV('job:Date_Completed'))				
                END ! IF
                
                trace('QAProcedure: Job: ' & p_web.GSV('job:Ref_Number'))
                trace('QAProcedure: Complete: ' & p_web.GSV('job:Date_Completed'))
                
                ! #13487 Check for Bouncer BEFORE checking warranty status (DBH: 20/02/2015)
                IF (JobSentToARC(p_web) = 0)
                    ! #13622 Do not Bouncer Check a job that has been sent to the ARC (DBH: 02/11/2015)
                    IF (VodacomClass.AccountCheckForBouncer(p_web.GSV('job:Account_Number')) = FALSE)
                        IF (VodacomClass.CountJobBouncers(p_web))
        
                            recs = RECORDS(qBouncedJobs)
                        ! Add job and bounced job(s) to bouncer table
                            LOOP i = 1 TO recs
                                GET(qBouncedJobs,i)
		
                                IF (qBouncedJobs.SessionID <> p_web.SessionID)
                                    CYCLE
                                END ! IF
		
                                IF (Access:BOUNCER.PrimeRecord() = Level:Benign)
                                    bou:Original_Ref_Number = p_web.GSV('job:Ref_Number')
                                    bou:Bouncer_Job_Number = qBouncedJobs.JobNumber
                                    IF (Access:BOUNCER.TryInsert())
                                        Access:BOUNCER.CancelAutoInc()
                                    END ! IF
                                END ! IF
                            END ! LOOP
        
                            If p_web.GSV('job:chargeable_job') = 'YES' And p_web.GSV('job:warranty_job') = 'YES'
                                p_web.SSV('job:bouncer_type','BOT')
                            End!If job:chargeable_job = 'YES' And job:warranty_job = 'YES'
                            If p_web.GSV('job:chargeable_job') = 'YES' And p_web.GSV('job:warranty_job') <> 'YES'
                                p_web.SSV('job:bouncer_type','CHA')
                            End!If job:chargeable_job = 'YES' And job:warranty_job <> 'YES'
                            IF p_web.GSV('job:chargeable_job') <> 'YES' And p_web.GSV('job:warranty_job') = 'YES'
                                p_web.SSV('job:bouncer_type','WAR')
                            End!IF job:chargeable_job <> 'YES' And job:warranty_job = 'YES'
                            p_web.SSV('job:bouncer','X')
        
                        END ! IF
                    END ! IF
                END ! IF
                
			
                PendingJob(p_web)
			
                IF (p_web.GSV('job:Warranty_Job') = 'YES')
                    IF (p_web.GSV('jobe:WarrantyClaimStatus') = '')
                        p_web.SSV('jobe:WarrantyClaimStatus','PENDING')
                    END ! IF
                END ! IF
			
                locPaid = 0
                Access:COURIER.ClearKey(cou:Courier_Key)
                cou:Courier = p_web.GSV('job:Courier_Key')
                IF (Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign)
                    IF (p_web.GSV('job:Warranty_Job') = 'YES')
                        IF (p_web.GSV('job:EDI') = 'FIN')
                            locPaid = 1
                        END ! IF
                    END ! IF
                    IF (p_web.GSV('job:Chargeable_Job') = 'YES')
                        TotalPrice(p_web,'C',vat$,tot$,bal$)
                        IF (tot$ = 0 OR bal$ <= 0)
                            locPaid = 1
                        END ! IF
                    END ! IF
                ELSE ! IF
                END ! IF
			
                IF (p_web.GSV('job:Date_Completed') > 0)
				! IF completed, then mark as despatched
                    p_web.SSV('wob:ReadyToDespatch',1)							
                    p_web.SSV('job:Despatched','REA')
                    p_web.SSV('job:Date_Despatched',TODAY())
                    p_web.SSV('job:Consignment_Number','N/A')
                    p_web.SSV('job:Despatch_User',p_web.GSV('BookingUserCode'))
                    p_web.SSV('jobe:DespatchType','JOB')
                    p_web.SSV('wob:DespatchCourier',p_web.GSV('job:Courier'))
                    
                    trace('QAProcedure: job:Date_Despatched = ' & p_web.GSV('job:Date_Despatched'))				
                    
                END !IF					
			

			
                Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                tra:Account_Number = p_web.GSV('BookingAccount')
                IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
				
                ELSE ! IF
                END ! IF
			
                IF (tra:ExchangeAcc = 'YES')
                    GetStatus(707,1,'JOB',p_web)
                ELSE ! IF
                    IF (p_web.GSV('jobe:OBFValidated') = 1)
                        GetStatus(710,1,'JOB',p_web)
                    ELSE ! IF
                        IF (p_web.GSV('job:Who_Booked') = 'WEB')
						! PUP
                            GetStatus(Sub(GETINI('RRC','StatusDespatchToPUP','463 SEND TO PUP',Clip(Path()) & '\SB2KDEF.INI'),1,3),0,'JOB',p_web)
                        ELSE ! IF
                            IF (cou:CustomerCollection)
                                IF (locPaid)
                                    GetStatus(915,0,'JOB',p_web) ! Paid Awaiting Collection
                                ELSE ! IF (locPaid)
                                    GetStatus(805,0,'JOB',p_web) ! Ready To Collect
                                END ! IF (locPaid)
                            ELSE ! IF
                                IF (locPaid)
                                    GetStatus(916,0,'JOB',p_web) ! Paid Awaiting Despatch
                                ELSE ! IF (locPaid)
                                    GetStatus(810,0,'JOB',p_web) ! Ready To Despatch
                                END ! IF (locPaid)
                            END ! IF
                        END ! IF
                    END ! IF
                END ! IF
            END ! LOOP
		END ! IF
		
        IF (RestoreAndSaveFilesFromSessionQ())
            loc:alert = 'ERROR: Unable to save job details. Please try again.'
            RetValue = Level:Fatal
        ELSE ! IF			
		    trace('QAProcedure: JobSaved')
            AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','RAPID QA UPDATE',p_web.gsv('locQAType')&' QA PASSED')
            
            IF (p_web.GSV('wob:DateCompleted') <> p_web.GSV('job:Date_Completed'))
                AddToLog('Debug',p_web.GSV('job:Ref_Number'),'WEBJOB Not Updated at QA Complete',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
            END ! IF
			
            IF (p_web.GSV('job:Bouncer') = 'X')
                ! #13487 Show Bouncer err1701or (DBH: 20/02/2015)
                AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','JOB MARKED AS BOUNCER','')
                loc:alert = 'This job has been found to be a Bouncer. It can now only be Invoiced after it has been authorised from the Bouncer List.'
            END ! IF
            
            UpdateJobChildren(job:Ref_Number)	
        END ! IF
        
        RETURN RetValue
RestoreAndSaveFilesFromSessionQ	PROCEDURE()!,LONG
	CODE
		Access:JOBS.ClearKey(job:Ref_Number_Key)
		job:Ref_Number = p_web.GSV('job:Ref_Number')
		IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
			
        ELSE ! IF
            AddToLog('Debug',ERROR(),'QA Error (Cannot Get JOB)',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
            RETURN Level:Fatal
		END ! IF
		
		Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
		jobe2:RefNumber = job:Ref_Number
		IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
			
        ELSE ! IF
            AddToLog('Debug',ERROR(),'QA Error (Cannot Get JOBSE2)',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
            RETURN Level:Fatal
		END ! IF
		
		Access:JOBSE.ClearKey(jobe:RefNumberKey)
		jobe:RefNumber = job:Ref_Number
		IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
			
        ELSE ! IF
            AddToLog('Debug',ERROR(),'QA Error (Cannot Get JOBSE)',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
            RETURN Level:Fatal
		END ! IF
		
		Access:WEBJOB.ClearKey(wob:RefNumberKey)
		wob:RefNumber = job:Ref_Number
		IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
			
        ELSE ! IF
            AddToLog('Debug',ERROR(),'QA Error (Cannot Get WEBJOB)',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
            RETURN Level:Fatal
		END ! IF
		
		Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
		jbn:RefNumber = job:Ref_Number
		IF (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
			
        ELSE ! IF
            AddToLog('Debug',ERROR(),'QA Error (Cannot Get JOBNOTES)',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
            RETURN Level:Fatal
		END ! IF
		
		p_web.SessionQueueToFile(JOBS)
		p_web.SessionQueueToFile(JOBSE)
		p_web.SessionQueueToFile(JOBSE2)
		p_web.SessionQueueToFile(WEBJOB)
		p_web.SessionQueueToFile(JOBNOTES)

        IF (Access:JOBS.TryUpdate())
            AddToLog('Debug',ERROR(),'QA Error (Cannot Save JOB)',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
            RETURN Level:Fatal
        END ! IF      		
        IF (Access:JOBSE.TryUpdate())
            AddToLog('Debug',ERROR(),'QA Error (Cannot Save JOBSE)',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
            RETURN Level:Fatal
		END ! IF
        IF (Access:WEBJOB.TryUpdate())
            AddToLog('Debug',ERROR(),'QA Error (Cannot Save WEBJOB)',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
            RETURN Level:Fatal
		END ! IF
        IF (Access:JOBNOTES.TryUpdate())
            AddToLog('Debug',ERROR(),'QA Error (Cannot Save JOBNOTES)',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
            RETURN Level:Fatal
		END ! IF
        IF (Access:JOBSE2.TryUpdate())
            AddToLog('Debug',ERROR(),'QA Error (Cannot Save JOBSE2)',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
            RETURN Level:Fatal
        END ! IF
        
        RETURN Level:Benign
UpdateJobChildren   PROCEDURE(LONG pJobNumber)
    CODE
        Access:JOBSOBF.ClearKey(jof:RefNumberKey)
        jof:RefNumber = pJobNumber	
        IF (Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign)
            jof:DateCompleted = job:Date_Completed
            jof:TimeCompleted = job:Time_Completed
            jof:HeadAccountNumber = wob:HeadAccountNumber
            jof:IMEINumber = job:ESN
            Access:JOBSOBF.TryUpdate()
        END ! IF

		! Escalate engineer
        Access:JOBSENG.ClearKey(joe:UserCodeKey)
        joe:JobNumber = pJobNumber
        joe:UserCode = job:Engineer
        joe:DateAllocated = TODAY()
        SET(joe:UserCodeKey,joe:UserCodeKey)
        LOOP UNTIL Access:JOBSENG.Previous() <> Level:Benign
            IF (joe:JobNumber <> pJobNumber OR |
                joe:UserCode <> job:Engineer OR |
                joe:DateAllocated > TODAY())
                BREAK
            END ! IF
			
            joe:Status = 'COMPLETED'
            joe:StatusDate = TODAY()
            joe:StatusTime = CLOCK()
            Access:JOBSENG.TryUpdate()
            BREAK
        END ! LOOP

		! Mark exchange unit as available
        IF (job:Exchange_Unit_Number > 0)
            Access:EXCHANGE_ALIAS.ClearKey(xch_ali:Ref_Number_Key)
            xch_ali:Ref_Number = job:Exchange_Unit_Number
            IF (Access:EXCHANGE_ALIAS.TryFetch(xch_ali:Ref_Number_Key) = Level:Benign)
                Access:EXCHANGE.ClearKey(xch:ESN_Available_Key)
                xch:Available = 'REP'
                xch:Stock_Type = xch_ali:Stock_Type
                xch:ESN = job:ESN
                IF (Access:EXCHANGE.TryFetch(xch:ESN_Available_Key) = Level:Benign)
                    trace('QAProcedure: Exchange Unit Available')
                    
                    xch:Available = 'RTS'
                    xch:StatusChangeDate = TODAY()
                    IF (Access:EXCHANGE.TryUpdate() = Level:Benign)
                        IF (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                            exh:Ref_Number = xch:Ref_Number
                            exh:Date = TODAY()
                            exh:Time = CLOCK()
                            exh:User = p_web.GSV('BookingUserCode')
                            exh:Status = 'REPAIR COMPLETED'
                            IF (Access:EXCHHIST.TryInsert())
                                Access:EXCHHIST.CancelAutoInc()
                            END ! IF
                        END ! IF
                    END ! IF
                ELSE ! IF
                END ! IF
            ELSE ! IF
            END ! IF
        END ! IF

		!BER SMS
        p_web.SSV('BER_SMS_Send','N')

        IF (job:Warranty_Job = 'YES')
            Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
            rtd:Manufacturer = job:Manufacturer
            rtd:Warranty = 'YES'
            rtd:Repair_Type = job:Repair_Type_Warranty
            IF (Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign)
                IF rtd:BER = 1 or rtd:BER = 4 then      !BER/Liquid Damage
                    p_web.SSV('BER_SMS_Send',rtd:SMSSendType)
					!will be used later in  SendSMSText('J','Y',BER_SMS_Send) 
                END !if BER etc		
            ELSE ! IF
            END ! IF
        END ! IF
        IF (job:Chargeable_Job = 'YES')
            Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
            rtd:Manufacturer = job:Manufacturer
            rtd:Chargeable = 'YES'
            rtd:Repair_Type = job:Repair_Type
            IF (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
                IF rtd:BER = 1 OR rtd:BER = 4 then      !BER/Liquid Damage
                    IF p_web.GSV('BER_SMS_Send') = 'N' or p_web.GSV('BER_SMS_Send') = '' then   !only change it if it has not been set, or been set blank
                        p_web.SSV('BER_SMS_Send',rtd:SMSSendType)
                    ENd
					!will be used later in  SendSMSText('J','Y',BER_SMS_Send)    !beyond economic repair
                END !if BER etc	
            ELSE ! IF
            END ! IF
        END ! IF

		!TB012477 - SMS notification for BER/Liquid Damage  dealt with here  JC 13/04/12
        IF p_web.gsv('BER_SMS_Send') <> 'N' and job:Who_Booked <> 'WEB' and jobe2:SMSNotification then
            trace('QAProcedure: SendSMSText')
            SendSMSTEXT('J','Y',p_web.gsv('BER_SMS_Send'),P_web)
        END !Ifthe sms for ber or liquid damage needs to be sent    

        IF (VodacomClass.JobSentToHub(pJobNumber))
            Access:JOBACC.ClearKey(jac:Ref_Number_Key)
            jac:Ref_Number = pJobNumber
            SET(jac:Ref_Number_Key,jac:Ref_Number_Key)
            LOOP UNTIL Access:JOBACC.Next() <> Level:Benign
                IF (jac:Ref_Number <> pJobNumber)
                    BREAK
                END ! IF
                IF (jac:Attached = 0)
                    loc:Alert = 'Note: The selected job has accessories that have been retained by the RRC.' 
                    BREAK
                END ! IF
            END ! LOOP
        ELSE ! IF (VodacomClass.JobSentToHub(pJobNumber)
        END ! IF (VodacomClass.JobSentToHub(pJobNumber)
UpdateJob		PROCEDURE() ! Reload Job And Perform Pass/Fail
locPaid	LONG
    CODE
        LOOP 1 TIMES
            Access:JOBS.TryFetch(job:Ref_Number_Key)
            job:Ref_Number = p_web.GSV('job:Ref_Number')
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
                loc:alert = 'ERROR: Unable to find the job details'
                BREAK
            END ! IF
        
            IF (JobInUse(job:Ref_Number))
                loc:Alert = 'ERROR: Unable to update job. It is in use by another station.'
                ! #13610 Don't stop if job update fails (DBH: 20/10/2015)
                BREAK
            END ! IF
        
            trace('QAProcedure: FailReason = ' & p_web.GSV('FailReason'))
            trace('QAProcedure: locHandsetType = ' & p_web.GSV('locHandsetType'))

            CASE p_web.GSV('FailReason')
            OF 'PASSED'
		
                CASE p_web.GSV('locHandsetType')
                OF 'Repair' OROF 'Third Party Repair'
                    IF (PassRepair()) THEN RETURN.
                OF 'Loan'		
                    IF PassExchangeLoan('LOA') THEN RETURN.
                OF 'Exchange'
                    IF PassExchangeLoan('EXC') THEN RETURN.
                OF '2nd Exchange'
                    IF PassExchangeLoan('2NE') THEN RETURN.
                END ! CASE
            
!            IF (Access:SBO_GenericFile.PrimeRecord() = Level:Benign)
!                sbogen:SessionID = p_web.SessionID
!                sbogen:Long1 = p_web.GSV('job:Ref_Number')
!                sbogen:String1 = 'PASSED'
!                IF (Access:SBO_GenericFile.TryInsert())
!                    Access:SBO_GenericFile.CancelAutoInc()
!                END ! IF
!            END ! IF
			
                p_web.SSV('QAJobsUpdated',p_web.GSV('QAJobsUpdated') & '<br/><span class="GreenBold LargeText">' & | 
                    p_web.GSV('job:Ref_Number') & ' -  PASSED</span>')
			
            ELSE
                CreateScript(p_web,packet,'pleaseWait("Updating Job Details...")')
                DO SendPacket
            
                CASE p_web.GSV('locHandsetType')
                OF 'Repair' OROF 'Third Party Repair'
                    IF FailRepairExchangeLoan('JOB') THEN RETURN.
                OF 'Loan'				
                    IF FailRepairExchangeLoan('LOA') THEN RETURN.
                OF 'Exchange'
                    IF FailRepairExchangeLoan('EXC') THEN RETURN.
                OF '2nd Exchange'	
                    IF FailRepairExchangeLoan('2NE') THEN RETURN.			
                END ! CASE	
            
!            IF (Access:SBO_GenericFile.PrimeRecord() = Level:Benign)
!                sbogen:SessionID = p_web.SessionID
!                sbogen:Long1 = p_web.GSV('job:Ref_Number')
!                sbogen:String1 = 'FAILED'
!                IF (Access:SBO_GenericFile.TryInsert())
!                    Access:SBO_GenericFile.CancelAutoInc()
!                END ! IF
!            END ! IF
            
                p_web.SSV('QAJobsUpdated',p_web.GSV('QAJobsUpdated') & '<br/><span class="RedBold LargeText">' & | 
                    p_web.GSV('job:Ref_Number') & ' -  FAILED</span>')
            END ! CASE
        END ! BREAK LOOP
        
        !Redirect back to this URL to prevent idiots pressing refresh
        IF (loc:alert <> '')
            ! #13487 Workaround for IE that doesn't display the "loc:alert" alert (DBH: 08/04/2015)
            CreateScript(p_web,packet,'alert("' & CLIP(loc:alert) & '")')
            loc:alert = ''
        END ! IF
        
        CreateScript(p_web,packet,'window.open("QAProcedure?clear=1","_self")')
        DO SendPacket
trace	PROCEDURE(STRING pText)
outString	CSTRING(1000)
    CODE
        RETURN ! Logging Disabled
