

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER264.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ReleasedForDespatch  PROCEDURE  (LONG pJobNumber)          ! Declare Procedure
foundEntry                  LONG(0)
FilesOpened     BYTE(0)

  CODE
    DO OpenFiles
        
    Access:AUDIT.ClearKey(aud:Action_Key)
    aud:Ref_Number = pJobNumber
    aud:Action  = 'OUT OF REGION: RELEASED FOR DESPATCH'
    SET(aud:Action_Key,aud:Action_Key)
    LOOP UNTIL Access:AUDIT.Next() <> Level:Benign
        IF (aud:Ref_Number <> pJobNumber OR |
            aud:Action <> 'OUT OF REGION: RELEASED FOR DESPATCH')
            BREAK
        END ! IF
        foundEntry = 1
        BREAK
    END ! LOOP
    DO CloseFiles
        
    RETURN foundEntry
        
!--------------------------------------
OpenFiles  ROUTINE
  Access:AUDIT.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:AUDIT.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:AUDIT.Close
     FilesOpened = False
  END
