

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER160.INC'),ONCE        !Local module procedure declarations
                     END


BrowseOutFaultsEstimateParts PROCEDURE  (NetWebServerWorker p_web)
Local                CLASS
EstimateCode         Procedure(Long func:FieldNumber,String func:Field)
                     END
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(SBO_OutFaultParts)
                      Project(sofp:sessionID)
                      Project(sofp:partType)
                      Project(sofp:fault)
                      Project(sofp:fault)
                      Project(sofp:description)
                      Project(sofp:level)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
MANFAULO::State  USHORT
PARTS::State  USHORT
  CODE
    Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
    epr:Ref_Number  = p_web.GSV('wob:RefNumber')
    Set(epr:Part_Number_Key,epr:Part_Number_Key)
    Loop
        If Access:ESTPARTS.NEXT()
           Break
        End !If
        If epr:Ref_Number  <> p_web.GSV('wob:RefNumber')      |
            Then Break.  ! End If
        If epr:Fault_Code1 <> ''
            Local.EstimateCode(1,epr:fault_Code1)
        End !If epr:Fault_Code1
        If epr:Fault_Code2 <> ''
            Local.EstimateCode(2,epr:fault_Code2)
        End !If epr:Fault_Code1
        If epr:Fault_Code3 <> ''
            Local.EstimateCode(3,epr:fault_Code3)
        End !If epr:Fault_Code1
        If epr:Fault_Code4 <> ''
            Local.EstimateCode(4,epr:fault_Code4)
        End !If epr:Fault_Code1
        If epr:Fault_Code5 <> ''
            Local.EstimateCode(5,epr:fault_Code5)
        End !If epr:Fault_Code1
        If epr:Fault_Code6 <> ''
            Local.EstimateCode(6,epr:fault_Code6)
        End !If epr:Fault_Code1
        If epr:Fault_Code7 <> ''
            Local.EstimateCode(7,epr:fault_Code7)
        End !If epr:Fault_Code1
        If epr:Fault_Code8 <> ''
            Local.EstimateCode(8,epr:fault_Code8)
        End !If epr:Fault_Code1
        If epr:Fault_Code9 <> ''
            Local.EstimateCode(9,epr:fault_Code9)
        End !If epr:Fault_Code1
        If epr:Fault_Code10 <> ''
            Local.EstimateCode(10,epr:fault_Code10)
        End !If epr:Fault_Code1
        If epr:Fault_Code11 <> ''
            Local.EstimateCode(11,epr:fault_Code11)
        End !If epr:Fault_Code1
        If epr:Fault_Code12 <> ''
            Local.EstimateCode(12,epr:fault_Code12)
        End !If epr:Fault_Code1
    End !Loop
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseOutFaultsEstimateParts')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseOutFaultsEstimateParts:NoForm')
      loc:NoForm = p_web.GetValue('BrowseOutFaultsEstimateParts:NoForm')
      loc:FormName = p_web.GetValue('BrowseOutFaultsEstimateParts:FormName')
    else
      loc:FormName = 'BrowseOutFaultsEstimateParts_frm'
    End
    p_web.SSV('BrowseOutFaultsEstimateParts:NoForm',loc:NoForm)
    p_web.SSV('BrowseOutFaultsEstimateParts:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseOutFaultsEstimateParts:NoForm')
    loc:FormName = p_web.GSV('BrowseOutFaultsEstimateParts:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseOutFaultsEstimateParts') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseOutFaultsEstimateParts')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(SBO_OutFaultParts,sofp:FaultKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'SOFP:FAULT') then p_web.SetValue('BrowseOutFaultsEstimateParts_sort','1')
    ElsIf (loc:vorder = 'SOFP:DESCRIPTION') then p_web.SetValue('BrowseOutFaultsEstimateParts_sort','2')
    ElsIf (loc:vorder = 'SOFP:LEVEL') then p_web.SetValue('BrowseOutFaultsEstimateParts_sort','3')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseOutFaultsEstimateParts:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseOutFaultsEstimateParts:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseOutFaultsEstimateParts:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseOutFaultsEstimateParts:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseOutFaultsEstimateParts:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  ! Save Window Name
  AddToLog('NetWebBrowse',p_web.RequestData.DataString,'BrowseOutFaultsEstimateParts',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseOutFaultsEstimateParts_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseOutFaultsEstimateParts_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sofp:fault)','-UPPER(sofp:fault)')
    Loc:LocateField = 'sofp:fault'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(sofp:description)','-UPPER(sofp:description)')
    Loc:LocateField = 'sofp:description'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'sofp:level','-sofp:level')
    Loc:LocateField = 'sofp:level'
  end
  if loc:vorder = ''
    loc:vorder = '+sofp:sessionID,+UPPER(sofp:partType),+UPPER(sofp:fault)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('sofp:fault')
    loc:SortHeader = p_web.Translate('Fault')
    p_web.SetSessionValue('BrowseOutFaultsEstimateParts_LocatorPic','@s30')
  Of upper('sofp:description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseOutFaultsEstimateParts_LocatorPic','@s60')
  Of upper('sofp:level')
    loc:SortHeader = p_web.Translate('Repair Index')
    p_web.SetSessionValue('BrowseOutFaultsEstimateParts_LocatorPic','@n8')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseOutFaultsEstimateParts:LookupFrom')
  End!Else
  loc:formaction = 'BrowseOutFaultsEstimateParts'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseOutFaultsEstimateParts:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseOutFaultsEstimateParts:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseOutFaultsEstimateParts:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="SBO_OutFaultParts"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="sofp:FaultKey"></input><13,10>'
  end
  If p_web.Translate('Estimate Parts') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Estimate Parts',0)&'</span>'&CRLF
  End
  If clip('Estimate Parts') <> ''
    !packet = clip(packet) & p_web.br !Bryan. Why is this here?
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsEstimateParts',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseOutFaultsEstimateParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseOutFaultsEstimateParts.locate(''Locator2BrowseOutFaultsEstimateParts'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseOutFaultsEstimateParts.cl(''BrowseOutFaultsEstimateParts'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseOutFaultsEstimateParts_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseOutFaultsEstimateParts_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseOutFaultsEstimateParts','Fault','Click here to sort by Fault',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Fault')&'">'&p_web.Translate('Fault')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseOutFaultsEstimateParts','Description','Click here to sort by Description',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Description')&'">'&p_web.Translate('Description')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseOutFaultsEstimateParts','Repair Index','Click here to sort by Repair Index',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Repair Index')&'">'&p_web.Translate('Repair Index')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('sofp:sessionid',lower(Thisview{prop:order}),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'sofp:sessionID'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:sessionID'),p_web.GetValue('sofp:sessionID'),p_web.GetSessionValue('sofp:sessionID'))
  If Instring('sofp:parttype',lower(Thisview{prop:order}),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'sofp:partType'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:partType'),p_web.GetValue('sofp:partType'),p_web.GetSessionValue('sofp:partType'))
  If Instring('sofp:fault',lower(Thisview{prop:order}),1,1) = 0 !and SBO_OutFaultParts{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'sofp:fault'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sofp:fault'),p_web.GetValue('sofp:fault'),p_web.GetSessionValue('sofp:fault'))
      loc:FilterWas = 'sofp:sessionID = ' & p_web.sessionID & ' and Upper(sofp:partType) = ''E'''
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsEstimateParts',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseOutFaultsEstimateParts_Filter')
    p_web.SetSessionValue('BrowseOutFaultsEstimateParts_FirstValue','')
    p_web.SetSessionValue('BrowseOutFaultsEstimateParts_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,SBO_OutFaultParts,sofp:FaultKey,loc:PageRows,'BrowseOutFaultsEstimateParts',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If SBO_OutFaultParts{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(SBO_OutFaultParts,loc:firstvalue)
              Reset(ThisView,SBO_OutFaultParts)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If SBO_OutFaultParts{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(SBO_OutFaultParts,loc:lastvalue)
            Reset(ThisView,SBO_OutFaultParts)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sofp:fault)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseOutFaultsEstimateParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseOutFaultsEstimateParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseOutFaultsEstimateParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseOutFaultsEstimateParts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseOutFaultsEstimateParts',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseOutFaultsEstimateParts_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseOutFaultsEstimateParts_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseOutFaultsEstimateParts',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseOutFaultsEstimateParts.locate(''Locator1BrowseOutFaultsEstimateParts'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseOutFaultsEstimateParts.cl(''BrowseOutFaultsEstimateParts'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseOutFaultsEstimateParts_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseOutFaultsEstimateParts_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseOutFaultsEstimateParts.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseOutFaultsEstimateParts.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseOutFaultsEstimateParts.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseOutFaultsEstimateParts.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = sofp:fault
    p_web._thisrow = p_web._nocolon('sofp:fault')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseOutFaultsEstimateParts:LookupField')) = sofp:fault and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((sofp:fault = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseOutFaultsEstimateParts.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If SBO_OutFaultParts{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(SBO_OutFaultParts)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If SBO_OutFaultParts{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(SBO_OutFaultParts)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','sofp:fault',clip(loc:field),,loc:checked,,,'onclick="BrowseOutFaultsEstimateParts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','sofp:fault',clip(loc:field),,'checked',,,'onclick="BrowseOutFaultsEstimateParts.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sofp:fault
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sofp:description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::sofp:level
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseOutFaultsEstimateParts.omv(this);" onMouseOut="BrowseOutFaultsEstimateParts.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseOutFaultsEstimateParts=new browseTable(''BrowseOutFaultsEstimateParts'','''&clip(loc:formname)&''','''&p_web._jsok('sofp:sessionID',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('sofp:sessionID')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseOutFaultsEstimateParts.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseOutFaultsEstimateParts.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseOutFaultsEstimateParts')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseOutFaultsEstimateParts')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseOutFaultsEstimateParts')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseOutFaultsEstimateParts')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(SBO_OutFaultParts)
  p_web._CloseFile(MANFAULO)
  p_web._CloseFile(PARTS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(SBO_OutFaultParts)
  Bind(sofp:Record)
  Clear(sofp:Record)
  NetWebSetSessionPics(p_web,SBO_OutFaultParts)
  p_web._OpenFile(MANFAULO)
  Bind(mfo:Record)
  NetWebSetSessionPics(p_web,MANFAULO)
  p_web._OpenFile(PARTS)
  Bind(par:Record)
  NetWebSetSessionPics(p_web,PARTS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('sofp:sessionID',loc:default)
    p_web.SetValue('sofp:partType',loc:default)
    p_web.SetValue('sofp:fault',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
      ! Save Window Name
      IF (loc:alert <> '')
          AddToLog('Alert',loc:alert,'BrowseOutFaultsEstimateParts',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
      END ! IF
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::sofp:fault   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sofp:fault_'&sofp:fault,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sofp:fault,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sofp:description   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sofp:description_'&sofp:fault,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sofp:description,'@s60')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::sofp:level   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('sofp:level_'&sofp:fault,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(sofp:level,'@n8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(MANFAULO)
  p_web._OpenFile(PARTS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MANFAULO)
  p_Web._CloseFile(PARTS)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = sofp:sessionID
  loc:default = sofp:partType
  loc:default = sofp:fault

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('sofp:sessionID',sofp:sessionID)
  p_web.SetSessionValue('sofp:partType',sofp:partType)
  p_web.SetSessionValue('sofp:fault',sofp:fault)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('sofp:sessionID',loc:default)
    p_web.SetSessionValue('sofp:partType',loc:default)
    p_web.SetSessionValue('sofp:fault',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('sofp:sessionID'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('sofp:sessionID'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
Local.EstimateCode      Procedure(Long func:FieldNumber,String func:Field)
Code
    !Is this fault code a "Main Fault"
    Access:MANFAUPA.ClearKey(map:Field_Number_Key)
    map:Manufacturer = p_web.GSV('job:manufacturer')
    map:Field_Number = func:FieldNumber
    If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        !Found
        If map:MainFault

            !Ok, get the details from the Job "Main Fault"
            Access:MANFAULT.ClearKey(maf:MainFaultKey)
            maf:Manufacturer = p_web.GSV('job:manufacturer')
            maf:MainFault    = 1
            If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Found
                Access:MANFAULO.ClearKey(mfo:Field_Key)
                mfo:Manufacturer = p_web.GSV('job:manufacturer')
                mfo:Field_Number = maf:Field_Number
                mfo:Field        = func:Field
                Set(mfo:Field_Key,mfo:Field_Key)
                Loop
                    If Access:MANFAULO.NEXT()
                       Break
                    End !If
                    If mfo:Manufacturer <> p_web.GSV('job:manufacturer')      |
                    Or mfo:Field_Number <> maf:Field_Number      |
                    Or mfo:Field        <> func:Field      |
                        Then Break.  ! End If
                    If mfo:RelatedPartCode <> 0
                        If mfo:RelatedPartCode <> func:FieldNumber
                            Cycle
                        End !If mfo:RelatedPartCode <> func:FieldNumber
                    !This 'END' was at the bottom which would
                    !mean only Ericsson faults would ever appear - 234694 (DBH: 25-07-2003)
                    End !If mfo:RelatedPartCode <> 0

                    if (mfo:NotAvailable)
                        ! #11655 Don't show "Not Available" Fault Codes (Bryan: 23/08/2010)
                        CYCLE
                    END
                    
                    
                    Access:SBO_OUTFAULTPARTS.Clearkey(sofp:faultKey)
                    sofp:sessionID    = p_web.sessionID
                    sofp:partType    = 'E'
                    sofp:fault    = func:field
                    if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                        ! Found
                    else ! if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                        ! Error
                        if (Access:SBO_OUTFAULTPARTS.PrimeRecord() = Level:Benign)
                            sofp:sessionID    = p_web.sessionID
                            sofp:partType    = 'E'
                            sofp:fault    = func:Field
                            sofp:description    = mfo:description
                            sofp:level    = mfo:importancelevel

                            if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                                ! Inserted
                            else ! if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                                ! Error
                            end ! if (Access:SBO_OUTFAULTPARTS.TryInsert() = Level:Benign)
                        end ! if (Access:SBO_OUTFAULTPARTS.PrimeRecord() = Level:Benign)
                    end ! if (Access:SBO_OUTFAULTPARTS.TryFetch(sofp:faultKey) = Level:Benign)
                    Break
                End !Loop
            Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        Else !If map:MainFault

        End !If map:MainFault
    Else!If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End            !If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
