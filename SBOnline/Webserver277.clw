

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER277.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
CanInvoiceBePrinted  PROCEDURE  (LONG pJobNumber)!,LONG    ! Declare Procedure
RetValue                    LONG(FALSE)
FilesOpened     BYTE(0)

  CODE
    !region Processed Code
        DO OpenFiles
        LOOP 1 TIMES
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = pJobNumber
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                IF (job:Chargeable_Job <> 'YES')
                    BREAK
                END ! IF
            END ! IF
        
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
            END ! IF
        
            IF (job:Bouncer = 'X')
                BREAK
            END ! IF
        
            IF (job:Date_Completed = '' AND job:Exchange_Unit_Number = 0)
                BREAK
            END !I F
        
            IF (job:Ignore_Chargeable_Charges = 'YES')
                IF (jobe:RRCCSubTotal = 0)
                    BREAK
                END ! IF
            END ! IF
            
            IF (job:Invoice_Number > 0)
                Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
                inv:Invoice_Number = job:Invoice_Number
                IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
                    IF (inv:Invoice_Type <> 'SIN')
                        BREAK
                    END ! IF
                END ! IF
            END ! IF
            
            RetValue = TRUE
        
        END ! LOOP
        
    DO CloseFiles

    RETURN RetValue
    !endregion
!--------------------------------------
OpenFiles  ROUTINE
  Access:INVOICE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:INVOICE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:INVOICE.Close
     Access:JOBSE.Close
     Access:JOBS.Close
     FilesOpened = False
  END
