

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER177.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER017.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER038.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER042.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER170.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER176.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER178.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER179.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER180.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER181.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER182.INC'),ONCE        !Req'd for module callout resolution
                     END


FormEstimateParts    PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:Location         STRING(30)                            !
tmp:ShelfLocation    STRING(30)                            !
tmp:SecondLocation   STRING(30)                            !
tmp:PurchaseCost     REAL                                  !
tmp:OutWarrantyCost  REAL                                  !
tmp:OutWarrantyMarkup REAL                                 !
tmp:ARCPart          BYTE                                  !
tmp:FaultCodesChecked BYTE                                 !
tmp:FaultCodes2      STRING(30)                            !
tmp:FaultCodes4      STRING(30)                            !
tmp:FaultCodes5      STRING(30)                            !
tmp:FaultCodes6      STRING(30)                            !
tmp:FaultCodes7      STRING(30)                            !
tmp:FaultCodes8      STRING(30)                            !
tmp:FaultCodes9      STRING(30)                            !
tmp:FaultCodes10     STRING(30)                            !
tmp:FaultCodes11     STRING(30)                            !
tmp:FaultCodes12     STRING(30)                            !
tmp:FaultCodes3      STRING(30)                            !
tmp:FaultCodes1      STRING(30)                            !
tmp:CreateOrder      BYTE                                  !
tmp:UnallocatePart   BYTE                                  !
locUserCode          STRING(3)                             !
locPartUsedOnRepair  BYTE                                  !
FilesOpened     Long
ESTPARTS::State  USHORT
STOMODEL::State  USHORT
STOCK::State  USHORT
SUPPLIER::State  USHORT
LOCATION::State  USHORT
CHARTYPE::State  USHORT
MANFAUPA::State  USHORT
MANFAULT::State  USHORT
MANFPALO::State  USHORT
DEFAULTS::State  USHORT
PARTS_ALIAS::State  USHORT
STOHIST::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
local       Class
AfterFaultCodeLookup Procedure(Long fNumber)
SetLookupButton      Procedure(Long fNumber)
            End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormEstimateParts')
  loc:formname = 'FormEstimateParts_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormEstimateParts',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormEstimateParts','')
    p_web._DivHeader('FormEstimateParts',clip('fdiv') & ' ' & clip('FormContent'))
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormEstimateParts',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEstimateParts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEstimateParts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormEstimateParts',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If loc:act = InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormEstimateParts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormEstimateParts',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormEstimateParts',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
buildFaultCodes    Routine
data
locFoundFault      Byte(0)
code
    ! Clear Variables
    if (p_web.GSV('FormEstimateParts:FirstTime') = 0)
        loop x# = 1 To 12
            p_web.SSV('Hide:PartFaultCode' & x#,1)
            p_web.SSV('Req:PartFaultCode' & x#,0)
            p_web.SSV('ReadOnly:PartFaultCode' & x#,0)
            p_web.SSV('Prompt:PartFaultCode' & x#,'Fault Code ' & x#)
            p_web.SSV('Picture:PartFaultCode' & x#,'@s30')
            p_web.SSV('ShowDate:PartFaultCode' & x#,0)
            p_web.SSV('Lookup:PartFaultCode' & x#,0)
            p_web.SSV('Comment:PartFaultCode' & x#,'')
        end ! loop x# = 1 To 12
        p_web.SSV('Hide:FaultCodesChecked',1)
    end ! if (p_web.GSV('FormEstimateParts:FirstTime') = 0)

    locMainFaultOnly# = 0
    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    map:Manufacturer    = p_web.GSV('job:Manufacturer')
    map:MainFault    = 1
    if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Found
        if (sto:Accessory <> 'YES' and man:ForceAccessoryCode)
            !Main Fault Only
            locMainFaultOnly# = 1
        end ! if (sto:Accessory <> 'YES' and man:ForceAccessoryCode)
    else ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Error
    end ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)

    Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
    map:Manufacturer    = p_web.GSV('job:Manufacturer')
    map:ScreenOrder    = 0
    set(map:ScreenOrderKey,map:ScreenOrderKey)
    loop
        if (Access:MANFAUPA.Next())
            Break
        end ! if (Access:MANFAUPA.Next())
        if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
            Break
        end ! if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))

        if (map:ScreenOrder = 0)
            cycle
        end ! if (map:ScreenOrder = 0)

        if (locMainFaultOnly# = 1)
            if (map:MainFault = 0)
                cycle
            end ! if (map:MainFault = 0)
        end ! if (locMainFaultOnly# = 1)

        p_web.SSV('Hide:PartFaultCode' & map:ScreenOrder,0)
        p_web.SSV('Prompt:PartFaultCode' & map:ScreenOrder,map:Field_Name)

        ! #11659 Don't force estimate fault codes (Bryan: 23/08/2010)
!        if (map:Compulsory = 'YES')
!            if (p_web.GSV('epr:Adjustment') = 'YES')
!                if (map:CompulsoryForAdjustment)
!                    p_web.SSV('Req:PartFaultCode' & map:ScreenOrder,1)
!                    p_web.SSV('Comment:PartFaultCode' & map:ScreenOrder,'Required')
!                end ! if (map:CompulsoryForAdjustment)
!            else !if (p_web.GSV('epr:Adjustment') = 'YES')
!                p_web.SSV('Req:PartFaultCode' & map:ScreenOrder,1)
!                p_web.SSV('Comment:PartFaultCode' & map:ScreenOrder,'Required')
!            end ! if (p_web.GSV('epr:Adjustment') = 'YES')
!        else ! if (map:Compulsory = 'YES')
!            p_web.SSV('Req:PartFaultCode' & map:ScreenOrder,0)
!        end ! if (map:Compulsory = 'YES')

        if (map:MainFault)
            !This is the main fault, use the job main fault
            Access:MANFAULT.Clearkey(maf:MainFaultKey)
            maf:Manufacturer    = p_web.GSV('job:Manufacturer')
            maf:MainFault    = 1
            if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                ! Found
                case maf:Field_Type
                of 'DATE'
                    p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,clip(maf:DateType))
                    ! Date Lookup Required
                of 'STRING'
                    if (maf:RestrictLength)
                        p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@s' & maf:LengthTo)
                    end !~ if (maf:RestrictLength)

                    ! Lookup Required
                of 'NUMBER'
                    if (maf:RestrictLength)
                        p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@n_' & maf:LengthTo)
                    else !end !~ if (maf:RestrictLength)
                        p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@n_9')
                    end !~ if (maf:RestrictLength)
                end ! case maf:Field_Type

                if (maf:Lookup = 'YES')
                    p_web.SSV('Lookup:PartFaultCode' & map:ScreenOrder,1)
                end ! if (map:Lookup = 'YES')
            else ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                ! Error
            end ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
        else ! if (map:MainFault)
            case map:Field_Type
            of 'DATE'
                p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,clip(map:DateType))
                p_web.SSV('ShowDate:PartFaultCode' & map:ScreenOrder,1)
                ! Date Lookup Required
            of 'STRING'
                if (map:RestrictLength)
                    p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@s' & map:LengthTo)
                end !~ if (maf:RestrictLength)

                ! Lookup Required
            of 'NUMBER'
                if (map:RestrictLength)
                    p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@n_' & map:LengthTo)
                else !end !~ if (maf:RestrictLength)
                    p_web.SSV('Picture:PartFaultCode' & map:ScreenOrder,'@n_9')
                end !~ if (maf:RestrictLength)
            end ! case maf:Field_Type
        end !if (map:MainFault)

        if (map:Lookup = 'YES')
            p_web.SSV('Lookup:PartFaultCode' & map:ScreenOrder,1)

        end ! if (map:Lookup = 'YES')

        if (map:NotAvailable = 1)
            if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '')
                p_web.SSV('Hide:PartFaultCode' & map:ScreenOrder,1)
            else ! if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '')
                p_web.SSV('ReadOnly:PartFaultCode' & map:ScreenOrder,1)
            end ! if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '')
        end ! if (map:NotAvailable = 1)

        if (map:CopyFromJobFaultCode And p_web.GSV('tmp:FaulCode' & map:ScreenOrder) = '')
            if (map:ScreenOrder < 13)
                p_web.SSV('tmp:FaultCode' & map:ScreenOrder,p_web.GSV('job:Fault_Code' & map:CopyJobFaultCode))
            else ! if (map:ScreenOrder < 13)
                p_web.SSV('tmp:FaultCode' & map:ScreenOrder,p_web.GSV('wob:FaultCode' & map:CopyJobFaultCode))
            end ! if (map:ScreenOrder < 13)
        end ! if (map:CopyFromJobFaultCode And p_web.GSV('tmp:FaulCode' & map:ScreenOrder) = '')

        if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '' And sto:Assign_Fault_Codes = 'YES')
            Access:STOMODEL.Clearkey(stm:Model_Number_Key)
            stm:Ref_Number    = sto:Ref_Number
            stm:Manufacturer    = p_web.GSV('job:Manufacturer')
            stm:Model_Number    = p_web.GSV('job:Model_Number')
            if (Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign)
                ! Found
                Case map:Field_Number
                Of 1
                    If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode1)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 2
                    If stm:FaultCode2 <> '' And stm:FaultCode2 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode2)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 3
                    If stm:FaultCode3 <> '' And stm:FaultCode3 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode3)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 4
                    If stm:FaultCode4 <> '' And stm:FaultCode4 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode4)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 5
                    If stm:FaultCode5 <> '' And stm:FaultCode5 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode5)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 6
                    If stm:FaultCode6 <> '' And stm:FaultCode6 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode6)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 7
                    If stm:FaultCode7 <> '' And stm:FaultCode7 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode7)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 8
                    If stm:FaultCode8 <> '' And stm:FaultCode8 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode8)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 9
                    If stm:FaultCode9 <> '' And stm:FaultCode9 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode9)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 10
                    If stm:FaultCode10 <> '' And stm:FaultCode10 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode10)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 11
                    If stm:FaultCode11 <> '' And stm:FaultCode11 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode11)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                Of 12
                    If stm:FaultCode12 <> '' And stm:FaultCode12 <> '** MULTIPLE VALUES **'
                        p_web.SSV('tmp:FaultCode' & map:ScreenOrder,stm:FaultCode12)
                    End ! If stm:FaultCode1 <> '' And stm:FaultCode1 <> '** MULTIPLE VALUES **'
                End ! Case map:Field_Number
            else ! if (Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign)
        end ! if (p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '' And sto:Assign_Fault_Codes = 'YES')
        locFoundFault = 1
    end ! loop

    !Check if the part main fault is set to assign value to anouther fault code

    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    map:Manufacturer    = p_web.GSV('job:Manufacturer')
    map:MainFault    = 1
    if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Found
        Access:MANFPALO.Clearkey(mfp:Field_Key)
        mfp:Manufacturer    = p_web.GSV('job:Manufacturer')
        mfp:Field_Number    = map:Field_Number
        mfp:Field    = p_web.GSV('tmp:FaultCode' & map:ScreenOrder)
        if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
            ! Found
            if (mfp:SetPartFaultCode)
                Access:MANFAUPA.Clearkey(map:Field_Number_Key)
                map:Manufacturer    = p_web.GSV('job:Manufacturer')
                map:Field_Number    = mfp:SelectPartFaultCode
                if (Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign)
                    ! Found
                    if (p_web.GSV('Hide:PartFaultCode' & map:ScreenOrder) <> 1 And |
                       p_web.GSV('tmp:FaultCode' & map:ScreenOrder) = '')
                       p_web.SSV('tmp:FaultCode' & map:ScreenOrder,mfp:PartFaultCodeValue)
                    end ! if (p_web.GSV('Hide:PartFaultCode' & map:ScreenOrder) <> 1 And |
                else ! if (Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign)
                    ! Error
                end ! if (Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign)
            end ! if (mfp:SetPartFaultCode)
        else ! if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
            ! Error
        end ! if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
    else ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Error
    end ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)

    if (locFoundFault)
        p_web.SSV('Hide:FaultCodesChecked',0)
    end ! if (locFoundFault)
deleteSessionValues     Routine
    p_web.deleteSessionValue('adjustment')
    p_web.deleteSessionValue('FormEstimateParts:FirstTime')
    p_web.deleteSessionValue('locOrderRequired')
    p_web.deleteSessionValue('locOutFaultCode')

    p_web.deleteSessionValue('ReadOnly:OutWarrantyMarkup')
    p_web.deleteSessionValue('ReadOnly:PurchaseCost')
    p_web.deleteSessionValue('ReadOnly:OutWarrantyCost')
    p_web.deleteSessionValue('ReadOnly:Quantity')

    p_web.deleteSessionValue('Parts:ViewOnly')

    p_web.deleteSessionValue('Show:UnallocatePart')
enableDisableCosts    Routine
    p_web.SSV('tmp:FixedPrice','')
    if (p_web.GSV('tmp:OutWarrantyMarkup') = 0)
        p_web.SSV('ReadOnly:OutWarrantyCost',0)
    else ! if (p_web.GSV('tmp:OutWarrantyMarkup') = 0)
        p_web.SSV('ReadOnly:OutWarrantyCost',1)
    end ! if (p_web.GSV('tmp:OutWarrantyMarkup') = 0)

    if (p_web.GSV('job:Invoice_Number') = 0)
        if (p_web.GSV('tmp:InWarrantyMarkup') > 0)
            p_web.SSV('tmp:InWarrantyCost',VodacomClass.Markup(p_web.GSV('tmp:InWarrantyCost'),|
                                                p_web.GSV('tmp:PurchaseCost'),|
                                                p_web.GSV('tmp:InWarrantyMarkup')))
        end ! if (p_web.GSV('tmp:InWarrantyMarkup') > 0)
        if (p_web.GSV('tmp:OutWarrantyMarkup') > 0)
            p_web.SSV('tmp:OutWarrantyCost',VodacomClass.Markup(p_web.GSV('tmp:OutWarrantyCost'),|
                                                p_web.GSV('tmp:PurchaseCost'),|
                                                p_web.GSV('tmp:OutWarrantyMarkup')))
        end ! if (p_web.GSV('tmp:OutWarrantyMarkup') > 0)
    end ! if (p_web.GSV('job:Invoice_Number') = 0)

    Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
    cha:Charge_Type    = p_web.GSV('job:Charge_Type')
    if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
        ! Found
        if (cha:Zero_Parts_ARC)
           if (p_web.GSV('tmp:ARCPart') = 1)
               p_web.SSV('tmp:FixedPrice','FIXED PRICE')
               p_web.SSV('ReadOnly:OutWarrantyMarkup',1)
               p_web.SSV('ReadOnly:OutWarrantyCost',1)
               p_web.SSV('tmp:OutWarrantyCost',0)
           end ! if (p_web.GSV('tmp:ARCPart'))
        end ! if (cha:Zero_Parts_ARC)

        if (cha:Zero_Parts = 'YES')
           if (p_web.GSV('tmp:ARCPart') = 0)
               p_web.SSV('tmp:FixedPrice','FIXED PRICE')
               p_web.SSV('ReadOnly:OutWarrantyMarkup',1)
               p_web.SSV('ReadOnly:OutWarrantyCost',1)
               p_web.SSV('tmp:OutWarrantyCost',0)
           end ! if (p_web.GSV('tmp:ARCPart'))
        end ! if (cha:Zero_Parts = 'YES')
    else ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
        ! Error
    end ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
lookupLocation    Routine
    !if (p_web.GSV('epr:Part_Ref_Number') <> '')
    IF (p_web.GSV('epr:Part_Ref_Number') > 0)
        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number    = p_web.GSV('epr:Part_Ref_Number')
        if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
            ! Found
            p_web.SSV('tmp:Location',sto:Location)
            p_web.SSV('tmp:ShelfLocation',sto:Shelf_Location)
            p_web.SSV('tmp:SecondLocation',sto:Second_Location)
        else ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
            ! Error
            p_web.SSV('tmp:Location','')
            p_web.SSV('tmp:ShelfLocation','')
            p_web.SSV('tmp:SecondLocation','')
        end ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
    else ! if (p_web.GSV('epr:Part_Ref_Number') <> '')
        p_web.SSV('tmp:Location','')
        p_web.SSV('tmp:ShelfLocation','')
        p_web.SSV('tmp:SecondLocation','')
    end ! if (p_web.GSV('epr:Part_Ref_Number') <> '')
lookupMainFault  Routine
    p_web.SSV('locOutFaultCode','')
    Access:MANFAUPA.Clearkey(map:MainFaultKey)
    map:Manufacturer    = p_web.GSV('job:Manufacturer')
    map:MainFault    = 1
    if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Found
        Access:MANFAULT.Clearkey(maf:MainFaultKey)
        maf:Manufacturer    = p_web.GSV('job:Manufacturer')
        maf:MainFault    = 1
        if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
            ! Found

            Access:MANFAULO.Clearkey(mfo:Field_Key)
            mfo:Manufacturer    = p_web.GSV('job:Manufacturer')
            mfo:Field_Number    = maf:Field_Number
            mfo:Field    = p_web.GSV('tmp:FaultCodes' & map:ScreenOrder)
            if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                ! Found
                p_web.SSV('locOutFaultCode','Out Fault Code: ' & mfo:Description)
            else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                ! Error
            end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)

        else ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
            ! Error
        end ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)

    else ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
        ! Error
    end ! if (Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign)
showCosts      Routine
    Access:STOCK.Clearkey(sto:Ref_Number_Key)
    sto:Ref_Number    = p_web.GSV('epr:Part_Ref_Number')
    if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
        ! Found
        if (sto:Location <> p_web.GSV('ARC:SiteLocation'))
            p_web.SSV('tmp:ARCPart',0)
            if (p_web.GSV('BookingSite') <> 'RRC')
                if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'AMEND RRC PART'))
                    p_web.SSV('Parts:ViewOnly',1)
                end ! if (SecurityCheckFailed(p_web.GSV('BookingUser'),'AMEND RRC PART'))
            end ! if (p_web.GSV('BookingSite') <> 'RRC')
        else ! if (sto:Location <> p_web.GSV('Default:SiteLocation'))
            p_web.SSV('tmp:ARCPart',1)
            if (p_web.GSV('BookingSite') <> 'ARC')
                p_web.SSV('Parts:ViewOnly',1)
            end ! if (p_web.GSV('BookingSite') <> 'ARC')
        end ! if (sto:Location <> p_web.GSV('Default:SiteLocation'))
    else ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
    if (p_web.GSV('tmp:ARCPart') = 1)
        p_web.SSV('tmp:PurchaseCost',p_web.GSV('epr:AveragePurchaseCost'))
        p_web.SSV('tmp:InWarrantyCost',p_web.GSV('epr:Purchase_Cost'))
        p_web.SSV('tmp:OutWarrantyCost',p_web.GSV('epr:Sale_Cost'))
        p_web.SSV('tmp:InWarrantyMarkup',p_web.GSV('epr:InWarrantyMarkup'))
        p_web.SSV('tmp:OutWarrantyMarkup',p_web.GSV('epr:OutWarrantyMarkup'))
    else ! if (tmp:ARCPart)
        if (p_web.GSV('epr:RRCAveragePurchaseCost') > 0)
            p_web.SSV('tmp:PurchaseCost',p_web.GSV('epr:RRCAveragePurchaseCost'))
        else ! if (p_web.GSV('epr:RRCAveragePurchaseCost') > 0)
            p_web.SSV('tmp:PurchaseCost',p_web.GSV('epr:RRCPurchaseCost'))
        end ! if (p_web.GSV('epr:RRCAveragePurchaseCost') > 0)

        p_web.SSV('tmp:InWarrantyCost',p_web.GSV('epr:RRCPurchaseCost'))
        p_web.SSV('tmp:OutWarrantyCost',p_web.GSV('epr:RRCSaleCost'))
        p_web.SSV('tmp:InWarrantyMarkup',p_web.GSV('epr:RRCInWarrantyMarkup'))
        p_web.SSV('tmp:OutWarrantyMarkup',p_web.GSV('epr:RRCOutWarrantyMarkup'))
    end !if (tmp:ARCPart)
UpdateComments    Routine
    loop x# = 1 to 12
        if (p_web.GSV('Hide:PartFaultCode' & x#) = 1)
            cycle
        end ! if (p_web.GSV('Hide:PartFaultCode' & x#) = 1)
        Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
        map:Manufacturer    = p_web.GSV('job:Manufacturer')
        map:ScreenOrder    = x#
        if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
            ! Found
            if (map:MainFault)
                Access:MANFAULT.Clearkey(maf:MainFaultKey)
                maf:Manufacturer    = p_web.GSV('job:Manufacturer')
                maf:MainFault    = 1
                if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                    ! Found
                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                    mfo:Manufacturer    = p_web.GSV('job:Manufacturer')
                    mfo:Field_Number    = maf:Field_Number
                    mfo:Field    = p_web.GSV('tmp:FaultCodes' & x#)
                    if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Found
                        p_web.SSV('Comment:PartFaultCode' & x#,mfo:Description)
                    else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Error
                        p_web.SSV('Comment:PartFaultCode' & x#,'')
                    end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                else ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                    ! Error
                end ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
            else ! if (map:MainFault)
                Access:MANFPALO.Clearkey(mfp:Field_Key)
                mfp:Manufacturer    = p_web.GSV('job:Manufacturer')
                mfp:Field_Number    = map:Field_Number
                mfp:Field    = p_web.GSV('tmp:FaultCodes' & x#)
                if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
                    ! Found
                    p_web.SSV('Comment:PartFaultCode' & x#,mfp:Description)
                else ! if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
                    ! Error
                    p_web.SSV('Comment:PartFaultCode' & x#,'')
                end ! if (Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign)
            end ! if (map:MainFault)
        else ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
            ! Error
        end ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
    end ! loop x# = 1 to 12


updatePartDetails      routine
! Update Part Details
    Access:STOCK.Clearkey(sto:Ref_Number_Key)
    sto:Ref_Number    = stm:Ref_Number
    if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
        ! Found
    else ! if (Access:STOCK.TryFetch(sto:Ref_Number) = Level:Benign)
        ! Error
    end ! if (Access:STOCK.TryFetch(sto:Ref_Number) = Level:Benign)

    Access:LOCATION.Clearkey(loc:Location_Key)
    loc:Location    = sto:Location
    if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
        ! Found
    else ! if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
        ! Error
    end ! if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)

    p_web.SSV('epr:Description',stm:Description)
    p_web.SSV('epr:Part_Ref_Number',stm:Ref_Number)
    p_web.SSV('epr:Supplier',sto:Supplier)
    p_web.SSV('epr:Purchase_Cost',sto:Purchase_Cost)
    p_web.SSV('epr:Sale_Cost',sto:Sale_Cost)
    p_web.SSV('epr:Retail_Cost',sto:Retail_Cost)
    p_web.SSV('epr:InWarrantyMarkup',sto:PurchaseMarkup)
    p_web.SSV('epr:OutWarrantyMarkup',sto:Percentage_Mark_Up)

    if (p_web.GSV('BookingSite') = 'RRC')
        p_web.SSV('epr:RRCAveragePurchaseCost',sto:AveragePurchaseCost)
        p_web.SSV('epr:PurchaseCost',sto:Purchase_Cost)
        p_web.SSV('epr:RRCSaleCost',sto:Sale_Cost)

        Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
        cha:Charge_Type    = p_web.GSV('job:Charge_Type')
        if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Found
            if (cha:Zero_Parts = 'YES')
                p_web.SSV('epr:RRCSaleCost',0)
            end ! if (cha:Zero_Parts = 'YES')
        else ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Error
        end ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)

        p_web.SSV('epr:RRCInWarrantyMarkup',sto:PurchaseMarkUp)
        p_web.SSV('epr:RRCOutWarrantyMarkup',sto:Percentage_Mark_Up)
        p_web.SSV('epr:Purchase_Cost',epr:RRCPurchaseCost)
        p_web.SSV('epr:Sale_Cost',epr:RRCSaleCost)
        p_web.SSV('epr:AveragePurchaseCost',epr:RRCAveragePurchaseCost)
    end ! if (p_web.GSV('BookingSite') = 'RRC')

    if (p_web.GSV('BookingSite') = 'ARC')
        p_web.SSV('epr:AveragePurchaseCost',sto:AveragePurchaseCost)
        p_web.SSV('epr:Purchase_Cost',sto:Purchase_Cost)
        p_web.SSV('epr:Sale_Code',sto:Sale_Cost)

        Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
        cha:Charge_Type    = p_web.GSV('job:Charge_Type')
        if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Found
            if (cha:Zero_Parts_ARC)
                p_web.SSV('epr:Sale_Cost',0)
            end ! if (cha:Zero_Parts = 'YES')
        else ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
            ! Error
        end ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)

        p_web.SSV('epr:RRCPurchaseCost',0)
        p_web.SSV('epr:RRCSaleCost',0)

        if (p_web.GSV('jobe:WebJob') = 1)
            p_web.SSV('epr:RRCAveragePurchaseCost',epr:Sale_Cost)
            p_web.SSV('epr:RRCPurchaseCost',VodacomClass.Markup(p_web.GSV('epr:RRCPurchaseCost'),|
                                                p_web.GSV('epr:RRCAveragePurchaseCost'),|
                                                InWarrantyMarkup(p_web.GSV('job:Manufacturer'),|
                                                sto:Location)))
            p_web.SSV('epr:RRCSaleCost',VodacomClass.Markup(p_web.GSV('epr:RRCSaleCost'),|
                                            p_web.GSV('epr:RRCAveragePurchaseCost'),|
                                            loc:OutWarrantyMarkup))
            p_web.SSV('epr:RRCInWarrantyMarkup',p_web.GSV('epr:InWarrantyMarkup'))
            p_web.SSV('epr:RRCOutWarrantyMarkup',p_web.GSV('epr:OutWarrantyMarkup'))
        end ! if (p_web.GSV('jobe:WebJob') = 1)
    end ! if (p_web.GSV('BookingSite') = 'ARC')

    if (sto:Assign_Fault_Codes = 'YES')
        p_web.SSV('tmp:FaultCode1',stm:FaultCode1)
        p_web.SSV('tmp:FaultCode2',stm:FaultCode2)
        p_web.SSV('tmp:FaultCode3',stm:FaultCode3)
        p_web.SSV('tmp:FaultCode4',stm:FaultCode4)
        p_web.SSV('tmp:FaultCode5',stm:FaultCode5)
        p_web.SSV('tmp:FaultCode6',stm:FaultCode6)
        p_web.SSV('tmp:FaultCode7',stm:FaultCode7)
        p_web.SSV('tmp:FaultCode8',stm:FaultCode8)
        p_web.SSV('tmp:FaultCode9',stm:FaultCode9)
        p_web.SSV('tmp:FaultCode10',stm:FaultCode10)
        p_web.SSV('tmp:FaultCode11',stm:FaultCode11)
        p_web.SSV('tmp:FaultCode12',stm:FaultCode12)
    end ! if (sto:Assign_Fault_Codes = 'YES')

    p_web.SSV('epr:Part_Ref_Number',sto:Ref_Number)
    p_web.SSV('locOrderRequired',0)

    do ShowCosts
    do lookupLocation
    do enableDisableCosts
OpenFiles  ROUTINE
  p_web._OpenFile(ESTPARTS)
  p_web._OpenFile(STOMODEL)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(SUPPLIER)
  p_web._OpenFile(LOCATION)
  p_web._OpenFile(CHARTYPE)
  p_web._OpenFile(MANFAUPA)
  p_web._OpenFile(MANFAULT)
  p_web._OpenFile(MANFPALO)
  p_web._OpenFile(DEFAULTS)
  p_web._OpenFile(PARTS_ALIAS)
  p_web._OpenFile(STOHIST)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(ESTPARTS)
  p_Web._CloseFile(STOMODEL)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(SUPPLIER)
  p_Web._CloseFile(LOCATION)
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(MANFAUPA)
  p_Web._CloseFile(MANFAULT)
  p_Web._CloseFile(MANFPALO)
  p_Web._CloseFile(DEFAULTS)
  p_Web._CloseFile(PARTS_ALIAS)
  p_Web._CloseFile(STOHIST)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
      !Initialize
      do buildFaultCodes
  
      p_web.SSV('Comment:OutWarrantyMarkup','')
      p_web.SSV('Comment:PartNumber','Required')
  p_web.SetValue('FormEstimateParts_form:inited_',1)
  p_web.SetValue('UpdateFile','ESTPARTS')
  p_web.SetValue('UpdateKey','epr:record_number_key')
  p_web.SetValue('IDField','epr:Record_Number')
  do RestoreMem

CancelForm  Routine
  IF p_web.GetSessionValue('FormEstimateParts:Primed') = 1
    p_web._deleteFile(ESTPARTS)
    p_web.SetSessionValue('FormEstimateParts:Primed',0)
  End
      do deleteSessionValues
  

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','ESTPARTS')
  p_web.SetValue('UpdateKey','epr:record_number_key')
  If p_web.IfExistsValue('epr:Order_Number')
    p_web.SetPicture('epr:Order_Number','@n08b')
  End
  p_web.SetSessionPicture('epr:Order_Number','@n08b')
  If p_web.IfExistsValue('epr:Date_Ordered')
    p_web.SetPicture('epr:Date_Ordered','@d6b')
  End
  p_web.SetSessionPicture('epr:Date_Ordered','@d6b')
  If p_web.IfExistsValue('epr:Date_Received')
    p_web.SetPicture('epr:Date_Received','@d6b')
  End
  p_web.SetSessionPicture('epr:Date_Received','@d6b')
  If p_web.IfExistsValue('epr:Part_Number')
    p_web.SetPicture('epr:Part_Number','@s30')
  End
  p_web.SetSessionPicture('epr:Part_Number','@s30')
  If p_web.IfExistsValue('epr:Description')
    p_web.SetPicture('epr:Description','@s30')
  End
  p_web.SetSessionPicture('epr:Description','@s30')
  If p_web.IfExistsValue('epr:Despatch_Note_Number')
    p_web.SetPicture('epr:Despatch_Note_Number','@s30')
  End
  p_web.SetSessionPicture('epr:Despatch_Note_Number','@s30')
  If p_web.IfExistsValue('epr:Quantity')
    p_web.SetPicture('epr:Quantity','@n4')
  End
  p_web.SetSessionPicture('epr:Quantity','@n4')
  If p_web.IfExistsValue('tmp:PurchaseCost')
    p_web.SetPicture('tmp:PurchaseCost','@n_14.2')
  End
  p_web.SetSessionPicture('tmp:PurchaseCost','@n_14.2')
  If p_web.IfExistsValue('tmp:OutWarrantyCost')
    p_web.SetPicture('tmp:OutWarrantyCost','@n_14.2')
  End
  p_web.SetSessionPicture('tmp:OutWarrantyCost','@n_14.2')
  If p_web.IfExistsValue('tmp:OutWarrantyMarkup')
    p_web.SetPicture('tmp:OutWarrantyMarkup','@n3')
  End
  p_web.SetSessionPicture('tmp:OutWarrantyMarkup','@n3')
  If p_web.IfExistsValue('epr:Supplier')
    p_web.SetPicture('epr:Supplier','@s30')
  End
  p_web.SetSessionPicture('epr:Supplier','@s30')
  If p_web.IfExistsValue('tmp:FaultCodes1')
    p_web.SetPicture('tmp:FaultCodes1',p_web.GSV('Picture:PartFaultCode1'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes1',p_web.GSV('Picture:PartFaultCode1'))
  If p_web.IfExistsValue('tmp:FaultCodes2')
    p_web.SetPicture('tmp:FaultCodes2',p_web.GSV('Picture:PartFaultCode2'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes2',p_web.GSV('Picture:PartFaultCode2'))
  If p_web.IfExistsValue('tmp:FaultCodes3')
    p_web.SetPicture('tmp:FaultCodes3',p_web.GSV('Picture:PartFaultCode3'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes3',p_web.GSV('Picture:PartFaultCode3'))
  If p_web.IfExistsValue('tmp:FaultCodes4')
    p_web.SetPicture('tmp:FaultCodes4',p_web.GSV('Picture:PartFaultCode4'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes4',p_web.GSV('Picture:PartFaultCode4'))
  If p_web.IfExistsValue('tmp:FaultCodes5')
    p_web.SetPicture('tmp:FaultCodes5',p_web.GSV('Picture:PartFaultCode5'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes5',p_web.GSV('Picture:PartFaultCode5'))
  If p_web.IfExistsValue('tmp:FaultCodes6')
    p_web.SetPicture('tmp:FaultCodes6',p_web.GSV('Picture:PartFaultCode6'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes6',p_web.GSV('Picture:PartFaultCode6'))
  If p_web.IfExistsValue('tmp:FaultCodes7')
    p_web.SetPicture('tmp:FaultCodes7',p_web.GSV('Picture:PartFaultCode7'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes7',p_web.GSV('Picture:PartFaultCode7'))
  If p_web.IfExistsValue('tmp:FaultCodes8')
    p_web.SetPicture('tmp:FaultCodes8',p_web.GSV('Picture:PartFaultCode8'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes8',p_web.GSV('Picture:PartFaultCode8'))
  If p_web.IfExistsValue('tmp:FaultCodes9')
    p_web.SetPicture('tmp:FaultCodes9',p_web.GSV('Picture:PartFaultCode9'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes9',p_web.GSV('Picture:PartFaultCode9'))
  If p_web.IfExistsValue('tmp:FaultCodes10')
    p_web.SetPicture('tmp:FaultCodes10',p_web.GSV('Picture:PartFaultCode10'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes10',p_web.GSV('Picture:PartFaultCode10'))
  If p_web.IfExistsValue('tmp:FaultCodes11')
    p_web.SetPicture('tmp:FaultCodes11',p_web.GSV('Picture:PartFaultCode11'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes11',p_web.GSV('Picture:PartFaultCode11'))
  If p_web.IfExistsValue('tmp:FaultCodes12')
    p_web.SetPicture('tmp:FaultCodes12',p_web.GSV('Picture:PartFaultCode12'))
  End
  p_web.SetSessionPicture('tmp:FaultCodes12',p_web.GSV('Picture:PartFaultCode12'))
AfterLookup Routine
  loc:TabNumber = -1
  If loc:act = ChangeRecord
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'epr:Part_Number'
    p_web.setsessionvalue('showtab_FormEstimateParts',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(STOMODEL)
      ! After Lookup
      ! After Lookup Assignments
      do updatePartDetails
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.epr:Description')
  of 'tmp:FaultCodes1'
      local.afterFaultCodeLookup(1)
  of 'tmp:FaultCodes2'
      local.afterFaultCodeLookup(2)
  of 'tmp:FaultCodes3'
      local.afterFaultCodeLookup(3)
  of 'tmp:FaultCodes4'
      local.afterFaultCodeLookup(4)
  of 'tmp:FaultCodes5'
      local.afterFaultCodeLookup(5)
  of 'tmp:FaultCodes6'
      local.afterFaultCodeLookup(6)
  of 'tmp:FaultCodes7'
      local.afterFaultCodeLookup(7)
  of 'tmp:FaultCodes8'
      local.afterFaultCodeLookup(8)
  of 'tmp:FaultCodes9'
      local.afterFaultCodeLookup(9)
  of 'tmp:FaultCodes10'
      local.afterFaultCodeLookup(10)
  of 'tmp:FaultCodes11'
      local.afterFaultCodeLookup(11)
  of 'tmp:FaultCodes12'
      local.afterFaultCodeLookup(12)
  
  
  Of 'epr:Supplier'
    p_web.setsessionvalue('showtab_FormEstimateParts',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUPPLIER)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.epr:Exclude_From_Order')
  of 'tmp:FaultCodes1'
      local.afterFaultCodeLookup(1)
  of 'tmp:FaultCodes2'
      local.afterFaultCodeLookup(2)
  of 'tmp:FaultCodes3'
      local.afterFaultCodeLookup(3)
  of 'tmp:FaultCodes4'
      local.afterFaultCodeLookup(4)
  of 'tmp:FaultCodes5'
      local.afterFaultCodeLookup(5)
  of 'tmp:FaultCodes6'
      local.afterFaultCodeLookup(6)
  of 'tmp:FaultCodes7'
      local.afterFaultCodeLookup(7)
  of 'tmp:FaultCodes8'
      local.afterFaultCodeLookup(8)
  of 'tmp:FaultCodes9'
      local.afterFaultCodeLookup(9)
  of 'tmp:FaultCodes10'
      local.afterFaultCodeLookup(10)
  of 'tmp:FaultCodes11'
      local.afterFaultCodeLookup(11)
  of 'tmp:FaultCodes12'
      local.afterFaultCodeLookup(12)
  
  
  End
  If p_web.GSV('locOrderRequired') = 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:Location',tmp:Location)
  p_web.SetSessionValue('tmp:SecondLocation',tmp:SecondLocation)
  p_web.SetSessionValue('tmp:ShelfLocation',tmp:ShelfLocation)
  p_web.SetSessionValue('tmp:PurchaseCost',tmp:PurchaseCost)
  p_web.SetSessionValue('tmp:OutWarrantyCost',tmp:OutWarrantyCost)
  p_web.SetSessionValue('tmp:OutWarrantyMarkup',tmp:OutWarrantyMarkup)
  p_web.SetSessionValue('tmp:UnallocatePart',tmp:UnallocatePart)
  p_web.SetSessionValue('tmp:CreateOrder',tmp:CreateOrder)
  p_web.SetSessionValue('tmp:FaultCodesChecked',tmp:FaultCodesChecked)
  p_web.SetSessionValue('tmp:FaultCodes1',tmp:FaultCodes1)
  p_web.SetSessionValue('tmp:FaultCodes2',tmp:FaultCodes2)
  p_web.SetSessionValue('tmp:FaultCodes3',tmp:FaultCodes3)
  p_web.SetSessionValue('tmp:FaultCodes4',tmp:FaultCodes4)
  p_web.SetSessionValue('tmp:FaultCodes5',tmp:FaultCodes5)
  p_web.SetSessionValue('tmp:FaultCodes6',tmp:FaultCodes6)
  p_web.SetSessionValue('tmp:FaultCodes7',tmp:FaultCodes7)
  p_web.SetSessionValue('tmp:FaultCodes8',tmp:FaultCodes8)
  p_web.SetSessionValue('tmp:FaultCodes9',tmp:FaultCodes9)
  p_web.SetSessionValue('tmp:FaultCodes10',tmp:FaultCodes10)
  p_web.SetSessionValue('tmp:FaultCodes11',tmp:FaultCodes11)
  p_web.SetSessionValue('tmp:FaultCodes12',tmp:FaultCodes12)
  p_web.SetSessionValue('locPartUsedOnRepair',locPartUsedOnRepair)

RestoreMem       Routine
  !FormSource=File
  if p_web.IfExistsValue('tmp:Location')
    tmp:Location = p_web.GetValue('tmp:Location')
    p_web.SetSessionValue('tmp:Location',tmp:Location)
  End
  if p_web.IfExistsValue('tmp:SecondLocation')
    tmp:SecondLocation = p_web.GetValue('tmp:SecondLocation')
    p_web.SetSessionValue('tmp:SecondLocation',tmp:SecondLocation)
  End
  if p_web.IfExistsValue('tmp:ShelfLocation')
    tmp:ShelfLocation = p_web.GetValue('tmp:ShelfLocation')
    p_web.SetSessionValue('tmp:ShelfLocation',tmp:ShelfLocation)
  End
  if p_web.IfExistsValue('tmp:PurchaseCost')
    tmp:PurchaseCost = p_web.GetValue('tmp:PurchaseCost')
    p_web.SetSessionValue('tmp:PurchaseCost',tmp:PurchaseCost)
  End
  if p_web.IfExistsValue('tmp:OutWarrantyCost')
    tmp:OutWarrantyCost = p_web.GetValue('tmp:OutWarrantyCost')
    p_web.SetSessionValue('tmp:OutWarrantyCost',tmp:OutWarrantyCost)
  End
  if p_web.IfExistsValue('tmp:OutWarrantyMarkup')
    tmp:OutWarrantyMarkup = p_web.GetValue('tmp:OutWarrantyMarkup')
    p_web.SetSessionValue('tmp:OutWarrantyMarkup',tmp:OutWarrantyMarkup)
  End
  if p_web.IfExistsValue('tmp:UnallocatePart')
    tmp:UnallocatePart = p_web.GetValue('tmp:UnallocatePart')
    p_web.SetSessionValue('tmp:UnallocatePart',tmp:UnallocatePart)
  End
  if p_web.IfExistsValue('tmp:CreateOrder')
    tmp:CreateOrder = p_web.GetValue('tmp:CreateOrder')
    p_web.SetSessionValue('tmp:CreateOrder',tmp:CreateOrder)
  End
  if p_web.IfExistsValue('tmp:FaultCodesChecked')
    tmp:FaultCodesChecked = p_web.GetValue('tmp:FaultCodesChecked')
    p_web.SetSessionValue('tmp:FaultCodesChecked',tmp:FaultCodesChecked)
  End
  if p_web.IfExistsValue('tmp:FaultCodes1')
    tmp:FaultCodes1 = p_web.GetValue('tmp:FaultCodes1')
    p_web.SetSessionValue('tmp:FaultCodes1',tmp:FaultCodes1)
  End
  if p_web.IfExistsValue('tmp:FaultCodes2')
    tmp:FaultCodes2 = p_web.GetValue('tmp:FaultCodes2')
    p_web.SetSessionValue('tmp:FaultCodes2',tmp:FaultCodes2)
  End
  if p_web.IfExistsValue('tmp:FaultCodes3')
    tmp:FaultCodes3 = p_web.GetValue('tmp:FaultCodes3')
    p_web.SetSessionValue('tmp:FaultCodes3',tmp:FaultCodes3)
  End
  if p_web.IfExistsValue('tmp:FaultCodes4')
    tmp:FaultCodes4 = p_web.GetValue('tmp:FaultCodes4')
    p_web.SetSessionValue('tmp:FaultCodes4',tmp:FaultCodes4)
  End
  if p_web.IfExistsValue('tmp:FaultCodes5')
    tmp:FaultCodes5 = p_web.GetValue('tmp:FaultCodes5')
    p_web.SetSessionValue('tmp:FaultCodes5',tmp:FaultCodes5)
  End
  if p_web.IfExistsValue('tmp:FaultCodes6')
    tmp:FaultCodes6 = p_web.GetValue('tmp:FaultCodes6')
    p_web.SetSessionValue('tmp:FaultCodes6',tmp:FaultCodes6)
  End
  if p_web.IfExistsValue('tmp:FaultCodes7')
    tmp:FaultCodes7 = p_web.GetValue('tmp:FaultCodes7')
    p_web.SetSessionValue('tmp:FaultCodes7',tmp:FaultCodes7)
  End
  if p_web.IfExistsValue('tmp:FaultCodes8')
    tmp:FaultCodes8 = p_web.GetValue('tmp:FaultCodes8')
    p_web.SetSessionValue('tmp:FaultCodes8',tmp:FaultCodes8)
  End
  if p_web.IfExistsValue('tmp:FaultCodes9')
    tmp:FaultCodes9 = p_web.GetValue('tmp:FaultCodes9')
    p_web.SetSessionValue('tmp:FaultCodes9',tmp:FaultCodes9)
  End
  if p_web.IfExistsValue('tmp:FaultCodes10')
    tmp:FaultCodes10 = p_web.GetValue('tmp:FaultCodes10')
    p_web.SetSessionValue('tmp:FaultCodes10',tmp:FaultCodes10)
  End
  if p_web.IfExistsValue('tmp:FaultCodes11')
    tmp:FaultCodes11 = p_web.GetValue('tmp:FaultCodes11')
    p_web.SetSessionValue('tmp:FaultCodes11',tmp:FaultCodes11)
  End
  if p_web.IfExistsValue('tmp:FaultCodes12')
    tmp:FaultCodes12 = p_web.GetValue('tmp:FaultCodes12')
    p_web.SetSessionValue('tmp:FaultCodes12',tmp:FaultCodes12)
  End
  if p_web.IfExistsValue('locPartUsedOnRepair')
    locPartUsedOnRepair = p_web.GetValue('locPartUsedOnRepair')
    p_web.SetSessionValue('locPartUsedOnRepair',locPartUsedOnRepair)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormEstimateParts_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
      if (p_web.GSV('FormEstimateParts:FirstTime') = 0)
          !Write Fault Codes
          p_web.SSV('epr:Fault_Code1',epr:Fault_Code1)
          p_web.SSV('epr:Fault_Code2',epr:Fault_Code2)
          p_web.SSV('epr:Fault_Code3',epr:Fault_Code3)
          p_web.SSV('epr:Fault_Code4',epr:Fault_Code4)
          p_web.SSV('epr:Fault_Code5',epr:Fault_Code5)
          p_web.SSV('epr:Fault_Code6',epr:Fault_Code6)
          p_web.SSV('epr:Fault_Code7',epr:Fault_Code7)
          p_web.SSV('epr:Fault_Code8',epr:Fault_Code8)
          p_web.SSV('epr:Fault_Code9',epr:Fault_Code9)
          p_web.SSV('epr:Fault_Code10',epr:Fault_Code10)
          p_web.SSV('epr:Fault_Code11',epr:Fault_Code11)
          p_web.SSV('epr:Fault_Code12',epr:Fault_Code12)
  
  
          Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
          map:Manufacturer    = p_web.GSV('job:Manufacturer')
          map:ScreenOrder    = 0
          set(map:ScreenOrderKey,map:ScreenOrderKey)
          loop
              if (Access:MANFAUPA.Next())
                  Break
              end ! if (Access:MANFAUPA.Next())
              if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                  Break
              end ! if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
              if (map:ScreenOrder    = 0)
                  cycle
              end ! if (map:ScreenOrder    <> 0)
  
              p_web.SSV('tmp:FaultCodes' & map:ScreenOrder,p_web.GSV('epr:Fault_Code' & map:Field_Number))
          end ! loop
  
  !        loop x# = 1 To 12
  !            p_web.SSV('tmp:FaultCodes' & x#,p_web.GSV('epr:Fault_Code' & x#))
  !            linePrint('tmp:FaultCode' & x# & ' - ' & p_web.GSV('tmp:FaultCode' & x#),'c:\log.log')
  !        end ! loop x# = 1 To 12
          p_web.SSV('FormEstimateParts:FirstTime',1)
          !p_web.SSV('locOrderRequired',0)
  
          ! Save For Later
          p_web.SSV('Save:Quantity',epr:Quantity)
      end ! if (p_web.GSV('FormEstimateParts:FirstTime',0))
      do updateComments
  
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:Location = p_web.RestoreValue('tmp:Location')
 tmp:SecondLocation = p_web.RestoreValue('tmp:SecondLocation')
 tmp:ShelfLocation = p_web.RestoreValue('tmp:ShelfLocation')
 tmp:PurchaseCost = p_web.RestoreValue('tmp:PurchaseCost')
 tmp:OutWarrantyCost = p_web.RestoreValue('tmp:OutWarrantyCost')
 tmp:OutWarrantyMarkup = p_web.RestoreValue('tmp:OutWarrantyMarkup')
 tmp:UnallocatePart = p_web.RestoreValue('tmp:UnallocatePart')
 tmp:CreateOrder = p_web.RestoreValue('tmp:CreateOrder')
 tmp:FaultCodesChecked = p_web.RestoreValue('tmp:FaultCodesChecked')
 tmp:FaultCodes1 = p_web.RestoreValue('tmp:FaultCodes1')
 tmp:FaultCodes2 = p_web.RestoreValue('tmp:FaultCodes2')
 tmp:FaultCodes3 = p_web.RestoreValue('tmp:FaultCodes3')
 tmp:FaultCodes4 = p_web.RestoreValue('tmp:FaultCodes4')
 tmp:FaultCodes5 = p_web.RestoreValue('tmp:FaultCodes5')
 tmp:FaultCodes6 = p_web.RestoreValue('tmp:FaultCodes6')
 tmp:FaultCodes7 = p_web.RestoreValue('tmp:FaultCodes7')
 tmp:FaultCodes8 = p_web.RestoreValue('tmp:FaultCodes8')
 tmp:FaultCodes9 = p_web.RestoreValue('tmp:FaultCodes9')
 tmp:FaultCodes10 = p_web.RestoreValue('tmp:FaultCodes10')
 tmp:FaultCodes11 = p_web.RestoreValue('tmp:FaultCodes11')
 tmp:FaultCodes12 = p_web.RestoreValue('tmp:FaultCodes12')
 locPartUsedOnRepair = p_web.RestoreValue('locPartUsedOnRepair')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormEstimateParts_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormEstimateParts_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormEstimateParts_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
    loc:viewonly = Choose(p_web.GSV('Parts:ViewOnly') = 1,1,0)
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  packet = clip(packet) & '<input type="hidden" name="ESTPARTS__FileAction" value="'&p_web.getSessionValue('ESTPARTS:FileAction')&'" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="file" value="ESTPARTS" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateFile" value="ESTPARTS" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateKey" value="epr:record_number_key" ></input><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormEstimateParts" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormEstimateParts" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormEstimateParts" ></input><13,10>'
  end

  do SendPacket
  packet = clip(packet) & p_web.CreateInput('hidden','epr:Record_Number',p_web._jsok(p_web.getSessionValue('epr:Record_Number'))) & '<13,10>'
  If p_web.Translate('Insert / Amend Estimate Parts') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Insert / Amend Estimate Parts',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormEstimateParts">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormEstimateParts" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  do GenerateTab4
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormEstimateParts')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If loc:act = ChangeRecord
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Status') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Details') & ''''
        If p_web.GSV('locOrderRequired') = 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Order Required') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Fault Codes') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Select When Part Will Be Used') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormEstimateParts')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormEstimateParts'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='STOMODEL'
            p_web.SetValue('SelectField',clip(loc:formname) & '.epr:Description')
    End
    If upper(p_web.getvalue('LookupFile'))='SUPPLIER'
        If p_web.GSV('Hide:PartFaultCode1') <> 1
            p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCodes1')
        End
    End
  Else
    If False
    ElsIf loc:act = ChangeRecord
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.epr:Part_Number')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If loc:act = ChangeRecord
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          If p_web.GSV('locOrderRequired') = 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab6'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormEstimateParts')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if loc:act = ChangeRecord
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    if p_web.GSV('locOrderRequired') = 1
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab6'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If loc:act = ChangeRecord
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Part Status') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormEstimateParts_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Status')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Part Status')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Status')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Status')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::epr:Order_Number
      do Value::epr:Order_Number
      do Comment::epr:Order_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::epr:Date_Ordered
      do Value::epr:Date_Ordered
      do Comment::epr:Date_Ordered
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::epr:Date_Received
      do Value::epr:Date_Received
      do Comment::epr:Date_Received
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locOutFaultCode
      do Comment::locOutFaultCode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Part Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormEstimateParts_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::epr:Part_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::epr:Part_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::epr:Part_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::epr:Description
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::epr:Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::epr:Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::epr:Despatch_Note_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::epr:Despatch_Note_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::epr:Despatch_Note_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::epr:Quantity
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::epr:Quantity
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::epr:Quantity
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      !Set Width
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:Location
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:Location
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:Location
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:SecondLocation
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:SecondLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:SecondLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ShelfLocation
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ShelfLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ShelfLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:PurchaseCost
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:PurchaseCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:PurchaseCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:OutWarrantyCost
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:OutWarrantyCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:OutWarrantyCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:OutWarrantyMarkup
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:OutWarrantyMarkup
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:OutWarrantyMarkup
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FixedPrice
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FixedPrice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FixedPrice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::epr:Supplier
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::epr:Supplier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::epr:Supplier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::epr:Exclude_From_Order
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::epr:Exclude_From_Order
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::epr:Exclude_From_Order
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::epr:PartAllocated
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::epr:PartAllocated
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::epr:PartAllocated
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('Show:UnallocatePart') = 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:UnallocatePart
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:UnallocatePart
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:UnallocatePart
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
  If p_web.GSV('locOrderRequired') = 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Order Required') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormEstimateParts_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Order Required')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3" class="'&clip('RedBold')&'">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Order Required')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3" class="'&clip('RedBold')&'">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Order Required')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Order Required')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::text:OrderRequired
      do Comment::text:OrderRequired
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::text:OrderRequired2
      do Comment::text:OrderRequired2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:CreateOrder
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:CreateOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:CreateOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Fault Codes') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormEstimateParts_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Fault Codes')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Fault Codes')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Fault Codes')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Fault Codes')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    If 0
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCodesChecked
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCodesChecked
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCodesChecked
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode1') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCodes1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCodes1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCodes1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode2') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode3') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode4') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode4
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode5') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode5
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode6') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode6
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode7') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode7
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode8') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode8
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode8
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode8
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode9') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode9
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode9
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode9
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode10') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode10
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode10
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode10
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode11') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode11
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode11
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode11
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:PartFaultCode12') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode12
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode12
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode12
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab4  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel6">'&CRLF &|
                                    '  <div id="panel6Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Select When Part Will Be Used') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel6Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormEstimateParts_6">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select When Part Will Be Used')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab6" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab6">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Select When Part Will Be Used')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab6">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select When Part Will Be Used')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab6">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select When Part Will Be Used')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'20%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::epr:UsedOnRepair
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'35%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::epr:UsedOnRepair
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::epr:Order_Number  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Order_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Order Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::epr:Order_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('epr:Order_Number',p_web.GetValue('NewValue'))
    epr:Order_Number = p_web.GetValue('NewValue') !FieldType= LONG Field = epr:Order_Number
    do Value::epr:Order_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('epr:Order_Number',p_web.dFormat(p_web.GetValue('Value'),'@n08b'))
    epr:Order_Number = p_web.Dformat(p_web.GetValue('Value'),'@n08b') !
  End

Value::epr:Order_Number  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Order_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- epr:Order_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(format(p_web.GetSessionValue('epr:Order_Number'),'@n08b')) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::epr:Order_Number  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Order_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::epr:Date_Ordered  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Date_Ordered') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Date Ordered')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::epr:Date_Ordered  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('epr:Date_Ordered',p_web.GetValue('NewValue'))
    epr:Date_Ordered = p_web.GetValue('NewValue') !FieldType= DATE Field = epr:Date_Ordered
    do Value::epr:Date_Ordered
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('epr:Date_Ordered',p_web.dFormat(p_web.GetValue('Value'),'@d6b'))
    epr:Date_Ordered = p_web.GetValue('Value')
  End

Value::epr:Date_Ordered  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Date_Ordered') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- epr:Date_Ordered
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('epr:Date_Ordered'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::epr:Date_Ordered  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Date_Ordered') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::epr:Date_Received  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Date_Received') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Date Received')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::epr:Date_Received  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('epr:Date_Received',p_web.GetValue('NewValue'))
    epr:Date_Received = p_web.GetValue('NewValue') !FieldType= DATE Field = epr:Date_Received
    do Value::epr:Date_Received
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('epr:Date_Received',p_web.dFormat(p_web.GetValue('Value'),'@d6b'))
    epr:Date_Received = p_web.GetValue('Value')
  End

Value::epr:Date_Received  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Date_Received') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- epr:Date_Received
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('epr:Date_Received'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::epr:Date_Received  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Date_Received') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locOutFaultCode  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locOutFaultCode',p_web.GetValue('NewValue'))
    do Value::locOutFaultCode
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::locOutFaultCode  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('locOutFaultCode') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('locOutFaultCode'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locOutFaultCode  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('locOutFaultCode') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::epr:Part_Number  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Part_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Part Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::epr:Part_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('epr:Part_Number',p_web.GetValue('NewValue'))
    epr:Part_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = epr:Part_Number
    do Value::epr:Part_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('epr:Part_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    epr:Part_Number = p_web.GetValue('Value')
  End
  If epr:Part_Number = ''
    loc:Invalid = 'epr:Part_Number'
    loc:alert = p_web.translate('Part Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    epr:Part_Number = Upper(epr:Part_Number)
    p_web.SetSessionValue('epr:Part_Number',epr:Part_Number)
      if (p_web.GSV('job:Engineer') <> '')
          locUserCode = p_web.GSV('job:Engineer')
      else !
          locUserCode = p_web.GSV('BookingUSerCOde')
      end !if (p_web.GSV('job:Engineer') <> '')
  
      case validFreeTextPart('C',locUserCode,p_web.GSV('job:Manufacturer'),|
                                  p_web.GSV('job:Model_Number'),p_web.GSV('epr:part_Number'))
      of 0
          p_web.SSV('Comment:PartNumber','')
  
          Access:STOMODEL.Clearkey(stm:Location_Part_Number_Key)
          stm:Model_Number    = p_web.GSV('job:Model_Number')
          stm:Location    = p_web.GSV('BookingSiteLocation')
          stm:Part_Number    = p_web.GSV('epr:Part_Number')
          if (Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign)
              ! Found
          else ! if (Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign)
              ! Error
          end ! if (Access:STOMODEL.TryFetch(stm:Location_Part_Number_Key) = Level:Benign)
  
          do updatePartDetails
      of 1
          p_web.SSV('epr:Part_Number','')
          p_web.SSV('Comment:PartNumber','Invalid User')
      of 2
          p_web.SSV('epr:Part_Number','')
          p_web.SSV('Comment:PartNumber','Error! Selected Part Is Not In Stock Location')
      of 3
          p_web.SSV('epr:Part_Number','')
          p_web.SSV('Comment:PartNumber','Error! Selected Part Is Suspended')
      of 4
          p_web.SSV('Comment:PartNumber','Warning! Cannot Find The Selected Part In Stock')
      of 5
          p_web.SSV('epr:part_Number','')
          p_web.SSV('Comment:PartNumber','Error! Access Level Is Not High Enough For Part')
  
      end ! case
  
  p_Web.SetValue('lookupfield','epr:Part_Number')
  do AfterLookup
  do Value::epr:Part_Number
  do SendAlert
  do Comment::epr:Part_Number
  do Comment::epr:Part_Number
  do Value::epr:Description  !1
  do Value::epr:Supplier  !1
  do Value::tmp:PurchaseCost  !1
  do Value::tmp:OutWarrantyCost  !1
  do Value::tmp:OutWarrantyMarkup  !1
  do Prompt::tmp:SecondLocation
  do Value::tmp:SecondLocation  !1
  do Prompt::tmp:ShelfLocation
  do Value::tmp:ShelfLocation  !1
  do Prompt::tmp:Location
  do Value::tmp:Location  !1

Value::epr:Part_Number  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Part_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- epr:Part_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('epr:Part_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If epr:Part_Number = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''epr:Part_Number'',''formestimateparts_epr:part_number_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('epr:Part_Number')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','epr:Part_Number',p_web.GetSessionValue('epr:Part_Number'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseModelStock')&'?LookupField=epr:Part_Number&Tab=4&ForeignField=stm:Part_Number&_sort=stm:Description&Refresh=sort&LookupFrom=FormEstimateParts&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('epr:Part_Number') & '_value')

Comment::epr:Part_Number  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartNumber'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Part_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('epr:Part_Number') & '_comment')

Prompt::epr:Description  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Description') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Description')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::epr:Description  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('epr:Description',p_web.GetValue('NewValue'))
    epr:Description = p_web.GetValue('NewValue') !FieldType= STRING Field = epr:Description
    do Value::epr:Description
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('epr:Description',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    epr:Description = p_web.GetValue('Value')
  End
  If epr:Description = ''
    loc:Invalid = 'epr:Description'
    loc:alert = p_web.translate('Description') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    epr:Description = Upper(epr:Description)
    p_web.SetSessionValue('epr:Description',epr:Description)
  do Value::epr:Description
  do SendAlert

Value::epr:Description  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Description') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- epr:Description
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.getValue('adjustment') = 1 OR loc:act = ChangeRecord
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('epr:Description')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If epr:Description = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''epr:Description'',''formestimateparts_epr:description_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','epr:Description',p_web.GetSessionValueFormat('epr:Description'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('epr:Description') & '_value')

Comment::epr:Description  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Description') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::epr:Despatch_Note_Number  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Despatch_Note_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Despatch Note Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::epr:Despatch_Note_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('epr:Despatch_Note_Number',p_web.GetValue('NewValue'))
    epr:Despatch_Note_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = epr:Despatch_Note_Number
    do Value::epr:Despatch_Note_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('epr:Despatch_Note_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    epr:Despatch_Note_Number = p_web.GetValue('Value')
  End
    epr:Despatch_Note_Number = Upper(epr:Despatch_Note_Number)
    p_web.SetSessionValue('epr:Despatch_Note_Number',epr:Despatch_Note_Number)
  do Value::epr:Despatch_Note_Number
  do SendAlert

Value::epr:Despatch_Note_Number  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Despatch_Note_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- epr:Despatch_Note_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('epr:Despatch_Note_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''epr:Despatch_Note_Number'',''formestimateparts_epr:despatch_note_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','epr:Despatch_Note_Number',p_web.GetSessionValueFormat('epr:Despatch_Note_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('epr:Despatch_Note_Number') & '_value')

Comment::epr:Despatch_Note_Number  Routine
      loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Despatch_Note_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::epr:Quantity  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Quantity') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Quantity')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::epr:Quantity  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('epr:Quantity',p_web.GetValue('NewValue'))
    epr:Quantity = p_web.GetValue('NewValue') !FieldType= LONG Field = epr:Quantity
    do Value::epr:Quantity
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('epr:Quantity',p_web.dFormat(p_web.GetValue('Value'),'@n4'))
    epr:Quantity = p_web.Dformat(p_web.GetValue('Value'),'@n4') !
  End
  If epr:Quantity = ''
    loc:Invalid = 'epr:Quantity'
    loc:alert = p_web.translate('Quantity') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    epr:Quantity = Upper(epr:Quantity)
    p_web.SetSessionValue('epr:Quantity',epr:Quantity)
  do Value::epr:Quantity
  do SendAlert

Value::epr:Quantity  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Quantity') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- epr:Quantity
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.getValue('adjustment') = 1 OR p_web.GSV('ReadOnly:Quantity') = 1 or loc:act = ChangeRecord,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.getValue('adjustment') = 1 OR p_web.GSV('ReadOnly:Quantity') = 1 or loc:act = ChangeRecord
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('epr:Quantity')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If epr:Quantity = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''epr:Quantity'',''formestimateparts_epr:quantity_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','epr:Quantity',p_web.GetSessionValue('epr:Quantity'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n4',loc:javascript,,'Quantity') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('epr:Quantity') & '_value')

Comment::epr:Quantity  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Quantity') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:Location  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:Location') & '_prompt',Choose(p_web.GSV('tmp:Location') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Location')
  If p_web.GSV('tmp:Location') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:Location') & '_prompt')

Validate::tmp:Location  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:Location',p_web.GetValue('NewValue'))
    tmp:Location = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:Location
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:Location',p_web.GetValue('Value'))
    tmp:Location = p_web.GetValue('Value')
  End

Value::tmp:Location  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:Location') & '_value',Choose(p_web.GSV('tmp:Location') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:Location') = '')
  ! --- DISPLAY --- tmp:Location
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('tmp:Location'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:Location') & '_value')

Comment::tmp:Location  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:Location') & '_comment',Choose(p_web.GSV('tmp:Location') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:Location') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:SecondLocation  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:SecondLocation') & '_prompt',Choose(p_web.GSV('tmp:SecondLocation') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Second Location')
  If p_web.GSV('tmp:SecondLocation') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:SecondLocation') & '_prompt')

Validate::tmp:SecondLocation  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:SecondLocation',p_web.GetValue('NewValue'))
    tmp:SecondLocation = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:SecondLocation
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:SecondLocation',p_web.GetValue('Value'))
    tmp:SecondLocation = p_web.GetValue('Value')
  End

Value::tmp:SecondLocation  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:SecondLocation') & '_value',Choose(p_web.GSV('tmp:SecondLocation') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:SecondLocation') = '')
  ! --- DISPLAY --- tmp:SecondLocation
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('tmp:SecondLocation'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:SecondLocation') & '_value')

Comment::tmp:SecondLocation  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:SecondLocation') & '_comment',Choose(p_web.GSV('tmp:SecondLocation') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:SecondLocation') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ShelfLocation  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:ShelfLocation') & '_prompt',Choose(p_web.GSV('tmp:ShelfLocation') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Shelf Location')
  If p_web.GSV('tmp:ShelfLocation') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:ShelfLocation') & '_prompt')

Validate::tmp:ShelfLocation  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ShelfLocation',p_web.GetValue('NewValue'))
    tmp:ShelfLocation = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ShelfLocation
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ShelfLocation',p_web.GetValue('Value'))
    tmp:ShelfLocation = p_web.GetValue('Value')
  End

Value::tmp:ShelfLocation  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:ShelfLocation') & '_value',Choose(p_web.GSV('tmp:ShelfLocation') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:ShelfLocation') = '')
  ! --- DISPLAY --- tmp:ShelfLocation
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('tmp:ShelfLocation'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:ShelfLocation') & '_value')

Comment::tmp:ShelfLocation  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:ShelfLocation') & '_comment',Choose(p_web.GSV('tmp:ShelfLocation') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:ShelfLocation') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:PurchaseCost  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:PurchaseCost') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Purchase Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:PurchaseCost  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:PurchaseCost',p_web.GetValue('NewValue'))
    tmp:PurchaseCost = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:PurchaseCost
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:PurchaseCost',p_web.dFormat(p_web.GetValue('Value'),'@n_14.2'))
    tmp:PurchaseCost = p_web.Dformat(p_web.GetValue('Value'),'@n_14.2') !
  End
  do Value::tmp:PurchaseCost
  do SendAlert

Value::tmp:PurchaseCost  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:PurchaseCost') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:PurchaseCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.getValue('adjustment') = 1 or p_web.GSV('ReadOnly:PurchaseCost') = 1 OR loc:act = ChangeRecord,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.getValue('adjustment') = 1 or p_web.GSV('ReadOnly:PurchaseCost') = 1 OR loc:act = ChangeRecord
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:PurchaseCost')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:PurchaseCost'',''formestimateparts_tmp:purchasecost_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:PurchaseCost',p_web.GetSessionValue('tmp:PurchaseCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n_14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:PurchaseCost') & '_value')

Comment::tmp:PurchaseCost  Routine
      loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:PurchaseCost') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:OutWarrantyCost  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyCost') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Out Of Warranty Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:OutWarrantyCost  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:OutWarrantyCost',p_web.GetValue('NewValue'))
    tmp:OutWarrantyCost = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:OutWarrantyCost
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:OutWarrantyCost',p_web.dFormat(p_web.GetValue('Value'),'@n_14.2'))
    tmp:OutWarrantyCost = p_web.Dformat(p_web.GetValue('Value'),'@n_14.2') !
  End
  do enableDisableCosts
  do Value::tmp:OutWarrantyCost
  do SendAlert
  do Value::tmp:OutWarrantyMarkup  !1

Value::tmp:OutWarrantyCost  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyCost') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:OutWarrantyCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:OutWarrantyCost') = 1 Or p_web.GSV('tmp:OutWarrantyMarkup') > 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:OutWarrantyCost') = 1 Or p_web.GSV('tmp:OutWarrantyMarkup') > 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:OutWarrantyCost')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:OutWarrantyCost'',''formestimateparts_tmp:outwarrantycost_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OutWarrantyCost')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:OutWarrantyCost',p_web.GetSessionValue('tmp:OutWarrantyCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n_14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyCost') & '_value')

Comment::tmp:OutWarrantyCost  Routine
      loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyCost') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:OutWarrantyMarkup  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyMarkup') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Markup')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:OutWarrantyMarkup  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:OutWarrantyMarkup',p_web.GetValue('NewValue'))
    tmp:OutWarrantyMarkup = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:OutWarrantyMarkup
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:OutWarrantyMarkup',p_web.dFormat(p_web.GetValue('Value'),'@n3'))
    tmp:OutWarrantyMarkup = p_web.Dformat(p_web.GetValue('Value'),'@n3') !
  End
  !Out Warranty Markup
  markup# = GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI')
  if (p_web.GSV('tmp:OutWarrantyMarkup') > markup#)
      p_web.SSV('Comment:OutWarrantyMarkup','You cannot markup more than ' & markup# & '%')
      p_web.SSV('tmp:OutWarrantyMarkup',markup#)
  else! if (p_web.GSV('tmp:OutWarrantyMarkup') > GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI'))
      p_web.SSV('Comment:OutWarrantyMarkup','')
  end ! if (p_web.GSV('tmp:OutWarrantyMarkup') > GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI'))
  
  if (p_web.GSV('tmp:OutWarrantyMarkup') > 0)
      p_web.SSV('tmp:OutWarrantyCost',format(p_web.GSV('tmp:PurchaseCost') + (p_web.GSV('tmp:PurchaseCost') * (p_web.GSV('tmp:OutWarrantyMarkup')/100)),@n_14.2))
  end !if (p_web.GSV('tmp:OutWarrantyMarkup') > 0)
  do Value::tmp:OutWarrantyMarkup
  do SendAlert
  do Value::tmp:OutWarrantyCost  !1
  do Comment::tmp:OutWarrantyMarkup

Value::tmp:OutWarrantyMarkup  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyMarkup') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:OutWarrantyMarkup
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:OutWarrantyMarkup') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:OutWarrantyMarkup') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:OutWarrantyMarkup')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:OutWarrantyMarkup'',''formestimateparts_tmp:outwarrantymarkup_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OutWarrantyMarkup')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:OutWarrantyMarkup',p_web.GetSessionValue('tmp:OutWarrantyMarkup'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n3',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyMarkup') & '_value')

Comment::tmp:OutWarrantyMarkup  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:OutWarrantyMarkup'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyMarkup') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:OutWarrantyMarkup') & '_comment')

Prompt::tmp:FixedPrice  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FixedPrice') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FixedPrice  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FixedPrice',p_web.GetValue('NewValue'))
    do Value::tmp:FixedPrice
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::tmp:FixedPrice  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FixedPrice') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::tmp:FixedPrice  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FixedPrice') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::epr:Supplier  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Supplier') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Supplier')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::epr:Supplier  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('epr:Supplier',p_web.GetValue('NewValue'))
    epr:Supplier = p_web.GetValue('NewValue') !FieldType= STRING Field = epr:Supplier
    do Value::epr:Supplier
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('epr:Supplier',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    epr:Supplier = p_web.GetValue('Value')
  End
    epr:Supplier = Upper(epr:Supplier)
    p_web.SetSessionValue('epr:Supplier',epr:Supplier)
  p_Web.SetValue('lookupfield','epr:Supplier')
  do AfterLookup
  do Value::epr:Supplier
  do SendAlert
  do Comment::epr:Supplier

Value::epr:Supplier  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Supplier') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- epr:Supplier
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:act = ChangeRecord,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If loc:act = ChangeRecord
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('epr:Supplier')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''epr:Supplier'',''formestimateparts_epr:supplier_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon(epr:Supplier)&''',2);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','epr:Supplier',p_web.GetSessionValue('epr:Supplier'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectSuppliers')&'?LookupField=epr:Supplier&Tab=4&ForeignField=sup:Company_Name&_sort=sup:Company_Name&Refresh=sort&LookupFrom=FormEstimateParts&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('epr:Supplier') & '_value')

Comment::epr:Supplier  Routine
      loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Supplier') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('epr:Supplier') & '_comment')

Prompt::epr:Exclude_From_Order  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Exclude_From_Order') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Exclude From Order')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::epr:Exclude_From_Order  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('epr:Exclude_From_Order',p_web.GetValue('NewValue'))
    epr:Exclude_From_Order = p_web.GetValue('NewValue') !FieldType= STRING Field = epr:Exclude_From_Order
    do Value::epr:Exclude_From_Order
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('epr:Exclude_From_Order',p_web.dFormat(p_web.GetValue('Value'),'@s3'))
    epr:Exclude_From_Order = p_web.GetValue('Value')
  End
  do Value::epr:Exclude_From_Order
  do SendAlert

Value::epr:Exclude_From_Order  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Exclude_From_Order') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- epr:Exclude_From_Order
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('epr:Exclude_From_Order')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExcludeFromOrder') = 1,'disabled','')
    if p_web.GetSessionValue('epr:Exclude_From_Order') = 'YES'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''epr:Exclude_From_Order'',''formestimateparts_epr:exclude_from_order_value'',1,'''&clip('YES')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','epr:Exclude_From_Order',clip('YES'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'epr:Exclude_From_Order_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ExcludeFromOrder') = 1,'disabled','')
    if p_web.GetSessionValue('epr:Exclude_From_Order') = 'NO'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''epr:Exclude_From_Order'',''formestimateparts_epr:exclude_from_order_value'',1,'''&clip('NO')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','epr:Exclude_From_Order',clip('NO'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'epr:Exclude_From_Order_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('epr:Exclude_From_Order') & '_value')

Comment::epr:Exclude_From_Order  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:Exclude_From_Order') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::epr:PartAllocated  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:PartAllocated') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Part Allocated')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::epr:PartAllocated  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('epr:PartAllocated',p_web.GetValue('NewValue'))
    epr:PartAllocated = p_web.GetValue('NewValue') !FieldType= BYTE Field = epr:PartAllocated
    do Value::epr:PartAllocated
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('epr:PartAllocated',p_web.dFormat(p_web.GetValue('Value'),'@n1'))
    epr:PartAllocated = p_web.GetValue('Value')
  End
  do Value::epr:PartAllocated
  do SendAlert

Value::epr:PartAllocated  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:PartAllocated') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- epr:PartAllocated
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('epr:PartAllocated')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartAllocated') = 1,'disabled','')
    if p_web.GetSessionValue('epr:PartAllocated') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''epr:PartAllocated'',''formestimateparts_epr:partallocated_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','epr:PartAllocated',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Part Allocated','epr:PartAllocated_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartAllocated') = 1,'disabled','')
    if p_web.GetSessionValue('epr:PartAllocated') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''epr:PartAllocated'',''formestimateparts_epr:partallocated_value'',1,'''&clip(0)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','epr:PartAllocated',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Part Allocated','epr:PartAllocated_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('epr:PartAllocated') & '_value')

Comment::epr:PartAllocated  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:PartAllocated') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:UnallocatePart  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:UnallocatePart') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Unallocate Part')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:UnallocatePart  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:UnallocatePart',p_web.GetValue('NewValue'))
    tmp:UnallocatePart = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:UnallocatePart
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:UnallocatePart',p_web.GetValue('Value'))
    tmp:UnallocatePart = p_web.GetValue('Value')
  End
  do Value::tmp:UnallocatePart
  do SendAlert

Value::tmp:UnallocatePart  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:UnallocatePart') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- tmp:UnallocatePart
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:UnallocatePart'',''formestimateparts_tmp:unallocatepart_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('tmp:UnallocatePart') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:UnallocatePart',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:UnallocatePart') & '_value')

Comment::tmp:UnallocatePart  Routine
    loc:comment = p_web.Translate('Return part to stock')
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:UnallocatePart') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::text:OrderRequired  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text:OrderRequired',p_web.GetValue('NewValue'))
    do Value::text:OrderRequired
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text:OrderRequired  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('text:OrderRequired') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBoldLarge')&'">' & p_web.Translate(p_web.GSV('text:OrderRequired'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::text:OrderRequired  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('text:OrderRequired') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::text:OrderRequired2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text:OrderRequired2',p_web.GetValue('NewValue'))
    do Value::text:OrderRequired2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text:OrderRequired2  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('text:OrderRequired2') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.Translate('Either reduce the quanitity required, or select the check box to create an order for the excess items',) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::text:OrderRequired2  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('text:OrderRequired2') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:CreateOrder  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:CreateOrder') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Create Order For Selected Part')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:CreateOrder  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:CreateOrder',p_web.GetValue('NewValue'))
    tmp:CreateOrder = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:CreateOrder
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:CreateOrder',p_web.GetValue('Value'))
    tmp:CreateOrder = p_web.GetValue('Value')
  End
  do Value::tmp:CreateOrder
  do SendAlert

Value::tmp:CreateOrder  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:CreateOrder') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- tmp:CreateOrder
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:CreateOrder'',''formestimateparts_tmp:createorder_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('tmp:CreateOrder') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:CreateOrder',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:CreateOrder') & '_value')

Comment::tmp:CreateOrder  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:CreateOrder') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCodesChecked  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCodesChecked') & '_prompt',Choose(p_web.GSV('Hide:FaultCodesChecked') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Fault Codes Checked')
  If p_web.GSV('Hide:FaultCodesChecked') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCodesChecked  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCodesChecked',p_web.GetValue('NewValue'))
    tmp:FaultCodesChecked = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCodesChecked
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:FaultCodesChecked',p_web.GetValue('Value'))
    tmp:FaultCodesChecked = p_web.GetValue('Value')
  End
  do Value::tmp:FaultCodesChecked
  do SendAlert

Value::tmp:FaultCodesChecked  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCodesChecked') & '_value',Choose(p_web.GSV('Hide:FaultCodesChecked') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:FaultCodesChecked') = 1)
  ! --- CHECKBOX --- tmp:FaultCodesChecked
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:FaultCodesChecked'',''formestimateparts_tmp:faultcodeschecked_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('tmp:FaultCodesChecked') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:FaultCodesChecked',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCodesChecked') & '_value')

Comment::tmp:FaultCodesChecked  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCodesChecked') & '_comment',Choose(p_web.GSV('Hide:FaultCodesChecked') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:FaultCodesChecked') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCodes1  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCodes1') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode1'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCodes1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCodes1',p_web.GetValue('NewValue'))
    tmp:FaultCodes1 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCodes1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes1',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode1')))
    tmp:FaultCodes1 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode1')) !
  End
  If tmp:FaultCodes1 = '' and p_web.GSV('Req:PartFautlCode1') = 1
    loc:Invalid = 'tmp:FaultCodes1'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode1')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes1 = Upper(tmp:FaultCodes1)
    p_web.SetSessionValue('tmp:FaultCodes1',tmp:FaultCodes1)
  do Value::tmp:FaultCodes1
  do SendAlert

Value::tmp:FaultCodes1  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCodes1') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode1') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode1') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFautlCode1') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes1')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes1 = '' and (p_web.GSV('Req:PartFautlCode1') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCodes1'',''formestimateparts_tmp:faultcodes1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes1',p_web.GetSessionValue('tmp:FaultCodes1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode1'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(1)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCodes1') & '_value')

Comment::tmp:FaultCodes1  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode1'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCodes1') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode2  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode2') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode2'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode2',p_web.GetValue('NewValue'))
    tmp:FaultCodes2 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes2',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode2')))
    tmp:FaultCodes2 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode2')) !
  End
  If tmp:FaultCodes2 = '' and p_web.GSV('Req:PartFaultCode2') = 1
    loc:Invalid = 'tmp:FaultCodes2'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode2')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes2 = Upper(tmp:FaultCodes2)
    p_web.SetSessionValue('tmp:FaultCodes2',tmp:FaultCodes2)
  do Value::tmp:FaultCode2
  do SendAlert

Value::tmp:FaultCode2  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode2') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode2') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode2') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode2') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes2')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes2 = '' and (p_web.GSV('Req:PartFaultCode2') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode2'',''formestimateparts_tmp:faultcode2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes2',p_web.GetSessionValue('tmp:FaultCodes2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode2'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(2)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode2') & '_value')

Comment::tmp:FaultCode2  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode2'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode2') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode3  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode3') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode3'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode3',p_web.GetValue('NewValue'))
    tmp:FaultCodes3 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes3',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode3')))
    tmp:FaultCodes3 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode3')) !
  End
  If tmp:FaultCodes3 = '' and p_web.GSV('Req:PartFaultCode3') = 1
    loc:Invalid = 'tmp:FaultCodes3'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode3')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes3 = Upper(tmp:FaultCodes3)
    p_web.SetSessionValue('tmp:FaultCodes3',tmp:FaultCodes3)
  do Value::tmp:FaultCode3
  do SendAlert

Value::tmp:FaultCode3  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode3') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode3') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode3') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode3') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes3')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes3 = '' and (p_web.GSV('Req:PartFaultCode3') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode3'',''formestimateparts_tmp:faultcode3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes3',p_web.GetSessionValue('tmp:FaultCodes3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode3'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(3)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode3') & '_value')

Comment::tmp:FaultCode3  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode3'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode3') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode4  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode4') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode4'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode4',p_web.GetValue('NewValue'))
    tmp:FaultCodes4 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes4',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode4')))
    tmp:FaultCodes4 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode4')) !
  End
  If tmp:FaultCodes4 = '' and p_web.GSV('Req:PartFaultCode4') = 1
    loc:Invalid = 'tmp:FaultCodes4'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode4')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes4 = Upper(tmp:FaultCodes4)
    p_web.SetSessionValue('tmp:FaultCodes4',tmp:FaultCodes4)
  do Value::tmp:FaultCode4
  do SendAlert

Value::tmp:FaultCode4  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode4') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes4
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode4') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode4') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode4') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes4')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes4 = '' and (p_web.GSV('Req:PartFaultCode4') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode4'',''formestimateparts_tmp:faultcode4_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes4',p_web.GetSessionValue('tmp:FaultCodes4'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode4'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(4)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode4') & '_value')

Comment::tmp:FaultCode4  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode4'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode4') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode5  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode5') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode5'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode5  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode5',p_web.GetValue('NewValue'))
    tmp:FaultCodes5 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode5
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes5',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode5')))
    tmp:FaultCodes5 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode5')) !
  End
  If tmp:FaultCodes5 = '' and p_web.GSV('Req:PartFaultCode5') = 1
    loc:Invalid = 'tmp:FaultCodes5'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode5')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes5 = Upper(tmp:FaultCodes5)
    p_web.SetSessionValue('tmp:FaultCodes5',tmp:FaultCodes5)
  do Value::tmp:FaultCode5
  do SendAlert

Value::tmp:FaultCode5  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode5') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes5
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode5') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode5') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode5') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes5')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes5 = '' and (p_web.GSV('Req:PartFaultCode5') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode5'',''formestimateparts_tmp:faultcode5_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes5',p_web.GetSessionValue('tmp:FaultCodes5'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode5'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(5)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode5') & '_value')

Comment::tmp:FaultCode5  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode5'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode5') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode6  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode6') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode6'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode6  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode6',p_web.GetValue('NewValue'))
    tmp:FaultCodes6 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode6
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes6',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode6')))
    tmp:FaultCodes6 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode6')) !
  End
  If tmp:FaultCodes6 = '' and p_web.GSV('Req:PartFaultCode6') = 1
    loc:Invalid = 'tmp:FaultCodes6'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode6')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes6 = Upper(tmp:FaultCodes6)
    p_web.SetSessionValue('tmp:FaultCodes6',tmp:FaultCodes6)
  do Value::tmp:FaultCode6
  do SendAlert

Value::tmp:FaultCode6  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode6') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes6
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode6') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode6') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode6') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes6')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes6 = '' and (p_web.GSV('Req:PartFaultCode6') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode6'',''formestimateparts_tmp:faultcode6_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes6',p_web.GetSessionValue('tmp:FaultCodes6'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode6'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(6)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode6') & '_value')

Comment::tmp:FaultCode6  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode6'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode6') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode7  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode7') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode7'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode7  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode7',p_web.GetValue('NewValue'))
    tmp:FaultCodes7 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode7
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes7',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode7')))
    tmp:FaultCodes7 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode7')) !
  End
  If tmp:FaultCodes7 = '' and p_web.GSV('Req:PartFaultCode7') = 1
    loc:Invalid = 'tmp:FaultCodes7'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode7')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes7 = Upper(tmp:FaultCodes7)
    p_web.SetSessionValue('tmp:FaultCodes7',tmp:FaultCodes7)
  do Value::tmp:FaultCode7
  do SendAlert

Value::tmp:FaultCode7  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode7') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes7
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode7') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode7') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode7') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes7')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes7 = '' and (p_web.GSV('Req:PartFaultCode7') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode7'',''formestimateparts_tmp:faultcode7_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes7',p_web.GetSessionValue('tmp:FaultCodes7'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode7'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(7)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode7') & '_value')

Comment::tmp:FaultCode7  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode7'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode7') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode8  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode8') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode8'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode8  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode8',p_web.GetValue('NewValue'))
    tmp:FaultCodes8 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode8
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes8',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode8')))
    tmp:FaultCodes8 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode8')) !
  End
  If tmp:FaultCodes8 = '' and p_web.GSV('Req:PartFaultCode8') = 1
    loc:Invalid = 'tmp:FaultCodes8'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode8')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes8 = Upper(tmp:FaultCodes8)
    p_web.SetSessionValue('tmp:FaultCodes8',tmp:FaultCodes8)
  do Value::tmp:FaultCode8
  do SendAlert

Value::tmp:FaultCode8  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode8') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes8
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode8') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode8') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode8') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes8')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes8 = '' and (p_web.GSV('Req:PartFaultCode8') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode8'',''formestimateparts_tmp:faultcode8_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes8',p_web.GetSessionValue('tmp:FaultCodes8'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode8'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(8)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode8') & '_value')

Comment::tmp:FaultCode8  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode8'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode8') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode9  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode9') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode9'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode9  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode9',p_web.GetValue('NewValue'))
    tmp:FaultCodes9 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode9
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes9',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode9')))
    tmp:FaultCodes9 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode9')) !
  End
  If tmp:FaultCodes9 = '' and p_web.GSV('Req:PartFaultCode9') = 1
    loc:Invalid = 'tmp:FaultCodes9'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode9')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes9 = Upper(tmp:FaultCodes9)
    p_web.SetSessionValue('tmp:FaultCodes9',tmp:FaultCodes9)
  do Value::tmp:FaultCode9
  do SendAlert

Value::tmp:FaultCode9  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode9') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes9
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode9') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode9') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode9') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes9')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes9 = '' and (p_web.GSV('Req:PartFaultCode9') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode9'',''formestimateparts_tmp:faultcode9_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes9',p_web.GetSessionValue('tmp:FaultCodes9'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode9'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(9)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode9') & '_value')

Comment::tmp:FaultCode9  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode9'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode9') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode10  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode10') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode10'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode10  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode10',p_web.GetValue('NewValue'))
    tmp:FaultCodes10 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode10
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes10',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode10')))
    tmp:FaultCodes10 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode10')) !
  End
  If tmp:FaultCodes10 = '' and p_web.GSV('Req:PartFaultCode10') = 1
    loc:Invalid = 'tmp:FaultCodes10'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode10')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes10 = Upper(tmp:FaultCodes10)
    p_web.SetSessionValue('tmp:FaultCodes10',tmp:FaultCodes10)
  do Value::tmp:FaultCode10
  do SendAlert

Value::tmp:FaultCode10  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode10') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes10
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode10') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode10') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode10') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes10')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes10 = '' and (p_web.GSV('Req:PartFaultCode10') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode10'',''formestimateparts_tmp:faultcode10_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes10',p_web.GetSessionValue('tmp:FaultCodes10'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode10'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(10)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode10') & '_value')

Comment::tmp:FaultCode10  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode10'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode10') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode11  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode11') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode11'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode11  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode11',p_web.GetValue('NewValue'))
    tmp:FaultCodes11 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode11
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes11',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode11')))
    tmp:FaultCodes11 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode11')) !
  End
  If tmp:FaultCodes11 = '' and p_web.GSV('Req:PartFaultCode11') = 1
    loc:Invalid = 'tmp:FaultCodes11'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode11')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes11 = Upper(tmp:FaultCodes11)
    p_web.SetSessionValue('tmp:FaultCodes11',tmp:FaultCodes11)
  do Value::tmp:FaultCode11
  do SendAlert

Value::tmp:FaultCode11  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode11') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes11
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode11') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode11') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode11') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes11')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes11 = '' and (p_web.GSV('Req:PartFaultCode11') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode11'',''formestimateparts_tmp:faultcode11_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes11',p_web.GetSessionValue('tmp:FaultCodes11'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode11'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(11)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode11') & '_value')

Comment::tmp:FaultCode11  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode11'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode11') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode12  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode12') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:PartFaultCode12'))
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode12  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode12',p_web.GetValue('NewValue'))
    tmp:FaultCodes12 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode12
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCodes12',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode12')))
    tmp:FaultCodes12 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:PartFaultCode12')) !
  End
  If tmp:FaultCodes12 = '' and p_web.GSV('Req:PartFaultCode12') = 1
    loc:Invalid = 'tmp:FaultCodes12'
    loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode12')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:FaultCodes12 = Upper(tmp:FaultCodes12)
    p_web.SetSessionValue('tmp:FaultCodes12',tmp:FaultCodes12)
  do Value::tmp:FaultCode12
  do SendAlert

Value::tmp:FaultCode12  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode12') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:FaultCodes12
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:PartFaultCode12') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:PartFaultCode12') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:PartFaultCode12') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCodes12')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCodes12 = '' and (p_web.GSV('Req:PartFaultCode12') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode12'',''formestimateparts_tmp:faultcode12_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCodes12',p_web.GetSessionValue('tmp:FaultCodes12'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:PartFaultCode12'),loc:javascript,,) & '<13,10>'
  do SendPacket
      local.SetLookupButton(12)
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode12') & '_value')

Comment::tmp:FaultCode12  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:PartFaultCode12'))
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('tmp:FaultCode12') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::epr:UsedOnRepair  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('epr:UsedOnRepair',p_web.GetValue('NewValue'))
    locPartUsedOnRepair = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::epr:UsedOnRepair
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPartUsedOnRepair',p_web.GetValue('Value'))
    locPartUsedOnRepair = p_web.GetValue('Value')
  End
  do Value::epr:UsedOnRepair
  do SendAlert

Value::epr:UsedOnRepair  Routine
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:UsedOnRepair') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locPartUsedOnRepair
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locPartUsedOnRepair')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:act = ChangeRecord,'disabled','')
    if p_web.GetSessionValue('locPartUsedOnRepair') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''epr:UsedOnRepair'',''formestimateparts_epr:usedonrepair_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locPartUsedOnRepair',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Used On Repair','locPartUsedOnRepair_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Use Part NOW') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:act = ChangeRecord,'disabled','')
    if p_web.GetSessionValue('locPartUsedOnRepair') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''epr:UsedOnRepair'',''formestimateparts_epr:usedonrepair_value'',1,'''&clip(2)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locPartUsedOnRepair',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Used On Repair','locPartUsedOnRepair_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Use Part When Estimate ACCEPTED') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormEstimateParts_' & p_web._nocolon('epr:UsedOnRepair') & '_value')

Comment::epr:UsedOnRepair  Routine
    loc:comment = ''
  p_web._DivHeader('FormEstimateParts_' & p_web._nocolon('epr:UsedOnRepair') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormEstimateParts_epr:Part_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:Part_Number
      else
        do Value::epr:Part_Number
      end
  of lower('FormEstimateParts_epr:Description_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:Description
      else
        do Value::epr:Description
      end
  of lower('FormEstimateParts_epr:Despatch_Note_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:Despatch_Note_Number
      else
        do Value::epr:Despatch_Note_Number
      end
  of lower('FormEstimateParts_epr:Quantity_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:Quantity
      else
        do Value::epr:Quantity
      end
  of lower('FormEstimateParts_tmp:PurchaseCost_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:PurchaseCost
      else
        do Value::tmp:PurchaseCost
      end
  of lower('FormEstimateParts_tmp:OutWarrantyCost_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:OutWarrantyCost
      else
        do Value::tmp:OutWarrantyCost
      end
  of lower('FormEstimateParts_tmp:OutWarrantyMarkup_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:OutWarrantyMarkup
      else
        do Value::tmp:OutWarrantyMarkup
      end
  of lower('FormEstimateParts_epr:Supplier_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:Supplier
      else
        do Value::epr:Supplier
      end
  of lower('FormEstimateParts_epr:Exclude_From_Order_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:Exclude_From_Order
      else
        do Value::epr:Exclude_From_Order
      end
  of lower('FormEstimateParts_epr:PartAllocated_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:PartAllocated
      else
        do Value::epr:PartAllocated
      end
  of lower('FormEstimateParts_tmp:UnallocatePart_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:UnallocatePart
      else
        do Value::tmp:UnallocatePart
      end
  of lower('FormEstimateParts_tmp:CreateOrder_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:CreateOrder
      else
        do Value::tmp:CreateOrder
      end
  of lower('FormEstimateParts_tmp:FaultCodesChecked_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCodesChecked
      else
        do Value::tmp:FaultCodesChecked
      end
  of lower('FormEstimateParts_tmp:FaultCodes1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCodes1
      else
        do Value::tmp:FaultCodes1
      end
  of lower('FormEstimateParts_tmp:FaultCode2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode2
      else
        do Value::tmp:FaultCode2
      end
  of lower('FormEstimateParts_tmp:FaultCode3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode3
      else
        do Value::tmp:FaultCode3
      end
  of lower('FormEstimateParts_tmp:FaultCode4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode4
      else
        do Value::tmp:FaultCode4
      end
  of lower('FormEstimateParts_tmp:FaultCode5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode5
      else
        do Value::tmp:FaultCode5
      end
  of lower('FormEstimateParts_tmp:FaultCode6_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode6
      else
        do Value::tmp:FaultCode6
      end
  of lower('FormEstimateParts_tmp:FaultCode7_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode7
      else
        do Value::tmp:FaultCode7
      end
  of lower('FormEstimateParts_tmp:FaultCode8_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode8
      else
        do Value::tmp:FaultCode8
      end
  of lower('FormEstimateParts_tmp:FaultCode9_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode9
      else
        do Value::tmp:FaultCode9
      end
  of lower('FormEstimateParts_tmp:FaultCode10_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode10
      else
        do Value::tmp:FaultCode10
      end
  of lower('FormEstimateParts_tmp:FaultCode11_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode11
      else
        do Value::tmp:FaultCode11
      end
  of lower('FormEstimateParts_tmp:FaultCode12_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode12
      else
        do Value::tmp:FaultCode12
      end
  of lower('FormEstimateParts_epr:UsedOnRepair_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::epr:UsedOnRepair
      else
        do Value::epr:UsedOnRepair
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormEstimateParts_form:ready_',1)
  p_web.SetSessionValue('FormEstimateParts_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormEstimateParts',0)
  epr:Ref_Number = p_web.GSV('wob:RefNumber')
  p_web.SetSessionValue('epr:Ref_Number',epr:Ref_Number)
  epr:Quantity = 1
  p_web.SetSessionValue('epr:Quantity',epr:Quantity)
  epr:Exclude_From_Order = 'NO'
  p_web.SetSessionValue('epr:Exclude_From_Order',epr:Exclude_From_Order)

PreCopy  Routine
  p_web.SetValue('FormEstimateParts_form:ready_',1)
  p_web.SetSessionValue('FormEstimateParts_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormEstimateParts',0)
  p_web._PreCopyRecord(ESTPARTS,epr:record_number_key)
  ! here we need to copy the non-unique fields across
  epr:Ref_Number = p_web.GSV('wob:RefNumber')
  p_web.SetSessionValue('epr:Ref_Number',p_web.GSV('wob:RefNumber'))
  epr:Quantity = 1
  p_web.SetSessionValue('epr:Quantity',1)
  epr:Exclude_From_Order = 'NO'
  p_web.SetSessionValue('epr:Exclude_From_Order','NO')

PreUpdate       Routine
  p_web.SetValue('FormEstimateParts_form:ready_',1)
  p_web.SetSessionValue('FormEstimateParts_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormEstimateParts:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormEstimateParts_form:ready_',1)
  p_web.SetSessionValue('FormEstimateParts_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormEstimateParts:Primed',0)
  p_web.setsessionvalue('showtab_FormEstimateParts',0)

LoadRelatedRecords  Routine
    if (p_web.ifExistsValue('adjustment'))
        p_web.storeValue('adjustment')
        !        p_web.SSV('adjustment',p_web.getValue('adjustment'))
        if (p_web.getValue('adjustment') = 1)
            epr:Part_Number = 'ADJUSTMENT'
            epr:Description = 'ADJUSTMENT'
            epr:Adjustment = 'YES'
            epr:Quantity = 1
            epr:Warranty_Part = 'NO'
            epr:Exclude_From_Order = 'YES'
      
            p_web.SSV('epr:Part_Number',epr:Part_Number)
            p_web.SSV('epr:Description',epr:Description)
            p_web.SSV('epr:Adjustment',epr:Adjustment)
            p_web.SSV('epr:Quantity',epr:Quantity)
            p_web.SSV('epr:Warranty_Part',epr:Warranty_Part)
            p_web.SSV('epr:Exclude_From_Order',epr:Exclude_From_Order)
      
        end ! if (p_web.getValue('adjustment') = 1)
    end !if (p_web.ifExistsValue('adjustment'))
      
    if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB PART COSTS - EDIT'))
        p_web.SSV('ReadOnly:OutWarrantyMarkup',1)
        p_web.SSV('ReadOnly:PurchaseCost',1)
        p_web.SSV('ReadOnly:OutWarrantyCOst',1)
    else !if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB PART COSTS - EDIT'))
        p_web.SSV('ReadOnly:OutWarrantyMarkup',0)
        p_web.SSV('ReadOnly:PurchaseCost',0)
        p_web.SSV('ReadOnly:OutWarrantyCOst',0)
    end ! if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'JOB PART COSTS - EDIT'))
      
    if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'PART ALLOCATED'))
        p_web.SSV('ReadOnly:PartAllocated',1)
    else ! if (SecurityCheckFailed(p_web.GSV('BookedUser'),'PART ALLOCATED'))
        p_web.SSV('ReadOnly:PartAllocated',0)
    end ! if (SecurityCheckFailed(p_web.GSV('BookedUser'),'PART ALLOCATED'))
      
    !      if (epr:WebOrder = 1)
    !          p_web.SSV('ReadOnly:Quantity',1)
    !      end ! if (epr:WebOrder = 1)
      
    p_web.SSV('ReadOnly:ExcludeFromOrder',0)
    IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'PARTS - EXCLUDE FROM ORDER'))
        p_web.SSV('ReadOnly:ExcludeFromOrder',1)
    END
    
    if (loc:Act = ChangeRecord)
        if (p_web.GSV('job:Date_Completed') > 0)
            if (~SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'JOBS COSTS - EDIT POST COMPLETE'))
                p_web.SSV('ReadOnly:OutWarrantyCost',1)
                p_web.SSV('ReadOnly:Quantity',1)
            end !if (SecurityCheck('JOBS COSTS - EDIT POST COMPLETE'))
        end ! if (p_web.GSV('job:Date_Completed') > 0)
      
        if (p_web.GSV('job:Estimate') = 'YES')
            Access:ESTPARTS.Clearkey(epr:part_Ref_Number_Key)
            epr:Ref_Number    = p_web.GSV('epr:Ref_Number')
            epr:Part_Ref_Number    = p_web.GSV('epr:Part_Ref_Number')
            if (Access:ESTPARTS.TryFetch(epr:part_Ref_Number_Key) = Level:Benign)
                ! Found
                ! This is same part as on the estimate
                p_web.SSV('ReadOnly:OutWarrantyCost',1)
                p_web.SSV('ReadOnly:OutWarrantyMarkup',1)
            else ! if (Access:ESTPARTS.TryFetch(epr:part_Ref_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:ESTPARTS.TryFetch(epr:part_Ref_Number_Key) = Level:Benign)
        end ! if (p_web.GSV('job:Estimate') = 'YES')
        p_web.SSV('ReadOnly:ExcludeFromOrder',1)
    end ! if (loc:Act = ChangeRecord)
      
    do enableDisableCosts
    do showCosts
    do lookupLocation
    do lookupMainFault
      
      
    if (p_web.GSV('Part:ViewOnly') <> 1)
        ! Show unallodate part tick box
      
        if (loc:Act = ChangeRecord)
            if (p_web.GSV('epr:Part_Ref_Number') > 0 And |
                p_web.GSV('epr:PartAllocated') = 1 And |
                p_web.GSV('epr:WebOrder') = 0)
                p_web.SSV('Show:UnallocatePart',1)
            end ! if (p_web.GSV('epr:Part_Ref_Number') <> '' And |
        end ! if (loc:Act = ChangeRecord)
    end ! if (p_web.GSV('Part:ViewOnly') <> 1)
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If loc:act = ChangeRecord
  End
    If (p_web.GSV('Show:UnallocatePart') = 1)
          If p_web.IfExistsValue('tmp:UnallocatePart') = 0
            p_web.SetValue('tmp:UnallocatePart',0)
            tmp:UnallocatePart = 0
          End
    End
  If p_web.GSV('locOrderRequired') = 1
          If p_web.IfExistsValue('tmp:CreateOrder') = 0
            p_web.SetValue('tmp:CreateOrder',0)
            tmp:CreateOrder = 0
          End
  End
    If (0)
      If(p_web.GSV('Hide:FaultCodesChecked') = 1)
          If p_web.IfExistsValue('tmp:FaultCodesChecked') = 0
            p_web.SetValue('tmp:FaultCodesChecked',0)
            tmp:FaultCodesChecked = 0
          End
      End
    End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
      if (p_web.GSV('locPartUsedOnRepair') = 0)
          loc:alert = 'Select When The Part Will Be Used'
          loc:invalid = 'locPartUsedOnRepair'
      END
  ! Insert Record Validation
  p_web.SSV('AddToStockAllocation:Type','')
  epr:PartAllocated = 1
  
  Access:DEFAULTS.Clearkey(def:RecordNumberKey)
  def:Record_Number    = 1
  set(def:RecordNumberKey,def:RecordNumberKey)
  loop
      if (Access:DEFAULTS.Next())
          Break
      end ! if (Access:DEFAULTS.Next())
      break
  end ! loop
  
  stockPart# = 0
  if (epr:part_Ref_Number > 0)
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number = p_web.GSV('epr:part_Ref_Number')
      if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
          ! Found
          ! Check for duplicate?
          stockPart# = 1
          found# = 0
          if (sto:AllowDuplicate = 0)
  
              Access:PARTS_ALIAS.Clearkey(par_ali:Part_Number_Key)
              par_ali:Ref_Number    = p_web.GSV('job:Ref_Number')
              par_Ali:Part_Number = p_web.GSV('epr:part_Number')
              set(par_ali:Part_Number_Key,par_ali:Part_Number_Key)
              loop
                  if (Access:PARTS_ALIAS.Next())
                      Break
                  end ! if (Access:PARTS.Next())
                  if (par_ali:Ref_Number    <> p_web.GSV('job:Ref_Number'))
                      Break
                  end ! if (epr:Ref_Number    <> p_web.GSV('job:Ref_Number'))
                  if (par_ali:part_Number <> p_web.GSV('epr:Part_Number'))
                      Break
                  end ! if (epr:Part_Number    <> p_web.GSV('epr:Part_Number'))
                  if (par_ali:Date_Received = '')
                      found# = 1
                      break
                  end ! if (epr:Date_Received = '')
              end ! loop
          end ! if (sto:AllowDuplicate = 0)
  
          if (found# = 1)
              loc:Invalid = 'epr:Part_Number'
              loc:Alert = 'This part is already attached to this job.'
              exit
          end ! if (epr:Date_Received = '')
      else ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
  else ! if (p_web.GSV('epr:Part_Ref_Number') <> '')
  end ! if (p_web.GSV('epr:Part_Ref_Number') <> '')
  
  ! update stock details
  p_web.SSV('locOrderRequired',0)
  if (p_web.GSV('epr:part_Number') <> 'ADJUSTMENT')
      if (p_web.GSV('epr:exclude_From_Order') <> 'YES')
          if (stockPart# = 1)
                  
              IF (p_web.GSV('locPartUsedOnRepair') = 1)
                  
                  p_web.SSV('epr:UsedOnRepair',1)
                  if (sto:Sundry_Item <> 'YES')
                      if (p_web.GSV('epr:Quantity') > sto:Quantity_Stock)
                          if (p_web.GSV('locOrderRequired') <> 1 and p_web.GSV('tmp:CreateOrder') = 0)
                              p_web.SSV('locOrderRequired',1)
                              loc:Invalid = 'tmp:OrderPart'
                              loc:Alert = 'Insufficient Items In Stock'
                              p_web.SSV('text:OrderRequired','Qty Required: ' & p_web.GSV('epr:Quantity') & ', Qty In Stock: ' & sto:Quantity_Stock)
                          else ! if (p_web.GSV('locOrderRequired') <> 1)
                                  
                          end ! if (p_web.GSV('locOrderRequired') <> 1)
                      else ! if (epr:Quantity > sto:Quantity_Stock)
                          p_web.SSV('epr:date_Ordered',Today())
                          if rapidLocation(sto:Location)
                              epr:PartAllocated = 0
                              p_web.SSV('AddToStockAllocation:Type','EST')
                              p_web.SSV('AddToStockAllocation:Status','')
                              p_web.SSV('AddToStockAllocation:Qty',p_web.GSV('epr:Quantity'))
                          end ! if rapidLocation(sto:Location)
  
                          sto:quantity_Stock -= epr:Quantity
                          if (sto:quantity_Stock < 0)
                              sto:quantity_Stock = 0
                          end ! if rapidLocation(sto:Location)
                          if (access:STOCK.tryUpdate() = level:Benign)
                              rtn# = AddToStockHistory(sto:Ref_Number, |
                                  'DEC', |
                                  epr:Despatch_Note_Number, |
                                  p_web.GSV('job:Ref_Number'), |
                                  0, |
                                  epr:Quantity, |
                                  p_web.GSV('tmp:InWarrantyCost'), |
                                  p_web.GSV('tmp:OutWarrantyCost'), |
                                  epr:Retail_Cost, |
                                  'STOCK DECREMENTED', |
                                  '', |
                                  p_web.GSV('BookingUserCode'), |
                                  sto:Quantity_Stock)
                          end ! if (access:STOCK.tryUpdate() = level:Benign)
                      end ! if (epr:Quantity > sto:Quantity_Stock)
                  else ! if (sto:Sundry_Item <> 'YES')
                      p_web.SSV('epr:date_Ordered',Today())
                  end ! if (sto:Sundry_Item <> 'YES')
              ELSE
                      
              end
          else ! if (stockPart# = 1)
  
          end ! if (stockPart# = 1)
      else ! if (epr:exclude_From_Order <> 'YES')
          p_web.SSV('epr:date_Ordered',Today())
      end ! if (epr:exclude_From_Order <> 'YES')
  else ! if (epr:part_Number <> 'ADJUSTMENT')
      p_web.SSV('epr:date_Ordered',Today())
  end !if (epr:part_Number <> 'ADJUSTMENT')
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord
  ! Change Record Validation

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormEstimateParts_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  ! Write Fields
      do deleteSessionValues
  
      !Write Fault Codes
      Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
      map:Manufacturer    = p_web.GSV('job:Manufacturer')
      map:ScreenOrder    = 0
      set(map:ScreenOrderKey,map:ScreenOrderKey)
      loop
          if (Access:MANFAUPA.Next())
              Break
          end ! if (Access:MANFAUPA.Next())
          if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
              Break
          end ! if (map:Manufacturer    <> p_web.GSV('job:Manufacturer'))
          if (map:ScreenOrder    = 0)
              cycle
          end ! if (map:ScreenOrder    <> 0)
  
          p_web.SSV('epr:Fault_Code' & map:Field_Number,p_web.GSV('tmp:FaultCodes' & map:ScreenOrder))
      end ! loop
  
  !    loop x# = 1 To 12
  !        p_web.SSV('epr:Fault_Code' & x#,p_web.GSV('tmp:FaultCodes' & x#))
  !        linePrint('epr:Fault_Code' & x# & ' - ' & p_web.GSV('tmp:FaultCodes' & x#),'c:\log.log')
  !    end ! loop x# = 1 To 12
  
      epr:Fault_Code1 = p_web.GSV('epr:Fault_Code1')
      epr:Fault_Code2 = p_web.GSV('epr:Fault_Code2')
      epr:Fault_Code3 = p_web.GSV('epr:Fault_Code3')
      epr:Fault_Code4 = p_web.GSV('epr:Fault_Code4')
      epr:Fault_Code5 = p_web.GSV('epr:Fault_Code5')
      epr:Fault_Code6 = p_web.GSV('epr:Fault_Code6')
      epr:Fault_Code7 = p_web.GSV('epr:Fault_Code7')
      epr:Fault_Code8 = p_web.GSV('epr:Fault_Code8')
      epr:Fault_Code9 = p_web.GSV('epr:Fault_Code9')
      epr:Fault_Code10 = p_web.GSV('epr:Fault_Code10')
      epr:Fault_Code11 = p_web.GSV('epr:Fault_Code11')
      epr:Fault_Code12 = p_web.GSV('epr:Fault_Code12')
  
      If p_web.GSV('tmp:ARCPart') = 1
          epr:AveragePurchaseCost = p_web.GSV('tmp:PurchaseCost')
          epr:Purchase_Cost       = p_web.GSV('tmp:InWarrantyCost')
          epr:Sale_Cost           = p_web.GSV('tmp:OutWarrantyCost')
          epr:InWarrantyMarkup    = p_web.GSV('tmp:InWarrantyMarkup')
          epr:OutWarrantyMarkup   = p_web.GSV('tmp:OutWarrantyMarkup')
      Else !If tmp:ARCPart
          epr:RRCAveragePurchaseCost  = p_web.GSV('tmp:PurchaseCost')
          epr:RRCPurchaseCost     = p_web.GSV('tmp:InWarrantyCost')
          epr:RRCSaleCost         = p_web.GSV('tmp:OutWarrantyCost')
          epr:RRCInWarrantyMarkup = p_web.GSV('tmp:InWarrantyMarkup')
          epr:RRCOutWarrantyMarkup   = p_web.GSV('tmp:OutWarrantyMarkup')
          epr:Purchase_Cost       = epr:RRCPurchaseCost
          epr:Sale_Cost           = epr:RRCSaleCost
      End !If tmp:ARCPart
  
  
  
  p_web.DeleteSessionValue('FormEstimateParts_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 4
  If loc:act = ChangeRecord
    loc:InvalidTab += 1
  End
  ! tab = 1
    loc:InvalidTab += 1
  If Loc:Invalid <> '' then exit.
        If epr:Part_Number = ''
          loc:Invalid = 'epr:Part_Number'
          loc:alert = p_web.translate('Part Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          epr:Part_Number = Upper(epr:Part_Number)
          p_web.SetSessionValue('epr:Part_Number',epr:Part_Number)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
        If epr:Description = ''
          loc:Invalid = 'epr:Description'
          loc:alert = p_web.translate('Description') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          epr:Description = Upper(epr:Description)
          p_web.SetSessionValue('epr:Description',epr:Description)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          epr:Despatch_Note_Number = Upper(epr:Despatch_Note_Number)
          p_web.SetSessionValue('epr:Despatch_Note_Number',epr:Despatch_Note_Number)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
        If epr:Quantity = ''
          loc:Invalid = 'epr:Quantity'
          loc:alert = p_web.translate('Quantity') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          epr:Quantity = Upper(epr:Quantity)
          p_web.SetSessionValue('epr:Quantity',epr:Quantity)
        If loc:Invalid <> '' then exit.
      if (p_web.GSV('tmp:OutWarrantyMarkup') > GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI'))
          loc:Invalid = 'tmp:OutWarrantyMarkup'
          loc:Alert = 'You cannot mark-up parts more than ' & GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI') & '%.'
      end !if p_web.GSV('tmp:OutWarrantyMarkup') > GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI')
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          epr:Supplier = Upper(epr:Supplier)
          p_web.SetSessionValue('epr:Supplier',epr:Supplier)
        If loc:Invalid <> '' then exit.
  ! tab = 3
  If p_web.GSV('locOrderRequired') = 1
    loc:InvalidTab += 1
  End
  ! tab = 2
    loc:InvalidTab += 1
    If p_web.GSV('Hide:PartFaultCode1') <> 1
        If tmp:FaultCodes1 = '' and p_web.GSV('Req:PartFautlCode1') = 1
          loc:Invalid = 'tmp:FaultCodes1'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode1')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes1 = Upper(tmp:FaultCodes1)
          p_web.SetSessionValue('tmp:FaultCodes1',tmp:FaultCodes1)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode2') <> 1
        If tmp:FaultCodes2 = '' and p_web.GSV('Req:PartFaultCode2') = 1
          loc:Invalid = 'tmp:FaultCodes2'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode2')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes2 = Upper(tmp:FaultCodes2)
          p_web.SetSessionValue('tmp:FaultCodes2',tmp:FaultCodes2)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode3') <> 1
        If tmp:FaultCodes3 = '' and p_web.GSV('Req:PartFaultCode3') = 1
          loc:Invalid = 'tmp:FaultCodes3'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode3')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes3 = Upper(tmp:FaultCodes3)
          p_web.SetSessionValue('tmp:FaultCodes3',tmp:FaultCodes3)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode4') <> 1
        If tmp:FaultCodes4 = '' and p_web.GSV('Req:PartFaultCode4') = 1
          loc:Invalid = 'tmp:FaultCodes4'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode4')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes4 = Upper(tmp:FaultCodes4)
          p_web.SetSessionValue('tmp:FaultCodes4',tmp:FaultCodes4)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode5') <> 1
        If tmp:FaultCodes5 = '' and p_web.GSV('Req:PartFaultCode5') = 1
          loc:Invalid = 'tmp:FaultCodes5'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode5')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes5 = Upper(tmp:FaultCodes5)
          p_web.SetSessionValue('tmp:FaultCodes5',tmp:FaultCodes5)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode6') <> 1
        If tmp:FaultCodes6 = '' and p_web.GSV('Req:PartFaultCode6') = 1
          loc:Invalid = 'tmp:FaultCodes6'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode6')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes6 = Upper(tmp:FaultCodes6)
          p_web.SetSessionValue('tmp:FaultCodes6',tmp:FaultCodes6)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode7') <> 1
        If tmp:FaultCodes7 = '' and p_web.GSV('Req:PartFaultCode7') = 1
          loc:Invalid = 'tmp:FaultCodes7'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode7')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes7 = Upper(tmp:FaultCodes7)
          p_web.SetSessionValue('tmp:FaultCodes7',tmp:FaultCodes7)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode8') <> 1
        If tmp:FaultCodes8 = '' and p_web.GSV('Req:PartFaultCode8') = 1
          loc:Invalid = 'tmp:FaultCodes8'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode8')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes8 = Upper(tmp:FaultCodes8)
          p_web.SetSessionValue('tmp:FaultCodes8',tmp:FaultCodes8)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode9') <> 1
        If tmp:FaultCodes9 = '' and p_web.GSV('Req:PartFaultCode9') = 1
          loc:Invalid = 'tmp:FaultCodes9'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode9')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes9 = Upper(tmp:FaultCodes9)
          p_web.SetSessionValue('tmp:FaultCodes9',tmp:FaultCodes9)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode10') <> 1
        If tmp:FaultCodes10 = '' and p_web.GSV('Req:PartFaultCode10') = 1
          loc:Invalid = 'tmp:FaultCodes10'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode10')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes10 = Upper(tmp:FaultCodes10)
          p_web.SetSessionValue('tmp:FaultCodes10',tmp:FaultCodes10)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode11') <> 1
        If tmp:FaultCodes11 = '' and p_web.GSV('Req:PartFaultCode11') = 1
          loc:Invalid = 'tmp:FaultCodes11'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode11')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes11 = Upper(tmp:FaultCodes11)
          p_web.SetSessionValue('tmp:FaultCodes11',tmp:FaultCodes11)
        If loc:Invalid <> '' then exit.
    End
    If p_web.GSV('Hide:PartFaultCode12') <> 1
        If tmp:FaultCodes12 = '' and p_web.GSV('Req:PartFaultCode12') = 1
          loc:Invalid = 'tmp:FaultCodes12'
          loc:alert = p_web.translate(p_web.GSV('Prompt:PartFaultCode12')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:FaultCodes12 = Upper(tmp:FaultCodes12)
          p_web.SetSessionValue('tmp:FaultCodes12',tmp:FaultCodes12)
        If loc:Invalid <> '' then exit.
    End
  ! tab = 6
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostInsert      Routine
  IF (p_web.GSV('AddToStockAllocation:Type') <> '')
      AddToStockAllocation(p_web.GSV('epr:Record_Number'),'EST',p_web.GSV('epr:Quantity'),p_web.GSV('AddToStockAllocation:Status'),p_web.GSV('job:Engineer'),p_web)
  END

PostCopy        Routine
  p_web.SetSessionValue('FormEstimateParts:Primed',0)

PostUpdate      Routine
  p_web.SetSessionValue('FormEstimateParts:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('tmp:Location')
  p_web.StoreValue('tmp:SecondLocation')
  p_web.StoreValue('tmp:ShelfLocation')
  p_web.StoreValue('tmp:PurchaseCost')
  p_web.StoreValue('tmp:OutWarrantyCost')
  p_web.StoreValue('tmp:OutWarrantyMarkup')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:UnallocatePart')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:CreateOrder')
  p_web.StoreValue('tmp:FaultCodesChecked')
  p_web.StoreValue('tmp:FaultCodes1')
  p_web.StoreValue('tmp:FaultCodes2')
  p_web.StoreValue('tmp:FaultCodes3')
  p_web.StoreValue('tmp:FaultCodes4')
  p_web.StoreValue('tmp:FaultCodes5')
  p_web.StoreValue('tmp:FaultCodes6')
  p_web.StoreValue('tmp:FaultCodes7')
  p_web.StoreValue('tmp:FaultCodes8')
  p_web.StoreValue('tmp:FaultCodes9')
  p_web.StoreValue('tmp:FaultCodes10')
  p_web.StoreValue('tmp:FaultCodes11')
  p_web.StoreValue('tmp:FaultCodes12')
  p_web.StoreValue('locPartUsedOnRepair')

PostDelete      Routine
local.AfterFaultCodeLookup        Procedure(Long fNumber)
code
    p_web.setsessionvalue('showtab_FormEstimateParts',Loc:TabNumber)
    if loc:LookupDone

!        Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
!        map:ScreenOrder    = fNumber
!        map:Manufacturer   = p_web.GSV('job:Manufacturer')
!        if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
!            ! Found
!        else ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
!            ! Error
!        end ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
!
!        if (map:MainFault)
!            p_web.FileToSessionQueue(MANFAULO)
!            p_web.SSV('Comment:PartFaultCode' & fNumber,mfo:Description)
!
!        else ! if (map:MainFault)
!            p_web.FileToSessionQueue(MANFPALO)
!            p_web.SSV('Comment:PartFaultCode' & fNumber,mfp:Description)
!        end ! if (map:MainFault)
        do buildFaultCodes
        do UpdateComments
    end
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCodes' & fNumber)
local.SetLookupButton      Procedure(Long fNumber)
locUseRelatedPart           String(30)
Code
    if (p_web.GSV('ShowDate:PartFaultCode' & fNumber) = 1)
        packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar(tmp__FaultCode' & fNumber & ',''dd/mm/yyyy'',this); ' & |
                  'Date.disabled=false;sv(''...'',''FormEstimateParts_pickdate_value'',1,FieldValue(this,1));nextFocus(FormEstimateParts_frm,'''',0);"' & |
                  'value="Select Date" name="Date" type="button">...</button>'
        do SendPacket
    end ! if (p_web.GSV('ShowDate:PartFaultCode1') = 1)
    if (p_web.GSV('Lookup:PartFaultCode' & fNumber) = 1)

        Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
        map:ScreenOrder    = fNumber
        map:Manufacturer    = p_web.GSV('job:Manufacturer')
        if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
            ! Found
        else ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
            ! Error
        end ! if (Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign)
        if (map:UseRelatedJobCode <> 0)
            locUseRelatedPart = 'relatedPartCode=' & map:Field_Number
        else
            locUseRelatedPart = ''
        end ! if (map:UseRelatedJobCode <> 0)

        if (map:MainFault)

            Access:MANFAULT.Clearkey(maf:MainFaultKey)
            maf:Manufacturer    = p_web.GSV('job:Manufacturer')
            maf:MainFault    = 1
            if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                ! Found
            else ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                ! Error
            end ! if (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
            packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseJobFaultCodeLookup')&|
                        '?LookupField=tmp:FaultCodes' & fNumber & '&Tab=2&ForeignField=mfo:Field&_sort=mfo:Field&Refresh=' & |
                        'sort&LookupFrom=FormEstimateParts&' & |
                        'fieldNumber=' & maf:Field_Number & '&partType=C&partMainFault=1&' & clip(locUseRelatedPart)),) !lookupextra

        else ! if (map:MainFault)
            found# = 0
            Access:STOCK.Clearkey(sto:Ref_Number_Key)
            sto:Ref_Number    = p_web.GSV('epr:Part_Ref_Number')
            if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                ! Found
                ! Check if any of the fault codes have been restricted by the Stock Part (DBH: 29/10/2007)
                If sto:Assign_Fault_Codes = 'YES'
                    Access:STOMODEL.Clearkey(stm:Model_Number_Key)
                    stm:Ref_Number = sto:Ref_Number
                    stm:Manufacturer = p_web.GSV('job:Manufacturer')
                    stm:Model_Number = p_web.GSV('job:Model_Number')
                    If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign

                        Case map:Field_Number
                        Of 1
                            If stm:FaultCode1 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 2
                            If stm:FaultCode2 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 3
                            If stm:FaultCode3 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 4
                            If stm:FaultCode4 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 5
                            If stm:FaultCode5 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 6
                            If stm:FaultCode6 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 7
                            If stm:FaultCode7 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 8
                            If stm:FaultCode8 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 9
                            If stm:FaultCode9 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 10
                            If stm:FaultCode10 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 11
                            If stm:FaultCode11 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        Of 12
                            If stm:FaultCode12 = '** MULTIPLE VALUES **'
                                Found# = 1
                            End ! If stm:FaultCode1 = '** MULTIPLE VALUES **'
                        End ! Case map:Field_Number
                        If Found# = 1
                            packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseStockPartFaultCodeLookup')&|
                                        '?LookupField=tmp:FaultCodes' & fNumber & '&Tab=2&ForeignField=stu:Field&_sort=stu:Field&Refresh=' & |
                                        'sort&LookupFrom=FormEstimateParts&' & |
                                        'fieldNumber=' & map:Field_Number & '&stockRefNumber=' & stm:RecordNumber),) !lookupextra

                        End ! If Found# = 1
                    End ! If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                End ! If sto:Assign_Fault_Codes = 'YES'
            else ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                ! Error
            end ! if (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)

            if (found# = 0)
                packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowsePartFaultCodeLookup')&|
                            '?LookupField=tmp:FaultCodes' & fNumber & '&Tab=2&ForeignField=mfp:Field&_sort=mfp:Field&Refresh=' & |
                            'sort&LookupFrom=FormEstimateParts&' & |
                            'fieldNumber=' & map:Field_Number & '&partType=C&'),) !lookupextra
            end ! if (found# = 0)
        end ! if (map:MainFault)
        do sendPacket
    end !if (p_web.GSV('Lookup:PartFaultCode1') = 1)
