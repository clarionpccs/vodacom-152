

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER223.INC'),ONCE        !Local module procedure declarations
                     END


FileDownload         PROCEDURE  (NetWebServerWorker p_web)
loc:x          Long
packet              string(NET:MaxBinData)
packetlen           long
CRLF           String('<13,10>')
NBSP           String('&#160;')

  CODE
  GlobalErrors.SetProcedureName('FileDownload')
        IF (p_web.IfExistsValue('rpq:RecordNumber'))
            Relate:REPORTQ.Open()
            Access:REPORTQ.ClearKey(rpq:RecordNumberKey)
            rpq:RecordNumber = p_web.GetValue('rpq:RecordNumber')
            IF (Access:REPORTQ.TryFetch(rpq:RecordNumberKey) = Level:Benign)
                REMOVE(CLIP(p_web.site.WebFolderPath) & '\temp\' & CLIP(rpq:FinalURL))
!                IF (ERRORCODE())
!                    STOP(ERROR())
!                END ! IF
              
                COPY(PATH() & '\downloads\' & CLIP(rpq:AccountNumber) & '\' & CLIP(rpq:FinalURL),CLIP(p_web.site.WebFolderPath) & '\temp\' & CLIP(rpq:FinalURL))
                
                p_web.SSV('ExportFileName1',rpq:ReportDescription)
                p_web.SSV('locExportFile','\temp\' & rpq:FinalURL)
                p_web.SSV('ReturnURL','') ! Clear return so that you can only close the window
            END ! IF
            Relate:REPORTQ.Close()
        END ! IF
        
  p_web.SetValue('_parentPage','FileDownload')
  If p_web.GetSessionLoggedIn() = 0
    If p_web.site.LoginPageIsControl
      p_web.MakePage(p_web.site.LoginPage)
    else
      p_web._SendFile(p_web.site.LoginPage)
    End
    Return
  End
  if p_web.sessionId = 0 then p_web.NewSession().
  do Header
  packet = clip(packet) & p_web._jsBodyOnLoad('PageBodyDownload',,'PageBodyDiv')
  do Footer
  packet = clip(packet) & p_web.Popup()
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

SendPacket  Routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,Net:NoHeader)
    packet = ''
  end
Header Routine
  packet = p_web.w3Header()
  packet = clip(packet) & '<head>'&|
      '<title>'&clip(p_web.site.PageTitle)&'</title>'&|
      '<meta http-equiv="Content-Type" content="text/html; charset='&clip(p_web.site.HtmlCharset)&'" /><13,10>'
  packet =  clip(packet) & p_web.IncludeStyles()
  packet =  clip(packet) & p_web.IncludeScripts()
    Do thePageHeader
  If (p_web.GSV('locExportFile') <> '')
    Do thePageFile1
  End
  If (p_web.GSV('locExportFile2') <> '')
    Do thePageFile2
  End
  If (p_web.GSV('locExportFile3') <> '')
    Do thePageFile3
  End
  If (p_web.GSV('locExportFile4') <> '')
    Do thePageFile4
  End
  If (p_web.GSV('ReturnURL') <> '')
    Do thePageFooter_ReturnURL
  End
  If (p_web.GSV('ReturnURL') = '')
    Do thePageFooter_NoReturnURL
  End
  packet = clip(packet) & '</head><13,10>'
  p_web.ParseHTML(packet,1,0,Net:SendHeader+Net:DontCache)
  packet = ''
Footer Routine
  packet = clip(packet) & '<!-- Net:SelectField --><13,10>' &|
                          '<script>bodyOnLoad();</script><13,10>' &|
                         '</div></body><13,10></html><13,10>'
thePageHeader  Routine
    IF (p_web.GetValue('rpq:RecordNumber') > 0)
        packet = CLIP(packet) & | 
            '<<h1>File Available To Download</h1>' & CR
    ELSE
        packet = CLIP(packet) & | 
            '<<h1>Export File Created</h1>' & CR
    END ! IF
    packet = CLIP(packet) & |
        '<<br/>' & CR & | 
        '<<h3>Click link below to download file<</h3>' & CR & |
        '<<br/>' & CR
    EXIT
  
                
  packet = clip(packet) & |
    '<<h1>Export File Created<</h1><13,10>'&|
    '<<br/><13,10>'&|
    '<<h3>Click link below to download file<</h3><13,10>'&|
    '<<br/><13,10>'&|
    ''
thePageFile1  Routine
  packet = clip(packet) & |
    '<<a href="<<!-- Net:s:locExportFile -->"><<!-- Net:s:ExportFileName1 --><</a><13,10>'&|
    '<<br/><13,10>'&|
    '<<br/><13,10>'&|
    ''
thePageFile2  Routine
  packet = clip(packet) & |
    '<<a href="<<!-- Net:s:locExportFile2 -->"><<!-- Net:s:ExportFileName2 --><</a><13,10>'&|
    '<<br/><13,10>'&|
    '<<br/><13,10>'&|
    ''
thePageFile3  Routine
  packet = clip(packet) & |
    '<<a href="<<!-- Net:s:locExportFile3 -->"><<!-- Net:s:ExportFileName3 --><</a><13,10>'&|
    '<<br/><13,10>'&|
    '<<br/><13,10>'&|
    ''
thePageFile4  Routine
  packet = clip(packet) & |
    '<<a href="<<!-- Net:s:locExportFile4 -->"><<!-- Net:s:ExportFileName4 --><</a><13,10>'&|
    '<<br/><13,10>'&|
    '<<br/><13,10>'&|
    ''
thePageFooter_ReturnURL  Routine
  packet = clip(packet) & |
    '<<br/><13,10>'&|
    '<<table width="100%"><13,10>'&|
    '<<tr><<td><13,10>'&|
    '<<button type="button" name="cancel_btn" id="cancel_btn" class="MainButtonIcon" onclick="window.location.href=''<<!-- Net:s:ReturnURL -->''"><<table class="MainButtonIconTable"><<tbody><<tr><<td class="MainButtonIconTable"><<span title="Click on this to Cancel the form"><<img src="images/psave.png" width="16px" height="16px" alt="" border="0" align="absmiddle" title="Click on this to Continue"><</span><</td><<td class="MainButtonIconTable">Continue<</td><</tr><</tbody><</table><</button><</td><</tr><</table><13,10>'&|
    ''
thePageFooter_NoReturnURL  Routine
  packet = clip(packet) & |
    '<<br/><13,10>'&|
    '<<table width="100%"><13,10>'&|
    '<<tr><<td><13,10>'&|
    '<<button type="button" name="cancel_btn" id="cancel_btn" class="MainButtonIcon" onclick="window.close();"><<table class="MainButtonIconTable"><<tbody><<tr><<td class="MainButtonIconTable"><<span title="Click on this to close the window"><<img src="images/psave.png" width="16px" height="16px" alt="" border="0" align="absmiddle" title="Click on this to close"><</span><</td><<td class="MainButtonIconTable">Close<</td><</tr><</tbody><</table><</button><13,10>'&|
    '<</td><</tr><</table><13,10>'&|
    ''
