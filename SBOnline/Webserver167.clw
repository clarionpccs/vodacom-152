

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER167.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER166.INC'),ONCE        !Req'd for module callout resolution
                     END


FormJobOutFaults     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:FreeTextOutFault BYTE                                  !
FilesOpened     Long
JOBOUTFL::State  USHORT
MANFAULO::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormJobOutFaults')
  loc:formname = 'FormJobOutFaults_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormJobOutFaults',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormJobOutFaults','')
    p_web._DivHeader('FormJobOutFaults',clip('fdiv') & ' ' & clip('FormContent'))
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormJobOutFaults',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormJobOutFaults',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormJobOutFaults',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormJobOutFaults',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If loc:act = InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormJobOutFaults',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormJobOutFaults',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormJobOutFaults',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(JOBOUTFL)
  p_web._OpenFile(MANFAULO)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBOUTFL)
  p_Web._CloseFile(MANFAULO)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormJobOutFaults_form:inited_',1)
  p_web.SetValue('UpdateFile','JOBOUTFL')
  p_web.SetValue('UpdateKey','joo:RecordNumberKey')
  p_web.SetValue('IDField','joo:RecordNumber')
  do RestoreMem

CancelForm  Routine
  IF p_web.GetSessionValue('FormJobOutFaults:Primed') = 1
    p_web._deleteFile(JOBOUTFL)
    p_web.SetSessionValue('FormJobOutFaults:Primed',0)
  End

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','JOBOUTFL')
  p_web.SetValue('UpdateKey','joo:RecordNumberKey')
  If p_web.IfExistsValue('joo:FaultCode')
    p_web.SetPicture('joo:FaultCode','@s30')
  End
  p_web.SetSessionPicture('joo:FaultCode','@s30')
  If p_web.IfExistsValue('joo:Level')
    p_web.SetPicture('joo:Level','@n8')
  End
  p_web.SetSessionPicture('joo:Level','@n8')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'joo:FaultCode'
    p_web.setsessionvalue('showtab_FormJobOutFaults',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MANFAULO)
        p_web.setsessionvalue('joo:Description',mfo:Description)
        p_web.setsessionvalue('joo:Level',mfo:ImportanceLevel)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.joo:Description')
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:FreeTextOutFault',tmp:FreeTextOutFault)

RestoreMem       Routine
  !FormSource=File
  if p_web.IfExistsValue('tmp:FreeTextOutFault')
    tmp:FreeTextOutFault = p_web.GetValue('tmp:FreeTextOutFault')
    p_web.SetSessionValue('tmp:FreeTextOutFault',tmp:FreeTextOutFault)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormJobOutFaults_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:FreeTextOutFault = p_web.RestoreValue('tmp:FreeTextOutFault')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'DisplayOutFaults'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormJobOutFaults_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormJobOutFaults_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormJobOutFaults_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'DisplayOutFaults'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  packet = clip(packet) & '<input type="hidden" name="JOBOUTFL__FileAction" value="'&p_web.getSessionValue('JOBOUTFL:FileAction')&'" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="file" value="JOBOUTFL" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateFile" value="JOBOUTFL" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateKey" value="joo:RecordNumberKey" ></input><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormJobOutFaults" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormJobOutFaults" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormJobOutFaults" ></input><13,10>'
  end

  do SendPacket
  packet = clip(packet) & p_web.CreateInput('hidden','joo:RecordNumber',p_web._jsok(p_web.getSessionValue('joo:RecordNumber'))) & '<13,10>'
  If p_web.Translate('Insert Out Fault') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Insert Out Fault',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormJobOutFaults">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormJobOutFaults" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormJobOutFaults')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('General') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormJobOutFaults')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormJobOutFaults'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='MANFAULO'
            p_web.SetValue('SelectField',clip(loc:formname) & '.joo:Description')
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FreeTextOutFault')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormJobOutFaults')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('General') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormJobOutFaults_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FreeTextOutFault
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'25%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FreeTextOutFault
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FreeTextOutFault
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::joo:FaultCode
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'25%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::joo:FaultCode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::joo:FaultCode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::joo:Description
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'25%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::joo:Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::joo:Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::joo:Level
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'25%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::joo:Level
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::joo:Level
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::tmp:FreeTextOutFault  Routine
  p_web._DivHeader('FormJobOutFaults_' & p_web._nocolon('tmp:FreeTextOutFault') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Free Text Out Fault')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FreeTextOutFault  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FreeTextOutFault',p_web.GetValue('NewValue'))
    tmp:FreeTextOutFault = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FreeTextOutFault
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:FreeTextOutFault',p_web.GetValue('Value'))
    tmp:FreeTextOutFault = p_web.GetValue('Value')
  End
  if (p_web.GSV('tmp:FreeTextOutfault') = 1)
      p_web.SSV('joo:FaultCode','0')
      p_web.SSV('joo:Level','0')
  end  !if (p_web.GSV('tmp:FreeTextOutfault') = 1)
  do Value::tmp:FreeTextOutFault
  do SendAlert
  do Value::joo:Description  !1
  do Prompt::joo:FaultCode
  do Value::joo:FaultCode  !1
  do Comment::joo:FaultCode
  do Prompt::joo:Level
  do Value::joo:Level  !1
  do Comment::joo:Level

Value::tmp:FreeTextOutFault  Routine
  p_web._DivHeader('FormJobOutFaults_' & p_web._nocolon('tmp:FreeTextOutFault') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- tmp:FreeTextOutFault
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:FreeTextOutFault'',''formjoboutfaults_tmp:freetextoutfault_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:FreeTextOutFault')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('tmp:FreeTextOutFault') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:FreeTextOutFault',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobOutFaults_' & p_web._nocolon('tmp:FreeTextOutFault') & '_value')

Comment::tmp:FreeTextOutFault  Routine
    loc:comment = ''
  p_web._DivHeader('FormJobOutFaults_' & p_web._nocolon('tmp:FreeTextOutFault') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::joo:FaultCode  Routine
  p_web._DivHeader('FormJobOutFaults_' & p_web._nocolon('joo:FaultCode') & '_prompt',Choose(p_web.GSV('tmp:FreeTextOutFault') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Fault Code')
  If p_web.GSV('tmp:FreeTextOutFault') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobOutFaults_' & p_web._nocolon('joo:FaultCode') & '_prompt')

Validate::joo:FaultCode  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('joo:FaultCode',p_web.GetValue('NewValue'))
    joo:FaultCode = p_web.GetValue('NewValue') !FieldType= STRING Field = joo:FaultCode
    do Value::joo:FaultCode
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('joo:FaultCode',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    joo:FaultCode = p_web.GetValue('Value')
  End
  If joo:FaultCode = ''
    loc:Invalid = 'joo:FaultCode'
    loc:alert = p_web.translate('Fault Code') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    joo:FaultCode = Upper(joo:FaultCode)
    p_web.SetSessionValue('joo:FaultCode',joo:FaultCode)
  p_Web.SetValue('lookupfield','joo:FaultCode')
  do AfterLookup
  do Value::joo:Description
  do Value::joo:Level
  do Value::joo:FaultCode
  do SendAlert
  do Comment::joo:FaultCode

Value::joo:FaultCode  Routine
  p_web._DivHeader('FormJobOutFaults_' & p_web._nocolon('joo:FaultCode') & '_value',Choose(p_web.GSV('tmp:FreeTextOutFault') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:FreeTextOutFault') = 1)
  ! --- STRING --- joo:FaultCode
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('joo:FaultCode')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If joo:FaultCode = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''joo:FaultCode'',''formjoboutfaults_joo:faultcode_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('joo:FaultCode')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','joo:FaultCode',p_web.GetSessionValue('joo:FaultCode'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),'Fault Code') & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseOutFaultCodes')&'?LookupField=joo:FaultCode&Tab=0&ForeignField=mfo:Field&_sort=mfo:Field&Refresh=sort&LookupFrom=FormJobOutFaults&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobOutFaults_' & p_web._nocolon('joo:FaultCode') & '_value')

Comment::joo:FaultCode  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormJobOutFaults_' & p_web._nocolon('joo:FaultCode') & '_comment',Choose(p_web.GSV('tmp:FreeTextOutFault') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:FreeTextOutFault') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobOutFaults_' & p_web._nocolon('joo:FaultCode') & '_comment')

Prompt::joo:Description  Routine
  p_web._DivHeader('FormJobOutFaults_' & p_web._nocolon('joo:Description') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Description')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobOutFaults_' & p_web._nocolon('joo:Description') & '_prompt')

Validate::joo:Description  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('joo:Description',p_web.GetValue('NewValue'))
    joo:Description = p_web.GetValue('NewValue') !FieldType= STRING Field = joo:Description
    do Value::joo:Description
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('joo:Description',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    joo:Description = p_web.GetValue('Value')
  End
  If joo:Description = ''
    loc:Invalid = 'joo:Description'
    loc:alert = p_web.translate('Description') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    joo:Description = Upper(joo:Description)
    p_web.SetSessionValue('joo:Description',joo:Description)
  do Value::joo:Description
  do SendAlert

Value::joo:Description  Routine
  p_web._DivHeader('FormJobOutFaults_' & p_web._nocolon('joo:Description') & '_value','adiv')
  loc:extra = ''
  ! --- TEXT --- joo:Description
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('tmp:FreeTextOutFault') = 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('joo:Description')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If joo:Description = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''joo:Description'',''formjoboutfaults_joo:description_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('tmp:FreeTextOutFault') = 0,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('joo:Description',p_web.GetSessionValue('joo:Description'),3,40,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,255,,Net:Web:Control) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobOutFaults_' & p_web._nocolon('joo:Description') & '_value')

Comment::joo:Description  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormJobOutFaults_' & p_web._nocolon('joo:Description') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobOutFaults_' & p_web._nocolon('joo:Description') & '_comment')

Prompt::joo:Level  Routine
  p_web._DivHeader('FormJobOutFaults_' & p_web._nocolon('joo:Level') & '_prompt',Choose(p_web.GSV('tmp:FreeTextOutFault') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Level')
  If p_web.GSV('tmp:FreeTextOutFault') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobOutFaults_' & p_web._nocolon('joo:Level') & '_prompt')

Validate::joo:Level  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('joo:Level',p_web.GetValue('NewValue'))
    joo:Level = p_web.GetValue('NewValue') !FieldType= LONG Field = joo:Level
    do Value::joo:Level
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('joo:Level',p_web.dFormat(p_web.GetValue('Value'),'@n8'))
    joo:Level = p_web.GetValue('Value')
  End
  do Value::joo:Level
  do SendAlert

Value::joo:Level  Routine
  p_web._DivHeader('FormJobOutFaults_' & p_web._nocolon('joo:Level') & '_value',Choose(p_web.GSV('tmp:FreeTextOutFault') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:FreeTextOutFault') = 1)
  ! --- STRING --- joo:Level
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('joo:Level')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''joo:Level'',''formjoboutfaults_joo:level_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','joo:Level',p_web.GetSessionValueFormat('joo:Level'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@n8'),'Level') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobOutFaults_' & p_web._nocolon('joo:Level') & '_value')

Comment::joo:Level  Routine
      loc:comment = ''
  p_web._DivHeader('FormJobOutFaults_' & p_web._nocolon('joo:Level') & '_comment',Choose(p_web.GSV('tmp:FreeTextOutFault') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:FreeTextOutFault') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormJobOutFaults_' & p_web._nocolon('joo:Level') & '_comment')

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormJobOutFaults_tmp:FreeTextOutFault_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FreeTextOutFault
      else
        do Value::tmp:FreeTextOutFault
      end
  of lower('FormJobOutFaults_joo:FaultCode_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::joo:FaultCode
      else
        do Value::joo:FaultCode
      end
  of lower('FormJobOutFaults_joo:Description_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::joo:Description
      else
        do Value::joo:Description
      end
  of lower('FormJobOutFaults_joo:Level_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::joo:Level
      else
        do Value::joo:Level
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormJobOutFaults_form:ready_',1)
  p_web.SetSessionValue('FormJobOutFaults_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormJobOutFaults',0)
  joo:JobNumber = p_web.GSV('wob:RefNumber')
  p_web.SetSessionValue('joo:JobNumber',joo:JobNumber)

PreCopy  Routine
  p_web.SetValue('FormJobOutFaults_form:ready_',1)
  p_web.SetSessionValue('FormJobOutFaults_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormJobOutFaults',0)
  p_web._PreCopyRecord(JOBOUTFL,joo:RecordNumberKey)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormJobOutFaults_form:ready_',1)
  p_web.SetSessionValue('FormJobOutFaults_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormJobOutFaults:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormJobOutFaults_form:ready_',1)
  p_web.SetSessionValue('FormJobOutFaults_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormJobOutFaults:Primed',0)
  p_web.setsessionvalue('showtab_FormJobOutFaults',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
          If p_web.IfExistsValue('tmp:FreeTextOutFault') = 0
            p_web.SetValue('tmp:FreeTextOutFault',0)
            tmp:FreeTextOutFault = 0
          End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormJobOutFaults_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormJobOutFaults_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  If Loc:Invalid <> '' then exit.
      If not (p_web.GSV('tmp:FreeTextOutFault') = 1)
        If joo:FaultCode = ''
          loc:Invalid = 'joo:FaultCode'
          loc:alert = p_web.translate('Fault Code') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          joo:FaultCode = Upper(joo:FaultCode)
          p_web.SetSessionValue('joo:FaultCode',joo:FaultCode)
        If loc:Invalid <> '' then exit.
      End
  If Loc:Invalid <> '' then exit.
        If joo:Description = ''
          loc:Invalid = 'joo:Description'
          loc:alert = p_web.translate('Description') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          joo:Description = Upper(joo:Description)
          p_web.SetSessionValue('joo:Description',joo:Description)
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostInsert      Routine

PostCopy        Routine
  p_web.SetSessionValue('FormJobOutFaults:Primed',0)

PostUpdate      Routine
  p_web.SetSessionValue('FormJobOutFaults:Primed',0)
  p_web.StoreValue('tmp:FreeTextOutFault')

PostDelete      Routine
