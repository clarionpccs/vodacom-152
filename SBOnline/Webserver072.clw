

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER072.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! Check for saved entry. Return: 0 (Not Found), 1 (Found), 2 (Found but different Type)
!!! </summary>
DuplicateTabCheck    PROCEDURE  (NetwebServerWorker p_web,STRING pType,STRING pValue)!,LONG ! Declare Procedure
RetValue                    LONG
FilesOpened     BYTE(0)

  CODE
!region ProcessedCode
        DO OpenFiles
        
        Access:SBO_DupCheck.ClearKey(sbodup:ValueKey)
        sbodup:SessionID = p_web.SessionID
        sbodup:DupType = pType
        SET(sbodup:ValueKey,sbodup:ValueKey)
        LOOP UNTIL Access:SBO_DupCheck.Next() <> Level:Benign
            IF (sbodup:SessionID <> p_web.SessionID OR |
                sbodup:DupType <> pType)
                BREAK
            END ! IF
    
            IF (sbodup:ValueField = pValue)
                RetValue = 1
                BREAK
            END  ! ID
    
            RetValue = 2
    
        END ! LOOP
        
        DO CloseFiles
        
        RETURN RetValue
!endregion        
!--------------------------------------
OpenFiles  ROUTINE
  Access:SBO_DupCheck.Open                                 ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SBO_DupCheck.UseFile                              ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:SBO_DupCheck.Close
     FilesOpened = False
  END
