

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER442.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER206.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER434.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER443.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER450.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER451.INC'),ONCE        !Req'd for module callout resolution
                     END


MenuStockControl     PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:MenuStockControl -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
  CODE
  GlobalErrors.SetProcedureName('MenuStockControl')
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
!----------- put your html code here -----------------------------------
  do WebMenus
  do SendPacket
!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end

WebMenus  Routine
  packet = clip(packet) & |
    '<div id="'&clip('ChromeMenu')&'" class="'&clip('chromemenu')&'"><13,10><ul><13,10>'
        packet = clip(packet) & '<li><a 0 onMouseover="cssdropdown.dropit(this,event,''dropmenu1'')">'&p_web.Translate('Stock Control Options')&'</a></li><13,10>'
  packet = clip(packet) & '</ul><13,10></div><13,10>'
        do Menu:1

Menu:1  Routine
  packet = clip(packet) & '<div id="dropmenu1" class="'&clip('DropMenu')&'"><13,10>'
        packet = clip(packet) & '<a href="'&p_web._MakeUrl(clip('BrowseAdjustedWebOrderReceipts'))&'">'&p_web.Translate('Adjusted Web Order Receipts')&'</a><13,10>'
        packet = clip(packet) & '<a href="'&p_web._MakeUrl(clip('BrowseRetailSales'))&'">'&p_web.Translate('Browse Retail Sales')&'</a><13,10>'
        packet = clip(packet) & '<a href="'&p_web._MakeUrl(clip('frmPendingWebOrder'))&'">'&p_web.Translate('Pending Orders')&'</a><13,10>'
        packet = clip(packet) & '<a href="'&p_web._MakeUrl(clip('FormStockReceive?firstime=1'))&'">'&p_web.Translate('Stock Receive Procedure')&'</a><13,10>'
        packet = clip(packet) & '<a href="'&p_web._MakeUrl(clip('PageProcess&ProcessType=ExchangeAllocation&ReturnURL=FormBrowseStock&RedirectURL=brwRapidExchangeAllocation'))&'">'&p_web.Translate('Exchange Allocation')&'</a><13,10>'
        packet = clip(packet) & '<a href="'&p_web._MakeUrl(clip('FormStockAllocation'))&'">'&p_web.Translate('Stock Allocation')&'</a><13,10>'
  packet = clip(packet) & '</div><13,10>'
  do SendPacket

