

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER202.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! Replacement For SendToHub. Requires JOBS and JOBSE in the Q
!!! Replacement For SendToHub
!!! 
!!! Required JOBS And JOBSE to be in the Queue
!!! </summary>
JobSentToARC         PROCEDURE  (<Netwebserverworker p_web>) ! Declare Procedure
retValue                    LONG(0)
jobRecord                   LIKE(job:Record)
jobeRecord                  LIKE(jobe:Record)
ARCLocation                 STRING(30)
FilesOpened     BYTE(0)

  CODE
        ! Requires JOBSE To Be Loaded
        IF (p_web &= NULL)
            jobeRecord.HubRepair = jobe:HubRepair
            jobeRecord.HubRepairDate = jobe:HubRepairDate
            jobeRecord.RefNumber = jobe:RefNumber
            ARCLocation = GETINI('RRC','ARCLocation','RECEIVED AT ARC',CLIP(PATH())&'\SB2KDEF.INI')
        ELSE
            jobeRecord.HubRepair = p_web.GSV('jobe:HubRepair')
            jobeRecord.HubRepairDate = p_web.GSV('jobe:HubRepairDate')
            jobeRecord.RefNumber = p_web.GSV('jobe:RefNumber')
            ARCLocation = p_web.GSV('Default:ARCLocation')
        END ! IF
        
        IF (jobeRecord.HubRepair = 1 OR jobeRecord.HubRepairDate > 0)
            retValue = 1
        ELSE
            DO OpenFiles
            Access:LOCATLOG.ClearKey(lot:NewLocationKey)
            lot:RefNumber   = jobeRecord.RefNumber
            lot:NewLocation = ARCLocation
            If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
                retValue = 1
            End !If Access:LOCATLOG.TryFetch(lot:NewLocationKey)
            DO CloseFiles
        END ! IF
        
    RETURN retValue
!--------------------------------------
OpenFiles  ROUTINE
  Access:LOCATLOG.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOCATLOG.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:LOCATLOG.Close
     FilesOpened = False
  END
