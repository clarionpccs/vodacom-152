

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER020.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
UseStockAllocation   PROCEDURE  (fLocation)                ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
    do openFiles

    retValue# = 0

    Access:LOCATION.Clearkey(loc:location_Key)
    loc:location    = fLocation
    if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)
        ! Found
        if (loc:UseRapidStock)
            retValue# = 1
        end !if (loc:useRapidStock)
    else ! if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)
        ! Error
    end ! if (Access:LOCATION.TryFetch(loc:location_Key) = Level:Benign)

    do closeFiles

    return retValue#
!--------------------------------------
OpenFiles  ROUTINE
  Access:LOCATION.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOCATION.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:LOCATION.Close
     FilesOpened = False
  END
