

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER394.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER120.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER393.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseEngineersPerformance PROCEDURE  (NetWebServerWorker p_web)
locEngineerName      STRING(61)                            !
DataGroup            GROUP,PRE()                           !
locAllocated         LONG                                  !
locEscalated         LONG                                  !
locEngineerQA        LONG                                  !
locHub               LONG                                  !
locRepair            LONG                                  !
locModification      LONG                                  !
locExchange          LONG                                  !
locLiquidDamage      LONG                                  !
locNFF               LONG                                  !
locBER               LONG                                  !
locRTM               LONG                                  !
locRNR               LONG                                  !
locTotalPoints       LONG                                  !
locAchieved          REAL                                  !
                     END                                   !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(USERS)
                      Project(use:User_Code)
                      Project(use:Repair_Target)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
JOBS::State  USHORT
REPTYDEF::State  USHORT
JOBSENG::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseEngineersPerformance')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseEngineersPerformance:NoForm')
      loc:NoForm = p_web.GetValue('BrowseEngineersPerformance:NoForm')
      loc:FormName = p_web.GetValue('BrowseEngineersPerformance:FormName')
    else
      loc:FormName = 'BrowseEngineersPerformance_frm'
    End
    p_web.SSV('BrowseEngineersPerformance:NoForm',loc:NoForm)
    p_web.SSV('BrowseEngineersPerformance:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseEngineersPerformance:NoForm')
    loc:FormName = p_web.GSV('BrowseEngineersPerformance:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseEngineersPerformance') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseEngineersPerformance')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(USERS,use:User_Code_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'LOCENGINEERNAME') then p_web.SetValue('BrowseEngineersPerformance_sort','1')
    ElsIf (loc:vorder = 'LOCENGINEERQA') then p_web.SetValue('BrowseEngineersPerformance_sort','4')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseEngineersPerformance:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseEngineersPerformance:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseEngineersPerformance:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseEngineersPerformance:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseEngineersPerformance:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:NoSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  p_web.SSV('locHubPoints',GETINI('ENGINEERSTATUS','HubPoints',,CLIP(PATH())&'\SB2KDEF.INI'))
  p_web.SSV('locEscalatedPoints',GETINI('ENGINEERSTATUS','EscalatedPoints',,CLIP(PATH())&'\SB2KDEF.INI'))
  p_web.SSV('locEngineerQAPoints',GETINI('ENGINEERSTATUS','EngineerQAPoints',,CLIP(PATH())&'\SB2KDEF.INI'))
  p_web.SSV('locExchangePoints',GETINI('ENGINEERSTATUS','ExchangePoints',,CLIP(PATH())&'\SB2KDEF.INI'))
  
  ! Save Window Name
  AddToLog('NetWebBrowse',p_web.RequestData.DataString,'BrowseEngineersPerformance',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseEngineersPerformance_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseEngineersPerformance_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'locEngineerName','-locEngineerName')
    Loc:LocateField = 'locEngineerName'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'locEngineerQA','-locEngineerQA')
    Loc:LocateField = 'locEngineerQA'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('locEngineerName')
    loc:SortHeader = p_web.Translate('Engineer Name')
    p_web.SetSessionValue('BrowseEngineersPerformance_LocatorPic','@s60')
  Of upper('locAllocated')
    loc:SortHeader = p_web.Translate('Allocated')
    p_web.SetSessionValue('BrowseEngineersPerformance_LocatorPic','@n_8')
  Of upper('locEscalated')
    loc:SortHeader = p_web.Translate('Escalated')
    p_web.SetSessionValue('BrowseEngineersPerformance_LocatorPic','@n_8')
  Of upper('locEngineerQA')
    loc:SortHeader = p_web.Translate('Eng QA')
    p_web.SetSessionValue('BrowseEngineersPerformance_LocatorPic','@n_8')
  Of upper('locHub')
    loc:SortHeader = p_web.Translate('Hub')
    p_web.SetSessionValue('BrowseEngineersPerformance_LocatorPic','@n_8')
  Of upper('locRepair')
    loc:SortHeader = p_web.Translate('Repair')
    p_web.SetSessionValue('BrowseEngineersPerformance_LocatorPic','@n_8')
  Of upper('locModification')
    loc:SortHeader = p_web.Translate('Modification')
    p_web.SetSessionValue('BrowseEngineersPerformance_LocatorPic','@n_8')
  Of upper('locExchange')
    loc:SortHeader = p_web.Translate('Exchange')
    p_web.SetSessionValue('BrowseEngineersPerformance_LocatorPic','@n_8')
  Of upper('locLiquidDamage')
    loc:SortHeader = p_web.Translate('Liquid Damage')
    p_web.SetSessionValue('BrowseEngineersPerformance_LocatorPic','@n_8')
  Of upper('locNFF')
    loc:SortHeader = p_web.Translate('NFF')
    p_web.SetSessionValue('BrowseEngineersPerformance_LocatorPic','@n_8')
  Of upper('locBER')
    loc:SortHeader = p_web.Translate('BER')
    p_web.SetSessionValue('BrowseEngineersPerformance_LocatorPic','@n_8')
  Of upper('locRTM')
    loc:SortHeader = p_web.Translate('RTM')
    p_web.SetSessionValue('BrowseEngineersPerformance_LocatorPic','@n_8')
  Of upper('locRNR')
    loc:SortHeader = p_web.Translate('RNR')
    p_web.SetSessionValue('BrowseEngineersPerformance_LocatorPic','@n_8')
  Of upper('locTotalPoints')
    loc:SortHeader = p_web.Translate('Total Points')
    p_web.SetSessionValue('BrowseEngineersPerformance_LocatorPic','@n_8')
  Of upper('use:Repair_Target')
    loc:SortHeader = p_web.Translate('Repair Target')
    p_web.SetSessionValue('BrowseEngineersPerformance_LocatorPic','@n_8')
  Of upper('locAchieved')
    loc:SortHeader = p_web.Translate('Achieved (%)')
    p_web.SetSessionValue('BrowseEngineersPerformance_LocatorPic','@n_8')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseEngineersPerformance:LookupFrom')
  End!Else
  loc:CloseAction = 'IndexPage'
  loc:formaction = 'BrowseEngineersPerformance'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseEngineersPerformance:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseEngineersPerformance:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseEngineersPerformance:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="USERS"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="use:User_Code_Key"></input><13,10>'
  end
  If p_web.Translate('Engineer Status') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Engineer Status',0)&'</span>'&CRLF
  End
    do SendPacket
  If (DoesUserHaveAccess(p_web,'ENGINEER STATUS DEFAULTS'))
    Do defaultLink
    do SendPacket
  End
  If clip('Engineer Status') <> ''
    !packet = clip(packet) & p_web.br !Bryan. Why is this here?
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseEngineersPerformance',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseEngineersPerformance',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseEngineersPerformance.locate(''Locator2BrowseEngineersPerformance'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseEngineersPerformance.cl(''BrowseEngineersPerformance'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseEngineersPerformance_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseEngineersPerformance_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseEngineersPerformance','Engineer Name',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Engineer Name')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th class="'&clip('red')&'">'&p_web.Translate('Allocated')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&p_web.Translate('Escalated')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowseEngineersPerformance','Eng QA',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Eng QA')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&p_web.Translate('Hub')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&p_web.Translate('Repair')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&p_web.Translate('Modification')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&p_web.Translate('Exchange')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&p_web.Translate('Liquid Damage')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&p_web.Translate('NFF')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&p_web.Translate('BER')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&p_web.Translate('RTM')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&p_web.Translate('RNR')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&p_web.Translate('Total Points')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&p_web.Translate('Repair Target')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&p_web.Translate('Achieved (%)')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('use:user_code',lower(Thisview{prop:order}),1,1) = 0 !and USERS{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'use:User_Code'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('use:User_Code'),p_web.GetValue('use:User_Code'),p_web.GetSessionValue('use:User_Code'))
      loc:FilterWas = 'use:IncludeInEngStatus = 1 AND UPPER(use:Team) = UPPER(''' & p_web.GSV('BookingSiteLocation') & ''') AND UPPER(use:User_Type) = ''ENGINEER''' 
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseEngineersPerformance',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseEngineersPerformance_Filter')
    p_web.SetSessionValue('BrowseEngineersPerformance_FirstValue','')
    p_web.SetSessionValue('BrowseEngineersPerformance_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,USERS,use:User_Code_Key,loc:PageRows,'BrowseEngineersPerformance',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If USERS{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(USERS,loc:firstvalue)
              Reset(ThisView,USERS)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If USERS{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(USERS,loc:lastvalue)
            Reset(ThisView,USERS)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(use:User_Code)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseEngineersPerformance.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseEngineersPerformance.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseEngineersPerformance.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseEngineersPerformance.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseEngineersPerformance',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseEngineersPerformance_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseEngineersPerformance_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseEngineersPerformance',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseEngineersPerformance.locate(''Locator1BrowseEngineersPerformance'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseEngineersPerformance.cl(''BrowseEngineersPerformance'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseEngineersPerformance_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseEngineersPerformance_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseEngineersPerformance.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseEngineersPerformance.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseEngineersPerformance.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseEngineersPerformance.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If loc:selecting = 0 and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:Formname,loc:CloseAction)
      do SendPacket
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    locEngineerName = CLIP(use:Surname) & ', ' & CLIP(use:Forename) 
    CLEAR(DataGroup)
    Access:JOBSENG.ClearKey(joe:UserCodeStatusKey)
    joe:UserCode = use:User_Code
    joe:StatusDate = TODAY()
    SET(joe:UserCodeStatusKey,joe:UserCodeStatusKey)
    LOOP UNTIL Access:JOBSENG.Next() <> Level:Benign
        IF (joe:UserCode <> use:User_Code OR |
            joe:StatusDate <> TODAY())
            BREAK
        END ! IF
    	
        CASE UPPER(joe:Status)
        OF 'ALLOCATED' !
            locAllocated += 1
        OF 'HUB'
    		! Check to see if there is an exchange unit attached
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = joe:JobNumber
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
            ELSE ! IF
            END ! IF
            IF (job:Exchange_Unit_Number > 0)
                locExchange += p_web.GSV('locExchangePoints')
            ELSE
                locHub += p_web.GSV('locHubPoints')
            END ! IF
        OF 'ESCALATED'
            locEscalated += p_web.GSV('locEscalatedPoints')
        OF 'ENGINEER QA'
            locEngineerQA += p_web.GSV('locEngineerQAPoints')
        OF 'COMPLETED'
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = joe:JobNumber
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
            ELSE ! IF
            END ! IF	
            IF (job:Warranty_Job = 'YES')
                IF (job:Repair_Type_Warranty <> '')
                    Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
                    rtd:Manufacturer = job:Manufacturer
                    rtd:Warranty = 'YES'
                    rtd:Repair_Type = job:Repair_Type_Warranty
                    IF (Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign)
                        Do WorkOutWeighting
                    ELSE ! IF
                    END ! IF
                END ! IF
            END ! IF
            IF (job:Chargeable_Job = 'YES')
                IF (job:Repair_Type <> '')
                    Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
                    rtd:Manufacturer = job:Manufacturer
                    rtd:Chargeable = 'YES'
                    rtd:Repair_Type = job:Repair_Type
                    IF (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
                        Do WorkOutWeighting
                    ELSE ! IF
                    END ! IF
                END ! IF
            END ! IF
        END ! CASE
    END ! LOOP
    loc:field = use:User_Code
    p_web._thisrow = p_web._nocolon('use:User_Code')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseEngineersPerformance:LookupField')) = use:User_Code and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((use:User_Code = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseEngineersPerformance.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If USERS{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(USERS)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If USERS{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(USERS)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','use:User_Code',clip(loc:field),,loc:checked,,,'onclick="BrowseEngineersPerformance.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','use:User_Code',clip(loc:field),,'checked',,,'onclick="BrowseEngineersPerformance.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locEngineerName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="'&clip('red')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locAllocated
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locEscalated
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locEngineerQA
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locHub
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locRepair
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locModification
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locExchange
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locLiquidDamage
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locNFF
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locBER
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locRTM
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locRNR
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locTotalPoints
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::use:Repair_Target
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locAcheived
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseEngineersPerformance.omv(this);" onMouseOut="BrowseEngineersPerformance.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseEngineersPerformance=new browseTable(''BrowseEngineersPerformance'','''&clip(loc:formname)&''','''&p_web._jsok('use:User_Code',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('use:User_Code')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseEngineersPerformance.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseEngineersPerformance.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseEngineersPerformance')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseEngineersPerformance')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseEngineersPerformance')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseEngineersPerformance')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(USERS)
  p_web._CloseFile(JOBS)
  p_web._CloseFile(REPTYDEF)
  p_web._CloseFile(JOBSENG)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(USERS)
  Bind(use:Record)
  Clear(use:Record)
  NetWebSetSessionPics(p_web,USERS)
  p_web._OpenFile(JOBS)
  Bind(job:Record)
  NetWebSetSessionPics(p_web,JOBS)
  p_web._OpenFile(REPTYDEF)
  Bind(rtd:Record)
  NetWebSetSessionPics(p_web,REPTYDEF)
  p_web._OpenFile(JOBSENG)
  Bind(joe:Record)
  NetWebSetSessionPics(p_web,JOBSENG)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('use:User_Code',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
      ! Save Window Name
      IF (loc:alert <> '')
          AddToLog('Alert',loc:alert,'BrowseEngineersPerformance',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
      END ! IF
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::locEngineerName   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locEngineerName_'&use:User_Code,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locEngineerName,'@s60')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locAllocated   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locAllocated_'&use:User_Code,'red',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locAllocated,'@n_8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locEscalated   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locEscalated_'&use:User_Code,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locEscalated,'@n_8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locEngineerQA   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locEngineerQA_'&use:User_Code,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locEngineerQA,'@n_8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locHub   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locHub_'&use:User_Code,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locHub,'@n_8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locRepair   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locRepair_'&use:User_Code,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locRepair,'@n_8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locModification   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locModification_'&use:User_Code,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locModification,'@n_8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locExchange   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locExchange_'&use:User_Code,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locExchange,'@n_8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locLiquidDamage   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locLiquidDamage_'&use:User_Code,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locLiquidDamage,'@n_8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locNFF   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locNFF_'&use:User_Code,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locNFF,'@n_8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locBER   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locBER_'&use:User_Code,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locBER,'@n_8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locRTM   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locRTM_'&use:User_Code,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locRTM,'@n_8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locRNR   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locRNR_'&use:User_Code,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locRNR,'@n_8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locTotalPoints   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locTotalPoints_'&use:User_Code,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locTotalPoints,'@n_8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::use:Repair_Target   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('use:Repair_Target_'&use:User_Code,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(use:Repair_Target,'@n_8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locAcheived   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locAcheived_'&use:User_Code,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locAchieved,'@n_8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(JOBS)
  p_web._OpenFile(REPTYDEF)
  p_web._OpenFile(JOBSENG)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(JOBSENG)
     FilesOpened = False
  END
  return
WorkOutWeighting        Routine
    !Found
    Case rtd:BER
        Of 0 !Repair
            locRepair += rtd:JobWeighting
        Of 1 !BER
            locBER    += rtd:JobWeighting
        Of 2 !Mod
            locModification     += rtd:JobWeighting
        !This status doesn't really apply.
        !Will use whether a job has an exchange unit attached instead - L832 (DBH: 16-07-2003)
!        Of 3 !Exc
!            tmp:Exchange    += rtd:JobWeighting
        Of 4 !Liq
            locLiquidDamage    += rtd:JobWeighting
        Of 5 !NFF
            locNFF += rtd:JobWeighting
        Of 6 !RTM
            locRTM += rtd:JobWeighting
        !of 7 not needed - this is "Excluded"
        of 8 !RNR
            locRNR += rtd:JobWeighting
    End !Case rtd:BER
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(use:User_Code_Key)
    loc:Invalid = 'use:User_Code'
    loc:Alert = clip(p_web.site.DuplicateText) & ' User_Code_Key --> use:User_Code = ' & clip(use:User_Code)
  End
  If Duplicate(use:password_key)
    loc:Invalid = 'use:Password'
    loc:Alert = clip(p_web.site.DuplicateText) & ' password_key --> use:Password = ' & clip(use:Password)
  End
PushDefaultSelection  Routine
  loc:default = use:User_Code

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('use:User_Code',use:User_Code)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('use:User_Code',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('use:User_Code'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('use:User_Code'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
defaultLink  Routine
  packet = clip(packet) & |
    '<<a href="FormEngineersPerformanceDefaults">Defaults<</a><13,10>'&|
    ''
