

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER404.INC'),ONCE        !Local module procedure declarations
                     END


FormRRCSubAddresses  PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
FilesOpened     Long
SUBURB::State  USHORT
SUBACCAD::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormRRCSubAddresses')
  loc:formname = 'FormRRCSubAddresses_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormRRCSubAddresses',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormRRCSubAddresses','')
    p_web._DivHeader('FormRRCSubAddresses',clip('fdiv') & ' ' & clip('FormContent'))
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormRRCSubAddresses',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormRRCSubAddresses',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormRRCSubAddresses',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormRRCSubAddresses',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If loc:act = InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormRRCSubAddresses',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormRRCSubAddresses',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormRRCSubAddresses',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(SUBURB)
  p_web._OpenFile(SUBACCAD)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(SUBURB)
  p_Web._CloseFile(SUBACCAD)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormRRCSubAddresses_form:inited_',1)
  p_web.SetValue('UpdateFile','SUBACCAD')
  p_web.SetValue('UpdateKey','sua:RecordNumberKey')
  p_web.SetValue('IDField','sua:RecordNumber')
  do RestoreMem

CancelForm  Routine
  IF p_web.GetSessionValue('FormRRCSubAddresses:Primed') = 1
    p_web._deleteFile(SUBACCAD)
    p_web.SetSessionValue('FormRRCSubAddresses:Primed',0)
  End

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','SUBACCAD')
  p_web.SetValue('UpdateKey','sua:RecordNumberKey')
  If p_web.IfExistsValue('sua:AccountNumber')
    p_web.SetPicture('sua:AccountNumber','@s15')
  End
  p_web.SetSessionPicture('sua:AccountNumber','@s15')
  If p_web.IfExistsValue('sua:CompanyName')
    p_web.SetPicture('sua:CompanyName','@s30')
  End
  p_web.SetSessionPicture('sua:CompanyName','@s30')
  If p_web.IfExistsValue('sua:AddressLine1')
    p_web.SetPicture('sua:AddressLine1','@s30')
  End
  p_web.SetSessionPicture('sua:AddressLine1','@s30')
  If p_web.IfExistsValue('sua:AddressLine2')
    p_web.SetPicture('sua:AddressLine2','@s30')
  End
  p_web.SetSessionPicture('sua:AddressLine2','@s30')
  If p_web.IfExistsValue('sua:AddressLine3')
    p_web.SetPicture('sua:AddressLine3','@s30')
  End
  p_web.SetSessionPicture('sua:AddressLine3','@s30')
  If p_web.IfExistsValue('sua:Postcode')
    p_web.SetPicture('sua:Postcode','@s15')
  End
  p_web.SetSessionPicture('sua:Postcode','@s15')
  If p_web.IfExistsValue('sua:TelephoneNumber')
    p_web.SetPicture('sua:TelephoneNumber','@s15')
  End
  p_web.SetSessionPicture('sua:TelephoneNumber','@s15')
  If p_web.IfExistsValue('sua:FaxNumber')
    p_web.SetPicture('sua:FaxNumber','@s15')
  End
  p_web.SetSessionPicture('sua:FaxNumber','@s15')
  If p_web.IfExistsValue('sua:EmailAddress')
    p_web.SetPicture('sua:EmailAddress','@s255')
  End
  p_web.SetSessionPicture('sua:EmailAddress','@s255')
  If p_web.IfExistsValue('sua:ContactName')
    p_web.SetPicture('sua:ContactName','@s30')
  End
  p_web.SetSessionPicture('sua:ContactName','@s30')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'sua:AddressLine3'
    p_web.setsessionvalue('showtab_FormRRCSubAddresses',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBURB)
        p_web.setsessionvalue('sua:Postcode',sur:Postcode)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.sua:Postcode')
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=File

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormRRCSubAddresses_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'BrowseRRCSubAddresses'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormRRCSubAddresses_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormRRCSubAddresses_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormRRCSubAddresses_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'BrowseRRCSubAddresses'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  packet = clip(packet) & '<input type="hidden" name="SUBACCAD__FileAction" value="'&p_web.getSessionValue('SUBACCAD:FileAction')&'" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="file" value="SUBACCAD" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateFile" value="SUBACCAD" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateKey" value="sua:RecordNumberKey" ></input><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormRRCSubAddresses" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormRRCSubAddresses" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormRRCSubAddresses" ></input><13,10>'
  end

  do SendPacket
  packet = clip(packet) & p_web.CreateInput('hidden','sua:RecordNumber',p_web._jsok(p_web.getSessionValue('sua:RecordNumber'))) & '<13,10>'
  If p_web.Translate('Update Address Details') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Update Address Details',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormRRCSubAddresses">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormRRCSubAddresses" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormRRCSubAddresses')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('General') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormRRCSubAddresses')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormRRCSubAddresses'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='SUBURB'
            p_web.SetValue('SelectField',clip(loc:formname) & '.sua:Postcode')
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.sua:AccountNumber')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormRRCSubAddresses')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('General') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormRRCSubAddresses_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sua:AccountNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sua:AccountNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sua:AccountNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sua:CompanyName
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sua:CompanyName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sua:CompanyName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::hidden
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sua:AddressLine1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sua:AddressLine1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sua:AddressLine1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sua:AddressLine2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sua:AddressLine2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sua:AddressLine2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sua:AddressLine3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sua:AddressLine3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sua:AddressLine3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sua:Postcode
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sua:Postcode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sua:Postcode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sua:TelephoneNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sua:TelephoneNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sua:TelephoneNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sua:FaxNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sua:FaxNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sua:FaxNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sua:EmailAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sua:EmailAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sua:EmailAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sua:ContactName
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sua:ContactName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sua:ContactName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::sua:AccountNumber  Routine
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:AccountNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sua:AccountNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sua:AccountNumber',p_web.GetValue('NewValue'))
    sua:AccountNumber = p_web.GetValue('NewValue') !FieldType= STRING Field = sua:AccountNumber
    do Value::sua:AccountNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sua:AccountNumber',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    sua:AccountNumber = p_web.GetValue('Value')
  End
  If sua:AccountNumber = ''
    loc:Invalid = 'sua:AccountNumber'
    loc:alert = p_web.translate('Account Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    sua:AccountNumber = Upper(sua:AccountNumber)
    p_web.SetSessionValue('sua:AccountNumber',sua:AccountNumber)
  do Value::sua:AccountNumber
  do SendAlert

Value::sua:AccountNumber  Routine
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:AccountNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sua:AccountNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('sua:AccountNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If sua:AccountNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sua:AccountNumber'',''formrrcsubaddresses_sua:accountnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sua:AccountNumber',p_web.GetSessionValueFormat('sua:AccountNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCSubAddresses_' & p_web._nocolon('sua:AccountNumber') & '_value')

Comment::sua:AccountNumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:AccountNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sua:CompanyName  Routine
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:CompanyName') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Company Name')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sua:CompanyName  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sua:CompanyName',p_web.GetValue('NewValue'))
    sua:CompanyName = p_web.GetValue('NewValue') !FieldType= STRING Field = sua:CompanyName
    do Value::sua:CompanyName
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sua:CompanyName',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sua:CompanyName = p_web.GetValue('Value')
  End
  If sua:CompanyName = ''
    loc:Invalid = 'sua:CompanyName'
    loc:alert = p_web.translate('Company Name') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    sua:CompanyName = Upper(sua:CompanyName)
    p_web.SetSessionValue('sua:CompanyName',sua:CompanyName)
  do Value::sua:CompanyName
  do SendAlert

Value::sua:CompanyName  Routine
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:CompanyName') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sua:CompanyName
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('sua:CompanyName')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If sua:CompanyName = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sua:CompanyName'',''formrrcsubaddresses_sua:companyname_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sua:CompanyName',p_web.GetSessionValueFormat('sua:CompanyName'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCSubAddresses_' & p_web._nocolon('sua:CompanyName') & '_value')

Comment::sua:CompanyName  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:CompanyName') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::hidden  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden',p_web.GetValue('NewValue'))
    do Value::hidden
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden  Routine
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('hidden') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::hidden  Routine
    loc:comment = ''
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('hidden') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sua:AddressLine1  Routine
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:AddressLine1') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Address')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sua:AddressLine1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sua:AddressLine1',p_web.GetValue('NewValue'))
    sua:AddressLine1 = p_web.GetValue('NewValue') !FieldType= STRING Field = sua:AddressLine1
    do Value::sua:AddressLine1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sua:AddressLine1',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sua:AddressLine1 = p_web.GetValue('Value')
  End
    sua:AddressLine1 = Upper(sua:AddressLine1)
    p_web.SetSessionValue('sua:AddressLine1',sua:AddressLine1)
  do Value::sua:AddressLine1
  do SendAlert

Value::sua:AddressLine1  Routine
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:AddressLine1') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sua:AddressLine1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sua:AddressLine1')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sua:AddressLine1'',''formrrcsubaddresses_sua:addressline1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sua:AddressLine1',p_web.GetSessionValueFormat('sua:AddressLine1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCSubAddresses_' & p_web._nocolon('sua:AddressLine1') & '_value')

Comment::sua:AddressLine1  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:AddressLine1') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sua:AddressLine2  Routine
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:AddressLine2') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sua:AddressLine2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sua:AddressLine2',p_web.GetValue('NewValue'))
    sua:AddressLine2 = p_web.GetValue('NewValue') !FieldType= STRING Field = sua:AddressLine2
    do Value::sua:AddressLine2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sua:AddressLine2',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sua:AddressLine2 = p_web.GetValue('Value')
  End
    sua:AddressLine2 = Upper(sua:AddressLine2)
    p_web.SetSessionValue('sua:AddressLine2',sua:AddressLine2)
  do Value::sua:AddressLine2
  do SendAlert

Value::sua:AddressLine2  Routine
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:AddressLine2') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sua:AddressLine2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sua:AddressLine2')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sua:AddressLine2'',''formrrcsubaddresses_sua:addressline2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sua:AddressLine2',p_web.GetSessionValueFormat('sua:AddressLine2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCSubAddresses_' & p_web._nocolon('sua:AddressLine2') & '_value')

Comment::sua:AddressLine2  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:AddressLine2') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sua:AddressLine3  Routine
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:AddressLine3') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Suburb')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sua:AddressLine3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sua:AddressLine3',p_web.GetValue('NewValue'))
    sua:AddressLine3 = p_web.GetValue('NewValue') !FieldType= STRING Field = sua:AddressLine3
    do Value::sua:AddressLine3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sua:AddressLine3',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sua:AddressLine3 = p_web.GetValue('Value')
  End
    sua:AddressLine3 = Upper(sua:AddressLine3)
    p_web.SetSessionValue('sua:AddressLine3',sua:AddressLine3)
  p_Web.SetValue('lookupfield','sua:AddressLine3')
  do AfterLookup
  do Value::sua:Postcode
  do Value::sua:AddressLine3
  do SendAlert
  do Comment::sua:AddressLine3

Value::sua:AddressLine3  Routine
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:AddressLine3') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sua:AddressLine3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sua:AddressLine3')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sua:AddressLine3'',''formrrcsubaddresses_sua:addressline3_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('sua:AddressLine3')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','sua:AddressLine3',p_web.GetSessionValue('sua:AddressLine3'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('LookupSuburbs?LookupField=sua:AddressLine3&Tab=0&ForeignField=sur:Suburb&_sort=&Refresh=sort&LookupFrom=FormRRCSubAddresses&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCSubAddresses_' & p_web._nocolon('sua:AddressLine3') & '_value')

Comment::sua:AddressLine3  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:AddressLine3') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCSubAddresses_' & p_web._nocolon('sua:AddressLine3') & '_comment')

Prompt::sua:Postcode  Routine
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:Postcode') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Postcode')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCSubAddresses_' & p_web._nocolon('sua:Postcode') & '_prompt')

Validate::sua:Postcode  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sua:Postcode',p_web.GetValue('NewValue'))
    sua:Postcode = p_web.GetValue('NewValue') !FieldType= STRING Field = sua:Postcode
    do Value::sua:Postcode
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sua:Postcode',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    sua:Postcode = p_web.GetValue('Value')
  End
    sua:Postcode = Upper(sua:Postcode)
    p_web.SetSessionValue('sua:Postcode',sua:Postcode)
  do Value::sua:Postcode
  do SendAlert

Value::sua:Postcode  Routine
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:Postcode') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sua:Postcode
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sua:Postcode')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sua:Postcode'',''formrrcsubaddresses_sua:postcode_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sua:Postcode',p_web.GetSessionValueFormat('sua:Postcode'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCSubAddresses_' & p_web._nocolon('sua:Postcode') & '_value')

Comment::sua:Postcode  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:Postcode') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCSubAddresses_' & p_web._nocolon('sua:Postcode') & '_comment')

Prompt::sua:TelephoneNumber  Routine
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:TelephoneNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Telephone Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sua:TelephoneNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sua:TelephoneNumber',p_web.GetValue('NewValue'))
    sua:TelephoneNumber = p_web.GetValue('NewValue') !FieldType= STRING Field = sua:TelephoneNumber
    do Value::sua:TelephoneNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sua:TelephoneNumber',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    sua:TelephoneNumber = p_web.GetValue('Value')
  End
    sua:TelephoneNumber = Upper(sua:TelephoneNumber)
    p_web.SetSessionValue('sua:TelephoneNumber',sua:TelephoneNumber)
  do Value::sua:TelephoneNumber
  do SendAlert

Value::sua:TelephoneNumber  Routine
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:TelephoneNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sua:TelephoneNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sua:TelephoneNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sua:TelephoneNumber'',''formrrcsubaddresses_sua:telephonenumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sua:TelephoneNumber',p_web.GetSessionValueFormat('sua:TelephoneNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCSubAddresses_' & p_web._nocolon('sua:TelephoneNumber') & '_value')

Comment::sua:TelephoneNumber  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:TelephoneNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sua:FaxNumber  Routine
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:FaxNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Fax Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sua:FaxNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sua:FaxNumber',p_web.GetValue('NewValue'))
    sua:FaxNumber = p_web.GetValue('NewValue') !FieldType= STRING Field = sua:FaxNumber
    do Value::sua:FaxNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sua:FaxNumber',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    sua:FaxNumber = p_web.GetValue('Value')
  End
    sua:FaxNumber = Upper(sua:FaxNumber)
    p_web.SetSessionValue('sua:FaxNumber',sua:FaxNumber)
  do Value::sua:FaxNumber
  do SendAlert

Value::sua:FaxNumber  Routine
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:FaxNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sua:FaxNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sua:FaxNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sua:FaxNumber'',''formrrcsubaddresses_sua:faxnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sua:FaxNumber',p_web.GetSessionValueFormat('sua:FaxNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCSubAddresses_' & p_web._nocolon('sua:FaxNumber') & '_value')

Comment::sua:FaxNumber  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:FaxNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sua:EmailAddress  Routine
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:EmailAddress') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Email Address')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sua:EmailAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sua:EmailAddress',p_web.GetValue('NewValue'))
    sua:EmailAddress = p_web.GetValue('NewValue') !FieldType= STRING Field = sua:EmailAddress
    do Value::sua:EmailAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sua:EmailAddress',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    sua:EmailAddress = p_web.GetValue('Value')
  End
  do Value::sua:EmailAddress
  do SendAlert

Value::sua:EmailAddress  Routine
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:EmailAddress') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sua:EmailAddress
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('sua:EmailAddress')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sua:EmailAddress'',''formrrcsubaddresses_sua:emailaddress_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sua:EmailAddress',p_web.GetSessionValueFormat('sua:EmailAddress'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s255'),'Email Address') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCSubAddresses_' & p_web._nocolon('sua:EmailAddress') & '_value')

Comment::sua:EmailAddress  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:EmailAddress') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sua:ContactName  Routine
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:ContactName') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Contact Name')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sua:ContactName  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sua:ContactName',p_web.GetValue('NewValue'))
    sua:ContactName = p_web.GetValue('NewValue') !FieldType= STRING Field = sua:ContactName
    do Value::sua:ContactName
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sua:ContactName',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sua:ContactName = p_web.GetValue('Value')
  End
    sua:ContactName = Upper(sua:ContactName)
    p_web.SetSessionValue('sua:ContactName',sua:ContactName)
  do Value::sua:ContactName
  do SendAlert

Value::sua:ContactName  Routine
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:ContactName') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sua:ContactName
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('sua:ContactName')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sua:ContactName'',''formrrcsubaddresses_sua:contactname_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sua:ContactName',p_web.GetSessionValueFormat('sua:ContactName'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRRCSubAddresses_' & p_web._nocolon('sua:ContactName') & '_value')

Comment::sua:ContactName  Routine
      loc:comment = ''
  p_web._DivHeader('FormRRCSubAddresses_' & p_web._nocolon('sua:ContactName') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormRRCSubAddresses_sua:AccountNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sua:AccountNumber
      else
        do Value::sua:AccountNumber
      end
  of lower('FormRRCSubAddresses_sua:CompanyName_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sua:CompanyName
      else
        do Value::sua:CompanyName
      end
  of lower('FormRRCSubAddresses_sua:AddressLine1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sua:AddressLine1
      else
        do Value::sua:AddressLine1
      end
  of lower('FormRRCSubAddresses_sua:AddressLine2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sua:AddressLine2
      else
        do Value::sua:AddressLine2
      end
  of lower('FormRRCSubAddresses_sua:AddressLine3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sua:AddressLine3
      else
        do Value::sua:AddressLine3
      end
  of lower('FormRRCSubAddresses_sua:Postcode_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sua:Postcode
      else
        do Value::sua:Postcode
      end
  of lower('FormRRCSubAddresses_sua:TelephoneNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sua:TelephoneNumber
      else
        do Value::sua:TelephoneNumber
      end
  of lower('FormRRCSubAddresses_sua:FaxNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sua:FaxNumber
      else
        do Value::sua:FaxNumber
      end
  of lower('FormRRCSubAddresses_sua:EmailAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sua:EmailAddress
      else
        do Value::sua:EmailAddress
      end
  of lower('FormRRCSubAddresses_sua:ContactName_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sua:ContactName
      else
        do Value::sua:ContactName
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormRRCSubAddresses_form:ready_',1)
  p_web.SetSessionValue('FormRRCSubAddresses_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormRRCSubAddresses',0)
  sua:RefNumber = p_web.GSV('sub:RecordNumber')
  p_web.SetSessionValue('sua:RefNumber',sua:RefNumber)

PreCopy  Routine
  p_web.SetValue('FormRRCSubAddresses_form:ready_',1)
  p_web.SetSessionValue('FormRRCSubAddresses_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormRRCSubAddresses',0)
  p_web._PreCopyRecord(SUBACCAD,sua:RecordNumberKey)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormRRCSubAddresses_form:ready_',1)
  p_web.SetSessionValue('FormRRCSubAddresses_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormRRCSubAddresses:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormRRCSubAddresses_form:ready_',1)
  p_web.SetSessionValue('FormRRCSubAddresses_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormRRCSubAddresses:Primed',0)
  p_web.setsessionvalue('showtab_FormRRCSubAddresses',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord
  do CheckForDuplicate

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord
  do CheckForDuplicate

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord
  do CheckForDuplicate
CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If ans = 0 then exit. ! no need to check, as no action happening
  If p_web.GetSessionValue('FormRRCSubAddresses:Primed') = 0 and Ans = InsertRecord
    Get(SUBACCAD,0)
  End
  ! Check for duplicates
  If Duplicate(sua:AccountNumberOnlyKey)
    loc:Invalid = 'sua:AccountNumber'
    loc:Alert = clip(p_web.site.DuplicateText) & ' AccountNumberOnlyKey --> '&clip('Account Number')&' = ' & clip(sua:AccountNumber)
  End

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormRRCSubAddresses_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormRRCSubAddresses_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  If Loc:Invalid <> '' then exit.
        If sua:AccountNumber = ''
          loc:Invalid = 'sua:AccountNumber'
          loc:alert = p_web.translate('Account Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          sua:AccountNumber = Upper(sua:AccountNumber)
          p_web.SetSessionValue('sua:AccountNumber',sua:AccountNumber)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
        If sua:CompanyName = ''
          loc:Invalid = 'sua:CompanyName'
          loc:alert = p_web.translate('Company Name') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          sua:CompanyName = Upper(sua:CompanyName)
          p_web.SetSessionValue('sua:CompanyName',sua:CompanyName)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sua:AddressLine1 = Upper(sua:AddressLine1)
          p_web.SetSessionValue('sua:AddressLine1',sua:AddressLine1)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sua:AddressLine2 = Upper(sua:AddressLine2)
          p_web.SetSessionValue('sua:AddressLine2',sua:AddressLine2)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sua:AddressLine3 = Upper(sua:AddressLine3)
          p_web.SetSessionValue('sua:AddressLine3',sua:AddressLine3)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sua:Postcode = Upper(sua:Postcode)
          p_web.SetSessionValue('sua:Postcode',sua:Postcode)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sua:TelephoneNumber = Upper(sua:TelephoneNumber)
          p_web.SetSessionValue('sua:TelephoneNumber',sua:TelephoneNumber)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sua:FaxNumber = Upper(sua:FaxNumber)
          p_web.SetSessionValue('sua:FaxNumber',sua:FaxNumber)
        If loc:Invalid <> '' then exit.
  If Loc:Invalid <> '' then exit.
          sua:ContactName = Upper(sua:ContactName)
          p_web.SetSessionValue('sua:ContactName',sua:ContactName)
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostInsert      Routine

PostCopy        Routine
  p_web.SetSessionValue('FormRRCSubAddresses:Primed',0)

PostUpdate      Routine
  p_web.SetSessionValue('FormRRCSubAddresses:Primed',0)
  p_web.StoreValue('')

PostDelete      Routine
