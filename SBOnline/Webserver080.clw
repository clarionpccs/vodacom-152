

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER080.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
NoAccessories        PROCEDURE  (func:refNumber)           ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
    retValue# = 0

    do openFiles
    found# = 0
    Access:JOBACC.Clearkey(jac:ref_Number_Key)
    jac:ref_Number    = func:refNumber
    set(jac:ref_Number_Key,jac:ref_Number_Key)
    loop
        if (Access:JOBACC.Next())
            Break
        end ! if (Access:JOBACC.Next())
        if (jac:ref_Number    <> func:refNumber)
            Break
        end ! if (jac:ref_Number    <> func:refNumber)
        found# = 1
        break
    end ! loop

    do closeFiles

    if (found# = 0)
        retValue# = 1
    end ! if (found# = 0)

    return retValue#
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBACC.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBACC.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBACC.Close
     FilesOpened = False
  END
