

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER043.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
IsUnitLiquidDamaged  PROCEDURE  (String f:ESN, Long f:JobNumber,<Byte ignoreARCCheck>) ! Declare Procedure
tmp:JobDate          DATE                                  !Job Date

  CODE
    Return vod.IsUnitLiquidDamage(f:ESN,f:JobNumber,ignoreARCCheck)
