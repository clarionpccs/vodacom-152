

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER219.INC'),ONCE        !Local module procedure declarations
                     END


BrowseMobileNoSearch PROCEDURE  (NetWebServerWorker p_web)
locJobNumber         STRING(40)                            !
locExchanged         STRING(3)                             !
locRepairType        STRING(70)                            !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(WEBJOB)
                      Project(wob:RecordNumber)
                      Project(wob:RefNumber)
                      Join(job:Ref_Number_Key,wob:RefNumber)      !Compile errors? Make sure you've got a key for both files in the relation:  <=> JOBS
                        Project(job:Account_Number)
                        Project(job:Surname)
                        Project(job:Mobile_Number)
                        Project(job:ESN)
                        Project(job:MSN)
                        Project(job:Charge_Type)
                        Project(job:Warranty_Charge_Type)
                        Project(job:Date_Completed)
                      END
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
TagFile::State  USHORT
EXCHANGE::State  USHORT
SUBTRACC::State  USHORT
REPTYDEF::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseMobileNoSearch')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseMobileNoSearch:NoForm')
      loc:NoForm = p_web.GetValue('BrowseMobileNoSearch:NoForm')
      loc:FormName = p_web.GetValue('BrowseMobileNoSearch:FormName')
    else
      loc:FormName = 'BrowseMobileNoSearch_frm'
    End
    p_web.SSV('BrowseMobileNoSearch:NoForm',loc:NoForm)
    p_web.SSV('BrowseMobileNoSearch:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseMobileNoSearch:NoForm')
    loc:FormName = p_web.GSV('BrowseMobileNoSearch:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseMobileNoSearch') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseMobileNoSearch')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('adiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(WEBJOB,wob:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'WOB:REFNUMBER') then p_web.SetValue('BrowseMobileNoSearch_sort','1')
    ElsIf (loc:vorder = 'JOB:ACCOUNT_NUMBER') then p_web.SetValue('BrowseMobileNoSearch_sort','2')
    ElsIf (loc:vorder = 'JOB:SURNAME') then p_web.SetValue('BrowseMobileNoSearch_sort','3')
    ElsIf (loc:vorder = 'JOB:MOBILE_NUMBER') then p_web.SetValue('BrowseMobileNoSearch_sort','4')
    ElsIf (loc:vorder = 'JOB:ESN') then p_web.SetValue('BrowseMobileNoSearch_sort','5')
    ElsIf (loc:vorder = 'JOB:MSN') then p_web.SetValue('BrowseMobileNoSearch_sort','6')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseMobileNoSearch:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseMobileNoSearch:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseMobileNoSearch:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseMobileNoSearch:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseMobileNoSearch:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  ! Save Window Name
  AddToLog('NetWebBrowse',p_web.RequestData.DataString,'BrowseMobileNoSearch',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseMobileNoSearch_sort',net:DontEvaluate)
  If loc:vordernumber = 0
    loc:vordernumber = -1
  End
  p_web.SetSessionValue('BrowseMobileNoSearch_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 13
    Loc:LocateField = ''
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'wob:RefNumber','-wob:RefNumber')
    Loc:LocateField = 'wob:RefNumber'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(job:Account_Number)','-UPPER(job:Account_Number)')
    Loc:LocateField = 'job:Account_Number'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(job:Surname)','-UPPER(job:Surname)')
    Loc:LocateField = 'job:Surname'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(job:Mobile_Number)','-UPPER(job:Mobile_Number)')
    Loc:LocateField = 'job:Mobile_Number'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(job:ESN)','-UPPER(job:ESN)')
    Loc:LocateField = 'job:ESN'
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(job:MSN)','-UPPER(job:MSN)')
    Loc:LocateField = 'job:MSN'
  of 12
    Loc:LocateField = ''
  of 14
    Loc:LocateField = ''
  end
  if loc:vorder = ''
    loc:vorder = '-wob:RefNumber'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('wob:RefNumber')
    loc:SortHeader = p_web.Translate('Job Number')
    p_web.SetSessionValue('BrowseMobileNoSearch_LocatorPic','@s40')
  Of upper('job:Account_Number')
    loc:SortHeader = p_web.Translate('Account Number')
    p_web.SetSessionValue('BrowseMobileNoSearch_LocatorPic','@s15')
  Of upper('job:Surname')
    loc:SortHeader = p_web.Translate('Surname')
    p_web.SetSessionValue('BrowseMobileNoSearch_LocatorPic','@s30')
  Of upper('job:Mobile_Number')
    loc:SortHeader = p_web.Translate('Mobile Number')
    p_web.SetSessionValue('BrowseMobileNoSearch_LocatorPic','@s15')
  Of upper('job:ESN')
    loc:SortHeader = p_web.Translate('I.M.E.I. Number')
    p_web.SetSessionValue('BrowseMobileNoSearch_LocatorPic','@s20')
  Of upper('job:MSN')
    loc:SortHeader = p_web.Translate('MSN')
    p_web.SetSessionValue('BrowseMobileNoSearch_LocatorPic','@s20')
  Of upper('locExchanged')
    loc:SortHeader = p_web.Translate('Exchanged')
    p_web.SetSessionValue('BrowseMobileNoSearch_LocatorPic','@s3')
  Of upper('locRepairType')
    loc:SortHeader = p_web.Translate('Repair Type')
    p_web.SetSessionValue('BrowseMobileNoSearch_LocatorPic','@s70')
  Of upper('job:Charge_Type')
    loc:SortHeader = p_web.Translate('Cha Charge Type')
    p_web.SetSessionValue('BrowseMobileNoSearch_LocatorPic','@s30')
  Of upper('job:Warranty_Charge_Type')
    loc:SortHeader = p_web.Translate('War Charge Type')
    p_web.SetSessionValue('BrowseMobileNoSearch_LocatorPic','@s30')
  Of upper('job:Date_Completed')
    loc:SortHeader = p_web.Translate('Completed')
    p_web.SetSessionValue('BrowseMobileNoSearch_LocatorPic','@D6b')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseMobileNoSearch:LookupFrom')
  End!Else
  loc:formaction = 'BrowseMobileNoSearch'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseMobileNoSearch:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseMobileNoSearch:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseMobileNoSearch:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="WEBJOB"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="wob:RecordNumberKey"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseMobileNoSearch',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseMobileNoSearch',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseMobileNoSearch.locate(''Locator2BrowseMobileNoSearch'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseMobileNoSearch.cl(''BrowseMobileNoSearch'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseMobileNoSearch_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseMobileNoSearch_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  ColorField
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseMobileNoSearch','Job Number','Click here to sort by Link to JOBS RefNumber',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Link to JOBS RefNumber')&'">'&p_web.Translate('Job Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseMobileNoSearch','Account Number','Click here to sort by Account Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Account Number')&'">'&p_web.Translate('Account Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseMobileNoSearch','Surname','Click here to sort by Surname',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Surname')&'">'&p_web.Translate('Surname')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowseMobileNoSearch','Mobile Number','Click here to sort by Mobile Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Mobile Number')&'">'&p_web.Translate('Mobile Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','BrowseMobileNoSearch','I.M.E.I. Number','Click here to sort by I.M.E.I. Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by I.M.E.I. Number')&'">'&p_web.Translate('I.M.E.I. Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'6','BrowseMobileNoSearch','MSN','Click here to sort by MSN',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by MSN')&'">'&p_web.Translate('MSN')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&p_web.Translate('Exchanged')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&p_web.Translate('Repair Type')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Charge Type')&'">'&p_web.Translate('Cha Charge Type')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Charge Type')&'">'&p_web.Translate('War Charge Type')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Date Completed')&'">'&p_web.Translate('Completed')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'12','BrowseMobileNoSearch','Days',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Days')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  Edit
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'job:Date_Completed' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('wob:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and WEBJOB{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'wob:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('wob:RecordNumber'),p_web.GetValue('wob:RecordNumber'),p_web.GetSessionValue('wob:RecordNumber'))
      loc:FilterWas = 'UPPER(wob:HeadAccountNumber) = UPPER(''' & p_web.GSV('BookingAccount') & ''') AND UPPER(wob:MobileNumber) = UPPER(''' & p_web.GSV('GlobalMobileNumber') & ''')'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseMobileNoSearch',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseMobileNoSearch_Filter')
    p_web.SetSessionValue('BrowseMobileNoSearch_FirstValue','')
    p_web.SetSessionValue('BrowseMobileNoSearch_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,WEBJOB,wob:RecordNumberKey,loc:PageRows,'BrowseMobileNoSearch',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If WEBJOB{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(WEBJOB,loc:firstvalue)
              Reset(ThisView,WEBJOB)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If WEBJOB{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(WEBJOB,loc:lastvalue)
            Reset(ThisView,WEBJOB)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(wob:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseMobileNoSearch.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseMobileNoSearch.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseMobileNoSearch.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseMobileNoSearch.last();',,loc:nextdisabled)
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseMobileNoSearch',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseMobileNoSearch_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseMobileNoSearch_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseMobileNoSearch',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseMobileNoSearch.locate(''Locator1BrowseMobileNoSearch'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseMobileNoSearch.cl(''BrowseMobileNoSearch'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseMobileNoSearch_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseMobileNoSearch_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseMobileNoSearch.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseMobileNoSearch.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseMobileNoSearch.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseMobileNoSearch.last();',,loc:nextdisabled)
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    ! Fill In Browse Variables
    locJobNumber = CLIP(wob:RefNumber) & '-' & p_web.GSV('BookingBranchID') & CLIP(wob:JobNumber)
    
    locExchanged = 'No'
    
    IF (job:Exchange_Unit_Number > 0)
        locExchanged = 'Yes'
    END ! IF
    
    IF (job:Chargeable_Job = 'YES')
        Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
        rtd:Manufacturer = job:Manufacturer
        rtd:Chargeable = 'YES'
        rtd:Repair_Type  = job:Repair_Type  
        IF (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
         
        ELSE ! IF
        END ! IF
    
        locRepairType = job:Repair_Type
    ELSE ! IF
        Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
        rtd:Manufacturer = job:Manufacturer
        rtd:Warranty = 'YES'
        rtd:Repair_Type  = job:Repair_Type_Warranty  
        IF (Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign)
         
        ELSE ! IF
        END ! IF
    
        locRepairType = job:Repair_Type_Warranty
    END ! IF   
    
    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = job:Account_Number
    IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key))
    END ! IF
    
    loc:field = wob:RecordNumber
    p_web._thisrow = p_web._nocolon('wob:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseMobileNoSearch:LookupField')) = wob:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((wob:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseMobileNoSearch.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If WEBJOB{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(WEBJOB)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If WEBJOB{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(WEBJOB)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','wob:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BrowseMobileNoSearch.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','wob:RecordNumber',clip(loc:field),,'checked',,,'onclick="BrowseMobileNoSearch.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
            if false
            elsif job:Date_Completed
              packet = clip(packet) & '<td class="'&clip('BrowseGreen')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif INLIST(rtd:BER,0,2,3)
              packet = clip(packet) & '<td class="'&clip('BrowseBlack')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif INLIST(rtd:BER,1,4)
              packet = clip(packet) & '<td class="'&clip('BrowseBlue')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif INLIST(rtd:BER,5,6,8)
              packet = clip(packet) & '<td class="'&clip('BrowsePurple')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif rtd:BER = 7
              packet = clip(packet) & '<td class="'&clip('BrowsePink')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif sub:Refurbishmentaccount = 1
              packet = clip(packet) & '<td class="'&clip('BrowseRed')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            else
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            end
          end ! loc:eip = 0
          do value::ColorField
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::wob:RefNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job:Account_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job:Surname
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job:Mobile_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job:ESN
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job:MSN
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locExchanged
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locRepairType
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job:Charge_Type
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job:Warranty_Charge_Type
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job:Date_Completed
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::DaysSinceBooking
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Edit
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseMobileNoSearch.omv(this);" onMouseOut="BrowseMobileNoSearch.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseMobileNoSearch=new browseTable(''BrowseMobileNoSearch'','''&clip(loc:formname)&''','''&p_web._jsok('wob:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('wob:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseMobileNoSearch.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseMobileNoSearch.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseMobileNoSearch')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseMobileNoSearch')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseMobileNoSearch')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseMobileNoSearch')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(WEBJOB)
  p_web._CloseFile(JOBS)
  p_web._CloseFile(TagFile)
  p_web._CloseFile(EXCHANGE)
  p_web._CloseFile(SUBTRACC)
  p_web._CloseFile(REPTYDEF)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(WEBJOB)
  Bind(wob:Record)
  Clear(wob:Record)
  NetWebSetSessionPics(p_web,WEBJOB)
  p_web._OpenFile(JOBS)
  Bind(job:Record)
  NetWebSetSessionPics(p_web,JOBS)
  p_web._OpenFile(TagFile)
  Bind(tag:Record)
  NetWebSetSessionPics(p_web,TagFile)
  p_web._OpenFile(EXCHANGE)
  Bind(xch:Record)
  NetWebSetSessionPics(p_web,EXCHANGE)
  p_web._OpenFile(SUBTRACC)
  Bind(sub:Record)
  NetWebSetSessionPics(p_web,SUBTRACC)
  p_web._OpenFile(REPTYDEF)
  Bind(rtd:Record)
  NetWebSetSessionPics(p_web,REPTYDEF)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('wob:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
      ! Save Window Name
      IF (loc:alert <> '')
          AddToLog('Alert',loc:alert,'BrowseMobileNoSearch',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
      END ! IF
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(WEBJOB)
  Case upper(p_web.GetValue('_EIPClm'))
  of upper('Edit')
    do Validate::Edit
  End
  p_web._CloseFile(WEBJOB)
! ----------------------------------------------------------------------------------------
value::ColorField   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    elsif job:Date_Completed
      packet = clip(packet) & p_web._DivHeader('ColorField_'&wob:RecordNumber,'BrowseGreen',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok('',0))
    elsif INLIST(rtd:BER,0,2,3)
      packet = clip(packet) & p_web._DivHeader('ColorField_'&wob:RecordNumber,'BrowseBlack',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok('',0))
    elsif INLIST(rtd:BER,1,4)
      packet = clip(packet) & p_web._DivHeader('ColorField_'&wob:RecordNumber,'BrowseBlue',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok('',0))
    elsif INLIST(rtd:BER,5,6,8)
      packet = clip(packet) & p_web._DivHeader('ColorField_'&wob:RecordNumber,'BrowsePurple',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok('',0))
    elsif rtd:BER = 7
      packet = clip(packet) & p_web._DivHeader('ColorField_'&wob:RecordNumber,'BrowsePink',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok('',0))
    elsif sub:Refurbishmentaccount = 1
      packet = clip(packet) & p_web._DivHeader('ColorField_'&wob:RecordNumber,'BrowseRed',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok('',0))
    else
      packet = clip(packet) & p_web._DivHeader('ColorField_'&wob:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format('','@s1')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::wob:RefNumber   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('wob:RefNumber_'&wob:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locJobNumber,'@s40')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::job:Account_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job:Account_Number_'&wob:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job:Account_Number,'@s15')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::job:Surname   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job:Surname_'&wob:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job:Surname,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::job:Mobile_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job:Mobile_Number_'&wob:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job:Mobile_Number,'@s15')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::job:ESN   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job:ESN_'&wob:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job:ESN,'@s20')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::job:MSN   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job:MSN_'&wob:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job:MSN,'@s20')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locExchanged   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locExchanged_'&wob:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locExchanged,'@s3')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locRepairType   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locRepairType_'&wob:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locRepairType,'@s70')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::job:Charge_Type   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job:Charge_Type_'&wob:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job:Charge_Type,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::job:Warranty_Charge_Type   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job:Warranty_Charge_Type_'&wob:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job:Warranty_Charge_Type,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::job:Date_Completed   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job:Date_Completed_'&wob:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job:Date_Completed,'@D6b')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::DaysSinceBooking   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('DaysSinceBooking_'&wob:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(TODAY() - job:Date_Booked,'@n_8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
Validate::Edit  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  wob:RecordNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  !#Bryan: Added embed to set extra key components for multi part keys
  loc:result = p_web._GetFile(WEBJOB,wob:RecordNumberKey)
  p_web.FileToSessionQueue(WEBJOB)
  do CheckForDuplicate
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::Edit   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Edit_'&wob:RecordNumber,,net:crc)
      loc:disabled = ''
      If '_self'='_self' and Loc:Disabled = 0
        Loc:Disabled = -1
      End
      ! the button is set as a "submit" button, and this must have a URL
      packet = clip(packet) & p_web.CreateButton('submit','ViewJob','View Job','SmallButton',loc:formname,p_web._MakeURL(clip('ViewJob?&wob__RecordNumber=' & wob:RecordNumber & '&Change_btn=Change&ViewJobReturnURL=FormGlobalIMEISearch')& '&PressedButton=change_btn'),clip('_self'),,,loc:disabled,,,,,) & '<13,10>'  !5
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(TagFile)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(REPTYDEF)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(REPTYDEF)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = wob:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('wob:RecordNumber',wob:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('wob:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('wob:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('wob:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
