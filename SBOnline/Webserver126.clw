

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER126.INC'),ONCE        !Local module procedure declarations
                     END


ClearAddressDetails  PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locClearAddressOption BYTE                                 !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('ClearAddressDetails')
  loc:formname = 'ClearAddressDetails_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'ClearAddressDetails',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('ClearAddressDetails','')
    p_web._DivHeader('ClearAddressDetails',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferClearAddressDetails',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferClearAddressDetails',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferClearAddressDetails',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ClearAddressDetails',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferClearAddressDetails',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_ClearAddressDetails',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'ClearAddressDetails',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
deleteVariables     ROUTINE
    p_web.DeleteSessionValue('locClearAddressOption')
    p_web.DeleteSessionValue('AddType')
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('ClearAddressDetails_form:inited_',1)
  do RestoreMem

CancelForm  Routine
      DO deleteVariables

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locClearAddressOption',locClearAddressOption)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locClearAddressOption')
    locClearAddressOption = p_web.GetValue('locClearAddressOption')
    p_web.SetSessionValue('locClearAddressOption',locClearAddressOption)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('ClearAddressDetails_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
      p_web.StoreValue('AddType')
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locClearAddressOption = p_web.RestoreValue('locClearAddressOption')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'AmendAddress'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('ClearAddressDetails_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('ClearAddressDetails_ChainTo')
    loc:formaction = p_web.GetSessionValue('ClearAddressDetails_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'AmendAddress'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="ClearAddressDetails" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="ClearAddressDetails" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="ClearAddressDetails" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Address Options') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Address Options',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  packet = clip(packet) & p_web.br
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_ClearAddressDetails">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_ClearAddressDetails" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_ClearAddressDetails')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Options') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_ClearAddressDetails')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_ClearAddressDetails'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_ClearAddressDetails')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Options') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ClearAddressDetails_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Options')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Options')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Options')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Options')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locClearAddressOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locClearAddressOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::locClearAddressOption  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locClearAddressOption',p_web.GetValue('NewValue'))
    locClearAddressOption = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locClearAddressOption
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locClearAddressOption',p_web.GetValue('Value'))
    locClearAddressOption = p_web.GetValue('Value')
  End
  do Value::locClearAddressOption
  do SendAlert

Value::locClearAddressOption  Routine
  p_web._DivHeader('ClearAddressDetails_' & p_web._nocolon('locClearAddressOption') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locClearAddressOption
  loc:fieldclass = Choose(sub(' noBorder',1,1) = ' ',clip('FormEntry') & ' noBorder',' noBorder')
  If lower(loc:invalid) = lower('locClearAddressOption')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locClearAddressOption') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locClearAddressOption'',''clearaddressdetails_locclearaddressoption_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locClearAddressOption',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locClearAddressOption_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Clear Address') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locClearAddressOption') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locClearAddressOption'',''clearaddressdetails_locclearaddressoption_value'',1,'''&clip(2)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locClearAddressOption',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locClearAddressOption_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Replicate Customer Address') & '<13,10>'
    packet = clip(packet) & p_web.br
  if p_web.GSV('Hide:CollectionAddress') <> 1
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If loc:readonly = '' and (p_web.GSV('AddType') = 'C') then loc:readonly = 'disabled'.
    if p_web.GetSessionValue('locClearAddressOption') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locClearAddressOption'',''clearaddressdetails_locclearaddressoption_value'',1,'''&clip(3)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locClearAddressOption',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locClearAddressOption_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Replicate Collection Address') & '<13,10>'
    packet = clip(packet) & p_web.br
  end
  if p_web.GSV('Hide:DeliveryAddress') <> 1
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If loc:readonly = '' and (p_web.GSV('AddType') = 'D') then loc:readonly = 'disabled'.
    if p_web.GetSessionValue('locClearAddressOption') = 4
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locClearAddressOption'',''clearaddressdetails_locclearaddressoption_value'',1,'''&clip(4)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locClearAddressOption',clip(4),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locClearAddressOption_4') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Replicate Delivery Address') & '<13,10>'
    packet = clip(packet) & p_web.br
  end
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ClearAddressDetails_' & p_web._nocolon('locClearAddressOption') & '_value')

Comment::locClearAddressOption  Routine
    loc:comment = ''
  p_web._DivHeader('ClearAddressDetails_' & p_web._nocolon('locClearAddressOption') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('ClearAddressDetails_locClearAddressOption_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locClearAddressOption
      else
        do Value::locClearAddressOption
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('ClearAddressDetails_form:ready_',1)
  p_web.SetSessionValue('ClearAddressDetails_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_ClearAddressDetails',0)

PreCopy  Routine
  p_web.SetValue('ClearAddressDetails_form:ready_',1)
  p_web.SetSessionValue('ClearAddressDetails_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_ClearAddressDetails',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('ClearAddressDetails_form:ready_',1)
  p_web.SetSessionValue('ClearAddressDetails_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('ClearAddressDetails:Primed',0)

PreDelete       Routine
  p_web.SetValue('ClearAddressDetails_form:ready_',1)
  p_web.SetSessionValue('ClearAddressDetails_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('ClearAddressDetails:Primed',0)
  p_web.setsessionvalue('showtab_ClearAddressDetails',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('ClearAddressDetails_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      CASE p_web.GSV('locClearAddressOption')
      OF 1 ! Clear Address
          CASE p_web.GSV('AddType')
          OF 'C'
              p_web.SSV('job:Postcode_Collection','')
              p_web.SSV('job:Company_Name_Collection','')
              p_web.SSV('job:Address_Line1_Collection','')
              p_web.SSV('job:Address_Line2_Collection','')
              p_web.SSV('job:Address_Line3_Collection','')
              p_web.SSV('job:Telephone_Collection','')
              p_web.SSV('jobe2:HubCollection','')            
          OF 'D'
              p_web.SSV('job:Postcode_Delivery','')
              p_web.SSV('job:Company_Name_Delivery','')
              p_web.SSV('job:Address_Line1_Delivery','')
              p_web.SSV('job:Address_Line2_Delivery','')
              p_web.SSV('job:Address_Line3_Delivery','')
              p_web.SSV('job:Telephone_Delivery','')
              p_web.SSV('jobe2:HubDelivery','')            
          END
         
      OF 2 ! Copy Customer Address
          CASE p_web.GSV('AddType')
          OF 'C'
              p_web.SSV('job:Postcode_Collection',p_web.GSV('job:Postcode'))
              p_web.SSV('job:Company_Name_Collection',p_web.GSV('job:Company_Name'))
              p_web.SSV('job:Address_Line1_Collection',p_web.GSV('job:Address_Line1'))
              p_web.SSV('job:Address_Line2_Collection',p_web.GSV('job:Address_Line2'))
              p_web.SSV('job:Address_Line3_Collection',p_web.GSV('job:Address_Line3'))
              p_web.SSV('job:Telephone_Collection',p_web.GSV('job:Telephone_Number'))
              p_web.SSV('jobe2:HubCollection',p_web.GSV('jobe2:HubCustomer'))            
          OF 'D'
              p_web.SSV('job:Postcode_Delivery',p_web.GSV('job:Postcode'))
              p_web.SSV('job:Company_Name_Delivery',p_web.GSV('job:Company_Name'))
              p_web.SSV('job:Address_Line1_Delivery',p_web.GSV('job:Address_Line1'))
              p_web.SSV('job:Address_Line2_Delivery',p_web.GSV('job:Address_Line2'))
              p_web.SSV('job:Address_Line3_Delivery',p_web.GSV('job:Address_Line3'))
              p_web.SSV('job:Telephone_Delivery',p_web.GSV('job:Telephone_Number'))
              p_web.SSV('jobe2:HubDelivery',p_web.GSV('jobe2:HubCustomer'))            
          END
      OF 3 ! Copy Collection Address
          IF (p_web.GSV('AddType') = 'D')
              p_web.SSV('job:Postcode_Delivery',p_web.GSV('job:Postcode_Collection'))
              p_web.SSV('job:Company_Name_Delivery',p_web.GSV('job:Company_Name_Collection'))
              p_web.SSV('job:Address_Line1_Delivery',p_web.GSV('job:Address_Line1_Collection'))
              p_web.SSV('job:Address_Line2_Delivery',p_web.GSV('job:Address_Line2_Collection'))
              p_web.SSV('job:Address_Line3_Delivery',p_web.GSV('job:Address_Line3_Collection'))
              p_web.SSV('job:Telephone_Delivery',p_web.GSV('job:Telephone_Number_Collection'))
              p_web.SSV('jobe2:HubDelivery',p_web.GSV('jobe2:HubCustomer_Collection'))            
          END
          
      OF 4 ! Copy Delivery Address
          IF (p_web.GSV('AddType') = 'C')
              p_web.SSV('job:Postcode_Collection',p_web.GSV('job:Postcode_Delivery'))
              p_web.SSV('job:Company_Name_Collection',p_web.GSV('job:Company_Name_Delivery'))
              p_web.SSV('job:Address_Line1_Collection',p_web.GSV('job:Address_Line1_Delivery'))
              p_web.SSV('job:Address_Line2_Collection',p_web.GSV('job:Address_Line2_Delivery'))
              p_web.SSV('job:Address_Line3_Collection',p_web.GSV('job:Address_Line3_Delivery'))
              p_web.SSV('job:Telephone_Collection',p_web.GSV('job:Telephone_Number_Delivery'))
              p_web.SSV('jobe2:HubCollection',p_web.GSV('jobe2:HubDelivery'))  
          END
      ELSE
          loc:alert = 'You must select an option.'
          loc:invalid = 'locClearAddressOption'
  
      END
  
         
         DO deleteVariables
  p_web.DeleteSessionValue('ClearAddressDetails_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('ClearAddressDetails:Primed',0)
  p_web.StoreValue('locClearAddressOption')
