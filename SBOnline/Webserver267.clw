

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER267.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
HubOutOfRegion       PROCEDURE  (STRING pHeadAccountNumber,STRING pHub) ! Declare Procedure
RetValue                    LONG(0)
TRAHUBS::State  USHORT
FilesOpened     BYTE(0)

  CODE
        DO OpenFiles
        LOOP 1 TIMES
            IF (pHeadAccountNumber = '')
                BREAK
            END ! IF
            IF (pHub = '')
                BREAK
            END ! IF
    
            Access:TRAHUBS.ClearKey(trh:HubKey)
            trh:TRADEACCAccountNumber = pHeadAccountNumber
            trh:Hub = pHub
            IF (Access:TRAHUBS.TryFetch(trh:HubKey) = Level:Benign)
                RetValue = 2
                BREAK
            END ! IF 
            
            Access:TRAHUBS.ClearKey(trh:HubKey)
            trh:TRADEACCAccountNumber = pHeadAccountNumber
            SET(trh:HubKey,trh:HubKey)
            LOOP UNTIL Access:TRAHUBS.Next() <> Level:Benign
                IF (trh:TRADEACCAccountNumber <> pHeadAccountNumber)
                    BREAK
                END ! IF
                IF (trh:Hub = pHub)
                    ! Double check, but this criteria shoud have been picked up above
                    CYCLE
                END ! IF
                RetValue = 1
                BREAK
            END ! LOOP
        
        END ! LOOP
        DO CloseFiles
        
        !0 = No Relevant Hub Data
        !1 = Hub is out of region
        !2 = Found Hub.        
    
        RETURN RetValue

! #13357 Refactored code as the loop below was unecessary and possibly slowing (DBH: 16/10/2015)
!        If pHeadAccountNumber = ''
!            Return 0
!        End ! If pHeadAccountNumber = ''
!        If pHub = ''
!            Return 0
!        End ! If pHub = ''
!        Do OpenFiles
!        Do SaveFiles
!        
!        
!        
!
!        Found# = 0
!        Access:TRAHUBS.Clearkey(trh:HubKey)
!        trh:TRADEACCAccountNumber = pHeadAccountNumber
!        Set(trh:HubKey,trh:HubKey)
!        Loop
!            If Access:TRAHUBS.Next()
!                Break
!            End ! If Access:TRAHUBS.Next()
!            If trh:TRADEACCAccountNumber <> pHeadAccountNumber
!                Break
!            End ! If trh:TRADEACCAccountNumber <> wob:HeadAccountNumber
!            Found# = 1
!            If trh:Hub = pHub
!                Found# = 2
!                Break
!            End ! If trh:Hub = pHub
!        End ! Loop
!
!        Do RestoreFiles
!        Do CloseFiles
!    !0 = No Relevant Hub Data
!    !1 = Hub is out of region
!    !2 = Found Hub.
!        Return Found#

!--------------------------------------
OpenFiles  ROUTINE
  Access:TRAHUBS.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRAHUBS.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TRAHUBS.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  TRAHUBS::State = Access:TRAHUBS.SaveFile()               ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF TRAHUBS::State <> 0
    Access:TRAHUBS.RestoreFile(TRAHUBS::State)             ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
