

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER073.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
CustCollectionValidated PROCEDURE  (LONG jobNumber)        ! Declare Procedure

  CODE
    RETURN vod.CustomerCollectionValidated(jobNumber)
