

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER233.INC'),ONCE        !Local module procedure declarations
                     END


ShowWait             PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:ShowWait -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
  CODE
  GlobalErrors.SetProcedureName('ShowWait')
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
!----------- put your html code here -----------------------------------
!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
ShowWait  Routine
  packet = clip(packet) & |
    '<<div id="waitframe" class="nt-process"><13,10>'&|
    '<<p class="nt-process-text">Please wait while ServiceBase searches for matching records<<BR /><<br/><13,10>'&|
    'This may take several seconds. . .<13,10>'&|
    '<</p><13,10>'&|
    '<<BR/><<BR/><13,10>'&|
    '<<p><13,10>'&|
    '<<a href="FormBrowseStock">Cancel Process<</a><13,10>'&|
    '<</p><13,10>'&|
    '<</div><13,10>'&|
    ''
