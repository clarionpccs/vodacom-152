

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER085.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ForceRepairType      PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !

  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Repair Type
    If (def:Force_Repair_Type = 'B' And func:Type = 'B') Or |
        (def:Force_Repair_Type <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Repair_Type = 'B'
    Return Level:Benign
