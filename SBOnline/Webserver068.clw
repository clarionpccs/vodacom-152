

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER068.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! Estimate
!!! </summary>
Estimate PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
RejectRecord         LONG,AUTO                             !
tmp:Ref_Number       STRING(20)                            !
tmp:PrintedBy        STRING(255)                           !
save_epr_id          USHORT,AUTO                           !
save_joo_id          USHORT,AUTO                           !
LocalRequest         LONG,AUTO                             !
LocalResponse        LONG,AUTO                             !
FilesOpened          LONG                                  !
WindowOpened         LONG                                  !
RecordsToProcess     LONG,AUTO                             !
RecordsProcessed     LONG,AUTO                             !
RecordsPerCycle      LONG,AUTO                             !
RecordsThisCycle     LONG,AUTO                             !
PercentProgress      BYTE                                  !
RecordStatus         BYTE,AUTO                             !
EndOfReport          BYTE,AUTO                             !
ReportRunDate        LONG,AUTO                             !
ReportRunTime        LONG,AUTO                             !
ReportPageNo         SHORT,AUTO                            !
FileOpensReached     BYTE                                  !
PartialPreviewReq    BYTE                                  !
DisplayProgress      BYTE                                  !
InitialPath          CSTRING(128)                          !
IniFileToUse         STRING(64)                            !
code_temp            BYTE                                  !
option_temp          BYTE                                  !
Bar_code_string_temp CSTRING(21)                           !
Bar_Code_Temp        CSTRING(21)                           !
Bar_Code2_Temp       CSTRING(21)                           !
Address_Line1_Temp   STRING(30)                            !
Address_Line2_Temp   STRING(30)                            !
Address_Line3_Temp   STRING(30)                            !
Address_Line4_Temp   STRING(30)                            !
Invoice_Name_Temp    STRING(30)                            !
Delivery_Company_Name_Temp STRING(30)                      !Delivery Company Name
Delivery_Address1_Temp STRING(30)                          !
Delivery_address2_temp STRING(30)                          !
Delivery_address3_temp STRING(30)                          !
Delivery_address4_temp STRING(30)                          !
Delivery_Telephone_Number_Temp STRING(30)                  !Delivery Telephone Number
Invoice_Company_Temp STRING(30)                            !
Invoice_address1_temp STRING(30)                           !
invoice_address2_temp STRING(30)                           !
invoice_address3_temp STRING(30)                           !
invoice_address4_temp STRING(30)                           !
accessories_temp     STRING(30),DIM(6)                     !
estimate_value_temp  STRING(40)                            !
despatched_user_temp STRING(40)                            !
vat_temp             REAL                                  !
total_temp           REAL                                  !
part_number_temp     STRING(30)                            !
line_cost_temp       REAL                                  !
job_number_temp      STRING(20)                            !
esn_temp             STRING(24)                            !
charge_type_temp     STRING(22)                            !
repair_type_temp     STRING(22)                            !
labour_temp          REAL                                  !
parts_temp           REAL                                  !
courier_cost_temp    REAL                                  !
Quantity_temp        REAL                                  !
Description_temp     STRING(30)                            !
Cost_Temp            REAL                                  !
customer_name_temp   STRING(60)                            !
sub_total_temp       REAL                                  !
invoice_company_name_temp STRING(30)                       !
invoice_telephone_number_temp STRING(15)                   !
invoice_fax_number_temp STRING(15)                         !
tmp:DefaultTelephone STRING(20)                            !
tmp:DefaultFax       STRING(20)                            !
tmp:InvoiceText      STRING(255)                           !Invoice Text
DefaultAddress       GROUP,PRE(address)                    !
Location             STRING(40)                            !
SiteName             STRING(40)                            !
Name2                STRING(40)                            !
RegistrationNo       STRING(40)                            !
VATNumber            STRING(30)                            !
Name                 STRING(40)                            !Name
AddressLine1         STRING(40)                            !Address Line 1
AddressLine2         STRING(40)                            !Address Line 2
AddressLine3         STRING(40)                            !Address Line 3
AddressLine4         STRING(40)                            !Postcode
Telephone            STRING(30)                            !Telephone
Fax                  STRING(30)                            !Fax
EmailAddress         STRING(255)                           !Email Address
                     END                                   !
locRefNumber         LONG                                  !
locAccountNumber     STRING(30)                            !
locOrderNumber       STRING(30)                            !
locAuthorityNumber   STRING(20)                            !
locChargeType        STRING(20)                            !
locRepairType        STRING(20)                            !
locModelNumber       STRING(30)                            !
locManufacturer      STRING(30)                            !
locUnitType          STRING(30)                            !
locESN               STRING(20)                            !
locMSN               STRING(20)                            !
Process:View         VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                     END
ProgressWindow       WINDOW('Report JOBS'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular),DOUBLE,CENTER,GRAY, |
  TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT,AT(396,6917,7521,1927),PRE(RPT),PAPER(PAPER:A4),FONT('Tahoma',10,,FONT:regular),THOUS
                       HEADER,AT(396,635,7521,6313),USE(?unnamed)
                         STRING('Job No:'),AT(4896,365),USE(?String25),FONT(,12),TRN
                         STRING(@s20),AT(5729,365),USE(tmp:Ref_Number),FONT(,10,,FONT:bold),LEFT,TRN
                         STRING('Estimate Date:'),AT(4896,573),USE(?String58),FONT(,8),TRN
                         STRING(@s60),AT(156,1563),USE(customer_name_temp),FONT(,8),TRN
                         STRING(@s60),AT(4063,1563),USE(customer_name_temp,,?customer_name_temp:2),FONT(,8),TRN
                         STRING(@s20),AT(1604,3240),USE(locOrderNumber),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(4531,3240),USE(locChargeType),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(3156,3240),USE(locAuthorityNumber),FONT(,8),TRN
                         STRING(@s20),AT(6042,3229),USE(locRepairType),FONT(,8),TRN
                         STRING(@s30),AT(156,3917,1000,156),USE(locModelNumber),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(1604,3917,1323,156),USE(locManufacturer),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(3156,3917,1396,156),USE(locUnitType),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(4604,3917,1396,156),USE(locESN),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(6083,3917),USE(locMSN),FONT(,8),TRN
                         STRING('Reported Fault: '),AT(156,4323),USE(?String64),FONT(,8,,FONT:bold),TRN
                         TEXT,AT(1615,4323,5625,417),USE(jbn:Fault_Description),FONT(,8,,,CHARSET:ANSI),TRN
                         TEXT,AT(1615,5052,5625,885),USE(tmp:InvoiceText),FONT(,8,,,CHARSET:ANSI),TRN
                         STRING('Engineers Report:'),AT(156,5000),USE(?String88),FONT(,8,,FONT:bold),TRN
                         GROUP,AT(104,5938,7500,208),USE(?PartsHeadingGroup),FONT('Tahoma')
                           STRING('PARTS REQUIRED'),AT(156,6042),USE(?String79),FONT(,9,,FONT:bold),TRN
                           STRING('Qty'),AT(1510,6042),USE(?String80),FONT(,8,,FONT:bold),TRN
                           STRING('Part Number'),AT(1917,6042),USE(?String81),FONT(,8,,FONT:bold),TRN
                           STRING('Description'),AT(3677,6042),USE(?String82),FONT(,8,,FONT:bold),TRN
                           STRING('Unit Cost'),AT(5719,6042),USE(?UnitCost),FONT(,8,,FONT:bold),TRN
                           STRING('Line Cost'),AT(6677,6042),USE(?LineCost),FONT(,8,,FONT:bold),TRN
                           LINE,AT(1406,6197,6042,0),USE(?Line1),COLOR(COLOR:Black)
                         END
                         STRING(@s30),AT(156,2344),USE(invoice_address4_temp),FONT(,8),TRN
                         STRING('Tel: '),AT(4063,2500),USE(?String32:2),FONT(,8),TRN
                         STRING(@s30),AT(4323,2500),USE(Delivery_Telephone_Number_Temp),FONT(,8)
                         STRING('Tel:'),AT(156,2500),USE(?String34:2),FONT(,8),TRN
                         STRING(@s15),AT(417,2500),USE(invoice_telephone_number_temp),FONT(,8),LEFT,TRN
                         STRING('Fax: '),AT(1927,2500),USE(?String35:2),FONT(,8),TRN
                         STRING(@s15),AT(2240,2500),USE(invoice_fax_number_temp),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(156,3240),USE(locAccountNumber),FONT(,8),TRN
                         STRING(@s30),AT(4063,1719),USE(Delivery_Company_Name_Temp),FONT(,8),TRN
                         STRING(@s30),AT(4063,1875,1917,156),USE(Delivery_Address1_Temp),FONT(,8),TRN
                         STRING(@s30),AT(4063,2031),USE(Delivery_address2_temp),FONT(,8),TRN
                         STRING(@s30),AT(4063,2188),USE(Delivery_address3_temp),FONT(,8),TRN
                         STRING(@s30),AT(4063,2344),USE(Delivery_address4_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1719),USE(invoice_company_name_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1875),USE(Invoice_address1_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,2188),USE(invoice_address3_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,2031),USE(invoice_address2_temp),FONT(,8),TRN
                         STRING('dd/mm/yyyy'),AT(5708,583),USE(?TodaysDate),FONT(,8,,FONT:bold),TRN
                       END
DETAIL                 DETAIL,AT(0,0,,198),USE(?DetailBand)
                         STRING(@n8b),AT(1156,0),USE(Quantity_temp),FONT(,8),RIGHT,TRN
                         STRING(@s25),AT(1917,0),USE(part_number_temp),FONT(,8),TRN
                         STRING(@s25),AT(3677,0),USE(Description_temp),FONT(,8),TRN
                         STRING(@n14.2b),AT(5208,0),USE(Cost_Temp),FONT(,8),RIGHT,TRN
                         STRING(@n14.2b),AT(6198,0),USE(line_cost_temp),FONT(,8),RIGHT,TRN
                       END
                       FOOTER,AT(385,9177,7521,2146),USE(?unnamed:4)
                         STRING('Labour:'),AT(4844,52),USE(?labour_string),FONT(,8),TRN
                         STRING(@n14.2b),AT(6250,52),USE(labour_temp),FONT(,8),RIGHT,TRN
                         STRING(@s20),AT(2865,313,1771,156),USE(job_number_temp),FONT(,8,,FONT:bold),CENTER,TRN
                         STRING(@s20),AT(2875,83,1760,198),USE(Bar_Code_Temp),FONT('C128 High 12pt LJ3',12),CENTER
                         STRING('Carriage:'),AT(4844,365),USE(?carriage_string),FONT(,8),TRN
                         STRING(@n14.2b),AT(6250,208),USE(parts_temp),FONT(,8),RIGHT,TRN
                         STRING('V.A.T.'),AT(4844,677),USE(?vat_String),FONT(,8),TRN
                         STRING(@n14.2b),AT(6250,365),USE(courier_cost_temp),FONT(,8),RIGHT,TRN
                         STRING('Total:'),AT(4844,833),USE(?total_string),FONT(,8,,FONT:bold),TRN
                         STRING('<128>'),AT(6146,833,156,208),USE(?Euro),FONT('Tahoma',8,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  HIDE,TRN
                         LINE,AT(6354,833,1000,0),USE(?line),COLOR(COLOR:Black)
                         LINE,AT(6354,521,1000,0),USE(?line:2),COLOR(COLOR:Black)
                         STRING(@n14.2b),AT(6198,833),USE(total_temp),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING('Sub Total:'),AT(4844,521),USE(?String92),FONT(,8),TRN
                         STRING(@n14.2b),AT(6250,521),USE(sub_total_temp),FONT(,8),RIGHT,TRN
                         STRING(@s20),AT(2865,521,1760,198),USE(Bar_Code2_Temp),FONT('C128 High 12pt LJ3',12),CENTER
                         STRING(@n14.2b),AT(6250,677),USE(vat_temp),FONT(,8),RIGHT,TRN
                         STRING(@s24),AT(2875,719,1760,240),USE(esn_temp),FONT(,8,,FONT:bold),CENTER,TRN
                         STRING('Parts:'),AT(4844,208),USE(?parts_string),FONT(,8),TRN
                         TEXT,AT(104,1094,7365,958),USE(stt:Text),FONT(,8)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RINVDET.GIF'),AT(0,0,7552,11198),USE(?Image1)
                         STRING('ESTIMATE'),AT(5521,0,1917,240),USE(?Chargeable_Repair_Type:2),FONT(,16,,FONT:bold), |
  RIGHT,TRN
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),FONT(,14,,FONT:bold),LEFT
                         STRING(@s40),AT(104,313,3073,208),USE(address:Name2),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,208,3073,208),USE(address:Name),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,417,2760,208),USE(address:Location),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING('REG NO:'),AT(104,573),USE(?stringREGNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(625,573,1667,208),USE(address:RegistrationNo),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('VAT NO: '),AT(104,677),USE(?stringVATNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s20),AT(625,677,1771,156),USE(address:VATNumber),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,833,2240,156),USE(address:AddressLine1),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,938,2240,156),USE(address:AddressLine2),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1042,2240,156),USE(address:AddressLine3),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1146),USE(address:AddressLine4),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('TEL:'),AT(104,1250),USE(?stringTEL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(469,1250,1458,208),USE(address:Telephone),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('FAX:'),AT(2083,1250,313,208),USE(?stringFAX),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(2396,1250,1510,188),USE(address:Fax),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('EMAIL:'),AT(104,1354,417,208),USE(?stringEMAIL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s255),AT(469,1354,3333,208),USE(address:EmailAddress),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('JOB DETAILS'),AT(4844,198),USE(?String57),FONT(,9,,FONT:bold),TRN
                         STRING('INVOICE ADDRESS'),AT(156,1500),USE(?String24),FONT(,9,,FONT:bold),TRN
                         STRING('DELIVERY ADDRESS'),AT(4010,1510,1677,156),USE(?String28),FONT(,9,,FONT:bold),TRN
                         STRING('GENERAL DETAILS'),AT(156,2958),USE(?String91),FONT(,9,,FONT:bold),TRN
                         STRING('AUTHORISATION DETAILS'),AT(156,8479),USE(?String73),FONT(,9,,FONT:bold),TRN
                         STRING('CHARGE DETAILS'),AT(4760,8479),USE(?String74),FONT(,9,,FONT:bold),TRN
                         STRING('Estimate Accepted'),AT(208,8750),USE(?String94),FONT(,8,,FONT:bold),TRN
                         BOX,AT(1406,8750,156,156),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Estimate Refused'),AT(208,8958),USE(?String95),FONT(,8,,FONT:bold),TRN
                         BOX,AT(1406,8958,156,156),USE(?Box2),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Name (Capitals)'),AT(208,9219),USE(?String96),FONT(,8,,FONT:bold),TRN
                         LINE,AT(1198,9323,1354,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('Signature'),AT(208,9479),USE(?String97),FONT(,8,,FONT:bold),TRN
                         LINE,AT(833,9583,1719,0),USE(?Line4),COLOR(COLOR:Black)
                         STRING('Model'),AT(156,3844),USE(?String40),FONT(,8,,FONT:bold),TRN
                         STRING('Account Number'),AT(156,3156),USE(?String40:2),FONT(,8,,FONT:bold),TRN
                         STRING('Order Number'),AT(1604,3156),USE(?String40:3),FONT(,8,,FONT:bold),TRN
                         STRING('Authority Number'),AT(3083,3156),USE(?String40:4),FONT(,8,,FONT:bold),TRN
                         STRING('Chargeable Type'),AT(4531,3156),USE(?Chargeable_Type),FONT(,8,,FONT:bold),TRN
                         STRING('Chargeable Repair Type'),AT(6000,3156),USE(?Repair_Type),FONT(,8,,FONT:bold),TRN
                         STRING('Make'),AT(1635,3844),USE(?String41),FONT(,8,,FONT:bold),TRN
                         STRING('Unit Type'),AT(3125,3844),USE(?String42),FONT(,8,,FONT:bold),TRN
                         STRING('I.M.E.I. Number'),AT(4563,3844),USE(?String43),FONT(,8,,FONT:bold),TRN
                         STRING('M.S.N.'),AT(6083,3844),USE(?String44),FONT(,8,,FONT:bold),TRN
                         STRING('REPAIR DETAILS'),AT(156,3635),USE(?String50),FONT(,9,,FONT:bold),TRN
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR2               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR2:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR2:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Estimate')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:AUDIT.SetOpenRelated()
  Relate:AUDIT.Open                                        ! File AUDIT used by this procedure, so make sure it's RelationManager is open
  Relate:DEFAULT2.Open                                     ! File DEFAULT2 used by this procedure, so make sure it's RelationManager is open
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:VATCODE.Open                                      ! File VATCODE used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:JOBNOTES.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBACC.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PARTS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:INVOICE.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBOUTFL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANUFACT.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAUPA.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAULO.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAULT.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:CHARTYPE.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  locRefNumber = p_web.GSV('job:Ref_Number')
  
  set(DEFAULTS)
  access:DEFAULTS.next()
  
  set(DEFAULT2)
  access:DEFAULT2.next()
  
  
  locAccountNumber = p_web.GSV('job:Account_Number')
  locOrderNumber = p_web.GSV('job:Order_Number')
  locAuthorityNumber = p_web.GSV('job:Authority_Number')
  locChargeType = p_web.GSV('job:Charge_Type')
  locRepairType = p_web.GSV('job:Repair_Type')
  locModelNumber = p_web.GSV('job:Model_Number')
  locManufacturer = p_web.GSV('job:Manufacturer')
  locUnitType = p_web.GSV('job:Unit_Type')
  locESN = p_web.GSV('job:ESN')
  locMSN = p_web.GSV('job:MSN')
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Estimate',ProgressWindow)                  ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisReport.AddSortOrder(job:Ref_Number_Key)
  ThisReport.AddRange(job:Ref_Number,locRefNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:STANTEXT.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('Estimate',ProgressWindow)               ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'Estimate',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
      Delivery_Company_Name_Temp = p_web.GSV('job:Company_Name_Delivery')
      delivery_address1_temp     = p_web.GSV('job:address_line1_delivery')
      delivery_address2_temp     = p_web.GSV('job:address_line2_delivery')
      If p_web.GSV('job:address_line3_delivery')   = ''
          delivery_address3_temp = p_web.GSV('job:postcode_delivery')
          delivery_address4_temp = ''
      Else
          delivery_address3_temp  = p_web.GSV('job:address_line3_delivery')
          delivery_address4_temp  = p_web.GSV('job:postcode_delivery')
      End
      Delivery_Telephone_Number_Temp = p_web.GSV('job:Telephone_Delivery')
  
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = p_web.GSV('job:account_number')
      access:subtracc.fetch(sub:account_number_key)
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = sub:main_account_number
      access:tradeacc.fetch(tra:account_number_key)
      if tra:invoice_sub_accounts = 'YES'
          If sub:invoice_customer_address = 'YES'
              invoice_address1_temp     = p_web.GSV('job:address_line1')
              invoice_address2_temp     = p_web.GSV('job:address_line2')
              If p_web.GSV('job:address_line3_delivery')   = ''
                  invoice_address3_temp = p_web.GSV('job:postcode')
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp  = p_web.GSV('job:address_line3')
                  invoice_address4_temp  = p_web.GSV('job:postcode')
              End
              invoice_company_name_temp   = p_web.GSV('job:company_name')
              invoice_telephone_number_temp   = p_web.GSV('job:telephone_number')
              invoice_fax_number_temp = p_web.GSV('job:fax_number')
          Else!If sub:invoice_customer_address = 'YES'
              invoice_address1_temp     = sub:address_line1
              invoice_address2_temp     = sub:address_line2
              If p_web.GSV('job:address_line3_delivery')   = ''
                  invoice_address3_temp = sub:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp  = sub:address_line3
                  invoice_address4_temp  = sub:postcode
              End
              invoice_company_name_temp   = sub:company_name
              invoice_telephone_number_temp   = sub:telephone_number
              invoice_fax_number_temp = sub:fax_number
  
  
          End!If sub:invoice_customer_address = 'YES'
  
      else!if tra:use_sub_accounts = 'YES'
          If tra:invoice_customer_address = 'YES'
              invoice_address1_temp     = p_web.GSV('job:address_line1')
              invoice_address2_temp     = p_web.GSV('job:address_line2')
              If p_web.GSV('job:address_line3_delivery')   = ''
                  invoice_address3_temp = p_web.GSV('job:postcode')
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp  = p_web.GSV('job:address_line3')
                  invoice_address4_temp  = p_web.GSV('job:postcode')
              End
              invoice_company_name_temp   = p_web.GSV('job:company_name')
              invoice_telephone_number_temp   = p_web.GSV('job:telephone_number')
              invoice_fax_number_temp = p_web.GSV('job:fax_number')
  
          Else!If tra:invoice_customer_address = 'YES'
              invoice_address1_temp     = tra:address_line1
              invoice_address2_temp     = tra:address_line2
              If p_web.GSV('job:address_line3_delivery')   = ''
                  invoice_address3_temp = tra:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp  = tra:address_line3
                  invoice_address4_temp  = tra:postcode
              End
              invoice_company_name_temp   = tra:company_name
              invoice_telephone_number_temp   = tra:telephone_number
              invoice_fax_number_temp = tra:fax_number
  
          End!If tra:invoice_customer_address = 'YES'
      End!!if tra:use_sub_accounts = 'YES'
  
  
      if p_web.GSV('job:title') = '' and p_web.GSV('job:initial') = ''
          customer_name_temp = clip(p_web.GSV('job:surname'))
      elsif p_web.GSV('job:title') = '' and p_web.GSV('job:initial') <> ''
          customer_name_temp = clip(p_web.GSV('job:initial')) & ' ' & clip(p_web.GSV('job:surname'))
      elsif p_web.GSV('job:title') <> '' and p_web.GSV('job:initial') = ''
          customer_name_temp = clip(p_web.GSV('job:title')) & ' ' & clip(p_web.GSV('job:surname'))
      elsif p_web.GSV('job:title') <> '' and p_web.GSV('job:initial') <> ''
          customer_name_temp = clip(p_web.GSV('job:title')) & ' ' & clip(p_web.GSV('job:initial')) & ' ' & clip(p_web.GSV('job:surname'))
      else
          customer_name_temp = ''
      end
  
      access:stantext.clearkey(stt:description_key)
      stt:description = 'ESTIMATE'
      access:stantext.fetch(stt:description_key)
  
  
      courier_cost_temp = p_web.GSV('job:courier_cost_estimate')
  
      !If booked at RRC, use RRC costs for estimate
      If p_web.GSV('jobe:WebJob') = 1
          Labour_Temp = p_web.GSV('jobe:RRCELabourCost')
          Parts_Temp  = p_web.GSV('jobe:RRCEPartsCost')
          Sub_Total_Temp = p_web.GSV('jobe:RRCELabourCost') + p_web.GSV('jobe:RRCEPartsCost') + p_web.GSV('job:Courier_Cost_Estimate')
      Else !p_web.GSV('jobe:WebJob')
          labour_temp = p_web.GSV('job:labour_cost_estimate')
          parts_temp  = p_web.GSV('job:parts_cost_estimate')
          sub_total_temp  = p_web.GSV('job:labour_cost_estimate') + p_web.GSV('job:parts_cost_estimate') + p_web.GSV('job:courier_cost_estimate')
      End !p_web.GSV('jobe:WebJob')
  
  
        ! Inserting (DBH 03/12/2007) # 8218 - Use the ARC details, if it's an RRC-ARC estimate job
        sentToHub(p_web)
        If p_web.GSV('SentToHub') = 1 And p_web.GSV('jobe:WebJob') = 1
            ! ARC Estimate, use ARC Details
            If p_web.GSV('BookingSite') <> 'RRC'
                ! Use ARC Details
                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                wob:RefNumber = p_web.GSV('job:Ref_Number')
                If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                    tra:Account_Number = wob:HeadAccountNumber
                    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                        Customer_Name_Temp = ''
                        Invoice_Company_Name_Temp = tra:Company_Name
                        Invoice_Address1_Temp = tra:Address_Line1
                        Invoice_Address2_Temp = tra:Address_Line2
                        Invoice_Address3_Temp = tra:Address_Line3
                        Invoice_Address4_Temp = tra:Postcode
                        Invoice_Telephone_Number_Temp = tra:Telephone_Number
                        Invoice_Fax_Number_Temp = tra:Fax_Number
  
                        Delivery_Company_Name_Temp = tra:Company_Name
                        Delivery_Address1_Temp = tra:Address_Line1
                        Delivery_Address2_Temp = tra:Address_Line2
                        Delivery_Address3_Temp = tra:Address_Line3
                        Delivery_Address4_Temp = tra:Postcode
                        Delivery_Telephone_Number_Temp = tra:Telephone_Number
  
                        labour_temp = p_web.GSV('job:labour_cost_estimate')
                        parts_temp  = p_web.GSV('job:parts_cost_estimate')
                        sub_total_temp  = p_web.GSV('job:labour_cost_estimate') + p_web.GSV('job:parts_cost_estimate')+ p_web.GSV('job:courier_cost_estimate')
  
                    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
            End ! If glo:WebJob
        End ! If SentToHub(p_web.GSV('job:Ref_Number) And p_web.GSV('jobe:WebJob
        ! End (DBH 03/12/2007) #8218
  
        TotalPrice(p_web,'E',vat_temp,total_temp,bal$)
  
      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
      man:Manufacturer    = p_web.GSV('job:Manufacturer')
      If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          !Found
          If ~man:UseInvTextForFaults
              tmp:InvoiceText = jbn:Invoice_Text
          Else !If ~man:UseInvTextForFaults
              tmp:InvoiceText = ''
              Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
              joo:JobNumber = p_web.GSV('job:Ref_Number')
              Set(joo:JobNumberKey,joo:JobNumberKey)
              Loop
                  If Access:JOBOUTFL.NEXT()
                     Break
                  End !If
                  If joo:JobNumber <> p_web.GSV('job:Ref_Number')      |
                      Then Break.  ! End If
                  ! Inserting (DBH 16/10/2006) # 8059 - Do not show the IMEI Validation text on the paperwork
                  If Instring('IMEI VALIDATION: ',joo:Description,1,1)
                      Cycle
                  End ! If Instring('IMEI VALIDATION: ',joo:Description,1,1)
                  ! End (DBH 16/10/2006) #8059
                  If tmp:InvoiceText = ''
                      tmp:InvoiceText = Clip(joo:FaultCode) & ' ' & Clip(joo:Description)
                  Else !If tmp:Invoice_Text = ''
                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(joo:FaultCode) & ' ' & Clip(joo:Description)
                  End !If tmp:Invoice_Text = ''
              End !Loop
  
              Access:MANFAUPA.ClearKey(map:MainFaultKey)
              map:Manufacturer = p_web.GSV('job:Manufacturer')
              map:MainFault    = 1
              If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                  !Found
                  Access:MANFAULT.ClearKey(maf:MainFaultKey)
                  maf:Manufacturer = p_web.GSV('job:Manufacturer')
                  maf:MainFault    = 1
                  If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                      !Found
                      Save_epr_ID = Access:ESTPARTS.SaveFile()
                      Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
                      epr:Ref_Number  = p_web.GSV('job:Ref_Number')
                      Set(epr:Part_Number_Key,epr:Part_Number_Key)
                      Loop
                          If Access:ESTPARTS.NEXT()
                             Break
                          End !If
                          If epr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                              Then Break.  ! End If
                          Access:MANFAULO.ClearKey(mfo:Field_Key)
                          mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                          mfo:Field_Number = maf:Field_Number
  
                          Case map:Field_Number
                              Of 1
                                  mfo:Field   = epr:Fault_Code1
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code1) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 2
                                  mfo:Field   = epr:Fault_Code2
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code2) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 3
                                  mfo:Field   = epr:Fault_Code3
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code3) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 4
                                  mfo:Field   = epr:Fault_Code4
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code4) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 5
                                  mfo:Field   = epr:Fault_Code5
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code5) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 6
                                  mfo:Field   = epr:Fault_Code6
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code6) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 7
                                  mfo:Field   = epr:Fault_Code7
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code7) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 8
                                  mfo:Field   = epr:Fault_Code8
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code8) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 9
                                  mfo:Field   = epr:Fault_Code9
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code9) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 10
                                  mfo:Field   = epr:Fault_Code10
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code10) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 11
                                  mfo:Field   = epr:Fault_Code11
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code11) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                              Of 12
                                  mfo:Field   = epr:Fault_Code12
                                  If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Found
                                      tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(epr:fault_Code12) & ' ' & Clip(mfp:Description)
                                  Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
  
                          End !Case map:Field_Number
                      End !Loop
  
                  Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
              Else!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
  
          End !If ~man:UseInvTextForFaults
      Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          !Error
      End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
  
      job_Number_Temp = locRefNumber
      esn_Temp = locESN
      Bar_Code_Temp = '*' & clip(job_Number_Temp) & '*'
      Bar_Code2_Temp = '*' & clip(esn_Temp) & '*'
  
      job_Number_temp = 'Job No: ' & clip(job_Number_Temp)
      esn_Temp = 'IMEI No: ' & clip(esn_Temp)
  !*********CHANGE LICENSE ADDRESS*********
  
  Access:TRADEACC.ClearKey(tra:Account_Number_Key)
  tra:Account_Number = p_web.GSV('BookingAccount')
  IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
  END
  
  tmp:Ref_Number = p_web.GSV('Job:Ref_Number') & '-' & |
      tra:BranchIdentification & p_web.GSV('wob:JobNumber')
  !      
  !  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  !  If (jobe:WebJob = 1)
  !      tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
  !  ELSE ! If (glo:WebJob = 1)
  !      tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  !  END ! If (glo:WebJob = 1)
  !  If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
      address:SiteName        = tra:Company_Name
      address:Name            = tra:coTradingName
      address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
      address:Location        = tra:coLocation
      address:RegistrationNo  = tra:coRegistrationNo
      address:AddressLine1    = tra:coAddressLine1
      address:AddressLine2    = tra:coAddressLine2
      address:AddressLine3    = tra:coAddressLine3
      address:AddressLine4    = tra:coAddressLine4
      address:Telephone       = tra:coTelephoneNumber
      address:Fax             = tra:coFaxNumber
      address:EmailAddress    = tra:coEmailAddress
      address:VatNumber       = tra:coVATNumber
  !  END
      jbn:Fault_Description = p_web.GSV('jbn:Fault_Description')
  
  
      
                
  
  
      If p_web.GSV('job:Estimate') = 'YES' And p_web.GSV('job:estimate_accepted') <> 'YES' and p_web.GSV('job:estimate_rejected') <> 'YES'
  !        p_web.SSV('GetStatus:Type','JOB')
  !        p_web.SSV('GetStatus:StatusNumber',520)
  !        getStatus(p_web)
  !
  !        Access:JOBS.Clearkey(job:ref_Number_Key)
  !        job:ref_Number    = p_web.GSV('job:Ref_Number')
  !        if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
  !            ! Found
  !            p_web.SessionQueueToFile(JOBS)
  !            access:JOBS.tryUpdate()
  !        else ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
  !            ! Error
  !        end ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
  !
  !
  !        Access:WEBJOB.Clearkey(wob:refNumberKey)
  !        wob:refNumber    = p_web.GSV('job:Ref_Number')
  !        if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
  !            ! Found
  !            p_web.SessionQueueToFile(WEBJOB)
  !            access:WEBJOB.tryUpdate()
  !        else ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
  !            ! Error
  !        end ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
  !
  !        p_web.SSV('AddToAudit:Type','JOB')
  !        p_web.SSV('AddToAudit:Notes','ESTIMATE VALUE: ' & Format(Total_Temp,@n14.2))
  !        p_web.SSV('AddToAudit:Action','ESTIMATE SENT')
  !        addToAudit(p_web)
  
      End!If p_web.GSV('job:estimate = 'YES' And p_web.GSV('job:estimate_accepted <> 'YES' and p_web.GSV('job:estimate_refused <> 'YES'
  
  
                SETTARGET(REPORT)
                ?TodaysDate{PROP:Text} = FORMAT(TODAY(),@d06)
                SETTARGET()
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR2.Init(Report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,Report{PROPPRINT:Paper},PAPER:USER), CHOOSE(Report{PROP:Thous}=True,PROP:Thous,CHOOSE(Report{PROP:MM}=True,PROP:MM,CHOOSE(Report{PROP:Points}=True,PROP:Points,0))), Report{PROPPRINT:PaperWidth}, Report{PROPPRINT:PaperHeight}, Report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetTitle('Estimate')                 !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR2.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR2:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR2:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR2:vpf = BOR(PDFXTR2:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR2:vpf = BOR(PDFXTR2:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR2:vpf = BOR(PDFXTR2:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR2:vpf = BOR(PDFXTR2:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR2:vpf = BOR(PDFXTR2:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR2:vpf = BOR(PDFXTR2:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetViewerPrefs(PDFXTR2:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetFontEmbedding(True, True, False, False, False)
    PDFXTR2.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'Estimate',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR2.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
        count# = 0
        FixedCharge# = 0
        Access:CHARTYPE.ClearKey(cha:Warranty_Key)
        cha:Warranty    = 'NO'
        cha:Charge_Type = p_web.GSV('job:Charge_Type')
        If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
          !Found
            If p_web.GSV('BookingSite') = 'RRC'
                If cha:Zero_Parts
                    FixedCharge# = 1
                End !If cha:Fixed_Charge
            Else !If glo:WebJob
                If cha:Zero_Parts_ARC
                    FixedCharge# = 1
                End !If cha:Fixed_Charge_ARC
            End !If glo:WebJob
        Else!If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
  
        If FixedCharge#
            SetTarget(Report)
            ?Cost_Temp{prop:Hide} = 1
            ?Line_Cost_Temp{prop:Hide} = 1
            ?UnitCost{prop:Hide} = 1
            ?LineCost{prop:Hide} = 1
            SetTarget()
        End !FixedCharge#
  
        access:estparts.clearkey(epr:part_number_key)
        epr:ref_number  = p_web.GSV('job:ref_number')
        set(epr:part_number_key,epr:part_number_key)
        loop
            if access:estparts.next()
                break
            end !if
            if epr:ref_number  <> p_web.GSV('job:ref_number')      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
                yield() ; yldcnt# = 0
            end !if
            count# += 1
            part_number_temp = epr:part_number
            description_temp = epr:description
            quantity_temp = epr:quantity
            cost_temp = epr:sale_cost
            line_cost_temp = epr:quantity * epr:sale_cost
            Print(rpt:detail)
  
        end !loop
  
        If count# = 0
            SetTarget(Report)
            ?PartsHeadingGroup{Prop:Hide} = 1
            SetTarget()
            Part_Number_Temp = ''
          !Print Blank Line
            Print(Rpt:Detail)
        End!If count# = 0  
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR2:rtn = PDFXTR2.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR2:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

