

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER131.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
SendSpecificNeedsEmail PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
locGenericFault             STRING(30)
gE                          GROUP(gEmail),PRE(gE).
FilesOpened     BYTE(0)
    MAP
trace	PROCEDURE(STRING pText)	
    END

  CODE
    DO OpenFiles
	
    Access:MANFAULT.ClearKey(maf:Field_Number_Key)
    maf:Manufacturer = p_web.GSV('job:Manufacturer')
    maf:Field_Number = 0
    SET(maf:Field_Number_Key,maf:Field_Number_Key)
    LOOP UNTIL Access:MANFAULT.Next() <> Level:Benign
        IF (maf:Manufacturer <> p_web.GSV('job:Manufacturer'))
            BREAK
        END ! IF
        IF (maf:GenericFault)
            IF (maf:Field_Number < 13)
                locGenericFault = p_web.GSV('job:Fault_Code' & maf:Field_Number)
            ELSE ! IF
                locGenericFault = p_web.GSV('wob:FaultCode' & maf:Field_Number)
            END ! IF
            BREAK
        END ! IF
    END ! LOOP
    IF (locGenericFault = '')
        locGenericFault = 'NOT_SET'
    END ! IF
        
		
    Access:DEFAULT2.ClearKey(de2:RecordNumberKey)
    Set(de2:RecordNumberKey,de2:RecordNumberKey)
    Access:Default2.next()  !gets the default from address
    trace('Default2 Error: ' & ERROR())
        
    CLEAR(gE)
    gE.EmailFrom = de2:DefaultFromEmail
    gE.EmailTo = p_web.GSV('BookingEmailAddress')
    gE.EmailSubject = 'Specific Needs Device '&p_web.GSV('job:Ref_Number')
    gE.EmailMessageText = 'Job number '               &   p_web.GSV('job:Ref_Number')            &|
        '<13,10>Imei number '       &   p_web.GSV('job:ESN')                   &|
        '<13,10>Manufacturer '      &   p_web.GSV('job:Manufacturer')          &|
        '<13,10>Model number '      &   p_web.GSV('job:Model_Number')          &|
        '<13,10>Generic fault code '&   CLIP(locGenericFault)          &|
        '<13,10>Date booked '       &   FORMAT(p_web.GSV('job:date_booked'),@d06)    &|
        '<13,10>Time booked '       &   FORMAT(p_web.GSV('job:time_booked'),@t04)    &|
        '<13,10>Current status '    &   p_web.GSV('job:Current_Status')
			
    DO CloseFiles
        
    RESUME(START(SendEmail,25000,gE)) ! Send email on thread.
!--------------------------------------
OpenFiles  ROUTINE
  Access:DEFAULT2.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULT2.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:DEFAULT2.Close
     Access:MANFAULT.Close
     FilesOpened = False
  END
trace	PROCEDURE(STRING pText)
outString	CSTRING(1000)
    CODE
        RETURN ! Logging Disabled
