

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER078.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! IMEI History
!!! </summary>
CountHistory         PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
save_job2_id         USHORT,AUTO                           !
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
FilesOpened     BYTE(0)

  CODE
    p_web.SSV('CountHistory',0)
    do openFiles
    Count# = 0

    Save_job2_ID = Access:JOBS2_ALIAS.SaveFile()
    Access:JOBS2_ALIAS.ClearKey(job2:ESN_Key)
    job2:ESN = p_web.GSV('wob:IMEINumber')
    Set(job2:ESN_Key,job2:ESN_Key)
    Loop
        If Access:JOBS2_ALIAS.NEXT()
           Break
        End !If
        If job2:ESN <> p_web.GSV('wob:IMEINumber')      |
            Then Break.  ! End If

        If job2:Cancelled = 'YES'
            Cycle
        End !If job2:Cancelled = 'YES'

        if job2:ref_number = p_web.GSV('wob:RefNumber')
            Cycle
        end ! if job2:ref_number >= p_web.GSV('wob:RefNumber')

        count# += 1
    End !Loop
    Access:JOBS2_ALIAS.RestoreFile(Save_job2_ID)

    do closeFiles

    p_web.SSV('CountHistory',count#)

!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBS2_ALIAS.Open                                  ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS2_ALIAS.UseFile                               ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBS2_ALIAS.Close
     FilesOpened = False
  END
