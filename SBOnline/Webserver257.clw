

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER257.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! Keeps the Unique bit of a printing URL unique
!!! </summary>
UpdateUniqueBitText  PROCEDURE  (NetWebServerWorker p_Web) ! Declare Procedure
UniqueBitLong        LONG                                  !
UniqueBitText        STRING(10)                            !

  CODE
  !How this all works

  !When you want a report called "PrintThis" to be run from a button you normally set the URL on the button to 'PrintThis'
  !change the URL to 
  !'PrintThis?var=' & p_web.gsv('UniqueBitText')
  
  !this session variable is set up on the indexpage to be '0000000011'

  !on the button calling the report add to the servercode a call to this routine UpdateUniqueBitText(p_web)
  !don't forget to tick the "Send new value to Server" box!
  
  !and this will give a unique number for this session
  !you can use this up to a billion times this session! That would take about 3000 years to run so we should be safe

  UniqueBitLong = deformat(p_web.gsv('UniqueBitText'))
  UniqueBitLong = UniqueBitLong + 1
  UniqueBitText = Format(UniqueBitLong,@n010)
  p_web.ssv('UniqueBitText',UniqueBitText)
