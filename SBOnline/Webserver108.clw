

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER108.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
CheckFaultFormat     PROCEDURE  (STRING func:FaultCode, STRING func:FieldFormat) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
tmp:MonthStart       LONG                                  !Month Start
tmp:MonthValue       LONG                                  !Month Value
tmp:YearStart        LONG                                  !Year Start
tmp:YearValue        LONG                                  !Year Value
tmp:StartQuotes      LONG                                  !Start Quotes
tmp:FaultYearStart   LONG                                  !Fault Year Start
tmp:FaultMonthStart  LONG                                  !Fault Month Start

  CODE
    y# = 1
    tmp:MonthStart = 0
    tmp:YearStart = 0
    Loop x# = 1 To Len(Clip(func:FieldFormat))
        !If this is in a quotes just do a straight comparison of characters
        If tmp:StartQuotes
            If Sub(func:FieldFormat,x#,1) = '"'
                !End Of The Quotes
                tmp:StartQuotes = 0
                Cycle
            End !If Sub(func:FieldFormat,x#,1) = '"'
            If Sub(func:FieldFormat,x#,1) <> Sub(func:FaultCode,y#,1)
                Return Level:Fatal
            Else! !If Sub(func:FieldFormat,x#,1) <> Sub(func:FaultCode,x#,1)
                y# += 1
                Cycle
            End !If Sub(func:FieldFormat,x#,1) <> Sub(func:FaultCode,x#,1)

        End !If tmp:StartQuotes

        If tmp:YearStart
            If Sub(func:FieldFormat,x#,1) <> 'Y'
                If x# - tmp:YearStart = 4
                    If Sub(func:FaultCode,tmp:FaultYearStart,4) < 1990 Or Sub(func:FaultCode,tmp:FaultYearStart,4) > 2010
                        !Failed Year
                        Return Level:Fatal
                    End !If Sub(func:FaultCode,tmp:YearStart,4) < 1990 Or Sub(func:FaultCode,tmp:YearStart,4) > 2010
                End !If x# - tmp:YearStart = 4

                If x# - tmp:YearStart = 2
                    If Sub(func:FaultCode,tmp:FaultYearStart,2) > 10 And Sub(func:FaultCode,tmp:FaultYearStart,2) < 90
                        !Failed Year
                        Return Level:Fatal
                    End !If Sub(func:FaultCode,tmp:YearStart,4) < 1990 Or Sub(func:FaultCode,tmp:YearStart,4) > 2010
                End !If x# - tmp:YearStart = 2
                tmp:YearStart = 0
                y# += 1
            Else !If Sub(func:FieldFormat,x#,1) <> 'Y'
                Cycle
            End !If Sub(func:FieldFormat,x#,1) <> 'Y'
        End !If tmp:YearStart

        If tmp:MonthStart
            If Sub(func:FieldFormat,x#,1) <> 'M'
                If Sub(func:FaultCode,tmp:FaultMonthStart,2) < 1 Or Sub(func:FaultCode,tmp:FaultMonthStart,2) > 12
                    !Month Fail
                    Return Level:Fatal
                End !If Sub(func:FaultCode,tmp:FaultMonthStart,2) < 1 Or Sub(func:FaultCode,tmp:FaultMonthStart,2) > 12
                tmp:MonthStart = 0
                y# += 1
            Else !If Sub(func:FieldFormat,x#,1) <> 'M'
                Cycle
            End !If Sub(func:FieldFormat,x#,1) <> 'M'
        End !If tmp:MonthStart

        Case Sub(func:FieldFormat,x#,1)
            Of 'A'
                If Val(Sub(func:FaultCode,y#,1)) < 65 Or Val(Sub(func:FaultCode,y#,1)) > 90
                    !Alpha Error
                    Return Level:Fatal
                End !If Val(Sub(func:FaultCode,x#,1)) < 65 Or Val(Sub(func:FaultCode,x#,1)) > 90
            Of '0'
                If Val(Sub(func:FaultCode,y#,1)) < 48 Or Val(Sub(func:FaultCode,y#,1)) > 57
                    !Numeric Error

                    Return Level:Fatal
                End !If Val(Sub(func:FaultCode,x#,1)) < 48 Or Val(Sub(func:FaultCode,x#,1)) > 57
            Of 'X'
                If ~((Val(Sub(func:FaultCode,y#,1)) >=48 And Val(Sub(func:FaultCode,y#,1)) <= 57) Or |
                    (Val(Sub(func:FaultCode,y#,1)) >=65 And Val(Sub(func:FaultCode,y#,1)) <=90))

                    !Alphanumeric Error
                    Return Level:Fatal
                End !(Val(Sub(func:FaultCode,x#,1)) >=65 And Val(Sub(func:FaultCode,x#,1)) <=90))
            Of '"'
                tmp:StartQuotes = 1
                Cycle
            Of 'Y'
                tmp:YearStart   = x#
                tmp:FaultYearStart  = y#
            Of 'M'
                tmp:MonthStart  = x#
            tmp:FaultMonthStart = y#
        Of '%'
                ! A % has been found. Check that all the remaining digits are numbers (DBH: 30/10/2007)
            Loop z# = y# To Len(Clip(func:FaultCode))
                If Val(Sub(func:FaultCode,z#,1)) < 48 Or Val(Sub(func:FaultCode,z#,1)) > 57
                        !Numeric Error
                    Return Level:Fatal
                End !If Val(Sub(func:FaultCode,x#,1)) < 48 Or Val(Sub(func:FaultCode,x#,1)) > 57
            End ! Loop Until y# = Len(Clip(func:FaultCode))
            Return Level:Benign

        Of '$'
            Loop z# = y# To Len(Clip(func:FaultCode))
                If Val(Sub(func:FaultCode,z#,1)) < 65 Or Val(Sub(func:FaultCode,z#,1)) > 90
                        !Numeric Error
                    Return Level:Fatal
                End !If Val(Sub(func:FaultCode,x#,1)) < 48 Or Val(Sub(func:FaultCode,x#,1)) > 57
            End ! Loop Until y# = Len(Clip(func:FaultCode))
            Return Level:Benign

        Of '*'
            Loop z# = y# To Len(Clip(func:FaultCode))
                If ~((Val(Sub(func:FaultCode,z#,1)) >= 65 AND Val(Sub(func:FaultCode,z#,1)) <= 90) OR |
                    (Val(Sub(func:FaultCode,z#,1)) >= 48 AND Val(Sub(func:FaultCode,z#,1)) <= 57))
                        !Numeric Error
                    Return Level:Fatal
                End !If Val(Sub(func:FaultCode,x#,1)) < 48 Or Val(Sub(func:FaultCode,x#,1)) > 57
            End ! Loop Until y# = Len(Clip(func:FaultCode))            
            Return Level:Benign            
        End !Case Sub(func:FaultCode,x#,1)
        y# += 1
    End !Loop x# = 1 To Len(func:FaultCode)

    If tmp:YearStart

        If x# - tmp:YearStart = 4
            If Sub(func:FaultCode,tmp:FaultYearStart,4) < 1990 Or Sub(func:FaultCode,tmp:FaultYearStart,4) > 2010
                !Failed Year
                Return Level:Fatal
            End !If Sub(func:FaultCode,tmp:YearStart,4) < 1990 Or Sub(func:FaultCode,tmp:YearStart,4) > 2010
        End !If x# - tmp:YearStart = 4

        If x# - tmp:YearStart = 2
            If Sub(func:FaultCode,tmp:FaultYearStart,2) < 90 And Sub(func:FaultCode,tmp:FaultYearStart,2) > 10
                !Failed Year
                Return Level:Fatal
            End !If Sub(func:FaultCode,tmp:YearStart,4) < 1990 Or Sub(func:FaultCode,tmp:YearStart,4) > 2010
        End !If x# - tmp:YearStart = 2
    End !If tmp:YearStart

        If tmp:MonthStart
            If Sub(func:FaultCode,tmp:FaultMonthStart,2) <1 Or Sub(func:FaultCode,tmp:FaultMonthStart,2) > 12
            !Month Fail
                Return Level:Fatal
            End !If Sub(func:FaultCode,tmp:FaultMonthStart,2) < 1 Or Sub(func:FaultCode,tmp:FaultMonthStart,2) > 12
        End !If tmp:MonthStart


    Return Level:Benign



