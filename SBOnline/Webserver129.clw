

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER129.INC'),ONCE        !Local module procedure declarations
                     END


AllocateEngineer     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:EngineerPassword STRING(30)                            !Engineer Password
FilesOpened     Long
USERS::State  USHORT
LOCATLOG::State  USHORT
JOBSENG::State  USHORT
JOBS::State  USHORT
WEBJOB::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('AllocateEngineer')
  loc:formname = 'AllocateEngineer_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'AllocateEngineer',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('AllocateEngineer','')
    p_web._DivHeader('AllocateEngineer',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferAllocateEngineer',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAllocateEngineer',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAllocateEngineer',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_AllocateEngineer',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAllocateEngineer',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_AllocateEngineer',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'AllocateEngineer',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  p_web._OpenFile(LOCATLOG)
  p_web._OpenFile(JOBSENG)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(WEBJOB)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(LOCATLOG)
  p_Web._CloseFile(JOBSENG)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WEBJOB)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
      p_web.SSV('comment:EngineerPassword','Enter password and press [TAB]')
      p_web.SSV('tmp:NewEngineer','')
      p_web.SSV('tmp:EngineerPassword','')
      p_web.SSV('locEngResults','')
  
      IF (p_web.GSV('jobe:Booking48HourOption') = 4 AND p_web.GSV('BookingSite') = 'ARC' AND p_web.GSV('job:Third_Party_Site') = '')
          p_web.SSV('Force3rdParty',1)
          p_web.SSV('locEngResults','Liquid Damage Handset! Send to 3rd Party for repairs.')
          ! DBH #10544 - Change Stauts to 3rd party.
          IF (SUB(p_web.GSV('job:Current_Status'),1,3) <> '405')
  !              p_web.SSV('AddToAudit:Type','JOB')
  !              p_web.SSV('AddToAudit:Action','STATUS CHANGED TO SEND TO 3RD PARTY')
              AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','STATUS CHANGED TO SEND TO 3RD PARTY','')
              
              p_web.SSV('GetStatus:Type','JOB')
              p_web.SSV('GetStatus:StatusNumber','405')
              GetStatus(405,0,'JOB',p_web)
          
              Access:JOBS.Clearkey(job:Ref_Number_Key)
              job:Ref_Number = p_web.GSV('wob:RefNumber')
              If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                  p_web.SessionQueueToFile(JOBS)
                  Access:JOBS.TryUpdate()
              End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
  
              Access:WEBJOB.Clearkey(wob:RefNumberKey)
              wob:RefNumber    = p_web.GSV('wob:RefNumber')
              if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                  ! Found
                  p_web.SessionQueueToFile(WEBJOB)
                  Access:WEBJOB.TryUpdate()
              else ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                  ! Error
              end ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)        
          END !IF (SUB(p_web.GSV('job:Current_Status'),1,3) <> '405')        
      ELSE
          p_web.SSV('Force3rdParty',0)
      END
  p_web.SetValue('AllocateEngineer_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('Force3rdParty') <> 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:EngineerPassword',tmp:EngineerPassword)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:EngineerPassword')
    tmp:EngineerPassword = p_web.GetValue('tmp:EngineerPassword')
    p_web.SetSessionValue('tmp:EngineerPassword',tmp:EngineerPassword)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('AllocateEngineer_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:EngineerPassword = p_web.RestoreValue('tmp:EngineerPassword')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('AllocateEngineer_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('AllocateEngineer_ChainTo')
    loc:formaction = p_web.GetSessionValue('AllocateEngineer_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="AllocateEngineer" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="AllocateEngineer" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="AllocateEngineer" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Allocate Engineer') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Allocate Engineer',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_AllocateEngineer">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_AllocateEngineer" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_AllocateEngineer')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GSV('Force3rdParty') <> 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Enter Engineer Password') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_AllocateEngineer')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_AllocateEngineer'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('Force3rdParty') <> 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:EngineerPassword')
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GSV('Force3rdParty') <> 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_AllocateEngineer')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GSV('Force3rdParty') <> 1
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GSV('Force3rdParty') <> 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Enter Engineer Password') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_AllocateEngineer_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Engineer Password')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Engineer Password')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Engineer Password')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Engineer Password')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:EngineerPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:EngineerPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:EngineerPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_AllocateEngineer_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::SaveMessage
      do Comment::SaveMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::tmp:EngineerPassword  Routine
  p_web._DivHeader('AllocateEngineer_' & p_web._nocolon('tmp:EngineerPassword') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Engineer Password')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:EngineerPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:EngineerPassword',p_web.GetValue('NewValue'))
    tmp:EngineerPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:EngineerPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:EngineerPassword',p_web.GetValue('Value'))
    tmp:EngineerPassword = p_web.GetValue('Value')
  End
  If tmp:EngineerPassword = ''
    loc:Invalid = 'tmp:EngineerPassword'
    loc:alert = p_web.translate('Engineer Password') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
      p_web.SSV('tmp:NewEngineer','')
      error# = 0
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = p_web.GSV('tmp:EngineerPassword')
      If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
          If use:User_Code <> p_web.GSV('job:Engineer')
              If use:User_Type <> 'ENGINEER'
                  p_web.SSV('comment:EngineerPassword','The selected password is not an Engineer.')
                  error# = 1
                  
              Else ! If use:User_Type <> 'ENGINEER'
                  ! Does the engineer match the site of where the job was booked (DBH: 14/12/2007)
                  If use:Location <> p_web.GSV('JobBookingSiteLocation')
                      If p_web.GSV('BookingSite') = 'RRC' Or (p_web.GSV('BookingSite') = 'ARC' And p_web.GSV('jobe:HubRepair') = 0)
                          p_web.SSV('comment:EngineerPassword','The selected engineer cannot be allocated to this job.')
                          error# = 1
                      End ! If p_web.GSV('BookingSite') <> 'ARC'
                  Else ! If use:Location <> p_web.GSV('BookingSiteLocation')
                      If use:Location <> p_web.GSV('BookingSiteLocation')
                          p_web.SSV('comment:EngineerPassword','The selected engineer is not available for this site.')
                          error# = 1
                      Else ! If use:Location <> p_web.GSV('BookingSiteLocation')
                          !If at ARC, check if job has gone through waybill generation
                          If p_web.GSV('BookingSite') = 'ARC'
                              Access:LOCATLOG.Clearkey(lot:NewLocationKey)
                              lot:RefNumber = job:Ref_Number
                              lot:NewLocation = Clip(GETINI('RRC','ARCLocation','RECEIVED AT ARC',Clip(Path()) & '\SB2KDEF.INI'))
                              If Access:LOCATLOG.TryFetch(lot:NewLocationKey)
                                  p_web.SSV('comment:EngineerPassword','This job is not at the ARC')
                                  error# = 1
                              End ! If Access:LOCATLOG.TryFetch(lot:NewLocationKey)
                          End ! If p_web.GSV('BookingSite') = 'ARC'
                      End ! If use:Location <> p_web.GSV('BookingSiteLocation')
                  End ! If use:Location <> p_web.GSV('BookingSiteLocation')
  
                  if (error# = 0)
                      p_web.SSV('tmp:NewEngineer',use:User_Code)
                      p_web.SSV('locEngResults','Engineer ' & clip(use:Forename) & ' ' & clip(use:Surname) & ' will be allocated to this job when you click ''Save''')
                  else
                      p_web.SSV('tmp:NewEngineer','')
                      p_web.SSV('locEngResults','')
                  end ! if (error# = 0)
              End !
          Else ! If use:User_Code <> p_web.GSV('job:Engineer')
              ! Picked the same engineer. Do nothing (DBH: 14/12/2007)
              p_web.SSV('tmp:NewEngineer','')
              p_web.SSV('comment:EngineerPassword','Engineer already attached to this job')
          End ! If use:User_Code <> p_web.GSV('job:Engineer')
      Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
          p_web.SSV('tmp:NewEngineer','')
          p_web.SSV('comment:EngineerPassword','Invalid password')
      End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
  do Value::tmp:EngineerPassword
  do SendAlert
  do Comment::tmp:EngineerPassword
  do Value::SaveMessage  !1

Value::tmp:EngineerPassword  Routine
  p_web._DivHeader('AllocateEngineer_' & p_web._nocolon('tmp:EngineerPassword') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:EngineerPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:EngineerPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:EngineerPassword = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:EngineerPassword'',''allocateengineer_tmp:engineerpassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:EngineerPassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','tmp:EngineerPassword',p_web.GetSessionValueFormat('tmp:EngineerPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AllocateEngineer_' & p_web._nocolon('tmp:EngineerPassword') & '_value')

Comment::tmp:EngineerPassword  Routine
    loc:comment = p_web.Translate(p_web.GSV('comment:EngineerPassword'))
  p_web._DivHeader('AllocateEngineer_' & p_web._nocolon('tmp:EngineerPassword') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AllocateEngineer_' & p_web._nocolon('tmp:EngineerPassword') & '_comment')

Validate::SaveMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('SaveMessage',p_web.GetValue('NewValue'))
    do Value::SaveMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::SaveMessage  Routine
  p_web._DivHeader('AllocateEngineer_' & p_web._nocolon('SaveMessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('locEngResults'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AllocateEngineer_' & p_web._nocolon('SaveMessage') & '_value')

Comment::SaveMessage  Routine
    loc:comment = ''
  p_web._DivHeader('AllocateEngineer_' & p_web._nocolon('SaveMessage') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('AllocateEngineer_tmp:EngineerPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:EngineerPassword
      else
        do Value::tmp:EngineerPassword
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('AllocateEngineer_form:ready_',1)
  p_web.SetSessionValue('AllocateEngineer_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_AllocateEngineer',0)

PreCopy  Routine
  p_web.SetValue('AllocateEngineer_form:ready_',1)
  p_web.SetSessionValue('AllocateEngineer_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_AllocateEngineer',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('AllocateEngineer_form:ready_',1)
  p_web.SetSessionValue('AllocateEngineer_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('AllocateEngineer:Primed',0)

PreDelete       Routine
  p_web.SetValue('AllocateEngineer_form:ready_',1)
  p_web.SetSessionValue('AllocateEngineer_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('AllocateEngineer:Primed',0)
  p_web.setsessionvalue('showtab_AllocateEngineer',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('Force3rdParty') <> 1
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
      IF (p_web.GSV('Force3rdParty') = 1)
  
      ELSE
          
  
          if (p_web.GSV('tmp:NewEngineer') = '')
              loc:Alert = 'You must select a new engineer'
              loc:Invalid = 'tmp:EngineerPassword'
              Exit
          else ! if (p_web.GSV('tmp:NewEngineer') = '')
          
              Access:USERS.Clearkey(use:User_Code_Key)
              use:User_Code    = p_web.GSV('tmp:NewEngineer')
              if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
              ! Found
  
                  engChanged# = TRUE    
                  IF (p_web.GSV('job:Engineer') = '')
  !                      p_web.SSV('AddToAudit:Type','JOB')
  !                      p_web.SSV('AddToAudit:Action','ENGINEER ALLOCATED: ' & p_web.GSV('tmp:NewEngineer'))
  !                      p_web.SSV('AddToAudit:Notes',Clip(use:Forename) & ' ' & Clip(use:Surname) & ' - ' | 
  !                          & Clip(use:Location) & ' - LEVEL ' & Clip(use:SkillLevel))
                        AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','ENGINEER ALLOCATED: ' & p_web.GSV('tmp:NewEngineer'),Clip(use:Forename) & ' ' & Clip(use:Surname) & ' - ' | 
                            & Clip(use:Location) & ' - LEVEL ' & Clip(use:SkillLevel))               
                      engChanged# = FALSE
                  END
              
                  p_web.SSV('job:Engineer',use:User_Code)
                  p_web.SSV('EngineerAllocated',Today())
  
                  p_web.SSV('ChangesSaved',1)
  
                  If p_web.GSV('BookingSite') = 'RRC' Or |
                      (p_web.GSV('BookingSite') = 'ARC' And p_web.GSV('jobe:WebJob') = 0)
                      If p_web.GSV('jobe2:SMSNotification')
                          AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('wob:HeadAccountNumber'),'2ENG','SMS',p_web.GSV('jobe2:SMSAlertNumber'),'',0,'')
                      End ! If p_web.GSV('jobe2:SMSNotification')
                      If p_web.GSV('jobe2:EmailNotification')
                          AddEmailSMS(p_web.GSV('job:Ref_Number'),p_web.GSV('wob:HeadAccountNumber'),'2ENG','EMAIL',p_web.GSV('jobe2:EMailAlertAddress'),'',0,'')
                      End ! If p_web.GSV('jobe2:EmailNotification')
  
          !                      If AddToAudit(p_web.GSV('job:Ref_Number'),'JOB','ENGINEER ALLOCATION: ' & Clip(p_web.GSV('job:Engineer')), Clip(use:Forename) & ' ' & Clip(use:Surname) & ' - ' & Clip(use:Location) & ' - LEVEL ' & Clip(use:SkillLevel))
          !
          !                      End ! AddToAudit
                      If Access:JOBSENG.PrimeRecord() = Level:Benign
                          joe:JobNumber     = p_web.GSV('job:Ref_Number')
                          joe:UserCode      = p_web.GSV('job:Engineer')
                          joe:DateAllocated = Today()
                          joe:TimeAllocated = Clock()
                          joe:AllocatedBy   = p_web.GSV('BookingUserCode')
                          joe:Status        = 'ALLOCATED'
                          joe:StatusDate    = Today()
                          joe:StatusTime    = Clock()
                          joe:EngSkillLevel = use:SkillLevel
                          joe:JobSkillLevel = p_web.GSV('jobe:SkillLevel')
                          If Access:JOBSENG.TryInsert() = Level:Benign
  
                          Else ! If Access:JOBSENG.TryInsert() = Level:Benign
                              Access:JOBSENG.CancelAutoInc()
                          End ! If Access:JOBSENG.TryInsert() = Level:Benign
                      End ! If Access:JOBSENG.PrimeRecord() = Level:Benign
                      p_web.SSV('GetStatus:Type','JOB')
                      p_web.SSV('GetStatus:StatusNumber','310')
  
                      GetStatus(310,0,'JOB',p_web)
                  End ! p_web.GSV('jobe:WebJob') = 0)
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number = p_web.GSV('wob:RefNumber')
                  If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                  ! #11300 DBH - Record if the engineer has changed.
                      IF (engChanged# = TRUE)
  !                          p_web.SSV('AddToAudit:Type','JOB')
  !                          p_web.SSV('AddToAudit:Action','ENGINEER CHANGED TO ' & p_web.GSV('job:Engineer'))
  !                          p_web.SSV('AddToAudit:Notes','NEW ENGINEER: ' & p_web.GSV('job:Engineer') & |
  !                              '<13,10>SKILL LEVEL: ' & use:SkillLevel & |
  !                              '<13,10,13,10>PREVIOUS ENGINEER: ' & CLip(job:Engineer))
                            AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','ENGINEER CHANGED TO ' & p_web.GSV('job:Engineer'),'NEW ENGINEER: ' & p_web.GSV('job:Engineer') & |
                                '<13,10>SKILL LEVEL: ' & use:SkillLevel & |
                                '<13,10,13,10>PREVIOUS ENGINEER: ' & CLip(job:Engineer))
                      END                
                      p_web.SessionQueueToFile(JOBS)
                      Access:JOBS.TryUpdate()
                  End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
  
                  Access:WEBJOB.Clearkey(wob:RefNumberKey)
                  wob:RefNumber    = p_web.GSV('wob:RefNumber')
                  if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                  ! Found
                      p_web.SessionQueueToFile(WEBJOB)
                      Access:WEBJOB.TryUpdate()
                  else ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                  ! Error
                  end ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
           
              else ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
              ! Error
                  loc:Alert = 'An error occurred retrieving the selected engineer''s details.'
                  loc:Invalid = 'tmp:EngineerPassword'
                  Exit
              end ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
          end !if (p_web.GSV('tmp:NewEngineer') = '')
      END
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('AllocateEngineer_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('AllocateEngineer_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
  If p_web.GSV('Force3rdParty') <> 1
    loc:InvalidTab += 1
        If tmp:EngineerPassword = ''
          loc:Invalid = 'tmp:EngineerPassword'
          loc:alert = p_web.translate('Engineer Password') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
  End
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('AllocateEngineer:Primed',0)
  p_web.StoreValue('tmp:EngineerPassword')
  p_web.StoreValue('')
