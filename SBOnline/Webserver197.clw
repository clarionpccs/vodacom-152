

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER197.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! Report the LOANAUI File
!!! </summary>
LoanAuditReport PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
tmp:DefaultTelephone STRING(30)                            !
tmp:DefaultFax       STRING(30)                            !
Make_Model           STRING(60)                            !
Total_No_Of_Lines    LONG                                  !
locAuditNumber       LONG                                  !
Process:View         VIEW(LOANAUI)
                       PROJECT(lau:Audit_Number)
                       PROJECT(lau:Confirmed)
                       PROJECT(lau:Internal_No)
                       PROJECT(lau:Shelf_Location)
                       PROJECT(lau:Site_Location)
                       PROJECT(lau:Stock_Type)
                     END
ProgressWindow       WINDOW('Report LOANAUI'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT,AT(396,2802,7521,7990),PRE(RPT),PAPER(PAPER:A4),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,531,7521,1802),USE(?unnamed:2)
                         STRING('LOAN AUDIT REPORT'),AT(4948,573),USE(?String3),FONT('Arial',8,,,CHARSET:ANSI),TRN
                         STRING('<<-- Date Stamp -->'),AT(5792,1333,927,135),USE(?ReportDateStamp:2),FONT('Arial',8, |
  ,FONT:regular),TRN
                         STRING('Date Printed:'),AT(4948,1302),USE(?String16),FONT('Arial',8,,,CHARSET:ANSI),TRN
                         STRING('String 21'),AT(3656,0,3438,260),USE(?String21),FONT('Arial',14,,FONT:bold),RIGHT(10), |
  TRN
                         STRING(@pPage <<#p),AT(4948,1500,700,135),USE(?PageCount:2),FONT('Arial',8,,FONT:regular), |
  PAGENO,TRN
                         STRING(@s30),AT(94,52),USE(def:User_Name),FONT(,14,,FONT:bold),TRN
                         STRING(@s30),AT(94,313,3531,198),USE(def:Address_Line1),FONT(,9),TRN
                         STRING(@s30),AT(94,469,3531,198),USE(def:Address_Line2),FONT(,9),TRN
                         STRING(@s30),AT(94,625,3531,198),USE(def:Address_Line3),FONT(,9),TRN
                         STRING(@s15),AT(94,781),USE(def:Postcode),FONT(,9),TRN
                         STRING('Tel: '),AT(94,990),USE(?String15),FONT(,9),TRN
                         STRING(@s20),AT(563,990),USE(tmp:DefaultTelephone),FONT(,9),TRN
                         STRING('Fax:'),AT(94,1146),USE(?String16:9),FONT(,9),TRN
                         STRING(@s20),AT(563,1146),USE(tmp:DefaultFax),FONT(,9),TRN
                         STRING('Email:'),AT(104,1302),USE(?String16:4),FONT(,9),TRN
                         STRING(@s255),AT(573,1302,3531,198),USE(def:EmailAddress),FONT(,9),TRN
                       END
DETAIL                 DETAIL,AT(0,0,,167),USE(?DetailBand)
                         STRING(@s30),AT(104,10),USE(loa:Stock_Type),FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(5104,10),USE(loa:ESN),FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s60),AT(2438,10),USE(Make_Model),FONT('Arial',8,,,CHARSET:ANSI)
                       END
detail1                DETAIL,AT(0,0,,52),USE(?unnamed:6),PAGEAFTER(1)
                       END
detail2                DETAIL,AT(0,0,,260),USE(?unnamed:7),PAGEAFTER(1)
                         STRING('NO ITEMS TO REPORT'),AT(115,42,7094,208),USE(?String25),FONT(,12,,FONT:bold),CENTER, |
  TRN
                       END
                       FOOTER,AT(385,10729,7583,667),USE(?unnamed:4)
                         LINE,AT(115,42,2146,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('Total Number Of Lines:'),AT(104,104),USE(?String29),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING(@n-10),AT(1625,104),USE(Total_No_Of_Lines),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  RIGHT(1),TRN
                       END
                       FORM,AT(365,510,7521,10802),USE(?unnamed)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,10802),USE(?Image1)
                         STRING('Stock Type'),AT(135,1979),USE(?String5),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Make/Model'),AT(2469,1979),USE(?String6),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('IMEI'),AT(5135,1979),USE(?String31),FONT('Arial',8,,FONT:bold),TRN
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR3               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
EndReport              PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.EndReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
        IF (Total_No_Of_Lines = 0)
            IF (p_web.GSV('ShortagePage') = 1)
  				! There was no shortages or excess?!
                PRINT(rpt:Detail2)
            END ! IF
            SETTARGET(REPORT)
            ?string21{prop:Text} = 'EXCESSES'
            SETTARGET()
            PRINT(rpt:Detail2)
        ELSE ! IF
            IF (p_web.GSV('ShortagePage') = 1)
  				! There was no excess
                PRINT(rpt:Detail1)
                SETTARGET(REPORT)
                ?string21{prop:Text} = 'EXCESSES'
                SETTARGET()
                PRINT(rpt:Detail2)
            ELSE
                PRINT(rpt:Detail1)
            END ! IF
        END ! IF
  ReturnValue = PARENT.EndReport()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('LoanAuditReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:LOAN.SetOpenRelated()
  Relate:LOAN.Open                                         ! File LOAN used by this procedure, so make sure it's RelationManager is open
  Relate:TRADEACC.SetOpenRelated()
  Relate:TRADEACC.Open                                     ! File TRADEACC used by this procedure, so make sure it's RelationManager is open
  Access:LOANAMF.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
        IF (p_web.IfExistsValue('lmf:Audit_Number'))
            Access:LOANAMF.ClearKey(lmf:Audit_Number_Key)
            lmf:Audit_Number = p_web.GetValue('lmf:Audit_Number')
            IF (Access:LOANAMF.TryFetch(lmf:Audit_Number_Key) = Level:Benign)
                p_web.FileToSessionQueue(LOANAMF)
            END ! IF
        END ! IF
        
        locAuditNumber = p_web.GSV('lmf:Audit_Number')
  		
        p_web.SSV('ShortagePage',1)
  		
  Do DefineListboxStyle
  INIMgr.Fetch('LoanAuditReport',ProgressWindow)           ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:LOANAUI, ?Progress:PctText, Progress:Thermometer, ProgressMgr, lau:Confirmed)
  ThisReport.AddSortOrder(lau:Main_Browse_Key)
  ThisReport.AddRange(lau:Audit_Number,locAuditNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:LOANAUI.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:LOAN.Close
    Relate:TRADEACC.Close
  END
  IF SELF.Opened
    INIMgr.Update('LoanAuditReport',ProgressWindow)        ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'LoanAuditReport',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
        SETTARGET(REPORT)
        ?String21{PROP:Text} = 'SHORTAGES'
        SETTARGET()   
        
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('BookingAccount')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            def:User_Name           = tra:Company_Name
            def:Address_Line1       = tra:Address_Line1
            def:Address_Line2       = tra:Address_Line2
            def:Address_Line3       = tra:Address_Line3
            def:Postcode            = tra:Postcode
            tmp:DefaultTelephone    = tra:Telephone_Number
            tmp:DefaultFax          = tra:Fax_Number
            def:EmailAddress        = tra:EmailAddress			
        ELSE ! IF
        END ! IF 
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.Init(Report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,Report{PROPPRINT:Paper},PAPER:USER), CHOOSE(Report{PROP:Thous}=True,PROP:Thous,CHOOSE(Report{PROP:MM}=True,PROP:MM,CHOOSE(Report{PROP:Points}=True,PROP:Points,0))), Report{PROPPRINT:PaperWidth}, Report{PROPPRINT:PaperHeight}, Report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetTitle()                           !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR3.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetViewerPrefs(PDFXTR3:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp:2{PROP:Text} = FORMAT(TODAY(),@d06)
  END
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'LoanAuditReport',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR3.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
        IF (lau:Confirmed = 1)
  	! Excess Page
            IF (p_web.GSV('ShortagePage') = 1)
  		        ! First Excess On A Shortage Page
                IF (Total_No_Of_Lines = 0)
  			        ! There were no shortages
                    PRINT(rpt:Detail2)
                ELSE
                    PRINT(rpt:Detail1)
                    Total_No_Of_Lines = 0
                    SETTARGET(REPORT)
                    ?string21{prop:Text} = 'EXCESSES'
                    SETTARGET()
                END ! IF
            END ! IF
            p_web.SSV('ShortagePage',0)	
            IF (lau:New_IMEI <> 1)
                RETURN Level:User
            END ! IF
        END ! IF
        
        Access:LOAN.ClearKey(loa:Ref_Number_Key)
        loa:Ref_Number = lau:Ref_Number
        IF (Access:LOAN.TryFetch(loa:Ref_Number_Key))
            Return Level:User
        END ! IF
        
        Make_Model = CLIP(loa:Manufacturer) & '-' & CLIP(loa:Model_Number)
        Total_No_Of_Lines += 1		
  PRINT(RPT:DETAIL)
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR3:rtn = PDFXTR3.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR3:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

