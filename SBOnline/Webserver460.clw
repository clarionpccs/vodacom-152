

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER460.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER462.INC'),ONCE        !Req'd for module callout resolution
                     END


FormBrowseReturnOrderTracking PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locReturnTrackingType BYTE                                 !
FilesOpened     Long
RTNORDER::State  USHORT
WAYBILLS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormBrowseReturnOrderTracking')
  loc:formname = 'FormBrowseReturnOrderTracking_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormBrowseReturnOrderTracking',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormBrowseReturnOrderTracking','')
    p_web._DivHeader('FormBrowseReturnOrderTracking',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormBrowseReturnOrderTracking',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseReturnOrderTracking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseReturnOrderTracking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormBrowseReturnOrderTracking',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseReturnOrderTracking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormBrowseReturnOrderTracking',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormBrowseReturnOrderTracking',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(RTNORDER)
  p_web._OpenFile(WAYBILLS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(RTNORDER)
  p_Web._CloseFile(WAYBILLS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SSV('RecordSelected',0)
  p_web.SetValue('FormBrowseReturnOrderTracking_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locReturnTrackingType',locReturnTrackingType)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locReturnTrackingType')
    locReturnTrackingType = p_web.GetValue('locReturnTrackingType')
    p_web.SetSessionValue('locReturnTrackingType',locReturnTrackingType)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormBrowseReturnOrderTracking_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
      p_web.site.SaveButton.TextValue = 'Close'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locReturnTrackingType = p_web.RestoreValue('locReturnTrackingType')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'FormBrowseStock'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormBrowseReturnOrderTracking_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormBrowseReturnOrderTracking_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormBrowseReturnOrderTracking_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'FormBrowseStock'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormBrowseReturnOrderTracking" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormBrowseReturnOrderTracking" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormBrowseReturnOrderTracking" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Return Order Tracking') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Return Order Tracking',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormBrowseReturnOrderTracking">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormBrowseReturnOrderTracking" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormBrowseReturnOrderTracking')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Click On Line Above To View Options Below') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormBrowseReturnOrderTracking')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormBrowseReturnOrderTracking'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormBrowseReturnOrderTracking_BrowseReturnOrderTracking_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormBrowseReturnOrderTracking_BrowseReturnOrderTracking_embedded_div')&''');'
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormBrowseReturnOrderTracking')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormBrowseReturnOrderTracking_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locReturnTrackingType
      do Value::locReturnTrackingType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseReturnOrderTracking
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Click On Line Above To View Options Below') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormBrowseReturnOrderTracking_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Click On Line Above To View Options Below')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Click On Line Above To View Options Below')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Click On Line Above To View Options Below')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Click On Line Above To View Options Below')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnPrintCreditNoteRequest
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnReprintWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locReturnTrackingType  Routine
  p_web._DivHeader('FormBrowseReturnOrderTracking_' & p_web._nocolon('locReturnTrackingType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locReturnTrackingType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locReturnTrackingType',p_web.GetValue('NewValue'))
    locReturnTrackingType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locReturnTrackingType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locReturnTrackingType',p_web.GetValue('Value'))
    locReturnTrackingType = p_web.GetValue('Value')
  End
  do Value::locReturnTrackingType
  do SendAlert
  do Value::BrowseReturnOrderTracking  !1

Value::locReturnTrackingType  Routine
  p_web._DivHeader('FormBrowseReturnOrderTracking_' & p_web._nocolon('locReturnTrackingType') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locReturnTrackingType
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locReturnTrackingType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locReturnTrackingType') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locReturnTrackingType'',''formbrowsereturnordertracking_locreturntrackingtype_value'',1,'''&clip(0)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locReturnTrackingType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locReturnTrackingType',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locReturnTrackingType_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Spares') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locReturnTrackingType') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locReturnTrackingType'',''formbrowsereturnordertracking_locreturntrackingtype_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locReturnTrackingType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locReturnTrackingType',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locReturnTrackingType_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Exchanges') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locReturnTrackingType') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locReturnTrackingType'',''formbrowsereturnordertracking_locreturntrackingtype_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locReturnTrackingType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locReturnTrackingType',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locReturnTrackingType_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Loans') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseReturnOrderTracking_' & p_web._nocolon('locReturnTrackingType') & '_value')


Validate::BrowseReturnOrderTracking  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseReturnOrderTracking',p_web.GetValue('NewValue'))
    do Value::BrowseReturnOrderTracking
  Else
    p_web.StoreValue('rtn:RecordNumber')
  End
  Access:RTNORDER.ClearKey(rtn:RecordNumberKey)
  rtn:RecordNumber = p_web.GSV('rtn:RecordNumber')
  IF (Access:RTNORDER.TryFetch(rtn:RecordNumberKey) = Level:Benign)
      p_web.SSV('rtn:CreditNoteRequestNumber',rtn:CreditNoteRequestNumber)
  
  ! Store Waybill Details Incase they choose to print
      p_web.SSV('locWaybillNumber',rtn:WaybillNumber)
      Access:WAYBILLS.Clearkey( way:WayBillNumberKey)
      way:WaybillNumber = rtn:WaybillNumber
      IF (Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign)
          p_web.SSV('Waybill:Courier',way:Courier)
          p_web.SSV('Waybill:FromType','TRA')
          p_web.SSV('waybill:FromAccountNumber',way:FromAccount)
          p_web.SSV('Waybill:ToType','TRA')
          p_web.SSV('Waybill:ToAccountNumber',way:ToAccount)
          p_web.SSV('Waybill:ConsignmentNumber',way:WayBillNumber)
      END
  END
  p_web.SSV('RecordSelected',1)
  do SendAlert
  do Value::btnPrintCreditNoteRequest  !1
  do Value::btnReprintWaybill  !1

Value::BrowseReturnOrderTracking  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseReturnOrderTracking --
  p_web.SetValue('BrowseReturnOrderTracking:NoForm',1)
  p_web.SetValue('BrowseReturnOrderTracking:FormName',loc:formname)
  p_web.SetValue('BrowseReturnOrderTracking:parentIs','Form')
  p_web.SetValue('_parentProc','FormBrowseReturnOrderTracking')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormBrowseReturnOrderTracking_BrowseReturnOrderTracking_embedded_div')&'"><!-- Net:BrowseReturnOrderTracking --></div><13,10>'
    p_web._DivHeader('FormBrowseReturnOrderTracking_' & lower('BrowseReturnOrderTracking') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormBrowseReturnOrderTracking_' & lower('BrowseReturnOrderTracking') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseReturnOrderTracking --><13,10>'
  end
  do SendPacket


Validate::btnPrintCreditNoteRequest  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnPrintCreditNoteRequest',p_web.GetValue('NewValue'))
    do Value::btnPrintCreditNoteRequest
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnPrintCreditNoteRequest  Routine
  p_web._DivHeader('FormBrowseReturnOrderTracking_' & p_web._nocolon('btnPrintCreditNoteRequest') & '_value',Choose(p_web.GSV('RecordSelected') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('RecordSelected') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnPrintCreditNoteRequest','Re-Print Credit Note Req.','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('CreditNoteRequest')) & ''','''&clip('_blank')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseReturnOrderTracking_' & p_web._nocolon('btnPrintCreditNoteRequest') & '_value')


Validate::btnReprintWaybill  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnReprintWaybill',p_web.GetValue('NewValue'))
    do Value::btnReprintWaybill
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnReprintWaybill  Routine
  p_web._DivHeader('FormBrowseReturnOrderTracking_' & p_web._nocolon('btnReprintWaybill') & '_value',Choose(p_web.GSV('RecordSelected') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('RecordSelected') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnReprintWaybill','Reprint Waybill','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('RepWaybillDespatch')) & ''','''&clip('_blank')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseReturnOrderTracking_' & p_web._nocolon('btnReprintWaybill') & '_value')


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormBrowseReturnOrderTracking_locReturnTrackingType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locReturnTrackingType
      else
        do Value::locReturnTrackingType
      end
  of lower('FormBrowseReturnOrderTracking_BrowseReturnOrderTracking_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::BrowseReturnOrderTracking
      else
        do Value::BrowseReturnOrderTracking
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormBrowseReturnOrderTracking_form:ready_',1)
  p_web.SetSessionValue('FormBrowseReturnOrderTracking_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormBrowseReturnOrderTracking',0)

PreCopy  Routine
  p_web.SetValue('FormBrowseReturnOrderTracking_form:ready_',1)
  p_web.SetSessionValue('FormBrowseReturnOrderTracking_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormBrowseReturnOrderTracking',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormBrowseReturnOrderTracking_form:ready_',1)
  p_web.SetSessionValue('FormBrowseReturnOrderTracking_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormBrowseReturnOrderTracking:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormBrowseReturnOrderTracking_form:ready_',1)
  p_web.SetSessionValue('FormBrowseReturnOrderTracking_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormBrowseReturnOrderTracking:Primed',0)
  p_web.setsessionvalue('showtab_FormBrowseReturnOrderTracking',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormBrowseReturnOrderTracking_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormBrowseReturnOrderTracking_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormBrowseReturnOrderTracking:Primed',0)
  p_web.StoreValue('locReturnTrackingType')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locReturnTrackingType',locReturnTrackingType) ! BYTE
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locReturnTrackingType = p_web.GSV('locReturnTrackingType') ! BYTE
