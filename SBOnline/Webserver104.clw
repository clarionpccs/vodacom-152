

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER104.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ForceAuthorityNumber PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !

  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Authority Number
    If (def:Force_Authority_Number = 'B' And func:Type = 'B') Or |
        (def:Force_Authority_Number <> 'I' and func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Authority_Number = 'B'
    Return Level:Benign
