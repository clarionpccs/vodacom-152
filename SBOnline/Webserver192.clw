

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER192.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! Report the STOCK File
!!! </summary>
StockValueReport PROCEDURE (NetWebServerWorker p_web)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
locPrintedBy         STRING(70)                            !
locLineCost          REAL                                  !
gAddress             GROUP,PRE(address)                    !
Username             STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
Postcode             STRING(30)                            !
TelephoneNumber      STRING(30)                            !
FaxNumber            STRING(30)                            !
EmailAddress         STRING(255)                           !
                     END                                   !
gSummary             GROUP,PRE()                           !
TotalStock           LONG                                  !
PurchaseValue        REAL                                  !
SaleValue            REAL                                  !
AveragePurchaseCost  REAL                                  !
                     END                                   !
gTotals              GROUP,PRE(totals)                     !
LineCost             REAL                                  !
Quantity             LONG                                  !
PurchaseValue        REAL                                  !
SalesValue           REAL                                  !
AveragePurchaseCost  REAL                                  !
Count                REAL                                  !
                     END                                   !
currentManufacturer  STRING(30)                            !
firstManufacturer    LONG                                  !
locSiteLocation      STRING(30)                            !
locIncludeAccessories STRING(3)                            !
locSuppressZeros     STRING(3)                             !
Process:View         VIEW(STOCK)
                       PROJECT(sto:Ref_Number)
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Report STOCK'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Stock Value Report'),AT(396,2792,7521,8510),PRE(RPT),PAPER(PAPER:A4),FONT('Arial', |
  10,,FONT:regular),THOUS
                       HEADER,AT(385,781,7531,1417),USE(?unnamed)
                         STRING('Inc. Accessories:'),AT(4948,313),USE(?String22),FONT(,8),TRN
                         STRING(@s3),AT(5885,313),USE(locIncludeAccessories),FONT(,8,,FONT:bold),LEFT,TRN
                         STRING(@s3),AT(5885,469),USE(locSuppressZeros),FONT(,8,,FONT:bold),LEFT,TRN
                         STRING('Suppress Zeros:'),AT(4948,469),USE(?String22:3),FONT(,8),TRN
                         STRING(@s30),AT(4958,125),USE(locSiteLocation),FONT(,8,,FONT:bold),LEFT,TRN
                         STRING('Printed By:'),AT(4948,833),USE(?String27),FONT(,8),TRN
                         STRING('Page Number:'),AT(4948,1250),USE(?String59),FONT(,8),TRN
                         STRING(@s60),AT(5885,833),USE(locPrintedBy),FONT(,8,,FONT:bold),TRN
                         STRING('Date Printed:'),AT(4948,1042),USE(?ReportDatePrompt),FONT(,8),TRN
                         STRING('<<-- Date Stamp -->'),AT(5875,1042),USE(?ReportDateStamp),FONT(,8,,FONT:bold),TRN
                         STRING(@N3),AT(5875,1250),USE(ReportPageNumber),FONT(,8,,FONT:bold),TRN
                       END
DetailDetail           DETAIL,AT(0,0,7521,208),USE(?DetailDetail)
                         STRING(@s20),AT(156,0,1198,156),USE(qStockValueReport.PartNumber),FONT(,7),TRN
                         STRING(@s30),AT(1250,0),USE(qStockValueReport.Description),FONT(,7),TRN
                         STRING(@s15),AT(3073,0,781,156),USE(qStockValueReport.firstmodel),FONT('Arial',7,,FONT:regular, |
  CHARSET:ANSI),LEFT,TRN
                         STRING(@S6),AT(3875,0),USE(qStockValueReport.MinimumLevel),FONT(,7),RIGHT,TRN
                         STRING(@N10.2),AT(4427,0),USE(qStockValueReport.SaleValue),FONT(,7),RIGHT,TRN
                         STRING(@S8),AT(5104,0),USE(qStockValueReport.Quantity),FONT(,7),RIGHT,TRN
                         STRING(@N10.2),AT(5698,0),USE(qStockValueReport.PurchaseValue),FONT(,7),RIGHT,TRN
                         STRING(@n10.2),AT(6844,0),USE(locLineCost),FONT(,7),RIGHT,TRN
                         STRING(@n10.2),AT(6281,0),USE(qStockValueReport.AveragePurchaseCost),FONT(,7),RIGHT,TRN
                       END
SummaryDetail          DETAIL,AT(0,0,7521,208),USE(?SummaryDetails)
                         STRING(@s30),AT(156,0,2135),USE(qStockValueReport.SiteLocation),FONT(,8),TRN
                         STRING(@s25),AT(2458,0),USE(currentManufacturer),FONT(,8),TRN
                         STRING(@s8),AT(4010,0),USE(TotalStock),FONT(,8),RIGHT,TRN
                         STRING(@n14.2),AT(4688,0),USE(PurchaseValue),FONT(,8,,,CHARSET:ANSI),RIGHT,TRN
                         STRING(@n14.2),AT(6604,0),USE(SaleValue),FONT(,8),RIGHT,TRN
                         STRING(@n14.2),AT(5677,0),USE(AveragePurchaseCost),FONT(,8,,,CHARSET:ANSI),RIGHT,TRN
                       END
TotalsSummaryDetail    DETAIL,AT(0,0,7521,250),USE(?TotalsSummaryDetail)
                         LINE,AT(208,52,7083,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Total Number Of Lines:'),AT(208,104),USE(?String26),FONT(,8,,FONT:bold),TRN
                         STRING(@s9),AT(3948,104),USE(totals:Quantity),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING(@n-14.2),AT(4708,104),USE(totals:PurchaseValue),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING(@n-14.2),AT(6625,104),USE(totals:SalesValue),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING(@n14.2),AT(5677,104),USE(totals:AveragePurchaseCost),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING(@s9),AT(2458,104),USE(totals:Count),FONT(,8,,FONT:bold),TRN
                       END
TotalsDetailDetail     DETAIL,AT(0,0,7521,250),USE(?TotalsDetailDetail)
                         LINE,AT(208,52,7083,0),USE(?Line1:2),COLOR(COLOR:Black)
                         STRING('Total Number Of Lines:'),AT(208,104),USE(?String26:2),FONT(,8,,FONT:bold),TRN
                         STRING(@s8),AT(5010,104),USE(totals:Quantity,,?totals:Quantity:2),FONT(,8,,FONT:bold),RIGHT, |
  TRN
                         STRING(@n10.2),AT(6750,104),USE(totals:LineCost),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING(@s9),AT(2083,104),USE(totals:Count,,?totals:Count:3),FONT(,8,,FONT:bold),TRN
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(52,0,7521,11156),USE(?Image1)
                         STRING(@s30),AT(156,0,3844,240),USE(address:Username),FONT(,16,,FONT:bold),LEFT,TRN
                         STRING('STOCK VALUE REPORT'),AT(4271,0,3177,260),USE(?String20),FONT(,16,,FONT:bold),RIGHT, |
  TRN
                         STRING(@s30),AT(156,260,3844,156),USE(address:AddressLine1),FONT(,9),TRN
                         STRING(@s30),AT(156,417,3844,156),USE(address:AddressLine2),FONT(,9),TRN
                         STRING(@s30),AT(156,573,3844,156),USE(address:AddressLine3),FONT(,9),TRN
                         STRING(@s30),AT(156,729,1156,156),USE(address:Postcode),FONT(,9),TRN
                         STRING('Tel:'),AT(156,1042),USE(?String16),FONT(,9),TRN
                         STRING(@s30),AT(521,1042),USE(address:TelephoneNumber),FONT(,9),TRN
                         STRING('Fax: '),AT(156,1198),USE(?String19),FONT(,9),TRN
                         STRING(@s30),AT(521,1198),USE(address:FaxNumber),FONT(,9),TRN
                         STRING(@s255),AT(521,1354,3844,198),USE(address:EmailAddress),FONT(,9),TRN
                         STRING('Email:'),AT(156,1354),USE(?String19:2),FONT(,9),TRN
                         GROUP,AT(42,1958,7458,500),USE(?grpDetail)
                           STRING('Description'),AT(1260,2083),USE(?String44:3),FONT(,7,,FONT:bold),TRN
                           STRING('Out Warranty'),AT(4500,2021),USE(?String44:4),FONT(,7,,FONT:bold),TRN
                           STRING('If Below'),AT(3969,2135),USE(?String24:4),FONT('Arial',7,,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING(' Cost '),AT(5896,2135),USE(?String44:5),FONT(,7,,FONT:bold),TRN
                           STRING(' Cost '),AT(6490,2135),USE(?String44:10),FONT(,7,,FONT:bold),TRN
                           STRING(' Cost '),AT(4656,2135),USE(?String44:9),FONT(,7,,FONT:bold),TRN
                           STRING('Purchase'),AT(6427,2021),USE(?String44:8),FONT(,7,,FONT:bold),TRN
                           STRING('Qty In'),AT(5323,2021),USE(?String45:6),FONT(,7,,FONT:bold),TRN
                           STRING('First Model No'),AT(3083,2083),USE(?String44:7),FONT(,7,,FONT:bold),TRN
                           STRING('In Warranty'),AT(5760,2021),USE(?String44:6),FONT(,7,,FONT:bold),TRN
                           STRING('Stock'),AT(5323,2135),USE(?String44:11),FONT(,7,,FONT:bold),TRN
                           STRING('Part Number'),AT(167,2083),USE(?String44:12),FONT(,7,,FONT:bold),TRN
                           STRING('Order'),AT(4021,2031),USE(?String45:7),FONT(,7,,FONT:bold),TRN
                           STRING('Line Cost'),AT(6937,2083),USE(?String25),FONT('Arial',7,,FONT:bold,CHARSET:ANSI),TRN
                         END
                         GROUP,AT(42,1958,7458,500),USE(?grpSummary),TRN
                           STRING('Manufacturer'),AT(2500,2083),USE(?String44),FONT(,8,,FONT:bold),TRN
                           STRING('Average Purchase'),AT(5802,2021),USE(?String45:5),FONT(,7,,FONT:bold),TRN
                           STRING('Site Location'),AT(167,2083),USE(?String44:2),FONT(,8,,FONT:bold),TRN
                           STRING('Stock Items'),AT(3969,2125),USE(?String45),FONT(,7,,FONT:bold),TRN
                           STRING('In Warranty'),AT(4979,2021),USE(?String45:3),FONT(,7,,FONT:bold),TRN
                           STRING('Value'),AT(5094,2125),USE(?String24),FONT('Arial',7,,FONT:bold,CHARSET:ANSI),TRN
                           STRING('Out Warranty'),AT(6823,2021),USE(?String45:4),FONT(,7,,FONT:bold),TRN
                           STRING('Value'),AT(7021,2125),USE(?String24:2),FONT('Arial',7,,FONT:bold,CHARSET:ANSI),TRN
                           STRING('Cost'),AT(6073,2135),USE(?String24:3),FONT('Arial',7,,FONT:bold,CHARSET:ANSI),TRN
                           STRING('Total'),AT(4115,2021),USE(?String45:2),FONT(,7,,FONT:bold),TRN
                         END
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR3               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
EndReport              PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Next                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

TotalsSummary       ROUTINE
    totals:Quantity += totalStock
    totals:PurchaseValue += purchaseValue
    totals:SalesValue += saleValue
    totals:AveragePurchaseCost += averagePurchaseCost
    totals:Count += 1
!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.EndReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
        CASE p_web.GSV('locReportType')
        OF 0
            IF (firstManufacturer = 1)
                firstManufacturer = 0
            ELSE
                qStockValueReport.SiteLocation = ''
            END !IF
            PRINT(rpt:SummaryDetail)
            DO TotalsSummary
            PRINT(rpt:TotalsSummaryDetail)
        OF 1
            PRINT(rpt:TotalsDetailDetail)
        END ! IF
        
        
  ReturnValue = PARENT.EndReport()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('StockValueReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:STOCK.SetOpenRelated()
  Relate:STOCK.Open                                        ! File STOCK used by this procedure, so make sure it's RelationManager is open
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('StockValueReport',ProgressWindow)          ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:STOCK, ?Progress:PctText, Progress:Thermometer, RECORDS(qStockValueReport))
  ThisReport.AddSortOrder()
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STOCK.Close
  END
  IF SELF.Opened
    INIMgr.Update('StockValueReport',ProgressWindow)       ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'StockValueReport',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Next PROCEDURE

ReturnValue          BYTE,AUTO

Progress BYTE,AUTO
  CODE
      ThisReport.RecordsProcessed+=1
      GET(qStockValueReport,ThisReport.RecordsProcessed)
      IF ERRORCODE() THEN
         ReturnValue = Level:Notify
      ELSE
         ReturnValue = Level:Benign
      END
      IF ReturnValue = Level:Notify
          IF ThisReport.RecordsProcessed>RECORDS(qStockValueReport)
             SELF.Response = RequestCompleted
             POST(EVENT:CloseWindow)
             RETURN Level:Notify
          ELSE
             SELF.Response = RequestCancelled
             POST(EVENT:CloseWindow)
             RETURN Level:Fatal
          END
      ELSE
         Progress = ThisReport.RecordsProcessed / ThisReport.RecordsToProcess*100
         IF Progress > 100 THEN Progress = 100.
         IF Progress <> Progress:Thermometer
           Progress:Thermometer = Progress
           DISPLAY()
         END
      END
      RETURN Level:Benign
  ReturnValue = PARENT.Next()
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
        SETTARGET(REPORT)
        CASE p_web.GSV('locReportType')
        OF 0
            ?grpDetail{PROP:Hide} = 1
            ?grpSummary{PROP:Hide} = 0
        OF 1
            ?grpDetail{PROP:Hide} = 0
            ?grpSummary{PROP:Hide} = 1
        END ! CASE
        SETTARGET()
        firstManufacturer = 1
        currentManufacturer = '!!!'
        
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = p_web.GSV('BookingAccount')
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Found
            address:UserName        = tra:Company_Name
            address:AddressLine1    = tra:Address_Line1
            address:AddressLine2    = tra:Address_Line2
            address:AddressLine3    = tra:Address_Line3
            address:Postcode        = tra:Postcode
            address:TelephoneNumber = tra:Telephone_Number
            address:FaxNumber       = tra:Fax_Number
            address:EmailAddress    = tra:EmailAddress
        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Error
        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign  
        
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = p_web.GSV('BookingUserCode')
        IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            locPrintedBy = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
        END ! IF
        
        IF (p_web.GSV('locSuppressZeros') = 1)
            locSuppressZeros = 'YES'
        ELSE
            locSuppressZeros = 'NO'
        END ! IF
        IF (p_web.GSV('locIncludeAccessories') = 1)
            locIncludeAccessories = 'YES'
        ELSE
            locIncludeAccessories = 'NO'
        END ! IF
        locSiteLocation = p_web.GSV('BookingSiteLocation')
            
        
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.Init(Report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,Report{PROPPRINT:Paper},PAPER:USER), CHOOSE(Report{PROP:Thous}=True,PROP:Thous,CHOOSE(Report{PROP:MM}=True,PROP:MM,CHOOSE(Report{PROP:Points}=True,PROP:Points,0))), Report{PROPPRINT:PaperWidth}, Report{PROPPRINT:PaperHeight}, Report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetTitle('Stock Value Report')       !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR3.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetViewerPrefs(PDFXTR3:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@d06)
  END
  IF ReturnValue = Level:Benign
    Report$?ReportPageNumber{PROP:PageNo} = True
  END
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'StockValueReport',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR3.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
        IF (qStockValueReport.SessionID = p_web.SessionID)
            CASE p_web.GSV('locReportType')
            OF 0
                IF (qStockValueReport.Manufacturer <> currentManufacturer AND currentManufacturer <> '!!!')
                    IF (firstManufacturer = 1)
                        firstManufacturer = 0
                    ELSE
                        qStockValueReport.SiteLocation = ''
                    END !IF
                    PRINT(rpt:SummaryDetail)
                    DO TotalsSummary
                    CLEAR(gSummary)
                END ! IF
                currentManufacturer = qStockValueReport.Manufacturer
                
                TotalStock += qStockValueReport.Quantity
                PurchaseValue += ROUND(qStockValueReport.PurchaseValue * qStockValueReport.Quantity,.01)
                AveragePurchaseCost += ROUND(qStockValueReport.AveragePurchaseCost * qStockValueReport.Quantity,.01)
                SaleValue += ROUND(qStockValueReport.SaleValue * qStockValueReport.Quantity,.01)
            OF 1
                locLineCost = qStockValueReport.AveragePurchaseCost * qStockValueReport.Quantity
                PRINT(rpt:DetailDetail)
                totals:Quantity += qStockValueReport.Quantity
                totals:LineCost += locLineCost
                totals:Count += 1
            END ! IF
        END ! IF
        p_web.Noop
        
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR3:rtn = PDFXTR3.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR3:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

