

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER507.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! Report the INVOICE File
!!! </summary>
RRCWarrantyInvoice PROCEDURE (NetWebServerWorker p_web)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
locInvoiceNumber     LONG                                  !
locJobsCount         LONG                                  !
locVat               DECIMAL(14,2)                         !
locGrandTotal        DECIMAL(14,2)                         !
locJobNumber         STRING(30)                            !
locReportInvoiceNumber STRING(30)                          !
locDateCreated       DATE                                  !
locPartsTotal        REAL                                  !
locLabourTotal       REAL                                  !
locSubTotal          REAL                                  !
DefaultAddress       GROUP,PRE(address)                    !
Name                 STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
Postcode             STRING(30)                            !
Telephone            STRING(30)                            !
Fax                  STRING(30)                            !
EmailAddress         STRING(255)                           !
                     END                                   !
Process:View         VIEW(INVOICE)
                       PROJECT(inv:Account_Number)
                       PROJECT(inv:Labour_Paid)
                       PROJECT(inv:Parts_Paid)
                       PROJECT(inv:Reconciled_Date)
                       PROJECT(inv:Total)
                     END
ProgressWindow       WINDOW('Report INVOICE'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

report               REPORT,AT(396,2792,7521,8542),PRE(rpt),PAPER(PAPER:A4),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(375,1000,7500,1000),USE(?mainHeader)
                         STRING('Invoice No:'),AT(5000,104),USE(?String26),FONT(,10),TRN
                         STRING(@s30),AT(6042,104),USE(locReportInvoiceNumber),FONT(,10,,FONT:bold),LEFT,TRN
                         STRING('Reconciled Date:'),AT(5000,365),USE(?string22),FONT(,8),TRN
                         STRING(@d6b),AT(6042,365),USE(inv:Reconciled_Date),FONT(,8,,FONT:bold),LEFT,TRN
                         STRING(@n_14),AT(6042,583),USE(locJobsCount,,?locJobsCount:2),FONT(,8,,FONT:bold),LEFT,TRN
                         STRING('Claims On Invoice:'),AT(5000,583),USE(?string22:2),FONT(,8),TRN
                       END
detail                 DETAIL,AT(0,0,7521,167),USE(?DETAIL1)
                         STRING(@n10.2),AT(6719,0),USE(jobe:RRCWSubTotal),FONT(,8,,,CHARSET:ANSI),RIGHT,TRN
                         STRING(@s15),AT(1250,0),USE(job:Manufacturer),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING(@s20),AT(2240,0),USE(job:Model_Number),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING(@s30),AT(156,0,1010),USE(locJobNumber),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),LEFT, |
  TRN
                         STRING(@s20),AT(3542,0),USE(job:ESN),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING(@d6),AT(4844,0),USE(job:Date_Completed),FONT(,8,,,CHARSET:ANSI),RIGHT,TRN
                         STRING(@n10.2),AT(5521,0),USE(jobe:InvRRCWPartsCost),FONT(,8,,,CHARSET:ANSI),RIGHT,TRN
                         STRING(@n10.2),AT(6146,0),USE(jobe:InvRRCWLabourCost),FONT(,8,,,CHARSET:ANSI),RIGHT,TRN
                       END
detail1                DETAIL,AT(0,0,7521,667),USE(?DETAIL2),PAGEAFTER(1)
                         LINE,AT(177,21,7135,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Total Claims:'),AT(135,73),USE(?String38),FONT(,8,,FONT:bold),TRN
                         STRING(@n10.2),AT(5500,73),USE(inv:Parts_Paid),FONT(,8),RIGHT,TRN
                         STRING(@n10.2),AT(6125,73),USE(inv:Labour_Paid),FONT(,8),RIGHT,TRN
                         STRING(@n10.2),AT(6698,73),USE(inv:Total),FONT(,8),RIGHT,TRN
                         STRING('V.A.T.:'),AT(5552,260),USE(?String41),FONT(,8),TRN
                         STRING(@n-19.2),AT(6198,260),USE(locVat),FONT(,8),RIGHT,TRN
                         LINE,AT(6490,437,885,0),USE(?Line2),COLOR(COLOR:Black)
                         STRING(@n-19.2),AT(6198,469),USE(locGrandTotal),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING('Total:'),AT(5552,469),USE(?String41:2),FONT(,8,,FONT:bold),TRN
                         STRING(@n-14),AT(969,73),USE(locJobsCount),FONT(,8,,FONT:bold),TRN
                       END
                       FORM,AT(396,479,7521,11198),USE(?FORM1)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(address:Name),FONT(,16,,FONT:bold),LEFT,TRN
                         STRING('WARRANTY CLAIM PAYMENT '),AT(4063,0,3385,260),USE(?string20),FONT(,10,,FONT:bold), |
  RIGHT,TRN
                         STRING(@s30),AT(156,260,2240,156),USE(address:AddressLine1),FONT(,9),TRN
                         STRING(@s30),AT(156,417,2240,156),USE(address:AddressLine2),FONT(,9),TRN
                         STRING('RECONCILIATION DOCUMENT'),AT(5500,156),USE(?String29),FONT(,10,,FONT:bold),RIGHT,TRN
                         STRING(@s30),AT(156,573,2240,156),USE(address:AddressLine3),FONT(,9),TRN
                         STRING(@s30),AT(156,729,1156,156),USE(address:Postcode),FONT(,9),TRN
                         STRING('Tel:'),AT(156,1042),USE(?string16),FONT(,9),TRN
                         STRING(@s30),AT(542,1042),USE(address:Telephone),FONT(,9),TRN
                         STRING('Fax: '),AT(156,1198),USE(?string19),FONT(,9),TRN
                         STRING(@s30),AT(542,1198),USE(address:Fax),FONT(,9),TRN
                         STRING(@s255),AT(542,1354),USE(address:EmailAddress),FONT(,9),TRN
                         STRING('Job No'),AT(156,2031),USE(?String30),FONT(,8,,FONT:bold),TRN
                         STRING('Manufacturer'),AT(1250,2031),USE(?String30:2),FONT(,8,,FONT:bold),TRN
                         STRING('Model Number'),AT(2240,2031),USE(?String30:3),FONT(,8,,FONT:bold),TRN
                         STRING('I.M.E.I. Number'),AT(3542,2031),USE(?String30:4),FONT(,8,,FONT:bold),TRN
                         STRING('Completed'),AT(4823,2031),USE(?String30:5),FONT(,8,,FONT:bold),TRN
                         STRING('Parts'),AT(5833,2031),USE(?String30:6),FONT(,8,,FONT:bold),TRN
                         STRING('Labour'),AT(6354,2031),USE(?String30:7),FONT(,8,,FONT:bold),TRN
                         STRING('Sub Total'),AT(6802,2031),USE(?String30:8),FONT(,8,,FONT:bold),TRN
                         STRING('Email:'),AT(156,1354),USE(?string19:2),FONT(,9),TRN
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR3               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
ApplyFilter            PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepStringClass                       ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('RRCWarrantyInvoice')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:INVOICE.SetOpenRelated()
  Relate:INVOICE.Open                                      ! File INVOICE used by this procedure, so make sure it's RelationManager is open
  Relate:INVOICE_ALIAS.Open                                ! File INVOICE_ALIAS used by this procedure, so make sure it's RelationManager is open
  Relate:SBO_WarrantyClaims.Open                           ! File SBO_WarrantyClaims used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:JOBSWARR.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBS.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
        locInvoiceNumber = 0
        locDateCreated = 0
        IF (p_web.IfExistsValue('RecordNumber'))    
            Access:SBO_WarrantyClaims.ClearKey(sbojow:RecordNumberKey)
            sbojow:RecordNumber = p_web.GetValue('RecordNumber')
            IF (Access:SBO_WarrantyClaims.TryFetch(sbojow:RecordNumberKey) = Level:Benign)
                Access:JOBSWARR.ClearKey(jow:RefNumberKey)
                jow:RefNumber = sbojow:JobNumber
                IF (Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign)
                    Access:WEBJOB.ClearKey(wob:RefNumberKey)
                    wob:RefNumber = jow:RefNumber
                    IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                        Access:INVOICE_ALIAS.ClearKey(inv_ali:Invoice_Number_Key)
                        inv_ali:Invoice_Number = wob:RRCWInvoiceNumber
                        IF (Access:INVOICE_ALIAS.TryFetch(inv_ali:Invoice_Number_Key) = Level:Benign)
                            locDateCreated = inv_ali:Reconciled_Date
                            IF (p_web.GetValue('Type') = 'Selected')
                                locInvoiceNumber = inv_ali:Invoice_Number
                            ELSE
                                locInvoiceNumber = 0
                            END ! IF
                        END ! IF
                    END !IF
                END ! IF
            END ! IF
        ELSE
            Access:INVOICE_ALIAS.ClearKey(inv_ali:Invoice_Number_Key)
            inv_ali:Invoice_Number = p_web.GSV('WarrInvoiceNumber')
            IF (Access:INVOICE_ALIAS.TryFetch(inv_ali:Invoice_Number_Key) = Level:Benign)
                locDateCreated = inv_ali:Date_Created
                locInvoiceNumber = inv_ali:Invoice_Number
            END ! IF            
        END ! IF
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('RRCWarrantyInvoice',ProgressWindow)        ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
        ! Bodge to change the report order depending on whether passing a single invoice number, or a date range
        IF (locInvoiceNumber > 0)
            ProgressMgr.Init(ScrollSort:AllowAlpha+ScrollSort:AllowNumeric,ScrollBy:RunTime)
            ThisReport.Init(Process:View, Relate:INVOICE, ?Progress:PctText, Progress:Thermometer, ProgressMgr, inv:Account_Number)
            ThisReport.CaseSensitiveValue = FALSE
            ThisReport.AddSortOrder(inv:Account_Number_Key)
            ThisReport.AddRange(inv:Invoice_Number,locInvoiceNumber)
            SELF.AddItem(?Progress:Cancel,RequestCancelled)
            SELF.Init(ThisReport,report,Previewer)
            ?Progress:UserString{PROP:Text} = ''
            Relate:INVOICE.SetQuickScan(1,Propagate:OneMany)
            ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
        ELSE ! IF           
            
  ProgressMgr.Init(ScrollSort:AllowAlpha+ScrollSort:AllowNumeric,ScrollBy:RunTime)
  ThisReport.Init(Process:View, Relate:INVOICE, ?Progress:PctText, Progress:Thermometer, ProgressMgr, inv:Account_Number)
  ThisReport.CaseSensitiveValue = FALSE
  ThisReport.AddSortOrder(inv:AccountReconciledKey)
  ThisReport.AddRange(inv:Reconciled_Date,locDateCreated)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:INVOICE.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
        END ! IF
        
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:INVOICE.Close
    Relate:INVOICE_ALIAS.Close
    Relate:SBO_WarrantyClaims.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('RRCWarrantyInvoice',ProgressWindow)     ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'RRCWarrantyInvoice',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.Init(report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,report{PROPPRINT:Paper},PAPER:USER), CHOOSE(report{PROP:Thous}=True,PROP:Thous,CHOOSE(report{PROP:MM}=True,PROP:MM,CHOOSE(report{PROP:Points}=True,PROP:Points,0))), report{PROPPRINT:PaperWidth}, report{PROPPRINT:PaperHeight}, report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetTitle('Warranty Invoice')         !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR3.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetViewerPrefs(PDFXTR3:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'RRCWarrantyInvoice',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR3.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.ApplyFilter PROCEDURE

  CODE
         GET(SELF.Order.RangeList.List,1)
         SELF.Order.RangeList.List.Right = p_web.GSV('BookingAccount') ! inv:Account_Number
  PARENT.ApplyFilter


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  p_web.NoOp() ! Keep Report Alive (Hopefully)
  !        print# = 1
  !        IF (locInvoiceNumber > 0)
  !            IF (inv:Invoice_Number <> locInvoiceNumber)
  !                print# = 0        
  !            END ! IF
  !        END ! IF
  !        
  !        IF (print# = 1)
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = inv:Account_Number
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
        END ! IF
        
        address:Name            = tra:Company_Name
        address:AddressLine1    = tra:Address_Line1
        address:AddressLine2    = tra:Address_Line2
        address:AddressLine3    = tra:Address_Line3
        address:Postcode        = tra:Postcode
        address:Telephone       = tra:Telephone_Number
        address:Fax             = tra:Fax_Number
        address:EmailAddress    = tra:EmailAddress
        
        locReportInvoiceNumber = Clip(inv:Invoice_Number) & '/' & Clip(tra:BranchIdentification)
        
        
  
  
        locJobsCount = 0
        
        locPartsTotal = 0
        locLabourTotal = 0
        locSubTotal = 0
            
        Access:WEBJOB.ClearKey(wob:RRCWInvoiceNumberKey)
        wob:RRCWInvoiceNumber = inv:Invoice_Number
        SET(wob:RRCWInvoiceNumberKey,wob:RRCWInvoiceNumberKey)
        LOOP UNTIL Access:WEBJOB.Next() <> Level:Benign
            IF (wob:RRCWInvoiceNumber <> inv:Invoice_Number)
                BREAK
            END ! IF
            
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = wob:RefNumber
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
            END ! IF
            
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = wob:RefNumber
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
            END ! IF
            
            locJobNumber = CLIP(wob:RefNumber) & '-' & CLIP(tra:BranchIdentification) & CLIP(wob:JobNumber)
            locJobsCount += 1
                
            locPartsTotal += jobe:InvRRCWPartsCost
            locLabourTotal += jobe:InvRRCWLabourCost
            locSubTotal += jobe:InvRRCWSubTotal
                
            PRINT(rpt:Detail)
                
        END ! LOOP   
        
        IF (inv:Labour_Paid <> locLabourTotal OR inv:Parts_Paid <> locPartsTotal OR inv:Total <> locSubTotal)
            inv:Labour_Paid = locLabourTotal
            inv:Parts_Paid = locPartsTotal
            inv:Total = locSubTotal
            Access:INVOICE.TryUpdate()
        END ! IF
        
        
        locVat = Round(inv:Labour_Paid * inv:Vat_Rate_Labour/100 +|
            inv:Parts_Paid * inv:Vat_Rate_Parts/100,.01)
        locGrandTotal = Round(inv:Total + locVat,.01)
        
        !END ! IF        
  ReturnValue = PARENT.TakeRecord()
  PRINT(rpt:detail1)
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR3:rtn = PDFXTR3.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR3:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

