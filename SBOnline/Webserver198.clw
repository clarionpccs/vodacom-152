

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER198.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
Goods_Received_Note_Retail PROCEDURE (NetWebServerWorker p_web)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
Default_Invoice_Company_Name_Temp STRING(30)               !
tmp:RecordsCount     LONG                                  !
tmp:DefaultTelephone STRING(20)                            !
tmp:DefaultFax       STRING(20)                            !
Default_Invoice_Address_Line1_Temp STRING(30)              !
Default_Invoice_Address_Line2_Temp STRING(30)              !
Default_Invoice_Address_Line3_Temp STRING(30)              !
Default_Invoice_Postcode_Temp STRING(15)                   !
Default_Invoice_Telephone_Number_Temp STRING(15)           !
Default_Invoice_Fax_Number_Temp STRING(15)                 !
Default_Invoice_VAT_Number_Temp STRING(30)                 !
tmp:PrintedBy        STRING(60)                            !
first_page_temp      BYTE                                  !
pos                  STRING(255)                           !
Part_Queue           QUEUE,PRE()                           !
Order_Number_Temp    REAL                                  !
Part_Number_Temp     STRING(30)                            !
Description_Temp     STRING(30)                            !
Quantity_Temp        LONG                                  !
Purchase_cost_temp   REAL                                  !
Sale_Cost_temp       REAL                                  !
                     END                                   !
Order_Temp           REAL                                  !
Address_Line1_Temp   STRING(30)                            !
Address_Line2_Temp   STRING(30)                            !
Address_Line3_Temp   STRING(30)                            !
Address_Line4_Temp   STRING(30)                            !
Total_Quantity_Temp  LONG                                  !
total_cost_temp      REAL                                  !
total_cost_total_temp REAL                                 !
Total_Lines_Temp     REAL                                  !
user_name_temp       STRING(22)                            !
no_temp              STRING('NO')                          !
tmp:Order_No_Filter  REAL                                  !
tmp:Date_Received_Filter DATE                              !
Parts_Q              QUEUE,PRE(pq)                         !
PartNo               STRING(30)                            !
Description          STRING(30)                            !
QtyOrdered           REAL                                  !
QtyReceived          LONG                                  !
Location             STRING(30)                            !
Cost                 REAL                                  !
OrderNumber          LONG                                  !
LineCost             REAL                                  !
                     END                                   !
Process:View         VIEW(RETSALES)
                       PROJECT(ret:Date_Despatched)
                       PROJECT(ret:Invoice_Number)
                       PROJECT(ret:Ref_Number)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT('Goods Received Note'),AT(396,4604,7521,4125),PRE(RPT),PAPER(PAPER:A4),FONT('Arial', |
  10,,FONT:regular),THOUS
                       HEADER,AT(396,479,7521,4167),USE(?unnamed)
                         STRING('Date Printed:'),AT(5083,760),USE(?RunPrompt),FONT(,8),TRN
                         STRING(@s30),AT(156,323,3135,201),USE(tra:Address_Line1,,?tra:Address_Line1:2),TRN
                         STRING(@s30),AT(156,104,4156,263),USE(tra:Company_Name,,?tra:Company_Name:2),FONT(,14,,FONT:bold), |
  TRN
                         STRING(@s30),AT(156,635,3135,201),USE(tra:Address_Line3,,?tra:Address_Line3:2),TRN
                         STRING('REPRINT'),AT(3385,677),USE(?Reprint),FONT(,12,,FONT:bold),HIDE,TRN
                         STRING(@s30),AT(156,802,3135,201),USE(tra:Postcode,,?tra:Postcode:2),TRN
                         STRING('Tel: '),AT(156,958),USE(?String15),TRN
                         STRING('Fax:'),AT(156,1094),USE(?String16),TRN
                         STRING(@s30),AT(573,1094,2495,201),USE(tra:Fax_Number,,?tra:Fax_Number:2),TRN
                         STRING('Email:'),AT(156,1250,521,156),USE(?String16:2),TRN
                         STRING(@s255),AT(573,1250,2500,201),USE(tra:EmailAddress),TRN
                         STRING(@s30),AT(156,1667),USE(def:OrderCompanyName,,?def:OrderCompanyName:2),FONT(,8),TRN
                         STRING(@s30),AT(4083,1667),USE(tra:Company_Name),FONT(,8),TRN
                         STRING(@s30),AT(156,1823),USE(def:OrderAddressLine1,,?def:OrderAddressLine1:2),FONT(,8),TRN
                         STRING(@s30),AT(4083,1823),USE(tra:Address_Line1),FONT(,8),TRN
                         STRING(@s30),AT(156,1979),USE(def:OrderAddressLine2,,?def:OrderAddressLine2:2),FONT(,8),TRN
                         STRING(@s30),AT(4083,1979),USE(tra:Address_Line2),FONT(,8),TRN
                         STRING(@s30),AT(156,2135),USE(def:OrderAddressLine3,,?def:OrderAddressLine3:2),FONT(,8),TRN
                         STRING(@s30),AT(4063,2135),USE(tra:Address_Line3),FONT(,8),TRN
                         STRING(@s30),AT(156,2292),USE(def:OrderPostcode,,?def:OrderPostcode:2),FONT(,8),TRN
                         STRING(@s15),AT(4083,2292),USE(tra:Postcode),FONT(,8),TRN
                         STRING('Tel:'),AT(156,2448),USE(?String26),FONT(,8),TRN
                         STRING(@s15),AT(521,2448),USE(def:OrderTelephoneNumber,,?def:OrderTelephoneNumber:3),FONT(, |
  8),TRN
                         STRING('Tel:'),AT(4083,2448),USE(?String26:2),FONT(,8),TRN
                         STRING(@s15),AT(4396,2448,990,188),USE(tra:Telephone_Number),FONT(,8),TRN
                         STRING('Fax:'),AT(156,2604),USE(?String28),FONT(,8),TRN
                         STRING(@s30),AT(521,2604,990,188),USE(def:OrderFaxNumber,,?def:OrderFaxNumber:2),FONT(,8), |
  TRN
                         STRING('Fax:'),AT(4083,2604),USE(?String28:2),FONT(,8),TRN
                         STRING(@s15),AT(4396,2604,990,188),USE(tra:Fax_Number),FONT(,8),TRN
                         STRING(@s15),AT(156,3385),USE(tra:Account_Number),FONT(,8),TRN
                         STRING(@s8),AT(1615,3385),USE(ret:Ref_Number),FONT(,8,,FONT:bold),TRN
                         STRING(@d6b),AT(3083,3385),USE(ret:Date_Despatched),FONT(,8),TRN
                         STRING(@s20),AT(5844,938,1406,156),USE(tmp:PrintedBy),FONT(,8),TRN
                         STRING(@s30),AT(5990,3385,1563,208),USE(def:OrderTelephoneNumber,,?def:OrderTelephoneNumber:2), |
  FONT(,8),TRN
                         STRING('RRC GOODS RECEIVED NOTE'),AT(4396,104,2917,313),USE(?String19),FONT(,14,,FONT:bold), |
  TRN
                         STRING(@s30),AT(573,958,2495,201),USE(tra:Telephone_Number,,?tra:Telephone_Number:2),TRN
                         STRING('Printed by'),AT(5083,938),USE(?String67),FONT(,8,COLOR:Black,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(156,479,3135,201),USE(tra:Address_Line2,,?tra:Address_Line2:2),TRN
                         STRING('dd/mm/yyyy'),AT(5833,750),USE(?ReportDateStamp),FONT(,8,,FONT:bold),TRN
                         STRING('hh:mm'),AT(6583,750),USE(?ReportTimeStamp),FONT(,8,,FONT:bold),TRN
                       END
DETAIL                 DETAIL,AT(0,0,,198),USE(?DetailBand)
                         STRING(@s8),AT(125,0),USE(pq:QtyOrdered),FONT(,7,COLOR:Black),RIGHT,TRN
                         STRING(@s30),AT(1833,0),USE(pq:PartNo),FONT(,7,COLOR:Black),TRN
                         STRING(@s30),AT(3333,0),USE(pq:Description),FONT(,7,COLOR:Black),TRN
                         STRING(@s8),AT(833,0),USE(pq:QtyReceived),FONT(,7,COLOR:Black),RIGHT,TRN
                         STRING(@s30),AT(5052,0),USE(pq:Location),FONT(,7,COLOR:Black),LEFT,TRN
                         STRING(@n14.2),AT(6719,0),USE(total_cost_temp),FONT(,7,COLOR:Black,,CHARSET:ANSI),TRN
                       END
detail1                DETAIL,AT(0,0,,42),USE(?detail1),ABSOLUTE,PAGEAFTER(-1)
                       END
Totals                 DETAIL,AT(396,9396),USE(?Totals),ABSOLUTE
                         STRING('Total Items: '),AT(396,83),USE(?String47),FONT(,10,,FONT:bold),TRN
                         STRING('Total Lines: '),AT(396,323),USE(?String40),FONT(,,,FONT:bold),TRN
                         STRING(@p<<<<<<<#p),AT(1323,323,773,201),USE(Total_Lines_Temp),FONT(,10,,FONT:bold),RIGHT, |
  TRN
                         STRING(@n14.2),AT(6156,156),USE(total_cost_total_temp),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING(@p<<<<<<<#p),AT(1323,83,773,201),USE(Total_Quantity_Temp),FONT(,10,,FONT:bold),RIGHT, |
  TRN
                         STRING('Total GRN Value:'),AT(4844,156),USE(?String41),FONT(,,,FONT:bold),TRN
                       END
Continue               DETAIL,AT(396,9396),USE(?Continue),ABSOLUTE
                         STRING('Number of Items On Page : 20'),AT(313,104),USE(?String60),FONT(,,,FONT:bold),TRN
                         STRING('Continued Over -'),AT(5990,104),USE(?String60:2),FONT(,,,FONT:bold),TRN
                       END
detailExchange         DETAIL,AT(0,0,,188),USE(?detailExchange)
                         STRING(@n_8),AT(156,0),USE(pq:OrderNumber),FONT(,7,,,CHARSET:ANSI),RIGHT(1),TRN
                         STRING(@n_8),AT(833,0),USE(pq:QtyReceived,,?pq:QtyReceived:2),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s60),AT(1354,0,2917,156),USE(pq:PartNo,,?pq:PartNo:2),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(4427,0,1406,156),USE(pq:Description,,?pq:Description:2),FONT(,7,,,CHARSET:ANSI), |
  TRN
                         STRING(@n14.2),AT(5990,0),USE(pq:Cost),FONT(,7,,,CHARSET:ANSI),RIGHT,TRN
                         STRING(@n10.2),AT(6823,0),USE(pq:LineCost),FONT(,7,,,CHARSET:ANSI),RIGHT,TRN
                       END
                       FOOTER,AT(396,10156,7521,1125),USE(?FOOTER1)
                         TEXT,AT(104,521,7344,521),USE(stt:Text),FONT(,8)
                       END
                       FORM,AT(396,479,7521,10802),USE(?unnamed:2)
                         IMAGE('RINVDET.GIF'),AT(0,0,7521,11156),USE(?Image1)
                         STRING('Sale Number'),AT(1604,3177),USE(?String32),FONT(,8,,FONT:bold),TRN
                         STRING('Sales Date'),AT(3083,3177),USE(?String44),FONT(,8,,FONT:bold),TRN
                         STRING('Contact'),AT(4531,3177),USE(?String45),FONT(,8,,FONT:bold),TRN
                         STRING('Contact Number'),AT(5990,3177),USE(?String58),FONT(,8,,FONT:bold),TRN
                         STRING('Account Number'),AT(125,3177),USE(?String30),FONT(,8,,FONT:bold),TRN
                         STRING('DELIVERY ADDRESS'),AT(4010,1458),USE(?String42),FONT(,9,,FONT:bold),TRN
                         GROUP,AT(104,3698,7344,500),USE(?groupTitle1)
                           STRING('Qty Ordered'),AT(156,3854),USE(?strQtyOrdered),FONT(,8,,FONT:bold),TRN
                           STRING('Qty Received'),AT(990,3854),USE(?strQtyReceived),FONT(,8,,FONT:bold),TRN
                           STRING('Part Number'),AT(1875,3854),USE(?String34),FONT(,8,,FONT:bold),TRN
                           STRING('Description'),AT(3333,3854),USE(?String33),FONT(,8,,FONT:bold),TRN
                           STRING('Shelf Location'),AT(5052,3854),USE(?String36),FONT(,8,,FONT:bold),TRN
                           STRING('GRN Value'),AT(6719,3854),USE(?String66),FONT('Arial',8,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                         END
                         GROUP,AT(52,3646,7344,500),USE(?groupTitle2),HIDE,TRN
                           STRING('Line Cost'),AT(6792,3854),USE(?String69:7),FONT(,8,,FONT:bold),TRN
                           STRING('Order No'),AT(156,3854),USE(?String69),FONT(,8,,FONT:bold),TRN
                           STRING('Quantity'),AT(823,3854),USE(?String69:2),FONT(,8,,FONT:bold),TRN
                           STRING('Part Number / Model Number'),AT(1354,3854),USE(?String69:3),FONT(,8,,FONT:bold),TRN
                           STRING('Description'),AT(4427,3854),USE(?String69:4),FONT(,8,,FONT:bold),TRN
                           STRING('Item Cost'),AT(6135,3854),USE(?String69:6),FONT(,8,,FONT:bold),TRN
                         END
                         STRING('Received By'),AT(104,9844),USE(?String64),FONT('Arial',9,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                         LINE,AT(937,9990,2656,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('SUPPLIER ADDRESS'),AT(104,1458),USE(?String25),FONT(,9,,FONT:bold),TRN
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR1               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Goods_Received_Note_Retail')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:DEFPRINT.Open                                     ! File DEFPRINT used by this procedure, so make sure it's RelationManager is open
  Relate:EXCHANGE.Open                                     ! File EXCHANGE used by this procedure, so make sure it's RelationManager is open
  Relate:RETSALES.SetOpenRelated()
  Relate:RETSALES.Open                                     ! File RETSALES used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:STOCK.SetOpenRelated()
  Relate:STOCK.Open                                        ! File STOCK used by this procedure, so make sure it's RelationManager is open
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUPPLIER.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:RETSTOCK.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  IF (p_web.IfExistsValue('GRN'))
      p_web.StoreValue('GRN')
  END
  IF (p_web.IfExistsValue('INV'))
      p_web.StoreValue('INV')
  END
  IF (p_web.IfExistsValue('GRD'))
      p_web.StoreValue('GRD')
  END
  IF (p_web.IfExistsValue('REP'))
      p_web.StoreValue('REP')
  END
  
  tmp:Order_No_Filter = p_web.GSV('INV')
  tmp:Date_Received_Filter = p_web.GSV('GRD')
  
  !Fetch this traders address
  Access:tradeacc.clearkey(tra:Account_Number_Key)
  tra:Account_Number = p_web.GSV('BookingAccount')
  access:tradeacc.fetch(tra:account_Number_key)
  
  
  !fetch the record
  Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
  ret:Invoice_Number = tmp:Order_No_Filter
  Access:RETSALES.TryFetch(ret:Invoice_Number_Key)
  
  
  Access:USERS.Clearkey(use:Password_Key)
  use:Password = p_web.GSV('BookingUserPassword')
  Access:USERS.TryFetch(use:Password_Key)
  tmp:PrintedBy = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
  
  !  Access:STANTEXT.Clearkey(stt:Description_Key)
  !  stt:Description = 'DESPATCH NOTE - RETAIL'
  !  Access:STANTEXT.Tryfetch(stt:Description_Key)
  ! Bryan: No point this here as it's called again below
  SELF.Open(ProgressWindow)                                ! Open window
  If p_web.GSV('REP') = 'Y' ! Reprint
      SetTarget(Report)
      ?Reprint{prop:Hide} = 0
      SetTarget()
  End !glo:Select4 = 'REPRINT'
  !Alternative Invoice Address
  Set(Defaults)
  access:Defaults.next()
  
  !removed ref G132/L277
  
  !If def:use_invoice_address = 'YES' And def:use_for_order = 'YES'
  !    Default_Invoice_Company_Name_Temp       = DEF:Invoice_Company_Name
  !    Default_Invoice_Address_Line1_Temp      = DEF:Invoice_Address_Line1
  !    Default_Invoice_Address_Line2_Temp      = DEF:Invoice_Address_Line2
  !    Default_Invoice_Address_Line3_Temp      = DEF:Invoice_Address_Line3
  !    Default_Invoice_Postcode_Temp           = DEF:Invoice_Postcode
  !    Default_Invoice_Telephone_Number_Temp   = DEF:Invoice_Telephone_Number
  !    Default_Invoice_Fax_Number_Temp         = DEF:Invoice_Fax_Number
  !    Default_Invoice_VAT_Number_Temp         = DEF:Invoice_VAT_Number
  !Else!If def:use_invoice_address = 'YES'
  !    Default_Invoice_Company_Name_Temp       = DEF:User_Name
  !    Default_Invoice_Address_Line1_Temp      = DEF:Address_Line1
  !    Default_Invoice_Address_Line2_Temp      = DEF:Address_Line2
  !    Default_Invoice_Address_Line3_Temp      = DEF:Address_Line3
  !    Default_Invoice_Postcode_Temp           = DEF:Postcode
  !    Default_Invoice_Telephone_Number_Temp   = DEF:Telephone_Number
  !    Default_Invoice_Fax_Number_Temp         = DEF:Fax_Number
  !    Default_Invoice_VAT_Number_Temp         = DEF:VAT_Number
  !End!If def:use_invoice_address = 'YES'
  
  ! Display standard text (DBH: 10/08/2006)
  Access:STANTEXT.ClearKey(stt:Description_Key)
  stt:Description = 'PARTS ORDER'
  If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Found
  Else ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Error
  End ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
  
  Do DefineListboxStyle
  INIMgr.Fetch('Goods_Received_Note_Retail',ProgressWindow) ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  Loc:NoRecords = 1 ! Default To No Records As Browse is "HandCoded"
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:RETSALES, ?Progress:PctText, Progress:Thermometer, ProgressMgr, ret:Invoice_Number)
  ThisReport.AddSortOrder(ret:Invoice_Number_Key)
  ThisReport.AddRange(ret:Invoice_Number,tmp:Order_No_Filter)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:RETSALES.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE.Close
    Relate:RETSALES.Close
    Relate:STANTEXT.Close
    Relate:STOCK.Close
  END
  IF SELF.Opened
    INIMgr.Update('Goods_Received_Note_Retail',ProgressWindow) ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'Goods_Received_Note_Retail',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.Init(Report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,Report{PROPPRINT:Paper},PAPER:USER), CHOOSE(Report{PROP:Thous}=True,PROP:Thous,CHOOSE(Report{PROP:MM}=True,PROP:MM,CHOOSE(Report{PROP:Points}=True,PROP:Points,0))), Report{PROPPRINT:PaperWidth}, Report{PROPPRINT:PaperHeight}, Report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetTitle('Goods Received Note')      !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR1.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetViewerPrefs(PDFXTR1:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@d06)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'Goods_Received_Note_Retail',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR1.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  IF (ret:ExchangeOrder = 1 OR ret:LoanOrder = 1)
      SETTARGET(REPORT)
      ?groupTitle1{prop:hide} = 1
      ?groupTitle2{prop:hide} = 0
      SETTARGET()
  END
  
  Access:RETSTOCK.ClearKey(res:Part_Number_Key)
  res:Ref_Number  = ret:Ref_Number
  Set(res:Part_Number_Key,res:Part_Number_Key)
  Loop
      If Access:RETSTOCK.NEXT()
         Break
      End !If
      If res:Ref_Number  <> ret:Ref_Number      |
          Then Break.  ! End If
      If ~res:Received
          Cycle
      End !If ~res:Received
      If res:GRNNumber <> p_web.GSV('GRN')
          Cycle
      End !If res:GRNNumber <> glo:Select1
  
      IF (ret:ExchangeOrder = 1 OR ret:LoanOrder = 1)
          Parts_Q.PartNo = res:Part_Number
  
          Access:STOCK.Clearkey(sto:Location_Key)
          sto:Location     = VodacomClass.MainStoreLocation()
          sto:Part_Number  = res:Part_Number
          IF (Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign)
  
          END ! IF (Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign)
  
          IF (ret:ExchangeOrder = 1)
              Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
              xch:Ref_Number = res:ExchangeRefNumber
              IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                  Parts_Q.Description = xch:ESN
              ELSE ! IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                  Parts_Q.Description = ''
              END ! IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
              Parts_Q.PartNo  = CLIP(Parts_Q.PartNo) & ' ' & CLIP(sto:ExchangeModelNumber)
          END
          IF (ret:LoanOrder = 1)
              Access:LOAN.ClearKey(loa:Ref_Number_Key)
              loa:Ref_Number = res:LoanRefNumber
              IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
                  Parts_Q.Description = loa:ESN
              ELSE
                  Parts_Q.Description = ''
              END
              Parts_Q.PartNo  = CLIP(Parts_Q.PartNo) & ' ' & CLIP(sto:LoanModelNumber)
          END
  
  
          Parts_Q.QtyReceived = res:Quantity
          Parts_Q.Cost        = res:Item_Cost
          Parts_Q.LineCost    = res:Item_Cost
          Parts_Q.OrderNumber = res:Purchase_Order_Number
  
          ADD(Parts_Q)
      ElSE ! IF (ret:ExchangeOrder = 1)
  
  
          Parts_Q.pq:PartNo = res:Part_Number
          Parts_Q.pq:Cost = res:Purchase_Cost
          get(Parts_Q, Parts_Q.pq:PartNo, Parts_Q:pq:Cost)
          if error()  ! Insert queue entry
              Parts_Q.pq:PartNo = res:Part_Number
              Parts_Q.pq:Description = res:Description
              Parts_Q.pq:QtyOrdered = res:Quantity   !=====================================
              Parts_Q.pq:Cost = res:Item_Cost        ! Was purchase_cost    ref G132 / l277
              if res:Despatched = 'YES'              !=====================================
      !            if res:QuantityReceived <> 0
                      Parts_Q.pq:QtyReceived = res:QuantityReceived
      !            else
      !                Parts_Q.pq:QtyReceived = res:Quantity
      !            end
              else
                  Parts_Q.pq:QtyReceived = 0
              end
              Parts_Q.pq:Location = ''
  
              !Find the shelf location of the RRC's part
              Access:USERS.Clearkey(use:Password_Key)
              use:Password    = p_web.GSV('BookingUserPassword')
              If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                  !Found
                  Access:STOCK.Clearkey(sto:Location_Key)
                  sto:Location    = use:Location
                  sto:Part_Number = res:Part_Number
                  If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                      !Found
                      Parts_Q.pq:Location = sto:Shelf_Location
                  Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                      !Error
                  End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
              Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                  !Error
              End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  
              add(Parts_Q, Parts_Q.pq:PartNo, Parts_Q.pq:Cost)
          else
              Parts_Q.pq:QtyOrdered += res:Quantity
              if res:Despatched = 'YES'
      !            if res:QuantityReceived <> 0
                      Parts_Q.pq:QtyReceived += res:QuantityReceived
      !            else
      !                Parts_Q.pq:QtyReceived += res:Quantity
      !            end
              end
              put(Parts_Q, Parts_Q.pq:PartNo, Parts_Q.pq:Cost)
          end
      END ! IF (ret:ExchangeOrder = 1)
  end
  
  sort(Parts_Q, Parts_Q.pq:PartNo)
  
  loop a# = 1 to records(Parts_Q)
      get(Parts_Q,a#)
  
  !TB13128 - non-printing GRN - J - 06/01/14
  !What is this supposed to do? It puts a blank page at the beginning of the report!
  !    If order_temp <> res:Ref_Number And first_page_temp <> 1
  !        Print(rpt:detail1)
  !    End
  
      tmp:RecordsCount += 1
      count# += 1
      If count# > 20
          Print(rpt:continue)
          Print(rpt:detail1)
          count# = 1
      End!If count# > 25
  
      total_cost_temp = Parts_Q.pq:Cost * Parts_Q.pq:QtyReceived
      total_cost_total_temp += total_cost_temp
      total_quantity_temp   += Parts_Q.pq:QtyReceived
      total_lines_temp      += 1
  
        IF (ret:ExchangeOrder = 1 OR ret:LoanOrder = 1)
            Print(rpt:detailExchange)
        ELSE
            Print(rpt:detail)
        END
        
        Loc:NoRecords = 0 ! Something has been printed
        
      order_temp = res:Ref_Number
      first_page_temp = 0
      print# = 1
  end
  
  If print# = 1
      Print(rpt:totals)
  End !If print# = 1
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR1:rtn = PDFXTR1.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR1:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

