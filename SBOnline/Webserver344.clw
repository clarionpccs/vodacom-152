

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER344.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER010.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER345.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER346.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER347.INC'),ONCE        !Req'd for module callout resolution
                     END


FormStatusReportCriteria PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locEndDate           DATE                                  !
locStartDate         DATE                                  !
locSavedCriteria     STRING(30)                            !
locPaperReportType   STRING(1)                             !
locExportJobs        STRING(1)                             !
locExportAudit       STRING(1)                             !
locExportCParts      STRING(1)                             !
locExportWParts      STRING(1)                             !
locStatusType        STRING(1)                             !
locReportOrder       STRING(30)                            !
locDateRangeType     STRING(1)                             !
locShowCustomerStatus LONG                                 !
locExportType        STRING(1)                             !
locTagAllSubAccounts LONG                                  !
locTagAllGenericAccounts LONG                              !
locTagAllStatusTypes LONG                                  !
locTagAllLocations   LONG                                  !
FilesOpened     Long
SUBTRACC::State  USHORT
STATCRIT::State  USHORT
STATREP::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
locSavedCriteria_OptionView   View(STATREP)
                          Project(star:Description)
                        End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormStatusReportCriteria')
  loc:formname = 'FormStatusReportCriteria_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormStatusReportCriteria',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormStatusReportCriteria','')
      do SendPacket
      Do newcriteriaform
      do SendPacket
    p_web._DivHeader('FormStatusReportCriteria',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormStatusReportCriteria',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStatusReportCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStatusReportCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormStatusReportCriteria',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStatusReportCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormStatusReportCriteria',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormStatusReportCriteria',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
DeleteSessionValues ROUTINE ! Auto generated by Bryan's Template
    ! Local Variables
    ! p_web.DeleteSessionValue('Ans') ! Excluded
    p_web.DeleteSessionValue('locEndDate')
    p_web.DeleteSessionValue('locStartDate')
    p_web.DeleteSessionValue('locSavedCriteria')
    p_web.DeleteSessionValue('locPaperReportType')
    p_web.DeleteSessionValue('locExportJobs')
    p_web.DeleteSessionValue('locExportAudit')
    p_web.DeleteSessionValue('locExportCParts')
    p_web.DeleteSessionValue('locExportWParts')
    p_web.DeleteSessionValue('locStatusType')
    p_web.DeleteSessionValue('locReportOrder')
    p_web.DeleteSessionValue('locDateRangeType')
    p_web.DeleteSessionValue('locShowCustomerStatus')
    p_web.DeleteSessionValue('locExportType')
    p_web.DeleteSessionValue('locTagAllSubAccounts')
    p_web.DeleteSessionValue('locTagAllGenericAccounts')
    p_web.DeleteSessionValue('locTagAllStatusTypes')
    p_web.DeleteSessionValue('locTagAllLocations')

    ! Other Variables
OpenFiles  ROUTINE
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(STATCRIT)
  p_web._OpenFile(STATREP)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(STATCRIT)
  p_Web._CloseFile(STATREP)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormStatusReportCriteria_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('locStartDate')
    p_web.SetPicture('locStartDate','@d06b')
  End
  p_web.SetSessionPicture('locStartDate','@d06b')
  If p_web.IfExistsValue('locEndDate')
    p_web.SetPicture('locEndDate','@d06b')
  End
  p_web.SetSessionPicture('locEndDate','@d06b')
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('tab') = 1
    loc:TabNumber += 1
  End
  If p_web.GSV('tab') = 2 OR p_web.GSV('tab') = 3
    loc:TabNumber += 1
  End
  If p_web.GSV('tab') = 4
    loc:TabNumber += 1
  End
  If p_web.GSV('tab') = 5
    loc:TabNumber += 1
  End
  If p_web.GSV('tab') = 6
    loc:TabNumber += 1
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locSavedCriteria',locSavedCriteria)
  p_web.SetSessionValue('locPaperReportType',locPaperReportType)
  p_web.SetSessionValue('locExportJobs',locExportJobs)
  p_web.SetSessionValue('locExportAudit',locExportAudit)
  p_web.SetSessionValue('locExportCParts',locExportCParts)
  p_web.SetSessionValue('locExportWParts',locExportWParts)
  p_web.SetSessionValue('locExportType',locExportType)
  p_web.SetSessionValue('locTagAllSubAccounts',locTagAllSubAccounts)
  p_web.SetSessionValue('locTagAllGenericAccounts',locTagAllGenericAccounts)
  p_web.SetSessionValue('locTagAllStatusTypes',locTagAllStatusTypes)
  p_web.SetSessionValue('locStatusType',locStatusType)
  p_web.SetSessionValue('locShowCustomerStatus',locShowCustomerStatus)
  p_web.SetSessionValue('locTagAllLocations',locTagAllLocations)
  p_web.SetSessionValue('locReportOrder',locReportOrder)
  p_web.SetSessionValue('locDateRangeType',locDateRangeType)
  p_web.SetSessionValue('locStartDate',locStartDate)
  p_web.SetSessionValue('locEndDate',locEndDate)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locSavedCriteria')
    locSavedCriteria = p_web.GetValue('locSavedCriteria')
    p_web.SetSessionValue('locSavedCriteria',locSavedCriteria)
  End
  if p_web.IfExistsValue('locPaperReportType')
    locPaperReportType = p_web.GetValue('locPaperReportType')
    p_web.SetSessionValue('locPaperReportType',locPaperReportType)
  End
  if p_web.IfExistsValue('locExportJobs')
    locExportJobs = p_web.GetValue('locExportJobs')
    p_web.SetSessionValue('locExportJobs',locExportJobs)
  End
  if p_web.IfExistsValue('locExportAudit')
    locExportAudit = p_web.GetValue('locExportAudit')
    p_web.SetSessionValue('locExportAudit',locExportAudit)
  End
  if p_web.IfExistsValue('locExportCParts')
    locExportCParts = p_web.GetValue('locExportCParts')
    p_web.SetSessionValue('locExportCParts',locExportCParts)
  End
  if p_web.IfExistsValue('locExportWParts')
    locExportWParts = p_web.GetValue('locExportWParts')
    p_web.SetSessionValue('locExportWParts',locExportWParts)
  End
  if p_web.IfExistsValue('locExportType')
    locExportType = p_web.GetValue('locExportType')
    p_web.SetSessionValue('locExportType',locExportType)
  End
  if p_web.IfExistsValue('locTagAllSubAccounts')
    locTagAllSubAccounts = p_web.GetValue('locTagAllSubAccounts')
    p_web.SetSessionValue('locTagAllSubAccounts',locTagAllSubAccounts)
  End
  if p_web.IfExistsValue('locTagAllGenericAccounts')
    locTagAllGenericAccounts = p_web.GetValue('locTagAllGenericAccounts')
    p_web.SetSessionValue('locTagAllGenericAccounts',locTagAllGenericAccounts)
  End
  if p_web.IfExistsValue('locTagAllStatusTypes')
    locTagAllStatusTypes = p_web.GetValue('locTagAllStatusTypes')
    p_web.SetSessionValue('locTagAllStatusTypes',locTagAllStatusTypes)
  End
  if p_web.IfExistsValue('locStatusType')
    locStatusType = p_web.GetValue('locStatusType')
    p_web.SetSessionValue('locStatusType',locStatusType)
  End
  if p_web.IfExistsValue('locShowCustomerStatus')
    locShowCustomerStatus = p_web.GetValue('locShowCustomerStatus')
    p_web.SetSessionValue('locShowCustomerStatus',locShowCustomerStatus)
  End
  if p_web.IfExistsValue('locTagAllLocations')
    locTagAllLocations = p_web.GetValue('locTagAllLocations')
    p_web.SetSessionValue('locTagAllLocations',locTagAllLocations)
  End
  if p_web.IfExistsValue('locReportOrder')
    locReportOrder = p_web.GetValue('locReportOrder')
    p_web.SetSessionValue('locReportOrder',locReportOrder)
  End
  if p_web.IfExistsValue('locDateRangeType')
    locDateRangeType = p_web.GetValue('locDateRangeType')
    p_web.SetSessionValue('locDateRangeType',locDateRangeType)
  End
  if p_web.IfExistsValue('locStartDate')
    locStartDate = p_web.dformat(clip(p_web.GetValue('locStartDate')),'@d06b')
    p_web.SetSessionValue('locStartDate',locStartDate)
  End
  if p_web.IfExistsValue('locEndDate')
    locEndDate = p_web.dformat(clip(p_web.GetValue('locEndDate')),'@d06b')
    p_web.SetSessionValue('locEndDate',locEndDate)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormStatusReportCriteria_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  ! Open Window
    IF (p_web.GetValue('tab') <> 6)
        p_web.site.SaveButton.Class = 'hidden'
    END ! IF
    IF (p_web.GetValue('FirstTime') = 1)
        DO DeleteSessionValues
        p_web.SetValue('tab',1)
        ClearGenericTagFile(p_web)
        p_web.SSV('locStatusType','0')
        p_web.SSV('locShowCustomerStatus',1)
        p_web.SSV('locStartDate',DEFORMAT('01/01/2000',@d06))
        p_web.SSV('locEndDate',TODAY())
        p_web.SSV('locDateRangeType',0)
        p_web.SSV('TagSubAccountsType','') ! Clear Tag SubAccounts Settings
        p_web.SSV('locTagAllSubAccounts',1)  ! #13587 Set "All" tick boxes to true (DBH: 04/09/2015)
        p_web.SSV('locTagAllGenericAccounts',1)
        p_web.SSV('locTagAllStatusTypes',1)
        p_web.SSV('locTagAllLocations',1)
    END ! IF
    IF (p_web.IfExistsValue('tab'))
        p_web.StoreValue('tab')
    ELSE
        p_web.SSV('tab',1)
    END ! IF
    
    CASE p_web.GSV('tab')
    OF 1
    OF 2
        p_web.SSV('locGenericAccounts',0)
        p_web.SSV('AccountTabTitle','Sub Accounts')
    OF 3
        p_web.SSV('locGenericAccounts',1)
        p_web.SSV('AccountTabTitle','Generic Accounts')
        
    OF ''
        p_web.SSV('tab',1)
    END ! CASE
  
    IF (p_web.GetValue('ShowCriteria') = 1)
        packet = CLIP(packet) & |
            '<script>openNewCriteria();</script>'
    END ! IF   
    
      p_web.site.SaveButton.TextValue = 'Export'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locSavedCriteria = p_web.RestoreValue('locSavedCriteria')
 locPaperReportType = p_web.RestoreValue('locPaperReportType')
 locExportJobs = p_web.RestoreValue('locExportJobs')
 locExportAudit = p_web.RestoreValue('locExportAudit')
 locExportCParts = p_web.RestoreValue('locExportCParts')
 locExportWParts = p_web.RestoreValue('locExportWParts')
 locExportType = p_web.RestoreValue('locExportType')
 locTagAllSubAccounts = p_web.RestoreValue('locTagAllSubAccounts')
 locTagAllGenericAccounts = p_web.RestoreValue('locTagAllGenericAccounts')
 locTagAllStatusTypes = p_web.RestoreValue('locTagAllStatusTypes')
 locStatusType = p_web.RestoreValue('locStatusType')
 locShowCustomerStatus = p_web.RestoreValue('locShowCustomerStatus')
 locTagAllLocations = p_web.RestoreValue('locTagAllLocations')
 locReportOrder = p_web.RestoreValue('locReportOrder')
 locDateRangeType = p_web.RestoreValue('locDateRangeType')
 locStartDate = p_web.RestoreValue('locStartDate')
 locEndDate = p_web.RestoreValue('locEndDate')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'javascript:runStatusReport(''E'');'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormStatusReportCriteria_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormStatusReportCriteria_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormStatusReportCriteria_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'FormReports'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormStatusReportCriteria" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormStatusReportCriteria" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormStatusReportCriteria" ></input><13,10>'
  end

  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormStatusReportCriteria">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormStatusReportCriteria" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  do GenerateTab4
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormStatusReportCriteria')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GSV('tab') = 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Report Type') & ''''
        End
        If p_web.GSV('tab') = 2 OR p_web.GSV('tab') = 3
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate(p_web.GSV('AccountTabTitle')) & ''''
        End
        If p_web.GSV('tab') = 4
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Status Types') & ''''
        End
        If p_web.GSV('tab') = 5
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Internal Locations') & ''''
        End
        If p_web.GSV('tab') = 6
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Final Settings') & ''''
        End
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormStatusReportCriteria')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormStatusReportCriteria'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormStatusReportCriteria_TagSubAccounts_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormStatusReportCriteria_TagStatusTypes_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormStatusReportCriteria_TagInternalLocations_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('tab') = 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.locSavedCriteria')
    ElsIf p_web.GSV('tab') = 2 OR p_web.GSV('tab') = 3
        p_web.SetValue('SelectField',clip(loc:formname) & '.locTagAllSubAccounts')
    ElsIf p_web.GSV('tab') = 4
        p_web.SetValue('SelectField',clip(loc:formname) & '.locTagAllStatusTypes')
    ElsIf p_web.GSV('tab') = 5
        p_web.SetValue('SelectField',clip(loc:formname) & '.locTagAllLocations')
    ElsIf p_web.GSV('tab') = 6
        p_web.SetValue('SelectField',clip(loc:formname) & '.locReportOrder')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GSV('tab') = 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          End
          If p_web.GSV('tab') = 2 OR p_web.GSV('tab') = 3
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          End
          If p_web.GSV('tab') = 4
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          End
          If p_web.GSV('tab') = 5
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          End
          If p_web.GSV('tab') = 6
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab5'''
          End
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormStatusReportCriteria')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GSV('tab') = 1
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    end
    if p_web.GSV('tab') = 2 OR p_web.GSV('tab') = 3
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    end
    if p_web.GSV('tab') = 4
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
    end
    if p_web.GSV('tab') = 5
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
    end
    if p_web.GSV('tab') = 6
      packet = clip(packet) & 'roundCorners(''tab5'');'&CRLF
    end
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GSV('tab') = 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Report Type') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormStatusReportCriteria_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Report Type')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Report Type')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Report Type')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Report Type')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSavedCriteria
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSavedCriteria
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If false
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__line1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If false
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPaperReportType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPaperReportType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__line2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtExportFile
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExportJobs
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExportJobs
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExportAudit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExportAudit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExportCParts
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExportCParts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExportWParts
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExportWParts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExportType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExportType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__hr1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnPrev
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnNext
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
  If p_web.GSV('tab') = 2 OR p_web.GSV('tab') = 3
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate(p_web.GSV('AccountTabTitle')) &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormStatusReportCriteria_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate(p_web.GSV('AccountTabTitle'))&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate(p_web.GSV('AccountTabTitle'))&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate(p_web.GSV('AccountTabTitle'))&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate(p_web.GSV('AccountTabTitle'))&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    If p_web.GSV('tab') = 2
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locTagAllSubAccounts
      do Value::locTagAllSubAccounts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('tab') = 3
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locTagAllGenericAccounts
      do Value::locTagAllGenericAccounts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::txtSubAccounts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwSubAccounts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnTagAll
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnUnTagAll
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__hr2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnBack2
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnNext2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab2  Routine
  If p_web.GSV('tab') = 4
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Status Types') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormStatusReportCriteria_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Status Types')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Status Types')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Status Types')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Status Types')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locTagAllStatusTypes
      do Value::locTagAllStatusTypes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::txtStatusTypes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locStatusType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locStatusType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If FALSE
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locShowCustomerStatus
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locShowCustomerStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnTagAll4
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnUnTagAll4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__hr4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnBack4
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnNext4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab3  Routine
  If p_web.GSV('tab') = 5
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Internal Locations') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormStatusReportCriteria_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Internal Locations')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Internal Locations')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Internal Locations')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Internal Locations')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locTagAllLocations
      do Value::locTagAllLocations
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::txtInternalLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwInternalLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnTagAll5
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnUnTagAll5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__hr5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnBack5
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnNext5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab4  Routine
  If p_web.GSV('tab') = 6
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel5">'&CRLF &|
                                    '  <div id="panel5Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Final Settings') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel5Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormStatusReportCriteria_5">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Final Settings')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab5" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab5">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Final Settings')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Final Settings')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Final Settings')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locReportOrder
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locReportOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locDateRangeType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locDateRangeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locStartDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locStartDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEndDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEndDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnSaveCriteria
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If false
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnPrintReport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('locExportJobs') = 1 OR p_web.GSV('locExportAudit') = 1 OR p_web.GSV('locExportCParts') = 1 OR p_web.GSV('locExportWParts') = 1 or false
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnExport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__hr6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnBack6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end


Prompt::locSavedCriteria  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locSavedCriteria') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Load Previous Criteria')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locSavedCriteria  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSavedCriteria',p_web.GetValue('NewValue'))
    locSavedCriteria = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSavedCriteria
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locSavedCriteria',p_web.GetValue('Value'))
    locSavedCriteria = p_web.GetValue('Value')
  End
  do Value::locSavedCriteria
  do SendAlert

Value::locSavedCriteria  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locSavedCriteria') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locSavedCriteria')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&clip('window.open(''PageProcess?ProcessType=StatusReportCriteriaGet&RedirectURL=FormStatusReportCriteria&Criteria='' + this.value,''_self'')')&';'
  loc:javascript = clip(loc:javascript) & ' ' &p_web._nocolon('sv(''locSavedCriteria'',''formstatusreportcriteria_locsavedcriteria_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locSavedCriteria',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locSavedCriteria') = 0
    p_web.SetSessionValue('locSavedCriteria','')
  end
    packet = clip(packet) & p_web.CreateOption('','',choose('' = p_web.getsessionvalue('locSavedCriteria')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(SUBTRACC)
  bind(sub:Record)
  p_web._OpenFile(STATCRIT)
  bind(stac:Record)
  p_web._OpenFile(STATREP)
  bind(star:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locSavedCriteria_OptionView)
  locSavedCriteria_OptionView{prop:filter} = 'UPPER(star:Location) = UPPER(''' & p_web.GSV('BookingAccount') & ''')'
  locSavedCriteria_OptionView{prop:order} = 'UPPER(star:Description)'
  Set(locSavedCriteria_OptionView)
  Loop
    Next(locSavedCriteria_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locSavedCriteria') = 0
      p_web.SetSessionValue('locSavedCriteria',star:Description)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,star:Description,choose(star:Description = p_web.getsessionvalue('locSavedCriteria')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locSavedCriteria_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(STATCRIT)
  p_Web._CloseFile(STATREP)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('locSavedCriteria') & '_value')


Validate::hidden1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden1',p_web.GetValue('NewValue'))
    do Value::hidden1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden1  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('hidden1') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::__line1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__line1',p_web.GetValue('NewValue'))
    do Value::__line1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__line1  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('__line1') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locPaperReportType  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locPaperReportType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Paper Report Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locPaperReportType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPaperReportType',p_web.GetValue('NewValue'))
    locPaperReportType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPaperReportType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPaperReportType',p_web.GetValue('Value'))
    locPaperReportType = p_web.GetValue('Value')
  End
  do Value::locPaperReportType
  do SendAlert

Value::locPaperReportType  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locPaperReportType') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locPaperReportType
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locPaperReportType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locPaperReportType') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locPaperReportType'',''formstatusreportcriteria_locpaperreporttype_value'',1,'''&clip(0)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locPaperReportType',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locPaperReportType_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Detailed') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locPaperReportType') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locPaperReportType'',''formstatusreportcriteria_locpaperreporttype_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locPaperReportType',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locPaperReportType_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Summary') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locPaperReportType') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locPaperReportType'',''formstatusreportcriteria_locpaperreporttype_value'',1,'''&clip(2)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locPaperReportType',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locPaperReportType_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Status Change') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('locPaperReportType') & '_value')


Validate::__line2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__line2',p_web.GetValue('NewValue'))
    do Value::__line2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__line2  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('__line2') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::txtExportFile  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtExportFile',p_web.GetValue('NewValue'))
    do Value::txtExportFile
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtExportFile  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('txtExportFile') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web.Translate('Export File Details',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locExportJobs  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locExportJobs') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Export Jobs File')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locExportJobs  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExportJobs',p_web.GetValue('NewValue'))
    locExportJobs = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExportJobs
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locExportJobs',p_web.GetValue('Value'))
    locExportJobs = p_web.GetValue('Value')
  End
  do Value::locExportJobs
  do SendAlert

Value::locExportJobs  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locExportJobs') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locExportJobs
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locExportJobs'',''formstatusreportcriteria_locexportjobs_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locExportJobs') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locExportJobs',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('locExportJobs') & '_value')


Prompt::locExportAudit  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locExportAudit') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Export Audit File')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locExportAudit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExportAudit',p_web.GetValue('NewValue'))
    locExportAudit = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExportAudit
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locExportAudit',p_web.GetValue('Value'))
    locExportAudit = p_web.GetValue('Value')
  End
  do Value::locExportAudit
  do SendAlert

Value::locExportAudit  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locExportAudit') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locExportAudit
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locExportAudit'',''formstatusreportcriteria_locexportaudit_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locExportAudit') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locExportAudit',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('locExportAudit') & '_value')


Prompt::locExportCParts  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locExportCParts') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Export Chargeable Parts File')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locExportCParts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExportCParts',p_web.GetValue('NewValue'))
    locExportCParts = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExportCParts
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locExportCParts',p_web.GetValue('Value'))
    locExportCParts = p_web.GetValue('Value')
  End
  do Value::locExportCParts
  do SendAlert

Value::locExportCParts  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locExportCParts') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locExportCParts
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locExportCParts'',''formstatusreportcriteria_locexportcparts_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locExportCParts') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locExportCParts',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('locExportCParts') & '_value')


Prompt::locExportWParts  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locExportWParts') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Export Warranty Parts File')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locExportWParts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExportWParts',p_web.GetValue('NewValue'))
    locExportWParts = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExportWParts
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locExportWParts',p_web.GetValue('Value'))
    locExportWParts = p_web.GetValue('Value')
  End
  do Value::locExportWParts
  do SendAlert

Value::locExportWParts  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locExportWParts') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locExportWParts
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locExportWParts'',''formstatusreportcriteria_locexportwparts_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locExportWParts') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locExportWParts',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('locExportWParts') & '_value')


Prompt::locExportType  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locExportType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Export Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locExportType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExportType',p_web.GetValue('NewValue'))
    locExportType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExportType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locExportType',p_web.GetValue('Value'))
    locExportType = p_web.GetValue('Value')
  End
  do Value::locExportType
  do SendAlert

Value::locExportType  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locExportType') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locExportType
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locExportType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locExportType') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locExportType'',''formstatusreportcriteria_locexporttype_value'',1,'''&clip(0)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locExportType',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locExportType_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Long Version') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locExportType') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locExportType'',''formstatusreportcriteria_locexporttype_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locExportType',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locExportType_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Short Version') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('locExportType') & '_value')


Validate::__hr1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__hr1',p_web.GetValue('NewValue'))
    do Value::__hr1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__hr1  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('__hr1') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::btnPrev  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnPrev',p_web.GetValue('NewValue'))
    do Value::btnPrev
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::btnPrev  !1

Value::btnPrev  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('btnPrev') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnPrev'',''formstatusreportcriteria_btnprev_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnPrev','','MainButton',loc:formname,,,,loc:javascript,0,'images/listtop.png',,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('btnPrev') & '_value')


Validate::btnNext  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnNext',p_web.GetValue('NewValue'))
    do Value::btnNext
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnNext  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('btnNext') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnNext','Next','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormStatusReportCriteria?' &'tab=2')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/listnext.png',,,,)

  do SendPacket
  p_web._DivFooter()


Prompt::locTagAllSubAccounts  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locTagAllSubAccounts') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('All Accounts')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locTagAllSubAccounts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locTagAllSubAccounts',p_web.GetValue('NewValue'))
    locTagAllSubAccounts = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locTagAllSubAccounts
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locTagAllSubAccounts',p_web.GetValue('Value'))
    locTagAllSubAccounts = p_web.GetValue('Value')
  End
    DO Value::txtSubAccounts
    DO Value::brwSubAccounts
    DO Value::btnTagAll
    DO Value::btnUnTagAll
  do Value::locTagAllSubAccounts
  do SendAlert

Value::locTagAllSubAccounts  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locTagAllSubAccounts') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locTagAllSubAccounts
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locTagAllSubAccounts'',''formstatusreportcriteria_loctagallsubaccounts_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locTagAllSubAccounts') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locTagAllSubAccounts',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('locTagAllSubAccounts') & '_value')


Prompt::locTagAllGenericAccounts  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locTagAllGenericAccounts') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('All Accounts')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locTagAllGenericAccounts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locTagAllGenericAccounts',p_web.GetValue('NewValue'))
    locTagAllGenericAccounts = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locTagAllGenericAccounts
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locTagAllGenericAccounts',p_web.GetValue('Value'))
    locTagAllGenericAccounts = p_web.GetValue('Value')
  End
    DO Value::txtSubAccounts
    DO Value::brwSubAccounts
    DO Value::btnTagAll
    DO Value::btnUnTagAll
  do Value::locTagAllGenericAccounts
  do SendAlert

Value::locTagAllGenericAccounts  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locTagAllGenericAccounts') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locTagAllGenericAccounts
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locTagAllGenericAccounts'',''formstatusreportcriteria_loctagallgenericaccounts_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locTagAllGenericAccounts') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locTagAllGenericAccounts',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('locTagAllGenericAccounts') & '_value')


Validate::txtSubAccounts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtSubAccounts',p_web.GetValue('NewValue'))
    do Value::txtSubAccounts
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtSubAccounts  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('txtSubAccounts') & '_value',Choose((p_web.GSV('tab') = 2 AND p_web.GSV('locTagAllSubAccounts') = 1) OR (p_web.GSV('tab') = 3 AND p_web.GSV('locTagAllGenericAccounts') = 1),'hdiv','adiv'))
  loc:extra = ''
  If Not ((p_web.GSV('tab') = 2 AND p_web.GSV('locTagAllSubAccounts') = 1) OR (p_web.GSV('tab') = 3 AND p_web.GSV('locTagAllGenericAccounts') = 1))
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web.Translate('If any entries are tagged, then all other entries will be excluded from the report.',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Validate::brwSubAccounts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwSubAccounts',p_web.GetValue('NewValue'))
    do Value::brwSubAccounts
  Else
    p_web.StoreValue('sub:RecordNumber')
  End

Value::brwSubAccounts  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose((p_web.GSV('tab') = 2 AND p_web.GSV('locTagAllSubAccounts') = 1) OR (p_web.GSV('tab') = 3 AND p_web.GSV('locTagAllGenericAccounts') = 1),1,0))
  ! --- BROWSE ---  TagSubAccounts --
  p_web.SetValue('TagSubAccounts:NoForm',1)
  p_web.SetValue('TagSubAccounts:FormName',loc:formname)
  p_web.SetValue('TagSubAccounts:parentIs','Form')
  p_web.SetValue('_parentProc','FormStatusReportCriteria')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormStatusReportCriteria_TagSubAccounts_embedded_div')&'"><!-- Net:TagSubAccounts --></div><13,10>'
    p_web._DivHeader('FormStatusReportCriteria_' & lower('TagSubAccounts') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormStatusReportCriteria_' & lower('TagSubAccounts') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagSubAccounts --><13,10>'
  end
  do SendPacket


Validate::btnTagAll  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnTagAll',p_web.GetValue('NewValue'))
    do Value::btnTagAll
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::brwSubAccounts  !1

Value::btnTagAll  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('btnTagAll') & '_value',Choose((p_web.GSV('tab') = 2 AND p_web.GSV('locTagAllSubAccounts') = 1) OR (p_web.GSV('tab') = 3 AND p_web.GSV('locTagAllGenericAccounts') = 1),'hdiv','adiv'))
  loc:extra = ''
  If Not ((p_web.GSV('tab') = 2 AND p_web.GSV('locTagAllSubAccounts') = 1) OR (p_web.GSV('tab') = 3 AND p_web.GSV('locTagAllGenericAccounts') = 1))
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnTagAll'',''formstatusreportcriteria_btntagall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnTagAll','Tag All','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=GenericTagFileTagAll&RedirectURL=FormStatusReportCriteria&tagtype=' & CHOOSE(p_web.GSV('AccountTabTitle') = 'Sub Accounts',kSubAccount,kGenericAccount) & '&tab=' & p_web.GSV('tab'))) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('btnTagAll') & '_value')


Validate::btnUnTagAll  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnUnTagAll',p_web.GetValue('NewValue'))
    do Value::btnUnTagAll
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::brwSubAccounts  !1

Value::btnUnTagAll  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('btnUnTagAll') & '_value',Choose((p_web.GSV('tab') = 2 AND p_web.GSV('locTagAllSubAccounts') = 1) OR (p_web.GSV('tab') = 3 AND p_web.GSV('locTagAllGenericAccounts') = 1),'hdiv','adiv'))
  loc:extra = ''
  If Not ((p_web.GSV('tab') = 2 AND p_web.GSV('locTagAllSubAccounts') = 1) OR (p_web.GSV('tab') = 3 AND p_web.GSV('locTagAllGenericAccounts') = 1))
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnUnTagAll'',''formstatusreportcriteria_btnuntagall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnUnTagAll','UnTag All','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=GenericTagFileClear&RedirectURL=FormStatusReportCriteria&tagtype=' & CHOOSE(p_web.GSV('AccountTabTitle') = 'Sub Accounts',kSubAccount,kGenericAccount) & '&tab=' & p_web.GSV('tab'))) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('btnUnTagAll') & '_value')


Validate::__hr2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__hr2',p_web.GetValue('NewValue'))
    do Value::__hr2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__hr2  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('__hr2') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::btnBack2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnBack2',p_web.GetValue('NewValue'))
    do Value::btnBack2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnBack2  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('btnBack2') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnBack2','Back','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormStatusReportCriteria?' &'tab=' & (p_web.GSV('tab') - 1))) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/listback.png',,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnNext2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnNext2',p_web.GetValue('NewValue'))
    do Value::btnNext2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnNext2  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('btnNext2') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnNext2','Next','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormStatusReportCriteria?' &'tab=' & (p_web.GSV('tab') + 1))) & ''','''&clip('_self')&''')',loc:javascript,0,'images/listnext.png',,,,)

  do SendPacket
  p_web._DivFooter()


Prompt::locTagAllStatusTypes  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locTagAllStatusTypes') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('All Status Types')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locTagAllStatusTypes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locTagAllStatusTypes',p_web.GetValue('NewValue'))
    locTagAllStatusTypes = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locTagAllStatusTypes
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locTagAllStatusTypes',p_web.GetValue('Value'))
    locTagAllStatusTypes = p_web.GetValue('Value')
  End
    DO Value::brwStatus
    DO Value::btnTagAll4
    DO Value::btnUnTagAll4
    DO Value::txtStatusTypes
    DO Value::locStatusType
  do Value::locTagAllStatusTypes
  do SendAlert

Value::locTagAllStatusTypes  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locTagAllStatusTypes') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locTagAllStatusTypes
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locTagAllStatusTypes'',''formstatusreportcriteria_loctagallstatustypes_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locTagAllStatusTypes') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locTagAllStatusTypes',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('locTagAllStatusTypes') & '_value')


Validate::txtStatusTypes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtStatusTypes',p_web.GetValue('NewValue'))
    do Value::txtStatusTypes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtStatusTypes  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('txtStatusTypes') & '_value',Choose(p_web.GSV('locTagAllStatusTypes') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locTagAllStatusTypes') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web.Translate('If any entries are tagged, then all other entries will be excluded from the report.',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::locStatusType  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locStatusType') & '_prompt',Choose(p_web.GSV('locTagAllStatusTypes') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Status Type')
  If p_web.GSV('locTagAllStatusTypes') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locStatusType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locStatusType',p_web.GetValue('NewValue'))
    locStatusType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locStatusType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locStatusType',p_web.GetValue('Value'))
    locStatusType = p_web.GetValue('Value')
  End
  do Value::locStatusType
  do SendAlert
  do Value::brwStatus  !1
  do Value::btnTagAll4  !1
  do Value::btnUnTagAll4  !1

Value::locStatusType  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locStatusType') & '_value',Choose(p_web.GSV('locTagAllStatusTypes') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locTagAllStatusTypes') = 1)
  ! --- RADIO --- locStatusType
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locStatusType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locStatusType') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locStatusType'',''formstatusreportcriteria_locstatustype_value'',1,'''&clip(0)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locStatusType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locStatusType',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locStatusType_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Job') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locStatusType') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locStatusType'',''formstatusreportcriteria_locstatustype_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locStatusType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locStatusType',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locStatusType_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Exchange') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locStatusType') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locStatusType'',''formstatusreportcriteria_locstatustype_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locStatusType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locStatusType',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locStatusType_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Loan') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('locStatusType') & '_value')


Validate::hidden3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden3',p_web.GetValue('NewValue'))
    do Value::hidden3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden3  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('hidden3') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locShowCustomerStatus  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locShowCustomerStatus') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Show "Customer" Status On Report')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locShowCustomerStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locShowCustomerStatus',p_web.GetValue('NewValue'))
    locShowCustomerStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locShowCustomerStatus
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locShowCustomerStatus',p_web.GetValue('Value'))
    locShowCustomerStatus = p_web.GetValue('Value')
  End
  do Value::locShowCustomerStatus
  do SendAlert

Value::locShowCustomerStatus  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locShowCustomerStatus') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locShowCustomerStatus
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locShowCustomerStatus'',''formstatusreportcriteria_locshowcustomerstatus_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locShowCustomerStatus') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locShowCustomerStatus',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('locShowCustomerStatus') & '_value')


Validate::brwStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwStatus',p_web.GetValue('NewValue'))
    do Value::brwStatus
  Else
    p_web.StoreValue('sts:Ref_Number')
  End

Value::brwStatus  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('locTagAllStatusTypes') = 1,1,0))
  ! --- BROWSE ---  TagStatusTypes --
  p_web.SetValue('TagStatusTypes:NoForm',1)
  p_web.SetValue('TagStatusTypes:FormName',loc:formname)
  p_web.SetValue('TagStatusTypes:parentIs','Form')
  p_web.SetValue('_parentProc','FormStatusReportCriteria')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormStatusReportCriteria_TagStatusTypes_embedded_div')&'"><!-- Net:TagStatusTypes --></div><13,10>'
    p_web._DivHeader('FormStatusReportCriteria_' & lower('TagStatusTypes') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormStatusReportCriteria_' & lower('TagStatusTypes') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagStatusTypes --><13,10>'
  end
  do SendPacket


Validate::btnTagAll4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnTagAll4',p_web.GetValue('NewValue'))
    do Value::btnTagAll4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnTagAll4  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('btnTagAll4') & '_value',Choose(p_web.GSV('locTagAllStatusTypes') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locTagAllStatusTypes') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnTagAll4','Tag All','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=GenericTagFileTagAll&RedirectURL=FormStatusReportCriteria&tagtype=' & kStatusTypes & '&tab=' & p_web.GSV('tab') & '&statustype=' & p_web.GSV('locStatusType'))) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('btnTagAll4') & '_value')


Validate::btnUnTagAll4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnUnTagAll4',p_web.GetValue('NewValue'))
    do Value::btnUnTagAll4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::brwStatus  !1

Value::btnUnTagAll4  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('btnUnTagAll4') & '_value',Choose(p_web.GSV('locTagAllStatusTypes') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locTagAllStatusTypes') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnUnTagAll4'',''formstatusreportcriteria_btnuntagall4_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnUntagall4','UnTag All','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=GenericTagFileClear&RedirectURL=FormStatusReportCriteria&tagtype=' & kStatusTypes & '&tab=' & p_web.GSV('tab'))) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('btnUnTagAll4') & '_value')


Validate::__hr4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__hr4',p_web.GetValue('NewValue'))
    do Value::__hr4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__hr4  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('__hr4') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::btnBack4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnBack4',p_web.GetValue('NewValue'))
    do Value::btnBack4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnBack4  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('btnBack4') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnBack4','Back','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormStatusReportCriteria?' &'tab=3')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/listback.png',,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnNext4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnNext4',p_web.GetValue('NewValue'))
    do Value::btnNext4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnNext4  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('btnNext4') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnNext4','Next','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormStatusReportCriteria?' &'tab=5')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/listnext.png',,,,)

  do SendPacket
  p_web._DivFooter()


Prompt::locTagAllLocations  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locTagAllLocations') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('All Locations')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locTagAllLocations  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locTagAllLocations',p_web.GetValue('NewValue'))
    locTagAllLocations = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locTagAllLocations
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locTagAllLocations',p_web.GetValue('Value'))
    locTagAllLocations = p_web.GetValue('Value')
  End
    DO Value::txtInternalLocation
    DO Value::brwInternalLocation
    DO Value::btnTagAll5
    DO Value::btnUnTagAll5
  do Value::locTagAllLocations
  do SendAlert

Value::locTagAllLocations  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locTagAllLocations') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locTagAllLocations
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locTagAllLocations'',''formstatusreportcriteria_loctagalllocations_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locTagAllLocations') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locTagAllLocations',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('locTagAllLocations') & '_value')


Validate::txtInternalLocation  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtInternalLocation',p_web.GetValue('NewValue'))
    do Value::txtInternalLocation
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtInternalLocation  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('txtInternalLocation') & '_value',Choose(p_web.GSV('locTagAllLocations') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locTagAllLocations') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web.Translate('If any entries are tagged, then all other entries will be excluded from the report.',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Validate::brwInternalLocation  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwInternalLocation',p_web.GetValue('NewValue'))
    do Value::brwInternalLocation
  Else
    p_web.StoreValue('loi:Location')
  End

Value::brwInternalLocation  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('locTagAllLocations') = 1,1,0))
  ! --- BROWSE ---  TagInternalLocations --
  p_web.SetValue('TagInternalLocations:NoForm',1)
  p_web.SetValue('TagInternalLocations:FormName',loc:formname)
  p_web.SetValue('TagInternalLocations:parentIs','Form')
  p_web.SetValue('_parentProc','FormStatusReportCriteria')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormStatusReportCriteria_TagInternalLocations_embedded_div')&'"><!-- Net:TagInternalLocations --></div><13,10>'
    p_web._DivHeader('FormStatusReportCriteria_' & lower('TagInternalLocations') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormStatusReportCriteria_' & lower('TagInternalLocations') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagInternalLocations --><13,10>'
  end
  do SendPacket


Validate::btnTagAll5  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnTagAll5',p_web.GetValue('NewValue'))
    do Value::btnTagAll5
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnTagAll5  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('btnTagAll5') & '_value',Choose(p_web.GSV('locTagAllLocations') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locTagAllLocations') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnTagAll5','Tag All','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=GenericTagFileTagAll&RedirectURL=FormStatusReportCriteria&tagtype=' & kInternalLocation & '&tab=' & p_web.GSV('tab'))) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()


Validate::btnUnTagAll5  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnUnTagAll5',p_web.GetValue('NewValue'))
    do Value::btnUnTagAll5
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::brwInternalLocation  !1

Value::btnUnTagAll5  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('btnUnTagAll5') & '_value',Choose(p_web.GSV('locTagAllLocations') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locTagAllLocations') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnUnTagAll5'',''formstatusreportcriteria_btnuntagall5_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnUntagall5','UnTag All','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=GenericTagFileClear&RedirectURL=FormStatusReportCriteria&tagtype=' & kInternalLocation & '&tab=' & p_web.GSV('tab'))) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('btnUnTagAll5') & '_value')


Validate::__hr5  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__hr5',p_web.GetValue('NewValue'))
    do Value::__hr5
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__hr5  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('__hr5') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::btnBack5  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnBack5',p_web.GetValue('NewValue'))
    do Value::btnBack5
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnBack5  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('btnBack5') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnBack5','Back','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormStatusReportCriteria?' &'tab=4')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/listback.png',,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnNext5  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnNext5',p_web.GetValue('NewValue'))
    do Value::btnNext5
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnNext5  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('btnNext5') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnNext5','Next','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormStatusReportCriteria?' &'tab=6')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/listnext.png',,,,)

  do SendPacket
  p_web._DivFooter()


Prompt::locReportOrder  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locReportOrder') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Report Order')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locReportOrder  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locReportOrder',p_web.GetValue('NewValue'))
    locReportOrder = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locReportOrder
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locReportOrder',p_web.GetValue('Value'))
    locReportOrder = p_web.GetValue('Value')
  End
  do Value::locReportOrder
  do SendAlert

Value::locReportOrder  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locReportOrder') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locReportOrder')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locReportOrder'',''formstatusreportcriteria_locreportorder_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locReportOrder',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locReportOrder') = 0
    p_web.SetSessionValue('locReportOrder','DATE BOOKED')
  end
    packet = clip(packet) & p_web.CreateOption('DATE BOOKED','DATE BOOKED',choose('DATE BOOKED' = p_web.getsessionvalue('locReportOrder')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('DATE COMPLETED','DATE COMPLETED',choose('DATE COMPLETED' = p_web.getsessionvalue('locReportOrder')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('JOB NUMBER','JOB NUMBER',choose('JOB NUMBER' = p_web.getsessionvalue('locReportOrder')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('I.M.E.I. NUMBER','I.M.E.I. NUMBER',choose('I.M.E.I. NUMBER' = p_web.getsessionvalue('locReportOrder')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('ACCOUNT NUMBER','ACCOUNT NUMBER',choose('ACCOUNT NUMBER' = p_web.getsessionvalue('locReportOrder')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('MODEL NUMBER','MODEL NUMBER',choose('MODEL NUMBER' = p_web.getsessionvalue('locReportOrder')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('STATUS','STATUS',choose('STATUS' = p_web.getsessionvalue('locReportOrder')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('M.S.N.','M.S.N.',choose('M.S.N.' = p_web.getsessionvalue('locReportOrder')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('SURNAME','SURNAME',choose('SURNAME' = p_web.getsessionvalue('locReportOrder')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('ENGINEER','ENGINEER',choose('ENGINEER' = p_web.getsessionvalue('locReportOrder')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption('MOBILE NUMBER','MOBILE NUMBER',choose('MOBILE NUMBER' = p_web.getsessionvalue('locReportOrder')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('locReportOrder') & '_value')


Validate::hidden6  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden6',p_web.GetValue('NewValue'))
    do Value::hidden6
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden6  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('hidden6') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locDateRangeType  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locDateRangeType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Date Range Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locDateRangeType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locDateRangeType',p_web.GetValue('NewValue'))
    locDateRangeType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locDateRangeType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locDateRangeType',p_web.GetValue('Value'))
    locDateRangeType = p_web.GetValue('Value')
  End
  do Value::locDateRangeType
  do SendAlert

Value::locDateRangeType  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locDateRangeType') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locDateRangeType
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locDateRangeType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locDateRangeType') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locDateRangeType'',''formstatusreportcriteria_locdaterangetype_value'',1,'''&clip(0)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locDateRangeType',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locDateRangeType_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Booking Date') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locDateRangeType') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locDateRangeType'',''formstatusreportcriteria_locdaterangetype_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locDateRangeType',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locDateRangeType_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Completed Date') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('locDateRangeType') & '_value')


Prompt::locStartDate  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locStartDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Start Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locStartDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locStartDate',p_web.GetValue('NewValue'))
    locStartDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locStartDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locStartDate',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    locStartDate = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  do Value::locStartDate
  do SendAlert

Value::locStartDate  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locStartDate') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locStartDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locStartDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locStartDate'',''formstatusreportcriteria_locstartdate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locStartDate',p_web.GetSessionValue('locStartDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
   packet = CLIP(packet) & |
     '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar' & |
     '(locStartDate,''dd/mm/yyyy'',this); Date.disabled=true;' & |
     'sv(''...'',''FormStatusReportCriteria_pickdate_value'',1,FieldValue(this,1));nextFocus(FormStatusReportCriteria_frm,'''',0);" ' & |
     'value="Select Date" name="Date" type="button">...</button>'
   DO SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('locStartDate') & '_value')


Prompt::locEndDate  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locEndDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('End Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locEndDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEndDate',p_web.GetValue('NewValue'))
    locEndDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEndDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEndDate',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    locEndDate = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  do Value::locEndDate
  do SendAlert

Value::locEndDate  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('locEndDate') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locEndDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locEndDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEndDate'',''formstatusreportcriteria_locenddate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locEndDate',p_web.GetSessionValue('locEndDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
   packet = CLIP(packet) & |
     '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar' & |
     '(locEndDate,''dd/mm/yyyy'',this); Date.disabled=true;' & |
     'sv(''...'',''FormStatusReportCriteria_pickdate_value'',1,FieldValue(this,1));nextFocus(FormStatusReportCriteria_frm,'''',0);" ' & |
     'value="Select Date" name="Date" type="button">...</button>'
   DO SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('locEndDate') & '_value')


Validate::btnSaveCriteria  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnSaveCriteria',p_web.GetValue('NewValue'))
    do Value::btnSaveCriteria
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnSaveCriteria  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('btnSaveCriteria') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('openNewCriteria()')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnSaveCriteria','Save Criteria','button-entryfield',loc:formname,,,,loc:javascript,0,'/images/disk.png',,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnPrintReport  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnPrintReport',p_web.GetValue('NewValue'))
    do Value::btnPrintReport
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::btnPrintReport
  do SendAlert

Value::btnPrintReport  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('btnPrintReport') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('runStatusReport(''P'')')&';'
  loc:javascript = clip(loc:javascript) & ' ' &p_web._nocolon('sv(''btnPrintReport'',''formstatusreportcriteria_btnprintreport_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btPrintReport','Print Report','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('btnPrintReport') & '_value')


Validate::btnExport  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnExport',p_web.GetValue('NewValue'))
    do Value::btnExport
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::btnExport
  do SendAlert

Value::btnExport  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('btnExport') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('runStatusReport(''E'')')&';'
  loc:javascript = clip(loc:javascript) & ' ' &p_web._nocolon('sv(''btnExport'',''formstatusreportcriteria_btnexport_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnExport','Create Export','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStatusReportCriteria_' & p_web._nocolon('btnExport') & '_value')


Validate::__hr6  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__hr6',p_web.GetValue('NewValue'))
    do Value::__hr6
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__hr6  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('__hr6') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::btnBack6  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnBack6',p_web.GetValue('NewValue'))
    do Value::btnBack6
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnBack6  Routine
  p_web._DivHeader('FormStatusReportCriteria_' & p_web._nocolon('btnBack6') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnBack6','Back','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormStatusReportCriteria?' &'tab=5')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/listback.png',,,,)

  do SendPacket
  p_web._DivFooter()


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormStatusReportCriteria_locSavedCriteria_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSavedCriteria
      else
        do Value::locSavedCriteria
      end
  of lower('FormStatusReportCriteria_locPaperReportType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPaperReportType
      else
        do Value::locPaperReportType
      end
  of lower('FormStatusReportCriteria_locExportJobs_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locExportJobs
      else
        do Value::locExportJobs
      end
  of lower('FormStatusReportCriteria_locExportAudit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locExportAudit
      else
        do Value::locExportAudit
      end
  of lower('FormStatusReportCriteria_locExportCParts_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locExportCParts
      else
        do Value::locExportCParts
      end
  of lower('FormStatusReportCriteria_locExportWParts_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locExportWParts
      else
        do Value::locExportWParts
      end
  of lower('FormStatusReportCriteria_locExportType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locExportType
      else
        do Value::locExportType
      end
  of lower('FormStatusReportCriteria_btnPrev_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnPrev
      else
        do Value::btnPrev
      end
  of lower('FormStatusReportCriteria_locTagAllSubAccounts_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locTagAllSubAccounts
      else
        do Value::locTagAllSubAccounts
      end
  of lower('FormStatusReportCriteria_locTagAllGenericAccounts_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locTagAllGenericAccounts
      else
        do Value::locTagAllGenericAccounts
      end
  of lower('FormStatusReportCriteria_btnTagAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnTagAll
      else
        do Value::btnTagAll
      end
  of lower('FormStatusReportCriteria_btnUnTagAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnUnTagAll
      else
        do Value::btnUnTagAll
      end
  of lower('FormStatusReportCriteria_locTagAllStatusTypes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locTagAllStatusTypes
      else
        do Value::locTagAllStatusTypes
      end
  of lower('FormStatusReportCriteria_locStatusType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locStatusType
      else
        do Value::locStatusType
      end
  of lower('FormStatusReportCriteria_locShowCustomerStatus_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locShowCustomerStatus
      else
        do Value::locShowCustomerStatus
      end
  of lower('FormStatusReportCriteria_btnUnTagAll4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnUnTagAll4
      else
        do Value::btnUnTagAll4
      end
  of lower('FormStatusReportCriteria_locTagAllLocations_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locTagAllLocations
      else
        do Value::locTagAllLocations
      end
  of lower('FormStatusReportCriteria_btnUnTagAll5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnUnTagAll5
      else
        do Value::btnUnTagAll5
      end
  of lower('FormStatusReportCriteria_locReportOrder_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locReportOrder
      else
        do Value::locReportOrder
      end
  of lower('FormStatusReportCriteria_locDateRangeType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locDateRangeType
      else
        do Value::locDateRangeType
      end
  of lower('FormStatusReportCriteria_locStartDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locStartDate
      else
        do Value::locStartDate
      end
  of lower('FormStatusReportCriteria_locEndDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEndDate
      else
        do Value::locEndDate
      end
  of lower('FormStatusReportCriteria_btnPrintReport_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnPrintReport
      else
        do Value::btnPrintReport
      end
  of lower('FormStatusReportCriteria_btnExport_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnExport
      else
        do Value::btnExport
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormStatusReportCriteria_form:ready_',1)
  p_web.SetSessionValue('FormStatusReportCriteria_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormStatusReportCriteria',0)

PreCopy  Routine
  p_web.SetValue('FormStatusReportCriteria_form:ready_',1)
  p_web.SetSessionValue('FormStatusReportCriteria_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormStatusReportCriteria',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormStatusReportCriteria_form:ready_',1)
  p_web.SetSessionValue('FormStatusReportCriteria_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormStatusReportCriteria:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormStatusReportCriteria_form:ready_',1)
  p_web.SetSessionValue('FormStatusReportCriteria_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormStatusReportCriteria:Primed',0)
  p_web.setsessionvalue('showtab_FormStatusReportCriteria',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('tab') = 1
          If p_web.IfExistsValue('locExportJobs') = 0
            p_web.SetValue('locExportJobs',0)
            locExportJobs = 0
          End
          If p_web.IfExistsValue('locExportAudit') = 0
            p_web.SetValue('locExportAudit',0)
            locExportAudit = 0
          End
          If p_web.IfExistsValue('locExportCParts') = 0
            p_web.SetValue('locExportCParts',0)
            locExportCParts = 0
          End
          If p_web.IfExistsValue('locExportWParts') = 0
            p_web.SetValue('locExportWParts',0)
            locExportWParts = 0
          End
  End
  If p_web.GSV('tab') = 2 OR p_web.GSV('tab') = 3
    If (p_web.GSV('tab') = 2)
          If p_web.IfExistsValue('locTagAllSubAccounts') = 0
            p_web.SetValue('locTagAllSubAccounts',0)
            locTagAllSubAccounts = 0
          End
    End
    If (p_web.GSV('tab') = 3)
          If p_web.IfExistsValue('locTagAllGenericAccounts') = 0
            p_web.SetValue('locTagAllGenericAccounts',0)
            locTagAllGenericAccounts = 0
          End
    End
  End
  If p_web.GSV('tab') = 4
          If p_web.IfExistsValue('locTagAllStatusTypes') = 0
            p_web.SetValue('locTagAllStatusTypes',0)
            locTagAllStatusTypes = 0
          End
    If (FALSE)
          If p_web.IfExistsValue('locShowCustomerStatus') = 0
            p_web.SetValue('locShowCustomerStatus',0)
            locShowCustomerStatus = 0
          End
    End
  End
  If p_web.GSV('tab') = 5
          If p_web.IfExistsValue('locTagAllLocations') = 0
            p_web.SetValue('locTagAllLocations',0)
            locTagAllLocations = 0
          End
  End
  If p_web.GSV('tab') = 6
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormStatusReportCriteria_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormStatusReportCriteria_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
  If p_web.GSV('tab') = 1
    loc:InvalidTab += 1
  End
  ! tab = 2
  If p_web.GSV('tab') = 2 OR p_web.GSV('tab') = 3
    loc:InvalidTab += 1
  End
  ! tab = 3
  If p_web.GSV('tab') = 4
    loc:InvalidTab += 1
  End
  ! tab = 4
  If p_web.GSV('tab') = 5
    loc:InvalidTab += 1
  End
  ! tab = 5
  If p_web.GSV('tab') = 6
    loc:InvalidTab += 1
  End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormStatusReportCriteria:Primed',0)
  p_web.StoreValue('locSavedCriteria')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locPaperReportType')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locExportJobs')
  p_web.StoreValue('locExportAudit')
  p_web.StoreValue('locExportCParts')
  p_web.StoreValue('locExportWParts')
  p_web.StoreValue('locExportType')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locTagAllSubAccounts')
  p_web.StoreValue('locTagAllGenericAccounts')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locTagAllStatusTypes')
  p_web.StoreValue('')
  p_web.StoreValue('locStatusType')
  p_web.StoreValue('')
  p_web.StoreValue('locShowCustomerStatus')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locTagAllLocations')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locReportOrder')
  p_web.StoreValue('')
  p_web.StoreValue('locDateRangeType')
  p_web.StoreValue('locStartDate')
  p_web.StoreValue('locEndDate')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
newcriteriaform  Routine
  packet = clip(packet) & |
    '<<style><13,10>'&|
    '#newcriteria {{<13,10>'&|
    '    background-color: rgb(255, 255, 255); <13,10>'&|
    '    width: 100%; <13,10>'&|
    '    height: 100%; <13,10>'&|
    '    display: block;<13,10>'&|
    '    position: fixed;<13,10>'&|
    '    z-index: 1000;<13,10>'&|
    '    top: 0;<13,10>'&|
    '    left: 0;<13,10>'&|
    '    opacity: 0.7;<13,10>'&|
    '}<13,10>'&|
    '#newcriteriaform {{<13,10>'&|
    '    text-align:center;<13,10>'&|
    '    border:1px solid #CFCFCF;<13,10>'&|
    '    border-radius: 3px;<13,10>'&|
    '    box-shadow: 2px 2px 10px #888888;<13,10>'&|
    '    position:fixed;<13,10>'&|
    '    top:20%;<13,10>'&|
    '    left:40%;<13,10>'&|
    '    padding-left:10px;<13,10>'&|
    '    padding-right:10px;<13,10>'&|
    '    width:260px;<13,10>'&|
    '    height:160px;<13,10>'&|
    '    margin:auto;<13,10>'&|
    '    background-color: #EFEFEF;<13,10>'&|
    '    z-index: 1001;<13,10>'&|
    '}<13,10>'&|
    '<</style><13,10>'&|
    '<13,10>'&|
    '<<div id="newcriteria" style="display:none;"><13,10>'&|
    '<</div><13,10>'&|
    '<<div id="newcriteriaform" style="display:none;"><13,10>'&|
    '    <<p class="SubHeading">Save Report Criteria<</p><<br/><13,10>'&|
    '    <<span class="FormPrompt" style="display:block;float:none;">Enter Criteria Name:<</span><13,10>'&|
    '    <<input class="FormEntry formrqd Upper" size="30" type="text" id="newcriterianame"><</input><<br/><<br/><13,10>'&|
    '    <<button class="MainButton" id="savebutton" onclick="saveNewCriteria();">Save<</button><13,10>'&|
    '    <<button class="MainButton" id="cancelbutton" onclick="closeNewCriteria();">Cancel<</button><13,10>'&|
    '<</div><13,10>'&|
    '<13,10>'&|
    '<13,10>'&|
    '<<script><13,10>'&|
    'function openNewCriteria() {{<13,10>'&|
    '    d = document.getElementById("newcriteria");<13,10>'&|
    '    d2 = document.getElementById("newcriteriaform");<13,10>'&|
    '    d.style.display = "block";<13,10>'&|
    '    d2.style.display = "block";<13,10>'&|
    '}<13,10>'&|
    'function closeNewCriteria() {{<13,10>'&|
    '    d = document.getElementById("newcriteria");<13,10>'&|
    '    d2 = document.getElementById("newcriteriaform");<13,10>'&|
    '    d.style.display = "none";<13,10>'&|
    '    d2.style.display = "none";<13,10>'&|
    '    }<13,10>'&|
    'function saveNewCriteria() {{<13,10>'&|
    '    n = document.getElementById("newcriterianame");<13,10>'&|
    '    if (n.value === "") {{<13,10>'&|
    '        alert("You must enter a Criteria Name.");<13,10>'&|
    '    } else {{<13,10>'&|
    '        window.open("PageProcess?ProcessType=StatusReportCriteriaSave&RedirectURL=FormStatusReportCriteria&Criteria=" + n.value,"_self");<13,10>'&|
    '    }<13,10>'&|
    '}    <13,10>'&|
    '<</script><13,10>'&|
    ''
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locEndDate',locEndDate) ! DATE
     p_web.SSV('locStartDate',locStartDate) ! DATE
     p_web.SSV('locSavedCriteria',locSavedCriteria) ! STRING(30)
     p_web.SSV('locPaperReportType',locPaperReportType) ! STRING(1)
     p_web.SSV('locExportJobs',locExportJobs) ! STRING(1)
     p_web.SSV('locExportAudit',locExportAudit) ! STRING(1)
     p_web.SSV('locExportCParts',locExportCParts) ! STRING(1)
     p_web.SSV('locExportWParts',locExportWParts) ! STRING(1)
     p_web.SSV('locStatusType',locStatusType) ! STRING(1)
     p_web.SSV('locReportOrder',locReportOrder) ! STRING(30)
     p_web.SSV('locDateRangeType',locDateRangeType) ! STRING(1)
     p_web.SSV('locShowCustomerStatus',locShowCustomerStatus) ! LONG
     p_web.SSV('locExportType',locExportType) ! STRING(1)
     p_web.SSV('locTagAllSubAccounts',locTagAllSubAccounts) ! LONG
     p_web.SSV('locTagAllGenericAccounts',locTagAllGenericAccounts) ! LONG
     p_web.SSV('locTagAllStatusTypes',locTagAllStatusTypes) ! LONG
     p_web.SSV('locTagAllLocations',locTagAllLocations) ! LONG
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locEndDate = p_web.GSV('locEndDate') ! DATE
     locStartDate = p_web.GSV('locStartDate') ! DATE
     locSavedCriteria = p_web.GSV('locSavedCriteria') ! STRING(30)
     locPaperReportType = p_web.GSV('locPaperReportType') ! STRING(1)
     locExportJobs = p_web.GSV('locExportJobs') ! STRING(1)
     locExportAudit = p_web.GSV('locExportAudit') ! STRING(1)
     locExportCParts = p_web.GSV('locExportCParts') ! STRING(1)
     locExportWParts = p_web.GSV('locExportWParts') ! STRING(1)
     locStatusType = p_web.GSV('locStatusType') ! STRING(1)
     locReportOrder = p_web.GSV('locReportOrder') ! STRING(30)
     locDateRangeType = p_web.GSV('locDateRangeType') ! STRING(1)
     locShowCustomerStatus = p_web.GSV('locShowCustomerStatus') ! LONG
     locExportType = p_web.GSV('locExportType') ! STRING(1)
     locTagAllSubAccounts = p_web.GSV('locTagAllSubAccounts') ! LONG
     locTagAllGenericAccounts = p_web.GSV('locTagAllGenericAccounts') ! LONG
     locTagAllStatusTypes = p_web.GSV('locTagAllStatusTypes') ! LONG
     locTagAllLocations = p_web.GSV('locTagAllLocations') ! LONG
