

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER017.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
RapidLocation        PROCEDURE  (STRING fLocation)         ! Declare Procedure
LOCATION::State  USHORT
FilesOpened     BYTE(0)

  CODE
    RETURN vod.IsRapidLocation(fLocation)
!--------------------------------------
OpenFiles  ROUTINE
  Access:LOCATION.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOCATION.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:LOCATION.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  LOCATION::State = Access:LOCATION.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF LOCATION::State <> 0
    Access:LOCATION.RestoreFile(LOCATION::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
