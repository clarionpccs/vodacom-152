

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER353.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER010.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER347.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER350.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER351.INC'),ONCE        !Req'd for module callout resolution
                     END


FormWarrantyIncomeReport PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locAllAccounts       LONG                                  !
locGenericAccounts   LONG                                  !
locAllChargeTypes    LONG                                  !
locStartDate         DATE                                  !
locEndDate           DATE                                  !
locZeroSupression    STRING(1)                             !
locReportOrder       LONG                                  !
locAllManufacturers  LONG                                  !
locAllWarrantyStatus LONG                                  !
locSubmitted         LONG                                  !
locResubmitted       LONG                                  !
locRejected          LONG                                  !
locRejectionAcknowledged LONG                              !
locReconciled        LONG                                  !
locDateRangeType     LONG                                  !
locExcludedOnly      LONG                                  !
locIncludeARCRepairedJobs LONG                             !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormWarrantyIncomeReport')
  loc:formname = 'FormWarrantyIncomeReport_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormWarrantyIncomeReport',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormWarrantyIncomeReport','')
    p_web._DivHeader('FormWarrantyIncomeReport',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormWarrantyIncomeReport',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormWarrantyIncomeReport',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormWarrantyIncomeReport',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormWarrantyIncomeReport',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormWarrantyIncomeReport',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormWarrantyIncomeReport',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormWarrantyIncomeReport',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormWarrantyIncomeReport_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('locStartDate')
    p_web.SetPicture('locStartDate','@d06')
  End
  p_web.SetSessionPicture('locStartDate','@d06')
  If p_web.IfExistsValue('locEndDate')
    p_web.SetPicture('locEndDate','@d06')
  End
  p_web.SetSessionPicture('locEndDate','@d06')
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GetValue('tab') = 1
    loc:TabNumber += 1
  End
  If p_web.GetValue('tab') = 2
    loc:TabNumber += 1
  End
  If p_web.GetValue('tab') = 3
    loc:TabNumber += 1
  End
  If p_web.GetValue('tab') = 4
    loc:TabNumber += 1
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locAllAccounts',locAllAccounts)
  p_web.SetSessionValue('locGenericAccounts',locGenericAccounts)
  p_web.SetSessionValue('locAllChargeTypes',locAllChargeTypes)
  p_web.SetSessionValue('locAllManufacturers',locAllManufacturers)
  p_web.SetSessionValue('locAllWarrantyStatus',locAllWarrantyStatus)
  p_web.SetSessionValue('locSubmitted',locSubmitted)
  p_web.SetSessionValue('locResubmitted',locResubmitted)
  p_web.SetSessionValue('locRejected',locRejected)
  p_web.SetSessionValue('locRejectionAcknowledged',locRejectionAcknowledged)
  p_web.SetSessionValue('locReconciled',locReconciled)
  p_web.SetSessionValue('locStartDate',locStartDate)
  p_web.SetSessionValue('locEndDate',locEndDate)
  p_web.SetSessionValue('locDateRangeType',locDateRangeType)
  p_web.SetSessionValue('locExcludedOnly',locExcludedOnly)
  p_web.SetSessionValue('locIncludeARCRepairedJobs',locIncludeARCRepairedJobs)
  p_web.SetSessionValue('locZeroSupression',locZeroSupression)
  p_web.SetSessionValue('locReportOrder',locReportOrder)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locAllAccounts')
    locAllAccounts = p_web.GetValue('locAllAccounts')
    p_web.SetSessionValue('locAllAccounts',locAllAccounts)
  End
  if p_web.IfExistsValue('locGenericAccounts')
    locGenericAccounts = p_web.GetValue('locGenericAccounts')
    p_web.SetSessionValue('locGenericAccounts',locGenericAccounts)
  End
  if p_web.IfExistsValue('locAllChargeTypes')
    locAllChargeTypes = p_web.GetValue('locAllChargeTypes')
    p_web.SetSessionValue('locAllChargeTypes',locAllChargeTypes)
  End
  if p_web.IfExistsValue('locAllManufacturers')
    locAllManufacturers = p_web.GetValue('locAllManufacturers')
    p_web.SetSessionValue('locAllManufacturers',locAllManufacturers)
  End
  if p_web.IfExistsValue('locAllWarrantyStatus')
    locAllWarrantyStatus = p_web.GetValue('locAllWarrantyStatus')
    p_web.SetSessionValue('locAllWarrantyStatus',locAllWarrantyStatus)
  End
  if p_web.IfExistsValue('locSubmitted')
    locSubmitted = p_web.GetValue('locSubmitted')
    p_web.SetSessionValue('locSubmitted',locSubmitted)
  End
  if p_web.IfExistsValue('locResubmitted')
    locResubmitted = p_web.GetValue('locResubmitted')
    p_web.SetSessionValue('locResubmitted',locResubmitted)
  End
  if p_web.IfExistsValue('locRejected')
    locRejected = p_web.GetValue('locRejected')
    p_web.SetSessionValue('locRejected',locRejected)
  End
  if p_web.IfExistsValue('locRejectionAcknowledged')
    locRejectionAcknowledged = p_web.GetValue('locRejectionAcknowledged')
    p_web.SetSessionValue('locRejectionAcknowledged',locRejectionAcknowledged)
  End
  if p_web.IfExistsValue('locReconciled')
    locReconciled = p_web.GetValue('locReconciled')
    p_web.SetSessionValue('locReconciled',locReconciled)
  End
  if p_web.IfExistsValue('locStartDate')
    locStartDate = p_web.dformat(clip(p_web.GetValue('locStartDate')),'@d06')
    p_web.SetSessionValue('locStartDate',locStartDate)
  End
  if p_web.IfExistsValue('locEndDate')
    locEndDate = p_web.dformat(clip(p_web.GetValue('locEndDate')),'@d06')
    p_web.SetSessionValue('locEndDate',locEndDate)
  End
  if p_web.IfExistsValue('locDateRangeType')
    locDateRangeType = p_web.GetValue('locDateRangeType')
    p_web.SetSessionValue('locDateRangeType',locDateRangeType)
  End
  if p_web.IfExistsValue('locExcludedOnly')
    locExcludedOnly = p_web.GetValue('locExcludedOnly')
    p_web.SetSessionValue('locExcludedOnly',locExcludedOnly)
  End
  if p_web.IfExistsValue('locIncludeARCRepairedJobs')
    locIncludeARCRepairedJobs = p_web.GetValue('locIncludeARCRepairedJobs')
    p_web.SetSessionValue('locIncludeARCRepairedJobs',locIncludeARCRepairedJobs)
  End
  if p_web.IfExistsValue('locZeroSupression')
    locZeroSupression = p_web.GetValue('locZeroSupression')
    p_web.SetSessionValue('locZeroSupression',locZeroSupression)
  End
  if p_web.IfExistsValue('locReportOrder')
    locReportOrder = p_web.GetValue('locReportOrder')
    p_web.SetSessionValue('locReportOrder',locReportOrder)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormWarrantyIncomeReport_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    IF (p_web.GetValue('FirstTime') = 1)
        ClearTagFile(p_web)   
        ClearGenericTagFile(p_web,'C')
        ClearGenericTagFile(p_web,kSubAccount)
        ClearGenericTagFile(p_web,kGenericAccount)
        p_web.SSV('locAllAccounts',1)
        p_web.SSV('locGenericAccounts','')  
        p_web.SSV('TagSubAccountsType','') ! Clear Tag SubAccounts Settings
        p_web.SSV('locAllChargeTypes',1)
        p_web.SSV('locStartDate',TODAY())
        p_web.SSV('locEndDate',TODAY())
        p_web.SSV('locZeroSupression','Y')
        p_web.SSV('locAllWarrantyStatus',1)
        p_web.SSV('locAllManufacturers',1)
        p_web.SSV('locReportOrder',0)
    END ! IF
    IF (p_web.GetValue('tab') <> 4)
        p_web.site.SaveButton.Class = 'hidden'
    END ! IF
    
      p_web.site.SaveButton.TextValue = 'Export'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locAllAccounts = p_web.RestoreValue('locAllAccounts')
 locGenericAccounts = p_web.RestoreValue('locGenericAccounts')
 locAllChargeTypes = p_web.RestoreValue('locAllChargeTypes')
 locAllManufacturers = p_web.RestoreValue('locAllManufacturers')
 locAllWarrantyStatus = p_web.RestoreValue('locAllWarrantyStatus')
 locSubmitted = p_web.RestoreValue('locSubmitted')
 locResubmitted = p_web.RestoreValue('locResubmitted')
 locRejected = p_web.RestoreValue('locRejected')
 locRejectionAcknowledged = p_web.RestoreValue('locRejectionAcknowledged')
 locReconciled = p_web.RestoreValue('locReconciled')
 locStartDate = p_web.RestoreValue('locStartDate')
 locEndDate = p_web.RestoreValue('locEndDate')
 locDateRangeType = p_web.RestoreValue('locDateRangeType')
 locExcludedOnly = p_web.RestoreValue('locExcludedOnly')
 locIncludeARCRepairedJobs = p_web.RestoreValue('locIncludeARCRepairedJobs')
 locZeroSupression = p_web.RestoreValue('locZeroSupression')
 locReportOrder = p_web.RestoreValue('locReportOrder')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'RunReport?RT=WarrantyIncomeReport'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormWarrantyIncomeReport_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormWarrantyIncomeReport_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormWarrantyIncomeReport_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'FormReports'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormWarrantyIncomeReport" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormWarrantyIncomeReport" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormWarrantyIncomeReport" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Warranty Income Report') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Warranty Income Report',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormWarrantyIncomeReport">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormWarrantyIncomeReport" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormWarrantyIncomeReport')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GetValue('tab') = 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Trade Account') & ''''
        End
        If p_web.GetValue('tab') = 2
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Charge Types') & ''''
        End
        If p_web.GetValue('tab') = 3
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Manufacturers') & ''''
        End
        If p_web.GetValue('tab') = 4
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Final Settings') & ''''
        End
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormWarrantyIncomeReport')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormWarrantyIncomeReport'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormWarrantyIncomeReport_TagSubAccounts_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormWarrantyIncomeReport_TagWarChargeTypes_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormWarrantyIncomeReport_BrowseTagManufacturers_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormWarrantyIncomeReport_TagSubAccounts_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormWarrantyIncomeReport_TagWarChargeTypes_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormWarrantyIncomeReport_BrowseTagManufacturers_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormWarrantyIncomeReport_TagSubAccounts_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormWarrantyIncomeReport_TagWarChargeTypes_embedded_div')&''');'
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormWarrantyIncomeReport_BrowseTagManufacturers_embedded_div')&''');'
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GetValue('tab') = 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.locAllAccounts')
    ElsIf p_web.GetValue('tab') = 2
        p_web.SetValue('SelectField',clip(loc:formname) & '.locAllChargeTypes')
    ElsIf p_web.GetValue('tab') = 3
        p_web.SetValue('SelectField',clip(loc:formname) & '.locAllManufacturers')
    ElsIf p_web.GetValue('tab') = 4
        p_web.SetValue('SelectField',clip(loc:formname) & '.locAllWarrantyStatus')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GetValue('tab') = 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          End
          If p_web.GetValue('tab') = 2
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          End
          If p_web.GetValue('tab') = 3
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          End
          If p_web.GetValue('tab') = 4
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          End
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormWarrantyIncomeReport')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GetValue('tab') = 1
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    end
    if p_web.GetValue('tab') = 2
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    end
    if p_web.GetValue('tab') = 3
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
    end
    if p_web.GetValue('tab') = 4
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
    end
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GetValue('tab') = 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Trade Account') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormWarrantyIncomeReport_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Trade Account')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Trade Account')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Trade Account')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Trade Account')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAllAccounts
      do Value::locAllAccounts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locGenericAccounts
      do Value::locGenericAccounts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwAccounts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnUnTagAll
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__line1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnPrevDis
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnNext
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
  If p_web.GetValue('tab') = 2
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Charge Types') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormWarrantyIncomeReport_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Charge Types')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Charge Types')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Charge Types')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Charge Types')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAllChargeTypes
      do Value::locAllChargeTypes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwChargeTypes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnUnTagAll2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__line2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnBack2
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnNext2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab2  Routine
  If p_web.GetValue('tab') = 3
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Manufacturers') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormWarrantyIncomeReport_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Manufacturers')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Manufacturers')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Manufacturers')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Manufacturers')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAllManufacturers
      do Value::locAllManufacturers
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwManufacturers
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnUntagAll3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__line3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnBack3
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnNext3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab3  Routine
  If p_web.GetValue('tab') = 4
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Final Settings') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormWarrantyIncomeReport_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Final Settings')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Final Settings')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Final Settings')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Final Settings')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAllWarrantyStatus
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAllWarrantyStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSubmitted
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSubmitted
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locResubmitted
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locResubmitted
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locRejected
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locRejected
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locRejectionAcknowledged
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locRejectionAcknowledged
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locReconciled
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locReconciled
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::__br1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locStartDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locStartDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEndDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEndDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locDateRangeType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="4">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locDateRangeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExcludedOnly
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExcludedOnly
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIncludeARCRepairedJobs
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIncludeARCRepairedJobs
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::__br2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locZeroSupression
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="4">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locZeroSupression
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locReportOrder
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="4">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locReportOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If false
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnPrintReport
    end
    do SendPacket
    If false
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnExport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__line4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnBack4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end


Prompt::locAllAccounts  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locAllAccounts') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('All Trade Accounts')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAllAccounts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAllAccounts',p_web.GetValue('NewValue'))
    locAllAccounts = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAllAccounts
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locAllAccounts',p_web.GetValue('Value'))
    locAllAccounts = p_web.GetValue('Value')
  End
  do Value::locAllAccounts
  do SendAlert
  do Value::brwAccounts  !1
  do Prompt::locGenericAccounts
  do Value::locGenericAccounts  !1
  do Value::btnUnTagAll  !1

Value::locAllAccounts  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locAllAccounts') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locAllAccounts
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locAllAccounts'',''formwarrantyincomereport_locallaccounts_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locAllAccounts')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locAllAccounts') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locAllAccounts',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locAllAccounts') & '_value')


Prompt::locGenericAccounts  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locGenericAccounts') & '_prompt',Choose(p_web.GSV('locAllAccounts') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Generic Accounts')
  If p_web.GSV('locAllAccounts') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locGenericAccounts') & '_prompt')

Validate::locGenericAccounts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locGenericAccounts',p_web.GetValue('NewValue'))
    locGenericAccounts = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locGenericAccounts
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locGenericAccounts',p_web.GetValue('Value'))
    locGenericAccounts = p_web.GetValue('Value')
  End
  do Value::locGenericAccounts
  do SendAlert
  do Value::brwAccounts  !1

Value::locGenericAccounts  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locGenericAccounts') & '_value',Choose(p_web.GSV('locAllAccounts') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllAccounts') = 1)
  ! --- CHECKBOX --- locGenericAccounts
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locGenericAccounts'',''formwarrantyincomereport_locgenericaccounts_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locGenericAccounts')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locGenericAccounts') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locGenericAccounts',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locGenericAccounts') & '_value')


Validate::brwAccounts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwAccounts',p_web.GetValue('NewValue'))
    do Value::brwAccounts
  Else
    p_web.StoreValue('sub:RecordNumber')
  End

Value::brwAccounts  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('locAllAccounts') = 1,1,0))
  ! --- BROWSE ---  TagSubAccounts --
  p_web.SetValue('TagSubAccounts:NoForm',1)
  p_web.SetValue('TagSubAccounts:FormName',loc:formname)
  p_web.SetValue('TagSubAccounts:parentIs','Form')
  p_web.SetValue('_parentProc','FormWarrantyIncomeReport')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormWarrantyIncomeReport_TagSubAccounts_embedded_div')&'"><!-- Net:TagSubAccounts --></div><13,10>'
    p_web._DivHeader('FormWarrantyIncomeReport_' & lower('TagSubAccounts') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormWarrantyIncomeReport_' & lower('TagSubAccounts') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagSubAccounts --><13,10>'
  end
  do SendPacket


Validate::btnUnTagAll  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnUnTagAll',p_web.GetValue('NewValue'))
    do Value::btnUnTagAll
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    IF (p_web.GSV('locGenericAccounts') = 0)
        ClearGenericTagFile(p_web,kSubAccount)
    ELSE
        ClearGenericTagFile(p_web,kGenericAccount)
    END ! IF    
  do SendAlert
  do Value::brwAccounts  !1

Value::btnUnTagAll  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('btnUnTagAll') & '_value',Choose(p_web.GSV('locAllAccounts') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllAccounts') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnUnTagAll'',''formwarrantyincomereport_btnuntagall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnUntagAll','UnTag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('btnUnTagAll') & '_value')


Validate::__line1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__line1',p_web.GetValue('NewValue'))
    do Value::__line1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__line1  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('__line1') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::btnPrevDis  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnPrevDis',p_web.GetValue('NewValue'))
    do Value::btnPrevDis
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::btnPrevDis  !1

Value::btnPrevDis  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('btnPrevDis') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnPrevDis'',''formwarrantyincomereport_btnprevdis_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnPrevDis','','MainButton',loc:formname,,,,loc:javascript,0,'images/listtop.png',,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('btnPrevDis') & '_value')


Validate::btnNext  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnNext',p_web.GetValue('NewValue'))
    do Value::btnNext
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnNext  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('btnNext') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnNext','Next','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormWarrantyIncomeReport?' &'tab=2')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/listnext.png',,,,)

  do SendPacket
  p_web._DivFooter()


Prompt::locAllChargeTypes  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locAllChargeTypes') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('All Charge Types')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAllChargeTypes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAllChargeTypes',p_web.GetValue('NewValue'))
    locAllChargeTypes = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAllChargeTypes
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locAllChargeTypes',p_web.GetValue('Value'))
    locAllChargeTypes = p_web.GetValue('Value')
  End
  do Value::locAllChargeTypes
  do SendAlert
  do Value::brwChargeTypes  !1
  do Value::btnUnTagAll2  !1

Value::locAllChargeTypes  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locAllChargeTypes') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locAllChargeTypes
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locAllChargeTypes'',''formwarrantyincomereport_locallchargetypes_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locAllChargeTypes')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locAllChargeTypes') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locAllChargeTypes',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locAllChargeTypes') & '_value')


Validate::brwChargeTypes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwChargeTypes',p_web.GetValue('NewValue'))
    do Value::brwChargeTypes
  Else
    p_web.StoreValue('cha:Charge_Type')
  End

Value::brwChargeTypes  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('locAllChargeTypes') = 1,1,0))
  ! --- BROWSE ---  TagWarChargeTypes --
  p_web.SetValue('TagWarChargeTypes:NoForm',1)
  p_web.SetValue('TagWarChargeTypes:FormName',loc:formname)
  p_web.SetValue('TagWarChargeTypes:parentIs','Form')
  p_web.SetValue('_parentProc','FormWarrantyIncomeReport')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormWarrantyIncomeReport_TagWarChargeTypes_embedded_div')&'"><!-- Net:TagWarChargeTypes --></div><13,10>'
    p_web._DivHeader('FormWarrantyIncomeReport_' & lower('TagWarChargeTypes') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormWarrantyIncomeReport_' & lower('TagWarChargeTypes') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagWarChargeTypes --><13,10>'
  end
  do SendPacket


Validate::btnUnTagAll2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnUnTagAll2',p_web.GetValue('NewValue'))
    do Value::btnUnTagAll2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    ClearGenericTagFile(p_web,'C')
  do SendAlert
  do Value::brwChargeTypes  !1

Value::btnUnTagAll2  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('btnUnTagAll2') & '_value',Choose(p_web.GSV('locAllChargeTypes') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllChargeTypes') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnUnTagAll2'',''formwarrantyincomereport_btnuntagall2_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnUnTagAll2','UnTag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('btnUnTagAll2') & '_value')


Validate::__line2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__line2',p_web.GetValue('NewValue'))
    do Value::__line2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__line2  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('__line2') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::btnBack2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnBack2',p_web.GetValue('NewValue'))
    do Value::btnBack2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnBack2  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('btnBack2') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnBack','Back','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormWarrantyIncomeReport?' &'tab=1')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/listback.png',,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnNext2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnNext2',p_web.GetValue('NewValue'))
    do Value::btnNext2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnNext2  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('btnNext2') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnNext','Next','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormWarrantyIncomeReport?' &'tab=3')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/listnext.png',,,,)

  do SendPacket
  p_web._DivFooter()


Prompt::locAllManufacturers  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locAllManufacturers') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('All Manufacturers')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAllManufacturers  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAllManufacturers',p_web.GetValue('NewValue'))
    locAllManufacturers = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAllManufacturers
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locAllManufacturers',p_web.GetValue('Value'))
    locAllManufacturers = p_web.GetValue('Value')
  End
  do Value::locAllManufacturers
  do SendAlert
  do Value::brwManufacturers  !1
  do Value::btnUntagAll3  !1

Value::locAllManufacturers  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locAllManufacturers') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locAllManufacturers
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locAllManufacturers'',''formwarrantyincomereport_locallmanufacturers_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locAllManufacturers')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locAllManufacturers') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locAllManufacturers',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locAllManufacturers') & '_value')


Validate::brwManufacturers  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwManufacturers',p_web.GetValue('NewValue'))
    do Value::brwManufacturers
  Else
    p_web.StoreValue('man:RecordNumber')
  End

Value::brwManufacturers  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('locAllManufacturers') = 1,1,0))
  ! --- BROWSE ---  BrowseTagManufacturers --
  p_web.SetValue('BrowseTagManufacturers:NoForm',1)
  p_web.SetValue('BrowseTagManufacturers:FormName',loc:formname)
  p_web.SetValue('BrowseTagManufacturers:parentIs','Form')
  p_web.SetValue('_parentProc','FormWarrantyIncomeReport')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormWarrantyIncomeReport_BrowseTagManufacturers_embedded_div')&'"><!-- Net:BrowseTagManufacturers --></div><13,10>'
    p_web._DivHeader('FormWarrantyIncomeReport_' & lower('BrowseTagManufacturers') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormWarrantyIncomeReport_' & lower('BrowseTagManufacturers') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseTagManufacturers --><13,10>'
  end
  do SendPacket


Validate::btnUntagAll3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnUntagAll3',p_web.GetValue('NewValue'))
    do Value::btnUntagAll3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnUntagAll3  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('btnUntagAll3') & '_value',Choose(p_web.GSV('locAllManufacturers') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllManufacturers') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnUntagAll','UnTag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('btnUntagAll3') & '_value')


Validate::__line3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__line3',p_web.GetValue('NewValue'))
    do Value::__line3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__line3  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('__line3') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::btnBack3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnBack3',p_web.GetValue('NewValue'))
    do Value::btnBack3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnBack3  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('btnBack3') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnBack3','Back','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormWarrantyIncomeReport?' &'tab=2')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/listback.png',,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnNext3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnNext3',p_web.GetValue('NewValue'))
    do Value::btnNext3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnNext3  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('btnNext3') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnNext3','Next','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormWarrantyIncomeReport?' &'tab=4')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/listnext.png',,,,)

  do SendPacket
  p_web._DivFooter()


Prompt::locAllWarrantyStatus  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locAllWarrantyStatus') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('All Warranty Status')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAllWarrantyStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAllWarrantyStatus',p_web.GetValue('NewValue'))
    locAllWarrantyStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAllWarrantyStatus
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locAllWarrantyStatus',p_web.GetValue('Value'))
    locAllWarrantyStatus = p_web.GetValue('Value')
  End
  do Value::locAllWarrantyStatus
  do SendAlert
  do Prompt::locSubmitted
  do Value::locSubmitted  !1
  do Prompt::locResubmitted
  do Value::locResubmitted  !1
  do Prompt::locRejected
  do Value::locRejected  !1
  do Prompt::locRejectionAcknowledged
  do Value::locRejectionAcknowledged  !1
  do Prompt::locReconciled
  do Value::locReconciled  !1

Value::locAllWarrantyStatus  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locAllWarrantyStatus') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locAllWarrantyStatus
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locAllWarrantyStatus'',''formwarrantyincomereport_locallwarrantystatus_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locAllWarrantyStatus')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locAllWarrantyStatus') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locAllWarrantyStatus',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locAllWarrantyStatus') & '_value')


Prompt::locSubmitted  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locSubmitted') & '_prompt',Choose(p_web.GSV('locAllWarrantyStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Submitted')
  If p_web.GSV('locAllWarrantyStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locSubmitted') & '_prompt')

Validate::locSubmitted  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSubmitted',p_web.GetValue('NewValue'))
    locSubmitted = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSubmitted
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locSubmitted',p_web.GetValue('Value'))
    locSubmitted = p_web.GetValue('Value')
  End
  do Value::locSubmitted
  do SendAlert

Value::locSubmitted  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locSubmitted') & '_value',Choose(p_web.GSV('locAllWarrantyStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllWarrantyStatus') = 1)
  ! --- CHECKBOX --- locSubmitted
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSubmitted'',''formwarrantyincomereport_locsubmitted_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locSubmitted') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locSubmitted',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locSubmitted') & '_value')


Prompt::locResubmitted  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locResubmitted') & '_prompt',Choose(p_web.GSV('locAllWarrantyStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Resubmitted')
  If p_web.GSV('locAllWarrantyStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locResubmitted') & '_prompt')

Validate::locResubmitted  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locResubmitted',p_web.GetValue('NewValue'))
    locResubmitted = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locResubmitted
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locResubmitted',p_web.GetValue('Value'))
    locResubmitted = p_web.GetValue('Value')
  End
  do Value::locResubmitted
  do SendAlert

Value::locResubmitted  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locResubmitted') & '_value',Choose(p_web.GSV('locAllWarrantyStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllWarrantyStatus') = 1)
  ! --- CHECKBOX --- locResubmitted
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locResubmitted'',''formwarrantyincomereport_locresubmitted_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locResubmitted') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locResubmitted',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locResubmitted') & '_value')


Prompt::locRejected  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locRejected') & '_prompt',Choose(p_web.GSV('locAllWarrantyStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Rejected')
  If p_web.GSV('locAllWarrantyStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locRejected') & '_prompt')

Validate::locRejected  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locRejected',p_web.GetValue('NewValue'))
    locRejected = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locRejected
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locRejected',p_web.GetValue('Value'))
    locRejected = p_web.GetValue('Value')
  End
  do Value::locRejected
  do SendAlert

Value::locRejected  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locRejected') & '_value',Choose(p_web.GSV('locAllWarrantyStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllWarrantyStatus') = 1)
  ! --- CHECKBOX --- locRejected
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locRejected'',''formwarrantyincomereport_locrejected_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locRejected') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locRejected',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locRejected') & '_value')


Prompt::locRejectionAcknowledged  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locRejectionAcknowledged') & '_prompt',Choose(p_web.GSV('locAllWarrantyStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Rejection Acknowledged')
  If p_web.GSV('locAllWarrantyStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locRejectionAcknowledged') & '_prompt')

Validate::locRejectionAcknowledged  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locRejectionAcknowledged',p_web.GetValue('NewValue'))
    locRejectionAcknowledged = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locRejectionAcknowledged
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locRejectionAcknowledged',p_web.GetValue('Value'))
    locRejectionAcknowledged = p_web.GetValue('Value')
  End
  do Value::locRejectionAcknowledged
  do SendAlert

Value::locRejectionAcknowledged  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locRejectionAcknowledged') & '_value',Choose(p_web.GSV('locAllWarrantyStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllWarrantyStatus') = 1)
  ! --- CHECKBOX --- locRejectionAcknowledged
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locRejectionAcknowledged'',''formwarrantyincomereport_locrejectionacknowledged_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locRejectionAcknowledged') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locRejectionAcknowledged',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locRejectionAcknowledged') & '_value')


Prompt::locReconciled  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locReconciled') & '_prompt',Choose(p_web.GSV('locAllWarrantyStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Reconciled')
  If p_web.GSV('locAllWarrantyStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locReconciled') & '_prompt')

Validate::locReconciled  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locReconciled',p_web.GetValue('NewValue'))
    locReconciled = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locReconciled
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locReconciled',p_web.GetValue('Value'))
    locReconciled = p_web.GetValue('Value')
  End
  do Value::locReconciled
  do SendAlert

Value::locReconciled  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locReconciled') & '_value',Choose(p_web.GSV('locAllWarrantyStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllWarrantyStatus') = 1)
  ! --- CHECKBOX --- locReconciled
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locReconciled'',''formwarrantyincomereport_locreconciled_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locReconciled') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locReconciled',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locReconciled') & '_value')


Validate::__br1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__br1',p_web.GetValue('NewValue'))
    do Value::__br1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__br1  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('__br1') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.Translate('<br/><br/>',1) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locStartDate  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locStartDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Invoice Start Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locStartDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locStartDate',p_web.GetValue('NewValue'))
    locStartDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locStartDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locStartDate',p_web.dFormat(p_web.GetValue('Value'),'@d06'))
    locStartDate = p_web.Dformat(p_web.GetValue('Value'),'@d06') !
  End
  If locStartDate = ''
    loc:Invalid = 'locStartDate'
    loc:alert = p_web.translate('Invoice Start Date') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locStartDate = Upper(locStartDate)
    p_web.SetSessionValue('locStartDate',locStartDate)
  do Value::locStartDate
  do SendAlert

Value::locStartDate  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locStartDate') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locStartDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locStartDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locStartDate = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locStartDate'',''formwarrantyincomereport_locstartdate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locStartDate',p_web.GetSessionValue('locStartDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06',loc:javascript,,) & '<13,10>'
  do SendPacket
   packet = CLIP(packet) & |
     '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar' & |
     '(locStartDate,''dd/mm/yyyy'',this); Date.disabled=true;' & |
     'sv(''...'',''FormWarrantyIncomeReport_pickdate_value'',1,FieldValue(this,1));nextFocus(FormWarrantyIncomeReport_frm,'''',0);" ' & |
     'value="Select Date" name="Date" type="button">...</button>'
   DO SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locStartDate') & '_value')


Prompt::locEndDate  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locEndDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Invoice End Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locEndDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEndDate',p_web.GetValue('NewValue'))
    locEndDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEndDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEndDate',p_web.dFormat(p_web.GetValue('Value'),'@d06'))
    locEndDate = p_web.Dformat(p_web.GetValue('Value'),'@d06') !
  End
  If locEndDate = ''
    loc:Invalid = 'locEndDate'
    loc:alert = p_web.translate('Invoice End Date') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locEndDate = Upper(locEndDate)
    p_web.SetSessionValue('locEndDate',locEndDate)
  do Value::locEndDate
  do SendAlert

Value::locEndDate  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locEndDate') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locEndDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locEndDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locEndDate = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEndDate'',''formwarrantyincomereport_locenddate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locEndDate',p_web.GetSessionValue('locEndDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06',loc:javascript,,) & '<13,10>'
  do SendPacket
   packet = CLIP(packet) & |
     '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar' & |
     '(locEndDate,''dd/mm/yyyy'',this); Date.disabled=true;' & |
     'sv(''...'',''FormWarrantyIncomeReport_pickdate_value'',1,FieldValue(this,1));nextFocus(FormWarrantyIncomeReport_frm,'''',0);" ' & |
     'value="Select Date" name="Date" type="button">...</button>'
   DO SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locEndDate') & '_value')


Prompt::locDateRangeType  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locDateRangeType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Date Range Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locDateRangeType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locDateRangeType',p_web.GetValue('NewValue'))
    locDateRangeType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locDateRangeType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locDateRangeType',p_web.GetValue('Value'))
    locDateRangeType = p_web.GetValue('Value')
  End
  do Value::locDateRangeType
  do SendAlert

Value::locDateRangeType  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locDateRangeType') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locDateRangeType
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locDateRangeType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locDateRangeType') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locDateRangeType'',''formwarrantyincomereport_locdaterangetype_value'',1,'''&clip(0)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locDateRangeType',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locDateRangeType_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Completed') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locDateRangeType') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locDateRangeType'',''formwarrantyincomereport_locdaterangetype_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locDateRangeType',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locDateRangeType_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Submitted') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locDateRangeType') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locDateRangeType'',''formwarrantyincomereport_locdaterangetype_value'',1,'''&clip(2)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locDateRangeType',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locDateRangeType_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Accepted') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locDateRangeType') & '_value')


Prompt::locExcludedOnly  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locExcludedOnly') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Excluded Only')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locExcludedOnly  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExcludedOnly',p_web.GetValue('NewValue'))
    locExcludedOnly = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExcludedOnly
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locExcludedOnly',p_web.GetValue('Value'))
    locExcludedOnly = p_web.GetValue('Value')
  End
  do Value::locExcludedOnly
  do SendAlert

Value::locExcludedOnly  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locExcludedOnly') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locExcludedOnly
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locExcludedOnly'',''formwarrantyincomereport_locexcludedonly_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locExcludedOnly') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locExcludedOnly',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locExcludedOnly') & '_value')


Prompt::locIncludeARCRepairedJobs  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locIncludeARCRepairedJobs') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Include ARC Repaired RRC Jobs')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIncludeARCRepairedJobs  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIncludeARCRepairedJobs',p_web.GetValue('NewValue'))
    locIncludeARCRepairedJobs = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIncludeARCRepairedJobs
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locIncludeARCRepairedJobs',p_web.GetValue('Value'))
    locIncludeARCRepairedJobs = p_web.GetValue('Value')
  End
  do Value::locIncludeARCRepairedJobs
  do SendAlert

Value::locIncludeARCRepairedJobs  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locIncludeARCRepairedJobs') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locIncludeARCRepairedJobs
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locIncludeARCRepairedJobs'',''formwarrantyincomereport_locincludearcrepairedjobs_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locIncludeARCRepairedJobs') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locIncludeARCRepairedJobs',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locIncludeARCRepairedJobs') & '_value')


Validate::__br2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__br2',p_web.GetValue('NewValue'))
    do Value::__br2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__br2  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('__br2') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.Translate('<br/><br/>',1) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locZeroSupression  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locZeroSupression') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Zero Suppression')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locZeroSupression  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locZeroSupression',p_web.GetValue('NewValue'))
    locZeroSupression = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locZeroSupression
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locZeroSupression',p_web.GetValue('Value'))
    locZeroSupression = p_web.GetValue('Value')
  End
  do Value::locZeroSupression
  do SendAlert

Value::locZeroSupression  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locZeroSupression') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locZeroSupression
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locZeroSupression')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locZeroSupression') = 'Y'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locZeroSupression'',''formwarrantyincomereport_loczerosupression_value'',1,'''&clip('Y')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locZeroSupression',clip('Y'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locZeroSupression_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Suppress Zeros') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locZeroSupression') = 'N'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locZeroSupression'',''formwarrantyincomereport_loczerosupression_value'',1,'''&clip('N')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locZeroSupression',clip('N'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locZeroSupression_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Show Zeros') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locZeroSupression') & '_value')


Prompt::locReportOrder  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locReportOrder') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Report Order')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locReportOrder  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locReportOrder',p_web.GetValue('NewValue'))
    locReportOrder = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locReportOrder
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locReportOrder',p_web.GetValue('Value'))
    locReportOrder = p_web.GetValue('Value')
  End
  do Value::locReportOrder
  do SendAlert

Value::locReportOrder  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('locReportOrder') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locReportOrder
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locReportOrder')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locReportOrder') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locReportOrder'',''formwarrantyincomereport_locreportorder_value'',1,'''&clip(0)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locReportOrder',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locReportOrder_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('By Manufacturer') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locReportOrder') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locReportOrder'',''formwarrantyincomereport_locreportorder_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locReportOrder',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locReportOrder_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('By Job Number') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locReportOrder') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locReportOrder'',''formwarrantyincomereport_locreportorder_value'',1,'''&clip(2)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locReportOrder',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locReportOrder_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('By Date Completed') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormWarrantyIncomeReport_' & p_web._nocolon('locReportOrder') & '_value')


Validate::btnPrintReport  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnPrintReport',p_web.GetValue('NewValue'))
    do Value::btnPrintReport
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnPrintReport  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('btnPrintReport') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnPrintReport','Print Report','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=WarrantyIncomeReportEngine&ReportType=R')) & ''','''&clip('_blank')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnExport  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnExport',p_web.GetValue('NewValue'))
    do Value::btnExport
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnExport  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('btnExport') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnExport','Create Export','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=WarrantyIncomeReportEngine&ReportType=E')) & ''','''&clip('_blank')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Validate::__line4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__line4',p_web.GetValue('NewValue'))
    do Value::__line4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__line4  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('__line4') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::btnBack4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnBack4',p_web.GetValue('NewValue'))
    do Value::btnBack4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnBack4  Routine
  p_web._DivHeader('FormWarrantyIncomeReport_' & p_web._nocolon('btnBack4') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnback3','Back','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormWarrantyIncomeReport?' &'tab=3')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/listback.png',,,,)

  do SendPacket
  p_web._DivFooter()


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormWarrantyIncomeReport_locAllAccounts_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAllAccounts
      else
        do Value::locAllAccounts
      end
  of lower('FormWarrantyIncomeReport_locGenericAccounts_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locGenericAccounts
      else
        do Value::locGenericAccounts
      end
  of lower('FormWarrantyIncomeReport_btnUnTagAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnUnTagAll
      else
        do Value::btnUnTagAll
      end
  of lower('FormWarrantyIncomeReport_btnPrevDis_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnPrevDis
      else
        do Value::btnPrevDis
      end
  of lower('FormWarrantyIncomeReport_locAllChargeTypes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAllChargeTypes
      else
        do Value::locAllChargeTypes
      end
  of lower('FormWarrantyIncomeReport_btnUnTagAll2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnUnTagAll2
      else
        do Value::btnUnTagAll2
      end
  of lower('FormWarrantyIncomeReport_locAllManufacturers_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAllManufacturers
      else
        do Value::locAllManufacturers
      end
  of lower('FormWarrantyIncomeReport_locAllWarrantyStatus_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAllWarrantyStatus
      else
        do Value::locAllWarrantyStatus
      end
  of lower('FormWarrantyIncomeReport_locSubmitted_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSubmitted
      else
        do Value::locSubmitted
      end
  of lower('FormWarrantyIncomeReport_locResubmitted_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locResubmitted
      else
        do Value::locResubmitted
      end
  of lower('FormWarrantyIncomeReport_locRejected_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locRejected
      else
        do Value::locRejected
      end
  of lower('FormWarrantyIncomeReport_locRejectionAcknowledged_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locRejectionAcknowledged
      else
        do Value::locRejectionAcknowledged
      end
  of lower('FormWarrantyIncomeReport_locReconciled_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locReconciled
      else
        do Value::locReconciled
      end
  of lower('FormWarrantyIncomeReport_locStartDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locStartDate
      else
        do Value::locStartDate
      end
  of lower('FormWarrantyIncomeReport_locEndDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEndDate
      else
        do Value::locEndDate
      end
  of lower('FormWarrantyIncomeReport_locDateRangeType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locDateRangeType
      else
        do Value::locDateRangeType
      end
  of lower('FormWarrantyIncomeReport_locExcludedOnly_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locExcludedOnly
      else
        do Value::locExcludedOnly
      end
  of lower('FormWarrantyIncomeReport_locIncludeARCRepairedJobs_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIncludeARCRepairedJobs
      else
        do Value::locIncludeARCRepairedJobs
      end
  of lower('FormWarrantyIncomeReport_locZeroSupression_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locZeroSupression
      else
        do Value::locZeroSupression
      end
  of lower('FormWarrantyIncomeReport_locReportOrder_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locReportOrder
      else
        do Value::locReportOrder
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormWarrantyIncomeReport_form:ready_',1)
  p_web.SetSessionValue('FormWarrantyIncomeReport_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormWarrantyIncomeReport',0)

PreCopy  Routine
  p_web.SetValue('FormWarrantyIncomeReport_form:ready_',1)
  p_web.SetSessionValue('FormWarrantyIncomeReport_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormWarrantyIncomeReport',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormWarrantyIncomeReport_form:ready_',1)
  p_web.SetSessionValue('FormWarrantyIncomeReport_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormWarrantyIncomeReport:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormWarrantyIncomeReport_form:ready_',1)
  p_web.SetSessionValue('FormWarrantyIncomeReport_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormWarrantyIncomeReport:Primed',0)
  p_web.setsessionvalue('showtab_FormWarrantyIncomeReport',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GetValue('tab') = 1
          If p_web.IfExistsValue('locAllAccounts') = 0
            p_web.SetValue('locAllAccounts',0)
            locAllAccounts = 0
          End
      If(p_web.GSV('locAllAccounts') = 1)
          If p_web.IfExistsValue('locGenericAccounts') = 0
            p_web.SetValue('locGenericAccounts',0)
            locGenericAccounts = 0
          End
      End
  End
  If p_web.GetValue('tab') = 2
          If p_web.IfExistsValue('locAllChargeTypes') = 0
            p_web.SetValue('locAllChargeTypes',0)
            locAllChargeTypes = 0
          End
  End
  If p_web.GetValue('tab') = 3
          If p_web.IfExistsValue('locAllManufacturers') = 0
            p_web.SetValue('locAllManufacturers',0)
            locAllManufacturers = 0
          End
  End
  If p_web.GetValue('tab') = 4
          If p_web.IfExistsValue('locAllWarrantyStatus') = 0
            p_web.SetValue('locAllWarrantyStatus',0)
            locAllWarrantyStatus = 0
          End
      If(p_web.GSV('locAllWarrantyStatus') = 1)
          If p_web.IfExistsValue('locSubmitted') = 0
            p_web.SetValue('locSubmitted',0)
            locSubmitted = 0
          End
      End
      If(p_web.GSV('locAllWarrantyStatus') = 1)
          If p_web.IfExistsValue('locResubmitted') = 0
            p_web.SetValue('locResubmitted',0)
            locResubmitted = 0
          End
      End
      If(p_web.GSV('locAllWarrantyStatus') = 1)
          If p_web.IfExistsValue('locRejected') = 0
            p_web.SetValue('locRejected',0)
            locRejected = 0
          End
      End
      If(p_web.GSV('locAllWarrantyStatus') = 1)
          If p_web.IfExistsValue('locRejectionAcknowledged') = 0
            p_web.SetValue('locRejectionAcknowledged',0)
            locRejectionAcknowledged = 0
          End
      End
      If(p_web.GSV('locAllWarrantyStatus') = 1)
          If p_web.IfExistsValue('locReconciled') = 0
            p_web.SetValue('locReconciled',0)
            locReconciled = 0
          End
      End
          If p_web.IfExistsValue('locExcludedOnly') = 0
            p_web.SetValue('locExcludedOnly',0)
            locExcludedOnly = 0
          End
          If p_web.IfExistsValue('locIncludeARCRepairedJobs') = 0
            p_web.SetValue('locIncludeARCRepairedJobs',0)
            locIncludeARCRepairedJobs = 0
          End
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormWarrantyIncomeReport_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormWarrantyIncomeReport_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
  If p_web.GetValue('tab') = 1
    loc:InvalidTab += 1
  End
  ! tab = 2
  If p_web.GetValue('tab') = 2
    loc:InvalidTab += 1
  End
  ! tab = 4
  If p_web.GetValue('tab') = 3
    loc:InvalidTab += 1
  End
  ! tab = 3
  If p_web.GetValue('tab') = 4
    loc:InvalidTab += 1
        If locStartDate = ''
          loc:Invalid = 'locStartDate'
          loc:alert = p_web.translate('Invoice Start Date') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locStartDate = Upper(locStartDate)
          p_web.SetSessionValue('locStartDate',locStartDate)
        If loc:Invalid <> '' then exit.
        If locEndDate = ''
          loc:Invalid = 'locEndDate'
          loc:alert = p_web.translate('Invoice End Date') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locEndDate = Upper(locEndDate)
          p_web.SetSessionValue('locEndDate',locEndDate)
        If loc:Invalid <> '' then exit.
  End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormWarrantyIncomeReport:Primed',0)
  p_web.StoreValue('locAllAccounts')
  p_web.StoreValue('locGenericAccounts')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locAllChargeTypes')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locAllManufacturers')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locAllWarrantyStatus')
  p_web.StoreValue('locSubmitted')
  p_web.StoreValue('locResubmitted')
  p_web.StoreValue('locRejected')
  p_web.StoreValue('locRejectionAcknowledged')
  p_web.StoreValue('locReconciled')
  p_web.StoreValue('')
  p_web.StoreValue('locStartDate')
  p_web.StoreValue('locEndDate')
  p_web.StoreValue('locDateRangeType')
  p_web.StoreValue('locExcludedOnly')
  p_web.StoreValue('locIncludeARCRepairedJobs')
  p_web.StoreValue('')
  p_web.StoreValue('locZeroSupression')
  p_web.StoreValue('locReportOrder')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locAllAccounts',locAllAccounts) ! LONG
     p_web.SSV('locGenericAccounts',locGenericAccounts) ! LONG
     p_web.SSV('locAllChargeTypes',locAllChargeTypes) ! LONG
     p_web.SSV('locStartDate',locStartDate) ! DATE
     p_web.SSV('locEndDate',locEndDate) ! DATE
     p_web.SSV('locZeroSupression',locZeroSupression) ! STRING(1)
     p_web.SSV('locReportOrder',locReportOrder) ! LONG
     p_web.SSV('locAllManufacturers',locAllManufacturers) ! LONG
     p_web.SSV('locAllWarrantyStatus',locAllWarrantyStatus) ! LONG
     p_web.SSV('locSubmitted',locSubmitted) ! LONG
     p_web.SSV('locResubmitted',locResubmitted) ! LONG
     p_web.SSV('locRejected',locRejected) ! LONG
     p_web.SSV('locRejectionAcknowledged',locRejectionAcknowledged) ! LONG
     p_web.SSV('locReconciled',locReconciled) ! LONG
     p_web.SSV('locDateRangeType',locDateRangeType) ! LONG
     p_web.SSV('locExcludedOnly',locExcludedOnly) ! LONG
     p_web.SSV('locIncludeARCRepairedJobs',locIncludeARCRepairedJobs) ! LONG
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locAllAccounts = p_web.GSV('locAllAccounts') ! LONG
     locGenericAccounts = p_web.GSV('locGenericAccounts') ! LONG
     locAllChargeTypes = p_web.GSV('locAllChargeTypes') ! LONG
     locStartDate = p_web.GSV('locStartDate') ! DATE
     locEndDate = p_web.GSV('locEndDate') ! DATE
     locZeroSupression = p_web.GSV('locZeroSupression') ! STRING(1)
     locReportOrder = p_web.GSV('locReportOrder') ! LONG
     locAllManufacturers = p_web.GSV('locAllManufacturers') ! LONG
     locAllWarrantyStatus = p_web.GSV('locAllWarrantyStatus') ! LONG
     locSubmitted = p_web.GSV('locSubmitted') ! LONG
     locResubmitted = p_web.GSV('locResubmitted') ! LONG
     locRejected = p_web.GSV('locRejected') ! LONG
     locRejectionAcknowledged = p_web.GSV('locRejectionAcknowledged') ! LONG
     locReconciled = p_web.GSV('locReconciled') ! LONG
     locDateRangeType = p_web.GSV('locDateRangeType') ! LONG
     locExcludedOnly = p_web.GSV('locExcludedOnly') ! LONG
     locIncludeARCRepairedJobs = p_web.GSV('locIncludeARCRepairedJobs') ! LONG
