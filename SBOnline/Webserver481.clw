

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER481.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
GetVATRate           PROCEDURE  (fAccountNumber,fType)     ! Declare Procedure
rtnValue             REAL                                  !
FilesOpened     BYTE(0)

  CODE
    DO openFiles
    rtnValue = 0
    If InvoiceSubAccounts(fAccountNumber)
        Access:VATCODE.Clearkey(vat:Vat_Code_Key)
        Case fType
        Of 'L'
            vat:Vat_Code    = sub:Labour_Vat_Code
        Of 'P'
            vat:Vat_Code    = sub:Parts_Vat_Code
        End !Case func:Type
        If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
            !Found
            rtnvalue =  vat:Vat_Rate
        Else ! If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
            !Error
        End !If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
    Else !If InvoiceSubAccounts(func:AccountNumber)
        Access:VATCODE.Clearkey(vat:Vat_Code_Key)
        Case fType
        Of 'L'
            vat:Vat_Code    = tra:Labour_Vat_Code
        Of 'P'
            vat:Vat_Code    = tra:Parts_Vat_Code
        End !Case func:Type
        If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
            !Found
            rtnValue =  vat:Vat_Rate
        Else ! If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
            !Error
        End !If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
    End !If InvoiceSubAccounts(func:AccountNumber)
    Do CloseFiles
    Return rtnValue
    
    
!--------------------------------------
OpenFiles  ROUTINE
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:SUBTRACC.Close
     Access:TRADEACC.Close
     Access:VATCODE.Close
     FilesOpened = False
  END
