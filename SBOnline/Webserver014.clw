

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER014.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
LoanAttachedToJob    PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
    p_web.SSV('LoanAttachedToJob',0)            
    if ((p_web.GSV('job:Courier_Cost') <> 0 or p_web.GSV('job:Courier_Cost_Estimate') <> 0 or |
            p_web.GSV('job:invoice_Courier_Cost') <> 0) and p_web.GSV('jobe:Engineer48HourOtion') <> 1)
        p_web.SSV('LoanAttachedToJob',1)
    else
        if (p_web.GSV('job:loan_unit_Number') <> 0)
            p_web.SSV('LoanAttachedToJob',1)
        else ! if (p_web.GSV('job:loan_unit_Number') <> 0)
            found# = 0
            Access:AUDIT.Clearkey(aud:typeActionKey)
            aud:ref_Number    = p_web.GSV('job:Ref_Number')
            aud:type    = 'LOA'
            aud:action    = 'LOAN UNIT ATTACHED TO JOB'
            set(aud:typeActionKey,aud:typeActionKey)
            loop
                if (Access:AUDIT.Next())
                    Break
                end ! if (Access:AUDIT.Next())
                if (aud:ref_Number    <> p_web.GSV('job:Ref_Number'))
                    Break
                end ! if (aud:ref_Number    <> p_web.GSV('job:Ref_Number'))
                if (aud:type    <> 'LOA')
                    Break
                end ! if (aud:type    <> 'LOA')
                if (aud:action    <> 'LOAN UNIT ATTACHED TO JOB')
                    Break
                end ! if (aud:action    <> 'LOAN UNIT ATTACHED TO JOB')
                p_web.SSV('LoanAttachedToJob',1)
                break
            end ! loop
        end ! if (p_web.GSV('job:loan_unit_Number') <> 0)
    end
!--------------------------------------
OpenFiles  ROUTINE
  Access:AUDIT.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:AUDIT.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:AUDIT.Close
     FilesOpened = False
  END
