

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER181.INC'),ONCE        !Local module procedure declarations
                     END


BrowsePartFaultCodeLookup PROCEDURE  (NetWebServerWorker p_web)
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(MANFPALO)
                      Project(mfp:RecordNumber)
                      Project(mfp:Field)
                      Project(mfp:Description)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
MANFAUPA::State  USHORT
MODELCCT::State  USHORT
MANFAULO_ALIAS::State  USHORT
MANFPARL::State  USHORT
MANFAUPA_ALIAS::State  USHORT
MANFPALO_ALIAS::State  USHORT
MANFAURL::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowsePartFaultCodeLookup')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowsePartFaultCodeLookup:NoForm')
      loc:NoForm = p_web.GetValue('BrowsePartFaultCodeLookup:NoForm')
      loc:FormName = p_web.GetValue('BrowsePartFaultCodeLookup:FormName')
    else
      loc:FormName = 'BrowsePartFaultCodeLookup_frm'
    End
    p_web.SSV('BrowsePartFaultCodeLookup:NoForm',loc:NoForm)
    p_web.SSV('BrowsePartFaultCodeLookup:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowsePartFaultCodeLookup:NoForm')
    loc:FormName = p_web.GSV('BrowsePartFaultCodeLookup:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowsePartFaultCodeLookup') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowsePartFaultCodeLookup')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(MANFPALO,mfp:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'MFP:FIELD') then p_web.SetValue('BrowsePartFaultCodeLookup_sort','1')
    ElsIf (loc:vorder = 'MFP:DESCRIPTION') then p_web.SetValue('BrowsePartFaultCodeLookup_sort','2')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowsePartFaultCodeLookup:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowsePartFaultCodeLookup:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowsePartFaultCodeLookup:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowsePartFaultCodeLookup:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowsePartFaultCodeLookup:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  ! Save Window Name
  AddToLog('NetWebBrowse',p_web.RequestData.DataString,'BrowsePartFaultCodeLookup',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    if (p_web.ifExistsValue('fieldNumber'))
        p_web.storeValue('fieldNumber')
    end !if (p_web.ifExistsValue('fieldNumber'))
    if (p_web.ifExistsValue('partType'))
        p_web.storeValue('partType')
    end ! if (p_web.ifExistsValue('partType'))  
  
  ! Inserting (DBH 30/04/2008) # 9723 - Limit by Model Number if it's a CCT Reference
  p_web.SSV('CCT',0)
  Access:MANFAUPA.Clearkey(map:Field_Number_Key)
  map:Manufacturer = p_web.GSV('job:Manufacturer')
  map:Field_Number = p_web.GSV('fieldNumber')
  If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
      ! Found
      ! Is this fault a CCT? (DBH: 30/04/2008)
      If map:CCTReferenceFaultCode
          p_web.SSV('CCT',1)
      End ! If map:CCTReferenceFaultCode
  End ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
  ! End (DBH 30/04/2008) #9723
  
  ! Build Associated Fault Codes Queue
  
  !Clear queue for this user
  clear(tmpfau:Record)
  tmpfau:sessionID = p_web.SessionID
  set(tmpfau:keySessionID,tmpfau:keySessionID)
  loop
      next(tempFaultCodes)
      if (error())
          break
      end ! if (error())
      if (tmpfau:sessionID <> p_web.sessionID)
          break
      end ! if (tmpfau:sessionID <> p_web.sessionID)
      delete(tempFaultCodes)
  end ! loop
  
  loop x# = 1 to 12
      if (x# = p_web.GSV('fieldNumber'))
          cycle
      end ! if (x# = fieldNumber)
  
      if (p_web.GSV('Hide:PartFaultCode' & x#))
          cycle
      end ! if (p_web.GSV('Hide:PartFaultCode' & x#))
  
      if (p_web.GSV('tmp:FaultCodes' & x#) = '')
          cycle
      end ! if (p_web.GSV('tmp:FaultCodes' & x#) = '')
  
      Access:MANFAUPA_ALIAS.Clearkey(map_ali:ScreenOrderKey)
      map_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
      map_ali:Field_Number    = x#
      if (Access:MANFAUPA_ALIAS.TryFetch(map_ali:ScreenOrderKey) = Level:Benign)
          ! Found
          Access:MANFPALO_ALIAS.Clearkey(mfp_ali:Field_Key)
          mfp_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
          mfp_ali:Field_Number    = map_ali:Field_Number
          mfp_ali:Field    = p_web.GSV('tmp:FaultCodes' & x#)
          if (Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign)
              ! Found
              found# = 0
  
              Access:MANFPARL.Clearkey(mpr:FieldKey)
              mpr:MANFPALORecordNumber    = mfp_ali:RecordNumber
              mpr:FieldNumber    = p_web.GSV('fieldNumber')
              set(mpr:FieldKey,mpr:FieldKey)
              loop
                  if (Access:MANFPARL.Next())
                      Break
                  end ! if (Access:MANFPARL.Next())
                  if (mpr:MANFPALORecordNumber    <> mfp_ali:RecordNumber)
                      Break
                  end ! if (mfp:MANFPALORecordNumber    <> mfp_ali:RecordNumber)
                  if (mpr:FieldNumber    <> p_web.GSV('fieldNumber'))
                      Break
                  end ! if (mfp:FieldNumber    <> p_web.GSV('fieldNumber'))
                  found# = 1
                  break
              end ! loop
  
              if (found# = 1)
                  tmpfau:sessionID = p_web.sessionID
                  tmpfau:recordNumber = mfp_ali:recordNumber
                  tmpfau:faultType = 'P'
                  add(tempFaultCodes)
              end ! if (found# = 1)
  
          else ! if (Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign)
              ! Error
          end ! if (Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign)
      else ! if (Access:MANFAUPA_ALIAS.TryFetch(map_ali:Field_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:MANFAUPA_ALIAS.TryFetch(map_ali:Field_Number_Key) = Level:Benign)
  
  
      Access:MANFAULT.Clearkey(maf:Field_Number_Key)
      maf:Manufacturer    = p_web.GSV('job:Manufacturer')
      maf:Field_Number    = x#
      if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
          ! Found
          if (maf:Lookup = 'YES')
  
              Access:MANFAULO.Clearkey(mfo:Field_Key)
              mfo:Manufacturer    = p_web.GSV('job:Manufacturer')
              mfo:Field_Number    = maf:Field_Number
              if (x# < 13)
                  mfo:Field = p_web.GSV('job:Fault_Code' & x#)
              else ! if (x# < 13)
                  mfo:Field = p_web.GSV('wob:FaultCode' & x#)
              end !if (x# < 13)
              if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                  ! Found
                  found# = 0
                  Access:MANFAURL.Clearkey(mnr:FieldKey)
                  mnr:MANFAULORecordNumber    = mfo:RecordNumber
                  mnr:PartFaultCode    = 1
                  mnr:FieldNumber    = p_web.GSV('fieldNumber')
                  set(mnr:FieldKey,mnr:FieldKey)
                  loop
                      if (Access:MANFAURL.Next())
                          Break
                      end ! if (Access:MANFAURL.Next())
                      if (mnr:MANFAULORecordNumber    <> mfo:RecordNumber)
                          Break
                      end ! if (mnr:MANFALORecordNumber    <> mfo:RecordNumber)
                      if (mnr:PartFaultCode    <> 1)
                          Break
                      end ! if (mnr:PartFaultCode    <> 1)
                      if (mnr:FieldNumber    <> p_web.GSV('fieldNumber'))
                          Break
                      end ! if (mnr:FieldNumber    <> fieldNumber)
                      found# = 1
                      break
                  end ! loop
                  if (found# = 1)
                      tmpfau:sessionID = p_web.sessionID
                      tmpfau:recordNumber = mfo:recordNumber
                      tmpfau:faultType = 'J'
                      add(tempFaultCodes)
                  end ! if (found# = 1)
              else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                  ! Error
              end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
          end ! if (maf:Lookup = 'YES')
      else ! if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
      
  end ! loop x# = 1 to 12
  p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowsePartFaultCodeLookup_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowsePartFaultCodeLookup_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 3
    Loc:LocateField = ''
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(mfp:Field)','-UPPER(mfp:Field)')
    Loc:LocateField = 'mfp:Field'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(mfp:Description)','-UPPER(mfp:Description)')
    Loc:LocateField = 'mfp:Description'
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(mfp:Manufacturer),+mfp:Field_Number,+UPPER(mfp:Field)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('mfp:Field')
    loc:SortHeader = p_web.Translate('Field')
    p_web.SetSessionValue('BrowsePartFaultCodeLookup_LocatorPic','@s30')
  Of upper('mfp:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowsePartFaultCodeLookup_LocatorPic','@s60')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowsePartFaultCodeLookup:LookupFrom')
  End!Else
  loc:formaction = 'BrowsePartFaultCodeLookup'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowsePartFaultCodeLookup:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowsePartFaultCodeLookup:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowsePartFaultCodeLookup:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="MANFPALO"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="mfp:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Select Part Fault Codes') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Select Part Fault Codes',0)&'</span>'&CRLF
  End
  If clip('Select Part Fault Codes') <> ''
    !packet = clip(packet) & p_web.br !Bryan. Why is this here?
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowsePartFaultCodeLookup',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowsePartFaultCodeLookup',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowsePartFaultCodeLookup.locate(''Locator2BrowsePartFaultCodeLookup'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowsePartFaultCodeLookup.cl(''BrowsePartFaultCodeLookup'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="BrowsePartFaultCodeLookup_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="BrowsePartFaultCodeLookup_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
    If loc:Selecting = 1
        packet = clip(packet) & '<th>'&p_web.Translate('Pick')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
    End ! Selecting
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowsePartFaultCodeLookup','Field','Click here to sort by Field',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Field')&'">'&p_web.Translate('Field')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowsePartFaultCodeLookup','Description','Click here to sort by Description',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Description')&'">'&p_web.Translate('Description')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('mfp:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and MANFPALO{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'mfp:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('mfp:RecordNumber'),p_web.GetValue('mfp:RecordNumber'),p_web.GetSessionValue('mfp:RecordNumber'))
      loc:FilterWas = 'mfp:NotAvailable = 0 AND Upper(mfp:Manufacturer) = Upper(''' & p_web.GSV('job:Manufacturer') & ''') AND Upper(mfp:Field_Number) = ' & p_web.GSV('fieldNumber')
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowsePartFaultCodeLookup',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowsePartFaultCodeLookup_Filter')
    p_web.SetSessionValue('BrowsePartFaultCodeLookup_FirstValue','')
    p_web.SetSessionValue('BrowsePartFaultCodeLookup_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,MANFPALO,mfp:RecordNumberKey,loc:PageRows,'BrowsePartFaultCodeLookup',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If MANFPALO{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(MANFPALO,loc:firstvalue)
              Reset(ThisView,MANFPALO)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If MANFPALO{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(MANFPALO,loc:lastvalue)
            Reset(ThisView,MANFPALO)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      if (p_web.GSV('partType') = 'C')
          if (mfp:RestrictLookup = 1)
              if (p_web.GSV('par:Adjustment') = 'YES')
                  if (mfp:RestrictLookupType <> 3)
                      Cycle
                  end ! if (mfp:RestrictLookupType <> 3)
              else ! if (p_web.GSV('par:Adjustment') = 'YES')
                  if (p_web.GSV('par:Correction') = 1)
                      if (mfp:RestrictLookupType <> 2)
                          Cycle
                      end ! if (mfp:RestrictLookupType <> 2)
                  else ! if (p_web.GSV('par:Correction') = 1)
                      if (mfp:RestrictLookupType <> 1)
                          Cycle
                      end ! if (mfp:RestrictLookupType <> 1)
                  end ! if (p_web.GSV('par:Correction') = 1)
              end ! if (p_web.GSV('par:Adjustment') = 'YES')
          end ! if (mfp:RestrictLookup = 1)
      end ! if (p_web.GSV('partType') = 'C')
      if (p_web.GSV('partType') = 'W')
          if (mfp:RestrictLookup = 1)
              if (p_web.GSV('wpr:Adjustment') = 'YES')
                  if (mfp:RestrictLookupType <> 3)
                      Cycle
                  end ! if (mfp:RestrictLookupType <> 3)
              else ! if (p_web.GSV('par:Adjustment') = 'YES')
                  if (p_web.GSV('wpr:Correction') = 1)
                      if (mfp:RestrictLookupType <> 2)
                          Cycle
                      end ! if (mfp:RestrictLookupType <> 2)
                  else ! if (p_web.GSV('par:Correction') = 1)
                      if (mfp:RestrictLookupType <> 1)
                          Cycle
                      end ! if (mfp:RestrictLookupType <> 1)
                  end ! if (p_web.GSV('par:Correction') = 1)
              end ! if (p_web.GSV('par:Adjustment') = 'YES')
          end ! if (mfp:RestrictLookup = 1)
      end ! if (p_web.GSV('partType') = 'C')
      
      if (mfp:JobTypeAvailability = 1)
          if (p_web.GSV('partType') <> 'C')
              Cycle
          end ! if (p_web.GSV('job:Warranty_Job') = 'YES')
      end ! if (mfp:JobTypeAvailability = 1)
      
      if (mfp:JobTypeAvailability = 2)
          if (p_web.GSV('partType') <> 'W')
              Cycle
          end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
      end ! if (mfp:JobTypeAvailability = 2)
      
      if (p_web.GSV('CCT') = 1)
          Access:MODELCCT.Clearkey(mcc:CCTRefKey)
          mcc:ModelNumber    = p_web.GSV('job:Model_Number')
          mcc:CCTReferenceNumber    = mfp:Field
          if (Access:MODELCCT.TryFetch(mcc:CCTRefKey) = Level:Benign)
              ! Found
          else ! if (Access:MODELCCT.TryFetch(mcc:CCTRefKey) = Level:Benign)
              ! Error
              Cycle
          end ! if (Access:MODELCCT.TryFetch(mcc:CCTRefKey) = Level:Benign)
      end !if (p_web.GSV('CCT') = 1)
      
      found# = 1
      clear(tmpfau:record)
      tmpfau:sessionID = p_web.sessionID
      set(tmpfau:keySessionID,tmpfau:keySessionID)
      loop
          next(tempFaultCodes)
          if (error())
              break
          end ! if (error())
          if (tmpfau:sessionID <> p_web.sessionID)
              break
          end ! if (tmpfau:sessionID <> p_web.sessionID)
      
          found# = 0
          case tmpfau:FaultType
          of 'P' ! Part Fault Code
              Access:MANFPARL.Clearkey(mpr:LinkedRecordNumberKey)
              mpr:MANFPALORecordNumber    = tmpfau:RecordNumber
              mpr:LinkedRecordNumber    = mfp:RecordNumber
              mpr:JobFaultCode    = 0
              if (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
                  ! Found
                  found# = 1
                  break
              else ! if (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
                  ! Error
              end ! if (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
      
          of 'J' ! Job Fault Code
      
              Access:MANFAURL.Clearkey(mnr:LinkedRecordNumberKey)
              mnr:MANFAULORecordNumber    = tmpfau:RecordNumber
              mnr:LinkedRecordNumber    = mfp:RecordNumber
              mnr:PartFaultCode    = 1
              if (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
                  ! Found
                  found# = 1
                  break
              else ! if (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
                  ! Error
              end ! if (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
          end ! case faultQueue.FaultType
      end ! loop
      
      if (found# = 0)
          cycle
      end ! if (found# = 0)
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(mfp:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowsePartFaultCodeLookup.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowsePartFaultCodeLookup.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowsePartFaultCodeLookup.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowsePartFaultCodeLookup.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      If loc:found
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SelectButton,'BrowsePartFaultCodeLookup')
      End
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
    If loc:selecting = 1 !and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowsePartFaultCodeLookup',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowsePartFaultCodeLookup_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowsePartFaultCodeLookup_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowsePartFaultCodeLookup',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowsePartFaultCodeLookup.locate(''Locator1BrowsePartFaultCodeLookup'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowsePartFaultCodeLookup.cl(''BrowsePartFaultCodeLookup'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowsePartFaultCodeLookup_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowsePartFaultCodeLookup_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowsePartFaultCodeLookup.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowsePartFaultCodeLookup.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowsePartFaultCodeLookup.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowsePartFaultCodeLookup.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    If loc:found
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SelectButton,'BrowsePartFaultCodeLookup')
    End
    do SendPacket
  End
  If loc:selecting = 1 !and loc:parent = ''
    packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = mfp:RecordNumber
    p_web._thisrow = p_web._nocolon('mfp:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowsePartFaultCodeLookup:LookupField')) = mfp:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((mfp:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowsePartFaultCodeLookup.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If MANFPALO{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(MANFPALO)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If MANFPALO{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(MANFPALO)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','mfp:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BrowsePartFaultCodeLookup.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','mfp:RecordNumber',clip(loc:field),,'checked',,,'onclick="BrowsePartFaultCodeLookup.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::mfp:Field
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::mfp:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowsePartFaultCodeLookup.omv(this);" onMouseOut="BrowsePartFaultCodeLookup.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowsePartFaultCodeLookup=new browseTable(''BrowsePartFaultCodeLookup'','''&clip(loc:formname)&''','''&p_web._jsok('mfp:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('mfp:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowsePartFaultCodeLookup.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowsePartFaultCodeLookup.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowsePartFaultCodeLookup')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowsePartFaultCodeLookup')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowsePartFaultCodeLookup')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowsePartFaultCodeLookup')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(MANFPALO)
  p_web._CloseFile(MANFAUPA)
  p_web._CloseFile(MODELCCT)
  p_web._CloseFile(MANFAULO_ALIAS)
  p_web._CloseFile(MANFPARL)
  p_web._CloseFile(MANFAUPA_ALIAS)
  p_web._CloseFile(MANFPALO_ALIAS)
  p_web._CloseFile(MANFAURL)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(MANFPALO)
  Bind(mfp:Record)
  Clear(mfp:Record)
  NetWebSetSessionPics(p_web,MANFPALO)
  p_web._OpenFile(MANFAUPA)
  Bind(map:Record)
  NetWebSetSessionPics(p_web,MANFAUPA)
  p_web._OpenFile(MODELCCT)
  Bind(mcc:Record)
  NetWebSetSessionPics(p_web,MODELCCT)
  p_web._OpenFile(MANFAULO_ALIAS)
  Bind(mfo_ali:Record)
  NetWebSetSessionPics(p_web,MANFAULO_ALIAS)
  p_web._OpenFile(MANFPARL)
  Bind(mpr:Record)
  NetWebSetSessionPics(p_web,MANFPARL)
  p_web._OpenFile(MANFAUPA_ALIAS)
  Bind(map_ali:Record)
  NetWebSetSessionPics(p_web,MANFAUPA_ALIAS)
  p_web._OpenFile(MANFPALO_ALIAS)
  Bind(mfp_ali:Record)
  NetWebSetSessionPics(p_web,MANFPALO_ALIAS)
  p_web._OpenFile(MANFAURL)
  Bind(mnr:Record)
  NetWebSetSessionPics(p_web,MANFAURL)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('mfp:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
      ! Save Window Name
      IF (loc:alert <> '')
          AddToLog('Alert',loc:alert,'BrowsePartFaultCodeLookup',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
      END ! IF
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Select_'&mfp:RecordNumber,,net:crc)
        If loc:SelectAction
          If(loc:SelectionMethod  = Net:Radio)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowsePartFaultCodeLookup',loc:field)
          ElsIf(loc:SelectionMethod  = Net:Highlight)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowsePartFaultCodeLookup',loc:field)
          End
        Else
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowsePartFaultCodeLookup',loc:field)
        End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::mfp:Field   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('mfp:Field_'&mfp:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(mfp:Field,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::mfp:Description   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('mfp:Description_'&mfp:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(mfp:Description,'@s60')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(MANFAUPA)
  p_web._OpenFile(MODELCCT)
  p_web._OpenFile(MANFAULO_ALIAS)
  p_web._OpenFile(MANFPARL)
  p_web._OpenFile(MANFAUPA_ALIAS)
  p_web._OpenFile(MANFPALO_ALIAS)
  p_web._OpenFile(MANFAURL)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MANFAUPA)
  p_Web._CloseFile(MODELCCT)
  p_Web._CloseFile(MANFAULO_ALIAS)
  p_Web._CloseFile(MANFPARL)
  p_Web._CloseFile(MANFAUPA_ALIAS)
  p_Web._CloseFile(MANFPALO_ALIAS)
  p_Web._CloseFile(MANFAURL)
     FilesOpened = False
  END
  p_web.deleteSessionValue('fieldNumber')
  p_web.deleteSessionValue('partType')
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
  If loc:invalid <> '' then exit. ! no need to check, record is already invalid
  If Duplicate(mfp:Field_Key)
    loc:Invalid = 'mfp:Manufacturer'
    loc:Alert = clip(p_web.site.DuplicateText) & ' Field_Key --> mfp:Manufacturer, mfp:Field_Number, '&clip('Field')&''
  End
PushDefaultSelection  Routine
  loc:default = mfp:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('mfp:RecordNumber',mfp:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('mfp:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('mfp:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('mfp:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
