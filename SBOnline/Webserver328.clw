

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER328.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER010.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER048.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER303.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER331.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER332.INC'),ONCE        !Req'd for module callout resolution
                     END


FormStockCheckCriteria PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locAllManufacturers  LONG                                  !
locManufacturer      STRING(30)                            !
locAllPriceBands     LONG                                  !
locPriceBand         STRING(30)                            !
locIncludeAccessories LONG                                 !
locSuppressZerosOnReport LONG                              !
locShowStockQuantityOnReport LONG                          !
locIncludeSuspendedParts LONG                              !
locShowUsageData     LONG                                  !
locStartDate         DATE                                  !
locEndDate           DATE                                  !
locReportOrder       LONG                                  !
locShelfLocations    LONG                                  !
locAllShelfLocations LONG                                  !
FilesOpened     Long
PRIBAND::State  USHORT
MANUFACT::State  USHORT
TagFile::State  USHORT
LOCSHELF::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormStockCheckCriteria')
  loc:formname = 'FormStockCheckCriteria_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormStockCheckCriteria',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormStockCheckCriteria','')
    p_web._DivHeader('FormStockCheckCriteria',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormStockCheckCriteria',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockCheckCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockCheckCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormStockCheckCriteria',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockCheckCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormStockCheckCriteria',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormStockCheckCriteria',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
TagAll              ROUTINE
    ClearTagFile(p_web)
    Access:LOCSHELF.ClearKey(los:Shelf_Location_Key)
    los:Site_Location = p_web.GSV('BookingSiteLocation')
    SET(los:Shelf_Location_Key,los:Shelf_Location_Key)
    LOOP UNTIL Access:LOCSHELF.Next() <> Level:Benign
        IF (los:Site_Location <> p_web.GSV('BookingSiteLocation'))
            BREAK
        END ! IF
        IF (Access:TagFile.PrimeRecord() = Level:Benign)
            tag:SessionID = p_web.SessionID
            tag:TaggedValue = los:Shelf_Location
            tag:Tagged = 1
            IF (Access:TagFile.TryInsert())
                Access:TagFile.CancelAutoInc()
            END ! IF
        END ! IF	
    END ! LOOP
OpenFiles  ROUTINE
  p_web._OpenFile(PRIBAND)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(TagFile)
  p_web._OpenFile(LOCSHELF)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(PRIBAND)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(LOCSHELF)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormStockCheckCriteria_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('locStartDate')
    p_web.SetPicture('locStartDate','@d06b')
  End
  p_web.SetSessionPicture('locStartDate','@d06b')
  If p_web.IfExistsValue('locEndDate')
    p_web.SetPicture('locEndDate','@d06b')
  End
  p_web.SetSessionPicture('locEndDate','@d06b')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'locManufacturer'
    p_web.setsessionvalue('showtab_FormStockCheckCriteria',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MANUFACT)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locAllPriceBands')
  Of 'locPriceBand'
    p_web.setsessionvalue('showtab_FormStockCheckCriteria',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(PRIBAND)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locIncludeAccessories')
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locAllShelfLocations',locAllShelfLocations)
  p_web.SetSessionValue('locAllManufacturers',locAllManufacturers)
  p_web.SetSessionValue('locManufacturer',locManufacturer)
  p_web.SetSessionValue('locAllPriceBands',locAllPriceBands)
  p_web.SetSessionValue('locPriceBand',locPriceBand)
  p_web.SetSessionValue('locIncludeAccessories',locIncludeAccessories)
  p_web.SetSessionValue('locSuppressZerosOnReport',locSuppressZerosOnReport)
  p_web.SetSessionValue('locShowStockQuantityOnReport',locShowStockQuantityOnReport)
  p_web.SetSessionValue('locIncludeSuspendedParts',locIncludeSuspendedParts)
  p_web.SetSessionValue('locShowUsageData',locShowUsageData)
  p_web.SetSessionValue('locStartDate',locStartDate)
  p_web.SetSessionValue('locEndDate',locEndDate)
  p_web.SetSessionValue('locReportOrder',locReportOrder)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locAllShelfLocations')
    locAllShelfLocations = p_web.GetValue('locAllShelfLocations')
    p_web.SetSessionValue('locAllShelfLocations',locAllShelfLocations)
  End
  if p_web.IfExistsValue('locAllManufacturers')
    locAllManufacturers = p_web.GetValue('locAllManufacturers')
    p_web.SetSessionValue('locAllManufacturers',locAllManufacturers)
  End
  if p_web.IfExistsValue('locManufacturer')
    locManufacturer = p_web.GetValue('locManufacturer')
    p_web.SetSessionValue('locManufacturer',locManufacturer)
  End
  if p_web.IfExistsValue('locAllPriceBands')
    locAllPriceBands = p_web.GetValue('locAllPriceBands')
    p_web.SetSessionValue('locAllPriceBands',locAllPriceBands)
  End
  if p_web.IfExistsValue('locPriceBand')
    locPriceBand = p_web.GetValue('locPriceBand')
    p_web.SetSessionValue('locPriceBand',locPriceBand)
  End
  if p_web.IfExistsValue('locIncludeAccessories')
    locIncludeAccessories = p_web.GetValue('locIncludeAccessories')
    p_web.SetSessionValue('locIncludeAccessories',locIncludeAccessories)
  End
  if p_web.IfExistsValue('locSuppressZerosOnReport')
    locSuppressZerosOnReport = p_web.GetValue('locSuppressZerosOnReport')
    p_web.SetSessionValue('locSuppressZerosOnReport',locSuppressZerosOnReport)
  End
  if p_web.IfExistsValue('locShowStockQuantityOnReport')
    locShowStockQuantityOnReport = p_web.GetValue('locShowStockQuantityOnReport')
    p_web.SetSessionValue('locShowStockQuantityOnReport',locShowStockQuantityOnReport)
  End
  if p_web.IfExistsValue('locIncludeSuspendedParts')
    locIncludeSuspendedParts = p_web.GetValue('locIncludeSuspendedParts')
    p_web.SetSessionValue('locIncludeSuspendedParts',locIncludeSuspendedParts)
  End
  if p_web.IfExistsValue('locShowUsageData')
    locShowUsageData = p_web.GetValue('locShowUsageData')
    p_web.SetSessionValue('locShowUsageData',locShowUsageData)
  End
  if p_web.IfExistsValue('locStartDate')
    locStartDate = p_web.dformat(clip(p_web.GetValue('locStartDate')),'@d06b')
    p_web.SetSessionValue('locStartDate',locStartDate)
  End
  if p_web.IfExistsValue('locEndDate')
    locEndDate = p_web.dformat(clip(p_web.GetValue('locEndDate')),'@d06b')
    p_web.SetSessionValue('locEndDate',locEndDate)
  End
  if p_web.IfExistsValue('locReportOrder')
    locReportOrder = p_web.GetValue('locReportOrder')
    p_web.SetSessionValue('locReportOrder',locReportOrder)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormStockCheckCriteria_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    IF (p_web.GetValue('FirstTime') = 1)
        p_web.SSV('locAllManufacturers',1)
        p_web.SSV('locManufacturer','')
        p_web.SSV('locAllPriceBands',1)
        p_web.SSV('locPriceBand','')
        p_web.SSV('locIncludeAccessories',0)
        p_web.SSV('locSuppressZerosOnReport',1)
        p_web.SSV('locShowStockQuantityOnReport',1)
        p_web.SSV('locIncludeSuspendedParts',1)
        p_web.SSV('locShowUsageData',1)
        p_web.SSV('locStartDate',DEFORMAT('01/01/1990',@d06))
        p_web.SSV('locEndDate',TODAY())
        p_web.SSV('locReportOrder',0)
        p_web.SSV('locAllShelfLocations',1)
    END ! IF
    IF (p_web.IfExistsValue('NoAudit'))
        p_web.StoreValue('NoAudit')
    END ! IF
    p_web.SSV('TagType',kShelfLocation)
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locAllShelfLocations = p_web.RestoreValue('locAllShelfLocations')
 locAllManufacturers = p_web.RestoreValue('locAllManufacturers')
 locManufacturer = p_web.RestoreValue('locManufacturer')
 locAllPriceBands = p_web.RestoreValue('locAllPriceBands')
 locPriceBand = p_web.RestoreValue('locPriceBand')
 locIncludeAccessories = p_web.RestoreValue('locIncludeAccessories')
 locSuppressZerosOnReport = p_web.RestoreValue('locSuppressZerosOnReport')
 locShowStockQuantityOnReport = p_web.RestoreValue('locShowStockQuantityOnReport')
 locIncludeSuspendedParts = p_web.RestoreValue('locIncludeSuspendedParts')
 locShowUsageData = p_web.RestoreValue('locShowUsageData')
 locStartDate = p_web.RestoreValue('locStartDate')
 locEndDate = p_web.RestoreValue('locEndDate')
 locReportOrder = p_web.RestoreValue('locReportOrder')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormStockCheckCriteria')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormStockCheckCriteria_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormStockCheckCriteria_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormStockCheckCriteria_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'FormStockAudits'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormStockCheckCriteria" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormStockCheckCriteria" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormStockCheckCriteria" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Stock Check Criteria') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Stock Check Criteria',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormStockCheckCriteria">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormStockCheckCriteria" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormStockCheckCriteria')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Criteria') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormStockCheckCriteria')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormStockCheckCriteria'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormStockCheckCriteria_TagShelfLocations_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='MANUFACT'
          If Not (p_web.GSV('locAllPriceBands') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.locPriceBand')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='PRIBAND'
            p_web.SetValue('SelectField',clip(loc:formname) & '.locStartDate')
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locAllShelfLocations')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormStockCheckCriteria')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Criteria') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormStockCheckCriteria_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAllShelfLocations
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAllShelfLocations
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAllShelfLocations
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('locAllShelfLocations') = 0
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwTagShelfLocations
      do Comment::brwTagShelfLocations
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('locAllShelfLocations') = 0
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnUnTagAll
      do Comment::btnUnTagAll
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAllManufacturers
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAllManufacturers
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAllManufacturers
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locManufacturer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAllPriceBands
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAllPriceBands
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAllPriceBands
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPriceBand
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPriceBand
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locPriceBand
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIncludeAccessories
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIncludeAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locIncludeAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSuppressZerosOnReport
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSuppressZerosOnReport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locSuppressZerosOnReport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locShowStockQuantityOnReport
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locShowStockQuantityOnReport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locShowStockQuantityOnReport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIncludeSuspendedParts
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIncludeSuspendedParts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locIncludeSuspendedParts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__line
      do Comment::__line
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locShowUsageData
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locShowUsageData
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locShowUsageData
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locStartDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locStartDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locStartDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEndDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEndDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEndDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locReportOrder
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locReportOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locReportOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnPrintStockCheckReport
      do Comment::btnPrintStockCheckReport
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnExportStockCheckReport
      do Comment::btnExportStockCheckReport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('NoAudit') = 0
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnCreateNewAudit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnCreateNewAudit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locAllShelfLocations  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locAllShelfLocations') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('All Shelf Locations')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAllShelfLocations  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAllShelfLocations',p_web.GetValue('NewValue'))
    locAllShelfLocations = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAllShelfLocations
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locAllShelfLocations',p_web.GetValue('Value'))
    locAllShelfLocations = p_web.GetValue('Value')
  End
  do Value::locAllShelfLocations
  do SendAlert

Value::locAllShelfLocations  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locAllShelfLocations') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locAllShelfLocations
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('window.open(''FormStockCheckCriteria'',''_self'')')&';'
  loc:javascript = clip(loc:javascript) & ' ' &p_web._nocolon('sv(''locAllShelfLocations'',''formstockcheckcriteria_locallshelflocations_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locAllShelfLocations')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locAllShelfLocations') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locAllShelfLocations',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockCheckCriteria_' & p_web._nocolon('locAllShelfLocations') & '_value')

Comment::locAllShelfLocations  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locAllShelfLocations') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::brwTagShelfLocations  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwTagShelfLocations',p_web.GetValue('NewValue'))
    do Value::brwTagShelfLocations
  Else
    p_web.StoreValue('los:RecordNumber')
  End

Value::brwTagShelfLocations  Routine
  loc:extra = ''
  ! --- BROWSE ---  TagShelfLocations --
  p_web.SetValue('TagShelfLocations:NoForm',1)
  p_web.SetValue('TagShelfLocations:FormName',loc:formname)
  p_web.SetValue('TagShelfLocations:parentIs','Form')
  p_web.SetValue('_parentProc','FormStockCheckCriteria')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormStockCheckCriteria_TagShelfLocations_embedded_div')&'"><!-- Net:TagShelfLocations --></div><13,10>'
    p_web._DivHeader('FormStockCheckCriteria_' & lower('TagShelfLocations') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormStockCheckCriteria_' & lower('TagShelfLocations') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagShelfLocations --><13,10>'
  end
  do SendPacket

Comment::brwTagShelfLocations  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('brwTagShelfLocations') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnUnTagAll  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnUnTagAll',p_web.GetValue('NewValue'))
    do Value::btnUnTagAll
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    ClearGenericTagFile(p_web,kShelfLocation)
  do SendAlert
  do Value::brwTagShelfLocations  !1

Value::btnUnTagAll  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('btnUnTagAll') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnUnTagAll'',''formstockcheckcriteria_btnuntagall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnUnTagAll','UnTag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockCheckCriteria_' & p_web._nocolon('btnUnTagAll') & '_value')

Comment::btnUnTagAll  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('btnUnTagAll') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAllManufacturers  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locAllManufacturers') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Manufacturers')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAllManufacturers  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAllManufacturers',p_web.GetValue('NewValue'))
    locAllManufacturers = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAllManufacturers
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAllManufacturers',p_web.GetValue('Value'))
    locAllManufacturers = p_web.GetValue('Value')
  End
  do Value::locAllManufacturers
  do SendAlert
  do Prompt::locManufacturer
  do Value::locManufacturer  !1

Value::locAllManufacturers  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locAllManufacturers') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locAllManufacturers
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locAllManufacturers')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locAllManufacturers') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locAllManufacturers'',''formstockcheckcriteria_locallmanufacturers_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locAllManufacturers')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locAllManufacturers',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locAllManufacturers_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('All') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locAllManufacturers') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locAllManufacturers'',''formstockcheckcriteria_locallmanufacturers_value'',1,'''&clip(0)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locAllManufacturers')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locAllManufacturers',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locAllManufacturers_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Select') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockCheckCriteria_' & p_web._nocolon('locAllManufacturers') & '_value')

Comment::locAllManufacturers  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locAllManufacturers') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locManufacturer  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locManufacturer') & '_prompt',Choose(p_web.GSV('locAllManufacturers') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Manufacturer')
  If p_web.GSV('locAllManufacturers') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockCheckCriteria_' & p_web._nocolon('locManufacturer') & '_prompt')

Validate::locManufacturer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locManufacturer',p_web.GetValue('NewValue'))
    locManufacturer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locManufacturer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locManufacturer',p_web.GetValue('Value'))
    locManufacturer = p_web.GetValue('Value')
  End
    locManufacturer = Upper(locManufacturer)
    p_web.SetSessionValue('locManufacturer',locManufacturer)
  p_Web.SetValue('lookupfield','locManufacturer')
  do AfterLookup
  do Value::locManufacturer
  do SendAlert
  do Comment::locManufacturer

Value::locManufacturer  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locManufacturer') & '_value',Choose(p_web.GSV('locAllManufacturers') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllManufacturers') = 1)
  ! --- STRING --- locManufacturer
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locManufacturer')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locManufacturer'',''formstockcheckcriteria_locmanufacturer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locManufacturer')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','locManufacturer',p_web.GetSessionValueFormat('locManufacturer'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('SelectManufacturers?LookupField=locManufacturer&Tab=0&ForeignField=man:Manufacturer&_sort=man:Manufacturer&Refresh=sort&LookupFrom=FormStockCheckCriteria&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockCheckCriteria_' & p_web._nocolon('locManufacturer') & '_value')

Comment::locManufacturer  Routine
      loc:comment = ''
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locManufacturer') & '_comment',Choose(p_web.GSV('locAllManufacturers') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAllManufacturers') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockCheckCriteria_' & p_web._nocolon('locManufacturer') & '_comment')

Prompt::locAllPriceBands  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locAllPriceBands') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Price Band')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAllPriceBands  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAllPriceBands',p_web.GetValue('NewValue'))
    locAllPriceBands = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAllPriceBands
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAllPriceBands',p_web.GetValue('Value'))
    locAllPriceBands = p_web.GetValue('Value')
  End
  do Value::locAllPriceBands
  do SendAlert
  do Prompt::locPriceBand
  do Value::locPriceBand  !1

Value::locAllPriceBands  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locAllPriceBands') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locAllPriceBands
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locAllPriceBands')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locAllPriceBands') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locAllPriceBands'',''formstockcheckcriteria_locallpricebands_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locAllPriceBands')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locAllPriceBands',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locAllPriceBands_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('All') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locAllPriceBands') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locAllPriceBands'',''formstockcheckcriteria_locallpricebands_value'',1,'''&clip(0)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locAllPriceBands')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locAllPriceBands',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locAllPriceBands_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Select') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockCheckCriteria_' & p_web._nocolon('locAllPriceBands') & '_value')

Comment::locAllPriceBands  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locAllPriceBands') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locPriceBand  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locPriceBand') & '_prompt',Choose(p_web.GSV('locAllPriceBands') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Price Band')
  If p_web.GSV('locAllPriceBands') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockCheckCriteria_' & p_web._nocolon('locPriceBand') & '_prompt')

Validate::locPriceBand  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPriceBand',p_web.GetValue('NewValue'))
    locPriceBand = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPriceBand
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPriceBand',p_web.GetValue('Value'))
    locPriceBand = p_web.GetValue('Value')
  End
    locPriceBand = Upper(locPriceBand)
    p_web.SetSessionValue('locPriceBand',locPriceBand)
  p_Web.SetValue('lookupfield','locPriceBand')
  do AfterLookup
  do Value::locPriceBand
  do SendAlert
  do Comment::locPriceBand

Value::locPriceBand  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locPriceBand') & '_value',Choose(p_web.GSV('locAllPriceBands') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllPriceBands') = 1)
  ! --- STRING --- locPriceBand
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locPriceBand')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPriceBand'',''formstockcheckcriteria_locpriceband_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPriceBand')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','locPriceBand',p_web.GetSessionValueFormat('locPriceBand'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('SelectPriceBand?LookupField=locPriceBand&Tab=0&ForeignField=prb:BandName&_sort=prb:BandName&Refresh=sort&LookupFrom=FormStockCheckCriteria&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockCheckCriteria_' & p_web._nocolon('locPriceBand') & '_value')

Comment::locPriceBand  Routine
      loc:comment = ''
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locPriceBand') & '_comment',Choose(p_web.GSV('locAllPriceBands') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAllPriceBands') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockCheckCriteria_' & p_web._nocolon('locPriceBand') & '_comment')

Prompt::locIncludeAccessories  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locIncludeAccessories') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Include Accessories')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIncludeAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIncludeAccessories',p_web.GetValue('NewValue'))
    locIncludeAccessories = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIncludeAccessories
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locIncludeAccessories',p_web.GetValue('Value'))
    locIncludeAccessories = p_web.GetValue('Value')
  End
  do Value::locIncludeAccessories
  do SendAlert

Value::locIncludeAccessories  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locIncludeAccessories') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locIncludeAccessories
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locIncludeAccessories'',''formstockcheckcriteria_locincludeaccessories_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locIncludeAccessories') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locIncludeAccessories',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockCheckCriteria_' & p_web._nocolon('locIncludeAccessories') & '_value')

Comment::locIncludeAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locIncludeAccessories') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locSuppressZerosOnReport  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locSuppressZerosOnReport') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Suppress Zeros On Report')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locSuppressZerosOnReport  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSuppressZerosOnReport',p_web.GetValue('NewValue'))
    locSuppressZerosOnReport = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSuppressZerosOnReport
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locSuppressZerosOnReport',p_web.GetValue('Value'))
    locSuppressZerosOnReport = p_web.GetValue('Value')
  End
  do Value::locSuppressZerosOnReport
  do SendAlert

Value::locSuppressZerosOnReport  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locSuppressZerosOnReport') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locSuppressZerosOnReport
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSuppressZerosOnReport'',''formstockcheckcriteria_locsuppresszerosonreport_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locSuppressZerosOnReport') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locSuppressZerosOnReport',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockCheckCriteria_' & p_web._nocolon('locSuppressZerosOnReport') & '_value')

Comment::locSuppressZerosOnReport  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locSuppressZerosOnReport') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locShowStockQuantityOnReport  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locShowStockQuantityOnReport') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Show Stock Quantity On Report')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locShowStockQuantityOnReport  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locShowStockQuantityOnReport',p_web.GetValue('NewValue'))
    locShowStockQuantityOnReport = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locShowStockQuantityOnReport
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locShowStockQuantityOnReport',p_web.GetValue('Value'))
    locShowStockQuantityOnReport = p_web.GetValue('Value')
  End
  do Value::locShowStockQuantityOnReport
  do SendAlert

Value::locShowStockQuantityOnReport  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locShowStockQuantityOnReport') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locShowStockQuantityOnReport
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locShowStockQuantityOnReport'',''formstockcheckcriteria_locshowstockquantityonreport_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locShowStockQuantityOnReport') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locShowStockQuantityOnReport',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockCheckCriteria_' & p_web._nocolon('locShowStockQuantityOnReport') & '_value')

Comment::locShowStockQuantityOnReport  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locShowStockQuantityOnReport') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locIncludeSuspendedParts  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locIncludeSuspendedParts') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Include Suspended Parts')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIncludeSuspendedParts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIncludeSuspendedParts',p_web.GetValue('NewValue'))
    locIncludeSuspendedParts = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIncludeSuspendedParts
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locIncludeSuspendedParts',p_web.GetValue('Value'))
    locIncludeSuspendedParts = p_web.GetValue('Value')
  End
  do Value::locIncludeSuspendedParts
  do SendAlert

Value::locIncludeSuspendedParts  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locIncludeSuspendedParts') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locIncludeSuspendedParts
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locIncludeSuspendedParts'',''formstockcheckcriteria_locincludesuspendedparts_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locIncludeSuspendedParts') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locIncludeSuspendedParts',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockCheckCriteria_' & p_web._nocolon('locIncludeSuspendedParts') & '_value')

Comment::locIncludeSuspendedParts  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locIncludeSuspendedParts') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::__line  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__line',p_web.GetValue('NewValue'))
    do Value::__line
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__line  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('__line') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::__line  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('__line') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locShowUsageData  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locShowUsageData') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Show Usage Data')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locShowUsageData  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locShowUsageData',p_web.GetValue('NewValue'))
    locShowUsageData = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locShowUsageData
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locShowUsageData',p_web.GetValue('Value'))
    locShowUsageData = p_web.GetValue('Value')
  End
  do Value::locShowUsageData
  do SendAlert

Value::locShowUsageData  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locShowUsageData') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locShowUsageData
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locShowUsageData'',''formstockcheckcriteria_locshowusagedata_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locShowUsageData') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locShowUsageData',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockCheckCriteria_' & p_web._nocolon('locShowUsageData') & '_value')

Comment::locShowUsageData  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locShowUsageData') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locStartDate  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locStartDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Start Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locStartDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locStartDate',p_web.GetValue('NewValue'))
    locStartDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locStartDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locStartDate',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    locStartDate = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  do Value::locStartDate
  do SendAlert

Value::locStartDate  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locStartDate') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locStartDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locStartDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locStartDate'',''formstockcheckcriteria_locstartdate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locStartDate',p_web.GetSessionValue('locStartDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
   packet = CLIP(packet) & |
     '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar' & |
     '(locStartDate,''dd/mm/yyyy'',this); Date.disabled=true;' & |
     'sv(''...'',''FormStockCheckCriteria_pickdate_value'',1,FieldValue(this,1));nextFocus(FormStockCheckCriteria_frm,'''',0);" ' & |
     'value="Select Date" name="Date" type="button">...</button>'
   DO SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockCheckCriteria_' & p_web._nocolon('locStartDate') & '_value')

Comment::locStartDate  Routine
      loc:comment = ''
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locStartDate') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locEndDate  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locEndDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('End Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locEndDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEndDate',p_web.GetValue('NewValue'))
    locEndDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEndDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEndDate',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    locEndDate = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  do Value::locEndDate
  do SendAlert

Value::locEndDate  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locEndDate') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locEndDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locEndDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEndDate'',''formstockcheckcriteria_locenddate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locEndDate',p_web.GetSessionValue('locEndDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
   packet = CLIP(packet) & |
     '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar' & |
     '(locEndDate,''dd/mm/yyyy'',this); Date.disabled=true;' & |
     'sv(''...'',''FormStockCheckCriteria_pickdate_value'',1,FieldValue(this,1));nextFocus(FormStockCheckCriteria_frm,'''',0);" ' & |
     'value="Select Date" name="Date" type="button">...</button>'
   DO SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockCheckCriteria_' & p_web._nocolon('locEndDate') & '_value')

Comment::locEndDate  Routine
      loc:comment = ''
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locEndDate') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locReportOrder  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locReportOrder') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Report Order')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locReportOrder  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locReportOrder',p_web.GetValue('NewValue'))
    locReportOrder = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locReportOrder
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locReportOrder',p_web.GetValue('Value'))
    locReportOrder = p_web.GetValue('Value')
  End
  do Value::locReportOrder
  do SendAlert

Value::locReportOrder  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locReportOrder') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locReportOrder
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locReportOrder')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locReportOrder') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locReportOrder'',''formstockcheckcriteria_locreportorder_value'',1,'''&clip(0)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locReportOrder',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locReportOrder_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('By Location') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locReportOrder') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locReportOrder'',''formstockcheckcriteria_locreportorder_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locReportOrder',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locReportOrder_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('By Description') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockCheckCriteria_' & p_web._nocolon('locReportOrder') & '_value')

Comment::locReportOrder  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('locReportOrder') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnPrintStockCheckReport  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnPrintStockCheckReport',p_web.GetValue('NewValue'))
    do Value::btnPrintStockCheckReport
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnPrintStockCheckReport  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('btnPrintStockCheckReport') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnPrintStockCheckReport','Print Stock Check Report','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('StockCheckReport')) & ''','''&clip('_blank')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()

Comment::btnPrintStockCheckReport  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('btnPrintStockCheckReport') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnExportStockCheckReport  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnExportStockCheckReport',p_web.GetValue('NewValue'))
    do Value::btnExportStockCheckReport
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnExportStockCheckReport  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('btnExportStockCheckReport') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnExportStockCheckReport','Stock Check Export','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=StockCheckReport&RedirectURL=FileDownload')) & ''','''&clip('_blank')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()

Comment::btnExportStockCheckReport  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('btnExportStockCheckReport') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnCreateNewAudit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnCreateNewAudit',p_web.GetValue('NewValue'))
    do Value::btnCreateNewAudit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnCreateNewAudit  Routine
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('btnCreateNewAudit') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('stockNewAuditType()')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnCreateNewAudit','Create New Audit','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()

Comment::btnCreateNewAudit  Routine
    loc:comment = ''
  p_web._DivHeader('FormStockCheckCriteria_' & p_web._nocolon('btnCreateNewAudit') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormStockCheckCriteria_locAllShelfLocations_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAllShelfLocations
      else
        do Value::locAllShelfLocations
      end
  of lower('FormStockCheckCriteria_btnUnTagAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnUnTagAll
      else
        do Value::btnUnTagAll
      end
  of lower('FormStockCheckCriteria_locAllManufacturers_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAllManufacturers
      else
        do Value::locAllManufacturers
      end
  of lower('FormStockCheckCriteria_locManufacturer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locManufacturer
      else
        do Value::locManufacturer
      end
  of lower('FormStockCheckCriteria_locAllPriceBands_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAllPriceBands
      else
        do Value::locAllPriceBands
      end
  of lower('FormStockCheckCriteria_locPriceBand_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPriceBand
      else
        do Value::locPriceBand
      end
  of lower('FormStockCheckCriteria_locIncludeAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIncludeAccessories
      else
        do Value::locIncludeAccessories
      end
  of lower('FormStockCheckCriteria_locSuppressZerosOnReport_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSuppressZerosOnReport
      else
        do Value::locSuppressZerosOnReport
      end
  of lower('FormStockCheckCriteria_locShowStockQuantityOnReport_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locShowStockQuantityOnReport
      else
        do Value::locShowStockQuantityOnReport
      end
  of lower('FormStockCheckCriteria_locIncludeSuspendedParts_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIncludeSuspendedParts
      else
        do Value::locIncludeSuspendedParts
      end
  of lower('FormStockCheckCriteria_locShowUsageData_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locShowUsageData
      else
        do Value::locShowUsageData
      end
  of lower('FormStockCheckCriteria_locStartDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locStartDate
      else
        do Value::locStartDate
      end
  of lower('FormStockCheckCriteria_locEndDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEndDate
      else
        do Value::locEndDate
      end
  of lower('FormStockCheckCriteria_locReportOrder_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locReportOrder
      else
        do Value::locReportOrder
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormStockCheckCriteria_form:ready_',1)
  p_web.SetSessionValue('FormStockCheckCriteria_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormStockCheckCriteria',0)

PreCopy  Routine
  p_web.SetValue('FormStockCheckCriteria_form:ready_',1)
  p_web.SetSessionValue('FormStockCheckCriteria_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormStockCheckCriteria',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormStockCheckCriteria_form:ready_',1)
  p_web.SetSessionValue('FormStockCheckCriteria_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormStockCheckCriteria:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormStockCheckCriteria_form:ready_',1)
  p_web.SetSessionValue('FormStockCheckCriteria_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormStockCheckCriteria:Primed',0)
  p_web.setsessionvalue('showtab_FormStockCheckCriteria',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
          If p_web.IfExistsValue('locAllShelfLocations') = 0
            p_web.SetValue('locAllShelfLocations',0)
            locAllShelfLocations = 0
          End
          If p_web.IfExistsValue('locIncludeAccessories') = 0
            p_web.SetValue('locIncludeAccessories',0)
            locIncludeAccessories = 0
          End
          If p_web.IfExistsValue('locSuppressZerosOnReport') = 0
            p_web.SetValue('locSuppressZerosOnReport',0)
            locSuppressZerosOnReport = 0
          End
          If p_web.IfExistsValue('locShowStockQuantityOnReport') = 0
            p_web.SetValue('locShowStockQuantityOnReport',0)
            locShowStockQuantityOnReport = 0
          End
          If p_web.IfExistsValue('locIncludeSuspendedParts') = 0
            p_web.SetValue('locIncludeSuspendedParts',0)
            locIncludeSuspendedParts = 0
          End
          If p_web.IfExistsValue('locShowUsageData') = 0
            p_web.SetValue('locShowUsageData',0)
            locShowUsageData = 0
          End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormStockCheckCriteria_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormStockCheckCriteria_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
      If not (p_web.GSV('locAllManufacturers') = 1)
          locManufacturer = Upper(locManufacturer)
          p_web.SetSessionValue('locManufacturer',locManufacturer)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('locAllPriceBands') = 1)
          locPriceBand = Upper(locPriceBand)
          p_web.SetSessionValue('locPriceBand',locPriceBand)
        If loc:Invalid <> '' then exit.
      End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormStockCheckCriteria:Primed',0)
  p_web.StoreValue('locAllShelfLocations')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locAllManufacturers')
  p_web.StoreValue('locManufacturer')
  p_web.StoreValue('locAllPriceBands')
  p_web.StoreValue('locPriceBand')
  p_web.StoreValue('locIncludeAccessories')
  p_web.StoreValue('locSuppressZerosOnReport')
  p_web.StoreValue('locShowStockQuantityOnReport')
  p_web.StoreValue('locIncludeSuspendedParts')
  p_web.StoreValue('')
  p_web.StoreValue('locShowUsageData')
  p_web.StoreValue('locStartDate')
  p_web.StoreValue('locEndDate')
  p_web.StoreValue('locReportOrder')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
