

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER495.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
GetTheMainOutFault   PROCEDURE  (LONG jobNumber,*STRING outFault,*STRING description) ! Declare Procedure

  CODE
    vod.GetMainOutFault(jobNumber,outFault,description)
