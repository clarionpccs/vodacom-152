

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER147.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER029.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER120.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER127.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER134.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER148.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER150.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER151.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER154.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER156.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER521.INC'),ONCE        !Req'd for module callout resolution
                     END


PickLoanUnit         PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:LoanUnitNumber   LONG                                  !
tmp:UnitDetails      STRING(100)                           !
tmp:LoanIMEINumber   STRING(30)                            !
tmp:LoanModelNumber  STRING(30)                            !
tmp:MSN              STRING(30)                            !
tmp:LoanUnitDetails  STRING(60)                            !
tmp:LoanAccessories  STRING(100)                           !
tmp:LoanLocation     STRING(100)                           !
tmp:ReplacementValue REAL                                  !
locLoanAlertMessage  STRING(255)                           !
tmp:RemovalReason    BYTE                                  !
locRemovalAlertMessage STRING(255)                         !
tmp:LostLoanFee      REAL                                  !
locValidateMessage   STRING(255)                           !
locLoanIDOption      BYTE                                  !
locLoanIDAlert       STRING(100)                           !
locSMSDate           STRING(20)                            !
FilesOpened     Long
Tagging::State  USHORT
JOBSE2::State  USHORT
COURIER::State  USHORT
JOBS::State  USHORT
LOAN::State  USHORT
JOBEXACC::State  USHORT
LOAN_ALIAS::State  USHORT
EXCHOR48::State  USHORT
MANUFACT::State  USHORT
PARTS::State  USHORT
MODELNUM::State  USHORT
STOCK::State  USHORT
WARPARTS::State  USHORT
STOCKTYP::State  USHORT
EXCHHIST::State  USHORT
PRODCODE::State  USHORT
JOBSE::State  USHORT
LOANACC::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
job:Loan_Courier_OptionView   View(COURIER)
                          Project(cou:Courier)
                        End
local       class
AllocateLoanPart    Procedure(String func:Status,Byte func:SecondUnit)
            end
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('PickLoanUnit')
  loc:formname = 'PickLoanUnit_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'PickLoanUnit',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('PickLoanUnit','')
    p_web._DivHeader('PickLoanUnit',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferPickLoanUnit',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickLoanUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickLoanUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PickLoanUnit',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPickLoanUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_PickLoanUnit',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'PickLoanUnit',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
addLoanUnit         Routine
    data
locAuditNotes   String(255)
    code
        Access:Loan.Clearkey(loa:Ref_Number_Key)
        loa:Ref_Number    = p_web.GSV('tmp:LoanUnitNumber')
        if (Access:Loan.TryFetch(loa:Ref_Number_Key) = Level:Benign)
            ! Found
            Access:MANUFACT.Clearkey(man:Manufacturer_Key)
            man:Manufacturer    = p_web.GSV('job:Manufacturer')
            if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                ! Found
                if (man:QALoan)
                    loa:Available = 'QA1'
                else ! if (man:QALoanLoan)
                    loa:Available = 'EXC'
                end !if (man:QALoanLoan)
                loa:Job_Number = p_web.GSV('job:Ref_Number')
                access:Loan.tryUpdate()

                if (Access:loanhist.PrimeRecord() = Level:Benign)
                    loh:Ref_Number    = tmp:LoanUnitNumber
                    loh:Date    = Today()
                    loh:Time    = Clock()
                    loh:User    = p_web.GSV('BookingUserCode')
                    if (man:QALoan)
                        loh:Status    = 'AWAITING QA. LOANED ON JOB NO: ' & p_web.GSV('job:ref_number')
                    else ! if (man:QALoanLoan)
                        loh:status = 'UNIT LOANED ON JOB NO: ' & p_web.GSV('job:Ref_number')
                    end !

                    if (Access:loanhist.TryInsert() = Level:Benign)
                        ! Inserted
                    else ! if (Access:loanhist.TryInsert() = Level:Benign)
                        ! Error
                        Access:loanhist.CancelAutoInc()
                    end ! if (Access:loanhist.TryInsert() = Level:Benign)
                end ! if (Access:loanhist.PrimeRecord() = Level:Benign)

                locAuditNotes = 'UNIT NUMBER: ' & CLip(loa:ref_number) & |
                    '<13,10>MODEL NUMBER: ' & CLip(loa:model_number) & |
                    '<13,10>I.M.E.I.: ' & CLip(loa:esn)

                if (MSNRequired(loa:Manufacturer))
                    locAuditNotes = Clip(locAuditNotes) & '<13,10>M.S.N.: ' & Clip(loa:MSN)
                end ! if (MSNRequired(loa:Manufacturer))

                locAuditNotes = clip(locAuditNotes) & '<13,10>STOCK TYPE: ' & Clip(loa:Stock_Type)

!                p_web.SSV('AddToAudit:Type','LOA')
!                p_web.SSV('AddToAudit:Action','LOAN UNIT ATTACHED TO JOB')
!                p_web.SSV('AddToAudit:Notes',clip(locAuditNotes))
                addToAudit(p_web,p_web.GSV('job:ref_number'),'LOA','LOAN UNIT ATTACHED TO JOB',clip(locAuditNotes))

                if (man:QALoan)
                    p_web.SSV('job:Despatched','')
                    p_web.SSV('job:DespatchType','')
                    p_web.SSV('GetStatus:StatusNumber',605)
                    p_web.SSV('GetStatus:Type','LOA')
                    getStatus(605,0,'LOA',p_web)
                else ! if (man:QALoanLoan)

                    if (p_web.GSV('BookingSite') = 'RRC')
                        p_web.SSV('jobe:DespatchType','LOA')
                        p_web.SSV('jobe:Despatched','REA')
                        p_web.SSV('wob:ReadyToDespatch',1)
                        p_web.SSV('wob:DespatchCourier',p_web.GSV('job:Loan_Courier'))

                    else ! if (p_web.GSV('BookingSite') = 'RRC')
                        p_web.SSV('job:Loan_User',p_web.GSV('BookingUserCode'))
                        p_web.SSV('job:Loan_Despatched','')
                        p_web.SSV('job:Despatched','LAT')
                        p_web.SSV('job:DespatchType','LOA')
                    end ! if (p_web.GSV('BookingSite') = 'RRC')
                    p_web.SSV('GetStatus:StatusNumber',105)
                    p_web.SSV('GetStatus:Type','LOA')
                    getStatus(105,0,'LOA',p_web)

                end !if (man:QALoanLoan)

                p_web.SSV('job:Loan_unit_Number',p_web.GSV('tmp:LoanUnitNumber'))

                !               saveJob(p_web) 
                ! #13379 Is this commented out for a reason?
                ! Is seems to be necessary (DBH: 12/09/2014)
                saveJob(p_web)

                loc:Alert = 'Loan Unit Added'

            else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                ! Error
            end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
        else ! if (Access:Loan.TryFetch(loa:Ref_Number_Key) = Level:Benign)
            ! Error
        end ! if (Access:Loan.TryFetch(loa:Ref_Number_Key) = Level:Benign)
clearLoanDetails        routine
    p_web.SSV('tmp:MSN','')
    p_web.SSV('tmp:LoanUnitDetails','')
    p_web.SSV('tmp:LoanLocation','')
    p_web.SSV('tmp:ReplacementValue','')
    p_web.SSV('tmp:LoanIMEINumber','')
    p_web.SSV('tmp:LoanUnitNumber','')
    p_web.SSV('tmp:LoanModelNumber','')
    p_web.SSV('tmp:LoanAccessories','')
    p_web.SSV('tmp:LoanLocation','')


clearTagFile        routine

    ClearTaggingFile(p_web)
deleteVariables     routine
    p_web.deleteSessionValue('PickLoan:FirstTime')
    p_web.deleteSessionValue('locLoanIDAlert')
    p_web.deleteSessionValue('locLoanIDOption')
    p_web.DeleteSessionValue('Hide:LoadIDAlert')
    p_web.DeleteSessionValue('ReadOnly:LoanDespatchDetails')
getLoanDetails      Routine
    p_web.SSV('locLoanAlertMessage','')

    do lookupLoanDetails


    Access:LOAN.Clearkey(loa:Ref_Number_Key)
    loa:Ref_Number    = p_web.GSV('tmp:LoanUnitNumber')
    if (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
        ! Found
    else ! if (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)

    if (loa:Model_Number <> p_web.GSV('job:Model_Number'))
        p_web.SSV('locLoanAlertMessage','Warning! The selected Loan Unit has a different Model Number!')
    end ! if (xch:Model_Number <> p_web.GSV('job:Model_Number')
    
    Access:MODELNUM.ClearKey(mod:Model_Number_Key)
    mod:Model_Number = loa:Model_Number
    IF (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
        p_web.SSV('jobe:LoanReplacementValue',mod:LoanReplacementValue)
!        Access:JOBSE.Clearkey(jobe:RefNumberKey)
!        jobe:RefNumber    = p_web.GSV('job:Ref_Number')
!        if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
!          ! Found
!            p_web.SessionQueueToFile(JOBSE)
!            access:JOBSE.tryUpdate()
!        else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
!          ! Error
!        end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)            
            
    ELSE ! IF
    END ! IF

lookupLoanDetails       Routine
    Access:Loan.Clearkey(loa:Ref_Number_Key)
    loa:Ref_Number    = p_web.GSV('tmp:LoanUnitNumber')
    if (Access:Loan.TryFetch(loa:Ref_Number_Key) = Level:Benign and loa:Ref_Number > 0)
        ! Found
        p_web.SSV('tmp:LoanIMEINumber',loa:ESN)
        p_web.SSV('tmp:MSN',loa:MSN)
        p_web.SSV('tmp:LoanUnitDetails',Clip(loa:Ref_Number) & ': ' & Clip(loa:Manufacturer) & ' ' & Clip(loa:Model_Number))
        p_web.SSV('tmp:LoanLocation',Clip(loa:Location) & ' / ' & Clip(loa:Stock_Type))
        p_web.SSV('tmp:LoanModelNumber',loa:Model_Number)
        
        count# = 0        
        p_web.SSV('tmp:LoanAccessories','')
        Access:LOANACC.ClearKey(lac:Ref_Number_Key)
        lac:Ref_Number = loa:Ref_Number 
        SET(lac:Ref_Number_Key,lac:Ref_Number_Key)
        LOOP UNTIL Access:LOANACC.Next() <> Level:Benign
            IF (lac:Ref_Number <> loa:Ref_Number)
                BREAK
            END ! IF
            p_web.SSV('tmp:LoanAccessories',lac:Accessory)
            count# += 1
        END ! LOOP        
        IF (count# > 1)
            p_web.SSV('tmp:LoanAccessories',count#)
        END ! IF
        

        
        
        p_web.FileToSessionQueue(LOAN)
        
        

    else ! if (Access:Loan.TryFetch(loa:Ref_Number_Key) = Level:Benign)
        ! Error
        p_web.SSV('tmp:LoanIMEINumber','')
        p_web.SSV('tmp:MSN','')
        p_web.SSV('tmp:LoanUnitDetails','')
        p_web.SSV('tmp:LoanLocation','')
        p_web.SSV('tmp:LoanAccessories','')
    end ! if (Access:Loan.TryFetch(loa:Ref_Number_Key) = Level:Benign)
OpenFiles  ROUTINE
  p_web._OpenFile(Tagging)
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(JOBEXACC)
  p_web._OpenFile(LOAN_ALIAS)
  p_web._OpenFile(EXCHOR48)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(MODELNUM)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(STOCKTYP)
  p_web._OpenFile(EXCHHIST)
  p_web._OpenFile(PRODCODE)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(LOANACC)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(Tagging)
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(JOBEXACC)
  p_Web._CloseFile(LOAN_ALIAS)
  p_Web._CloseFile(EXCHOR48)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(PRODCODE)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(LOANACC)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('PickLoanUnit_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  do deleteVariables

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('jobe:LoanReplacementValue')
    p_web.SetPicture('jobe:LoanReplacementValue','@n14.2')
  End
  p_web.SetSessionPicture('jobe:LoanReplacementValue','@n14.2')
  If p_web.IfExistsValue('job:Loan_Despatched')
    p_web.SetPicture('job:Loan_Despatched','@d06b')
  End
  p_web.SetSessionPicture('job:Loan_Despatched','@d06b')
  If p_web.IfExistsValue('tmp:LostLoanFee')
    p_web.SetPicture('tmp:LostLoanFee','@n14.2')
  End
  p_web.SetSessionPicture('tmp:LostLoanFee','@n14.2')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:LoanIMEINumber'
    p_web.setsessionvalue('showtab_PickLoanUnit',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(LOAN)
        p_web.setsessionvalue('tmp:LoanUnitNumber',loa:Ref_Number)
      do getLoanDetails
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locLoanAlertMessage')
  End
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:Loan_Courier'
    p_web.setsessionvalue('showtab_PickLoanUnit',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(COURIER)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Loan_Consignment_Number')
  End
  If p_web.GSV('job:Loan_Unit_Number') > 0 AND p_web.GSV('ReadOnly:LoanIMEINumber') = 0
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('job:ESN',job:ESN)
  p_web.SetSessionValue('job:MSN',job:MSN)
  p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  p_web.SetSessionValue('tmp:LoanIMEINumber',tmp:LoanIMEINumber)
  p_web.SetSessionValue('locLoanAlertMessage',locLoanAlertMessage)
  p_web.SetSessionValue('tmp:MSN',tmp:MSN)
  p_web.SetSessionValue('tmp:LoanUnitDetails',tmp:LoanUnitDetails)
  p_web.SetSessionValue('tmp:LoanAccessories',tmp:LoanAccessories)
  p_web.SetSessionValue('tmp:LoanLocation',tmp:LoanLocation)
  p_web.SetSessionValue('jobe:LoanReplacementValue',jobe:LoanReplacementValue)
  p_web.SetSessionValue('jobe2:LoanIDNumber',jobe2:LoanIDNumber)
  p_web.SetSessionValue('locLoanIDAlert',locLoanIDAlert)
  p_web.SetSessionValue('locLoanIDOption',locLoanIDOption)
  p_web.SetSessionValue('job:Loan_Courier',job:Loan_Courier)
  p_web.SetSessionValue('job:Loan_Consignment_Number',job:Loan_Consignment_Number)
  p_web.SetSessionValue('job:Loan_Despatched',job:Loan_Despatched)
  p_web.SetSessionValue('job:Loan_Despatched_User',job:Loan_Despatched_User)
  p_web.SetSessionValue('job:Loan_Despatch_Number',job:Loan_Despatch_Number)
  p_web.SetSessionValue('locRemovalAlertMessage',locRemovalAlertMessage)
  p_web.SetSessionValue('tmp:RemovalReason',tmp:RemovalReason)
  p_web.SetSessionValue('locValidateMessage',locValidateMessage)
  p_web.SetSessionValue('tmp:LostLoanFee',tmp:LostLoanFee)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('job:ESN')
    job:ESN = p_web.GetValue('job:ESN')
    p_web.SetSessionValue('job:ESN',job:ESN)
  End
  if p_web.IfExistsValue('job:MSN')
    job:MSN = p_web.GetValue('job:MSN')
    p_web.SetSessionValue('job:MSN',job:MSN)
  End
  if p_web.IfExistsValue('job:Charge_Type')
    job:Charge_Type = p_web.GetValue('job:Charge_Type')
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  End
  if p_web.IfExistsValue('job:Warranty_Charge_Type')
    job:Warranty_Charge_Type = p_web.GetValue('job:Warranty_Charge_Type')
    p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  End
  if p_web.IfExistsValue('tmp:LoanIMEINumber')
    tmp:LoanIMEINumber = p_web.GetValue('tmp:LoanIMEINumber')
    p_web.SetSessionValue('tmp:LoanIMEINumber',tmp:LoanIMEINumber)
  End
  if p_web.IfExistsValue('locLoanAlertMessage')
    locLoanAlertMessage = p_web.GetValue('locLoanAlertMessage')
    p_web.SetSessionValue('locLoanAlertMessage',locLoanAlertMessage)
  End
  if p_web.IfExistsValue('tmp:MSN')
    tmp:MSN = p_web.GetValue('tmp:MSN')
    p_web.SetSessionValue('tmp:MSN',tmp:MSN)
  End
  if p_web.IfExistsValue('tmp:LoanUnitDetails')
    tmp:LoanUnitDetails = p_web.GetValue('tmp:LoanUnitDetails')
    p_web.SetSessionValue('tmp:LoanUnitDetails',tmp:LoanUnitDetails)
  End
  if p_web.IfExistsValue('tmp:LoanAccessories')
    tmp:LoanAccessories = p_web.GetValue('tmp:LoanAccessories')
    p_web.SetSessionValue('tmp:LoanAccessories',tmp:LoanAccessories)
  End
  if p_web.IfExistsValue('tmp:LoanLocation')
    tmp:LoanLocation = p_web.GetValue('tmp:LoanLocation')
    p_web.SetSessionValue('tmp:LoanLocation',tmp:LoanLocation)
  End
  if p_web.IfExistsValue('jobe:LoanReplacementValue')
    jobe:LoanReplacementValue = p_web.dformat(clip(p_web.GetValue('jobe:LoanReplacementValue')),'@n14.2')
    p_web.SetSessionValue('jobe:LoanReplacementValue',jobe:LoanReplacementValue)
  End
  if p_web.IfExistsValue('jobe2:LoanIDNumber')
    jobe2:LoanIDNumber = p_web.GetValue('jobe2:LoanIDNumber')
    p_web.SetSessionValue('jobe2:LoanIDNumber',jobe2:LoanIDNumber)
  End
  if p_web.IfExistsValue('locLoanIDAlert')
    locLoanIDAlert = p_web.GetValue('locLoanIDAlert')
    p_web.SetSessionValue('locLoanIDAlert',locLoanIDAlert)
  End
  if p_web.IfExistsValue('locLoanIDOption')
    locLoanIDOption = p_web.GetValue('locLoanIDOption')
    p_web.SetSessionValue('locLoanIDOption',locLoanIDOption)
  End
  if p_web.IfExistsValue('job:Loan_Courier')
    job:Loan_Courier = p_web.GetValue('job:Loan_Courier')
    p_web.SetSessionValue('job:Loan_Courier',job:Loan_Courier)
  End
  if p_web.IfExistsValue('job:Loan_Consignment_Number')
    job:Loan_Consignment_Number = p_web.GetValue('job:Loan_Consignment_Number')
    p_web.SetSessionValue('job:Loan_Consignment_Number',job:Loan_Consignment_Number)
  End
  if p_web.IfExistsValue('job:Loan_Despatched')
    job:Loan_Despatched = p_web.dformat(clip(p_web.GetValue('job:Loan_Despatched')),'@d06b')
    p_web.SetSessionValue('job:Loan_Despatched',job:Loan_Despatched)
  End
  if p_web.IfExistsValue('job:Loan_Despatched_User')
    job:Loan_Despatched_User = p_web.GetValue('job:Loan_Despatched_User')
    p_web.SetSessionValue('job:Loan_Despatched_User',job:Loan_Despatched_User)
  End
  if p_web.IfExistsValue('job:Loan_Despatch_Number')
    job:Loan_Despatch_Number = p_web.GetValue('job:Loan_Despatch_Number')
    p_web.SetSessionValue('job:Loan_Despatch_Number',job:Loan_Despatch_Number)
  End
  if p_web.IfExistsValue('locRemovalAlertMessage')
    locRemovalAlertMessage = p_web.GetValue('locRemovalAlertMessage')
    p_web.SetSessionValue('locRemovalAlertMessage',locRemovalAlertMessage)
  End
  if p_web.IfExistsValue('tmp:RemovalReason')
    tmp:RemovalReason = p_web.GetValue('tmp:RemovalReason')
    p_web.SetSessionValue('tmp:RemovalReason',tmp:RemovalReason)
  End
  if p_web.IfExistsValue('locValidateMessage')
    locValidateMessage = p_web.GetValue('locValidateMessage')
    p_web.SetSessionValue('locValidateMessage',locValidateMessage)
  End
  if p_web.IfExistsValue('tmp:LostLoanFee')
    tmp:LostLoanFee = p_web.dformat(clip(p_web.GetValue('tmp:LostLoanFee')),'@n14.2')
    p_web.SetSessionValue('tmp:LostLoanFee',tmp:LostLoanFee)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('PickLoanUnit_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  ! Security Checks
    IF (p_web.IfExistsValue('ReturnURL'))
        p_web.StoreValue('ReturnURL')
  !    ELSE
  !        p_web.SSV('ReturnURL','ViewJob') ! #13379 Don't default URL it causes problems (DBH: 12/09/2014)
    END
  
      if (p_web.GSV('PickLoan:FirstTime') = 0)
            p_web.SSV('ReadOnly:LoanIMEINumber',0)
  
            IF (p_web.GSV('BookingAccount') <> p_web.GSV('wob:HeadAccountNumber'))
                ! #13618 The only time this should be read only is when viewed by another RRC (DBH: 28/10/2015)
                p_web.SSV('ReadOnly:LoanIMEINumber',1)
  !            IF (p_web.GSV('Job:ViewOnly') = 1)
  !                p_web.SSV('ReadOnly:LoanIMEINumber',1)
            ELSE ! IF
                IF (p_web.GSV('job:Exchange_Unit_Number')> 0)
                    IF (~DoesUserHaveAccess(p_web,'JOBS - AMEND LOAN UNIT'))
                    ! #13418 Don't allow user to CHANGE the exchange unit (DBH: 05/11/2014)
                        p_web.SSV('ReadOnly:LoanIMEINumber',1)
                    END ! IF
                ELSE ! IF
                    IF (~DoesUserHaveAccess(p_web,'JOBS - ADD LOAN UNIT'))
                    ! #13418 Don't allow user to ADD an exchange unit (DBH: 05/11/2014)
                        p_web.SSV('ReadOnly:LoanIMEINumber',1)
                    END ! IF
                END ! IF
            END ! IF
  
          p_web.SSV('tmp:LoanUnitNumber',p_web.GSV('job:Loan_Unit_Number'))
  
          if (p_web.GSV('job:Loan_Unit_Number') > 0)
              do lookupLoanDetails
          else
              do clearLoanDetails
          end
  
          p_web.SSV('locLoanAlertMessage','')
          p_web.SSV('locRemoveAlertMessage','')
          p_web.SSV('Hide:RemovalReason',1)
          p_web.SSV('tmp:RemovalReason',0)
          p_web.SSV('tmp:NoUnitAvailable',0)
          p_web.SSV('locValidateMessage','')
          p_web.SSV('Hide:LostLoanFee',1)
          p_web.SSV('Hide:ConfirmRemovalButton',1)
          p_web.SSV('Hide:ValidateAccessoriesButton',0)
          p_web.SSV('locLoanIDOption',0)
          p_web.SSV('locLoanIDAlert','')
          p_web.SSV('Hide:LoanIDAlert',1)
  
          do clearTagFile
          p_web.SSV('PickLoan:FirstTime',1)
      end !if (p_web.GSV('PickLoan:FirstTime') = 0)
  
      if (p_web.GSV('job:Exchange_Unit_Number') > 0 And ((p_web.GSV('BookingSite') = 'RRC' And p_web.GSV('jobe:Despatched') = 'REA' And p_web.GSV('jobe:DespatchType') = 'EXC') Or |
          (p_web.GSV('BookingSite') = 'ARC' And p_web.GSV('job:Despatched') = 'REA' And p_web.GSV('job:Despatch_Type') = 'EXC')))
            CreateScript(p_web,packet,'alert("You cannot attached a Loan Unit until the Exchange Unit has been despatched.")')
          do sendPacket
          p_web.SSV('ReadOnly:LoanIMEINumber',1)
      End !
  
      p_web.SSV('ReadOnly:LoanDespatchDetails',1)
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:LoanIMEINumber = p_web.RestoreValue('tmp:LoanIMEINumber')
 locLoanAlertMessage = p_web.RestoreValue('locLoanAlertMessage')
 tmp:MSN = p_web.RestoreValue('tmp:MSN')
 tmp:LoanUnitDetails = p_web.RestoreValue('tmp:LoanUnitDetails')
 tmp:LoanAccessories = p_web.RestoreValue('tmp:LoanAccessories')
 tmp:LoanLocation = p_web.RestoreValue('tmp:LoanLocation')
 locLoanIDAlert = p_web.RestoreValue('locLoanIDAlert')
 locLoanIDOption = p_web.RestoreValue('locLoanIDOption')
 locRemovalAlertMessage = p_web.RestoreValue('locRemovalAlertMessage')
 tmp:RemovalReason = p_web.RestoreValue('tmp:RemovalReason')
 locValidateMessage = p_web.RestoreValue('locValidateMessage')
 tmp:LostLoanFee = p_web.RestoreValue('tmp:LostLoanFee')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('ReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('PickLoanUnit_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('PickLoanUnit_ChainTo')
    loc:formaction = p_web.GetSessionValue('PickLoanUnit_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ReturnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="PickLoanUnit" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="PickLoanUnit" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="PickLoanUnit" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Pick Loan Unit') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Pick Loan Unit',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_PickLoanUnit">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_PickLoanUnit" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  do GenerateTab4
  do GenerateTab5
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_PickLoanUnit')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Loan Unit Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Loan ID Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Despatch Details') & ''''
        If p_web.GSV('job:Loan_Unit_Number') > 0 AND p_web.GSV('ReadOnly:LoanIMEINumber') = 0
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Remove Loan Unit') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_PickLoanUnit')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_PickLoanUnit'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('PickLoanUnit_TagValidateLoanAccessories_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('PickLoanUnit_TagValidateLoanAccessories_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('PickLoanUnit_TagValidateLoanAccessories_embedded_div')&''');'
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='LOAN'
          If Not (p_web.GSV('tmp:LoanIMEINumber') = '')
            p_web.SetValue('SelectField',clip(loc:formname) & '.jobe:LoanReplacementValue')
          End
    End
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab6'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab5'''
          If p_web.GSV('job:Loan_Unit_Number') > 0 AND p_web.GSV('ReadOnly:LoanIMEINumber') = 0
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab7'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_PickLoanUnit')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab6'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab5'');'&CRLF
    if p_web.GSV('job:Loan_Unit_Number') > 0 AND p_web.GSV('ReadOnly:LoanIMEINumber') = 0
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab7'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Job Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickLoanUnit_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:ESN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:MSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locUnitDetails
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locUnitDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locUnitDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Warranty_Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Warranty_Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Warranty_Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Loan Unit Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickLoanUnit_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Loan Unit Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Loan Unit Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Loan Unit Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Loan Unit Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:LoanIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:LoanIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:LoanIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& ' rowspan="4">'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locLoanAlertMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' rowspan="4">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locLoanAlertMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&' rowspan="4">'
      loc:columncounter += 1
      do SendPacket
      do Comment::locLoanAlertMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('ReadOnly:LoanIMEINumber') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPickLoanUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonPickLoanUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('tmp:MSN') <> ''
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:MSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:LoanUnitDetails
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:LoanUnitDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:LoanUnitDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:LoanAccessories
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:LoanAccessories
      do Comment::tmp:LoanAccessories
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnLoanAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnLoanAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:LoanLocation
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:LoanLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:LoanLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:LoanReplacementValue
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:LoanReplacementValue
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe:LoanReplacementValue
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel6">'&CRLF &|
                                    '  <div id="panel6Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Loan ID Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel6Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickLoanUnit_6">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Loan ID Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab6" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab6">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Loan ID Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab6">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Loan ID Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab6">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Loan ID Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:LoanIDNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:LoanIDNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:LoanIDNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locLoanIDAlert
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locLoanIDAlert
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locLoanIDOption
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locLoanIDOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locLoanIDOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel5">'&CRLF &|
                                    '  <div id="panel5Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Despatch Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel5Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickLoanUnit_5">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab5" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab5">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Loan_Courier
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Loan_Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Loan_Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Loan_Consignment_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Loan_Consignment_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Loan_Consignment_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Loan_Despatched
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Loan_Despatched
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Loan_Despatched
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Loan_Despatched_User
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Loan_Despatched_User
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Loan_Despatched_User
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Loan_Despatch_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Loan_Despatch_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Loan_Despatch_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button:AmendDespatchDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::button:AmendDespatchDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab4  Routine
  If p_web.GSV('job:Loan_Unit_Number') > 0 AND p_web.GSV('ReadOnly:LoanIMEINumber') = 0
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Remove Loan Unit') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickLoanUnit_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Remove Loan Unit')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Remove Loan Unit')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Remove Loan Unit')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Remove Loan Unit')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locRemoveText
      do Comment::locRemoveText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::buttonRemoveAttachedUnit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonRemoveAttachedUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonRemoveAttachedUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& ' rowspan="5">'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locRemovalAlertMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' valign="top" rowspan="5">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locRemovalAlertMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&' rowspan="5">'
      loc:columncounter += 1
      do SendPacket
      do Comment::locRemovalAlertMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RemovalReason
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RemovalReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RemovalReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::TagValidateLoanAccessories
      do Value::TagValidateLoanAccessories
      do Comment::TagValidateLoanAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonValidateAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonValidateAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locValidateMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locValidateMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locValidateMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:LostLoanFee
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:LostLoanFee
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:LostLoanFee
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::buttonConfirmLoanRemoval
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonConfirmLoanRemoval
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonConfirmLoanRemoval
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab5  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel7">'&CRLF &|
                                    '  <div id="panel7Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel7Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PickLoanUnit_7">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab7" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab7">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab7">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab7">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::link:SMSHistory
      do Comment::link:SMSHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::job:ESN  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:ESN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('IMEI Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:ESN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:ESN',p_web.GetValue('NewValue'))
    job:ESN = p_web.GetValue('NewValue') !FieldType= STRING Field = job:ESN
    do Value::job:ESN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:ESN',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    job:ESN = p_web.GetValue('Value')
  End

Value::job:ESN  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:ESN') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:ESN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:ESN'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:ESN  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:ESN') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:MSN  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:MSN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('M.S.N.')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:MSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:MSN',p_web.GetValue('NewValue'))
    job:MSN = p_web.GetValue('NewValue') !FieldType= STRING Field = job:MSN
    do Value::job:MSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:MSN',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    job:MSN = p_web.GetValue('Value')
  End

Value::job:MSN  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:MSN') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:MSN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:MSN'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:MSN  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:MSN') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locUnitDetails  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locUnitDetails') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Unit Details')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locUnitDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locUnitDetails',p_web.GetValue('NewValue'))
    do Value::locUnitDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::locUnitDetails  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locUnitDetails') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('job:Manufacturer') & ' ' & p_web.GSV('job:Model_Number') & ' - ' & p_web.GSV('job:Unit_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locUnitDetails  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locUnitDetails') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Charge_Type  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Charge_Type') & '_prompt',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Charge Type')
  If p_web.GSV('job:Chargeable_Job') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Charge_Type',p_web.GetValue('NewValue'))
    job:Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Charge_Type
    do Value::job:Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Charge_Type = p_web.GetValue('Value')
  End

Value::job:Charge_Type  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Charge_Type') & '_value',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Chargeable_Job') <> 'YES')
  ! --- DISPLAY --- job:Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::job:Charge_Type  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Charge_Type') & '_comment',Choose(p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Chargeable_Job') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Warranty_Charge_Type  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Warranty_Charge_Type') & '_prompt',Choose(p_web.GSV('job:Warranty_Job') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Warr Charge Type')
  If p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Warranty_Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Warranty_Charge_Type',p_web.GetValue('NewValue'))
    job:Warranty_Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Warranty_Charge_Type
    do Value::job:Warranty_Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Warranty_Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Warranty_Charge_Type = p_web.GetValue('Value')
  End

Value::job:Warranty_Charge_Type  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Warranty_Charge_Type') & '_value',Choose(p_web.GSV('job:Warranty_Job') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Warranty_Job') <> 'YES')
  ! --- DISPLAY --- job:Warranty_Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Warranty_Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::job:Warranty_Charge_Type  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Warranty_Charge_Type') & '_comment',Choose(p_web.GSV('job:Warranty_Job') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:LoanIMEINumber  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanIMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Loan IMEI No')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:LoanIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:LoanIMEINumber',p_web.GetValue('NewValue'))
    tmp:LoanIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:LoanIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:LoanIMEINumber',p_web.GetValue('Value'))
    tmp:LoanIMEINumber = p_web.GetValue('Value')
  End
    tmp:LoanIMEINumber = Upper(tmp:LoanIMEINumber)
    p_web.SetSessionValue('tmp:LoanIMEINumber',tmp:LoanIMEINumber)
  ! Validate IMEI
  p_web.SSV('locLoanAlertMessage','')
  Access:Loan.Clearkey(loa:ESN_Only_Key)
  loa:ESN    = p_web.GSV('tmp:LoanIMEINumber')
  if (Access:Loan.TryFetch(loa:ESN_Only_Key) = Level:Benign)
      ! Found
      if (loa:Location <> p_web.GSV('BookingSiteLocation'))
          p_web.SSV('locLoanAlertMessage','Selected IMEI is from a different location.')
      else !  !if (loa:Location <> p_web.GSV('BookingSiteLocation'))
          if (loa:Available = 'AVL')
              Access:STOCKTYP.Clearkey(stp:Stock_Type_Key)
              stp:Stock_Type    = loa:Stock_Type
              if (Access:STOCKTYP.TryFetch(stp:Stock_Type_Key) = Level:Benign)
                  ! Found
                  if (stp:Available <> 1)
                      p_web.SSV('locLoanAlertMessage','Selected Stock Type is not available')
                  else ! if (stp:Available <> 1)
                      p_web.SSV('tmp:LoanUnitNumber',loa:Ref_Number)
                  end ! if (stp:Available <> 1)
              else ! if (Access:STOCKTYP.TryFetch(stp:Stock_Type_Key) = Level:Benign)
                  ! Error
              end ! if (Access:STOCKTYP.TryFetch(stp:Stock_Type_Key) = Level:Benign)
          else ! if (loa:Available = 'AVL')
              p_web.SSV('locLoanAlertMessage','Selected IMEI is not available')
          end ! if (loa:Available = 'AVL')
      end !if (loa:Location <> p_web.GSV('BookingSiteLocation'))
  else ! if (Access:Loan.TryFetch(loa:ESN_Only_Key) = Level:Benign)
      ! Error
      p_web.SSV('locLoanAlertMessage','Cannot find the selected IMEI Number')
  end ! if (Access:Loan.TryFetch(loa:ESN_Only_Key) = Level:Benign)
  
  if (p_web.GSV('locLoanAlertMessage') = '')
      do getLoanDetails
  else
      do clearLoanDetails
  end ! if (p_web.GSV('locLoanAlertMessage') = '')
  p_Web.SetValue('lookupfield','tmp:LoanIMEINumber')
  do AfterLookup
  do Value::tmp:LoanIMEINumber
  do SendAlert
  do Comment::tmp:LoanIMEINumber
  do Prompt::locLoanAlertMessage
  do Value::locLoanAlertMessage  !1
  do Value::tmp:LoanLocation  !1
  do Value::tmp:LoanUnitDetails  !1

Value::tmp:LoanIMEINumber  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanIMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:LoanIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:LoanIMEINumber') = 1 OR p_web.GSV('job:Loan_Unit_Number') > 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:LoanIMEINumber') = 1 OR p_web.GSV('job:Loan_Unit_Number') > 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:LoanIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:LoanIMEINumber'',''pickloanunit_tmp:loanimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:LoanIMEINumber')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','tmp:LoanIMEINumber',p_web.GetSessionValueFormat('tmp:LoanIMEINumber'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('FormLoanUnitFilter')&'?LookupField=tmp:LoanIMEINumber&Tab=5&ForeignField=loa:ESN&_sort=&Refresh=sort&LookupFrom=PickLoanUnit&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('tmp:LoanIMEINumber') & '_value')

Comment::tmp:LoanIMEINumber  Routine
      loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanIMEINumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('tmp:LoanIMEINumber') & '_comment')

Prompt::locLoanAlertMessage  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanAlertMessage') & '_prompt',Choose(p_web.GSV('locLoanAlertMessage') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locLoanAlertMessage') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('locLoanAlertMessage') & '_prompt')

Validate::locLoanAlertMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locLoanAlertMessage',p_web.GetValue('NewValue'))
    locLoanAlertMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locLoanAlertMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locLoanAlertMessage',p_web.GetValue('Value'))
    locLoanAlertMessage = p_web.GetValue('Value')
  End

Value::locLoanAlertMessage  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanAlertMessage') & '_value',Choose(p_web.GSV('locLoanAlertMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locLoanAlertMessage') = '')
  ! --- DISPLAY --- locLoanAlertMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web.Translate(p_web.GSV('locLoanAlertMessage'),1) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('locLoanAlertMessage') & '_value')

Comment::locLoanAlertMessage  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanAlertMessage') & '_comment',Choose(p_web.GSV('locLoanAlertMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locLoanAlertMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPickLoanUnit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPickLoanUnit',p_web.GetValue('NewValue'))
    do Value::buttonPickLoanUnit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::buttonPickLoanUnit
  do SendAlert

Value::buttonPickLoanUnit  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('buttonPickLoanUnit') & '_value',Choose(1 or p_web.GSV('job:Loan_Unit_Number') > 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (1 or p_web.GSV('job:Loan_Unit_Number') > 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPickLoanUnit'',''pickloanunit_buttonpickloanunit_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PickLoanUnit','Pick Loan Unit','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormLoanUnitFilter?LookupField=tmp:LoanIMEINumber&Tab=2&ForeignField=loa:ESN&_sort=loa:ESN&Refresh=sort&LookupFrom=PickLoanUnit&')) & ''','''&clip('_self')&''')',loc:javascript,0,'images\packinsert.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('buttonPickLoanUnit') & '_value')

Comment::buttonPickLoanUnit  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('buttonPickLoanUnit') & '_comment',Choose(1 or p_web.GSV('job:Loan_Unit_Number') > 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If 1 or p_web.GSV('job:Loan_Unit_Number') > 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:MSN  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:MSN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('M.S.N.')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:MSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:MSN',p_web.GetValue('NewValue'))
    tmp:MSN = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:MSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:MSN',p_web.GetValue('Value'))
    tmp:MSN = p_web.GetValue('Value')
  End

Value::tmp:MSN  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:MSN') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- tmp:MSN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:MSN'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('tmp:MSN') & '_value')

Comment::tmp:MSN  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:MSN') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:LoanUnitDetails  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanUnitDetails') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Unit Details')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:LoanUnitDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:LoanUnitDetails',p_web.GetValue('NewValue'))
    tmp:LoanUnitDetails = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:LoanUnitDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:LoanUnitDetails',p_web.GetValue('Value'))
    tmp:LoanUnitDetails = p_web.GetValue('Value')
  End

Value::tmp:LoanUnitDetails  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanUnitDetails') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- tmp:LoanUnitDetails
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:LoanUnitDetails'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('tmp:LoanUnitDetails') & '_value')

Comment::tmp:LoanUnitDetails  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanUnitDetails') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:LoanAccessories  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanAccessories') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Accessories')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:LoanAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:LoanAccessories',p_web.GetValue('NewValue'))
    tmp:LoanAccessories = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:LoanAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:LoanAccessories',p_web.GetValue('Value'))
    tmp:LoanAccessories = p_web.GetValue('Value')
  End

Value::tmp:LoanAccessories  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanAccessories') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- tmp:LoanAccessories
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:LoanAccessories'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('tmp:LoanAccessories') & '_value')

Comment::tmp:LoanAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanAccessories') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnLoanAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnLoanAccessories',p_web.GetValue('NewValue'))
    do Value::btnLoanAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnLoanAccessories  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('btnLoanAccessories') & '_value',Choose(p_web.GSV('tmp:LoanAccessories') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:LoanAccessories') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnLoanAccessories','...','LookupButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('BrowseJobLoanAccessories')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::btnLoanAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('btnLoanAccessories') & '_comment',Choose(p_web.GSV('tmp:LoanAccessories') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:LoanAccessories') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:LoanLocation  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanLocation') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Location / Stock Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:LoanLocation  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:LoanLocation',p_web.GetValue('NewValue'))
    tmp:LoanLocation = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:LoanLocation
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:LoanLocation',p_web.GetValue('Value'))
    tmp:LoanLocation = p_web.GetValue('Value')
  End

Value::tmp:LoanLocation  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanLocation') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- tmp:LoanLocation
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:LoanLocation'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('tmp:LoanLocation') & '_value')

Comment::tmp:LoanLocation  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LoanLocation') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe:LoanReplacementValue  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('jobe:LoanReplacementValue') & '_prompt',Choose(p_web.GSV('tmp:LoanIMEINumber') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Replacement Value')
  If p_web.GSV('tmp:LoanIMEINumber') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('jobe:LoanReplacementValue') & '_prompt')

Validate::jobe:LoanReplacementValue  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:LoanReplacementValue',p_web.GetValue('NewValue'))
    jobe:LoanReplacementValue = p_web.GetValue('NewValue') !FieldType= REAL Field = jobe:LoanReplacementValue
    do Value::jobe:LoanReplacementValue
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:LoanReplacementValue',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    jobe:LoanReplacementValue = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End

Value::jobe:LoanReplacementValue  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('jobe:LoanReplacementValue') & '_value',Choose(p_web.GSV('tmp:LoanIMEINumber') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:LoanIMEINumber') = '')
  ! --- STRING --- jobe:LoanReplacementValue
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('jobe:LoanReplacementValue')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe:LoanReplacementValue',p_web.GetSessionValue('jobe:LoanReplacementValue'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,'Loan Replacement Value') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('jobe:LoanReplacementValue') & '_value')

Comment::jobe:LoanReplacementValue  Routine
      loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('jobe:LoanReplacementValue') & '_comment',Choose(p_web.GSV('tmp:LoanIMEINumber') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:LoanIMEINumber') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe2:LoanIDNumber  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('jobe2:LoanIDNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Loan ID Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe2:LoanIDNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:LoanIDNumber',p_web.GetValue('NewValue'))
    jobe2:LoanIDNumber = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:LoanIDNumber
    do Value::jobe2:LoanIDNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:LoanIDNumber',p_web.dFormat(p_web.GetValue('Value'),'@s13'))
    jobe2:LoanIDNumber = p_web.GetValue('Value')
  End
      Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
      jobe2:RefNumber = p_web.GSV('job:Ref_Number')
      IF (access:jobse2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
          IF (jobe2:LoanIDNumber <> p_web.GSV('jobe2:LoanIDNumber') AND jobe2:LoanIDNumber <> '')
              p_web.SSV('Hide:LoanIDAlert',0)
              p_web.SSV('locLoanIDAlert','You have changed the Loan ID. Please select one of the following options:')
              p_web.SSV('locLoanIDOption',0)
          ELSE
              p_web.SSV('Hide:LoanIDAlert',1)
          END
      END
          
  do Value::jobe2:LoanIDNumber
  do SendAlert
  do Prompt::locLoanIDOption
  do Value::locLoanIDOption  !1
  do Value::locLoanIDAlert  !1

Value::jobe2:LoanIDNumber  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('jobe2:LoanIDNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe2:LoanIDNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('jobe2:LoanIDNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:LoanIDNumber'',''pickloanunit_jobe2:loanidnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:LoanIDNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:LoanIDNumber',p_web.GetSessionValueFormat('jobe2:LoanIDNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s13'),'Loan ID Number') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('jobe2:LoanIDNumber') & '_value')

Comment::jobe2:LoanIDNumber  Routine
      loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('jobe2:LoanIDNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locLoanIDAlert  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locLoanIDAlert',p_web.GetValue('NewValue'))
    locLoanIDAlert = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locLoanIDAlert
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locLoanIDAlert',p_web.GetValue('Value'))
    locLoanIDAlert = p_web.GetValue('Value')
  End

Value::locLoanIDAlert  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanIDAlert') & '_value',Choose(p_web.GSV('Hide:LoanIDAlert') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:LoanIDAlert') = 1)
  ! --- DISPLAY --- locLoanIDAlert
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locLoanIDAlert'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('locLoanIDAlert') & '_value')

Comment::locLoanIDAlert  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanIDAlert') & '_comment',Choose(p_web.GSV('Hide:LoanIDAlert') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:LoanIDAlert') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locLoanIDOption  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanIDOption') & '_prompt',Choose(p_web.GSV('Hide:LoanIDAlert') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Select Option')
  If p_web.GSV('Hide:LoanIDAlert') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('locLoanIDOption') & '_prompt')

Validate::locLoanIDOption  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locLoanIDOption',p_web.GetValue('NewValue'))
    locLoanIDOption = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locLoanIDOption
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locLoanIDOption',p_web.GetValue('Value'))
    locLoanIDOption = p_web.GetValue('Value')
  End
      Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
      jobe2:RefNumber = p_web.GSV('job:Ref_Number')
      IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
          CASE p_web.GSV('locLoanIDOption')
          OF 1
          OF 2
          OF 9
              p_web.SSV('jobe2:LoanIDNumber',jobe2:LoanIDNumber)
              p_web.SSV('Hide:LoanIDAlert',1)
          END
      END
  
          
  do Value::locLoanIDOption
  do SendAlert
  do Value::jobe2:LoanIDNumber  !1
  do Prompt::locLoanIDOption
  do Value::locLoanIDAlert  !1

Value::locLoanIDOption  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanIDOption') & '_value',Choose(p_web.GSV('Hide:LoanIDAlert') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:LoanIDAlert') = 1)
  ! --- RADIO --- locLoanIDOption
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locLoanIDOption')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locLoanIDOption') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locLoanIDOption'',''pickloanunit_locloanidoption_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locLoanIDOption')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locLoanIDOption',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locLoanIDOption_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Change Loan ID Only') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locLoanIDOption') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locLoanIDOption'',''pickloanunit_locloanidoption_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locLoanIDOption')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locLoanIDOption',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locLoanIDOption_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Change Loan And Customer ID') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locLoanIDOption') = 9
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locLoanIDOption'',''pickloanunit_locloanidoption_value'',1,'''&clip(9)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locLoanIDOption')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locLoanIDOption',clip(9),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locLoanIDOption_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Do Not Change Loan ID') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('locLoanIDOption') & '_value')

Comment::locLoanIDOption  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locLoanIDOption') & '_comment',Choose(p_web.GSV('Hide:LoanIDAlert') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:LoanIDAlert') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Loan_Courier  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Courier') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Courier')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Loan_Courier  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Loan_Courier',p_web.GetValue('NewValue'))
    job:Loan_Courier = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Loan_Courier
    do Value::job:Loan_Courier
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Loan_Courier',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Loan_Courier = p_web.GetValue('Value')
  End

Value::job:Loan_Courier  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Courier') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('job:Loan_Courier')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('job:Loan_Courier',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('job:Loan_Courier') = 0
    p_web.SetSessionValue('job:Loan_Courier','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('job:Loan_Courier')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(Tagging)
  bind(tgg:Record)
  p_web._OpenFile(JOBSE2)
  bind(jobe2:Record)
  p_web._OpenFile(COURIER)
  bind(cou:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  p_web._OpenFile(LOAN)
  bind(loa:Record)
  p_web._OpenFile(JOBEXACC)
  bind(jea:Record)
  p_web._OpenFile(LOAN_ALIAS)
  bind(loa_ali:Record)
  p_web._OpenFile(EXCHOR48)
  bind(ex4:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  p_web._OpenFile(MODELNUM)
  bind(mod:Record)
  p_web._OpenFile(STOCK)
  bind(sto:Record)
  p_web._OpenFile(WARPARTS)
  bind(wpr:Record)
  p_web._OpenFile(STOCKTYP)
  bind(stp:Record)
  p_web._OpenFile(EXCHHIST)
  bind(exh:Record)
  p_web._OpenFile(PRODCODE)
  bind(prd:Record)
  p_web._OpenFile(JOBSE)
  bind(jobe:Record)
  p_web._OpenFile(LOANACC)
  bind(lac:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(job:Loan_Courier_OptionView)
  job:Loan_Courier_OptionView{prop:order} = 'UPPER(cou:Courier)'
  Set(job:Loan_Courier_OptionView)
  Loop
    Next(job:Loan_Courier_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('job:Loan_Courier') = 0
      p_web.SetSessionValue('job:Loan_Courier',cou:Courier)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cou:Courier,choose(cou:Courier = p_web.getsessionvalue('job:Loan_Courier')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(job:Loan_Courier_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(Tagging)
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(JOBEXACC)
  p_Web._CloseFile(LOAN_ALIAS)
  p_Web._CloseFile(EXCHOR48)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(PRODCODE)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(LOANACC)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()

Comment::job:Loan_Courier  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Courier') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Loan_Consignment_Number  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Consignment_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Consignment Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Loan_Consignment_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Loan_Consignment_Number',p_web.GetValue('NewValue'))
    job:Loan_Consignment_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Loan_Consignment_Number
    do Value::job:Loan_Consignment_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Loan_Consignment_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Loan_Consignment_Number = p_web.GetValue('Value')
  End

Value::job:Loan_Consignment_Number  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Consignment_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Loan_Consignment_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:LoanDespatchDetails') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:LoanDespatchDetails') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Loan_Consignment_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Loan_Consignment_Number',p_web.GetSessionValueFormat('job:Loan_Consignment_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('job:Loan_Consignment_Number') & '_value')

Comment::job:Loan_Consignment_Number  Routine
      loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Consignment_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Loan_Despatched  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatched') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Loan_Despatched  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Loan_Despatched',p_web.GetValue('NewValue'))
    job:Loan_Despatched = p_web.GetValue('NewValue') !FieldType= DATE Field = job:Loan_Despatched
    do Value::job:Loan_Despatched
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Loan_Despatched',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    job:Loan_Despatched = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End

Value::job:Loan_Despatched  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatched') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Loan_Despatched
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:LoanDespatchDetails') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:LoanDespatchDetails') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Loan_Despatched')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Loan_Despatched',p_web.GetSessionValue('job:Loan_Despatched'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatched') & '_value')

Comment::job:Loan_Despatched  Routine
      loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatched') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Loan_Despatched_User  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatched_User') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('User')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Loan_Despatched_User  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Loan_Despatched_User',p_web.GetValue('NewValue'))
    job:Loan_Despatched_User = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Loan_Despatched_User
    do Value::job:Loan_Despatched_User
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Loan_Despatched_User',p_web.dFormat(p_web.GetValue('Value'),'@s3'))
    job:Loan_Despatched_User = p_web.GetValue('Value')
  End

Value::job:Loan_Despatched_User  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatched_User') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Loan_Despatched_User
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:LoanDespatchDetails') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:LoanDespatchDetails') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Loan_Despatched_User')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Loan_Despatched_User',p_web.GetSessionValueFormat('job:Loan_Despatched_User'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s3'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatched_User') & '_value')

Comment::job:Loan_Despatched_User  Routine
      loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatched_User') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Loan_Despatch_Number  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatch_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Despatch No')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Loan_Despatch_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Loan_Despatch_Number',p_web.GetValue('NewValue'))
    job:Loan_Despatch_Number = p_web.GetValue('NewValue') !FieldType= LONG Field = job:Loan_Despatch_Number
    do Value::job:Loan_Despatch_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Loan_Despatch_Number',p_web.dFormat(p_web.GetValue('Value'),'@s8'))
    job:Loan_Despatch_Number = p_web.GetValue('Value')
  End

Value::job:Loan_Despatch_Number  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatch_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Loan_Despatch_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:LoanDespatchDetails') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:LoanDespatchDetails') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Loan_Despatch_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Loan_Despatch_Number',p_web.GetSessionValueFormat('job:Loan_Despatch_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s8'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatch_Number') & '_value')

Comment::job:Loan_Despatch_Number  Routine
      loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('job:Loan_Despatch_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::button:AmendDespatchDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button:AmendDespatchDetails',p_web.GetValue('NewValue'))
    do Value::button:AmendDespatchDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      if (p_web.GSV('ReadOnly:LoanDespatchDetails') = 1)
          p_web.SSV('ReadOnly:LoanDespatchDetails',0)
      ELSE
          p_web.SSV('ReadOnly:LoanDespatchDetails',1)
      END
  do Value::button:AmendDespatchDetails
  do SendAlert
  do Value::job:Loan_Consignment_Number  !1
  do Value::job:Loan_Despatch_Number  !1
  do Value::job:Loan_Despatched  !1
  do Value::job:Loan_Despatched_User  !1

Value::button:AmendDespatchDetails  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('button:AmendDespatchDetails') & '_value',Choose(p_web.GSV('job:Loan_Consignment_Number') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Loan_Consignment_Number') = '')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''button:AmendDespatchDetails'',''pickloanunit_button:amenddespatchdetails_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AmendDespatchDetails','Amend Despatch Details','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('button:AmendDespatchDetails') & '_value')

Comment::button:AmendDespatchDetails  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('button:AmendDespatchDetails') & '_comment',Choose(p_web.GSV('job:Loan_Consignment_Number') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Loan_Consignment_Number') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locRemoveText  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locRemoveText',p_web.GetValue('NewValue'))
    do Value::locRemoveText
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::locRemoveText  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locRemoveText') & '_value',Choose(p_web.GSV('job:Loan_unit_Number') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Loan_unit_Number') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web.Translate('You must remove the existing Loan before you can add a new one',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('locRemoveText') & '_value')

Comment::locRemoveText  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locRemoveText') & '_comment',Choose(p_web.GSV('job:Loan_unit_Number') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Loan_unit_Number') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::buttonRemoveAttachedUnit  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('buttonRemoveAttachedUnit') & '_prompt',Choose(p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Loan_Unit_Number') > 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Loan_Unit_Number') > 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::buttonRemoveAttachedUnit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonRemoveAttachedUnit',p_web.GetValue('NewValue'))
    do Value::buttonRemoveAttachedUnit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  p_web.SSV('Hide:RemovalReason',0)
  
  if (p_web.GSV('job:Loan_Consignment_Number') <> '')
      p_web.SSV('locRemoveAlertMessage','Warning!<br>The Loan Unit has already been despatched. '&|
                          'If the continue the unit will be removed and returned to Loan Stock.')
  end ! if (p_web.GSV('job:Loan_Consignment_Number') <> '')
  do SendAlert
  do Prompt::tmp:RemovalReason
  do Value::tmp:RemovalReason  !1
  do Value::buttonConfirmLoanRemoval  !1
  do Value::buttonRemoveAttachedUnit  !1
  do Prompt::locRemovalAlertMessage
  do Value::locRemovalAlertMessage  !1
  do Value::TagValidateLoanAccessories  !1

Value::buttonRemoveAttachedUnit  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('buttonRemoveAttachedUnit') & '_value',Choose(p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Loan_Unit_Number') > 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Loan_Unit_Number') > 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonRemoveAttachedUnit'',''pickloanunit_buttonremoveattachedunit_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','RemoveLoanUnit','Remove Loan Unit','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('buttonRemoveAttachedUnit') & '_value')

Comment::buttonRemoveAttachedUnit  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('buttonRemoveAttachedUnit') & '_comment',Choose(p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Loan_Unit_Number') > 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:RemovalReason') = 0 AND p_web.GSV('job:Loan_Unit_Number') > 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locRemovalAlertMessage  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_prompt',Choose(p_web.GSV('locRemovalAlertMessage') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Alert')
  If p_web.GSV('locRemovalAlertMessage') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_prompt')

Validate::locRemovalAlertMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locRemovalAlertMessage',p_web.GetValue('NewValue'))
    locRemovalAlertMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locRemovalAlertMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locRemovalAlertMessage',p_web.GetValue('Value'))
    locRemovalAlertMessage = p_web.GetValue('Value')
  End
  do Value::locRemovalAlertMessage
  do SendAlert

Value::locRemovalAlertMessage  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_value',Choose(p_web.GSV('locRemovalAlertMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locRemovalAlertMessage') = '')
  ! --- TEXT --- locRemovalAlertMessage
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locRemovalAlertMessage')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locRemovalAlertMessage'',''pickloanunit_locremovalalertmessage_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
  packet = clip(packet) & p_web.CreateTextArea('locRemovalAlertMessage',p_web.GetSessionValue('locRemovalAlertMessage'),5,25,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locRemovalAlertMessage),,Net:Web:Frame) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_value')

Comment::locRemovalAlertMessage  Routine
      loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locRemovalAlertMessage') & '_comment',Choose(p_web.GSV('locRemovalAlertMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locRemovalAlertMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:RemovalReason  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:RemovalReason') & '_prompt',Choose(p_web.GSV('Hide:RemovalReason') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Removal Reason')
  If p_web.GSV('Hide:RemovalReason') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('tmp:RemovalReason') & '_prompt')

Validate::tmp:RemovalReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RemovalReason',p_web.GetValue('NewValue'))
    tmp:RemovalReason = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RemovalReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RemovalReason',p_web.GetValue('Value'))
    tmp:RemovalReason = p_web.GetValue('Value')
  End
  if p_web.GSV('tmp:RemovalReason') = 3 or p_web.GSV('tmp:RemovalReason') = 0
      p_web.SSV('Hide:ConfirmRemovalButton',1)
  else ! if p_web.GSV('tmp:RemovalReason') = 3
      p_web.SSV('Hide:ConfirmRemovalButton',0)
  end ! if p_web.GSV('tmp:RemovalReason') = 3
  do Value::tmp:RemovalReason
  do SendAlert
  do Value::buttonConfirmLoanRemoval  !1
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonValidateAccessories  !1

Value::tmp:RemovalReason  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:RemovalReason') & '_value',Choose(p_web.GSV('Hide:RemovalReason') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:RemovalReason') = 1)
  ! --- RADIO --- tmp:RemovalReason
  loc:fieldclass = Choose(sub(' noButton',1,1) = ' ',clip('FormEntry') & ' noButton',' noButton')
  If lower(loc:invalid) = lower('tmp:RemovalReason')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:RemovalReason') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RemovalReason'',''pickloanunit_tmp:removalreason_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RemovalReason')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:RemovalReason',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:RemovalReason_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Replace Faulty Unit') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:RemovalReason') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RemovalReason'',''pickloanunit_tmp:removalreason_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RemovalReason')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:RemovalReason',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:RemovalReason_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Alternative Model Required') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:RemovalReason') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RemovalReason'',''pickloanunit_tmp:removalreason_value'',1,'''&clip(3)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RemovalReason')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:RemovalReason',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:RemovalReason_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Restock Unit') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:RemovalReason') = 4
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RemovalReason'',''pickloanunit_tmp:removalreason_value'',1,'''&clip(4)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RemovalReason')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:RemovalReason',clip(4),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:RemovalReason_4') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Unit Lost') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('tmp:RemovalReason') & '_value')

Comment::tmp:RemovalReason  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:RemovalReason') & '_comment',Choose(p_web.GSV('Hide:RemovalReason') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:RemovalReason') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::TagValidateLoanAccessories  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('TagValidateLoanAccessories') & '_prompt',Choose(p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Validate Accessories')
  If p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::TagValidateLoanAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('TagValidateLoanAccessories',p_web.GetValue('NewValue'))
    do Value::TagValidateLoanAccessories
  Else
    p_web.StoreValue('acr:Accessory')
  End

Value::TagValidateLoanAccessories  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3,1,0))
  ! --- BROWSE ---  TagValidateLoanAccessories --
  p_web.SetValue('TagValidateLoanAccessories:NoForm',1)
  p_web.SetValue('TagValidateLoanAccessories:FormName',loc:formname)
  p_web.SetValue('TagValidateLoanAccessories:parentIs','Form')
  p_web.SetValue('_parentProc','PickLoanUnit')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('PickLoanUnit_TagValidateLoanAccessories_embedded_div')&'"><!-- Net:TagValidateLoanAccessories --></div><13,10>'
    p_web._DivHeader('PickLoanUnit_' & lower('TagValidateLoanAccessories') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('PickLoanUnit_' & lower('TagValidateLoanAccessories') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagValidateLoanAccessories --><13,10>'
  end
  do SendPacket

Comment::TagValidateLoanAccessories  Routine
    loc:comment = p_web.Translate('Do not tag if no accessories attached')
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('TagValidateLoanAccessories') & '_comment',Choose(p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonValidateAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonValidateAccessories',p_web.GetValue('NewValue'))
    do Value::buttonValidateAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  !Validate Accessories
  CASE ValidateAccessories(p_web,'LOA',p_web.GSV('tmp:LoanUnitNumber'))   ! #13465 Change  to Btrieve Tagging (DBH: 27/10/2015) 
  !  p_web.SSV('AccessoryCheck:Type','LOA')
  !  p_web.SSV('AccessoryCheck:RefNumber',p_web.GSV('tmp:LoanUnitNumber'))
  !  AccessoryCheck(p_web)
  !  
  !  case p_web.GSV('AccessoryCheck:Return')
  of 1
      p_web.SSV('locValidateMessage','<span class="RedBold">There is a missing accessory. If required, enter a Lost Loan Charge below or revalidate the accessories.</span>')
      p_web.SSV('Hide:LostLoanFee',0)
  of 2
      p_web.SSV('locValidateMessage','<span class="RedBold">There is an accessory mismatch. If required, enter a Lost Loan Charge below or revalidate the accessories.</span>')
      p_web.SSV('Hide:LostLoanFee',0)
  else
      p_web.SSV('locValidateMessage','<span class="GreenBold">Accessories Validated</span>')
      p_web.SSV('Hide:LostLoanFee',1)
      p_web.SSV('Hide:ValidateAccessoriesButton',1)
          
  end
  
  p_web.SSV('Hide:ConfirmRemovalButton',0)
  do Value::buttonValidateAccessories
  do SendAlert
  do Prompt::locValidateMessage
  do Value::locValidateMessage  !1
  do Prompt::tmp:LostLoanFee
  do Value::tmp:LostLoanFee  !1
  do Value::buttonConfirmLoanRemoval  !1
  do Value::TagValidateLoanAccessories  !1

Value::buttonValidateAccessories  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('buttonValidateAccessories') & '_value',Choose(p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3 Or p_web.GSV('Hide:ValidateAccessoriesButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3 Or p_web.GSV('Hide:ValidateAccessoriesButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonValidateAccessories'',''pickloanunit_buttonvalidateaccessories_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ValidateAccessories','Validate Accessories','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('buttonValidateAccessories') & '_value')

Comment::buttonValidateAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('buttonValidateAccessories') & '_comment',Choose(p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3 Or p_web.GSV('Hide:ValidateAccessoriesButton') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:RemovalReason') = 1 or p_web.GSV('tmp:RemovalReason') <> 3 Or p_web.GSV('Hide:ValidateAccessoriesButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locValidateMessage  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locValidateMessage') & '_prompt',Choose(p_web.GSV('locValidateMessage') = '' or p_web.GSV('Hide:RemovalReason') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Message')
  If p_web.GSV('locValidateMessage') = '' or p_web.GSV('Hide:RemovalReason') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('locValidateMessage') & '_prompt')

Validate::locValidateMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locValidateMessage',p_web.GetValue('NewValue'))
    locValidateMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locValidateMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locValidateMessage',p_web.GetValue('Value'))
    locValidateMessage = p_web.GetValue('Value')
  End

Value::locValidateMessage  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locValidateMessage') & '_value',Choose(p_web.GSV('locValidateMessage') = '' or p_web.GSV('Hide:RemovalReason') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locValidateMessage') = '' or p_web.GSV('Hide:RemovalReason') = 1)
  ! --- TEXT --- locValidateMessage
  loc:fieldclass = Choose(sub('RedBoldSmall',1,1) = ' ',clip('FormEntry') & 'RedBoldSmall','RedBoldSmall')
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locValidateMessage')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
  packet = clip(packet) & p_web.CreateTextArea('locValidateMessage',p_web.GetSessionValue('locValidateMessage'),5,40,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locValidateMessage),,Net:Web:Frame) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('locValidateMessage') & '_value')

Comment::locValidateMessage  Routine
      loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('locValidateMessage') & '_comment',Choose(p_web.GSV('locValidateMessage') = '' or p_web.GSV('Hide:RemovalReason') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locValidateMessage') = '' or p_web.GSV('Hide:RemovalReason') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:LostLoanFee  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LostLoanFee') & '_prompt',Choose(p_web.GSV('Hide:LostLoanFee') = 1 or p_web.GSV('Hide:RemovalReason') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Lost Loan Fee')
  If p_web.GSV('Hide:LostLoanFee') = 1 or p_web.GSV('Hide:RemovalReason') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('tmp:LostLoanFee') & '_prompt')

Validate::tmp:LostLoanFee  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:LostLoanFee',p_web.GetValue('NewValue'))
    tmp:LostLoanFee = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:LostLoanFee
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:LostLoanFee',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:LostLoanFee = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:LostLoanFee
  do SendAlert

Value::tmp:LostLoanFee  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LostLoanFee') & '_value',Choose(p_web.GSV('Hide:LostLoanFee') = 1 or p_web.GSV('Hide:RemovalReason') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:LostLoanFee') = 1 or p_web.GSV('Hide:RemovalReason') = 1)
  ! --- STRING --- tmp:LostLoanFee
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:LostLoanFee')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:LostLoanFee'',''pickloanunit_tmp:lostloanfee_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:LostLoanFee',p_web.GetSessionValue('tmp:LostLoanFee'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('tmp:LostLoanFee') & '_value')

Comment::tmp:LostLoanFee  Routine
      loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('tmp:LostLoanFee') & '_comment',Choose(p_web.GSV('Hide:LostLoanFee') = 1 or p_web.GSV('Hide:RemovalReason') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:LostLoanFee') = 1 or p_web.GSV('Hide:RemovalReason') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::buttonConfirmLoanRemoval  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('buttonConfirmLoanRemoval') & '_prompt',Choose(p_web.GSV('Hide:RemovalReason') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:RemovalReason') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::buttonConfirmLoanRemoval  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonConfirmLoanRemoval',p_web.GetValue('NewValue'))
    do Value::buttonConfirmLoanRemoval
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      ! Remove Loan Unit
      p_web.SSV('AddToAudit:Type','LOA')
      if p_web.GSV('job:Loan_Consignemnt_Number') > 0
          p_web.SSV('AddToAudit:Notes','UNIT HAD BEEN DESPATCHED: ' & |
              '<13,10>UNIT NUMBER: ' & p_web.GSV('job:Loan_unit_number') & |
              '<13,10>COURIER: ' & p_web.GSV('job:Loan_Courier') & |
              '<13,10>CONSIGNMENT NUMBER: ' & p_web.GSV('job:Loan_consignment_number') & |
              '<13,10>DATE DESPATCHED: ' & Format(p_web.GSV('job:Loan_despatched'),@d6) &|
              '<13,10>DESPATCH USER: ' & p_web.GSV('job:Loan_despatched_user') &|
              '<13,10>DESPATCH NUMBER: ' & p_web.GSV('job:Loan_despatch_number'))
      else ! if p_web.GSV('job:Loan_Consignemnt_Number') > 0
          p_web.SSV('AddToAudit:Notes','UNIT NUMBER: ' & p_web.GSV('job:Loan_unit_Number'))
      end !if p_web.GSV('job:Loan_Consignemnt_Number') > 0
      case p_web.GSV('tmp:RemovalReason')
      of 1
          p_web.SSV('AddToAudit:Action','REPLACED FAULTY LOAN')
      of 2
          p_web.SSV('AddToAudit:Action','ALTERNATIVE MODEL REQUIRED')
      of 3
          p_web.SSV('AddToAudit:Action','RETURN LOAN: RE-STOCKED')
      of 4
          p_web.SSV('AddToAudit:Action','RETURN LOAN: LOAN LOST')
      end ! case p_web.GSV('tmp:RemovalReason')
  
      addToAudit(p_web,p_web.GSV('job:Ref_Number'),'LOA',p_web.GSV('AddToAudit:Action'),p_web.GSV('AddToAudit:Notes'))
  
      !p_web.SSV('job:Loan_Unit_Number','')
      p_web.SSV('job:Loan_Accessory','')
      p_web.SSV('job:Loan_Consignment_Number','')
      p_web.SSV('job:Loan_Despatched','')
      p_web.SSV('job:Loan_Despatched_User','')
      p_web.SSV('job:Loan_Issued_Date','')
      p_web.SSV('job:Loan_User','')
      if (p_web.GSV('job:Despatch_Type') = 'LOA')
          p_web.SSV('job:Despatched','NO')
          p_web.SSV('job:Despatch_type','')
      else ! if (p_web.GSV('job:Despatch_Type') = 'EXC')
          if (p_web.GSV('jobe:DespatchType') = 'LOA')
              p_web.SSV('jobe:DespatchType','')
              p_web.SSV('wob:ReadyToDespatch',0)
          end ! if (p_web.GSV('jobe:DespatchType') = 'EXC')
      end !
  
      p_web.SSV('GetStatus:Type','LOA')
      p_web.SSV('GetSTatus:StatusNumber',101) !Not Issued
      getStatus(816,0,'LOA',p_web) !returned to loan stock
  
      IF (SUB(p_web.GSV('job:Current_Status'),1,3) = '811')
          p_web.SSV('GetStatus:Type','JOB')
          p_web.SSV('GetStatus:StatusNumber','810')
          GetStatus(810,0,'JOB',p_web)
      END
  
      Access:LOAN.ClearKey(loa:Ref_Number_Key)
      loa:Ref_Number = p_web.GSV('job:loan_unit_number')
      IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
          IF (p_web.GSV('tmp:RemovalReason') = 4)
              loa:Available = 'LOS'
          ELSE
              loa:Available = 'AVL'
          END
          loa:StatusChangeDate = TODAY()
          loa:Job_Number = 0
          IF (Access:LOAN.TryUpdate() = Level:Benign)
              IF (Access:LOANHIST.PrimeRecord() = Level:Benign)
                  loh:Ref_Number = loa:Ref_Number
                  loh:Date = TODAY()
                  loh:Time = CLOCK()
                  loh:User = p_web.GSV('BookingUserCode')
                  loh:Status = 'UNIT RE-STOCKED FROM JOB NO: ' & clip(format(p_web.GSV('job:Ref_Number'),@p<<<<<<<#p))
                  IF (Access:LOANHIST.TryInsert())
                      Access:LOANHIST.CancelAutoInc()
                  END
              END
          END
      END
  
      p_web.SSV('job:Loan_Unit_Number','') ! Remove Loan Unit
  
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number    = p_web.GSV('job:Ref_Number')
      if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          ! Found
          p_web.SessionQueueToFile(JOBS)
          access:JOBS.tryUpdate()
      else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
  
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber    = p_web.GSV('job:Ref_Number')
      if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
          ! Found
          p_web.SessionQueueToFile(JOBSE)
          access:JOBSE.tryUpdate()
      else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          ! Error
      end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
  
      p_web.SessionQueueToFile(WEBJOB)
      access:WEBJOB.TryUpdate()
  
      p_web.SSV('tmp:LoanUnitNumber',0)
  
      do clearLoanDetails
  
      p_web.SSV('Hide:RemovalReason',1)
      p_web.SSV('Hide:ConfirmLoanRemoval',1)
  do Value::buttonConfirmLoanRemoval
  do SendAlert
  do Value::buttonPickLoanUnit  !1
  do Value::locLoanAlertMessage  !1
  do Value::locRemovalAlertMessage  !1
  do Value::tmp:LoanAccessories  !1
  do Value::tmp:LoanIMEINumber  !1
  do Value::tmp:LoanLocation  !1
  do Value::tmp:LoanUnitDetails  !1
  do Value::tmp:MSN  !1
  do Prompt::tmp:RemovalReason
  do Value::tmp:RemovalReason  !1
  do Value::locRemoveText  !1
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonValidateAccessories  !1
  do Prompt::locValidateMessage
  do Value::locValidateMessage  !1
  do Prompt::tmp:LostLoanFee
  do Value::tmp:LostLoanFee  !1
  do Prompt::jobe:LoanReplacementValue
  do Value::jobe:LoanReplacementValue  !1

Value::buttonConfirmLoanRemoval  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('buttonConfirmLoanRemoval') & '_value',Choose(p_web.GSV('Hide:RemovalReason') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:RemovalReason') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonConfirmLoanRemoval'',''pickloanunit_buttonconfirmloanremoval_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ConfirmLoanRemoval','Confirm Loan Removal','button-entryfield',loc:formname,,,,loc:javascript,0,'images\packdelete.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PickLoanUnit_' & p_web._nocolon('buttonConfirmLoanRemoval') & '_value')

Comment::buttonConfirmLoanRemoval  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('buttonConfirmLoanRemoval') & '_comment',Choose(p_web.GSV('Hide:RemovalReason') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:RemovalReason') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::link:SMSHistory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('link:SMSHistory',p_web.GetValue('NewValue'))
    do Value::link:SMSHistory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::link:SMSHistory  Routine
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('link:SMSHistory') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','SMSHistory','SMS History','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormBrowseSMSHistory?' &'shReturnURL=PickLoanUnit')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()

Comment::link:SMSHistory  Routine
    loc:comment = ''
  p_web._DivHeader('PickLoanUnit_' & p_web._nocolon('link:SMSHistory') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('PickLoanUnit_tmp:LoanIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:LoanIMEINumber
      else
        do Value::tmp:LoanIMEINumber
      end
  of lower('PickLoanUnit_buttonPickLoanUnit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPickLoanUnit
      else
        do Value::buttonPickLoanUnit
      end
  of lower('PickLoanUnit_jobe2:LoanIDNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:LoanIDNumber
      else
        do Value::jobe2:LoanIDNumber
      end
  of lower('PickLoanUnit_locLoanIDOption_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locLoanIDOption
      else
        do Value::locLoanIDOption
      end
  of lower('PickLoanUnit_button:AmendDespatchDetails_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::button:AmendDespatchDetails
      else
        do Value::button:AmendDespatchDetails
      end
  of lower('PickLoanUnit_buttonRemoveAttachedUnit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonRemoveAttachedUnit
      else
        do Value::buttonRemoveAttachedUnit
      end
  of lower('PickLoanUnit_locRemovalAlertMessage_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locRemovalAlertMessage
      else
        do Value::locRemovalAlertMessage
      end
  of lower('PickLoanUnit_tmp:RemovalReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RemovalReason
      else
        do Value::tmp:RemovalReason
      end
  of lower('PickLoanUnit_buttonValidateAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonValidateAccessories
      else
        do Value::buttonValidateAccessories
      end
  of lower('PickLoanUnit_tmp:LostLoanFee_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:LostLoanFee
      else
        do Value::tmp:LostLoanFee
      end
  of lower('PickLoanUnit_buttonConfirmLoanRemoval_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonConfirmLoanRemoval
      else
        do Value::buttonConfirmLoanRemoval
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('PickLoanUnit_form:ready_',1)
  p_web.SetSessionValue('PickLoanUnit_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_PickLoanUnit',0)

PreCopy  Routine
  p_web.SetValue('PickLoanUnit_form:ready_',1)
  p_web.SetSessionValue('PickLoanUnit_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_PickLoanUnit',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('PickLoanUnit_form:ready_',1)
  p_web.SetSessionValue('PickLoanUnit_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('PickLoanUnit:Primed',0)

PreDelete       Routine
  p_web.SetValue('PickLoanUnit_form:ready_',1)
  p_web.SetSessionValue('PickLoanUnit_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('PickLoanUnit:Primed',0)
  p_web.setsessionvalue('showtab_PickLoanUnit',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('job:Loan_Unit_Number') > 0 AND p_web.GSV('ReadOnly:LoanIMEINumber') = 0
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('PickLoanUnit_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
    IF (p_web.GSV('Hide:LoanIDAlert') = 0)
        IF (p_web.GSV('locLoanIDOption') = 0)
            loc:alert = 'You must select an Loan ID option'
            loc:invalid = 'jobe2:LoanIDNumber'
            exit
        END
          
        Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
        jobe2:RefNumber = p_web.GSV('job:Ref_Number')
        IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
            CASE(p_web.GSV('locLoanIDOption'))
            OF 1
  !                  p_web.SSV('AddToAudit:Type','JOB')
  !                  p_web.SSV('AddToAudit:Action','LOAN ID NUMBER CHANGED')
  !                  p_web.SSV('AddToAudit:Notes','OLD ID NO: ' & Clip(jobe2:LoanIDNumber) & |
  !                      '<13,10>NEW ID NO: ' & p_web.GSV('jobe2:LoanIDNumber'))    
                AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','LOAN ID NUMBER CHANGED','OLD ID NO: ' & Clip(jobe2:LoanIDNumber) & |
                    '<13,10>NEW ID NO: ' & p_web.GSV('jobe2:LoanIDNumber'))   
                jobe2:LoanIDNumber = p_web.GSV('jobe2:LoanIDNumber')
                IF (Access:JOBSE2.TryUpdate() = Level:Benign)
  
                END
                  
            OF 2
  !                  p_web.SSV('AddToAudit:Type','JOB')
  !                  p_web.SSV('AddToAudit:Action','LOAN ID NUMBER CHANGED')
  !                  p_web.SSV('AddToAudit:Notes','OLD ID NO: ' & Clip(jobe2:LoanIDNumber) & |
  !                      '<13,10>NEW ID NO: ' & p_web.GSV('jobe2:LoanIDNumber'))    
                AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','LOAN ID NUMBER CHANGED','OLD ID NO: ' & Clip(jobe2:LoanIDNumber) & |
                    '<13,10>NEW ID NO: ' & p_web.GSV('jobe2:LoanIDNumber')) 
                    
  !                  p_web.SSV('AddToAudit:Type','JOB')
  !                  p_web.SSV('AddToAudit:Action','CUSTOMER ID NUMBER CHANGED')
  !                  p_web.SSV('AddToAudit:Notes','OLD ID NO: ' & Clip(jobe2:IDNumber) & |
  !                      '<13,10>NEW ID NO: ' & p_web.GSV('jobe2:LoanIDNumber'))    
                AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','CUSTOMER ID NUMBER CHANGED','OLD ID NO: ' & Clip(jobe2:IDNumber) & |
                    '<13,10>NEW ID NO: ' & p_web.GSV('jobe2:LoanIDNumber')) 
                    
                jobe2:LoanIDNumber = p_web.GSV('jobe2:LoanIDNumber')
                p_web.SSV('jobe2:IDNumber',p_web.GSV('jobe2:LoanIDNumber'))
                jobe2:IDNumber = p_web.GSV('jobe2:IDNumber')
                IF (Access:JOBSE2.TryUpdate() = Level:Benign)
  
                END
            END
              
        END
            
  
    END
    
    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber    = p_web.GSV('job:Ref_Number')
    if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
          ! Found
        p_web.SessionQueueToFile(JOBSE)
        access:JOBSE.tryUpdate()
    else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          ! Error
    end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)     
  
      if (p_web.GSV('tmp:LoanUnitNumber') > 0)
          if (p_web.GSV('tmp:LoanUnitNumber') <> p_web.GSV('job:Loan_Unit_Number'))
              do addLoanUnit
          end ! if (p_web.GSV('tmp:LoanUnitNumber') <> p_web.GSV('job:Loan_Unit_Number'))
      else ! if (p_web.GSV('tmp:LoanUnitNumber') > 0)
          if (p_web.GSV('tmp:NoUnitAvailable') = 1)
              p_web.SSV('GetStatus:Type','EXC')
              p_web.SSV('GetStatus:StatusNumber',350)
              getStatus(350,0,'EXC',p_web)
  
              p_web.SSV('GetStatus:Type','JOB')
              p_web.SSV('GetStatus:StatusNumber',355)
              getStatus(355,0,'JOB',p_web)
  
              local.AllocateLoanPart('ORD',0)
          end ! if (p_web.GSV('tmp:NoUnitAvailable') = 1)
      end ! if (p_web.GSV('tmp:LoanUnitNumber') > 0)
  
      do clearTagFile
  p_web.DeleteSessionValue('PickLoanUnit_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
          tmp:LoanIMEINumber = Upper(tmp:LoanIMEINumber)
          p_web.SetSessionValue('tmp:LoanIMEINumber',tmp:LoanIMEINumber)
        If loc:Invalid <> '' then exit.
  ! tab = 6
    loc:InvalidTab += 1
  ! tab = 5
    loc:InvalidTab += 1
          job:Loan_Consignment_Number = Upper(job:Loan_Consignment_Number)
          p_web.SetSessionValue('job:Loan_Consignment_Number',job:Loan_Consignment_Number)
        If loc:Invalid <> '' then exit.
          job:Loan_Despatched = Upper(job:Loan_Despatched)
          p_web.SetSessionValue('job:Loan_Despatched',job:Loan_Despatched)
        If loc:Invalid <> '' then exit.
          job:Loan_Despatch_Number = Upper(job:Loan_Despatch_Number)
          p_web.SetSessionValue('job:Loan_Despatch_Number',job:Loan_Despatch_Number)
        If loc:Invalid <> '' then exit.
  ! tab = 3
  If p_web.GSV('job:Loan_Unit_Number') > 0 AND p_web.GSV('ReadOnly:LoanIMEINumber') = 0
    loc:InvalidTab += 1
  End
  ! tab = 7
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
      do deleteVariables
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('PickLoanUnit:Primed',0)
  p_web.StoreValue('job:ESN')
  p_web.StoreValue('job:MSN')
  p_web.StoreValue('')
  p_web.StoreValue('job:Charge_Type')
  p_web.StoreValue('job:Warranty_Charge_Type')
  p_web.StoreValue('tmp:LoanIMEINumber')
  p_web.StoreValue('locLoanAlertMessage')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:MSN')
  p_web.StoreValue('tmp:LoanUnitDetails')
  p_web.StoreValue('tmp:LoanAccessories')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:LoanLocation')
  p_web.StoreValue('jobe:LoanReplacementValue')
  p_web.StoreValue('jobe2:LoanIDNumber')
  p_web.StoreValue('locLoanIDAlert')
  p_web.StoreValue('locLoanIDOption')
  p_web.StoreValue('job:Loan_Courier')
  p_web.StoreValue('job:Loan_Consignment_Number')
  p_web.StoreValue('job:Loan_Despatched')
  p_web.StoreValue('job:Loan_Despatched_User')
  p_web.StoreValue('job:Loan_Despatch_Number')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locRemovalAlertMessage')
  p_web.StoreValue('tmp:RemovalReason')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locValidateMessage')
  p_web.StoreValue('tmp:LostLoanFee')
  p_web.StoreValue('')
  p_web.StoreValue('')
Local.AllocateLoanPart        Procedure(String    func:Status,Byte func:SecondUnit)
local:FoundPart                 Byte(0)
Code
    If p_web.GSV('job:Warranty_Job') = 'YES'
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number  = p_web.GSV('job:Ref_Number')
        wpr:Part_Number = 'EXCH'
        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
        Loop
            If Access:WARPARTS.NEXT()
               Break
            End !If
            If wpr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
            Or wpr:Part_Number <> 'EXCH'      |
                Then Break.  ! End If
            If func:SecondUnit
            Else !If func:SecondUnit
                local:FoundPart = True
            End !If func:SecondUnit
            If local:FoundPart = True
                wpr:Status = func:Status
                If func:Status = 'PIK'
                    wpr:PartAllocated = 1
                End !If func:Status = 'PIK'
                Access:WARPARTS.Update()
                !Remove the EXCH line from Stock Allocation,
                !just incase the Loan isn't being added properly - L873 (DBH: 24-07-2003)
                RemoveFromStockAllocation(wpr:Record_Number,'WAR')
                Break
            End !If local:FoundPart = True
        End !Loop
    End !If job:Warranty_Job = 'YES'


    If local:FoundPart = False
        If p_web.GSV('job:Chargeable_Job') = 'YES'
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = p_web.GSV('job:Ref_Number')
            par:Part_Number = 'EXCH'
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                Or par:Part_Number <> 'EXCH'      |
                    Then Break.  ! End If
                If func:SecondUnit
                Else !If func:SecondUnit
                    local:FoundPart = True
                End !If func:SecondUnit
                If local:FoundPart = True
                    par:Status = func:Status
                    If func:Status = 'PIK'
                        par:PartAllocated = 1
                    End !If func:Status = 'PIK'
                    Access:PARTS.Update()
                    !Remove the EXCH line from Stock Allocation,
                    !just incase the Loan isn't being added properly - L873 (DBH: 24-07-2003)
                    RemoveFromStockAllocation(par:Record_Number,'CHA')
                    Break
                End !If local:FoundPart = True

            End !Loop
        End !If job:Chargeable_Job = 'YES'
    End !If local:FounPart = False

    If local:FoundPart = False
        If p_web.GSV('job:Engineer') = ''
            Access:USERS.Clearkey(use:Password_Key)
            use:Password    = p_web.GSV('BookingUserPassword')
            If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Found

            Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                !Error
            End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        Else !If job:Engineer = ''
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code   = p_web.GSV('job:Engineer')
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

        End !If job:Engineer = ''
        Access:STOCK.Clearkey(sto:Location_Key)
        sto:Location    = use:Location
        sto:Part_Number = 'EXCH'
        If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
            !Found
            Access:LOCATION.Clearkey(loc:Location_Key)
            loc:Location    = sto:Location
            If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Found
!                    If ~loc:UseRapidStock
!                        Return
!                    End !If ~loc:UseRapidStock

            Else ! If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                !Error
            End !If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
            If p_web.GSV('job:Warranty_Job') = 'YES'
               get(warparts,0)
               if access:warparts.primerecord() = level:benign
                   wpr:PArt_Ref_Number      = sto:Ref_Number
                   wpr:ref_number            = p_web.GSV('job:ref_number')
                   wpr:adjustment            = 'YES'
                   wpr:part_number           = 'EXCH'
                   wpr:description           = p_web.GSV('job:Manufacturer') & ' Loan UNIT'
                   wpr:quantity              = 1
                   wpr:warranty_part         = 'NO'
                   wpr:exclude_from_order    = 'YES'
                   wpr:PartAllocated         = 1
                   wpr:Status                = func:Status
!                   wpr:LoanUnit          = True
!                   wpr:SecondLoanUnit    = func:SecondUnit
                   If sto:Assign_Fault_Codes = 'YES'
                       !Try and get the fault codes. This key should get the only record
                       Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                       stm:Ref_Number  = sto:Ref_Number
                       stm:Part_Number = sto:Part_Number
                       stm:Location    = sto:Location
                       stm:Description = sto:Description
                       If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Found
                           wpr:Fault_Code1  = stm:FaultCode1
                           wpr:Fault_Code2  = stm:FaultCode2
                           wpr:Fault_Code3  = stm:FaultCode3
                           wpr:Fault_Code4  = stm:FaultCode4
                           wpr:Fault_Code5  = stm:FaultCode5
                           wpr:Fault_Code6  = stm:FaultCode6
                           wpr:Fault_Code7  = stm:FaultCode7
                           wpr:Fault_Code8  = stm:FaultCode8
                           wpr:Fault_Code9  = stm:FaultCode9
                           wpr:Fault_Code10 = stm:FaultCode10
                           wpr:Fault_Code11 = stm:FaultCode11
                           wpr:Fault_Code12 = stm:FaultCode12
                       Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                       !Error
                       !Assert(0,'<13,10>Fetch Error<13,10>')
                       End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                   end !If sto:Assign_Fault_Codes = 'YES'
                   if access:warparts.insert()
                       access:warparts.cancelautoinc()
                   end
                End !If Prime

             Else !If job:Warranty_Job = 'YES'
                If p_web.GSV('job:Chargeable_Job') = 'YES'
                    get(parts,0)
                    if access:parts.primerecord() = level:benign
                        !message('At break2')
                        par:PArt_Ref_Number      = sto:Ref_Number
                        par:ref_number            = p_web.GSV('job:ref_number')
                        par:adjustment            = 'YES'
                        par:part_number           = 'EXCH'
                        par:description           = p_web.GSV('job:Manufacturer') & ' Loan UNIT'
                        par:quantity              = 1
                        par:warranty_part         = 'NO'
                        par:exclude_from_order    = 'YES'
                        par:PartAllocated         = 1
                        par:Status                = func:Status
!                        par:LoanUnit          = True
!                        par:SecondLoanUnit    = func:SecondUnit
                        If sto:Assign_Fault_Codes = 'YES'
                           !Try and get the fault codes. This key should get the only record
                           Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                           stm:Ref_Number  = sto:Ref_Number
                           stm:Part_Number = sto:Part_Number
                           stm:Location    = sto:Location
                           stm:Description = sto:Description
                           If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Found
                               par:Fault_Code1  = stm:FaultCode1
                               par:Fault_Code2  = stm:FaultCode2
                               par:Fault_Code3  = stm:FaultCode3
                               par:Fault_Code4  = stm:FaultCode4
                               par:Fault_Code5  = stm:FaultCode5
                               par:Fault_Code6  = stm:FaultCode6
                               par:Fault_Code7  = stm:FaultCode7
                               par:Fault_Code8  = stm:FaultCode8
                               par:Fault_Code9  = stm:FaultCode9
                               par:Fault_Code10 = stm:FaultCode10
                               par:Fault_Code11 = stm:FaultCode11
                               par:Fault_Code12 = stm:FaultCode12
                           Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                               !Error
                               !Assert(0,'<13,10>Fetch Error<13,10>')
                           End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                        End !If sto:Assign_Fault_Codes = 'YES'
                        if access:parts.insert()
                            access:parts.cancelautoinc()
                        end
                    End !If access:Prime

                End !If job:Chargeable_Job = 'YES'
             End !If job:Warranty_Job = 'YES'

        Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
             !Error
!            Case Missive('You must have a Part setup in Stock Control with a part number of "EXCH" under the location ' & Clip(use:Location) & '.','ServiceBase 3g',|
!                           'mstop.jpg','/OK')
!                Of 1 ! OK Button
!            End ! Case Missive
        End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
    End !If local:FounPart = False
