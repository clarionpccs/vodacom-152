

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER208.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER148.INC'),ONCE        !Req'd for module callout resolution
                     END


frmChangeStockStatus PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locStockRefNo        LONG                                  !
FoundAnyPart         BYTE                                  !
FoundOrderedPart     BYTE                                  !
locJobNumber         LONG                                  !
RapidQueue           QUEUE,PRE(rapque)                     !
SessionID            LONG                                  !
RecordNumber         LONG                                  !
                     END                                   !
RapidJobQueue        QUEUE,PRE(jobque)                     !
SessionID            LONG                                  !
JobNumber            LONG                                  !
                     END                                   !
                    MAP
AddFaulty               Procedure(String  func:Type)
                    END
FilesOpened     Long
STOFAULT::State  USHORT
USERS::State  USHORT
STOCK::State  USHORT
JOBS::State  USHORT
WARPARTS::State  USHORT
PARTS::State  USHORT
ESTPARTS::State  USHORT
STOCKALL::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('frmChangeStockStatus')
  loc:formname = 'frmChangeStockStatus_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'frmChangeStockStatus',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('frmChangeStockStatus','')
    p_web._DivHeader('frmChangeStockStatus',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferfrmChangeStockStatus',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmChangeStockStatus',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmChangeStockStatus',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmChangeStockStatus',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmChangeStockStatus',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_frmChangeStockStatus',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'frmChangeStockStatus',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(STOFAULT)
  p_web._OpenFile(USERS)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(ESTPARTS)
  p_web._OpenFile(STOCKALL)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOFAULT)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(ESTPARTS)
  p_Web._CloseFile(STOCKALL)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('frmChangeStockStatus_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine

RestoreMem       Routine
  !FormSource=Memory

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('frmChangeStockStatus_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  IF (p_web.IfExistsValue('STA'))
      p_web.StoreValue('STA')
  END
  
  Access:STOCKALL.Clearkey(stl:RecordNumberKey)
  stl:RecordNumber    = p_web.GSV('stl:RecordNumber')
  Access:STOCKALL.Tryfetch(stl:RecordNumberKey)
  IF (p_web.GSV('STA') = 'PIK' OR (p_web.GSV('STA') = 'PRO'))
  
          !Found
      p_web.SSV('txtErrorText','')
      p_web.SSV('txtRequestFulfilled','The selected part has been updated.')
      If stl:PartNumber = 'EXCH'
          p_web.SSV('txtErrorText','The selected part in an "Exchange Part".')
          p_web.SSV('txtRequestFulfilled','')
      Else !If stl:PartNumber = 'EXCH'
          Error# = 0
  
          If stl:Status = 'WEB'
              p_web.SSV('txtErrorText','The selected part is On Order.')
              p_web.SSV('txtRequestFulfilled','')
              Error# = 1
          End !If stl:Status = 'WEB'
  
          If Error# = 0
              Case stl:PartType
              Of 'CHA'
                  Access:PARTS.Clearkey(par:recordnumberkey)
                  par:Record_Number   = stl:PartRecordNumber
                  If Access:PARTS.Tryfetch(par:recordnumberkey) = Level:Benign
                              !Found
                      par:Status  = p_web.GSV('STA')
                      Access:PARTS.Update()
                  Else ! If Access:PARTS.Tryfetch(par:Record_Number_Key) = Level:Benign
                              !Error
                      Error# = 1
                  End !If AccessPARTS.Tryfetch(parRecord_Number_Key) = LevelBenign
              Of 'WAR'
                  Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                  wpr:Record_Number   = stl:PartRecordNumber
                  If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                              !Found
                      wpr:Status  = p_web.GSV('STA')
                      Access:WARPARTS.Update()
                  Else ! If Access:WARPARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
                              !Error
                      Error# = 1
                  End !If AccessWARPARTS.Tryfetch(wprRecord_Number_Key) = LevelBenign
              Of 'EST'
                  Access:ESTPARTS.Clearkey(epr:Record_Number_Key)
                  epr:Record_Number   = stl:PartRecordNumber
                  If Access:ESTPARTS.Tryfetch(epr:Record_Number_Key) = Level:Benign
                              !Found
                      epr:Status = p_web.GSV('STA')
                      Access:ESTPARTS.Update()
                  Else ! If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
                              !Error
                  End !If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
              End !Case rapsto:PartType
              If Error# = 0
                  stl:Status = p_web.GSV('STA')
                  Access:STOCKALL.Update()
  
              End !If Error# = 0
  
          End !If Error# = 0
      End !If stl:PartNumber = 'EXCH'
  END
  IF (p_web.GSV('STA') = 'ALL')
      If stl:PartNumber = 'EXCH'
          p_web.SSV('txtErrorText','The selected part in an "Exchange Part".')
          p_web.SSV('txtRequestFulfilled','')
      Else !If stl:PartNumber = 'EXCH'
          Error# = 0
          If stl:Status = 'WEB'
              p_web.SSV('txtErrorText','The selected part in On Order.')
              p_web.SSV('txtRequestFulfilled','')
              Error# = 1
          End !If stl:Status = 'WEB'
          If Error# = 0
              p_web.SSV('txtErrorText','')
              p_web.SSV('txtRequestFulfilled','The selected part has been allocated.')
              Case stl:PartType
              Of 'CHA'
                  Access:PARTS.Clearkey(par:recordnumberkey)
                  par:Record_Number   = stl:PartRecordNumber
                  If Access:PARTS.Tryfetch(par:recordnumberkey) = Level:Benign
                          !Found
                      Access:STOCK.Clearkey(sto:Ref_Number_Key)
                      sto:Ref_Number  = par:Part_Ref_Number
                      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Found
                          If sto:ReturnFaultySpare
                              p_web.SSV('txtErrorText','.')
                              p_web.SSV('txtRequestFulfilled','Part Updated. The faulty part must be returned before a new part can be issued.')
                              AddFaulty('C')
                          End !If sto:ReturnFaultySpare
                      Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Error
                      End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      If Error# = 0
                          par:PartAllocated = 1
                          par:WebOrder = 0
                          Access:PARTS.Update()
                      End !If Error# = 0
                  Else ! If Access:PARTS.Tryfetch(par:Record_Number_Key) = Level:Benign
                          !Error
                      Error# = 1
                  End !If AccessPARTS.Tryfetch(parRecord_Number_Key) = LevelBenign
              Of 'WAR'
                  Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                  wpr:Record_Number   = stl:PartRecordNumber
                  If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                          !Found
                      Access:STOCK.Clearkey(sto:Ref_Number_Key)
                      sto:Ref_Number  = wpr:Part_Ref_Number
                      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Found
                          If sto:ReturnFaultySpare
                              p_web.SSV('txtErrorText','.')
                              p_web.SSV('txtRequestFulfilled','Part Updated. The faulty part must be returned before a new part can be issued.')
  
                              AddFaulty('W')
                          End !If sto:ReturnFaultySpare
  
                      Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Error
                      End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      If Error# = 0
                          wpr:PartAllocated = 1
                          wpr:WebOrder = 0
                          Access:WARPARTS.Update()
                      End !If Error# = 0
  
                  Else ! If Access:WARPARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
                          !Error
                      Error# = 1
                  End !If AccessWARPARTS.Tryfetch(wprRecord_Number_Key) = LevelBenign
              Of 'EST'
                  Access:ESTPARTS.Clearkey(epr:Record_Number_Key)
                  epr:Record_Number   = stl:PartRecordNumber
                  If Access:ESTPARTS.Tryfetch(epr:Record_Number_Key) = Level:Benign
                          !Found
                      Access:STOCK.Clearkey(sto:Ref_Number_Key)
                      sto:Ref_Number  = epr:Part_Ref_Number
                      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Found
                          If sto:ReturnFaultySpare
                              p_web.SSV('txtErrorText','.')
                              p_web.SSV('txtRequestFulfilled','Part Updated. The faulty part must be returned before a new part can be issued.')
  
                              AddFaulty('E')
                          End !If sto:ReturnFaultySpare
  
                      Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Error
                      End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      If Error# = 0
                          epr:PartAllocated = 1
                          Access:ESTPARTS.Update()
                      End !If Error# = 0
  
                  Else ! If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
                          !Error
                      Error# = 1
                  End !If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
  
              End !Case stl:PartType
              If Error# = 0
                  Relate:STOCKALL.Delete(0)
              End !If Error# = 0
  
  
              !Added 05/12/02 - L393 / VP109 -
              !If all warranty/chargeable/estimate parts are now allocated - change job status to 315 - In Repair
              FoundAnyPart = false
  
              !first check the chargeable parts
              access:Parts.clearkey(par:Part_Number_Key)
              par:Ref_Number = stl:JobNumber
              set(par:Part_Number_Key,par:Part_Number_Key)
              Loop !to find an unallocated part
                  if access:Parts.next() then
                      !leave no more jobs
                      break
                  ELSE
                      if par:Ref_Number <> stl:JobNumber then
                          !Leave no more matching jobs
                          break
                      ELSE
                          !found a part for this job
                          if par:PartAllocated then
                              !ignore this it was allocated
                          ELSE
                              !found an unallocated part!
                              FoundAnyPart = true
                              break
                          END !if partAllocated
                      END !If ref_numbers disagree
                  END !if access:jobs.next()
              End !loop to find an unallocated part
  
              if FoundAnyPart = false
                  !Check warranty parts
                  access:warParts.clearkey(wpr:Part_Number_Key)
                  wpr:Ref_Number = stl:JobNumber
                  set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                  Loop !to find an unallocated part
                      if access:WarParts.next() then
                          !leave no more jobs
                          break
                      ELSE
                          if wpr:Ref_Number <> stl:JobNumber then
                              !Leave no more matching jobs
                              break
                          ELSE
                              !found a part for this job
                              if wpr:PartAllocated then
                                  !ignore this it was allocated
                              ELSE
                                  !found an unallocated part!
                                  FoundAnyPart = true
                                  break
                              END !if partAllocated
                          END !If ref_numbers disagree
                      END !if access:jobs.next()
                  End !loop to find an unallocated part
  
              END !if I haven't FoundAnyPart
  
              if FoundAnyPart = false
                  !Check estimate parts
                  access:EstParts.clearkey(epr:Part_Number_Key)
                  epr:Ref_Number = stl:JobNumber
                  set(epr:Part_Number_Key,epr:Part_Number_Key)
                  Loop !to find an unallocated part
                      if access:EstParts.next() then
                          !leave no more jobs
                          break
                      ELSE
                          if epr:Ref_Number <> stl:JobNumber then
                              !Leave no more matching jobs
                              break
                          ELSE
                              !found a part for this job
                              if epr:PartAllocated then
                                  !ignore this it was allocated
                              ELSE
                                  !found an unallocated part!
                                  FoundAnyPart = true
                                  break
                              END !if partAllocated
                          END !If ref_numbers disagree
                      END !if access:jobs.next()
                  End !loop to find an unallocated part
              END !if I haven't FoundAnyPart
  
              if FoundAnyPart = false
                   !change job status to 315
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = stl:JobNumber
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      Error# = 0
  
                      Pointer# = Pointer(JOBS)
                      Hold(JOBS,1)
                      Get(JOBS,Pointer#)
                      If Errorcode() = 43
  
                          Error# = 1
                !          !Post(Event:CloseWindow)
                      ELSE !If Errorcode() = 43
                            !Found
                          getstatus(315,0,'JOB')
                          Access:JOBS.TryUpdate()
                      End  !If Errorcode() = 43
                      Release(JOBS)
                  END !if tryfetch on jobs
              END !if I haven't FoundAnyPart
  
  
          !end of Added 05/12/02 - L393 / VP109 -
  
          End !If Error# = 0
      End !If stl:PartNumber = 'EXCH'
  
  END
      p_web.site.SaveButton.TextValue = 'OK'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'FormStockAllocation'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('frmChangeStockStatus_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('frmChangeStockStatus_ChainTo')
    loc:formaction = p_web.GetSessionValue('frmChangeStockStatus_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="frmChangeStockStatus" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="frmChangeStockStatus" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="frmChangeStockStatus" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Fulfil Request') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Fulfil Request',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_frmChangeStockStatus">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_frmChangeStockStatus" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_frmChangeStockStatus')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Fulfil Request') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_frmChangeStockStatus')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_frmChangeStockStatus'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_frmChangeStockStatus')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Fulfil Request') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_frmChangeStockStatus_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Fulfil Request')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Fulfil Request')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Fulfil Request')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Fulfil Request')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::txtRequestFulfilled
      do Comment::txtRequestFulfilled
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtErrorText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::txtErrorText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::txtRequestFulfilled  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtRequestFulfilled',p_web.GetValue('NewValue'))
    do Value::txtRequestFulfilled
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtRequestFulfilled  Routine
  p_web._DivHeader('frmChangeStockStatus_' & p_web._nocolon('txtRequestFulfilled') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold LargeText')&'">' & p_web.Translate(p_web.GSV('txtRequestFulfilled'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::txtRequestFulfilled  Routine
    loc:comment = ''
  p_web._DivHeader('frmChangeStockStatus_' & p_web._nocolon('txtRequestFulfilled') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::txtErrorText  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtErrorText',p_web.GetValue('NewValue'))
    do Value::txtErrorText
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtErrorText  Routine
  p_web._DivHeader('frmChangeStockStatus_' & p_web._nocolon('txtErrorText') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('red bold LargeText')&'">' & p_web.Translate(p_web.GSV('txtErrorText'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::txtErrorText  Routine
    loc:comment = ''
  p_web._DivHeader('frmChangeStockStatus_' & p_web._nocolon('txtErrorText') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('frmChangeStockStatus_form:ready_',1)
  p_web.SetSessionValue('frmChangeStockStatus_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_frmChangeStockStatus',0)

PreCopy  Routine
  p_web.SetValue('frmChangeStockStatus_form:ready_',1)
  p_web.SetSessionValue('frmChangeStockStatus_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_frmChangeStockStatus',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('frmChangeStockStatus_form:ready_',1)
  p_web.SetSessionValue('frmChangeStockStatus_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('frmChangeStockStatus:Primed',0)

PreDelete       Routine
  p_web.SetValue('frmChangeStockStatus_form:ready_',1)
  p_web.SetSessionValue('frmChangeStockStatus_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('frmChangeStockStatus:Primed',0)
  p_web.setsessionvalue('showtab_frmChangeStockStatus',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('frmChangeStockStatus_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('frmChangeStockStatus_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('frmChangeStockStatus:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locStockRefNo',locStockRefNo) ! LONG
     p_web.SSV('FoundAnyPart',FoundAnyPart) ! BYTE
     p_web.SSV('FoundOrderedPart',FoundOrderedPart) ! BYTE
     p_web.SSV('locJobNumber',locJobNumber) ! LONG
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locStockRefNo = p_web.GSV('locStockRefNo') ! LONG
     FoundAnyPart = p_web.GSV('FoundAnyPart') ! BYTE
     FoundOrderedPart = p_web.GSV('FoundOrderedPart') ! BYTE
     locJobNumber = p_web.GSV('locJobNumber') ! LONG
AddFaulty     Procedure(String  func:Type)
Code
    If Access:STOFAULT.PrimeRecord() = Level:Benign
        Case func:Type
        Of 'W'
            stf:PartNumber  = wpr:Part_Number
            stf:Description = wpr:Description
            stf:Quantity    = wpr:Quantity
            stf:PurchaseCost    = wpr:Purchase_Cost
        Of 'C'
            stf:PartNumber  = par:Part_Number
            stf:Description = par:Description
            stf:Quantity    = par:Quantity
            stf:PurchaseCost    = par:Purchase_Cost
        Of 'E'
            stf:PartNumber  = epr:Part_Number
            stf:Description = epr:Description
            stf:Quantity    = epr:Quantity
            stf:PurchaseCost    = epr:Purchase_Cost
        End !Case func:Type

        stf:ModelNumber = job:Model_Number
        stf:IMEI        = job:ESN
        stf:Engineer    = job:Engineer
        stf:StoreUserCode   = p_web.GSV('BookingUserCode')
        stf:PartType        = 'FAU'
        If Access:STOFAULT.TryInsert() = Level:Benign
            !Insert Successful

        Else !If Access:STOFAULT.TryInsert() = Level:Benign
            !Insert Failed
            Access:STOFAULT.CancelAutoInc()
        End !If Access:STOFAULT.TryInsert() = Level:Benign
    End !If Access:STOFAULT.PrimeRecord() = Level:Benign
