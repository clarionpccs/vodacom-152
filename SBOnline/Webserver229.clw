

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER229.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ClearGenericTagFile  PROCEDURE  (NetWebServerWorker p_web,<STRING pTagType>) ! Declare Procedure
skipUpdate                  LONG

  CODE
        Relate:SBO_GenericTagFile.Open()
        STREAM(SBO_GenericTagFile)
        Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
        tagf:SessionID  = p_web.SessionID
        IF (pTagType <> '')
            tagf:TagType    = pTagType
        END ! IF
        SET(tagf:KeyTagged,tagf:KeyTagged)
        LOOP UNTIL Access:SBO_GenericTagFile.Next() <> Level:Benign
            IF (tagf:SessionID <> p_web.SessionID)
                BREAK
            END
            IF (pTagType <> '' AND tagf:TagType <> pTagType)
                BREAK
            END ! IF
            Access:SBO_GenericTagFile.DeleteRecord(0)
            IF (tagf:Tagged <> 0)
                tagf:Tagged = 0  ! Thinking that it's quicker to put that to delete
                Access:SBO_GenericTagFile.TryUpdate()
                skipUpdate += 1
!                IF (skipUpdate > 200)
!                !p_web.Noop()
!                    skipUpdate = 0
!                END ! IF
            END ! IF
            
        END ! IF      
        FLUSH(SBO_GenericTagFile)
        Relate:SBO_GenericTagFile.Close()
