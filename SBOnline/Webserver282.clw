

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER282.INC'),ONCE        !Local module procedure declarations
                     END


CreateCreditNote     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locCreditType        BYTE                                  !
locTotalValue        REAL                                  !
locCreditAmount      REAL                                  !
locCreditMessage     STRING(100)                           !
CreditAmount         DECIMAL(10,2)                         !
JobTotal             DECIMAL(10,2)                         !
FilesOpened     Long
JOBPAYMT::State  USHORT
CHARTYPE::State  USHORT
STOCK::State  USHORT
PARTS::State  USHORT
JOBSINV::State  USHORT
INVOICE::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('CreateCreditNote')
  loc:formname = 'CreateCreditNote_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'CreateCreditNote',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('CreateCreditNote','')
    p_web._DivHeader('CreateCreditNote',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferCreateCreditNote',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCreateCreditNote',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCreateCreditNote',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_CreateCreditNote',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCreateCreditNote',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_CreateCreditNote',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'CreateCreditNote',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
CheckCreditAmount   ROUTINE
    IF (p_web.GSV('locCreditType') = 0)
        p_web.SSV('Hide:CreateCreditNoteButton',0)
    ELSE
        locCreditAmount = p_web.GSV('locCreditAmount')
        locTotalValue = p_web.GSV('locTotalValue')
        IF (locCreditAmount > locTotalValue OR locTotalValue = 0 OR locCreditAmount = 0)
            p_web.SSV('Comment:CreditAmount','Invalid Amount')
            p_web.SSV('Hide:CreateCreditNoteButton',1)
        ELSE
            p_web.SSV('Hide:CreateCreditNoteButton',0)
            p_web.SSV('Comment:CreditAmount','Required')
        END
        
    END
    
    
    
CreateCreditNote    ROUTINE
    ! Find the last credit note
    p_web.SSV('CreditNoteCreated',0)
    p_web.SSV('LastSuffix','')
    p_web.SSV('locCreditMessage','')
    found# = 0
    Access:JOBSINV.ClearKey(jov:TypeRecordKey)
    jov:RefNumber = p_web.GSV('job:Ref_Number')
    jov:Type = 'C'
    jov:RecordNumber = 0
    SET(jov:TypeRecordKey,jov:TypeRecordKey)
    LOOP UNTIL Access:JOBSINV.Next()
        IF (jov:RefNumber <> p_web.GSV('job:Ref_Number'))
            BREAK
        END
        IF (jov:Type <> 'C')
            BREAK
        END
        p_web.SSV('LastSuffix',jov:Suffix)
        found# = 1    
    END
    IF (p_web.GSV('LastSuffix') = '')
        IF (found# = 0)
            p_web.SSV('NextSuffix','')
        ELSE
            p_web.SSV('NextSuffix','A')
        END
    ELSE
        p_web.SSV('LastSuffixNumber',Val(p_web.GSV('LastSuffix')))
        p_web.SSV('NextSuffix',chr(p_web.GSV('LastSuffixNumber') + 1))
    END
    
    IF (Access:JOBSINV.PrimeRecord() = Level:Benign)
        jov:BookingAccount      = p_web.GSV('wob:HeadAccountNumber')
        jov:UserCode            = p_web.GSV('BookingUserCode')
        jov:Type                = 'C'
        jov:RefNumber           = p_web.GSV('job:Ref_Number')
        jov:InvoiceNumber       = p_web.GSV('job:Invoice_Number')
        jov:CreditAmount        = p_web.GSV('locCreditAmount')
        jov:NewTotalCost        = p_web.GSV('locTotalValue') - p_web.GSV('locCreditAmount')
        jov:Suffix              = p_web.GSV('NextSuffix')
        jov:OriginalTotalCost   = p_web.GSV('JobTotal')
        jov:NewInvoiceNumber    = ''
        jov:ChargeType          = p_web.GSV('job:Charge_Type')
        jov:RepairType          = p_web.GSV('job:Repair_Type')
        jov:HandlingFee         = p_web.GSV('HandlingFee')
        jov:ExchangeRate        = p_web.GSV('ExchangeRate')
        jov:ARCCharge           = p_web.GSV('ARCCharge')
        jov:RRCLostLoanCost     = p_web.GSV('RRCLostLoanCost')
        jov:RRCPartsCost        = p_web.GSV('PartsCost')
        jov:RRCPartsSelling     = p_web.GSV('jobe:InvRRCCPartsCost')
        jov:RRCLabour           = p_web.GSV('jobe:InvRRCCLabourCost')
        jov:ARCMarkUp           = p_web.GSV('ARCMarkup')
        jov:RRCVAT              = p_web.GSV('job:Invoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
            p_web.GSV('jobe:InvRRCCLabourCost') * (inv:Vat_Rate_Labour/100) + |
            p_web.GSV('jobe:InvRRCCPartsCost') * (inv:Vat_Rate_Parts/100)
        jov:Paid                = p_web.GSV('Paid')
        jov:Outstanding         = p_web.GSV('JobTotal') - p_web.GSV('Paid')
        jov:Refund              = p_web.GSV('Refund')
        IF (Access:JOBSINV.TryInsert() = Level:Benign)
            p_web.SSV('CreditSuffix',jov:Suffix)
            p_web.SSV('CreditRecordNumber',jov:RecordNumber)
        ELSE
            Access:JOBSINV.CancelAutoInc()
        END
        
    END

    ! Find the last invoice number
    p_web.SSV('LastSuffix','')
    Access:JOBSINV.ClearKey(jov:TypeRecordKey)
    jov:RefNumber = p_web.GSV('job:Ref_Number')
    jov:Type = 'I'
    jov:RecordNumber = 0
    SET(jov:TypeRecordKey,jov:TypeRecordKey)
    LOOP UNTIL Access:JOBSINV.Next()
        IF (jov:RefNumber <> p_web.GSV('job:Ref_Number'))
            BREAK
        END
        IF (jov:Type <> 'I')
            BREAK
        END
        p_web.SSV('LastSuffix',jov:Suffix)
    END
    
    IF (p_web.GSV('LastSuffix') = '')
        p_web.SSV('LastSuffix','A')
    ELSE
        p_web.SSV('LastSuffixNumber',Val(p_web.GSV('LastSuffix')))
        p_web.SSV('NextSuffix',chr(p_web.GSV('LastSuffixNumber') + 1))
    END
    
    IF (Access:JOBSINV.PrimeRecord() = Level:Benign)
        jov:BookingAccount      = p_web.GSV('wob:HeadAccountNumber')
        jov:UserCode            = p_web.GSV('BookingUserCode')
        jov:Type                = 'I'
        jov:RefNumber           = p_web.GSV('job:Ref_Number')
        jov:InvoiceNumber       = p_web.GSV('job:Invoice_Number')
        jov:CreditAmount        = p_web.GSV('locCreditAmount')
        jov:NewTotalCost        = p_web.GSV('locTotalValue') - p_web.GSV('locCreditAmount')
        jov:Suffix              = p_web.GSV('NextSuffix')
        jov:OriginalTotalCost   = p_web.GSV('JobTotal')
        jov:NewInvoiceNumber    = ''
        jov:ChargeType          = p_web.GSV('job:Charge_Type')
        jov:RepairType          = p_web.GSV('job:Repair_Type')
        jov:HandlingFee         = p_web.GSV('HandlingFee')
        jov:ExchangeRate        = p_web.GSV('ExchangeRate')
        jov:ARCCharge           = p_web.GSV('ARCCharge')
        jov:RRCLostLoanCost     = p_web.GSV('RRCLostLoanCharge')
        jov:RRCPartsCost        = p_web.GSV('PartsCost')
        jov:RRCPartsSelling     = p_web.GSV('jobe:InvRRCCPartsCost')
        jov:RRCLabour           = p_web.GSV('jobe:InvRRCCLabourCost')
        jov:ARCMarkup           = p_web.GSV('ARCMarkup')
        jov:RRCVat              = p_web.GSV('job:Invoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
                            p_web.GSV('jobe:InvRRCCLabourCost') * (inv:Vat_Rate_Labour/100) + |
                            p_web.GSV('jobe:InvRRCCPartsCost') * (inv:Vat_Rate_Parts/100)
        jov:Paid                = p_web.GSV('Paid')
        jov:Outstanding         = p_web.GSV('JobTotal') - p_web.GSV('Paid')
        jov:Refund              = p_web.GSV('Refund')
        IF (Access:JOBSINV.TryInsert() = Level:Benign)
            p_web.SSV('InvoiceSuffix',jov:Suffix)
            p_web.SSV('NewInvoiceNumber',CLIP(jov:InvoiceNumber) & '-' & | 
                Clip(tra:BranchIdentification) & Clip(jov:Suffix))
                    
            ! Record the associated invoice number on the credit note
            Access:JOBSINV.ClearKey(jov:RecordNumberKey)
            jov:RecordNumber = p_web.GSV('CreditRecordNumber')
            IF (Access:JOBSINV.TryFetch(jov:RecordNumberKey) = Level:Benign)
                jov:NewInvoiceNumber = p_web.GSV('NewInvoiceNumber')
                Access:JOBSINV.TryUpdate()
                
            END
            p_web.SSV('CreditNoteCreated',1)
        ELSE
            Access:JOBSINV.CancelAutoInc()
        END
        
    END
    
    p_web.SSV('locCreditMessage','Credit Note Created!')
    IF (p_web.GSV('JobTotal') - p_web.GSV('Paid') > 0) AND (p_web.GSV('Paid') > 0 AND | 
        p_web.GSV('Paid') <> p_web.GSV('Refund'))
        p_web.SSV('locCreditMessage','Credit Note Created! <br/>There is payment allocated to this job. If necessary, a' & | 
            ' refund should be issued to the customer.')
        
    END
    
    
        
DeleteSessionValues ROUTINE
    p_web.DeleteSessionValue('Hide:CreateCreditNoteButton')
    p_web.DeleteSessionValue('Comment:CreditAmount')
    p_web.DeleteSessionValue('CreditCreditNote')
    p_web.DeleteSessionValue('locCreditMessage')
    p_web.DeleteSessionValue('locCreditType')
    p_web.DeleteSessionValue('locTotalValue')
    p_web.DeleteSessionValue('locCreditAmount')
    p_web.DeleteSessionValue('CreditNoteCreated')
    
OpenFiles  ROUTINE
  p_web._OpenFile(JOBPAYMT)
  p_web._OpenFile(CHARTYPE)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(JOBSINV)
  p_web._OpenFile(INVOICE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBPAYMT)
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(JOBSINV)
  p_Web._CloseFile(INVOICE)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('CreateCreditNote_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  DO DeleteSessionValues

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('locTotalValue')
    p_web.SetPicture('locTotalValue','@n-14.2')
  End
  p_web.SetSessionPicture('locTotalValue','@n-14.2')
  If p_web.IfExistsValue('locCreditAmount')
    p_web.SetPicture('locCreditAmount','@n-14.2')
  End
  p_web.SetSessionPicture('locCreditAmount','@n-14.2')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locTotalValue',locTotalValue)
  p_web.SetSessionValue('locCreditType',locCreditType)
  p_web.SetSessionValue('locCreditAmount',locCreditAmount)
  p_web.SetSessionValue('locCreditMessage',locCreditMessage)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locTotalValue')
    locTotalValue = p_web.dformat(clip(p_web.GetValue('locTotalValue')),'@n-14.2')
    p_web.SetSessionValue('locTotalValue',locTotalValue)
  End
  if p_web.IfExistsValue('locCreditType')
    locCreditType = p_web.GetValue('locCreditType')
    p_web.SetSessionValue('locCreditType',locCreditType)
  End
  if p_web.IfExistsValue('locCreditAmount')
    locCreditAmount = p_web.dformat(clip(p_web.GetValue('locCreditAmount')),'@n-14.2')
    p_web.SetSessionValue('locCreditAmount',locCreditAmount)
  End
  if p_web.IfExistsValue('locCreditMessage')
    locCreditMessage = p_web.GetValue('locCreditMessage')
    p_web.SetSessionValue('locCreditMessage',locCreditMessage)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('CreateCreditNote_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  p_web.site.CancelButton.TextValue = 'Close'
    p_web.SSV('Hide:CreateCreditNoteButton',0)
    p_web.SSV('Comment:CreditAmount','Required')
  
    ! Is there anything left to credit?
    Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
    inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
    If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
        !Found
    Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
        !Error
    End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
    
    JobTotal = p_web.GSV('jobe:InvRRCCSubTotal') + (p_web.GSV('job:Invoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
                    p_web.GSV('jobe:InvRRCCLabourCost') * (inv:Vat_Rate_Labour/100) + |
                    p_web.GSV('jobe:InvRRCCPartsCOst') * (inv:Vat_Rate_Labour/100))
  
    
    CreditAmount = 0
    
    Access:JOBSINV.ClearKey(jov:TypeRecordKey)
    jov:RefNumber = p_web.GSV('job:Ref_Number')
    jov:Type = 'C'
    SET(jov:TypeRecordKey,jov:TypeRecordKey)
    LOOP UNTIL Access:JOBSINV.Next()
        IF (jov:RefNumber <> p_web.GSV('job:Ref_Number'))
            BREAK
        END
        IF (jov:Type <> 'C')
            BREAK
        END
        CreditAmount += jov:CreditAmount
      
    END
    
    IF (CreditAmount > JobTotal)
        CreateScript(p_web,packet,'alert("There is nothing remaining to credit on the selected job.")')
        CreateScript(p_web,packet,'window.open("DisplayBrowsePayments","_self")')
        DO Sendpacket
        EXIT
    END
  
    SentToHub(p_web)
    p_web.SSV('ExchangeRate',0)
    p_web.SSV('HandlingFee',0)
    IF (p_web.GSV('job:Exchange_Unit_Number') > 0)
        IF (p_web.GSV('jobe:ExchangedAtRRC') = 1)
            p_web.SSV('ExchangeRate',p_web.GSV('jobe:InvoiceExchangeRate'))
        ELSE
            p_web.SSV('HandlingFee',p_web.GSV('jobe:InvoiceHandlingFee'))
        END
    ELSE
        IF (p_web.GSV('SentToHub') = 1)
            p_web.SSV('HandlingFee',p_web.GSV('jobe:InvoiceHandlingFee'))
        END
    END
  
    ! Get ARC Charges
    p_web.SSV('ARCCharge',0)
    p_web.SSV('ARCPartsCost',0)
    IF (p_web.GSV('SentToHub') = 1)
        IF (inv:ARCInvoiceDate > 0)
            p_web.SSV('ARCCharge',p_web.GSV('job:Invoice_Courier_Cost') + p_web.GSV('job:Invoice_Parts_Cost') + p_web.GSV('job:Invoice_Labour_Cost') + |
                            p_web.GSV('job:Invoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
                            p_web.GSV('job:Invoice_Parts_Cost') * (inv:Vat_Rate_Parts/100) + |
                            p_web.GSV('job:Invoice_Labour_Cost') * (inv:Vat_Rate_Labour/100))
            p_web.SSV('ARCPartsCost',p_web.GSV('job:Invoice_Parts_Cost'))
        ELSE
            p_web.SSV('ARCCharge',p_web.GSV('job:Courier_Cost') + p_web.GSV('job:Labour_Cost') + p_web.GSV('job:Parts_Cost') + |
                            (p_web.GSV('job:Courier_Cost') + (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)) + |
                            (p_web.GSV('job:Parts_Cost') + (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                            (p_web.GSV('job:Labour_Cost') + (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)))
            p_web.SSV('ARCPartsCost',p_web.GSV('job:Parts_Cost'))
                  
        END
    END
  
    ! Get Loan Charges
    p_web.SSV('RRCLostLoanCharge',0)
    LoanAttachedToJob(p_web)
    IF (p_web.GSV('LoanAttAchedToJob') = 1)
        IF (p_web.GSV('SentToHub') <> 1)
            p_web.SSV('RRCLostLoanCharge',p_web.GSV('job:Invoice_Courier_Cost'))
        END
    END
  
    ! Get Parts Prices
    p_web.SSV('PartsCost',0)
    Access:PARTS.ClearKey(par:Part_Number_Key)
    par:Ref_Number = p_web.GSV('job:Ref_Number')
    SET(par:Part_Number_Key,par:Part_Number_Key)
    LOOP UNTIL Access:PARTS.Next()
        IF (par:Ref_Number <> p_web.GSV('job:Ref_Number'))
            BREAK
        END
        Access:STOCK.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = par:Part_Ref_Number
        IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
            IF (sto:Location <> p_web.GSV('ARC:SiteLocation'))
                p_web.SSV('PartsCost',p_web.GSV('PartsCost') + par:RRCAveragePurchaseCost)
            END
        END
    END
  
    ! Get ARC Markup
    p_web.SSV('ARCMarkup',0)
    Access:CHARTYPE.ClearKey(cha:Warranty_Key)
    cha:Warranty = 'NO'
    cha:Charge_Type = p_web.GSV('job:Charge_Type')
    IF (Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign)
        IF (cha:Zero_Parts_ARC OR cha:Zero_Parts = 'YES')
            p_web.SSV('ARCMarkup',p_web.GSV('jobe:InvRRCCPartsCost') - p_web.GSV('ARCPartsCost'))
            IF (p_web.GSV('ARCMarkup') < 0)
                p_web.SSV('ARCMarkup',0)
            END
        END
    END
  
    ! How much has been paid?
    p_web.SSV('Paid',0)
    p_web.SSV('Refund',0)
    Access:JOBPAYMT.ClearKey(jpt:All_Date_Key)
    jpt:Ref_Number = p_web.GSV('job:ref_Number')
    SET(jpt:All_Date_Key,jpt:All_Date_Key)
    LOOP UNTIL Access:JOBPAYMT.Next()
        IF (jpt:Ref_Number <> p_web.GSV('job:Ref_Number'))
            BREAK
        END
        IF (jpt:Amount > 0)
            p_web.SSV('Paid',p_web.GSV('Paid') + jpt:Amount)
        ELSE
            p_web.SSV('Refund',p_web.GSV('Refund') + (-jpt:Amount))
        END
    END
  
    !!!
    p_web.SSV('locTotalValue',JobTotal - CreditAmount)
  
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locTotalValue = p_web.RestoreValue('locTotalValue')
 locCreditType = p_web.RestoreValue('locCreditType')
 locCreditAmount = p_web.RestoreValue('locCreditAmount')
 locCreditMessage = p_web.RestoreValue('locCreditMessage')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'DisplayBrowsePayments'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('CreateCreditNote_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('CreateCreditNote_ChainTo')
    loc:formaction = p_web.GetSessionValue('CreateCreditNote_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'DisplayBrowsePayments'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="CreateCreditNote" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="CreateCreditNote" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="CreateCreditNote" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Create Credit Note') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Create Credit Note',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_CreateCreditNote">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_CreateCreditNote" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_CreateCreditNote')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_CreateCreditNote')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_CreateCreditNote'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_CreateCreditNote')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_CreateCreditNote_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locTotalValue
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locTotalValue
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locTotalValue
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_CreateCreditNote_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCreditType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCreditType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCreditType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCreditAmount
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCreditAmount
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCreditAmount
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonCreateCreditNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonCreateCreditNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCreditMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCreditMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPrintCreditNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonPrintCreditNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locTotalValue  Routine
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('locTotalValue') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Total Value Of Job')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locTotalValue  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locTotalValue',p_web.GetValue('NewValue'))
    locTotalValue = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locTotalValue
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locTotalValue',p_web.dFormat(p_web.GetValue('Value'),'@n-14.2'))
    locTotalValue = p_web.Dformat(p_web.GetValue('Value'),'@n-14.2') !
  End

Value::locTotalValue  Routine
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('locTotalValue') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locTotalValue
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBoldLarge')&'">' & p_web._jsok(format(p_web.GetSessionValue('locTotalValue'),'@n-14.2')) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locTotalValue  Routine
    loc:comment = ''
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('locTotalValue') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locCreditType  Routine
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('locCreditType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Credit Amount?')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locCreditType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCreditType',p_web.GetValue('NewValue'))
    locCreditType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCreditType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCreditType',p_web.GetValue('Value'))
    locCreditType = p_web.GetValue('Value')
  End
  DO CheckCreditAmount
  do Value::locCreditType
  do SendAlert
  do Prompt::locCreditAmount
  do Value::locCreditAmount  !1
  do Comment::locCreditAmount
  do Value::buttonCreateCreditNote  !1

Value::locCreditType  Routine
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('locCreditType') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locCreditType
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locCreditType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(p_web.GSV('CreditNoteCreated') = 1,'disabled','')
    if p_web.GetSessionValue('locCreditType') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locCreditType'',''createcreditnote_loccredittype_value'',1,'''&clip(0)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locCreditType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locCreditType',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locCreditType_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Credit Full Amount') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(p_web.GSV('CreditNoteCreated') = 1,'disabled','')
    if p_web.GetSessionValue('locCreditType') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locCreditType'',''createcreditnote_loccredittype_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locCreditType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locCreditType',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locCreditType_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Credit Specific Amount') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CreateCreditNote_' & p_web._nocolon('locCreditType') & '_value')

Comment::locCreditType  Routine
    loc:comment = ''
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('locCreditType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locCreditAmount  Routine
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('locCreditAmount') & '_prompt',Choose(p_web.GSV('locCreditType') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Credit Amount')
  If p_web.GSV('locCreditType') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CreateCreditNote_' & p_web._nocolon('locCreditAmount') & '_prompt')

Validate::locCreditAmount  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCreditAmount',p_web.GetValue('NewValue'))
    locCreditAmount = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCreditAmount
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCreditAmount',p_web.dFormat(p_web.GetValue('Value'),'@n-14.2'))
    locCreditAmount = p_web.Dformat(p_web.GetValue('Value'),'@n-14.2') !
  End
  If locCreditAmount = ''
    loc:Invalid = 'locCreditAmount'
    loc:alert = p_web.translate('Credit Amount') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  DO CheckCreditAmount
  do Value::locCreditAmount
  do SendAlert
  do Comment::locCreditAmount
  do Value::buttonCreateCreditNote  !1

Value::locCreditAmount  Routine
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('locCreditAmount') & '_value',Choose(p_web.GSV('locCreditType') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locCreditType') <> 1)
  ! --- STRING --- locCreditAmount
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('CreditNoteCreated') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('CreditNoteCreated') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locCreditAmount')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locCreditAmount = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locCreditAmount'',''createcreditnote_loccreditamount_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locCreditAmount')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locCreditAmount',p_web.GetSessionValue('locCreditAmount'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n-14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('CreateCreditNote_' & p_web._nocolon('locCreditAmount') & '_value')

Comment::locCreditAmount  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:CreditAmount'))
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('locCreditAmount') & '_comment',Choose(p_web.GSV('locCreditType') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locCreditType') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CreateCreditNote_' & p_web._nocolon('locCreditAmount') & '_comment')

Validate::buttonCreateCreditNote  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonCreateCreditNote',p_web.GetValue('NewValue'))
    do Value::buttonCreateCreditNote
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  DO CreateCreditNote
  do Value::buttonCreateCreditNote
  do SendAlert
  do Value::buttonPrintCreditNote  !1
  do Value::locCreditAmount  !1
  do Value::locCreditMessage  !1
  do Value::locCreditType  !1

Value::buttonCreateCreditNote  Routine
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('buttonCreateCreditNote') & '_value',Choose(p_web.GSV('Hide:CreateCreditNoteButton') =1 OR p_web.GSV('CreditNoteCreated') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CreateCreditNoteButton') =1 OR p_web.GSV('CreditNoteCreated') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonCreateCreditNote'',''createcreditnote_buttoncreatecreditnote_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CreateCreditNote','Create Credit Note','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('CreateCreditNote_' & p_web._nocolon('buttonCreateCreditNote') & '_value')

Comment::buttonCreateCreditNote  Routine
    loc:comment = ''
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('buttonCreateCreditNote') & '_comment',Choose(p_web.GSV('Hide:CreateCreditNoteButton') =1 OR p_web.GSV('CreditNoteCreated') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CreateCreditNoteButton') =1 OR p_web.GSV('CreditNoteCreated') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locCreditMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCreditMessage',p_web.GetValue('NewValue'))
    locCreditMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCreditMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCreditMessage',p_web.GetValue('Value'))
    locCreditMessage = p_web.GetValue('Value')
  End

Value::locCreditMessage  Routine
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('locCreditMessage') & '_value',Choose(p_web.GSV('locCreditMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locCreditMessage') = '')
  ! --- DISPLAY --- locCreditMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locCreditMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('CreateCreditNote_' & p_web._nocolon('locCreditMessage') & '_value')

Comment::locCreditMessage  Routine
    loc:comment = ''
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('locCreditMessage') & '_comment',Choose(p_web.GSV('locCreditMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locCreditMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPrintCreditNote  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintCreditNote',p_web.GetValue('NewValue'))
    do Value::buttonPrintCreditNote
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonPrintCreditNote  Routine
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('buttonPrintCreditNote') & '_value',Choose(p_web.GSV('CreditNoteCreated') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('CreditNoteCreated') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintCreditNote','Print Credit Note','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('InvoiceNote')) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('CreateCreditNote_' & p_web._nocolon('buttonPrintCreditNote') & '_value')

Comment::buttonPrintCreditNote  Routine
    loc:comment = ''
  p_web._DivHeader('CreateCreditNote_' & p_web._nocolon('buttonPrintCreditNote') & '_comment',Choose(p_web.GSV('CreditNoteCreated') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('CreditNoteCreated') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('CreateCreditNote_locCreditType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locCreditType
      else
        do Value::locCreditType
      end
  of lower('CreateCreditNote_locCreditAmount_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locCreditAmount
      else
        do Value::locCreditAmount
      end
  of lower('CreateCreditNote_buttonCreateCreditNote_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonCreateCreditNote
      else
        do Value::buttonCreateCreditNote
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('CreateCreditNote_form:ready_',1)
  p_web.SetSessionValue('CreateCreditNote_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_CreateCreditNote',0)
  locCreditType = 0
  p_web.SetSessionValue('locCreditType',locCreditType)

PreCopy  Routine
  p_web.SetValue('CreateCreditNote_form:ready_',1)
  p_web.SetSessionValue('CreateCreditNote_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_CreateCreditNote',0)
  ! here we need to copy the non-unique fields across
  locCreditType = 0
  p_web.SetSessionValue('locCreditType',0)

PreUpdate       Routine
  p_web.SetValue('CreateCreditNote_form:ready_',1)
  p_web.SetSessionValue('CreateCreditNote_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('CreateCreditNote:Primed',0)

PreDelete       Routine
  p_web.SetValue('CreateCreditNote_form:ready_',1)
  p_web.SetSessionValue('CreateCreditNote_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('CreateCreditNote:Primed',0)
  p_web.setsessionvalue('showtab_CreateCreditNote',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('CreateCreditNote_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('CreateCreditNote_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 3
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
      If not (p_web.GSV('locCreditType') <> 1)
        If locCreditAmount = ''
          loc:Invalid = 'locCreditAmount'
          loc:alert = p_web.translate('Credit Amount') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('CreateCreditNote:Primed',0)
  p_web.StoreValue('locTotalValue')
  p_web.StoreValue('locCreditType')
  p_web.StoreValue('locCreditAmount')
  p_web.StoreValue('')
  p_web.StoreValue('locCreditMessage')
  p_web.StoreValue('')
