

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER327.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! Add script line to packet string (trailing ; added)
!!! </summary>
CreateScript         PROCEDURE  (Netwebserverworker p_web, *STRING pPacket, STRING pScript) ! Declare Procedure
packetString STRING(16384)

  CODE
        packetString = pPacket
        pPacket = CLIP(pPacket) & '<script>' & CLIP(pScript) & ';</script>'    
        
        AddToLog('Script',CLIP(pScript),'CreateScript',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
