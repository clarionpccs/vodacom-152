

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER035.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER034.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
JobPricingRoutine    PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
save_wpr_id          USHORT,AUTO                           !
save_par_id          USHORT,AUTO                           !
save_epr_id          USHORT,AUTO                           !
tmp:ARCCLabourCost   REAL                                  !ARC Labour Cost
tmp:RRCCLabourCost   REAL                                  !RRC Labour Cost
tmp:ARCCPartsCost    REAL                                  !ARCC Parts Cost
tmp:RRCCPartsCost    REAL                                  !RRCC Parts Cost
tmp:ARCWLabourCost   REAL                                  !ARCW Labour Cost
tmp:RRCWLabourCost   REAL                                  !RRCW Labour Cost
tmp:ARCWPartsCost    REAL                                  !ARCW Parts Cost
tmp:RRCWPartsCost    REAL                                  !RRCW Parts Cost
tmp:ARCELabourCost   REAL                                  !ARCE Labour Cost
tmp:RRCELabourCost   REAL                                  !ARCELabour Cost
tmp:ARCEPartsCost    REAL                                  !ARCE Parts Cost
tmp:RRCEPartsCost    REAL                                  !RRCE Parts Cost
tmp:ClaimPartsCost   REAL                                  !Claim PartsCost
tmp:UseStandardRate  BYTE(0)                               !Use Standard Rate
tmp:ClaimValue       REAL                                  !Claim Value
tmp:FixedCharge      BYTE(0)                               !Fixed Charge
tmp:ExcludeHandlingFee BYTE(0)                             !Exclude Handling Fee
tmp:ManufacturerExchangeFee REAL                           !Manufacturer Exchange Fee
tmp:FixedChargeARC   BYTE(0)                               !Fixed Charge ARC
tmp:FixedChargeRRC   BYTE(0)                               !Fixed Charge RRC
tmp:ARCLocation      STRING(30)                            !ARC Location
tmp:RRCLocation      STRING(30)                            !RRCLocation
tmp:SecondYearWarranty BYTE(0)                             !Second Year Warranty (True/False)
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
FilesOpened     BYTE(0)

  CODE
    do OpenFiles

    sentToHub(p_web)

    tmp:FixedCharge = 0
    tmp:ExcludeHandlingFee = 0
    tmp:ManufacturerExchangeFee = 0
    tmp:FixedChargeARC = 0
    tmp:FixedChargeRRC = 0
    tmp:ClaimPartsCost = 0
    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number  = GETINI('BOOKING','HeadAccount','AA20',CLIP(PATH())&'\SB2KDEF.INI')
    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Found
        tmp:ARCLocation = tra:SiteLocation
    Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Error
    End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

    Access:WEBJOB.Clearkey(wob:RefNumberKey)
    wob:RefNumber  = p_web.GSV('job:Ref_Number')
    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = wob:HeadAccountNumber
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            tmp:RRCLocation = tra:SiteLocation
        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    Else ! If Access:WEBJOB.Tryfetch(wob:Ref_Number_Key) = Level:Benign
        !Error
    End !If Access:WEBJOB.Tryfetch(wob:Ref_Number_Key) = Level:Benign

    If p_web.GSV('job:Chargeable_Job') <> 'YES'
        !If not chargeable job, blank chargeable costs - L874 (DBH: 16-07-2003)
        tmp:ARCCLabourCost  = 0
        tmp:ARCCPartsCost   = 0
        tmp:RRCCLabourCost  = 0
        tmp:RRCCPartsCost   = 0
        p_web.SSV('job:Ignore_Chargeable_Charges','NO')
        p_web.SSV('jobe:IgnoreRRCChaCosts',0)

    Else !If p_web.GSV('job:Chargeable_Job <> 'YES'
        tmp:ARCCLabourCost  = 0
        tmp:ARCCPartsCost   = 0
        tmp:RRCCLabourCost  = 0
        tmp:RRCCPartsCost   = 0
        p_web.SSV('jobe:HandlingFee',0)
        p_web.SSV('jobe:ExchangeRate',0)

        If ExcludeHandlingFee('C',p_web.GSV('job:Manufacturer'),p_web.GSV('job:Repair_Type'))
            tmp:ExcludeHandlingFee = 1
        End !If ExcludeHandingFee('C',p_web.GSV('job:Manfacturer,p_web.GSV('job:Repair_Type)

        Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
        cha:Charge_Type = p_web.GSV('job:Charge_Type')
        If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
            !Found
            If cha:Zero_Parts_ARC
                tmp:FixedChargeARC = 1
            End !If cha:Zero_Parts_ARC = 'YES'
            If cha:Zero_Parts = 'YES'
                tmp:FixedChargeRRC = 1
            End !If cha:Zero_Parts = 'YES'
            Save_par_ID = Access:PARTS.SaveFile()
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = p_web.GSV('job:Ref_Number')
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                    Then Break.  ! End If


                If ~cha:Zero_Parts_ARC
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = par:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Found
                        If sto:Location = tmp:ARCLocation
                            If p_web.GSV('job:Estimate') = 'YES'
                                !Use the cost of any estimated parts.
                                Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
                                epr:Ref_Number  = p_web.GSV('job:Ref_Number')
                                epr:Part_Number = par:Part_Number
                                If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                                    !Found
                                    tmp:ARCCPartsCost += epr:Sale_Cost * par:Quantity
                                Else!If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                            Else !If p_web.GSV('job:Estimate = 'YES'
                                tmp:ARCCPartsCost += par:Sale_Cost * par:Quantity
                            End !If p_web.GSV('job:Estimate = 'YES'
                        End !If sto:Location = tmp:ARCLocatin
                    Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Error
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                Else !If cha:Zero_Parts_ARC <> 'YES'
                    If p_web.GSV('job:Invoice_Number') = 0
                        par:Sale_Cost = 0
                        Access:PARTS.Update()
                    End !If p_web.GSV('job:Invoice_Number = 0
                End !If cha:Zero_Parts_ARC <> 'YES'
                If cha:Zero_Parts <> 'YES'
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = par:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Found
                        If sto:Location = tmp:RRCLocation
                            If p_web.GSV('job:Estimate') = 'YES'
                                !Use the cost of any estimated parts.
                                Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
                                epr:Ref_Number  = p_web.GSV('job:Ref_Number')
                                epr:Part_Number = par:Part_Number
                                If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                                    !Found
                                    tmp:RRCCPartsCost += epr:RRCSaleCost * par:Quantity
                                Else!If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:ESTPARTS.TryFetch(epr:Part_Number_Key) = Level:Benign
                            Else !If p_web.GSV('job:Estimate = 'YES'
                                tmp:RRCCPartsCost += par:RRCSaleCost * par:Quantity
                            End !If p_web.GSV('job:Estimate = 'YES'

                        End !If sto:Location <> tmp:RRCLocation
                    Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Error
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                Else !If cha:Zero_Parts <> 'YES'
                    If p_web.GSV('job:Invoice_Number') = 0
                        par:RRCSaleCost = 0
                        Access:PARTS.Update()
                    End !If p_web.GSV('job:Invoice_Number = 0
                End !If cha:Zero_Parts <> 'YES'
            End !Loop
            Access:PARTS.RestoreFile(Save_par_ID)

            tmp:UseStandardRate = 1
            If InvoiceSubAccounts(p_web.GSV('job:Account_Number'))
                access:subchrge.clearkey(suc:model_repair_type_key)
                suc:account_number = p_web.GSV('job:account_number')
                suc:model_number   = p_web.GSV('job:model_number')
                suc:charge_type    = p_web.GSV('job:charge_type')
                suc:unit_type      = p_web.GSV('job:unit_type')
                suc:repair_type    = p_web.GSV('job:repair_type')
                if access:subchrge.fetch(suc:model_repair_type_key)
                    access:trachrge.clearkey(trc:account_charge_key)
                    trc:account_number = sub:main_account_number
                    trc:model_number   = p_web.GSV('job:model_number')
                    trc:charge_type    = p_web.GSV('job:charge_type')
                    trc:unit_type      = p_web.GSV('job:unit_type')
                    trc:repair_type    = p_web.GSV('job:repair_type')
                    if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                        tmp:ARCCLabourCost      = trc:Cost
                        tmp:RRCCLabourCost      = trc:RRCRate
                        p_web.SSV('jobe:HandlingFee',trc:HandlingFee)
                        p_web.SSV('jobe:ExchangeRate',trc:Exchange)
                        tmp:useStandardRate = 0
                    End!if access:trachrge.fetch(trc:account_charge_key)
                Else
                    tmp:ARCCLabourCost      = suc:Cost
                    tmp:RRCCLabourCost      = suc:RRCRate
                    p_web.SSV('jobe:HandlingFee',suc:HandlingFee)
                    p_web.SSV('jobe:ExchangeRate',suc:Exchange)

                    tmp:useStandardRate = 0
                End!if access:subchrge.fetch(suc:model_repair_type_key)
            Else !If InvoiceSubAccounts(p_web.GSV('job:Account_Number)
                access:trachrge.clearkey(trc:account_charge_key)
                trc:account_number = sub:main_account_number
                trc:model_number   = p_web.GSV('job:model_number')
                trc:charge_type    = p_web.GSV('job:charge_type')
                trc:unit_type      = p_web.GSV('job:unit_type')
                trc:repair_type    = p_web.GSV('job:repair_type')
                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                    tmp:ARCCLabourCost      = trc:Cost
                    tmp:RRCCLabourCost      = trc:RRCRate
                    p_web.SSV('jobe:HandlingFee',trc:HandlingFee)
                    p_web.SSV('jobe:ExchangeRate',trc:Exchange)
                    tmp:useStandardRate = 0

                End!if access:trachrge.fetch(trc:account_charge_key)

            End !If InvoiceSubAccounts(p_web.GSV('job:Account_Number)

            If tmp:useStandardRate
                access:stdchrge.clearkey(sta:model_number_charge_key)
                sta:model_number = p_web.GSV('job:model_number')
                sta:charge_type  = p_web.GSV('job:charge_type')
                sta:unit_type    = p_web.GSV('job:unit_type')
                sta:repair_type  = p_web.GSV('job:repair_type')
                if access:stdchrge.fetch(sta:model_number_charge_key)
                Else !if access:stdchrge.fetch(sta:model_number_charge_key)
                    tmp:ARCCLabourCost      = sta:Cost
                    tmp:RRCCLabourCost      = sta:RRCRate
                    p_web.SSV('jobe:HandlingFee',sta:HandlingFee)
                    p_web.SSV('jobe:ExchangeRate',sta:Exchange)
                end !if access:stdchrge.fetch(sta:model_number_charge_key)
            End !If tmp:useStandardRate
        Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
            !Error
        End !If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        !Dont blank the cost because of their cock up .
        !They used this field for other costs, and now want to see it
        !even if it's not a loan
!        If ~LoanAttachedToJob(p_web.GSV('job:Ref_Number)
!            p_web.GSV('job:Courier_Cost = 0
!            p_web.GSV('job:Courier_Cost_Estimate = 0
!        End !If LoanAttachedToJob(p_web.GSV('job:Ref_Number)
    End !p_web.GSV('job:Chargeable_Job = 'YES'

    tmp:SecondYearWarranty = 0
    If p_web.GSV('job:Warranty_Job') <> 'YES'
        !Blank costs if not a Warranty Job - L874 (DBH: 16-07-2003)
        tmp:ARCWLabourCost  = 0
        tmp:ARCWPartsCost   = 0
        tmp:RRCWLabourCost  = 0
        tmp:RRCWPartsCost   = 0
        tmp:ClaimValue      = 0
        p_web.SSV('job:Ignore_Warranty_Charges','NO')
        p_web.SSV('jobe:IgnoreRRCWarCosts',0)
        p_web.SSV('jobe:IgnoreClaimCosts',0)
    Else !If p_web.GSV('job:Warranty_Job = 'YES'
        !Do not reprice Warranty Completed Jobs - L945  (DBH: 03-09-2003)
        !Allow to force the reprice if the job conditions change - L945 (DBH: 04-09-2003)
        If p_web.GSV('job:Date_Completed') = 0 Or p_web.GSV('JobPricingRoutine:ForceWarranty') = 1
            tmp:ARCWLabourCost  = 0
            tmp:ARCWPartsCost   = 0
            tmp:RRCWLabourCost  = 0
            tmp:RRCWPartsCost   = 0
            p_web.SSV('jobe:HandlingFee',0)
            p_web.SSV('jobe:ExchangeRate',0)

            If ExcludeHandlingFee('W',p_web.GSV('job:Manufacturer'),p_web.GSV('job:Repair_Type_Warranty'))
                tmp:ExcludeHandlingFee = 1
            End !If ExcludeHandingFee('C',p_web.GSV('job:Manfacturer,p_web.GSV('job:Repair_Type)

            Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
            cha:Charge_Type = p_web.GSV('job:Warranty_Charge_Type')
            If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Found
                tmp:SecondYearWarranty = cha:SecondYearWarranty


                Save_wpr_ID = Access:WARPARTS.SaveFile()
                Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                wpr:Ref_Number  = p_web.GSV('job:Ref_Number')
                Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                Loop
                    If Access:WARPARTS.NEXT()
                       Break
                    End !If
                    If wpr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                        Then Break.  ! End If

                    tmp:ClaimPartsCost += wpr:Purchase_Cost * wpr:Quantity

                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = wpr:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Found
                        If sto:Location = tmp:ARCLocation
                            tmp:ARCWPartsCost += wpr:Purchase_Cost * wpr:Quantity
                        End !If sto:Location = tmp:ARCLocation
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = wpr:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Found
                        If sto:Location = tmp:RRCLocation
                            tmp:RRCWPartsCost += wpr:RRCPurchaseCost * wpr:Quantity
                        End !If sto:Location = tmp:RRCLocation
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                End !Loop
                Access:WARPARTS.RestoreFile(Save_wpr_ID)

                tmp:UseStandardRate = 1
                If InvoiceSubAccounts(p_web.GSV('job:Account_Number'))
                    access:subchrge.clearkey(suc:model_repair_type_key)
                    suc:account_number = p_web.GSV('job:account_number')
                    suc:model_number   = p_web.GSV('job:model_number')
                    suc:charge_type    = p_web.GSV('job:Warranty_charge_type')
                    suc:unit_type      = p_web.GSV('job:unit_type')
                    suc:repair_type    = p_web.GSV('job:repair_type_Warranty')
                    if access:subchrge.fetch(suc:model_repair_type_key)
                        access:trachrge.clearkey(trc:account_charge_key)
                        trc:account_number = sub:main_account_number
                        trc:model_number   = p_web.GSV('job:model_number')
                        trc:charge_type    = p_web.GSV('job:Warranty_charge_type')
                        trc:unit_type      = p_web.GSV('job:unit_type')
                        trc:repair_type    = p_web.GSV('job:repair_type_Warranty')
                        if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                            tmp:ARCWLabourCost      = trc:Cost
                            tmp:RRCWLabourCost      = trc:RRCRate
                            tmp:ClaimValue          = trc:WarrantyClaimRate
                            p_web.SSV('jobe:HandlingFee',trc:HandlingFee)
                            p_web.SSV('jobe:ExchangeRate',trc:Exchange)
                            tmp:useStandardRate = 0
                        End!if access:trachrge.fetch(trc:account_charge_key)
                    Else
                        tmp:ARCWLabourCost      = suc:Cost
                        tmp:RRCWLabourCost      = suc:RRCRate
                        tmp:ClaimValue          = suc:WarrantyClaimRate
                        p_web.SSV('jobe:HandlingFee',suc:HandlingFee)
                        p_web.SSV('jobe:ExchangeRate',suc:Exchange)

                        tmp:useStandardRate = 0
                    End!if access:subchrge.fetch(suc:model_repair_type_key)
                Else !If InvoiceSubAccounts(p_web.GSV('job:Account_Number)
                    access:trachrge.clearkey(trc:account_charge_key)
                    trc:account_number = sub:main_account_number
                    trc:model_number   = p_web.GSV('job:model_number')
                    trc:charge_type    = p_web.GSV('job:Warranty_charge_type')
                    trc:unit_type      = p_web.GSV('job:unit_type')
                    trc:repair_type    = p_web.GSV('job:repair_type_Warranty')
                    if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                        tmp:ARCWLabourCost      = trc:Cost
                        tmp:RRCWLabourCost      = trc:RRCRate
                        tmp:ClaimValue          = trc:WarrantyClaimRate
                        p_web.SSV('jobe:HandlingFee',trc:HandlingFee)
                        p_web.SSV('jobe:ExchangeRate',trc:Exchange)
                        tmp:useStandardRate = 0

                    End!if access:trachrge.fetch(trc:account_charge_key)

                End !If InvoiceSubAccounts(p_web.GSV('job:Account_Number)

                If tmp:useStandardRate
                    access:stdchrge.clearkey(sta:model_number_charge_key)
                    sta:model_number = p_web.GSV('job:model_number')
                    sta:charge_type  = p_web.GSV('job:Warranty_charge_type')
                    sta:unit_type    = p_web.GSV('job:unit_type')
                    sta:repair_type  = p_web.GSV('job:repair_type_Warranty')
                    if access:stdchrge.fetch(sta:model_number_charge_key)
                    Else !if access:stdchrge.fetch(sta:model_number_charge_key)
                        tmp:ARCWLabourCost      = sta:Cost
                        tmp:RRCWLabourCost      = sta:RRCRate
                        tmp:ClaimValue          = sta:WarrantyClaimRate
                        p_web.SSV('jobe:HandlingFee',sta:HandlingFee)
                        p_web.SSV('jobe:ExchangeRate',sta:Exchange)
                    end !if access:stdchrge.fetch(sta:model_number_charge_key)
                End !If tmp:useStandardRate
            Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Error
            End !If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign

            Access:MANUFACT.Clearkey(man:Manufacturer_Key)
            man:Manufacturer    = p_web.GSV('job:Manufacturer')
            If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                !Found
                If tmp:SecondYearWarranty
                    tmp:ManufacturerExchangeFee = man:SecondYrExchangeFee
                Else !If tmp:SecondYearWarranty
                    tmp:ManufacturerExchangeFee = man:ExchangeFee
                End !If tmp:SecondYearWarranty

            Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                !Error
            End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        End !If p_web.GSV('job:Date_Completed = 0
    End !p_web.GSV('job:Warranty_Job = 'YES'

    tmp:ARCELabourCost = 0
    tmp:ARCEPartsCost  = 0
    tmp:RRCELabourCost = 0
    tmp:RRCEPartsCost   = 0
    Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
    cha:Charge_Type = p_web.GSV('job:Charge_Type')
    If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        !Found
        Save_epr_ID = Access:ESTPARTS.SaveFile()
        Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
        epr:Ref_Number  = p_web.GSV('job:Ref_Number')
        Set(epr:Part_Number_Key,epr:Part_Number_Key)
        Loop
            If Access:ESTPARTS.NEXT()
               Break
            End !If
            If epr:Ref_Number  <> p_web.GSV('job:Ref_Number')      |
                Then Break.  ! End If
            If ~cha:Zero_Parts_ARC
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = epr:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Found
                    If sto:Location = tmp:ARCLocation
                        tmp:ARCEPartsCost += epr:Sale_Cost * epr:Quantity
                    End !If sto:Location = tmp:ARCLocation
                End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            Else !If cha:Zero_Parts_ARC <> 'YES'
                If p_web.GSV('job:Invoice_Number') = 0
                    epr:Sale_Cost = 0
                    Access:ESTPARTS.Update()
                End !If p_web.GSV('job:Invoice_Number = 0
            End !If cha:Zero_Parts_ARC <> 'YES'

            If cha:Zero_Parts <> 'YES'
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = epr:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Found
                    If sto:Location = tmp:RRCLocation
                        tmp:RRCEPartsCost += epr:RRCSaleCost * epr:Quantity
                    End !If sto:Location = tmp:RRCLocation
                End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            Else !If cha:Zero_Parts <> 'YES'
                If p_web.GSV('job:Invoice_Number') = 0
                    epr:RRCSaleCost = 0
                    Access:ESTPARTS.Update()
                End !If p_web.GSV('job:Invoice_Number = 0
            End !If cha:Zero_Parts <> 'YES'
        End !Loop
        Access:ESTPARTS.RestoreFile(Save_epr_ID)
        If p_web.GSV('job:Estimate') = 'YES' And p_web.GSV('job:Estimate_Accepted') = 'YES'
                !If estimate, then use the estimate cost.
                !it doesn't matter what the current costs are
                tmp:ARCELabourCost  = p_web.GSV('job:Labour_Cost_Estimate')
                tmp:RRCELabourCost  = p_web.GSV('jobe:RRCELabourCost')
                tmp:ARCCLabourCost  = p_web.GSV('job:Labour_Cost_Estimate')
                tmp:RRCCLabourCost  = p_web.GSV('jobe:RRCELabourCost')

                ! Inserting (DBH 03/12/2007) # 8218 - For RRC-ARC estimate, the RRC costs must mirror the ARC costs
                
                if (p_web.GSV('SentToHub') = 1 And p_web.GSV('jobe:WebJob'))
                    tmp:RRCCLabourCost = tmp:ARCCLabourCost
                    tmp:RRCCPartsCost = tmp:ARCCPartsCost
                End ! If SentToHub(p_web.GSV('job:Ref_Number) And p_web.GSV('jobe:WebJob
                ! End (DBH 03/12/2007) #8218
        Else !If p_web.GSV('job:Estimate = 'YES' And p_web.GSV('job:Estimate_Accepted = 'YES'

            tmp:UseStandardRate = 1
            If InvoiceSubAccounts(p_web.GSV('job:Account_Number'))
                access:subchrge.clearkey(suc:model_repair_type_key)
                suc:account_number = p_web.GSV('job:account_number')
                suc:model_number   = p_web.GSV('job:model_number')
                suc:charge_type    = p_web.GSV('job:charge_type')
                suc:unit_type      = p_web.GSV('job:unit_type')
                suc:repair_type    = p_web.GSV('job:repair_type')
                if access:subchrge.fetch(suc:model_repair_type_key)
                    access:trachrge.clearkey(trc:account_charge_key)
                    trc:account_number = sub:main_account_number
                    trc:model_number   = p_web.GSV('job:model_number')
                    trc:charge_type    = p_web.GSV('job:charge_type')
                    trc:unit_type      = p_web.GSV('job:unit_type')
                    trc:repair_type    = p_web.GSV('job:repair_type')
                    if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                        tmp:ARCELabourCost = trc:Cost
                        tmp:RRCELabourCost  = trc:RRCRate
                        tmp:useStandardRate = 0
                    End!if access:trachrge.fetch(trc:account_charge_key)
                Else
                    tmp:ARCELabourCost = suc:Cost
                    tmp:RRCELabourCost = suc:RRCRate

                    tmp:useStandardRate = 0
                End!if access:subchrge.fetch(suc:model_repair_type_key)
            Else !If InvoiceSubAccounts(p_web.GSV('job:Account_Number)
                access:trachrge.clearkey(trc:account_charge_key)
                trc:account_number = sub:main_account_number
                trc:model_number   = p_web.GSV('job:model_number')
                trc:charge_type    = p_web.GSV('job:charge_type')
                trc:unit_type      = p_web.GSV('job:unit_type')
                trc:repair_type    = p_web.GSV('job:repair_type')
                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                    tmp:ARCELabourCost     = trc:Cost
                    tmp:RRCELabourCost  = trc:RRCRate
                    tmp:useStandardRate = 0

                End!if access:trachrge.fetch(trc:account_charge_key)

            End !If InvoiceSubAccounts(p_web.GSV('job:Account_Number)

            If tmp:useStandardRate
                access:stdchrge.clearkey(sta:model_number_charge_key)
                sta:model_number = p_web.GSV('job:model_number')
                sta:charge_type  = p_web.GSV('job:charge_type')
                sta:unit_type    = p_web.GSV('job:unit_type')
                sta:repair_type  = p_web.GSV('job:repair_type')
                if access:stdchrge.fetch(sta:model_number_charge_key)
                Else !if access:stdchrge.fetch(sta:model_number_charge_key)
                    tmp:ARCELabourCost = sta:Cost
                    tmp:RRCELabourCost = sta:RRCRate
                end !if access:stdchrge.fetch(sta:model_number_charge_key)
            End !If tmp:useStandardRate
        End !If p_web.GSV('job:Estimate = 'YES' And p_web.GSV('job:Estimate_Accepted = 'YES'
    Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        !Error
    End !If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign

    If p_web.GSV('job:Exchange_Unit_Number') <> 0
        If p_web.GSV('jobe:ExchangedAtRRC')
            p_web.SSV('jobe:HandlingFee',0)
        Else !If p_web.GSV('jobe:ExchangedAtRRC
            p_web.SSV('jobe:ExchangeRate',0)
        End !If p_web.GSV('jobe:ExchangedAtRRC
    Else !p_web.GSV('job:Exchange_Unit_Number <> 0
        If (p_web.GSV('SentToHub') = 0)
            p_web.SSV('jobe:HandlingFee',0)
        End !If ~SentToHub(p_web.GSV('job:Ref_Number)
        p_web.SSV('jobe:ExchangeRate',0)
    End !p_web.GSV('job:Exchange_Unit_Number <> 0

    If tmp:ExcludeHandlingFee
        p_web.SSV('jobe:HandlingFee',0)
    End !If tmp:ExcludeHandlingFee

    If p_web.GSV('job:Ignore_Chargeable_Charges') <> 'YES'
        p_web.SSV('job:Labour_Cost',tmp:ARCCLabourCost)
        p_web.SSV('job:Parts_Cost', tmp:ARCCPartsCost)
        If p_web.GSV('job:Third_Party_Site') <> ''
            Access:TRDPARTY.ClearKey(trd:Company_Name_Key)
            trd:Company_Name = p_web.GSV('job:Third_Party_Site')
            If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                !Found
                If p_web.GSV('jobe:Ignore3rdPartyCosts') <> 1
                    If p_web.GSV('jobe:ARC3rdPartyMarkup') = 0
                        p_web.SSV('jobe:ARC3rdPartyMarkup',trd:Markup)
                    End !If p_web.GSV('jobe:ARC3rdPartyMarkup = 0
                End !If ~p_web.GSV('jobe:Ignore3rdPartyCosts
                p_web.SSV('jobe:ARC3rdPartyVAT',p_web.GSV('jobe:ARC3rdPartyCost') * (trd:VatRate/100))
            Else!If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
            If tmp:FixedChargeARC = 0
                p_web.SSV('job:Labour_Cost',p_web.GSV('jobe:ARC3rdPartyCost') + p_web.GSV('jobe:ARC3rdPartyMarkup'))
            End !If tmp:FixedChargeARC = 0
        End !If p_web.GSV('job:Third_Party_Site <> ''
    End !p_web.GSV('job:Ignore_Chargeable_Charges <> 'YES'

    If p_web.GSV('jobe:IgnoreRRCChaCosts') <> 1
        p_web.SSV('jobe:RRCCLabourCost',tmp:RRCCLabourCost)

        If p_web.GSV('job:Third_Party_Site') <> ''
            Access:TRDPARTY.ClearKey(trd:Company_Name_Key)
            trd:Company_Name = p_web.GSV('job:Third_Party_Site')
            If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                !Found
                If p_web.GSV('jobe:Ignore3rdPartyCosts') <> 1
                    If p_web.GSV('jobe:ARC3rdPartyMarkup') = 0
                        p_web.SSV('jobe:ARC3rdPartyMarkup',trd:Markup)
                    End !If p_web.GSV('jobe:ARC3rdPartyMarkup = 0
                End !If ~p_web.GSV('jobe:Ignore3rdPartyCosts
                p_web.SSV('jobe:ARC3rdPartyVAT',p_web.GSV('jobe:ARC3rdPartyCost') * (trd:VatRate/100))
            Else!If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign

            If tmp:FixedChargeRRC = 0
                p_web.SSV('jobe:RRCCLabourCost',p_web.GSV('jobe:ARC3rdPartyCost') + p_web.GSV('jobe:ARC3rdPartyMarkup'))
            End !If tmp:FixedCharge = 0
        End !If p_web.GSV('job:Third_Party_Site <> ''
    End !~p_web.GSV('jobe:IgnoreRRCChaCosts
    p_web.SSV('jobe:RRCCPartsCost',tmp:RRCCPartsCost)

    !Only update prices on incomplete jobs - L945 (DBH: 03-09-2003)
    !Allow to reprice if the job conditions change - L945 (DBH: 04-09-2003)
    If p_web.GSV('job:Date_Completed') = 0 Or p_web.GSV('JobPricingRoutine:ForceWarranty') = 1
        If p_web.GSV('job:Ignore_Warranty_Charges') <> 'YES'
            p_web.SSV('job:Labour_Cost_Warranty',tmp:ARCWLabourCost)
            p_web.SSV('job:Courier_Cost_Warranty',0)

            If p_web.GSV('jobe:ExchangedAtRRC') = True
                If p_web.GSV('jobe:SecondExchangeNumber') <> 0
                    !Exchange attached at RRC
                    !Don't claim, unless the ARC has added a second exchange - 3788 (DBH: 07-04-2004)
                    p_web.SSV('job:Courier_Cost_Warranty',tmp:ManufacturerExchangeFee)
                Else !If p_web.GSV('jobe:SecondExchangeNumber <> 0
                    !Exchange attached at RRC.
                    !Do not claim, unless sent to ARC, and job is RTM - 3788 (DBH: 07-04-2004)
                    If p_web.GSV('jobe:Engineer48HourOption') <> 1
                        If p_web.GSV('job:Repair_Type_Warranty') = 'R.T.M.'
                            p_web.SSV('job:Courier_Cost_Warranty',tmp:ManufacturerExchangeFee)
                        End !If p_web.GSV('job:Repair_Type_Warranty = 'R.T.M.'
                    End !If p_web.GSV('jobe:Engineer48HourOption = 1
                End !If p_web.GSV('jobe:SecondExchangeNumber <> 0
            Else !If p_web.GSV('jobe:ExchangedAtRRC = True
                !Exchange attached at ARC. Claim for it - 3788 (DBH: 07-04-2004)
                If p_web.GSV('job:Exchange_Unit_Number') <> 0
                    p_web.SSV('job:Courier_Cost_Warranty',tmp:ManufacturerExchangeFee)
                End !If p_web.GSV('job:Exchange_Unit_Number <> 0
            End !If p_web.GSV('jobe:ExchangedAtRRC = True
        End !p_web.GSV('job:Invoice_Warranty_Charges <> 'YES'

        ! Inserting (DBH 08/12/2005) #6644 - Add the Handset Replacement Cost to the parts cost
        If p_web.GSV('job:Exchange_Unit_Number') > 0
            If p_web.GSV('jobe:HandsetReplacmentValue') > 0
                tmp:ARCWPartsCost += p_web.GSV('jobe:HandsetReplacmentValue')
            End ! If p_web.GSV('jobe:HandsetReplacmentValue > 0
        End ! If p_web.GSV('job:Exchange_Unit_Number > 0
        If p_web.GSV('jobe:SecondExchangeNumber') > 0
            If p_web.GSV('jobe:SecondHandsetRepValue') > 0
                tmp:ARCWPartsCost += p_web.GSV('jobe:SecondHandsetRepValue')
            End ! If p_web.GSV('jobe:SecondHandsetReplacementValue > 0
        End ! If p_web.GSV('jobe:SecondExchangeNumber > 0
        ! End (DBH 08/12/2005) #6644

        p_web.SSV('job:Parts_Cost_Warranty',tmp:ARCWPartsCost)

        If p_web.GSV('jobe:IgnoreRRCWarCosts') <> 1
            p_web.SSV('jobe:RRCWLabourCost',tmp:RRCWLabourCost)
        End !~p_web.GSV('jobe:IgnoreRRCWarCosts

        p_web.SSV('jobe:RRCWPartsCost',tmp:RRCWPartsCost)

        If p_web.GSV('jobe:IgnoreClaimCosts') <> 1
            p_web.SSV('jobe:ClaimValue',tmp:ClaimValue)
        End !If ~p_web.GSV('jobe:IgnoreClaimCosts

        ! Inserting (DBH 08/12/2005) #6644 - Add the Handset Replacement Cost to the parts cost
        If p_web.GSV('job:Exchange_Unit_Number') > 0
            If p_web.GSV('jobe:HandsetReplacmentValue') > 0
                tmp:ClaimPartsCost += p_web.GSV('jobe:HandsetReplacmentValue')
            End ! If p_web.GSV('jobe:HandsetReplacmentValue > 0
        End ! If p_web.GSV('job:Exchange_Unit_Number > 0
        If p_web.GSV('jobe:SecondExchangeNumber') > 0
            If p_web.GSV('jobe:SecondHandsetRepValue') > 0
                tmp:ClaimPartsCost += p_web.GSV('jobe:SecondHandsetRepValue')
            End ! If p_web.GSV('jobe:SecondHandsetReplacementValue > 0
        End ! If p_web.GSV('jobe:SecondExchangeNumber > 0
        ! End (DBH 08/12/2005) #6644

        p_web.SSV('jobe:ClaimPartsCost',tmp:ClaimPartsCost)
    End !If p_web.GSV('job:Date_Completed = 0

    If p_web.GSV('job:Ignore_Estimate_Charges') <> 'YES'
        p_web.SSV('job:Labour_Cost_Estimate',tmp:ARCELabourCost)
    End !p_web.GSV('job:Ignore_Estimate_Charges <> 'YES'
    p_web.SSV('job:Parts_Cost_Estimate',tmp:ARCEPartsCost)

    If p_web.GSV('jobe:IgnoreRRCEstCosts') <> 1
        p_web.SSV('jobe:RRCELabourCost',tmp:RRCELabourCost)
    End !~p_web.GSV('jobe:IgnoreRRCEstCosts

    p_web.SSV('jobe:RRCEPartsCost',tmp:RRCEPartsCost)

    !Fixed Pricing

    If (p_web.GSV('SentToHub') = 0)
        !Not a hub job
        If tmp:FixedChargeRRC
            p_web.SSV('jobe:RRCCPartsCost',0)
            p_web.SSV('jobe:RRCEPartsCost',0)
        End !If tmp:FixedChargeRRC
    Else !If ~SentToHub(p_web.GSV('job:Ref_Number)
        If p_web.GSV('jobe:WebJob') = 1
            If tmp:FixedChargeARC
                p_web.SSV('job:Parts_Cost',0)
                p_web.SSV('job:Parts_Cost_Estimate',0)
            End !If tmp:FixedChargeRRC

            If tmp:FixedChargeRRC
                p_web.SSV('jobe:RRCCPartsCost',0)
                p_web.SSV('jobe:RRCEPartsCost',0)
            Else !If tmp:FixedChargeRRC
                If p_web.GSV('jobe:IgnoreRRCChaCosts') <> 1
                    p_web.SSV('jobe:RRCCLabourCost',p_web.GSV('job:Labour_Cost'))
                End !If ~p_web.GSV('jobe:IgnoreRRCChaCosts
            End !If tmp:FixedChargeRRC
        Else !If p_web.GSV('jobe:WebJob = 1
            If tmp:FixedChargeARC
                p_web.SSV('job:Parts_Cost',0)
                p_web.SSV('job:Parts_Cost_Estimate',0)
            End !If tmp:FixedChargeARC
        End !If p_web.GSV('jobe:WebJob = 1
    End !If ~SentToHub(p_web.GSV('job:Ref_Number)

    !Totals
! Inserting (DBH 03/12/2007) # 8218 - RRC has the same costs as the ARC for an estimate
    If (p_web.GSV('SentToHub') = 1 And p_web.GSV('jobe:WebJob'))
        p_web.SSV('jobe:RRCELabourCost',p_web.GSV('job:Labour_Cost_Estimate'))
        p_web.SSV('jobe:RRCEPartsCost',p_web.GSV('job:Parts_Cost_Estimate'))
    End ! If SentToHub(p_web.GSV('job:Ref_Number) And p_web.GSV('jobe:WebJob
! End (DBH 03/12/2007) #8218

    p_web.SSV('job:Sub_Total',p_web.GSV('job:Courier_Cost') + p_web.GSV('job:Labour_Cost') + p_web.GSV('job:Parts_Cost'))
    p_web.SSV('jobe:RRCCSubTotal',p_web.GSV('jobe:RRCCLabourCost') + p_web.GSV('jobe:RRCCPartsCost') + p_web.GSV('job:Courier_Cost'))
    p_web.SSV('job:Sub_Total_Warranty',p_web.GSV('job:Labour_Cost_Warranty') + p_web.GSV('job:Parts_Cost_Warranty') + p_web.GSV('job:Courier_Cost_Warranty'))
    p_web.SSV('jobe:RRCWSubTotal',p_web.GSV('jobe:RRCWLabourCost') + p_web.GSV('jobe:RRCWPartsCost'))
    p_web.SSV('job:Sub_Total_Estimate',p_web.GSV('job:Labour_Cost_Estimate') + p_web.GSV('job:Parts_Cost_Estimate') + p_web.GSV('job:Courier_Cost_Estimate'))
    p_web.SSV('jobe:RRCESubTotal',p_web.GSV('jobe:RRCELabourCost') + p_web.GSV('jobe:RRCEPartsCost') + p_web.GSV('job:Courier_Cost_Estimate'))


    do CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:PARTS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCK.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ESTPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ESTPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:CHARTYPE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:CHARTYPE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBCHRGE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBCHRGE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRACHRGE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRACHRGE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STDCHRGE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STDCHRGE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRDPARTY.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRDPARTY.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WEBJOB.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WEBJOB.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:PARTS.Close
     Access:STOCK.Close
     Access:ESTPARTS.Close
     Access:WARPARTS.Close
     Access:CHARTYPE.Close
     Access:SUBCHRGE.Close
     Access:TRACHRGE.Close
     Access:STDCHRGE.Close
     Access:MANUFACT.Close
     Access:TRDPARTY.Close
     Access:WEBJOB.Close
     Access:JOBS.Close
     Access:JOBSE.Close
     FilesOpened = False
  END
