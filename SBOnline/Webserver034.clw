

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER034.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ExcludeHandlingFee   PROCEDURE  (func:Type,func:Manufacturer,func:RepairType) ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
    do OpenFiles

    rtnValue# = 0
    !Return Fatal if the Handling Fee should be excluded
    Case func:Type
        Of 'C'
            Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
            rtd:Manufacturer = func:Manufacturer
            rtd:Chargeable   = 'YES'
            rtd:Repair_Type  = func:RepairType
            If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                !Found
                IF rtd:ExcludeHandlingFee
                    rtnValue# = 1
                End !IF rtd:ExcludeHandlingFee
            Else!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
        Of 'W'
            Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
            rtd:Manufacturer = func:Manufacturer
            rtd:Warranty     = 'YES'
            rtd:Repair_Type  = func:RepairType
            If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                !Found
                If rtd:ExcludeHandlingFee
                    rtnValue# = 1
                End !If rtd:ExcludeHandlingFee
            Else!If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
    End !Case func:Type

    do closeFiles
    Return rtnValue#
!--------------------------------------
OpenFiles  ROUTINE
  Access:REPTYDEF.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:REPTYDEF.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:REPTYDEF.Close
     FilesOpened = False
  END
