

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER283.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
CheckPaid            PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure

  CODE
!region Processed Code	
        IF (p_web.GSV('job:Date_Completed') > 0)
			
            IF (p_web.GSV('job:Chargeable_Job') = 'YES')			
                TotalPrice(p_web,'C',vat$,tot$,bal$)
                
                IF (tot$ = 0)
                    p_web.SSV('job:Paid','YES')
                ELSE ! IF
                    IF (bal$ <=0)
                        p_web.SSV('job:Paid','YES')
						
                        Access:COURIER.Clearkey(cou:Courier_Key)
                        cou:Courier = p_web.GSV('job:Courier')
                        IF (Access:COURIER.Tryfetch(cou:Courier_Key))
                        END !IF
						
                        IF (p_web.GSV('BookingSite') = 'RRC')
                            IF (p_web.GSV('job:Exchange_Unit_Number') > 0)
                            ELSE
                                IF (p_web.GSV('job:Location') = p_web.GSV('Default:InTransitPUP') OR |
                                    p_web.GSV('job:Location') = p_web.GSV('Default:PUPLocation'))
                                    ! #13503 Do not update status if on the way, or at the PUP (DBH: 27/03/2015)
                                ELSE
                                    
                                    IF (p_web.GSV('job:Location') = p_web.GSV('Default:DespatchToCustomer'))
                                        GetStatus(910,1,'JOB',p_web) ! Despatch Paid
                                    ELSE !IF 
                                        IF (cou:CustomerCollection)
                                            GetStatus(915,1,'JOB',p_web) ! Paid Awaiting Collection
                                        ELSE
                                            GetStatus(916,1,'JOB',p_web) ! Paid Awaiting Despatch
                                        END !IF
                                    END ! IF
                                END ! IF
                                
                            END ! IF
                        ELSE ! IF (p_web.GSV('BookingSite') = 'RRC')
                            !region Not Relevant for SBOnline
                            
                            IF (p_web.GSV('job:Exchange_Unit_Number') > 0)
                            ELSE
                                IF (p_web.GSV('job:Date_Despatched') > 0)
                                    GetStatus(910,1,'JOB',p_web) ! Despatch Paid
                                ELSE ! IF
                                    IF (cou:CustomerCollection)
                                        GetStatus(915,1,'JOB',p_web) ! Paid Awaiting Collection
                                    ELSE ! IF
                                        GetStatus(916,1,'JOB',p_web) ! Paid Awaiting Despatch
                                    END !IF
                                END ! IF
                            END ! IF
							
                            IF (p_web.GSV('job:Despatched') = 'REA' AND p_web.GSV('job:Despatch_Type') = 'JOB')
                                GetStatus(p_web.GSV('Default:StatusSendToRRC'),1,'JOB',p_web) ! Send To RRC
                            END ! IF
                            !endregion
                        END ! IF (p_web.GSV('BookingSite') = 'RRC')
                    END ! IF
                    IF (bal$ > 0)
                        IF (p_web.GSV('job:Paid') = 'YES')
                            p_web.SSV('job:Paid','NO')
                        END
                    END !IF
                END ! IF (tot$ = 0)
				
                IF (SUB(p_web.GSV('job:Current_Status'),1,3) = '910' AND p_web.GSV('job:Paid') = 'NO')
                    IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                        GetStatus(905,1,'JOB',p_web) ! Despatch Unpaid
                    END
                END
            END ! IF
			
            IF (p_web.GSV('job:Warranty_Job') = 'YES')
                IF (p_web.GSV('job:Invoice_Number_Warranty') > 0)
                    p_web.SSV('job:Paid_Warranty','YES')
                ELSE
                    p_web.SSV('job:Paid_Warranty','NO')
                END
            END ! IF
        ELSE ! IF (p_web.GSV('job:Date_Completed') > 0)
            p_web.SSV('job:Paid','NO')
            p_web.SSV('job:Paid_Warranty','NO')
        END ! IF

        
!endregion				

