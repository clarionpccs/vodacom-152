

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER232.INC'),ONCE        !Local module procedure declarations
                     END


BuildStockAuditQueue PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:BuildStockAuditQueue -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
recordCount                 LONG(0)
locUsed                     LONG(0)
totalRecords                LONG()
recordsRead                 LONG()
percent LONG()

                    MAP
Export                  PROCEDURE(STRING pShelfLocation)
UpdateProgressBar   PROCEDURE(STRING pText,LONG pPercentage)
                    END ! MAP

i                           LONG()
qTotal  LONG()  

skip LONG()
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
  CODE
  GlobalErrors.SetProcedureName('BuildStockAuditQueue')
 IF (p_web.GSV('pUpdateProgress') = 0)
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
!----------- put your html code here -----------------------------------
    END ! IF
!region Processed Code
        ! Clear Temp Queue
        totalRecords = RECORDS(qStockCheck)
        
        
        
        qTotal = RECORDS(qStockCheck)
        
        LOOP i = RECORDS(qStockCheck) TO 1 BY -1
            GET(qStockCheck,i)
            
            IF (p_web.GSV('pUpdateProgress') = 1)
                recordsRead += 1
                skip += 1
                IF skip > 100
                    skip = 0
                    percent = INT((recordsRead / TotalRecords) * 100)
                    IF (percent > 100)
                        recordsRead = 0
                        percent = 0
                    END ! IF
                    !UpdateProgress(p_web,percent,p_web.GSV('ReturnURL'),'Preparing..')
                    UpdateProgressBar('Preparing...',percent)
                END ! IF
                
            END ! IF	
            
            IF (qStockCheck.SessionID = p_web.SessionID)
                DELETE(qStockCheck)
                IF (ERROR())
                    
                    
                    
                END !I F
            END ! IF
        END ! LOOP


!        LOOP i = 1 TO RECORDS(qStockCheck)
!            GET(qStockCheck,i)
!            IF (pUpdateProgress = 1)
!                recordsRead += 1
!                skip += 1
!                IF skip > 100
!                    skip = 0
!                    percent = INT((recordsRead / TotalRecords) * 100)
!                    IF (percent > 100)
!                        recordsRead = 0
!                        percent = 0
!                    END ! IF
!                    UpdateProgress(p_web,percent,p_web.GSV('ReturnURL'),'Preparing..')
!                END ! IF
!                
!            END ! IF	
!            BHAddToDebugLog('BuildStockAuditQueue: ' & i & ') SessionID = ' & qStockCheck.SessionID & ' - ' & p_web.SessionID)
!            IF (qStockCheck.SessionID <> p_web.SessionID)
!                CYCLE
!            END ! IF
!            
!            DELETE(qStockCheck)	
!            IF (ERROR())
!                BHAddToDebugLog('BuildStockAuditQueue: DELETE ERROR = ' & ERROR())
!            END !I F
!            
!        END ! LOOP			
        
        
        
        Relate:STOCK.Open()
        Relate:SBO_GenericTagFile.Open()
        STREAM(STOCK)
        
        totalRecords = (RECORDS(STOCK) / 10)
        
        IF (p_web.GSV('locAllShelfLocations') = 1)
            Access:LOCSHELF.ClearKey(los:Shelf_Location_Key)
            los:Site_Location = p_web.GSV('BookingSiteLocation')
            SET(los:Shelf_Location_Key,los:Shelf_Location_Key)
            LOOP UNTIL Access:LOCSHELF.Next() <> Level:Benign
                IF (los:Site_Location <> p_web.GSV('BookingSiteLocation'))
                    BREAK
                END ! IF
            
                Export(los:Shelf_Location)
            END ! LOOP
        ELSE
            Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
            tagf:SessionID = p_web.SessionID
            tagf:TagType     = kShelfLocation
            SET(tagf:KeyTagged,tagf:KeyTagged)
            LOOP UNTIL Access:SBO_GenericTagFile.Next() <> Level:Benign
                IF (tagf:SessionID <> p_web.SessionID OR |
                    tagf:TagType <> kShelfLocation)
                    BREAK
                END ! IF
                IF (tagf:Tagged <> 1)
                    CYCLE
                END ! IF
                Export(tagf:TaggedValue)
                		
            END ! LOOP
        END ! IF
        
        
        FLUSH(STOCK)
        
        Relate:STOCK.Close()
        Relate:SBO_GenericTagFile.Close()
        
        
        
        p_web.SSV('recordCount',recordCount)
!endregion
  IF (p_web.GSV('pUpdateProgress') = 0)
!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

    END ! 
    RETURN
!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
Export              PROCEDURE(STRING pShelfLocation)
    CODE
        
        Access:STOCK.ClearKey(sto:SecondLocKey)
        sto:Location = p_web.GSV('BookingSiteLocation')
        sto:Shelf_Location = pShelfLocation
        SET(sto:SecondLocKey,sto:SecondLocKey)
        LOOP UNTIL Access:STOCK.Next() <> Level:Benign
            IF (sto:Location <> p_web.GSV('BookingSiteLocation') OR |
                sto:Shelf_Location <> pShelfLocation)
                BREAK
            END ! IF
                
            IF (p_web.GSV('pUpdateProgress') = 1)
                recordsRead += 1
                skip += 1
                IF skip > 100
                    skip = 0
                    percent = INT((recordsRead / TotalRecords) * 100)
                    IF (percent > 100)
                        recordsRead = 0
                        percent = 0
                    END ! IF
!                UpdateProgress(p_web,percent,p_web.GSV('ReturnURL'),'Counting Stock..')
                    UpdateProgressBar('Counting Stock (' & pShelfLocation & ') ...',percent)
                END ! IF
            ELSE
                p_web.Noop()
            END ! IF
				
				! Validation
            IF (p_web.GSV('locIncludeSuspendedParts') <> 1)
                IF (sto:Suspend = 1)
                    CYCLE
                END !I F
            END ! IF
            IF (p_web.GSV('locIncludeAccessories') <> 1)
                IF (sto:Accessory = 'YES')
                    CYCLE
                END ! IF
            END ! IF
            IF (p_web.GSV('pBuildForAudit') <> 1)
            ! Don't exclude for Audits
                IF (p_web.GSV('locSuppressZerosOnReport') = 1)
                    IF (sto:Quantity_Stock = 0)
                        CYCLE
                    END ! IF
                END ! IF
            END ! IF
                
            IF (p_web.GSV('locAllManufacturers') <> 1)
                IF (sto:Manufacturer <> p_web.GSV('locManufacturer'))
                    CYCLE
                END ! IF
            END ! IF
            IF (p_web.GSV('locAllPriceBands') <> 1)
                IF (sto:Purchase_Cost < p_web.GSV('prb:MinPrice') OR sto:Purchase_Cost > p_web.GSV('prb:MaxPrice'))
                    CYCLE
                END ! IF
            END ! IF
                
            IF (p_web.GSV('pBuildForAudit') <> 1)
            ! Ignore For Audits
				
                IF (p_web.GSV('locShowUsageData') = 1)
                    Relate:STOHIST.Open()
                    locUsed = 0
                    Access:STOHIST.ClearKey(shi:Ref_Number_Key)
                    shi:Ref_Number = sto:Ref_Number
                    shi:Date	= p_web.GSV('locStartDate')
                    SET(shi:Ref_Number_Key,shi:Ref_Number_Key)
                    LOOP UNTIL Access:STOHIST.Next() <> Level:Benign
                        IF (shi:Ref_Number <> sto:Ref_Number OR |
                            shi:Date > p_web.GSV('locEndDate'))
                            BREAK
                        END ! IF
						
                        IF (shi:Job_Number = 0)
                            CYCLE
                        END ! IF
                        IF (shi:Notes = 'STOCK READJUSTED FROM SERVICEBASE DOS')
                            CYCLE
                        END ! IF
                        IF (shi:Transaction_Type = 'DEC')
                            locUsed += shi:Quantity
                        END ! IF
                        IF (shi:Transaction_Type = 'REC')	
                            locUsed -= shi:Quantity
                        END ! IF
                    END ! LOOP
                    Relate:STOHIST.Close()
                END ! IF
            END !I F
                
            CLEAR(qStockCheck)
            qStockCheck.SessionID = p_web.SessionID
            qStockCheck.RefNumber = sto:Ref_Number
            qStockCheck.Description = sto:Description
            qStockCheck.ShelfLocation = sto:Shelf_Location
            qStockCheck.Usage = locUsed
            ADD(qStockCheck)
        
            recordCount += 1
				
        END ! LOOP	

UpdateProgressBar   PROCEDURE(STRING pText,LONG pPercentage)
    CODE
        packet = CLIP(packet) & '<script>updateProgressBar("' & CLIP(pText) & '",' & pPercentage & ');</script>'
        DO SendPacket 
