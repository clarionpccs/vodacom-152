

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER170.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER165.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseJobFaultCodeLookup PROCEDURE  (NetWebServerWorker p_web)
local:FaultCodeValue STRING(255)                           !
FieldNumber          LONG                                  !
i                               LONG
recs                            LONG
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(MANFAULO)
                      Project(mfo:RecordNumber)
                      Project(mfo:Field)
                      Project(mfo:Description)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
MANFAUPA::State  USHORT
MODELCCT::State  USHORT
MANFAULO_ALIAS::State  USHORT
MANFPARL::State  USHORT
MANFAUPA_ALIAS::State  USHORT
MANFPALO_ALIAS::State  USHORT
MANFAURL::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseJobFaultCodeLookup')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseJobFaultCodeLookup:NoForm')
      loc:NoForm = p_web.GetValue('BrowseJobFaultCodeLookup:NoForm')
      loc:FormName = p_web.GetValue('BrowseJobFaultCodeLookup:FormName')
    else
      loc:FormName = 'BrowseJobFaultCodeLookup_frm'
    End
    p_web.SSV('BrowseJobFaultCodeLookup:NoForm',loc:NoForm)
    p_web.SSV('BrowseJobFaultCodeLookup:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseJobFaultCodeLookup:NoForm')
    loc:FormName = p_web.GSV('BrowseJobFaultCodeLookup:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseJobFaultCodeLookup') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseJobFaultCodeLookup')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(MANFAULO,mfo:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'MFO:FIELD') then p_web.SetValue('BrowseJobFaultCodeLookup_sort','1')
    ElsIf (loc:vorder = 'MFO:DESCRIPTION') then p_web.SetValue('BrowseJobFaultCodeLookup_sort','2')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseJobFaultCodeLookup:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseJobFaultCodeLookup:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseJobFaultCodeLookup:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseJobFaultCodeLookup:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseJobFaultCodeLookup:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  ! Save Window Name
  AddToLog('NetWebBrowse',p_web.RequestData.DataString,'BrowseJobFaultCodeLookup',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  !region OpenBrowse
    if (p_web.ifExistsValue('fieldNumber'))
        p_web.storeValue('fieldNumber')
    end !if (p_web.ifExistsValue('fieldNumber'))
    if (p_web.ifExistsValue('partType'))
        p_web.storeValue('partType')
    end ! if (p_web.ifExistsValue('partType'))
  ! Has this been called from Parts Main Fault?
    if (p_web.ifExistsValue('partMainFault'))
        p_web.storeValue('partMainFault')
    end ! if (p_web.ifExistsValue('partMainFault'))
    if (p_web.ifExistsValue('relatedPartCode'))
        p_web.storeValue('relatedPartCode')
    end ! if (p-web.ifExistsValue('relatedPartCode'))
    
    FieldNumber = p_web.GSV('FieldNumber')
    BIND('FieldNumber',FieldNumber)
  
    
    
    ! Clear Q
    recs = RECORDS(qLinkedFaultCodes)
    LOOP i = recs TO 1 BY -1
        GET(qLinkedFaultCodes,i)
        IF (qLinkedFaultCodes.SessionID = p_web.SessionID)
            DELETE(qLinkedFaultCodes)
        END ! IF
    END ! LOOP
    
  
    if (p_web.GSV('BrowseJobFaultCodeLookup:LookupFrom') = 'JobFaultCodes' OR | 
        p_web.GSV('BrowseJobFaultCodeLookup:LookupFrom') = 'ViewFaultCodes')
        BuildLinkedFaultCodeQ(p_web,FieldNumber)
        
  !        loop x# = 1 to 20
  !            if (p_web.GSV('Hide:JobFaultCode' & x#) = 1 )
  !                cycle
  !            end ! if (p_web.GSV('Hide:JobFaultCode' & x#) = 1 )
  !            if (p_web.GSV('tmp:FaultCode' & x#) = '')
  !                cycle
  !            end ! if (p_web.GSV('tmp:FaultCode' & x#) = '')
  !  
  !            Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
  !            maf:Manufacturer    = p_web.GSV('job:Manufacturer')
  !            maf:ScreenOrder    = x#
  !            if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
  !              ! Found
  !                if (maf:Lookup = 'YES')
  !                    Access:MANFAULO_ALIAS.Clearkey(mfo_ali:Field_Key)
  !                    mfo_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
  !                    mfo_ali:Field_Number    = maf:Field_Number
  !                    mfo_ali:Field = p_web.GSV('tmp:FaultCode' & x#)
  !                    if (Access:MANFAULO_ALIAS.TryFetch(mfo_ali:Field_Key) = Level:Benign)
  !                      ! Found
  !                        found# = 0
  !                        Access:MANFAURL.Clearkey(mnr:FieldKey)
  !                        mnr:MANFAULORecordNumber    = mfo_ali:RecordNumber
  !  !                    mnr:PartFaultCode    = 1
  !                        mnr:FieldNumber    = p_web.GSV('fieldNumber')
  !                        set(mnr:FieldKey,mnr:FieldKey)
  !                        loop
  !                            if (Access:MANFAURL.Next())
  !                                Break
  !                            end ! if (Access:MANFAURL.Next())
  !                            if (mnr:MANFAULORecordNumber    <> mfo_ali:RecordNumber)
  !                                Break
  !                            end ! if (mnr:MANFALORecordNumber    <> mfo:RecordNumber)
  !  !                        if (mnr:PartFaultCode    <> 1)
  !  !                            Break
  !  !                        end ! if (mnr:PartFaultCode    <> 1)
  !                            if (mnr:FieldNumber    <> p_web.GSV('fieldNumber'))
  !                                Break
  !                            end ! if (mnr:FieldNumber    <> fieldNumber)
  !                            found# = 1
  !                            break
  !                        end ! loop
  !                        if (found# = 1)
  !                            
  !                            qLinkedFaultCodes.SessionID = p_web.SessionID
  !                            qLinkedFaultCodes.RecordNumber = mfo_ali:RecordNumber
  !                            qLinkedFaultCodes.FaultType = 'J'
  !                            ADD(qLinkedFaultCodes)
  !                        end ! if (found# = 1)
  !                    else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
  !                      ! Error
  !                    end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
  !                end ! if (maf:Lookup = 'YES')
  !            else ! if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
  !              ! Error
  !            end ! if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
  !  
  !  
  !            local:FaultCodeValue = ''
  !            Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
  !            wpr:Ref_Number = p_web.GSV('job:Ref_Number')
  !            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
  !            Loop
  !                If Access:WARPARTS.Next()
  !                    Break
  !                End ! If Access:WARPARTS.Next()
  !                If wpr:Ref_Number <> p_web.GSV('job:Ref_Number')
  !                    Break
  !                End ! If wpr:Ref_Number <> job:Ref_Number
  !                Loop f# = 1 To 12
  !                    Case f#
  !                    Of 1
  !                        If wpr:Fault_Code1 = ''
  !                            Cycle
  !                        End ! If wpr:Fault_Code1 = ''
  !                        local:FaultCodeValue = wpr:Fault_Code1
  !                    Of 2
  !                        If wpr:Fault_Code2 = ''
  !                            Cycle
  !                        End ! If wpr:Fault_Code1 = ''
  !                        local:FaultCodeValue = wpr:Fault_Code2
  !                    Of 3
  !                        If wpr:Fault_Code3 = ''
  !                            Cycle
  !                        End ! If wpr:Fault_Code1 = ''
  !                        local:FaultCodeValue = wpr:Fault_Code3
  !                    Of 4
  !                        If wpr:Fault_Code4 = ''
  !                            Cycle
  !                        End ! If wpr:Fault_Code1 = ''
  !                        local:FaultCodeValue = wpr:Fault_Code4
  !                    Of 5
  !                        If wpr:Fault_Code5 = ''
  !                            Cycle
  !                        End ! If wpr:Fault_Code1 = ''
  !                        local:FaultCodeValue = wpr:Fault_Code5
  !                    Of 6
  !                        If wpr:Fault_Code6 = ''
  !                            Cycle
  !                        End ! If wpr:Fault_Code1 = ''
  !                        local:FaultCodeValue = wpr:Fault_Code6
  !                    Of 7
  !                        If wpr:Fault_Code7 = ''
  !                            Cycle
  !                        End ! If wpr:Fault_Code1 = ''
  !                        local:FaultCodeValue = wpr:Fault_Code7
  !                    Of 8
  !                        If wpr:Fault_Code8 = ''
  !                            Cycle
  !                        End ! If wpr:Fault_Code1 = ''
  !                        local:FaultCodeValue = wpr:Fault_Code8
  !                    Of 9
  !                        If wpr:Fault_Code9 = ''
  !                            Cycle
  !                        End ! If wpr:Fault_Code1 = ''
  !                        local:FaultCodeValue = wpr:Fault_Code9
  !                    Of 10
  !                        If wpr:Fault_Code10 = ''
  !                            Cycle
  !                        End ! If wpr:Fault_Code1 = ''
  !                        local:FaultCodeValue = wpr:Fault_Code10
  !                    Of 11
  !                        If wpr:Fault_Code11 = ''
  !                            Cycle
  !                        End ! If wpr:Fault_Code1 = ''
  !                        local:FaultCodeValue = wpr:Fault_Code11
  !                    Of 12
  !                        If wpr:Fault_Code12 = ''
  !                            Cycle
  !                        End ! If wpr:Fault_Code1 = ''
  !                        local:FaultCodeValue = wpr:Fault_Code12
  !                    End ! Case
  !  
  !                    Access:MANFAUPA.Clearkey(map:Field_Number_Key)
  !                    map:Manufacturer = p_web.GSV('job:Manufacturer')
  !                    map:Field_Number = f#
  !                        If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
  !                      ! Found
  !                            If map:UseRelatedJobCode
  !                                Access:MANFAULT_ALIAS.Clearkey(maf_ali:MainFaultKey)
  !                                maf_ali:Manufacturer = p_web.GSV('job:Manufacturer')
  !                                maf_ali:MainFault = 1
  !                                If Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign
  !                              ! Found
  !                                    Access:MANFAULO_ALIAS.Clearkey(mfo_ali:RelatedFieldKey)
  !                                    mfo_ali:Manufacturer = p_web.GSV('job:Manufacturer')
  !                                    mfo_ali:RelatedPartCode = map:Field_Number
  !                                    mfo_ali:Field_Number = maf_ali:Field_Number
  !                                    mfo_ali:Field = local:FaultCodeValue
  !                                    If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RelatedFieldKey) = Level:Benign
  !                                  ! Found
  !                                        Found# = 0
  !                                        Access:MANFAURL.Clearkey(mnr:FieldKey)
  !                                        mnr:MANFAULORecordNumber = mfo_ali:RecordNumber
  !                                        mnr:PartFaultCode = 0
  !                                        mnr:FieldNumber = p_web.GSV('fieldNumber')
  !                                        Set(mnr:FieldKey,mnr:FieldKey)
  !                                        Loop
  !                                            If Access:MANFAURL.Next()
  !                                                Break
  !                                            End ! If Access:MANFAURL.Next()
  !                                            If mnr:MANFAULORecordNumber <> mfo_ali:RecordNumber
  !                                                Break
  !                                            End ! If mnr:MANFAULORecordNumber <> mfo_ali:RecordNumber
  !                                            If mnr:PartFaultCode <> 0
  !                                                Break
  !                                            End ! If mnr:PartFaultCOde <> 1
  !                                            If mnr:FieldNUmber <> p_web.GSV('fieldNumber')
  !                                                Break
  !                                            End ! If mnr:FIeldNUmber <> maf:Field_Number
  !                                            If mnr:RelatedPartFaultCode > 0
  !                                                If mnr:RelatedPartFaultCode <> map:Field_Number
  !                                                    Cycle
  !                                                End ! If mnr:RelatedPartFaultCode <> map_ali:Field_Number
  !                                            End ! If mnr:RelaterdPartFaultCode > 0
  !                                            Found# = 1
  !                                            Break
  !                                        End ! Loop (MANFAURL)
  !                                        If Found# = 1
  !                                     
  !                                            qLinkedFaultCodes.SessionID = p_web.SessionID
  !                                            qLinkedFaultCodes.RecordNumber = mfo_ali:RecordNumber
  !                                            qLinkedFaultCodes.FaultType = 'J'
  !                                            ADD(qLinkedFaultCodes)
  !                                        End ! If Found# = 1
  !  
  !                                    Else ! If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RelatedFieldKey) = Level:Benign
  !                                    End ! If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RelatedFieldKey) = Level:Benign
  !                                Else ! If Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign
  !                                End ! If Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign
  !                            Else ! If map:UseRelatedJobCode
  !                                Access:MANFPALO.Clearkey(mfp:Field_Key)
  !                                mfp:Manufacturer = p_web.GSV('job:Manufacturer')
  !                                mfp:Field_Number = map:Field_Number
  !                                mfp:Field = local:FaultCodeValue
  !                                If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
  !                              ! Found
  !                                    Access:MANFPARL.Clearkey(mpr:FieldKey)
  !                                    mpr:MANFPALORecordNumber = mfp:RecordNumber
  !                                    mpr:JobFaultCode = 1
  !                                    mpr:FieldNumber = p_web.GSV('fieldNumber')
  !                                    Set(mpr:FieldKey,mpr:FieldKey)
  !                                    Loop
  !                                        If Access:MANFPARL.Next()
  !                                            Break
  !                                        End ! If Access:MANFPARL.Next()
  !                                        If mpr:MANFPALORecordNumber <> mfp:RecordNumber
  !                                            Break
  !                                        End ! If mpr:MANFPALORecordNumber <> mfp:RecordNumber
  !                                        If mpr:JobFaultCode <> 1
  !                                            Break
  !                                        End ! If mpr:JobFaultCode <> 1
  !                                        If mpr:FieldNumber <> p_web.GSV('fieldNumber')
  !                                            Break
  !                                        End ! If mpr:FieldNumber <> maf:Field_Number
  !  
  !                                        Found# = 1
  !                                        Break
  !                                    End ! Loop (MANFPARL)
  !                                    If Found# = 1
  !                                   
  !                                        qLinkedFaultCodes.SessionID = p_web.SessionID
  !                                        qLinkedFaultCodes.RecordNumber = mfo_ali:RecordNumber
  !                                        qLinkedFaultCodes.FaultType = 'J'
  !                                        ADD(qLinkedFaultCodes)
  !                                    End ! If Found# = 1
  !                                Else ! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
  !                                End ! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
  !                            End ! If map:UseRelatedJobCode
  !                        Else ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
  !                        End ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
  !                    End ! Loop f# = 1 To 20
  !  
  !                    End ! Loop (WARPARTS)
  !  
  !                end
            else ! if (p_web.GSV('BrowseJobFaultCodeLookup:LookupFrom') = 'JobFaultCodes')
                if (p_web.GSV('partMainFault') = 0)
          ! Build Associated Fault Codes Queue
  
                    loop x# = 1 to 12
                        if (x# = p_web.GSV('fieldNumber'))
                            cycle
                        end ! if (x# = fieldNumber)
  
                        if (p_web.GSV('Hide:PartFaultCode' & x#))
                            cycle
                        end ! if (p_web.GSV('Hide:PartFaultCode' & x#))
  
                        if (p_web.GSV('tmp:FaultCodes' & x#) = '')
                            cycle
                        end ! if (p_web.GSV('tmp:FaultCodes' & x#) = '')
  
                        Access:MANFAUPA_ALIAS.Clearkey(map_ali:ScreenOrderKey)
                        map_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
                        map_ali:ScreenOrder    = x#
                        if (Access:MANFAUPA_ALIAS.TryFetch(map_ali:ScreenOrderKey) = Level:Benign)
                  ! Found
                            Access:MANFPALO_ALIAS.Clearkey(mfp_ali:Field_Key)
                            mfp_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
                            mfp_ali:Field_Number    = map_ali:Field_Number
                            mfp_ali:Field    = p_web.GSV('tmp:FaultCodes' & x#)
                            if (Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign)
                      ! Found
                                found# = 0
  
                                Access:MANFPARL.Clearkey(mpr:FieldKey)
                                mpr:MANFPALORecordNumber    = mfp_ali:RecordNumber
                                mpr:FieldNumber    = p_web.GSV('fieldNumber')
                                set(mpr:FieldKey,mpr:FieldKey)
                                loop
                                    if (Access:MANFPARL.Next())
                                        Break
                                    end ! if (Access:MANFPARL.Next())
                                    if (mpr:MANFPALORecordNumber    <> mfp_ali:RecordNumber)
                                        Break
                                    end ! if (mfp:MANFPALORecordNumber    <> mfp_ali:RecordNumber)
                                    if (mpr:FieldNumber    <> p_web.GSV('fieldNumber'))
                                        Break
                                    end ! if (mfp:FieldNumber    <> p_web.GSV('fieldNumber'))
                                    found# = 1
                                    break
                                end ! loop
  
                                if (found# = 1)
                         
                                    qLinkedFaultCodes.SessionID = p_web.SessionID
                                    qLinkedFaultCodes.RecordNumber = mfo_ali:RecordNumber
                                    qLinkedFaultCodes.FaultType = 'J'
                                    ADD(qLinkedFaultCodes)
                                end ! if (found# = 1)
  
                            else ! if (Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign)
                      ! Error
                            end ! if (Access:MANFPALO_ALIAS.TryFetch(mfp_ali:Field_Key) = Level:Benign)
                        else ! if (Access:MANFAUPA_ALIAS.TryFetch(map_ali:Field_Number_Key) = Level:Benign)
                  ! Error
                        end ! if (Access:MANFAUPA_ALIAS.TryFetch(map_ali:Field_Number_Key) = Level:Benign)
  
                    end
                    loop x# = 1 to 20
                        Access:MANFAULT.Clearkey(maf:Field_Number_Key)
                        maf:Manufacturer    = p_web.GSV('job:Manufacturer')
                        maf:Field_Number    = x#
                        if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
                  ! Found
                            if (maf:Lookup = 'YES')
  
                                Access:MANFAULO.Clearkey(mfo:Field_Key)
                                mfo:Manufacturer    = p_web.GSV('job:Manufacturer')
                                mfo:Field_Number    = maf:Field_Number
                                if (x# < 13)
                                    mfo:Field = p_web.GSV('job:Fault_Code' & x#)
                                else ! if (x# < 13)
                                    mfo:Field = p_web.GSV('wob:FaultCode' & x#)
                                end !if (x# < 13)
                                if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                          ! Found
                                    found# = 0
                                    Access:MANFAURL.Clearkey(mnr:FieldKey)
                                    mnr:MANFAULORecordNumber    = mfo:RecordNumber
                                    mnr:PartFaultCode    = 1
                                    mnr:FieldNumber    = p_web.GSV('fieldNumber')
                                    set(mnr:FieldKey,mnr:FieldKey)
                                    loop
                                        if (Access:MANFAURL.Next())
                                            Break
                                        end ! if (Access:MANFAURL.Next())
                                        if (mnr:MANFAULORecordNumber    <> mfo:RecordNumber)
                                            Break
                                        end ! if (mnr:MANFALORecordNumber    <> mfo:RecordNumber)
                                        if (mnr:PartFaultCode    <> 1)
                                            Break
                                        end ! if (mnr:PartFaultCode    <> 1)
                                        if (mnr:FieldNumber    <> p_web.GSV('fieldNumber'))
                                            Break
                                        end ! if (mnr:FieldNumber    <> fieldNumber)
                                        found# = 1
                                        break
                                    end ! loop
                                    if (found# = 1)
  
                                        qLinkedFaultCodes.SessionID = p_web.SessionID
                                        qLinkedFaultCodes.RecordNumber = mfo_ali:RecordNumber
                                        qLinkedFaultCodes.FaultType = 'J'
                                        ADD(qLinkedFaultCodes)
                                    end ! if (found# = 1)
                                else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                          ! Error
                                end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                            end ! if (maf:Lookup = 'YES')
                        else ! if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
                  ! Error
                        end ! if (Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign)
              
                    end ! loop x# = 1 to 12
  
                end ! if (p_web.GSV('partMainFault') = 0)
  
            end !if (p_web.GSV('BrowseJobFaultCodeLookup:LookupFrom') = 'JobFaultCodes')
            !CLOSE(fFaultCodes)
  
  
  !endregion  
  p_web.site.SmallSelectButton.TextValue = p_web.Translate('>>')
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseJobFaultCodeLookup_sort',net:DontEvaluate)
  If loc:vordernumber = 0
    loc:vordernumber = 1
  End
  p_web.SetSessionValue('BrowseJobFaultCodeLookup_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 3
    Loc:LocateField = ''
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(mfo:Field)','-UPPER(mfo:Field)')
    Loc:LocateField = 'mfo:Field'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(mfo:Description)','-UPPER(mfo:Description)')
    Loc:LocateField = 'mfo:Description'
  end
  if loc:vorder = ''
    loc:vorder = '+UPPER(mfo:Manufacturer),+mfo:Field_Number,+UPPER(mfo:Field)'
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('mfo:Field')
    loc:SortHeader = p_web.Translate('Field')
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_LocatorPic','@s30')
  Of upper('mfo:Description')
    loc:SortHeader = p_web.Translate('Description')
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_LocatorPic','@s60')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseJobFaultCodeLookup:LookupFrom')
  End!Else
  loc:formaction = 'BrowseJobFaultCodeLookup'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseJobFaultCodeLookup:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseJobFaultCodeLookup:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseJobFaultCodeLookup:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="MANFAULO"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="mfo:RecordNumberKey"></input><13,10>'
  end
  If p_web.Translate('Select Fault Codes') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Select Fault Codes',0)&'</span>'&CRLF
  End
  If clip('Select Fault Codes') <> ''
    !packet = clip(packet) & p_web.br !Bryan. Why is this here?
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobFaultCodeLookup',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseJobFaultCodeLookup',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseJobFaultCodeLookup.locate(''Locator2BrowseJobFaultCodeLookup'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseJobFaultCodeLookup.cl(''BrowseJobFaultCodeLookup'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="sortable" id="BrowseJobFaultCodeLookup_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('BrowseLookup')&'"><table class="'&clip('BrowseTable')&'" id="BrowseJobFaultCodeLookup_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
    If loc:Selecting = 1
        packet = clip(packet) & '<th>'&p_web.Translate('Pick')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
    End ! Selecting
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseJobFaultCodeLookup','Field','Click here to sort by Field',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Field')&'">'&p_web.Translate('Field')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseJobFaultCodeLookup','Description','Click here to sort by Description',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Description')&'">'&p_web.Translate('Description')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('mfo:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and MANFAULO{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'mfo:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('mfo:RecordNumber'),p_web.GetValue('mfo:RecordNumber'),p_web.GetSessionValue('mfo:RecordNumber'))
      loc:FilterWas = 'mfo:NotAvailable = 0 AND UPPER(mfo:Manufacturer) = UPPER(''' & p_web.GSV('job:Manufacturer') & ''') AND mfo:Field_Number = FieldNumber'
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobFaultCodeLookup',Net:Both)
  !  ThisView{prop:order} = clip(loc:vorder)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseJobFaultCodeLookup_Filter')
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_FirstValue','')
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,MANFAULO,mfo:RecordNumberKey,loc:PageRows,'BrowseJobFaultCodeLookup',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If MANFAULO{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(MANFAULO,loc:firstvalue)
              Reset(ThisView,MANFAULO)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If MANFAULO{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(MANFAULO,loc:lastvalue)
            Reset(ThisView,MANFAULO)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      !region ValidateRecord
      !        IF (mfo:NotAvailable)
      !          ! #11655 Don't show IF not available (Bryan: 24/08/2010)
      !            CYCLE
      !        END
      
        IF (p_web.GSV('partType') = 'C')
            IF (mfo:RestrictLookup = 1)
                IF (p_web.GSV('par:Adjustment') = 'YES')
                    IF (mfo:RestrictLookupType <> 3)
                        CYCLE
                    END ! IF (mfo:RestrictLookupType <> 3)
                ELSE ! IF (p_web.GSV('par:Adjustment') = 'YES')
                    IF (p_web.GSV('par:Correction') = 1)
                        IF (mfo:RestrictLookupType <> 2)
                            CYCLE
                        END ! IF (mfo:RestrictLookupType <> 2)
                    ELSE ! IF (p_web.GSV('par:Correction') = 1)
                        IF (mfo:RestrictLookupType <> 1)
                            CYCLE
                        END ! IF (mfo:RestrictLookupType <> 1)
                    END ! IF (p_web.GSV('par:Correction') = 1)
                END ! IF (p_web.GSV('par:Adjustment') = 'YES')
            END ! IF (mfo:RestrictLookup = 1)
        END ! IF (p_web.GSV('partType') = 'C')
      
        IF (p_web.GSV('partType') = 'W')
            IF (mfo:RestrictLookup = 1)
                IF (p_web.GSV('wpr:Adjustment') = 'YES')
                    IF (mfo:RestrictLookupType <> 3)
                        CYCLE
                    END ! IF (mfo:RestrictLookupType <> 3)
                ELSE ! IF (p_web.GSV('par:Adjustment') = 'YES')
                    IF (p_web.GSV('wpr:Correction') = 1)
                        IF (mfo:RestrictLookupType <> 2)
                            CYCLE
                        END ! IF (mfo:RestrictLookupType <> 2)
                    ELSE ! IF (p_web.GSV('par:Correction') = 1)
                        IF (mfo:RestrictLookupType <> 1)
                            CYCLE
                        END ! IF (mfo:RestrictLookupType <> 1)
                    END ! IF (p_web.GSV('par:Correction') = 1)
                END ! IF (p_web.GSV('par:Adjustment') = 'YES')
            END ! IF (mfo:RestrictLookup = 1)
        END ! IF (p_web.GSV('partType') = 'C')
      
        IF (mfo:JobTypeAvailability = 1 AND p_web.GSV('partType') <> '')
            IF (p_web.GSV('partType') <> 'C')
                CYCLE
            END ! IF (p_web.GSV('job:Warranty_Job') = 'YES')
        END ! IF (mfo:JobTypeAvailability = 1)
      
        IF (mfo:JobTypeAvailability = 2 AND p_web.GSV('partType') <> '')
            IF (p_web.GSV('parType') <> 'W')
                CYCLE
            END ! IF (p_web.GSV('job:Chargeable_Job') = 'YES')
        END ! IF (mfo:JobTypeAvailability = 2)
      
      
      !IF (p_web.GSV('partMainFault') = 0)
      !        found# = 1
      !        CLEAR(tmpfau:record)
      !        tmpfau:sessionID = p_web.sessionID
      !        SET(tmpfau:keySessionID,tmpfau:keySessionID)
      !        LOOP
      !            NEXT(tempFaultCodes)
      !            IF (ERROR())
      !                BREAK
      !            END ! IF (ERROR())
      !            IF (tmpfau:sessionID <> p_web.sessionID)
      !                BREAK
      !            END ! IF (tmpfau:sessionID <> p_web.sessionID)
      !      
      !            found# = 0
      !            CASE tmpfau:FaultType
      !            OF 'P' ! Part Fault Code
      !                Access:MANFPARL.ClearKey(mpr:LinkedRecordNumberKey)
      !                mpr:MANFPALORecordNumber    = tmpfau:RecordNumber
      !                mpr:LinkedRecordNumber    = mfo:RecordNumber
      !                mpr:JobFaultCode    = 0
      !                IF (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
      !                      ! Found
      !                    found# = 1
      !                    BREAK
      !                ELSE ! IF (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
      !                      ! ERROR
      !                END ! IF (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
      !      
      !            OF 'J' ! Job Fault Code
      !      
      !                Access:MANFAURL.ClearKey(mnr:LinkedRecordNumberKey)
      !                mnr:MANFAULORecordNumber    = tmpfau:RecordNumber
      !                mnr:LinkedRecordNumber    = mfo:RecordNumber
      !                mnr:PartFaultCode    = 1
      !                IF (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
      !                      ! Found
      !                    found# = 1
      !                    BREAK
      !                ELSE ! IF (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
      !                      ! ERROR
      !                END ! IF (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
      !            END ! CASE faultQueue.FaultType
      !        END ! LOOP
        
        found# = 1
      
      
              recs = RECORDS(qLinkedFaultCodes)
              LOOP i = 1 TO recs
                  GET(qLinkedFaultCodes,i)
                  IF (qLinkedFaultCodes.SessionID <> p_web.SessionID)
                      CYCLE
                  END ! IF
                  
                  ! There are "some" linked records, lets see if we can
                  ! find a match
                  found# = 0
                  CASE qLinkedFaultCodes.FaultType
                  OF 'P' ! Part Fault Code
                      Access:MANFPARL.ClearKey(mpr:LinkedRecordNumberKey)
                      mpr:MANFPALORecordNumber    = qLinkedFaultCodes.RecordNumber
                      mpr:LinkedRecordNumber    = mfo:RecordNumber
                      mpr:JobFaultCode    = 1
                      IF (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
                            ! Found
                          found# = 1
                          BREAK
                      ELSE ! IF (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
                            ! ERROR
                      END ! IF (Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign)
            
                  OF 'J' ! Job Fault Code
            
                      Access:MANFAURL.ClearKey(mnr:LinkedRecordNumberKey)
                      mnr:MANFAULORecordNumber    = qLinkedFaultCodes.RecordNumber
                      mnr:LinkedRecordNumber    = mfo:RecordNumber
                      mnr:PartFaultCode    = 0
                      IF (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
                            ! Found
                          found# = 1
                          BREAK
                      ELSE ! IF (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
                            ! ERROR
                      END ! IF (Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign)
                  END ! CASE faultQueue.FaultType
                  
              END ! LOOP
              
              !CLOSE(fFaultCodes)
              
              IF (found# = 0)
                  CYCLE
              END ! IF (found# = 0)
            !END !IF (p_web.GSV('partMainFault') = 0)
            
              IF (p_web.GSV('partMainFault') = 1 AND p_web.GSV('relatedPartCode') <> 0)
                  IF (mfo:RelatedPartCode <> p_web.GSV('relatedPartCode'))
                      CYCLE
                  END ! IF (mfo:RelatedPartCode <> p_web.GSV('relatedPartCode'))
              END ! IF (p_web.GSV('relatedPartCode') <> 0)
      !endregion        
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(mfo:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseJobFaultCodeLookup.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseJobFaultCodeLookup.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseJobFaultCodeLookup.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseJobFaultCodeLookup.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
    If loc:selecting = 1 !and loc:parent = ''
      If loc:found
        packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SelectButton,'BrowseJobFaultCodeLookup')
      End
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
    If loc:selecting = 1 !and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
      TableQueue.Kind = Net:BeforeTable
      do AddPacket
    End
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobFaultCodeLookup',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseJobFaultCodeLookup_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseJobFaultCodeLookup',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseJobFaultCodeLookup.locate(''Locator1BrowseJobFaultCodeLookup'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseJobFaultCodeLookup.cl(''BrowseJobFaultCodeLookup'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseJobFaultCodeLookup_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseJobFaultCodeLookup_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseJobFaultCodeLookup.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseJobFaultCodeLookup.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseJobFaultCodeLookup.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseJobFaultCodeLookup.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  If loc:selecting = 1 !and loc:parent = ''
    If loc:found
      packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SelectButton,'BrowseJobFaultCodeLookup')
    End
    do SendPacket
  End
  If loc:selecting = 1 !and loc:parent = ''
    packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:BrowseCancelButton,loc:FormName,loc:SelectAction)
    do SendPacket
  End
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = mfo:RecordNumber
    p_web._thisrow = p_web._nocolon('mfo:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseJobFaultCodeLookup:LookupField')) = mfo:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((mfo:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseJobFaultCodeLookup.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If MANFAULO{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(MANFAULO)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If MANFAULO{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(MANFAULO)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','mfo:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BrowseJobFaultCodeLookup.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','mfo:RecordNumber',clip(loc:field),,'checked',,,'onclick="BrowseJobFaultCodeLookup.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
        If Loc:Selecting = 1
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Select
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
        End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::mfo:Field
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::mfo:Description
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseJobFaultCodeLookup.omv(this);" onMouseOut="BrowseJobFaultCodeLookup.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseJobFaultCodeLookup=new browseTable(''BrowseJobFaultCodeLookup'','''&clip(loc:formname)&''','''&p_web._jsok('mfo:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('mfo:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseJobFaultCodeLookup.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseJobFaultCodeLookup.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseJobFaultCodeLookup')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseJobFaultCodeLookup')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseJobFaultCodeLookup')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseJobFaultCodeLookup')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(MANFAULO)
  p_web._CloseFile(MANFAUPA)
  p_web._CloseFile(MODELCCT)
  p_web._CloseFile(MANFAULO_ALIAS)
  p_web._CloseFile(MANFPARL)
  p_web._CloseFile(MANFAUPA_ALIAS)
  p_web._CloseFile(MANFPALO_ALIAS)
  p_web._CloseFile(MANFAURL)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(MANFAULO)
  Bind(mfo:Record)
  Clear(mfo:Record)
  NetWebSetSessionPics(p_web,MANFAULO)
  p_web._OpenFile(MANFAUPA)
  Bind(map:Record)
  NetWebSetSessionPics(p_web,MANFAUPA)
  p_web._OpenFile(MODELCCT)
  Bind(mcc:Record)
  NetWebSetSessionPics(p_web,MODELCCT)
  p_web._OpenFile(MANFAULO_ALIAS)
  Bind(mfo_ali:Record)
  NetWebSetSessionPics(p_web,MANFAULO_ALIAS)
  p_web._OpenFile(MANFPARL)
  Bind(mpr:Record)
  NetWebSetSessionPics(p_web,MANFPARL)
  p_web._OpenFile(MANFAUPA_ALIAS)
  Bind(map_ali:Record)
  NetWebSetSessionPics(p_web,MANFAUPA_ALIAS)
  p_web._OpenFile(MANFPALO_ALIAS)
  Bind(mfp_ali:Record)
  NetWebSetSessionPics(p_web,MANFPALO_ALIAS)
  p_web._OpenFile(MANFAURL)
  Bind(mnr:Record)
  NetWebSetSessionPics(p_web,MANFAURL)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('mfo:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
      ! Save Window Name
      IF (loc:alert <> '')
          AddToLog('Alert',loc:alert,'BrowseJobFaultCodeLookup',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
      END ! IF
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::Select   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Select_'&mfo:RecordNumber,,net:crc)
        If loc:SelectAction
          If(loc:SelectionMethod  = Net:Radio)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseJobFaultCodeLookup',loc:field)
          ElsIf(loc:SelectionMethod  = Net:Highlight)
            packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseJobFaultCodeLookup',loc:field)
          End
        Else
          packet = clip(packet) & p_web.CreateStdBrowseButton(Net:Web:SmallSelectButton,'BrowseJobFaultCodeLookup',loc:field)
        End
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::mfo:Field   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('mfo:Field_'&mfo:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(mfo:Field,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::mfo:Description   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('mfo:Description_'&mfo:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(mfo:Description,'@s60')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(MANFAUPA)
  p_web._OpenFile(MODELCCT)
  p_web._OpenFile(MANFAULO_ALIAS)
  p_web._OpenFile(MANFPARL)
  p_web._OpenFile(MANFAUPA_ALIAS)
  p_web._OpenFile(MANFPALO_ALIAS)
  p_web._OpenFile(MANFAURL)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MANFAUPA)
  p_Web._CloseFile(MODELCCT)
  p_Web._CloseFile(MANFAULO_ALIAS)
  p_Web._CloseFile(MANFPARL)
  p_Web._CloseFile(MANFAUPA_ALIAS)
  p_Web._CloseFile(MANFPALO_ALIAS)
  p_Web._CloseFile(MANFAURL)
     FilesOpened = False
  END
  p_web.deleteSessionValue('fieldNumber')
  p_web.deleteSessionValue('partType')
  p_web.deleteSessionValue('partMainFault')
  p_web.deleteSessionValue('relatedPartCode')
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = mfo:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('mfo:RecordNumber',mfo:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('mfo:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('mfo:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('mfo:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
