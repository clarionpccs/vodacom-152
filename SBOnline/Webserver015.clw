

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER015.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
VSingleInvoice PROCEDURE (NetWebServerWorker p_web)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
Default_Invoice_Company_Name_Temp STRING(30)               !
locInvoiceNumber     LONG                                  !
locInvoiceDate       DATE                                  !
save_tradeacc_id     USHORT,AUTO                           !
save_invoice_id      USHORT,AUTO                           !
save_jobs_id         USHORT,AUTO                           !
save_jobse_id        USHORT,AUTO                           !
save_audit_id        USHORT,AUTO                           !
save_aud_id          USHORT,AUTO                           !
tmp:PrintA5Invoice   BYTE                                  !
tmp:Ref_Number       STRING(20)                            !
tmp:DefaultTelephone STRING(20)                            !
tmp:DefaultFax       STRING(20)                            !
save_par_id          USHORT,AUTO                           !
save_jpt_id          USHORT,AUTO                           !
tmp:PrintedBy        STRING(255)                           !
code_temp            BYTE                                  !
option_temp          BYTE                                  !
Bar_code_string_temp CSTRING(21)                           !
Bar_Code_Temp        CSTRING(21)                           !
Bar_Code2_Temp       CSTRING(21)                           !
accessories_temp     STRING(30),DIM(6)                     !
estimate_value_temp  STRING(40)                            !
despatched_user_temp STRING(40)                            !
vat_temp             REAL                                  !
total_temp           REAL                                  !
part_number_temp     STRING(30)                            !
line_cost_temp       REAL                                  !
job_number_temp      STRING(20)                            !
esn_temp             STRING(24)                            !
customer_name_temp   STRING(60)                            !
sub_total_temp       REAL                                  !
payment_date_temp    DATE,DIM(4)                           !
other_amount_temp    REAL                                  !
total_paid_temp      REAL                                  !
payment_type_temp    STRING(20),DIM(4)                     !
payment_amount_temp  REAL,DIM(4)                           !
invoice_company_name_temp STRING(30)                       !
invoice_telephone_number_temp STRING(15)                   !
invoice_fax_number_temp STRING(15)                         !
DefaultAddress       GROUP,PRE(address)                    !
SiteName             STRING(40)                            !
Name                 STRING(30)                            !
Name2                STRING(40)                            !
Location             STRING(40)                            !
RegistrationNo       STRING(30)                            !
AddressLine1         STRING(40)                            !
AddressLine2         STRING(40)                            !
AddressLine3         STRING(40)                            !
AddressLine4         STRING(40)                            !
Telephone            STRING(30)                            !
Fax                  STRING(30)                            !
EmailAddress         STRING(255)                           !
VatNumber            STRING(30)                            !
                     END                                   !
tmp:LabourCost       REAL                                  !
tmp:PartsCost        REAL                                  !
tmp:CourierCost      REAL                                  !
tmp:InvoiceNumber    STRING(30)                            !
InvoiceAddressGroup  GROUP,PRE(ia)                         !
CustomerName         STRING(60)                            !
CompanyName          STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
AddressLine4         STRING(30)                            !
TelephoneNumber      STRING(30)                            !
FaxNumber            STRING(30)                            !
EmailAddress         STRING(255)                           !
VatNumber            STRING(30)                            !
                     END                                   !
DeliveryAddressGroup GROUP,PRE(da)                         !
CustomerName         STRING(60)                            !
CompanyName          STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
AddressLine4         STRING(30)                            !
TelephoneNumber      STRING(30)                            !
                     END                                   !
locFranchiseAccountNumber STRING(20)                       !
locDateIn            DATE                                  !
locDateOut           DATE                                  !
locTimeIn            TIME                                  !
locTimeOut           TIME                                  !
locMainOutFault      STRING(1000)                          !
locAuditNotes        STRING(255)                           !
Process:View         VIEW(INVOICE)
                       PROJECT(inv:Invoice_Number)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT,AT(385,6646,7521,2260),PRE(RPT),PAPER(PAPER:A4),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,2240,7521,6687),USE(?unnamed)
                         STRING(@s20),AT(6354,1615),USE(tmp:Ref_Number),FONT(,8),LEFT,TRN
                         STRING(@s10),AT(3906,1615),USE(wob:HeadAccountNumber),FONT(,8),LEFT,TRN
                         STRING(@s15),AT(2917,1615),USE(tmp:InvoiceNumber),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(5000,1615),USE(job:Order_Number),FONT(,8),LEFT,TRN
                         STRING(@s60),AT(156,52),USE(ia:CustomerName),FONT(,8),TRN
                         STRING(@s60),AT(4115,52),USE(da:CustomerName),FONT(,8),TRN
                         STRING(@d6b),AT(156,1615),USE(locInvoiceDate),FONT(,8),RIGHT,TRN
                         STRING(@s25),AT(1458,2135,1000,156),USE(job:Model_Number),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(156,2135,1323,156),USE(job:Manufacturer),FONT(,8),LEFT,TRN
                         STRING(@s16),AT(5000,2135,990,156),USE(job:ESN),FONT(,8),LEFT,TRN
                         STRING(@s16),AT(2917,2135),USE(job:MSN),FONT(,8),TRN
                         STRING('REPORTED FAULT'),AT(104,2552,1187),USE(?String64),FONT(,9,,FONT:bold),TRN
                         TEXT,AT(1625,2542,5313,656),USE(jbn:Fault_Description),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('ENGINEERS REPORT'),AT(83,3208,1354),USE(?String88),FONT(,9,,FONT:bold),TRN
                         STRING('PARTS USED'),AT(104,4219),USE(?String79),FONT(,9,,FONT:bold),TRN
                         STRING('Qty'),AT(1521,4219),USE(?String80),FONT(,8,,FONT:bold),TRN
                         STRING('Part Number'),AT(1917,4219),USE(?String81),FONT(,8,,FONT:bold),TRN
                         STRING('Description'),AT(3677,4219),USE(?String82),FONT(,8,,FONT:bold),TRN
                         STRING('Unit Cost'),AT(5719,4219),USE(?String83),FONT(,8,,FONT:bold),TRN
                         STRING('Line Cost'),AT(6677,4219),USE(?String89),FONT(,8,,FONT:bold),TRN
                         LINE,AT(1354,4375,6042,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING(@s30),AT(156,833),USE(ia:AddressLine4),FONT(,8),TRN
                         STRING('Tel: '),AT(4115,990),USE(?String32:2),FONT(,8),TRN
                         STRING(@s30),AT(4323,990),USE(da:TelephoneNumber),FONT(,8)
                         STRING('Vat No:'),AT(156,990),USE(?String34:3),FONT(,8),TRN
                         STRING(@s30),AT(625,990,1406,208),USE(ia:VatNumber),FONT(,8),LEFT,TRN
                         STRING('Tel:'),AT(2083,677),USE(?String34:2),FONT(,8),TRN
                         STRING(@s30),AT(2344,677,1406,208),USE(ia:TelephoneNumber),FONT(,8),LEFT,TRN
                         STRING('Fax: '),AT(2083,833),USE(?String35:2),FONT(,8),TRN
                         STRING(@s30),AT(2344,833,1094,208),USE(ia:FaxNumber),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(4115,208),USE(da:CompanyName),FONT(,8),TRN
                         STRING(@s30),AT(4115,365,1917,156),USE(da:AddressLine1),FONT(,8),TRN
                         STRING(@s30),AT(4115,521),USE(da:AddressLine2),FONT(,8),TRN
                         STRING(@s30),AT(4115,677),USE(da:AddressLine3),FONT(,8),TRN
                         STRING(@s30),AT(4115,833),USE(da:AddressLine4),FONT(,8),TRN
                         STRING(@s30),AT(156,208),USE(ia:CompanyName),FONT(,8),TRN
                         STRING(@s30),AT(156,365),USE(ia:AddressLine1),FONT(,8),TRN
                         STRING(@s30),AT(156,677,1885),USE(ia:AddressLine3),FONT(,8),TRN
                         STRING(@s30),AT(156,521),USE(ia:AddressLine2),FONT(,8),TRN
                         TEXT,AT(1625,3208,5312,875),USE(locMainOutFault),FONT('Arial',7,,FONT:bold,CHARSET:ANSI),TRN
                       END
DETAIL                 DETAIL,AT(0,0,7521,167),USE(?DetailBand)
                         STRING(@n8b),AT(1156,0),USE(par:Quantity),FONT(,8),RIGHT,TRN
                         STRING(@s25),AT(1917,0),USE(part_number_temp),FONT(,8),TRN
                         STRING(@s30),AT(3708,0),USE(par:Description),FONT(,8),TRN
                         STRING(@n14.2b),AT(5396,0),USE(par:Sale_Cost),FONT(,8),RIGHT,TRN
                         STRING(@n14.2b),AT(6396,0),USE(line_cost_temp),FONT(,8),RIGHT,TRN
                       END
ANewPage               DETAIL,AT(0,0,7521,83),USE(?detailANewPage),PAGEAFTER(1)
                       END
                       FOOTER,AT(385,9104,7521,2406),USE(?unnamed:4)
                         STRING('Labour:'),AT(4844,104),USE(?String90),FONT(,8),TRN
                         STRING(@n-14.2),AT(6417,104),USE(tmp:LabourCost),FONT(,8),RIGHT,TRN
                         STRING('Time In:'),AT(1615,156),USE(?String90:3),FONT(,8),TRN
                         STRING(@d06b),AT(781,573),USE(locDateOut),FONT(,8),TRN
                         STRING('Other:'),AT(4844,417),USE(?String93),FONT(,8),TRN
                         STRING(@n-14.2),AT(6417,260),USE(tmp:PartsCost),FONT(,8),RIGHT,TRN
                         STRING(@t7b),AT(2240,573),USE(locTimeOut),FONT(,8),TRN
                         STRING(@t7b),AT(2240,156),USE(locTimeIn),FONT(,8),TRN
                         STRING('Date Out:'),AT(156,573),USE(?String90:4),FONT(,8),TRN
                         STRING('V.A.T.'),AT(4844,729),USE(?String94),FONT(,8),TRN
                         STRING(@n-14.2),AT(6417,417),USE(tmp:CourierCost),FONT(,8),RIGHT,TRN
                         STRING('Technician:'),AT(156,990),USE(?String90:6),FONT(,8),TRN
                         STRING(@s30),AT(833,990),USE(job:Engineer),FONT(,8),TRN
                         STRING('Time Out:'),AT(1615,573),USE(?String90:5),FONT(,8),TRN
                         STRING('Date In:'),AT(156,156),USE(?String90:2),FONT(,8),TRN
                         STRING('Total:'),AT(4844,990),USE(?String95),FONT(,8,,FONT:bold),TRN
                         LINE,AT(6302,937,1000,0),USE(?Line2),COLOR(COLOR:Black)
                         STRING(@n-14.2),AT(6417,990),USE(total_temp),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING('Received By: '),AT(156,1510),USE(?String90:7),FONT(,10),TRN
                         STRING('Signature: '),AT(3021,1510),USE(?String90:8),FONT(,10),TRN
                         STRING('Date:'),AT(5885,1510),USE(?String90:9),FONT(,10),TRN
                         LINE,AT(990,1719,2031,0),USE(?Line9),COLOR(COLOR:Black)
                         LINE,AT(3646,1719,2240,0),USE(?Line9:2),COLOR(COLOR:Black)
                         LINE,AT(6198,1719,1250,0),USE(?Line9:3),COLOR(COLOR:Black)
                         STRING(@d06b),AT(781,156),USE(locDateIn),FONT(,8),TRN
                         LINE,AT(6302,573,1000,0),USE(?Line2:2),COLOR(COLOR:Black)
                         STRING(@n14.2),AT(6396,573),USE(sub_total_temp),FONT(,8),RIGHT,TRN
                         STRING('Sub Total:'),AT(4844,573),USE(?String106),FONT(,8),TRN
                         STRING(@n14.2),AT(6396,729),USE(vat_temp),FONT(,8),RIGHT,TRN
                         STRING('<128>'),AT(6344,990),USE(?Euro),FONT(,8,,FONT:bold),HIDE,TRN
                         STRING('Parts:'),AT(4844,260),USE(?String92),FONT(,8),TRN
                       END
                       FORM,AT(385,406,7521,11198),USE(?unnamed:3),COLOR(COLOR:White)
                         BOX,AT(52,3125,7396,260),USE(?Box2),COLOR(COLOR:Black),FILL(00ECECECh),LINEWIDTH(1),ROUND
                         BOX,AT(52,3646,7396,260),USE(?Box2:3),COLOR(COLOR:Black),FILL(00ECECECh),LINEWIDTH(1),ROUND
                         STRING('TAX INVOICE'),AT(4167,0,3281),USE(?Text:Invoice),FONT(,18,,FONT:bold),RIGHT,TRN
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),FONT(,14,,FONT:bold),LEFT
                         STRING(@s40),AT(104,417,3073,208),USE(address:Name2),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,938,2240,156),USE(address:AddressLine1),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1042,2240,156),USE(address:AddressLine2),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1146,2240,156),USE(address:AddressLine3),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1250),USE(address:AddressLine4),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('TEL:'),AT(104,1406),USE(?String16),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(521,1406,1458,208),USE(address:Telephone),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,260,3073,208),USE(address:Name),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,573,2760,208),USE(address:Location),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(625,729,1667,208),USE(address:RegistrationNo),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('FAX:'),AT(2083,1406,313,208),USE(?String19),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(2396,1406,1510,188),USE(address:Fax),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('EMAIL:'),AT(104,1510,417,208),USE(?String19:2),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s255),AT(521,1510,3333,208),USE(address:EmailAddress),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s20),AT(625,833,1771,156),USE(address:VatNumber),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('INVOICE ADDRESS'),AT(104,1667,1229),USE(?String119),FONT(,8,,FONT:bold),TRN
                         STRING('Make'),AT(156,3698),USE(?String40),FONT(,8,,FONT:bold),TRN
                         STRING('Date'),AT(156,3177),USE(?String40:2),FONT(,8,,FONT:bold),TRN
                         LINE,AT(1354,3125,0,1042),USE(?Line5),COLOR(COLOR:Black)
                         STRING('Our Ref'),AT(1458,3177),USE(?String40:3),FONT(,8,,FONT:bold),TRN
                         STRING('Invoice No'),AT(2917,3177),USE(?String40:4),FONT(,8,,FONT:bold),TRN
                         STRING('Your Order No'),AT(5000,3177),USE(?String40:5),FONT(,8,,FONT:bold),TRN
                         STRING('Job No'),AT(6354,3177),USE(?String40:6),FONT(,8,,FONT:bold),TRN
                         STRING('Account No'),AT(3906,3177),USE(?String40:7),FONT(,8,,FONT:bold),TRN
                         BOX,AT(52,3385,7396,260),USE(?Box2:2),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         LINE,AT(6250,3125,0,521),USE(?Line5:4),COLOR(COLOR:Black)
                         LINE,AT(2812,3125,0,1042),USE(?Line5:2),COLOR(COLOR:Black)
                         LINE,AT(3802,3125,0,521),USE(?Line5:8),COLOR(COLOR:Black)
                         BOX,AT(52,3906,7396,260),USE(?Box2:4),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         STRING('Model'),AT(1458,3698),USE(?String41),FONT(,8,,FONT:bold),TRN
                         STRING('Serial Number'),AT(2917,3698),USE(?String42),FONT(,8,,FONT:bold),TRN
                         STRING('I.M.E.I. Number'),AT(5000,3698),USE(?String43),FONT(,8,,FONT:bold),TRN
                         BOX,AT(52,8698,2760,1302),USE(?Box4),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         BOX,AT(4688,8698,2760,1302),USE(?Box5),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         BOX,AT(52,4219,7396,4323),USE(?Box3),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         LINE,AT(4896,3125,0,1042),USE(?Line5:3),COLOR(COLOR:Black)
                         BOX,AT(52,1823,3438,1250),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         BOX,AT(4010,1823,3438,1250),USE(?Box6),COLOR(COLOR:Black),LINEWIDTH(1),ROUND
                         STRING('DELIVERY ADDRESS'),AT(4063,1667,1396),USE(?String119:2),FONT(,8,,FONT:bold),TRN
                         TEXT,AT(4542,292,2917,1333),USE(de2:GlobalPrintText),FONT(,8),RIGHT,TRN
                         STRING('REG NO:'),AT(104,729,542),USE(?String16:2),FONT(,7,,,CHARSET:ANSI),COLOR(COLOR:White), |
  TRN
                         STRING('VAT NO:'),AT(104,833,542,156),USE(?String16:3),FONT(,7,,,CHARSET:ANSI),COLOR(COLOR:White), |
  TRN
                       END
                     END
    MAP
trace	PROCEDURE(STRING pText)	
    END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR2               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR2:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR2:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

CreateARCRRCInvoices        Routine
    trace('CreateInvoices: inv:Invoice_Number = ' & inv:Invoice_Number)
    If (p_web.GSV('BookingSite') = 'RRC')
        !Line500_XML(p_web,'RIV') ! Temp test
        
        IF (inv:RRCInvoiceDate = 0)
            ! RRC Invoice has not been created
            inv:RRCInvoiceDate = TODAY()
            IF (inv:ExportedRRCOracle = 0)
                p_web.SSV('jobe:InvRRCCLabourCost',p_web.GSV('jobe:RRCCLabourCost'))
                p_web.SSV('jobe:InvRRCCPartsCost',p_web.GSV('jobe:RRCCPartsCost'))
                p_web.SSV('jobe:InvRRCCSubTotal',p_web.GSV('jobe:RRCCSubTotal'))
                p_web.SSV('jobe:InvoiceHandlingFee',p_web.GSV('jobe:HandlingFee'))
                p_web.SSV('jobe:InvoiceExchangeRate',p_web.GSV('jobe:ExchangeRate'))
                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber = p_web.GSV('jobe:RefNumber')
                IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                    p_web.SessionQueueToFile(JOBSE)
                    Access:JOBSE.TryUpdate()
                    
                END
                
                inv:ExportedRRCOracle = 1
                Access:INVOICE.TryUpdate()

                Line500_XML(p_web,'RIV')
            END

            foundInvoiceCreated# = 0
            Access:AUDIT.Clearkey(aud:Action_Key)
            aud:Ref_Number = p_web.GSV('job:Ref_Number')
            aud:Action = 'INVOICE CREATED'
            SET(aud:Action_Key,aud:Action_Key)
            LOOP UNTIL Access:AUDIT.Next()
                IF (aud:Ref_Number <> p_web.GSV('job:Ref_Number') OR |
                    aud:Action <> 'INVOICE CREATED')
                    BREAK
                END
                IF (INSTRING('INVOICE NUMBER: ' & CLIP(inv:Invoice_Number) & '-' & |                   
                    CLIP(tra:BranchIdentification),1,1))
                    foundInvoiceCreated# = 1
                    BREAK
                END
            END

            IF (foundInvoiceCreated# = 0)
                locAuditNotes = 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
                LoanAttachedToJob(p_web)
                IF (p_web.GSV('LoanAttachedToJob') = 1)
                    locAuditNotes   = Clip(locAuditNotes) & '<13,10>LOST LOAN FEE: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
                END
                IF (p_web.GSV('jobe:Engineer48HourOption') = 1)
                    IF (p_web.GSV('jobe:ExcReplcamentCharge') = 1)
                        locAuditNotes   = Clip(locAuditNotes) & '<13,10>EXC REPL: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
                    END
                END
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>PARTS: ' & Format(p_web.GSV('jobe:InvRRCCPartsCost'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>LABOUR: ' & Format(p_web.GSV('jobe:InvRRCCLabourCost'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>SUB TOTAL: ' & Format(p_web.GSV('jobe:InvRRCCSubTotal'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>VAT: ' & Format((p_web.GSV('jobe:InvRRCCPartsCOst') * inv:RRCVatRateParts/100) + |
                    (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
                    (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>TOTAL: ' & Format(p_web.GSV('jobe:InvRRCCSubTotal') + (p_web.GSV('jobe:InvRRCCPartsCost') * inv:RRCVatRateParts/100) + |
                    (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
                    (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100),@n14.2)

!                p_web.SSV('AddToAudit:Type','JOB')
!                p_web.SSV('AddToAudit:Action','INVOICE CREATED')
!                p_web.SSV('AddToAudit:Notes',CLIP(locAuditNotes))
                AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','INVOICE CREATED',CLIP(locAuditNotes))
            END
!            p_web.SSV('AddToAudit:Type','JOB')
!            p_web.SSV('AddToAudit:Action','INVOICE PRINTED')
!            p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
!                '<13,10>INVOICE DATE: ' & FOrmat(inv:RRCInvoiceDate,@d06))
            AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','INVOICE PRINTED','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
                '<13,10>INVOICE DATE: ' & FOrmat(inv:RRCInvoiceDate,@d06))
        ELSE


        END
!        p_web.SSV('AddToAudit:Type','JOB')
!        p_web.SSV('AddToAudit:Action','INVOICE PRINTED')
!        p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
!            '<13,10>INVOICE DATE: ' & FOrmat(inv:RRCInvoiceDate,@d06))
        AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','INVOICE PRINTED','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
            '<13,10>INVOICE DATE: ' & FOrmat(inv:RRCInvoiceDate,@d06))
        locInvoiceDate = inv:RRCInvoiceDate
    ELSE ! If (glo:WebJob = 1)
        IF (inv:ARCInvoiceDate = 0)
            inv:ARCInvoiceDate = TODAY()
            p_web.SSV('job:Invoice_Labour_Cost',p_web.GSV('job:Labour_Cost'))
            p_web.SSV('job:Invoice_Courier_Cost',p_web.GSV('job:Courier_Cost'))
            p_web.SSV('job:Invoice_Parts_Cost',p_web.GSV('job:Parts_Cost'))
            p_web.SSV('job:Invoice_Sub_Total',p_web.GSV('job:Invoice_Labour_Cost') + |
                p_web.GSV('job:Invoice_Parts_Cost') + |
                p_web.GSV('job:Invoice_Courier_Cost'))                 
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = p_web.GSV('job:Ref_Number')
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                p_web.SessionQueueToFile(JOBS)
                Access:JOBS.TryUpdate()
            END
            
            

            inv:ExportedARCOracle = 1
            Access:INVOICe.TryUpdate()
            

            Line500_XML(p_web,'AOW')

            foundInvoiceCreated# = 0
            Access:AUDIT.Clearkey(aud:Action_Key)
            aud:Ref_Number = p_web.GSV('job:Ref_Number')
            aud:Action = 'INVOICE CREATED'
            SET(aud:Action_Key,aud:Action_Key)
            LOOP UNTIL Access:AUDIT.Next()
                IF (aud:Ref_Number <> p_web.GSV('job:Ref_Number') OR |
                    aud:Action <> 'INVOICE CREATED')
                    BREAK
                END
                IF (INSTRING('INVOICE NUMBER: ' & CLIP(inv:Invoice_Number) & '-' & |
                    CLIP(tra:BranchIdentification),1,1))
                    foundInvoiceCreated# = 1
                    BREAK
                END
            END

            IF (foundInvoiceCreated# = 0)
                locAuditNotes   = 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
                LoanAttachedToJob(p_web)
                If (p_web.GSV('LoanAttachedToJob') = 1)
                    ! Changing (DBH 27/06/2006) #7584 - If loan attached, then invoice number is not being written to the audit trail
                    ! locAuditNotes   = '<13,10>LOST LOAN FEE: ' & Format(p_web.GSV('job:Invoice_Courier_Cost,@n14.2)
                    ! to (DBH 27/06/2006) #7584
                    locAuditNotes   = Clip(locAuditNotes) & '<13,10>LOST LOAN FEE: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
                    ! End (DBH 27/06/2006) #7584

                End !If LoanAttachedToJob(p_web.GSV('job:Ref_Number)
                If p_web.GSV('jobe:Engineer48HourOption') = 1
                    If p_web.GSV('jobe:ExcReplcamentCharge') = 1
                        locAuditNotes   = Clip(locAuditNotes) & '<13,10>EXC REPL: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
                    End !If p_web.GSV('jobe:ExcReplcamentCharge
                End !If p_web.GSV('jobe:Engineer48HourOption = 1
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>PARTS: ' & Format(p_web.GSV('job:Invoice_Parts_Cost'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>LABOUR: ' & Format(p_web.GSV('job:Invoice_Labour_Cost'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>SUB TOTAL: ' & Format(p_web.GSV('job:Invoice_Sub_Total'),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>VAT: ' & Format((p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100) + |
                    (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
                    (p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100),@n14.2)
                locAuditNotes   = Clip(locAuditNotes) & '<13,10>TOTAL: ' & Format(p_web.GSV('job:Invoice_Sub_Total') + (p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100) + |
                    (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
                    (p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100),@n14.2)
!                p_web.SSV('AddToAudit:Type','JOB')
!                p_web.SSV('AddToAudit:Action','INVOICE CREATED')
!                p_web.SSV('AddToAudit:Notes',CLIP(locAuditNotes))
                AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','INVOICE CREATED',CLIP(locAuditNotes))
            END
!            p_web.SSV('AddToAudit:Type','JOB')
!            p_web.SSV('AddToAudit:Action','INVOICE PRINTED')
!            p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
!                '<13,10>INVOICE DATE: ' & FOrmat(inv:ARCInvoiceDate,@d06))
            AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','INVOICE PRINTED','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
                '<13,10>INVOICE DATE: ' & FOrmat(inv:ARCInvoiceDate,@d06))

        ELSE ! IF (inv:ARCInvoiceDate = 0)

        END ! IF (inv:ARCInvoiceDate = 0)
!        p_web.SSV('AddToAudit:Type','JOB')
!        p_web.SSV('AddToAudit:Action','INVOICE PRINTED')
!        p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
!            '<13,10>INVOICE DATE: ' & FOrmat(inv:ARCInvoiceDate,@d06))
        AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB', 'INVOICE PRINTED', 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
            '<13,10>INVOICE DATE: ' & FOrmat(inv:ARCInvoiceDate,@d06))     
        locInvoiceDate = inv:ARCInvoiceDate
    END !If (glo:WebJob = 1)
PrintParts          ROUTINE
DATA
PartsAttached       BYTE(0)
CODE
    
    Access:PARTS.ClearKey(par:Part_Number_Key)
    par:Ref_Number = p_web.GSV('job:Ref_Number')
    SET(par:Part_Number_Key,par:Part_Number_Key)
    LOOP UNTIL Access:PARTS.Next()
        IF (par:Ref_Number <> p_web.GSV('job:Ref_Number'))
            BREAK
        END
        
        part_number_temp = par:Part_Number
        line_cost_temp = par:Quantity * par:Sale_Cost
        Print(rpt:Detail)
        PartsAttached = 1
    END
    IF (PartsAttached = 0)
        part_number_temp = 'No Parts Attached'
        par:Quantity = 0
        par:Description = ''
        par:Sale_Cost = 0
        Print(rpt:Detail)
    END
!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('VSingleInvoice')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
        locInvoiceNumber = p_web.GSV('job:Invoice_Number')
        IF (p_web.IfExistsValue('CreditSuffix'))
            p_web.StoreValue('CreditSuffix')
        ELSE
            p_web.DeleteSessionValue('CreditSuffix')
        END ! IF  
        IF (p_web.IfExistsValue('CreditNote'))
            p_web.StoreValue('CreditNote')
        ELSE
            p_web.DeleteSessionValue('CreditNote')
        END ! IF        
  Relate:AUDIT.SetOpenRelated()
  Relate:AUDIT.Open                                        ! File AUDIT used by this procedure, so make sure it's RelationManager is open
  Relate:DEFAULT2.Open                                     ! File DEFAULT2 used by this procedure, so make sure it's RelationManager is open
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:DEFPRINT.Open                                     ! File DEFPRINT used by this procedure, so make sure it's RelationManager is open
  Relate:JOBPAYMT.Open                                     ! File JOBPAYMT used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:AUDIT2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBNOTES.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PARTS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAULT.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAULO.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOCATLOG.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSINV.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBS.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBOUTFL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBACC.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:AUDSTATS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('VSingleInvoice',ProgressWindow)            ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:INVOICE, ?Progress:PctText, Progress:Thermometer, ProgressMgr, inv:Invoice_Number)
  ThisReport.AddSortOrder(inv:Invoice_Number_Key)
  ThisReport.AddRange(inv:Invoice_Number,locInvoiceNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:INVOICE.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  ! after lookup
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBPAYMT.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('VSingleInvoice',ProgressWindow)         ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'VSingleInvoice',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR2.Init(Report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,Report{PROPPRINT:Paper},PAPER:USER), CHOOSE(Report{PROP:Thous}=True,PROP:Thous,CHOOSE(Report{PROP:MM}=True,PROP:MM,CHOOSE(Report{PROP:Points}=True,PROP:Points,0))), Report{PROPPRINT:PaperWidth}, Report{PROPPRINT:PaperHeight}, Report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetTitle('Invoice')                  !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR2.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR2:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR2:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR2:vpf = BOR(PDFXTR2:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR2:vpf = BOR(PDFXTR2:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR2:vpf = BOR(PDFXTR2:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR2:vpf = BOR(PDFXTR2:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR2:vpf = BOR(PDFXTR2:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR2:vpf = BOR(PDFXTR2:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetViewerPrefs(PDFXTR2:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'VSingleInvoice',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR2.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  ! Get Records
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = p_web.GSV('job:Ref_Number')
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
  END
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = p_web.GSV('wob:RefNumber')
  IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
  END
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = p_web.GSV('jobe:RefNumber')
  IF (Access:JOBSE.Tryfetch(jobe:RefNumberKey))
  END
  
  Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
  jbn:RefNumber = p_web.GSV('job:Ref_Number')
  IF (Access:JOBNOTES.TryFetch(jbn:RefNumberKey))
  END
  
  SET(DEFAULT2,0) ! #12079 Pick up new "Global Text" (Bryan: 13/04/2011)
  Access:DEFAULT2.Next()
  ! Lookups
  ! OutFault
  locMainOutFault = ''
  ! #12091 Display outfaults list like the old A5 invoice. (Bryan: 09/05/2011)
  line# = 0
  Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
  joo:JobNumber = job:Ref_Number
  Set(joo:JobNumberKey,joo:JobNumberKey)
  Loop ! Begin Loop
      If Access:JOBOUTFL.Next()
          Break
      End ! If Access:JOBOUTFL.Next()
      If joo:JobNumber <> job:Ref_Number
          Break
      End ! If joo:JobNumber <> job:Ref_Number
      If Instring('IMEI VALIDATION',joo:Description,1,1)
          Cycle
      End ! If Instring('IMEI VALIDATION',joo:Description,1,1)
      line# += 1
      locMainOutFault = Clip(locMainoutfault) & '<13,10>' & Format(line#,@n2) & ' - ' & Clip(joo:Description)
  End ! Loop
  locMainOutFault = CLIP(Sub(locMainOutFault,3,1000))
  
  ! Set Default Address
  ! Set Job Number / Invoice Number
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
  IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
      tmp:Ref_Number = p_web.GSV('job:Ref_Number') & '-' & Clip(tra:BranchIdentification) & p_web.GSV('wob:JobNumber')
      tmp:InvoiceNumber = CLIP(p_web.GSV('job:Invoice_Number')) & '-' & Clip(tra:BranchIdentification)
  END
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  If (p_web.GSV('BookingSite') = 'RRC')
      tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
  ELSE ! If (glo:WebJob = 1)
      tra:Account_Number = GETINI('BOOKING','HeadAccount','AA20',CLIP(PATH())&'\SB2KDEF.INI')
  END ! If (glo:WebJob = 1)
  If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
      address:SiteName        = tra:Company_Name
      address:Name            = tra:coTradingName
      address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
      address:Location        = tra:coLocation
      address:RegistrationNo  = tra:coRegistrationNo
      address:AddressLine1    = tra:coAddressLine1
      address:AddressLine2    = tra:coAddressLine2
      address:AddressLine3    = tra:coAddressLine3
      address:AddressLine4    = tra:coAddressLine4
      address:Telephone       = tra:coTelephoneNumber
      address:Fax             = tra:coFaxNumber
      address:EmailAddress    = tra:coEmailAddress
      address:VatNumber       = tra:coVATNumber
  
      foundPrinted# = 0
      Access:AUDIT.Clearkey(aud:Action_Key)
      aud:Ref_Number = p_web.GSV('job:Ref_Number')
      aud:Action = 'INVOICE PRINTED'
      SET(aud:Action_Key,aud:Action_Key)
      LOOP UNTIL Access:AUDIT.Next()
            IF (aud:Ref_Number <> p_web.GSV('job:Ref_Number') OR |
                aud:Action <> 'INVOICE PRINTED')
                BREAK
            END
            Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
            aud2:AUDRecordNumber = aud:record_number
            IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                IF (INSTRING('INVOICE NUMBER: ' & CLIP(inv:Invoice_Number) & '-' & | 
                    CLIP(tra:BranchIdentification),aud2:Notes,1,1))
                    foundPrinted# = 1
                    BREAK
                END
            END ! IF
            
      END
  
      If (foundPrinted# = 1)
          REPORT $ ?Text:Invoice{prop:Text} = 'COPY TAX INVOICE'
      END
          
      Do CreateARCRRCInvoices
  END
  
  ! Set Invoice  / Delivery Address
  Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
  sub:Account_Number = p_web.GSV('job:Account_Number')
  IF (Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign)
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number = sub:Main_Account_Number
      IF (Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign)
      END
  END
  CLEAR(InvoiceAddressGroup)
  
  IF (sub:OverrideHeadVATNo = 1)
      ia:VatNumber = sub:Vat_Number
  ELSE
      ia:VatNumber = tra:Vat_Number
  END
  
  IF (tra:Invoice_Sub_Accounts = 'YES')
      IF (sub:Invoice_Customer_Address <> 'YES')
          ia:CompanyName = sub:Company_Name
          ia:AddressLine1 = sub:Address_Line1
          ia:AddressLine2 = sub:Address_Line2
          ia:AddressLine3 = sub:Address_Line3
          ia:AddressLine4 = sub:Postcode
          ia:TelephoneNumber = sub:Telephone_Number
          ia:FaxNumber = sub:Fax_Number
          ia:CustomerName = sub:Contact_Name
      END
      IF (ia:VatNumber = '')
          ia:VatNumber = p_web.GSV('jobe:VatNumber')
      END
  ELSE
      IF (tra:Invoice_Customer_Address <> 'YES')
          ia:CompanyName = tra:Company_Name
          ia:AddressLine1 = tra:Address_Line1
          ia:AddressLine2 = tra:Address_Line2
          ia:AddressLine3 = tra:Address_Line3
          ia:AddressLine4 = tra:Postcode
          ia:TelephoneNumber = tra:Telephone_Number
          ia:FaxNumber = tra:Fax_Number
          ia:CustomerName = tra:Contact_Name
      END
  
  END
  IF (ia:CompanyName) = ''
      IF (p_web.GSV('job:Surname') <> '')
          IF (p_web.GSV('job:Title') = '')
              ia:CustomerName = p_web.GSV('job:Surname')
          ELSE
              IF (p_web.GSV('job:Initial') <> '')
                  ia:CustomerName = CLIP(p_web.GSV('job:Title')) & ' ' & CLIP(p_web.GSV('job:Initial')) & ' ' & CLIP(p_web.GSV('job:Surname'))
              ELSE
                  ia:CustomerName = CLIP(p_web.GSV('job:Title')) & ' ' & CLIP(p_web.GSV('job:Surname'))
              END
          END
      END
  
      ia:CompanyName = p_web.GSV('job:Company_Name')
      ia:AddressLine1 = p_web.GSV('job:Address_Line1')
      ia:AddressLine2 = p_web.GSV('job:Address_Line2')
      ia:AddressLine3 = p_web.GSV('job:Address_Line3')
      ia:AddressLine4 = p_web.GSV('job:Postcode')
      ia:TelephoneNumber = p_web.GSV('job:Telephone_Number')
      ia:FaxNumber = p_web.GSV('job:Fax_Number')
  END
  
  da:CompanyName = p_web.GSV('job:Company_Name_Delivery')
  da:AddressLine1 = p_web.GSV('job:Address_Line1_Delivery')
  da:AddressLine2 = p_web.GSV('job:Address_Line2_Delivery')
  da:AddressLine3 = p_web.GSV('job:Address_Line3_Delivery')
  da:AddressLine4 = p_web.GSV('job:Postcode_Delivery')
  da:TelephoneNumber = p_web.GSV('job:Telephone_Delivery')
  
  IF (p_web.GSV('BookingSite') <> 'RRC')
      IF (p_web.GSV('jobe:WebJob') = 1)
          ! RRC to ARC job.
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
          IF (Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign)
              da:CompanyName  = tra:Company_Name
              da:AddressLine1 = tra:Address_Line1
              da:AddressLine2 = tra:Address_Line2
              da:AddressLine3 = tra:Address_Line3
              da:AddressLine4 = tra:Postcode
              da:TelephoneNumber = tra:Telephone_Number
  
              ia:CompanyName  = tra:Company_Name
              ia:AddressLine1 = tra:Address_Line1
              ia:AddressLine2 = tra:Address_Line2
              ia:AddressLine3 = tra:Address_Line3
              ia:AddressLine4 = tra:Postcode
              ia:TelephoneNumber = tra:Telephone_Number
              ia:VatNumber = tra:Vat_Number
  
          END
      ELSE ! ! IF (p_web.GSV('jobe:WebJob = 1)
  
  
  
  
      END ! IF (p_web.GSV('jobe:WebJob = 1)
  
  END ! IF (glo:WebJob = 1)
  
  
  ! Find Date In/Out
  
      locDateIn = job:Date_Booked
      locTimeIn = job:Time_Booked
      ! #11881 Use booking date unless RRC job AT ARC. Then use the "date received at ARC".
      ! (Note: Vodacom didn't specifically mention this, but i've assumed
      ! for VCP jobs to use the "received at RRC" date, rather than the booking date (Bryan: 16/03/2011)
      If (p_web.GSV('BookingSite') = 'RRC')
          IF (p_web.GSV('job:Who_Booked') = 'WEB')
              Access:AUDIT.Clearkey(aud:TypeActionKey)
              aud:Ref_Number = p_web.GSV('job:Ref_Number')
              aud:Type = 'JOB'
              aud:Action = 'UNIT RECEIVED AT RRC FROM PUP'
              SET(aud:TypeActionKey,aud:TypeActionKey)
              LOOP UNTIL Access:AUDIT.Next()
                  IF (aud:Ref_Number <> p_web.GSV('job:Ref_Number') OR |
                      aud:Type <> 'JOB' OR |
                      aud:Action <> 'UNIT RECEIVED AT RRC FROM PUP')
                      BREAK
                  END
                  locDateIn = aud:Date
                  locTimeIn = aud:Time
                  BREAK
              END
  
          END ! IF (job:Who_Booked = 'WEB')
  
      ELSE ! If (glo:WebJob = 1)
          IF (p_web.GSV('jobe:WebJob') = 1)
              ! RRC Job
              foundDateIn# = 0
              Access:AUDSTATS.Clearkey(aus:DateChangedKey)
              aus:RefNumber = p_web.GSV('job:Ref_Number')
              aus:Type = 'JOB'
              SET(aus:DateChangedKey,aus:DateChangedKey)
              LOOP UNTIL Access:AUDSTATS.Next()
                  IF (aus:RefNumber <> p_web.GSV('job:Ref_Number') OR |
                      aus:TYPE <> 'JOB')
                      BREAK
                  END
                  IF (SUB(UPPER(aus:NewStatus),1,3) = '452')
                      locDateIn = aus:DateChanged
                      locTimeIn = aus:TimeChanged
                      foundDateIn# = 1
                      BREAK
                  END
              END
              IF (foundDateIn# = 0)
                  Access:LOCATLOG.Clearkey(lot:DateKey)
                  lot:RefNumber = p_web.GSV('job:Ref_Number')
                  SET(lot:DateKey,lot:DateKey)
                  LOOP UNTIL Access:LOCATLOG.Next()
                      IF (lot:RefNumber <> p_web.GSV('job:Ref_Number'))
                          BREAK
                      END
                      IF (lot:NewLocation = Clip(GETINI('RRC','ARCLocation','RECEIVED AT ARC',CLIP(PATH())&'\SB2KDEF.INI')))
                          locDateIn = lot:TheDate
                          locTimeIn = lot:TheTime
                          BREAK
                      END
                  END
              END
          END
      END ! If (glo:WebJob = 1)
  
  
      ! Use despatch to customer date (use last occurance), unless ARC to RRC job
      locDateOut = 0
      locTimeOut = 0
      Access:LOCATLOG.Clearkey(lot:DateKey)
      lot:RefNumber = p_web.GSV('job:Ref_Number')
      SET(lot:DateKey,lot:DateKey)
      LOOP UNTIL Access:LOCATLOG.Next()
          IF (lot:RefNumber <> p_web.GSV('job:Ref_Number'))
              BREAK
          END
          IF (lot:NewLocation = Clip(GETINI('RRC','DespatchToCustomer','DESPATCHED',CLIP(PATH())&'\SB2KDEF.INI')))
              locDateOut = lot:TheDate
              locTimeOut = lot:TheTime
  !            BREAK
          END
      END
  
      If (p_web.GSV('BookingSite') <> 'RRC')
          IF (p_web.GSV('jobe:WebJob') = 1)
              ! RRC Job
              Access:LOCATLOG.Clearkey(lot:DateKey)
              lot:RefNumber = p_web.GSV('job:Ref_Number')
              SET(lot:DateKey,lot:DateKey)
              LOOP UNTIL Access:LOCATLOG.Next()
                  IF (lot:RefNumber <> p_web.GSV('job:Ref_Number'))
                      BREAK
                  END
                  IF (lot:NewLocation = Clip(GETINI('RRC','InTransitRRC','IN-TRANSIT TO RRC',CLIP(PATH())&'\SB2KDEF.INI')))
                      locDateOut = lot:TheDate
                      locTimeOut = lot:TheTime
                      BREAK
                  END
              END
          END
      END ! If (glo:WebJob = 1)
        ! Printing Details
        !Costs
  
        SETTARGET(REPORT)
        If (p_Web.GSV('BookingSite') = 'RRC')
            tmp:LabourCost = p_web.GSV('jobe:InvRRCCLabourCost')
            tmp:PartsCost = p_web.GSV('jobe:InvRRCCPartsCost')
            tmp:CourierCost = p_web.GSV('job:Invoice_Courier_Cost')
  
            vat_temp = (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
                (p_web.GSV('jobe:InvRRCCPartsCost') * inv:RRCVatRateParts/100) + |
                (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100)
            total_temp = p_web.GSV('jobe:InvRRCCLabourCost') + |
                p_web.GSV('jobe:InvRRCCPartsCost') + |
                p_web.GSV('job:Invoice_Courier_Cost') + vat_temp
        ELSE
            tmp:LabourCost = p_web.GSV('job:Invoice_Labour_Cost')
            tmp:PartsCost = p_web.GSV('job:Invoice_Parts_Cost')
            tmp:CourierCost = p_web.GSV('job:Invoice_Courier_Cost')
            vat_temp = (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
                (p_web.GSV('jobe:InvRRCCPartsCost') * inv:RRCVatRateParts/100) + |
                (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100)
            total_temp = p_web.GSV('jobe:InvRRCCLabourCost') + |
                p_web.GSV('jobe:InvRRCCPartsCost') + |
                p_web.GSV('job:Invoice_Courier_Cost') + vat_temp
        END
        Sub_Total_Temp = tmp:LabourCost + tmp:PartsCost + tmp:CourierCost
        SETTARGET()
  
        ! Is this a credit note?
        IF (p_web.GSV('CreditNote') = 1)
            
            Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
            jov:RefNumber = p_web.GSV('job:ref_Number')
            jov:Type = 'C'
            jov:Suffix = p_web.GSV('CreditSuffix')
            IF (Access:JOBSINV.TryFetch(jov:TypeSuffixKey))
            END
            tmp:InvoiceNumber = 'CN' & Clip(tmp:InvoiceNumber) & p_web.GSV('CreditSuffix')
            Total_Temp = -jov:CreditAmount
            SetTarget(Report)
            ?Text:Invoice{prop:Text} = 'CREDIT NOTE'
            SetTarget()
            DO PrintParts
        
            IF (p_web.GSV('InvoiceSuffix') <> '')
            ! Stop duplicates being printed when just reprinting Credit Note
                Print(rpt:ANewPage)
                Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
                jov:RefNumber = p_web.GSV('job:ref_Number')
                jov:Type = 'I'
                jov:Suffix = p_web.GSV('InvoiceSuffix')
                IF (Access:JOBSINV.TryFetch(jov:TypeSuffixKey))
                END
                tmp:InvoiceNumber = Clip(tmp:InvoiceNumber) & p_web.GSV('InvoiceSuffix')
                Total_Temp = jov:NewTotalCost
                DO PrintParts
            END !I F
        ELSE
            DO PrintParts
        END
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR2:rtn = PDFXTR2.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR2:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

trace	PROCEDURE(STRING pText)
outString	CSTRING(1000)
    CODE
        RETURN ! Logging Disabled
