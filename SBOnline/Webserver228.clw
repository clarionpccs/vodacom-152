

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER228.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ClearSBOWarrantyClaims PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
keepAlive   LONG()
FilesOpened     BYTE(0)

  CODE
        DO OpenFiles
        STREAM(SBO_WarrantyClaims)
        Access:SBO_WarrantyClaims.Clearkey(sbojow:JobNumberKey)
        sbojow:SessionID = p_web.SessionID
        SET(sbojow:JobNumberKey,sbojow:JobNumberKey)
        LOOP UNTIL Access:SBO_WarrantyClaims.Next()
            IF (sbojow:SessionID <> p_web.SessionID)
                BREAK
            END

            Access:SBO_WarrantyClaims.DeleteRecord(0)
            
            keepAlive += 1
            IF (keepAlive > 50)
                p_web.Noop()
                keepAlive = 0
            END ! IF
            
        END
        FLUSH(SBO_WarrantyClaims)
        
        DO CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:SBO_WarrantyClaims.Open                           ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SBO_WarrantyClaims.UseFile                        ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:SBO_WarrantyClaims.Close
     FilesOpened = False
  END
