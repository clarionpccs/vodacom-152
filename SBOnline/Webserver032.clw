

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER032.INC'),ONCE        !Local module procedure declarations
                     END


MessageQuestion      PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:MessageQuestion -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
  CODE
  GlobalErrors.SetProcedureName('MessageQuestion')
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
!----------- put your html code here -----------------------------------
    packet = clip(packet) & |
        '<<html>' & |
        '<<script type="text/javascript"><13,10>'&|
        '<<!--<13,10>'&|
        'function confirmation() {{<13,10>'&|
        'var answer = confirm("' & p_web.GSV('Message:Text') & '")<13,10>'&|
        'if (answer){{<13,10>'&|
        'window.location.href = "' & p_web.GSV('Message:PassURL') & '";<13,10>'&|
        '}<13,10>'&|
        'else{{<13,10>'&|
        'window.location.href = "' & p_web.GSV('Message:FailURL') & '";<13,10>'&|
        '}<13,10>'&|
        '}<13,10>'&|
        'document.write(confirmation())<13,10>'&|
        '// --><13,10>'&|
        '<</script>'&|
        '<</html>'
!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
