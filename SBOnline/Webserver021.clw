

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER021.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
UpdateDateTimeStamp  PROCEDURE  (LONG pRefNumber)          ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
    ! Inserting (DBH 05/08/2008) # 10253 - Update date/time stamp everytime a job changes
    Do OpenFiles
    Access:JOBSTAMP.ClearKey(jos:JOBSRefNumberKey)
    jos:JOBSRefNumber = pRefNumber
    IF (Access:JOBSTAMP.TryFetch(jos:JOBSRefNumberKey) = Level:Benign)
        jos:DateStamp   = TODAY()
        jos:TimeStamp   = CLOCK()
        Access:JOBSTAMP.TryUpdate()
    ELSE ! IF
        IF (Access:JOBSTAMP.PrimeRecord() = Level:Benign)
            jos:JOBSRefNumber = pRefNumber
            jos:DateStamp  = TODAY()
            jos:TimeStamp = CLOCK()
            IF (Access:JOBSTAMP.TryInsert() = Level:Benign)
            ELSE !
                Access:JOBSTAMP.CancelAutoInc()
            END !
        END ! IF
    END ! IF

    Do CloseFiles
    ! End (DBH 05/08/2008) #10253
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBSTAMP.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSTAMP.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBSTAMP.Close
     FilesOpened = False
  END
