

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER009.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
RepWaybillDespatch PROCEDURE (NetWebServerWorker p_web)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
locWaybillNumber     LONG                                  !
DespatchType         STRING(3)                             !
save_jea_id          USHORT,AUTO                           !
save_jac_id          USHORT,AUTO                           !
save_xca_id          USHORT,AUTO                           !
save_waj_id          USHORT,AUTO                           !
tmp:PrintedBy        STRING(255)                           !
save_jpt_id          USHORT,AUTO                           !
print_group          QUEUE,PRE(prngrp)                     !
esn                  STRING(30)                            !
msn                  STRING(30)                            !
job_number           REAL                                  !
                     END                                   !
Unit_Ref_Number_Temp REAL                                  !
Despatch_Type_Temp   STRING(3)                             !
Invoice_Account_Number_Temp STRING(15)                     !
Delivery_Account_Number_Temp STRING(15)                    !
count_temp           REAL                                  !
model_number_temp    STRING(50)                            !
tmp:DefaultTelephone STRING(20)                            !
tmp:DefaultFax       STRING(20)                            !
tmp:DefaultEmailAddress STRING(255)                        !
tmp:Courier          STRING(30)                            !
tmp:ConsignmentNumber STRING(30)                           !
tmp:DespatchBatchNumber STRING(30)                         !
tmp:IMEI             STRING(30)                            !
tmp:Accessories      STRING(255)                           !
FromAddressGroup     GROUP,PRE(from)                       !
CompanyName          STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
TelephoneNumber      STRING(30)                            !
ContactName          STRING(60)                            !
EmailAddress         STRING(255)                           !
                     END                                   !
ToAddressGroup       GROUP,PRE(to)                         !
AccountNumber        STRING(30)                            !
CompanyName          STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
TelephoneNumber      STRING(30)                            !
ContactName          STRING(30)                            !
EmailAddress         STRING(255)                           !
                     END                                   !
code_temp            BYTE                                  !
option_temp          BYTE                                  !
Bar_code_string_temp CSTRING(31)                           !
Bar_Code_Temp        CSTRING(31)                           !
Webmaster_Group      GROUP,PRE(tmp)                        !
Ref_Number           STRING(20)                            !
BranchIdentification LIKE(tra:BranchIdentification)        !
                     END                                   !
tmp:Comments         STRING(255)                           !
tmp:SecurityPackNumber STRING(30)                          !
tmp:CustomerAddress  STRING(3)                             !
tmp:WayBillID        LONG                                  !
tmp:Exchanged        STRING(1)                             !
tmp:DateDespatched   DATE                                  !
tmp:TimeDespatched   TIME                                  !
tmp:HeadAccountNo    STRING(15)                            !
LOC:SaveToQueue      PrintPreviewFileQueue                 !
locNewProcedure      LONG                                  !
Process:View         VIEW(WAYBILLS)
                       PROJECT(way:SecurityPackNumber)
                       PROJECT(way:UserNotes)
                       PROJECT(way:WayBillNumber)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

report               REPORT,AT(448,4063,7771,5406),PRE(rpt),PAPER(PAPER:LETTER),FONT('Arial',10,,FONT:regular), |
  THOUS
                       HEADER,AT(375,469,7802,3219),USE(?unnamed)
                         STRING('Courier:'),AT(4896,781),USE(?String23),FONT(,8),TRN
                         STRING(@s30),AT(6042,781,3260,180),USE(tmp:Courier),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  LEFT,TRN
                         STRING('Despatch Date:'),AT(4896,365),USE(?DespatchDate),FONT(,8),TRN
                         STRING(@d6),AT(6042,365),USE(tmp:DateDespatched),FONT(,8,,FONT:bold),TRN
                         STRING(@t1b),AT(6042,573),USE(tmp:TimeDespatched),FONT(,8,,FONT:bold),TRN
                         STRING('From Sender:'),AT(104,104),USE(?String16),FONT(,8),TRN
                         STRING('WAYBILL REJECTION'),AT(5365,52),USE(?WaybillRejection),FONT(,16,,FONT:bold),HIDE,TRN
                         STRING('Deliver To:'),AT(104,1667),USE(?String16:5),FONT(,8),TRN
                         STRING('Tel No:'),AT(104,1042),USE(?String16:2),FONT(,8),TRN
                         STRING('Page Number:'),AT(4896,990),USE(?String62),FONT('Arial',8,,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING(@n3),AT(6042,990),USE(?ReportPageNo),FONT(,8,,FONT:bold),PAGENO,TRN
                         STRING(@s30),AT(938,1042),USE(from:TelephoneNumber),FONT(,8,,FONT:bold),TRN
                         STRING('Contact Name:'),AT(104,2813),USE(?String16:7),FONT(,8),TRN
                         STRING(@s60),AT(938,1198),USE(from:ContactName),FONT(,8,,FONT:bold),TRN
                         STRING(@s255),AT(938,1354),USE(from:EmailAddress),FONT(,8,,FONT:bold),TRN
                         STRING('Email:'),AT(104,1354),USE(?String16:4),FONT(,8),TRN
                         STRING('WAYBILL NUMBER'),AT(3490,1667,3906,260),USE(?String39),FONT(,12,,FONT:bold),CENTER, |
  TRN
                         STRING('Contact Name:'),AT(104,1198),USE(?String16:3),FONT(,8),TRN
                         STRING(@s30),AT(104,260,4771,281),USE(from:CompanyName),FONT(,12,,FONT:bold),TRN
                         STRING(@s30),AT(3490,1979,3906,260),USE(Bar_Code_Temp),FONT('C39 High 12pt LJ3',12,COLOR:Black, |
  ,CHARSET:ANSI),CENTER,COLOR(COLOR:White)
                         STRING(@s30),AT(104,469,3906,180),USE(from:AddressLine1),FONT(,8,,FONT:bold),TRN
                         STRING(@s30),AT(104,625,3906,180),USE(from:AddressLine2),FONT(,8,,FONT:bold),TRN
                         STRING('Account Number:'),AT(104,1875),USE(?AccountNumber),FONT(,8),TRN
                         STRING(@s30),AT(1042,1875,1927,188),USE(to:AccountNumber),FONT(,8,,FONT:bold),TRN
                         STRING('Company Name:'),AT(104,2031),USE(?CompanyName),FONT(,8),TRN
                         STRING(@s30),AT(1042,2031),USE(to:CompanyName),FONT(,8,,FONT:bold),TRN
                         STRING('Address 1:'),AT(104,2188),USE(?Address1),FONT(,8),TRN
                         STRING(@s30),AT(1042,2188),USE(to:AddressLine1),FONT(,8,,FONT:bold),TRN
                         BOX,AT(52,2031,3177,156),USE(?Box1:2),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Address 2:'),AT(104,2344),USE(?Address2),FONT(,8),TRN
                         STRING(@s30),AT(1042,2344),USE(to:AddressLine2),FONT(,8,,FONT:bold),TRN
                         STRING('Suburb:'),AT(104,2500),USE(?Suburb),FONT(,8),TRN
                         STRING(@s30),AT(1042,2500),USE(to:AddressLine3),FONT(,8,,FONT:bold),TRN
                         STRING('Contact Name:'),AT(104,2813),USE(?ContactName),FONT(,8),TRN
                         STRING(@s30),AT(1042,2813),USE(to:ContactName),FONT(,8,,FONT:bold),TRN
                         LINE,AT(990,1875,0,1250),USE(?Line5),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2969,3177,156),USE(?Box1:8),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2656,3177,156),USE(?Box1:6),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Contact Number:'),AT(104,2656),USE(?ContactNumber),FONT(,8),TRN
                         STRING(@s30),AT(1042,2656),USE(to:TelephoneNumber),FONT(,8,,FONT:bold),TRN
                         BOX,AT(52,2813,3177,156),USE(?Box1:7),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2188,3177,156),USE(?Box1:3),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Email:'),AT(104,2969),USE(?Email),FONT(,8),TRN
                         STRING(@s255),AT(1042,2969),USE(to:EmailAddress),FONT(,8,,FONT:bold),TRN
                         STRING('Despatch Time:'),AT(4896,573),USE(?DespatchTime),FONT(,8),TRN
                         STRING(@s30),AT(104,781,3906,180),USE(from:AddressLine3),FONT(,8,,FONT:bold),TRN
                         STRING(@s30),AT(3490,2292,3906,260),USE(tmp:ConsignmentNumber),FONT('Arial',12,,FONT:bold, |
  CHARSET:ANSI),CENTER,TRN
                         BOX,AT(52,2500,3177,156),USE(?Box1:5),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2344,3177,156),USE(?Box1:4),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,1875,3177,156),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(1)
                       END
detail                 DETAIL,AT(0,0,,240),USE(?detailband)
                         STRING(@s20),AT(104,0),USE(tmp:Ref_Number,,?job:Ref_Number:2),FONT('Arial',7,,FONT:bold,CHARSET:ANSI), |
  LEFT,TRN
                         STRING(@s18),AT(1250,0),USE(tmp:IMEI),FONT('Arial',7,,FONT:bold,CHARSET:ANSI),LEFT,TRN
                         TEXT,AT(5417,0,1875,156),USE(tmp:Accessories),FONT(,7),RESIZE,TRN
                         STRING(@s1),AT(7396,0),USE(tmp:Exchanged),FONT(,8),TRN
                         STRING(@s15),AT(4375,0),USE(tmp:SecurityPackNumber),FONT('Arial',7,,,CHARSET:ANSI),LEFT,TRN
                         STRING(@s30),AT(2552,0,1771,156),USE(job:Order_Number),FONT('Arial',7,,,CHARSET:ANSI),LEFT, |
  TRN
                       END
Totals                 DETAIL,AT(0,0,,385),USE(?Totals)
                         LINE,AT(198,52,7135,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Total Number of Items On Waybill :'),AT(260,104),USE(?String59),FONT('Arial',8,,FONT:bold, |
  CHARSET:ANSI),LEFT,TRN
                         STRING(@s8),AT(2344,104),USE(count_temp),FONT(,8,,FONT:bold),TRN
                       END
SundryDetail           DETAIL,AT(0,0,,219),USE(?SundryDetail)
                         STRING(@s30),AT(146,0),USE(was:Description),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),LEFT, |
  TRN
                         STRING(@s30),AT(2344,0),USE(was:Quantity),FONT(,8),TRN
                       END
CNRDetail              DETAIL,AT(0,0,,219),USE(?CNRDetail)
                         STRING(@s30),AT(146,0),USE(wcr:PartNumber),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),LEFT, |
  TRN
                         STRING(@s30),AT(2344,0),USE(wcr:Description),FONT(,8),TRN
                         STRING(@n_8),AT(4427,0),USE(wcr:Quantity),FONT(,8),TRN
                       END
SundryNotes            DETAIL,AT(0,0,7771,1187),USE(?SundryNotes)
                         TEXT,AT(156,52,4094,729),USE(way:UserNotes),FONT(,8),RESIZE,TRN
                         STRING(@s30),AT(4375,52),USE(way:SecurityPackNumber),FONT(,8),LEFT,TRN
                         LINE,AT(104,833,7292,0),USE(?Line6:3),COLOR(COLOR:Black)
                         STRING('Description'),AT(146,885),USE(?Description),FONT(,7),TRN
                         STRING('Quantity'),AT(2344,885),USE(?Quantity),FONT(,7),TRN
                         LINE,AT(104,1094,7292,0),USE(?Line6:4),COLOR(COLOR:Black)
                       END
                       FOOTER,AT(385,9479,7781,1229),USE(?unnamed:2)
                         LINE,AT(208,52,7135,0),USE(?Line1:2),COLOR(COLOR:Black)
                         TEXT,AT(208,104,7135,1042),USE(stt:Text),FONT(,8)
                       END
                       FORM,AT(385,448,7792,10521),USE(?unnamed:3)
                         STRING('Part Number'),AT(208,3333),USE(?crnPartNumber),FONT(,8,,FONT:bold),HIDE,TRN
                         STRING('Description'),AT(2406,3333),USE(?crnDescription),FONT(,8,,FONT:bold),HIDE,TRN
                         STRING('Job No'),AT(156,3333,,156),USE(?JobNo),FONT(,7),TRN
                         STRING('I.M.E.I. No'),AT(1313,3333),USE(?IMEINo),FONT(,7),TRN
                         STRING('Order No'),AT(2615,3333),USE(?OrderNo),FONT(,7),TRN
                         STRING('Accessories'),AT(5479,3333),USE(?Accessories),FONT(,7),TRN
                         STRING('Quantity'),AT(4385,3333),USE(?crnQuantity),FONT(,8,,FONT:bold),HIDE,TRN
                         STRING('Security Pack No'),AT(4438,3333),USE(?SecurityPackNo),FONT(,7),TRN
                         LINE,AT(104,3281,7292,0),USE(?Line6),COLOR(COLOR:Black)
                         LINE,AT(104,3542,7292,0),USE(?Line6:2),COLOR(COLOR:Black)
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR1               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('RepWaybillDespatch')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:DEFPRINT.Open                                     ! File DEFPRINT used by this procedure, so make sure it's RelationManager is open
  Relate:EXCHACC.Open                                      ! File EXCHACC used by this procedure, so make sure it's RelationManager is open
  Relate:JOBACC.SetOpenRelated()
  Relate:JOBACC.Open                                       ! File JOBACC used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:WAYBILLS.SetOpenRelated()
  Relate:WAYBILLS.Open                                     ! File WAYBILLS used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:WAYSUND.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:WAYCNR.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOAN.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBS.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  !pass   func:FromAccountNumber - The Account number to appear under "From Sender"
  !       func:FromType          - Is the account number of the Head "TRA" or Sub "SUB" account
  !       func:ToACcountNumber   - The Account number to appear under "Deliver To"
  !       func:ToType            - Is the account number of the Head "TRA" or Sub "SUB" account
  !       func:ConsignmentNUmber - Waybill number
        !       func:Courier           - Despatch Courier
        
        locWaybillNumber = p_web.GSV('Waybill:ConsignmentNumber')  
  
          !Complete change to waybill prefix
          !change made by Paul 29/09/2009 - log no 11042
        If GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI') = 1
            ! TODO: What is the point of this code?
            tmp:HeadAccountNo = clip(GETINI('BOOKING','HeadAccount','AA20',Clip(Path()) & '\SB2KDEF.INI'))
        Else
      !cheat and manually set to 'AA20'
            tmp:HeadAccountNo = 'AA20'
        End !If GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI') = 1
        tmp:Courier = p_web.GSV('Waybill::Courier')
  
  
  !now look up the waybill and check if it was procduced from the head account or not
  
  
  
  
  Access:WAYBILLS.ClearKey(way:WaybillNumberKey)
  way:WaybillNumber = p_web.GSV('Waybill:ConsignmentNumber')
  If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
      !Found
      If way:FromAccount = tmp:HeadAccountNo then
          !its been produced from the head account - so attach prefix accordingly
          If GETINI('PRINTING','SetWaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
              tmp:ConsignmentNumber = Clip(GETINI('PRINTING','WaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI')) & Format(p_web.GSV('Waybill:ConsignmentNumber'),@n07)
          Else ! If GETINI('PRINTING','setwaybillprefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
              tmp:ConsignmentNumber = 'VDC' & Format(p_web.GSV('Waybill:ConsignmentNumber'),@n07)
          End ! If GETINI('PRINTING','setwaybillprefix',,Clip(Path()) & '\SB2KDEF.INI') = 1
      Else
          !must have been produced from an RRC account - so change the prefix
          !now look up the trade account
  
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = way:FromAccount
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              !now see if the RRC waybill prefix has been filled in
              If clip(tra:RRCWaybillPrefix) <> '' then
                  !Prefix filled in - so use it
                  tmp:ConsignmentNumber = clip(tra:RRCWaybillPrefix) & Format(p_web.GSV('Waybill:ConsignmentNumber'),@n07)
              Else
                  !Changed By Paul 17/09/09 - log no 11042
                  tmp:ConsignmentNumber = 'VDC' & Format(p_web.GSV('Waybill:ConsignmentNumber'),@n07)
              End !If clip(tra:RRCWaybillPrefix) <> '' then
          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
      End !If way:AccountNumber = tmp:HeadAccountNo then
  Else
      !error
  End ! If Access:WAYBILLS.TryFetch(way:WaybillNumber) = Level:Benign
  
  
  
  !Change End
  
  
  
  
  
  
  SELF.Open(ProgressWindow)                                ! Open window
  !printed by
  set(defaults)
  access:defaults.next()
  
  access:users.clearkey(use:User_Code_Key)
  use:password =p_web.GSV('BookingUserCode')
  access:users.fetch(use:User_Code_Key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  
  !Barcode Bit
  ! Changing (DBH 04/08/2006) # 6643 - I think Code39 = 1
  !code_temp            = 3
  ! to (DBH 04/08/2006) # 6643
  Code_Temp = 1
  ! End (DBH 04/08/2006) #6643
  option_temp          = 0
  
  Bar_Code_String_Temp = CLIP(tmp:ConsignmentNumber)
  !SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  Bar_Code_Temp = '*' & CLIP(tmp:ConsignmentNumber) & '*'
  
  !Work out addresses
  
  access:stantext.clearkey(stt:description_key)
  stt:description = 'WAYBILL'
  access:stantext.fetch(stt:description_key)
  
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  
  
  Case p_web.GSV('Waybill:FromType')
      Of 'TRA'
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = p_web.GSV('Wayill:FromAccountNumber')
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              from:CompanyName        = tra:Company_Name
              from:AddressLine1       = tra:Address_Line1
              from:AddressLine2       = tra:Address_Line2
              from:AddressLine3       = tra:Address_Line3
              from:TelephoneNumber    = tra:Telephone_Number
              from:ContactName        = tra:Contact_Name
              from:EmailAddress       = tra:EmailAddress
          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      Of 'SUB'
          Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
          sub:Account_Number  = p_web.GSV('Wayill:FromAccountNumber')
          If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
              !Found
              from:CompanyName        = sub:Company_Name
              from:AddressLine1       = sub:Address_Line1
              from:AddressLine2       = sub:Address_Line2
              from:AddressLine3       = sub:Address_Line3
              from:TelephoneNumber    = sub:Telephone_Number
              from:ContactName        = sub:Contact_Name
              from:EmailAddress       = sub:EmailAddress
          Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      Of 'DEF'
          from:CompanyName        = def:User_Name
          from:AddressLine1       = def:Address_Line1
          from:AddressLine2       = def:Address_Line2
          from:AddressLine3       = def:Address_Line3
          from:TelephoneNumber    = def:Telephone_Number
          from:ContactName        = ''
          from:EmailAddress       = def:EmailAddress
  
  End !func:FromType
  
  Case p_web.GSV('Waybill:ToType')
      Of 'TRA'
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = p_web.GSV('Waybill:ToAccountNumber')
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              to:AccountNumber      = tra:Account_Number
              to:CompanyName        = tra:Company_Name
              to:AddressLine1       = tra:Address_Line1
              to:AddressLine2       = tra:Address_Line2
              to:AddressLine3       = tra:Address_Line3
              to:TelephoneNumber    = tra:Telephone_Number
              to:ContactName        = tra:Contact_Name
              to:EmailAddress       = tra:EmailAddress
          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
      Of 'SUB'
          Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
          sub:Account_Number  = p_web.GSV('Waybill:ToAccountNumber')
          If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
              !Found
              to:AccountNumber      = sub:Account_Number
              to:CompanyName        = sub:Company_Name
              to:AddressLine1       = sub:Address_Line1
              to:AddressLine2       = sub:Address_Line2
              to:AddressLine3       = sub:Address_Line3
              to:TelephoneNumber    = sub:Telephone_Number
              to:ContactName        = sub:Contact_Name
              to:EmailAddress       = sub:EmailAddress
              if sub:UseCustDespAdd = 'YES'
                  tmp:CustomerAddress = 'CUS'
              else
                  tmp:CustomerAddress = 'DEL'
              end
          Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
  
  End !func:ToType
  
      !New way of working out addressess
      tmp:WayBillID = 0
      Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
      way:WayBillNumber = p_web.GSV('Waybill:ConsignmentNumber')
      If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
          !Found
          If way:WaybillID <> 0
  
              !Is this a rejection. If so, show title - 4285 (DBH: 14-06-2004)
              IF way:WaybillID = 13
                  SetTarget(Report)
                  ?WaybillRejection{prop:Hide} = False
                  SetTarget()
              End !IF way:WaybillID = 13
              tmp:WayBillID = way:WaybillID
              Access:TRADEACC.Clearkey(tra:Account_Number_Key)
              tra:Account_Number  = way:FromAccount
              If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  !Found
                  from:CompanyName    = tra:Company_Name
                  from:AddressLine1   = tra:Address_Line1
                  from:AddressLine2   = tra:Address_Line2
                  from:AddressLine3   = tra:Address_Line3
                  from:TelephoneNumber= tra:Telephone_Number
                  from:ContactName    = tra:Contact_Name
                  from:EmailAddress   = tra:EmailAddress
              Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  !Error
              End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
              ! Inserting (DBH 07/07/2006) # 7149 - PUP to RRC Waybill. Pickup the address from the job
              If way:WayBillID = 20
                  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
                  sub:Account_Number  = way:FromAccount
                  If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                      !Found
                      from:CompanyName    = sub:Company_Name
                      from:AddressLine1   = sub:Address_Line1
                      from:AddressLine2   = sub:Address_Line2
                      from:AddressLine3   = sub:Address_Line3
                      from:TelephoneNumber= sub:Telephone_Number
                      from:ContactName    = sub:Contact_Name
                      from:EmailAddress   = sub:EmailAddress
                  Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                      !Error
                  End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                  ! Insert --- Use the prefix based on the account of the job (DBH: 11/08/2009) #11005
                  if (sub:VCPWaybillPRefix <> '')
                      settarget(report)
                      tmp:ConsignmentNumber = clip(sub:VCPWaybillPrefix) & Format(p_web.GSV('Waybill:ConsignmentNumber'),@n07)
                      settarget()
                  else ! if (sub:VCPWaybillPRefix <> '')
                      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                      tra:Account_Number    = sub:Main_Account_Number
                      if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                          ! Found
                          if (tra:VCPWaybillPrefix <> '')
                              settarget(report)
                              tmp:ConsignmentNumber = clip(tra:VCPWaybillPrefix) & Format(p_web.GSV('Waybill:ConsignmentNumber'),@n07)
                              settarget()
                          end ! if (tra:VCPWaybillPrefix <> '')
                      else ! if (Access:TRADE.TryFetch(tra:Account_Number_Key) = Level:Benign)
                          ! Error
                      end ! if (Access:TRADE.TryFetch(tra:Account_Number_Key) = Level:Benign)
                  end ! if (sub:VCPWaybillPRefix <> '')
                  ! end --- (DBH: 11/08/2009) #11005
  
              End ! If way:WayBillID = 20
              ! End (DBH 07/07/2006) #7149
  
              Case way:WayBillID
              Of 1 Orof 5 Orof 6 Orof 11 Orof 12 Orof 13 Orof 20
                  ! Inserting (DBH 19/10/2007) # 9451 - Make allowance for PUP Waybills going back to the customer
                  If way:ToAccount = 'CUSTOMER'
                      !added by Paul 28/04/2010 - Log no 11419
                      !lookup sub account to try and get the contact name
                      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                      sub:Account_Number  = way:ToAccount
                      If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:notify then
                        !lookup the head account
                          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                          tra:Account_Number  = way:ToAccount
                          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign then
                              If tra:Use_Delivery_Address = 'YES' then
                                  to:ContactName     = clip(tra:Contact_Name)
                              Else
                                  to:ContactName     = ''
                              End
                          Else
                              to:ContactName     = ''
                          End
                      Else
                          If sub:Use_Delivery_Address = 'YES' then
                              to:ContactName     = clip(sub:Contact_Name)
                          Else
                              to:ContactName     = ''
                          End
                      End
  
                      to:AccountNumber   = job:Account_Number
                      to:CompanyName     = job:Company_Name
                      to:AddressLine1    = job:Address_Line1
                      to:AddressLine2    = job:Address_Line2
                      to:AddressLine3    = job:Address_Line3
                      to:TelephoneNumber = job:Telephone_Number
  
                      to:EmailAddress    = jobe:EndUserEmailAddress
                  Else ! If way:ToAccount = 'CUSTOMER'
                  ! End (DBH 19/10/2007) #9451
  
                      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                      tra:Account_Number  = way:ToAccount
                      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                          !Found
                          to:AccountNumber  = tra:Account_Number
                          to:CompanyName    = tra:Company_Name
                          to:AddressLine1   = tra:Address_Line1
                          to:AddressLine2   = tra:Address_Line2
                          to:AddressLine3   = tra:Address_Line3
                          to:TelephoneNumber= tra:Telephone_Number
                          to:ContactName    = tra:Contact_Name
                          to:EmailAddress   = tra:EmailAddress
                      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                          !Error
                      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  End ! If way:ToAccount = 'CUSTOMER'
              ! Inserting (DBH 07/07/2006) # 7149 - RRC to PUP Address from the job account
              Of 21 Orof 22 !RRC To PUP
                  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
                  sub:Account_Number  = way:ToAccount
                  If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                      !Found
                      to:AccountNumber  = sub:Account_Number
                      to:CompanyName    = sub:Company_Name
                      to:AddressLine1   = sub:Address_Line1
                      to:AddressLine2   = sub:Address_Line2
                      to:AddressLine3   = sub:Address_Line3
                      to:TelephoneNumber= sub:Telephone_Number
                      to:ContactName    = sub:Contact_Name
                      to:EmailAddress   = sub:EmailAddress
                  Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                      !Error
                  End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                  ! End (DBH 07/07/2006) #7149
              ! Inserting (DBH 05/09/2006) # 7995 - Use default addresses for sundry waybill
              Of 300 !Sundry Waybill. Address covered above
                  Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                  tra:Account_Number = way:FromAccount
                  If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                      !Found
                      from:CompanyName    = tra:Company_Name
                      from:AddressLine1   = tra:Address_Line1
                      from:AddressLine2   = tra:Address_Line2
                      from:AddressLine3   = tra:Address_Line3
                      from:TelephoneNumber    = tra:Telephone_Number
                      from:ContactName    = tra:Contact_Name
                      from:EmailAddress   = tra:EmailAddress
                  Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                      !Error
                  End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                  If way:ToAccount = 'OTHER'
                      to:AccountNumber = way:OtherAccountNumber
                      to:CompanyName  = way:OtherCompanyName
                      to:AddressLine1 = way:OtherAddress1
                      to:AddressLine2 = way:OtherAddress2
                      to:AddressLine3 = way:OtherAddress3
                      to:TelephoneNumber = way:OtherTelephoneNO
                      to:ContactName  = way:OtherContactName
                      to:EmailAddress = way:OtherEmailAddress
                  Else ! If way:ToAccount = 'OTHER'
                      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                      tra:Account_Number = way:ToAccount
                      If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                          !Found
                          to:AccountNumber      = tra:Account_Number
                          to:CompanyName        = tra:Company_Name
                          to:AddressLine1       = tra:Address_Line1
                          to:AddressLine2       = tra:Address_Line2
                          to:AddressLine3       = tra:Address_Line3
                          to:TelephoneNumber    = tra:Telephone_Number
                          to:ContactName        = tra:Contact_Name
                          to:EmailAddress       = tra:EmailAddress
                      Else ! If Access:TRADEAC.TryFetch(tra:Account_Number_Key) = Level:Benign
                          !Error
                      End ! If Access:TRADEAC.TryFetch(tra:Account_Number_Key) = Level:Benign
                  End ! If way:ToAccount = 'OTHER'
              ! End (DBH 05/09/2006) #7995
              Else
                  Get(glo:q_JobNumber,1)
  
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = glo:Job_Number_Pointer
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Found
  
                  Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Error
                  End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                  sub:Account_Number  = way:ToAccount
                  If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                      !Found
                      if sub:UseCustDespAdd = 'YES'
                          to:AccountNumber   = job:Account_Number
                          to:CompanyName     = job:Company_Name
                          to:AddressLine1    = job:Address_Line1
                          to:AddressLine2    = job:Address_Line2
                          to:AddressLine3    = job:Address_Line3
                          to:TelephoneNumber = job:Telephone_Number
                          !changed bt Paul 28/04/2010 - log no 11419
                          if sub:Use_Delivery_Address = 'YES' then
                              to:ContactName     = clip(sub:Contact_Name)
                          Else
                              to:ContactName     = ''
                          End
                          !end change
                          to:EmailAddress    = jobe:EndUserEmailAddress
                      else
                          to:AccountNumber   = job:Account_Number
                          to:CompanyName     = job:Company_Name_Delivery
                          to:AddressLine1    = job:Address_Line1_Delivery
                          to:AddressLine2    = job:Address_Line2_Delivery
                          to:AddressLine3    = job:Address_Line3_Delivery
                          to:TelephoneNumber = job:Telephone_Delivery
                          !changed bt Paul 28/04/2010 - log no 11419
                          if sub:Use_Delivery_Address = 'YES' then
                              to:ContactName     = clip(sub:Contact_Name)
                          Else
                              to:ContactName     = ''
                          End
                          !end change
                          to:EmailAddress    = jobe:EndUserEmailAddress
                      end
                  Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                      !Error
                  End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
  
              End ! Case way:WayBillID
          End !If way:WaybillID <> 0
      Else!If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
  Do DefineListboxStyle
  INIMgr.Fetch('RepWaybillDespatch',ProgressWindow)        ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:WAYBILLS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, way:WayBillNumber)
  ThisReport.AddSortOrder(way:WayBillNumberKey)
  ThisReport.AddRange(way:WayBillNumber,locWaybillNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:WAYBILLS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHACC.Close
    Relate:JOBACC.Close
    Relate:STANTEXT.Close
    Relate:WAYBILLS.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('RepWaybillDespatch',ProgressWindow)     ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'RepWaybillDespatch',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.Init(report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,report{PROPPRINT:Paper},PAPER:USER), CHOOSE(report{PROP:Thous}=True,PROP:Thous,CHOOSE(report{PROP:MM}=True,PROP:MM,CHOOSE(report{PROP:Points}=True,PROP:Points,0))), report{PROPPRINT:PaperWidth}, report{PROPPRINT:PaperHeight}, report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetTitle('Waybill')                  !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR1.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetViewerPrefs(PDFXTR1:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetFontEmbedding(True, True, False, False, False)
    PDFXTR1.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'RepWaybillDespatch',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR1.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  CASE way:WaybillID !============================================
  OF 400 OROF 401 ! Credit Note Request
      SetTarget(Report)
      ?JobNo{prop:Hide} = 1
      ?IMEINo{prop:Hide} = 1
      ?OrderNo{prop:Hide} = 1
      ?Accessories{prop:Hide} = 1
      ?SecurityPackNo{prop:Hide} = 1
      ?crnPartNumber{prop:Hide} = 0
      ?crnDescription{prop:Hide} = 0
      ?crnQuantity{prop:Hide} = 0
      tmp:DateDespatched = way:TheDate
      tmp:TimeDespatched = way:TheTime
      tmp:Courier        = way:Courier
      IF (way:WaybillID = 401)
          ?crnPartNumber{prop:Text} = 'Model Number'
          ?crnDescription{prop:Text} = 'IMEI Number'
          ?crnQuantity{prop:Hide} = 1
          ?wcr:Quantity{prop:Hide} = 1
      END
      SetTarget()
  
      first# = 1
      Access:WAYCNR.Clearkey(wcr:EnteredKey)
      wcr:WAYBILLSRecordNumber = way:RecordNumber
      Set(wcr:EnteredKey,wcr:EnteredKey)
      Loop ! Begin Loop
          If Access:WAYCNR.Next()
              Break
          End ! If Access:WAYSUND.Next()
          If wcr:WAYBILLSRecordNumber <> way:RecordNumber
              Break
          End ! If was:WAYBILLSRecordNumber <> way:RecordNumber
          Print(rpt:CNRDetail)
          count_temp += 1
      End ! Loop
    OF 300 !Sunry Waybill
      SetTarget(Report)
      ?JobNo{prop:Text} = 'User Notes'
      ?IMEINo{prop:Hide} = True
      ?OrderNo{prop:Hide} = True
      ?Accessories{prop:Hide} = True
      tmp:DateDespatched = way:TheDate
      tmp:TimeDespatched = way:TheTime
      tmp:Courier        = way:Courier
      SetTarget()
      Print(rpt:SundryNotes)
  
      Access:WAYSUND.Clearkey(was:EnteredKey)
      was:WAYBILLSRecordNumber = way:RecordNumber
      Set(was:EnteredKey,was:EnteredKey)
      Loop ! Begin Loop
          If Access:WAYSUND.Next()
              Break
          End ! If Access:WAYSUND.Next()
          If was:WAYBILLSRecordNumber <> way:RecordNumber
              Break
          End ! If was:WAYBILLSRecordNumber <> way:RecordNumber
            Print(rpt:SundryDetail)
            count_temp += was:Quantity
      End ! Loop
  
  ELSE
      !New WayBill Procedure
      locNewProcedure = 1
      Save_waj_ID = Access:WAYBILLJ.SaveFile()
      Access:WAYBILLJ.ClearKey(waj:JobNumberKey)
      waj:WayBillNumber = p_web.GSV('Waybill:ConsignmentNumber')
      Set(waj:JobNumberKey,waj:JobNumberKey)
      Loop
          If Access:WAYBILLJ.NEXT()
             Break
          End !If
          If waj:WayBillNumber <> p_web.GSV('Waybill:ConsignmentNumber')     |
              Then Break.  ! End If
          !Write the Date Despatched on the report - L879 (DBH: 17-07-2003)
  !                Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
  !                way:WayBillNumber = func:ConsignmentNumber
  !                If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
              !Found
              tmp:DateDespatched  = way:TheDate
              tmp:TimeDespatched  = way:TheTime
  !                Else !If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
  !                    !Error
  !                End !If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
  
          tmp:Exchanged = ''
          Access:JOBS.Clearkey(job:Ref_Number_Key)
          job:Ref_Number  = waj:JobNumber
          If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              !Found
              Access:WEBJOB.Clearkey(wob:RefNumberKey)
              wob:RefNumber   = job:Ref_Number
              If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                  !Found
  
              Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                  !Error
              End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
              Access:TRADEACC.Clearkey(tra:Account_Number_Key)
              tra:Account_Number  = wob:HeadAccountNumber
              If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  !Found
  
              Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  !Error
              End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
              tmp:Ref_Number  = Clip(job:Ref_Number) & '-' & Clip(tra:BranchIdentification) & Clip(wob:JobNumber)
  
              ! -----------------------------------------------------------------------------
              ! VP115 / VP120  21st NOV 02 - must display correct IMEI number
              case waj:WayBillNumber
                  of job:Exchange_Consignment_Number
                      ! Exchange waybill
                      Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                      xch:Ref_Number  = job:Exchange_Unit_Number
                      if Access:EXCHANGE.Fetch(xch:Ref_Number_Key)
                          tmp:IMEI = waj:IMEINumber
                      else
                          ! Display exchange unit IMEI number
                          tmp:IMEI = xch:ESN
                      end
                  else
                      tmp:IMEI = waj:IMEINumber
                      If job:Exchange_Unit_Number <> 0
                          tmp:Exchanged = 'E'
                      End !If job:Exchange_Unit_Number <> 0
              end
              ! -----------------------------------------------------------------------------
  
              !tmp:IMEI        = waj:IMEINumber
              tmp:SecurityPackNumber  = waj:SecurityPackNumber
              tmp:Accessories = ''
              !Only print accessories, if it's a job, and there is no accessories attached
              !Or it's an exchange unit being despatched
  
              If job:Exchange_Unit_Number = 0 Or way:WaybillID = 3 Or way:WaybillID = 6 Or way:WayBillID = 8 Or |
                  way:WaybillID = 10 Or way:WaybillID = 11 Or way:WaybillID = 12
                  Save_jac_ID = Access:JOBACC.SaveFile()
                  Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                  jac:Ref_Number = job:Ref_Number
                  Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
                  Loop
                      If Access:JOBACC.NEXT()
                         Break
                      End !If
                      If jac:Ref_Number <> job:Ref_Number      |
                          Then Break.  ! End If
                      ! Inserting (DBH 08/03/2007) # 8703 - Show all accessories if the isn't a 0 (to ARC) or 1 (to RRC)
                      If way:WaybillType = 0 Or way:WaybillType = 1
                      ! End (DBH 08/03/2007) #8703
                          !Only show accessories that were sent to ARC - 4285 (DBH: 26-05-2004)
                          If jac:Attached <> True
                              Cycle
                          End !If jac:Attached <> True
                      End ! If way:WaybillType <> 0 And way:WaybillType <> 1
  
                      If tmp:Accessories = ''
                          tmp:Accessories = Clip(jac:Accessory)
                      Else !If tmp:Accessories = ''
                          tmp:Accessories = Clip(tmp:Accessories) & ', ' & Clip(jac:Accessory)
                      End !If tmp:Accessories = ''
                  End !Loop
                  Access:JOBACC.RestoreFile(Save_jac_ID)
  
              End !If waj:JobType = 'JOB' And job:Exchange_Unit_Number = 0
              !added by Paul 26/04/2010 - log no 10546
              If clip(glo:Notes_Global) = 'OBF' then
                  !need to check if the new OBF address is filled in
                  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                  tra:Account_Number  = p_web.GSV('Waybill:ToAccountNumber')
                  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                      If clip(tra:OBFAddress1) <> '' then
                          !address is filled in
                          SetTarget(Report)
                          to:CompanyName      = clip(tra:OBFCompanyName)
                          to:AddressLine1     = clip(tra:OBFAddress1)
                          to:AddressLine2     = clip(tra:OBFAddress2)
                          to:AddressLine3     = clip(tra:OBFSuburb)
                          to:TelephoneNumber  = clip(tra:OBFContactNumber)
                          to:ContactName      = clip(tra:OBFContactName)
                          to:EmailAddress     = clip(tra:OBFEmailAddress)
                          Settarget()
                      End
                  End
              End
              Print(rpt:Detail)
          Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              !Error
          End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      End !Loop
      Access:WAYBILLJ.RestoreFile(Save_waj_ID)
  END   !============================================
      If locNewProcedure = 1
      ! Don't really know the despatch date with any accuracy.
      ! So will show date printed on the report instead - L879 (DBH: 17-07-2003)
          tmp:DateDespatched = Today()
          tmp:TimeDespatched = Clock()
          SetTarget(Report)
          ?DespatchDate{prop:Text} = 'Date Printed:'
          ?DespatchTime{prop:Text} = 'Time Printed:'
          SetTarget()
  
          Free(print_group)
          Clear(print_group)
          setcursor(cursor:wait)
  
          If tmp:WaybillID <> 0
              Loop x# = 1 To Records(glo:q_JobNumber)
                  Get(glo:Q_JobNumber, x#)
                  
  
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = glo:Job_Number_Pointer
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      ! Found
                      Access:JOBSE.Clearkey(jobe:RefNumberKey)
                      jobe:RefNumber  = job:Ref_Number
                      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      ! Found
  
                      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      ! Error
                      End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
                      Access:WEBJOB.Clearkey(wob:RefNumberKey)
                      wob:RefNumber   = job:Ref_Number
                      If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                      ! Found
  
                      Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                      ! Error
                      End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  
                      tmp:Ref_Number = Job:Ref_Number & '-' & tmp:BranchIdentification & wob:JobNumber
  
                      ! What is being despatched?
                      job# = 0
                      exc# = 0
                      loa# = 0
  
                      Case p_web.GSV('Waybill:ConsignmentNumber')
                      Of job:Incoming_Consignment_Number
                          job#                   = 1
                          tmp:SecurityPackNumber = jobe:InSecurityPackNo
                      Of job:Exchange_Consignment_Number
                          exc#                   = 1
                          tmp:SecurityPackNumber = jobe:ExcSecurityPackNo
                      Of job:Loan_Consignment_Number
                          loa#                   = 1
                          tmp:SecurityPackNumber = jobe:LoaSecurityPackNo
                      Of job:Consignment_Number
                          job#                   = 1
                          tmp:SecurityPackNumber = jobe:JobSecurityPackNo
                      Of wob:ExcWaybillNumber
                          exc#                   = 1
                          tmp:SecurityPackNumber = jobe:ExcSecurityPackNo
                      Of wob:LoaWayBillNumber
                          loa#                   = 1
                          tmp:SecurityPackNumber = jobe:LoaSecurityPackNo
                      Of wob:JobWayBillNumber
                          job#                   = 1
                          tmp:SecurityPackNumber = jobe:JobSecurityPackNo
                      End ! Case func:ConsignmentNumber
  
                      If job#
                          tmp:Accessories = ''
                          Save_jac_ID     = Access:JOBACC.SaveFile()
                          Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                          jac:Ref_Number = job:Ref_Number
                          Set(jac:Ref_Number_Key, jac:Ref_Number_Key)
                          Loop
                              If Access:JOBACC.NEXT()
                                  Break
                              End ! If
                              If jac:Ref_Number <> job:Ref_Number      |
                                  Then Break   ! End If
                              End ! If
                              If tmp:Accessories = ''
                                  tmp:Accessories = Clip(jac:Accessory)
                              Else ! If tmp:Accessories = ''
                                  tmp:Accessories = Clip(tmp:Accessories) & ', ' & Clip(jac:Accessory)
                              End ! If tmp:Accessories = ''
                          End ! Loop
                          Access:JOBACC.RestoreFile(Save_jac_ID)
                          tmp:IMEI    = job:ESN
  
                      End ! If job#
  
                      If exc#
                          Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                          xch:Ref_Number  = job:Exchange_Unit_Number
                          If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                              ! Found
                              Save_xca_ID = Access:EXCHACC.SaveFile()
                              Access:EXCHACC.ClearKey(xca:Ref_Number_Key)
                              xca:Ref_Number = xch:Ref_Number
                              Set(xca:Ref_Number_Key, xca:Ref_Number_Key)
                              Loop
                                  If Access:EXCHACC.NEXT()
                                      Break
                                  End ! If
                                  If xca:Ref_Number <> xch:Ref_Number      |
                                      Then Break   ! End If
                                  End ! If
                                  If tmp:Accessories = ''
                                      tmp:Accessories = Clip(xca:Accessory)
                                  Else ! If tmp:Accessories = ''
                                      tmp:Accessories = Clip(tmp:Accessories) & ', ' & Clip(xca:Accessory)
                                  End ! If tmp:Accessories = ''
  
                              End ! Loop
                              Access:EXCHACC.RestoreFile(Save_xca_ID)
                          Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                          ! Error
  
                          End ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                          tmp:IMEI    = xch:ESN
  
                      End ! If exc#
  
                      If loa#
                          Access:LOAN.Clearkey(loa:Ref_Number_Key)
                          loa:Ref_Number  = job:Loan_Unit_Number
                          If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                              ! Found
                              tmp:IMEI    = loa:ESN
                          Else ! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                          ! Error
                          End ! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                      End ! If loa#
                      Print(rpt:Detail)
                  Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  ! Error
                  End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              End ! Loop x# = 1 To Records(glo:q_JobNumber)
          Else ! tmp:WaybillID <> 0
  
              Loop x# = 1 To Records(glo:q_jobnumber)
                  Get(glo:q_jobnumber, x#)
  
  
                  access:jobs.clearkey(job:ref_number_key)
                  job:ref_number = glo:job_number_pointer
                  if access:jobs.fetch(job:ref_number_key) = Level:Benign
                      Access:JOBSE.Clearkey(jobe:RefNumberKey)
                      jobe:RefNumber  = job:Ref_Number
                      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      ! Found
                      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      ! Error
                      End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
  
                  !--- Webify tmp:Ref_Number Using job:Ref_Number----------------
                  ! 26 Aug 2002 John
                  !
                      tmp:Ref_number = job:Ref_number
                      if glo:WebJob then
  
                          DespatchType = jobe:DespatchType
  
                      ! Change the tmp ref number if you
                      ! can Look up from the wob file
                          access:Webjob.clearkey(wob:RefNumberKey)
                          wob:RefNumber = job:Ref_number
                          if access:Webjob.fetch(wob:refNumberKey) = level:benign then
                              tmp:Ref_Number = Job:Ref_Number & '-' & tmp:BranchIdentification & wob:JobNumber
                          end ! if
  
                          If p_web.GSV('Waybill:ToType') = 'CUS'
                              SetTarget(Report)
                              Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                              sub:Account_Number  = job:Account_Number
                              If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                                  ! Found
                                  to:CompanyName     = sub:Company_Name
                                  to:AddressLine1    = sub:Address_Line1
                                  to:AddressLine2    = sub:Address_Line2
                                  to:AddressLine3    = sub:Address_Line3
                                  to:TelephoneNumber = sub:Telephone_Number
                                  to:ContactName     = sub:Contact_Name
                                  to:EmailAddress    = sub:EmailAddress
                                  if sub:UseCustDespAdd = 'YES'
                                      to:CompanyName     = job:Company_Name
                                      to:AddressLine1    = job:Address_Line1
                                      to:AddressLine2    = job:Address_Line2
                                      to:AddressLine3    = job:Address_Line3
                                      to:TelephoneNumber = job:Telephone_Number
                                      to:ContactName     = ''
                                      to:EmailAddress    = jobe:EndUserEmailAddress
                                  else
                                      to:CompanyName     = job:Company_Name_Delivery
                                      to:AddressLine1    = job:Address_Line1_Delivery
                                      to:AddressLine2    = job:Address_Line2_Delivery
                                      to:AddressLine3    = job:Address_Line3_Delivery
                                      to:TelephoneNumber = job:Telephone_Delivery
                                      to:ContactName     = ''
                                      to:EmailAddress    = jobe:EndUserEmailAddress
                                  end ! if
                              Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                              ! Error
                              End ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
  
  
                              settarget()
                          End ! If
  
                      ELSE
                          Despatchtype = job:Despatch_type
  
                          ! Check to see if it *IS* an RRC!
                          Access:SubTracc.ClearKey(sub:Account_Number_Key)
                          sub:Account_Number = p_web.GSV('Waybill:ToAccountNumber')
                          IF Access:SubTracc.Fetch(sub:Account_Number_Key)
                      ! Error!
                      ! Don't need to change location here, it should be hanled by the despatch procedures
                      ! LocationChange(Clip(GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI')))
                              if Records(glo:q_jobnumber) = 1 ! Individual despatch
                                  settarget(Report)
                                  case tmp:CustomerAddress
                                  of 'CUS' ! Customer address
                                      to:CompanyName     = job:Company_Name
                                      to:AddressLine1    = job:Address_Line1
                                      to:AddressLine2    = job:Address_Line2
                                      to:AddressLine3    = job:Address_Line3
                                      to:TelephoneNumber = job:Telephone_Number
                                      to:ContactName     = ''
                                      to:EmailAddress    = jobe:EndUserEmailAddress
                                  of 'DEL' ! Delivery address
                                      to:CompanyName     = job:Company_Name_Delivery
                                      to:AddressLine1    = job:Address_Line1_Delivery
                                      to:AddressLine2    = job:Address_Line2_Delivery
                                      to:AddressLine3    = job:Address_Line3_Delivery
                                      to:TelephoneNumber = job:Telephone_Delivery
                                      to:ContactName     = ''
                                      to:EmailAddress    = jobe:EndUserEmailAddress
                                  end ! case
                                  settarget()
                              end ! if
                          ELSE
                              Access:TradeAcc.ClearKey(tra:Account_Number_Key)
                              tra:Account_Number = sub:Main_Account_Number
                              IF Access:TradeAcc.Fetch(tra:Account_Number_Key)
                                  ! Error!
                                  if despatchType = 'JOB' then
  !                                            If glo:Select21 <> 'REPRINT'
  !                                                LocationChange(Clip(GETINI('RRC', 'DespatchToCustomer',, CLIP(PATH()) & '\SB2KDEF.INI')))
  !                                            End ! If glo:Select21 <> 'REPRINT'
  
                                      if Records(glo:q_jobnumber) = 1 and tmp:WaybillID = 0! Individual despatch
                                          settarget(Report)
                                          case tmp:CustomerAddress
                                          of 'CUS' ! Customer address
                                              to:CompanyName     = job:Company_Name
                                              to:AddressLine1    = job:Address_Line1
                                              to:AddressLine2    = job:Address_Line2
                                              to:AddressLine3    = job:Address_Line3
                                              to:TelephoneNumber = job:Telephone_Number
                                              to:ContactName     = ''
                                              to:EmailAddress    = jobe:EndUserEmailAddress
                                          of 'DEL' ! Delivery address
                                              to:CompanyName     = job:Company_Name_Delivery
                                              to:AddressLine1    = job:Address_Line1_Delivery
                                              to:AddressLine2    = job:Address_Line2_Delivery
                                              to:AddressLine3    = job:Address_Line3_Delivery
                                              to:TelephoneNumber = job:Telephone_Delivery
                                              to:ContactName     = ''
                                              to:EmailAddress    = jobe:EndUserEmailAddress
                                          end ! case
                                          settarget()
                                      end ! if
                                  end ! if
                              ELSE
                                  IF tra:RemoteRepairCentre = TRUE
  !                                            if despatchType = 'JOB' And glo:Select21 <> 'REPRINT' then
  !                                                LocationChange(Clip(GETINI('RRC', 'InTransitRRC',, CLIP(PATH()) & '\SB2KDEF.INI')))
  !                                            end ! if
                                  ELSE
                                      if despatchType = 'JOB' then
  !                                                If glo:Select21 <> 'REPRINT'
  !                                                    LocationChange(Clip(GETINI('RRC', 'DespatchToCustomer',, CLIP(PATH()) & '\SB2KDEF.INI')))
  !                                                End ! If glo:Select21 <> 'REPRINT'
                                          if Records(glo:q_jobnumber) = 1 and tmp:WaybillID = 0! Individual despatch
                                              settarget(Report)
                                              case tmp:CustomerAddress
                                              of 'CUS' ! Customer address
                                                  to:CompanyName     = job:Company_Name
                                                  to:AddressLine1    = job:Address_Line1
                                                  to:AddressLine2    = job:Address_Line2
                                                  to:AddressLine3    = job:Address_Line3
                                                  to:TelephoneNumber = job:Telephone_Number
                                                  to:ContactName     = ''
                                                  to:EmailAddress    = jobe:EndUserEmailAddress
                                              of 'DEL' ! Delivery address
                                                  to:CompanyName     = job:Company_Name_Delivery
                                                  to:AddressLine1    = job:Address_Line1_Delivery
                                                  to:AddressLine2    = job:Address_Line2_Delivery
                                                  to:AddressLine3    = job:Address_Line3_Delivery
                                                  to:TelephoneNumber = job:Telephone_Delivery
                                                  to:ContactName     = ''
                                                  to:EmailAddress    = jobe:EndUserEmailAddress
                                              end ! case
                                              settarget()
                                          end ! if
                                      end ! if
                                  END ! IF
                              END ! IF
                          END ! IF
                          Access:Jobs.Update()
                      end ! if
                      !--------------------------------------------------------------
  
                      Case DespatchType
                      Of 'EXC'
                          Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                          xch:Ref_Number  = job:Exchange_Unit_Number
                          If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                              ! Found
                              Save_xca_ID = Access:EXCHACC.SaveFile()
                              Access:EXCHACC.ClearKey(xca:Ref_Number_Key)
                              xca:Ref_Number = xch:Ref_Number
                              Set(xca:Ref_Number_Key, xca:Ref_Number_Key)
                              Loop
                                  If Access:EXCHACC.NEXT()
                                      Break
                                  End ! If
                                  If xca:Ref_Number <> xch:Ref_Number      |
                                      Then Break   ! End If
                                  End ! If
                                  If tmp:Accessories = ''
                                      tmp:Accessories = Clip(xca:Accessory)
                                  Else ! If tmp:Accessories = ''
                                      tmp:Accessories = Clip(tmp:Accessories) & ', ' & Clip(xca:Accessory)
                                  End ! If tmp:Accessories = ''
  
                              End ! Loop
                              Access:EXCHACC.RestoreFile(Save_xca_ID)
                          Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                          ! Error
  
                          End ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                          tmp:IMEI    = xch:ESN
                      Else
                          tmp:Accessories = ''
                          Save_jac_ID     = Access:JOBACC.SaveFile()
                          Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                          jac:Ref_Number = job:Ref_Number
                          Set(jac:Ref_Number_Key, jac:Ref_Number_Key)
                          Loop
                              If Access:JOBACC.NEXT()
                                  Break
                              End ! If
                              If jac:Ref_Number <> job:Ref_Number      |
                                  Then Break   ! End If
                              End ! If
                              If tmp:Accessories = ''
                                  tmp:Accessories = Clip(jac:Accessory)
                              Else ! If tmp:Accessories = ''
                                  tmp:Accessories = Clip(tmp:Accessories) & ', ' & Clip(jac:Accessory)
                              End ! If tmp:Accessories = ''
                          End ! Loop
                          Access:JOBACC.RestoreFile(Save_jac_ID)
                          tmp:IMEI    = job:ESN
  
                      End ! Case job:Despatch_Type
                      !--------------------------------------------------------------
                      If DespatchType = 'LOA'
  
                          Access:LOAN.Clearkey(loa:Ref_Number_Key)
                          loa:Ref_Number  = job:Loan_Unit_Number
                          If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                              ! Found
                              tmp:IMEI    = loa:ESN
  
                          Else ! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                          ! Error
                          End ! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                      End ! If job:Despatch_Type = 'LOA'
                      !--------------------------------------------------------------
                      If job:Despatch_Type <> ''
                          Despatch_Type_Temp = job:Despatch_Type
                      Else ! If job:Despatch_Type <> ''
                          Despatch_Type_Temp = 'JOB'
                      End ! If job:Despatch_Type <> ''
                      !--------------------------------------------------------------
  
  
                      ! Security Pack Number
                      If glo:WebJob
                          ! Waybill Generation
                          Case glo:Select20
                          Of 'WAYREPRINTJTOARC'
                              tmp:SecurityPackNumber = jobe:InSecurityPackNo
                          Of 'WAYREPRINTJTOCUST'
                              tmp:SecurityPackNumber = jobe:JobSecurityPackNo
                          Of 'WAYREPRINTE'
                              tmp:SecurityPackNumber = jobe:ExcSecurityPackNo
                          Else
                              ! j - change this if it is a despatch from the RRC
                              Case jobe:DespatchType
                              Of 'EXC'
                                  tmp:SecurityPackNumber = jobe:ExcSecurityPackNo
                              Of 'LOA'
                                  tmp:SecurityPackNumber = jobe:LoaSecurityPackNo
                              Of 'JOB'
                                  tmp:SecurityPackNumber = jobe:JobSecurityPackNo
                              Else
                                  tmp:SecurityPackNumber = jobe:InSecurityPackNo
                              End ! Case job:Despatch_Type
                          End ! Case glo:Select21
  
                      Else ! If glo:WebJob
                          ! Waybill Generation
                          Case glo:Select20
                          Of 'WAYREPRINTJTORRC'
                              tmp:SecurityPackNumber = jobe:JobSecurityPackNo
                          Of 'WAYREPRINTE'
                              tmp:SecurityPackNumber = jobe:ExcSecurityPackNo
                          Of 'WAYREPRINTJTOCUST'
                              tmp:SecurityPackNumber = jobe:ExcSecurityPackNo
                          Else
  
                              Case job:Despatch_Type
                              Of 'EXC'
                                  tmp:SecurityPackNumber = jobe:ExcSecurityPackNo
                              Of 'LOA'
                                  tmp:SecurityPackNumber = jobe:LoaSecurityPackNo
                              Else
                                  tmp:SecurityPackNumber = jobe:JobSecurityPackNo
                              End ! Case job:Despatch_Type
                          End ! Case glo:Select21
  
                      End ! If glo:WebJob
                      Print(rpt:Detail)
                  !--------------------------------------------------------------
                  end! if access:jobs.fetch(job:ref_number_key) = Level:Benign
              End! Loop x# = 1 To Records(glo:q_jobnumber)
  
          End ! tmp:WaybillID <> 0
          count_temp = Records(glo:q_jobnumber)
          Print(rpt:totals)
  
          setcursor()
      End ! locNewProcedure = 0
  
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR1:rtn = PDFXTR1.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR1:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

