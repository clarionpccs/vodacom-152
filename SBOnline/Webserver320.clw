

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER320.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER303.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER323.INC'),ONCE        !Req'd for module callout resolution
                     END


FormProcessWIPAudits PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locJobNumber         STRING(30)                            !
locIMEINumber        STRING(30)                            !
locNewLevel          LONG                                  !
                    MAP
GetNewLevel             PROCEDURE()
ProcessJob              PROCEDURE()
                    END ! MAP
FilesOpened     Long
WIPSCAN::State  USHORT
WIPAMF::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormProcessWIPAudits')
  loc:formname = 'FormProcessWIPAudits_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormProcessWIPAudits',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormProcessWIPAudits','')
    p_web._DivHeader('FormProcessWIPAudits',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormProcessWIPAudits',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormProcessWIPAudits',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormProcessWIPAudits',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormProcessWIPAudits',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormProcessWIPAudits',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormProcessWIPAudits',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormProcessWIPAudits',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(WIPSCAN)
  p_web._OpenFile(WIPAMF)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(WIPSCAN)
  p_Web._CloseFile(WIPAMF)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormProcessWIPAudits_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('wim:Ignore_Job_Number',wim:Ignore_Job_Number)
  p_web.SetSessionValue('wim:Ignore_IMEI',wim:Ignore_IMEI)
  p_web.SetSessionValue('locNewLevel',locNewLevel)
  p_web.SetSessionValue('locJobNumber',locJobNumber)
  p_web.SetSessionValue('locIMEINumber',locIMEINumber)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('wim:Ignore_Job_Number')
    wim:Ignore_Job_Number = p_web.GetValue('wim:Ignore_Job_Number')
    p_web.SetSessionValue('wim:Ignore_Job_Number',wim:Ignore_Job_Number)
  End
  if p_web.IfExistsValue('wim:Ignore_IMEI')
    wim:Ignore_IMEI = p_web.GetValue('wim:Ignore_IMEI')
    p_web.SetSessionValue('wim:Ignore_IMEI',wim:Ignore_IMEI)
  End
  if p_web.IfExistsValue('locNewLevel')
    locNewLevel = p_web.GetValue('locNewLevel')
    p_web.SetSessionValue('locNewLevel',locNewLevel)
  End
  if p_web.IfExistsValue('locJobNumber')
    locJobNumber = p_web.GetValue('locJobNumber')
    p_web.SetSessionValue('locJobNumber',locJobNumber)
  End
  if p_web.IfExistsValue('locIMEINumber')
    locIMEINumber = p_web.GetValue('locIMEINumber')
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormProcessWIPAudits_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    IF (p_web.IfExistsValue('NewAudit'))
        IF (Access:WIPAMF.PrimeRecord() = Level:Benign)
            wim:Date = TODAY()
            wim:Time = CLOCK()
            wim:User = p_web.GSV('BookingUserCode')
            wim:Complete_Flag = 0
            wim:Status = 'AUDIT IN PROGRESS'
            wim:Site_Location = p_web.GSV('BookingSiteLocation')
            IF (Access:WIPAMF.TryInsert())
                Access:WIPAMF.CancelAutoInc()
            END ! IF
            
        END ! IF
    ELSE
        IF (p_web.IfExistsValue('AuditNo'))
            Access:WIPAMF.ClearKey(wim:Audit_Number_Key)
            wim:Audit_Number = p_web.GetValue('AuditNo')
            IF (Access:WIPAMF.TryFetch(wim:Audit_Number_Key) = Level:Benign)
            END ! IF
        END ! IF
        
    END ! IF
    
    p_web.FileToSessionQueue(WIPAMF)
    
    p_web.SSV('ReadOnly:IgnoreJobNumber',0)
    p_web.SSV('ReadOnly:IgnoreIMEINumber',0)
    
    IF (p_web.GSV('wim:Status') = 'SUSPENDED')
        p_web.SSV('Hide:CompleteAudit',1)
        p_web.SSV('Hide:ContinueAudit',0)
        p_web.SSV('hide:SuspendAudit',1)
        p_web.SSV('Hide:FinishScan',1)
        p_web.SSV('ReadOnly:IgnoreJobNumber',1)
        p_web.SSV('ReadOnly:IgnoreIMEINumber',1)        
    ELSE
        IF (p_web.GSV('wim:User') <> p_web.GSV('BookingUSerCode'))
            p_web.SSV('Hide:CompleteAudit',1)
            p_web.SSV('Hide:ContinueAudit',1)
            p_web.SSV('hide:SuspendAudit',1)
            p_web.SSV('Hide:FinishScan',0)
            p_web.SSV('ReadOnly:IgnoreJobNumber',1)
            p_web.SSV('ReadOnly:IgnoreIMEINumber',1)
        ELSE
            p_web.SSV('Hide:CompleteAudit',0)
            p_web.SSV('Hide:ContinueAudit',1)
            p_web.SSV('hide:SuspendAudit',0)
            p_web.SSV('Hide:FinishScan',0)    
        END ! IF
        
    END ! IF
    
    GetNewLevel()
    
    
    
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locNewLevel = p_web.RestoreValue('locNewLevel')
 locJobNumber = p_web.RestoreValue('locJobNumber')
 locIMEINumber = p_web.RestoreValue('locIMEINumber')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormProcessWIPAudits')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormProcessWIPAudits_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormProcessWIPAudits_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormProcessWIPAudits_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormProcessWIPAudits" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormProcessWIPAudits" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormProcessWIPAudits" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('WIP Audit Procedure') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('WIP Audit Procedure',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormProcessWIPAudits">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormProcessWIPAudits" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormProcessWIPAudits')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Processing') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Actions') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormProcessWIPAudits')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormProcessWIPAudits'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.wim:Ignore_Job_Number')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormProcessWIPAudits')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
    do SendPacket
    Do spacer
    do SendPacket
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Processing') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormProcessWIPAudits_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Processing')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Processing')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Processing')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Processing')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::wim:Ignore_Job_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::wim:Ignore_Job_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::wim:Ignore_Job_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::wim:Ignore_IMEI
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::wim:Ignore_IMEI
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::wim:Ignore_IMEI
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locNewLevel
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locNewLevel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locNewLevel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&2&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::txt
      do Comment::txt
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::___line
      do Comment::___line
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td rowspan="6" colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwProcessed
      do Comment::brwProcessed
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Actions') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormProcessWIPAudits_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnCompleteAudit
      do Comment::btnCompleteAudit
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnSuspendAudit
      do Comment::btnSuspendAudit
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnContinueAudit
      do Comment::btnContinueAudit
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnFinishScan
      do Comment::btnFinishScan
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::wim:Ignore_Job_Number  Routine
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('wim:Ignore_Job_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Ignore Job Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormProcessWIPAudits_' & p_web._nocolon('wim:Ignore_Job_Number') & '_prompt')

Validate::wim:Ignore_Job_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('wim:Ignore_Job_Number',p_web.GetValue('NewValue'))
    wim:Ignore_Job_Number = p_web.GetValue('NewValue') !FieldType= BYTE Field = wim:Ignore_Job_Number
    do Value::wim:Ignore_Job_Number
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('wim:Ignore_Job_Number',p_web.GetValue('Value'))
    wim:Ignore_Job_Number = p_web.GetValue('Value')
  End
    ! Save State Straight Away
    Access:WIPAMF.ClearKey(wim:Audit_Number_Key)
    wim:Audit_Number = p_web.GSV('wim:Audit_Number')
    IF (Access:WIPAMF.TryFetch(wim:Audit_Number_Key) = Level:Benign)
        p_web.SessionQueueToFile(WIPAMF)
        Access:WIPAMF.TryUpdate()
    ELSE ! IF
    END ! IF
  do Value::wim:Ignore_Job_Number
  do SendAlert
  do Prompt::locJobNumber
  do Value::locJobNumber  !1
  do Value::brwProcessed  !1

Value::wim:Ignore_Job_Number  Routine
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('wim:Ignore_Job_Number') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- wim:Ignore_Job_Number
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''wim:Ignore_Job_Number'',''formprocesswipaudits_wim:ignore_job_number_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('wim:Ignore_Job_Number')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:IgnoreJobNumber') = 1,'disabled','')
  If p_web.GetSessionValue('wim:Ignore_Job_Number') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','wim:Ignore_Job_Number',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormProcessWIPAudits_' & p_web._nocolon('wim:Ignore_Job_Number') & '_value')

Comment::wim:Ignore_Job_Number  Routine
    loc:comment = ''
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('wim:Ignore_Job_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::wim:Ignore_IMEI  Routine
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('wim:Ignore_IMEI') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Ignore IMEI Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormProcessWIPAudits_' & p_web._nocolon('wim:Ignore_IMEI') & '_prompt')

Validate::wim:Ignore_IMEI  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('wim:Ignore_IMEI',p_web.GetValue('NewValue'))
    wim:Ignore_IMEI = p_web.GetValue('NewValue') !FieldType= BYTE Field = wim:Ignore_IMEI
    do Value::wim:Ignore_IMEI
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('wim:Ignore_IMEI',p_web.GetValue('Value'))
    wim:Ignore_IMEI = p_web.GetValue('Value')
  End
    ! Save State Straight Away
    Access:WIPAMF.ClearKey(wim:Audit_Number_Key)
    wim:Audit_Number = p_web.GSV('wim:Audit_Number')
    IF (Access:WIPAMF.TryFetch(wim:Audit_Number_Key) = Level:Benign)
        p_web.SessionQueueToFile(WIPAMF)
        Access:WIPAMF.TryUpdate()
    ELSE ! IF
    END ! IF
  do Value::wim:Ignore_IMEI
  do SendAlert
  do Prompt::locIMEINumber
  do Value::locIMEINumber  !1
  do Value::brwProcessed  !1

Value::wim:Ignore_IMEI  Routine
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('wim:Ignore_IMEI') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- wim:Ignore_IMEI
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''wim:Ignore_IMEI'',''formprocesswipaudits_wim:ignore_imei_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('wim:Ignore_IMEI')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:IgnoreIMEINumber') = 1,'disabled','')
  If p_web.GetSessionValue('wim:Ignore_IMEI') = true
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','wim:Ignore_IMEI',clip(true),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormProcessWIPAudits_' & p_web._nocolon('wim:Ignore_IMEI') & '_value')

Comment::wim:Ignore_IMEI  Routine
    loc:comment = ''
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('wim:Ignore_IMEI') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locNewLevel  Routine
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('locNewLevel') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Counted:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormProcessWIPAudits_' & p_web._nocolon('locNewLevel') & '_prompt')

Validate::locNewLevel  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locNewLevel',p_web.GetValue('NewValue'))
    locNewLevel = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locNewLevel
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locNewLevel',p_web.GetValue('Value'))
    locNewLevel = p_web.GetValue('Value')
  End

Value::locNewLevel  Routine
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('locNewLevel') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locNewLevel
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('bold green LargeText')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locNewLevel'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormProcessWIPAudits_' & p_web._nocolon('locNewLevel') & '_value')

Comment::locNewLevel  Routine
    loc:comment = ''
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('locNewLevel') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locJobNumber  Routine
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('locJobNumber') & '_prompt',Choose(p_web.GSV('wim:Ignore_Job_Number') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Job Number')
  If p_web.GSV('wim:Ignore_Job_Number') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormProcessWIPAudits_' & p_web._nocolon('locJobNumber') & '_prompt')

Validate::locJobNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobNumber',p_web.GetValue('NewValue'))
    locJobNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobNumber',p_web.GetValue('Value'))
    locJobNumber = p_web.GetValue('Value')
  End
    IF (p_web.GSV('wim:Ignore_IMEI') = 1)
        ProcessJob()
    END ! IF
  do Value::locJobNumber
  do SendAlert
  do Value::locIMEINumber  !1
  do Value::brwProcessed  !1
  do Prompt::locNewLevel
  do Value::locNewLevel  !1

Value::locJobNumber  Routine
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('locJobNumber') & '_value',Choose(p_web.GSV('wim:Ignore_Job_Number') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('wim:Ignore_Job_Number') = 1)
  ! --- STRING --- locJobNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('wim:Status') = 'SUSPENDED','readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('wim:Status') = 'SUSPENDED'
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('locJobNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobNumber'',''formprocesswipaudits_locjobnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobNumber')&''',2);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locJobNumber',p_web.GetSessionValueFormat('locJobNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormProcessWIPAudits_' & p_web._nocolon('locJobNumber') & '_value')

Comment::locJobNumber  Routine
      loc:comment = ''
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('locJobNumber') & '_comment',Choose(p_web.GSV('wim:Ignore_Job_Number') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('wim:Ignore_Job_Number') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locIMEINumber  Routine
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('locIMEINumber') & '_prompt',Choose(p_web.GSV('wim:Ignore_IMEI') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('IMEI Number')
  If p_web.GSV('wim:Ignore_IMEI') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormProcessWIPAudits_' & p_web._nocolon('locIMEINumber') & '_prompt')

Validate::locIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('NewValue'))
    locIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('Value'))
    locIMEINumber = p_web.GetValue('Value')
  End
    PRocessJob()  
  do Value::locIMEINumber
  do SendAlert
  do Value::locJobNumber  !1
  do Value::brwProcessed  !1
  do Prompt::locNewLevel
  do Value::locNewLevel  !1

Value::locIMEINumber  Routine
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('locIMEINumber') & '_value',Choose(p_web.GSV('wim:Ignore_IMEI') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('wim:Ignore_IMEI') = 1)
  ! --- STRING --- locIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('wim:Status') = 'SUSPENDED','readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('wim:Status') = 'SUSPENDED'
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('locIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locIMEINumber'',''formprocesswipaudits_locimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobNumber')&''',2);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locIMEINumber',p_web.GetSessionValueFormat('locIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormProcessWIPAudits_' & p_web._nocolon('locIMEINumber') & '_value')

Comment::locIMEINumber  Routine
      loc:comment = ''
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('locIMEINumber') & '_comment',Choose(p_web.GSV('wim:Ignore_IMEI') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('wim:Ignore_IMEI') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::txt  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txt',p_web.GetValue('NewValue'))
    do Value::txt
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txt  Routine
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('txt') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.Translate('If scanning only job numbers, the WIP Audit will only run<br/>against the original job status and not the exchange status.',1) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::txt  Routine
    loc:comment = ''
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('txt') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::___line  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('___line',p_web.GetValue('NewValue'))
    do Value::___line
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::___line  Routine
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('___line') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::___line  Routine
    loc:comment = ''
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('___line') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::brwProcessed  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwProcessed',p_web.GetValue('NewValue'))
    do Value::brwProcessed
  Else
    p_web.StoreValue('wia:Internal_No')
  End

Value::brwProcessed  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseProcessWIPAudits --
  p_web.SetValue('BrowseProcessWIPAudits:NoForm',1)
  p_web.SetValue('BrowseProcessWIPAudits:FormName',loc:formname)
  p_web.SetValue('BrowseProcessWIPAudits:parentIs','Form')
  p_web.SetValue('_parentProc','FormProcessWIPAudits')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormProcessWIPAudits_BrowseProcessWIPAudits_embedded_div')&'"><!-- Net:BrowseProcessWIPAudits --></div><13,10>'
    p_web._DivHeader('FormProcessWIPAudits_' & lower('BrowseProcessWIPAudits') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormProcessWIPAudits_' & lower('BrowseProcessWIPAudits') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseProcessWIPAudits --><13,10>'
  end
  do SendPacket

Comment::brwProcessed  Routine
    loc:comment = ''
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('brwProcessed') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnCompleteAudit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnCompleteAudit',p_web.GetValue('NewValue'))
    do Value::btnCompleteAudit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnCompleteAudit  Routine
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('btnCompleteAudit') & '_value',Choose(p_web.GSV('Hide:CompleteAudit') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CompleteAudit') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('wipAuditCompleteConfirm()')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnCompleteAudit','Complete Audit','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormProcessWIPAudits_' & p_web._nocolon('btnCompleteAudit') & '_value')

Comment::btnCompleteAudit  Routine
    loc:comment = ''
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('btnCompleteAudit') & '_comment',Choose(p_web.GSV('Hide:CompleteAudit') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CompleteAudit') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnSuspendAudit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnSuspendAudit',p_web.GetValue('NewValue'))
    do Value::btnSuspendAudit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnSuspendAudit  Routine
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('btnSuspendAudit') & '_value',Choose(p_web.GSV('Hide:SuspendAudit') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:SuspendAudit') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnSuspendAudit','Suspend Audit','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('AuditProcess?' &'ProcessType=WIPSuspendAudit&okURL=FormWIPAudits&errorURL=FormProcessWIPAudits')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormProcessWIPAudits_' & p_web._nocolon('btnSuspendAudit') & '_value')

Comment::btnSuspendAudit  Routine
    loc:comment = ''
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('btnSuspendAudit') & '_comment',Choose(p_web.GSV('Hide:SuspendAudit') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:SuspendAudit') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnContinueAudit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnContinueAudit',p_web.GetValue('NewValue'))
    do Value::btnContinueAudit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    Access:WIPAMF.ClearKey(wim:Audit_Number_Key)
    wim:Audit_Number = p_web.GSV('wim:Audit_Number')
    IF (Access:WIPAMF.TryFetch(wim:Audit_Number_Key) = Level:Benign)
        wim:Status = 'AUDIT IN PROGRESS'
        Access:WIPAMF.TryUpdate()
        p_web.SSV('wim:Status',wim:Status)
    END ! IF
  
    IF (p_web.GSV('wim:User') = p_web.GSV('BookingUserCode'))
        p_web.SSV('Hide:SuspendAudit',0)
        p_web.SSV('Hide:ContinueAudit',1)
        p_web.SSV('Hide:FinishScan',0)
        p_web.SSV('Hide:CompleteAudit',0)
        p_web.SSV('ReadOnly:IgnoreJobNumber',0)
        p_web.SSV('ReadOnly:IgnoreIMEINumber',0)
    ELSE
        p_web.SSV('Hide:SuspendAudit',1)
        p_web.SSV('Hide:ContinueAudit',1)
        p_web.SSV('Hide:FinishScan',0)
        p_web.SSV('Hide:CompleteAudit',1)
        p_web.SSV('ReadOnly:IgnoreJobNumber',1)
        p_web.SSV('ReadOnly:IgnoreIMEINumber',1)
    END ! IF
  
        
  do SendAlert
  do Value::btnCompleteAudit  !1
  do Value::btnContinueAudit  !1
  do Value::btnFinishScan  !1
  do Value::btnSuspendAudit  !1
  do Prompt::locIMEINumber
  do Value::locIMEINumber  !1
  do Prompt::locJobNumber
  do Value::locJobNumber  !1
  do Prompt::wim:Ignore_IMEI
  do Value::wim:Ignore_IMEI  !1
  do Prompt::wim:Ignore_Job_Number
  do Value::wim:Ignore_Job_Number  !1

Value::btnContinueAudit  Routine
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('btnContinueAudit') & '_value',Choose(p_web.GSV('Hide:ContinueAudit') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ContinueAudit') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnContinueAudit'',''formprocesswipaudits_btncontinueaudit_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnContinueAudit','Continue Audit','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormProcessWIPAudits_' & p_web._nocolon('btnContinueAudit') & '_value')

Comment::btnContinueAudit  Routine
    loc:comment = ''
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('btnContinueAudit') & '_comment',Choose(p_web.GSV('Hide:ContinueAudit') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ContinueAudit') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnFinishScan  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnFinishScan',p_web.GetValue('NewValue'))
    do Value::btnFinishScan
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnFinishScan  Routine
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('btnFinishScan') & '_value',Choose(p_web.GSV('Hide:FinishScan') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:FinishScan') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnFinishScan','Finish Scan','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('AuditProcess?' &'ProcessType=WIPFinishScan&okURL=FormWIPAudits&errorURL=FormProcessWIPAudits')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormProcessWIPAudits_' & p_web._nocolon('btnFinishScan') & '_value')

Comment::btnFinishScan  Routine
    loc:comment = ''
  p_web._DivHeader('FormProcessWIPAudits_' & p_web._nocolon('btnFinishScan') & '_comment',Choose(p_web.GSV('Hide:FinishScan') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:FinishScan') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormProcessWIPAudits_wim:Ignore_Job_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::wim:Ignore_Job_Number
      else
        do Value::wim:Ignore_Job_Number
      end
  of lower('FormProcessWIPAudits_wim:Ignore_IMEI_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::wim:Ignore_IMEI
      else
        do Value::wim:Ignore_IMEI
      end
  of lower('FormProcessWIPAudits_locJobNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobNumber
      else
        do Value::locJobNumber
      end
  of lower('FormProcessWIPAudits_locIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIMEINumber
      else
        do Value::locIMEINumber
      end
  of lower('FormProcessWIPAudits_btnContinueAudit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnContinueAudit
      else
        do Value::btnContinueAudit
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormProcessWIPAudits_form:ready_',1)
  p_web.SetSessionValue('FormProcessWIPAudits_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormProcessWIPAudits',0)

PreCopy  Routine
  p_web.SetValue('FormProcessWIPAudits_form:ready_',1)
  p_web.SetSessionValue('FormProcessWIPAudits_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormProcessWIPAudits',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormProcessWIPAudits_form:ready_',1)
  p_web.SetSessionValue('FormProcessWIPAudits_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormProcessWIPAudits:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormProcessWIPAudits_form:ready_',1)
  p_web.SetSessionValue('FormProcessWIPAudits_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormProcessWIPAudits:Primed',0)
  p_web.setsessionvalue('showtab_FormProcessWIPAudits',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
        If (p_web.GSV('ReadOnly:IgnoreJobNumber') = 1)
          If p_web.IfExistsValue('wim:Ignore_Job_Number') = 0
            p_web.SetValue('wim:Ignore_Job_Number',0)
            wim:Ignore_Job_Number = 0
          End
        End
        If (p_web.GSV('ReadOnly:IgnoreIMEINumber') = 1)
          If p_web.IfExistsValue('wim:Ignore_IMEI') = 0
            p_web.SetValue('wim:Ignore_IMEI',false)
            wim:Ignore_IMEI = false
          End
        End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormProcessWIPAudits_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormProcessWIPAudits_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormProcessWIPAudits:Primed',0)
  p_web.StoreValue('locNewLevel')
  p_web.StoreValue('locJobNumber')
  p_web.StoreValue('locIMEINumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
spacer  Routine
  packet = clip(packet) & |
    '<<br/><13,10>'&|
    '<<br/><13,10>'&|
    '<<br/><13,10>'&|
    '<<br/><13,10>'&|
    ''
GetNewLevel         PROCEDURE()
newLevel    LONG()
    CODE
        newLevel = 0
        Access:WIPAUI.ClearKey(wia:Locate_IMEI_Key)
        wia:Audit_Number = p_web.GSV('wim:Audit_Number')
        wia:Site_Location = p_web.GSV('BookingSiteLocation')
        SET(wia:Locate_IMEI_Key,wia:Locate_IMEI_Key)
        LOOP UNTIL Access:WIPAUI.Next() <> Level:Benign
            IF (wia:Audit_Number <> p_web.GSV('wim:Audit_Number') OR |
                wia:Site_Location <> p_web.GSV('BookingSiteLocation'))
                BREAK
            END ! IF
            newLevel += 1
        END ! LOOP

        p_web.SSV('locNewLevel',newLevel)
ProcessJob          PROCEDURE()
invalidIMEI             LONG(0)
isExchange              LONG(0)
lastJobNumber           LONG(0)
theStatus               STRING(30)
    CODE
        IF (p_web.GSV('wim:Ignore_Job_Number') = 1)
            Access:JOBS.ClearKey(job:ESN_Key)
            job:ESN = p_web.GSV('locIMEINumber')
            SET(job:ESN_Key,job:ESN_Key)
            LOOP UNTIL Access:JOBS.Next() <> Level:Benign
                IF (job:ESN <> p_web.GSV('locIMEINumber'))
                    BREAK
                END ! IF
                IF (job:Ref_Number > lastJobNumber)
                    lastJobNumber = job:Ref_Number
                END ! IF
            END ! LOOP
            
            
			
            IF (lastJobNumber = 0)
                Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
                xch:ESN = p_web.GSV('locIMEINumber')
                SET(xch:ESN_Only_Key,xch:ESN_Only_Key)
                LOOP UNTIL Access:EXCHANGE.Next() <> Level:Benign
                    IF (xch:ESN <> p_web.GSV('locIMEINumber'))
                        BREAK
                    END ! IF
                    IF (xch:Job_Number = 0)
                        CYCLE
                    END ! IF
                    IF (xch:Job_Number > lastJobNumber)
                        Access:JOBS.ClearKey(job:Ref_Number_Key)
                        job:Ref_Number = xch:Job_Number
                        IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                            lastJobNumber = xch:Job_Number
                        END ! IF
                    END ! IF
                END ! LOOP
            END!  IF
            
			
            IF (lastJobNumber = 0)
                loc:alert = 'A job cannot be found matching the entered job number. Please re-check'
                loc:invalid = 'locJobNumber'
                RETURN				
				
            END! IF
            
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = lastJobNumber
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                ! Reget the correct job
            END ! IF
            
        ELSE ! IF
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = p_web.GSV('locJobNumber')
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
				
            ELSE ! IF
                loc:alert = 'A job cannot be found matching the entered job number. Please re-check'
                loc:invalid = 'locJobNumber'
                RETURN
            END ! IF
			
            IF (p_web.GSV('wim:Ignore_IMEI') = 1)
            ELSE ! IF
                IF (job:ESN <> p_web.GSV('locIMEINumber'))
                    invalidIMEI = 1
					! Is this the exchange unit then?
                    IF (job:Exchange_Unit_Number > 0)
                        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                        xch:Ref_Number = job:Exchange_Unit_Number
                        IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                            IF (xch:ESN = p_web.GSV('locIMEINumber'))
                                invalidIMEI = 0
                                isExchange = 1
                            END ! IF
                        ELSE ! IF
                        END ! IF
                    END ! IF
                    IF (invalidIMEI = 0)
                        loc:alert = 'The I.M.E.I. number on the job does not match the entered I.M.E.I. number. Please re-check.'
                        loc:invalid = 'locIMEINumber'
                        RETURN
                    END !IF
                ELSE ! IF
                END ! IF
            END ! IF
			
        END ! IF
        
        
        
		
		! OK We are here then we have found a job
		! Check we haven't already audited the job
        Access:WIPAUI.ClearKey(wia:AuditRefNumberKey)
        wia:Audit_Number = p_web.GSV('wim:Audit_Number')
        wia:Ref_Number = job:Ref_Number
        IF (Access:WIPAUI.TryFetch(wia:AuditRefNumberKey) = Level:Benign)
            loc:alert = 'This job is already on this audit.'
            loc:invalid = 'locJobNumber'
            RETURN
        END ! IF
		
        IF (Access:WIPAUI.PrimeRecord() = Level:Benign)
            wia:Site_Location = p_web.GSV('BookingSiteLocation')
            IF (isExchange)
                wia:Status = job:Exchange_Status
            ELSE
                wia:Status = job:Current_Status
            END 			
            wia:Audit_Number = p_web.GSV('wim:Audit_Number')
            wia:Ref_Number = job:Ref_Number
            wia:New_In_Status = 0
            wia:Confirmed = 1
            wia:IsExchange = isExchange
            IF (Access:WIPAUI.TryInsert())
                Access:WIPAUI.CancelAutoInc()
            END ! IF
        END ! IF
        
        GetNewLevel()
		
        p_web.SSV('locIMEINumber','')	
        p_web.SSV('locJobNumber','')
        	
        
