

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER200.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! Report the EXCHAUI File
!!! </summary>
ExchangeAuditReport PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
locAuditNumber       LONG                                  !
TheAddress           GROUP,PRE(address)                    !
TheName              STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
Postcode             STRING(30)                            !
Telephone            STRING(30)                            !
Fax                  STRING(30)                            !
EmailAddress         STRING(255)                           !
                     END                                   !
Make_Model           STRING(60)                            !
Total_Audited        LONG                                  !
Percentage_Audited   REAL                                  !
Total_No_Of_Lines    LONG                                  !
Process:View         VIEW(EXCHAUI)
                       PROJECT(eau:Audit_Number)
                       PROJECT(eau:Confirmed)
                       PROJECT(eau:Internal_No)
                       PROJECT(eau:Shelf_Location)
                       PROJECT(eau:Site_Location)
                       PROJECT(eau:Stock_Type)
                     END
ProgressWindow       WINDOW('Report EXCHAUI'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT,AT(396,2802,7521,7573),PRE(RPT),PAPER(PAPER:A4),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,531,7521,1802),USE(?unnamed:2)
                         STRING('EXCHANGE AUDIT REPORT'),AT(4948,573),USE(?String3),FONT('Arial',8,,,CHARSET:ANSI), |
  TRN
                         STRING('Date Printed:'),AT(4948,1302),USE(?String16:5),FONT('Arial',8,,,CHARSET:ANSI),TRN
                         STRING('Page Number:'),AT(4948,1458),USE(?String16:2),FONT('Arial',8,,,CHARSET:ANSI),TRN
                         STRING('This Audit is not fully completed'),AT(83,1531),USE(?StringAuditNotComplete),FONT(, |
  12,COLOR:Black,FONT:bold,CHARSET:ANSI),HIDE,TRN
                         STRING('String 21'),AT(3656,0,3438,260),USE(?String21),FONT('Arial',14,,FONT:bold),RIGHT(10), |
  TRN
                         STRING(@s30),AT(94,52),USE(address:TheName),FONT(,14,,FONT:bold),TRN
                         STRING(@s30),AT(94,313,3531,198),USE(address:AddressLine1),FONT(,9),TRN
                         STRING(@s30),AT(94,469,3531,198),USE(address:AddressLine2),FONT(,9),TRN
                         STRING(@s30),AT(94,625,3531,198),USE(address:AddressLine3),FONT(,9),TRN
                         STRING('<<-- Date Stamp -->'),AT(5802,1302,927,135),USE(?ReportDateStamp:2),FONT('Arial',8, |
  ,FONT:regular),TRN
                         STRING(@s30),AT(94,781),USE(address:Postcode),FONT(,9),TRN
                         STRING('Tel: '),AT(94,990),USE(?String15),FONT(,9),TRN
                         STRING('<<-- Time Stamp -->'),AT(6760,1302,927,135),USE(?ReportTimeStamp:2),FONT('Arial',8, |
  ,FONT:regular),TRN
                         STRING(@s30),AT(563,990),USE(address:Telephone),FONT(,9),TRN
                         STRING(@pPage <<#p),AT(5802,1458,700,135),USE(?PageCount:2),FONT('Arial',8,,FONT:regular), |
  PAGENO,TRN
                         STRING('Fax:'),AT(94,1146),USE(?String16),FONT(,9),TRN
                         STRING(@s30),AT(563,1146),USE(address:Fax),FONT(,9),TRN
                         STRING('Email:'),AT(104,1302),USE(?String16:4),FONT(,9),TRN
                         STRING(@s255),AT(573,1302,3531,198),USE(address:EmailAddress),FONT(,9),TRN
                       END
DETAIL                 DETAIL,AT(0,0,,167),USE(?DetailBand)
                         STRING(@s30),AT(104,10),USE(xch:Stock_Type),FONT('Arial',8)
                         STRING(@s60),AT(2448,10),USE(Make_Model),FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s30),AT(5115,10),USE(xch:ESN),FONT('Arial',8,,,CHARSET:ANSI)
                       END
detail1                DETAIL,AT(0,0,,52),USE(?unnamed:6),PAGEAFTER(-1)
                       END
detail2                DETAIL,AT(0,0,,260),USE(?unnamed:7)
                         STRING('NO ITEMS TO REPORT'),AT(115,42,7094,208),USE(?String25),FONT(,12,,FONT:bold),CENTER, |
  TRN
                       END
                       FOOTER,AT(417,10417,7521,708),USE(?unnamed:3)
                         LINE,AT(115,42,2146,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING(@n-10),AT(1615,260),USE(Total_Audited),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),RIGHT(1), |
  HIDE
                         STRING('Stock Items Audited:'),AT(104,260),USE(?String26),FONT('Arial',8,,FONT:bold),HIDE, |
  TRN
                         STRING(@n-10.2),AT(1615,417),USE(Percentage_Audited),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  RIGHT(1),HIDE
                         STRING('%'),AT(2240,417),USE(?String30),FONT('Arial',8),HIDE,TRN
                         STRING('Percentage Audited:'),AT(104,417),USE(?String27),FONT('Arial',8,,FONT:bold),HIDE,TRN
                         STRING('Total Number Of Lines:'),AT(104,104),USE(?String29),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING(@n-10),AT(1625,104),USE(Total_No_Of_Lines),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  RIGHT(1),TRN
                       END
                       FORM,AT(365,510,7521,10802),USE(?unnamed)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,10802),USE(?Image1)
                         STRING('Stock Type'),AT(135,1979),USE(?String5),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Make/Model'),AT(2469,1979),USE(?String6),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('IMEI'),AT(5135,1979),USE(?String31),FONT('Arial',8,,FONT:bold),TRN
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR3               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
EndReport              PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.EndReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
        IF (Total_No_Of_Lines = 0)
            IF (p_web.GSV('ShortagePage') = 1)
  		! There was no shortages or excess?!
                PRINT(rpt:Detail2)
            END ! IF
            SETTARGET(REPORT)
            ?string21{prop:Text} = 'EXCESSES'
            SETTARGET()
            PRINT(rpt:Detail2)
        ELSE ! IF
            IF (p_web.GSV('ShortagePage') = 1)
  		! There was no excess
                PRINT(rpt:Detail1)
                SETTARGET(REPORT)
                ?string21{prop:Text} = 'EXCESSES'
                SETTARGET()
                PRINT(rpt:Detail2)
            ELSE
                PRINT(rpt:Detail1)
            END ! IF
        END ! IF
  ReturnValue = PARENT.EndReport()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ExchangeAuditReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:EXCHAMF.SetOpenRelated()
  Relate:EXCHAMF.Open                                      ! File EXCHAMF used by this procedure, so make sure it's RelationManager is open
  Relate:TRADEACC.SetOpenRelated()
  Relate:TRADEACC.Open                                     ! File TRADEACC used by this procedure, so make sure it's RelationManager is open
  Access:EXCHANGE.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
        IF (p_web.IfExistsValue('emf:Audit_Number'))
            Access:EXCHAMF.ClearKey(emf:Audit_Number_Key)
            emf:Audit_Number = p_web.GetValue('emf:Audit_Number')
            IF (Access:EXCHAMF.TryFetch(emf:Audit_Number_Key) = Level:User)
                p_web.FileToSessionQueue(EXCHAMF)
            END ! IF
        END !  IF
        locAuditNumber = p_web.GSV('emf:Audit_Number')  
  
        p_web.SSV('ShortagePage',1)
        
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('BookingAccount')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
        END !I F
        address:TheName           = tra:Company_Name
        address:AddressLine1       = tra:Address_Line1
        address:AddressLine2       = tra:Address_Line2
        address:AddressLine3       = tra:Address_Line3
        address:Postcode            = tra:Postcode
        address:Telephone    = tra:Telephone_Number
        address:Fax          = tra:Fax_Number
        address:EmailAddress        = tra:EmailAddress
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('ExchangeAuditReport',ProgressWindow)       ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:EXCHAUI, ?Progress:PctText, Progress:Thermometer, ProgressMgr, eau:Confirmed)
  ThisReport.AddSortOrder(eau:Main_Browse_Key)
  ThisReport.AddRange(eau:Audit_Number,locAuditNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:EXCHAUI.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:EXCHAMF.Close
    Relate:TRADEACC.Close
  END
  IF SELF.Opened
    INIMgr.Update('ExchangeAuditReport',ProgressWindow)    ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'ExchangeAuditReport',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
        SETTARGET(REPORT)
        ?String21{PROP:Text} = 'SHORTAGES'
        
        IF (p_web.GSV('emf:Complete_Flag') = 0)
            ?StringAuditNotComplete{PROP:Hide} = 0
        END ! IF
        
        SETTARGET()  
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.Init(Report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,Report{PROPPRINT:Paper},PAPER:USER), CHOOSE(Report{PROP:Thous}=True,PROP:Thous,CHOOSE(Report{PROP:MM}=True,PROP:MM,CHOOSE(Report{PROP:Points}=True,PROP:Points,0))), Report{PROPPRINT:PaperWidth}, Report{PROPPRINT:PaperHeight}, Report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetTitle('Exchange Audit Report')    !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR3.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetViewerPrefs(PDFXTR3:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp:2{PROP:Text} = FORMAT(TODAY(),@d06)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp:2{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'ExchangeAuditReport',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR3.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
        IF (eau:Confirmed = 1)
  	! Excess Page
            IF (p_web.GSV('ShortagePage') = 1)
  		! First Excess On A Shortage Page
                IF (Total_No_Of_Lines = 0)
  			! There were no shortages
                    PRINT(rpt:Detail2)
                ELSE
                    PRINT(rpt:Detail1)
                    Total_No_Of_Lines = 0
                    SETTARGET(REPORT)
                    ?string21{prop:Text} = 'EXCESSES'
                    SETTARGET()
                END ! IF
            END ! IF
            p_web.SSV('ShortagePage',0)	
            IF (eau:New_IMEI <> 1)
                RETURN Level:User
            END ! IF
        END ! IF
        
        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
        xch:Ref_Number = eau:Ref_Number
        IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
            RETURN Level:User
        END ! IF
        
        Make_Model = CLIP(xch:Manufacturer) & '-' & CLIP(xch:Model_Number)
        Total_No_Of_Lines += 1
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR3:rtn = PDFXTR3.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR3:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

