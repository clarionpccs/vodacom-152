

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER157.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER029.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER042.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER111.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER136.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER142.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER159.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER161.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER169.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER170.INC'),ONCE        !Req'd for module callout resolution
                     END


JobFaultCodes        PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:FaultCode1       STRING(30)                            !
tmp:FaultCode2       STRING(30)                            !
tmp:FaultCode3       STRING(30)                            !
tmp:FaultCode4       STRING(30)                            !
tmp:FaultCode5       STRING(30)                            !
tmp:FaultCode6       STRING(30)                            !
tmp:FaultCode7       STRING(30)                            !
tmp:FaultCode8       STRING(30)                            !
tmp:FaultCode9       STRING(30)                            !
tmp:FaultCode10      STRING(30)                            !
tmp:FaultCode11      STRING(30)                            !
tmp:FaultCode12      STRING(30)                            !
tmp:FaultCode13      STRING(30)                            !
tmp:FaultCode14      STRING(30)                            !
tmp:FaultCode15      STRING(30)                            !
tmp:FaultCode16      STRING(30)                            !
tmp:FaultCode17      STRING(30)                            !
tmp:FaultCode18      STRING(30)                            !
tmp:FaultCode19      STRING(30)                            !
tmp:FaultCode20      STRING(30)                            !
tmp:MSN              STRING(30)                            !
tmp:VerifyMSN        STRING(30)                            !
tmp:ConfirmMSNChange BYTE                                  !
tmp:processExchange  BYTE                                  !
ReturnURL            STRING(50)                            !Used to say where to return on finish
FilesOpened     Long
AUDIT::State  USHORT
CHARTYPE::State  USHORT
REPTYDEF::State  USHORT
MANFAULO_ALIAS::State  USHORT
JOBSE3::State  USHORT
MANFAULO::State  USHORT
JOBOUTFL::State  USHORT
MANFAULT_ALIAS::State  USHORT
MANFAULT::State  USHORT
WARPARTS::State  USHORT
STOCK::State  USHORT
STOMODEL::State  USHORT
STOMJFAU::State  USHORT
MANFAUPA::State  USHORT
MANFPALO::State  USHORT
TRADEACC::State  USHORT
LOCATION::State  USHORT
MANUFACT::State  USHORT
JOBS::State  USHORT
MODPROD::State  USHORT
SBO_OutFaultParts::State  USHORT
PARTS::State  USHORT
USERS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
local       class
AllocateExchangePart      Procedure(String    func:Type,Byte  func:Allocated,Byte func:SecondUnit)
PartForceFaultCode    Procedure(Long f:PartFieldNumber,String f:FaultCode, Long f:JobFieldNumber ),Byte
AfterFaultCodeLookup        Procedure(Long fNumber)
SetLookupButton      Procedure(Long fNumber)
            end
  CODE
    ReturnURL = p_web.gsv('ReturnURL')  !has to be set on any routine calling this VIEWJOB and QAProcedure so far
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('JobFaultCodes')
  loc:formname = 'JobFaultCodes_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'JobFaultCodes',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('JobFaultCodes','')
    p_web._DivHeader('JobFaultCodes',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferJobFaultCodes',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobFaultCodes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobFaultCodes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_JobFaultCodes',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobFaultCodes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_JobFaultCodes',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'JobFaultCodes',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
CheckStockModelFaultCodes        Routine
    Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
    wpr:Ref_Number = p_web.GSV('job:Ref_Number')
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.Next()
            Break
        End ! If Access:WARPARTS.Next()
        If wpr:Ref_Number <> p_web.GSV('job:Ref_Number')
            Break
        End ! If wpr:Ref_Number <> job:Ref_Number

        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number = wpr:Part_Ref_Number
        If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
            If sto:Assign_Fault_Codes = 'YES'
                Access:STOMODEL.Clearkey(stm:Mode_Number_Only_Key)
                stm:Ref_Number = sto:Ref_Number
                stm:Model_NUmber = p_web.GSV('job:Ref_Number')
                If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                    Access:STOMJFAU.Clearkey(stj:FieldKey)
                    stj:RefNumber = stm:RecordNumber
                    If Access:STOMJFAU.TryFetch(stj:FieldKey) = Level:Benign
                        Case maf:Field_Number
                        Of 1
                            If stj:FaultCode1 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 30/10/2007)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 30/10/2007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode1
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 30/10/2007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 30/10/2007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 30/10/2007)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode1)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode1)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode1 <> ''
                        Of 2
                            If stj:FaultCode2 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 30/20/2007)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 30/20/2007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode2
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 30/20/2007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 30/20/2007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 30/20/2007)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode2)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode2)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode2 <> ''

                        Of 3
                            If stj:FaultCode3 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 30/30/3007)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 30/30/3007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode3
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 30/30/3007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 30/30/3007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 30/30/3007)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode3)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode3)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode3 <> ''

                        Of 4
                            If stj:FaultCode4 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 40/40/4007)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 40/40/4007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode4
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 40/40/4007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 40/40/4007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 40/40/4007)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode4)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode4)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode4 <> ''

                        Of 5
                            If stj:FaultCode5 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 50/50/5007)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 50/50/5007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode5
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 50/50/5007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 50/50/5007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 50/50/5007)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode5)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode5)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode5 <> ''

                        Of 6
                            If stj:FaultCode6 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 60/60/6007)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 60/60/6007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode6
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 60/60/6007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 60/60/6007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 60/60/6007)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode6)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode6)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode6 <> ''

                        Of 7
                            If stj:FaultCode7 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 70/70/7007)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 70/70/7007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode7
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 70/70/7007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 70/70/7007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 70/70/7007)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode7)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode7)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode7 <> ''

                        Of 8
                            If stj:FaultCode8 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 80/80/8008)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 80/80/8008)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode8
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 80/80/8008)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 80/80/8008)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 80/80/8008)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode8)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode8)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode8 <> ''

                        Of 9
                            If stj:FaultCode9 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 90/90/9009)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 90/90/9009)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode9
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 90/90/9009)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 90/90/9009)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 90/90/9009)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode9)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode9)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode9 <> ''

                        Of 10
                            If stj:FaultCode10 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 100/100/100010)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 100/100/100010)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode10
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 100/100/100010)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 100/100/100010)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 100/100/100010)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode10)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode10)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode10 <> ''

                        Of 11
                            If stj:FaultCode11 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 110/110/110011)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 110/110/110011)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode11
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 110/110/110011)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 110/110/110011)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 110/110/110011)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode11)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode11)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode11 <> ''

                        Of 12
                            If stj:FaultCode12 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 120/120/120012)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 120/120/120012)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode12
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 120/120/120012)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 120/120/120012)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 120/120/120012)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode12)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode12)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode12 <> ''

                        Of 13
                            If stj:FaultCode13 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 130/130/130013)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 130/130/130013)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode13
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 130/130/130013)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 130/130/130013)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 130/130/130013)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode13)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode13)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode13 <> ''

                        Of 14
                            If stj:FaultCode14 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 140/140/140014)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 140/140/140014)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode14
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 140/140/140014)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 140/140/140014)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 140/140/140014)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode14)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode14)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode14 <> ''

                        Of 15
                            If stj:FaultCode15 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 150/150/150015)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 150/150/150015)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode15
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 150/150/150015)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 150/150/150015)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 150/150/150015)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode15)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode15)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode15 <> ''

                        Of 16
                            If stj:FaultCode16 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 160/160/160016)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 160/160/160016)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode16
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 160/160/160016)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 160/160/160016)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 160/160/160016)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode16)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode16)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode16 <> ''

                        Of 17
                            If stj:FaultCode17 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 170/170/170017)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 170/170/170017)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode17
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 170/170/170017)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 170/170/170017)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 170/170/170017)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode17)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode17)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode17 <> ''

                        Of 18
                            If stj:FaultCode18 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 180/180/180018)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 180/180/180018)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode18
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 180/180/180018)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 180/180/180018)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 180/180/180018)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode18)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode18)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode18 <> ''

                        Of 19
                            If stj:FaultCode19 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 190/190/190019)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 190/190/190019)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode19
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 190/190/190019)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 190/190/190019)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 190/190/190019)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode19)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode19)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode19 <> ''

                        Of 20
                            If stj:FaultCode20 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 200/200/200020)
                                If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 200/200/200020)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode20
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 200/200/200020)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = p_web.GSV('tmp:faultCode' & maf:ScreenOrder)
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 200/200/200020)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 200/200/200020)
                                    If NewIndex# > CurrentIndex#
                                        p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode20)
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                                    p_web.SSV('tmp:faultCode' & maf:ScreenOrder,stj:FaultCode20)
                                End ! If p_web.GSV('tmp:faultCode' & maf:ScreenOrder) <> ''
                            End ! If stj:FaultCode20 <> ''

                        End ! Case maf:Field_Number
                    End ! If Access:STOMJFAU.TryFetch(stj:FieldKey) = Level:Benign
                End ! If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
            End ! If sto:Assign_Fault_Codes = 'YES'
        End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
    End ! Loop
buildFaultCodes     routine
    data
locRequired     byte(0)
locFaultRequired        byte(0)
locChargeable   byte(0)
locWarranty     byte(0)
locHide byte(0)
locReadOnly     byte(0)
locViewUnavailable      byte(0)
locAmendUnavailable     byte(0)
locField        String(30)
locRepairIndex  Long()
locSetFaultCode String(30)
    code
        if (p_web.GSV('JobFaultCodes:FirstTime') = 0)
            loop x# = 1 to 20
            !p_web.SSV('tmp:FaultCode' & x#,'')
                p_web.SSV('Hide:JobFaultCode' & x#,1)
                p_web.SSV('Req:JobFaultCode' & x#,0)
                p_web.SSV('ReadOnly:JobFaultCode' & x#,0)
                p_web.SSV('Prompt:JobFaultCode' & x#,'Fault Code ' & x#)
                p_web.SSV('Picture:JobFaultCode' & x#,'@s30')
                p_web.SSV('ShowDate:JobFaultCode' & x#,0)
                p_web.SSV('Lookup:JobFaultCode' & x#,0)
                p_web.SSV('Comment:JobFaultCode' & x#,'')
            end ! loop x# = 1 to 20
        end ! if (p_web.GSV('JobFaultCodes:FirstTime') = 0)

        if (p_web.GSV('job:Chargeable_Job') = 'YES')
            locChargeable = 1
        end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
        if (p_web.GSV('job:Warranty_Job') = 'YES')
            locWarranty = 1
        end ! if (p_web.GSV('job:Warranty_Job') = 'YES')

        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number    = p_web.GSV('wob:HeadACcountNumber')
        if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            ! Found
            Access:LOCATION.Clearkey(loc:Location_Key)
            loc:Location    = tra:SiteLocation
            if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
                ! Found
            else ! if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
                ! Error
            end ! if (Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign)
        else ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            ! Error
        end ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)

        if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'VIEW UNAVAILABLE FAULT CODES') = 0)
            locViewUnavailable = 1
            if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'AMEND UNAVAILABLE FAULT CODES') = 0)
                locAmendUnavailable = 1
            end ! if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'AMEND UNAVAILABLE FAULT CODES') = 0)
        end ! if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'VIEW UNAVAILABLE FAULT CODES') = 0)

        if (locChargeable = 1)
            Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
            cha:Charge_Type    = p_web.GSV('job:Charge_Type')
            if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
                ! Found
                if (cha:Force_Warranty = 'YES')
                    if (p_web.GSV('job:Repair_Type') <> '')
                        Access:REPTYDEF.Clearkey(rtd:ChaManRepairTypeKey)
                        rtd:Manufacturer    = p_web.GSV('job:Manufacturer')
                        rtd:Chargeable    = 'YES'
                        rtd:Repair_Type    = p_web.GSV('job:Repair_Type')
                        if (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
                            ! Found
                            if (rtd:CompFaultCoding = 1)
                                locRequired = 1
                            end ! if (rtd:CompFaultCoding = 1)
                        else ! if (Access:RETYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
                            ! Error
                        end ! if (Access:RETYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
                    else ! if (p_web.GSV('job:Repair_Type') <> '')
                        locRequired = 1
                    end ! if (p_web.GSV('job:Repair_Type') <> '')

                end ! if (cha:Force_Warranty = 'YES')
            else ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
                ! Error
            end ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
        end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
        
        if (locWarranty = 1 and locRequired <> 1)
            Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
            cha:Charge_Type    = p_web.GSV('job:Warranty_Charge_Type')
            if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
                ! Found
                if (cha:Force_Warranty = 'YES')
                    if (p_web.GSV('job:Repair_Type') <> '')
                        Access:REPTYDEF.Clearkey(rtd:WarManRepairTypeKey)
                        rtd:Manufacturer    = p_web.GSV('job:Manufacturer')
                        rtd:Warranty    = 'YES'
                        rtd:Repair_Type    = p_web.GSV('job:Repair_Type_Warranty')
                        if (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
                        ! Found
                            if (rtd:CompFaultCoding = 1)
                                locRequired = 1
                            end ! if (rtd:CompFaultCoding = 1)
                        else ! if (Access:RETYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
                        ! Error
                        end ! if (Access:RETYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)

                    else ! if (p_web.GSV('job:Repair_Type') <> '')
                        locRequired = 1
                    end ! if (p_web.GSV('job:Repair_Type') <> '')
                end !if (cha:Force_Warranty = 'YES')
            else ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
                ! Error
            end ! if (Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign)
        end ! if (p_web.GSV('job:Warranty_Job') = 'YES')

        Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
        maf:Manufacturer    = p_web.GSV('job:Manufacturer')
        maf:ScreenOrder    = 0
        set(maf:ScreenOrderKey,maf:ScreenOrderKey)
        loop
            if (Access:MANFAULT.Next())
                Break
            end ! if (Access:MANFAULT.Next())
            if (maf:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                Break
            end ! if (maf:Manufacturer    <> job:Manufacturer)
            if (maf:ScreenOrder    = 0)
                Cycle
            end ! if (maf:ScreenOrder    <> 0)
            if (maf:MainFault)
                p_web.SSV('ReadOnly:JobFaultCode' & maf:ScreenOrder,1)
                p_web.SSV('Lookup:JobFaultCode' & maf:ScreenOrder,0)
                p_web.SSV('Req:JobFaultCode' & maf:ScreenOrder,0)
                p_web.SSV('Hide:JobFaultCode' & maf:ScreenOrder,0)
                p_web.SSV('Prompt:JobFaultCode' & maf:ScreenOrder,maf:Field_Name)
                Access:MANFAULO.Clearkey(mfo:Field_Key)
                mfo:Manufacturer    = maf:Manufacturer
                mfo:Field_Number    = maf:Field_Number
                mfo:Field    = p_web.GSV('tmp:FaultCode' & maf:ScreenOrder)
                if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                    ! Found
                    p_web.SSV('Comment:JobFaultCode' & maf:ScreenOrder,clip(mfo:Description))
                else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                    ! Error
                    p_web.SSV('Comment:JobFaultCode' & maf:ScreenOrder,'')
                end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)            
                cycle
            end ! if (maf:MainFault)

            locHide = 0
            locReadOnly = 0

            if (locWarranty = 1)
                if (maf:Compulsory_At_Booking = 'YES')
                    if (locRequired)
                        locFaultRequired = 1
                    end ! if (locRequired)
                end ! if (maf:Compulsory_At_Booking = 'YES')
                if (maf:Compulsory = 'YES' and locRequired)
                    locFaultRequired = 1
                end !if (maf:Compulsory = 'YES' and locRequired)
            end ! if (loc:Warranty = 1)

            if (locChargeable = 1)
                if (maf:CharCompulsoryBooking)
                    if (locRequired)
                        locFaultRequired = 1
                    end ! if (locRequird)
                end ! if (maf:CharCompulsoryBooking)
                if (maf:CharCompulsory and locRequired)
                    locFaultRequired = 1
                end ! if (maf:CharCompulsory and locRequired)
            end ! if (locChargeable = 1)

            if (maf:RestrictAvailability)
                If ((maf:RestrictServiceCentre = 1 and ~loc:Level1) Or |
                    (maf:RestrictServiceCentre = 2 and ~loc:Level2) Or |
                    (maf:RestrictServiceCentre = 3 and ~loc:Level3))
                    if (locViewUnavailable = 0)
                        locHide = 1
                    else ! if (locViewUnavailable = 0)
                        if (locAmendUnavailable = 0)
                            locReadOnly = 1
                        end ! if (locAmendUnavailable = 0)
                    end ! if (locViewUnavailable = 0)
                end
            end ! if (maf:RestrictAvailability)

            if (maf:NotAvailable)
                if (locViewUnavailable = 0)
                    locHide = 1
                else ! if (locViewUnavailable = 0)
                    if (locAmendUnavailable = 0)
                        if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
                            locHide = 1
                        else ! if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
                            locReadonly = 1
                        end ! if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
                    end ! if (locAmendUnavailable = 0)
                end ! if (locViewUnavailable = 0)
            end ! if (maf:NotAvailable)

            if (locHide = 0)
                if (maf:HideRelatedCodeIfBlank)
                    Access:MANFAULT_ALIAS.Clearkey(maf_ali:Field_Number_Key)
                    maf_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
                    maf_ali:Field_Number    = maf:Field_Number
                    if (Access:MANFAULT_ALIAS.TryFetch(maf_ali:Field_Number_Key) = Level:Benign)
                    ! Found
                        if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
                            locHide = 1
                        end ! if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
                    else ! if (Access:MANFAULT_ALIAS.TryFetch(maf_ali:Field_Number_Key) = Level:Benign)
                    ! Error
                    end ! if (Access:MANFAULT_ALIAS.TryFetch(maf_ali:Field_Number_Key) = Level:Benign)
                end ! if (maf:HideRelatedCodeIfBlank)
            end ! if (locHide = 0)

            if (p_web.GSV('job:Third_party_Site') <> '')
                if (locRequired = 1)
                    if (maf:NotCompulsoryThirdParty)
                        locRequired = 0
                        if (maf:HideThirdParty)
                            locHide = 1
                        end ! if (maf:HideThirdParty)
                    end ! if (maf:NotCompulsoryThirdParty)
                end ! if (locRequired = 1)
            end ! if (p_web.GSV('job:Third_party_Site') <> '')

            if (locHide = 0)
                p_web.SSV('Hide:JobFaultCode' & maf:ScreenOrder,0)
                p_web.SSV('Prompt:JobFaultCode' & maf:ScreenOrder,maf:Field_Name)

                if (locRequired = 0)
                    Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
                    wpr:Ref_Number    = p_web.GSV('job:Ref_Number')
                    set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                    loop
                        if (Access:WARPARTS.Next())
                            Break
                        end ! if (Access:WARPARTS.Next())
                        if (wpr:Ref_Number    <> p_web.GSV('job:Ref_Number'))
                            Break
                        end ! if (wpr:Ref_Number    <> p_web.GSV('job:Ref_Number'))

                        If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                            locRequired = 1
                            Break
                        End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                        If local.PartForceFaultCode(2,wpr:Fault_Code2,maf:Field_Number)
                            locRequired = 1
                            Break
                        End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                        If local.PartForceFaultCode(3,wpr:Fault_Code3,maf:Field_Number)
                            locRequired = 1
                            Break
                        End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                        If local.PartForceFaultCode(4,wpr:Fault_Code4,maf:Field_Number)
                            locRequired = 1
                            Break
                        End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                        If local.PartForceFaultCode(5,wpr:Fault_Code5,maf:Field_Number)
                            locRequired = 1
                            Break
                        End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                        If local.PartForceFaultCode(6,wpr:Fault_Code6,maf:Field_Number)
                            locRequired = 1
                            Break
                        End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                        If local.PartForceFaultCode(7,wpr:Fault_Code7,maf:Field_Number)
                            locRequired = 1
                            Break
                        End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                        If local.PartForceFaultCode(8,wpr:Fault_Code8,maf:Field_Number)
                            locRequired = 1
                            Break
                        End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                        If local.PartForceFaultCode(9,wpr:Fault_Code9,maf:Field_Number)
                            locRequired = 1
                            Break
                        End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                        If local.PartForceFaultCode(10,wpr:Fault_Code10,maf:Field_Number)
                            locRequired = 1
                            Break
                        End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                        If local.PartForceFaultCode(11,wpr:Fault_Code11,maf:Field_Number)
                            locRequired = 1
                            Break
                        End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                        If local.PartForceFaultCode(12,wpr:Fault_Code12,maf:Field_Number)
                            locRequired = 1
                            Break
                        End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    end ! loop
                end ! if (locRequired = 0)

!            if (locRequired)
!                p_web.SSV('Req:JobFaultCode' & maf:ScreenOrder,1)
!            else ! if (locRequired)
!                p_web.SSV('Req:JobFaultCode' & maf:ScreenOrder,0)
!            end ! if (locRequired)

                case (maf:Field_Type)
                of 'DATE'
                    p_web.SSV('Picture:JobFaultCode' & maf:ScreenOrder,clip(maf:DateType))
                    p_web.SSV('ShowDate:JobFaultCode' & maf:ScreenOrder,1)
                of 'STRING'
                    if (maf:RestrictLength)
                        p_web.SSV('Picture:JobFaultCode' & maf:ScreenOrder,'@s' & maf:LengthTo)
                    else ! if (maf:RestrictLength)
                        if (maf:Field_Number = 10 or maf:Field_Number = 11 or maf:Field_Number = 12)
                            p_web.SSV('Picture:JobFaultCode' & maf:ScreenOrder,'@s255')
                        else ! if (maf:Field_Number = 10 or maf:Field_Number = 11 or maf:Field_Number = 12)
                            p_web.SSV('Picture:JobFaultCode' & maf:ScreenOrder,'@s30')
                        end ! if (maf:Field_Number = 10 or maf:Field_Number = 11 or maf:Field_Number = 12)
                    end ! if (maf:RestrictLength)

                    if (maf:Lookup = 'YES')
                        Access:MANFAULO.Clearkey(mfo:Field_Key)
                        mfo:Manufacturer    = maf:Manufacturer
                        mfo:Field_Number    = maf:Field_Number
                        mfo:Field    = p_web.GSV('tmp:FaultCode' & maf:ScreenOrder)
                        if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Found
                            p_web.SSV('Comment:JobFaultCode' & maf:ScreenOrder,clip(mfo:Description))
                        else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Error
                            p_web.SSV('Comment:JobFaultCode' & maf:ScreenOrder,'')
                        end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        if (maf:Force_Lookup = 'YES')
                            locReadOnly =1
                        end ! if (maf:Force_Lookup = 'YES')
                    else ! if (maf:Lookup = 'YES')
                        p_web.SSV('Comment:JobFaultCode' & maf:ScreenOrder,'')
                    end ! if (maf:Lookup = 'YES')
                of 'NUMBER'
                    if (maf:RestrictLength)
                        p_web.SSV('Picture:JobFaultCode' & maf:ScreenOrder,'@n_' & maf:LengthTo)
                    else ! if (maf:RestrictLength)
                        p_web.SSV('Picture:JobFaultCode' & maf:ScreenOrder,'@n_9')
                    end ! if (maf:RestrictLength)
                end ! case (maf:Field_Type)

                if (maf:GenericFault)
                    if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'GENERIC FAULT - AMEND'))
                        locReadOnly = 1
                    end ! if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'GENERIC FAULT - AMEND'))
                end ! if (maf:GenericFault)

                if (maf:MainFault)
                    locReadOnly = 1
                end ! if (maf:MainFault)

                if (locReadonly)
                    p_web.SSV('ReadOnly:JobFaulCode' & maf:ScreenOrder,1)
                    p_web.SSV('Req:JobFaultCode' & maf:ScreenOrder,0)
                    p_web.SSV('Lookup:JobFaultCode' & maf:ScreenOrder,0)
                end ! if (locReadonly)

                if (maf:Lookup = 'YES')
                    p_web.SSV('Lookup:JobFaultCode' & maf:ScreenOrder,1)
                    if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) <> '' and |
                        p_web.GSV('Hide:JobFaultCode' & maf:ScreenOrder) = 0 and |
                        p_web.GSV('ReadOnly:JobFaultCode' & maf:ScreenOrder) = 0)

                        Access:MANFAULO.Clearkey(mfo:PrimaryLookupKey)
                        mfo:Manufacturer    = maf:Manufacturer
                        mfo:Field_Number    = maf:Field_Number
                        mfo:PrimaryLookup    = 1
                        if (Access:MANFAULO.TryFetch(mfo:PrimaryLookupKey) = Level:Benign)
                        ! Found
                            if (mfo:JobTypeAvailability = 0)
                                p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,mfo:Field)
                            elsif mfo:JobTypeAvailability = 1
                                if (locChargeable = 1)
                                    p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,mfo:Field)
                                end ! if (locChargeable = 1)
                            else ! if (mfo:JobTypeAvailability = 0)
                                if (locWarranty = 1)
                                    p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,mfo:Field)
                                end ! if (locWarranty = 1)
                            end !if (mfo:JobTypeAvailability = 0)
                        else ! if (Access:MANFAULO.TryFetch(mfo:PrimaryLookupKey) = Level:Benign)
                            ! Error
                        end ! if (Access:MANFAULO.TryFetch(mfo:PrimaryLookupKey) = Level:Benign)
                    end ! if (maf:Lookup = 'YES')
                    if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) <> '' and |
                        p_web.GSV('Hide:JobFaultCode' & maf:ScreenOrder) = 0 and |
                        p_web.GSV('ReadOnly:JobFaultCode' & maf:ScreenOrder) = 0)

                        locField = ''
                        Access:MANFAULO.Clearkey(mfo:Field_Key)
                        mfo:Manufacturer    = maf:Manufacturer
                        mfo:Field_Number    = maf:Field_Number
                        set(mfo:Field_Key,mfo:Field_Key)
                        loop
                            if (Access:MANFAULO.Next())
                                Break
                            end ! if (Access:MANFAULO.Next())
                            if (mfo:Manufacturer    <> maf:Manufacturer)
                                Break
                            end ! if (mfo:Manufacturer    <> maf:Manufacturer)
                            if (mfo:Field_Number    <> maf:Field_Number)
                                Break
                            end ! if (mfo:Field_Number    <> maf:Field_Number)

                            if (mfo:JobTypeAvailability = 1)
                                if (locChargeable = 0)
                                    cycle
                                end ! if (locWarranty <> 1)
                            end ! if (mfo:JobTypeAvailability = 1)
                            if (mfo:JobTypeAvailability = 2)
                                if (locWarranty = 0)
                                    cycle
                                end ! if (locWarranty = 0)
                            end ! if (mfo:JobTypeAvailability = 2)

                            count# += 1
                            if (count# > 1)
                                locField = ''
                                break
                            end ! if (count# > 1)
                            locField = mfo:Field
                        end ! loop
                        if (locField <> '')
                            p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,locField)
                        end ! if (locField <> '')
                    end
                end ! if (maf:Lookup = 'YES')

                if (maf:FillFromDOP)
                    if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
                        p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,p_web.GSV('job:DOP'))
                    end ! if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
                end ! if (maf:FillFromDOP)

                if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')
                    do checkStockModelFaultCodes
                end ! if (p_web.GSV('tmp:FaultCode' & maf:ScreenOrder) = '')

                locRepairIndex = 0
                locSetFaultCode = ''

                Access:MANFAULT_ALIAS.Clearkey(maf_ali:MainFaultKey)
                maf_ali:Manufacturer    = p_web.GSV('job:Manufacturer')
                maf_ali:MainFault    = 1
                if (Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign)
                    ! Found
                    Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
                    joo:JobNumber    = p_web.GSV('job:Ref_number')
                    set(joo:JobNumberKey,joo:JobNumberKey)
                    loop
                        if (Access:JOBOUTFL.Next())
                            Break
                        end ! if (Access:JOBOUTFL.Next())
                        if (joo:JobNumber    <> p_web.GSV('job:Ref_number'))
                            Break
                        end ! if (joo:JobNumber    <> p_web.GSV('job:Ref_number'))

                        Access:MANFAULO.Clearkey(mfo:Field_Key)
                        mfo:Manufacturer    = maf:Manufacturer
                        mfo:Field_Number    = maf_ali:Field_Number
                        mfo:Field    = joo:FaultCode
                        if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Found
                            if (mfo:SetJobFaultCode)
                                if (mfo:SelectJobFaultCode = maf:Field_Number)
                                    if (mfo:SkillLevel >= locRepairIndex)
                                        locSetFaultCode = mfo:JobFaultCodeValue
                                    end ! if (mfo:SkillLevel >= locRepairIndex)
                                end ! if (mfo:SelectJobFaultCode = maf:Field_Number)
                            end ! if (mfo:SetJobFaultCode)
                        else ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                        ! Error
                        end ! if (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                    end ! loop
                else ! if (Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign)
                ! Error
                end ! if (Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign)
                if (locSetFaultCode <> '')
                    p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,locSetFaultCode)
                end ! if (locSetFaultCode <> '')
            else! if (locHide = 0)
                p_web.SSV('Prompt:JobFaultCode' & maf:ScreenOrder,'')
                p_web.SSV('Comment:JobFaultCode' & maf:ScreenOrder,'')
                p_web.SSV('Hide:JobFaultCode' & maf:ScreenOrder,1)
            end ! if (locHide = 0)
        end ! loop
ValidateFaultCodes      routine
    case p_web.GSV('job:manufacturer')
    of 'MOTOROLA' Orof 'SAMSUNG' orof 'PHILIPS'
        if (dateCodeValidation(p_web.GSV('job:manufacturer'),p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked')))
            if p_web.gsv('ReturnURL') = 'QAProcedure' THEN
                p_web.SSV('locNextURL','QAProcedure')
            ELSE    
                p_web.SSV('locNextURL','ProofOfPurchase')
            END
        else ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
            p_web.SSV('job:warranty_Job','YES')
            p_web.SSV('job:warranty_Charge_Type','WARRANTY (MFTR)')
            if (p_web.GSV('job:POP') = '')
                p_web.SSV('job:POP','F')
            end ! if (p_web.GSV('job:POP') = '')
        end ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
    of 'ALCATEL' orof 'SIEMENS'
        if (p_web.GSV('job:fault_code3') <> '' and p_web.GSV('job:POP') = '')
            if (dateCodeValidation(p_web.GSV('job:manufacturer'),p_web.GSV('job:fault_code3'),p_web.GSV('job:date_Booked')))
                if p_web.gsv('ReturnURL') = 'QAProcedure' THEN
                    p_web.SSV('locNextURL','QAProcedure')
                ELSE    
                    p_web.SSV('locNextURL','ProofOfPurchase')
                END
            else ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
                p_web.SSV('job:warranty_Job','YES')
                p_web.SSV('job:warranty_Charge_Type','WARRANTY (MFTR)')
                if (p_web.GSV('job:POP') = '')
                    p_web.SSV('job:POP','F')
                end ! if (p_web.GSV('job:POP') = '')
            end ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
        end ! if (p_web.GSV('job:fault_code3') <> '')
    of 'ERICSSON'
        if (p_web.GSV('job:fault_code7') <> '' and p_web.GSV('job:POP') = '')
            if (dateCodeValidation(p_web.GSV('job:manufacturer'),p_web.GSV('job:fault_code7') & p_web.GSV('job:fault_code8'),p_web.GSV('job:date_Booked')))
                if p_web.gsv('ReturnURL') = 'QAProcedure' THEN
                    p_web.SSV('locNextURL','QAProcedure')
                ELSE    
                    p_web.SSV('locNextURL','ProofOfPurchase')
                END
            else ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
                p_web.SSV('job:warranty_Job','YES')
                p_web.SSV('job:warranty_Charge_Type','WARRANTY (MFTR)')
                if (p_web.GSV('job:POP') = '')
                    p_web.SSV('job:POP','F')
                end ! if (p_web.GSV('job:POP') = '')
            end ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
        end ! if (p_web.GSV('job:fault_code7') <> '')
    of 'BOSCH'
        if (p_web.GSV('job:fault_code7') <> '' and p_web.GSV('job:POP') = '')
            if (dateCodeValidation(p_web.GSV('job:manufacturer'),p_web.GSV('job:fault_code7') ,p_web.GSV('job:date_Booked')))
                if p_web.gsv('ReturnURL') = 'QAProcedure' THEN
                    p_web.SSV('locNextURL','QAProcedure')
                ELSE    
                    p_web.SSV('locNextURL','ProofOfPurchase')
                END
            else ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
                p_web.SSV('job:warranty_Job','YES')
                p_web.SSV('job:warranty_Charge_Type','WARRANTY (MFTR)')
                if (p_web.GSV('job:POP') = '')
                    p_web.SSV('job:POP','F')
                end ! if (p_web.GSV('job:POP') = '')
            end ! if (dateCodeValidation('MOTOROLA',p_web.GSV('job:MSN'),p_web.GSV('job:date_Booked'))
        end ! if (p_web.GSV('job:fault_code7') <> '')
    end ! case p_web.GSV('job:manufacturer')

    if (p_web.GSV('job:DOP') = '')
            if p_web.gsv('ReturnURL') = 'QAProcedure' THEN
                p_web.SSV('locNextURL','QAProcedure')
            ELSE    
                p_web.SSV('locNextURL','ProofOfPurchase')
            END
    end ! if (p_web.GSV('job:DOP') = '')
OpenFiles  ROUTINE
  p_web._OpenFile(AUDIT)
  p_web._OpenFile(CHARTYPE)
  p_web._OpenFile(REPTYDEF)
  p_web._OpenFile(MANFAULO_ALIAS)
  p_web._OpenFile(JOBSE3)
  p_web._OpenFile(MANFAULO)
  p_web._OpenFile(JOBOUTFL)
  p_web._OpenFile(MANFAULT_ALIAS)
  p_web._OpenFile(MANFAULT)
  p_web._OpenFile(WARPARTS)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(STOMODEL)
  p_web._OpenFile(STOMJFAU)
  p_web._OpenFile(MANFAUPA)
  p_web._OpenFile(MANFPALO)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(LOCATION)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(MODPROD)
  p_web._OpenFile(SBO_OutFaultParts)
  p_web._OpenFile(PARTS)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(AUDIT)
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(MANFAULO_ALIAS)
  p_Web._CloseFile(JOBSE3)
  p_Web._CloseFile(MANFAULO)
  p_Web._CloseFile(JOBOUTFL)
  p_Web._CloseFile(MANFAULT_ALIAS)
  p_Web._CloseFile(MANFAULT)
  p_Web._CloseFile(WARPARTS)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(STOMODEL)
  p_Web._CloseFile(STOMJFAU)
  p_Web._CloseFile(MANFAUPA)
  p_Web._CloseFile(MANFPALO)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(LOCATION)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(MODPROD)
  p_Web._CloseFile(SBO_OutFaultParts)
  p_Web._CloseFile(PARTS)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('JobFaultCodes_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  p_web.deletesessionvalue('JobFaultCodes:FirstTime')

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('tmp:FaultCode1')
    p_web.SetPicture('tmp:FaultCode1',p_web.GSV('Picture:JobFaultCode1'))
  End
  p_web.SetSessionPicture('tmp:FaultCode1',p_web.GSV('Picture:JobFaultCode1'))
  If p_web.IfExistsValue('tmp:FaultCode2')
    p_web.SetPicture('tmp:FaultCode2',p_web.GSV('Picture:JobFaultCode2'))
  End
  p_web.SetSessionPicture('tmp:FaultCode2',p_web.GSV('Picture:JobFaultCode2'))
  If p_web.IfExistsValue('tmp:FaultCode3')
    p_web.SetPicture('tmp:FaultCode3',p_web.GSV('Picture:JobFaultCode3'))
  End
  p_web.SetSessionPicture('tmp:FaultCode3',p_web.GSV('Picture:JobFaultCode3'))
  If p_web.IfExistsValue('tmp:FaultCode4')
    p_web.SetPicture('tmp:FaultCode4',p_web.GSV('Picture:JobFaultCode4'))
  End
  p_web.SetSessionPicture('tmp:FaultCode4',p_web.GSV('Picture:JobFaultCode4'))
  If p_web.IfExistsValue('tmp:FaultCode5')
    p_web.SetPicture('tmp:FaultCode5',p_web.GSV('Picture:JobFaultCode5'))
  End
  p_web.SetSessionPicture('tmp:FaultCode5',p_web.GSV('Picture:JobFaultCode5'))
  If p_web.IfExistsValue('tmp:FaultCode6')
    p_web.SetPicture('tmp:FaultCode6',p_web.GSV('Picture:JobFaultCode6'))
  End
  p_web.SetSessionPicture('tmp:FaultCode6',p_web.GSV('Picture:JobFaultCode6'))
  If p_web.IfExistsValue('tmp:FaultCode7')
    p_web.SetPicture('tmp:FaultCode7',p_web.GSV('Picture:JobFaultCode7'))
  End
  p_web.SetSessionPicture('tmp:FaultCode7',p_web.GSV('Picture:JobFaultCode7'))
  If p_web.IfExistsValue('tmp:FaultCode8')
    p_web.SetPicture('tmp:FaultCode8',p_web.GSV('Picture:JobFaultCode8'))
  End
  p_web.SetSessionPicture('tmp:FaultCode8',p_web.GSV('Picture:JobFaultCode8'))
  If p_web.IfExistsValue('tmp:FaultCode9')
    p_web.SetPicture('tmp:FaultCode9',p_web.GSV('Picture:JobFaultCode9'))
  End
  p_web.SetSessionPicture('tmp:FaultCode9',p_web.GSV('Picture:JobFaultCode9'))
  If p_web.IfExistsValue('tmp:FaultCode10')
    p_web.SetPicture('tmp:FaultCode10',p_web.GSV('Picture:JobFaultCode10'))
  End
  p_web.SetSessionPicture('tmp:FaultCode10',p_web.GSV('Picture:JobFaultCode10'))
  If p_web.IfExistsValue('tmp:FaultCode11')
    p_web.SetPicture('tmp:FaultCode11',p_web.GSV('Picture:JobFaultCode11'))
  End
  p_web.SetSessionPicture('tmp:FaultCode11',p_web.GSV('Picture:JobFaultCode11'))
  If p_web.IfExistsValue('tmp:FaultCode12')
    p_web.SetPicture('tmp:FaultCode12',p_web.GSV('Picture:JobFaultCode12'))
  End
  p_web.SetSessionPicture('tmp:FaultCode12',p_web.GSV('Picture:JobFaultCode12'))
  If p_web.IfExistsValue('tmp:FaultCode13')
    p_web.SetPicture('tmp:FaultCode13',p_web.GSV('Picture:JobFaultCode13'))
  End
  p_web.SetSessionPicture('tmp:FaultCode13',p_web.GSV('Picture:JobFaultCode13'))
  If p_web.IfExistsValue('tmp:FaultCode14')
    p_web.SetPicture('tmp:FaultCode14',p_web.GSV('Picture:JobFaultCode14'))
  End
  p_web.SetSessionPicture('tmp:FaultCode14',p_web.GSV('Picture:JobFaultCode14'))
  If p_web.IfExistsValue('tmp:FaultCode15')
    p_web.SetPicture('tmp:FaultCode15',p_web.GSV('Picture:JobFaultCode15'))
  End
  p_web.SetSessionPicture('tmp:FaultCode15',p_web.GSV('Picture:JobFaultCode15'))
  If p_web.IfExistsValue('tmp:FaultCode16')
    p_web.SetPicture('tmp:FaultCode16',p_web.GSV('Picture:JobFaultCode16'))
  End
  p_web.SetSessionPicture('tmp:FaultCode16',p_web.GSV('Picture:JobFaultCode16'))
  If p_web.IfExistsValue('tmp:FaultCode17')
    p_web.SetPicture('tmp:FaultCode17',p_web.GSV('Picture:JobFaultCode17'))
  End
  p_web.SetSessionPicture('tmp:FaultCode17',p_web.GSV('Picture:JobFaultCode17'))
  If p_web.IfExistsValue('tmp:FaultCode18')
    p_web.SetPicture('tmp:FaultCode18',p_web.GSV('Picture:JobFaultCode18'))
  End
  p_web.SetSessionPicture('tmp:FaultCode18',p_web.GSV('Picture:JobFaultCode18'))
  If p_web.IfExistsValue('tmp:FaultCode19')
    p_web.SetPicture('tmp:FaultCode19',p_web.GSV('Picture:JobFaultCode19'))
  End
  p_web.SetSessionPicture('tmp:FaultCode19',p_web.GSV('Picture:JobFaultCode19'))
  If p_web.IfExistsValue('tmp:FaultCode20')
    p_web.SetPicture('tmp:FaultCode20',p_web.GSV('Picture:JobFaultCode20'))
  End
  p_web.SetSessionPicture('tmp:FaultCode20',p_web.GSV('Picture:JobFaultCode20'))
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('MSNValidation') = 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:ProductCode'
    p_web.setsessionvalue('showtab_JobFaultCodes',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODPROD)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCode1')
  of 'tmp:FaultCode1'
      local.AfterFaultCodeLookup(1)
  of 'tmp:FaultCode2'
      local.AfterFaultCodeLookup(2)
  of 'tmp:FaultCode3'
      local.AfterFaultCodeLookup(3)
  of 'tmp:FaultCode4'
      local.AfterFaultCodeLookup(4)
  of 'tmp:FaultCode5'
      local.AfterFaultCodeLookup(5)
  of 'tmp:FaultCode6'
      local.AfterFaultCodeLookup(6)
  of 'tmp:FaultCode7'
      local.AfterFaultCodeLookup(7)
  of 'tmp:FaultCode8'
      local.AfterFaultCodeLookup(8)
  of 'tmp:FaultCode9'
      local.AfterFaultCodeLookup(9)
  of 'tmp:FaultCode10'
      local.AfterFaultCodeLookup(10)
  of 'tmp:FaultCode11'
      local.AfterFaultCodeLookup(11)
  of 'tmp:FaultCode12'
      local.AfterFaultCodeLookup(12)
  of 'tmp:FaultCode13'
      local.AfterFaultCodeLookup(13)
  of 'tmp:FaultCode14'
      local.AfterFaultCodeLookup(14)
  of 'tmp:FaultCode15'
      local.AfterFaultCodeLookup(15)
  of 'tmp:FaultCode16'
      local.AfterFaultCodeLookup(16)
  of 'tmp:FaultCode17'
      local.AfterFaultCodeLookup(17)
  of 'tmp:FaultCode18'
      local.AfterFaultCodeLookup(18)
  of 'tmp:FaultCode19'
      local.AfterFaultCodeLookup(19)
  of 'tmp:FaultCode20'
      local.AfterFaultCodeLookup(20)
  
  End
  If p_web.gsv('HideTools') = 0
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:MSN',tmp:MSN)
  p_web.SetSessionValue('tmp:VerifyMSN',tmp:VerifyMSN)
  p_web.SetSessionValue('tmp:ConfirmMSNChange',tmp:ConfirmMSNChange)
  p_web.SetSessionValue('job:ProductCode',job:ProductCode)
  p_web.SetSessionValue('tmp:FaultCode1',tmp:FaultCode1)
  p_web.SetSessionValue('tmp:FaultCode2',tmp:FaultCode2)
  p_web.SetSessionValue('tmp:FaultCode3',tmp:FaultCode3)
  p_web.SetSessionValue('tmp:FaultCode4',tmp:FaultCode4)
  p_web.SetSessionValue('tmp:FaultCode5',tmp:FaultCode5)
  p_web.SetSessionValue('tmp:FaultCode6',tmp:FaultCode6)
  p_web.SetSessionValue('tmp:FaultCode7',tmp:FaultCode7)
  p_web.SetSessionValue('tmp:FaultCode8',tmp:FaultCode8)
  p_web.SetSessionValue('tmp:FaultCode9',tmp:FaultCode9)
  p_web.SetSessionValue('tmp:FaultCode10',tmp:FaultCode10)
  p_web.SetSessionValue('tmp:FaultCode11',tmp:FaultCode11)
  p_web.SetSessionValue('tmp:FaultCode12',tmp:FaultCode12)
  p_web.SetSessionValue('tmp:FaultCode13',tmp:FaultCode13)
  p_web.SetSessionValue('tmp:FaultCode14',tmp:FaultCode14)
  p_web.SetSessionValue('tmp:FaultCode15',tmp:FaultCode15)
  p_web.SetSessionValue('tmp:FaultCode16',tmp:FaultCode16)
  p_web.SetSessionValue('tmp:FaultCode17',tmp:FaultCode17)
  p_web.SetSessionValue('tmp:FaultCode18',tmp:FaultCode18)
  p_web.SetSessionValue('tmp:FaultCode19',tmp:FaultCode19)
  p_web.SetSessionValue('tmp:FaultCode20',tmp:FaultCode20)
  p_web.SetSessionValue('jobe3:FaultCode21',jobe3:FaultCode21)
  p_web.SetSessionValue('jobe3:SN_Software_Installed',jobe3:SN_Software_Installed)
  p_web.SetSessionValue('tmp:processExchange',tmp:processExchange)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:MSN')
    tmp:MSN = p_web.GetValue('tmp:MSN')
    p_web.SetSessionValue('tmp:MSN',tmp:MSN)
  End
  if p_web.IfExistsValue('tmp:VerifyMSN')
    tmp:VerifyMSN = p_web.GetValue('tmp:VerifyMSN')
    p_web.SetSessionValue('tmp:VerifyMSN',tmp:VerifyMSN)
  End
  if p_web.IfExistsValue('tmp:ConfirmMSNChange')
    tmp:ConfirmMSNChange = p_web.GetValue('tmp:ConfirmMSNChange')
    p_web.SetSessionValue('tmp:ConfirmMSNChange',tmp:ConfirmMSNChange)
  End
  if p_web.IfExistsValue('job:ProductCode')
    job:ProductCode = p_web.GetValue('job:ProductCode')
    p_web.SetSessionValue('job:ProductCode',job:ProductCode)
  End
  if p_web.IfExistsValue('tmp:FaultCode1')
    tmp:FaultCode1 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode1')),p_web.GSV('Picture:JobFaultCode1'))
    p_web.SetSessionValue('tmp:FaultCode1',tmp:FaultCode1)
  End
  if p_web.IfExistsValue('tmp:FaultCode2')
    tmp:FaultCode2 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode2')),p_web.GSV('Picture:JobFaultCode2'))
    p_web.SetSessionValue('tmp:FaultCode2',tmp:FaultCode2)
  End
  if p_web.IfExistsValue('tmp:FaultCode3')
    tmp:FaultCode3 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode3')),p_web.GSV('Picture:JobFaultCode3'))
    p_web.SetSessionValue('tmp:FaultCode3',tmp:FaultCode3)
  End
  if p_web.IfExistsValue('tmp:FaultCode4')
    tmp:FaultCode4 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode4')),p_web.GSV('Picture:JobFaultCode4'))
    p_web.SetSessionValue('tmp:FaultCode4',tmp:FaultCode4)
  End
  if p_web.IfExistsValue('tmp:FaultCode5')
    tmp:FaultCode5 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode5')),p_web.GSV('Picture:JobFaultCode5'))
    p_web.SetSessionValue('tmp:FaultCode5',tmp:FaultCode5)
  End
  if p_web.IfExistsValue('tmp:FaultCode6')
    tmp:FaultCode6 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode6')),p_web.GSV('Picture:JobFaultCode6'))
    p_web.SetSessionValue('tmp:FaultCode6',tmp:FaultCode6)
  End
  if p_web.IfExistsValue('tmp:FaultCode7')
    tmp:FaultCode7 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode7')),p_web.GSV('Picture:JobFaultCode7'))
    p_web.SetSessionValue('tmp:FaultCode7',tmp:FaultCode7)
  End
  if p_web.IfExistsValue('tmp:FaultCode8')
    tmp:FaultCode8 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode8')),p_web.GSV('Picture:JobFaultCode8'))
    p_web.SetSessionValue('tmp:FaultCode8',tmp:FaultCode8)
  End
  if p_web.IfExistsValue('tmp:FaultCode9')
    tmp:FaultCode9 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode9')),p_web.GSV('Picture:JobFaultCode9'))
    p_web.SetSessionValue('tmp:FaultCode9',tmp:FaultCode9)
  End
  if p_web.IfExistsValue('tmp:FaultCode10')
    tmp:FaultCode10 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode10')),p_web.GSV('Picture:JobFaultCode10'))
    p_web.SetSessionValue('tmp:FaultCode10',tmp:FaultCode10)
  End
  if p_web.IfExistsValue('tmp:FaultCode11')
    tmp:FaultCode11 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode11')),p_web.GSV('Picture:JobFaultCode11'))
    p_web.SetSessionValue('tmp:FaultCode11',tmp:FaultCode11)
  End
  if p_web.IfExistsValue('tmp:FaultCode12')
    tmp:FaultCode12 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode12')),p_web.GSV('Picture:JobFaultCode12'))
    p_web.SetSessionValue('tmp:FaultCode12',tmp:FaultCode12)
  End
  if p_web.IfExistsValue('tmp:FaultCode13')
    tmp:FaultCode13 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode13')),p_web.GSV('Picture:JobFaultCode13'))
    p_web.SetSessionValue('tmp:FaultCode13',tmp:FaultCode13)
  End
  if p_web.IfExistsValue('tmp:FaultCode14')
    tmp:FaultCode14 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode14')),p_web.GSV('Picture:JobFaultCode14'))
    p_web.SetSessionValue('tmp:FaultCode14',tmp:FaultCode14)
  End
  if p_web.IfExistsValue('tmp:FaultCode15')
    tmp:FaultCode15 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode15')),p_web.GSV('Picture:JobFaultCode15'))
    p_web.SetSessionValue('tmp:FaultCode15',tmp:FaultCode15)
  End
  if p_web.IfExistsValue('tmp:FaultCode16')
    tmp:FaultCode16 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode16')),p_web.GSV('Picture:JobFaultCode16'))
    p_web.SetSessionValue('tmp:FaultCode16',tmp:FaultCode16)
  End
  if p_web.IfExistsValue('tmp:FaultCode17')
    tmp:FaultCode17 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode17')),p_web.GSV('Picture:JobFaultCode17'))
    p_web.SetSessionValue('tmp:FaultCode17',tmp:FaultCode17)
  End
  if p_web.IfExistsValue('tmp:FaultCode18')
    tmp:FaultCode18 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode18')),p_web.GSV('Picture:JobFaultCode18'))
    p_web.SetSessionValue('tmp:FaultCode18',tmp:FaultCode18)
  End
  if p_web.IfExistsValue('tmp:FaultCode19')
    tmp:FaultCode19 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode19')),p_web.GSV('Picture:JobFaultCode19'))
    p_web.SetSessionValue('tmp:FaultCode19',tmp:FaultCode19)
  End
  if p_web.IfExistsValue('tmp:FaultCode20')
    tmp:FaultCode20 = p_web.dformat(clip(p_web.GetValue('tmp:FaultCode20')),p_web.GSV('Picture:JobFaultCode20'))
    p_web.SetSessionValue('tmp:FaultCode20',tmp:FaultCode20)
  End
  if p_web.IfExistsValue('jobe3:FaultCode21')
    jobe3:FaultCode21 = p_web.GetValue('jobe3:FaultCode21')
    p_web.SetSessionValue('jobe3:FaultCode21',jobe3:FaultCode21)
  End
  if p_web.IfExistsValue('jobe3:SN_Software_Installed')
    jobe3:SN_Software_Installed = p_web.GetValue('jobe3:SN_Software_Installed')
    p_web.SetSessionValue('jobe3:SN_Software_Installed',jobe3:SN_Software_Installed)
  End
  if p_web.IfExistsValue('tmp:processExchange')
    tmp:processExchange = p_web.GetValue('tmp:processExchange')
    p_web.SetSessionValue('tmp:processExchange',tmp:processExchange)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('JobFaultCodes_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  !    p_web.SSV('locNextURL','BillingConfirmation')
    if p_web.gsv('ReturnURL') = 'QAProcedure' THEN
        p_web.SSV('locNextURL','QAProcedure')
    ELSE    
        p_web.SSV('locNextURL','BillingConfirmation')
    END
    
    IF (p_web.IfExistsValue('soc'))
        ! #13287 Save Jobs On Close? (DBH: 21/01/2015)
        p_web.StoreValue('soc')
    END ! IF
  
    if (p_web.GSV('JobFaultCodes:FirstTime') = 0)
          CalculateBilling(p_web)
          Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
          maf:Manufacturer    = p_web.GSV('job:Manufacturer')
          maf:ScreenOrder    = 0
          set(maf:ScreenOrderKey,maf:ScreenOrderKey)
          loop
              if (Access:MANFAULT.Next())
                  Break
              end ! if (Access:MANFAULT.Next())
              if (maf:Manufacturer    <> p_web.GSV('job:Manufacturer'))
                  Break
              end ! if (maf:Manufacturer    <> p_web.GSV('job:Manufacturer'))
              if (maf:Field_Number < 13)
                 p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,p_web.GSV('job:Fault_Code' & maf:Field_Number))
              else ! if (maf:ScreenOrder < 13)
                 p_web.SSV('tmp:FaultCode' & maf:ScreenOrder,p_web.GSV('wob:FaultCode' & maf:Field_Number))
              end ! if (maf:ScreenOrder < 13)
          end ! loop
          p_web.SSV('Hide:ExchangePrompt',1)
          p_web.SSV('tmp:processExchange',0)
          p_web.SSV('tmp:MSN','')
          p_web.SSV('tmp:VerifyMSN','')
          
          ! Verify MSN
          p_web.SSV('MSNValidation',0)
          p_web.SSV('MSNValidated',0)
          p_web.SSV('Hide:ButtonVerifyMSN',1)
          p_web.SSV('Hide:VerifyMSN',1)
          p_web.SSV('Hide:ButtonVerifyMSN2',1)
          p_web.SSV('Hide:ConfirmMSNChange',1)
  
          if (MSNRequired(p_web.GSV('job:Manufacturer')))
              found# = 0
  
              Access:AUDIT.Clearkey(aud:typeRefKey)
              aud:ref_Number    = p_web.GSV('job:Ref_Number')
              aud:type    = 'JOB'
              set(aud:typeRefKey,aud:typeRefKey)
              loop
                  if (Access:AUDIT.Next())
                      Break
                  end ! if (Access:AUDIT.Next())
                  if (aud:ref_Number    <> p_web.GSV('job:Ref_Number'))
                      Break
                  end ! if (aud:ref_Number    <> p_web.GSV('job:Ref_Number'))
                  if (aud:type    <> 'JOB')
                      Break
                  end ! if (aud:type    <> 'JOB')
                  if (instring('MSN VERIFICATION',upper(aud:Action),1,1))
                      found# = 1
                      break
                  end ! if (instring('MSN VERIFICATION',upper(aud:Action),1,1)
              end ! loop
  
              if (found# = 0)
                  p_web.SSV('MSNValidation',1)
                  p_web.SSV('Comment:MSN','Verify MSN')
                  p_web.SSV('Hide:ButtonVerifyMSN',0)
              end ! if (found# = 0)
          end ! if (MSNRequired(p_web.GSV('job:Manufacturer'))        
          
      end ! if (p_web.GSV('JobFaultCodes:FirstTime') = 0)
  
      do buildfaultcodes
  
      p_web.SSV('JobFaultCodes:FirstTime',1)
  
  
      if (productCodeRequired(p_web.GSV('job:Manufacturer') )= 0)
          p_web.SSV('Hide:ProductCode',0)
          if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'PRODUCT CODE - AMEND'))
              ! No Lookup
              p_web.SSV('ReadOnly:ProdutCode',1)
          else
              p_web.SSV('ReadOnly:ProdutCode',0)
          end ! if (securityCheckFailed(p_web.GSV('BookingUserPassword'),'PRODUCT CODE - AMEND'))
      else ! if (productCodeRequired(p_web.GSV('job:Manufacturer')))
          p_web.SSV('Hide:ProductCode',1)
      end ! if (productCodeRequired(p_web.GSV('job:Manufacturer')))
  
  
      !Clear Part Out Fault Table
  
      Access:SBO_OUTFAULTPARTS.Clearkey(sofp:faultKey)
      sofp:sessionID    = p_web.sessionID
      set(sofp:faultKey,sofp:faultKey)
      loop
          if (Access:SBO_OUTFAULTPARTS.Next())
              Break
          end ! if (Access:SBO_OUTFAULTPARTS.Next())
          if (sofp:sessionID    <> p_web.sessionID)
              Break
          end ! if (sofp:sessionID    <> p_web.sessionID)
          access:SBO_OUTFAULTPARTS.deleterecord(0)
      end ! loop
  
      
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:MSN = p_web.RestoreValue('tmp:MSN')
 tmp:VerifyMSN = p_web.RestoreValue('tmp:VerifyMSN')
 tmp:ConfirmMSNChange = p_web.RestoreValue('tmp:ConfirmMSNChange')
 tmp:FaultCode1 = p_web.RestoreValue('tmp:FaultCode1')
 tmp:FaultCode2 = p_web.RestoreValue('tmp:FaultCode2')
 tmp:FaultCode3 = p_web.RestoreValue('tmp:FaultCode3')
 tmp:FaultCode4 = p_web.RestoreValue('tmp:FaultCode4')
 tmp:FaultCode5 = p_web.RestoreValue('tmp:FaultCode5')
 tmp:FaultCode6 = p_web.RestoreValue('tmp:FaultCode6')
 tmp:FaultCode7 = p_web.RestoreValue('tmp:FaultCode7')
 tmp:FaultCode8 = p_web.RestoreValue('tmp:FaultCode8')
 tmp:FaultCode9 = p_web.RestoreValue('tmp:FaultCode9')
 tmp:FaultCode10 = p_web.RestoreValue('tmp:FaultCode10')
 tmp:FaultCode11 = p_web.RestoreValue('tmp:FaultCode11')
 tmp:FaultCode12 = p_web.RestoreValue('tmp:FaultCode12')
 tmp:FaultCode13 = p_web.RestoreValue('tmp:FaultCode13')
 tmp:FaultCode14 = p_web.RestoreValue('tmp:FaultCode14')
 tmp:FaultCode15 = p_web.RestoreValue('tmp:FaultCode15')
 tmp:FaultCode16 = p_web.RestoreValue('tmp:FaultCode16')
 tmp:FaultCode17 = p_web.RestoreValue('tmp:FaultCode17')
 tmp:FaultCode18 = p_web.RestoreValue('tmp:FaultCode18')
 tmp:FaultCode19 = p_web.RestoreValue('tmp:FaultCode19')
 tmp:FaultCode20 = p_web.RestoreValue('tmp:FaultCode20')
 tmp:processExchange = p_web.RestoreValue('tmp:processExchange')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('locNextURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('JobFaultCodes_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('JobFaultCodes_ChainTo')
    loc:formaction = p_web.GetSessionValue('JobFaultCodes_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = ReturnURL
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
    loc:viewonly = Choose(p_web.GSV('Job:ViewOnly') = 1,1,0)
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="JobFaultCodes" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="JobFaultCodes" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="JobFaultCodes" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Job Fault Codes') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Job Fault Codes',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_JobFaultCodes">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_JobFaultCodes" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_JobFaultCodes')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GSV('MSNValidation') = 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Verify MSN') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Fault Codes') & ''''
        If p_web.gsv('HideTools') = 0
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Tools') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_JobFaultCodes')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_JobFaultCodes'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='MODPROD'
          If Not (p_web.GSV('Hide:JobFaultCode1') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCode1')
          End
    End
  Else
    If False
    ElsIf p_web.GSV('MSNValidation') = 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:MSN')
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.job:ProductCode')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GSV('MSNValidation') = 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          If p_web.gsv('HideTools') = 0
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_JobFaultCodes')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GSV('MSNValidation') = 1
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    if p_web.gsv('HideTools') = 0
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GSV('MSNValidation') = 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Verify MSN') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobFaultCodes_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Verify MSN')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Verify MSN')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Verify MSN')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Verify MSN')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:MSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonVerifyMSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonVerifyMSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:VerifyMSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:VerifyMSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:VerifyMSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonVerifyMSN2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonVerifyMSN2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ConfirmMSNChange
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ConfirmMSNChange
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ConfirmMSNChange
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Job Fault Codes') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobFaultCodes_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Fault Codes')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Job Fault Codes')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Fault Codes')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Fault Codes')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:ProductCode
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:ProductCode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:ProductCode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode4
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode5
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode6
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode7
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode8
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode8
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode8
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode9
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode9
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode9
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode10
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode10
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode10
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode11
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode11
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode11
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode12
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode12
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode12
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode13
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode13
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode13
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode14
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode14
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode14
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode15
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode15
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode15
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode16
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode16
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode16
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode17
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode17
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode17
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode18
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode18
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode18
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode19
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode19
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode19
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:FaultCode20
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:FaultCode20
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:FaultCode20
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('jobe3:SpecificNeeds') = 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe3:FaultCode21
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe3:FaultCode21
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe3:FaultCode21
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('jobe3:SpecificNeeds') = 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe3:SN_Software_Installed
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe3:SN_Software_Installed
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe3:SN_Software_Installed
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
  If p_web.gsv('HideTools') = 0
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Tools') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobFaultCodes_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Tools')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Tools')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Tools')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Tools')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonRepairNotes
      do Comment::buttonRepairNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonOutFaults
      do Comment::buttonOutFaults
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobFaultCodes_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::promptExchange
      do Comment::promptExchange
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:processExchange
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:processExchange
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:processExchange
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::tmp:MSN  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:MSN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('M.S.N.')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:MSN') & '_prompt')

Validate::tmp:MSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:MSN',p_web.GetValue('NewValue'))
    tmp:MSN = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:MSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:MSN',p_web.GetValue('Value'))
    tmp:MSN = p_web.GetValue('Value')
  End
  If tmp:MSN = ''
    loc:Invalid = 'tmp:MSN'
    loc:alert = p_web.translate('M.S.N.') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:MSN = Upper(tmp:MSN)
    p_web.SetSessionValue('tmp:MSN',tmp:MSN)
  do Value::tmp:MSN
  do SendAlert
  do Prompt::tmp:VerifyMSN
  do Value::tmp:VerifyMSN  !1
  do Comment::tmp:VerifyMSN

Value::tmp:MSN  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:MSN') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:MSN
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('MSNValidated') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('MSNValidated') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:MSN')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:MSN = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:MSN'',''jobfaultcodes_tmp:msn_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:MSN')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:MSN',p_web.GetSessionValueFormat('tmp:MSN'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:MSN') & '_value')

Comment::tmp:MSN  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:MSN'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:MSN') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:MSN') & '_comment')

Validate::buttonVerifyMSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonVerifyMSN',p_web.GetValue('NewValue'))
    do Value::buttonVerifyMSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  if (p_web.GSV('tmp:MSN') <> '')
      error# = 0
      if (len(clip(p_web.GSV('tmp:MSN'))) = 11 and p_web.GSV('job:Manufacturer') = 'ERICSSON') 
          p_web.SSV('tmp:MSN',sub(p_web.GSV('tmp:MSN'),2,10))
      end ! if (len(clip(p_web.GSV('tmp:MSN'))) and p_web.GSV('job:Manufacturer') = 'ERICSSON')
  
      if (checkLength('MSN',p_web.GSV('job:Model_Number'),p_web.GSV('tmp:MSN')))
          error# = 1
      else !if (checkLength('MSN',p_web.GSV('job:Model_Number'),p_web.GSV('tmp:MSN'))
          Access:MANUFACT.Clearkey(man:Manufacturer_Key)
          man:Manufacturer    = p_web.GSV('job:Manufacturer')
          if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
              ! Found
              if (man:ApplyMSNFormat)
                  if (checkFaultFormat(p_web.GSV('tmp:MSN'),man:MSNFormat))
                      p_web.SSV('Comment:MSN','M.S.N. Failed Format Validation')
                      error# = 1
                  end ! if (checkFaultFormat(p_web.GSV('tmp:MSN'),man:MSNFormat))
              end ! if (man:ApplyMSNFormat)
          else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
              ! Error
          end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
      end ! if (checkLength('MSN   ',p_web.GSV('job:Model_Number'),p_web.GSV('tmp:MSN'))
  
      if (error# = 0)
          if (p_web.GSV('tmp:MSN') = p_web.GSV('job:MSN'))
              p_web.SSV('MSNValidated',1)
              p_web.SSV('Comment:MSN','M.S.N. Verification Passed')
  !              p_web.SSV('AddToAudit:Type','JOB')
  !              p_web.SSV('AddToAudit:Action','MSN VERIFICATION: PASSED')
  !              p_web.SSV('AddToAudit:Notes','MSN: ' & p_web.GSV('job:MSN'))
              AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','MSN VERIFICATION: PASSED','MSN: ' & p_web.GSV('job:MSN'))
          else ! if (p_web.GSV('tmp:MSN') = p_web.GSV('job:MSN'))
              p_web.SSV('Hide:VerifyMSN',0)
              p_web.SSV('Hide:ButtonVerifyMSN',1)
              p_web.SSV('Hide:ButtonVerifyMSN2',0)
              p_web.SSV('Comment:MSN','MSN does not match job. Re-Verify entered MSN Below')
              p_web.SSV('Comment:MSN2','Verify MSN')
          end ! if (p_web.GSV('tmp:MSN') = p_web.GSV('job:MSN'))
      end ! if (error# = 0)
  end ! if (p_web.GSV('tmp:MSN') <> '')
  do Value::buttonVerifyMSN
  do SendAlert
  do Prompt::tmp:VerifyMSN
  do Value::tmp:VerifyMSN  !1
  do Comment::tmp:VerifyMSN
  do Prompt::tmp:MSN
  do Value::tmp:MSN  !1
  do Comment::tmp:MSN
  do Value::buttonVerifyMSN2  !1

Value::buttonVerifyMSN  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('buttonVerifyMSN') & '_value',Choose(p_web.GSV('Hide:ButtonVerifyMSN') = 1 Or p_web.GSV('MSNValidated') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ButtonVerifyMSN') = 1 Or p_web.GSV('MSNValidated') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonVerifyMSN'',''jobfaultcodes_buttonverifymsn_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','VerifyMSN','Verify M.S.N.','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('buttonVerifyMSN') & '_value')

Comment::buttonVerifyMSN  Routine
    loc:comment = ''
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('buttonVerifyMSN') & '_comment',Choose(p_web.GSV('Hide:ButtonVerifyMSN') = 1 Or p_web.GSV('MSNValidated') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ButtonVerifyMSN') = 1 Or p_web.GSV('MSNValidated') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:VerifyMSN  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:VerifyMSN') & '_prompt',Choose(p_web.GSV('Hide:VerifyMSN') = 1 or p_web.GSV('MSNValidated') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Verify M.S.N.')
  If p_web.GSV('Hide:VerifyMSN') = 1 or p_web.GSV('MSNValidated') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:VerifyMSN') & '_prompt')

Validate::tmp:VerifyMSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:VerifyMSN',p_web.GetValue('NewValue'))
    tmp:VerifyMSN = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:VerifyMSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:VerifyMSN',p_web.GetValue('Value'))
    tmp:VerifyMSN = p_web.GetValue('Value')
  End
  If tmp:VerifyMSN = ''
    loc:Invalid = 'tmp:VerifyMSN'
    loc:alert = p_web.translate('Verify M.S.N.') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:VerifyMSN = Upper(tmp:VerifyMSN)
    p_web.SetSessionValue('tmp:VerifyMSN',tmp:VerifyMSN)
  do Value::tmp:VerifyMSN
  do SendAlert

Value::tmp:VerifyMSN  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:VerifyMSN') & '_value',Choose(p_web.GSV('Hide:VerifyMSN') = 1 or p_web.GSV('MSNValidated') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:VerifyMSN') = 1 or p_web.GSV('MSNValidated') = 1)
  ! --- STRING --- tmp:VerifyMSN
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:VerifyMSN')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:VerifyMSN = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:VerifyMSN'',''jobfaultcodes_tmp:verifymsn_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:VerifyMSN',p_web.GetSessionValueFormat('tmp:VerifyMSN'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:VerifyMSN') & '_value')

Comment::tmp:VerifyMSN  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:VerifyMSN'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:VerifyMSN') & '_comment',Choose(p_web.GSV('Hide:VerifyMSN') = 1 or p_web.GSV('MSNValidated') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:VerifyMSN') = 1 or p_web.GSV('MSNValidated') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:VerifyMSN') & '_comment')

Validate::buttonVerifyMSN2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonVerifyMSN2',p_web.GetValue('NewValue'))
    do Value::buttonVerifyMSN2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ! Second MSN Validation
  if (p_web.GSV('tmp:VerifyMSN') <> '')
      error# = 0
      if (len(clip(p_web.GSV('tmp:VerifyMSN'))) = 11 and p_web.GSV('job:Manufacturer') = 'ERICSSON')
          p_web.SSV('tmp:VerifyMSN',sub(p_web.GSV('tmp:VerifyMSN'),2,10))
      end ! if (len(clip(p_web.GSV('tmp:VerifyMSN'))) and p_web.GSV('job:Manufacturer') = 'ERICSSON')
  
      if (checkLength('MSN',p_web.GSV('job:Model_Number'),p_web.GSV('tmp:VerifyMSN')))
          error# = 1
      else !if (checkLength('MSN',p_web.GSV('job:Model_Number'),p_web.GSV('tmp:VerifyMSN'))
          Access:MANUFACT.Clearkey(man:Manufacturer_Key)
          man:Manufacturer    = p_web.GSV('job:Manufacturer')
          if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
              ! Found
              if (man:ApplyMSNFormat)
                  if (checkFaultFormat(p_web.GSV('tmp:VerifyMSN'),man:MSNFormat))
                      p_web.SSV('Comment:VerifyMSN','M.S.N. Failed Format Validation')
                      error# = 1
                  end ! if (checkFaultFormat(p_web.GSV('tmp:VerifyMSN'),man:MSNFormat))
              end ! if (man:ApplyMSNFormat)
          else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
              ! Error
          end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
      end ! if (checkLength('MSN   ',p_web.GSV('job:Model_Number'),p_web.GSV('tmp:VerifyMSN'))
  
      if (error# = 0)
          if (p_web.GSV('tmp:VerifyMSN') = p_web.GSV('tmp:MSN'))
              p_web.SSV('Hide:ConfirmMSNChange',0)
              p_web.SSV('Hide:ButtonVerifyMSN2',1)
              p_web.SSV('Comment:VerifyMSN','MSNs Match. Confirm Change To Job')
          else ! if (p_web.GSV('tmp:VerifyMSN') = p_web.GSV('job:MSN'))
              p_web.SSV('Comment:VerifyMSN','MSN does not match')
          end ! if (p_web.GSV('tmp:VerifyMSN') = p_web.GSV('job:MSN'))
      end ! if (error# = 0)
  end ! if (p_web.GSV('tmp:VerifyMSN') <> '')
  do Value::buttonVerifyMSN2
  do SendAlert
  do Prompt::tmp:VerifyMSN
  do Value::tmp:VerifyMSN  !1
  do Comment::tmp:VerifyMSN
  do Value::buttonVerifyMSN  !1
  do Prompt::tmp:MSN
  do Value::tmp:MSN  !1
  do Comment::tmp:MSN
  do Prompt::tmp:ConfirmMSNChange
  do Value::tmp:ConfirmMSNChange  !1
  do Comment::tmp:ConfirmMSNChange

Value::buttonVerifyMSN2  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('buttonVerifyMSN2') & '_value',Choose(p_web.GSV('Hide:ButtonVerifyMSN2') = 1 Or p_web.GSV('MSNValidated') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ButtonVerifyMSN2') = 1 Or p_web.GSV('MSNValidated') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonVerifyMSN2'',''jobfaultcodes_buttonverifymsn2_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','VerifyMSN2','Verify M.S.N.','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('buttonVerifyMSN2') & '_value')

Comment::buttonVerifyMSN2  Routine
    loc:comment = ''
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('buttonVerifyMSN2') & '_comment',Choose(p_web.GSV('Hide:ButtonVerifyMSN2') = 1 Or p_web.GSV('MSNValidated') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ButtonVerifyMSN2') = 1 Or p_web.GSV('MSNValidated') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ConfirmMSNChange  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:ConfirmMSNChange') & '_prompt',Choose(p_web.GSV('Hide:ConfirmMSNChange') =1 OR p_web.GSV('MSNValidated') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Change Job MSN?')
  If p_web.GSV('Hide:ConfirmMSNChange') =1 OR p_web.GSV('MSNValidated') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:ConfirmMSNChange') & '_prompt')

Validate::tmp:ConfirmMSNChange  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ConfirmMSNChange',p_web.GetValue('NewValue'))
    tmp:ConfirmMSNChange = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ConfirmMSNChange
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ConfirmMSNChange',p_web.GetValue('Value'))
    tmp:ConfirmMSNChange = p_web.GetValue('Value')
  End
  ! Confirm MSN Change
  if (p_web.GSV('tmp:ConfirmMSNChange') = 'Y')
      p_web.SSV('MSNValidated',1)
      
      p_web.SSV('tmp:ConfirmMSNChange','')
  
  !      p_web.SSV('AddToAudit:Type','JOB')
  !      p_web.SSV('AddToAudit:Action','MSN VERIFICATION: AMENDMENT')
  !      p_web.SSV('AddToAudit:Notes','ORIGINAL MSN: ' & p_web.GSV('job:MSN') & '<13,10>NEW MSN: ' & p_web.GSV('tmp:MSN'))
      addToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','MSN VERIFICATION: AMENDMENT','ORIGINAL MSN: ' & p_web.GSV('job:MSN') & '<13,10>NEW MSN: ' & p_web.GSV('tmp:MSN'))
      p_web.SSV('job:MSN',p_web.GSV('tmp:MSN'))
      p_web.SSV('Comment:MSN','Verified')
  else ! if (p_web.GSV('tmp:ConfirmMSNChange') = 'Y')
      p_web.SSV('Hide:ButtonVerifyMSN2',1)
      p_web.SSV('Hide:ConfirmMSNChange',1)
      p_web.SSV('Hide:ButtonVerifyMSN',0)
      p_web.SSV('Hide:VerifyMSN',1)
      p_web.SSV('tmp:ConfirmMSNChange','')
      p_web.SSV('Comment:MSN','Verify MSN')
      p_web.SSV('Comment:VerifyMSN','')
  end  !if (p_web.GSV('tmp:ConfirmMSNChange') = 'Y')
  do Value::tmp:ConfirmMSNChange
  do SendAlert
  do Prompt::tmp:ConfirmMSNChange
  do Comment::tmp:ConfirmMSNChange
  do Value::buttonVerifyMSN2  !1
  do Value::buttonVerifyMSN  !1
  do Prompt::tmp:MSN
  do Value::tmp:MSN  !1
  do Comment::tmp:MSN
  do Prompt::tmp:VerifyMSN
  do Value::tmp:VerifyMSN  !1
  do Comment::tmp:VerifyMSN

Value::tmp:ConfirmMSNChange  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:ConfirmMSNChange') & '_value',Choose(p_web.GSV('Hide:ConfirmMSNChange') =1 OR p_web.GSV('MSNValidated') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ConfirmMSNChange') =1 OR p_web.GSV('MSNValidated') = 1)
  ! --- RADIO --- tmp:ConfirmMSNChange
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:ConfirmMSNChange')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:ConfirmMSNChange') = 'Y'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:ConfirmMSNChange'',''jobfaultcodes_tmp:confirmmsnchange_value'',1,'''&clip('Y')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ConfirmMSNChange')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:ConfirmMSNChange',clip('Y'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:ConfirmMSNChange_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:ConfirmMSNChange') = 'N'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:ConfirmMSNChange'',''jobfaultcodes_tmp:confirmmsnchange_value'',1,'''&clip('N')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ConfirmMSNChange')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:ConfirmMSNChange',clip('N'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:ConfirmMSNChange_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:ConfirmMSNChange') & '_value')

Comment::tmp:ConfirmMSNChange  Routine
    loc:comment = ''
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:ConfirmMSNChange') & '_comment',Choose(p_web.GSV('Hide:ConfirmMSNChange') =1 OR p_web.GSV('MSNValidated') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ConfirmMSNChange') =1 OR p_web.GSV('MSNValidated') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:ConfirmMSNChange') & '_comment')

Prompt::job:ProductCode  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('job:ProductCode') & '_prompt',Choose(p_web.GSV('Hide:ProductCode') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Product Code')
  If p_web.GSV('Hide:ProductCode') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:ProductCode  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:ProductCode',p_web.GetValue('NewValue'))
    job:ProductCode = p_web.GetValue('NewValue') !FieldType= STRING Field = job:ProductCode
    do Value::job:ProductCode
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:ProductCode',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:ProductCode = p_web.GetValue('Value')
  End
    job:ProductCode = Upper(job:ProductCode)
    p_web.SetSessionValue('job:ProductCode',job:ProductCode)
  p_Web.SetValue('lookupfield','job:ProductCode')
  do AfterLookup
  do Value::job:ProductCode
  do SendAlert
  do Comment::job:ProductCode

Value::job:ProductCode  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('job:ProductCode') & '_value',Choose(p_web.GSV('Hide:ProductCode') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ProductCode') = 1)
  ! --- STRING --- job:ProductCode
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:ProductCode') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('ReadOnly:ProductCode') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:ProductCode')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:ProductCode'',''jobfaultcodes_job:productcode_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:ProductCode')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','job:ProductCode',p_web.GetSessionValue('job:ProductCode'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('LookupProductCodes')&'?LookupField=job:ProductCode&Tab=3&ForeignField=mop:ProductCode&_sort=mop:ProductCode&Refresh=sort&LookupFrom=JobFaultCodes&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('job:ProductCode') & '_value')

Comment::job:ProductCode  Routine
      loc:comment = ''
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('job:ProductCode') & '_comment',Choose(p_web.GSV('Hide:ProductCode') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ProductCode') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('job:ProductCode') & '_comment')

Prompt::tmp:FaultCode1  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode1') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode1') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode1'))
  If p_web.GSV('Hide:JobFaultCode1') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode1',p_web.GetValue('NewValue'))
    tmp:FaultCode1 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode1',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode1')))
    tmp:FaultCode1 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode1')) !
  End
  If tmp:FaultCode1 = '' and p_web.GSV('Req:JobFaultCode1') = 1
    loc:Invalid = 'tmp:FaultCode1'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode1')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode1
  do SendAlert

Value::tmp:FaultCode1  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode1') & '_value',Choose(p_web.GSV('Hide:JobFaultCode1') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode1') = 1)
  ! --- STRING --- tmp:FaultCode1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode1') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode1') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode1') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode1')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode1 = '' and (p_web.GSV('Req:JobFaultCode1') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode1'',''jobfaultcodes_tmp:faultcode1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode1',p_web.GetSessionValue('tmp:FaultCode1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode1'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(1)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode1') & '_value')

Comment::tmp:FaultCode1  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode1'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode1') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode1') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode1') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode2  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode2') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode2') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode2'))
  If p_web.GSV('Hide:JobFaultCode2') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode2',p_web.GetValue('NewValue'))
    tmp:FaultCode2 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode2',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode2')))
    tmp:FaultCode2 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode2')) !
  End
  If tmp:FaultCode2 = '' and p_web.GSV('Req:JobFaultCode2') = 1
    loc:Invalid = 'tmp:FaultCode2'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode2')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode2
  do SendAlert

Value::tmp:FaultCode2  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode2') & '_value',Choose(p_web.GSV('Hide:JobFaultCode2') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode2') = 1)
  ! --- STRING --- tmp:FaultCode2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode2') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode2') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode2') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode2')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode2 = '' and (p_web.GSV('Req:JobFaultCode2') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode2'',''jobfaultcodes_tmp:faultcode2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode2',p_web.GetSessionValue('tmp:FaultCode2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode2'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(2)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode2') & '_value')

Comment::tmp:FaultCode2  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode2'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode2') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode2') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode2') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode3  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode3') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode3') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode3'))
  If p_web.GSV('Hide:JobFaultCode3') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode3',p_web.GetValue('NewValue'))
    tmp:FaultCode3 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode3',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode3')))
    tmp:FaultCode3 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode3')) !
  End
  If tmp:FaultCode3 = '' and p_web.GSV('Req:JobFaultCode3') = 1
    loc:Invalid = 'tmp:FaultCode3'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode3')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode3
  do SendAlert

Value::tmp:FaultCode3  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode3') & '_value',Choose(p_web.GSV('Hide:JobFaultCode3') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode3') = 1)
  ! --- STRING --- tmp:FaultCode3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode3') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode3') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode3') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode3')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode3 = '' and (p_web.GSV('Req:JobFaultCode3') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode3'',''jobfaultcodes_tmp:faultcode3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode3',p_web.GetSessionValue('tmp:FaultCode3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode3'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(3)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode3') & '_value')

Comment::tmp:FaultCode3  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode3'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode3') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode3') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode3') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode4  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode4') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode4') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode4'))
  If p_web.GSV('Hide:JobFaultCode4') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode4',p_web.GetValue('NewValue'))
    tmp:FaultCode4 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode4',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode4')))
    tmp:FaultCode4 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode4')) !
  End
  If tmp:FaultCode4 = '' and p_web.GSV('Req:JobFaultCode4') = 1
    loc:Invalid = 'tmp:FaultCode4'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode4')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode4
  do SendAlert

Value::tmp:FaultCode4  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode4') & '_value',Choose(p_web.GSV('Hide:JobFaultCode4') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode4') = 1)
  ! --- STRING --- tmp:FaultCode4
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode4') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode4') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode4') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode4')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode4 = '' and (p_web.GSV('Req:JobFaultCode4') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode4'',''jobfaultcodes_tmp:faultcode4_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode4',p_web.GetSessionValue('tmp:FaultCode4'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode4'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(4)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode4') & '_value')

Comment::tmp:FaultCode4  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode4'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode4') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode4') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode4') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode5  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode5') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode5') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode5'))
  If p_web.GSV('Hide:JobFaultCode5') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode5  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode5',p_web.GetValue('NewValue'))
    tmp:FaultCode5 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode5
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode5',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode5')))
    tmp:FaultCode5 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode5')) !
  End
  If tmp:FaultCode5 = '' and p_web.GSV('Req:JobFaultCode5') = 1
    loc:Invalid = 'tmp:FaultCode5'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode5')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode5
  do SendAlert

Value::tmp:FaultCode5  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode5') & '_value',Choose(p_web.GSV('Hide:JobFaultCode5') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode5') = 1)
  ! --- STRING --- tmp:FaultCode5
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode5') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode5') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode5') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode5')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode5 = '' and (p_web.GSV('Req:JobFaultCode5') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode5'',''jobfaultcodes_tmp:faultcode5_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode5',p_web.GetSessionValue('tmp:FaultCode5'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode5'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(5)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode5') & '_value')

Comment::tmp:FaultCode5  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode5'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode5') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode5') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode5') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode6  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode6') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode6') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode6'))
  If p_web.GSV('Hide:JobFaultCode6') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode6  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode6',p_web.GetValue('NewValue'))
    tmp:FaultCode6 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode6
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode6',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode6')))
    tmp:FaultCode6 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode6')) !
  End
  If tmp:FaultCode6 = '' and p_web.GSV('Req:JobFaultCode6') = 1
    loc:Invalid = 'tmp:FaultCode6'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode6')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode6
  do SendAlert

Value::tmp:FaultCode6  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode6') & '_value',Choose(p_web.GSV('Hide:JobFaultCode6') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode6') = 1)
  ! --- STRING --- tmp:FaultCode6
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode6') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode6') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode6') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode6')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode6 = '' and (p_web.GSV('Req:JobFaultCode6') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode6'',''jobfaultcodes_tmp:faultcode6_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode6',p_web.GetSessionValue('tmp:FaultCode6'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode6'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(6)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode6') & '_value')

Comment::tmp:FaultCode6  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode6'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode6') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode6') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode6') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode7  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode7') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode7') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode7'))
  If p_web.GSV('Hide:JobFaultCode7') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode7  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode7',p_web.GetValue('NewValue'))
    tmp:FaultCode7 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode7
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode7',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode7')))
    tmp:FaultCode7 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode7')) !
  End
  If tmp:FaultCode7 = '' and p_web.GSV('Req:JobFaultCode7') = 1
    loc:Invalid = 'tmp:FaultCode7'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode7')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode7
  do SendAlert

Value::tmp:FaultCode7  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode7') & '_value',Choose(p_web.GSV('Hide:JobFaultCode7') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode7') = 1)
  ! --- STRING --- tmp:FaultCode7
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode7') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode7') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode7') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode7')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode7 = '' and (p_web.GSV('Req:JobFaultCode7') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode7'',''jobfaultcodes_tmp:faultcode7_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode7',p_web.GetSessionValue('tmp:FaultCode7'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode7'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(7)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode7') & '_value')

Comment::tmp:FaultCode7  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode7'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode7') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode7') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode7') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode8  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode8') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode8') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode8'))
  If p_web.GSV('Hide:JobFaultCode8') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode8  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode8',p_web.GetValue('NewValue'))
    tmp:FaultCode8 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode8
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode8',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode8')))
    tmp:FaultCode8 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode8')) !
  End
  If tmp:FaultCode8 = '' and p_web.GSV('Req:JobFaultCode8') = 1
    loc:Invalid = 'tmp:FaultCode8'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode8')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode8
  do SendAlert

Value::tmp:FaultCode8  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode8') & '_value',Choose(p_web.GSV('Hide:JobFaultCode8') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode8') = 1)
  ! --- STRING --- tmp:FaultCode8
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode8') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode8') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode8') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode8')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode8 = '' and (p_web.GSV('Req:JobFaultCode8') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode8'',''jobfaultcodes_tmp:faultcode8_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode8',p_web.GetSessionValue('tmp:FaultCode8'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode8'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(8)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode8') & '_value')

Comment::tmp:FaultCode8  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode8'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode8') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode8') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode8') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode9  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode9') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode9') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode9'))
  If p_web.GSV('Hide:JobFaultCode9') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode9  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode9',p_web.GetValue('NewValue'))
    tmp:FaultCode9 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode9
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode9',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode9')))
    tmp:FaultCode9 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode9')) !
  End
  If tmp:FaultCode9 = '' and p_web.GSV('Req:JobFaultCode9') = 1
    loc:Invalid = 'tmp:FaultCode9'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode9')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode9
  do SendAlert

Value::tmp:FaultCode9  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode9') & '_value',Choose(p_web.GSV('Hide:JobFaultCode9') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode9') = 1)
  ! --- STRING --- tmp:FaultCode9
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode9') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode9') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode9') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode9')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode9 = '' and (p_web.GSV('Req:JobFaultCode9') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode9'',''jobfaultcodes_tmp:faultcode9_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode9',p_web.GetSessionValue('tmp:FaultCode9'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode9'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(9)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode9') & '_value')

Comment::tmp:FaultCode9  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode9'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode9') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode9') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode9') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode10  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode10') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode10') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode10'))
  If p_web.GSV('Hide:JobFaultCode10') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode10  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode10',p_web.GetValue('NewValue'))
    tmp:FaultCode10 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode10
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode10',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode10')))
    tmp:FaultCode10 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode10')) !
  End
  If tmp:FaultCode10 = '' and p_web.GSV('Req:JobFaultCode10') = 1
    loc:Invalid = 'tmp:FaultCode10'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode10')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode10
  do SendAlert

Value::tmp:FaultCode10  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode10') & '_value',Choose(p_web.GSV('Hide:JobFaultCode10') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode10') = 1)
  ! --- STRING --- tmp:FaultCode10
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode10') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode10') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode10') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode10')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode10 = '' and (p_web.GSV('Req:JobFaultCode10') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode10'',''jobfaultcodes_tmp:faultcode10_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode10',p_web.GetSessionValue('tmp:FaultCode10'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode10'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(10)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode10') & '_value')

Comment::tmp:FaultCode10  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode10'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode10') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode10') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode10') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode11  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode11') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode11') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode11'))
  If p_web.GSV('Hide:JobFaultCode11') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode11  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode11',p_web.GetValue('NewValue'))
    tmp:FaultCode11 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode11
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode11',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode11')))
    tmp:FaultCode11 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode11')) !
  End
  If tmp:FaultCode11 = '' and p_web.GSV('Req:JobFaultCode11') = 1
    loc:Invalid = 'tmp:FaultCode11'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode11')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode11
  do SendAlert

Value::tmp:FaultCode11  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode11') & '_value',Choose(p_web.GSV('Hide:JobFaultCode11') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode11') = 1)
  ! --- STRING --- tmp:FaultCode11
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode11') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode11') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode11') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode11')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode11 = '' and (p_web.GSV('Req:JobFaultCode11') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode11'',''jobfaultcodes_tmp:faultcode11_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode11',p_web.GetSessionValue('tmp:FaultCode11'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode11'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(11)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode11') & '_value')

Comment::tmp:FaultCode11  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode11'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode11') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode11') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode11') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode12  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode12') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode12') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode12'))
  If p_web.GSV('Hide:JobFaultCode12') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode12  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode12',p_web.GetValue('NewValue'))
    tmp:FaultCode12 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode12
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode12',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode12')))
    tmp:FaultCode12 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode12')) !
  End
  If tmp:FaultCode12 = '' and p_web.GSV('Req:JobFaultCode12') = 1
    loc:Invalid = 'tmp:FaultCode12'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode12')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode12
  do SendAlert

Value::tmp:FaultCode12  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode12') & '_value',Choose(p_web.GSV('Hide:JobFaultCode12') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode12') = 1)
  ! --- STRING --- tmp:FaultCode12
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode12') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode12') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode12') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode12')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode12 = '' and (p_web.GSV('Req:JobFaultCode12') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode12'',''jobfaultcodes_tmp:faultcode12_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode12',p_web.GetSessionValue('tmp:FaultCode12'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode12'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(12)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode12') & '_value')

Comment::tmp:FaultCode12  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode12'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode12') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode12') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode12') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode13  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode13') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode13') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode13'))
  If p_web.GSV('Hide:JobFaultCode13') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode13  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode13',p_web.GetValue('NewValue'))
    tmp:FaultCode13 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode13
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode13',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode13')))
    tmp:FaultCode13 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode13')) !
  End
  If tmp:FaultCode13 = '' and p_web.GSV('Req:JobFaultCode13') = 1
    loc:Invalid = 'tmp:FaultCode13'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode13')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode13
  do SendAlert

Value::tmp:FaultCode13  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode13') & '_value',Choose(p_web.GSV('Hide:JobFaultCode13') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode13') = 1)
  ! --- STRING --- tmp:FaultCode13
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode13') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode13') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode13') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode13')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode13 = '' and (p_web.GSV('Req:JobFaultCode13') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode13'',''jobfaultcodes_tmp:faultcode13_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode13',p_web.GetSessionValue('tmp:FaultCode13'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode13'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(13)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode13') & '_value')

Comment::tmp:FaultCode13  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode13'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode13') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode13') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode13') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode14  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode14') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode14') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode14'))
  If p_web.GSV('Hide:JobFaultCode14') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode14  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode14',p_web.GetValue('NewValue'))
    tmp:FaultCode14 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode14
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode14',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode14')))
    tmp:FaultCode14 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode14')) !
  End
  If tmp:FaultCode14 = '' and p_web.GSV('Req:JobFaultCode14') = 1
    loc:Invalid = 'tmp:FaultCode14'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode14')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode14
  do SendAlert

Value::tmp:FaultCode14  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode14') & '_value',Choose(p_web.GSV('Hide:JobFaultCode14') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode14') = 1)
  ! --- STRING --- tmp:FaultCode14
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode14') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode14') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode14') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode14')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode14 = '' and (p_web.GSV('Req:JobFaultCode14') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode14'',''jobfaultcodes_tmp:faultcode14_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode14',p_web.GetSessionValue('tmp:FaultCode14'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode14'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(14)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode14') & '_value')

Comment::tmp:FaultCode14  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode14'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode14') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode14') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode14') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode15  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode15') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode15') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode15'))
  If p_web.GSV('Hide:JobFaultCode15') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode15  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode15',p_web.GetValue('NewValue'))
    tmp:FaultCode15 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode15
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode15',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode15')))
    tmp:FaultCode15 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode15')) !
  End
  If tmp:FaultCode15 = '' and p_web.GSV('Req:JobFaultCode15') = 1
    loc:Invalid = 'tmp:FaultCode15'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode15')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode15
  do SendAlert

Value::tmp:FaultCode15  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode15') & '_value',Choose(p_web.GSV('Hide:JobFaultCode15') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode15') = 1)
  ! --- STRING --- tmp:FaultCode15
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode15') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode15') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode15') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode15')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode15 = '' and (p_web.GSV('Req:JobFaultCode15') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode15'',''jobfaultcodes_tmp:faultcode15_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode15',p_web.GetSessionValue('tmp:FaultCode15'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode15'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(15)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode15') & '_value')

Comment::tmp:FaultCode15  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode15'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode15') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode15') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode15') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode16  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode16') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode16') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode16'))
  If p_web.GSV('Hide:JobFaultCode16') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode16  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode16',p_web.GetValue('NewValue'))
    tmp:FaultCode16 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode16
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode16',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode16')))
    tmp:FaultCode16 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode16')) !
  End
  If tmp:FaultCode16 = '' and p_web.GSV('Req:JobFaultCode16') = 1
    loc:Invalid = 'tmp:FaultCode16'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode16')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode16
  do SendAlert

Value::tmp:FaultCode16  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode16') & '_value',Choose(p_web.GSV('Hide:JobFaultCode16') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode16') = 1)
  ! --- STRING --- tmp:FaultCode16
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode16') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode16') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode16') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode16')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode16 = '' and (p_web.GSV('Req:JobFaultCode16') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode16'',''jobfaultcodes_tmp:faultcode16_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode16',p_web.GetSessionValue('tmp:FaultCode16'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode16'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(16)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode16') & '_value')

Comment::tmp:FaultCode16  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode16'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode16') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode16') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode16') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode17  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode17') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode17') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode17'))
  If p_web.GSV('Hide:JobFaultCode17') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode17  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode17',p_web.GetValue('NewValue'))
    tmp:FaultCode17 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode17
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode17',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode17')))
    tmp:FaultCode17 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode17')) !
  End
  If tmp:FaultCode17 = '' and p_web.GSV('Req:JobFaultCode17') = 1
    loc:Invalid = 'tmp:FaultCode17'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode17')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode17
  do SendAlert

Value::tmp:FaultCode17  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode17') & '_value',Choose(p_web.GSV('Hide:JobFaultCode17') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode17') = 1)
  ! --- STRING --- tmp:FaultCode17
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode17') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode17') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode17') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode17')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode17 = '' and (p_web.GSV('Req:JobFaultCode17') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode17'',''jobfaultcodes_tmp:faultcode17_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode17',p_web.GetSessionValue('tmp:FaultCode17'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode17'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(17)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode17') & '_value')

Comment::tmp:FaultCode17  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode17'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode17') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode17') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode17') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode18  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode18') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode18') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode18'))
  If p_web.GSV('Hide:JobFaultCode18') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode18  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode18',p_web.GetValue('NewValue'))
    tmp:FaultCode18 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode18
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode18',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode18')))
    tmp:FaultCode18 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode18')) !
  End
  If tmp:FaultCode18 = '' and p_web.GSV('Req:JobFaultCode18') = 1
    loc:Invalid = 'tmp:FaultCode18'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode18')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode18
  do SendAlert

Value::tmp:FaultCode18  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode18') & '_value',Choose(p_web.GSV('Hide:JobFaultCode18') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode18') = 1)
  ! --- STRING --- tmp:FaultCode18
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode18') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode18') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode18') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode18')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode18 = '' and (p_web.GSV('Req:JobFaultCode18') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode18'',''jobfaultcodes_tmp:faultcode18_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode18',p_web.GetSessionValue('tmp:FaultCode18'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode18'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(18)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode18') & '_value')

Comment::tmp:FaultCode18  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode18'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode18') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode18') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode18') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode19  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode19') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode19') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode19'))
  If p_web.GSV('Hide:JobFaultCode19') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode19  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode19',p_web.GetValue('NewValue'))
    tmp:FaultCode19 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode19
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode19',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode19')))
    tmp:FaultCode19 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode19')) !
  End
  If tmp:FaultCode19 = '' and p_web.GSV('Req:JobFaultCode19') = 1
    loc:Invalid = 'tmp:FaultCode19'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode19')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode19
  do SendAlert

Value::tmp:FaultCode19  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode19') & '_value',Choose(p_web.GSV('Hide:JobFaultCode19') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode19') = 1)
  ! --- STRING --- tmp:FaultCode19
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode19') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode19') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode19') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode19')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode19 = '' and (p_web.GSV('Req:JobFaultCode19') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode19'',''jobfaultcodes_tmp:faultcode19_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode19',p_web.GetSessionValue('tmp:FaultCode19'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode19'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(19)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode19') & '_value')

Comment::tmp:FaultCode19  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode19'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode19') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode19') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode19') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:FaultCode20  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode20') & '_prompt',Choose(p_web.GSV('Hide:JobFaultCode20') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:JobFaultCode20'))
  If p_web.GSV('Hide:JobFaultCode20') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:FaultCode20  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:FaultCode20',p_web.GetValue('NewValue'))
    tmp:FaultCode20 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:FaultCode20
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:FaultCode20',p_web.dFormat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode20')))
    tmp:FaultCode20 = p_web.Dformat(p_web.GetValue('Value'),p_web.GSV('Picture:JobFaultCode20')) !
  End
  If tmp:FaultCode20 = '' and p_web.GSV('Req:JobFaultCode20') = 1
    loc:Invalid = 'tmp:FaultCode20'
    loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode20')) & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:FaultCode20
  do SendAlert

Value::tmp:FaultCode20  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode20') & '_value',Choose(p_web.GSV('Hide:JobFaultCode20') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:JobFaultCode20') = 1)
  ! --- STRING --- tmp:FaultCode20
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:JobFaultCode20') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:JobFaultCode20') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('Req:JobFaultCode20') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:FaultCode20')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:FaultCode20 = '' and (p_web.GSV('Req:JobFaultCode20') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:FaultCode20'',''jobfaultcodes_tmp:faultcode20_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:FaultCode20',p_web.GetSessionValue('tmp:FaultCode20'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),p_web.GSV('Picture:JobFaultCode20'),loc:javascript,,) & '<13,10>'
  do SendPacket
  local.SetLookupButton(20)
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode20') & '_value')

Comment::tmp:FaultCode20  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:JobFaultCode20'))
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:FaultCode20') & '_comment',Choose(p_web.GSV('Hide:JobFaultCode20') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:JobFaultCode20') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe3:FaultCode21  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('jobe3:FaultCode21') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Specific Needs Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe3:FaultCode21  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe3:FaultCode21',p_web.GetValue('NewValue'))
    jobe3:FaultCode21 = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe3:FaultCode21
    do Value::jobe3:FaultCode21
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe3:FaultCode21',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe3:FaultCode21 = p_web.GetValue('Value')
  End
  do Value::jobe3:FaultCode21
  do SendAlert

Value::jobe3:FaultCode21  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('jobe3:FaultCode21') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- jobe3:FaultCode21
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('jobe3:FaultCode21')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('jobe3:FaultCode21') = 'HEARING IMPAIRED'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe3:FaultCode21'',''jobfaultcodes_jobe3:faultcode21_value'',1,'''&clip('HEARING IMPAIRED')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','jobe3:FaultCode21',clip('HEARING IMPAIRED'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Specific Needs Type','jobe3:FaultCode21_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Hearing Impaired') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('jobe3:FaultCode21') = 'VISUAL IMPAIRMENT'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe3:FaultCode21'',''jobfaultcodes_jobe3:faultcode21_value'',1,'''&clip('VISUAL IMPAIRMENT')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','jobe3:FaultCode21',clip('VISUAL IMPAIRMENT'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Specific Needs Type','jobe3:FaultCode21_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Visual Impairment') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('jobe3:FaultCode21') = 'ELDERLY'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe3:FaultCode21'',''jobfaultcodes_jobe3:faultcode21_value'',1,'''&clip('ELDERLY')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','jobe3:FaultCode21',clip('ELDERLY'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,'Specific Needs Type','jobe3:FaultCode21_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Elderly') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('jobe3:FaultCode21') & '_value')

Comment::jobe3:FaultCode21  Routine
    loc:comment = ''
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('jobe3:FaultCode21') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe3:SN_Software_Installed  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('jobe3:SN_Software_Installed') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('SN Software Installed')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe3:SN_Software_Installed  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe3:SN_Software_Installed',p_web.GetValue('NewValue'))
    jobe3:SN_Software_Installed = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe3:SN_Software_Installed
    do Value::jobe3:SN_Software_Installed
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe3:SN_Software_Installed',p_web.GetValue('Value'))
    jobe3:SN_Software_Installed = p_web.GetValue('Value')
  End
  do Value::jobe3:SN_Software_Installed
  do SendAlert

Value::jobe3:SN_Software_Installed  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('jobe3:SN_Software_Installed') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- jobe3:SN_Software_Installed
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe3:SN_Software_Installed'',''jobfaultcodes_jobe3:sn_software_installed_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('job:Date_Completed') > 0,'disabled','')
  If p_web.GetSessionValue('jobe3:SN_Software_Installed') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe3:SN_Software_Installed',clip(1),,loc:readonly,,,loc:javascript,,'SN Software Installed') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('jobe3:SN_Software_Installed') & '_value')

Comment::jobe3:SN_Software_Installed  Routine
    loc:comment = ''
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('jobe3:SN_Software_Installed') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonRepairNotes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonRepairNotes',p_web.GetValue('NewValue'))
    do Value::buttonRepairNotes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonRepairNotes  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('buttonRepairNotes') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','RepairNotes','Repair Notes','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('BrowseRepairNotes')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()

Comment::buttonRepairNotes  Routine
    loc:comment = ''
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('buttonRepairNotes') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonOutFaults  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonOutFaults',p_web.GetValue('NewValue'))
    do Value::buttonOutFaults
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonOutFaults  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('buttonOutFaults') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','OutFaults','Edit Out Faults List','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('DisplayOutFaults')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()

Comment::buttonOutFaults  Routine
    loc:comment = ''
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('buttonOutFaults') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::promptExchange  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('promptExchange',p_web.GetValue('NewValue'))
    do Value::promptExchange
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::promptExchange  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('promptExchange') & '_value',Choose(p_web.GSV('Hide:ExchangePrompt') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ExchangePrompt') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web.Translate('You have requested an Exchange Unit for this repair.',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::promptExchange  Routine
    loc:comment = ''
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('promptExchange') & '_comment',Choose(p_web.GSV('Hide:ExchangePrompt') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ExchangePrompt') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:processExchange  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:processExchange') & '_prompt',Choose(p_web.GSV('Hide:ExchangePrompt') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Process Exchange Request?')
  If p_web.GSV('Hide:ExchangePrompt') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:processExchange  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:processExchange',p_web.GetValue('NewValue'))
    tmp:processExchange = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:processExchange
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:processExchange',p_web.GetValue('Value'))
    tmp:processExchange = p_web.GetValue('Value')
  End
  do Value::tmp:processExchange
  do SendAlert

Value::tmp:processExchange  Routine
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:processExchange') & '_value',Choose(p_web.GSV('Hide:ExchangePrompt') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ExchangePrompt') = 1)
  ! --- RADIO --- tmp:processExchange
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:processExchange')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  if p_web.GSV('job:Warranty_Job') = 'YES'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:processExchange') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:processExchange'',''jobfaultcodes_tmp:processexchange_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:processExchange',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:processExchange_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Add Warranty Exchange') & '<13,10>'
    packet = clip(packet) & p_web.br
  end
  if p_web.GSV('job:Chargeable_Job') = 'YES'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:processExchange') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:processExchange'',''jobfaultcodes_tmp:processexchange_value'',1,'''&clip(2)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:processExchange',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:processExchange_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Add Chargeable Exchange') & '<13,10>'
    packet = clip(packet) & p_web.br
  end
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:processExchange') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:processExchange'',''jobfaultcodes_tmp:processexchange_value'',1,'''&clip(3)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:processExchange',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:processExchange_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Cancel Exchange Request') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobFaultCodes_' & p_web._nocolon('tmp:processExchange') & '_value')

Comment::tmp:processExchange  Routine
    loc:comment = ''
  p_web._DivHeader('JobFaultCodes_' & p_web._nocolon('tmp:processExchange') & '_comment',Choose(p_web.GSV('Hide:ExchangePrompt') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ExchangePrompt') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('JobFaultCodes_tmp:MSN_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:MSN
      else
        do Value::tmp:MSN
      end
  of lower('JobFaultCodes_buttonVerifyMSN_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonVerifyMSN
      else
        do Value::buttonVerifyMSN
      end
  of lower('JobFaultCodes_tmp:VerifyMSN_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:VerifyMSN
      else
        do Value::tmp:VerifyMSN
      end
  of lower('JobFaultCodes_buttonVerifyMSN2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonVerifyMSN2
      else
        do Value::buttonVerifyMSN2
      end
  of lower('JobFaultCodes_tmp:ConfirmMSNChange_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ConfirmMSNChange
      else
        do Value::tmp:ConfirmMSNChange
      end
  of lower('JobFaultCodes_job:ProductCode_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:ProductCode
      else
        do Value::job:ProductCode
      end
  of lower('JobFaultCodes_tmp:FaultCode1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode1
      else
        do Value::tmp:FaultCode1
      end
  of lower('JobFaultCodes_tmp:FaultCode2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode2
      else
        do Value::tmp:FaultCode2
      end
  of lower('JobFaultCodes_tmp:FaultCode3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode3
      else
        do Value::tmp:FaultCode3
      end
  of lower('JobFaultCodes_tmp:FaultCode4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode4
      else
        do Value::tmp:FaultCode4
      end
  of lower('JobFaultCodes_tmp:FaultCode5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode5
      else
        do Value::tmp:FaultCode5
      end
  of lower('JobFaultCodes_tmp:FaultCode6_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode6
      else
        do Value::tmp:FaultCode6
      end
  of lower('JobFaultCodes_tmp:FaultCode7_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode7
      else
        do Value::tmp:FaultCode7
      end
  of lower('JobFaultCodes_tmp:FaultCode8_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode8
      else
        do Value::tmp:FaultCode8
      end
  of lower('JobFaultCodes_tmp:FaultCode9_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode9
      else
        do Value::tmp:FaultCode9
      end
  of lower('JobFaultCodes_tmp:FaultCode10_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode10
      else
        do Value::tmp:FaultCode10
      end
  of lower('JobFaultCodes_tmp:FaultCode11_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode11
      else
        do Value::tmp:FaultCode11
      end
  of lower('JobFaultCodes_tmp:FaultCode12_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode12
      else
        do Value::tmp:FaultCode12
      end
  of lower('JobFaultCodes_tmp:FaultCode13_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode13
      else
        do Value::tmp:FaultCode13
      end
  of lower('JobFaultCodes_tmp:FaultCode14_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode14
      else
        do Value::tmp:FaultCode14
      end
  of lower('JobFaultCodes_tmp:FaultCode15_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode15
      else
        do Value::tmp:FaultCode15
      end
  of lower('JobFaultCodes_tmp:FaultCode16_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode16
      else
        do Value::tmp:FaultCode16
      end
  of lower('JobFaultCodes_tmp:FaultCode17_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode17
      else
        do Value::tmp:FaultCode17
      end
  of lower('JobFaultCodes_tmp:FaultCode18_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode18
      else
        do Value::tmp:FaultCode18
      end
  of lower('JobFaultCodes_tmp:FaultCode19_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode19
      else
        do Value::tmp:FaultCode19
      end
  of lower('JobFaultCodes_tmp:FaultCode20_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:FaultCode20
      else
        do Value::tmp:FaultCode20
      end
  of lower('JobFaultCodes_jobe3:FaultCode21_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe3:FaultCode21
      else
        do Value::jobe3:FaultCode21
      end
  of lower('JobFaultCodes_jobe3:SN_Software_Installed_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe3:SN_Software_Installed
      else
        do Value::jobe3:SN_Software_Installed
      end
  of lower('JobFaultCodes_tmp:processExchange_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:processExchange
      else
        do Value::tmp:processExchange
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('JobFaultCodes_form:ready_',1)
  p_web.SetSessionValue('JobFaultCodes_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_JobFaultCodes',0)

PreCopy  Routine
  p_web.SetValue('JobFaultCodes_form:ready_',1)
  p_web.SetSessionValue('JobFaultCodes_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_JobFaultCodes',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('JobFaultCodes_form:ready_',1)
  p_web.SetSessionValue('JobFaultCodes_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('JobFaultCodes:Primed',0)

PreDelete       Routine
  p_web.SetValue('JobFaultCodes_form:ready_',1)
  p_web.SetSessionValue('JobFaultCodes_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('JobFaultCodes:Primed',0)
  p_web.setsessionvalue('showtab_JobFaultCodes',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('MSNValidation') = 1
  End
    If (p_web.GSV('jobe3:SpecificNeeds') = 1)
        If (p_web.GSV('job:Date_Completed') > 0)
          If p_web.IfExistsValue('jobe3:SN_Software_Installed') = 0
            p_web.SetValue('jobe3:SN_Software_Installed',0)
            jobe3:SN_Software_Installed = 0
          End
        End
    End
  If p_web.gsv('HideTools') = 0
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('JobFaultCodes_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
      if (p_web.GSV('MSNValidation') = 1 And p_web.GSV('MSNValidated') = 0)
          loc:alert = 'You must Verify the MSN'
          loc:invalid = 'tmp:MSN'
          exit
      end ! if (p_web.GSV('MSNValidation') = 1 And p_web.GSV('MSNValidated') = 0)
  
      ! Double Check Fault Code Values
  
      loop x# = 1 to 20
          if (p_web.GSV('Hide:JobFaultCode' & x#) = 1 or |
              p_web.GSV('ReadOnly:JobFaultCode' & x#) = 1 or |
              p_web.GSV('Lookup:JobFaultCode' & x#) <> 1 or |
              p_web.GSV('tmp:FaultCode' & x#) = '')
              cycle
          end ! p_web.GSV('ReadOnly:JobFaultCode' & x#) = 1)
          
  
          Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
          maf:ScreenOrder    = x#
          maf:Manufacturer    = p_web.GSV('job:Manufacturer')
          if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
              ! Found
              if (maf:Lookup = 'YES' and maf:Force_Lookup = 'YES' and maf:MainFault <> 1)
                  Access:MANFAULO.Clearkey(mfo:HideFieldKey)
                  mfo:NotAvailable    = 0
                  mfo:Manufacturer    = p_web.GSV('job:Manufacturer')
                  mfo:Field_Number    = maf:Field_Number
                  mfo:Field    = p_web.GSV('tmp:FaultCode' & x#)
                  if (Access:MANFAULO.TryFetch(mfo:HideFieldKey) = Level:Benign)
                      ! Found
                  else ! if (Access:MANFAULO.TryFetch(mfo:HideFieldKey) = Level:Benign)
                      ! Error
                      loc:Invalid = 'tmp:FaultCode' & x#
                      loc:Alert = 'Invalid Fault Code: ' & p_web.GSV('Prompt:JobFaultCode' & x#)
                      break
                  end ! if (Access:MANFAULO.TryFetch(mfo:HideFieldKey) = Level:Benign)
              end ! if (maf:Lookup = 'YES' and maf:Force_Lookup = 'YES')
          else ! if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
              ! Error
          end ! if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)        
      end ! loop x# = 1 to 20
  
      if (loc:Invalid = '' And p_web.GSV('Hide:ExchangePrompt') = 0 And p_web.GSV('tmp:ProcessExchange') < 1)
          loc:alert = 'Please select how you wish to process the Exchange Request'
          loc:invalid = 'tmp:promptExchange'
          exit
      end ! if (loc:Invalid = '' And p_web.GSV('Hide:ExchangePrompt') = 0 And p_web.GSV('tmp:ProcessExchange') = 0)
  
  
  p_web.DeleteSessionValue('JobFaultCodes_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
  If p_web.GSV('MSNValidation') = 1
    loc:InvalidTab += 1
        If tmp:MSN = ''
          loc:Invalid = 'tmp:MSN'
          loc:alert = p_web.translate('M.S.N.') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:MSN = Upper(tmp:MSN)
          p_web.SetSessionValue('tmp:MSN',tmp:MSN)
        If loc:Invalid <> '' then exit.
      If not (p_web.GSV('Hide:VerifyMSN') = 1 or p_web.GSV('MSNValidated') = 1)
        If tmp:VerifyMSN = ''
          loc:Invalid = 'tmp:VerifyMSN'
          loc:alert = p_web.translate('Verify M.S.N.') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:VerifyMSN = Upper(tmp:VerifyMSN)
          p_web.SetSessionValue('tmp:VerifyMSN',tmp:VerifyMSN)
        If loc:Invalid <> '' then exit.
      End
  End
  ! tab = 2
    loc:InvalidTab += 1
      If not (p_web.GSV('Hide:ProductCode') = 1)
          job:ProductCode = Upper(job:ProductCode)
          p_web.SetSessionValue('job:ProductCode',job:ProductCode)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode1') = 1)
        If tmp:FaultCode1 = '' and p_web.GSV('Req:JobFaultCode1') = 1
          loc:Invalid = 'tmp:FaultCode1'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode1')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode2') = 1)
        If tmp:FaultCode2 = '' and p_web.GSV('Req:JobFaultCode2') = 1
          loc:Invalid = 'tmp:FaultCode2'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode2')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode3') = 1)
        If tmp:FaultCode3 = '' and p_web.GSV('Req:JobFaultCode3') = 1
          loc:Invalid = 'tmp:FaultCode3'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode3')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode4') = 1)
        If tmp:FaultCode4 = '' and p_web.GSV('Req:JobFaultCode4') = 1
          loc:Invalid = 'tmp:FaultCode4'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode4')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode5') = 1)
        If tmp:FaultCode5 = '' and p_web.GSV('Req:JobFaultCode5') = 1
          loc:Invalid = 'tmp:FaultCode5'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode5')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode6') = 1)
        If tmp:FaultCode6 = '' and p_web.GSV('Req:JobFaultCode6') = 1
          loc:Invalid = 'tmp:FaultCode6'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode6')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode7') = 1)
        If tmp:FaultCode7 = '' and p_web.GSV('Req:JobFaultCode7') = 1
          loc:Invalid = 'tmp:FaultCode7'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode7')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode8') = 1)
        If tmp:FaultCode8 = '' and p_web.GSV('Req:JobFaultCode8') = 1
          loc:Invalid = 'tmp:FaultCode8'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode8')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode9') = 1)
        If tmp:FaultCode9 = '' and p_web.GSV('Req:JobFaultCode9') = 1
          loc:Invalid = 'tmp:FaultCode9'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode9')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode10') = 1)
        If tmp:FaultCode10 = '' and p_web.GSV('Req:JobFaultCode10') = 1
          loc:Invalid = 'tmp:FaultCode10'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode10')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode11') = 1)
        If tmp:FaultCode11 = '' and p_web.GSV('Req:JobFaultCode11') = 1
          loc:Invalid = 'tmp:FaultCode11'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode11')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode12') = 1)
        If tmp:FaultCode12 = '' and p_web.GSV('Req:JobFaultCode12') = 1
          loc:Invalid = 'tmp:FaultCode12'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode12')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode13') = 1)
        If tmp:FaultCode13 = '' and p_web.GSV('Req:JobFaultCode13') = 1
          loc:Invalid = 'tmp:FaultCode13'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode13')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode14') = 1)
        If tmp:FaultCode14 = '' and p_web.GSV('Req:JobFaultCode14') = 1
          loc:Invalid = 'tmp:FaultCode14'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode14')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode15') = 1)
        If tmp:FaultCode15 = '' and p_web.GSV('Req:JobFaultCode15') = 1
          loc:Invalid = 'tmp:FaultCode15'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode15')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode16') = 1)
        If tmp:FaultCode16 = '' and p_web.GSV('Req:JobFaultCode16') = 1
          loc:Invalid = 'tmp:FaultCode16'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode16')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode17') = 1)
        If tmp:FaultCode17 = '' and p_web.GSV('Req:JobFaultCode17') = 1
          loc:Invalid = 'tmp:FaultCode17'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode17')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode18') = 1)
        If tmp:FaultCode18 = '' and p_web.GSV('Req:JobFaultCode18') = 1
          loc:Invalid = 'tmp:FaultCode18'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode18')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode19') = 1)
        If tmp:FaultCode19 = '' and p_web.GSV('Req:JobFaultCode19') = 1
          loc:Invalid = 'tmp:FaultCode19'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode19')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:JobFaultCode20') = 1)
        If tmp:FaultCode20 = '' and p_web.GSV('Req:JobFaultCode20') = 1
          loc:Invalid = 'tmp:FaultCode20'
          loc:alert = p_web.translate(p_web.GSV('Prompt:JobFaultCode20')) & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
  ! tab = 3
  If p_web.gsv('HideTools') = 0
    loc:InvalidTab += 1
  End
  ! tab = 4
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
      !Write Back The Fault Codes
      loop x# = 1 to 20
          Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
          maf:Manufacturer = p_web.GSV('job:Manufacturer')
          maf:ScreenOrder    = x#
          if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
              ! Found
              if (p_web.GSV('Hide:JobFaultCode' & x#) <> 1 and p_web.GSV('ReadOnly:JobFaultCode' & x#) <> 1)
                  if (maf:Field_Number < 13)
                      p_web.SSV('job:Fault_Code' & maf:Field_Number,p_web.GSV('tmp:FaultCode' & x#))
                  else ! if (maf:Field_Number < 13)
                      p_web.SSV('wob:FaultCode' & maf:Field_Number,p_web.GSV('tmp:FaultCode' & x#))
                  end ! if (maf:Field_Number < 13)
              end !if (p_web.GSV('Hide:JobFaultCode') <> 1)
          else ! if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
              ! Error
          end ! if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
      end !loop x# = 1 to 20
  
      do ValidateFaultCodes
  
      IF (p_web.GSV('Job:ViewOnly') = 1)
          ! Don't call POP screen if view only job.
          if p_web.gsv('ReturnURL') = 'QAProcedure' THEN
              p_web.SSV('locNextURL','QAProcedure')
          ELSE    
              p_web.SSV('locNextURL','BillingConfirmation')
          END          
          !p_web.SSV('locNextURL','BillingConfirmation')
      END
  
  
      if (p_web.GSV('Hide:ExchangePrompt') = 0)
          if (p_web.GSV('tmp:processExchange') = 1)
              local.AllocateExchangePart('CHA',0,0)
          end
          if (p_web.GSV('tmp:processExchange') = 2)
              local.AllocateExchangePart('CHA',0,0)
          end
      end
  
  
    p_web.deletesessionvalue('JobFaultCodes:FirstTime')
    
    IF (p_web.GSV('soc') = 1)
        ! Save Back To Job Record
        SaveJob(p_web)
    END ! IF
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('JobFaultCodes:Primed',0)
  p_web.StoreValue('tmp:MSN')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:VerifyMSN')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ConfirmMSNChange')
  p_web.StoreValue('job:ProductCode')
  p_web.StoreValue('tmp:FaultCode1')
  p_web.StoreValue('tmp:FaultCode2')
  p_web.StoreValue('tmp:FaultCode3')
  p_web.StoreValue('tmp:FaultCode4')
  p_web.StoreValue('tmp:FaultCode5')
  p_web.StoreValue('tmp:FaultCode6')
  p_web.StoreValue('tmp:FaultCode7')
  p_web.StoreValue('tmp:FaultCode8')
  p_web.StoreValue('tmp:FaultCode9')
  p_web.StoreValue('tmp:FaultCode10')
  p_web.StoreValue('tmp:FaultCode11')
  p_web.StoreValue('tmp:FaultCode12')
  p_web.StoreValue('tmp:FaultCode13')
  p_web.StoreValue('tmp:FaultCode14')
  p_web.StoreValue('tmp:FaultCode15')
  p_web.StoreValue('tmp:FaultCode16')
  p_web.StoreValue('tmp:FaultCode17')
  p_web.StoreValue('tmp:FaultCode18')
  p_web.StoreValue('tmp:FaultCode19')
  p_web.StoreValue('tmp:FaultCode20')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:processExchange')
local.AfterFaultCodeLookup        Procedure(Long fNumber)
code
    p_web.setsessionvalue('showtab_JobFaultCodes',Loc:TabNumber)
    if loc:LookupDone
        ! Prompt For Exchange
        p_web.SSV('Hide:ExchangePrompt',1)
        loop x# = 1 to 20
            if (p_web.GSV('Hide:JobFaultCode' & x#) = 1)
                cycle
            end ! if (p_web.GSV('Hide:JobFaultCode' & x#) = 1)

            Access:MANFAULT.Clearkey(maf:screenOrderKey)
            maf:Manufacturer    = p_web.GSV('job:manufacturer')
            maf:screenOrder    = x#
            if (Access:MANFAULT.TryFetch(maf:screenOrderKey) = Level:Benign)
                ! Found
                Access:MANFAULO.Clearkey(mfo:field_Key)
                mfo:manufacturer    = p_web.GSV('job:manufacturer')
                mfo:field_Number    = maf:field_number
                mfo:field    = p_web.GSV('tmp:FaultCode' & x#)
                if (Access:MANFAULO.TryFetch(mfo:field_Key) = Level:Benign)
                    ! Found
                    if (mfo:promptForExchange)
                        p_web.SSV('Hide:ExchangePrompt',0)
                        break
                    end ! if (mfo:promptForExchange)
                else ! if (Access:MANFAULO.TryFetch(mfo:field_Key) = Level:Benign)
                    ! Error
                end ! if (Access:MANFAULO.TryFetch(mfo:field_Key) = Level:Benign)

            else ! if (Access:MANFAULT.TryFetch(maf:screenOrderKey) = Level:Benign)
                ! Error
            end ! if (Access:MANFAULT.TryFetch(maf:screenOrderKey) = Level:Benign)

        end ! loop x# = 1 to 20
!        do buildFaultCodes
!        do UpdateComments
    end
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:FaultCode' & fNumber)
Local.AllocateExchangePart      Procedure(String    func:Type,Byte  func:Allocated,Byte func:SecondUnit)
local:SecondExchangeUnit        Byte(0)
local:ExchangePartAttached      Byte(0)
local:AttachUnit                Byte(0)
local:AttachSecondUnit          Byte(0)
local:FoundExchange             Byte(0)
Code
    Access:USERS.Clearkey(use:user_code_key)
    use:user_code    = p_web.GSV('job:engineer')
    if (Access:USERS.TryFetch(use:user_code_key) = Level:Benign)
        ! Found
    else ! if (Access:USERS.TryFetch(use:user_code_key) = Level:Benign)
        ! Error
    end ! if (Access:USERS.TryFetch(use:user_code_key) = Level:Benign)


    Access:STOCK.Clearkey(sto:location_key)
    sto:location    = use:location
    sto:part_number    = 'EXCH'
    if (Access:STOCK.TryFetch(sto:location_key) = Level:Benign)
        ! Found
    else ! if (Access:STOCK.TryFetch(sto:location_key) = Level:Benign)
        ! Error
    end ! if (Access:STOCK.TryFetch(sto:location_key) = Level:Benign)

    Case func:Type
        Of 'WAR'
            !Check to see if an Exchange Part line already exists -  (DBH: 19-12-2003)
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number  = p_web.GSV('job:ref_number')
            wpr:Part_Number = 'EXCH'
            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            Loop
                If Access:WARPARTS.NEXT()
                   Break
                End !If
                If wpr:Ref_Number  <> p_web.GSV('job:ref_number')      |
                Or wpr:Part_Number <> 'EXCH'      |
                    Then Break.  ! End If
                If func:SecondUnit = True
                    If wpr:SecondExchangeUnit
                        Return
                    End !If func:SecondUnit And wpr:SecondExchangeUnit
                Else !If func:SecondUnit = True
                    Return
                End !If func:SecondUnit = True
            End !Loop

            If Access:WARPARTS.PrimeRecord() = Level:Benign
                wpr:Part_Ref_Number = sto:Ref_Number
                wpr:Ref_Number      = p_web.GSV('job:ref_number')
                wpr:Adjustment      = 'YES'
                wpr:Part_Number     = 'EXCH'
                wpr:Description     = Clip(p_web.GSV('job:manufacturer')) & ' EXCHANGE UNIT'
                wpr:Quantity        = 1
                wpr:Warranty_Part   = 'NO'
                wpr:Exclude_From_Order = 'YES'
                wpr:PartAllocated   = func:Allocated
                If func:Allocated = 0
                    wpr:Status  = 'REQ'
                Else !If func:Allocated = 0
                    wpr:Status  = 'PIK'
                End !If func:Allocated = 0
                wpr:ExchangeUnit        = True
                wpr:SecondExchangeUnit = func:SecondUnit

                If sto:Assign_Fault_Codes = 'YES'
                    !Try and get the fault codes. This key should get the only record
                    Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                    stm:Ref_Number  = sto:Ref_Number
                    stm:Part_Number = sto:Part_Number
                    stm:Location    = sto:Location
                    stm:Description = sto:Description
                    If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                        !Found
                        wpr:Fault_Code1  = stm:FaultCode1
                        wpr:Fault_Code2  = stm:FaultCode2
                        wpr:Fault_Code3  = stm:FaultCode3
                        wpr:Fault_Code4  = stm:FaultCode4
                        wpr:Fault_Code5  = stm:FaultCode5
                        wpr:Fault_Code6  = stm:FaultCode6
                        wpr:Fault_Code7  = stm:FaultCode7
                        wpr:Fault_Code8  = stm:FaultCode8
                        wpr:Fault_Code9  = stm:FaultCode9
                        wpr:Fault_Code10 = stm:FaultCode10
                        wpr:Fault_Code11 = stm:FaultCode11
                        wpr:Fault_Code12 = stm:FaultCode12
                    Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                    End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign

                End !If sto:Assign_Fault_Codes = 'YES'
                If Access:WARPARTS.TryInsert() = Level:Benign
                    !Insert Successful
                    p_web.SSV('AddToStockAllocation:Type','WAR')
                    p_web.SSV('AddToStockAllocation:Status','')
                    p_web.SSV('AddToStockAllocation:Qty',1)
                    AddToStockAllocation(wpr:Record_Number,'WAR',1,'',p_web.GSV('job:Engineer'),p_web)
                Else !If Access:WARPARTS.TryInsert() = Level:Benign
                    !Insert Failed
                    Access:WARPARTS.CancelAutoInc()
                End !If Access:WARPARTS.TryInsert() = Level:Benign
            End !If Access:WARPARTS.PrimeRecord() = Level:Benign

        Of 'CHA'
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = p_web.GSV('job:ref_number')
            par:Part_Number = 'EXCH'
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> p_web.GSV('job:ref_number')      |
                Or par:Part_Number <> 'EXCH'      |
                    Then Break.  ! End If
                If func:SecondUnit = True
                    If par:SecondExchangeUnit
                        Return
                    End !If func:SecondUnit And wpr:SecondExchangeUnit
                Else !If func:SecondUnit = True
                    Return
                End !If func:SecondUnit = True
            End !Loop

            if access:parts.primerecord() = level:benign
                par:PArt_Ref_Number      = sto:Ref_Number
                par:ref_number            = p_web.GSV('job:ref_number')
                par:adjustment            = 'YES'
                par:part_number           = 'EXCH'
                par:description           = Clip(p_web.GSV('job:manufacturer')) & ' EXCHANGE UNIT'
                par:quantity              = 1
                par:warranty_part         = 'NO'
                par:exclude_from_order    = 'YES'
                par:PartAllocated         = func:Allocated
                par:ExchangeUnit        = True
                par:SecondExchangeUnit = func:SecondUnit

                !see above for confusion
                if func:allocated = 0 then
                    par:Status            = 'REQ'
                ELSE
                    par:status            = 'PIK'
                END
                If sto:Assign_Fault_Codes = 'YES'
                   !Try and get the fault codes. This key should get the only record
                   Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                   stm:Ref_Number  = sto:Ref_Number
                   stm:Part_Number = sto:Part_Number
                   stm:Location    = sto:Location
                   stm:Description = sto:Description
                   If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                       !Found
                       par:Fault_Code1  = stm:FaultCode1
                       par:Fault_Code2  = stm:FaultCode2
                       par:Fault_Code3  = stm:FaultCode3
                       par:Fault_Code4  = stm:FaultCode4
                       par:Fault_Code5  = stm:FaultCode5
                       par:Fault_Code6  = stm:FaultCode6
                       par:Fault_Code7  = stm:FaultCode7
                       par:Fault_Code8  = stm:FaultCode8
                       par:Fault_Code9  = stm:FaultCode9
                       par:Fault_Code10 = stm:FaultCode10
                       par:Fault_Code11 = stm:FaultCode11
                       par:Fault_Code12 = stm:FaultCode12
                   Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                       !Error
                       !Assert(0,'<13,10>Fetch Error<13,10>')
                   End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                End !If sto:Assign_Fault_Codes = 'YES'
                if access:parts.insert()
                    access:parts.cancelautoinc()
                end
                p_web.SSV('AddToStockAllocation:Type','CHA')
                p_web.SSV('AddToStockAllocation:Status','')
                p_web.SSV('AddToStockAllocation:Qty',1)
                AddToStockAllocation(par:Record_Number,'CHA',1,'',p_web.GSV('job:Engineer'),p_web)
            End !If access:Prime
    End !Case func:Type
local.PartForceFaultCode    Procedure(Long f:PartFieldNumber,String f:FaultCode, Long f:JobFieldNumber )
Code
    If f:FaultCode = ''
        Return 0
    End ! If f:FaultCode = ''

    Access:MANFAUPA.Clearkey(map:Field_Number_Key)
    map:Manufacturer = job:Manufacturer
    map:Field_Number = f:PartFieldNumber
    If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        If map:NotAvailable = 0
            Access:MANFPALO.Clearkey(mfp:Field_Key)
            mfp:Manufacturer = job:Manufacturer
            mfp:Field_Number = f:PartFieldNumber
            mfp:Field = f:FaultCode
            If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                If mfp:ForceJobFaultCode
                    If mfp:ForceFaultCodeNumber = f:JobFieldNumber
                        Return 1
                    End ! If mfp:ForceFaultCodeNumber= maf:Field_Number
                End ! If mfp:ForceJobFaultCode
            Else ! Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
            End ! Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
        End ! If map:NotAvailable = 0
    Else ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
    End ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
    Return 0
local.SetLookupButton      Procedure(Long fNumber)
Code
    if (p_web.GSV('ReadOnly:JobFaultCode' & fNumber) = 1)
        return
    end ! if (p_web.GSV('ReadOnly:JobFaultCode' & fNumber) = 1)
    if (p_web.GSV('ShowDate:JobFaultCode' & fNumber) = 1)
        packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar(tmp__FaultCode' & fNumber & ',''dd/mm/yyyy'',this); ' & |
                  'Date.disabled=false;sv(''...'',''JobFaultCodes_pickdate_value'',1,FieldValue(this,1));nextFocus(JobFaultCodes_frm,'''',0);"' & |
                  'value="Select Date" name="Date" type="button">...</button>'
        do SendPacket
    end ! if (p_web.GSV('ShowDate:PartFaultCode1') = 1)
    if (p_web.GSV('Lookup:JobFaultCode' & fNumber) = 1)

        Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
        maf:Manufacturer    = p_web.GSV('job:Manufacturer')
        maf:ScreenOrder    = fNumber
        if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
            ! Found
        else ! if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)
            ! Error
        end ! if (Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign)

        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseJobFaultCodeLookup')&|
                    '?LookupField=tmp:FaultCode' & fNumber & '&Tab=2&ForeignField=mfo:Field&_sort=mfo:Field&Refresh=' & |
                    'sort&LookupFrom=JobFaultCodes&' & |
                    'fieldNumber=' & maf:Field_Number & '&partType=&partMainFault='),) !lookupextra
    end
