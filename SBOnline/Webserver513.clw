

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER513.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER010.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER485.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER514.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER515.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER516.INC'),ONCE        !Req'd for module callout resolution
                     END


FormPreJobCriteria   PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locStartDate         DATE                                  !
locEndDate           DATE                                  !
locAllRegions        LONG                                  !
locRegion            STRING(30)                            !
FilesOpened     Long
REGIONS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormPreJobCriteria')
  loc:formname = 'FormPreJobCriteria_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormPreJobCriteria',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormPreJobCriteria','')
    p_web._DivHeader('FormPreJobCriteria',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormPreJobCriteria',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormPreJobCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormPreJobCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormPreJobCriteria',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormPreJobCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormPreJobCriteria',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormPreJobCriteria',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(REGIONS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(REGIONS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormPreJobCriteria_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('locStartDate')
    p_web.SetPicture('locStartDate','@d06b')
  End
  p_web.SetSessionPicture('locStartDate','@d06b')
  If p_web.IfExistsValue('locEndDate')
    p_web.SetPicture('locEndDate','@d06b')
  End
  p_web.SetSessionPicture('locEndDate','@d06b')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'locRegion'
    p_web.setsessionvalue('showtab_FormPreJobCriteria',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(REGIONS)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.')
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locStartDate',locStartDate)
  p_web.SetSessionValue('locEndDate',locEndDate)
  p_web.SetSessionValue('locAllRegions',locAllRegions)
  p_web.SetSessionValue('locRegion',locRegion)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locStartDate')
    locStartDate = p_web.dformat(clip(p_web.GetValue('locStartDate')),'@d06b')
    p_web.SetSessionValue('locStartDate',locStartDate)
  End
  if p_web.IfExistsValue('locEndDate')
    locEndDate = p_web.dformat(clip(p_web.GetValue('locEndDate')),'@d06b')
    p_web.SetSessionValue('locEndDate',locEndDate)
  End
  if p_web.IfExistsValue('locAllRegions')
    locAllRegions = p_web.GetValue('locAllRegions')
    p_web.SetSessionValue('locAllRegions',locAllRegions)
  End
  if p_web.IfExistsValue('locRegion')
    locRegion = p_web.GetValue('locRegion')
    p_web.SetSessionValue('locRegion',locRegion)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormPreJobCriteria_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    IF (p_web.GetValue('firsttime') = 1)
        !ClearSBOGenericFile(p_web)
        p_web.SSV('locStartDate',TODAY() - 2)
        p_web.SSV('locEndDate',TODAY())
        p_web.SSV('locAllRegions',0)
        p_web.SSV('locRegion','')
    END ! IF  
      p_web.site.CancelButton.TextValue = 'Close'
      p_web.site.CancelButton.Image = 'images/psave.png'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locStartDate = p_web.RestoreValue('locStartDate')
 locEndDate = p_web.RestoreValue('locEndDate')
 locAllRegions = p_web.RestoreValue('locAllRegions')
 locRegion = p_web.RestoreValue('locRegion')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormPreJobCriteria')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormPreJobCriteria_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormPreJobCriteria_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormPreJobCriteria_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormPreJobCriteria" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormPreJobCriteria" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormPreJobCriteria" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Vodacom Portal Pre-Booked Jobs') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Vodacom Portal Pre-Booked Jobs',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormPreJobCriteria">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormPreJobCriteria" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormPreJobCriteria')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Criteria') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Browse Jobs Booked Within Date Range') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormPreJobCriteria')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormPreJobCriteria'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormPreJobCriteria_BrowsePreJobs_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locStartDate')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormPreJobCriteria')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Criteria') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormPreJobCriteria_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& ' colspan="1">'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::txtDisplay
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="6">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtDisplay
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locStartDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locStartDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEndDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEndDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnBuildJobsList
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAllRegions
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAllRegions
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locRegion
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locRegion
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Browse Jobs Booked Within Date Range') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormPreJobCriteria_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Jobs Booked Within Date Range')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Jobs Booked Within Date Range')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Jobs Booked Within Date Range')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Jobs Booked Within Date Range')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    If p_web.GetValue('firsttime') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwJobs
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnExport
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnCreateNewJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::txtDisplay  Routine
  p_web._DivHeader('FormPreJobCriteria_' & p_web._nocolon('txtDisplay') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::txtDisplay  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtDisplay',p_web.GetValue('NewValue'))
    do Value::txtDisplay
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtDisplay  Routine
  p_web._DivHeader('FormPreJobCriteria_' & p_web._nocolon('txtDisplay') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('To display jobs, select a booking date range and press "Build Jobs List".<br/>Then use the filters below to limit the data displayed.',1) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locStartDate  Routine
  p_web._DivHeader('FormPreJobCriteria_' & p_web._nocolon('locStartDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Booking Start Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locStartDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locStartDate',p_web.GetValue('NewValue'))
    locStartDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locStartDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locStartDate',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    locStartDate = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  do Value::locStartDate
  do SendAlert

Value::locStartDate  Routine
  p_web._DivHeader('FormPreJobCriteria_' & p_web._nocolon('locStartDate') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locStartDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locStartDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locStartDate'',''formprejobcriteria_locstartdate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locStartDate',p_web.GetSessionValue('locStartDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
   packet = CLIP(packet) & |
     '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar' & |
     '(locStartDate,''dd/mm/yyyy'',this); Date.disabled=true;' & |
     'sv(''...'',''FormPreJobCriteria_pickdate_value'',1,FieldValue(this,1));nextFocus(FormPreJobCriteria_frm,'''',0);" ' & |
     'value="Select Date" name="Date" type="button">...</button>'
   DO SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPreJobCriteria_' & p_web._nocolon('locStartDate') & '_value')


Prompt::locEndDate  Routine
  p_web._DivHeader('FormPreJobCriteria_' & p_web._nocolon('locEndDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('End Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locEndDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEndDate',p_web.GetValue('NewValue'))
    locEndDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEndDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEndDate',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    locEndDate = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  do Value::locEndDate
  do SendAlert

Value::locEndDate  Routine
  p_web._DivHeader('FormPreJobCriteria_' & p_web._nocolon('locEndDate') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locEndDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locEndDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEndDate'',''formprejobcriteria_locenddate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locEndDate',p_web.GetSessionValue('locEndDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
   packet = CLIP(packet) & |
     '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar' & |
     '(locEndDate,''dd/mm/yyyy'',this); Date.disabled=true;' & |
     'sv(''...'',''FormPreJobCriteria_pickdate_value'',1,FieldValue(this,1));nextFocus(FormPreJobCriteria_frm,'''',0);" ' & |
     'value="Select Date" name="Date" type="button">...</button>'
   DO SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPreJobCriteria_' & p_web._nocolon('locEndDate') & '_value')


Validate::btnBuildJobsList  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnBuildJobsList',p_web.GetValue('NewValue'))
    do Value::btnBuildJobsList
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnBuildJobsList  Routine
  p_web._DivHeader('FormPreJobCriteria_' & p_web._nocolon('btnBuildJobsList') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnBuildJobsList','Build Jobs List','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=BuildPreJobList&ReturnURL=FormPreJobCriteria&RedirectURL=FormPreJobCriteria')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Prompt::locAllRegions  Routine
  p_web._DivHeader('FormPreJobCriteria_' & p_web._nocolon('locAllRegions') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Region Filter')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAllRegions  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAllRegions',p_web.GetValue('NewValue'))
    locAllRegions = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAllRegions
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAllRegions',p_web.GetValue('Value'))
    locAllRegions = p_web.GetValue('Value')
  End
  do Value::locAllRegions
  do SendAlert
  do Prompt::locRegion
  do Value::locRegion  !1
  do Value::brwJobs  !1

Value::locAllRegions  Routine
  p_web._DivHeader('FormPreJobCriteria_' & p_web._nocolon('locAllRegions') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locAllRegions
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locAllRegions')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locAllRegions') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locAllRegions'',''formprejobcriteria_locallregions_value'',1,'''&clip(0)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locAllRegions')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locAllRegions',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locAllRegions_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('All Regions') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locAllRegions') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locAllRegions'',''formprejobcriteria_locallregions_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locAllRegions')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locAllRegions',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locAllRegions_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Select') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPreJobCriteria_' & p_web._nocolon('locAllRegions') & '_value')


Prompt::locRegion  Routine
  p_web._DivHeader('FormPreJobCriteria_' & p_web._nocolon('locRegion') & '_prompt',Choose(p_web.GSV('locAllRegions') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Region')
  If p_web.GSV('locAllRegions') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPreJobCriteria_' & p_web._nocolon('locRegion') & '_prompt')

Validate::locRegion  Routine
    If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('Value')
      p_web.GetDescription(REGIONS,reg:AccountNumberKey,,reg:Region,reg:Region,p_web.GetValue('Value'))
      loc:lookupdone = 1
    Else
      p_web.GetDescription(REGIONS,reg:AccountNumberKey,,reg:Region,reg:Region,p_web.GetSessionValue('locRegion'))
    End
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locRegion',p_web.GetValue('NewValue'))
    locRegion = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locRegion
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locRegion',p_web.GetValue('Value'))
    locRegion = p_web.GetValue('Value')
  End
  p_Web.SetValue('lookupfield','locRegion')
  do AfterLookup
  do Value::locRegion
  do SendAlert
  do Value::brwJobs  !1

Value::locRegion  Routine
  p_web._DivHeader('FormPreJobCriteria_' & p_web._nocolon('locRegion') & '_value',Choose(p_web.GSV('locAllRegions') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllRegions') = 0)
  ! --- STRING --- locRegion
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locRegion')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locRegion'',''formprejobcriteria_locregion_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locRegion')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','locRegion',p_web.GetSessionValueFormat('locRegion'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('SelectRegions?LookupField=locRegion&Tab=1&ForeignField=reg:Region&_sort=&Refresh=sort&LookupFrom=FormPreJobCriteria&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPreJobCriteria_' & p_web._nocolon('locRegion') & '_value')


Validate::brwJobs  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwJobs',p_web.GetValue('NewValue'))
    do Value::brwJobs
  Else
    p_web.StoreValue('sbogen:RecordNumber')
  End
  do SendAlert
  do Value::btnCreateNewJob  !1
  do Value::btnExport  !1

Value::brwJobs  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowsePreJobs --
  p_web.SetValue('BrowsePreJobs:NoForm',1)
  p_web.SetValue('BrowsePreJobs:FormName',loc:formname)
  p_web.SetValue('BrowsePreJobs:parentIs','Form')
  p_web.SetValue('_parentProc','FormPreJobCriteria')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormPreJobCriteria_BrowsePreJobs_embedded_div')&'"><!-- Net:BrowsePreJobs --></div><13,10>'
    p_web._DivHeader('FormPreJobCriteria_' & lower('BrowsePreJobs') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormPreJobCriteria_' & lower('BrowsePreJobs') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowsePreJobs --><13,10>'
  end
  do SendPacket


Validate::btnExport  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnExport',p_web.GetValue('NewValue'))
    do Value::btnExport
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnExport  Routine
  p_web._DivHeader('FormPreJobCriteria_' & p_web._nocolon('btnExport') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnExport','Export','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('VodacomPortalExportCriteria?' &'firsttime=1')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPreJobCriteria_' & p_web._nocolon('btnExport') & '_value')


Validate::btnCreateNewJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnCreateNewJob',p_web.GetValue('NewValue'))
    do Value::btnCreateNewJob
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnCreateNewJob  Routine
  p_web._DivHeader('FormPreJobCriteria_' & p_web._nocolon('btnCreateNewJob') & '_value',Choose(p_web.GSV('sbogen:RecordNumber') < 1 OR p_web.GetValue('firsttime') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('sbogen:RecordNumber') < 1 OR p_web.GetValue('firsttime') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnCreateNewJob','Create New Job','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PreNewJobBooking?' &'MultipleJobBooking=0&firsttime=1&PreBookingRefNo=' & p_web.GSV('sbogen:RecordNumber') & '&ReturnPreJobURL=FormPreJobCriteria')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormPreJobCriteria_' & p_web._nocolon('btnCreateNewJob') & '_value')


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormPreJobCriteria_locStartDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locStartDate
      else
        do Value::locStartDate
      end
  of lower('FormPreJobCriteria_locEndDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEndDate
      else
        do Value::locEndDate
      end
  of lower('FormPreJobCriteria_locAllRegions_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAllRegions
      else
        do Value::locAllRegions
      end
  of lower('FormPreJobCriteria_locRegion_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locRegion
      else
        do Value::locRegion
      end
  of lower('FormPreJobCriteria_BrowsePreJobs_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::brwJobs
      else
        do Value::brwJobs
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormPreJobCriteria_form:ready_',1)
  p_web.SetSessionValue('FormPreJobCriteria_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormPreJobCriteria',0)

PreCopy  Routine
  p_web.SetValue('FormPreJobCriteria_form:ready_',1)
  p_web.SetSessionValue('FormPreJobCriteria_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormPreJobCriteria',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormPreJobCriteria_form:ready_',1)
  p_web.SetSessionValue('FormPreJobCriteria_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormPreJobCriteria:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormPreJobCriteria_form:ready_',1)
  p_web.SetSessionValue('FormPreJobCriteria_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormPreJobCriteria:Primed',0)
  p_web.setsessionvalue('showtab_FormPreJobCriteria',0)

LoadRelatedRecords  Routine
    p_web.GetDescription(REGIONS,reg:AccountNumberKey,,reg:Region,,p_web.GetSessionValue('locRegion'))
    p_web.FileToSessionQueue(REGIONS)
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormPreJobCriteria_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormPreJobCriteria_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormPreJobCriteria:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('locStartDate')
  p_web.StoreValue('locEndDate')
  p_web.StoreValue('')
  p_web.StoreValue('locAllRegions')
  p_web.StoreValue('locRegion')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locStartDate',locStartDate) ! DATE
     p_web.SSV('locEndDate',locEndDate) ! DATE
     p_web.SSV('locAllRegions',locAllRegions) ! LONG
     p_web.SSV('locRegion',locRegion) ! STRING(30)
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locStartDate = p_web.GSV('locStartDate') ! DATE
     locEndDate = p_web.GSV('locEndDate') ! DATE
     locAllRegions = p_web.GSV('locAllRegions') ! LONG
     locRegion = p_web.GSV('locRegion') ! STRING(30)
