

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER467.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER134.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER185.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER201.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER264.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER266.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER468.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER521.INC'),ONCE        !Req'd for module callout resolution
                     END


IndividualDespatch   PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
tmp:LoanModelNumber  STRING(30)                            !
Ans                  LONG                                  !
locAccessoryErrorMessage STRING(255)                       !
locIMEINumber        STRING(30)                            !
locJobNumber         LONG                                  !
locErrorMessage      STRING(255)                           !
locMessage           STRING(255)                           !
locAccessoryMessage  STRING(255)                           !
locSecurityPackID    STRING(30)                            !
locAccessoryPassword STRING(30)                            !
locAccessoryPasswordMessage STRING(255)                    !
locAuditTrail        STRING(255)                           !
locConsignmentNumber STRING(30)                            !
FilesOpened     Long
Tagging::State  USHORT
LOANACC::State  USHORT
JOBACC::State  USHORT
WEBJOB::State  USHORT
WAYBILLJ::State  USHORT
WAYBILLS::State  USHORT
COURIER::State  USHORT
MULDESPJ::State  USHORT
SUBTRACC::State  USHORT
LOAN::State  USHORT
TRADEACC::State  USHORT
EXCHANGE::State  USHORT
JOBS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
cou:Courier_OptionView   View(COURIER)
                          Project(cou:Courier)
                        End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('IndividualDespatch')
  loc:formname = 'IndividualDespatch_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'IndividualDespatch',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('IndividualDespatch','')
    p_web._DivHeader('IndividualDespatch',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferIndividualDespatch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferIndividualDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferIndividualDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_IndividualDespatch',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferIndividualDespatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_IndividualDespatch',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'IndividualDespatch',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
ClearVariables      Routine
    p_web.SSV('UnitValidated',0)
    p_web.SSV('locJobNumber','')
    p_web.SSV('locIMEINumber','')
    p_web.SSV('ValidateButtonText','Validate Unit Details')
    p_web.SSV('locAccessoryMessage','')
    p_web.SSV('locAccessoryErrorMessage','')        
    p_web.SSV('ValidateAccessories',0)
    p_web.SSV('Hide:ValidateAccessoriesButton',1)
    p_web.SSV('AccessoryConfirmationRequired',0)
    p_web.SSV('AccessoryPasswordRequired',0)
    p_web.SSV('locAccessoryPasswordMessage','')
    p_web.SSV('locAccessoryPassword','')
    p_web.SSV('AccessoriesValidated',0)
    p_web.SSV('Show:DespatchInvoiceDetails',0) ! Show invoice details on despatch screen
    p_web.SSV('CreateDespatchInvoice',0) ! Auto create invoice
    p_web.SSV('Show:ConsignmentNumber',0)
    p_web.ssv('ShowInvoiceButton',0)

ValidateUnitDetails Routine
    ClearTaggingFile(p_web)
    if (p_web.GSV('UnitValidated') = 1)
        Do ClearVariables
        exit
    end
    p_web.SSV('tmp:LoanModelNumber','')
    p_web.SSV('UnitValidated',0)
    p_web.SSV('locErrorMessage','')
    p_web.SSV('locMessage','')
    p_web.SSV('DespatchType','')
    p_web.SSV('ValidateAccessories',0)
    p_web.SSV('locSecurityPackID','')
    p_web.SSV('Hide:ValidateAccessoriesButton',1)
    p_web.SSV('AccessoryConfirmationRequired',0)
    p_web.SSV('AccessoryPasswordRequired',0)
    p_web.SSV('locAccessoryPasswordMessage','')    
    p_web.SSV('locAccessoryPassword','')
    p_web.SSV('AccessoriesValidated',0)

    if (p_web.GSV('locJobNumber')= '')
        exit
    end
    if (p_web.GSV('locIMEINumber') = '')
        exit
    end

    Access:JOBS.Clearkey(job:Ref_Number_Key)
    job:Ref_Number = p_web.GSV('locJobNumber')
    If (Access:JOBS.TryFetch(job:Ref_Number_Key))
        p_web.SSV('locErrorMessage','Cannot find selected Job Number!')
        Exit
    end

        
    if (JobInUse(job:Ref_Number))
        p_web.SSV('locErrorMessage','Cannot despatch the selected job is in use.')
        exit
    end
    
    p_web.FileToSessionQueue(JOBS)
    
    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    If (Access:JOBSE.TryFetch(jobe:RefNumberKey))
    End
    p_web.FileToSessionQueue(JOBSE)
    
    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    wob:RefNumber  = job:Ref_Number
    IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
    END
    p_web.FileToSessionQueue(WEBJOB)
    

    
    if (p_web.GSV('BookingSite') = 'RRC')
        ! RRC Despatch
        
        IF (wob:ReadyToDespatch <> 1)
            p_web.SSV('locErrorMessage','The selected job is not ready for despatch.')
            EXIT
        END
        
        IF (wob:HeadAccountNumber <> p_web.GSV('BookingAccount'))
            p_web.SSV('locErrorMessage','The selected job is not from your RRC.')
            EXIT
        END
        
        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number
        if (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
            Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
            jobe2:RefNumber = job:Ref_Number
            If (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
                if (sub:UseCustDespAdd = 'YES')
                    if (HubOutOfRegion(p_web.GSV('BookingAccount'),jobe2:HubCustomer) = 1)
                        if (ReleasedForDespatch(job:Ref_Number) = 0)
                            p_web.SSV('locErrorMessage','Unable to despatch. The delivery address is outside your region.')
                            exit
                        end
                    end
                else
                    if (HubOutOfRegion(p_web.GSV('BookingAccount'),jobe2:HubDelivery) = 1)
                        if (ReleasedForDespatch(job:Ref_Number) = 0)
                            p_web.SSV('locErrorMessage','Unable to despatch. The delivery address is outside your region.')
                            exit
                        end
                    end
                end
            end
        end

        found# = 0
        Access:MULDESPJ.Clearkey(mulj:JobNumberOnlyKey)
        mulj:JobNumber = job:Ref_Number
        set(mulj:JobNumberOnlyKey,mulj:JobNumberOnlyKey)
        Loop Until Access:MULDESPJ.Next()
            if (mulj:JobNumber <> job:Ref_Number)
                break
            end
            Access:MULDESP.Clearkey(muld:RecordNumberKey)
            muld:RecordNumber = mulj:RefNumber
            if (Access:MULDESP.TryFetch(muld:RecordNumberKey)= Level:Benign)
                if (muld:HeadAccountNumber = p_web.GSV('BookingAccount'))
                    found# = 1
                    break
                end
            end
        end
        if (found# = 1)
            p_web.SSV('locErrorMessage','Cannot despatch! This job is part of a multiple batch that has not yet been despatched.')
            exit
        end
        
        IF (GETINI('DESPATCH','DoNotDespatchLoan',,CLIP(PATH())&'\SB2KDEF.INI') = 1 And jobe:Despatchtype = 'JOB')
            IF (p_web.GSV('BookingSite') = 'RRC' OR (p_web.GSV('BookingSite') = 'ARC' AND jobe:WebJob <> 1)) 
                ! #11817 Only stop despatch if RRC, or ARC back to customer. (Bryan: 11/05/2011)
                if (job:Loan_Unit_Number <> 0)
                    ! #13618 Allow a loan attached by the VCP to be despatched. Using Hard Coded site location (DBH: 28/10/2015)
                    Access:LOAN.ClearKey(loa:Ref_Number_Key)
                    loa:Ref_Number = job:Loan_Unit_Number
                    IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
                        ! #13618 I will assume a Loan record exists. (DBH: 28/10/2015)
                        IF (loa:Location <> 'VODACARE COLLECTION POINT')
                            p_web.SSV('locErrorMessage','Cannot despatch! The attached loan unit has not been returned')
                            EXIT
                        END ! IF
                    END ! IF
                end
            END !IF (jobe:WebJob <> 1)
        END
        
        Access:COURIER.ClearKey(cou:Courier_Key)
        CASE jobe:DespatchType
        OF 'JOB'
            cou:Courier = job:Courier
        OF 'EXC'
            cou:Courier = job:Exchange_Courier
        of 'LOA'
            cou:Courier = job:Loan_Courier
        end
        if (Access:COURIER.TryFetch(cou:Courier_Key))
            p_web.SSV('locErrorMessage','Cannot despatch! Cannot find the attached courier.')
            exit            
        end
        
        
        p_web.SSV('DespatchType',jobe:DespatchType)
        
       
        
    else ! if (p_web.GSV('BookingSite') = 'RRC')
        ! ARC Despatch

        
        if (GETINI('DESPATCH','DoNotDespatchLoan',,CLIP(PATH())&'\SB2KDEF.INI') = 1 And job:Despatch_type = 'JOB')
            if (job:Loan_Unit_Number <> 0)
                p_web.SSV('locErrorMessage','Cannot despatch! The attached loan unit has not been returned')
                exit
            end
        end
        
        Access:MULDESPJ.Clearkey(mulj:JobNumberOnlyKey)
        mulj:JobNumber = job:Ref_Number
        if (Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:benign)
            p_web.SSV('locErrorMessage','Cannot despatch! This job is part of a multiple batch that has not yet been despatched.')
            exit
        end
        
        if (job:Despatched <> 'REA')
            p_web.SSV('locErrorMessage','The selected job is not ready for despatch.')
            exit
        end

        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number
        if (Access:SUBTRACC.TryFetch(sub:Account_NUmber_Key) = Level:Benign)
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number = sub:Main_Account_Number
            if (access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign)
                if (tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_Accounts = 'YES')
                    if (sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES')
                        p_web.SSV('locErrorMessage','Cannot despatch! The Trade Account is on stop and this is not a warranty job.')
                        exit
                    end
                        
                else
                    if (tra:Stop_Account = 'YES' and job:Warranty_Job <> 'YES')
                        p_web.SSV('locErrorMessage','Cannot despatch! The Trade Account is on stop and this is not a warranty job.')
                        exit
                    end
                end
            end
        end
        
        Access:COURIER.Clearkey(cou:Courier_Key)
        cou:Courier = job:Current_Courier
        if (Access:COURIER.TryFetch(cou:Courier_Key))
            p_web.SSV('locErrorMessage','Cannot despatch! Cannot find the attached courier.')
            exit
        end
        
        p_web.SSV('DespatchType',job:Despatch_Type)
        
    end
    
    p_web.FileToSessionQueue(COURIER)
    
    case p_web.GSV('DespatchType')
    of 'JOB'
        if (job:ESN <> p_web.GSV('locIMEINumber'))
            p_web.SSV('locErrorMessage','The selected I.M.E.I. Number does not match the selected job.')
            exit
        end
        p_web.SSV('tmp:LoanModelNumber',job:Model_Number)
        p_web.SSV('AccessoryRefNumber',job:Ref_Number)
        p_web.SSV('Hide:ValidateAccessoriesButton',0)
        p_web.SSV('ValidateAccessories',1)
        p_web.SSV('AccessoriesValidated',0)
    of 'EXC'
        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number = job:Exchange_Unit_Number
        if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
            p_web.SSV('locErrorMessage','Exchange Despatch! Unable to find Exchange Unit on selected job.')
            exit
        else
            if (xch:ESN <> p_web.GSV('locIMEINumber'))
                p_web.SSV('locErrorMessage','Exchange Despatch! The selected I.M.E.I. Number does not match the Exchange Unit attached to the selected job.')
                exit
            end
        end
        p_web.SSV('tmp:LoanModelNumber',xch:Model_Number)
        p_web.SSV('AccessoryRefNumber',job:Ref_Number)
        p_web.SSV('ValidateAccessories',0)
        p_web.SSV('Hide:ValidateAccessoriesButton',1)
        p_web.SSV('AccessoriesValidated',1)
    of 'LOA'
        Access:LOAN.Clearkey(loa:Ref_Number_Key)
        loa:Ref_Number = job:Loan_Unit_Number
        if (Access:LOAN.TryFetch(loa:Ref_Number_Key))
            p_web.SSV('locErrorMessage','Loan Despatch! Unable to find Loan Unit on selected job.')
            exit
        else
            if (loa:ESN <> p_web.GSV('locIMEINumber'))
                p_web.SSV('locErrorMessage','Loan Despatch! The selected I.M.E.I. Number does not match the Loan Unit attached to the selected job.')
                exit    
            end
        end
        p_web.SSV('tmp:LoanModelNumber',loa:Model_Number)
        p_web.SSV('AccessoryRefNumber',job:Loan_Unit_Number)
        p_web.SSV('Hide:ValidateAccessoriesButton',0)
        p_web.SSV('ValidateAccessories',1)
        p_web.SSV('AccessoriesValidated',0)
    end
    
    p_web.ssv('ShowInvoiceButton',0)
    ! Can job be despatched?
    if (job:Chargeable_Job = 'YES' AND p_web.GSV('DespatchType') <> 'LOA') ! #12496 Do not check if Loan Unit (DBH: 22/06/2012)
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number = wob:HeadAccountNumber
        if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) )
                
        End 
        if (tra:Despatch_Paid_Jobs = 'YES' And NOT JobPaid(job:Ref_Number))
            p_web.SSV('locErrorMessage','Unable to despatch. The selected job has not been paid.')
            exit
        end
        if (tra:Despatch_Invoiced_Jobs = 'YES' And NOT IsJobInvoiced(job:Invoice_Number,p_web))
            p_web.SSV('locErrorMessage','Unable to despatch. The selected job has not been invoiced.')
            p_web.ssv('ShowInvoiceButton',1)
            exit
        end
    end
    
    p_web.SSV('UnitValidated',1)
    
    p_web.SSV('ValidateButtonText','Reset Details')
    
    !Can the job be invoiced?
    IF (job:Bouncer <> 'X' AND job:Chargeable_Job = 'YES')
        p_web.SSV('Show:DespatchInvoiceDetails',1)
        IF (InvoiceSubAccounts(job:Account_Number))
            IF (sub:InvoiceAtDespatch AND job:Despatch_Type = 'JOB')
                CASE sub:InvoiceType
                OF 0 ! Manual invoice
                    p_web.SSV('CreateDespatchInvoice',0)
                OF 1 !Auto Invoice
                    IF (job:Invoice_Number = '')
                        ! Create Invoice
                        p_web.SSV('CreateDespatchInvoice',1)
                    END
                END !case

            END
            
        ELSE !if invoiceSubAccounts
            
            IF (tra:InvoiceAtDespatch AND job:Despatch_Type = 'JOB')
                CASE tra:InvoiceType
                OF 0 ! Manual Invoice
                    p_web.SSV('CreateDespatchInvoice',0)
                OF 1 ! Auto Invoice
                    p_web.SSV('CreateDespatchInvoice',1)
                END
                 
            END
            
        END
        
    END
ShowConsignmentNumber       ROUTINE
    Access:COURIER.ClearKey(cou:Courier_Key)
    cou:Courier = p_web.GSV('cou:Courier')
    IF (Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign)
        p_web.FileToSessionQueue(COURIER)
        IF (cou:PrintWaybill <> 1)
            p_web.SSV('Hide:SecurityPackID',1)
        ELSE
            p_web.SSV('Hide:SecurityPackID',0)
        END
        
        IF (p_web.GSV('cou:PrintWaybill') <> 1 AND p_web.GSV('cou:AutoConsignmentNo') <> 1)
            p_web.SSV('Show:ConsignmentNumber',1)
        ELSE
            p_web.SSV('Show:ConsignmentNumber',0)
        END
    END
    
    
        
DeleteSessionValues ROUTINE ! Auto generated by Bryan's Template
    ! Local Variables
    p_web.DeleteSessionValue('tmp:LoanModelNumber')
    p_web.DeleteSessionValue('Ans')
    p_web.DeleteSessionValue('locAccessoryErrorMessage')
    p_web.DeleteSessionValue('locIMEINumber')
    p_web.DeleteSessionValue('locJobNumber')
    p_web.DeleteSessionValue('locErrorMessage')
    p_web.DeleteSessionValue('locMessage')
    p_web.DeleteSessionValue('locAccessoryMessage')
    p_web.DeleteSessionValue('locSecurityPackID')
    p_web.DeleteSessionValue('locAccessoryPassword')
    p_web.DeleteSessionValue('locAccessoryPasswordMessage')
    p_web.DeleteSessionValue('locAuditTrail')
    p_web.DeleteSessionValue('locConsignmentNumber')

    ! Other Variables
    p_web.DeleteSessionValue('UnitValidated')
    p_web.DeleteSessionValue('ValidateButtonText')
    p_web.DeleteSessionValue('AccessoryRefNumber')
    p_web.DeleteSessionValue('DespatchType')
    p_web.DeleteSessionValue('Hide:ValidateAccessoriesButton')
    p_web.DeleteSessionValue('ValidateAccessories')
    p_web.DeleteSessionValue('AccessoryConfirmationRequired')
    p_web.DeleteSessionValue('AccessoryPasswordRequired')
    p_web.DeleteSessionValue('AccessoriesValidated')
    p_web.DeleteSessionValue('Show:DespatchInvoiceDetails')
    p_web.DeleteSessionValue('CreateDespatchInvoice')
OpenFiles  ROUTINE
  p_web._OpenFile(Tagging)
  p_web._OpenFile(LOANACC)
  p_web._OpenFile(JOBACC)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(WAYBILLJ)
  p_web._OpenFile(WAYBILLS)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(MULDESPJ)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(Tagging)
  p_Web._CloseFile(LOANACC)
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(WAYBILLJ)
  p_Web._CloseFile(WAYBILLS)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(MULDESPJ)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.site.CancelButton.TextValue = 'Close'
  p_web.SSV('ValidateButtonText','Validate Unit Details')
  Do ClearVariables
  p_web.SetValue('IndividualDespatch_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  do deletesessionvalues

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'cou:Courier'
    p_web.setsessionvalue('showtab_IndividualDespatch',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(COURIER)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locConsignmentNumber')
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locJobNumber',locJobNumber)
  p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  p_web.SetSessionValue('locErrorMessage',locErrorMessage)
  p_web.SetSessionValue('locAccessoryMessage',locAccessoryMessage)
  p_web.SetSessionValue('locAccessoryErrorMessage',locAccessoryErrorMessage)
  p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)
  p_web.SetSessionValue('locAccessoryPasswordMessage',locAccessoryPasswordMessage)
  p_web.SetSessionValue('cou:Courier',cou:Courier)
  p_web.SetSessionValue('locConsignmentNumber',locConsignmentNumber)
  p_web.SetSessionValue('locSecurityPackID',locSecurityPackID)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locJobNumber')
    locJobNumber = p_web.GetValue('locJobNumber')
    p_web.SetSessionValue('locJobNumber',locJobNumber)
  End
  if p_web.IfExistsValue('locIMEINumber')
    locIMEINumber = p_web.GetValue('locIMEINumber')
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  End
  if p_web.IfExistsValue('locErrorMessage')
    locErrorMessage = p_web.GetValue('locErrorMessage')
    p_web.SetSessionValue('locErrorMessage',locErrorMessage)
  End
  if p_web.IfExistsValue('locAccessoryMessage')
    locAccessoryMessage = p_web.GetValue('locAccessoryMessage')
    p_web.SetSessionValue('locAccessoryMessage',locAccessoryMessage)
  End
  if p_web.IfExistsValue('locAccessoryErrorMessage')
    locAccessoryErrorMessage = p_web.GetValue('locAccessoryErrorMessage')
    p_web.SetSessionValue('locAccessoryErrorMessage',locAccessoryErrorMessage)
  End
  if p_web.IfExistsValue('locAccessoryPassword')
    locAccessoryPassword = p_web.GetValue('locAccessoryPassword')
    p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)
  End
  if p_web.IfExistsValue('locAccessoryPasswordMessage')
    locAccessoryPasswordMessage = p_web.GetValue('locAccessoryPasswordMessage')
    p_web.SetSessionValue('locAccessoryPasswordMessage',locAccessoryPasswordMessage)
  End
  if p_web.IfExistsValue('cou:Courier')
    cou:Courier = p_web.GetValue('cou:Courier')
    p_web.SetSessionValue('cou:Courier',cou:Courier)
  End
  if p_web.IfExistsValue('locConsignmentNumber')
    locConsignmentNumber = p_web.GetValue('locConsignmentNumber')
    p_web.SetSessionValue('locConsignmentNumber',locConsignmentNumber)
  End
  if p_web.IfExistsValue('locSecurityPackID')
    locSecurityPackID = p_web.GetValue('locSecurityPackID')
    p_web.SetSessionValue('locSecurityPackID',locSecurityPackID)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('IndividualDespatch_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    p_web.SSV('Hide:SecurityPackID',0)
    IF (p_web.IfExistsValue('idReturnURL'))
        p_web.StoreValue('idReturnURL')
    END ! IF   
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locJobNumber = p_web.RestoreValue('locJobNumber')
 locIMEINumber = p_web.RestoreValue('locIMEINumber')
 locErrorMessage = p_web.RestoreValue('locErrorMessage')
 locAccessoryMessage = p_web.RestoreValue('locAccessoryMessage')
 locAccessoryErrorMessage = p_web.RestoreValue('locAccessoryErrorMessage')
 locAccessoryPassword = p_web.RestoreValue('locAccessoryPassword')
 locAccessoryPasswordMessage = p_web.RestoreValue('locAccessoryPasswordMessage')
 locConsignmentNumber = p_web.RestoreValue('locConsignmentNumber')
 locSecurityPackID = p_web.RestoreValue('locSecurityPackID')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'IndexPage'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('IndividualDespatch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('IndividualDespatch_ChainTo')
    loc:formaction = p_web.GetSessionValue('IndividualDespatch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('idReturnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="IndividualDespatch" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="IndividualDespatch" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="IndividualDespatch" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Individual Despatch') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Individual Despatch',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_IndividualDespatch">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_IndividualDespatch" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_IndividualDespatch')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Confirm Unit Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Confirm Despatch') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_IndividualDespatch')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_IndividualDespatch'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('IndividualDespatch_TagValidateLoanAccessories_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locJobNumber')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab5'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_IndividualDespatch')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab5'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Confirm Unit Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_IndividualDespatch_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Unit Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Unit Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Unit Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Unit Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonValidateJobDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonValidateJobDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locErrorMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ButtonInvoice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::ButtonInvoice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_IndividualDespatch_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::TagValidateLoanAccessories
      do Value::TagValidateLoanAccessories
      do Comment::TagValidateLoanAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonValidateAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonValidateAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAccessoryMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAccessoryMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAccessoryMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAccessoryErrorMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAccessoryErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAccessoryErrorMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAccessoryPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAccessoryPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAccessoryPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonConfirmMismatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonConfirmMismatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonFailAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonFailAccessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAccessoryPasswordMessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAccessoryPasswordMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAccessoryPasswordMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel5">'&CRLF &|
                                    '  <div id="panel5Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Confirm Despatch') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel5Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_IndividualDespatch_5">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Despatch')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab5" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab5">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Despatch')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Despatch')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm Despatch')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::cou:Courier
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::cou:Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::cou:Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locConsignmentNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locConsignmentNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locConsignmentNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSecurityPackID
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSecurityPackID
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locSecurityPackID
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'50%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonConfirmDespatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonConfirmDespatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locJobNumber  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locJobNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locJobNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobNumber',p_web.GetValue('NewValue'))
    locJobNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobNumber',p_web.GetValue('Value'))
    locJobNumber = p_web.GetValue('Value')
  End
  If locJobNumber = ''
    loc:Invalid = 'locJobNumber'
    loc:alert = p_web.translate('Job Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  p_web.SSV('locErrorMessage','')
  do Value::locJobNumber
  do SendAlert
  do Value::locErrorMessage  !1

Value::locJobNumber  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locJobNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locJobNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('UnitValidated') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('UnitValidated') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locJobNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locJobNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobNumber'',''individualdespatch_locjobnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locJobNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locJobNumber',p_web.GetSessionValueFormat('locJobNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locJobNumber') & '_value')

Comment::locJobNumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locJobNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locJobNumber') & '_comment')

Prompt::locIMEINumber  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locIMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('I.M.E.I. Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('NewValue'))
    locIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('Value'))
    locIMEINumber = p_web.GetValue('Value')
  End
  If locIMEINumber = ''
    loc:Invalid = 'locIMEINumber'
    loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  p_web.SSV('locErrorMessage','')
  do Value::locIMEINumber
  do SendAlert
  do Value::locErrorMessage  !1

Value::locIMEINumber  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locIMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('UnitValidated') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('UnitValidated') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locIMEINumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locIMEINumber'',''individualdespatch_locimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locIMEINumber',p_web.GetSessionValueFormat('locIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locIMEINumber') & '_value')

Comment::locIMEINumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locIMEINumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locIMEINumber') & '_comment')

Validate::buttonValidateJobDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonValidateJobDetails',p_web.GetValue('NewValue'))
    do Value::buttonValidateJobDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do ValidateUnitDetails
  do Value::buttonValidateJobDetails
  do SendAlert
  do Value::locErrorMessage  !1
  do Value::TagValidateLoanAccessories  !1
  do Value::locJobNumber  !1
  do Value::locIMEINumber  !1
  do Value::buttonValidateAccessories  !1
  do Value::buttonConfirmDespatch  !1
  do Prompt::locSecurityPackID
  do Value::locSecurityPackID  !1
  do Value::locAccessoryErrorMessage  !1
  do Value::locAccessoryMessage  !1
  do Value::buttonConfirmMismatch  !1
  do Prompt::locAccessoryPassword
  do Value::locAccessoryPassword  !1
  do Value::locAccessoryPasswordMessage  !1
  do Value::buttonFailAccessory  !1
  do Prompt::cou:Courier
  do Value::cou:Courier  !1
  do Prompt::locConsignmentNumber
  do Value::locConsignmentNumber  !1
  do Value::ButtonInvoice  !1
  do Comment::ButtonInvoice

Value::buttonValidateJobDetails  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonValidateJobDetails') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonValidateJobDetails'',''individualdespatch_buttonvalidatejobdetails_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ValidateJob',p_web.GSV('ValidateButtonText'),'button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('buttonValidateJobDetails') & '_value')

Comment::buttonValidateJobDetails  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonValidateJobDetails') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locErrorMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locErrorMessage') & '_prompt',Choose(p_web.GSV('locErrorMessage') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locErrorMessage') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locErrorMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locErrorMessage',p_web.GetValue('NewValue'))
    locErrorMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locErrorMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locErrorMessage',p_web.GetValue('Value'))
    locErrorMessage = p_web.GetValue('Value')
  End

Value::locErrorMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locErrorMessage') & '_value',Choose(p_web.GSV('locErrorMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locErrorMessage') = '')
  ! --- DISPLAY --- locErrorMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locErrorMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locErrorMessage') & '_value')

Comment::locErrorMessage  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locErrorMessage') & '_comment',Choose(p_web.GSV('locErrorMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locErrorMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::ButtonInvoice  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ButtonInvoice',p_web.GetValue('NewValue'))
    do Value::ButtonInvoice
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::ButtonInvoice  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('ButtonInvoice') & '_value',Choose(p_web.gsv('ShowInvoiceButton')=0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.gsv('ShowInvoiceButton')=0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ButtonInvoice','Invoice Job','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('CreateInvoice?returnURL=IndividualDespatch')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('ButtonInvoice') & '_value')

Comment::ButtonInvoice  Routine
    loc:comment = p_web.Translate('Click to Create Invoice')
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('ButtonInvoice') & '_comment',Choose(p_web.gsv('ShowInvoiceButton')=0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.gsv('ShowInvoiceButton')=0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('ButtonInvoice') & '_comment')

Prompt::TagValidateLoanAccessories  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('TagValidateLoanAccessories') & '_prompt',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1),'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Validate Accessories')
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1)
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::TagValidateLoanAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('TagValidateLoanAccessories',p_web.GetValue('NewValue'))
    do Value::TagValidateLoanAccessories
  Else
    p_web.StoreValue('acr:Accessory')
  End

Value::TagValidateLoanAccessories  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1),1,0))
  ! --- BROWSE ---  TagValidateLoanAccessories --
  p_web.SetValue('TagValidateLoanAccessories:NoForm',1)
  p_web.SetValue('TagValidateLoanAccessories:FormName',loc:formname)
  p_web.SetValue('TagValidateLoanAccessories:parentIs','Form')
  p_web.SetValue('_parentProc','IndividualDespatch')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('IndividualDespatch_TagValidateLoanAccessories_embedded_div')&'"><!-- Net:TagValidateLoanAccessories --></div><13,10>'
    p_web._DivHeader('IndividualDespatch_' & lower('TagValidateLoanAccessories') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('IndividualDespatch_' & lower('TagValidateLoanAccessories') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagValidateLoanAccessories --><13,10>'
  end
  do SendPacket

Comment::TagValidateLoanAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('TagValidateLoanAccessories') & '_comment',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1)
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonValidateAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonValidateAccessories',p_web.GetValue('NewValue'))
    do Value::buttonValidateAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ! Validate Accessories
  p_web.SSV('locAccessoryErrorMessage','')
  p_web.SSV('locAccessoryMessage','')
  p_web.SSV('Hide:ValidateAccessoriesButton',1)
  p_web.SSV('AccessoryConfirmationRequired',0)
  p_web.SSV('AccessoryPasswordRequired',0)
  p_web.SSV('AccessoriesValidated',0)
  
  ! Validate
  !  p_web.SSV('AccessoryCheck:Type',p_web.GSV('DespatchType'))
  !  p_web.SSV('AccessoryCheck:RefNumber',p_web.GSV('AccessoryRefNumber'))
  !  AccessoryCheck(p_web)
  !  Case p_web.GSV('AccessoryCheck:Return')
  CASE ValidateAccessories(p_web, p_web.GSV('DespatchType'), p_web.GSV('AccessoryRefNumber'))
  Of 1 ! Missing
      p_web.SSV('locAccessoryErrorMessage','The selected unit has a missing accessory.')
      p_web.SSV('AccessoryConfirmationRequired',1)
      p_web.SSV('ConfirmMismatchText','Confirm Access. Validation')
      
      if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'ACCESSORY MISMATCH - ACCEPT'))
          p_web.SSV('AccessoryPasswordRequired',1)
      end            
  Of 2 ! Mismatch
      p_web.SSV('locAccessoryErrorMessage','There is a mismatch between the selected unit''s accessories.')    
      p_web.SSV('AccessoryConfirmationRequired',1)
      p_web.SSV('ConfirmMismatchText','Confirm Access. Validation')
      if (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'ACCESSORY MISMATCH - ACCEPT'))
          p_web.SSV('AccessoryPasswordRequired',1)
      end                
  Else ! A Ok
      p_web.SSV('locAccessoryMessage','Accessory Validated')
      p_web.SSV('AccessoryConfirmationRequired',0)
      p_web.SSV('AccessoriesValidated',1)
      DO ShowConsignmentNumber
  End
  do Value::buttonValidateAccessories
  do SendAlert
  do Value::locAccessoryErrorMessage  !1
  do Value::locAccessoryMessage  !1
  do Value::buttonConfirmDespatch  !1
  do Prompt::locSecurityPackID
  do Value::locSecurityPackID  !1
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonConfirmMismatch  !1
  do Prompt::locAccessoryPassword
  do Value::locAccessoryPassword  !1
  do Value::buttonFailAccessory  !1
  do Prompt::locConsignmentNumber
  do Value::locConsignmentNumber  !1
  do Prompt::cou:Courier
  do Value::cou:Courier  !1

Value::buttonValidateAccessories  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonValidateAccessories') & '_value',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1 AND p_web.GSV('AccessoryConfirmationRequired') <> 1 AND p_web.GSV('Hide:ValidateAccessoriesButton') <> 1),'hdiv','adiv'))
  loc:extra = ''
  If Not (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1 AND p_web.GSV('AccessoryConfirmationRequired') <> 1 AND p_web.GSV('Hide:ValidateAccessoriesButton') <> 1))
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonValidateAccessories'',''individualdespatch_buttonvalidateaccessories_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ValidateAccessories','Validate Accessories','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('buttonValidateAccessories') & '_value')

Comment::buttonValidateAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonValidateAccessories') & '_comment',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1 AND p_web.GSV('AccessoryConfirmationRequired') <> 1 AND p_web.GSV('Hide:ValidateAccessoriesButton') <> 1),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('ValidateAccessories') = 1 AND p_web.GSV('AccessoryConfirmationRequired') <> 1 AND p_web.GSV('Hide:ValidateAccessoriesButton') <> 1)
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAccessoryMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryMessage') & '_prompt',Choose(p_web.GSV('locAccessoryMessage') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locAccessoryMessage') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAccessoryMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAccessoryMessage',p_web.GetValue('NewValue'))
    locAccessoryMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAccessoryMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAccessoryMessage',p_web.GetValue('Value'))
    locAccessoryMessage = p_web.GetValue('Value')
  End

Value::locAccessoryMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryMessage') & '_value',Choose(p_web.GSV('locAccessoryMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAccessoryMessage') = '')
  ! --- DISPLAY --- locAccessoryMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAccessoryMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locAccessoryMessage') & '_value')

Comment::locAccessoryMessage  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryMessage') & '_comment',Choose(p_web.GSV('locAccessoryMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAccessoryMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAccessoryErrorMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_prompt',Choose(p_web.GSV('locAccessoryErrorMessage') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locAccessoryErrorMessage') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAccessoryErrorMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAccessoryErrorMessage',p_web.GetValue('NewValue'))
    locAccessoryErrorMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAccessoryErrorMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAccessoryErrorMessage',p_web.GetValue('Value'))
    locAccessoryErrorMessage = p_web.GetValue('Value')
  End

Value::locAccessoryErrorMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_value',Choose(p_web.GSV('locAccessoryErrorMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAccessoryErrorMessage') = '')
  ! --- DISPLAY --- locAccessoryErrorMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAccessoryErrorMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_value')

Comment::locAccessoryErrorMessage  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryErrorMessage') & '_comment',Choose(p_web.GSV('locAccessoryErrorMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAccessoryErrorMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAccessoryPassword  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPassword') & '_prompt',Choose(p_web.GSV('AccessoryPasswordRequired') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Password Required For Confirmation')
  If p_web.GSV('AccessoryPasswordRequired') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locAccessoryPassword') & '_prompt')

Validate::locAccessoryPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAccessoryPassword',p_web.GetValue('NewValue'))
    locAccessoryPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAccessoryPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAccessoryPassword',p_web.GetValue('Value'))
    locAccessoryPassword = p_web.GetValue('Value')
  End
  If locAccessoryPassword = ''
    loc:Invalid = 'locAccessoryPassword'
    loc:alert = p_web.translate('Password Required For Confirmation') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locAccessoryPassword = Upper(locAccessoryPassword)
    p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)
  do Value::locAccessoryPassword
  do SendAlert

Value::locAccessoryPassword  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPassword') & '_value',Choose(p_web.GSV('AccessoryPasswordRequired') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('AccessoryPasswordRequired') <> 1)
  ! --- STRING --- locAccessoryPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locAccessoryPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locAccessoryPassword = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locAccessoryPassword'',''individualdespatch_locaccessorypassword_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locAccessoryPassword',p_web.GetSessionValueFormat('locAccessoryPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locAccessoryPassword') & '_value')

Comment::locAccessoryPassword  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPassword') & '_comment',Choose(p_web.GSV('AccessoryPasswordRequired') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('AccessoryPasswordRequired') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonConfirmMismatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonConfirmMismatch',p_web.GetValue('NewValue'))
    do Value::buttonConfirmMismatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ! Mismatch Confirmation
  if (p_web.GSV('AccessoryPasswordRequired') = 1)
      if (p_web.GSV('locAccessoryPassword') = '')
          p_web.SSV('locAccessoryPasswordMessage','Password Required')    
      else
          If (SecurityCheckFailed(p_web.GSV('locAccessoryPassword'),'ACCESSORY MISMATCH - ACCEPT'))
              p_web.SSV('locAccessoryPasswordMessage','The selected password does not have access to this option')
          else
              p_web.SSV('locAccessoryMessage','Accessory Validated')
              p_web.SSV('locAccessoryErrorMessage','')
              p_web.SSV('Hide:ValidateAccessoriesButton',1)
              p_web.SSV('AccessoryConfirmationRequired',0)
              p_web.SSV('AccessoriesValidated',1)
              p_web.SSV('AccessoryPasswordRequired',0)
              p_web.SSV('locAccessoryPasswordMessage','')
          end
      end
  else
      p_web.SSV('locAccessoryMessage','Accessory Validated')
      p_web.SSV('locAccessoryErrorMessage','')
      p_web.SSV('Hide:ValidateAccessoriesButton',1)
      p_web.SSV('AccessoryConfirmationRequired',0)
      p_web.SSV('AccessoriesValidated',1)
      p_web.SSV('AccessoryPasswordRequired',0)
      p_web.SSV('locAccessoryPasswordMessage','')
      DO ShowConsignmentNumber
  end
  do Value::buttonConfirmMismatch
  do SendAlert
  do Value::locAccessoryPasswordMessage  !1
  do Value::buttonValidateAccessories  !1
  do Value::TagValidateLoanAccessories  !1
  do Value::locAccessoryMessage  !1
  do Value::buttonConfirmDespatch  !1
  do Prompt::locSecurityPackID
  do Value::locSecurityPackID  !1
  do Prompt::locAccessoryPassword
  do Value::locAccessoryPassword  !1
  do Value::locAccessoryErrorMessage  !1
  do Value::buttonFailAccessory  !1
  do Prompt::cou:Courier
  do Value::cou:Courier  !1
  do Prompt::locConsignmentNumber
  do Value::locConsignmentNumber  !1

Value::buttonConfirmMismatch  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonConfirmMismatch') & '_value',Choose(NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1),'hdiv','adiv'))
  loc:extra = ''
  If Not (NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1))
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonConfirmMismatch'',''individualdespatch_buttonconfirmmismatch_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ConfirmMismatch',p_web.GSV('ConfirmMismatchText'),'button-entryfield',loc:formname,,,,loc:javascript,0,'images\tick.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('buttonConfirmMismatch') & '_value')

Comment::buttonConfirmMismatch  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonConfirmMismatch') & '_comment',Choose(NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1)
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonFailAccessory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonFailAccessory',p_web.GetValue('NewValue'))
    do Value::buttonFailAccessory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ! Fail Accessory Check
  p_web.SSV('GetStatus:StatusNumber',850)
  p_web.SSV('GetStatus:Type',p_web.GSV('DespatchType'))
  GetStatus(850,0,p_web.GSV('DespatchType'),p_web)
  
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = p_web.GSV('job:Ref_Number')
  IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
      p_web.SessionQueueToFile(JOBS)
      Access:JOBS.TryUpdate()
  END
  
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
      p_web.SessionQueueToFile(WEBJOB)
      Access:WEBJOB.TryUpdate()
  END
  
  locAuditTrail = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
  IF (p_web.GSV('DespatchType') <> 'Loan')
      Access:JOBACC.ClearKey(jac:Ref_Number_Key)
      jac:Ref_Number = job:Ref_Number
      Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
      Loop
          If Access:JOBACC.NEXT()
              Break
          End !If
          If jac:Ref_Number <> job:Ref_Number      |
              Then Break.  ! End If
          locAuditTrail = CLIP(locAuditTrail) & '<13,10>' & Clip(jac:Accessory)
      End !Loop
  ELSE
      Access:LOANACC.ClearKey(lac:Ref_Number_Key)
      lac:Ref_Number = job:Ref_Number
      Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
      Loop
          If Access:LOANACC.NEXT()
              Break
          End !If
          If lac:Ref_Number <> job:Ref_Number      |
              Then Break.  ! End If
          locAuditTrail = CLIP(locAuditTrail) & '<13,10>' & Clip(lac:Accessory)
      End !Loop
  END
  locAuditTrail = CLIP(locAuditTrail) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
  
    Access:Tagging.ClearKey(tgg:SessionIDKey)
    tgg:SessionID = p_web.SessionID
    SET(tgg:SessionIDKey,tgg:SessionIDKey)
    LOOP UNTIL Access:Tagging.Next() <> Level:Benign
        IF (tgg:SessionID <> p_web.SessionID)
            BREAK
        END ! IF
        IF (tgg:Tagged = 0)
            CYCLE
        END ! IF
        locAuditTrail = CLIP(locAuditTrail) & '<13,10>' & Clip(tgg:TaggedValue)
    END ! LOOP
    
  
  !  p_web.SSV('AddToAudit:Type',p_web.GSV('DespatchType'))
  !  p_web.SSV('AddToAudit:Action','FAILED DESPATCH VALIDATION')
  !  p_web.SSV('AddToAudit:Notes',CLIP(locAuditTrail))
  AddToAudit(p_web,job:Ref_Number,p_web.GSV('DespatchType'),'FAILED DESPATCH VALIDATION',CLIP(locAuditTrail))
  
  
  DO ValidateUnitDetails
  
  do Value::buttonFailAccessory
  do SendAlert
  do Value::TagValidateLoanAccessories  !1
  do Value::buttonConfirmDespatch  !1
  do Value::buttonConfirmMismatch  !1
  do Value::buttonValidateAccessories  !1
  do Value::buttonValidateJobDetails  !1
  do Value::locErrorMessage  !1
  do Value::locIMEINumber  !1
  do Comment::locIMEINumber
  do Value::locJobNumber  !1
  do Comment::locJobNumber
  do Value::locAccessoryMessage  !1
  do Value::locAccessoryErrorMessage  !1
  do Prompt::cou:Courier
  do Value::cou:Courier  !1
  do Prompt::locConsignmentNumber
  do Value::locConsignmentNumber  !1

Value::buttonFailAccessory  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonFailAccessory') & '_value',Choose(NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1),'hdiv','adiv'))
  loc:extra = ''
  If Not (NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1))
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonFailAccessory'',''individualdespatch_buttonfailaccessory_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','FailAccessory','Fail Accessory Validation','button-entryfield',loc:formname,,,,loc:javascript,0,'\images\cross.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('buttonFailAccessory') & '_value')

Comment::buttonFailAccessory  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonFailAccessory') & '_comment',Choose(NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If NOT (p_web.GSV('AccessoryConfirmationRequired') = 1 AND p_web.GSV('AccessoriesValidated') <> 1)
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAccessoryPasswordMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPasswordMessage') & '_prompt',Choose(p_web.GSV('locAccessoryPasswordMessage') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locAccessoryPasswordMessage') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAccessoryPasswordMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAccessoryPasswordMessage',p_web.GetValue('NewValue'))
    locAccessoryPasswordMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAccessoryPasswordMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAccessoryPasswordMessage',p_web.GetValue('Value'))
    locAccessoryPasswordMessage = p_web.GetValue('Value')
  End

Value::locAccessoryPasswordMessage  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPasswordMessage') & '_value',Choose(p_web.GSV('locAccessoryPasswordMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAccessoryPasswordMessage') = '')
  ! --- DISPLAY --- locAccessoryPasswordMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAccessoryPasswordMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locAccessoryPasswordMessage') & '_value')

Comment::locAccessoryPasswordMessage  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locAccessoryPasswordMessage') & '_comment',Choose(p_web.GSV('locAccessoryPasswordMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAccessoryPasswordMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::cou:Courier  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('cou:Courier') & '_prompt',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Courier')
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1)
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('cou:Courier') & '_prompt')

Validate::cou:Courier  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('cou:Courier',p_web.GetValue('NewValue'))
    cou:Courier = p_web.GetValue('NewValue') !FieldType= STRING Field = cou:Courier
    do Value::cou:Courier
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('cou:Courier',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    cou:Courier = p_web.GetValue('Value')
  End
  DO ShowConsignmentNumber
  do Value::cou:Courier
  do SendAlert
  do Prompt::locConsignmentNumber
  do Value::locConsignmentNumber  !1
  do Prompt::locSecurityPackID
  do Value::locSecurityPackID  !1

Value::cou:Courier  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('cou:Courier') & '_value',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'hdiv','adiv'))
  loc:extra = ''
  If Not (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1))
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('cou:Courier')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''cou:Courier'',''individualdespatch_cou:courier_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('cou:Courier')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('cou:Courier',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('cou:Courier') = 0
    p_web.SetSessionValue('cou:Courier','')
  end
    packet = clip(packet) & p_web.CreateOption('','',choose('' = p_web.getsessionvalue('cou:Courier')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(Tagging)
  bind(tgg:Record)
  p_web._OpenFile(LOANACC)
  bind(lac:Record)
  p_web._OpenFile(JOBACC)
  bind(jac:Record)
  p_web._OpenFile(WEBJOB)
  bind(wob:Record)
  p_web._OpenFile(WAYBILLJ)
  bind(waj:Record)
  p_web._OpenFile(WAYBILLS)
  bind(way:Record)
  p_web._OpenFile(COURIER)
  bind(cou:Record)
  p_web._OpenFile(MULDESPJ)
  bind(mulj:Record)
  p_web._OpenFile(SUBTRACC)
  bind(sub:Record)
  p_web._OpenFile(LOAN)
  bind(loa:Record)
  p_web._OpenFile(TRADEACC)
  bind(tra:Record)
  p_web._OpenFile(EXCHANGE)
  bind(xch:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(cou:Courier_OptionView)
  cou:Courier_OptionView{prop:order} = 'UPPER(cou:Courier)'
  Set(cou:Courier_OptionView)
  Loop
    Next(cou:Courier_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('cou:Courier') = 0
      p_web.SetSessionValue('cou:Courier',cou:Courier)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cou:Courier,choose(cou:Courier = p_web.getsessionvalue('cou:Courier')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(cou:Courier_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(Tagging)
  p_Web._CloseFile(LOANACC)
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(WAYBILLJ)
  p_Web._CloseFile(WAYBILLS)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(MULDESPJ)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(JOBS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('cou:Courier') & '_value')

Comment::cou:Courier  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('cou:Courier') & '_comment',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1)
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locConsignmentNumber  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locConsignmentNumber') & '_prompt',Choose(p_web.GSV('Show:ConsignmentNumber') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Consignment Number')
  If p_web.GSV('Show:ConsignmentNumber') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locConsignmentNumber') & '_prompt')

Validate::locConsignmentNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locConsignmentNumber',p_web.GetValue('NewValue'))
    locConsignmentNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locConsignmentNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locConsignmentNumber',p_web.GetValue('Value'))
    locConsignmentNumber = p_web.GetValue('Value')
  End
    locConsignmentNumber = Upper(locConsignmentNumber)
    p_web.SetSessionValue('locConsignmentNumber',locConsignmentNumber)
  do Value::locConsignmentNumber
  do SendAlert

Value::locConsignmentNumber  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locConsignmentNumber') & '_value',Choose(p_web.GSV('Show:ConsignmentNumber') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Show:ConsignmentNumber') <> 1)
  ! --- STRING --- locConsignmentNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locConsignmentNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locConsignmentNumber'',''individualdespatch_locconsignmentnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locConsignmentNumber',p_web.GetSessionValueFormat('locConsignmentNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locConsignmentNumber') & '_value')

Comment::locConsignmentNumber  Routine
      loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locConsignmentNumber') & '_comment',Choose(p_web.GSV('Show:ConsignmentNumber') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Show:ConsignmentNumber') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locSecurityPackID  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locSecurityPackID') & '_prompt',Choose(p_web.GSV('Hide:SecurityPackID') = 1 OR (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1)),'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Security Pack ID')
  If p_web.GSV('Hide:SecurityPackID') = 1 OR (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1))
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locSecurityPackID') & '_prompt')

Validate::locSecurityPackID  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSecurityPackID',p_web.GetValue('NewValue'))
    locSecurityPackID = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSecurityPackID
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locSecurityPackID',p_web.GetValue('Value'))
    locSecurityPackID = p_web.GetValue('Value')
  End
  do Value::locSecurityPackID
  do SendAlert

Value::locSecurityPackID  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locSecurityPackID') & '_value',Choose(p_web.GSV('Hide:SecurityPackID') = 1 OR (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1)),'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:SecurityPackID') = 1 OR (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1)))
  ! --- STRING --- locSecurityPackID
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locSecurityPackID')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locSecurityPackID'',''individualdespatch_locsecuritypackid_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locSecurityPackID',p_web.GetSessionValueFormat('locSecurityPackID'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('locSecurityPackID') & '_value')

Comment::locSecurityPackID  Routine
      loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('locSecurityPackID') & '_comment',Choose(p_web.GSV('Hide:SecurityPackID') = 1 OR (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1)),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:SecurityPackID') = 1 OR (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1))
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonConfirmDespatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonConfirmDespatch',p_web.GetValue('NewValue'))
    do Value::buttonConfirmDespatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::buttonConfirmDespatch
  do SendAlert

Value::buttonConfirmDespatch  Routine
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonConfirmDespatch') & '_value',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'hdiv','adiv'))
  loc:extra = ''
  If Not (NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1))
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonConfirmDespatch'',''individualdespatch_buttonconfirmdespatch_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ConfirmDespatch','Confirm Despatch','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('DespatchConfirmation?' &'firsttime=1')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('IndividualDespatch_' & p_web._nocolon('buttonConfirmDespatch') & '_value')

Comment::buttonConfirmDespatch  Routine
    loc:comment = ''
  p_web._DivHeader('IndividualDespatch_' & p_web._nocolon('buttonConfirmDespatch') & '_comment',Choose(NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If NOT (p_web.GSV('UnitValidated') = 1 AND p_web.GSV('AccessoriesValidated') = 1)
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('IndividualDespatch_locJobNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobNumber
      else
        do Value::locJobNumber
      end
  of lower('IndividualDespatch_locIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIMEINumber
      else
        do Value::locIMEINumber
      end
  of lower('IndividualDespatch_buttonValidateJobDetails_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonValidateJobDetails
      else
        do Value::buttonValidateJobDetails
      end
  of lower('IndividualDespatch_buttonValidateAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonValidateAccessories
      else
        do Value::buttonValidateAccessories
      end
  of lower('IndividualDespatch_locAccessoryPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAccessoryPassword
      else
        do Value::locAccessoryPassword
      end
  of lower('IndividualDespatch_buttonConfirmMismatch_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonConfirmMismatch
      else
        do Value::buttonConfirmMismatch
      end
  of lower('IndividualDespatch_buttonFailAccessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonFailAccessory
      else
        do Value::buttonFailAccessory
      end
  of lower('IndividualDespatch_cou:Courier_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::cou:Courier
      else
        do Value::cou:Courier
      end
  of lower('IndividualDespatch_locConsignmentNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locConsignmentNumber
      else
        do Value::locConsignmentNumber
      end
  of lower('IndividualDespatch_locSecurityPackID_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSecurityPackID
      else
        do Value::locSecurityPackID
      end
  of lower('IndividualDespatch_buttonConfirmDespatch_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonConfirmDespatch
      else
        do Value::buttonConfirmDespatch
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('IndividualDespatch_form:ready_',1)
  p_web.SetSessionValue('IndividualDespatch_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_IndividualDespatch',0)

PreCopy  Routine
  p_web.SetValue('IndividualDespatch_form:ready_',1)
  p_web.SetSessionValue('IndividualDespatch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_IndividualDespatch',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('IndividualDespatch_form:ready_',1)
  p_web.SetSessionValue('IndividualDespatch_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('IndividualDespatch:Primed',0)

PreDelete       Routine
  p_web.SetValue('IndividualDespatch_form:ready_',1)
  p_web.SetSessionValue('IndividualDespatch_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('IndividualDespatch:Primed',0)
  p_web.setsessionvalue('showtab_IndividualDespatch',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('IndividualDespatch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('IndividualDespatch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
        If locJobNumber = ''
          loc:Invalid = 'locJobNumber'
          loc:alert = p_web.translate('Job Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
        If locIMEINumber = ''
          loc:Invalid = 'locIMEINumber'
          loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
  ! tab = 4
    loc:InvalidTab += 1
      If not (p_web.GSV('AccessoryPasswordRequired') <> 1)
        If locAccessoryPassword = ''
          loc:Invalid = 'locAccessoryPassword'
          loc:alert = p_web.translate('Password Required For Confirmation') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locAccessoryPassword = Upper(locAccessoryPassword)
          p_web.SetSessionValue('locAccessoryPassword',locAccessoryPassword)
        If loc:Invalid <> '' then exit.
      End
  ! tab = 5
    loc:InvalidTab += 1
      If not (p_web.GSV('Show:ConsignmentNumber') <> 1)
          locConsignmentNumber = Upper(locConsignmentNumber)
          p_web.SetSessionValue('locConsignmentNumber',locConsignmentNumber)
        If loc:Invalid <> '' then exit.
      End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('IndividualDespatch:Primed',0)
  p_web.StoreValue('locJobNumber')
  p_web.StoreValue('locIMEINumber')
  p_web.StoreValue('')
  p_web.StoreValue('locErrorMessage')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locAccessoryMessage')
  p_web.StoreValue('locAccessoryErrorMessage')
  p_web.StoreValue('locAccessoryPassword')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locAccessoryPasswordMessage')
  p_web.StoreValue('cou:Courier')
  p_web.StoreValue('locConsignmentNumber')
  p_web.StoreValue('locSecurityPackID')
  p_web.StoreValue('')
