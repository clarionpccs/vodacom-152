

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER411.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER013.INC'),ONCE        !Req'd for module callout resolution
                     END


WarrantyBatchProcessingBrowse PROCEDURE  (NetWebServerWorker p_web)
locJobNumber         STRING(60)                            !
locClaimValue        REAL                                  !
locFilterStartDate   DATE                                  !
locFilterEndDate     DATE                                  !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(SBO_WarrantyClaims)
                      Project(sbojow:RecordNumber)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
JOBS::State  USHORT
JOBSWARR::State  USHORT
WEBJOB::State  USHORT
JOBSE::State  USHORT
TagFile::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('WarrantyBatchProcessingBrowse')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('WarrantyBatchProcessingBrowse:NoForm')
      loc:NoForm = p_web.GetValue('WarrantyBatchProcessingBrowse:NoForm')
      loc:FormName = p_web.GetValue('WarrantyBatchProcessingBrowse:FormName')
    else
      loc:FormName = 'WarrantyBatchProcessingBrowse_frm'
    End
    p_web.SSV('WarrantyBatchProcessingBrowse:NoForm',loc:NoForm)
    p_web.SSV('WarrantyBatchProcessingBrowse:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('WarrantyBatchProcessingBrowse:NoForm')
    loc:FormName = p_web.GSV('WarrantyBatchProcessingBrowse:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('WarrantyBatchProcessingBrowse') & '_' & lower(loc:parent)
  else
    loc:divname = lower('WarrantyBatchProcessingBrowse')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('adiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(SBO_WarrantyClaims,sbojow:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'LOCJOBNUMBER') then p_web.SetValue('WarrantyBatchProcessingBrowse_sort','3')
    ElsIf (loc:vorder = 'JOB:MODEL_NUMBER') then p_web.SetValue('WarrantyBatchProcessingBrowse_sort','1')
    ElsIf (loc:vorder = 'JOB:WARRANTY_CHARGE_TYPE') then p_web.SetValue('WarrantyBatchProcessingBrowse_sort','2')
    ElsIf (loc:vorder = 'JOB:REPAIR_TYPE_WARRANTY') then p_web.SetValue('WarrantyBatchProcessingBrowse_sort','4')
    ElsIf (loc:vorder = 'LOCCLAIMVALUE') then p_web.SetValue('WarrantyBatchProcessingBrowse_sort','5')
    ElsIf (loc:vorder = 'WOB:RRCWINVOICENUMBER') then p_web.SetValue('WarrantyBatchProcessingBrowse_sort','8')
    ElsIf (loc:vorder = 'JOW:RRCDATERECONCILED') then p_web.SetValue('WarrantyBatchProcessingBrowse_sort','7')
    ElsIf (loc:vorder = 'JOW:DATEACCEPTED') then p_web.SetValue('WarrantyBatchProcessingBrowse_sort','6')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('WarrantyBatchProcessingBrowse:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('WarrantyBatchProcessingBrowse:LookupFrom','LookupFrom')
    p_web.StoreValue('WarrantyBatchProcessingBrowse:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('WarrantyBatchProcessingBrowse:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('WarrantyBatchProcessingBrowse:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  ! Save Window Name
  AddToLog('NetWebBrowse',p_web.RequestData.DataString,'WarrantyBatchProcessingBrowse',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('WarrantyBatchProcessingBrowse_sort',net:DontEvaluate)
  p_web.SetSessionValue('WarrantyBatchProcessingBrowse_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 9
    Loc:LocateField = ''
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'locJobNumber','-locJobNumber')
    Loc:LocateField = 'locJobNumber'
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(job:Model_Number)','-UPPER(job:Model_Number)')
    Loc:LocateField = 'job:Model_Number'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(job:Warranty_Charge_Type)','-UPPER(job:Warranty_Charge_Type)')
    Loc:LocateField = 'job:Warranty_Charge_Type'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(job:Repair_Type_Warranty)','-UPPER(job:Repair_Type_Warranty)')
    Loc:LocateField = 'job:Repair_Type_Warranty'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'locClaimValue','-locClaimValue')
    Loc:LocateField = 'locClaimValue'
  of 8
    loc:vorder = Choose(Loc:SortDirection=1,'wob:RRCWInvoiceNumber','-wob:RRCWInvoiceNumber')
    Loc:LocateField = 'wob:RRCWInvoiceNumber'
  of 7
    loc:vorder = Choose(Loc:SortDirection=1,'jow:RRCDateReconciled','-jow:RRCDateReconciled')
    Loc:LocateField = 'jow:RRCDateReconciled'
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'jow:DateAccepted','-jow:DateAccepted')
    Loc:LocateField = 'jow:DateAccepted'
  of 11
    Loc:LocateField = ''
  end
  if loc:vorder = ''
    Loc:LocateField = 'jow:RefNumber'
    loc:sortheader = 'Job Number'
    loc:vorder = '+UPPER(jow:RefNumber)'
  end
  If False ! add range fields to sort order
  ElsIf (p_web.GSV('locBatchProcessingType') = 1 AND p_web.GSV('locFilterByDate') = 0)
  ElsIf (p_web.GSV('locBatchProcessingType') = 1 AND p_web.GSV('locFilterByDate') = 1)
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('tag:tagged')
    loc:SortHeader = p_web.Translate('')
    p_web.SetSessionValue('WarrantyBatchProcessingBrowse_LocatorPic','@n3')
  Of upper('locJobNumber')
    loc:SortHeader = p_web.Translate('Job Number')
    p_web.SetSessionValue('WarrantyBatchProcessingBrowse_LocatorPic','@s60')
  Of upper('job:Model_Number')
    loc:SortHeader = p_web.Translate('Model Number')
    p_web.SetSessionValue('WarrantyBatchProcessingBrowse_LocatorPic','@s30')
  Of upper('job:Warranty_Charge_Type')
    loc:SortHeader = p_web.Translate('Charge Type')
    p_web.SetSessionValue('WarrantyBatchProcessingBrowse_LocatorPic','@s30')
  Of upper('job:Repair_Type_Warranty')
    loc:SortHeader = p_web.Translate('Repair Type')
    p_web.SetSessionValue('WarrantyBatchProcessingBrowse_LocatorPic','@s30')
  Of upper('locClaimValue')
    loc:SortHeader = p_web.Translate('Claim Value')
    p_web.SetSessionValue('WarrantyBatchProcessingBrowse_LocatorPic','@n_14.2')
  Of upper('wob:RRCWInvoiceNumber')
    loc:SortHeader = p_web.Translate('Invoice No')
    p_web.SetSessionValue('WarrantyBatchProcessingBrowse_LocatorPic','@s8')
  Of upper('jow:RRCDateReconciled')
    loc:SortHeader = p_web.Translate('Date Reconciled')
    p_web.SetSessionValue('WarrantyBatchProcessingBrowse_LocatorPic','@d6')
  Of upper('jow:DateAccepted')
    loc:SortHeader = p_web.Translate('Date Accepted')
    p_web.SetSessionValue('WarrantyBatchProcessingBrowse_LocatorPic','@d6')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('WarrantyBatchProcessingBrowse:LookupFrom')
  End!Else
  loc:formaction = 'WarrantyBatchProcessingBrowse'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="WarrantyBatchProcessingBrowse:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="WarrantyBatchProcessingBrowse:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('WarrantyBatchProcessingBrowse:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="SBO_WarrantyClaims"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="sbojow:RecordNumberKey"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'WarrantyBatchProcessingBrowse',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator fixedtd')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2WarrantyBatchProcessingBrowse',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator fixedtd',,'onchange="WarrantyBatchProcessingBrowse.locate(''Locator2WarrantyBatchProcessingBrowse'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'WarrantyBatchProcessingBrowse.cl(''WarrantyBatchProcessingBrowse'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="WarrantyBatchProcessingBrowse_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="WarrantyBatchProcessingBrowse_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
  If (p_web.GSV('locBatchProcessingType') = 0) AND  true
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'9','WarrantyBatchProcessingBrowse','Mark',,,'BrowseWhite',,1)
        Else
          packet = clip(packet) & '<th class="'&clip('BrowseWhite')&'">'&p_web.Translate('Mark')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  End ! Field condition
  If (p_web.GSV('locBatchProcessingType') = 0) AND  true
        packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by tagged')&'">'&p_web.Translate('')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
  End ! Field condition
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','WarrantyBatchProcessingBrowse','Job Number',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Job Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','WarrantyBatchProcessingBrowse','Model Number','Click here to sort by Model Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Model Number')&'">'&p_web.Translate('Model Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','WarrantyBatchProcessingBrowse','Charge Type','Click here to sort by Charge Type',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Charge Type')&'">'&p_web.Translate('Charge Type')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','WarrantyBatchProcessingBrowse','Repair Type','Click here to sort by Repair Type',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Repair Type')&'">'&p_web.Translate('Repair Type')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  If (p_web.GSV('locBatchProcessingType') = 0) AND  true
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','WarrantyBatchProcessingBrowse','Claim Value',,,'RightJustify',,1)
        Else
          packet = clip(packet) & '<th class="RightJustify">'&p_web.Translate('Claim Value')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  End ! Field condition
  If (p_web.GSV('locBatchProcessingType') = 1) AND  true
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'8','WarrantyBatchProcessingBrowse','Invoice No','Click here to sort by Warranty Invoice Number',,'RightJustify',,1)
        Else
          packet = clip(packet) & '<th class="RightJustify" Title="'&p_web.Translate('Click here to sort by Warranty Invoice Number')&'">'&p_web.Translate('Invoice No')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  End ! Field condition
  If (p_web.GSV('locBatchProcessingType') = 1) AND  true
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'7','WarrantyBatchProcessingBrowse','Date Reconciled','Click here to sort by Date Reconciled',,'RightJustify',,1)
        Else
          packet = clip(packet) & '<th class="RightJustify" Title="'&p_web.Translate('Click here to sort by Date Reconciled')&'">'&p_web.Translate('Date Reconciled')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  End ! Field condition
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'6','WarrantyBatchProcessingBrowse','Date Accepted','Click here to sort by Date Accepted',,'RightJustify',,1)
        Else
          packet = clip(packet) & '<th class="RightJustify" Title="'&p_web.Translate('Click here to sort by Date Accepted')&'">'&p_web.Translate('Date Accepted')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  btnViewJob
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'jow:RRCDateReconciled' then Loc:NoBuffer = 1.
  If Loc:LocateField = 'jow:DateAccepted' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('sbojow:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and SBO_WarrantyClaims{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'sbojow:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sbojow:RecordNumber'),p_web.GetValue('sbojow:RecordNumber'),p_web.GetSessionValue('sbojow:RecordNumber'))
  If False  ! Generate Filter
  ElsIf (p_web.GSV('locBatchProcessingType') = 1 AND p_web.GSV('locFilterByDate') = 0)
      loc:FilterWas = 'sbojow:SessionID = ' & p_web.SessionID & ' AND UPPER(sbojow:RRCStatus) = ''PAY'' AND UPPER(sbojow:Manufacturer) = UPPER(''' & p_web.GSV('locWarrantyManufacturer') & ''')'
  ElsIf (p_web.GSV('locBatchProcessingType') = 1 AND p_web.GSV('locFilterByDate') = 1)
      locFilterStartDate = p_web.RestoreValue('locFilterStartDate')
      locFilterEndDate = p_web.RestoreValue('locFilterEndDate')
      loc:FilterWas = 'sbojow:DateReconciled >= ''' & clip(locFilterStartDate) & ''' AND sbojow:DateReconciled <= ''' & clip(locFilterEndDate) & ''''
      If 'sbojow:SessionID = ' & p_web.SessionID & ' AND UPPER(sbojow:RRCStatus) = ''PAY'' AND UPPER(sbojow:Manufacturer) = UPPER(''' & p_web.GSV('locWarrantyManufacturer') & ''')' <> ''
        loc:FilterWas = clip(loc:FilterWas) & ' AND ' & 'sbojow:SessionID = ' & p_web.SessionID & ' AND UPPER(sbojow:RRCStatus) = ''PAY'' AND UPPER(sbojow:Manufacturer) = UPPER(''' & p_web.GSV('locWarrantyManufacturer') & ''')'
      End
  Else
        loc:FilterWas = 'sbojow:SessionID = ' & p_web.SessionID & ' AND UPPER(sbojow:RRCStatus) = ''APP'' AND UPPER(sbojow:Manufacturer) = UPPER(''' & p_web.GSV('locWarrantyManufacturer') & ''')'
  End
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'WarrantyBatchProcessingBrowse',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('WarrantyBatchProcessingBrowse_Filter')
    p_web.SetSessionValue('WarrantyBatchProcessingBrowse_FirstValue','')
    p_web.SetSessionValue('WarrantyBatchProcessingBrowse_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,SBO_WarrantyClaims,sbojow:RecordNumberKey,loc:PageRows,'WarrantyBatchProcessingBrowse',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If SBO_WarrantyClaims{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(SBO_WarrantyClaims,loc:firstvalue)
              Reset(ThisView,SBO_WarrantyClaims)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If SBO_WarrantyClaims{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(SBO_WarrantyClaims,loc:lastvalue)
            Reset(ThisView,SBO_WarrantyClaims)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
        Access:JOBSWARR.ClearKey(jow:RefNumberKey)
        jow:RefNumber = sbojow:JobNumber
        IF (Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign)
            
        ELSE ! IF
            CYCLE
        END ! IF
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = jow:RefNumber
        IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
            
        ELSE ! IF
            CYCLE
        END ! IF
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
            
        ELSE ! IF
        END ! IF
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
            
        ELSE ! IF
        END ! IF
        
        IF (p_web.GSV('locBatchProcessingType') = 0)
            IF (jow:DateAccepted < p_web.GSV('locPaidStartDate') OR | 
                jow:DateAccepted > p_web.GSV('locPaidEndDate'))
                CYCLE
            END ! IF
        ELSE ! 
      !            IF (jow:RRCDateReconciled < p_web.GSV('locWarrantyStartDate') OR | 
      !                jow:RRCDateReconciled > p_web.GSV('locWarrantyEndDate'))
      !                CYCLE
      !            END ! IF
        END ! 
        
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sbojow:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'WarrantyBatchProcessingBrowse.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'WarrantyBatchProcessingBrowse.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'WarrantyBatchProcessingBrowse.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'WarrantyBatchProcessingBrowse.last();',,loc:nextdisabled)
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'WarrantyBatchProcessingBrowse',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('WarrantyBatchProcessingBrowse_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('WarrantyBatchProcessingBrowse_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator fixedtd')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1WarrantyBatchProcessingBrowse',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator fixedtd',,'onchange="WarrantyBatchProcessingBrowse.locate(''Locator1WarrantyBatchProcessingBrowse'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'WarrantyBatchProcessingBrowse.cl(''WarrantyBatchProcessingBrowse'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('WarrantyBatchProcessingBrowse_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('WarrantyBatchProcessingBrowse_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'WarrantyBatchProcessingBrowse.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'WarrantyBatchProcessingBrowse.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'WarrantyBatchProcessingBrowse.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'WarrantyBatchProcessingBrowse.last();',,loc:nextdisabled)
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    locJobNumber = CLIP(job:Ref_Number) & '-' & p_web.GSV('locBranchID') & CLIP(wob:JobNumber)  
    locClaimValue  = jobe:RRCWLabourCost + jobe:RRCWPartsCost
    
    Access:TAGFILE.Clearkey(tag:keyTagged)
    tag:sessionID    = p_web.SessionID
    tag:taggedValue    = sbojow:JobNumber
    if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
        ! Found
    else ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
        ! Error
        tag:Tagged = 0
    end ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
    
    loc:field = sbojow:RecordNumber
    p_web._thisrow = p_web._nocolon('sbojow:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('WarrantyBatchProcessingBrowse:LookupField')) = sbojow:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((sbojow:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="WarrantyBatchProcessingBrowse.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If SBO_WarrantyClaims{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(SBO_WarrantyClaims)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If SBO_WarrantyClaims{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(SBO_WarrantyClaims)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','sbojow:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="WarrantyBatchProcessingBrowse.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','sbojow:RecordNumber',clip(loc:field),,'checked',,,'onclick="WarrantyBatchProcessingBrowse.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
      If (p_web.GSV('locBatchProcessingType') = 0) AND  true
          If Loc:Eip = 0
            if false
            elsif wob:ReconciledMarker = 1
              packet = clip(packet) & '<td class="'&clip('BrowseGreen')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif wob:ReconciledMarker = 2
              packet = clip(packet) & '<td class="'&clip('BrowseBlue')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            elsif wob:ReconciledMarker = 3
              packet = clip(packet) & '<td class="'&clip('BrowseRed')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            else
              packet = clip(packet) & '<td class="'&clip('BrowseWhite')&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            end
          end ! loc:eip = 0
          do value::brwMark
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      If (p_web.GSV('locBatchProcessingType') = 0) AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::tag:tagged
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locJobNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job:Model_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job:Warranty_Charge_Type
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job:Repair_Type_Warranty
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      If (p_web.GSV('locBatchProcessingType') = 0) AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="RightJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locClaimValue
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      If (p_web.GSV('locBatchProcessingType') = 1) AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="RightJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::wob:RRCWInvoiceNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
      If (p_web.GSV('locBatchProcessingType') = 1) AND  true
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="RightJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::jow:RRCDateReconciled
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      End ! Field Condition
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="RightJustify">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::jow:DateAccepted
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::btnViewJob
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="WarrantyBatchProcessingBrowse.omv(this);" onMouseOut="WarrantyBatchProcessingBrowse.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var WarrantyBatchProcessingBrowse=new browseTable(''WarrantyBatchProcessingBrowse'','''&clip(loc:formname)&''','''&p_web._jsok('sbojow:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('sbojow:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'WarrantyBatchProcessingBrowse.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'WarrantyBatchProcessingBrowse.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2WarrantyBatchProcessingBrowse')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1WarrantyBatchProcessingBrowse')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1WarrantyBatchProcessingBrowse')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2WarrantyBatchProcessingBrowse')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(SBO_WarrantyClaims)
  p_web._CloseFile(JOBS)
  p_web._CloseFile(JOBSWARR)
  p_web._CloseFile(WEBJOB)
  p_web._CloseFile(JOBSE)
  p_web._CloseFile(TagFile)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(SBO_WarrantyClaims)
  Bind(sbojow:Record)
  Clear(sbojow:Record)
  NetWebSetSessionPics(p_web,SBO_WarrantyClaims)
  p_web._OpenFile(JOBS)
  Bind(job:Record)
  NetWebSetSessionPics(p_web,JOBS)
  p_web._OpenFile(JOBSWARR)
  Bind(jow:Record)
  NetWebSetSessionPics(p_web,JOBSWARR)
  p_web._OpenFile(WEBJOB)
  Bind(wob:Record)
  NetWebSetSessionPics(p_web,WEBJOB)
  p_web._OpenFile(JOBSE)
  Bind(jobe:Record)
  NetWebSetSessionPics(p_web,JOBSE)
  p_web._OpenFile(TagFile)
  Bind(tag:Record)
  NetWebSetSessionPics(p_web,TagFile)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('sbojow:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
      ! Save Window Name
      IF (loc:alert <> '')
          AddToLog('Alert',loc:alert,'WarrantyBatchProcessingBrowse',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
      END ! IF
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(SBO_WarrantyClaims)
  Case upper(p_web.GetValue('_EIPClm'))
  of upper('tag:tagged')
    do Validate::tag:tagged
  of upper('btnViewJob')
    do Validate::btnViewJob
  End
  p_web._CloseFile(SBO_WarrantyClaims)
! ----------------------------------------------------------------------------------------
value::brwMark   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('locBatchProcessingType') = 0)
    if false
    elsif wob:ReconciledMarker = 1
      packet = clip(packet) & p_web._DivHeader('brwMark_'&sbojow:RecordNumber,'BrowseGreen',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok('',0))
    elsif wob:ReconciledMarker = 2
      packet = clip(packet) & p_web._DivHeader('brwMark_'&sbojow:RecordNumber,'BrowseBlue',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok('',0))
    elsif wob:ReconciledMarker = 3
      packet = clip(packet) & p_web._DivHeader('brwMark_'&sbojow:RecordNumber,'BrowseRed',net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok('',0))
    else
      packet = clip(packet) & p_web._DivHeader('brwMark_'&sbojow:RecordNumber,'BrowseWhite',net:crc)
      packet = clip(packet) & p_web._jsok('',0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
Validate::tag:tagged  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  sbojow:RecordNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  !#Bryan: Added embed to set extra key components for multi part keys
  loc:result = p_web._GetFile(SBO_WarrantyClaims,sbojow:RecordNumberKey)
  p_web.FileToSessionQueue(SBO_WarrantyClaims)
  loc:was = tag:tagged
  tag:tagged = Choose(p_web.GetValue('value') = true,true,false)
        Access:TAGFILE.Clearkey(tag:keyTagged)
        tag:sessionID    = p_web.SessionID
        tag:taggedValue    = sbojow:JobNumber
        if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
      ! Found
            tag:tagged = Choose(p_web.getValue('value') = 1,true,false)
            access:TAGFILE.tryUpdate()
        else ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
      ! Error
            if (Access:TAGFILE.PrimeRecord() = Level:Benign)
                tag:sessionID    = p_web.sessionID
                tag:taggedValue    = sbojow:JobNumber
                tag:tagged = Choose(p_web.getValue('value') = 1,true,false)
                if (Access:TAGFILE.TryInsert() = Level:Benign)
              ! Inserted
                else ! if (Access:TAGFILE.TryInsert() = Level:Benign)
              ! Error
                end ! if (Access:TAGFILE.TryInsert() = Level:Benign)
            end ! if (Access:TAGFILE.PrimeRecord() = Level:Benign)
        end ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)  
  ! Validation goes here, if fails set loc:invalid & loc:alert
  do CheckForDuplicate
  If loc:invalid = '' ! save record
      p_web._updatefile(TagFile)
      p_web.FileToSessionQueue(TagFile)
  End
  do SendAlert
  if loc:alert <> ''
    tag:tagged = loc:was
    do Value::tag:tagged
  End
  ! updating other browse cells goes here.
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::tag:tagged   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('locBatchProcessingType') = 0)
    if false
    else
      packet = clip(packet) & p_web._DivHeader('tag:tagged_'&sbojow:RecordNumber,,net:crc)
      loc:extra = Choose(tag:tagged = true,'checked','')
      packet = clip(packet) & p_web.CreateInput('checkbox','inp'&p_web.crc32('tag:tagged'&sbojow:RecordNumber),clip(true),,loc:extra & ' ' & loc:disabled,,,'onclick="WarrantyBatchProcessingBrowse.eip(this,'''&p_web._jsok('tag:tagged')&''','''&p_web._jsok(loc:viewstate)&''');"',,) & '<13,10>'
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
value::locJobNumber   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locJobNumber_'&sbojow:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locJobNumber,'@s60')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::job:Model_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job:Model_Number_'&sbojow:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job:Model_Number,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::job:Warranty_Charge_Type   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job:Warranty_Charge_Type_'&sbojow:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job:Warranty_Charge_Type,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::job:Repair_Type_Warranty   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job:Repair_Type_Warranty_'&sbojow:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job:Repair_Type_Warranty,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locClaimValue   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('locBatchProcessingType') = 0)
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locClaimValue_'&sbojow:RecordNumber,'RightJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locClaimValue,'@n_14.2')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
value::wob:RRCWInvoiceNumber   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('locBatchProcessingType') = 1)
    if false
    else
      packet = clip(packet) & p_web._DivHeader('wob:RRCWInvoiceNumber_'&sbojow:RecordNumber,'RightJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(wob:RRCWInvoiceNumber,'@s8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
value::jow:RRCDateReconciled   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If (p_web.GSV('locBatchProcessingType') = 1)
    if false
    else
      packet = clip(packet) & p_web._DivHeader('jow:RRCDateReconciled_'&sbojow:RecordNumber,'RightJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(jow:RRCDateReconciled,'@d6')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
  End
! ----------------------------------------------------------------------------------------
value::jow:DateAccepted   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('jow:DateAccepted_'&sbojow:RecordNumber,'RightJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(jow:DateAccepted,'@d6')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
Validate::btnViewJob  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  sbojow:RecordNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  !#Bryan: Added embed to set extra key components for multi part keys
  loc:result = p_web._GetFile(SBO_WarrantyClaims,sbojow:RecordNumberKey)
  p_web.FileToSessionQueue(SBO_WarrantyClaims)
  do CheckForDuplicate
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::btnViewJob   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('btnViewJob_'&sbojow:RecordNumber,,net:crc)
      loc:disabled = ''
      packet = clip(packet) & p_web.CreateButton('button','btnViewJob','View Job','button-flat',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ViewJob?&wob__RecordNumber=' & wob:RecordNumber & '&Change_btn=Change&ViewJobReturnURL=WarrantyBatchProcessing')& '' ) & ''','''&clip('_self')&''')',,loc:disabled,,,,,) & '<13,10>' !3
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(JOBS)
  p_web._OpenFile(JOBSWARR)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(TagFile)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(JOBSWARR)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(TagFile)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = sbojow:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('sbojow:RecordNumber',sbojow:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('sbojow:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('sbojow:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('sbojow:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
