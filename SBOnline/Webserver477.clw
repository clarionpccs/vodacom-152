

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER477.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! Report the JOBS File
!!! </summary>
OBFValidationReport PROCEDURE (NetWebServerWorker p_web)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
locJobNumber         LONG                                  !
tmp:DefaultTelephone STRING(20)                            !
tmp:DefaultFax       STRING(20)                            !
code_temp            BYTE                                  !
option_temp          BYTE                                  !
bar_code_string_temp CSTRING(21)                           !
bar_code_temp        CSTRING(21)                           !
qa_reason_temp       STRING(255)                           !
engineer_temp        STRING(60)                            !
tmp:POP              STRING(3)                             !
tmp:OriginalPackaging STRING(3)                            !
tmp:OriginalBattery  STRING(3)                             !
tmp:OriginalCharger  STRING(3)                             !
tmp:OriginalAntenna  STRING(3)                             !
tmp:OriginalManuals  STRING(3)                             !
tmp:PhysicalDamage   STRING(3)                             !
tmp:FailureReason    STRING(255)                           !
tmp:Replacement      STRING(3)                             !
tmp:Failed           BYTE                                  !
Process:View         VIEW(JOBS)
                       PROJECT(job:DOP)
                       PROJECT(job:ESN)
                       PROJECT(job:Ref_Number)
                     END
ProgressWindow       WINDOW('Report JOBS'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

report               REPORT,AT(406,1042,7521,9781),PRE(rpt),PAPER(PAPER:A4),FONT('Arial',10,,FONT:regular),THOUS
detail                 DETAIL,AT(,,,9115),USE(?tmp:OriginalAntenna:Prompt:4)
                         STRING('Job Number:'),AT(1667,208),USE(?String2),FONT(,20,,FONT:bold,CHARSET:ANSI),TRN
                         STRING(@s8),AT(4063,208),USE(job:Ref_Number,,?JOB_ALI:Ref_Number:2),FONT(,20,,FONT:bold),LEFT, |
  TRN
                         STRING('Handset I.M.E.I.:'),AT(1302,781),USE(?String6),FONT(,18,,FONT:bold),TRN
                         STRING('Box I.M.E.I.:'),AT(1302,1115),USE(?String6:2),FONT(,18,,FONT:bold),TRN
                         STRING(@s20),AT(4063,1115),USE(jobe:BoxESN),FONT(,18,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Network:'),AT(1302,1448),USE(?String6:3),FONT(,18,,FONT:bold),TRN
                         STRING(@s30),AT(4063,1448),USE(jobe:Network),FONT(,18,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Purchase Date:'),AT(1302,1781),USE(?String6:4),FONT(,18,,FONT:bold),TRN
                         STRING(@d6),AT(4063,1781),USE(job:DOP),FONT(,18,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Return Date:'),AT(1302,2115),USE(?String6:5),FONT(,18,,FONT:bold),TRN
                         STRING(@s16),AT(4063,781),USE(job:ESN),FONT(,18,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Talk Time:'),AT(1302,2448),USE(?String6:6),FONT(,18,,FONT:bold),TRN
                         STRING(@d6),AT(4063,2115),USE(jobe:ReturnDate),FONT(,18,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Original Dealer:'),AT(1302,2781),USE(?String6:7),FONT(,18,,FONT:bold),TRN
                         STRING(@s3),AT(4063,2448),USE(jobe:TalkTime),FONT(,18,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Mins'),AT(4531,2448),USE(?String6:15),FONT(,18),TRN
                         STRING(@s254),AT(4063,2781),USE(jobe:OriginalDealer),FONT(,18,,,CHARSET:ANSI),LEFT
                         STRING('Branch Of Return:'),AT(1302,3115),USE(?String6:8),FONT(,18,,FONT:bold),TRN
                         STRING(@s30),AT(4063,3115),USE(jobe:BranchOfReturn),FONT(,18,,,CHARSET:ANSI),LEFT,TRN
                         STRING(@s30),AT(4063,3448),USE(jof:StoreReferenceNumber),FONT(,18,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Store Ref No:'),AT(1302,3448),USE(?String6:11),FONT(,18,,FONT:bold),TRN
                         STRING('Proof Of Purchase:'),AT(1302,3781),USE(?String6:9),FONT(,18,,FONT:bold),TRN
                         STRING('Original Packaging:'),AT(1302,4115),USE(?String6:10),FONT(,18,,FONT:bold),TRN
                         STRING(@s3),AT(4063,3781),USE(tmp:POP),FONT(,18,,,CHARSET:ANSI),LEFT
                         STRING(@s3),AT(4063,4115),USE(tmp:OriginalPackaging),FONT(,18,,,CHARSET:ANSI),LEFT,TRN
                         STRING(@s3),AT(4063,5115),USE(tmp:PhysicalDamage),FONT(,18)
                         STRING(@s3),AT(4063,5448),USE(tmp:Replacement),FONT(,18),TRN
                         STRING(@s30),AT(4063,5781),USE(jof:LAccountNumber),FONT(,18),TRN
                         STRING(@s30),AT(4063,6115),USE(jof:ReplacementIMEI),FONT(,18),TRN
                         STRING('Replacement IMEI No:'),AT(1302,6115),USE(?String6:17),FONT(,18,,FONT:bold),TRN
                         STRING('L/Account Number:'),AT(1302,5781),USE(?String6:16),FONT(,18,,FONT:bold),TRN
                         STRING('Replacement:'),AT(1302,5448),USE(?String6:13),FONT(,18,,FONT:bold),TRN
                         STRING('Failure Reason:'),AT(1302,6510),USE(?FailedReasonText),FONT(,18,,FONT:bold),HIDE,TRN
                         TEXT,AT(4063,6510,3125,1146),USE(tmp:FailureReason),FONT(,18),RESIZE,TRN
                         STRING('Physical Damage:'),AT(1302,5115),USE(?String6:14),FONT(,18,,FONT:bold),TRN
                         STRING(@s3),AT(4063,4781),USE(tmp:OriginalManuals),FONT(,18),TRN
                         STRING(@s3),AT(4063,4448),USE(tmp:OriginalBattery),FONT(,18),TRN
                         STRING('Original Accessories:'),AT(1302,4448),USE(?Third_Party_Agent),FONT(,18,,FONT:bold), |
  TRN
                         STRING('Original Manuals:'),AT(1302,4781),USE(?String6:12),FONT(,18,,FONT:bold),TRN
                       END
                       FORM,AT(396,479,7521,10552),USE(?unnamed:3)
                         STRING('OBF Validation Report'),AT(1208,52,5104,417),USE(?string20),FONT(,24,,FONT:bold),CENTER, |
  TRN
                         BOX,AT(104,521,7292,9844),USE(?Box1),COLOR(COLOR:Black)
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR3               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('OBFValidationReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
    locJobNumber = p_web.GSV('locJobNumber')
  Relate:AUDIT.SetOpenRelated()
  Relate:AUDIT.Open                                        ! File AUDIT used by this procedure, so make sure it's RelationManager is open
  Access:AUDIT2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSOBF.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('OBFValidationReport',ProgressWindow)       ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisReport.AddSortOrder(job:Ref_Number_Key)
  ThisReport.AddRange(job:Ref_Number,locJobNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
  END
  IF SELF.Opened
    INIMgr.Update('OBFValidationReport',ProgressWindow)    ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'OBFValidationReport',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.Init(report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,report{PROPPRINT:Paper},PAPER:USER), CHOOSE(report{PROP:Thous}=True,PROP:Thous,CHOOSE(report{PROP:MM}=True,PROP:MM,CHOOSE(report{PROP:Points}=True,PROP:Points,0))), report{PROPPRINT:PaperWidth}, report{PROPPRINT:PaperHeight}, report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetTitle('OBF Validation Report')    !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR3.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetViewerPrefs(PDFXTR3:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'OBFValidationReport',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR3.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
            
            Access:JOBSOBF.ClearKey(jof:RefNumberKey)
            jof:RefNumber = job:Ref_Number
            IF (Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign)
                
            END ! IF
            
            tmp:FailureReason = ''
            
            Found# = 0
            Access:AUDIT.ClearKey(aud:Action_Key)
            aud:Ref_Number = job:Ref_Number
            aud:Action     = 'O.B.F. VALIDATION FAILED'
            Set(aud:Action_Key,aud:Action_Key)
            LOOP UNTIL Access:AUDIT.Next() <> Level:Benign
                If aud:Ref_Number <> job_ali:Ref_Number      |
                    Or aud:Action     <> 'O.B.F. VALIDATION FAILED'      |
                    Then Break.  ! End If
                Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                aud2:AUDRecordNumber = aud:Record_Number
                IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                    SetTarget(Report)
                    ?FailedReasonText{prop:Hide} = 0
                    SetTarget()
                    tmp:FailureReason = Clip(aud2:Notes)
                    Found# = 1
                    Break
  
                END ! IF
            End !Loop
            
            IF (Found# = 0)
                Access:AUDIT.ClearKey(aud:Action_Key)
                aud:Ref_Number = job:Ref_Number
                aud:Action     = 'OBF REJECTED'
                Set(aud:Action_Key,aud:Action_Key)
                LOOP UNTIL Access:AUDIT.Next() <> Level:Benign
                    If aud:Ref_Number <> job_ali:Ref_Number      |
                        Or aud:Action     <> 'OBF REJECTED'      |
                        Then Break.  ! End If
                    Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                    aud2:AUDRecordNumber = aud:Record_Number
                    IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                        SetTarget(Report)
                        ?FailedReasonText{prop:Hide} = 0
                        SetTarget()
                        tmp:FailureReason = Clip(Sub(aud2:Notes,9,255))
                        Found# = 1
                        Break
  
                    END ! IF
                End !Loop
            END ! IF
            
            If jobe:ValidPOP = 'YES'
                tmp:POP = 'YES'
            Else !If jobe:ValidPOP
                tmp:POP = 'NO'
            End !If jobe:ValidPOP
  
            If jobe:OriginalPackaging
                tmp:OriginalPackaging = 'YES'
            Else !If jobe:OriginalPackaging
                tmp:OriginalPackaging = 'NO'
            End !If jobe:OriginalPackaging
  
            If jobe:OriginalBattery
                tmp:OriginalBattery = 'YES'
            Else !If jobe:OriginalBattery
                tmp:OriginalBattery = 'NO'
            End !If jobe:OriginalBattery
  
            If jobe:OriginalCharger
                tmp:OriginalCharger = 'YES'
            Else !If jobe:OriginalCharger
                tmp:OriginalCharger = 'NO'
            End !If jobe:OriginalCharger
  
            If jobe:OriginalAntenna
                tmp:OriginalAntenna = 'YES'
            Else !If jobe:OriginalAntenna
                tmp:OriginalAntenna = 'NO'
            End !If jobe:OriginalAntenna
  
            If jobe:OriginalManuals
                tmp:OriginalManuals = 'YES'
            Else !If jobe:OriginalManuals
                tmp:OriginalManuals = 'NO'
            End !If jobe:OriginalManuals
  
            If jobe:PhysicalDamage
                tmp:PhysicalDamage = 'YES'
            Else !If jobe:PhysicalDamage
                tmp:PhysicalDamage = 'NO'
            End !If jobe:PhysicalDamage
  
            If jof:Replacement
                tmp:Replacement = 'YES'
            Else ! If jof:Replacement
                tmp:Replacement = 'NO'
            End ! If jof:Replacement
  
            
        END ! IF
        
  PRINT(rpt:detail)
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR3:rtn = PDFXTR3.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR3:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

