

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER133.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ClearTagFile         PROCEDURE  (NetWebServerWorker p_web, LONG pForceDelete=0) ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
        DO OpenFiles
        STREAM(TagFile)

        Access:TAGFILE.Clearkey(tag:keyTagged)
        tag:sessionID    = p_web.sessionID
        set(tag:keyTagged,tag:keyTagged)
        loop
            if (Access:TAGFILE.Next())
                Break
            end ! if (Access:TAGFILE.Next())
            if (tag:sessionID    <> p_web.sessionID)
                Break
            end ! if (tag:sessionID    <> p_web.sessionID)
            IF (pForceDelete)
                ! #13575 Sometimes we do need to delete the tag entries (DBH: 28/07/2015)
                Access:TagFile.DeleteRecord(0)
            ELSE
                tag:tagged = 0
                Access:TagFile.TryUpdate()
            END ! IF
            
        end ! loop
        FLUSH(TagFile)

        DO CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:TagFile.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TagFile.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TagFile.Close
     FilesOpened = False
  END
