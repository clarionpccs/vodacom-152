

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER461.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
CreditNoteRequest PROCEDURE (NetWebServerWorker p_web)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
locCreditNoteNumber  LONG                                  !
AddressGroup         GROUP,PRE(address)                    !
CompanyName          STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
Postcode             STRING(30)                            !
TelephoneNumber      STRING(30)                            !
FaxNumber            STRING(30)                            !
EmailAddress         STRING(255)                           !
                     END                                   !
locLineCost          REAL                                  !
locWhoPrinted        STRING(60)                            !
locTotalLines        LONG                                  !
locTotalLineCount    REAL                                  !
locItemCost          REAL                                  !
Process:View         VIEW(RTNORDER)
                       PROJECT(rtn:CreditNoteRequestNumber)
                       PROJECT(rtn:Description)
                       PROJECT(rtn:InvoiceNumber)
                       PROJECT(rtn:OrderNumber)
                       PROJECT(rtn:PartNumber)
                       PROJECT(rtn:QuantityReturned)
                       PROJECT(rtn:ReturnType)
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

report               REPORT,AT(396,2729,7521,6302),PRE(rpt),PAPER(PAPER:A4),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,979,7521,1385),USE(?unnamed)
                         STRING('Printed By:'),AT(5000,583),USE(?string27),FONT(,8),TRN
                         STRING('Date Printed:'),AT(5000,792),USE(?string27:2),FONT(,8),TRN
                         STRING(@s60),AT(5885,583),USE(locWhoPrinted),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),LEFT, |
  TRN
                         STRING(@s30),AT(5885,375),USE(tra:StoresAccount),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),LEFT, |
  TRN
                         STRING(@n_8),AT(5885,156),USE(rtn:CreditNoteRequestNumber),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  LEFT,TRN
                         STRING('Credit Note No:'),AT(5000,156),USE(?string27:5),FONT(,8),TRN
                         STRING('Account No:'),AT(5000,365),USE(?string27:4),FONT(,8),TRN
                         STRING('Page Number:'),AT(5000,1000),USE(?string27:3),FONT(,8),TRN
                         STRING(@N3),AT(5875,1000),USE(ReportPageNumber),FONT(,8,,FONT:bold),TRN
                         STRING('dd/mm/yyyy'),AT(5875,792),USE(?ReportDateStamp),FONT(,8,,FONT:bold),TRN
                         STRING('tt:mm'),AT(6667,792),USE(?ReportTimeStamp),FONT(,8,,FONT:bold),TRN
                       END
detail                 DETAIL,AT(0,0,,177),USE(?detailband)
                         STRING(@n_8),AT(104,0),USE(rtn:QuantityReturned),FONT(,7),RIGHT(1),TRN
                         STRING(@s20),AT(677,0),USE(rtn:PartNumber),FONT(,7),LEFT(1),TRN
                         STRING(@s25),AT(1771,0,1615,156),USE(rtn:Description),FONT(,7),LEFT(1),TRN
                         STRING(@n_8),AT(3281,0),USE(rtn:OrderNumber),FONT(,7),RIGHT(1),TRN
                         STRING(@n_8),AT(3854,0),USE(rtn:InvoiceNumber),FONT(,7),RIGHT(1),TRN
                         STRING(@s25),AT(4531,0),USE(rtn:ReturnType),FONT(,7),LEFT(1),TRN
                         STRING(@n14.2),AT(5833,0),USE(locItemCost),FONT(,7),RIGHT(1),TRN
                         STRING(@n14.2),AT(6563,0),USE(locLineCost),FONT(,7),RIGHT(1),SUM(locTotalLineCount),TRN
                       END
detailTotal            DETAIL,AT(0,0,,333),USE(?detailTotal)
                         LINE,AT(156,52,7188,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Total No Of Lines:'),AT(156,104),USE(?String40),FONT(,8,,FONT:bold),TRN
                         STRING(@n_8),AT(1250,104),USE(locTotalLines),FONT(,8),CNT,TRN
                         STRING('Total:'),AT(5938,104),USE(?String40:2),FONT(,8,,FONT:bold),TRN
                         STRING(@n14.2),AT(6438,104),USE(locTotalLineCount),FONT(,8),RIGHT,TRN
                       END
                       FOOTER,AT(375,9073,7521,2292),USE(?unnamed:4)
                         TEXT,AT(260,52,7031,781),USE(stt:Text),FONT(,8),TRN
                         BOX,AT(156,885,7240,156),USE(?Box1),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1)
                         STRING('AUTHORISATION'),AT(3333,885),USE(?String46),FONT(,8,,FONT:bold),TRN
                         STRING('Franchisee/Delegate'),AT(833,1042),USE(?String46:2),FONT(,8,,FONT:bold),TRN
                         STRING('National Stores Representative'),AT(2917,1042),USE(?String46:3),FONT(,8,,FONT:bold), |
  TRN
                         STRING('Logistics Manager'),AT(5781,1042),USE(?String46:4),FONT(,8,,FONT:bold),TRN
                         BOX,AT(156,1042,7240,208),USE(?Box1:2),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(156,1250,7240,260),USE(?Box1:3),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Name:'),AT(208,1250),USE(?String46:5),FONT(,8,,FONT:bold),TRN
                         BOX,AT(156,1510,7240,469),USE(?Box1:4),COLOR(COLOR:Black),LINEWIDTH(1)
                         LINE,AT(5156,885,0,1302),USE(?Line2:2),COLOR(COLOR:Black)
                         STRING('Name:'),AT(5208,1250),USE(?String46:7),FONT(,8,,FONT:bold),TRN
                         STRING('Signature:'),AT(208,1771),USE(?String46:8),FONT(,8,,FONT:bold),TRN
                         BOX,AT(156,1979,7240,208),USE(?Box1:5),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Signature:'),AT(2552,1771),USE(?String46:9),FONT(,8,,FONT:bold),TRN
                         STRING('Signature:'),AT(5208,1771),USE(?String46:10),FONT(,8,,FONT:bold),TRN
                         STRING('Date:'),AT(208,1979),USE(?String46:11),FONT(,8,,FONT:bold),TRN
                         STRING('Date:'),AT(2552,1979),USE(?String46:12),FONT(,8,,FONT:bold),TRN
                         STRING('Date:'),AT(5208,1979),USE(?String46:13),FONT(,8,,FONT:bold),TRN
                         LINE,AT(2500,885,0,1302),USE(?Line2),COLOR(COLOR:Black)
                         STRING('Name:'),AT(2552,1250),USE(?String46:6),FONT(,8,,FONT:bold),TRN
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,4167,260),USE(address:CompanyName),FONT(,16,,FONT:bold),LEFT,TRN
                         STRING('CREDIT NOTE REQUEST'),AT(4583,0,2760,260),USE(?string20),FONT(,14,,FONT:bold),RIGHT, |
  TRN
                         STRING(@s30),AT(156,260,2240,156),USE(address:AddressLine1),FONT(,9),TRN
                         STRING(@s30),AT(156,417,2240,156),USE(address:AddressLine2),FONT(,9),TRN
                         STRING(@s30),AT(156,573,2240,156),USE(address:AddressLine3),FONT(,9),TRN
                         STRING(@s30),AT(156,729,1156,156),USE(address:Postcode),FONT(,9),TRN
                         STRING('Tel:'),AT(156,1042),USE(?string16),FONT(,9),TRN
                         STRING(@s30),AT(521,1042),USE(address:TelephoneNumber),FONT(,9),TRN
                         STRING('Fax: '),AT(156,1198),USE(?string19),FONT(,9),TRN
                         STRING(@s30),AT(521,1198),USE(address:FaxNumber),FONT(,9),TRN
                         STRING(@s255),AT(521,1354),USE(address:EmailAddress),FONT(,9),TRN
                         STRING('Email:'),AT(156,1354),USE(?string19:2),FONT(,9),TRN
                         STRING('Qty'),AT(375,2083),USE(?string44),FONT(,8,,FONT:bold),TRN
                         STRING('Part No'),AT(677,2083),USE(?strPartNumber),FONT(,8,,FONT:bold),TRN
                         STRING('Description'),AT(1771,2083),USE(?strDescription),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Sale No'),AT(3333,2083),USE(?string25),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Inv No'),AT(4010,2083),USE(?string25:2),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Return Type'),AT(4531,2083),USE(?string25:3),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Item Cost'),AT(6031,2083),USE(?string25:4),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Line Cost'),AT(6771,2083),USE(?string25:5),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  TRN
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR1               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
EndReport              PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.EndReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
    !Print Detail Total
        PRINT(rpt:detailTotal)  
  ReturnValue = PARENT.EndReport()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('CreditNoteRequest')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:RTNORDER.Open                                     ! File RTNORDER used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:TRADEACC.SetOpenRelated()
  Relate:TRADEACC.Open                                     ! File TRADEACC used by this procedure, so make sure it's RelationManager is open
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  locCreditNoteNumber = p_web.GSV('rtn:CreditNoteRequestNumber')
  
  SELF.Open(ProgressWindow)                                ! Open window
        Access:USERS.Clearkey(use:user_Code_Key)
        use:User_Code = p_web.GSV('BookingUserCode') ! Do not use Global Vars!! (DBH: 21/11/2014) glo:Password
        IF (Access:USERS.TryFetch(use:User_Code_Key))
        END
  
  Access:TRADEACC.Clearkey(tra:SiteLocationKey)
  tra:SiteLocation  = use:Location
  If Access:TRADEACC.Tryfetch(tra:SiteLocationKey) = Level:Benign
      !Found
      address:CompanyName     = tra:Company_Name
      address:AddressLine1    = tra:Address_Line1
      address:AddressLine2    = tra:Address_Line2
      address:AddressLine3    = tra:Address_Line3
      address:Postcode        = tra:Postcode
      address:TelephoneNumber = tra:Telephone_Number
      address:FaxNumber       = tra:Fax_Number
      address:EmailAddress    = tra:EmailAddress
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  locWhoPrinted = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
  
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'CREDIT NOTE REQUEST'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  Do DefineListboxStyle
  INIMgr.Fetch('CreditNoteRequest',ProgressWindow)         ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:RTNORDER, ?Progress:PctText, Progress:Thermometer, ProgressMgr, rtn:CreditNoteRequestNumber)
  ThisReport.AddSortOrder(rtn:CreditNoteNumberKey)
  ThisReport.AddRange(rtn:CreditNoteRequestNumber,locCreditNoteNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:RTNORDER.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RTNORDER.Close
    Relate:STANTEXT.Close
    Relate:TRADEACC.Close
  END
  IF SELF.Opened
    INIMgr.Update('CreditNoteRequest',ProgressWindow)      ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'CreditNoteRequest',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.Init(report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,report{PROPPRINT:Paper},PAPER:USER), CHOOSE(report{PROP:Thous}=True,PROP:Thous,CHOOSE(report{PROP:MM}=True,PROP:MM,CHOOSE(report{PROP:Points}=True,PROP:Points,0))), report{PROPPRINT:PaperWidth}, report{PROPPRINT:PaperHeight}, report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetTitle('Create Note Request')      !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR1.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetViewerPrefs(PDFXTR1:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  IF ReturnValue = Level:Benign
    report$?ReportPageNumber{PROP:PageNo} = True
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@d06)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'CreditNoteRequest',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR1.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  IF (rtn:ExchangeOrder)
      locItemCost = rtn:ExchangePrice
      SETTARGET(REPORT)
      ?strPartNumber{prop:Text} = 'Model Number'
      ?strDescription{prop:Text} = 'IMEI Number'
      SETTARGET()
  ELSE
      locItemCost = rtn:PurchaseCost
  END
  
  locLineCost = rtn:QuantityReturned * locItemCost
  ReturnValue = PARENT.TakeRecord()
  PRINT(rpt:detail)
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR1:rtn = PDFXTR1.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR1:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

