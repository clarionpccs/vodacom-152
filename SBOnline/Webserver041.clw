

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER041.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
AddToStockHistory    PROCEDURE  (Long f:RefNo,String f:Trans,String f:Desp,Long f:JobNo,Long f:SaleNo,Long f:Qty,Real f:Purch,Real f:Sale,Real f:Retail,String f:Notes,String f:Info,String f:UserCode,Long f:QtyStock,<Real f:AvPC>,<Real f:PC>,<Real f:SC>,<Real f:RC>) ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
    rtnValue# = 0
    do openFiles
    If Access:STOHIST.PrimeRecord() = Level:Benign
        shi:Ref_Number           = f:RefNo
        shi:User                 = f:UserCode
        shi:Transaction_Type     = f:Trans
        shi:Despatch_Note_Number = f:Desp
        shi:Job_Number           = f:JobNo
        shi:Sales_Number         = f:SaleNo
        shi:Quantity             = f:Qty
        shi:Date                 = Today()
        shi:Purchase_Cost        = f:Purch
        shi:Sale_Cost            = f:Sale
        shi:Retail_Cost          = f:Retail
        shi:Notes                = f:Notes
        shi:Information          = f:Info
        shi:StockOnHand          = f:QtyStock
        If Access:STOHIST.TryInsert() = Level:Benign
            ! Insert Successful
            rtnValue# = 1
            IF (Access:STOHISTE.PrimeRecord() = Level:Benign)
                stoe:SHIRecordNumber = shi:Record_Number
                IF (f:AvPC = 0)
                    f:AvPC = f:Purch
                END
                IF (f:PC = 0)
                    f:PC = f:Purch
                END
                IF (f:SC = 0)
                    f:SC = f:Sale
                END
                IF (f:RC = 0)
                    f:RC = f:Retail
                END
                
                stoe:PreviousAveragePurchaseCost = f:AvPC
                stoe:PurchaseCost = f:PC
                stoe:SaleCost = f:SC
                stoe:RetailCost = f:RC
                IF (Access:STOHISTE.TryInsert())
                    Access:STOHISTE.CancelAutoInc()
                END
            END
        Else ! If Access:STOHIST.TryInsert() = Level:Benign
            ! Insert Failed
            Access:STOHIST.CancelAutoInc()
        End ! If Access:STOHIST.TryInsert() = Level:Benign
    End ! If Access:STOHIST.PrimeRecord() = Level:Benign

    do closeFiles

    Return rtnValue#
!--------------------------------------
OpenFiles  ROUTINE
  Access:STOHISTE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOHISTE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STOHIST.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOHIST.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:STOHISTE.Close
     Access:STOHIST.Close
     FilesOpened = False
  END
