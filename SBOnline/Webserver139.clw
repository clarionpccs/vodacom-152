

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER139.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER034.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER106.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER141.INC'),ONCE        !Req'd for module callout resolution
                     END


ViewCosts            PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:ARCCost1         REAL                                  !
tmp:ARCCost2         REAL                                  !
tmp:ARCCost3         REAL                                  !
tmp:ARCCost4         REAL                                  !
tmp:ARCCost5         REAL                                  !
tmp:ARCCost6         REAL                                  !
tmp:ARCCost7         REAL                                  !
tmp:ARCCost8         REAL                                  !
tmp:RRCCost0         REAL                                  !
tmp:RRCCost1         REAL                                  !
tmp:RRCCost2         REAL                                  !
tmp:RRCCost3         REAL                                  !
tmp:RRCCost4         REAL                                  !
tmp:RRCCost5         REAL                                  !
tmp:RRCCost6         REAL                                  !
tmp:RRCCost7         REAL                                  !
tmp:RRCCost8         REAL                                  !
tmp:ARCIgnoreDefaultCharges BYTE                           !
tmp:RRCIgnoreDefaultCharges BYTE                           !
tmp:ARCViewCostType  STRING(30)                            !
tmp:AdjustmentCost1  REAL                                  !
tmp:AdjustmentCost2  REAL                                  !
tmp:AdjustmentCost3  REAL                                  !
tmp:AdjustmentCost4  REAL                                  !
tmp:AdjustmentCost5  REAL                                  !
tmp:AdjustmentCost6  REAL                                  !
tmp:RRCViewCostType  STRING(30)                            !
tmp:ARCIgnoreReason  STRING(255)                           !
tmp:RRCIgnoreReason  STRING(255)                           !
tmp:originalInvoice  STRING(30)                            !
tmp:ARCInvoiceNumber STRING(60)                            !
tmp:RRCInvoiceNumber STRING(60)                            !
FilesOpened     Long
JOBSE2::State  USHORT
SUBCHRGE::State  USHORT
TRACHRGE::State  USHORT
STDCHRGE::State  USHORT
USERS::State  USHORT
JOBPAYMT::State  USHORT
JOBSE::State  USHORT
INVOICE::State  USHORT
JOBSINV::State  USHORT
TRADEACC::State  USHORT
TRDPARTY::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
local       class
AddToTempQueue        Procedure(String fField,String fSaveField,String fReason,Byte fSaveCost,String fCost)
            END
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('ViewCosts')
  loc:formname = 'ViewCosts_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'ViewCosts',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('ViewCosts','')
    p_web._DivHeader('ViewCosts',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferViewCosts',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewCosts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewCosts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ViewCosts',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewCosts',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_ViewCosts',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'ViewCosts',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
DeleteSessionValues ROUTINE
    p_web.DeleteSessionValue('ViewCostsReturnURL')
addToAudit    routine
    data
locNotes        String(255)
    code
    clear(tmpaud:Record)
    tmpaud:sessionID = p_web.SessionID
    set(tmpaud:keySessionID,tmpaud:keySessionID)
    loop
        next(tempAuditQueue)
        IF (error())
            break
        END ! IF (error())
        IF (tmpaud:sessionID <> p_web.sessionID)
            break
        END ! IF (tmpaud:sessionID <> p_web.sessionID)
        locNotes = 'REASON: ' & clip(tmpaud:Reason)
        IF (tmpaud:SaveCost)
            locNotes = clip(locNotes) & '<13,10>PREVIOUS CHARGE: ' & format(tmpaud:Cost,@n14.2)
        END !IF (tmpaud:SaveCost)
!        p_web.SSV('AddToAudit:Type','JOB')
!        p_web.SSV('AddToAudit:Action',tmpaud:Field)
!        p_web.SSV('AddToAudit:Notes',locNotes)
        addToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB',tmpaud:Field,locNotes)
    END

    !Empty Audit Queue
    clear(tmpaud:Record)
    tmpaud:sessionID = p_web.SessionID
    set(tmpaud:keySessionID,tmpaud:keySessionID)
    loop
        next(tempAuditQueue)
        IF (error())
            break
        END ! IF (error())
        IF (tmpaud:sessionID <> p_web.sessionID)
            break
        END ! IF (tmpaud:sessionID <> p_web.sessionID)
        delete(tempAuditQueue)
    END



DefaultLabourCost   ROUTINE
    DefaultLabourCost(p_web)
    
    
displayARCCostFields      routine
    case (p_web.GSV('tmp:ARCViewCostType'))
    of 'Chargeable'
        p_web.SSV('Prompt:Cost1','')
        IF (p_web.GSV('LoanAttachedToJob') = 1)
            p_web.SSV('Prompt:Cost1','Lost Loan Charge')
        END
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','Paid')
        p_web.SSV('Prompt:Cost8','Outstanding')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:Courier_Cost'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('job:Labour_Cost'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('job:Parts_Cost'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('job:Sub_Total'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCCVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCCTotal'))
        p_web.SSV('tmp:ARCCost7',p_web.GSV('tmp:ARCCPaid'))
        p_web.SSV('tmp:ARCCost8',p_web.GSV('tmp:ARCOutstanding'))

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('job:Ignore_Chargeable_Charges'))


    of 'Warranty'
        p_web.SSV('Prompt:Cost1','Exchange Fee')
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:Courier_Cost_Warranty'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('job:Labour_Cost_Warranty'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('job:Parts_Cost_Warranty'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('job:Sub_Total_Warranty'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCWVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCWTotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('job:Ignore_Warranty_Charges'))

    of 'Claim'
        p_web.SSV('Prompt:Cost1','Exchange Fee')
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:Courier_Cost_Warranty'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('jobe:ClaimValue'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('jobe:ClaimPartsCost'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('tmp:ARCClaimSubTotal'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCClaimVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCClaimTotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreClaimCosts'))

    of '3rd Party'
        p_web.SSV('Prompt:Cost1','Cost')
        p_web.SSV('Prompt:Cost2','')
        p_web.SSV('Prompt:Cost3','')
        p_web.SSV('Prompt:Cost4','')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','Markup')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('jobe:ARC3rdPartyCost'))
        p_web.SSV('tmp:ARCCost2',0)
        p_web.SSV('tmp:ARCCost3',0)
        p_web.SSV('tmp:ARCCost4',0)
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARC3rdPartyVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARC3rdPartyTotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',p_web.GSV('tmp:ARC3rdPartyMarkup'))

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('jobe:Ignore3rdPartyCosts'))

    of 'Estimate'
        p_web.SSV('Prompt:Cost1','')
        IF (p_web.GSV('LoanAttachedToJob') = 1)
            p_web.SSV('Prompt:Cost1','Lost Loan Charge')
        END
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:Courier_Cost_Estimate'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('job:Labour_Cost_Estimate'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('job:Parts_Cost_Estimate'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('job:Sub_Total_Estimate'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCEVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCETotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('job:Ignore_Estimate_Charges'))

    of 'Chargeable - Invoiced'
        p_web.SSV('Prompt:Cost1','')
        IF (p_web.GSV('LoanAttachedToJob') = 1)
            p_web.SSV('Prompt:Cost1','Lost Loan Charge')
        END
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','Paid')
        p_web.SSV('Prompt:Cost8','Outstanding')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:Invoice_Courier_Cost'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('job:Invoice_Labour_Cost'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('job:Invoice_Parts_Cost'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('job:Invoice_Sub_Total'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCCIVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCCITotal'))
        p_web.SSV('tmp:ARCCost7',p_web.GSV('tmp:ARCCIPaid'))
        p_web.SSV('tmp:ARCCost8',p_web.GSV('tmp:ARCIOutstanding'))

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('job:Ignore_Chargeable_Charges'))

    of 'Warranty - Invoiced'
        p_web.SSV('Prompt:Cost1','Exchange Fee')
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:WInvoice_Courier_Cost'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('job:WInvoice_Labour_Cost'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('job:WInvoice_Parts_Cost'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('job:WInvoice_Sub_Total'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCWIVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCWITotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('job:Ignore_Warranty_Charges'))

    of 'Claim - Invoiced'
        p_web.SSV('Prompt:Cost1','Exchange Fee')
        p_web.SSV('Prompt:Cost2','Labour Cost')
        p_web.SSV('Prompt:Cost3','Parts Cost')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Cost')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('job:WInvoice_Courier_Cost'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('jobe:InvoiceClaimValue'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('jobe:InvClaimPartsCost'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('tmp:ARCClaimISubTotal'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('tmp:ARCClaimIVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('tmp:ARCClaimITotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)

        p_web.SSV('tmp:AdjustmentCost1',p_web.GSV('jobe:ExchangeAdjustment'))
        p_web.SSV('tmp:AdjustmentCost2',p_web.GSV('jobe:LabourAdjustment'))
        p_web.SSV('tmp:AdjustmentCost3',p_web.GSV('jobe:PartsAdjustment'))
        p_web.SSV('tmp:AdjustmentCost4',p_web.GSV('tmp:AdjustmentSubTotal'))
        p_web.SSV('tmp:AdjustmentCost5',p_web.GSV('tmp:AdjustmentVAT'))
        p_web.SSV('tmp:AdjustmentCost6',p_web.GSV('tmp:AdjustmentTotal'))

        p_web.SSV('tmp:ARCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreClaimCosts'))

    of 'Manufacturer Payment'
        p_web.SSV('Prompt:Cost1','Labour Paid')
        p_web.SSV('Prompt:Cost2','Parts Paid')
        p_web.SSV('Prompt:Cost3','Other Costs')
        p_web.SSV('Prompt:Cost4','Sub Total')
        p_web.SSV('Prompt:Cost5','V.A.T.')
        p_web.SSV('Prompt:Cost6','Total Paid')
        p_web.SSV('Prompt:Cost7','')
        p_web.SSV('Prompt:Cost8','')

        p_web.SSV('tmp:ARCCost1',p_web.GSV('jobe2:WLabourPaid'))
        p_web.SSV('tmp:ARCCost2',p_web.GSV('jobe2:WPartsPaid'))
        p_web.SSV('tmp:ARCCost3',p_web.GSV('jobe2:WOtherCosts'))
        p_web.SSV('tmp:ARCCost4',p_web.GSV('jobe2:WSubTotal'))
        p_web.SSV('tmp:ARCCost5',p_web.GSV('jobe2:WVAT'))
        p_web.SSV('tmp:ARCCost6',p_web.GSV('jobe2:WTotal'))
        p_web.SSV('tmp:ARCCost7',0)
        p_web.SSV('tmp:ARCCost8',0)
    END ! case (p_web.GSV('tmp:ARCViewCostType'))


displayRRCCostFields        routine
    
    p_web.SSV('class:RCost6','FormPrompt')
    case (p_web.GSV('tmp:RRCViewCostType'))
    of 'Chargeable'
        
        p_web.SSV('Prompt:RCost1','')
        IF (p_web.GSV('LoanAttachedToJob') = 1)
            p_web.SSV('Prompt:RCost1','Lost Loan Charge')
        END
        
        p_web.SSV('Prompt:RCost2','Labour Cost')
        p_web.SSV('Prompt:RCost3','Parts Cost')
        p_web.SSV('Prompt:RCost4','Sub Total')
        p_web.SSV('Prompt:RCost5','V.A.T.')
        p_web.SSV('Prompt:RCost6','Total Cost')
        p_web.SSV('Prompt:RCost7','Paid')
        p_web.SSV('Prompt:RCost8','Outstanding')

        p_web.SSV('tmp:RRCCost0',p_web.GSV('jobe2:JobDiscountAmnt'))
        p_web.SSV('tmp:RRCCost1',p_web.GSV('job:Courier_Cost'))
        p_web.SSV('tmp:RRCCost2',p_web.GSV('jobe:RRCCLabourCost'))
        p_web.SSV('tmp:RRCCost3',p_web.GSV('jobe:RRCCPartsCost'))
        p_web.SSV('tmp:RRCCost4',p_web.GSV('jobe:RRCCSubTotal'))
        p_web.SSV('tmp:RRCCost5',p_web.GSV('tmp:RRCCVAT'))
        p_web.SSV('tmp:RRCCost6',p_web.GSV('tmp:RRCCTotal'))
        p_web.SSV('tmp:RRCCost7',p_web.GSV('tmp:RRCCPaid'))
        p_web.SSV('tmp:RRCCost8',p_web.GSV('tmp:RRCOutstanding'))

        p_web.SSV('tmp:RRCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreRRCChaCosts'))
        
        IF (p_web.GSV('jobe2:JobDiscountAmnt') > 0 or p_web.GSV('tmp:RRCIgnoreDefaultCharges') <> 0)
            p_web.SSV('Prompt:RCost0','Discount')
        ELSE
            p_web.SSV('Prompt:RCost0','')
            p_web.SSV('Comment:RCost0','')
        END
        
        IF (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
            p_web.SSV('ReadOnly:RRCCost2',0)
        ELSE
            p_web.SSV('ReadOnly:RRCCost2',1)
        END

    of 'Warranty'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('Prompt:RCost1','')
        p_web.SSV('Prompt:RCost2','Labour Cost')
        p_web.SSV('Prompt:RCost3','Parts Cost')
        p_web.SSV('Prompt:RCost4','Sub Total')
        p_web.SSV('Prompt:RCost5','V.A.T.')
        p_web.SSV('Prompt:RCost6','Total Cost')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','')

        p_web.SSV('tmp:RRCCost1',p_web.GSV('job:Courier_Cost_Warranty'))
        p_web.SSV('tmp:RRCCost2',p_web.GSV('jobe:RRCWLabourCost'))
        p_web.SSV('tmp:RRCCost3',p_web.GSV('jobe:RRCWPartsCost'))
        p_web.SSV('tmp:RRCCost4',p_web.GSV('jobe:RRCWSubTotal'))
        p_web.SSV('tmp:RRCCost5',p_web.GSV('tmp:RRCWVAT'))
        p_web.SSV('tmp:RRCCost6',p_web.GSV('tmp:RRCWTotal'))
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',0)

        p_web.SSV('tmp:RRCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreRRCWarCosts'))
        IF (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
            p_web.SSV('ReadOnly:RRCCost2',0)
        ELSE
            p_web.SSV('ReadOnly:RRCCost2',1)
        END

    of 'Handling'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('Prompt:RCost2','Handling Fee')
        p_web.SSV('Prompt:RCost1','')
        p_web.SSV('Prompt:RCost3','')
        p_web.SSV('Prompt:RCost4','')
        p_web.SSV('Prompt:RCost5','')
        p_web.SSV('Prompt:RCost6','')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','')

        p_web.SSV('tmp:RRCCost2',p_web.GSV('tmp:HandlingFee'))
        p_web.SSV('tmp:RRCCost1',0)
        p_web.SSV('tmp:RRCCost3',0)
        p_web.SSV('tmp:RRCCost4',0)
        p_web.SSV('tmp:RRCCost5',0)
        p_web.SSV('tmp:RRCCost6',0)
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',0)

        

    of 'Exchange'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('Prompt:RCost2','Exchange Fee')
        p_web.SSV('Prompt:RCost1','')
        p_web.SSV('Prompt:RCost3','')
        p_web.SSV('Prompt:RCost4','')
        p_web.SSV('Prompt:RCost5','')
        p_web.SSV('Prompt:RCost6','')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','')

        p_web.SSV('tmp:RRCCost2',p_web.GSV('tmp:ExchangeFee'))
        p_web.SSV('tmp:RRCCost1',0)
        p_web.SSV('tmp:RRCCost3',0)
        p_web.SSV('tmp:RRCCost4',0)
        p_web.SSV('tmp:RRCCost5',0)
        p_web.SSV('tmp:RRCCost6',0)
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',0)

    of 'Estimate'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('Prompt:RCost1','Lost Loan Charge')
        p_web.SSV('Prompt:RCost2','Labour Cost')
        p_web.SSV('Prompt:RCost3','Parts Cost')
        p_web.SSV('Prompt:RCost4','Sub Total')
        p_web.SSV('Prompt:RCost5','V.A.T.')
        p_web.SSV('Prompt:RCost6','Total Cost')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','')

        p_web.SSV('tmp:RRCCost1',p_web.GSV('job:Courier_Cost_Estimate'))
        p_web.SSV('tmp:RRCCost2',p_web.GSV('jobe:RRCELabourCost'))
        p_web.SSV('tmp:RRCCost3',p_web.GSV('jobe:RRCEPartsCost'))
        p_web.SSV('tmp:RRCCost4',p_web.GSV('jobe:RRCESubTotal'))
        p_web.SSV('tmp:RRCCost5',p_web.GSV('tmp:RRCEVAT'))
        p_web.SSV('tmp:RRCCost6',p_web.GSV('tmp:RRCETotal'))
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',0)

        p_web.SSV('tmp:RRCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreRRCEstCosts'))
        IF (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
            p_web.SSV('ReadOnly:RRCCost2',0)
        ELSE
            p_web.SSV('ReadOnly:RRCCost2',1)
        END        

    of 'Chargeable - Invoiced'
        
        IF (p_web.GSV('jobe2:InvDiscountAmnt') > 0  or p_web.GSV('tmp:RRCIgnoreDefaultCharges') <> 0)
            p_web.SSV('Prompt:RCost0','Discount')
        ELSE
            p_web.SSV('Prompt:RCost0','')
            p_web.SSV('Comment:RCost0','')
        END

        p_web.SSV('Prompt:RCost1','')
        IF (p_web.GSV('LoanAttachedToJob') = 1)
            p_web.SSV('Prompt:RCost1','Lost Loan Charge')
        END
        p_web.SSV('Prompt:RCost2','Labour Cost')
        p_web.SSV('Prompt:RCost3','Parts Cost')
        p_web.SSV('Prompt:RCost4','Sub Total')
        p_web.SSV('Prompt:RCost5','V.A.T.')
        p_web.SSV('Prompt:RCost6','Total Cost')
        ! Credit Exists?
        IF (p_web.GSV('Hide:Credit') = 0)
            p_web.SSV('Prompt:RCost6','Total Cost (CREDIT EXISTS)')  ! #12501 Show IF a credit exists (DBH: 20/06/2012)
            p_web.SSV('class:RCost6','FormPrompt RedBold')
        END
        p_web.SSV('Prompt:RCost7','Paid')
        p_web.SSV('Prompt:RCost8','Outstanding')

        p_web.SSV('tmp:RRCCOst0',p_web.GSV('jobe2:InvDiscountAmnt'))
        p_web.SSV('tmp:RRCCost1',p_web.GSV('job:Invoice_Courier_Cost'))
        p_web.SSV('tmp:RRCCost2',p_web.GSV('jobe:InvRRCCLabourCost'))
        p_web.SSV('tmp:RRCCost3',p_web.GSV('jobe:InvRRCCPartsCost'))
        p_web.SSV('tmp:RRCCost4',p_web.GSV('jobe:InvRRCCSubTotal'))
        p_web.SSV('tmp:RRCCost5',p_web.GSV('tmp:RRCCIVAT'))
        p_web.SSV('tmp:RRCCost6',p_web.GSV('tmp:RRCCITotal'))
        p_web.SSV('tmp:RRCCost7',p_web.GSV('tmp:RRCCIPaid'))
        p_web.SSV('tmp:RRCCost8',p_web.GSV('tmp:RRCIOutstanding'))

        p_web.SSV('tmp:RRCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreRRCChaCosts'))
    

    of 'Warranty - Invoiced'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('Prompt:RCost1','Lost Loan Charge')
        p_web.SSV('Prompt:RCost2','Labour Cost')
        p_web.SSV('Prompt:RCost3','Parts Cost')
        p_web.SSV('Prompt:RCost4','Sub Total')
        p_web.SSV('Prompt:RCost5','V.A.T.')
        p_web.SSV('Prompt:RCost6','Total Cost')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','')

        p_web.SSV('tmp:RRCCost1',p_web.GSV('job:WInvoice_Courier_Cost'))
        p_web.SSV('tmp:RRCCost2',p_web.GSV('jobe:InvRRCWLabourCost'))
        p_web.SSV('tmp:RRCCost3',p_web.GSV('jobe:InvRRCWPartsCost'))
        p_web.SSV('tmp:RRCCost4',p_web.GSV('jobe:InvRRCWSubTotal'))
        p_web.SSV('tmp:RRCCost5',p_web.GSV('tmp:RRCWIVAT'))
        p_web.SSV('tmp:RRCCost6',p_web.GSV('tmp:RRCWITotal'))
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',0)

        p_web.SSV('tmp:RRCIgnoreDefaultCharges',p_web.GSV('jobe:IgnoreRRCWarCosts'))

    of 'Credits'
        p_web.SSV('Prompt:RCost0','')
        p_web.SSV('Comment:RCost0','')
        p_web.SSV('tmp:CreditTotal',p_web.GSV('tmp:RRCCITotal'))
        Access:JOBSINV.Clearkey(jov:typeRecordKey)
        jov:refNumber    = p_web.GSV('job:Ref_Number')
        jov:type    = 'C'
        set(jov:typeRecordKey,jov:typeRecordKey)
        loop
            IF (Access:JOBSINV.Next())
                Break
            END ! IF (Access:JOBSINV.Next())
            IF (jov:refNumber    <> p_web.GSV('job:Ref_Number'))
                Break
            END ! IF (jov:refNumber    <> p_web.GSV('job:Ref_Number'))
            IF (jov:type    <> 'C')
                Break
            END ! IF (jov:type    <> 'C')
            p_web.SSV('tmp:RRCCITotal',p_web.GSV('tmp:RRCCITotal') - jov:creditAmount)
            p_web.SSV('tmp:RRCIOutstanding',p_web.GSV('tmp:RRCIOutstanding') - jov:creditAmount)
        END ! loop

        access:INVOICE.clearKey(inv:invoice_Number_Key)
        inv:invoice_Number = p_web.GSV('job:invoice_number')
        IF (access:INVOICE.tryfetch(inv:invoice_Number_Key) = level:Benign)
        END !

        access:TRADEACC.clearKey(tra:account_Number_Key)
        tra:account_Number = p_web.GSV('wob:HeadAccountNumber')
        IF (access:TRADEACC.tryfetch(tra:account_Number_Key) = level:Benign)
        END

        p_web.SSV('tmp:OriginalInvoice',clip(inv:invoice_number) & '-' & clip(tra:BranchIdentIFication))


        p_web.SSV('Prompt:RCost1','Original Total')
        p_web.SSV('Prompt:RCost2','')
        p_web.SSV('Prompt:RCost3','')
        p_web.SSV('Prompt:RCost4','')
        p_web.SSV('Prompt:RCost5','')
        p_web.SSV('Prompt:RCost6','')
        p_web.SSV('Prompt:RCost7','')
        p_web.SSV('Prompt:RCost8','Current Total')

        p_web.SSV('tmp:RRCCost1',p_web.GSV('tmp:CreditTotal'))
        p_web.SSV('tmp:RRCCost2',0)
        p_web.SSV('tmp:RRCCost3',0)
        p_web.SSV('tmp:RRCCost4',0)
        p_web.SSV('tmp:RRCCost5',0)
        p_web.SSV('tmp:RRCCost6',0)
        p_web.SSV('tmp:RRCCost7',0)
        p_web.SSV('tmp:RRCCost8',p_web.GSV('tmp:RRCCITotal'))

    END !case (p_web.GSV('tmp:RRCViewCostType'))
    

displayOtherFields      routine
    p_web.SSV('Hide:ExchangeReplacement',1)

    p_web.SSV('Prompt:RCost1','')
    p_web.SSV('Prompt:ACost1','')


    IF (p_web.GSV('tmp:ARCViewCostType') = 'Warranty')
        IF (p_web.GSV('job:exchange_unit_number') > 0 and |
            (p_web.GSV('jobe:exchangedAtRRC') = 0 or |
                p_web.GSV('jobe:exchangedAtRRC') = 1 and p_web.GSV('job:repair_Type_Warranty') = 'R.T.M.'))
            p_web.SSV('Prompt:RCost1','Lost Loan Charge')
        ELSE !job:Courier_Cost
            p_web.SSV('Prompt:RCost1','')
        END ! job:Courier_Cost
    END ! IF (p_web.GSV('tmp:ARCViewCostType') = 'Warranty')

    LoanAttachedToJob(p_web)
    IF (p_web.GSV('LoanAttachedToJob') = 1)
        IF (p_web.GSV('jobe:HubRepair') = 0)
            IF (p_web.GSV('tmp:RRCViewCostType') = 'Chargeable' Or |
                p_web.GSV('tmp:RRCViewCostType') = 'Chargeable - Invoiced' Or |
                p_web.GSV('tmp:RRCViewCostType') = 'Estimate')

                p_web.SSV('Prompt:RCost1','Lost Loan Charge')
            END
        ELSE ! IF (p_web.GSV('jobe:HubRepair') = 0)
            IF (p_web.GSV('tmp:ARCViewCostType') = 'Chargeable' Or |
                p_web.GSV('tmp:ARCViewCostType') = 'Chargeable - Invoiced' Or |
                p_web.GSV('tmp:ARCViewCostType') = 'Estimate')

                p_web.SSV('Prompt:ACost1','Lost Loan Charge')
            END

        END ! IF (p_web.GSV('jobe:HubRepair') = 0)
    ELSE ! IF (p_web.GSV('LoanAttachedToJob') = 1)
        p_web.SSV('Prompt:RCost1','')
        IF (p_web.GSV('jobe:Engineer48HourOption') = 1 and p_web.GSV('job:chargeable_job') = 'YES')
            IF (p_web.GSV('jobe:WebJob') = 1)
                p_web.SSV('Hide:ExchangeReplacement',0)

                IF (securityCheckFailed(p_web.GSV('BookingUserPassword'),'EXCHANGE REPLACEMENT CHARGE'))
                ELSE ! IF (securityCheckFailed(p_web.GSV('BookingUserPassword','EXCHANGE REPLACEMENT CHARGE')
                END ! IF (securityCheckFailed(p_web.GSV('BookingUserPassword','EXCHANGE REPLACEMENT CHARGE'))

                IF (p_web.GSV('jobe:ExcReplcamentCharge') = 1)
                    p_web.SSV('Prompt:RCost1','Exchange Replacement')

                    p_web.SSV('Prompt:ACost1','Exchange Replacement')

                END ! IF (p_web.GSV('jobe:ExcReplcamentCharge') = 1)
            END ! IF (p_web.GSV('jobe:WebJob') = 1)
        END !IF (p_web.GSV('jobe:Engineer48HourOption') = 1 and p_web.GSV('job:chargeable_job') = 'YES')
    END ! IF (p_web.GSV('LoanAttachedToJob') = 1)
pricingRoutine      Routine
data
locARCPaid    Real()
locRRCPaid    Real()
locTotalPaid    Real()
code
    jobPricingRoutine(p_web)
    IF (p_web.GSV('job:warranty_job') = 'YES')
        IF (p_web.GSV('job:invoice_number_warranty') > 0)
            p_web.SSV('tmp:HandlingFee',p_web.GSV('jobe:invoiceHandlingFee'))
            p_web.SSV('tmp:ExchangeFee',p_web.GSV('jobe:invoiceExchangeRate'))

            Access:INVOICE.Clearkey(inv:invoice_number_Key)
            inv:invoice_number    = p_web.GSV('job:invoice_Number_warranty')
            IF (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
                ! Found
                p_web.SSV('tmp:ARCInvoiceNumber',inv:invoice_number)

                p_web.SSV('tmp:ARCWIVat',p_web.GSV('job:WInvoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
                                p_web.GSV('job:WInvoice_Parts_Cost') * (inv:Vat_Rate_Parts/100) + |
                                p_web.GSV('job:WInvoice_Labour_Cost') * (inv:Vat_Rate_Labour/100))
                p_web.SSV('tmp:ARCWITotal',p_web.GSV('tmp:ARCWIVat') + p_web.GSV('job:WInvoice_Sub_Total'))


                p_web.SSV('tmp:ARCClaimIVAT',p_web.GSV('job:WInvoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
                                p_web.GSV('jobe:InvClaimPartsCost') * (inv:Vat_Rate_Parts/100) + |
                                p_web.GSV('jobe:InvoiceClaimValue') * (inv:Vat_Rate_Labour/100))
                p_web.SSV('tmp:ARCClaimISubTotal',p_web.GSV('job:WInvoice_Courier_Cost') + p_web.GSV('jobe:InvClaimPartsCost') + |
                                    p_web.GSV('jobe:InvoiceClaimValue'))
                p_web.SSV('tmp:ARCClaimITotal',p_web.GSV('tmp:ARCClaimIVat') + p_web.GSV('tmp:ARCClaimISubTotal'))

                p_web.SSV('tmp:AdjustmentSubTotal',p_web.GSV('jobe:LabourAdjustment') + p_web.GSV('jobe:ExchangeAdjustment') + |
                                        p_web.GSV('jobe:PartsAdjustment'))
                p_web.SSV('tmp:AdjustmentVAT',p_web.GSV('jobe:PartsAdjustment') * (inv:Vat_Rate_Parts/100) + |
                                    p_web.GSV('jobe:LabourAdjustment') * (inv:Vat_Rate_Labour/100) +|
                                    p_web.GSV('jobe:ExchangeAdjustment') * (inv:Vat_Rate_Labour/100))
                p_web.SSV('tmp:AdjustmentTotal',p_web.GSV('tmp:AdjustmentVAT') + p_web.GSV('tmp:AdjustmentSubTotal'))

            ELSE ! IF (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
                ! Error
            END ! IF (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
        ELSE ! IF (p_web.GSV('job:invoice_number_warranty') > 0)
            p_web.SSV('tmp:HandlingFee',p_web.GSV('jobe:HandlingFee'))
            p_web.SSV('tmp:ExchangeFee',p_web.GSV('jobe:ExchangeRate'))
        END ! IF (p_web.GSV('job:invoice_number_warranty') > 0)
    END ! IF (p_web.GSV('job:warranty_job') = 'YES')

    IF (p_web.GSV('job:Chargeable_Job') = 'YES')
        IF (p_web.GSV('job:Invoice_Number') > 0)
            p_web.SSV('tmp:HandlingFee',p_web.GSV('jobe:InvoiceHandlingFee'))
            p_web.SSV('tmp:ExchangeFee',p_web.GSV('jobe:InvoiceExchangeRate'))

            Access:INVOICE.Clearkey(inv:invoice_number_Key)
            inv:invoice_number    = p_web.GSV('job:invoice_number')
            IF (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
                ! Found

                Access:TRADEACC.Clearkey(tra:account_number_key)
                tra:account_number    = p_web.GSV('ARC:AccountNumber')
                IF (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)
                    ! Found
                    p_web.SSV('tmp:ARCInvoiceNumber',Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentIFication) & ' (' &CLIP(Format(inv:Date_Created,@d6))& ')')
                ELSE ! IF (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)
                    ! Error
                END ! IF (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)

                IF (inv:exportedRRCOracle)
                    p_web.SSV('jobe:InvRRCCSubTotal',p_web.GSV('job:Invoice_Courier_Cost') + |
                                            p_web.GSV('jobe:InvRRCCLabourCost') + |
                                            p_web.GSV('jobe:InvRRCCPartsCost'))

                    p_web.SSV('tmp:RRCCIVat',p_web.GSV('job:Invoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
                                    p_web.GSV('jobe:InvRRCCLabourCost') * (inv:Vat_Rate_Labour/100) + |
                                    p_web.GSV('jobe:InvRRCCPartsCost') * (inv:Vat_Rate_Parts/100))
                    p_web.SSV('tmp:RRCCITotal',p_web.GSV('tmp:RRCCIVat') + p_web.GSV('jobe:InvRRCCSubTotal'))

                    Access:TRADEACC.Clearkey(tra:account_number_key)
                    tra:account_number    = p_web.GSV('wob:HeadAccountNumber')
                    IF (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)
                        ! Found
                        p_web.SSV('tmp:RRCInvoiceNumber',Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentIFication) & ' (' &CLIP(Format(inv:RRCInvoiceDate,@d6))& ')')
                        IF (p_web.GSV('job:exchange_unit_Number') > 0)
                            p_web.SSV('tmp:RRCInvoiceNumber',p_web.GSV('tmp:RRCInvoiceNumber') & ' Exchange')
                        END ! IF (p_web.GSV('job:exchange_unit_Number') > 0)
                    ELSE ! IF (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)
                        ! Error
                    END ! IF (Access:TRADEACC.TryFetch(tra:account_number_key) = Level:Benign)
                END ! IF (inv:exportedRRCOracle)

                p_web.SSV('tmp:ARCCIVat',p_web.GSV('job:Invoice_Courier_Cost') * (inv:Vat_Rate_Labour/100) + |
                                p_web.GSV('job:Invoice_Parts_Cost') * (inv:Vat_Rate_Parts/100) + |
                                p_web.GSV('job:Invoice_Labour_Cost') * (inv:Vat_Rate_Labour/100))
                p_web.SSV('job:Invoice_Sub_Total',p_web.GSV('job:Invoice_Courier_Cost') + |
                                        p_web.GSV('job:Invoice_Labour_Cost') + |
                                        p_web.GSV('job:Invoice_Parts_Cost'))
                p_web.SSV('tmp:ARCCITotal',p_web.GSV('tmp:ARCCIVat') + p_web.GSV('job:invoice_Sub_Total'))

            ELSE ! IF (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
                ! Error
            END ! IF (Access:INVOICE.TryFetch(inv:invoice_number_Key) = Level:Benign)
        ELSE ! IF (p_web.GSV('job:Invoice_Number') > 0)
            p_web.SSV('tmp:HandlingFee',p_web.GSV('jobe:HandlingFee'))
            p_web.SSV('tmp:ExchangeFee',p_web.GSV('jobe:ExchangeRate'))
        END ! IF (p_web.GSV('job:Invoice_Number') > 0)
    END ! IF (p_web.GSV('job:Chargeable_Job') = 'YES')


    ! Paid ?

    Access:JOBPAYMT.Clearkey(jpt:all_Date_Key)
    jpt:ref_Number    = p_web.GSV('job:Ref_Number')
    set(jpt:all_Date_Key,jpt:all_Date_Key)
    loop
        IF (Access:JOBPAYMT.Next())
            Break
        END ! IF (Access:JOBPAYMT.Next())
        IF (jpt:ref_Number    <> p_web.GSV('job:Ref_Number'))
            Break
        END ! IF (jpt:ref_Number    <> p_web.GSV('job:Ref_Number'))

        Access:USERS.Clearkey(use:user_code_Key)
        use:user_code    = jpt:user_code
        IF (Access:USERS.TryFetch(use:user_code_Key) = Level:Benign)
            ! Found
            IF (use:location = p_web.GSV('ARC:SiteLocation'))
                locARCPaid += jpt:Amount
            ELSE
                locRRCPaid += jpt:Amount
            END ! IF (use:location = p_web.GSV('ARC:SiteLocation'))
        ELSE ! IF (Access:USERS.TryFetch(use:user_code_Key) = Level:Benign)
            ! Error
        END ! IF (Access:USERS.TryFetch(use:user_code_Key) = Level:Benign)
        locTotalPaid += jpt:amount
    END ! loop

    p_web.SSV('tmp:RRCCPaid',locRRCPaid)
    p_web.SSV('tmp:RRCCIPaid',locRRCPaid)
    p_web.SSV('tmp:ARCCPaid',locARCPaid)
    p_web.SSV('tmp:ARCCIPaid',locARCPaid)


    Access:TRDPARTY.Clearkey(trd:company_Name_Key)
    trd:company_Name    = p_web.GSV('job:Third_Party_Site')
    IF (Access:TRDPARTY.TryFetch(trd:company_Name_Key) = Level:Benign)
        ! Found
        p_web.SSV('tmp:ARC3rdPartyVAT',(p_web.GSV('jobe:ARC3rdPartyCost') * trd:VatRate/100))
        p_web.SSV('tmp:ARC3rdPartyTotal',(p_web.GSV('jobe:ARC3rdPartyCost') * trd:VatRate/100))
        p_web.SSV('tmp:ARC3rdPartyMarkup',p_web.GSV('jobe:ARC3rdPartyCost') + p_web.GSV('tmp:ARC3rdPartyTotal'))

    ELSE ! IF (Access:TRDPARTY.TryFetch(trd:company_Name_Ke) = Level:Benign)
        ! Error
    END ! IF (Access:TRDPARTY.TryFetch(trd:company_Name_Ke) = Level:Benign)


    p_web.SSV('tmp:ARCCVat',(p_web.GSV('job:Courier_Cost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)) + |
                               (p_web.GSV('job:Parts_Cost') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                                (p_web.GSV('job:Labour_Cost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)))

    p_web.SSV('tmp:ARCCTotal',p_web.GSV('tmp:ARCCVat') + p_web.GSV('job:sub_total'))

    p_web.SSV('tmp:RRCCVat',(p_web.GSV('job:Courier_Cost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)) + |
                           (p_web.GSV('jobe:RRCCPartsCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                            (p_web.GSV('jobe:RRCCLabourCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)))

    p_web.SSV('tmp:RRCCTotal',p_web.GSV('tmp:RRCCVat') + p_web.GSV('jobe:RRCCSubTotal'))

    p_web.SSV('tmp:ARCWVAT',(p_web.GSV('job:Parts_Cost_Warranty') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                    (p_web.GSV('job:Labour_Cost_Warranty') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)) + |
                    (p_web.GSV('job:Courier_Cost_Warranty') * (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)))
    p_web.SSV('tmp:ARCWTotal',p_web.GSV('tmp:ARCWVat') + p_web.GSV('job:sub_total_warranty'))

    p_web.SSV('tmp:RRCWVAT',(p_web.GSV('jobe:RRCWPartsCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                    (p_web.GSV('jobe:RRCWLabourCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)))
    p_web.SSV('tmp:RRCWTotal',p_web.GSV('tmp:RRCWVAT') + p_web.GSV('jobe:RRCWSubTotal'))

    p_web.SSV('tmp:ARCEVAT',(p_web.GSV('job:Courier_Cost_Estimate') * (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)) + |
                           (p_web.GSV('job:Parts_Cost_Estimate') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                            (p_web.GSV('job:Labour_Cost_Estimate') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)))

    p_web.SSV('tmp:ARCETotal',p_web.GSV('tmp:ARCEVat') + p_web.GSV('job:Sub_Total_Estimate'))

    p_web.SSV('tmp:RRCEVAT',(p_web.GSV('job:Courier_Cost_Estimate') * (GetVATRate(p_web.GSV('job:Account_Number'),'L') /100)) + |
                           (p_web.GSV('jobe:RRCEPartsCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'P') /100)) +  |
                            (p_web.GSV('jobe:RRCELabourCost') * (GetVATRate(p_web.GSV('job:Account_Number'),'L')/100)))
    p_web.SSV('tmp:RRCETotal',p_web.GSV('tmp:RRCEVAT') + p_web.GSV('jobe:RRCESubTotal'))

    p_web.SSV('jobe:InvRRCWSubTotal',p_web.GSV('jobe:InvRRCWPartsCost') + p_web.GSV('jobe:InvRRCWLabourCost'))

    p_web.SSV('tmp:RRCWIVAT',p_web.GSV('jobe:InvRRCWPartsCost') * (inv:Vat_Rate_Parts/100) + |
                    p_web.GSV('jobe:InvRRCWLabourCost') * (inv:Vat_Rate_Labour/100))
    p_web.SSV('tmp:RRCWITotal',p_web.GSV('tmp:RRCWIVAT') + p_web.GSV('jobe:InvRRCWSubTotal'))


    p_web.SSV('tmp:RRCOutstanding',p_web.GSV('tmp:RRCCTotal') - p_web.GSV('tmp:RRCCPaid'))
    p_web.SSV('tmp:ARCOutstanding',p_web.GSV('tmp:ARCCTotal') - p_web.GSV('tmp:ARCCPaid'))
    p_web.SSV('tmp:RRCIOutstanding',p_web.GSV('tmp:RRCCITotal') - p_web.GSV('tmp:RRCCIPaid'))
    p_web.SSV('tmp:ARCIOutstanding',p_web.GSV('tmp:ARCCITotal') - p_web.GSV('tmp:ARCCIPaid'))
    
    !TB13308 - J - 24/06/14
    !there seem to be some display problems with the tmp:HandlingFee and tmp:ExchangeFee IF they are blank rather than zero so ...
    IF p_web.gsv('tmp:HandlingFee') = '' then p_web.ssv('tmp:HandlingFee','0.00').
    IF p_web.gsv('tmp:ExchangeFee') = '' then p_web.ssv('tmp:ExchangeFee','0.00').
    !TB13308 END
restoreFields    routine
    p_web.SSV('job:Courier_Cost',p_web.GSV('save:Courier_Cost'))
    p_web.SSV('job:Labour_Cost',p_web.GSV('save:Labour_Cost'))
    p_web.SSV('job:Parts_Cot',p_web.GSV('save:Parts_Cost'))
    p_web.SSV('job:Sub_Total',p_web.GSV('save:Sub_Total'))
    p_web.SSV('job:Courier_Cost_Warranty',p_web.GSV('save:Courier_Cost_Warranty'))
    p_web.SSV('job:Labour_Cost_Warranty',p_web.GSV('save:Labour_Cost_Warranty'))
    p_web.SSV('job:Parts_Cost_Warranty',p_web.GSV('save:Parts_Cost_Warranty'))
    p_web.SSV('job:Sub_Total_Warranty',p_web.GSV('save:Sub_Total_Warranty'))
    p_web.SSV('jobe:ClaimValue',p_web.GSV('save:ClaimValue'))
    p_web.SSV('jobe:ClaimPartsCost',p_web.GSV('save:ClaimPartsCost'))
    p_web.SSV('jobe:ARC3rdPartyCost',p_web.GSV('save:ARC3rdPartyCost'))
    p_web.SSV('job:Courier_Cost_Estimate',p_web.GSV('save:Courier_Cost_Estimate'))
    p_web.SSV('job:Labour_Cost_Estimate',p_web.GSV('save:Labour_Cost_Estimate'))
    p_web.SSV('job:Parts_Cost_Estimate',p_web.GSV('save:Parts_Cost_Estimate'))
    p_web.SSV('job:Sub_Total_Estimate',p_web.GSV('save:Sub_Total_Estimate'))
    p_web.SSV('job:Invoice_Courier_Cost',p_web.GSV('save:Invoice_Courier_Cost'))
    p_web.SSV('job:Invoice_Labour_Cost',p_web.GSV('save:Invoice_Labour_Cost'))
    p_web.SSV('job:Invoice_Parts_Cost',p_web.GSV('save:Invoice_Parts_Cost'))
    p_web.SSV('job:Invoice_Sub_Total',p_web.GSV('save:Invoice_Sub_Total'))
    p_web.SSV('job:WInvoice_Courier_Cost',p_web.GSV('save:WInvoice_Courier_Cost'))
    p_web.SSV('job:WInvoice_Labour_Cost',p_web.GSV('save:WInvoice_Labour_Cost'))
    p_web.SSV('job:WInvoice_Parts_Cost',p_web.GSV('save:WInvoice_Parts_Cost'))
    p_web.SSV('job:WInvoice_Sub_Total',p_web.GSV('save:WInvoice_Sub_Total'))
    p_web.SSV('jobe:ExchangAdjustment',p_web.GSV('save:ExchangeAdjustment'))
    p_web.SSV('jobe:LabourAdjustment',p_web.GSV('save:LabourAdjustment'))
    p_web.SSV('jobe:PartsAdjustment',p_web.GSV('save:PartsAdjustment'))
    p_web.SSV('jobe2:WLabourPaid',p_web.GSV('save:WLabourPaid'))
    p_web.SSV('jobe2:WPartsPaid',p_web.GSV('save:WPartsPaid'))
    p_web.SSV('jobe2:WOtherCosts',p_web.GSV('save:WOtherCosts'))
    p_web.SSV('jobe2:WVat',p_web.GSV('save:WVat'))
    p_web.SSV('jobe2:WTotal',p_web.GSV('save:WTotal'))
    p_web.SSV('jobe:IgnoreClaimCosts',p_web.GSV('save:IgnoreClaimCosts'))
    p_web.SSV('job:Ignore_Warranty_Charges',p_web.GSV('save:Ignore_Warranty_Charges'))
    p_web.SSV('job:Ignore_Chargeable_Charges',p_web.GSV('save:Ignore_Chargeable_Charges'))
    p_web.SSV('job:Ignore_Estimate_Charges',p_web.GSV('save:Ignore_Estimate_Charges'))
    p_web.SSV('jobe:RRCClabourCost',p_web.GSV('save:RRCCLabourCost'))
    p_web.SSV('jobe:RRCCPartsCost',p_web.GSV('save:RRCCPartsCost'))
    p_web.SSV('jobe:RRCCSubTotal',p_web.GSV('save:RRCCSubTotal'))
    p_web.SSV('jobe:RRCWLabourCost',p_web.GSV('save:RRCWLabourCost'))
    p_web.SSV('jobe:RRCWPartsCost',p_web.GSV('save:RRCWPartsCost'))
    p_web.SSV('jobe:RRCWSubTotal',p_web.GSV('save:RRCWSubTotal'))
    p_web.SSV('jobe:IgnoreRRCChaCosts',p_web.GSV('save:IgnoreRRCChaCosts'))
    p_web.SSV('jobe:IgnoreRRCWarCosts',p_web.GSV('save:IgnoreRRCWarCosts'))
    p_web.SSV('jobe:RRCELabourCost',p_web.GSV('save:RRCELabourCost'))
    p_web.SSV('jobe:RRCEPartsCost',p_web.GSV('save:RRCEPartsCost'))
    p_web.SSV('jobe:RRCESubTotal',p_web.GSV('save:RRCESubTotal'))
    p_web.SSV('jobe:InvRRCCLabourCost',p_web.GSV('save:InvRRCCLabourCost'))
    p_web.SSV('jobe:InvRRCCPartsCost',p_web.GSV('save:InvRRCCPartsCost'))
    p_web.SSV('jobe:InvRRCCSubTotal',p_web.GSV('save:InvRRCCSubTotal'))
    p_web.SSV('jobe:IgnoreRRCEstCosts',p_web.GSV('save:IgnoreRRCEstCosts'))
    p_web.SSV('jobe:InvRRCWLabourCost',p_web.GSV('save:InvRRCWLabourCost'))
    p_web.SSV('jobe:InvRRCWPartsCost',p_web.GSV('save:InvRRCWPartsCost'))
    p_web.SSV('jobe:InvRRCWSubTotal',p_web.GSV('save:InvRRCWSubTotal'))
    p_web.SSV('jobe2:JobDiscountAmnt',p_web.GSV('save:JobDiscountAmnt'))
    p_web.SSV('jobe2:InvDiscountAmnt',p_web.GSV('save:InvDiscountAmnt'))
saveFields    routine
    p_web.SSV('save:Courier_Cost',p_web.GSV('job:Courier_Cost'))
    p_web.SSV('save:Labour_Cost',p_web.GSV('job:Labour_Cost'))
    p_web.SSV('save:Parts_Cot',p_web.GSV('job:Parts_Cost'))
    p_web.SSV('save:Sub_Total',p_web.GSV('job:Sub_Total'))
    p_web.SSV('save:Courier_Cost_Warranty',p_web.GSV('job:Courier_Cost_Warranty'))
    p_web.SSV('save:Labour_Cost_Warranty',p_web.GSV('job:Labour_Cost_Warranty'))
    p_web.SSV('save:Parts_Cost_Warranty',p_web.GSV('job:Parts_Cost_Warranty'))
    p_web.SSV('save:Sub_Total_Warranty',p_web.GSV('job:Sub_Total_Warranty'))
    p_web.SSV('save:ClaimValue',p_web.GSV('jobe:ClaimValue'))
    p_web.SSV('save:ClaimPartsCost',p_web.GSV('jobe:ClaimPartsCost'))
    p_web.SSV('save:ARC3rdPartyCost',p_web.GSV('jobe:ARC3rdPartyCost'))
    p_web.SSV('save:Courier_Cost_Estimate',p_web.GSV('job:Courier_Cost_Estimate'))
    p_web.SSV('save:Labour_Cost_Estimate',p_web.GSV('job:Labour_Cost_Estimate'))
    p_web.SSV('save:Parts_Cost_Estimate',p_web.GSV('job:Parts_Cost_Estimate'))
    p_web.SSV('save:Sub_Total_Estimate',p_web.GSV('job:Sub_Total_Estimate'))
    p_web.SSV('save:Invoice_Courier_Cost',p_web.GSV('job:Invoice_Courier_Cost'))
    p_web.SSV('save:Invoice_Labour_Cost',p_web.GSV('job:Invoice_Labour_Cost'))
    p_web.SSV('save:Invoice_Parts_Cost',p_web.GSV('job:Invoice_Parts_Cost'))
    p_web.SSV('save:Invoice_Sub_Total',p_web.GSV('job:Invoice_Sub_Total'))
    p_web.SSV('save:WInvoice_Courier_Cost',p_web.GSV('job:WInvoice_Courier_Cost'))
    p_web.SSV('save:WInvoice_Labour_Cost',p_web.GSV('job:WInvoice_Labour_Cost'))
    p_web.SSV('save:WInvoice_Parts_Cost',p_web.GSV('job:WInvoice_Parts_Cost'))
    p_web.SSV('save:WInvoice_Sub_Total',p_web.GSV('job:WInvoice_Sub_Total'))
    p_web.SSV('save:ExchangAdjustment',p_web.GSV('jobe:ExchangeAdjustment'))
    p_web.SSV('save:LabourAdjustment',p_web.GSV('jobe:LabourAdjustment'))
    p_web.SSV('save:PartsAdjustment',p_web.GSV('jobe:PartsAdjustment'))
    p_web.SSV('save:WLabourPaid',p_web.GSV('jobe2:WLabourPaid'))
    p_web.SSV('save:WPartsPaid',p_web.GSV('jobe2:WPartsPaid'))
    p_web.SSV('save:WOtherCosts',p_web.GSV('jobe2:WOtherCosts'))
    p_web.SSV('save:WVat',p_web.GSV('jobe2:WVat'))
    p_web.SSV('save:WTotal',p_web.GSV('jobe2:WTotal'))
    p_web.SSV('save:IgnoreClaimCosts',p_web.GSV('jobe:IgnoreClaimCosts'))
    p_web.SSV('save:Ignore_Warranty_Charges',p_web.GSV('job:Ignore_Warranty_Charges'))
    p_web.SSV('save:Ignore_Chargeable_Charges',p_web.GSV('job:Ignore_Chargeable_Charges'))
    p_web.SSV('save:Ignore_Estimate_Charges',p_web.GSV('job:Ignore_Estimate_Charges'))
    p_web.SSV('save:RRCClabourCost',p_web.GSV('jobe:RRCCLabourCost'))
    p_web.SSV('save:RRCCPartsCost',p_web.GSV('jobe:RRCCPartsCost'))
    p_web.SSV('save:RRCCSubTotal',p_web.GSV('jobe:RRCCSubTotal'))
    p_web.SSV('save:RRCWLabourCost',p_web.GSV('jobe:RRCWLabourCost'))
    p_web.SSV('save:RRCWPartsCost',p_web.GSV('jobe:RRCWPartsCost'))
    p_web.SSV('save:RRCWSubTotal',p_web.GSV('jobe:RRCWSubTotal'))
    p_web.SSV('save:IgnoreRRCChaCosts',p_web.GSV('jobe:IgnoreRRCChaCosts'))
    p_web.SSV('save:IgnoreRRCWarCosts',p_web.GSV('jobe:IgnoreRRCWarCosts'))
    p_web.SSV('save:RRCELabourCost',p_web.GSV('jobe:RRCELabourCost'))
    p_web.SSV('save:RRCEPartsCost',p_web.GSV('jobe:RRCEPartsCost'))
    p_web.SSV('save:RRCESubTotal',p_web.GSV('jobe:RRCESubTotal'))
    p_web.SSV('save:InvRRCCLabourCost',p_web.GSV('jobe:InvRRCCLabourCost'))
    p_web.SSV('save:InvRRCCPartsCost',p_web.GSV('jobe:InvRRCCPartsCost'))
    p_web.SSV('save:InvRRCCSubTotal',p_web.GSV('jobe:InvRRCCSubTotal'))
    p_web.SSV('save:IgnoreRRCEstCosts',p_web.GSV('jobe:IgnoreRRCEstCosts'))
    p_web.SSV('save:InvRRCWLabourCost',p_web.GSV('jobe:InvRRCWLabourCost'))
    p_web.SSV('save:InvRRCWPartsCost',p_web.GSV('jobe:InvRRCWPartsCost'))
    p_web.SSV('save:InvRRCWSubTotal',p_web.GSV('jobe:InvRRCWSubTotal'))
    p_web.SSV('save:JobDiscountAmnt',p_web.GSV('jobe2:JobDiscountAmnt'))
    p_web.SSV('save:InvDiscountAmnt',p_web.GSV('jobe2:InvDiscountAmnt'))
    
updateARCCost      routine
    case (p_web.GSV('tmp:ARCViewCostType'))
    of 'Chargeable'
        p_web.SSV('job:Labour_Cost',p_web.GSV('tmp:ARCCost2'))
    of 'Warranty'
        p_web.SSV('job:Labour_Cost_Warranty',p_web.GSV('tmp:ARCCost2'))
    of 'Estimate'
        p_web.SSV('job:Labour_Cost_Estimate',p_web.GSV('tmp:ARCCost2'))
    END ! case (p_web.GSV('tmp:RRCViewCostType'))
updateRRCCost      routine
    case (p_web.GSV('tmp:RRCViewCostType'))
    of 'Chargeable'
        p_web.SSV('jobe:RRCCLabourCost',p_web.GSV('tmp:RRCCost2'))
        p_web.SSV('jobe2:JobDiscountAmnt',p_web.GSV('DefaultLabourCost') - p_web.GSV('jobe:RRCCLabourCost'))
        IF (p_web.GSV('jobe2:JobDiscountAmnt') < 0)
            p_web.SSV('jobe2:JobDiscountAmnt',0)
        END
            
    of 'Warranty'
        p_web.SSV('jobe:RRCWLabourCost',p_web.GSV('tmp:RRCCost2'))
    of 'Estimate'
        p_web.SSV('jobe:RRCELabourCost',p_web.GSV('tmp:RRCCost2'))
    END ! case (p_web.GSV('tmp:RRCViewCostType'))

OpenFiles  ROUTINE
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(SUBCHRGE)
  p_web._OpenFile(TRACHRGE)
  p_web._OpenFile(STDCHRGE)
  p_web._OpenFile(USERS)
  p_web._OpenFile(JOBPAYMT)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(INVOICE)
  p_web._OpenFile(JOBSINV)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(TRDPARTY)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(SUBCHRGE)
  p_Web._CloseFile(TRACHRGE)
  p_Web._CloseFile(STDCHRGE)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(JOBPAYMT)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(INVOICE)
  p_Web._CloseFile(JOBSINV)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(TRDPARTY)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('ViewCosts_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  do restoreFields
  Do DeleteSessionValues

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('tmp:ARCCost1')
    p_web.SetPicture('tmp:ARCCost1','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost1','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost1')
    p_web.SetPicture('tmp:AdjustmentCost1','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost1','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost2')
    p_web.SetPicture('tmp:ARCCost2','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost2','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost2')
    p_web.SetPicture('tmp:AdjustmentCost2','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost2','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost3')
    p_web.SetPicture('tmp:ARCCost3','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost3','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost3')
    p_web.SetPicture('tmp:AdjustmentCost3','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost3','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost4')
    p_web.SetPicture('tmp:ARCCost4','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost4','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost4')
    p_web.SetPicture('tmp:AdjustmentCost4','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost4','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost5')
    p_web.SetPicture('tmp:ARCCost5','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost5','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost5')
    p_web.SetPicture('tmp:AdjustmentCost5','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost5','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost6')
    p_web.SetPicture('tmp:ARCCost6','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost6','@n14.2')
  If p_web.IfExistsValue('tmp:AdjustmentCost6')
    p_web.SetPicture('tmp:AdjustmentCost6','@n14.2')
  End
  p_web.SetSessionPicture('tmp:AdjustmentCost6','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost7')
    p_web.SetPicture('tmp:ARCCost7','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost7','@n14.2')
  If p_web.IfExistsValue('tmp:ARCCost8')
    p_web.SetPicture('tmp:ARCCost8','@n14.2')
  End
  p_web.SetSessionPicture('tmp:ARCCost8','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost0')
    p_web.SetPicture('tmp:RRCCost0','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost0','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost1')
    p_web.SetPicture('tmp:RRCCost1','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost1','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost2')
    p_web.SetPicture('tmp:RRCCost2','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost2','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost3')
    p_web.SetPicture('tmp:RRCCost3','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost3','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost4')
    p_web.SetPicture('tmp:RRCCost4','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost4','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost5')
    p_web.SetPicture('tmp:RRCCost5','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost5','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost6')
    p_web.SetPicture('tmp:RRCCost6','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost6','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost7')
    p_web.SetPicture('tmp:RRCCost7','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost7','@n14.2')
  If p_web.IfExistsValue('tmp:RRCCost8')
    p_web.SetPicture('tmp:RRCCost8','@n14.2')
  End
  p_web.SetSessionPicture('tmp:RRCCost8','@n14.2')
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('Hide:ARCCosts') <> 1
    loc:TabNumber += 1
  End
  If p_web.GSV('Hide:RRCCosts') <> 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:ARCViewCostType',tmp:ARCViewCostType)
  p_web.SetSessionValue('tmp:ARCIgnoreDefaultCharges',tmp:ARCIgnoreDefaultCharges)
  p_web.SetSessionValue('tmp:ARCIgnoreReason',tmp:ARCIgnoreReason)
  p_web.SetSessionValue('tmp:ARCCost1',tmp:ARCCost1)
  p_web.SetSessionValue('tmp:AdjustmentCost1',tmp:AdjustmentCost1)
  p_web.SetSessionValue('tmp:ARCCost2',tmp:ARCCost2)
  p_web.SetSessionValue('tmp:AdjustmentCost2',tmp:AdjustmentCost2)
  p_web.SetSessionValue('tmp:ARCCost3',tmp:ARCCost3)
  p_web.SetSessionValue('tmp:AdjustmentCost3',tmp:AdjustmentCost3)
  p_web.SetSessionValue('tmp:ARCCost4',tmp:ARCCost4)
  p_web.SetSessionValue('tmp:AdjustmentCost4',tmp:AdjustmentCost4)
  p_web.SetSessionValue('tmp:ARCCost5',tmp:ARCCost5)
  p_web.SetSessionValue('tmp:AdjustmentCost5',tmp:AdjustmentCost5)
  p_web.SetSessionValue('tmp:ARCCost6',tmp:ARCCost6)
  p_web.SetSessionValue('tmp:AdjustmentCost6',tmp:AdjustmentCost6)
  p_web.SetSessionValue('tmp:ARCCost7',tmp:ARCCost7)
  p_web.SetSessionValue('tmp:ARCCost8',tmp:ARCCost8)
  p_web.SetSessionValue('jobe:ARC3rdPartyInvoiceNumber',jobe:ARC3rdPartyInvoiceNumber)
  p_web.SetSessionValue('tmp:ARCInvoiceNumber',tmp:ARCInvoiceNumber)
  p_web.SetSessionValue('tmp:RRCViewCostType',tmp:RRCViewCostType)
  p_web.SetSessionValue('tmp:RRCIgnoreDefaultCharges',tmp:RRCIgnoreDefaultCharges)
  p_web.SetSessionValue('tmp:RRCIgnoreReason',tmp:RRCIgnoreReason)
  p_web.SetSessionValue('tmp:RRCCost0',tmp:RRCCost0)
  p_web.SetSessionValue('tmp:RRCCost1',tmp:RRCCost1)
  p_web.SetSessionValue('tmp:originalInvoice',tmp:originalInvoice)
  p_web.SetSessionValue('tmp:RRCCost2',tmp:RRCCost2)
  p_web.SetSessionValue('tmp:RRCCost3',tmp:RRCCost3)
  p_web.SetSessionValue('tmp:RRCCost4',tmp:RRCCost4)
  p_web.SetSessionValue('tmp:RRCCost5',tmp:RRCCost5)
  p_web.SetSessionValue('tmp:RRCCost6',tmp:RRCCost6)
  p_web.SetSessionValue('tmp:RRCCost7',tmp:RRCCost7)
  p_web.SetSessionValue('tmp:RRCCost8',tmp:RRCCost8)
  p_web.SetSessionValue('tmp:RRCInvoiceNumber',tmp:RRCInvoiceNumber)
  p_web.SetSessionValue('jobe:ExcReplcamentCharge',jobe:ExcReplcamentCharge)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:ARCViewCostType')
    tmp:ARCViewCostType = p_web.GetValue('tmp:ARCViewCostType')
    p_web.SetSessionValue('tmp:ARCViewCostType',tmp:ARCViewCostType)
  End
  if p_web.IfExistsValue('tmp:ARCIgnoreDefaultCharges')
    tmp:ARCIgnoreDefaultCharges = p_web.GetValue('tmp:ARCIgnoreDefaultCharges')
    p_web.SetSessionValue('tmp:ARCIgnoreDefaultCharges',tmp:ARCIgnoreDefaultCharges)
  End
  if p_web.IfExistsValue('tmp:ARCIgnoreReason')
    tmp:ARCIgnoreReason = p_web.GetValue('tmp:ARCIgnoreReason')
    p_web.SetSessionValue('tmp:ARCIgnoreReason',tmp:ARCIgnoreReason)
  End
  if p_web.IfExistsValue('tmp:ARCCost1')
    tmp:ARCCost1 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost1')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost1',tmp:ARCCost1)
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost1')
    tmp:AdjustmentCost1 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost1')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost1',tmp:AdjustmentCost1)
  End
  if p_web.IfExistsValue('tmp:ARCCost2')
    tmp:ARCCost2 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost2')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost2',tmp:ARCCost2)
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost2')
    tmp:AdjustmentCost2 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost2')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost2',tmp:AdjustmentCost2)
  End
  if p_web.IfExistsValue('tmp:ARCCost3')
    tmp:ARCCost3 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost3')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost3',tmp:ARCCost3)
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost3')
    tmp:AdjustmentCost3 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost3')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost3',tmp:AdjustmentCost3)
  End
  if p_web.IfExistsValue('tmp:ARCCost4')
    tmp:ARCCost4 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost4')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost4',tmp:ARCCost4)
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost4')
    tmp:AdjustmentCost4 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost4')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost4',tmp:AdjustmentCost4)
  End
  if p_web.IfExistsValue('tmp:ARCCost5')
    tmp:ARCCost5 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost5')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost5',tmp:ARCCost5)
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost5')
    tmp:AdjustmentCost5 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost5')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost5',tmp:AdjustmentCost5)
  End
  if p_web.IfExistsValue('tmp:ARCCost6')
    tmp:ARCCost6 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost6')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost6',tmp:ARCCost6)
  End
  if p_web.IfExistsValue('tmp:AdjustmentCost6')
    tmp:AdjustmentCost6 = p_web.dformat(clip(p_web.GetValue('tmp:AdjustmentCost6')),'@n14.2')
    p_web.SetSessionValue('tmp:AdjustmentCost6',tmp:AdjustmentCost6)
  End
  if p_web.IfExistsValue('tmp:ARCCost7')
    tmp:ARCCost7 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost7')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost7',tmp:ARCCost7)
  End
  if p_web.IfExistsValue('tmp:ARCCost8')
    tmp:ARCCost8 = p_web.dformat(clip(p_web.GetValue('tmp:ARCCost8')),'@n14.2')
    p_web.SetSessionValue('tmp:ARCCost8',tmp:ARCCost8)
  End
  if p_web.IfExistsValue('jobe:ARC3rdPartyInvoiceNumber')
    jobe:ARC3rdPartyInvoiceNumber = p_web.GetValue('jobe:ARC3rdPartyInvoiceNumber')
    p_web.SetSessionValue('jobe:ARC3rdPartyInvoiceNumber',jobe:ARC3rdPartyInvoiceNumber)
  End
  if p_web.IfExistsValue('tmp:ARCInvoiceNumber')
    tmp:ARCInvoiceNumber = p_web.GetValue('tmp:ARCInvoiceNumber')
    p_web.SetSessionValue('tmp:ARCInvoiceNumber',tmp:ARCInvoiceNumber)
  End
  if p_web.IfExistsValue('tmp:RRCViewCostType')
    tmp:RRCViewCostType = p_web.GetValue('tmp:RRCViewCostType')
    p_web.SetSessionValue('tmp:RRCViewCostType',tmp:RRCViewCostType)
  End
  if p_web.IfExistsValue('tmp:RRCIgnoreDefaultCharges')
    tmp:RRCIgnoreDefaultCharges = p_web.GetValue('tmp:RRCIgnoreDefaultCharges')
    p_web.SetSessionValue('tmp:RRCIgnoreDefaultCharges',tmp:RRCIgnoreDefaultCharges)
  End
  if p_web.IfExistsValue('tmp:RRCIgnoreReason')
    tmp:RRCIgnoreReason = p_web.GetValue('tmp:RRCIgnoreReason')
    p_web.SetSessionValue('tmp:RRCIgnoreReason',tmp:RRCIgnoreReason)
  End
  if p_web.IfExistsValue('tmp:RRCCost0')
    tmp:RRCCost0 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost0')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost0',tmp:RRCCost0)
  End
  if p_web.IfExistsValue('tmp:RRCCost1')
    tmp:RRCCost1 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost1')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost1',tmp:RRCCost1)
  End
  if p_web.IfExistsValue('tmp:originalInvoice')
    tmp:originalInvoice = p_web.GetValue('tmp:originalInvoice')
    p_web.SetSessionValue('tmp:originalInvoice',tmp:originalInvoice)
  End
  if p_web.IfExistsValue('tmp:RRCCost2')
    tmp:RRCCost2 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost2')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost2',tmp:RRCCost2)
  End
  if p_web.IfExistsValue('tmp:RRCCost3')
    tmp:RRCCost3 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost3')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost3',tmp:RRCCost3)
  End
  if p_web.IfExistsValue('tmp:RRCCost4')
    tmp:RRCCost4 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost4')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost4',tmp:RRCCost4)
  End
  if p_web.IfExistsValue('tmp:RRCCost5')
    tmp:RRCCost5 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost5')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost5',tmp:RRCCost5)
  End
  if p_web.IfExistsValue('tmp:RRCCost6')
    tmp:RRCCost6 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost6')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost6',tmp:RRCCost6)
  End
  if p_web.IfExistsValue('tmp:RRCCost7')
    tmp:RRCCost7 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost7')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost7',tmp:RRCCost7)
  End
  if p_web.IfExistsValue('tmp:RRCCost8')
    tmp:RRCCost8 = p_web.dformat(clip(p_web.GetValue('tmp:RRCCost8')),'@n14.2')
    p_web.SetSessionValue('tmp:RRCCost8',tmp:RRCCost8)
  End
  if p_web.IfExistsValue('tmp:RRCInvoiceNumber')
    tmp:RRCInvoiceNumber = p_web.GetValue('tmp:RRCInvoiceNumber')
    p_web.SetSessionValue('tmp:RRCInvoiceNumber',tmp:RRCInvoiceNumber)
  End
  if p_web.IfExistsValue('jobe:ExcReplcamentCharge')
    jobe:ExcReplcamentCharge = p_web.GetValue('jobe:ExcReplcamentCharge')
    p_web.SetSessionValue('jobe:ExcReplcamentCharge',jobe:ExcReplcamentCharge)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('ViewCosts_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  ! Return URL
  IF (p_web.IFExistsValue('ViewCostsReturnURL'))
      p_web.StoreValue('ViewCostsReturnURL')
  ELSE
      p_web.SSV('ViewCostsReturnURL','ViewJob')
  END
  p_web.SSV('Prompt:RCost0','')
  p_web.SSV('Prompt:RCost1','')
  p_web.SSV('Prompt:RCost2','')
  p_web.SSV('Prompt:RCost3','')
  p_web.SSV('Prompt:RCost4','')
  p_web.SSV('Prompt:RCost5','')
  p_web.SSV('Prompt:RCost6','')
  p_web.SSV('Prompt:RCost7','')
  p_web.SSV('Prompt:RCost8','')
  p_web.SSV('Prompt:ACost1','')
  p_web.SSV('Prompt:ACost2','')
  p_web.SSV('Prompt:ACost3','')
  p_web.SSV('Prompt:ACost4','')
  p_web.SSV('Prompt:ACost5','')
  p_web.SSV('Prompt:ACost6','')
  p_web.SSV('Prompt:ACost7','')
  p_web.SSV('Prompt:ACost8','')
  p_web.SSV('tmp:ARCIgnoreReason','')
  p_web.SSV('tmp:RRCIgnoreReason','')
  p_web.SSV('ValidateIgnoreTickBoxARC',0)
  p_web.SSV('ValidateIgnoreTickBox',0)
  
  !Empty Audit Queue
  clear(tmpaud:Record)
  tmpaud:sessionID = p_web.SessionID
  set(tmpaud:keySessionID,tmpaud:keySessionID)
  loop
      next(tempAuditQueue)
      IF (error())
          break
      END ! IF (error())
      IF (tmpaud:sessionID <> p_web.sessionID)
          break
      END ! IF (tmpaud:sessionID <> p_web.sessionID)
      delete(tempAuditQueue)
  END
  
  p_web.SSV('JobPricingRoutine:ForceWarranty',0)
  JobPricingRoutine(p_web)
  
  do pricingRoutine
  
  p_web.SSV('Hide:ARCCosts',1)
  p_web.SSV('Hide:RRCCosts',1)
  
  p_web.SSV('Hide:ARCChargeable',1)
  p_web.SSV('Hide:ARCWarranty',1)
  p_web.SSV('Hide:ARCClaim',1)
  p_web.SSV('Hide:ARC3rdParty',1)
  p_web.SSV('Hide:ARCEstimate',1)
  p_web.SSV('Hide:ARCCInvoice',1)
  p_web.SSV('Hide:ARCWInvoice',1)
  p_web.SSV('Hide:ARCClaimInvoice',1)
  p_web.SSV('Hide:ManufacturerPaid',1)
  p_web.SSV('Hide:RRCChargeable',1)
  p_web.SSV('Hide:RRCWarranty',1)
  p_web.SSV('Hide:RRCEstimate',1)
  p_web.SSV('Hide:RRCHandling',1)
  p_web.SSV('Hide:RRCExchange',1)
  p_web.SSV('Hide:RRCCInvoice',1)
  p_web.SSV('Hide:RRCWInvoice',1)
  p_web.SSV('Hide:Credit',1)
  
  p_web.SSV('ReadOnly:ARCCost1',1)
  p_web.SSV('ReadOnly:ARCCost2',1)
  p_web.SSV('ReadOnly:ARCCost3',1)
  p_web.SSV('ReadOnly:RRCCost1',1)
  p_web.SSV('ReadOnly:RRCCost2',1)
  p_web.SSV('ReadOnly:RRCCost3',1)
  
  
  sentToHub(p_web)
  
  IF (p_web.GSV('jobe:WebJob') = 1)
      IF (p_web.GSV('job:Chargeable_job') = 'YES')
          IF (p_web.GSV('BookingSite') = 'RRC')
              p_web.SSV('Hide:RRCChargeable',0)
              IF (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  IF (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCExchange',0)
                  ELSE ! IF (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCHandling',0)
                      p_web.SSV('Hide:ARCChargeable',0)
                  END ! IF (p_web.GSV('jobe:ExchangedAtRRC'))
              ELSE ! IF (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  sentToHub(p_web)
                  IF (p_web.GSV('SentToHub') = 1)
                      p_web.SSV('Hide:ARCChargeable',0)
                      p_web.SSV('Hide:RRCHandling',0)
                  END ! IF (p_web.GSV('SentToHub') = 1)
              END ! IF (p_web.GSV('job:Exchange_Unit_Number') > 0)
          ELSE ! IF (p_web.GSV('BookingSite') = 'RRC')  
              p_web.SSV('Hide:RRCChargeable',0)
              IF (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  IF (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCExchange',0)
                  ELSE ! IF (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCHandling',0)
                  END ! IF (p_web.GSV('jobe:ExchangedAtRRC'))
              ELSE ! IF (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  IF (p_web.GSV('SentToHub') = 1)
                      p_web.SSV('Hide:RRCHandling',0)
                  END ! IF (p_web.GSV('SentToHub') = 1)
              END ! IF (p_web.GSV('job:Exchange_Unit_Number') > 0)
  
              IF (p_web.GSV('SentToHub') = 1)
                  p_web.SSV('Hide:ARCChargeable',0)
              END ! IF (p_web.GSV('SentToHub') = 1)
          END ! IF (p_web.GSV('BookingSite') = 'RRC')
      END ! IF (p_web.GSV('job:Chargeable_job') = 'YES')
  
      IF (p_web.GSV('job:Warranty_job') = 'YES')
          IF (p_web.GSV('BookingSite') = 'RRC')
              IF (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  IF (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCExchange',0)
                  ELSE ! IF (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCHandling',0)
                  END ! IF (p_web.GSV('jobe:ExchangedAtRRC'))
              ELSE ! IF (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  IF (p_web.GSV('SentToHub') = 1)
                      p_web.SSV('Hide:RRCHandling',0)
                  ELSE
                      p_web.SSV('Hide:RRCWarranty',0)
                  END ! IF (p_web.GSV('SentToHub') = 1)
              END ! IF (p_web.GSV('job:Exchange_Unit_Number') > 0)
          ELSE ! IF (p_web.GSV('BookingSite') = 'RRC'           )
              p_web.SSV('Hide:ARCClaim',0)
              IF (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  IF (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCExchange',0)
                      p_web.SSV('Hide:ARCWarranty',0)
                  ELSE ! IF (p_web.GSV('jobe:ExchangedAtRRC'))
                      p_web.SSV('Hide:RRCHandling',0)
                      p_web.SSV('Hide:ARCWarranty',0)
                  END ! IF (p_web.GSV('jobe:ExchangedAtRRC'))
              ELSE ! IF (p_web.GSV('job:Exchange_Unit_Number') > 0)
                  IF (p_web.GSV('SentToHub') = 1)
                      p_web.SSV('Hide:RRCHandling',0)
                      p_web.SSV('Hide:ARCWarranty',0)
                  ELSE
                      p_web.SSV('Hide:RRCWarranty',0)
                  END ! IF (p_web.GSV('SentToHub') = 1)
              END ! IF (p_web.GSV('job:Exchange_Unit_Number') > 0)
          END !
      END  !IF (p_web.GSV('job:Warranty_job') <> 'YES')
  ELSE ! IF (p_web.GSV('jobe:WebJob'))
      IF (p_web.GSV('BookingSite') <> 'RRC')
          IF (p_web.GSV('job:Chargeable_Job') = 'YES')
              p_web.SSV('Hide:ARCChargeable',0)
          END ! IF (p_web.GSV('job:Chargeable_Job') = 'YES')
          IF (p_web.GSV('job:Warranty_job') = 'YES')
              p_web.SSV('Hide:ARCWarranty',0)
              p_web.SSV('Hide:ARCClaim',0)
          END ! IF (p_web.GSV('job:Warranty_job') = 'YES')
      END ! IF (p_web.GSV('BookingSite') <> 'RRC')
  END ! IF (p_web.GSV('jobe:WebJob'))
  
  
  Access:INVOICE.Clearkey(inv:invoice_Number_Key)
  inv:Invoice_Number    = p_web.GSV('job:Invoice_Number')
  IF (Access:INVOICE.TryFetch(inv:invoice_Number_Key) = Level:Benign)
      ! Found
  ELSE ! IF (Access:INVOICE.TryFetch(inv:invoice_Number_Key) = Level:Benign)
      ! Error
  END ! IF (Access:INVOICE.TryFetch(inv:invoice_Number_Key) = Level:Benign)
  
  IF (p_web.GSV('job:Invoice_Number') > 0)
      IF (p_web.GSV('Hide:ARCChargeable') = 0 and inv:ARCInvoiceDate > 0)
          p_web.SSV('Hide:ARCChargeable',1)
          p_web.SSV('Hide:ARCCInvoice',0)
      END ! IF (p_web.GSV('Hide:ARCChargeable') = 0)
  
      IF (p_web.GSV('Hide:RRCChargeable') = 0 and inv:ExportedRRCOracle)
          p_web.SSV('Hide:RRCChargeable',1)
          p_web.SSV('Hide:RRCCInvoice',0)
      END ! IF (p_web.GSV('Hide:RRCChargeable') = 0 and inv:ExportedRRCOracle)
  END ! IF (p_web.GSV('job:Invoice_Number') > 0)
  
  IF (p_web.GSV('Hide:RRCWarranty') = 0 and p_web.GSV('wob:RRCWInvoiceNumber') > 0)
      p_web.SSV('Hide:RRCWarranty',1)
      p_web.SSV('Hide:RRCWInvoice',0)
  END ! IF (p_web.GSV('Hide:RRCWarranty',0))
  
  IF (p_web.GSV('job:Invoice_Number_Waranty') > 0)
      IF (p_web.GSV('Hide:ARCWarranty') = 0)
          p_web.SSV('Hide:ARCWarranty',1)
          p_web.SSV('Hide:ARCWInvoice',0)
      END ! IF (p_web.GSV('Hide:RRCWarranty',0))
  
      IF (p_web.GSV('Hide:ARCClaim') = 0)
          p_web.SSV('Hide:ARCClaim',1)
          p_web.SSV('Hide:ARCClaimInvoice',0)
          p_web.SSV('Hide:ManufacturerPaid',0)
      END ! IF (p_web.GSV('Hide:ARCClaim',0))
  END ! IF (p_web.GSV('job:Invoice_Number_Waranty') > 0)
  
  IF (p_web.GSV('Hide:RRCHandling') = 0)
      IF ((p_web.GSV('job:Chargeable_Job') = 'YES' And |
          ExcludeHandlingFee('C',p_web.GSV('job:Manufacturer'),p_web.GSV('job:Repair_Type'))) Or |
          (p_web.GSV('job:Warranty_job') = 'YES' And |
          ExcludeHandlingFee('W',p_web.GSV('job:Manufacturer'),p_web.GSV('job:Repair_Type_Warranty'))))
  
          p_web.SSV('Hide:RRCHandling',1)
      END
  END ! IF (p_web.GSV('Hide:RRCHandling',0))
  
  IF (p_web.GSV('job:Chargeable_Job') = 'YES')
      IF (p_web.GSV('job:Estimate') = 'YES')
          IF (p_web.GSV('job:Estimate_Accepted') <> 'YES' And |
              p_web.GSV('job:Estimate_Rejected') <> 'YES')  ! #13380 Hiding costs when Rejected. (DBH: 16/09/2014)
              p_web.SSV('Hide:RRCChargeable',1)
              p_web.SSV('Hide:ARCChargeable',1)
          END
  
          IF (p_web.GSV('jobe:WebJob') = 1)
              p_web.SSV('Hide:RRCEstimate',0)
              IF (p_web.GSV('SentToHub') = 1)
                  p_web.SSV('Hide:ARCEstimate',0)
              END ! IF (p_web.GSV('SentToHub') = 1)
          ELSE ! IF (p_web.GSV('jobe:WebJob'))
              p_web.SSV('Hide:ARCEstimate',0)
          END ! IF (p_web.GSV('jobe:WebJob'))
  
      END ! IF (p_web.GSV('job:Estimate') = 'YES')
  
  END ! IF (p_web.GSV('job:Chargeable_Job') = 'YES')
  
  IF (p_web.GSV('BookingSite') <> 'RRC')
      Access:JOBTHIRD.Clearkey(jot:RefNumberKey)
      jot:RefNumber    = p_web.GSV('job:ref_Number')
      IF (Access:JOBTHIRD.TryFetch(jot:RefNumberKey) = Level:Benign)
          ! Found
          p_web.SSV('Hide:ARC3rdParty',0)
      ELSE ! IF (Access:JOBTHIRD.TryFetch(jot:RefNumberKey) = Level:Benign)
          ! Error
      END ! IF (Access:JOBTHIRD.TryFetch(jot:RefNumberKey) = Level:Benign)
  END ! IF (p_web.GSV('BookingSite') <> 'RRC')
  
  IF (p_web.GSV('Hide:RRCHandling') = 0)
      IF (p_web.GSV('job:Chargeable_Job') = 'YES')
  
          Access:REPTYDEF.Clearkey(rtd:ChaManRepairTypeKey)
          rtd:Manufacturer    = p_web.GSV('job:Manufacturer')
          rtd:Chargeable    = 'YES'
          rtd:Repair_Type    = p_web.GSV('job:repair_Type')
          IF (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
              ! Found
              IF (rtd:BER = 4)
                  p_web.SSV('Hide:RRCHandling',1)
              END ! IF (rtd:BER = 4)
          ELSE ! IF (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
              ! Error
          END ! IF (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
      END ! IF (p_web.GSV('job:Chargeable_Job') = 'YES')
  
      IF (p_web.GSV('job:Warranty_Job') = 'YES')
  
          Access:REPTYDEF.Clearkey(rtd:WarManRepairTypeKey)
          rtd:Manufacturer    = p_web.GSV('job:Manufacturer')
          rtd:warranty    = 'YES'
          rtd:Repair_Type    = p_web.GSV('job:repair_Type_warranty')
          IF (Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign)
              ! Found
              IF (rtd:BER = 4)
                  p_web.SSV('Hide:RRCHandling',1)
              END ! IF (rtd:BER = 4)
          ELSE ! IF (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
              ! Error
          END ! IF (Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign)
      END ! IF (p_web.GSV('job:Chargeable_Job') = 'YES')
  END ! IF (p_web.GSV('Hide:RRCHandling') = 0)
  
  IF (p_web.GSV('Hide:RRCCInvoice') = 0)
  
      Access:JOBSINV.Clearkey(jov:typeRecordKey)
      jov:refNumber    = p_web.GSV('job:Ref_Number')
      jov:type    = 'C'
      set(jov:typeRecordKey,jov:typeRecordKey)
      loop
          IF (Access:JOBSINV.Next())
              Break
          END ! IF (Access:JOBSINV.Next())
          IF (jov:refNumber    <> p_web.GSV('job:Ref_Number'))
              Break
          END ! IF (jov:refNumber    <> p_web.GSV('job:Ref_Number'))
          IF (jov:type    <> 'C')
              Break
          END ! IF (jov:type    <> 'C')
          p_web.SSV('Hide:Credit',0)
          break
      END ! loop
  END ! IF (p_web.GSV('Hide:RRCCInvoice') = 0)
    
  !TB13308 - J - 25/06/14 - somehow the unhiding above is not catching all possibilities
  !for the handling charge - IF there is one it should be shown
  !IF p_web.gsv('tmp:HandlingFee') <> '' then p_web.SSV('Hide:RRCHandling',0).
  
  
  IF (p_web.GSV('Hide:ARCChargeable') = 0 or |
      p_web.GSV('Hide:ARCWarranty') = 0 or |
      p_web.GSV('Hide:ARCClaim') = 0 or |
      p_web.GSV('Hide:ARC3rdParty') = 0 or |
      p_web.GSV('Hide:ARCEstimate') = 0 or |
      p_web.GSV('Hide:ARCCInvoice') = 0 or |
      p_web.GSV('Hide:ARCWInvoice') = 0 or |
      p_web.GSV('Hide:ARCClaimInvoice') = 0 or |
      p_web.GSV('Hide:ManufacturerPaid') = 0)
      p_web.SSV('Hide:ARCCosts',0)
  END
  
  IF (p_web.GSV('Hide:RRCChargeable') = 0 or |
      p_web.GSV('Hide:RRCWarranty') = 0 or |
      p_web.GSV('Hide:RRCEstimate') = 0 or |
      p_web.GSV('Hide:RRCHandling') = 0 or |
      p_web.GSV('Hide:RRCExchange') = 0 or |
      p_web.GSV('Hide:RRCCInvoice') = 0 or |
      p_web.GSV('Hide:RRCWInvoice') = 0 or |
      p_web.GSV('Hide:Credit') = 0)
      p_web.SSV('Hide:RRCCosts',0)
  END
  
  IF (p_web.GSV('Hide:RRCChargeable') = 0)
      p_web.SSV('tmp:RRCViewCostType','Chargeable')
  elsIF (p_web.GSV('Hide:RRCWarranty') = 0)
      p_web.SSV('tmp:RRCViewCostType','Warranty')
  elsIF (p_web.GSV('Hide:RRCEstimate') = 0)
      p_web.SSV('tmp:RRCViewCostType','Estimate')
  elsIF (p_web.GSV('Hide:RRCHandling') = 0)
      p_web.SSV('tmp:RRCViewCostType','Handling')
  elsIF (p_web.GSV('Hide:RRCExchange') = 0)
      p_web.SSV('tmp:RRCViewCostType','Exchange')
  elsIF (p_web.GSV('Hide:RRCCInvoice') = 0)
      p_web.SSV('tmp:RRCViewCostType','Chargeable - Invoiced')
  elsIF (p_web.GSV('Hide:RRCWInvoice') = 0)
      p_web.SSV('tmp:RRCViewCostType','Warranty - Invoiced')
  elsIF (p_web.GSV('Hide:Credit') = 0)
      p_web.SSV('tmp:RRCViewCostType','Credits')
  ELSE
      p_web.SSV('tmp:RRCVIewCostType','')
  END !IF (p_web.GSV('Hide:RRCChargeable') = 0)
  
  IF (p_web.GSV('Hide:ARCChargeable') = 0)
      p_web.SSV('tmp:ARCViewCostType','Chargeable')
  elsIF (p_web.GSV('Hide:ARCWarranty') = 0)
      p_web.SSV('tmp:ARCViewCostType','Warranty')
  elsIF (p_web.GSV('Hide:ARCClaim') = 0)
      p_web.SSV('tmp:ARCViewCostType','Claim')
  elsIF (p_web.GSV('Hide:ARC3rdParty') = 0)
      p_web.SSV('tmp:ARCViewCostType','3rd Party')
  elsIF (p_web.GSV('Hide:ARCEstimate') = 0)
      p_web.SSV('tmp:ARCViewCostType','Estimate')
  elsIF (p_web.GSV('Hide:ARCCInvoice') = 0)
      p_web.SSV('tmp:ARCViewCostType','Chargeable - Invoiced')
  elsIF (p_web.GSV('Hide:ARCWInvoice') = 0)
      p_web.SSV('tmp:ARCViewCostType','Warranty - Invoiced')
  elsIF (p_web.GSV('Hide:ARCClaimInvoice') = 0)
      p_web.SSV('tmp:ARCViewCostType','Claim - Invoiced')
  elsIF (p_web.GSV('Hide:ManufacturerPaid') = 0)
      p_web.SSV('tmp:ARCViewCostType','Manufacturer Payment')
  ELSE
      p_web.SSV('tmp:ARCVIewCostType','')
  END !IF (p_web.GSV('Hide:RRCChargeable') = 0)
  
  
  do displayARCCostFields
  do displayRRCCostFields
    do displayOtherFields
    
  
    
  
  do saveFields
  
  do DefaultLabourCost
  
    LoanAttachedToJob(p_web) ! Has a loan unit been, or is, attached?
    
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:ARCViewCostType = p_web.RestoreValue('tmp:ARCViewCostType')
 tmp:ARCIgnoreDefaultCharges = p_web.RestoreValue('tmp:ARCIgnoreDefaultCharges')
 tmp:ARCIgnoreReason = p_web.RestoreValue('tmp:ARCIgnoreReason')
 tmp:ARCCost1 = p_web.RestoreValue('tmp:ARCCost1')
 tmp:AdjustmentCost1 = p_web.RestoreValue('tmp:AdjustmentCost1')
 tmp:ARCCost2 = p_web.RestoreValue('tmp:ARCCost2')
 tmp:AdjustmentCost2 = p_web.RestoreValue('tmp:AdjustmentCost2')
 tmp:ARCCost3 = p_web.RestoreValue('tmp:ARCCost3')
 tmp:AdjustmentCost3 = p_web.RestoreValue('tmp:AdjustmentCost3')
 tmp:ARCCost4 = p_web.RestoreValue('tmp:ARCCost4')
 tmp:AdjustmentCost4 = p_web.RestoreValue('tmp:AdjustmentCost4')
 tmp:ARCCost5 = p_web.RestoreValue('tmp:ARCCost5')
 tmp:AdjustmentCost5 = p_web.RestoreValue('tmp:AdjustmentCost5')
 tmp:ARCCost6 = p_web.RestoreValue('tmp:ARCCost6')
 tmp:AdjustmentCost6 = p_web.RestoreValue('tmp:AdjustmentCost6')
 tmp:ARCCost7 = p_web.RestoreValue('tmp:ARCCost7')
 tmp:ARCCost8 = p_web.RestoreValue('tmp:ARCCost8')
 tmp:ARCInvoiceNumber = p_web.RestoreValue('tmp:ARCInvoiceNumber')
 tmp:RRCViewCostType = p_web.RestoreValue('tmp:RRCViewCostType')
 tmp:RRCIgnoreDefaultCharges = p_web.RestoreValue('tmp:RRCIgnoreDefaultCharges')
 tmp:RRCIgnoreReason = p_web.RestoreValue('tmp:RRCIgnoreReason')
 tmp:RRCCost0 = p_web.RestoreValue('tmp:RRCCost0')
 tmp:RRCCost1 = p_web.RestoreValue('tmp:RRCCost1')
 tmp:originalInvoice = p_web.RestoreValue('tmp:originalInvoice')
 tmp:RRCCost2 = p_web.RestoreValue('tmp:RRCCost2')
 tmp:RRCCost3 = p_web.RestoreValue('tmp:RRCCost3')
 tmp:RRCCost4 = p_web.RestoreValue('tmp:RRCCost4')
 tmp:RRCCost5 = p_web.RestoreValue('tmp:RRCCost5')
 tmp:RRCCost6 = p_web.RestoreValue('tmp:RRCCost6')
 tmp:RRCCost7 = p_web.RestoreValue('tmp:RRCCost7')
 tmp:RRCCost8 = p_web.RestoreValue('tmp:RRCCost8')
 tmp:RRCInvoiceNumber = p_web.RestoreValue('tmp:RRCInvoiceNumber')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('ViewCostsReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('ViewCosts_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('ViewCosts_ChainTo')
    loc:formaction = p_web.GetSessionValue('ViewCosts_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ViewCostsReturnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
    loc:viewonly = Choose(p_web.GSV('Job:ViewOnly') = 1,1,0)
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="ViewCosts" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="ViewCosts" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="ViewCosts" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('View Costs') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('View Costs',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_ViewCosts">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_ViewCosts" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_ViewCosts')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GSV('Hide:ARCCosts') <> 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('ARC Costs') & ''''
        End
        If p_web.GSV('Hide:RRCCosts') <> 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('RRC Costs') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_ViewCosts')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_ViewCosts'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewCosts_BrowseJobCredits_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewCosts_BrowseJobCredits_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ViewCosts_BrowseJobCredits_embedded_div')&''');'
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('Hide:ARCCosts') <> 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:ARCViewCostType')
    ElsIf p_web.GSV('Hide:RRCCosts') <> 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:RRCViewCostType')
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.jobe:ExcReplcamentCharge')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GSV('Hide:ARCCosts') <> 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          End
          If p_web.GSV('Hide:RRCCosts') <> 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_ViewCosts')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GSV('Hide:ARCCosts') <> 1
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    end
    if p_web.GSV('Hide:RRCCosts') <> 1
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GSV('Hide:ARCCosts') <> 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('ARC Costs') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewCosts_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('ARC Costs')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('ARC Costs')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('ARC Costs')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('ARC Costs')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCViewCostType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCViewCostType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCViewCostType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__line1
      do Comment::__line1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCIgnoreDefaultCharges
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCIgnoreDefaultCharges
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCIgnoreDefaultCharges
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCIgnoreReason
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCIgnoreReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCIgnoreReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonAcceptARCReason
      do Comment::buttonAcceptARCReason
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonCancelARCReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonCancelARCReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCCost1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCCost1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCCost1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:AdjustmentCost1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:AdjustmentCost1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCCost2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCCost2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCCost2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:AdjustmentCost2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:AdjustmentCost2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCCost3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCCost3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCCost3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:AdjustmentCost3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:AdjustmentCost3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__line2
      do Comment::__line2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCCost4
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCCost4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCCost4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:AdjustmentCost4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:AdjustmentCost4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCCost5
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCCost5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCCost5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:AdjustmentCost5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:AdjustmentCost5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCCost6
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCCost6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCCost6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:AdjustmentCost6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:AdjustmentCost6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCCost7
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCCost7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCCost7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCCost8
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCCost8
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCCost8
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:ARC3rdPartyInvoiceNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:ARC3rdPartyInvoiceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe:ARC3rdPartyInvoiceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ARCInvoiceNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ARCInvoiceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ARCInvoiceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
  If p_web.GSV('Hide:RRCCosts') <> 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('RRC Costs') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewCosts_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('RRC Costs')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('RRC Costs')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('RRC Costs')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('RRC Costs')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCViewCostType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCViewCostType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCViewCostType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__line3
      do Comment::__line3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCIgnoreDefaultCharges
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCIgnoreDefaultCharges
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCIgnoreDefaultCharges
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCIgnoreReason
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCIgnoreReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCIgnoreReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonAcceptRRCReason
      do Comment::buttonAcceptRRCReason
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonCancelRRCReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonCancelRRCReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCCost0
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCCost0
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCCost0
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCCost1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCCost1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCCost1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:originalInvoice
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:originalInvoice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:originalInvoice
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCCost2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCCost2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCCost2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCCost3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCCost3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCCost3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::BrowseJobCredits
      do Value::BrowseJobCredits
      do Comment::BrowseJobCredits
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__line4
      do Comment::__line4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCCost4
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCCost4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCCost4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCCost5
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCCost5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCCost5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCCost6
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCCost6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCCost6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCCost7
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCCost7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCCost7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCCost8
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCCost8
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCCost8
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:RRCInvoiceNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:RRCInvoiceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:RRCInvoiceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewCosts_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:ExcReplcamentCharge
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&180&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:ExcReplcamentCharge
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe:ExcReplcamentCharge
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::tmp:ARCViewCostType  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCViewCostType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('View Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCViewCostType') & '_prompt')

Validate::tmp:ARCViewCostType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCViewCostType',p_web.GetValue('NewValue'))
    tmp:ARCViewCostType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCViewCostType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ARCViewCostType',p_web.GetValue('Value'))
    tmp:ARCViewCostType = p_web.GetValue('Value')
  End
  do displayARCCostFields
  do displayOtherFields
  do Value::tmp:ARCViewCostType
  do SendAlert
  do Prompt::tmp:ARCCost1
  do Value::tmp:ARCCost1  !1
  do Comment::tmp:ARCCost1
  do Prompt::tmp:ARCCost2
  do Value::tmp:ARCCost2  !1
  do Comment::tmp:ARCCost2
  do Prompt::tmp:ARCCost3
  do Value::tmp:ARCCost3  !1
  do Comment::tmp:ARCCost3
  do Prompt::tmp:ARCCost4
  do Value::tmp:ARCCost4  !1
  do Comment::tmp:ARCCost4
  do Prompt::tmp:ARCCost5
  do Value::tmp:ARCCost5  !1
  do Comment::tmp:ARCCost5
  do Prompt::tmp:ARCCost6
  do Value::tmp:ARCCost6  !1
  do Comment::tmp:ARCCost6
  do Prompt::tmp:ARCCost7
  do Value::tmp:ARCCost7  !1
  do Comment::tmp:ARCCost7
  do Prompt::tmp:ARCCost8
  do Value::tmp:ARCCost8  !1
  do Comment::tmp:ARCCost8
  do Value::tmp:AdjustmentCost1  !1
  do Value::tmp:AdjustmentCost2  !1
  do Value::tmp:AdjustmentCost3  !1
  do Value::tmp:AdjustmentCost4  !1
  do Value::tmp:AdjustmentCost5  !1
  do Value::tmp:AdjustmentCost6  !1
  do Prompt::jobe:ExcReplcamentCharge
  do Value::jobe:ExcReplcamentCharge  !1
  do Prompt::jobe:ARC3rdPartyInvoiceNumber
  do Value::jobe:ARC3rdPartyInvoiceNumber  !1
  do Prompt::tmp:ARCIgnoreDefaultCharges
  do Value::tmp:ARCIgnoreDefaultCharges  !1
  do Prompt::tmp:ARCInvoiceNumber
  do Value::tmp:ARCInvoiceNumber  !1

Value::tmp:ARCViewCostType  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCViewCostType') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:ARCViewCostType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCViewCostType'',''viewcosts_tmp:arcviewcosttype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ARCViewCostType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:ARCViewCostType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:ARCViewCostType') = 0
    p_web.SetSessionValue('tmp:ARCViewCostType','Chargeable')
  end
  If p_web.GSV('Hide:ARCChargeable') <> 1
    packet = clip(packet) & p_web.CreateOption('Chargeable','Chargeable',choose('Chargeable' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCWarranty') <> 1
    packet = clip(packet) & p_web.CreateOption('Warranty','Warranty',choose('Warranty' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCClaim') <> 1
    packet = clip(packet) & p_web.CreateOption('Claim','Claim',choose('Claim' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARC3rdParty') <> 1
    packet = clip(packet) & p_web.CreateOption('3rd Party','3rd Party',choose('3rd Party' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCEstimate') <> 1
    packet = clip(packet) & p_web.CreateOption('Estimate','Estimate',choose('Estimate' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCCInvoice') <> 1
    packet = clip(packet) & p_web.CreateOption('Chargeable - Invoiced','Chargeable - Invoiced',choose('Chargeable - Invoiced' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCWInvoice') <> 1
    packet = clip(packet) & p_web.CreateOption('Warranty - Invoiced','Warranty - Invoiced',choose('Warranty - Invoiced' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ARCClaimInvoice') <> 1
    packet = clip(packet) & p_web.CreateOption('Claim - Invoiced','Claim - Invoiced',choose('Claim - Invoiced' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:ManufacturerPaid') <> 1
    packet = clip(packet) & p_web.CreateOption('Manufacturer Payment','Manufacturer Payment',choose('Manufacturer Payment' = p_web.getsessionvalue('tmp:ARCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCViewCostType') & '_value')

Comment::tmp:ARCViewCostType  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCViewCostType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::__line1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__line1',p_web.GetValue('NewValue'))
    do Value::__line1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__line1  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('__line1') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::__line1  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('__line1') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ARCIgnoreDefaultCharges  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreDefaultCharges') & '_prompt',Choose(p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Ignore Default Charges')
  If p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreDefaultCharges') & '_prompt')

Validate::tmp:ARCIgnoreDefaultCharges  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCIgnoreDefaultCharges',p_web.GetValue('NewValue'))
    tmp:ARCIgnoreDefaultCharges = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCIgnoreDefaultCharges
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:ARCIgnoreDefaultCharges',p_web.GetValue('Value'))
    tmp:ARCIgnoreDefaultCharges = p_web.GetValue('Value')
  End
  IF (p_web.GSV('tmp:ARCIgnoreDefaultCharges') = 1)
      p_web.SSV('ValidateIgnoreTickBoxARC',1)
      p_web.SSV('tmp:ARCIgnoreReason','')
  ELSE  ! IF (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
      p_web.SSV('ReadOnly:ARCCost2',1)
      case p_web.GSV('tmp:ARCViewCostType')
      of 'Chargeable'
          p_web.SSV('job:Ignore_Chargeable_Charges','NO')
      of 'Warranty'
          p_web.SSV('job:Ignore_Warranty_Charges','NO')
      of 'Estimate'
          p_web.SSV('job:Ignore_Estimate_Charges','NO')
      END ! case p_web.GSV('tmp:ARCViewCostType')
      do pricingRoutine
  END ! IF (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
  do Value::tmp:ARCIgnoreDefaultCharges
  do SendAlert
  do Value::buttonAcceptARCReason  !1
  do Value::buttonCancelARCReason  !1
  do Value::tmp:ARCViewCostType  !1
  do Prompt::tmp:ARCIgnoreReason
  do Value::tmp:ARCIgnoreReason  !1
  do Value::tmp:ARCCost2  !1
  do Value::tmp:ARCCost3  !1
  do Value::tmp:ARCCost4  !1
  do Value::tmp:ARCCost5  !1
  do Value::tmp:ARCCost6  !1
  do Value::tmp:ARCCost7  !1
  do Value::tmp:ARCCost8  !1

Value::tmp:ARCIgnoreDefaultCharges  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreDefaultCharges') & '_value',Choose(p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '')
  ! --- CHECKBOX --- tmp:ARCIgnoreDefaultCharges
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:ARCIgnoreDefaultCharges'',''viewcosts_tmp:arcignoredefaultcharges_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ARCIgnoreDefaultCharges')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC' Or Instring('- Invoiced',p_web.GSV('tmp:ARCViewCostType'),1,1) or p_web.GSV('ValidateIgnoreTickBoxARC') = 1,'disabled','')
  If p_web.GetSessionValue('tmp:ARCIgnoreDefaultCharges') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:ARCIgnoreDefaultCharges',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreDefaultCharges') & '_value')

Comment::tmp:ARCIgnoreDefaultCharges  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreDefaultCharges') & '_comment',Choose(p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ARCIgnoreReason  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreReason') & '_prompt',Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Enter Reason For Ignoring Standard Charges')
  If p_web.GSV('ValidateIgnoreTickBoxARC') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreReason') & '_prompt')

Validate::tmp:ARCIgnoreReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCIgnoreReason',p_web.GetValue('NewValue'))
    tmp:ARCIgnoreReason = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCIgnoreReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ARCIgnoreReason',p_web.GetValue('Value'))
    tmp:ARCIgnoreReason = p_web.GetValue('Value')
  End
  p_web.SSV('tmp:ARCIgnoreReason',BHStripReplace(p_web.GSV('tmp:ARCIgnoreReason'),'<9>',''))
  do Value::tmp:ARCIgnoreReason
  do SendAlert

Value::tmp:ARCIgnoreReason  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreReason') & '_value',Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBoxARC') <> 1)
  ! --- TEXT --- tmp:ARCIgnoreReason
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:ARCIgnoreReason')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCIgnoreReason'',''viewcosts_tmp:arcignorereason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ARCIgnoreReason')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('tmp:ARCIgnoreReason',p_web.GetSessionValue('tmp:ARCIgnoreReason'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(tmp:ARCIgnoreReason),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreReason') & '_value')

Comment::tmp:ARCIgnoreReason  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCIgnoreReason') & '_comment',Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('ValidateIgnoreTickBoxARC') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonAcceptARCReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonAcceptARCReason',p_web.GetValue('NewValue'))
    do Value::buttonAcceptARCReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  IF (p_web.GSV('tmp:ARCIgnoreReason') <> '')
      p_web.SSV('ValidateIgnoreTickBoxARC',0)
      case p_web.GSV('tmp:ARCViewCostType')
      of 'Chargeable'
          local.AddToTempQueue('IGNORE DEFAULT ARC CHARGEABLE COSTS',|
                          p_web.GSV('job:Ignore_Chargeable_Charges'), |
                          p_web.GSV('tmp:ARCIgnoreReason'),1,|
                          p_web.GSV('tmp:ARCCost2'))
          p_web.SSV(job:Ignore_Chargeable_Charges,'YES')
      of 'Warranty'
          local.AddToTempQueue('IGNORE DEFAULT ARC WARRANTY COSTS',|
                          p_web.GSV('job:Ignore_Warranty_Charges'),|
                          p_web.GSV('tmp:ARCIgnoreReason'),1,|
                          p_web.GSV('tmp:ARCCost2'))
          p_web.SSV('job:Ignore_Warranty_Charges','YES')
      of 'Estimate'
          local.AddToTempQueue('IGNORE DEFAULT RRC ESTIMATE COSTS',|
                          p_web.GSV('job:Ignore_Estimate_Charges'),|
                          p_web.GSV('tmp:ARCIgnoreReason'),1,|
                          p_web.GSV('tmp:ARCCost2'))
          p_web.SSV('job:Ignore_Estimate_Charges','YES')
      END ! case p_web.GSV('tmp:ARCViewCostType')
      p_web.SSV('ReadOnly:ARCCost2',0)
  ELSE ! IF (p_web.GSV('tmp:RRCIgnoreReason') <> '')
      p_web.SSV('ReadOnly:ARCCost2',1)
      case p_web.GSV('tmp:ARCViewCostType')
      of 'Chargeable'
          p_web.SSV('job:Ignore_Chargeable_Charges','YES')
      of 'Warranty'
          p_web.SSV('job:Ignore_Warranty_Charges','YES')
      of 'Estimate'
          p_web.SSV('job:Ignore_Estimate_Charges','YES')
      END ! case p_web.GSV('tmp:ARCViewCostType')
      p_web.SSV('tmp:ARCIgnoreDefaultCharges',0)
  END ! IF (p_web.GSV('tmp:RRCIgnoreReason') <> '')
  do Value::buttonAcceptARCReason
  do SendAlert
  do Value::buttonCancelARCReason  !1
  do Prompt::tmp:ARCIgnoreDefaultCharges
  do Value::tmp:ARCIgnoreDefaultCharges  !1
  do Prompt::tmp:ARCIgnoreReason
  do Value::tmp:ARCIgnoreReason  !1
  do Prompt::tmp:ARCCost2
  do Value::tmp:ARCCost2  !1
  do Value::tmp:ARCViewCostType  !1

Value::buttonAcceptARCReason  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('buttonAcceptARCReason') & '_value',Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBoxARC') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonAcceptARCReason'',''viewcosts_buttonacceptarcreason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AcceptARCReason','Accept','SmallButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('buttonAcceptARCReason') & '_value')

Comment::buttonAcceptARCReason  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('buttonAcceptARCReason') & '_comment',Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('ValidateIgnoreTickBoxARC') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonCancelARCReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonCancelARCReason',p_web.GetValue('NewValue'))
    do Value::buttonCancelARCReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  p_web.SSV('ValidateIgnoreTickBoxARC',0)
  p_web.SSV('ReadOnly:ARCCost2',1)
  case p_web.GSV('tmp:ARCViewCostType')
  of 'Chargeable'
      p_web.SSV('job:Ignore_Chargeable_Charges','NO')
  of 'Warranty'
      p_web.SSV('job:Ignore_Warranty_Charges','NO')
  of 'Estimate'
      p_web.SSV('job:Ignore_Warranty_Charges','NO')
  END ! case p_web.GSV('tmp:ARCViewCostType')
  p_web.SSV('tmp:ARCIgnoreDefaultCharges',0)
  do Value::buttonCancelARCReason
  do SendAlert
  do Value::buttonAcceptARCReason  !1
  do Prompt::tmp:ARCIgnoreDefaultCharges
  do Value::tmp:ARCIgnoreDefaultCharges  !1
  do Prompt::tmp:ARCIgnoreReason
  do Value::tmp:ARCIgnoreReason  !1
  do Prompt::tmp:ARCCost2
  do Value::tmp:ARCCost2  !1
  do Prompt::tmp:ARCViewCostType
  do Value::tmp:ARCViewCostType  !1

Value::buttonCancelARCReason  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('buttonCancelARCReason') & '_value',Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBoxARC') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonCancelARCReason'',''viewcosts_buttoncancelarcreason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CancelARCReason','Cancel','SmallButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('buttonCancelARCReason') & '_value')

Comment::buttonCancelARCReason  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('buttonCancelARCReason') & '_comment',Choose(p_web.GSV('ValidateIgnoreTickBoxARC') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('ValidateIgnoreTickBoxARC') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ARCCost1  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost1') & '_prompt',Choose(p_web.GSV('Prompt:Cost1') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:Cost1'))
  If p_web.GSV('Prompt:Cost1') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost1') & '_prompt')

Validate::tmp:ARCCost1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCCost1',p_web.GetValue('NewValue'))
    tmp:ARCCost1 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCCost1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ARCCost1',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:ARCCost1 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:ARCCost1
  do SendAlert

Value::tmp:ARCCost1  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost1') & '_value',Choose(p_web.GSV('Prompt:Cost1') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost1') = '')
  ! --- STRING --- tmp:ARCCost1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost1') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost1') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:ARCCost1')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost1'',''viewcosts_tmp:arccost1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost1',p_web.GetSessionValue('tmp:ARCCost1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost1') & '_value')

Comment::tmp:ARCCost1  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost1') & '_comment',Choose(p_web.GSV('Prompt:Cost1') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:Cost1') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost1') & '_comment')

Validate::tmp:AdjustmentCost1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:AdjustmentCost1',p_web.GetValue('NewValue'))
    tmp:AdjustmentCost1 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:AdjustmentCost1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:AdjustmentCost1',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:AdjustmentCost1 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:AdjustmentCost1
  do SendAlert

Value::tmp:AdjustmentCost1  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost1') & '_value',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:AdjustmentCost1')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost1'',''viewcosts_tmp:adjustmentcost1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost1',p_web.GetSessionValue('tmp:AdjustmentCost1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost1') & '_value')

Comment::tmp:AdjustmentCost1  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost1') & '_comment',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ARCCost2  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost2') & '_prompt',Choose(p_web.GSV('Prompt:Cost2') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:Cost2'))
  If p_web.GSV('Prompt:Cost2') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost2') & '_prompt')

Validate::tmp:ARCCost2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCCost2',p_web.GetValue('NewValue'))
    tmp:ARCCost2 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCCost2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ARCCost2',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:ARCCost2 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:ARCCost2
  do SendAlert

Value::tmp:ARCCost2  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost2') & '_value',Choose(p_web.GSV('Prompt:Cost2') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost2') = '')
  ! --- STRING --- tmp:ARCCost2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost2') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost2') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:ARCCost2')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost2'',''viewcosts_tmp:arccost2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost2',p_web.GetSessionValue('tmp:ARCCost2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost2') & '_value')

Comment::tmp:ARCCost2  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost2') & '_comment',Choose(p_web.GSV('Prompt:Cost2') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:Cost2') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost2') & '_comment')

Validate::tmp:AdjustmentCost2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:AdjustmentCost2',p_web.GetValue('NewValue'))
    tmp:AdjustmentCost2 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:AdjustmentCost2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:AdjustmentCost2',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:AdjustmentCost2 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:AdjustmentCost2
  do SendAlert

Value::tmp:AdjustmentCost2  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost2') & '_value',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:AdjustmentCost2')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost2'',''viewcosts_tmp:adjustmentcost2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost2',p_web.GetSessionValue('tmp:AdjustmentCost2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost2') & '_value')

Comment::tmp:AdjustmentCost2  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost2') & '_comment',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ARCCost3  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost3') & '_prompt',Choose(p_web.GSV('Prompt:Cost3') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:Cost3'))
  If p_web.GSV('Prompt:Cost3') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost3') & '_prompt')

Validate::tmp:ARCCost3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCCost3',p_web.GetValue('NewValue'))
    tmp:ARCCost3 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCCost3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ARCCost3',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:ARCCost3 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:ARCCost3
  do SendAlert

Value::tmp:ARCCost3  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost3') & '_value',Choose(p_web.GSV('Prompt:Cost3') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost3') = '')
  ! --- STRING --- tmp:ARCCost3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost3') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('BookingSite') = 'RRC' Or p_web.GSV('ReadOnly:ARCCost3') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:ARCCost3')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost3'',''viewcosts_tmp:arccost3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost3',p_web.GetSessionValue('tmp:ARCCost3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost3') & '_value')

Comment::tmp:ARCCost3  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost3') & '_comment',Choose(p_web.GSV('Prompt:Cost3') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:Cost3') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost3') & '_comment')

Validate::tmp:AdjustmentCost3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:AdjustmentCost3',p_web.GetValue('NewValue'))
    tmp:AdjustmentCost3 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:AdjustmentCost3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:AdjustmentCost3',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:AdjustmentCost3 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:AdjustmentCost3
  do SendAlert

Value::tmp:AdjustmentCost3  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost3') & '_value',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:AdjustmentCost3')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost3'',''viewcosts_tmp:adjustmentcost3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost3',p_web.GetSessionValue('tmp:AdjustmentCost3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost3') & '_value')

Comment::tmp:AdjustmentCost3  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost3') & '_comment',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::__line2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__line2',p_web.GetValue('NewValue'))
    do Value::__line2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__line2  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('__line2') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::__line2  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('__line2') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ARCCost4  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost4') & '_prompt',Choose(p_web.GSV('Prompt:Cost4') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:Cost4'))
  If p_web.GSV('Prompt:Cost4') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost4') & '_prompt')

Validate::tmp:ARCCost4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCCost4',p_web.GetValue('NewValue'))
    tmp:ARCCost4 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCCost4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ARCCost4',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:ARCCost4 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:ARCCost4
  do SendAlert

Value::tmp:ARCCost4  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost4') & '_value',Choose(p_web.GSV('Prompt:Cost4') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost4') = '')
  ! --- STRING --- tmp:ARCCost4
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:ARCCost4')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost4'',''viewcosts_tmp:arccost4_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost4',p_web.GetSessionValue('tmp:ARCCost4'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost4') & '_value')

Comment::tmp:ARCCost4  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost4') & '_comment',Choose(p_web.GSV('Prompt:Cost4') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:Cost4') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost4') & '_comment')

Validate::tmp:AdjustmentCost4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:AdjustmentCost4',p_web.GetValue('NewValue'))
    tmp:AdjustmentCost4 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:AdjustmentCost4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:AdjustmentCost4',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:AdjustmentCost4 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:AdjustmentCost4
  do SendAlert

Value::tmp:AdjustmentCost4  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost4') & '_value',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost4
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:AdjustmentCost4')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost4'',''viewcosts_tmp:adjustmentcost4_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost4',p_web.GetSessionValue('tmp:AdjustmentCost4'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost4') & '_value')

Comment::tmp:AdjustmentCost4  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost4') & '_comment',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ARCCost5  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost5') & '_prompt',Choose(p_web.GSV('Prompt:Cost5') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:Cost5'))
  If p_web.GSV('Prompt:Cost5') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost5') & '_prompt')

Validate::tmp:ARCCost5  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCCost5',p_web.GetValue('NewValue'))
    tmp:ARCCost5 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCCost5
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ARCCost5',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:ARCCost5 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:ARCCost5
  do SendAlert

Value::tmp:ARCCost5  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost5') & '_value',Choose(p_web.GSV('Prompt:Cost5') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost5') = '')
  ! --- STRING --- tmp:ARCCost5
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:ARCCost5')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost5'',''viewcosts_tmp:arccost5_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost5',p_web.GetSessionValue('tmp:ARCCost5'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost5') & '_value')

Comment::tmp:ARCCost5  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost5') & '_comment',Choose(p_web.GSV('Prompt:Cost5') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:Cost5') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost5') & '_comment')

Validate::tmp:AdjustmentCost5  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:AdjustmentCost5',p_web.GetValue('NewValue'))
    tmp:AdjustmentCost5 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:AdjustmentCost5
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:AdjustmentCost5',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:AdjustmentCost5 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:AdjustmentCost5
  do SendAlert

Value::tmp:AdjustmentCost5  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost5') & '_value',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost5
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:AdjustmentCost5')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost5'',''viewcosts_tmp:adjustmentcost5_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost5',p_web.GetSessionValue('tmp:AdjustmentCost5'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost5') & '_value')

Comment::tmp:AdjustmentCost5  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost5') & '_comment',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ARCCost6  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost6') & '_prompt',Choose(p_web.GSV('Prompt:Cost6') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:Cost6'))
  If p_web.GSV('Prompt:Cost6') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost6') & '_prompt')

Validate::tmp:ARCCost6  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCCost6',p_web.GetValue('NewValue'))
    tmp:ARCCost6 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCCost6
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ARCCost6',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:ARCCost6 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:ARCCost6
  do SendAlert

Value::tmp:ARCCost6  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost6') & '_value',Choose(p_web.GSV('Prompt:Cost6') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost6') = '')
  ! --- STRING --- tmp:ARCCost6
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:ARCCost6')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost6'',''viewcosts_tmp:arccost6_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost6',p_web.GetSessionValue('tmp:ARCCost6'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost6') & '_value')

Comment::tmp:ARCCost6  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost6') & '_comment',Choose(p_web.GSV('Prompt:Cost6') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:Cost6') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost6') & '_comment')

Validate::tmp:AdjustmentCost6  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:AdjustmentCost6',p_web.GetValue('NewValue'))
    tmp:AdjustmentCost6 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:AdjustmentCost6
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:AdjustmentCost6',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:AdjustmentCost6 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:AdjustmentCost6
  do SendAlert

Value::tmp:AdjustmentCost6  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost6') & '_value',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced')
  ! --- STRING --- tmp:AdjustmentCost6
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:AdjustmentCost6')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:AdjustmentCost6'',''viewcosts_tmp:adjustmentcost6_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:AdjustmentCost6',p_web.GetSessionValue('tmp:AdjustmentCost6'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost6') & '_value')

Comment::tmp:AdjustmentCost6  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:AdjustmentCost6') & '_comment',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:ARCViewCostType') <> 'Claim - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ARCCost7  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost7') & '_prompt',Choose(p_web.GSV('Prompt:Cost7') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:Cost7'))
  If p_web.GSV('Prompt:Cost7') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost7') & '_prompt')

Validate::tmp:ARCCost7  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCCost7',p_web.GetValue('NewValue'))
    tmp:ARCCost7 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCCost7
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ARCCost7',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:ARCCost7 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:ARCCost7
  do SendAlert

Value::tmp:ARCCost7  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost7') & '_value',Choose(p_web.GSV('Prompt:Cost7') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost7') = '')
  ! --- STRING --- tmp:ARCCost7
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC','readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:ARCCost7')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost7'',''viewcosts_tmp:arccost7_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost7',p_web.GetSessionValue('tmp:ARCCost7'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost7') & '_value')

Comment::tmp:ARCCost7  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost7') & '_comment',Choose(p_web.GSV('Prompt:Cost7') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:Cost7') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost7') & '_comment')

Prompt::tmp:ARCCost8  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost8') & '_prompt',Choose(p_web.GSV('Prompt:Cost8') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:Cost8'))
  If p_web.GSV('Prompt:Cost8') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost8') & '_prompt')

Validate::tmp:ARCCost8  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCCost8',p_web.GetValue('NewValue'))
    tmp:ARCCost8 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCCost8
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ARCCost8',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:ARCCost8 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:ARCCost8
  do SendAlert

Value::tmp:ARCCost8  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost8') & '_value',Choose(p_web.GSV('Prompt:Cost8') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:Cost8') = '')
  ! --- STRING --- tmp:ARCCost8
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('BookingSite') = 'RRC','readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:ARCCost8')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ARCCost8'',''viewcosts_tmp:arccost8_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ARCCost8',p_web.GetSessionValue('tmp:ARCCost8'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost8') & '_value')

Comment::tmp:ARCCost8  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCCost8') & '_comment',Choose(p_web.GSV('Prompt:Cost8') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:Cost8') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCCost8') & '_comment')

Prompt::jobe:ARC3rdPartyInvoiceNumber  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('jobe:ARC3rdPartyInvoiceNumber') & '_prompt',Choose(p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Invoice Number')
  If p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('jobe:ARC3rdPartyInvoiceNumber') & '_prompt')

Validate::jobe:ARC3rdPartyInvoiceNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:ARC3rdPartyInvoiceNumber',p_web.GetValue('NewValue'))
    jobe:ARC3rdPartyInvoiceNumber = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe:ARC3rdPartyInvoiceNumber
    do Value::jobe:ARC3rdPartyInvoiceNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:ARC3rdPartyInvoiceNumber',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe:ARC3rdPartyInvoiceNumber = p_web.GetValue('Value')
  End

Value::jobe:ARC3rdPartyInvoiceNumber  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('jobe:ARC3rdPartyInvoiceNumber') & '_value',Choose(p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party')
  ! --- DISPLAY --- jobe:ARC3rdPartyInvoiceNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('jobe:ARC3rdPartyInvoiceNumber'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('jobe:ARC3rdPartyInvoiceNumber') & '_value')

Comment::jobe:ARC3rdPartyInvoiceNumber  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('jobe:ARC3rdPartyInvoiceNumber') & '_comment',Choose(p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('jobe:ARC3rdPartyInvoiceNumber') = 0 Or p_web.GSV('tmp:ARCViewCostType') <> '3rd Party'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ARCInvoiceNumber  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCInvoiceNumber') & '_prompt',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Chargeable - Invoiced' AND p_web.GSV('tmp:ARCViewCostType') <> 'Warranty - Invoiced','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Invoice Number')
  If p_web.GSV('tmp:ARCViewCostType') <> 'Chargeable - Invoiced' AND p_web.GSV('tmp:ARCViewCostType') <> 'Warranty - Invoiced'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCInvoiceNumber') & '_prompt')

Validate::tmp:ARCInvoiceNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ARCInvoiceNumber',p_web.GetValue('NewValue'))
    tmp:ARCInvoiceNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ARCInvoiceNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ARCInvoiceNumber',p_web.GetValue('Value'))
    tmp:ARCInvoiceNumber = p_web.GetValue('Value')
  End

Value::tmp:ARCInvoiceNumber  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCInvoiceNumber') & '_value',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Chargeable - Invoiced' AND p_web.GSV('tmp:ARCViewCostType') <> 'Warranty - Invoiced','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:ARCViewCostType') <> 'Chargeable - Invoiced' AND p_web.GSV('tmp:ARCViewCostType') <> 'Warranty - Invoiced')
  ! --- DISPLAY --- tmp:ARCInvoiceNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:ARCInvoiceNumber'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:ARCInvoiceNumber') & '_value')

Comment::tmp:ARCInvoiceNumber  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:ARCInvoiceNumber') & '_comment',Choose(p_web.GSV('tmp:ARCViewCostType') <> 'Chargeable - Invoiced' AND p_web.GSV('tmp:ARCViewCostType') <> 'Warranty - Invoiced','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:ARCViewCostType') <> 'Chargeable - Invoiced' AND p_web.GSV('tmp:ARCViewCostType') <> 'Warranty - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:RRCViewCostType  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCViewCostType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('View Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:RRCViewCostType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCViewCostType',p_web.GetValue('NewValue'))
    tmp:RRCViewCostType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCViewCostType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCViewCostType',p_web.GetValue('Value'))
    tmp:RRCViewCostType = p_web.GetValue('Value')
  End
  do displayRRCCostFields
  do displayOtherFields
  do Value::tmp:RRCViewCostType
  do SendAlert
  do Prompt::tmp:RRCCost0
  do Value::tmp:RRCCost0  !1
  do Prompt::tmp:RRCCost1
  do Value::tmp:RRCCost1  !1
  do Comment::tmp:RRCCost1
  do Prompt::tmp:RRCCost2
  do Value::tmp:RRCCost2  !1
  do Comment::tmp:RRCCost2
  do Prompt::tmp:RRCCost3
  do Value::tmp:RRCCost3  !1
  do Comment::tmp:RRCCost3
  do Prompt::tmp:RRCCost4
  do Value::tmp:RRCCost4  !1
  do Comment::tmp:RRCCost4
  do Prompt::tmp:RRCCost5
  do Value::tmp:RRCCost5  !1
  do Comment::tmp:RRCCost5
  do Prompt::tmp:RRCCost6
  do Value::tmp:RRCCost6  !1
  do Comment::tmp:RRCCost6
  do Prompt::tmp:RRCCost7
  do Value::tmp:RRCCost7  !1
  do Comment::tmp:RRCCost7
  do Prompt::tmp:RRCCost8
  do Value::tmp:RRCCost8  !1
  do Comment::tmp:RRCCost8
  do Prompt::jobe:ExcReplcamentCharge
  do Value::jobe:ExcReplcamentCharge  !1
  do Prompt::tmp:RRCIgnoreDefaultCharges
  do Value::tmp:RRCIgnoreDefaultCharges  !1
  do Prompt::tmp:RRCInvoiceNumber
  do Value::tmp:RRCInvoiceNumber  !1
  do Prompt::BrowseJobCredits
  do Value::BrowseJobCredits  !1
  do Prompt::tmp:originalInvoice
  do Value::tmp:originalInvoice  !1

Value::tmp:RRCViewCostType  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCViewCostType') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:RRCViewCostType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCViewCostType'',''viewcosts_tmp:rrcviewcosttype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RRCViewCostType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ValidateIgnoreTickBox') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:RRCViewCostType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:RRCViewCostType') = 0
    p_web.SetSessionValue('tmp:RRCViewCostType','')
  end
    packet = clip(packet) & p_web.CreateOption('','',choose('' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCChargeable') <> 1
    packet = clip(packet) & p_web.CreateOption('Chargeable','Chargeable',choose('Chargeable' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCWarranty') <> 1
    packet = clip(packet) & p_web.CreateOption('Warranty','Warranty',choose('Warranty' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCHandling') <> 1
    packet = clip(packet) & p_web.CreateOption('Handling','Handling',choose('Handling' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCExchange') <> 1
    packet = clip(packet) & p_web.CreateOption('Exchange','Exchange',choose('Exchange' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCEstimate') <> 1
    packet = clip(packet) & p_web.CreateOption('Estimate','Estimate',choose('Estimate' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCCInvoice') <> 1
    packet = clip(packet) & p_web.CreateOption('Chargeable - Invoiced','Chargeable - Invoiced',choose('Chargeable - Invoiced' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:RRCWInvoice') <> 1
    packet = clip(packet) & p_web.CreateOption('Warranty - Invoiced','Warranty - Invoiced',choose('Warranty - Invoiced' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  If p_web.GSV('Hide:Credit') <> 1
    packet = clip(packet) & p_web.CreateOption('Credits','Credits',choose('Credits' = p_web.getsessionvalue('tmp:RRCViewCostType')),clip(loc:rowstyle),,)&CRLF
  End
  loc:even = Choose(loc:even=1,2,1)
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCViewCostType') & '_value')

Comment::tmp:RRCViewCostType  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCViewCostType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::__line3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__line3',p_web.GetValue('NewValue'))
    do Value::__line3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__line3  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('__line3') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::__line3  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('__line3') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:RRCIgnoreDefaultCharges  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreDefaultCharges') & '_prompt',Choose(p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Ignore Default Charges')
  If p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreDefaultCharges') & '_prompt')

Validate::tmp:RRCIgnoreDefaultCharges  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCIgnoreDefaultCharges',p_web.GetValue('NewValue'))
    tmp:RRCIgnoreDefaultCharges = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCIgnoreDefaultCharges
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:RRCIgnoreDefaultCharges',p_web.GetValue('Value'))
    tmp:RRCIgnoreDefaultCharges = p_web.GetValue('Value')
  End
  IF (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
      p_web.SSV('tmp:RRCIgnoreReason','')
      p_web.SSV('ValidateIgnoreTickBox',1)
  ELSE  ! IF (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
      p_web.SSV('ReadOnly:RRCCost2',1)
      case p_web.GSV('tmp:RRCViewCostType')
      of 'Chargeable'
          p_web.SSV('jobe:IgnoreRRCChaCosts',0)
      of 'Warranty'
          p_web.SSV('jobe:IgnoreRRCWarCosts',0)
      of 'Estimate'
          p_web.SSV('jobe:IgnoreRRCEstCosts',0)
      END ! case p_web.GSV('tmp:ARCViewCostType')
      p_web.SSV('jobe2:JobDiscountAmnt',0)
      do pricingRoutine
      do displayRRCCostFields
  END ! IF (p_web.GSV('tmp:RRCIgnoreDefaultCharges') = 1)
  do Value::tmp:RRCIgnoreDefaultCharges
  do SendAlert
  do Prompt::tmp:RRCIgnoreReason
  do Value::tmp:RRCIgnoreReason  !1
  do Value::buttonAcceptRRCReason  !1
  do Value::buttonCancelRRCReason  !1
  do Value::tmp:RRCViewCostType  !1
  do Value::tmp:RRCCost2  !1
  do Value::tmp:RRCCost3  !1
  do Value::tmp:RRCCost4  !1
  do Value::tmp:RRCCost5  !1
  do Value::tmp:RRCCost6  !1
  do Value::tmp:RRCCost7  !1
  do Value::tmp:RRCCost8  !1
  do Prompt::tmp:RRCCost0
  do Value::tmp:RRCCost0  !1
  do Comment::tmp:RRCCost0

Value::tmp:RRCIgnoreDefaultCharges  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreDefaultCharges') & '_value',Choose(p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '')
  ! --- CHECKBOX --- tmp:RRCIgnoreDefaultCharges
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:RRCIgnoreDefaultCharges'',''viewcosts_tmp:rrcignoredefaultcharges_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon(tmp:RRCIgnoreReason)&''',2);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ValidateIgnoreTickBox') = 1 Or Instring('- Invoiced',p_web.GSV('tmp:RRCViewCostType'),1,1),'disabled','')
  If p_web.GetSessionValue('tmp:RRCIgnoreDefaultCharges') = true
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:RRCIgnoreDefaultCharges',clip(true),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreDefaultCharges') & '_value')

Comment::tmp:RRCIgnoreDefaultCharges  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreDefaultCharges') & '_comment',Choose(p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:RRCIgnoreReason  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreReason') & '_prompt',Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Enter Reason For Ignoring Standard Charges')
  If p_web.GSV('ValidateIgnoreTickBox') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreReason') & '_prompt')

Validate::tmp:RRCIgnoreReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCIgnoreReason',p_web.GetValue('NewValue'))
    tmp:RRCIgnoreReason = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCIgnoreReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCIgnoreReason',p_web.GetValue('Value'))
    tmp:RRCIgnoreReason = p_web.GetValue('Value')
  End
  If tmp:RRCIgnoreReason = ''
    loc:Invalid = 'tmp:RRCIgnoreReason'
    loc:alert = p_web.translate('Enter Reason For Ignoring Standard Charges') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:RRCIgnoreReason = Upper(tmp:RRCIgnoreReason)
    p_web.SetSessionValue('tmp:RRCIgnoreReason',tmp:RRCIgnoreReason)
  p_web.SSV('tmp:RRCIgnoreReason',BHStripReplace(p_web.GSV('tmp:RRCIgnoreReason'),'<9>',''))
  do Value::tmp:RRCIgnoreReason
  do SendAlert

Value::tmp:RRCIgnoreReason  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreReason') & '_value',Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBox') <> 1)
  ! --- TEXT --- tmp:RRCIgnoreReason
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:RRCIgnoreReason')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:RRCIgnoreReason = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCIgnoreReason'',''viewcosts_tmp:rrcignorereason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RRCIgnoreReason')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('tmp:RRCIgnoreReason',p_web.GetSessionValue('tmp:RRCIgnoreReason'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(tmp:RRCIgnoreReason),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreReason') & '_value')

Comment::tmp:RRCIgnoreReason  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCIgnoreReason') & '_comment',Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('ValidateIgnoreTickBox') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonAcceptRRCReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonAcceptRRCReason',p_web.GetValue('NewValue'))
    do Value::buttonAcceptRRCReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  IF (p_web.GSV('tmp:RRCIgnoreReason') <> '')
      p_web.SSV('ValidateIgnoreTickBox',0)
      case p_web.GSV('tmp:RRCViewCostType')
      of 'Chargeable'
          local.AddToTempQueue('IGNORE DEFAULT RRC CHARGEABLE COSTS',|
                          p_web.GSV('jobe:IgnoreRRCChaCosts'), |
                          p_web.GSV('tmp:RRCIgnoreReason'), |
                          1,|
                          p_web.GSV('tmp:RRCCost2'))
          p_web.SSV('jobe:IgnoreRRCChaCosts',1)
          p_web.SSV('Prompt:RCost0','Discount')
      of 'Warranty'
          local.AddToTempQueue('IGNORE DEFAULT RRC WARRANTY COSTS',|
                          p_web.GSV('jobe:IgnoreRRCWarCosts'),|
                          p_web.GSV('tmp:RRCIgnoreReason'), |
                          1,|
                          p_web.GSV('tmp:RRCCost2'))
          p_web.SSV('jobe:IgnoreRRCWarCosts',1)
      of 'Estimate'
          local.AddToTempQueue('IGNORE DEFAULT RRC ESTIMATE COSTS',|
                          p_web.GSV('jobe:IgnoreRRCEstCosts'),|
                          p_web.GSV('tmp:RRCIgnoreReason'),|
                          1,|
                          p_web.GSV('tmp:RRCCost2'))
          p_web.SSV('jobe:IgnoreRRCEstCosts',1)
      END ! case p_web.GSV('tmp:ARCViewCostType')
      p_web.SSV('ReadOnly:RRCCost2',0)
  ELSE ! IF (p_web.GSV('tmp:RRCIgnoreReason') <> '')
      p_web.SSV('ReadOnly:RRCCost2',1)
      case p_web.GSV('tmp:RRCViewCostType')
      of 'Chargeable'
          p_web.SSV('jobe:IgnoreRRCChaCosts',0)
      of 'Warranty'
          p_web.SSV('jobe:IgnoreRRCWarCosts',0)
      of 'Estimate'
          p_web.SSV('jobe:IgnoreRRCEstCosts',0)
      END ! case p_web.GSV('tmp:ARCViewCostType')
      p_web.SSV('tmp:RRCIgnoreDefaultCharges',0)
      p_web.SSV('Prompt:RCost0','')
  END ! IF (p_web.GSV('tmp:RRCIgnoreReason') <> '')
  do Value::buttonAcceptRRCReason
  do SendAlert
  do Value::buttonCancelRRCReason  !1
  do Value::tmp:RRCIgnoreDefaultCharges  !1
  do Prompt::tmp:RRCIgnoreReason
  do Value::tmp:RRCIgnoreReason  !1
  do Value::tmp:RRCCost2  !1
  do Value::tmp:RRCViewCostType  !1
  do Prompt::tmp:RRCCost0
  do Value::tmp:RRCCost0  !1

Value::buttonAcceptRRCReason  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('buttonAcceptRRCReason') & '_value',Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBox') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonAcceptRRCReason'',''viewcosts_buttonacceptrrcreason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AcceptRRCReason','Accept','SmallButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('buttonAcceptRRCReason') & '_value')

Comment::buttonAcceptRRCReason  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('buttonAcceptRRCReason') & '_comment',Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('ValidateIgnoreTickBox') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonCancelRRCReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonCancelRRCReason',p_web.GetValue('NewValue'))
    do Value::buttonCancelRRCReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  p_web.SSV('ValidateIgnoreTickBox',0)
  p_web.SSV('ReadOnly:RRCCost2',1)
      case p_web.GSV('tmp:RRCViewCostType')
      of 'Chargeable'
          p_web.SSV('jobe:IgnoreRRCChaCosts',0)
      of 'Warranty'
          p_web.SSV('jobe:IgnoreRRCWarCosts',0)
      of 'Estimate'
          p_web.SSV('jobe:IgnoreRRCEstCosts',0)
      END ! case p_web.GSV('tmp:ARCViewCostType')
  p_web.SSV('tmp:RRCIgnoreDefaultCharges',0)
  do Value::buttonCancelRRCReason
  do SendAlert
  do Value::buttonAcceptRRCReason  !1
  do Prompt::tmp:RRCIgnoreReason
  do Value::tmp:RRCIgnoreReason  !1
  do Value::tmp:RRCViewCostType  !1
  do Value::tmp:RRCIgnoreDefaultCharges  !1
  do Value::tmp:RRCCost2  !1
  do Prompt::tmp:RRCCost0
  do Value::tmp:RRCCost0  !1

Value::buttonCancelRRCReason  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('buttonCancelRRCReason') & '_value',Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('ValidateIgnoreTickBox') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonCancelRRCReason'',''viewcosts_buttoncancelrrcreason_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CancelRRCReason','Cancel','SmallButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('buttonCancelRRCReason') & '_value')

Comment::buttonCancelRRCReason  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('buttonCancelRRCReason') & '_comment',Choose(p_web.GSV('ValidateIgnoreTickBox') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('ValidateIgnoreTickBox') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:RRCCost0  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost0') & '_prompt',Choose(p_web.GSV('Prompt:RCost0') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:RCost0'))
  If p_web.GSV('Prompt:RCost0') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost0') & '_prompt')

Validate::tmp:RRCCost0  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCCost0',p_web.GetValue('NewValue'))
    tmp:RRCCost0 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCCost0
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCCost0',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:RRCCost0 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:RRCCost0
  do SendAlert

Value::tmp:RRCCost0  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost0') & '_value',Choose(p_web.GSV('Prompt:RCost0') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost0') = '')
  ! --- STRING --- tmp:RRCCost0
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:RRCCost0')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost0'',''viewcosts_tmp:rrccost0_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost0',p_web.GetSessionValue('tmp:RRCCost0'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost0') & '_value')

Comment::tmp:RRCCost0  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost0') & '_comment',Choose(p_web.GSV('Prompt:RCost0') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:RCost0') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost0') & '_comment')

Prompt::tmp:RRCCost1  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost1') & '_prompt',Choose(p_web.GSV('Prompt:RCost1') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:RCost1'))
  If p_web.GSV('Prompt:RCost1') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost1') & '_prompt')

Validate::tmp:RRCCost1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCCost1',p_web.GetValue('NewValue'))
    tmp:RRCCost1 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCCost1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCCost1',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:RRCCost1 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:RRCCost1
  do SendAlert

Value::tmp:RRCCost1  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost1') & '_value',Choose(p_web.GSV('Prompt:RCost1') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost1') = '')
  ! --- STRING --- tmp:RRCCost1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:RRCCost1') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:RRCCost1') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:RRCCost1')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost1'',''viewcosts_tmp:rrccost1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost1',p_web.GetSessionValue('tmp:RRCCost1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost1') & '_value')

Comment::tmp:RRCCost1  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost1') & '_comment',Choose(p_web.GSV('Prompt:RCost1') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:RCost1') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost1') & '_comment')

Prompt::tmp:originalInvoice  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:originalInvoice') & '_prompt',Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Original Invoice No')
  If p_web.GSV('tmp:RRCViewCostType') <> 'Credits'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:originalInvoice') & '_prompt')

Validate::tmp:originalInvoice  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:originalInvoice',p_web.GetValue('NewValue'))
    tmp:originalInvoice = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:originalInvoice
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:originalInvoice',p_web.GetValue('Value'))
    tmp:originalInvoice = p_web.GetValue('Value')
  End
  do Value::tmp:originalInvoice
  do SendAlert

Value::tmp:originalInvoice  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:originalInvoice') & '_value',Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:RRCViewCostType') <> 'Credits')
  ! --- STRING --- tmp:originalInvoice
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:originalInvoice')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:originalInvoice'',''viewcosts_tmp:originalinvoice_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:originalInvoice',p_web.GetSessionValueFormat('tmp:originalInvoice'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:originalInvoice') & '_value')

Comment::tmp:originalInvoice  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:originalInvoice') & '_comment',Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:RRCViewCostType') <> 'Credits'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:RRCCost2  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost2') & '_prompt',Choose(p_web.GSV('Prompt:RCost2') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:RCost2'))
  If p_web.GSV('Prompt:RCost2') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost2') & '_prompt')

Validate::tmp:RRCCost2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCCost2',p_web.GetValue('NewValue'))
    tmp:RRCCost2 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCCost2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCCost2',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:RRCCost2 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  IF (p_web.GSV('tmp:RRCViewCostType') = 'Chargeable')
      IF (format(p_web.GSV('tmp:RRCCost2'),@n_14.2) > format(p_web.GSV('DefaultLabourCost'),@n_14.2))
          p_web.SSV('Comment:RCost2','Error! Cost is higher than Default Cost')
          p_web.SSV('Prompt:RCost0','') ! Blank the discount field.
      ELSE
          p_web.SSV('Comment:RCost2','')
      END
  END ! 'tmp:RRCViewCostType'
  do updateRRCCost
  do pricingRoutine
  do displayRRCCostFields
  do Value::tmp:RRCCost2
  do SendAlert
  do Prompt::tmp:RRCCost2
  do Comment::tmp:RRCCost2
  do Value::tmp:RRCCost3  !1
  do Value::tmp:RRCCost4  !1
  do Value::tmp:RRCCost5  !1
  do Value::tmp:RRCCost6  !1
  do Value::tmp:RRCCost7  !1
  do Value::tmp:RRCCost8  !1
  do Prompt::tmp:RRCCost0
  do Value::tmp:RRCCost0  !1
  do Comment::tmp:RRCCost0

Value::tmp:RRCCost2  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost2') & '_value',Choose(p_web.GSV('Prompt:RCost2') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost2') = '')
  ! --- STRING --- tmp:RRCCost2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:RRCCost2') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:RRCCost2') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:RRCCost2')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost2'',''viewcosts_tmp:rrccost2_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:RRCCost2')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost2',p_web.GetSessionValue('tmp:RRCCost2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost2') & '_value')

Comment::tmp:RRCCost2  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:RCost2'))
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost2') & '_comment',Choose(p_web.GSV('Prompt:RCost2') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:RCost2') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost2') & '_comment')

Prompt::tmp:RRCCost3  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost3') & '_prompt',Choose(p_web.GSV('Prompt:RCost3') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:RCost3'))
  If p_web.GSV('Prompt:RCost3') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost3') & '_prompt')

Validate::tmp:RRCCost3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCCost3',p_web.GetValue('NewValue'))
    tmp:RRCCost3 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCCost3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCCost3',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:RRCCost3 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:RRCCost3
  do SendAlert

Value::tmp:RRCCost3  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost3') & '_value',Choose(p_web.GSV('Prompt:RCost3') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost3') = '')
  ! --- STRING --- tmp:RRCCost3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:RRCCost3') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:RRCCost3') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('tmp:RRCCost3')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost3'',''viewcosts_tmp:rrccost3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost3',p_web.GetSessionValue('tmp:RRCCost3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost3') & '_value')

Comment::tmp:RRCCost3  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost3') & '_comment',Choose(p_web.GSV('Prompt:RCost3') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:RCost3') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost3') & '_comment')

Prompt::BrowseJobCredits  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('BrowseJobCredits') & '_prompt',Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits','hdiv','' & clip('FormPrompt fixedwidth-180') & ''))
  loc:prompt = p_web.Translate('Credits/Invoices')
  If p_web.GSV('tmp:RRCViewCostType') <> 'Credits'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('BrowseJobCredits') & '_prompt')

Validate::BrowseJobCredits  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseJobCredits',p_web.GetValue('NewValue'))
    do Value::BrowseJobCredits
  Else
    p_web.StoreValue('jov:RecordNumber')
  End

Value::BrowseJobCredits  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits',1,0))
  ! --- BROWSE ---  BrowseJobCredits --
  p_web.SetValue('BrowseJobCredits:NoForm',1)
  p_web.SetValue('BrowseJobCredits:FormName',loc:formname)
  p_web.SetValue('BrowseJobCredits:parentIs','Form')
  p_web.SetValue('_parentProc','ViewCosts')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('ViewCosts_BrowseJobCredits_embedded_div')&'"><!-- Net:BrowseJobCredits --></div><13,10>'
    p_web._DivHeader('ViewCosts_' & lower('BrowseJobCredits') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('ViewCosts_' & lower('BrowseJobCredits') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseJobCredits --><13,10>'
  end
  do SendPacket

Comment::BrowseJobCredits  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('BrowseJobCredits') & '_comment',Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Credits','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:RRCViewCostType') <> 'Credits'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::__line4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__line4',p_web.GetValue('NewValue'))
    do Value::__line4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__line4  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('__line4') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::__line4  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('__line4') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:RRCCost4  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost4') & '_prompt',Choose(p_web.GSV('Prompt:RCost4') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:RCost4'))
  If p_web.GSV('Prompt:RCost4') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost4') & '_prompt')

Validate::tmp:RRCCost4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCCost4',p_web.GetValue('NewValue'))
    tmp:RRCCost4 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCCost4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCCost4',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:RRCCost4 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:RRCCost4
  do SendAlert

Value::tmp:RRCCost4  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost4') & '_value',Choose(p_web.GSV('Prompt:RCost4') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost4') = '')
  ! --- STRING --- tmp:RRCCost4
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:RRCCost4')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost4'',''viewcosts_tmp:rrccost4_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost4',p_web.GetSessionValue('tmp:RRCCost4'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost4') & '_value')

Comment::tmp:RRCCost4  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost4') & '_comment',Choose(p_web.GSV('Prompt:RCost4') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:RCost4') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost4') & '_comment')

Prompt::tmp:RRCCost5  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost5') & '_prompt',Choose(p_web.GSV('Prompt:RCost5') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:RCost5'))
  If p_web.GSV('Prompt:RCost5') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost5') & '_prompt')

Validate::tmp:RRCCost5  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCCost5',p_web.GetValue('NewValue'))
    tmp:RRCCost5 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCCost5
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCCost5',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:RRCCost5 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:RRCCost5
  do SendAlert

Value::tmp:RRCCost5  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost5') & '_value',Choose(p_web.GSV('Prompt:RCost5') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost5') = '')
  ! --- STRING --- tmp:RRCCost5
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:RRCCost5')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost5'',''viewcosts_tmp:rrccost5_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost5',p_web.GetSessionValue('tmp:RRCCost5'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost5') & '_value')

Comment::tmp:RRCCost5  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost5') & '_comment',Choose(p_web.GSV('Prompt:RCost5') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:RCost5') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost5') & '_comment')

Prompt::tmp:RRCCost6  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost6') & '_prompt',Choose(p_web.GSV('Prompt:RCost6') = '','hdiv','' & clip(p_web.GSV('class:RCost6')) & ''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:RCost6'))
  If p_web.GSV('Prompt:RCost6') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost6') & '_prompt')

Validate::tmp:RRCCost6  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCCost6',p_web.GetValue('NewValue'))
    tmp:RRCCost6 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCCost6
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCCost6',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:RRCCost6 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:RRCCost6
  do SendAlert

Value::tmp:RRCCost6  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost6') & '_value',Choose(p_web.GSV('Prompt:RCost6') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost6') = '')
  ! --- STRING --- tmp:RRCCost6
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:RRCCost6')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost6'',''viewcosts_tmp:rrccost6_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost6',p_web.GetSessionValue('tmp:RRCCost6'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost6') & '_value')

Comment::tmp:RRCCost6  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost6') & '_comment',Choose(p_web.GSV('Prompt:RCost6') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:RCost6') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost6') & '_comment')

Prompt::tmp:RRCCost7  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost7') & '_prompt',Choose(p_web.GSV('Prompt:RCost7') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:RCost7'))
  If p_web.GSV('Prompt:RCost7') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost7') & '_prompt')

Validate::tmp:RRCCost7  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCCost7',p_web.GetValue('NewValue'))
    tmp:RRCCost7 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCCost7
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCCost7',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:RRCCost7 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:RRCCost7
  do SendAlert

Value::tmp:RRCCost7  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost7') & '_value',Choose(p_web.GSV('Prompt:RCost7') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost7') = '')
  ! --- STRING --- tmp:RRCCost7
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:RRCCost7')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost7'',''viewcosts_tmp:rrccost7_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost7',p_web.GetSessionValue('tmp:RRCCost7'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost7') & '_value')

Comment::tmp:RRCCost7  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost7') & '_comment',Choose(p_web.GSV('Prompt:RCost7') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:RCost7') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost7') & '_comment')

Prompt::tmp:RRCCost8  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost8') & '_prompt',Choose(p_web.GSV('Prompt:RCost8') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt:RCost8'))
  If p_web.GSV('Prompt:RCost8') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost8') & '_prompt')

Validate::tmp:RRCCost8  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCCost8',p_web.GetValue('NewValue'))
    tmp:RRCCost8 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCCost8
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCCost8',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    tmp:RRCCost8 = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::tmp:RRCCost8
  do SendAlert

Value::tmp:RRCCost8  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost8') & '_value',Choose(p_web.GSV('Prompt:RCost8') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Prompt:RCost8') = '')
  ! --- STRING --- tmp:RRCCost8
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:RRCCost8')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:RRCCost8'',''viewcosts_tmp:rrccost8_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:RRCCost8',p_web.GetSessionValue('tmp:RRCCost8'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost8') & '_value')

Comment::tmp:RRCCost8  Routine
      loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCCost8') & '_comment',Choose(p_web.GSV('Prompt:RCost8') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Prompt:RCost8') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCCost8') & '_comment')

Prompt::tmp:RRCInvoiceNumber  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCInvoiceNumber') & '_prompt',Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Chargeable - Invoiced' AND p_web.GSV('tmp:RRCViewCostType') <> 'Warranty - Invoiced','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Invoice Number')
  If p_web.GSV('tmp:RRCViewCostType') <> 'Chargeable - Invoiced' AND p_web.GSV('tmp:RRCViewCostType') <> 'Warranty - Invoiced'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCInvoiceNumber') & '_prompt')

Validate::tmp:RRCInvoiceNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:RRCInvoiceNumber',p_web.GetValue('NewValue'))
    tmp:RRCInvoiceNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:RRCInvoiceNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:RRCInvoiceNumber',p_web.GetValue('Value'))
    tmp:RRCInvoiceNumber = p_web.GetValue('Value')
  End

Value::tmp:RRCInvoiceNumber  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCInvoiceNumber') & '_value',Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Chargeable - Invoiced' AND p_web.GSV('tmp:RRCViewCostType') <> 'Warranty - Invoiced','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('tmp:RRCViewCostType') <> 'Chargeable - Invoiced' AND p_web.GSV('tmp:RRCViewCostType') <> 'Warranty - Invoiced')
  ! --- DISPLAY --- tmp:RRCInvoiceNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('tmp:RRCInvoiceNumber'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('tmp:RRCInvoiceNumber') & '_value')

Comment::tmp:RRCInvoiceNumber  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('tmp:RRCInvoiceNumber') & '_comment',Choose(p_web.GSV('tmp:RRCViewCostType') <> 'Chargeable - Invoiced' AND p_web.GSV('tmp:RRCViewCostType') <> 'Warranty - Invoiced','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('tmp:RRCViewCostType') <> 'Chargeable - Invoiced' AND p_web.GSV('tmp:RRCViewCostType') <> 'Warranty - Invoiced'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe:ExcReplcamentCharge  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('jobe:ExcReplcamentCharge') & '_prompt',Choose(p_web.GSV('Hide:ExchangeReplacement') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Exchange Replacement')
  If p_web.GSV('Hide:ExchangeReplacement') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('jobe:ExcReplcamentCharge') & '_prompt')

Validate::jobe:ExcReplcamentCharge  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:ExcReplcamentCharge',p_web.GetValue('NewValue'))
    jobe:ExcReplcamentCharge = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe:ExcReplcamentCharge
    do Value::jobe:ExcReplcamentCharge
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe:ExcReplcamentCharge',p_web.GetValue('Value'))
    jobe:ExcReplcamentCharge = p_web.GetValue('Value')
  End
  do Value::jobe:ExcReplcamentCharge
  do SendAlert

Value::jobe:ExcReplcamentCharge  Routine
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('jobe:ExcReplcamentCharge') & '_value',Choose(p_web.GSV('Hide:ExchangeReplacement') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ExchangeReplacement') = 1)
  ! --- CHECKBOX --- jobe:ExcReplcamentCharge
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe:ExcReplcamentCharge'',''viewcosts_jobe:excreplcamentcharge_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe:ExcReplcamentCharge') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe:ExcReplcamentCharge',clip(1),,loc:readonly,,,loc:javascript,,'Exchange Replacement Charge') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewCosts_' & p_web._nocolon('jobe:ExcReplcamentCharge') & '_value')

Comment::jobe:ExcReplcamentCharge  Routine
    loc:comment = ''
  p_web._DivHeader('ViewCosts_' & p_web._nocolon('jobe:ExcReplcamentCharge') & '_comment',Choose(p_web.GSV('Hide:ExchangeReplacement') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ExchangeReplacement') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('ViewCosts_tmp:ARCViewCostType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCViewCostType
      else
        do Value::tmp:ARCViewCostType
      end
  of lower('ViewCosts_tmp:ARCIgnoreDefaultCharges_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCIgnoreDefaultCharges
      else
        do Value::tmp:ARCIgnoreDefaultCharges
      end
  of lower('ViewCosts_tmp:ARCIgnoreReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCIgnoreReason
      else
        do Value::tmp:ARCIgnoreReason
      end
  of lower('ViewCosts_buttonAcceptARCReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonAcceptARCReason
      else
        do Value::buttonAcceptARCReason
      end
  of lower('ViewCosts_buttonCancelARCReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonCancelARCReason
      else
        do Value::buttonCancelARCReason
      end
  of lower('ViewCosts_tmp:ARCCost1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost1
      else
        do Value::tmp:ARCCost1
      end
  of lower('ViewCosts_tmp:AdjustmentCost1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost1
      else
        do Value::tmp:AdjustmentCost1
      end
  of lower('ViewCosts_tmp:ARCCost2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost2
      else
        do Value::tmp:ARCCost2
      end
  of lower('ViewCosts_tmp:AdjustmentCost2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost2
      else
        do Value::tmp:AdjustmentCost2
      end
  of lower('ViewCosts_tmp:ARCCost3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost3
      else
        do Value::tmp:ARCCost3
      end
  of lower('ViewCosts_tmp:AdjustmentCost3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost3
      else
        do Value::tmp:AdjustmentCost3
      end
  of lower('ViewCosts_tmp:ARCCost4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost4
      else
        do Value::tmp:ARCCost4
      end
  of lower('ViewCosts_tmp:AdjustmentCost4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost4
      else
        do Value::tmp:AdjustmentCost4
      end
  of lower('ViewCosts_tmp:ARCCost5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost5
      else
        do Value::tmp:ARCCost5
      end
  of lower('ViewCosts_tmp:AdjustmentCost5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost5
      else
        do Value::tmp:AdjustmentCost5
      end
  of lower('ViewCosts_tmp:ARCCost6_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost6
      else
        do Value::tmp:ARCCost6
      end
  of lower('ViewCosts_tmp:AdjustmentCost6_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:AdjustmentCost6
      else
        do Value::tmp:AdjustmentCost6
      end
  of lower('ViewCosts_tmp:ARCCost7_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost7
      else
        do Value::tmp:ARCCost7
      end
  of lower('ViewCosts_tmp:ARCCost8_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ARCCost8
      else
        do Value::tmp:ARCCost8
      end
  of lower('ViewCosts_tmp:RRCViewCostType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCViewCostType
      else
        do Value::tmp:RRCViewCostType
      end
  of lower('ViewCosts_tmp:RRCIgnoreDefaultCharges_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCIgnoreDefaultCharges
      else
        do Value::tmp:RRCIgnoreDefaultCharges
      end
  of lower('ViewCosts_tmp:RRCIgnoreReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCIgnoreReason
      else
        do Value::tmp:RRCIgnoreReason
      end
  of lower('ViewCosts_buttonAcceptRRCReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonAcceptRRCReason
      else
        do Value::buttonAcceptRRCReason
      end
  of lower('ViewCosts_buttonCancelRRCReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonCancelRRCReason
      else
        do Value::buttonCancelRRCReason
      end
  of lower('ViewCosts_tmp:RRCCost0_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost0
      else
        do Value::tmp:RRCCost0
      end
  of lower('ViewCosts_tmp:RRCCost1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost1
      else
        do Value::tmp:RRCCost1
      end
  of lower('ViewCosts_tmp:originalInvoice_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:originalInvoice
      else
        do Value::tmp:originalInvoice
      end
  of lower('ViewCosts_tmp:RRCCost2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost2
      else
        do Value::tmp:RRCCost2
      end
  of lower('ViewCosts_tmp:RRCCost3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost3
      else
        do Value::tmp:RRCCost3
      end
  of lower('ViewCosts_tmp:RRCCost4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost4
      else
        do Value::tmp:RRCCost4
      end
  of lower('ViewCosts_tmp:RRCCost5_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost5
      else
        do Value::tmp:RRCCost5
      end
  of lower('ViewCosts_tmp:RRCCost6_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost6
      else
        do Value::tmp:RRCCost6
      end
  of lower('ViewCosts_tmp:RRCCost7_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost7
      else
        do Value::tmp:RRCCost7
      end
  of lower('ViewCosts_tmp:RRCCost8_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:RRCCost8
      else
        do Value::tmp:RRCCost8
      end
  of lower('ViewCosts_jobe:ExcReplcamentCharge_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:ExcReplcamentCharge
      else
        do Value::jobe:ExcReplcamentCharge
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('ViewCosts_form:ready_',1)
  p_web.SetSessionValue('ViewCosts_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_ViewCosts',0)

PreCopy  Routine
  p_web.SetValue('ViewCosts_form:ready_',1)
  p_web.SetSessionValue('ViewCosts_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_ViewCosts',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('ViewCosts_form:ready_',1)
  p_web.SetSessionValue('ViewCosts_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('ViewCosts:Primed',0)

PreDelete       Routine
  p_web.SetValue('ViewCosts_form:ready_',1)
  p_web.SetSessionValue('ViewCosts_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('ViewCosts:Primed',0)
  p_web.setsessionvalue('showtab_ViewCosts',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('Hide:ARCCosts') <> 1
      If(p_web.GSV('tmp:ARCViewCostType') = 'Manufacturer Payment' or p_web.GSV('tmp:ARCViewCostType') = '')
        If (p_web.GSV('BookingSite') = 'RRC' Or Instring('- Invoiced',p_web.GSV('tmp:ARCViewCostType'),1,1) or p_web.GSV('ValidateIgnoreTickBoxARC') = 1)
          If p_web.IfExistsValue('tmp:ARCIgnoreDefaultCharges') = 0
            p_web.SetValue('tmp:ARCIgnoreDefaultCharges',0)
            tmp:ARCIgnoreDefaultCharges = 0
          End
        End
      End
  End
  If p_web.GSV('Hide:RRCCosts') <> 1
      If(p_web.GSV('tmp:RRCViewCostType') = 'Credits' Or p_web.GSV('tmp:RRCViewCostType') = 'Handling' Or p_web.GSV('tmp:RRCViewCostType') = 'Exchange' Or p_web.GSV('tmp:RRCViewCostType') = '')
        If (p_web.GSV('ValidateIgnoreTickBox') = 1 Or Instring('- Invoiced',p_web.GSV('tmp:RRCViewCostType'),1,1))
          If p_web.IfExistsValue('tmp:RRCIgnoreDefaultCharges') = 0
            p_web.SetValue('tmp:RRCIgnoreDefaultCharges',false)
            tmp:RRCIgnoreDefaultCharges = false
          End
        End
      End
  End
      If(p_web.GSV('Hide:ExchangeReplacement') = 1)
          If p_web.IfExistsValue('jobe:ExcReplcamentCharge') = 0
            p_web.SetValue('jobe:ExcReplcamentCharge',0)
            jobe:ExcReplcamentCharge = 0
          End
      End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  IF (p_web.GSV('tmp:RRCViewCostType') = 'Chargeable')
      IF (format(p_web.GSV('tmp:RRCCost2'),@n_14.2) > format(p_web.GSV('DefaultLabourCost'),@n_14.2))
          loc:Alert = 'Error! Labour Cost is higher than Default Cost'
          loc:Invalid = 'tmp:RRCCost2'
          exit
      END
  END ! 'tmp:RRCViewCostType'
  do addToAudit
  Do DeleteSessionValues
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('ViewCosts_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('ViewCosts_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
  If p_web.GSV('Hide:ARCCosts') <> 1
    loc:InvalidTab += 1
  End
  ! tab = 2
  If p_web.GSV('Hide:RRCCosts') <> 1
    loc:InvalidTab += 1
      If not (p_web.GSV('ValidateIgnoreTickBox') <> 1)
        If tmp:RRCIgnoreReason = ''
          loc:Invalid = 'tmp:RRCIgnoreReason'
          loc:alert = p_web.translate('Enter Reason For Ignoring Standard Charges') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:RRCIgnoreReason = Upper(tmp:RRCIgnoreReason)
          p_web.SetSessionValue('tmp:RRCIgnoreReason',tmp:RRCIgnoreReason)
        If loc:Invalid <> '' then exit.
      End
  End
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('ViewCosts:Primed',0)
  p_web.StoreValue('tmp:ARCViewCostType')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ARCIgnoreDefaultCharges')
  p_web.StoreValue('tmp:ARCIgnoreReason')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ARCCost1')
  p_web.StoreValue('tmp:AdjustmentCost1')
  p_web.StoreValue('tmp:ARCCost2')
  p_web.StoreValue('tmp:AdjustmentCost2')
  p_web.StoreValue('tmp:ARCCost3')
  p_web.StoreValue('tmp:AdjustmentCost3')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ARCCost4')
  p_web.StoreValue('tmp:AdjustmentCost4')
  p_web.StoreValue('tmp:ARCCost5')
  p_web.StoreValue('tmp:AdjustmentCost5')
  p_web.StoreValue('tmp:ARCCost6')
  p_web.StoreValue('tmp:AdjustmentCost6')
  p_web.StoreValue('tmp:ARCCost7')
  p_web.StoreValue('tmp:ARCCost8')
  p_web.StoreValue('tmp:ARCInvoiceNumber')
  p_web.StoreValue('tmp:RRCViewCostType')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:RRCIgnoreDefaultCharges')
  p_web.StoreValue('tmp:RRCIgnoreReason')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:RRCCost0')
  p_web.StoreValue('tmp:RRCCost1')
  p_web.StoreValue('tmp:originalInvoice')
  p_web.StoreValue('tmp:RRCCost2')
  p_web.StoreValue('tmp:RRCCost3')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:RRCCost4')
  p_web.StoreValue('tmp:RRCCost5')
  p_web.StoreValue('tmp:RRCCost6')
  p_web.StoreValue('tmp:RRCCost7')
  p_web.StoreValue('tmp:RRCCost8')
  p_web.StoreValue('tmp:RRCInvoiceNumber')
local.AddToTempQueue        Procedure(String fField,String fSaveField,String fReason,Byte fSaveCost,String fCost)
    Code
        clear(tmpaud:Record)
        tmpaud:sessionID = p_web.SessionID
        tmpaud:field = fField
        get(TempAuditQueue,tmpaud:keyField)
        IF (error())
            tmpaud:Reason = fReason
            IF (fSaveCost = 1)
                tmpaud:SaveCost = 1
                tmpaud:Cost = fCost
            END !IF (fSaveCost = 1)
            add(tempAuditQueue)
        ELSE ! IF (error())
            tmpaud:Reason = fReason
            put(tempAuditQueue)
        END ! IF (error())
