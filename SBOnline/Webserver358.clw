

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER358.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! Report the LOAN File
!!! </summary>
LoanPhoneReport PROCEDURE (NetwebserverWorker p_web)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
tmp:printedby        STRING(60)                            !
tmp:TelephoneNumber  STRING(20)                            !
tmp:FaxNumber        STRING(20)                            !
tmp:Status           STRING(60)                            !
tmp:Contact          STRING(60)                            !
tmp:Total            LONG                                  !
locLocation          STRING(30)                            !
AddressGroup         GROUP,PRE(address)                    !
CompanyName          STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
Postcode             STRING(30)                            !
TelephoneNumber      STRING(30)                            !
FaxNumber            STRING(30)                            !
EmailAddress         STRING(255)                           !
                     END                                   !
Process:View         VIEW(LOAN)
                       PROJECT(loa:ESN)
                       PROJECT(loa:Location)
                       PROJECT(loa:Model_Number)
                     END
ProgressWindow       WINDOW('Report LOAN'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

report               REPORT,AT(396,2792,7521,8500),PRE(rpt),PAPER(PAPER:A4),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,1000,7521,1000),USE(?unnamed)
                         STRING('Printed By:'),AT(5000,365),USE(?string27),FONT(,8),TRN
                         STRING(@s60),AT(5885,365),USE(tmp:printedby),FONT(,8,,FONT:bold,CHARSET:ANSI),LEFT,TRN
                         STRING('<<-- Date Stamp -->'),AT(5896,583,927,135),USE(?ReportDateStamp:2),FONT('Arial',8, |
  ,FONT:bold),TRN
                         STRING('Date Printed:'),AT(5000,573),USE(?string27:2),FONT(,8),TRN
                         STRING('Page Number:'),AT(5000,781),USE(?string27:3),FONT(,8),TRN
                         STRING('<<-- Time Stamp -->'),AT(6896,583,927,135),USE(?ReportTimeStamp:2),FONT('Arial',8, |
  ,FONT:bold),TRN
                         STRING(@n_3),AT(5885,781,700,135),USE(?PageCount:2),FONT('Arial',8,,FONT:bold),PAGENO,TRN
                       END
detail                 DETAIL,AT(0,0,,229),USE(?detailband)
                         STRING(@s16),AT(104,0),USE(loa:ESN),FONT(,8),TRN
                         STRING(@s20),AT(1198,0),USE(loa:Model_Number),FONT(,8),TRN
                         STRING(@s60),AT(2500,0,2240,208),USE(tmp:Status),FONT('Arial',8,,FONT:regular,CHARSET:ANSI), |
  LEFT,TRN
                         STRING(@s60),AT(4844,0,2396,208),USE(tmp:Contact),FONT(,8),TRN
                       END
totals                 DETAIL,AT(0,0,7521,292),USE(?totals)
                         LINE,AT(104,42,7188,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Total Number Of Lines:'),AT(94,73),USE(?String28),FONT(,8,,FONT:bold),TRN
                         STRING(@s8),AT(1521,83),USE(tmp:Total),FONT(,8,,FONT:bold),TRN
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(address:CompanyName),FONT(,16,,FONT:bold),LEFT,TRN
                         STRING('LOAN PHONE REPORT'),AT(4063,0,3385,260),USE(?string20),FONT(,14,,FONT:bold),RIGHT, |
  TRN
                         STRING(@s30),AT(156,260,2240,156),USE(address:AddressLine1),FONT(,9),TRN
                         STRING(@s30),AT(156,417,2240,156),USE(address:AddressLine2),FONT(,9),TRN
                         STRING(@s30),AT(156,573,2240,156),USE(address:AddressLine3),FONT(,9),TRN
                         STRING(@s30),AT(156,729,1156,156),USE(address:Postcode),FONT(,9),TRN
                         STRING('Tel:'),AT(156,1042),USE(?string16),FONT(,9),TRN
                         STRING(@s30),AT(521,1042),USE(address:TelephoneNumber),FONT(,9),TRN
                         STRING('Fax: '),AT(156,1198),USE(?string19),FONT(,9),TRN
                         STRING(@s30),AT(521,1198),USE(address:FaxNumber),FONT(,9),TRN
                         STRING(@s255),AT(521,1354),USE(address:EmailAddress),FONT(,9),TRN
                         STRING('Email:'),AT(156,1354),USE(?string19:2),FONT(,9),TRN
                         STRING('I.M.E.I. Number'),AT(208,2083),USE(?string44),FONT(,8,,FONT:bold),TRN
                         STRING('Model No'),AT(1198,2083),USE(?string45),FONT(,8,,FONT:bold),TRN
                         STRING('Status'),AT(2500,2083),USE(?string24),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Contact'),AT(4844,2083),USE(?string25),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),TRN
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR3               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
EndReport              PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepStringClass                       ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.EndReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
      PRINT(rpt:Totals)  
  ReturnValue = PARENT.EndReport()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('LoanPhoneReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:JOBS.SetOpenRelated()
  Relate:JOBS.Open                                         ! File JOBS used by this procedure, so make sure it's RelationManager is open
  Relate:LOAN.Open                                         ! File LOAN used by this procedure, so make sure it's RelationManager is open
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
        locLocation = p_web.GSV('BookingSiteLocation')  
        
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = p_web.GSV('BookingUserCode')
        IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            tmp:printedby = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
        END ! IF
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('LoanPhoneReport',ProgressWindow)           ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowAlpha+ScrollSort:AllowNumeric,ScrollBy:RunTime)
  ThisReport.Init(Process:View, Relate:LOAN, ?Progress:PctText, Progress:Thermometer, ProgressMgr, loa:ESN)
  ThisReport.CaseSensitiveValue = FALSE
  ThisReport.AddSortOrder(loa:LocIMEIKey)
  ThisReport.AddRange(loa:Location,locLocation)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:LOAN.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
    Relate:LOAN.Close
  END
  IF SELF.Opened
    INIMgr.Update('LoanPhoneReport',ProgressWindow)        ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'LoanPhoneReport',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = p_web.GSV('BookingAccount')
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Found
            address:CompanyName     = tra:Company_Name
            address:AddressLine1    = tra:Address_Line1
            address:AddressLine2    = tra:Address_Line2
            address:AddressLine3    = tra:Address_Line3
            address:Postcode        = tra:Postcode
            address:TelephoneNumber = tra:Telephone_Number
            address:FaxNumber       = tra:Fax_Number
            address:EmailAddress    = tra:EmailAddress
        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Error
        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  ReturnValue = PARENT.OpenReport()
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.Init(report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,report{PROPPRINT:Paper},PAPER:USER), CHOOSE(report{PROP:Thous}=True,PROP:Thous,CHOOSE(report{PROP:MM}=True,PROP:MM,CHOOSE(report{PROP:Points}=True,PROP:Points,0))), report{PROPPRINT:PaperWidth}, report{PROPPRINT:PaperHeight}, report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetTitle('Loan Phone Report')        !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR3.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetViewerPrefs(PDFXTR3:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp:2{PROP:Text} = FORMAT(TODAY(),@d06)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp:2{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'LoanPhoneReport',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR3.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
      
  ReturnValue = PARENT.TakeRecord()
        p_web.Noop()
        
        IF (p_web.GSV('locLoanStatus') <> '') 
            IF (loa:Available <> p_web.GSV('locLoanStatus')) 
                RETURN Level:User 
            END !
        END ! IF
        IF (p_web.GSV('locStockType') <> '')
            IF (loa:Stock_Type <> p_web.GSV('locStockType'))
                RETURN Level:User
            END ! IF
        END !I F
        
        IF (p_web.GSV('locStartDate') > 0)
            IF (loa:StatusChangeDate < p_web.GSV('locStartDate'))
                RETURN Level:User
            END! I F
        END !I F
        
        IF (p_web.GSV('locEndDate') > 0)
            IF (loa:StatusChangeDate > p_web.GSV('locEndDate'))
                RETURN Level:User
            END! I F
        END !I F
        
        tmp:Status = VodacomClass.GetLoanStatus(loa:Available,loa:Job_Number)
        
        tmp:Contact = ''
        IF (loa:Job_Number > 0)
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = loa:Job_Number
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                tmp:Contact = Clip(job:Title) & ' ' & Clip(job:Initial) & ' ' & Clip(job:Surname) & ' ' & Clip(job:Telephone_Delivery)
            END ! IF
            
        END ! IF
        tmp:Total += 1
        
  PRINT(rpt:detail)
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR3:rtn = PDFXTR3.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR3:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

