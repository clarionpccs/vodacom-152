

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER120.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! Replacement For SecurityCheckFailed
!!! </summary>
DoesUserHaveAccess   PROCEDURE  (NetwebServerWorker p_web,STRING pAccessArea) ! Declare Procedure
retValue                    LONG(0)
FilesOpened     BYTE(0)

  CODE
        DO OpenFiles

        Access:USERS.ClearKey(use:password_key)
        use:Password = p_web.GSV('BookingUserPassword')
        IF (Access:USERS.TryFetch(use:password_key) = Level:Benign)
            Access:ACCAREAS.ClearKey(acc:Access_Level_Key)
            acc:User_Level = use:User_Level
            acc:Access_Area = pAccessArea
            IF (Access:ACCAREAS.TryFetch(acc:Access_level_key) = Level:Benign)
                retValue = 1
            END ! IF
        END ! IF

        DO CloseFiles
        
        RETURN retValue
!--------------------------------------
OpenFiles  ROUTINE
  Access:USERS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:USERS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ACCAREAS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ACCAREAS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:USERS.Close
     Access:ACCAREAS.Close
     FilesOpened = False
  END
