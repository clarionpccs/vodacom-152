

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER508.INC'),ONCE        !Local module procedure declarations
                     END


PutJobDetailsCellular PROCEDURE  (NetWebServerWorker p_web)
qJob                        QUEUE(qSkylinePutJobDetails),PRE(qJob)
                            END ! QUEUE
rtn                         LONG()
defaultHeader               STRING(1000)
errorStr                    STRING(255)
newJobNumber                LONG()
loc:x          Long
packet              string(NET:MaxBinData)
packetlen           long
CRLF           String('<13,10>')
NBSP           String('&#160;')

  CODE
  GlobalErrors.SetProcedureName('PutJobDetailsCellular')
  p_web.SetValue('_parentPage','PutJobDetailsCellular')
  p_web.publicpage = 1
  if p_web.sessionId = 0 then p_web.NewSession().
  do Header
!region Request
        LOOP 1 TIMES
            rtn = SL_Init(p_web,p_web.GetHeaderField('Authorization'),qJob)
            GET(qJob,1)
            
            ! Get SL Number, if passed
            defaultHeader = '<SLNumber>' & CLIP(qJob.SLNumber) & '</SLNumber>'
            CASE rtn
            OF 1 ! Nothing Received or Parsed
                BREAK
            OF 2 ! Login Error
                SL_CreateResponse(p_web,0,'Login Details Incorrect')
!                SL_CreateResponse(p_web,Skyline:Error,'Login Details Incorrect',defaultHeader)
                BREAK
            END ! CASE
            IF (qJob.SCJobNo = 0)
                ! No job number passed, therefore this is a New Job Booking
                newJobNumber = SL_PutNewJob(p_web,qJob,errorStr)
                
                IF (newJobNumber = 0)
                    SL_CreateResponse(p_web,0,'Job Insertion Failed: ' & errorStr)    
                    !SL_CreateResponse(p_web,Skyline:InsertError,CLIP(errorStr),defaultHeader)
                    BREAK
                END ! IF
                
                defaultHeader = CLIP(defaultHeader) & '<SCJobNo>' & CLIP(newJobNumber) & '</SCJobNo>'
                
                !SL_CreateResponse(p_web,Skyline:Success,'Success',defaultHeader)
                SL_CreateResponse(p_web,1,,'<SCJobNo>' & CLIP(newJobNumber) & '</SCJobNo>')
            END ! IF
        END!  LOOP
        
!endRegion
        
  do Footer
!region Response
        packet = CLIP(packet) & p_web.GSV('SkylineResponseString')
        
        p_web.AddLog('RESPONSE PutJobDetailsCellular<13,10,13,10>' & CLIP(packet))
!endregion
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

SendPacket  Routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,Net:NoHeader)
    packet = ''
  end
Header Routine
  packet = ''
  p_web.ReplyContentType = 'text/xml'
  p_web.ParseHTML(packet,1,0,Net:SendHeader+Net:DontCache)
  packet = ''
Footer Routine
