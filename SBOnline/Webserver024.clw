

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER024.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
SendSMSText          PROCEDURE  (String SentJob,String SentAuto,String SentSpecial,<NetWebServerWorker p_web>) ! Declare Procedure

  CODE
        VodacomClass.SendSMS(SentJob,SentAuto,SentSpecial,p_web)  ! #12477 Call the SMS routine (DBH: 17/05/2012)
