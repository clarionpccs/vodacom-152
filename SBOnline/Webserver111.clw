

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER111.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER110.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER112.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! Work Out Charge/Repair Types
!!! </summary>
CalculateBilling     PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
local:CFaultCode        String(255)
local:CIndex            Byte(0)
local:CRepairType       String(30)
local:WFaultCOde        String(255)
local:WIndex            Byte(0)
local:WRepairType       String(30)
local:KeyRepair         Long()
local:FaultCode         String(255)
FilesOpened     BYTE(0)

  CODE
    do openfiles

    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer = p_web.GSV('job:Manufacturer')
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        If man:UseInvTextForFaults = True
            If man:AutoRepairType = True
                ! Get the main out fault (DBH: 23/11/2007)
                Access:MANFAULT.Clearkey(maf:MainFaultKey)
                maf:Manufacturer = p_web.GSV('job:Manufacturer')
                maf:MainFault = 1
                If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

                    ! Check the out faults list (DBH: 23/11/2007)
                    Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
                    joo:JobNumber = p_web.GSV('job:Ref_Number')
                    Set(joo:JobNumberKey,joo:JobNumberKey)
                    Loop
                        If Access:JOBOUTFL.Next()
                            Break
                        End ! If Access:JOBOUTFL.Next()
                        If joo:JobNumber <> p_web.GSV('job:Ref_Number')
                            Break
                        End ! If joo:JobNumber <> job:Ref_Number

                        ! Get the lookup details (e.g. index and repair types) (DBH: 23/11/2007)
                        Access:MANFAULO.Clearkey(mfo:Field_Key)
                        mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                        mfo:Field_Number = maf:Field_Number
                        mfo:Field = joo:FaultCode
                        If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            ! Is there a repair type? (DBH: 23/11/2007)
                            If mfo:RepairType <> ''
                                ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                If mfo:ImportanceLevel > local:CIndex
                                    local:CFaultCode = mfo:Field
                                    local:CIndex = mfo:ImportanceLevel
                                    local:CRepairType = mfo:RepairType
                                End ! If mfo:ImportanceLevel > local:CIndex
                            End ! If mfo:RepairType <> ''
                            If mfo:RepairTypeWarranty <> ''
                                ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                If mfo:ImportanceLevel > local:WIndex
                                    local:WFaultCode = mfo:Field
                                    local:WIndex = mfo:ImportanceLevel
                                    local:WRepairType = mfo:RepairTypeWarranty
                                End ! If mfo:ImportanceLevel > local:CIndex
                            End ! If mfo:RepairTypeWarranty <> ''
                        End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                    End ! Loop

                    ! Inserting (DBH 30/04/2008) # 9723 - Is there a "Key Repair" fault code? If so, that is counted as the OUT Fault
                    Access:MANFAUPA.Clearkey(map:KeyRepairKey)
                    map:Manufacturer = p_web.GSV('job:Manufacturer')
                    map:KeyRepair = 1
                    If Access:MANFAUPA.TryFetch(map:KeyRepairKey) = Level:Benign
                        ! Found
                        local:KeyRepair = map:Field_Number
                    Else ! If Access:MANFAUPA.TryFetch(map:KeyRepairKey) = Level:Benign
                        local:KeyRepair = 0
                    End ! If Access:MANFAUPA.TryFetch(map:KeyRepairKey) = Level:Benign
                    ! End (DBH 30/04/2008) #9723

! Changing (DBH 21/05/2008) # 9723 - Can be more than one part out fault
!                    Access:MANFAUPA.Clearkey(map:MainFaultKey)
!                    map:Manufacturer = job:Manufacturer
!                    map:MainFault = 1
!                    If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
! to (DBH 21/05/2008) # 9723
                    ! Is the out fault held on the parts. If so, check for the highest repair index there too (DBH: 23/11/2007)
                    Access:MANFAUPA.Clearkey(map:MainFaultKey)
                    map:Manufacturer = p_web.GSV('job:Manufacturer')
                    map:MainFault = 1
                    Set(map:MainFaultKey,map:MainFaultKey)
                    Loop ! Begin Loop
                        If Access:MANFAUPA.Next()
                            Break
                        End ! If Access:MANFAUPA.Next()
                        If map:Manufacturer <> p_web.GSV('job:Manufacturer')
                            Break
                        End ! If map:Manufacturer <> job:Manufacturer
                        If map:MainFault <> 1
                            Break
                        End ! If map:MainFault <> 1

! End (DBH 21/05/2008) #9723
                        ! Check the Warranty Parts (DBH: 23/11/2007)
                        Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
                        wpr:Ref_Number = p_web.GSV('job:Ref_Number')
                        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                        Loop
                            If Access:WARPARTS.Next()
                                Break
                            End ! If Access:WARPARTS.Next()
                            If wpr:Ref_Number <> p_web.GSV('job:Ref_Number')
                                Break
                            End ! If wpr:Ref_Number <> job:Ref_Number

                            ! Inserting (DBH 30/04/2008) # 9723 - If there is a key repair. Only count the part that ISthe Key Repair
                            Case local:KeyRepair
                            Of 1
                                If wpr:Fault_Code1 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code1 <> 1
                            Of 2
                                If wpr:Fault_Code2 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code2 <> 1
                            Of 3
                                If wpr:Fault_Code3 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 4
                                If wpr:Fault_Code4 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 5
                                If wpr:Fault_Code5 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 6
                                If wpr:Fault_Code6 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 7
                                If wpr:Fault_Code7 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 8
                                If wpr:Fault_Code8 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 9
                                If wpr:Fault_Code9 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 10
                                If wpr:Fault_Code10 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 11
                                If wpr:Fault_Code11 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 12
                                If wpr:Fault_Code12 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            End ! Case local:KeyRepair
                            ! End (DBH 30/04/2008) #9723

                            ! Lookup the fault code details using whichever is designated as the out fault (DBH: 23/11/2007)
                            Access:MANFAULO.Clearkey(mfo:Field_Key)
                            mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                            mfo:Field_Number = maf:Field_NUmber
                            Case map:Field_Number
                            Of 1
                                mfo:Field = wpr:Fault_Code1
                            Of 2
                                mfo:Field = wpr:Fault_Code2
                            Of 3
                                mfo:Field = wpr:Fault_Code3
                            Of 4
                                mfo:Field = wpr:Fault_Code4
                            Of 5
                                mfo:Field = wpr:Fault_Code5
                            Of 6
                                mfo:Field = wpr:Fault_Code6
                            Of 7
                                mfo:Field = wpr:Fault_Code7
                            Of 8
                                mfo:Field = wpr:Fault_Code8
                            Of 9
                                mfo:Field = wpr:Fault_Code9
                            Of 10
                                mfo:Field = wpr:Fault_Code10
                            Of 11
                                mfo:Field = wpr:Fault_Code11
                            Of 12
                                mfo:Field = wpr:Fault_Code12
                            End ! Case map:Field_Number
                            If Access:MANFAULO.Tryfetch(mfo:Field_Key) = Level:Benign
                                ! Is there a repair type? (DBH: 23/11/2007)
                                If mfo:RepairType <> ''
                                    ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                    If mfo:ImportanceLevel > local:CIndex
                                        local:CFaultCode = mfo:Field
                                        local:CIndex = mfo:ImportanceLevel
                                        local:CRepairType = mfo:RepairType
                                    End ! If mfo:ImportanceLevel > local:CIndex
                                End ! If mfo:RepairType <> ''
                                If mfo:RepairTypeWarranty <> ''
                                    ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                    If mfo:ImportanceLevel > local:WIndex
                                        local:WFaultCode = mfo:Field
                                        local:WIndex = mfo:ImportanceLevel
                                        local:WRepairType = mfo:RepairTypeWarranty
                                    End ! If mfo:ImportanceLevel > local:CIndex
                                End ! If mfo:RepairTypeWarranty <> ''
                            End ! If Access:MANFAULO.Clearkey(mfo:Field_Key) = Level:Benign
                        End ! Loop

                        ! Check the estimate parts (DBH: 23/11/2007)
                        If p_web.GSV('job:Estimate') = 'YES' And p_web.GSV('job:Chargeable_Job') = 'YES' And p_web.GSV('job:Estimate_Accepted') <> 'YES' And p_web.GSV('job:Estimate_Rejected') <> 'YES'
                            Access:ESTPARTS.Clearkey(epr:Part_Number_Key)
                            epr:Ref_Number = p_web.GSV('job:Ref_Number')
                            Set(epr:Part_Number_Key,epr:Part_Number_Key)
                            Loop
                                If Access:ESTPARTS.Next()
                                    Break
                                End ! If Access:ESTPARTS.Next()
                                If epr:Ref_Number <> p_web.GSV('job:Ref_Number')
                                    Break
                                End ! If epr:Ref_Number <> job:Ref_Number

                                ! Inserting (DBH 30/04/2008) # 9723 - If there is a key repair. Only count the part that ISthe Key Repair
                                Case local:KeyRepair
                                Of 1
                                    If epr:Fault_Code1 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code1 <> 1
                                Of 2
                                    If epr:Fault_Code2 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code2 <> 1
                                Of 3
                                    If epr:Fault_Code3 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 4
                                    If epr:Fault_Code4 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 5
                                    If epr:Fault_Code5 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 6
                                    If epr:Fault_Code6 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 7
                                    If epr:Fault_Code7 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 8
                                    If epr:Fault_Code8 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 9
                                    If epr:Fault_Code9 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 10
                                    If epr:Fault_Code10 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 11
                                    If epr:Fault_Code11 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 12
                                    If epr:Fault_Code12 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                End ! Case local:KeyRepair
                                ! End (DBH 30/04/2008) #9723

                                ! Lookup the fault code details using whichever is designated as the out fault (DBH: 23/11/2007)
                                Access:MANFAULO.Clearkey(mfo:Field_Key)
                                mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                mfo:Field_Number = maf:Field_NUmber
                                Case map:Field_Number
                                Of 1
                                    mfo:Field = epr:Fault_Code1
                                Of 2
                                    mfo:Field = epr:Fault_Code2
                                Of 3
                                    mfo:Field = epr:Fault_Code3
                                Of 4
                                    mfo:Field = epr:Fault_Code4
                                Of 5
                                    mfo:Field = epr:Fault_Code5
                                Of 6
                                    mfo:Field = epr:Fault_Code6
                                Of 7
                                    mfo:Field = epr:Fault_Code7
                                Of 8
                                    mfo:Field = epr:Fault_Code8
                                Of 9
                                    mfo:Field = epr:Fault_Code9
                                Of 10
                                    mfo:Field = epr:Fault_Code10
                                Of 11
                                    mfo:Field = epr:Fault_Code11
                                Of 12
                                    mfo:Field = epr:Fault_Code12
                                End ! Case map:Field_Number
                                If Access:MANFAULO.Tryfetch(mfo:Field_Key) = Level:Benign
                                    ! Is there a repair type? (DBH: 23/11/2007)
                                    If mfo:RepairType <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:CIndex
                                            local:CFaultCode = mfo:Field
                                            local:CIndex = mfo:ImportanceLevel
                                            local:CRepairType = mfo:RepairType
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairType <> ''
                                    If mfo:RepairTypeWarranty <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:WIndex
                                            local:WFaultCode = mfo:Field
                                            local:WIndex = mfo:ImportanceLevel
                                            local:WRepairType = mfo:RepairTypeWarranty
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairTypeWarranty <> ''
                                End ! If Access:MANFAULO.Clearkey(mfo:Field_Key) = Level:Benign
                            End ! Loop
                        Else ! If job:Estimate = 'YES' And job:Chargeabe_Job = 'YES' And job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
                            Access:PARTS.Clearkey(par:Part_Number_Key)
                            par:Ref_Number = p_web.GSV('job:Ref_Number')
                            Set(par:Part_Number_Key,par:Part_Number_Key)
                            Loop
                                If Access:PARTS.Next()
                                    Break
                                End ! If Access:PARTS.Next()
                                If par:Ref_Number <> p_web.GSV('job:Ref_Number')
                                    Break
                                End ! If par:Ref_Number <> job:Ref_Number
                                ! Inserting (DBH 30/04/2008) # 9723 - If there is a key repair. Only count the part that ISthe Key Repair
                                Case local:KeyRepair
                                Of 1
                                    If par:Fault_Code1 <> 1
                                        Cycle
                                    End ! If par:Fault_Code1 <> 1
                                Of 2
                                    If par:Fault_Code2 <> 1
                                        Cycle
                                    End ! If par:Fault_Code2 <> 1
                                Of 3
                                    If par:Fault_Code3 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 4
                                    If par:Fault_Code4 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 5
                                    If par:Fault_Code5 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 6
                                    If par:Fault_Code6 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 7
                                    If par:Fault_Code7 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 8
                                    If par:Fault_Code8 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 9
                                    If par:Fault_Code9 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 10
                                    If par:Fault_Code10 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 11
                                    If par:Fault_Code11 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 12
                                    If par:Fault_Code12 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                End ! Case local:KeyRepair
                                ! End (DBH 30/04/2008) #9723

                                ! Lookup the fault code details using whichever is designated as the out fault (DBH: 23/11/2007)
                                Access:MANFAULO.Clearkey(mfo:Field_Key)
                                mfo:Manufacturer = p_web.GSV('job:Manufacturer')
                                mfo:Field_Number = maf:Field_NUmber
                                Case map:Field_Number
                                Of 1
                                    mfo:Field = par:Fault_Code1
                                Of 2
                                    mfo:Field = par:Fault_Code2
                                Of 3
                                    mfo:Field = par:Fault_Code3
                                Of 4
                                    mfo:Field = par:Fault_Code4
                                Of 5
                                    mfo:Field = par:Fault_Code5
                                Of 6
                                    mfo:Field = par:Fault_Code6
                                Of 7
                                    mfo:Field = par:Fault_Code7
                                Of 8
                                    mfo:Field = par:Fault_Code8
                                Of 9
                                    mfo:Field = par:Fault_Code9
                                Of 10
                                    mfo:Field = par:Fault_Code10
                                Of 11
                                    mfo:Field = par:Fault_Code11
                                Of 12
                                    mfo:Field = par:Fault_Code12
                                End ! Case map:Field_Number
                                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    ! Is there a repair type? (DBH: 23/11/2007)
                                    If mfo:RepairType <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:CIndex
                                            local:CFaultCode = mfo:Field
                                            local:CIndex = mfo:ImportanceLevel
                                            local:CRepairType = mfo:RepairType
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairType <> ''
                                    
                                    If mfo:RepairTypeWarranty <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:WIndex
                                            local:WFaultCode = mfo:Field
                                            local:WIndex = mfo:ImportanceLevel
                                            local:WRepairType = mfo:RepairTypeWarranty
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairTypeWarranty <> ''
                                End ! If Access:MANFAULO.Clearkey(mfo:Field_Key) = Level:Benign

                            End ! Loop
                        End ! If job:Estimate = 'YES' And job:Chargeabe_Job = 'YES' And job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
                    End ! If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                End ! If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            End ! If man:AutoRepairType = True
        End ! If man:UseInvTextForFaults = True
    End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    ! If the charge types are already filled then assume "overwrite" box has been ticked.
!    ! So don't change. (DBH: 23/11/2007)
!    if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
!        p_web.SSV('job:Repair_Type',local:CRepairType)
!    end ! if (p_web.GSV('jobe:COverwriteRepairType') = 0)
!
!    if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
!        p_web.SSV('job:Warranty_Repair_Type',local:WRepairType)
!    end ! if (p_web.GSV('jobe:WOverwriteRepairType') = 0)

    if (local:CRepairType <> '' and local:WRepairType <> '')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type',local:CRepairType)
            end
            if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type_Warranty',local:WRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') <> 'YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type',local:CRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') <> 'YES' and p_web.GSV('job:warranty_job') = 'YES')
            if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type_Warranty',local:WRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
    end  !if (local:CRepairType <> '' and local:WRepairType <> '')
    if (local:CRepairType = '' and local:WRepairType <> '')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type','')
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') <> 'YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type','')
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') <> 'YES' and p_web.GSV('job:warranty_job') = 'YES')
            if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type_Warranty',local:WRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
    end  !if (local:CRepairType <> '' and local:WRepairType <> '')
    if (local:CRepairType <> '' and local:WRepairType = '')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type',local:CRepairType)
            end
            if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
                p_web.SSV('job:Warranty_Job','NO')
            end
            transferParts_C(p_web)
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') <> 'YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type',local:CRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
        if (p_web.GSV('job:chargeable_job') <> 'YES' and p_web.GSV('job:warranty_job') = 'YES')
            if (p_web.GSV('jobe:WOverwriteRepairType') <> 1)
                p_web.SSV('job:warranty_Job','NO')
            end
            p_web.SSV('job:chargeable_Job','YES')
            if (p_web.GSV('jobe:COverwriteRepairType') <> 1)
                p_web.SSV('job:repair_Type',local:CRepairType)
            end
        end ! if (p_web.GSV('job:chargeable_job') = 'YES' and p_web.GSV('job:warranty_job') ='YES')
    end  !if (local:CRepairType <> '' and local:WRepairType <> '')


    ! Inserting (DBH 13/06/2006) #6733 - Work out if to use Warranty or Chargeable Fault Code
    local:FaultCode = ''
    if (p_web.GSV('job:Chargeable_Job') <> 'YES' And p_web.GSV('job:Warranty_Job') = 'YES')
        local:FaultCode = local:WFaultCode
    End ! If job:Chargeable_Job <> 'YES' And job:Warranty_Job = 'YES'
    if (p_web.GSV('job:Chargeable_Job') = 'YES' And p_web.GSV('job:Warranty_Job') <> 'YES')
        local:FaultCode = local:CFaultCode
    End ! If job:Chargeable_Job = 'YES' And job:Warranty_Job <> 'YES'
    if (p_web.GSV('job:Chargeable_Job') = 'YES' And p_web.GSV('job:Warranty_Job') = 'YES')
        If local:WFaultCode <> ''
            local:FaultCode = local:WFaultCode
        Else
            local:FaultCode = local:CFaultCode
        End ! If tmp:FaultCodeW_T <> ''
    End ! If job:Chargeable_Job = 'YES' ANd job:Warranty_Job = 'YES'
    ! End (DBH 13/06/2006) #6733



    Access:MANFAULT.Clearkey(maf:mainFaultKey)
    maf:manufacturer    = p_web.GSV('job:manufacturer')
    maf:mainFault    = 1
    if (Access:MANFAULT.TryFetch(maf:mainFaultKey) = Level:Benign)
        ! Found
        if (maf:field_Number < 13)
            p_web.SSV('job:fault_Code' & maf:field_Number,local:faultCode)
        else ! if (maf:field_Number < 13)
            p_web.SSV('wob:faultCode' & maf:field_Number,local:faultCode)
        end ! if (maf:field_Number < 13)
    else ! if (Access:MANFAULT.TryFetch(maf:mainFaultKey) = Level:Benign)
        ! Error
    end ! if (Access:MANFAULT.TryFetch(maf:mainFaultKey) = Level:Benign)

    do closefiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBOUTFL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBOUTFL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULO.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAULO.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAUPA.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANFAUPA.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WARPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ESTPARTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ESTPARTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:PARTS.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:MANUFACT.Close
     Access:MANFAULT.Close
     Access:JOBOUTFL.Close
     Access:MANFAULO.Close
     Access:MANFAUPA.Close
     Access:WARPARTS.Close
     Access:ESTPARTS.Close
     Access:PARTS.Close
     FilesOpened = False
  END
