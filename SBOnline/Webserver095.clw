

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER095.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ForceIMEI            PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !

  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !ESN
    If (def:Force_ESN = 'B' And func:Type = 'B') Or |
        (def:Force_ESN <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_ESN = 'B'
    Return Level:Benign
