

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER031.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
PendingJob           PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
addAuditEntry               LONG()
locPreClaim                 LONG
FilesOpened     BYTE(0)

  CODE
!region ProcessedCode
        do OpenFiles
        
        !p_web._trace('PendingJob Start - job:EDI = ' & p_web.GSV('job:EDI'))
        !p_web._trace('PendingJob Start - job:Date_Completed = ' & p_web.GSV('job:Date_Completed'))
        !p_web._trace('PendingJob Start - job:Ref_Number = ' & p_web.GSV('job:Ref_Number'))

        Access:MANUFACT.ClearKey(man:Manufacturer_Key)
        man:Manufacturer = p_web.GSV('job:Manufacturer')
        If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Found
            If man:EDIFileType    = 'ALCATEL' Or  |
                man:EDIFileType   = 'APPLE' Or    | ! #13393 Add APPLE as warranty manufacturer (DBH: 17/04/2015)
                man:EDIFileType   = 'BOSCH' Or    |
                man:EDIFileType   = 'BLACKBERRY' Or | ! #1336 Not added here, when added to SB (DBH: 22/08/2014)
                man:EDIFileType   = 'ERICSSON' Or |
                man:EDIFileType   = 'HTC' Or      | ! #1336 Not added here, when added to SB (DBH: 22/08/2014)
                man:EDIFileType   = 'HUAWEI' Or   | ! #1336 Not added here, when added to SB (DBH: 22/08/2014)
                man:EDIFileType   = 'LG' Or    |
                man:EDIFileType   = 'MAXON' Or    |
                man:EDIFileType   = 'MOTOROLA' Or |
                man:EDIFileType   = 'NEC' Or      |
                man:EDIFileType   = 'NOKIA' Or    |
                man:EDIFileType   = 'SIEMENS' Or  |
                man:EDIFileType   = 'SAMSUNG' Or  |
                man:EDIFileType   = 'SAMSUNG SA' Or  |
                man:EDIFileType   = 'MITSUBISHI' Or   |
                man:EDIFileType   = 'TELITAL' Or  |
                man:EDIFileType   = 'SAGEM' Or    |
                man:EDIFileType   = 'SONY' Or     |
                man:EDIFileType   = 'PHILIPS' Or     |
                man:EDIFileType   = 'PANASONIC' OR  |
                man:EDIFileType   = 'ZTE'! #1336 Not added here, when added to SB (DBH: 22/08/2014)

                Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
                If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Found
                Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Error
                End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign

            !Job should never becaome an EDI exception by mistake
            !So no need to double check if it's edi is set to exception
            
                case p_web.GSV('job:EDI')
                of 'EXC' orof 'REJ'
                    p_web.SSV('wob:EDI',p_web.GSV('job:EDI'))
                    return
                of 'EXM' orof 'AAJ'
                    return
                end ! case p_web.GSV('job:EDI')
                
                ! #13620 Third Party Claim Check Missing (DBH: 27/10/2015)
                locPreClaim = 0
                IF (p_web.GSV('job:Third_Party_Site') <> '' AND ~(p_web.GSV('job:Date_Completed') > 0) AND job:Workshop <> 'YES')
                    Access:TRDPARTY.Clearkey(trd:Company_Name_Key)
                    trd:Company_Name = p_web.GSV('job:Third_Party_Site')
                    IF (Access:TRDPARTY.Tryfetch(trd:Company_Name_Key) = Level:Benign)
                        IF (trd:PreCompletionClaiming)
                            Access:TRDMAN.Clearkey(tdm:ManufacturerKey)
                            tdm:ThirdPartyCompanyName = p_web.GSV('job:Third_Party_Site')
                            tdm:Manufacturer = p_web.GSV('job:Manufacturer')
                            IF (Access:TRDMAN.Tryfetch(tdm:ManufacturerKey) = Level:Benign)
                            ! Pre Claiming Is Go!
                                locPreClaim = 1
                            END ! IF (Access:TRDMAN.Tryfetch(tdm:ManufacturerKey) = Level:Benign)
                        END ! IF (trd:PreCompletionClaiming)
                    END ! IF (Access:TRDPARTY.Tryfetch(trd:Company_Name_Key) = Level:Benign)
                END ! IF

            !Do the calculation to check if a job should appear in the Pending Claims Table
            ! Job must be completed, a warranty job, and not already have a batch number. Not be an exception, a bouncer and not be excluded (by Charge TYpe or Repair Type)

                found# = 0
                If p_web.GSV('job:Date_Completed') > 0 OR locPreClaim = 1 ! #12363 If pre-claim then job doesn't need to be completed (DBH: 03/02/2012)
!Stop(2)
                    If p_web.GSV('job:Warranty_Job') = 'YES'
!Stop(3)
!                        If p_web.GSV('job:EDI_Batch_Number') = 0
!Stop(4)
                            If p_web.GSV('job:EDI') <> 'AAJ'
!Stop(5)
                                If p_web.GSV('job:EDI') <> 'EXC'
!Stop(6)
                                    If ~(p_web.GSV('job:Bouncer') <> '' And (p_web.GSV('job:Bouncer_Type') = 'BOT' Or p_web.GSV('job:Bouncer_Type') = 'WAR'))
!Stop(7)
                                        Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
                                        cha:Charge_Type = p_web.GSV('job:Warranty_Charge_Type')
                                        If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
                                        !Found
                                            If cha:Exclude_EDI <> 'YES'
!Stop(8)
                                                Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
                                                rtd:Manufacturer = p_web.GSV('job:Manufacturer')
                                                rtd:Warranty = 'YES'
                                                rtd:Repair_Type = p_web.GSV('job:Repair_Type_Warranty')
                                                If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                                                !Found
                                                    If ~rtd:ExcludeFromEDI
!Stop(9)
                                                        If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') <> 'APP'
                                                            p_web.SSV('wob:EDI','NO')
                                                        End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'

                                                    !Check if the job is a 48 Hour Scrap/RTM with only one exchange - 4449
                                                        ScrapCheck# = 0
                                                    ! Is this a 48 hour job? (DBH: 05/02/2007)
                                                        If p_web.GSV('jobe:Engineer48HourOption') = 1
                                                        ! Is there only one exchange attached? (DBH: 05/02/2007)
                                                            If p_web.GSV('job:Exchange_Unit_Number') > 0
                                                                If p_web.GSV('jobe:SecondExchangeNumber') = 0
                                                                    If p_web.GSV('job:Repair_Type_Warranty') = 'R.T.M.' Or p_web.GSV('job:Repair_Type_Warranty') = 'SCRAP'
                                                                        ScrapCheck# = 1
                                                                    ! Do not claim for this job (DBH: 05/02/2007)
                                                                    End ! If job:Repair_Type_Warranty = 'R.T.M.' Or job:Repair_Type_Warranty = 'SCRAP'
                                                                End ! If jobe:SecondExchangeNumber = 0
                                                            End ! If job:Exchange_Unit_Number > 0
                                                        End ! If jobe:Engineer48HourOption = 1

                                                        If ScrapCheck# = 0
!Stop(10)
                                                            If p_web.GSV('job:EDI') <> 'NO'
                                                                p_web.SSV('jobe2:InPendingDate',Today())
                                                            End ! If job:EDI <> 'NO'
                                                            p_web.SSV('job:EDI','NO')
                                                            found# = 1
                                                        End ! If ScrapCheck# = 0
                                                    End ! If ~rtd:ExcludeFromEDI
                                                Else ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                                                !Error
                                                End ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign

                                            End ! If cha:Exclude_EDI <> 'YES'
                                        Else ! If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
                                        !Error
                                        End ! If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign

                                    End ! If (~job:Bouncer <> '' And (job:Bouncer_Type = 'BOT' Or job:Bouncer_Type = 'WAR'))
                                Else ! If job:EDI <> 'EXC'
                                    if (p_web.GSV('BookingSite') = 'RRC')
                                        p_web.SSV('wob:EDI','AAJ')
                                        p_web.SSV('job:EDI','AAJ')
                                    Else ! If glo:WebJob
                                        p_web.SSV('job:EDI','EXC')
                                    End ! If glo:WebJob
                                    found# = 1
                                End ! If job:EDI <> 'EXC'
                            Else ! If job:EDI <> 'AAJ'
                            End ! If job:EDI <> 'AAJ'
!                        Else ! If job:EDI_Batch_Number = 0
!                        ! This is an error check (DBH: 05/02/2007)
!                        ! If the job has a batch number then it can only be 3 things. Invoice, Reconciled or exception (DBH: 05/02/2007)
!                            !If p_web.GSV('job:Invoice_Number_Warranty') <> '' ! #13578 Invoice Number could be zero (DBH: 16/10/2015)
!                            IF (p_web.GSV('job:Invoice_Number_Warranty') > 0 )
!                                If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') <> 'APP'
!                                    p_web.SSV('wob:EDI','FIN')
!                                End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
!                                p_web.SSV('job:EDI','FIN')
!                            Else ! If job:Invoice_Number_Warranty <> ''
!                                If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') <> 'APP'
!                                    p_web.SSV('wob:EDI','YES')
!                                End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
!                                p_web.SSV('job:EDI','YES')
!                            End ! If job:Invoice_Number_Warranty <> ''
!                            found# = 1
!                        End ! If job:EDI_Batch_Number = 0
                    End ! If job:Warranty_Job = 'YES'
                End ! If job:Date_Completed <> ''
            End ! man:EDIFileType   = 'PANASONIC'

        ! Make sure the EDI value doesn't change once it's set (DBH: 05/02/2007)

        ! Reconciled (DBH: 05/02/2007)
            If (found# = 0)
                If (p_web.GSV('job:EDI') = 'YES')
                    If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') = 'APP'
                        p_web.SSV('wob:EDI','YES')
                    End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI = 'APP'
                    p_web.SSV('job:EDI','YES')
                    found# = 1
                End ! If job:EDI = 'YES'
            End ! If Clip(tmp:Return) = ''

        ! Invoiced (DBH: 05/02/2007)
            If (found# = 0)
                If p_web.GSV('job:EDI') = 'FIN'
                    If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') <> 'APP'
                        p_web.SSV('wob:EDI','FIN')
                    End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
                    p_web.SSV('job:EDI','FIN')
                    found# = 1
                End ! If job:EDI = 'FIN'
            End ! If Clip(tmp:Return) = ''


        ! If all else failes, then this shouldn't be a warranty claim (DBH: 05/02/2007)
            If (found# = 0)
                If p_web.GSV('wob:EDI') <> 'PAY' And p_web.GSV('wob:EDI') <> 'AAJ' And p_web.GSV('wob:EDI') <> 'APP'
                    p_web.SSV('wob:EDI','XXX')
                End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
                p_web.SSV('job:EDI','XXX')
                found# = 1
            End ! If Clip(tmp:Return) = ''

        ! Inserting (DBH 05/02/2007) # 8707 - Write warranty information to new file
            Access:JOBSWARR.ClearKey(jow:RefNumberKey)
            jow:RefNumber = p_web.GSV('job:Ref_Number')
            If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
            !Found
                addAuditEntry = FALSE
            Else ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
            !Error
                If Access:JOBSWARR.PrimeRecord() = Level:Benign
                    jow:RefNumber = p_web.GSV('job:Ref_Number')
                    jow:Orig_sub_date = today()     !TB12588 - new field to be added on insert - JC 04/08/12 (! #13346 Ouch. Never been added. (DBH: 06/08/2014))
                    jow:ClaimSubmitted = today()   !TB12588 - set up the claim submitted date - JC 04/08/12 (! #13346 Ouch. Never been added. But don't see the point of this? (DBH: 06/08/2014))
                
                    If Access:JOBSWARR.TryInsert() = Level:Benign
                    !Insert
                        addAuditEntry = TRUE
                    Else ! If Access:JOBSWARR.TryInsert() = Level:Benign
                        Access:JOBSWARR.CancelAutoInc()
                    End ! If Access:JOBSWARR.TryInsert() = Level:Benign
                End ! If Access.JOBSWARR.PrimeRecord() = Level:Benign
            End ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign

            !p_web._trace('PendingJob Setting - job:EDI = ' & p_web.GSV('job:EDI'))
        
            If (p_web.GSV('job:EDI') = 'XXX' Or found#= 0)
                Relate:JOBSWARR.Delete(0)
            
                Access:SBO_WarrantyClaims.ClearKey(sbojow:JobNumberKey)
                sbojow:SessionID = p_web.SessionID
                sbojow:JobNumber = p_web.GSV('job:Ref_Number')
                IF (Access:SBO_WarrantyClaims.TryFetch(sbojow:JobNumberKey) = Level:Benign)
                ! #13449 Remove from the temp, unlive, need to update manually, list. (DBH: 10/12/2014)
                    Relate:SBO_WarrantyClaims.Delete(0)
                END ! IF
            
            Else ! If tmp:Return = 'XXX'
                IF (addAuditEntry)
                    AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','ORIGINAL SUBMISSION TO WARRANTY PROCESS SCREEN','JOB HAS BEEN PUT ON THE PENDING TAB OF THE WARRANTY PROCESS SCREEN')
                END ! IF
            
            
                jow:Status = p_web.GSV('job:EDI')
                jow:RRCStatus = p_web.GSV('wob:EDI')
                jow:BranchID = tra:BranchIdentification
                
                SentToHub(p_web)  ! #13605 Don't use the VodacomClass routine as this uses the actual data files, not the Session Q (DBH: 21/09/2015)
                IF (p_web.GSV('SentToHub') = 1)
!                If VodacomClass.JobSentToHub(p_web.GSV('job:Ref_Number'))
                    jow:RepairedAt = 'ARC'
                Else ! If SentToHub(job:Ref_Number)
                    jow:RepairedAt = 'RRC'
                End ! If SentToHub(job:Ref_Number)
                
                jow:Manufacturer = p_web.GSV('job:Manufacturer')

                Access:CHARTYPE.ClearKey(cha:Warranty_Key)
                cha:Warranty = 'YES'
                cha:Charge_Type = p_web.GSV('job:Warranty_Charge_Type')
                If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
                !Found
                Else ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
                !Error
                End ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
                If cha:SecondYearWarranty
                    jow:FirstSecondYear = 1
                Else ! If cha:SecondYearWarranty
                    jow:FirstSecondYear = 0
                End ! If cha:SecondYearWarranty

                If (p_web.GSV('job:EDI') = 'NO')
                    If jow:Submitted = 0
                        jow:Submitted = 1
                    End ! If jow:Submitted = 0
                End ! If tmp:Return = 'NO'
            
                If jow:ClaimSubmitted = 0
                    jow:ClaimSubmitted = p_web.GSV('job:Date_Completed')
                End ! If jow:SubmittedDate = 0
            
                IF (p_web.GSV('job:EDI') = 'NO')
                    IF (jow:Submitted = 0)
                        jow:Submitted = 1
                        jow:ClaimSubmitted = TODAY()
                    END ! IF
                END ! IF            
            
                Access:JOBSWARR.Update()
            End ! If tmp:Return = 'XXX'
        ! End (DBH 05/02/2007) #8707

        Else ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Error
        End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

        do CloseFiles
!endregion
!--------------------------------------
OpenFiles  ROUTINE
  Access:TRDMAN.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRDMAN.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SBO_WarrantyClaims.Open                           ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SBO_WarrantyClaims.UseFile                        ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:CHARTYPE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:CHARTYPE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:REPTYDEF.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:REPTYDEF.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MANUFACT.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRDPARTY.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRDPARTY.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSWARR.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSWARR.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TRDMAN.Close
     Access:SBO_WarrantyClaims.Close
     Access:CHARTYPE.Close
     Access:REPTYDEF.Close
     Access:MANUFACT.Close
     Access:TRDPARTY.Close
     Access:JOBSWARR.Close
     Access:TRADEACC.Close
     FilesOpened = False
  END
