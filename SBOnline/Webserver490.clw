

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER490.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
RunDOS               PROCEDURE  (string aCmd, byte aWait)  ! Declare Procedure
! dwCreationFlag values
NORMAL_PRIORITY_CLASS             equate(00000020h)
CREATE_NO_WINDOW                  equate(08000000h)

isSTILL_ACTIVE          equate(259)

ProcessExitCode       ulong
CommandLine           cstring(1024)

StartUpInfo           group
Cb                      ulong
lpReserved              ulong
lpDesktop               ulong
lpTitle                 ulong
dwX                     ulong
dwY                     ulong
dwXSize                 ulong
dwYSize                 ulong
dwXCountChars           ulong
dwYCountChars           ulong
dwFillAttribute         ulong
dwFlags                 ulong
wShowWindow             signed
cbReserved2             signed
lpReserved2             ulong
hStdInput               unsigned
hStdOutput              unsigned
hStdError               unsigned
                      end

ProcessInformation    group
hProcess                unsigned
hThread                 unsigned
dwProcessId             ulong
dwThreadId              ulong
                      end

  CODE
! Run DOS Program
    CommandLine = clip(aCmd)
    StartUpInfo.Cb = size(StartUpInfo)

    ReturnCode# = CreateProcess(0,                                             |
                                address(CommandLine),                          |
                                0,                                             |
                                0,                                             |
                                0,                                             |
                                BOR(NORMAL_PRIORITY_CLASS, CREATE_NO_WINDOW),  |
                                0,                                             |
                                0,                                             |
                                address(StartUpInfo),                          |
                                address(ProcessInformation))

    if ReturnCode# = 0  then
        ! Error Return
        return false
    end

    timeout# = GETINI('MQ','TimeOut',,Clip(Path()) & '\SB2KDEF.INI')
    if (timeout# = 0)
        timeout# = 20
    end ! if (timeout# = 0)
    timeout# = clock() + (timeout# * 100)

    if aWait then
        setcursor(cursor:wait)
        loop
            Sleep(100)
            ! check for when process is finished
            ReturnCode# = GetExitCodeProcess(ProcessInformation.hProcess, address(ProcessExitCode))
        while ProcessExitCode = isSTILL_ACTIVE and clock() < timeout#
        setcursor
    end

    return(true)
