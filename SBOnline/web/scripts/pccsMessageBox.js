var messageBoxDIV = false;
var messageBoxBackDIV = false;
var winWidth;
var winHeight;
var titleText;
var messText;
var nameBut1;
var nameBut2;
var nameBut3;
var nameBut4;
var nameBut5;
var urlBut1;
var urlBut2;
var urlBut3;
var urlBut4;
var urlBut5;
var urlTarget1;
var urlTarget2;
var urlTarget3;
var urlTarget4;
var urlTarget5;
var nBackDIV = false;
var nDIV = false;
//var buttonsNames = new Array[0];
//var buttonsURL = new Array[0];
//var buttonsTarget = new Array[0];
//var buttonsFunction = new Array[0];
var buttonsNames = [0];
var buttonsURL = [0];
var buttonsTarget = [0];
var buttonsFunction = [0];

var mbBackDIV = false;
var mbFrontDIV = false;

function openMyMessageBox(tText, mText, bArray, wWidth, wHeight) {
    // Button Array Format
	// ["Button Name","URL default to #","or Function Name","Target, default to _self"]
	//
	var windowWidth, windowHeight;
	if (bArray === undefined) {
		bArray = ["OK"];
	}
	// Set Default Window Height / Width
	if (wWidth !== undefined) {
		windowWidth = wWidth;
	} else {
		windowWidth = 300;
	}
	if (wHeight !== undefined) {
		windowHeight = wHeight;
	} else {
		windowHeight = 120;
	}
	
	var g = document.getElementsByTagName("body")[0];
	var x = g.clientWidth;
	var y = g.clientHeight;
//    var x = window.innerWidth;
//    var y = window.innerHeight;    
	var paramLeft = Math.round((((x / 2) - (windowWidth / 2)) / x) * 100);
    var paramTop = Math.round((((y / 2) - (windowHeight / 2)) / y) * 100) - 5;
	
	if (!mbFrontDIV || mbFrontDIV.style.display !== "block") // DIV not created, or not visible
	{
		mbBackDIV = document.createElement("DIV");
		mbBackDIV.id = "mbBackDIV";
		mbBackDIV.style.zIndex = 1000;
		mbBackDIV.style.cssText = "opacity: 0.7;filter:alpha(opacity=70);top: 0;left: 0;background-color: white;width: 100%;height: 100%;position: fixed;"
		mbBackDIV.style.display = "block";
		document.body.appendChild(mbBackDIV);
		
		mbFrontDIV = document.createElement("DIV");
		mbFrontDIV.id = "mbFrontDIV";
		mbFrontDIV.style.zIndex = 1000;
		mbFrontDIV.style.cssText = "text-align: center;border: 1px solid #CFCFCF;position: fixed; top: " + paramTop + "%; left: " + paramLeft + "%;border-radius: 3px;box-shadow: 2px 2px 10px #888888;padding-left: 10px;padding-right: 10px;width: " + windowWidth + "px;height:" + windowHeight + "px;background-color: #EFEFEF;margin:auto;";
		mbFrontDIV.style.display = "block";
		
		mbFrontDIV.innerHTML = '<p class="SubHeading">' + tText + '</p><b>' + mText + '</b><br/><br/>';
		document.body.appendChild(mbFrontDIV);
		
		var destination = "#";
		for (i=0 ; i < bArray.length ; i++)
		{
			// alert("Name: " + buttonArray[i] + "\nTarget: " + buttonArray[i + 1] + "\nLoc: " + buttonArray[i + 2] + "\nFunction: " + buttonArray[i + 3]);

			// i = Button Name
			// i + 1 = URL
			// i + 2 = Function Name
			// i + 3 = Target
			iName = i;
			iURL = i + 1;
			iFunction = i + 2;
			iTarget = i + 3;
			
			// mbFrontDIV.innerHTML = mbFrontDIV.innerHTML + '<button type="button" name="Button' + i + '" class="MessageBoxButton" value="Button' + i + '"  onclick="closeMyMessageBox(\'' + bArray[iURL] + '\',' + bArray[iFunction] + ',\'' + bArray[iTarget] + '\');">' + bArray[iName] + '</button>';
			
			mbFrontDIV.innerHTML = mbFrontDIV.innerHTML + '<a onclick="javascript:closeMyMessageBox(\'' + bArray[iURL] + '\',' + bArray[iFunction] + ',\'' + bArray[iTarget] + '\');" class="MessageBoxButton">' + bArray[iName] + '</a>';
			
			i += 3;
		}

	}
}

function closeMyMessageBox(url,functionName,target)
{
	mbFrontDIV.style.display = "none";
	mbBackDIV.style.display = "none";
	
	if (functionName !== undefined)
	{
		functionName();
	} else {
		if (url !== 'undefined')
		{
			url = url || "#";
		} else {
			url = "#";
		}
		
		if (target !== 'undefined')
		{ 
			target = target || '_self';
		} else {
			target = "_self";
		}
		
		window.open(url,target);
	}

}

//===============================================


function ShowNotes(title,textFieldValue)
{
    if (!nDIV || nDIV.style.display !== 'block')
    {
        // alert(textFieldValue);
        nBackDIV = document.createElement('DIV');
        nBackDIV.id = 'nBackDIV';
        nBackDIV.style.cssText = 'background-color: rgb(255, 255, 255); width: 100%; height: 100%; display: block;position: fixed;z-index: 1000;top: 0;left: 0;opacity: 0.7;filter:alpha(opacity=70);';
        nBackDIV.style.display = 'block';
        document.body.appendChild(nBackDIV);
        
        nDIV = document.createElement('DIV');
        nDIV.id = 'nDIV';
        nDIV.style.cssText = 'text-align:center;border:1px solid #CFCFCF;border-radius: 3px;box-shadow: 2px 2px 10px #888888;position:fixed;top:20%;left:40%;padding-left:10px;padding-right:10px';
        nDIV.style.zIndex = 1000;
        nDIV.style.width = "240px";
        nDIV.style.height = "220px";
        nDIV.style.backgroundColor = "#EFEFEF";
        nDIV.style.display = 'block';
        nDIV.style.margin = 'auto';       
        nDIV.innerHTML = '<p class="SubHeading">' + title + '</p></br>';
        nDIV.innerHTML = nDIV.innerHTML + '<textarea id="showNotesField" cols="45" rows="8" class="TextEntry Upper formreadonly">' + textFieldValue + '</textarea>';
        nDIV.innerHTML = nDIV.innerHTML + '</br></br><button id="closeButton" onclick="CloseNotes();" class="MessageBoxButton">Close</button>';
        document.body.appendChild(nDIV);
    }
}

function CloseNotes()
{
    nDIV.style.display = 'none';
    nBackDIV.style.display = 'none';
}

// Deprecate This Message Box When You Have Time
function messageBox()
{
    var d = document;
    var g = d.getElementsByTagName("body")[0];
    var x = g.clientWidth;
    var y = g.clientHeight;
    var paramLeft = Math.round(( ((x / 2) - (winWidth / 2)) / x ) * 100);
    var paramTop = Math.round(( ((y / 2) - (winHeight / 2)) / y ) * 100) - 5;
    if (!messageBoxDIV || messageBoxDIV.style.display !== 'block')
    {
        messageBoxBackDIV = document.createElement('DIV');
        messageBoxBackDIV.id = 'messageBoxBackDIV';
        messageBoxBackDIV.style.cssText = 'background-color: rgb(255, 255, 255); width: 100%; height: 100%; display: block;position: fixed;z-index: 1000;top: 0;left: 0;opacity: 0.7;filter:alpha(opacity=70);'
        messageBoxBackDIV.style.display = 'block';
        document.body.appendChild(messageBoxBackDIV);

        messageBoxDIV = document.createElement('DIV');
        messageBoxDIV.id = 'messageBoxDIV';
        messageBoxDIV.style.cssText = 'text-align:center;border:1px solid #CFCFCF;border-radius: 3px;box-shadow: 2px 2px 10px #888888;position:fixed;top:' + paramTop + '%;left:' + paramLeft + '%;padding-left:10px;padding-right:10px';
        messageBoxDIV.style.zIndex = 1000;

        messageBoxDIV.style.width = winWidth + "px";
        messageBoxDIV.style.height = winHeight + "px";

        messageBoxDIV.style.backgroundColor = "#EFEFEF";
        messageBoxDIV.style.display = 'block';

        messageBoxDIV.style.margin = 'auto';
        messageBoxDIV.innerHTML = '<p class="SubHeading">' + titleText + '</p><b>' + messText + '</b><br/><br/>';
        
        // Clear unset fields
        for (i=0 ; i < buttonsNames.length ; i ++)
        {
            if (buttonsTarget[i] === undefined)
            {
                buttonsTarget[i] = "";
            }
            if (buttonsURL[i] === undefined)
            {
                buttonsURL[i] = "";
            }
        }

        for (i=0 ; i < buttonsNames.length ; i ++)
        {
            messageBoxDIV.innerHTML = messageBoxDIV.innerHTML + '<button type="button" name="Button' + i + '" class="MessageBoxButton" value="Button' + i + '"  onclick="closeMessageBox(\'' + buttonsURL[i] + '\',\'' + buttonsTarget[i] + '\',' + buttonsFunction[i] + ');">' + buttonsNames[i] + '</button>';
        }       
        document.body.appendChild(messageBoxDIV);
    }
}

function closeMessageBox(theURL,theTarget,theFunction)
{

    messageBoxDIV.style.display = 'none';
    messageBoxBackDIV.style.display = 'none';
    
    theTarget = theTarget || '_self';

    if (theURL !== '')
    {
        theURL = theURL || '#';
        window.open(theURL,theTarget);
    } else {
        // Didn't pass a URL so hoping that a fuction name was passed instead
        if (theFunction !== undefined)
        {
            theFunction();
        }
    }
}

// Specific Defaults

function mbWarrantyBatch_ReprintInvoice(jowRefNumber)
{
    if (jowRefNumber === undefined)
    {
        alert("You must select a job first.");
        return;
    }
	
	buttonArray = ["Selected Job","RRCWarrantyInvoice?Type=Selected&RecordNumber=" + jowRefNumber,,"_blank",
					"Selected Date","RRCWarrantyInvoice?Type=Date&RecordNumber=" + jowRefNumber,,"_blank",
					"Cancel"];
	openMyMessageBox("Print Options","Do you want to reprint the invoice containing the SELECTED job, or print all invoices created on the same DATE as the selected job?",buttonArray,360,150);
}

function mbWarrantyClaims_ProcessRejection(jowRefNumber)
{
    if (jowRefNumber === undefined)
    {
        alert("You must select a job first.");
        return;
    }
	buttonArray = ["Resubmit Claim","WarrantyProcessRejection?RType=1",,,
					"Reject Claim","WarrantyProcessRejection?RType=2",,,
					"Cancel"];
	openMyMessageBox("Process Rejection","Do you wish to RESUBMIT the claim, or acknowledge the REJECTION?",buttonArray,340);
}

function mbIndexPage_WaybillProcedures()
{
	buttonArray = ["Waybill Generation","WaybillGeneration",,,
					"Waybill Confirmation","WaybillConfirmation?clearvars=1",,,
					"Despatch Sundry Items","WaybillCreateSundry?clearvars=1",,,
                    "Waybill Procedure","FormWaybillProcedureCriteria?firsttime=1",,,
					"Cancel"];
	openMyMessageBox("Waybill Procedures","Select An Option",buttonArray,690);
}

function mbIndexPage_AuditProcedures()
{

	buttonArray = ["Stock Audit","FormStockAudits?firsttime=1",,,
					"WIP Audit","FormWIPAudits",,,
					"Exchange Audit","FormExchangeAudits",,,
					"Loan Audit","FormLoanAudits",,,
					"Cancel"];
	openMyMessageBox("Audit Procedures","Select An Option",buttonArray,590);
}

function clearButtonArrays() 
{
    buttonsNames.length = 0;
    buttonsURL.length = 0;
    buttonsTarget.length = 0;
}

// ===================
function simpleConfirm(textMessage,firstURL,secondURL)
{
    var r=confirm(textMessage);
    if (r==true)
    {
        window.open(firstURL,'_self');
    } else {
        window.open(secondURL,'_self');
    }
}

function simpleQuestion(title,message,yesURL,noURL)
{
    buttonArray=["Yes",yesURL,,,"No",noURL];
    openMyMessageBox(title,message,buttonArray);
}

function pleaseWait(message)
{
	var array=[];
	openMyMessageBox('Please Wait',message,array,220,80);
}
// ===================
function generateSundryWaybill(wayBillItems, sendTo, dest, trans, secPack, comp, telNo, conName, add1, hub)
{
    // WaybillCreateSundry Procedure

    var errorField = '';

    if (dest === '')
    {
        displayRequiredAlert("Destination");
        return;
    }
    if (trans === '')
    {
        displayRequiredAlert("Mode Of Transport");
        return;
    }
    if (secPack === '')
    {
        displayRequiredAlert("Security Pack No");
        return;
    }
    if (sendTo === '1')
    {
        if (comp === '')
        {
            displayRequiredAlert("Company Name");
            return;
        }
        if (telNo === '')
        {
            displayRequiredAlert("Telephone Number");
            return;
        }
        if (conName === '')
        {
            displayRequiredAlert("Contact Name");
            return;
        }
        if (add1 === '')
        {
            displayRequiredAlert("Address");
            return;
        }
        if (hub === '')
        {
            displayRequiredAlert("Hub");
            return;
        }
    }

    // Display Warning If No Waybill Items Attached
    if (wayBillItems === 0)
    {
        var r = confirm("You have not attached any items to the Waybill.\n\nAre you sure you want to create a waybill with no items attached?");
        if (r === false) return;
    } else {
        // Confirm
        r = confirm("Are you sure you want to create a Waybill for\n" + comp + "?");
        if (r === false) return;
    }

    // Call Waybill Generation Page
    window.open("PageProcess?ProcessType=WaybillSundryGeneration&ReturnURL=WaybillCreateSundry?clearvars=1&RedirectURL=WaybillCreate&wayType=Sundry","_self");
}

function displayRequiredAlert(fieldName)
{
    alert(fieldName + " Required.");
}

// ===================
function completeWaybill(unProcCount)
{
    if (unProcCount !== 0)
    {
        var r = confirm("Are you sure you want to complete this Waybill?\n\nAny un-processed jobs will be marked as 'Query'?");
        if (r === false) return;
    }
    window.open("PageProcess?ProcessType=WaybillComplete&ReturnURL=WaybillConfirmation&RedirectURL=WaybillConfirmation","_self");
}

// ===================
function waybillProcessJob(contHist)
{
    if (contHist === 1)
    {
        window.open("BrowseContactHistory?type=Waybill","_self");
    } else {
        window.open("PageProcess?ProcessType=WaybillProcessJob&ReturnURL=WaybillConfirmation&RedirectURL=WaybillConfirmation","_self");
    }

}

// ===================
function areYouSureYouWantToChange(id,prevValue,description)
{
    //alert(prevValue);
    r = confirm("Are you sure you want to change " + description + "?");
    if (r === false) {
        id.value = prevValue;
    }
}


// =======================
function wipAuditStartQuestion(user,currentUser,status,url,auditNo)
{
    if (auditNo !== undefined)
    {
        url = url + "?AuditNo=" + auditNo;
    }
    if (user !== currentUser)
    {
        if (status === "AUDIT IN PROGRESS")
        {
			messTitle = "Audit In Progress";
			messMessage = "This audit is already in progress. Do you want to assist in scanning this unit?";
        } else {
            if (status === "SUSPENDED")
            {
				messTitle = "Audit Suspended";
				messMessage = "This audit has been suspended. Do you wish to continue this audit?";
            }
        }
		buttonArray = ["Yes",url,,,"No"];
		openMyMessageBox(messTitle,messMessage,buttonArray);
    } else {
		window.open(url,"_self");
	}
}

function wipAuditCompleteConfirm()
{
	buttonArray = ["Yes",,"wipAuditCompleteConfirm2",,"No"];
    openMyMessageBox("Complete WIP Audit","Do you wish to complete the current WIP Audit?",buttonArray);
}
function wipAuditCompleteConfirm2()
{
	buttonArray = ["Yes","AuditProcess?ProcessType=WIPCompleteAudit&okURL=FormWIPAuditCriteria&errorURL=FormProcessWIPAudits",,,
						"No"];
	openMyMessageBox("Complete WIP Audit","Are you sure? Once completed the audit cannot be edited!",buttonArray);
	
}

function exchangeAuditCompleteConfirm()
{
	buttonArray = ["Yes","AuditProcess?ProcessType=ExchangeCompleteAudit&okURL=PrintGenericDocument&errorURL=FormExchangeAuditProcess",,,"No"];
	openMyMessageBox("Complete Exchange Audit","Do you wish to complete the current Exchange Audit?",buttonArray);
}

function exchangeAuditReprintOption(auditNo)
{
	buttonArray = ["Report","ExchangeAuditReport?emf__Audit_Number=" + auditNo,,"_blank",
	"Export","PageProcess?ProcessType=ExchangeAuditExport&emf__Audit_Number=" + auditNo,,"_blank","Cancel"];
	openMyMessageBox("Exchange Audit Report","Select Option Below",buttonArray);
}

function loanAuditCompleteConfirm()
{
	buttonArray = ["Yes","AuditProcess?ProcessType=LoanCompleteAudit&okURL=LoanAuditReport&errorURL=FormLoanAuditProcess",,,"No"];
	openMyMessageBox("Complete Loan Audit","Do you wish to complete the current Loan Audit?",buttonArray);
}

function stockNewAuditType()
{
	buttonArray = ["Scan","AuditProcess?ProcessType=StockNewAudit&okURL=FormStockAudits&errorURL=FormStockAudits&AuditType=S",,,
					"Manual","AuditProcess?ProcessType=StockNewAudit&okURL=FormStockAudits&errorURL=FormStockAudits&AuditType=M",,,
					"Cancel"];
    openMyMessageBox("Stock Audit","How do you intend to complete this audit? By scanning the items, or by manually entering stock quantities?<br/><br/>Note: Once you have chosen a method you cannot change it.",buttonArray,380,155);
}

function confirmStockAuditStart(records)
{
	r = confirm("This will create an audit with " + records + " entries. Are you sure?");
	if (r === true)
	{
		window.open("AuditProcess?ProcessType=StockNewAuditStart&okURL=FormStockAuditProcess&errorURL=FormStockAudits","_self");
	} else {
		window.open("FormStockAudits","_self");
	}
}

function stockJoinAuditQuestion(auditNo)
{
	buttonArray = ["Yes","FormStockAuditProcess?AuditNo=" + auditNo,,,"No"];
	openMyMessageBox("Stock Audit","Are you sure you want to be added as a secondary scanner to assist in scanning for this audit?",buttonArray);
}

function stockCompleteAudit()
{
	buttonArray=["Yes","AuditProcess?ProcessType=StockCompleteAudit&okURL=PrintGenericDocument&errorURL=FormStockAuditProcess&PrintType=StockAuditReport",,,"No"];
	openMyMessageBox("Stock Audit","Do you wish to complete the current stock audit?",buttonArray);
}

function loanMakeAvailable(refNo,jobNumber)
{
	var url = "PageProcess?ProcessType=LoanUnitAvailable&ReturnURL=FormAllLoanUnits&RedirectURL=FormAllLoanUnits&loa__Ref_Number=" + refNo;
	buttonArray=["Yes",url,,,"No"];
	if (jobNumber > 0) 
	{
		openMyMessageBox("Make Unit Available","You are about to remove this unit from job number " + jobNumber + ".<br/><br/>Are you sure?",buttonArray,320,152);
	} else {
		openMyMessageBox("Make Unit Available","Are you sure you want to make this unit available?",buttonArray);
	}
}

function stockOutstandingOrders()
{
    url = "PageProcess?ProcessType=OutstandingStockOrders&ReturnURL=FormBrowseStock&RedirectURL=FormOutstandingOrders";
    buttonArray = ["Stock",url + "&ordType=S",,,"Exchange",url + "&ordType=E",,,"Loan",url + "&ordType=L",,,"All",url + "&ordType=A",,,"Cancel"];
    openMyMessageBox("Outstanding Stock Orders","Select which Outstanding Orders you wish to see",buttonArray,500);
      
}

function generateWaybill(waybillType,force)
{
    url = "PageProcess?ProcessType=WaybillGeneration&ReturnURL=WaybillGeneration&RedirectURL=WaybillCreate&wayType=" + waybillType + "&force=" + force
    buttonArray = ["Yes",url,,,"No"];
    openMyMessageBox("Generate Waybill","Are you sure you want to generate a waybill for the processed jobs?",buttonArray);
}

function autoFulFilRequests()
{
    var url = "frmFulFilRequest?FulFilType=MULTIPLE";
    var buttonArray = ["Yes",url,,,"No"];
    openMyMessageBox("Auto FulFil Requests","Are you sure you want to automatically fulfil ALL parts on order?",buttonArray);
}

function runStatusReport(type)
{
    var fromDate = document.getElementById("locStartDate").value;
    var toDate = document.getElementById("locEndDate").value;
    //var url = "PageProcess?ProcessType=StatusReportEngine&type=" + type;
    var url = "RunReport?RT=StatusReport";
    var buttonArray = ["OK",url,,,"Cancel"];
    openMyMessageBox("Status Report","Confirm Date Range:<br/><br/>From: " + fromDate + " To: " + toDate + "<br/><br/>Click OK To Run Report",buttonArray,300,170);
}

function cancelWIPAudit(id) 
{
    var url = "PageProcess?ProcessType=CancelWIPAudit&RedirectURL=FormWIPAudits&id=" + id;
    var buttonArray = ["Confirm",url,,,"Stop"];
    openMyMessageBox("Cancel Audit","Please confirm that you wish to cancel the highlighted Audit? Please select CONFIRM to continue.",buttonArray);
}

function rebuildStockAllocation() {
    var url = "PageProcess?ProcessType=RefreshStockAllocation&RedirectURL=FormStockAllocation&fin=1";
    var buttonArray = ["Begin",url,,,"Cancel"];
    openMyMessageBox("Refresh Stock Allocation","Warning! <br/><br/>There should be no need to refresh the Stock Allocation browse unless there has been an error and you have been advised to do so by your System Supervisor.<br/><br/>Note: If you begin, you *MUST* wait for this process to finish otherwise you will affect network performance.",buttonArray,400,200);
}

function deleteReport(id) {
    var url = "RunReport?RT=DeleteReport&rpq__RecordNumber=" + id;
    var buttonArray = ["Delete",url,,,"Cancel"];
    openMyMessageBox("Delete Report","Are you sure you want to delete the selected report?",buttonArray,400,120);
}
function deleteAllReports() {
    var url = "RunReport?RT=DeleteAllReports";
    var buttonArray = ["Delete",url,,,"Cancel"];
    openMyMessageBox("Delete All Reports","Are you sure you want to delete ALL of your generated report?",buttonArray,400,120);
}    

function scramble(inString) {
  inString = inString.toUpperCase();
  var str = inString;
  
  var len = str.length;
  
  var char;
  var charVal;
  var bxor;
  var finalChar;
  var scrambled="";
  var dayNumber = new Date().getDate();
  
  for(i = 0; i < len; i++) {
  char = str.substring(i,i+1);
  charVal = char.charCodeAt(0) * dayNumber;
  scrambled = scrambled + charVal;
  
  }
  
  var finalNumber = scrambled + dayNumber;
  
  return finalNumber;
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

// Edit Warranty Rejected Job, Password Validation

function validateJobPassword(viewOnly){
    
  var url = window.location;
  
  if (viewOnly === 1) {
    url = url + "&ViewOnly=1";
  } else {
  
    var pass = document.getElementById("userpassword").value;
    if (pass != "") {
      url = url + "&valpass=" + scramble(pass);
    } else {
      return;
    }
      
  }
  window.open(url,"_self");
}

function showJobPassword() {
    document.getElementById("div-background-warrantyjobalert").style.display = "block";
    document.getElementById("userpassword").focus();
}

// Rapid Loan/Exchange IMEI Validation

function validateIMEINumber(){
    var url = "FormRapidLoanExchangeInsert?tab=2";


    var imei = document.getElementById("imeiNumber").value;
  
    if (imei != "") {
      imei = encodeURIComponent(scramble(imei));

      url = url + "&locIMEINumber=" + document.getElementById("locIMEINumber").value + "&imeiconf=" + imei;
    } else {
      return;
    }
      
  window.open(url,"_self");
}

function showIMEIValidation() {
    document.getElementById("div-background-imeivalidation").style.display = "block";
    document.getElementById("imeiNumber").focus();
    
}

var cnt=0;
var tcnt=0;
var fcnt='';
var icnt='';
function countDown(){
  // hh = parseInt( cnt / 3600 );
  // mm = parseInt( cnt / 60 ) % 60;
  // ss = cnt % 60;
  
    var defaultTimeOut = 15; // SB Times out after 15 mins
    //var defaultMessageTime = 10 * 60; // Show warning message after 10 mins
    var defaultMessageTime = 2; // Debug 2 Seconds
    
    var currentdate = new Date();
    currentdate.setMinutes(currentdate.getMinutes() + defaultTimeOut);
    var hh = currentdate.getHours();
    var mm = currentdate.getMinutes();
    var ss = currentdate.getSeconds();
    
    var t = hh + ":" + (mm < 10 ? "0" + mm : mm) + ":" + (ss < 10 ? "0" + ss : ss);
    
  cnt -= 1;
  if (cnt == (tcnt - defaultMessageTime)) {
      alert("You have been idle for 10 minutes. Click 'OK' before " + t + " to keep your session alive.");
      resetCountDown();
      // window.location.reload(); 
      // var r=confirm("You have been idle for 20 seconds. Your session will expiry at " + expiryTime + ". Click 'OK' to keep your session alive or 'Cancel' to logout now.");
      // if (r===true) {
        // resetCountDown();
        // window.location.reload(); 
      // } else {
          // window.open(fcnt,"_top");
      // }
  } else {
		setTimeout("countDown();",1000);  
  }
};

function resetCountDown(){
  cnt = tcnt;
}

function startCountDown(t,f,i){
  if (t){
		tcnt = t;
	}	
	if (f){
		fcnt = f;
	}
	if (i){
		icnt = i;
	}
	cnt = tcnt;
  countDown();
};
