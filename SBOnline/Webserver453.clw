

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER453.INC'),ONCE        !Local module procedure declarations
                     END


FormReturnStock      PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
LocRefNumber         LONG                                  !
locReturnType        STRING(30)                            !
locInvoiceNumber     STRING(30)                            !
locSalesNumber       STRING(30)                            !
locQuantity          LONG                                  !
locNotes             STRING(255)                           !
locDateReceived      DATE                                  !
FilesOpened     Long
RTNORDER::State  USHORT
SUBTRACC::State  USHORT
TRADEACC::State  USHORT
RETSALES::State  USHORT
RETTYPES::State  USHORT
STOCK::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
locReturnType_OptionView   View(RETTYPES)
                          Project(rtt:Description)
                        End
  CODE
  GlobalErrors.SetProcedureName('FormReturnStock')
  loc:formname = 'FormReturnStock_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormReturnStock',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormReturnStock','')
    p_web._DivHeader('FormReturnStock',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormReturnStock',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormReturnStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormReturnStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormReturnStock',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormReturnStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormReturnStock',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormReturnStock',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
DeleteSessionValues ROUTINE ! Auto generated by Bryan's Template
    ! Local Variables
    p_web.DeleteSessionValue('Ans')
    p_web.DeleteSessionValue('LocRefNumber')
    p_web.DeleteSessionValue('locReturnType')
    p_web.DeleteSessionValue('locInvoiceNumber')
    p_web.DeleteSessionValue('locSalesNumber')
    p_web.DeleteSessionValue('locQuantity')
    p_web.DeleteSessionValue('locNotes')
    p_web.DeleteSessionValue('locDateReceived')

    ! Other Variables
    p_web.DeleteSessionValue('Prompt:locInvoiceNumber')
    p_web.DeleteSessionValue('Prompt:locQuantity')
Validate:locInvoiceNumber   ROUTINE
DATA
lReturnType        EQUATE('locReturnType')
lInvoiceNumber EQUATE('locInvoiceNumber')
lSalesNumber EQUATE('locSalesNumber')
lQuantity   EQUATE('locQuantity')
lQuantityHigh       EQUATE('locQuantityHigh')
Prompt:lInvoiceNumber       EQUATE('Prompt:locInvoiceNumber')
Prompt:lQuantity    EQUATE('Prompt:locQuantity')
CODE

    p_web.SSV(lQuantity,0)
    p_web.SSV(lSalesNumber,0)
    p_web.SSV(lQuantityHigh,0)
    p_web.SSV(Prompt:lInvoiceNumber,'')
    p_web.SSV(Prompt:lQuantity,'')

    Access:RETSALES.Clearkey(ret:Invoice_Number_Key)
    ret:Invoice_Number = p_web.GSV(lInvoiceNumber)
    IF (Access:RETSALES.TryFetch(ret:Invoice_Number_Key))
        p_web.SSV(Prompt:lInvoiceNumber,'Unable to find the selected Invoice Number')
        EXIT
    END

    DoNormalCheck# = 0
    If glo:WebJob
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = p_web.GSV('BookingAccountNumber')
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:StoresAccount <> ''
                If tra:StoresAccount <> ret:Account_Number
                    Error# = 1
                End !If tra:StoresAccount <> ret:Account_Number
            Else !If tra:Stores_Account <> ''
                DoNormalCheck# = 1
            End !If tra:Stores_Account <> ''
        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

    End !If glo:WebJob

    If DoNormalCheck#
        !Look up Head Account!
        Access:SubTrAcc.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = ret:Account_Number
        IF Access:SubTracc.Fetch(sub:Account_Number_Key)
          !Error!
        ELSE
            Access:TradeAcc.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = sub:Main_Account_Number
            IF Access:TradeAcc.Fetch(tra:Account_Number_Key)
                !Error!
            ELSE
                IF glo:Location <> tra:SiteLocation
                    Error# = 1
                END
            END
        END

    End !If DoNormalCheck#

    If Error# = 1
        p_web.SSV(Prompt:lInvoiceNumber,'This invoice number is not for this site location.')
        EXIT
    END

    ! How many of this part were on the retail sale?
    locAvailableQty# = 0
    found# = 0
    Access:RETSTOCK.Clearkey(res:DespatchedPartKey)
    res:Ref_Number = ret:Ref_Number
    res:Despatched = 'YES'
    res:Part_Number = p_web.GSV('sto:Part_Number')
    SET(res:DespatchedPartKey,res:DespatchedPartKey)
    LOOP UNTIL Access:RETSTOCK.Next()
        IF (res:Ref_Number <> ret:Ref_Number OR |
            res:Despatched <> 'YES' OR |
            res:Part_Number <> p_web.GSV('sto:Part_Number'))
            BREAK
        END
        found# = 1
        IF (res:Received <> 1)
            CYCLE
        END
        locAvailableQty# += res:QuantityReceived
        ! Don't think this should happen, but received dates could all be different, so pick the newest
        IF (p_web.GSV('locDateReceived') < res:DateReceived)
            p_web.SSV('locDateReceived',res:DateReceived)
        END

    END
    IF (found# = 0)
        p_web.SSV(Prompt:lInvoiceNumber,'The selected part is not on the entered invoice.')
        EXIT
    END

    IF (locAvailableQty# = 0)
        p_web.SSV(Prompt:lInvoiceNumber,'The selected order has not yet been received.')
        EXIT
    END

    ! How many of this part have already been ordered
    locAvailableQty# -= VodacomClass.ReturnUnitAlreadyOnOrder(p_web.GSV('sto:Ref_Number'),0,ret:Ref_Number)

    ! Don't allow to return more than is in stock
    IF (locAvailableQty# > p_web.GSV('sto:Quantity_Stock'))
        locAvailableQty# = p_web.GSV('sto:Quantity_Stock')
    END

    IF (locAvailableQty# <= 0)
        p_web.SSV(Prompt:lInvoiceNumber,'All the available quantity for this part has already been returned.')
        EXIT
    ELSE
        p_web.SSV(lQuantityHigh,locAvailableQty#)

        p_web.SSV(Prompt:lQuantity,'Qty Available: ' & locAvailableQty#)
    END

    p_web.SSV(Prompt:lInvoiceNumber,'Invoice Found.')
    p_web.SSV(lSalesNumber,ret:Ref_Number)
OpenFiles  ROUTINE
  p_web._OpenFile(RTNORDER)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(RETSALES)
  p_web._OpenFile(RETTYPES)
  p_web._OpenFile(STOCK)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(RTNORDER)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(RETSALES)
  p_Web._CloseFile(RETTYPES)
  p_Web._CloseFile(STOCK)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  DO DeleteSessionvalues
  
  !attempt to get at stm:RefNumber (in "BrowseID") from BrowseStoModel - did not work 
  !p_web.ssv('BrowseID','STOMODEL') flags this has come from STOMODEL screen
  if p_web.gsv('BrowseID') = 'STOMODEL' THEN
      Access:StoModel_Alias.clearkey(stm_ali:RecordNumberKey)
      stm_ali:RecordNumber = p_web.gsv('BrowseID2')
      if access:StoModel_Alias.fetch(stm_ali:RecordNumberKey)
          !error
      END
      LocRefNumber = stm_ali:Ref_Number
  ELSE
      LocRefNumber = p_web.gsv('BrowseID')
  END
  
  
  !now we can look up the stock
  Access:STOCK.ClearKey(sto:Ref_Number_Key)
  sto:Ref_Number = LocRefNumber       !was p_web.GSV('sto:Ref_Number')
  IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
      p_web.FileToSessionQueue(STOCK)
  
  END
  p_web.SetValue('FormReturnStock_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  DO DeleteSessionvalues

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('sto:Part_Number')
    p_web.SetPicture('sto:Part_Number','@s30')
  End
  p_web.SetSessionPicture('sto:Part_Number','@s30')
  If p_web.IfExistsValue('sto:Description')
    p_web.SetPicture('sto:Description','@s30')
  End
  p_web.SetSessionPicture('sto:Description','@s30')
  If p_web.IfExistsValue('locInvoiceNumber')
    p_web.SetPicture('locInvoiceNumber','@n_7')
  End
  p_web.SetSessionPicture('locInvoiceNumber','@n_7')
  If p_web.IfExistsValue('locQuantity')
    p_web.SetPicture('locQuantity','@n_7')
  End
  p_web.SetSessionPicture('locQuantity','@n_7')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('sto:Part_Number',sto:Part_Number)
  p_web.SetSessionValue('sto:Description',sto:Description)
  p_web.SetSessionValue('locReturnType',locReturnType)
  p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber)
  p_web.SetSessionValue('locSalesNumber',locSalesNumber)
  p_web.SetSessionValue('locQuantity',locQuantity)
  p_web.SetSessionValue('locNotes',locNotes)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('sto:Part_Number')
    sto:Part_Number = p_web.GetValue('sto:Part_Number')
    p_web.SetSessionValue('sto:Part_Number',sto:Part_Number)
  End
  if p_web.IfExistsValue('sto:Description')
    sto:Description = p_web.GetValue('sto:Description')
    p_web.SetSessionValue('sto:Description',sto:Description)
  End
  if p_web.IfExistsValue('locReturnType')
    locReturnType = p_web.GetValue('locReturnType')
    p_web.SetSessionValue('locReturnType',locReturnType)
  End
  if p_web.IfExistsValue('locInvoiceNumber')
    locInvoiceNumber = p_web.dformat(clip(p_web.GetValue('locInvoiceNumber')),'@n_7')
    p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber)
  End
  if p_web.IfExistsValue('locSalesNumber')
    locSalesNumber = p_web.GetValue('locSalesNumber')
    p_web.SetSessionValue('locSalesNumber',locSalesNumber)
  End
  if p_web.IfExistsValue('locQuantity')
    locQuantity = p_web.dformat(clip(p_web.GetValue('locQuantity')),'@n_7')
    p_web.SetSessionValue('locQuantity',locQuantity)
  End
  if p_web.IfExistsValue('locNotes')
    locNotes = p_web.GetValue('locNotes')
    p_web.SetSessionValue('locNotes',locNotes)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormReturnStock_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  ! Store return URL
  IF (p_web.IfExistsValue('ReturnURL'))
      p_web.StoreValue('ReturnURL')
  END
  
    ! #13128 Error if part not selected. (DBH: 18/03/2014)
    IF (p_web.GSV('BrowseID') = '')
        CreateScript(p_web,packet,'alert("Please select a part.");window.open("' & p_web.GSV('ReturnURL') & '","_self")')
        DO SendPacket
    END ! IF    
    
      p_web.site.SaveButton.TextValue = 'Return Stock'
      p_web.site.SaveButton.Class = 'button-entryfield'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locReturnType = p_web.RestoreValue('locReturnType')
 locInvoiceNumber = p_web.RestoreValue('locInvoiceNumber')
 locSalesNumber = p_web.RestoreValue('locSalesNumber')
 locQuantity = p_web.RestoreValue('locQuantity')
 locNotes = p_web.RestoreValue('locNotes')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('ReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormReturnStock_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormReturnStock_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormReturnStock_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ReturnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormReturnStock" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormReturnStock" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormReturnStock" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Return Stock') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Return Stock',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormReturnStock">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormReturnStock" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormReturnStock')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Details') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormReturnStock')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormReturnStock'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locReturnType')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormReturnStock')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Part Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormReturnStock_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Part_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Part_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sto:Part_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Description
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sto:Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locReturnType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locReturnType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locReturnType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locInvoiceNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locInvoiceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locInvoiceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSalesNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSalesNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locSalesNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locQuantity
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locQuantity
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locQuantity
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locNotes
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'30%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::sto:Part_Number  Routine
  p_web._DivHeader('FormReturnStock_' & p_web._nocolon('sto:Part_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Part Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Part_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Part_Number',p_web.GetValue('NewValue'))
    sto:Part_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = sto:Part_Number
    do Value::sto:Part_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Part_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sto:Part_Number = p_web.GetValue('Value')
  End

Value::sto:Part_Number  Routine
  p_web._DivHeader('FormReturnStock_' & p_web._nocolon('sto:Part_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:Part_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Part_Number'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::sto:Part_Number  Routine
    loc:comment = ''
  p_web._DivHeader('FormReturnStock_' & p_web._nocolon('sto:Part_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sto:Description  Routine
  p_web._DivHeader('FormReturnStock_' & p_web._nocolon('sto:Description') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Description')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Description  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Description',p_web.GetValue('NewValue'))
    sto:Description = p_web.GetValue('NewValue') !FieldType= STRING Field = sto:Description
    do Value::sto:Description
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Description',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sto:Description = p_web.GetValue('Value')
  End

Value::sto:Description  Routine
  p_web._DivHeader('FormReturnStock_' & p_web._nocolon('sto:Description') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:Description
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Description'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::sto:Description  Routine
    loc:comment = ''
  p_web._DivHeader('FormReturnStock_' & p_web._nocolon('sto:Description') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locReturnType  Routine
  p_web._DivHeader('FormReturnStock_' & p_web._nocolon('locReturnType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Return Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locReturnType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locReturnType',p_web.GetValue('NewValue'))
    locReturnType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locReturnType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locReturnType',p_web.GetValue('Value'))
    locReturnType = p_web.GetValue('Value')
  End
  If locReturnType = ''
    loc:Invalid = 'locReturnType'
    loc:alert = p_web.translate('Return Type') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locReturnType = Upper(locReturnType)
    p_web.SetSessionValue('locReturnType',locReturnType)
  do Value::locReturnType
  do SendAlert

Value::locReturnType  Routine
  p_web._DivHeader('FormReturnStock_' & p_web._nocolon('locReturnType') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locReturnType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locReturnType = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locReturnType'',''formreturnstock_locreturntype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locReturnType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locReturnType') = 0
    p_web.SetSessionValue('locReturnType','')
  end
    packet = clip(packet) & p_web.CreateOption('','',choose('' = p_web.getsessionvalue('locReturnType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(RTNORDER)
  bind(rtn:Record)
  p_web._OpenFile(SUBTRACC)
  bind(sub:Record)
  p_web._OpenFile(TRADEACC)
  bind(tra:Record)
  p_web._OpenFile(RETSALES)
  bind(ret:Record)
  p_web._OpenFile(RETTYPES)
  bind(rtt:Record)
  p_web._OpenFile(STOCK)
  bind(sto:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locReturnType_OptionView)
  locReturnType_OptionView{prop:filter} = 'rtt:Active = 1'
  locReturnType_OptionView{prop:order} = 'UPPER(rtt:Description)'
  Set(locReturnType_OptionView)
  Loop
    Next(locReturnType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locReturnType') = 0
      p_web.SetSessionValue('locReturnType',rtt:Description)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,rtt:Description,choose(rtt:Description = p_web.getsessionvalue('locReturnType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locReturnType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(RTNORDER)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(RETSALES)
  p_Web._CloseFile(RETTYPES)
  p_Web._CloseFile(STOCK)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormReturnStock_' & p_web._nocolon('locReturnType') & '_value')

Comment::locReturnType  Routine
    loc:comment = ''
  p_web._DivHeader('FormReturnStock_' & p_web._nocolon('locReturnType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locInvoiceNumber  Routine
  p_web._DivHeader('FormReturnStock_' & p_web._nocolon('locInvoiceNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Invoice Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locInvoiceNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locInvoiceNumber',p_web.GetValue('NewValue'))
    locInvoiceNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locInvoiceNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locInvoiceNumber',p_web.dFormat(p_web.GetValue('Value'),'@n_7'))
    locInvoiceNumber = p_web.Dformat(p_web.GetValue('Value'),'@n_7') !
  End
  If locInvoiceNumber = ''
    loc:Invalid = 'locInvoiceNumber'
    loc:alert = p_web.translate('Invoice Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locInvoiceNumber = Upper(locInvoiceNumber)
    p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber)
  DO Validate:locInvoiceNumber
  do Value::locInvoiceNumber
  do SendAlert
  do Value::locSalesNumber  !1
  do Comment::locInvoiceNumber
  do Value::locQuantity  !1
  do Comment::locQuantity

Value::locInvoiceNumber  Routine
  p_web._DivHeader('FormReturnStock_' & p_web._nocolon('locInvoiceNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locInvoiceNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locInvoiceNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locInvoiceNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locInvoiceNumber'',''formreturnstock_locinvoicenumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locInvoiceNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locInvoiceNumber',p_web.GetSessionValue('locInvoiceNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n_7',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormReturnStock_' & p_web._nocolon('locInvoiceNumber') & '_value')

Comment::locInvoiceNumber  Routine
    loc:comment = p_web.Translate(p_web.GSV('Prompt:locInvoiceNumber'))
  p_web._DivHeader('FormReturnStock_' & p_web._nocolon('locInvoiceNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormReturnStock_' & p_web._nocolon('locInvoiceNumber') & '_comment')

Prompt::locSalesNumber  Routine
  p_web._DivHeader('FormReturnStock_' & p_web._nocolon('locSalesNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Sales Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locSalesNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSalesNumber',p_web.GetValue('NewValue'))
    locSalesNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSalesNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locSalesNumber',p_web.GetValue('Value'))
    locSalesNumber = p_web.GetValue('Value')
  End

Value::locSalesNumber  Routine
  p_web._DivHeader('FormReturnStock_' & p_web._nocolon('locSalesNumber') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locSalesNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locSalesNumber'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormReturnStock_' & p_web._nocolon('locSalesNumber') & '_value')

Comment::locSalesNumber  Routine
    loc:comment = ''
  p_web._DivHeader('FormReturnStock_' & p_web._nocolon('locSalesNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locQuantity  Routine
  p_web._DivHeader('FormReturnStock_' & p_web._nocolon('locQuantity') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Quantity')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locQuantity  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locQuantity',p_web.GetValue('NewValue'))
    locQuantity = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locQuantity
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locQuantity',p_web.dFormat(p_web.GetValue('Value'),'@n_7'))
    locQuantity = p_web.Dformat(p_web.GetValue('Value'),'@n_7') !
  End
  If locQuantity = ''
    loc:Invalid = 'locQuantity'
    loc:alert = p_web.translate('Quantity') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  If Numeric(locQuantity) = 0
    loc:Invalid = 'locQuantity'
    loc:alert = p_web.translate('Quantity') & ' ' & p_web.translate(p_web.site.NumericText)
  ElsIf InRange(locQuantity,1,p_web.GSV('locQuantityHigh')) = false
    loc:Invalid = 'locQuantity'
    loc:alert = clip('Quantity') & ' ' & p_web.translate(p_web.site.MoreThanText) & ' ' & 1 & ', ' & p_web.translate(p_web.site.LessThanText) & ' ' & p_web.GSV('locQuantityHigh')
  End
  do Value::locQuantity
  do SendAlert

Value::locQuantity  Routine
  p_web._DivHeader('FormReturnStock_' & p_web._nocolon('locQuantity') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locQuantity
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locQuantity')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locQuantity = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
    If Numeric(locQuantity) = 0
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    Else
      If locQuantity < 1
        loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
      End
      If locQuantity > p_web.GSV('locQuantityHigh')
        loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
      End
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locQuantity'',''formreturnstock_locquantity_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locQuantity',p_web.GetSessionValue('locQuantity'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n_7',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormReturnStock_' & p_web._nocolon('locQuantity') & '_value')

Comment::locQuantity  Routine
    loc:comment = p_web.Translate(p_web.GSV('Prompt:LocQuantity'))
  p_web._DivHeader('FormReturnStock_' & p_web._nocolon('locQuantity') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormReturnStock_' & p_web._nocolon('locQuantity') & '_comment')

Prompt::locNotes  Routine
  p_web._DivHeader('FormReturnStock_' & p_web._nocolon('locNotes') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Notes')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locNotes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locNotes',p_web.GetValue('NewValue'))
    locNotes = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locNotes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locNotes',p_web.GetValue('Value'))
    locNotes = p_web.GetValue('Value')
  End
    locNotes = Upper(locNotes)
    p_web.SetSessionValue('locNotes',locNotes)
  do Value::locNotes
  do SendAlert

Value::locNotes  Routine
  p_web._DivHeader('FormReturnStock_' & p_web._nocolon('locNotes') & '_value','adiv')
  loc:extra = ''
  ! --- TEXT --- locNotes
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locNotes')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locNotes'',''formreturnstock_locnotes_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('locNotes',p_web.GetSessionValue('locNotes'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,255,,Net:Web:Control) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormReturnStock_' & p_web._nocolon('locNotes') & '_value')

Comment::locNotes  Routine
      loc:comment = ''
  p_web._DivHeader('FormReturnStock_' & p_web._nocolon('locNotes') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormReturnStock_locReturnType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locReturnType
      else
        do Value::locReturnType
      end
  of lower('FormReturnStock_locInvoiceNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locInvoiceNumber
      else
        do Value::locInvoiceNumber
      end
  of lower('FormReturnStock_locQuantity_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locQuantity
      else
        do Value::locQuantity
      end
  of lower('FormReturnStock_locNotes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locNotes
      else
        do Value::locNotes
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormReturnStock_form:ready_',1)
  p_web.SetSessionValue('FormReturnStock_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormReturnStock',0)

PreCopy  Routine
  p_web.SetValue('FormReturnStock_form:ready_',1)
  p_web.SetSessionValue('FormReturnStock_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormReturnStock',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormReturnStock_form:ready_',1)
  p_web.SetSessionValue('FormReturnStock_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormReturnStock:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormReturnStock_form:ready_',1)
  p_web.SetSessionValue('FormReturnStock_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormReturnStock:Primed',0)
  p_web.setsessionvalue('showtab_FormReturnStock',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormReturnStock_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  Access:STOCK.ClearKey(sto:Ref_Number_Key)
  sto:Ref_Number = p_web.GSV('sto:Ref_Number')
  IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
  
      Access:RETTYPES.Clearkey(rtt:DescriptionKey)
      rtt:Description = p_web.GSV('locReturnType')
      IF (Access:RETTYPES.TryFetch(rtt:DescriptionKey) = Level:Benign)
          IF (rtt:UseReturnDays)
              IF (p_web.GSV('locDateReceived') + rtt:SparesReturnDays < TODAY())
                  loc:alert = 'Part cannot be returned as was received at Franchise over the set period.'
                  loc:invalid = 'locReturnType'
                  EXIT
  
              END
          END ! IF (rtt:UseReturnDays)
      END
  
      IF (p_web.GSV('locQuantity') > sto:Quantity_Stock)
          loc:alert = 'The quantity entered is greater than the quantity in stock.'
          loc:invalid = 'locQuantity'
          EXIT
  
      END
  
  
  
  
      Pointer# = Pointer(STOCK)
      Hold(STOCK,1)
      Get(STOCK,Pointer#)
      If Errorcode() = 43
          loc:Alert = 'This stock item is current in use by another station'
          loc:invalid = 'loc:Quantity'
          EXIT
      END
  
      Access:RETSALES.Clearkey(ret:Invoice_Number_Key)
      ret:Invoice_Number = p_web.GSV('locInvoiceNumber')
      IF (Access:RETSALES.TryFetch(ret:Invoice_Number_Key))
      END
  
      sto:Quantity_Stock -= p_web.GSV('locQuantity')
      IF (sto:Quantity_Stock < 0)
          sto:Quantity_Stock = 0
      END
  
      IF (Access:STOCK.TryUpdate() = Level:Benign)
      ! Does an item already exist for this order?
          Access:RTNORDER.Clearkey(rtn:OrderStatusReturnRefNoKey)
          rtn:OrderNumber = ret:Ref_Number
          rtn:ExchangeOrder = ret:ExchangeOrder
          rtn:ReturnType = p_web.GSV('locReturnType')
          rtn:Status = 'NEW'
          rtn:RefNumber = sto:Ref_Number
          IF (Access:RTNORDER.TryFetch(rtn:OrderStatusReturnRefNoKey) = Level:Benign)
              rtn:QuantityReturned += p_web.GSV('locQuantity')
              rtn:Notes = CLIP(rtn:Notes) & '<13,10>' & p_web.GSV('locNotes')
              Access:RTNORDER.TryUpdate()
          ELSE
          ! Create Return Order
              IF (Access:RTNORDER.PrimeRecord() = Level:Benign)
                  rtn:Location = p_web.GSV('BookingSiteLocation')
                  rtn:UserCode = p_web.GSV('BookingUserCode')
                  rtn:RefNumber = sto:Ref_Number
                  rtn:OrderNumber = ret:Ref_Number
                  rtn:InvoiceNumber = ret:Invoice_Number
                  rtn:PartNumber = sto:Part_Number
                  rtn:Description = sto:Description
                  rtn:QuantityReturned = p_web.GSV('locQuantity')
                  rtn:ExchangeOrder = ret:ExchangeOrder
                  rtn:Status = 'NEW'
                  rtn:Notes = locNotes
                  rtn:ReturnType = locReturnType
                  rtn:PurchaseCost = sto:Purchase_Cost
                  rtn:SaleCost = sto:Sale_Cost
                  IF (Access:RTNORDER.TryInsert())
                      Access:RTNORDER.CancelAutoInc()
                  END
              END
          END
          If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
              'DEC', | ! Transaction_Type
              '', | ! Depatch_Note_Number
              0, | ! Job_Number
              0, | ! Sales_Number
              p_web.GSV('locQuantity'), | ! Quantity
              sto:Purchase_Cost, | ! Purchase_Cost
              sto:Sale_Cost, | ! Sale_Cost
              sto:Retail_Cost, | ! Retail_Cost
              'RETURN ORDER GENERATED', | ! Notes
              '', |
              p_web.GSV('BookingUserCode'), |
              sto:Quantity_stock) ! Information
                    ! Added OK
          Else ! AddToStockHistory
                    ! Error
          End ! AddToStockHistory
      END
      RELEASE(STOCK)
  
  END
  p_web.DeleteSessionValue('FormReturnStock_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
        If locReturnType = ''
          loc:Invalid = 'locReturnType'
          loc:alert = p_web.translate('Return Type') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locReturnType = Upper(locReturnType)
          p_web.SetSessionValue('locReturnType',locReturnType)
        If loc:Invalid <> '' then exit.
        If locInvoiceNumber = ''
          loc:Invalid = 'locInvoiceNumber'
          loc:alert = p_web.translate('Invoice Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locInvoiceNumber = Upper(locInvoiceNumber)
          p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber)
        If loc:Invalid <> '' then exit.
        If locQuantity = ''
          loc:Invalid = 'locQuantity'
          loc:alert = p_web.translate('Quantity') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If Numeric(locQuantity) = 0
          loc:Invalid = 'locQuantity'
          loc:alert = p_web.translate('Quantity') & ' ' & p_web.translate(p_web.site.NumericText)
        ElsIf InRange(locQuantity,1,p_web.GSV('locQuantityHigh')) = false
          loc:Invalid = 'locQuantity'
          loc:alert = clip('Quantity') & ' ' & p_web.translate(p_web.site.MoreThanText) & ' ' & 1 & ', ' & p_web.translate(p_web.site.LessThanText) & ' ' & p_web.GSV('locQuantityHigh')
        End
        If loc:Invalid <> '' then exit.
          locNotes = Upper(locNotes)
          p_web.SetSessionValue('locNotes',locNotes)
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormReturnStock:Primed',0)
  p_web.StoreValue('sto:Part_Number')
  p_web.StoreValue('sto:Description')
  p_web.StoreValue('locReturnType')
  p_web.StoreValue('locInvoiceNumber')
  p_web.StoreValue('locSalesNumber')
  p_web.StoreValue('locQuantity')
  p_web.StoreValue('locNotes')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('LocRefNumber',LocRefNumber) ! LONG
     p_web.SSV('locReturnType',locReturnType) ! STRING(30)
     p_web.SSV('locInvoiceNumber',locInvoiceNumber) ! STRING(30)
     p_web.SSV('locSalesNumber',locSalesNumber) ! STRING(30)
     p_web.SSV('locQuantity',locQuantity) ! LONG
     p_web.SSV('locNotes',locNotes) ! STRING(255)
     p_web.SSV('locDateReceived',locDateReceived) ! DATE
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     LocRefNumber = p_web.GSV('LocRefNumber') ! LONG
     locReturnType = p_web.GSV('locReturnType') ! STRING(30)
     locInvoiceNumber = p_web.GSV('locInvoiceNumber') ! STRING(30)
     locSalesNumber = p_web.GSV('locSalesNumber') ! STRING(30)
     locQuantity = p_web.GSV('locQuantity') ! LONG
     locNotes = p_web.GSV('locNotes') ! STRING(255)
     locDateReceived = p_web.GSV('locDateReceived') ! DATE
