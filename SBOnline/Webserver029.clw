

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER029.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
SaveJob              PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
!region Processed Code
        do openFiles

        Access:WEBJOB.Clearkey(wob:refNumberKey)
        wob:refNumber    = p_web.GSV('wob:RefNumber')
        if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
        ! Found
            p_web.SessionQueueToFile(WEBJOB)
            
            IF (access:WEBJOB.tryUpdate())
            END ! IF
            
        else ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)
        ! Error
        end ! if (Access:WEBJOB.TryFetch(wob:refNumberKey) = Level:Benign)

        Access:JOBS.Clearkey(job:ref_Number_Key)
        job:ref_Number    = p_web.GSV('wob:RefNumber')
        if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
        ! Found
            p_web.SessionQueueToFile(JOBS)
            access:JOBS.tryupdate()
        else ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
        ! Error
        end ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)

        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber    = p_web.GSV('wob:RefNumber')
        if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
        ! Found
            p_web.SessionQueueToFile(JOBSE)
            access:JOBSE.tryupdate()
        else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
        ! Error
        end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)


        Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
        jobe2:RefNumber    = p_web.GSV('wob:RefNumber')
        if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
        ! Found
            p_web.SessionQueueToFile(JOBSE2)
            access:JOBSE2.tryupdate()
        else ! if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
        ! Error
        end ! if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
        
        Access:JOBSE3.ClearKey(jobe3:KeyRefNumber)
        jobe3:RefNumber = p_web.GSV('wob:RefNumber')
        IF (Access:JOBSE3.TryFetch(jobe3:KeyRefNumber) = Level:Benign)
            p_web.SessionQueueToFile(JOBSE3)
            Access:JOBSE3.TryUpdate()
        ELSE ! IF
        END ! IF


        Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
        jbn:RefNumber    = p_web.GSV('wob:RefNumber')
        if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
        ! Found
            p_web.SessionQueueToFile(JOBNOTES)
            if access:JOBNOTES.tryupdate()
            end
        else ! if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
        ! Error
        end ! if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)

        do closeFiles
!endRegion
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBSE3.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE3.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBNOTES.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBNOTES.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE2.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE2.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WEBJOB.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WEBJOB.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBSE3.Close
     Access:JOBS.Close
     Access:JOBSE.Close
     Access:JOBNOTES.Close
     Access:JOBSE2.Close
     Access:WEBJOB.Close
     FilesOpened = False
  END
