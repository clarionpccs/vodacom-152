

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER038.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
createWebOrder       PROCEDURE  (fAccountNumber,fPartNumber,fDescription,fQuantity,fRetailCost) ! Declare Procedure
locAccountNumber     STRING(30)                            !
TRADEACC::State  USHORT
ORDWEBPR::State  USHORT
FilesOpened     BYTE(0)

  CODE
    do openFiles
    do saveFiles

    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number    = fAccountNumber
    if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
        ! Found
        if (tra:StoresAccount <> '')
            locAccountNumber = tra:StoresAccount
        else ! if (tra:StoresAccount <> '')
            locAccountNumber = tra:Account_Number
        end ! if (tra:StoresAccount <> '')
    else ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
        ! Error
    end ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)

    found# = 0
    Access:ORDWEBPR.Clearkey(orw:PartNumberKey)
    orw:AccountNumber    = locAccountNumber
    orw:PartNumber    = fPartNumber
    set(orw:PartNumberKey,orw:PartNumberKey)
    loop
        if (Access:ORDWEBPR.Next())
            Break
        end ! if (Access:ORDWEBPR.Next())
        if (orw:AccountNumber    <> locAccountNumber)
            Break
        end ! if (orw:AccountNumber    <> locAccountNumber)
        if (orw:PartNumber    <> fPartNumber)
            Break
        end ! if (orw:PartNumber    <> fPartNumber)
        if (orw:Description = fDescription)
            orw:Quantity += fQuantity
            access:ORDWEBPR.tryUpdate()
            found# = 1
            break
        end ! if (orw:Description = fDescription)
    end ! loop

    if (found# = 0)
        if (Access:ORDWEBPR.PrimeRecord() = Level:Benign)
            orw:AccountNumber    = locAccountNumber
            orw:PartNumber    = fPartNumber
            orw:Quantity    = fQuantity
            orw:ItemCost    = fRetailCost
            orw:Description = fDescription
            if (Access:ORDWEBPR.TryInsert() = Level:Benign)
                ! Inserted
            else ! if (Access:ORDWEBPR.TryInsert() = Level:Benign)
                ! Error
                Access:ORDWEBPR.CancelAutoInc()
            end ! if (Access:ORDWEBPR.TryInsert() = Level:Benign)
        end ! if (Access:ORDWEBPR.PrimeRecord() = Level:Benign)
    end !if (found# = 0)

    do restoreFiles
    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:ORDWEBPR.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:ORDWEBPR.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TRADEACC.Close
     Access:ORDWEBPR.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  TRADEACC::State = Access:TRADEACC.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  ORDWEBPR::State = Access:ORDWEBPR.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF TRADEACC::State <> 0
    Access:TRADEACC.RestoreFile(TRADEACC::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF ORDWEBPR::State <> 0
    Access:ORDWEBPR.RestoreFile(ORDWEBPR::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
