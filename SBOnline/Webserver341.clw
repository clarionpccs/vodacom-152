

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER341.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ClearUpdateJobVariables PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
! Clear Variables
    p_web.SSV('FirstTime',1)
    p_web.SSV('Job:ViewOnly',0)
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBACC.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBACC.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBACC.Close
     FilesOpened = False
  END
