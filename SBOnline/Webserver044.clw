

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER044.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
InvoiceNote PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
locJobNumber         LONG                                  !
locDetail            STRING(10000)                         !
locCompanyName       STRING(30)                            !
locAddressLine1      STRING(30)                            !
locAddressLine2      STRING(30)                            !
locAddressLine3      STRING(30)                            !
locPostcode          STRING(30)                            !
locTelephoneNumber   STRING(30)                            !
locCustCompanyName   STRING(30)                            !
locCustAddressLine1  STRING(30)                            !
locCustAddressLine2  STRING(30)                            !
locCustAddressLine3  STRING(30)                            !
locCustPostcode      STRING(30)                            !
locCustTelephoneNumber STRING(30)                          !
locCustVatNumber     STRING(30)                            !
locSubTotal          REAL                                  !
locVat               REAL                                  !
locTotal             REAL                                  !
locInvoiceTime       TIME                                  !
locInvoiceDate       DATE                                  !
loc:PDFFilename      STRING(255)                           !
Process:View         VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT,AT(0,0,8430,8230),PRE(RPT),PAPER(PAPER:USER,8430,8230),LANDSCAPE,FONT('Arial',10,,FONT:regular, |
  CHARSET:ANSI),THOUS
Detail                 DETAIL,AT(0,0,8430,8230),USE(?Detail),ABSOLUTE
                         TEXT,AT(375,-10,7656,7800),USE(locDetail),FONT('Courier New',11)
                       END
ANewPage               DETAIL,AT(0,0,8427,83),USE(?ANewPage),PAGEAFTER(1)
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR1               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END

                    MAP
addToDetail             PROCEDURE(<STRING textString>)
newLine                 PROCEDURE(<LONG number>)
PrintReport             PROCEDURE(BYTE fCredit)
CheckCreateInvoiceRRC   PROCEDURE(BYTE fCredit)
CheckCreateInvoiceARC   PROCEDURE(BYTE fCredit)
                    END

Detail_Queue        Queue,Pre(detail)
Line                    String(56)
Filler1                 String(4)
Amount                  String(20)
                    End ! Detail_Queue        Queue,Pre(detail)

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
CreatePartsList     Routine
    Data
local:FaultDescription  String(255)
    Code
        Free(Detail_Queue)
        Clear(Detail_Queue)

        local:FaultDescription = Clip(BHStripReplace(p_web.GSV('jbn:Fault_Description'),'<13,10>','. '))

        If Clip(local:FaultDescription) <> ''
            detail:Line = Clip(Sub(local:FaultDescription,1,55))
            detail:Amount = ''
            Add(Detail_Queue)
            detail:Line = Clip(Sub(local:FaultDescription,56,55))
            detail:Amount = ''
            Add(Detail_Queue)
            If p_web.GSV('jobe2:InvDiscountAmnt') <> 0 then
                detail:Line = 'OOW Discount ' & Format(p_web.GSV('jobe2:InvDiscountAmnt'),@n_10.2)
                detail:Amount = ''
                Add(Detail_Queue)
            Else
                detail:Line = ''
                detail:Amount = ''
                Add(Detail_Queue)
            End            
        Else ! If Clip(jbn:Fault_Description) <> ''
            detail:Line = ''
            detail:Amount = ''
            Add(Detail_Queue)
            detail:Line = ''
            detail:Amount = ''
            Add(Detail_Queue)
            If p_web.GSV('jobe2:InvDiscountAmnt') <> 0 then
                detail:Line = 'OOW Discount ' & Format(p_web.GSV('jobe2:InvDiscountAmnt'),@n_10.2)
                detail:Amount = ''
                Add(Detail_Queue)
            Else
                detail:Line = ''
                detail:Amount = ''
                Add(Detail_Queue)
            End            
        End ! If Clip(jbn:Fault_Description) <> ''

        detail:Line = ALL('-',55)
        detail:Amount = ''
        Add(Detail_Queue)

    ! Show Parts (DBH: 27/07/2007)
        RowNum# = 0
        Access:PARTS.Clearkey(par:Order_Number_Key)
        par:Ref_Number = p_web.GSV('job:Ref_Number')
        Set(par:Order_Number_Key,par:Order_Number_Key)
        Loop ! Begin Loop
            If Access:PARTS.Next()
                Break
            End ! If Access:PARTS.Next()
            If par:Ref_Number <> p_web.GSV('job:Ref_Number')
                Break
            End ! If par:Ref_Number <> job:Ref_Number
            RowNum# += 1
            detail:Line = ' ' & Format(RowNum#,@n2) & ' ' & Sub(par:Description,1,50)
            detail:Amount = ''
            Add(Detail_Queue)
        End ! Loop

    ! Show Fault Codes (DBH: 27/07/2007)
        Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
        joo:JobNumber = p_web.GSV('job:Ref_Number')
        Set(joo:JobNumberKey,joo:JobNumberKey)
        Loop ! Begin Loop
            If Access:JOBOUTFL.Next()
                Break
            End ! If Access:JOBOUTFL.Next()
            If joo:JobNumber <> p_web.GSV('job:Ref_Number')
                Break
            End ! If joo:JobNumber <> job:Ref_Number
            If Instring('IMEI VALIDATION',joo:Description,1,1)
                Cycle
            End ! If Instring('IMEI VALIDATION',joo:Description,1,1)
            RowNum# += 1
            detail:Line = ' ' & Format(RowNum#,@n2) & ' ' & Sub(joo:Description,1,50)
            detail:Amount = ''
            Add(Detail_Queue)
        End ! Loop

!    ! I guess this is filling in the blank lines (DBH: 27/07/2007)
!        detail:Line = ''
!        detail:Amount = ''
!        Loop
!            If Records(Detail_Queue) % prn:DetailRows = 0
!                Break
!            End ! If Records(Detail_Queue) % prn:DetailRows = 0
!            Add(Detail_Queue)
!        End ! Loop

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('InvoiceNote')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  locJobNumber = p_web.GSV('job:Ref_Number')
  
  Relate:AUDIT.SetOpenRelated()
  Relate:AUDIT.Open                                        ! File AUDIT used by this procedure, so make sure it's RelationManager is open
  Access:JOBSE2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBOUTFL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:INVOICE.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:WARPARTS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PARTS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('InvoiceNote',ProgressWindow)               ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisReport.AddSortOrder(job:Ref_Number_Key)
  ThisReport.AddRange(job:Ref_Number,locJobNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
  END
  IF SELF.Opened
    INIMgr.Update('InvoiceNote',ProgressWindow)            ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'InvoiceNote',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.Init(Report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,Report{PROPPRINT:Paper},PAPER:USER), CHOOSE(Report{PROP:Thous}=True,PROP:Thous,CHOOSE(Report{PROP:MM}=True,PROP:MM,CHOOSE(Report{PROP:Points}=True,PROP:Points,0))), Report{PROPPRINT:PaperWidth}, Report{PROPPRINT:PaperHeight}, Report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetTitle('Invoice')                  !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR1.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetViewerPrefs(PDFXTR1:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'InvoiceNote',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR1.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords

addToDetail         PROCEDURE(<STRING textString>)
    CODE
        locDetail = CLIP(locDetail) & textString
        
newLine             PROCEDURE(<LONG number>)
    CODE
        IF (number = 0)
            locDetail = CLIP(locDetail) & '<13,10>'
        ELSE
            LOOP xx# = 1 TO number
                locDetail = CLIP(locDetail) & '<13,10>'
            END
        END
        
CheckCreateInvoiceARC       Procedure(BYTE fCredit)
local:AuditNotes        String(255)
    Code
        If inv:ARCInvoiceDate = 0
        ! Create an RRC Invoice (DBH: 27/07/2007)
            inv:ARCInvoiceDate = Today()

            locInvoiceTime = Clock()

            p_web.SSV('job:Invoice_Labour_Cost',p_web.GSV('job:Labour_Cost'))
            p_web.SSV('job:Invoice_Courier_Cost',p_web.GSV('job:Courier_Cost'))
            p_web.SSV('job:Invoice_Parts_Cost',p_web.GSV('job:Parts_Cost'))
            p_web.SSV('job:Invoice_Sub_Total',p_web.GSV('job:Invoice_Labour_Cost') + p_web.GSV('job:Invoice_Parts_Cost') |
                + p_web.GSV('job:Invoice_Courier_Cost'))
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = p_web.GSV('jobe:RefNumber')
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                p_web.SessionQueueToFile(JOBSE)
                Access:JOBSE.tryupdate()
            END
            

            inv:ExportedARCOracle = 1

            Access:INVOICE.Update()

            Line500_XML(p_web,'AOW')

        ! Add Relevant Audit Entry (DBH: 27/07/2007)
            local:AuditNotes = 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
            LoanAttachedToJob(p_web)
            
            If p_web.GSV('LoanAttachedToJob') = 1
                local:AuditNotes = Clip(local:AuditNotes) & '<13,10>LOST LOAN FEE: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
            End ! If LoanAttachedToJob(p_web.GSV('job:Ref_Number)

        ! Show replacement charge (DBH: 27/07/2007)
            If (p_web.GSV('jobe:Engineer48HourOption') = 1)
                If (p_web.GSV('jobe:ExcReplcamentCharge') = 1)
                    local:AuditNotes = Clip(local:AuditNotes) & '<13,10>EXC REPL: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
                End ! If p_web.GSV('jobe:ExcReplcamentCharge
            End  ! If p_web.GSV('jobe:Engineer48HourOption = 1
            
           
            local:AuditNotes = Clip(local:AuditNotes) & '<13,10>PARTS: ' & Format(p_web.GSV('job:Invoice_Parts_Cost'),@n14.2) & |
                '<13,10>LABOUR: ' & Format(p_web.GSV('job:Invoice_Labour_Cost'),@n14.2) & |
                '<13,10>SUB TOTAL: ' & Format(p_web.GSV('job:Invoice_Sub_Total'),@n14.2) & |
                '<13,10>VAT: ' & Format((p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100) + |
                (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
                (p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100),@n14.2) & |
                '<13,10>TOTAL: ' & Format(p_web.GSV('job:Invoice_Sub_Total') + (p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100) + |
                (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
                (p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100),@n14.2)
                    

            
!            p_web.SSV('AddToAudit:Type','JOB') 
!            p_web.SSV('AddToAudit:Action','INVOICE CREATED')
!            p_web.SSV('AddToAudit:Notes',Clip(local:AuditNotes))
            AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','INVOICE CREATED',Clip(local:AuditNotes))
        Else ! If inv:RRCInvoiceDate = 0
        ! Reprint (DBH: 27/07/2007)
        ! Normal Invoice Reprint (DBH: 27/07/2007)
!            p_web.SSV('AddToAudit:Type','JOB') 
!            p_web.SSV('AddToAudit:Action','INVOICE REPRINTED')
!            p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
!                '<13,10>INVOICE DATE: ' & FOrmat(inv:ARCInvoiceDate,@d06) )
            AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB', 'INVOICE REPRINTED','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
                '<13,10>INVOICE DATE: ' & FOrmat(inv:ARCInvoiceDate,@d06) ) 
              

            locInvoiceTime = p_web.GSV('job:Time_Booked')
            Access:AUDIT.Clearkey(aud:TypeActionKey)
            aud:Ref_Number = p_web.GSV('job:Ref_Number')
            aud:Type = 'JOB'
            aud:Action = 'INVOICE CREATED'
            aud:Date = inv:ARCInvoiceDate
            Set(aud:TypeActionKey,aud:TypeActionKey)
            Loop ! Begin Loop
                If Access:AUDIT.Next()
                    Break
                End ! If Access:AUDIT.Next()
                If aud:Ref_Number <> p_web.GSV('job:Ref_Number')
                    Break
                End ! If aud:Ref_Number <> p_web.GSV('job:Ref_Number
                If aud:Type <> 'JOB'
                    Break
                End ! If aud:Type <> 'JOB'
                If aud:Action <> 'INVOICE CREATED'
                    Break
                End ! If aud:Action <> 'INVOICE CREATED'
                If aud:Date <> inv:ARCInvoiceDate
                    Break
                End ! If aud:Date <> inv:RRCInvoiceDate
                locInvoiceTime = aud:Time
                Break
            End ! Loop
        End ! If inv:RRCInvoiceDate = 0

        locVAT = (p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100) + |
            (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
            (p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100)
        locSubTotal = p_web.GSV('job:Invoice_Sub_Total')
        locTotal = locSubTotal + locVAT

CheckCreateInvoiceRRC       Procedure(BYTE fCredit)
local:AuditNotes                String(255)
Code
    If inv:RRCInvoiceDate = 0
        ! Create an RRC Invoice (DBH: 27/07/2007)
        inv:RRCInvoiceDate = Today()

        locInvoiceTime = Clock()

        p_web.SSV('jobe:InvRRCCLabourCost',p_web.GSV('jobe:RRCCLabourCost'))
        p_web.SSV('jobe:InvRRCCPartsCost',p_web.GSV('jobe:RRCCPartsCost'))
        p_web.SSV('jobe:InvRRCCSubTotal',p_web.GSV('jobe:RRCCSubTotal'))
        p_web.SSV('jobe:InvoiceHandlingFee',p_web.GSV('jobe:HandlingFee'))
        p_web.SSV('jobe:InvoiceExchangeRate',p_web.GSV('jobe:ExchangeRate'))
        p_web.SSV('jobe2:InvDiscountAmnt',p_web.GSV('jobe2:JobDiscountAmnt'))

        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = p_web.GSV('jobe:RefNumber')
        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = level:Benign)
            p_web.SessionQueueToFile(JOBSE)
            Access:JOBSE.TryUpdate()
        END
            
        Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
        jobe2:RefNumber = p_web.GSV('jobe2:RefNumber')
        IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
            p_web.SessionQueueToFile(JOBSE2)
            Access:JOBSE2.TryUpdate()
        END
            

        inv:ExportedRRCOracle = 1

        Access:INVOICE.Update()

!    If f:Suffix = '' And f:Prefix = ''
!            ! Don't export credit notes (DBH: 02/08/2007)
!        Line500_XML('RIV')
!    End ! If f:Suffix = '' And f:Prefix = ''

        ! Add Relevant Audit Entry (DBH: 27/07/2007)
        local:AuditNotes = 'INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
            
        LoanAttachedToJob(p_web)
        If (p_web.GSV('LoanAttachedToJob') = 1)
            local:AuditNotes = Clip(local:AuditNotes) & '<13,10>LOST LOAN FEE: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
        End ! If LoanAttachedToJob(job:Ref_Number)

        ! Show replacement charge (DBH: 27/07/2007)
        If p_web.GSV('jobe:Engineer48HourOption') = 1
            If p_web.GSV('jobe:ExcReplcamentCharge') = 1
                local:AuditNotes = Clip(local:AuditNotes) & '<13,10>EXC REPL: ' & Format(p_web.GSV('job:Invoice_Courier_Cost'),@n14.2)
            End ! If jobe:ExcReplcamentCharge
        End ! If jobe:Engineer48HourOption = 1

        local:AuditNotes = Clip(local:AuditNotes) & '<13,10>PARTS: ' & Format(p_web.GSV('jobe:InvRRCCPartsCost'),@n14.2) & |
            '<13,10>LABOUR: ' & Format(p_web.GSV('jobe:InvRRCCLabourCost'),@n14.2) & |
            '<13,10>SUB TOTAL: ' & Format(p_web.GSV('jobe:InvRRCCSubTotal'),@n14.2) & |
            '<13,10>VAT: ' & Format((p_web.GSV('jobe:InvRRCCPartsCOst') * inv:RRCVatRateParts/100) + |
            (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
            (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100),@n14.2) & |
            '<13,10>TOTAL: ' & Format(p_web.GSV('jobe:InvRRCCSubTotal') + (p_web.GSV('jobe:InvRRCCPartsCost') * inv:RRCVatRateParts/100) + |
            (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
            (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100),@n14.2)
            
!        p_web.SSV('AddToAudit:Type','JOB')
!        p_web.SSV('AddToAudit:Notes',CLIP(local:AuditNotes))
!        p_web.SSV('AddToAudit:Action','INVOICE CREATED')
        AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','INVOICE CREATED',CLIP(local:AuditNotes))

    Else ! If inv:RRCInvoiceDate = 0
        ! Reprint (DBH: 27/07/2007)
            
        IF (fCredit = 2)
                
            ! Credit Note (DBH: 27/07/2007)
!            p_web.SSV('AddToAudit:Notes','CREDIT NOTE: ' & Clip('CN') & Clip(jov:InvoiceNumber) & '-' & Clip(tra:BranchIdentification) & Clip(p_web.GSV('InvoiceSuffix')) & |
!                'AMOUNT: ' & Format(jov:CreditAmount,@n14.2))
!            p_web.SSV('AddToAudit:Action','CREDIT NOTE PRINTED')
!            p_web.SSV('AddToAudit:Type','JOB')
            AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','CREDIT NOTE PRINTED','CREDIT NOTE: ' & Clip('CN') & Clip(jov:InvoiceNumber) & '-' & Clip(tra:BranchIdentification) & Clip(p_web.GSV('InvoiceSuffix')) & |
                'AMOUNT: ' & Format(jov:CreditAmount,@n14.2))
        Else ! If f:Prefix <> ''
            If (fCredit = 1)
                ! Credit Invoice (DBH: 27/07/2007)
!                p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(jov:InvoiceNumber) & '-' & Clip(tra:BranchIdentification) & Clip(p_web.GSV('InvoiceSuffix')) & |
!                    'AMOUNT: ' & Format(jov:NewTotalCost,@n14.2))
!                p_web.SSV('AddToAudit:Action','CREDITED INVOICE PRINTED')
!                p_web.SSV('AddToAudit:Type','JOB')
                AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','CREDITED INVOICE PRINTED','INVOICE NUMBER: ' & Clip(jov:InvoiceNumber) & '-' & Clip(tra:BranchIdentification) & Clip(p_web.GSV('InvoiceSuffix')) & |
                    'AMOUNT: ' & Format(jov:NewTotalCost,@n14.2))                   
            Else ! If f:Suffix <> ''
                ! Normal Invoice Reprint (DBH: 27/07/2007)
!                p_web.SSV('AddToAudit:Type','JOB')
!                p_web.SSV('AddToAudit:Notes','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
!                    '<13,10>INVOICE DATE: ' & FOrmat(inv:RRCInvoiceDate,@d06) )
!                p_web.SSV('AddToAudit:Action','INVOICE REPRINTED')
                AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','INVOICE REPRINTED','INVOICE NUMBER: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & |
                    '<13,10>INVOICE DATE: ' & FOrmat(inv:RRCInvoiceDate,@d06) )           

                locInvoiceTime = p_web.GSV('job:Time_Booked')
                Access:AUDIT.Clearkey(aud:TypeActionKey)
                aud:Ref_Number = p_web.GSV('job:Ref_Number')
                aud:Type = 'JOB'
                aud:Action = 'INVOICE CREATED'
                aud:Date = inv:RRCInvoiceDate
                Set(aud:TypeActionKey,aud:TypeActionKey)
                Loop ! Begin Loop
                    If Access:AUDIT.Next()
                        Break
                    End ! If Access:AUDIT.Next()
                    If aud:Ref_Number <> p_web.GSV('job:Ref_Number')
                        Break
                    End ! If aud:Ref_Number <> job:Ref_Number
                    If aud:Type <> 'JOB'
                        Break
                    End ! If aud:Type <> 'JOB'
                    If aud:Action <> 'INVOICE CREATED'
                        Break
                    End ! If aud:Action <> 'INVOICE CREATED'
                    If aud:Date <> inv:RRCInvoiceDate
                        Break
                    End ! If aud:Date <> inv:RRCInvoiceDate
                    locInvoiceTime = aud:Time
                    Break
                End ! Loop


            End ! If f:Suffix <> ''
        End ! If f:Prefix <> ''
    End ! If inv:RRCInvoiceDate = 0

    locVAT = (p_web.GSV('jobe:InvRRCCLabourCost') * inv:RRCVatRateLabour/100) + |
        (p_web.GSV('jobe:InvRRCCPartsCost') * inv:RRCVatRateParts/100) + |
        (p_web.GSV('job:Invoice_Courier_Cost') * inv:RRCVatRateLabour/100)
    locSubTotal = p_web.GSV('jobe:InvRRCCLabourCost') + | 
        p_web.GSV('jobe:InvRRCCPartsCost') + | 
        p_web.GSV('job:Invoice_Courier_Cost')
    locTotal = locSubTotal + locVAT

    IF (fCredit = 1)
        locTotal = jov:NewTotalCost
    END
    IF (fCredit = 2)
        locTotal = -jov:CreditAmount
    END
PrintReport         procedure(BYTE fCredit)
CODE
    
    
    newLine(7)

    IF (p_web.GSV('BookingSite') = 'ARC')
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('ARC:AccountNumber')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
        END
        locCompanyName = tra:Company_Name
        locAddressLine1 = tra:Address_Line1
        locADdressLine2 = tra:Address_Line2
        locAddressLine3 = tra:Address_Line3
        locPostcode = tra:Postcode
        locTelephoneNumber = tra:Telephone_Number
        
        locInvoiceDate = inv:ARCInvoiceDate
        CheckCreateInvoiceARC(fCredit)
    ELSE
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
        END
        locCompanyName = tra:Company_Name
        locAddressLine1 = tra:Address_Line1
        locADdressLine2 = tra:Address_Line2
        locAddressLine3 = tra:Address_Line3
        locPostcode = tra:Postcode
        locTelephoneNumber = tra:Telephone_Number
        
        locInvoiceDate = inv:RRCInvoiceDate
        CheckCreateInvoiceRRC(fCredit)
    END


    IF (p_web.GSV('jobe:WebJob') = 1)
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = p_web.GSV('job:Account_Number')
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key))
        END
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
        END
        IF (sub:OverrideHeadVATNo = TRUE)
            locCustVatNumber = sub:VAT_Number            
        ELSE
            locCustVatNumber = tra:VAT_Number
        END
        
        IF (sub:Invoice_Customer_Address = 'YES')
            IF (p_web.GSV('job:Company_Name') = '')
                locCustCompanyName = p_web.GSV('job:Initial') & ' ' & p_web.GSV('job:Initial') & | 
                    ' ' & p_web.GSV('job:Surname')
            ELSE
                locCustCompanyName = p_web.GSV('job:Company_Name')
            END
            locCustAddressLine1 = p_web.GSV('job:Address_Line1')
            locCustAddressLine2 = p_web.GSV('job:Address_Line2')
            locCustAddressLine3 = p_web.GSV('job:Address_Line3')
            locCustPostcode = p_web.GSV('job:Postcode')
            locCustTelephoneNumber = p_web.GSV('job:Telephone_Number')
        ELSE
            locCustCompanyName = sub:Company_Name
            locCustAddressLine1 = sub:Address_Line1
            locCustAddressLine2 = sub:Address_Line2
            locCustAddressLine3 = sub:Address_Line3
            locCustPostcode = sub:Postcode
            locCustTelephoneNumber = sub:Telephone_Number
        END
    END

        
    addToDetail(LEFT(locCustCOmpanyName,33) & locCompanyName)
    newLine()
    addToDetail(LEFT(locCustAddressLine1,33) & locAddressLine1)
    newLine()
    addToDetail(LEFT(locCustAddressLine2,33) & locAddressLine2)
    newLine()
    addToDetail(LEFT(locCustAddressLine3,33) & locAddressLine3)
    newLine()
    addToDetail(LEFT(locCustPostcode,33) & locPostcode)
    newLine()
    addToDetail(LEFT(locCustTelephoneNumber,33) & locTelephoneNumber)
    newLine()
    addToDetail('VAT NO.: ' & locCustVatNumber)
    newLine(2)
        
! Job Number
    Access:TRADEACC.ClearKey(tra:Account_Number_Key)
    tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
    IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
    END
    addToDetail(RIGHT('',33) & 'Job Number ' & p_web.GSV('job:Ref_Number') & | 
        '-' & tra:BranchIdentification & p_web.GSV('wob:JobNumber'))
    newLine(5)
        
! Order Line
        
    IF (fCredit = 1)
        addToDetail(LEFT(FORMAT(locInvoiceDate,@d06),23) & | 
            LEFT(CLIP(inv:Invoice_Number) & p_web.GSV('InvoiceSuffix'),13) & | 
            LEFT(p_web.GSV('job:Account_Number'),15) & | 
            LEFT(p_web.GSV('job:Order_Number'),9))
    ELSIF (fCredit = 2)
        addToDetail(LEFT(FORMAT(locInvoiceDate,@d06),23) & | 
            LEFT('CN' & CLIP(inv:Invoice_Number) & p_web.GSV('CreditSuffix'),13) & | 
            LEFT(p_web.GSV('job:Account_Number'),15) & | 
            LEFT(p_web.GSV('job:Order_Number'),9))
    ELSE
        addToDetail(LEFT(FORMAT(locInvoiceDate,@d06),23) & | 
            LEFT(inv:Invoice_Number,13) & | 
            LEFT(p_web.GSV('job:Account_Number'),15) & | 
            LEFT(p_web.GSV('job:Order_Number'),9))

    END
    
        
    newLine(5)
        
! IMEI LIne
    addToDetail(LEFT(p_web.GSV('job:Manufacturer'),16) & | 
        LEFT(p_web.GSV('job:Model_Number'),17) & |
        LEFT(p_web.GSV('job:MSN'),25) & |
        LEFT(p_web.GSV('job:ESN'),20))
        
    newLine(2)
        
    Do CreatePartsList

    Loop x# = 1 To 14
        Get(Detail_Queue,x#)
        IF (ERROR())
            ERROR# = TRUE
        ELSE
            ERROR# = FALSE
        END
        detail:Amount = ''
        Case x#
        Of 1
            IF (p_web.GSV('BookingSite') = 'RRC')
                detail:Amount = (Format(p_web.GSV('jobe:invRRCCLabourCost'),@n-14.2))
            ELSE
                detail:Amount = (Format(p_web.GSV('job:Invoice_Labour_Cost'),@n-14.2))
            END
                
        Of 3
            IF (p_web.GSV('BookingSite') = 'RRC')
                detail:Amount = (Format(p_web.GSV('jobe:invRRCCPartsCost'),@n-14.2))
            ELSE
                detail:Amount = (Format(p_web.GSV('job:Invoice_Parts_Cost'),@n-14.2))
            END
                
        Of 5
            IF (p_web.GSV('BookingSite') = 'RRC')
                detail:Amount = (Format(p_web.GSV('job:invoice_Courier_Cost'),@n-14.2))
            ELSE
                detail:Amount = (Format(p_web.GSV('job:invoice_Courier_Cost'),@n-14.2))
            END
                
        Of 10
            detail:Amount = (Format(locSubTotal,@n-14.2))
        Of 12
            detail:Amount = (Format(locVAT,@n-14.2))
        Of 14
            detail:Amount = (Format(locTotal,@n-14.2))
        End ! Case x#

        IF (ERROR#)
            detail:Line = '-'
            Add(Detail_Queue)
        ELSE
            Put(Detail_Queue)
        END
        
    End ! Loop x# = 1 To Records(Detail_Queue)

    Get(Detail_Queue,1)

    Loop x# = 1 To 15
        Get(Detail_Queue,x#)
        IF ~(ERROR())
            addToDetail(LEFT(detail:Line,68) & | 
                RIGHT(detail:Amount,10))
        END
        newLine()
    End ! Loop x# = 1 To Records(Detail_Queue)        
        
! Two Dates
    addToDetail(LEFT('',10) & |
        LEFT(FORMAT(p_web.GSV('job:Date_Booked'),@d06),32) & | 
        FORMAT(p_web.GSV('job:Time_Booked'),@t01))
    newLine(2)
    
    IF (fCredit <> 0)
        ! If credit use dates
        locInvoiceDate = jov:DateCreated
        locInvoiceTime = jov:TimeCreated
    END
    
    addToDetail(LEFT('',10) & | 
        LEFT(FORMAT(locInvoiceDate,@d06),32) & | 
        LEFT(FORMAT(locInvoiceTime,@t01),20) & | 
        p_web.GSV('job:Engineer'))
              


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Printing Details
      
  Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
  inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
  IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key))
  END
  
  ! Is this a credit note?
  IF (p_web.GSV('CreditNoteCreated') = 1)
      Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
      jov:RefNumber = p_web.GSV('job:ref_Number')
      jov:Type = 'C'
      jov:Suffix = p_web.GSV('CreditSuffix')
      IF (Access:JOBSINV.TryFetch(jov:TypeSuffixKey))
      END
      PrintReport(2)
      Print(rpt:Detail)
      Print(rpt:ANewPage)
      locDetail = ''
      
      Access:JOBSINV.ClearKey(jov:TypeSuffixKey)
      jov:RefNumber = p_web.GSV('job:ref_Number')
      jov:Type = 'I'
      jov:Suffix = p_web.GSV('InvoiceSuffix')
      IF (Access:JOBSINV.TryFetch(jov:TypeSuffixKey))
      END
      PrintReport(1)
      Print(rpt:Detail)
      
      
  
      
  ELSE
      PrintReport(0)
      Print(rpt:Detail)
  END
  
  
  
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR1:rtn = PDFXTR1.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR1:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

