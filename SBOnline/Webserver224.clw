

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER224.INC'),ONCE        !Local module procedure declarations
                     END


DeclareProgressBar   PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:DeclareProgressBar -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
  CODE
  GlobalErrors.SetProcedureName('DeclareProgressBar')
  If p_web.RequestAjax = 1
    GlobalErrors.SetProcedureName()
    Return
  End
!----------- put your html code here -----------------------------------
  Do DeclareProgressBar
!----------- end of custom code ----------------------------------------
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
DeclareProgressBar  Routine
  packet = clip(packet) & |
    '<<!-- Progess bar widget, by Matthew Harvey (matt at smallish.com) --><13,10>'&|
    '    <<!-- Licensed under a Creative Commons Attribution-Share Alike 2.5<13,10>'&|
    '         License (http://creativecommons.org/licenses/by-sa/2.5/) --><13,10>'&|
    '    <<style type="text/css"><13,10>'&|
    '      div.smallish-progress-wrapper<13,10>'&|
    '      {{<13,10>'&|
    '        /* Don''t change the following lines. */<13,10>'&|
    '        position: relative;<13,10>'&|
    '        border: 1px solid rgb(223, 223, 223);<13,10>'&|
    '        border-radius: 4px;<13,10>'&|
    '        height: 20px;<13,10>'&|
    '        box-shadow: 0px 0px 0px 4px #E9E9E9;<13,10>'&|
    '      }<13,10>'&|
    '<13,10>'&|
    '      div.smallish-progress-bar<13,10>'&|
    '      {{<13,10>'&|
    '        /* Don''t change the following lines. */<13,10>'&|
    '        position: absolute;<13,10>'&|
    '        top: 0; left: 0;<13,10>'&|
    '        height: 100%;<13,10>'&|
    '        border-radius: 4px 0 0 4px;<13,10>'&|
    '      }<13,10>'&|
    '<13,10>'&|
    '      div.smallish-progress-text<13,10>'&|
    '      {{<13,10>'&|
    '        /* Don''t change the following lines. */<13,10>'&|
    '        text-align: center;<13,10>'&|
    '        position: relative;<13,10>'&|
    '        /* Add your customizations after this line. */<13,10>'&|
    '        font-size: 17px;<13,10>'&|
    '        font-weight: bold;<13,10>'&|
    '        color: #616060;<13,10>'&|
    '      }<13,10>'&|
    '    <</style><13,10>'&|
    '<13,10>'&|
    '    <<!-- Progess bar widget, by Matthew Harvey (matt at smallish.com) --><13,10>'&|
    '    <<!-- Licensed under a Creative Commons Attribution-Share Alike 2.5<13,10>'&|
    '         License (http://creativecommons.org/licenses/by-sa/2.5/) --><13,10>'&|
    '    <<script type="text/javascript">  <13,10>'&|
    '      function drawProgressBar(color, width, percent)  <13,10>'&|
    '      {{  <13,10>'&|
    '        var pixels = width * (percent / 100);  <13,10>'&|
    '      <13,10>'&|
    '        document.write(''<<div class="smallish-progress-wrapper" style="width: '' + width + ''px">'');  <13,10>'&|
    '        document.write(''<<div class="smallish-progress-bar" style="width: '' + pixels + ''px; background-color: '' + color + '';"><</div>'');  <13,10>'&|
    '        document.write(''<<div class="smallish-progress-text" style="width: '' + width + ''px">'' + percent + ''%<</div>'');  <13,10>'&|
    '        document.write(''<</div>'');  <13,10>'&|
    '      }  <13,10>'&|
    '    <</script><13,10>'&|
    ''
