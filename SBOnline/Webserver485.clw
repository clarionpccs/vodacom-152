

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER485.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER046.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER048.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER070.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER071.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER072.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER486.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER497.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER498.INC'),ONCE        !Req'd for module callout resolution
                     END


PreNewJobBooking     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:TransitType      STRING(30)                            !
tmp:ESN              STRING(30)                            !
tmp:Manufacturer     STRING(30)                            !
tmp:ModelNumber      STRING(30)                            !
filter:Manufacturer  STRING(1000)                          !
filter:ModelNumber   STRING(1000)                          !
tmp:DOP              DATE                                  !
tmp:ChangeDOPReason  STRING(255)                           !
tmp:NewDOP           DATE                                  !
mj:ModelNumber       BYTE                                  !
mj:Colour            BYTE                                  !
mj:OrderNumber       BYTE                                  !
mj:FaultCode         BYTE                                  !
mj:ContactHistory    BYTE                                  !
mj:EngineersNotes    BYTE                                  !
locSpecificNeeds     LONG                                  !
locSpecificNeedsType STRING(30)                            !
local       Class
CheckLength         Procedure(String f:Type),Byte
IMEIModelNumber     Procedure(),Byte
IsIMEIValid         Procedure(),Byte
ValidateIMEI        Procedure(),Byte
ValidateMSN         Procedure(),Byte
            End ! local       Class
!MQ Setup
mqFileName  string(260), static
! Input / Output files - pipe delimited (|)
MQIInputFile file, driver('BASIC','/COMMA=124 /ALWAYSQUOTE=OFF'), pre(MQIF), name(mqFileName), create
MQIFRec         record
imei                string(30)
                end ! record
            end ! file

MQIOutputFile file, driver('BASIC','/COMMA=124 /ALWAYSQUOTE=OFF'), pre(MQOF), name(mqFileName)
MQOFRec         record
activationDate      string(255)
activity            string(255)
warranty            string(255)
blackGrey           string(255)
supplier            string(255)
swap                string(255)
usage               string(255)
statusCode          string(255)
statusDescription   string(255)
                end ! record
            end ! file



FilesOpened     Long
JOBNOTES::State  USHORT
PREJOB::State  USHORT
WEBJOB::State  USHORT
JOBS::State  USHORT
TRANTYPE::State  USHORT
JOBSE::State  USHORT
MANUFACT::State  USHORT
MODELNUM::State  USHORT
ESNMODEL::State  USHORT
ESNMODAL::State  USHORT
JOBS_ALIAS::State  USHORT
EXCHANGE::State  USHORT
LOAN::State  USHORT
SBO_DupCheck::State  USHORT
USERS::State  USHORT
ACCAREAS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('PreNewJobBooking')
  loc:formname = 'PreNewJobBooking_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'PreNewJobBooking',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('PreNewJobBooking','')
    p_web._DivHeader('PreNewJobBooking',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferPreNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPreNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPreNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_PreNewJobBooking',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferPreNewJobBooking',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_PreNewJobBooking',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'PreNewJobBooking',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
ClearVariables      ROUTINE
    !Multiple Job Booking
    p_web.DeleteSessionValue('mj:InProgress')
    p_web.DeleteSessionValue('mj:Colour')
    p_web.DeleteSessionValue('mj:OrderNumber')
    p_web.DeleteSessionValue('mj:FaultCode')
    p_web.DeleteSessionValue('mj:EngineersNotes')
    p_web.DeleteSessionValue('mj:PreviousJobNumber')
    

CheckManModel       Routine
    Data
local:LastJobNumber     Long()
local:Bouncers  Long()
local:PreviousDOP       Date()
local:Found     Byte(0)
    Code
        Access:MODELNUM.Clearkey(mod:Manufacturer_Key)
        mod:Manufacturer = p_web.GSV('tmp:Manufacturer')
        mod:Model_Number = p_web.GSV('tmp:ModelNumber')
        If Access:MODELNUM.TryFetch(mod:Manufacturer_Key)
            p_web.SSV('tmp:ModelNumber','')
            p_web.SSV('Hide:IMEINumberButton',1)
            Exit
        End ! If Access:MODELNUM.TryFetch(mod:Manufacturer_Key)

        If p_web.GSV('tmp:Manufacturer') <> '' And p_web.GSV('tmp:ModelNumber') <> '' And p_web.GSV('tmp:TransitType') <> '' And p_web.GSV('tmp:ESN') <> ''
            p_web.SSV('Filter:Manufacturer','man:Manufacturer = <39>' & p_web.GSV('tmp:Manufacturer') & '<39>')
            p_web.SSV('Drop:ManufacturerDefault',p_web.GSV('tmp:Manufacturer'))
            p_web.SSV('Filter:ModelNumber','mod:Model_Number = <39>' & p_web.GSV('tmp:ModelNumber') & '<39>')
            p_web.SSV('Drop:ModelNumberDefault',p_web.GSV('tmp:ModelNumber'))
            p_web.SSV('Hide:IMEINumberButton','')                      

            Access:TRANTYPE.Clearkey(trt:Transit_Type_Key)
            trt:Transit_Type = p_web.GSV('tmp:TransitType')
            If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign
                If trt:OBF
                    p_web.SSV('OBFValidation','Pending')
                Else ! If trt:OBF
                    p_web.SSV('OBFValidation','')
                End ! If trt:OBF
            End ! If Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign

            If p_web.GSV('OBFValidation') = 'Pending'
                p_web.SSV('IMEINumberButtonURL','OBFValidation')
            Else ! If p_web.SSV('OBFValidation','Pending')
                p_web.SSV('IMEINumberButtonURL','NewJobBooking?&Insert_btn=Insert&')
            End ! If p_web.SSV('OBFValidation','Pending')
            p_web.SSV('ReadOnly:Manufacturer',1)
            p_web.SSV('ReadOnly:ModelNumber',1)
            p_web.SSV('ReadOnly:TransitType',1)
            p_web.SSV('ReadOnly:IMEINumber',1)

            local:LastJobNumber = 0
            local:Bouncers = 0
            Access:JOBS_ALIAS.Clearkey(job_ali:ESN_Key)
            job_ali:ESN    = p_web.GSV('tmp:ESN')
            set(job_ali:ESN_Key,job_ali:ESN_Key)
            loop
                if (Access:JOBS_ALIAS.Next())
                    Break
                end ! if (Access:JOBS_ALIAS.Next())
                if (job_ali:ESN    <> p_web.GSV('tmp:ESN'))
                    Break
                end ! if (job_ali:ESN    <> p_web.GSV('tmp:ESN'))
                if (job_ali:Cancelled = 'YES')
                    cycle
                end !if (job_ali:Cancelled = 'YES')
                if (job_ali:Ref_Number > local:LastJobNumber)
                ! Get the recent job DOP
                    local:LastJobNumber = job_ali:Ref_Number
                    local:PreviousDOP = job_ali:DOP
                end ! if (job_ali:Ref_Number > lastJobNumber#)
                local:Bouncers += 1
            end ! loop

        ! Insert --- Give user option to change DOP (DBH: 25/02/2009) #10591
            CopyDOPFromBouncer(p_web.GSV('tmp:Manufacturer'),local:PreviousDOP,job:DOP,p_web.GSV('tmp:ESN'),local:Bouncers,local:Found)
            if (local:Found)
                p_web.SSV('job:DOP',job:DOP)
                p_web.SSV('Hide:ChangeDOP',1)
                p_web.SSV('Hide:ChangeDOPButton',1)
                p_web.SSV('mq:DOP',job:DOP)
                p_web.SSV('tmp:DateOfPurchase',job:DOP)
                p_web.SSV('Hide:ChangeDOP',0)
            end ! if (local:Found)
            ! end --- (DBH: 25/02/2009) #10591
            
            Do MQ:Process
            
            ! #11344 Check One Year Warranty again, just incase model/manufact hadn't been filled in (DBH: 10/06/2010)
            IF (p_web.GSV('job:POP') = 'S')
                If (IsOneYearWarranty(p_web.GSV('tmp:Manufacturer'),p_web.GSV('tmp:ModelNumber')))
                    p_web.SSV('job:Chargeable_Job','YES')
                    p_web.SSV('job:Charge_Type','NON-WARRANTY')
                    p_web.SSV('job:Warranty_Job','NO')
                    p_web.SSV('job:Warranty_Charge_Type','')
                    p_web.SSV('Comment:IMEINumber','IMEI Validation Result:    This Phone must be repaired out of Warranty')
                    p_web.SSV('job:POP','C')
                    p_web.SSV('mq:ChargeableJob',p_web.GSV('job:Chargeable_Job'))
                    p_web.SSV('mq:Charge_Type',p_web.GSV('job:Charge_Type'))
                    p_web.SSV('mq:WarrantyJob',p_web.GSV('job:Warranty_Job'))
                    p_web.SSV('mq:Warranty_Charge_Type',p_web.GSV('job:Warranty_Charge_Type'))
                    p_web.SSV('mq:DOP',job:DOP)
                    p_web.SSV('mq:POP',p_web.GSV('job:POP'))
                    p_web.SSV('tmp:DateOfPurchase',p_web.GSV('job:DOP'))
                    p_web.SSV('mq:IMEIValidation',p_web.GSV('IMEIValidation'))
                    p_web.SSV('mq:IMEIValidationText','This Phone must be repaired out of Warranty')
                       
                end                
            END
            
        Else
            p_web.SSV('Hide:IMEINumberButton',1)
        End ! If p_web.GSV('tmp:Manufacturer') <> '' And p_web.GSV('tmp:ModelNumber') <> ''
MQ:Process          Routine
    Data
local:SystemError       Byte(0)
local:Error     Byte(0)
local:ErrorMessage      String(255)
locOneYearWarranty      STRING(1)
locSpecNeeds   BYTE
    Code
        IF (p_web.GSV('tmp:ESN') = '')
            EXIT
        END
        ! #11344 Don't validate if man/model aren't filled in. (DBH: 29/06/2010)
        IF (p_web.GSV('tmp:Manufacturer') = '' OR p_web.GSV('tmp:ModelNumber') = '')
            EXIT
        END
            
        tmp:ESN = p_web.GSV('tmp:ESN')

        locOneYearWarranty = 'N'
        
        If (IsOneYearWarranty(p_web.GSV('tmp:Manufacturer'),p_web.GSV('tmp:ModelNumber')))
            locOneYearWarranty = 'Y'
        else
            locOneYearWarranty = 'N'
        end

        local:SystemError = MQProcess(tmp:ESN,job:POP,job:DOP,local:Error,local:errorMessage,locOneYearWarranty,locSpecNeeds)
        
        IF (locSpecNeeds = 1)
            ! #13287 Tick specific needs if validated (DBH: 16/01/2015)
            p_web.SSV('locSpecificNeeds',1)
        END ! IF

        if (local:SystemError)
            p_web.SSV('Comment:IMEINumber','IMEI Validation Result:    ' &  Clip(local:ErrorMessage))
            p_web.SSV('IMEIValidation','Failed')
            p_web.SSV('IMEIValidationText',local:ErrorMessage)
        ELSE

            p_web.SSV('job:DOP',job:DOP)
            p_web.SSV('tmp:DOP',p_web.GSV('job:DOP'))
            p_web.SSV('IMEIActivationDate',format(job:DOP,@d06))
            p_web.SSV('job:POP',job:POP)

            case job:POP
            of 'C' ! Chargeable
                p_web.SSV('job:Chargeable_Job','YES')
                p_web.SSV('job:Charge_Type','NON-WARRANTY')
                p_web.SSV('job:Warranty_Job','NO')
                p_web.SSV('job:Warranty_Charge_Type','')
                p_web.SSV('Comment:IMEINumber','IMEI Validation Result:    ' & Clip(local:ErrorMessage))
                p_web.SSV('job:POP','C')
            of 'F' ! First Year Warranty
                p_web.SSV('Comment:IMEINumber','IMEI Validation Result:    This IMEI can be reparied under the First Year Manufacturer Warranty as per manufacturer criteria.')
                p_web.SSV('job:Warranty_Job','YES')
                p_web.SSV('job:Warranty_Charge_Type','WARRANTY (MFTR)')
                p_web.SSV('job:POP','F')
            of 'S' ! Second Year Warranty
                p_web.SSV('Comment:IMEINumber','IMEI Validation Result:    The IMEI can be repaired under the Second Year Warranty.')
                p_web.SSV('job:Warranty_Job','YES')
                p_web.SSV('job:Warranty_Charge_Type','WARRANTY (2ND YR)')
                p_web.SSV('job:POP','S')
            end ! case job:POP

            p_web.SSV('IMEIValidation','Passed')
            p_web.SSV('IMEIValidationText',Clip(local:ErrorMessage))

            p_web.SSV('mq:ChargeableJob',p_web.GSV('job:Chargeable_Job'))
            p_web.SSV('mq:Charge_Type',p_web.GSV('job:Charge_Type'))
            p_web.SSV('mq:WarrantyJob',p_web.GSV('job:Warranty_Job'))
            p_web.SSV('mq:Warranty_Charge_Type',p_web.GSV('job:Warranty_Charge_Type'))
            p_web.SSV('mq:DOP',job:DOP)
            p_web.SSV('mq:POP',p_web.GSV('job:POP'))
            p_web.SSV('tmp:DateOfPurchase',p_web.GSV('job:DOP'))

            p_web.SSV('mq:IMEIValidation',p_web.GSV('IMEIValidation'))
            p_web.SSV('mq:IMEIValidationText',Clip(local:ErrorMessage))

        end ! if (local:SystemError)
        
        IF (IsUnitLiquidDamaged(p_web.GSV('tmp:ESN'),0,TRUE) = TRUE)
            p_web.SSV('mq:ChargeableJob','YES')
            p_web.SSV('mq:WarrantyJob','NO')
            p_web.SSV('mq:Charge_Type','NON-WARRANTY')
            p_web.SSV('mq:Warranty_Charge_Type','')
            p_web.SSV('mq:POP','C')
            local:ErrorMessage = 'This IMEI must be repaired out of warranty due to the following reason: ' & |
                'Previous Repair, handset deemed Liquid Damage.'
            p_web.SSV('Comment:IMEINumber','IMEI Validation Result:    ' & CLIP(local:ErrorMessage))  
            p_web.SSV('mq:IMEIValidationText',Clip(local:ErrorMessage))
        END 
    
PreBooking  ROUTINE
    IF (p_web.GSV('PreBookingRefNo') > 0)
        Access:SBO_GenericFile.ClearKey(sbogen:RecordNumberKey)
        sbogen:RecordNumber = p_web.GSV('PreBookingRefNo')
        IF (Access:SBO_GenericFile.TryFetch(sbogen:RecordNumberKey) = Level:Benign)
            Access:PREJOB.ClearKey(PRE:keyRefNumber)
            PRE:RefNumber = sbogen:Long2
            IF (Access:PREJOB.TryFetch(PRE:keyRefNumber) = Level:Benign)
                p_web.SSV('tmp:ESN',PRE:ESN)
                p_web.SSV('tmp:ModelNumber',PRE:Model_Number)
                p_web.SSV('tmp:Manufacturer',PRE:Manufacturer)
            
                IF (PRE:ESN <> '' AND PRE:Model_Number <> '' AND PRE:Manufacturer <> '')
                    p_web.SSV('IMEINumberButtonURL','NewJobBooking?&Insert_btn=Insert&PreBookingRefNo=' & pre:RefNumber & '&')
                    p_web.SSV('Hide:IMEINumberButton',0)
                END
            END ! IF
        END ! IF
    END ! IF
Reset:IMEINumber     Routine
    p_web.SetSessionValue('Drop:Manufacturer','- Enter A Valid IMEI Number -')
    p_web.SetSessionValue('Drop:ModelNumber','- Enter A Valid Manufacturer -')
    p_web.SetSessionValue('Drop:ManufacturerDefault','')
    p_web.SetSessionValue('Drop:ModelNumberDefault','')

    p_web.SetSessionValue('tmp:ESN','')

    p_web.SetSessionValue('Filter:ModelNumber',0)
    p_web.SetSessionValue('Filter:Manufacturer',0)

    p_web.SetSessionValue('tmp:ModelNumber','')
    p_web.SetSessionValue('tmp:Manufacturer','')

    p_web.SetSessionValue('OBFValidation','')

    p_web.SetSessionValue('Hide:IMEINumberButton',1)
    p_web.SetSessionValue('IMEINumberAccepted',0)

    If p_web.GetSessionValue('BookingSite') = 'ARC'
        p_web.SetSessionValue('Filter:TransitType','trt:ARC = 1')
    Else ! If p_web.GetSessionValue('BookingSite') = 'ARC'
        p_web.SetSessionValue('Filter:TransitType','trt:RRC = 1')
    End ! If p_web.GetSessionValue('BookingSite') = 'ARC'

    p_web.SetSessionValue('ReadOnly:Manufacturer',1)
    p_web.SetSessionValue('ReadOnly:ModelNumber',1)
    p_web.SetSessionValue('ReadOnly:TransitType','')
    p_web.SetSessionValue('ReadOnly:IMEINumber','')
Value:IMEINumber    Routine
    Data
local:Bouncers  Long()
local:LiveBouncers      Long()
local:PreviousDOP       Date()
local:Found     Byte()
    Code
        If p_web.GSV('save:IMEINumber') = p_web.GSV('tmp:ESN')
            Exit
        End ! Comment:IMEINumber

        p_web.SSV('Comment:IMEINumber','')
        If p_web.GSV('tmp:TransitType') = ''
            Do Reset:IMEINumber
            p_web.SSV('Comment:IMEINumber','Transit Type Required')
            Exit
        End ! If p_web.GSV('tmp:TransitType') = ''

        If p_web.GSV('tmp:ESN') = ''
            Do Reset:IMEINumber
            Exit
        End ! If p_web.GSV('tmp:ESN') = ''

! Insert --- Check the unit isn't available somewhere else (DBH: 24/02/2009) #10545
        Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
        xch:ESN    = p_web.GSV('tmp:ESN')
        if (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)
        ! Found
            if (xch:Available = 'AVL')
                error# = 1
            end ! if (xch:Available <> 'AVL')
        else ! if (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)
        ! Error
        end ! if (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)

        if (error# = 0)
            Access:LOAN.Clearkey(loa:ESN_Only_Key)
            loa:ESN    = p_web.GSV('tmp:ESN')
            if (Access:LOAN.TryFetch(loa:ESN_Only_Key) = Level:Benign)
            ! Found
                if (loa:Available = 'AVL')
                    error# = 1
                end ! if (loa:Available = 'AVL')
            else ! if (Access:LOAN.TryFetch(loa:ESN_Only_Key) = Level:Benign)
            ! Error
            end ! if (Access:LOAN.TryFetch(loa:ESN_Only_Key) = Level:Benign)
        end !if (error# = 0)

        if (error# = 1)
            p_web.SSV('Comment:IMEINumber','Error! The selected unit is available in another location.')
            Do Reset:IMEINumber
            Exit
        end ! if (error# = 1)
! end --- (DBH: 24/02/2009) #10545


        If local.IMEIModelNumber()
            Do Reset:IMEINumber
            Exit
        End ! If local.IMEIModelNumber()

        If local.ValidateIMEI()
            Do Reset:IMEINumber
            Exit
        End ! If local.ValidateIMEI()

        Access:DEFAULTS.Clearkey(def:RecordNumberKey)
        def:Record_Number = 1
        Set(def:RecordNumberKey,def:RecordNumberKey)
        Loop ! Begin Loop
            If Access:DEFAULTS.Next()
                Break
            End ! If Access:DEFAULTS.Next()
            Break
        End ! Loop

        local:Bouncers = 0
        local:LiveBouncers = 0
        Access:JOBS_ALIAS.Clearkey(job_ali:ESN_Key)
        job_ali:ESN = p_web.GSV('tmp:ESN')
        Set(job_ali:ESN_Key,job_ali:ESN_Key)
        Loop
            If Access:JOBS_ALIAS.Next()
                Break
            End ! If Access:JOBS_ALIAS.Next()
            If job_ali:ESN <> p_web.GSV('tmp:ESN')
                Break
            End ! If job_ali:ESN <> tmp:ESN
            If job_ali:Cancelled = 'YES'
                Cycle
            End ! If job_ali:Cancelled = 'YES'
        ! Insert --- Autofil DOP with date from previous job (DBH: 26/02/2009) #10591
            local:PreviousDOP = job_ali:DOP

        ! end --- (DBH: 26/02/2009) #10591
            If job_ali:Date_Completed = ''
                local:LiveBouncers += 1
            End ! If job_ali:Date_Completed = ''
            local:Bouncers += 1
            p_web.SSV('tmp:PreviousAddress',Clip(job_ali:Company_Name) & '<13,10>' & Clip(job_ali:Address_Line1) & '<13,10>' & Clip(job_ali:Address_Line2) & '<13,10>' & Clip(job_ali:Address_Line3) & '<13,10>')
        End ! Loop

        If def:Allow_Bouncer <> 'YES'
            If local:Bouncers
                Do Reset:IMEINumber
                p_web.SSV('Comment:IMEINumber','IMEI Number has been previously entered within the default period.')
                Exit
            End ! If local:Bouncers
        Else ! If def:Allow_Bouncer <> 'YES'
            If GETINI('BOUNCER','StopLive',,Clip(Path()) & '\SB2KDEF.INI') = 1
                If local:LiveBouncers > 0
                    Do Reset:IMEINumber
                    p_web.SSV('Comment:IMEINumber','IMEI Number already exists on a Live Job.')
                    Exit
                End ! If LiveBouncers(tmp:ESN,Today())
            End ! If GETINI('BOUNCER','StopLive',,Clip(Path()) & '\SB2KDEF.INI') = 1
        End ! If def:Allow_Bouncer <> 'YES'

        If local:Bouncers = 0
            If PreviousIMEI(p_web.GSV('tmp:ESN'))
                p_web.SSV('Comment:Bouncer','The selected IMEI Number has been entered on the system before.')
            End ! If PreviousIMEI(job:ESN)
        End ! If local:Bouncers = 0

        If def:Allow_Bouncer <> 'YES'
            If local:Bouncers
                p_web.SSV('Comment:Bouncer','IMEI Number has been previously entered within the default period.')
                p_web.SSV('tmp:ESN','')
                Do Reset:IMEINumber
                Exit
            End ! If local:Bouncers
        Else ! If def:Allow_Bouncer <> 'YES'
            If GETINI('BOUNCER','StopLive',,Clip(Path()) & '\SB2KDEF.INI') = 1
                If local:LiveBouncers > 0
                    p_web.SSV('Comment:IMEINumber','IMEI Number already exists on a Live Job.')
                    p_web.SSV('tmp:ESN','')
                    Do Reset:IMEINumber
                    Exit
                End ! If LiveBouncers(job:ESN,Today())
            End ! If GETINI('BOUNCER','StopLive',,Clip(Path()) & '\SB2KDEF.INI') = 1
            If local:Bouncers > 0
                p_web.SSV('IMEIHistory','1')
                p_web.SSV('tmp:Bouncer','B')
                p_web.SSV('Comment:Bouncer',local:Bouncers & ' Previous Jobs(s)')
                p_web.SSV('Hide:PreviousAddress',0)
            End ! If local:Bouncers > 0
        End ! If def:Allow_Bouncer <> 'YES'

        !p_web.SSV('Comment:IMEINumber','Valid')
        p_web.SSV('Filter:TransitType','Upper(trt:Transit_Type) = Upper(<39>' & p_web.GSV('tmp:TransitType') & '<39>)')
        p_web.SSV('ReadOnly:TransitType',1)

        Do CheckManModel

        ! #11344 Move to above routine (DBH: 29/06/2010)    
        !Do MQ:Process


        p_web.SSV('save:IMEINumber',p_web.GSV('tmp:ESN'))
        
        
Value:Manufacturer      Routine
    If p_web.GetSessionValue('man:Manufacturer') <> ''
        p_web.SetSessionValue('tmp:Manufacturer',p_web.GetSessionValue('man:Manufacturer'))
        If local.IMEIModelNumber() = 0
        End ! If local.IMEIModelNumber()
        If local.ValidateIMEI()
            p_web.SetSessionValue('man:Manufacturer','')
            Do Reset:IMEINumber
            Exit
        End ! If local.ValidateIMEI()
        p_web.SetSessionValue('Filter:ModelNumber',p_web.GetSessionValue('Filter:ModelNumber') & ' And Upper(mod:Manufacturer) = Upper(<39>' & p_web.GetSessionValue('tmp:Manufacturer') & '<39>)')

        Do CheckManMOdel
    End ! If p_web.GetSessionValue('mod:Manufacturer') <> ''
Value:ModelNumber       Routine
    p_web.SetSessionValue('tmp:ModelNumber',p_web.GetSessionValue('mod:Model_Number'))
    If p_web.GetSessionValue('tmp:ModelNumber') = ''
        p_web.SetSessionValue('mod:Model_Number','')
        Exit
    End ! If p_web.GetSessionValue('tmp:ModelNumber') = ''

    If local.ValidateIMEI()
        p_web.SetSessionValue('mod:Model_Number','')
        Do Reset:IMEINumber
        Exit
    End ! If local.ValidateIMEI()

    Do CheckManModel
Value:TransitType       Routine
    If trt:OBF = 1
        p_web.SetSessionValue('Comment:TransitType','OBF Required')
        p_web.SetSessionValue('OBFValidation','Pending')
    Else ! If trt:OBF
        p_web.SetSessionValue('Comment:TransitType','Required')
        p_web.SetSessionValue('OBFValidation','')
    End ! If trt:OBF
OpenFiles  ROUTINE
  p_web._OpenFile(JOBNOTES)
  p_web._OpenFile(PREJOB)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(TRANTYPE)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(MODELNUM)
  p_web._OpenFile(ESNMODEL)
  p_web._OpenFile(ESNMODAL)
  p_web._OpenFile(JOBS_ALIAS)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(SBO_DupCheck)
  p_web._OpenFile(USERS)
  p_web._OpenFile(ACCAREAS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBNOTES)
  p_Web._CloseFile(PREJOB)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(TRANTYPE)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(ESNMODEL)
  p_Web._CloseFile(ESNMODAL)
  p_Web._CloseFile(JOBS_ALIAS)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(SBO_DupCheck)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(ACCAREAS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('PreNewJobBooking_form:inited_',1)
  do RestoreMem

CancelForm  Routine
    Do ClearVariables
    DuplicateTabCheckDelete(p_web,'NEWJOBBOOKING','')  
    DuplicateTabCheckDelete(p_web,'NEWJOBBOOKINGIMEI','')

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('MultipleJobBooking') = 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:TransitType'
    p_web.setsessionvalue('showtab_PreNewJobBooking',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(TRANTYPE)
      Do Value:TransitType
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:ESN')
  Of 'tmp:Manufacturer'
    p_web.setsessionvalue('showtab_PreNewJobBooking',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MANUFACT)
      DO Value:Manufacturer
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:ModelNumber')
  Of 'tmp:ModelNumber'
    p_web.setsessionvalue('showtab_PreNewJobBooking',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODELNUM)
        p_web.setsessionvalue('tmp:Manufacturer',mod:Manufacturer)
      Do Value:ModelNumber
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locSpecificNeeds')
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('mj:Colour',mj:Colour)
  p_web.SetSessionValue('mj:OrderNumber',mj:OrderNumber)
  p_web.SetSessionValue('mj:FaultCode',mj:FaultCode)
  p_web.SetSessionValue('mj:EngineersNotes',mj:EngineersNotes)
  p_web.SetSessionValue('tmp:TransitType',tmp:TransitType)
  p_web.SetSessionValue('tmp:ESN',tmp:ESN)
  p_web.SetSessionValue('tmp:Manufacturer',tmp:Manufacturer)
  p_web.SetSessionValue('tmp:ModelNumber',tmp:ModelNumber)
  p_web.SetSessionValue('locSpecificNeeds',locSpecificNeeds)
  p_web.SetSessionValue('locSpecificNeedsType',locSpecificNeedsType)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('mj:Colour')
    mj:Colour = p_web.GetValue('mj:Colour')
    p_web.SetSessionValue('mj:Colour',mj:Colour)
  End
  if p_web.IfExistsValue('mj:OrderNumber')
    mj:OrderNumber = p_web.GetValue('mj:OrderNumber')
    p_web.SetSessionValue('mj:OrderNumber',mj:OrderNumber)
  End
  if p_web.IfExistsValue('mj:FaultCode')
    mj:FaultCode = p_web.GetValue('mj:FaultCode')
    p_web.SetSessionValue('mj:FaultCode',mj:FaultCode)
  End
  if p_web.IfExistsValue('mj:EngineersNotes')
    mj:EngineersNotes = p_web.GetValue('mj:EngineersNotes')
    p_web.SetSessionValue('mj:EngineersNotes',mj:EngineersNotes)
  End
  if p_web.IfExistsValue('tmp:TransitType')
    tmp:TransitType = p_web.GetValue('tmp:TransitType')
    p_web.SetSessionValue('tmp:TransitType',tmp:TransitType)
  End
  if p_web.IfExistsValue('tmp:ESN')
    tmp:ESN = p_web.GetValue('tmp:ESN')
    p_web.SetSessionValue('tmp:ESN',tmp:ESN)
  End
  if p_web.IfExistsValue('tmp:Manufacturer')
    tmp:Manufacturer = p_web.GetValue('tmp:Manufacturer')
    p_web.SetSessionValue('tmp:Manufacturer',tmp:Manufacturer)
  End
  if p_web.IfExistsValue('tmp:ModelNumber')
    tmp:ModelNumber = p_web.GetValue('tmp:ModelNumber')
    p_web.SetSessionValue('tmp:ModelNumber',tmp:ModelNumber)
  End
  if p_web.IfExistsValue('locSpecificNeeds')
    locSpecificNeeds = p_web.GetValue('locSpecificNeeds')
    p_web.SetSessionValue('locSpecificNeeds',locSpecificNeeds)
  End
  if p_web.IfExistsValue('locSpecificNeedsType')
    locSpecificNeedsType = p_web.GetValue('locSpecificNeedsType')
    p_web.SetSessionValue('locSpecificNeedsType',locSpecificNeedsType)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('PreNewJobBooking_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  !Prime
    IF (p_web.GSV('JobBookingIncomplete') = 1)
        CreateScript(p_web, packet, 'alert("You did not complete your last job booking correctly. Please re-login and try again.")')
        CreateScript(p_web, packet, 'window.open("LoginForm","_self")')
        DO SendPacket
        EXIT
    END ! IF 
    
    IF (p_web.GetValue('firsttime') = 1 OR (p_web.GetValue('mjb') = 1))
        ! This is the first time this has been opened, or has been called again as part of multiple job booking    
        IF (DuplicateTabCheck(p_web,'NEWJOBBOOKING',1) OR DuplicateTabCheck(p_web,'JOBUPDATEREFNO',''))
            CreateScript(p_web,packet,'alert("Error. A job booking/editing may already be in progress, or a previous booking/editing did not complete correctly.\n\nRe-login if you wish to try again.");window.open("IndexPage","_self")')
            DO SendPacket
            EXIT
        ELSE
            DuplicateTabCheckAdd(p_web,'NEWJOBBOOKING',1)
            DuplicateTabCheckDelete(p_web,'NEWJOBBOOKINGIMEI','')
        END !I F
        
    END ! IF   
    
    
    IF (p_web.GetValue('firsttime') = 1)
        ClearJobVariables(p_web)
    END
    
    
  p_web.SSV('Hide:PleaseWaitMessage',1)
  p_web.site.CancelButton.TextValue = 'Quit'
  If p_web.GSV('ReadyForNewJobBooking') <> 1
      loc:Invalid = 'job:ESN'
      loc:Alert = 'An Error Has Occurred. Please "Quit Booking" and try again!'
      !Exit
  End ! If p_web.GetSessionValue('ReadyForNewJobBooking') = ''
  
  If p_web.GSV('FirstTime') = 1
      p_web.SSV('FirstTime','')
  
  End ! If p_web.GetSessionValue('FirstTime') = 1
  
  ! Only run certain code when the IMEI Number changes
  p_web.SSV('save:IMEINumber','')
  
  ! Is this multiple job booking?
  If (p_web.IfExistsValue('MultipleJobBooking'))
      p_web.StoreValue('MultipleJobBooking')
  END
  p_web.SSV('ReturnPreJobURL','IndexPage')
    IF (p_web.IfExistsValue('PreBookingRefNo'))
        p_web.StoreValue('PreBookingRefNo')
        DO PreBooking
        p_web.SSV('ReturnPreJobURL','FormPreJobCriteria')
    END
    
    IF (p_web.IfExistsValue('repJob'))
        p_web.StoreValue('repJob')
        IF (p_web.IfExistsValue('repJobNo'))
            p_web.StoreValue('repJobNo')
        END ! IF
    END ! IF
    
  
  !if (p_web.GSV('mj:InProgress') = 1)
  !    Access:JOBS.Clearkey(job:Ref_Number_Key)
  !    job:Ref_Number = p_web.GSV('mj:PreviousJobNumber')
  !    if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
  !!        if (p_web.GSV('mj:ModelNumber') <> 1)
  !!            p_web.SSV('tmp:ModelNumber',job:Model_Number)
  !!            p_web.SSV('tmp:Manufacturer',job:Manufacturer)
  !!        END
  !    END
  !END
  
    ! #13392 Clear file variables. (DBH: 13/10/2014)
    Access:JOBS.ClearKey(job:Ref_Number_Key)
    p_web.FileToSessionQueue(JOBS)
    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    p_web.FileToSessionQueue(WEBJOB)
    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    p_web.FileToSessionQueue(JOBSE)
    Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
    p_web.FileToSessionQueue(JOBSE2)
    Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
    p_web.FileToSessionQueue(JOBNOTES)
  
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 mj:Colour = p_web.RestoreValue('mj:Colour')
 mj:OrderNumber = p_web.RestoreValue('mj:OrderNumber')
 mj:FaultCode = p_web.RestoreValue('mj:FaultCode')
 mj:EngineersNotes = p_web.RestoreValue('mj:EngineersNotes')
 tmp:TransitType = p_web.RestoreValue('tmp:TransitType')
 tmp:ESN = p_web.RestoreValue('tmp:ESN')
 tmp:Manufacturer = p_web.RestoreValue('tmp:Manufacturer')
 tmp:ModelNumber = p_web.RestoreValue('tmp:ModelNumber')
 locSpecificNeeds = p_web.RestoreValue('locSpecificNeeds')
 locSpecificNeedsType = p_web.RestoreValue('locSpecificNeedsType')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'NewJobBooking?&Insert_btn=Insert&'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('PreNewJobBooking_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('PreNewJobBooking_ChainTo')
    loc:formaction = p_web.GetSessionValue('PreNewJobBooking_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ReturnPreJobURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="PreNewJobBooking" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="PreNewJobBooking" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="PreNewJobBooking" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('New Job Booking') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('New Job Booking',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_PreNewJobBooking">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_PreNewJobBooking" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_PreNewJobBooking')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GSV('MultipleJobBooking') = 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Multiple Job Booking Options') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Unit Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_PreNewJobBooking')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_PreNewJobBooking'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='TRANTYPE'
            p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:ESN')
    End
    If upper(p_web.getvalue('LookupFile'))='MANUFACT'
            p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:ModelNumber')
    End
  Else
    If False
    ElsIf p_web.GSV('MultipleJobBooking') = 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.mj:Colour')
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:TransitType')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GSV('MultipleJobBooking') = 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab5'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_PreNewJobBooking')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GSV('MultipleJobBooking') = 1
      packet = clip(packet) & 'roundCorners(''tab5'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GSV('MultipleJobBooking') = 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel5">'&CRLF &|
                                    '  <div id="panel5Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Multiple Job Booking Options') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel5Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PreNewJobBooking_5">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Multiple Job Booking Options')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab5" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab5">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Multiple Job Booking Options')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Multiple Job Booking Options')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Multiple Job Booking Options')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::text:SelectFields
      do Comment::text:SelectFields
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::mj:Colour
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::mj:Colour
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::mj:Colour
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::mj:OrderNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::mj:OrderNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::mj:OrderNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::mj:FaultCode
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::mj:FaultCode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::mj:FaultCode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::mj:EngineersNotes
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::mj:EngineersNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::mj:EngineersNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Unit Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PreNewJobBooking_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Unit Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Unit Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Unit Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Unit Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:TransitType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:TransitType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:TransitType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ESN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonValidateIMEI
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonValidateIMEI
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::pleasewaitmessage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::pleasewaitmessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::pleasewaitmessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:Manufacturer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:Manufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:Manufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ModelNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSpecificNeeds
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSpecificNeeds
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locSpecificNeeds
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSpecificNeedsType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSpecificNeedsType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locSpecificNeedsType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_PreNewJobBooking_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::Button:AcceptIMEI
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Button:AcceptIMEI
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Button:AcceptIMEI
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::text:SelectFields  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text:SelectFields',p_web.GetValue('NewValue'))
    do Value::text:SelectFields
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text:SelectFields  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('text:SelectFields') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Select the fields that will change from one job to the another.',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::text:SelectFields  Routine
    loc:comment = ''
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('text:SelectFields') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::mj:Colour  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:Colour') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Colour')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::mj:Colour  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('mj:Colour',p_web.GetValue('NewValue'))
    mj:Colour = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::mj:Colour
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('mj:Colour',p_web.GetValue('Value'))
    mj:Colour = p_web.GetValue('Value')
  End
  do Value::mj:Colour
  do SendAlert

Value::mj:Colour  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:Colour') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- mj:Colour
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''mj:Colour'',''prenewjobbooking_mj:colour_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('mj:InProgress') = 1,'disabled','')
  If p_web.GetSessionValue('mj:Colour') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','mj:Colour',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('mj:Colour') & '_value')

Comment::mj:Colour  Routine
    loc:comment = ''
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:Colour') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::mj:OrderNumber  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:OrderNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Order Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::mj:OrderNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('mj:OrderNumber',p_web.GetValue('NewValue'))
    mj:OrderNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::mj:OrderNumber
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('mj:OrderNumber',p_web.GetValue('Value'))
    mj:OrderNumber = p_web.GetValue('Value')
  End
  do Value::mj:OrderNumber
  do SendAlert

Value::mj:OrderNumber  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:OrderNumber') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- mj:OrderNumber
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''mj:OrderNumber'',''prenewjobbooking_mj:ordernumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('mj:InProgress') = 1,'disabled','')
  If p_web.GetSessionValue('mj:OrderNumber') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','mj:OrderNumber',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('mj:OrderNumber') & '_value')

Comment::mj:OrderNumber  Routine
    loc:comment = ''
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:OrderNumber') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::mj:FaultCode  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:FaultCode') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Fault Codes')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::mj:FaultCode  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('mj:FaultCode',p_web.GetValue('NewValue'))
    mj:FaultCode = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::mj:FaultCode
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('mj:FaultCode',p_web.GetValue('Value'))
    mj:FaultCode = p_web.GetValue('Value')
  End
  do Value::mj:FaultCode
  do SendAlert

Value::mj:FaultCode  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:FaultCode') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- mj:FaultCode
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''mj:FaultCode'',''prenewjobbooking_mj:faultcode_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('mj:InProgress') = 1,'disabled','')
  If p_web.GetSessionValue('mj:FaultCode') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','mj:FaultCode',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('mj:FaultCode') & '_value')

Comment::mj:FaultCode  Routine
    loc:comment = ''
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:FaultCode') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::mj:EngineersNotes  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:EngineersNotes') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Engineers Notes')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::mj:EngineersNotes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('mj:EngineersNotes',p_web.GetValue('NewValue'))
    mj:EngineersNotes = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::mj:EngineersNotes
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('mj:EngineersNotes',p_web.GetValue('Value'))
    mj:EngineersNotes = p_web.GetValue('Value')
  End
  do Value::mj:EngineersNotes
  do SendAlert

Value::mj:EngineersNotes  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:EngineersNotes') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- mj:EngineersNotes
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''mj:EngineersNotes'',''prenewjobbooking_mj:engineersnotes_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('mj:InProgress') = 1,'disabled','')
  If p_web.GetSessionValue('mj:EngineersNotes') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','mj:EngineersNotes',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('mj:EngineersNotes') & '_value')

Comment::mj:EngineersNotes  Routine
    loc:comment = ''
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('mj:EngineersNotes') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:TransitType  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:TransitType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Transit Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:TransitType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:TransitType',p_web.GetValue('NewValue'))
    tmp:TransitType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:TransitType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:TransitType',p_web.GetValue('Value'))
    tmp:TransitType = p_web.GetValue('Value')
  End
  If tmp:TransitType = ''
    loc:Invalid = 'tmp:TransitType'
    loc:alert = p_web.translate('Transit Type') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  p_Web.SetValue('lookupfield','tmp:TransitType')
  do AfterLookup
  do Value::tmp:TransitType
  do SendAlert
  do Comment::tmp:TransitType
  do Comment::tmp:TransitType

Value::tmp:TransitType  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:TransitType') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:TransitType
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:TransitType') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GetSessionValue('ReadOnly:TransitType') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:TransitType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:TransitType = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:TransitType'',''prenewjobbooking_tmp:transittype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:TransitType')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','tmp:TransitType',p_web.GetSessionValueFormat('tmp:TransitType'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectTransitTypes')&'?LookupField=tmp:TransitType&Tab=2&ForeignField=trt:Transit_Type&_sort=&Refresh=sort&LookupFrom=PreNewJobBooking&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('tmp:TransitType') & '_value')

Comment::tmp:TransitType  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:TransitType'))
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:TransitType') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('tmp:TransitType') & '_comment')

Prompt::tmp:ESN  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:ESN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('I.M.E.I. Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ESN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ESN',p_web.GetValue('NewValue'))
    tmp:ESN = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ESN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ESN',p_web.GetValue('Value'))
    tmp:ESN = p_web.GetValue('Value')
  End
  If tmp:ESN = ''
    loc:Invalid = 'tmp:ESN'
    loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:ESN = Upper(tmp:ESN)
    p_web.SetSessionValue('tmp:ESN',tmp:ESN)
      p_web.SSV('ReadOnly:Manufacturer',1)
      p_web.SSV('ReadOnly:ModelNumber',1)
  do Value::tmp:ESN
  do SendAlert
  do Value::tmp:Manufacturer  !1
  do Comment::tmp:Manufacturer
  do Value::tmp:ModelNumber  !1
  do Comment::tmp:ModelNumber
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:AcceptIMEI
  do Value::Button:AcceptIMEI  !1
  do Comment::Button:AcceptIMEI
  do Comment::tmp:ESN
  do Value::pleasewaitmessage  !1

Value::tmp:ESN  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:ESN') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:ESN
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:IMEINumber') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:IMEINumber') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:ESN')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:ESN = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ESN'',''prenewjobbooking_tmp:esn_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ESN')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ESN',p_web.GetSessionValueFormat('tmp:ESN'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('tmp:ESN') & '_value')

Comment::tmp:ESN  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:ESN') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('tmp:ESN') & '_comment')

Validate::buttonValidateIMEI  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonValidateIMEI',p_web.GetValue('NewValue'))
    do Value::buttonValidateIMEI
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  Do Value:IMEINumber
  do Value::buttonValidateIMEI
  do SendAlert
  do Value::tmp:Manufacturer  !1
  do Value::tmp:ModelNumber  !1
  do Value::tmp:TransitType  !1
  do Value::tmp:ESN  !1
  do Value::pleasewaitmessage  !1
  do Comment::buttonValidateIMEI
  do Value::Button:AcceptIMEI  !1
  do Comment::Button:AcceptIMEI
  do Prompt::locSpecificNeedsType
  do Value::locSpecificNeedsType  !1

Value::buttonValidateIMEI  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('buttonValidateIMEI') & '_value',Choose(p_web.GetSessionValue('ReadOnly:IMEINumber') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('ReadOnly:IMEINumber') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonValidateIMEI'',''prenewjobbooking_buttonvalidateimei_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ValidateIMEI','Validate I.M.E.I.','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('buttonValidateIMEI') & '_value')

Comment::buttonValidateIMEI  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:IMEINumber'))
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('buttonValidateIMEI') & '_comment',Choose(p_web.GetSessionValue('ReadOnly:IMEINumber') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('ReadOnly:IMEINumber') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('buttonValidateIMEI') & '_comment')

Prompt::pleasewaitmessage  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('pleasewaitmessage') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::pleasewaitmessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('pleasewaitmessage',p_web.GetValue('NewValue'))
    do Value::pleasewaitmessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::pleasewaitmessage  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('pleasewaitmessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('SmallText')&'">' & p_web.Translate('Note: IMEI Validation may take a few mintues to complete.',0) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('pleasewaitmessage') & '_value')

Comment::pleasewaitmessage  Routine
    loc:comment = ''
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('pleasewaitmessage') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:Manufacturer  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:Manufacturer') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Manufacturer')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('tmp:Manufacturer') & '_prompt')

Validate::tmp:Manufacturer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:Manufacturer',p_web.GetValue('NewValue'))
    tmp:Manufacturer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:Manufacturer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:Manufacturer',p_web.GetValue('Value'))
    tmp:Manufacturer = p_web.GetValue('Value')
  End
  If tmp:Manufacturer = ''
    loc:Invalid = 'tmp:Manufacturer'
    loc:alert = p_web.translate('Manufacturer') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:Manufacturer = Upper(tmp:Manufacturer)
    p_web.SetSessionValue('tmp:Manufacturer',tmp:Manufacturer)
  p_Web.SetValue('lookupfield','tmp:Manufacturer')
  do AfterLookup
  do Value::tmp:Manufacturer
  do SendAlert
  do Comment::tmp:Manufacturer
  do Value::tmp:ModelNumber  !1
  do Comment::tmp:ModelNumber
  do Value::tmp:ESN  !1
  do Comment::tmp:ESN
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:AcceptIMEI
  do Value::Button:AcceptIMEI  !1
  do Comment::Button:AcceptIMEI
  do Value::buttonValidateIMEI  !1
  do Comment::buttonValidateIMEI

Value::tmp:Manufacturer  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:Manufacturer') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:Manufacturer
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:Manufacturer') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:Manufacturer') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:Manufacturer')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:Manufacturer = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:Manufacturer'',''prenewjobbooking_tmp:manufacturer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:Manufacturer')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','tmp:Manufacturer',p_web.GetSessionValueFormat('tmp:Manufacturer'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectManufacturers')&'?LookupField=tmp:Manufacturer&Tab=2&ForeignField=man:Manufacturer&_sort=man:Manufacturer&Refresh=sort&LookupFrom=PreNewJobBooking&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('tmp:Manufacturer') & '_value')

Comment::tmp:Manufacturer  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:Manufacturer'))
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:Manufacturer') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('tmp:Manufacturer') & '_comment')

Prompt::tmp:ModelNumber  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:ModelNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Model Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ModelNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ModelNumber',p_web.GetValue('NewValue'))
    tmp:ModelNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ModelNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ModelNumber',p_web.GetValue('Value'))
    tmp:ModelNumber = p_web.GetValue('Value')
  End
  If tmp:ModelNumber = ''
    loc:Invalid = 'tmp:ModelNumber'
    loc:alert = p_web.translate('Model Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:ModelNumber = Upper(tmp:ModelNumber)
    p_web.SetSessionValue('tmp:ModelNumber',tmp:ModelNumber)
  p_Web.SetValue('lookupfield','tmp:ModelNumber')
  do AfterLookup
  do Value::tmp:Manufacturer
  do Value::tmp:ModelNumber
  do SendAlert
  do Comment::tmp:ModelNumber
  do Value::tmp:ESN  !1
  do Comment::tmp:ESN
  do Value::tmp:Manufacturer  !1
  do Comment::tmp:Manufacturer
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:AcceptIMEI
  do Value::Button:AcceptIMEI  !1
  do Comment::Button:AcceptIMEI
  do Comment::tmp:ModelNumber
  do Value::buttonValidateIMEI  !1
  do Comment::buttonValidateIMEI

Value::tmp:ModelNumber  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:ModelNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:ModelNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:ModelNumber') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:ModelNumber') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:ModelNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:ModelNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ModelNumber'',''prenewjobbooking_tmp:modelnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ModelNumber')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','tmp:ModelNumber',p_web.GetSessionValueFormat('tmp:ModelNumber'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectModelNumbers')&'?LookupField=tmp:ModelNumber&Tab=2&ForeignField=mod:Model_Number&_sort=&Refresh=sort&LookupFrom=PreNewJobBooking&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('tmp:ModelNumber') & '_value')

Comment::tmp:ModelNumber  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:ModelNumber'))
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('tmp:ModelNumber') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('tmp:ModelNumber') & '_comment')

Prompt::locSpecificNeeds  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('locSpecificNeeds') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Specific Needs Device')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locSpecificNeeds  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSpecificNeeds',p_web.GetValue('NewValue'))
    locSpecificNeeds = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSpecificNeeds
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locSpecificNeeds',p_web.GetValue('Value'))
    locSpecificNeeds = p_web.GetValue('Value')
  End
  do Value::locSpecificNeeds
  do SendAlert
  do Prompt::locSpecificNeedsType
  do Value::locSpecificNeedsType  !1
  do Value::Button:AcceptIMEI  !1

Value::locSpecificNeeds  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('locSpecificNeeds') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locSpecificNeeds
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSpecificNeeds'',''prenewjobbooking_locspecificneeds_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSpecificNeeds')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locSpecificNeeds') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locSpecificNeeds',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('locSpecificNeeds') & '_value')

Comment::locSpecificNeeds  Routine
    loc:comment = ''
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('locSpecificNeeds') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locSpecificNeedsType  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('locSpecificNeedsType') & '_prompt',Choose(p_web.GSV('locSpecificNeeds') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Specific Needs Type')
  If p_web.GSV('locSpecificNeeds') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('locSpecificNeedsType') & '_prompt')

Validate::locSpecificNeedsType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSpecificNeedsType',p_web.GetValue('NewValue'))
    locSpecificNeedsType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSpecificNeedsType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locSpecificNeedsType',p_web.GetValue('Value'))
    locSpecificNeedsType = p_web.GetValue('Value')
  End
  do Value::locSpecificNeedsType
  do SendAlert
  do Value::Button:AcceptIMEI  !1

Value::locSpecificNeedsType  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('locSpecificNeedsType') & '_value',Choose(p_web.GSV('locSpecificNeeds') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locSpecificNeeds') <> 1)
  ! --- RADIO --- locSpecificNeedsType
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locSpecificNeedsType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locSpecificNeedsType') = 'HEARING IMPAIRED'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSpecificNeedsType'',''prenewjobbooking_locspecificneedstype_value'',1,'''&clip('HEARING IMPAIRED')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSpecificNeedsType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locSpecificNeedsType',clip('HEARING IMPAIRED'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locSpecificNeedsType_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Hearing Impaired') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locSpecificNeedsType') = 'VISUAL IMPAIRMENT'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSpecificNeedsType'',''prenewjobbooking_locspecificneedstype_value'',1,'''&clip('VISUAL IMPAIRMENT')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSpecificNeedsType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locSpecificNeedsType',clip('VISUAL IMPAIRMENT'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locSpecificNeedsType_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Visual Impairment') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locSpecificNeedsType') = 'ELDERLY'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSpecificNeedsType'',''prenewjobbooking_locspecificneedstype_value'',1,'''&clip('ELDERLY')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSpecificNeedsType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locSpecificNeedsType',clip('ELDERLY'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locSpecificNeedsType_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Elderly') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('locSpecificNeedsType') & '_value')

Comment::locSpecificNeedsType  Routine
    loc:comment = ''
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('locSpecificNeedsType') & '_comment',Choose(p_web.GSV('locSpecificNeeds') <> 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('locSpecificNeeds') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::Button:AcceptIMEI  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('Button:AcceptIMEI') & '_prompt',Choose(p_web.GSV('Hide:IMEINumberButton') = 1 OR (p_web.GSV('locSpecificNeeds') = 1 AND p_web.GSV('locSpecificNeedsType') = ''),'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:IMEINumberButton') = 1 OR (p_web.GSV('locSpecificNeeds') = 1 AND p_web.GSV('locSpecificNeedsType') = '')
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('Button:AcceptIMEI') & '_prompt')

Validate::Button:AcceptIMEI  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:AcceptIMEI',p_web.GetValue('NewValue'))
    do Value::Button:AcceptIMEI
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::Button:AcceptIMEI  Routine
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('Button:AcceptIMEI') & '_value',Choose(p_web.GSV('Hide:IMEINumberButton') = 1 OR (p_web.GSV('locSpecificNeeds') = 1 AND p_web.GSV('locSpecificNeedsType') = ''),'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:IMEINumberButton') = 1 OR (p_web.GSV('locSpecificNeeds') = 1 AND p_web.GSV('locSpecificNeedsType') = ''))
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AcceptIMEI','Accept IMEI / Model','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip(p_web.GSV('IMEINumberButtonURL'))) & ''','''&clip('_self')&''')',loc:javascript,0,'images/ptick.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('Button:AcceptIMEI') & '_value')

Comment::Button:AcceptIMEI  Routine
    loc:comment = p_web.Translate(p_web.GetSessionValue('Comment:IMEINumber'))
  p_web._DivHeader('PreNewJobBooking_' & p_web._nocolon('Button:AcceptIMEI') & '_comment',Choose(p_web.GSV('Hide:IMEINumberButton') = 1 OR (p_web.GSV('locSpecificNeeds') = 1 AND p_web.GSV('locSpecificNeedsType') = ''),'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:IMEINumberButton') = 1 OR (p_web.GSV('locSpecificNeeds') = 1 AND p_web.GSV('locSpecificNeedsType') = '')
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('PreNewJobBooking_' & p_web._nocolon('Button:AcceptIMEI') & '_comment')

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('PreNewJobBooking_mj:Colour_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::mj:Colour
      else
        do Value::mj:Colour
      end
  of lower('PreNewJobBooking_mj:OrderNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::mj:OrderNumber
      else
        do Value::mj:OrderNumber
      end
  of lower('PreNewJobBooking_mj:FaultCode_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::mj:FaultCode
      else
        do Value::mj:FaultCode
      end
  of lower('PreNewJobBooking_mj:EngineersNotes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::mj:EngineersNotes
      else
        do Value::mj:EngineersNotes
      end
  of lower('PreNewJobBooking_tmp:TransitType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:TransitType
      else
        do Value::tmp:TransitType
      end
  of lower('PreNewJobBooking_tmp:ESN_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ESN
      else
        do Value::tmp:ESN
      end
  of lower('PreNewJobBooking_buttonValidateIMEI_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonValidateIMEI
      else
        do Value::buttonValidateIMEI
      end
  of lower('PreNewJobBooking_tmp:Manufacturer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:Manufacturer
      else
        do Value::tmp:Manufacturer
      end
  of lower('PreNewJobBooking_tmp:ModelNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ModelNumber
      else
        do Value::tmp:ModelNumber
      end
  of lower('PreNewJobBooking_locSpecificNeeds_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSpecificNeeds
      else
        do Value::locSpecificNeeds
      end
  of lower('PreNewJobBooking_locSpecificNeedsType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSpecificNeedsType
      else
        do Value::locSpecificNeedsType
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('PreNewJobBooking_form:ready_',1)
  p_web.SetSessionValue('PreNewJobBooking_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_PreNewJobBooking',0)

PreCopy  Routine
  p_web.SetValue('PreNewJobBooking_form:ready_',1)
  p_web.SetSessionValue('PreNewJobBooking_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_PreNewJobBooking',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('PreNewJobBooking_form:ready_',1)
  p_web.SetSessionValue('PreNewJobBooking_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('PreNewJobBooking:Primed',0)

PreDelete       Routine
  p_web.SetValue('PreNewJobBooking_form:ready_',1)
  p_web.SetSessionValue('PreNewJobBooking_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('PreNewJobBooking:Primed',0)
  p_web.setsessionvalue('showtab_PreNewJobBooking',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('MultipleJobBooking') = 1
        If (p_web.GSV('mj:InProgress') = 1)
          If p_web.IfExistsValue('mj:Colour') = 0
            p_web.SetValue('mj:Colour',0)
            mj:Colour = 0
          End
        End
        If (p_web.GSV('mj:InProgress') = 1)
          If p_web.IfExistsValue('mj:OrderNumber') = 0
            p_web.SetValue('mj:OrderNumber',0)
            mj:OrderNumber = 0
          End
        End
        If (p_web.GSV('mj:InProgress') = 1)
          If p_web.IfExistsValue('mj:FaultCode') = 0
            p_web.SetValue('mj:FaultCode',0)
            mj:FaultCode = 0
          End
        End
        If (p_web.GSV('mj:InProgress') = 1)
          If p_web.IfExistsValue('mj:EngineersNotes') = 0
            p_web.SetValue('mj:EngineersNotes',0)
            mj:EngineersNotes = 0
          End
        End
  End
          If p_web.IfExistsValue('locSpecificNeeds') = 0
            p_web.SetValue('locSpecificNeeds',0)
            locSpecificNeeds = 0
          End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('PreNewJobBooking_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('PreNewJobBooking_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 5
  If p_web.GSV('MultipleJobBooking') = 1
    loc:InvalidTab += 1
  End
  ! tab = 1
    loc:InvalidTab += 1
        If tmp:TransitType = ''
          loc:Invalid = 'tmp:TransitType'
          loc:alert = p_web.translate('Transit Type') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
        If tmp:ESN = ''
          loc:Invalid = 'tmp:ESN'
          loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:ESN = Upper(tmp:ESN)
          p_web.SetSessionValue('tmp:ESN',tmp:ESN)
        If loc:Invalid <> '' then exit.
        If tmp:Manufacturer = ''
          loc:Invalid = 'tmp:Manufacturer'
          loc:alert = p_web.translate('Manufacturer') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:Manufacturer = Upper(tmp:Manufacturer)
          p_web.SetSessionValue('tmp:Manufacturer',tmp:Manufacturer)
        If loc:Invalid <> '' then exit.
        If tmp:ModelNumber = ''
          loc:Invalid = 'tmp:ModelNumber'
          loc:alert = p_web.translate('Model Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:ModelNumber = Upper(tmp:ModelNumber)
          p_web.SetSessionValue('tmp:ModelNumber',tmp:ModelNumber)
        If loc:Invalid <> '' then exit.
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
  !Validation
  Do ClearVariables
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('PreNewJobBooking:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('mj:Colour')
  p_web.StoreValue('mj:OrderNumber')
  p_web.StoreValue('mj:FaultCode')
  p_web.StoreValue('mj:EngineersNotes')
  p_web.StoreValue('tmp:TransitType')
  p_web.StoreValue('tmp:ESN')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:Manufacturer')
  p_web.StoreValue('tmp:ModelNumber')
  p_web.StoreValue('locSpecificNeeds')
  p_web.StoreValue('locSpecificNeedsType')
  p_web.StoreValue('')
local.CheckLength       Procedure(String f:Type)
local:String            CString(31)
local:LengthFrom        Long()
local:LengthTo          Long()
Code
    Case f:Type
    Of 'IMEI'
        tmp:ModelNumber = p_web.GetSessionValue('tmp:ModelNumber')
        If tmp:ModelNumber = ''
            Return 0
        End ! If p_web.GetSessionValue('tmp:ModelNumber') = '' Or p_web.GetSessionValue('tmp:ModelNumber') = '- Select Model Number -'
        Access:MODELNUM.ClearKey(mod:Model_Number_Key)
        mod:Model_Number = tmp:ModelNumber
        If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
            !Found
            local:String = p_web.GetSessionValue('tmp:ESN')
            If Len(local:String) < mod:ESN_Length_From Or Len(local:String) > mod:ESN_Length_To
                If mod:ESN_Length_To = mod:ESN_Length_From
                    p_web.SetSessionValue('Comment:IMEINumber','IMEI Number should be ' & Clip(mod:ESN_Length_To) & ' characters long.')
                Else ! If mod:ESN_Length_To = mod:ESN_Length_From
                    p_web.SetSessionValue('Comment:IMEINumber','IMEI Number should be ' & Clip(mod:ESN_Length_From) & ' to ' & |
                                                                Clip(mod:ESN_Length_To) & ' characters long.')
                End ! If mod:ESN_Length_To = mod:ESN_Length_From
                Do Reset:IMEINumber
                Return 1
            End ! If Len(local:String) < mod:ESN_Length_From Or Len(local:String) > mod:ESN_Length_To
        Else ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
            !Error
        End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign


    End ! Case f:Type

    Return 0
local.IMEIModelNumber       Procedure()
ModelQueue                      Queue(),Pre(local)
ModManufacturer                     String(30)
ModelNumber                         String(30)
                                End ! ModelQueue        Queue()
ManufacturerQueue               Queue(),Pre(local)
Manufacturer                        String(30)
                                End ! Manufacturer        Queue(),Pre(local)
local:Filter                    String(1000)
inactiveManufacturer            BYTE(0)
inactiveManufacturerName        STRING(30)
    Code
        filter:Manufacturer = ''
        filter:ModelNumber = ''
        p_web.SSV('filter:ModelNumber','')
        p_web.SSV('filter:Manufacturer','')
    !Match the first 6 digits of the IMEI Number
        tmp:ESN = p_web.GSV('tmp:ESN')
        tmp:Manufacturer = p_web.GSV('tmp:Manufacturer')
        Access:ESNMODEL.Clearkey(esn:ESN_Only_Key)
        esn:ESN = Sub(tmp:ESN,1,6)
        Set(esn:ESN_Only_Key,esn:ESN_Only_Key)
        Loop ! Begin Loop
            If Access:ESNMODEL.Next()
                Break
            End ! If Access:ESNMODEL.Next()
            If esn:ESN <> Sub(tmp:ESN,1,6)
                Break
            End ! If esn_ESN <> Sub(p_web.GSV('tmp:ESN'),1,6)
            
            ! #11466 Only include active models (DBH: 30/06/2010)
            IF (esn:Active <> 'Y')
                CYCLE
            END
            
            ! #11466 Only include active models (DBH: 30/06/2010)
            Access:MODELNUM.ClearKey(mod:Manufacturer_Key)
            mod:Manufacturer = esn:Manufacturer
            mod:Model_Number = esn:Model_Number
            IF (Access:MODELNUM.TryFetch(mod:Manufacturer_Key))
            END
            IF (mod:Active <> 'Y')
                CYCLE
            END       
            
            ! #12488 Record if the manufacturer is inactive
            ! (Although i'm not sure how this copes with multiple manufacturers) (DBH: 17/09/2012) 
            Access:MANUFACT.Clearkey(man:Manufacturer_Key)
            man:Manufacturer = mod:Manufacturer
            IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                !IF (man:Notes[1:8] = 'INACTIVE')
                IF (man:Inactive = 1)
                    inactiveManufacturer = 1
                    inactiveManufacturerName = man:Manufacturer
                    CYCLE
                END
            END
            

            local:Manufacturer = esn:Manufacturer
            Get(ManufacturerQueue,local:Manufacturer)
            If Error()
                local:Manufacturer = esn:Manufacturer
                Add(ManufacturerQueue)
            End ! If Error()

            If tmp:Manufacturer <> ''
                If tmp:Manufacturer <> esn:Manufacturer
                    Cycle
                End ! If tmp:Manufacturer <> esn:Manufacturer
            End ! If tmp:Manufacturer <> ''
            
            local:ModelNumber = esn:Model_Number
            local:ModManufacturer = esn:Manufacturer
            Add(ModelQueue)

        End ! Loop

    !Match the first 8 digits of the IMEI Number
        Access:ESNMODEL.Clearkey(esn:ESN_Only_Key)
        esn:ESN = Sub(tmp:ESN,1,8)
        Set(esn:ESN_Only_Key,esn:ESN_Only_Key)
        Loop ! Begin Loop
            If Access:ESNMODEL.Next()
                Break
            End ! If Access:ESNMODEL.Next()
            If esn:ESN <> Sub(tmp:ESN,1,8)
                Break
            End ! If esn_ESN <> Sub(p_web.GSV('tmp:ESN'),1,6)
            
            ! #11466 Remove models if allowed in 6 digit (DBH: 30/06/2010)
            IF (esn:Active <> 'Y')
                CYCLE
            END
            
            ! #11466 Only include active models (DBH: 30/06/2010)
            Access:MODELNUM.ClearKey(mod:Manufacturer_Key)
            mod:Manufacturer = esn:Manufacturer
            mod:Model_Number = esn:Model_Number
            IF (Access:MODELNUM.TryFetch(mod:Manufacturer_Key))
            END
            IF (mod:Active <> 'Y')
                CYCLE
            END
            
            ! #12488 Record if the manufacturer is inactive
            ! (Although i'm not sure how this copes with multiple manufacturers) (DBH: 17/09/2012) 
            Access:MANUFACT.Clearkey(man:Manufacturer_Key)
            man:Manufacturer = mod:Manufacturer
            IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                !IF (man:Notes[1:8] = 'INACTIVE')
                IF (man:Inactive = 1)
                    inactiveManufacturer = 1
                    inactiveManufacturerName = man:Manufacturer
                    CYCLE
                END
            END

            local:Manufacturer = esn:Manufacturer
            Get(ManufacturerQueue,local:Manufacturer)
            If Error()
                local:Manufacturer = esn:Manufacturer
                Add(ManufacturerQueue)
            End ! If Error()

            If tmp:Manufacturer <> ''
                If tmp:Manufacturer <> esn:Manufacturer
                    Cycle
                End ! If tmp:Manufacturer <> esn:Manufacturer
            End ! If tmp:Manufacturer <> ''
            
            local:ModelNumber = esn:Model_Number
            Get(ModelQueue,local:ModelNumber)
            If Error()
                local:ModelNumber = esn:Model_Number
                local:ModManufacturer = esn:Manufacturer
                Add(ModelQueue)
            End ! If Error()

        End ! Loop

        p_web.SSV('save:FilterModelNumber','')
        Case Records(ModelQueue)
        Of 0
            IF (inactiveManufacturer)
                p_web.SSV('Comment:IMEINumber','Model found but Manufacturer ' & CLIP(inactiveManufacturerName) & ' is inactive.')
            ELSE
                p_web.SSV('Comment:IMEINumber','There are no model numbers matching the selected IMEI Number.')
            END
            
            p_web.SSV('tmp:ESN','')
            Do Reset:IMEINumber
            Return True
        Of 1
            Get(ModelQueue,1)
            p_web.SSV('tmp:ModelNumber',local:ModelNumber)
            p_web.SSV('tmp:Manufacturer',local:ModManufacturer)
            p_web.SSV('filter:ModelNumber','mod:Model_Number = ''' & Clip(local:ModelNumber) & '''')
            Case Records(ManufacturerQueue)
            Of 1
                p_web.SSV('filter:Manufacturer','man:Manufacturer = ''' & Clip(local:ModManufacturer) & '''')
            Else
                Loop x# = 1 To Records(ManufacturerQueue)
                    Get(ManufacturerQueue,x#)
                    filter:Manufacturer = Clip(filter:Manufacturer) & ' OR man:Manufacturer = ''' & Clip(local:Manufacturer) & ''''
                End ! Loop x# = 1 To Records(ManufacturerQueue)
                filter:Manufacturer = Clip(Sub(filter:Manufacturer,5,1000))
                p_web.SSV('filter:Manufacturer',filter:Manufacturer)
            End ! Case Record(ManufacturerQueue)
!        p_web.SSV('Comment:IMEINumber','Valid')
            p_web.SSV('ReadOnly:Manufacturer',1)
            p_web.SSV('ReadOnly:ModelNumber',1)
        Else
            p_web.SSV('ReadOnly:Manufacturer','')
            p_web.SSV('ReadOnly:ModelNumber','')

            Loop x# = 1 To Records(ModelQueue)
                Get(ModelQueue,x#)
                filter:ModelNumber = Clip(filter:ModelNumber) & ' OR Upper(mod:Model_Number) = Upper(<39>' & Clip(local:ModelNumber) & '<39>)'
            End ! Loop x# = 1 To Records(ModelQueue)

            If Records(ManufacturerQueue) = 1
                Get(ManufacturerQueue,1)
                filter:Manufacturer = 'Upper(man:Manufacturer) = Upper(<39>' & Clip(local:Manufacturer) & '<39>)'
                p_web.SSV('tmp:Manufacturer',local:Manufacturer)
                p_web.SSV('ReadOnly:Manufacturer',1)
            Else ! If Records(ManufacturerQueue) = 1
                Loop x# = 1 To Records(ManufacturerQueue)
                    Get(ManufacturerQueue,x#)
                    filter:Manufacturer = Clip(filter:Manufacturer) & ' OR Upper(man:Manufacturer) = Upper(<39>' & Clip(local:Manufacturer) & '<39>)'
                End ! Loop x# = 1 To Records(ManufacturerQueue)
                filter:Manufacturer = Clip(Sub(filter:Manufacturer,5,1000))
            End ! If Records(ManufacturerQueue) = 1

            filter:ModelNumber = Clip(Sub(filter:ModelNumber,5,1000))

            p_web.SSV('filter:Manufacturer',filter:Manufacturer)
            p_web.SSV('filter:ModelNumber',filter:ModelNumber)
            p_web.SSV('save:FilterModelNumber',filter:ModelNumber)
            
            ! #11344 Show that the man/mod must be entered before a geniune validation will be done. (DBH: 29/06/2010)
            p_web.SSV('Comment:IMEINumber','IMEI Number will be validated once the Manufacturer and Model Number have been selected.')

        End ! Case Records(ModelQueue)
       

        Return False
local.IsIMEIValid        Procedure()
Code
    tmp:ESN = p_web.GetSessionValue('tmp:ESN')
    tmp:ModelNumber = p_web.GetSessionValue('tmp:ModelNumber')

    If tmp:ESN = '' Or tmp:ModelNumber = ''
        Return True
    End ! If tmp:ESN = '' Or tmp:ModelNumber = ''

    Found# = False
    Loop x# = 1 To Len(Clip(tmp:ESN))
        If IsAlpha(Sub(tmp:ESN,x#,1))
            Found# = True
        End ! If IsAlpha(Sub(tmp:ESN,x#,1)
    End ! Loop x# = 1 To Len(Clip(tmp:ESN))

    If Found# = True
        Access:MODELNUM.ClearKey(mod:Model_Number_Key)
        mod:Model_Number = tmp:ModelNumber
        If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
            !Found
            If mod:AllowIMEICharacters
                Return True
            Else ! If mod:AllowIMEICharacter
                p_web.SetSessionValue('Comment:IMEINumber','IMEI Number Format is invalid.')
                p_web.SetSessionValue('tmp:ESN','')
                p_web.SetSessionValue('Save:IMEINumber','')
                Do Reset:IMEINumber
                Return False
            End ! If mod:AllowIMEICharacter
        Else ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
            !Error
        End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
    End ! If Found# = True
    Return True
local.ValidateIMEI        Procedure()
Code
    If p_web.GetSessionValue('tmp:ModelNumber') = ''
        Return False
    End ! If p_web.GetSessionValue('tmp:ModelNumber') = ''
    If p_web.GetSessionValue('tmp:ESN') = ''
        Return False
    End ! If p_web.GetSessionValue('tmp:ESN') = ''
    If CheckLength('IMEI',p_web.GetSessionValue('tmp:ModelNumber'),p_web.GetSessionValue('tmp:ESN'))
        Do Reset:IMEINumber
        p_web.SetSessionValue('Comment:IMEINumber','Invalid Length.')
        Return True
    End ! If local.CheckLength('IMEI')

    If local.IsIMEIValid() = 0
        Do Reset:IMEINumber
        Return True
    End ! If local.IsIMEIValid() = 0

    Return False
local.ValidateMSN     Procedure()
Code
    If p_web.GetSessionValue('Hide:MSN') = 1
        Return 0
    End ! If p_web.GetSessionValue('job:MSN') = '' Or p_web.GetSessionValue('Hide:MSN') = 1


    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = job:Manufacturer
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Found
        If man:ApplyMSNFormat
            If CheckFaultFormat(job:MSN,man:MSNFormat)
                p_web.SetSessionValue('job:MSN','')
                p_web.SetSessionValue('Comment:MSN','Invalid Format.')
                Return 1
            End ! If CheckFaultFormat(job:MSN,man:MSNFormat)
        Else ! If man:ApplyMSNFormat
            If CheckLength('MSN',job:Model_Number,job:MSN)
                p_web.SetSessionValue('job:MSN','')
                p_web.SetSessionValue('Comment:MSN','Invalid Length.')
                Return 1
            End ! If CheckLength('MSN',p_web.GetSessionValue('tmp:Manufacturer'),p_web.GetSessionValue('job:MSN'))

        End ! If man:ApplyMSNFormat
    Else ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Error
    End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    Return 0
