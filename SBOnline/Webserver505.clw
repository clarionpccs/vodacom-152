

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER505.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
ds_Message PROCEDURE (STRING MessageTxt,<STRING HeadingTxt>,<STRING IconSent>,<STRING ButtonsPar>,UNSIGNED MsgDefaults=0,BOOL StylePar=FALSE)

FilesOpened          BYTE                                  !
Loc:ButtonPressed    UNSIGNED                              !
EmailLink            CSTRING(4096)                         !
DontShowThisAgain       byte(0)         !CapeSoft MessageBox Data
LocalMessageBoxdata     group,pre(LMBD) !CapeSoft MessageBox Data
MessageText               cstring(4096)
HeadingText               cstring(1024)
UseIcon                   cstring(1024)
Buttons                   cstring(1024)
Defaults                  unsigned
                        end
window               WINDOW('Caption'),AT(,,404,108),FONT('MS Sans Serif',8,,FONT:regular),GRAY
                       IMAGE,AT(11,18),USE(?Image1),HIDE
                       PROMPT(''''),AT(118,32),USE(?MainTextPrompt)
                       STRING('HyperActive Link'),AT(91,46),USE(?HALink),HIDE
                       STRING('Time Out:'),AT(103,54),USE(?TimerCounter),HIDE
                       CHECK('Dont Show This Again'),AT(85,65),USE(DontShowThisAgain),HIDE
                       BUTTON('Button 1'),AT(9,81,45,14),USE(?Button1),HIDE
                       BUTTON('Button 2'),AT(59,81,45,14),USE(?Button2),HIDE
                       BUTTON('Button 3'),AT(109,81,45,14),USE(?Button3),HIDE
                       BUTTON('Button 4'),AT(159,81,45,14),USE(?Button4),HIDE
                       BUTTON('Button 5'),AT(209,81,45,14),USE(?Button5),HIDE
                       BUTTON('Button 6'),AT(259,81,45,14),USE(?Button6),HIDE
                       BUTTON('Button 7'),AT(309,81,45,14),USE(?Button7),HIDE
                       BUTTON('Button 8'),AT(359,81,45,14),USE(?Button8),HIDE
                     END

locCallingWindowName STRING(255)
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(Loc:ButtonPressed)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ThisMessageBoxLogInit          routine         !CapeSoft MessageBox Initialize properties routine
    if ~omitted(2) then LMBD:HeadingText = HeadingTxt .
    if ~omitted(3) then LMBD:UseIcon = IconSent .
    if ~omitted(4) then LMBD:Buttons = ButtonsPar .
    if ~omitted(5) then LMBD:Defaults = MsgDefaults .
    LMBD:MessageText = MessageTxt
    ThisMessageBox.FromExe = command(0)
    if instring('\',ThisMessageBox.FromExe,1,1)
      ThisMessageBox.FromExe = ThisMessageBox.FromExe[ (instring('\',ThisMessageBox.FromExe,-1,len(ThisMessageBox.FromExe)) + 1) : len(ThisMessageBox.FromExe) ]
    end
    ThisMessageBox.TrnStrings = 1
    ThisMessageBox.PromptControl = ?MainTextPrompt
    ThisMessageBox.IconControl = ?Image1
         !End of CapeSoft MessageBox Initialize properties routine
    AddToLog('Message Box Open',CLIP(MessageTxt),CLIP(locCallingWindowName))

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !  ThisMessageBox.DontShowWindow = 1
    ThisMessageBox.SavedResponse = GlobalResponse        !CapeSoft MessageBox Code - Preserves the GlobalResponse
  GlobalErrors.SetProcedureName('ds_Message')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
        GlobalRequest = ThisWindow.Request         !Keep GlobalRequest the correct value.
        if ThisMessageBox.MessagesUsed then
          return level:fatal
        end
        ThisMessageBox.MessagesUsed += 1
        do ThisMessageBoxLogInit                               !CapeSoft MessageBox Code
        ThisMessageBox.ReturnValue = ThisMessageBox.PreOpen(LMBD:MessageText,LMBD:HeadingText,LMBD:Defaults)
        if ThisMessageBox.ReturnValue then
          self.Response = RequestCompleted
          return level:notify
        end
        ThisMessageBox.DontShowThisAgain = ?DontShowThisAgain
        ThisMessageBox.TimeOutPrompt = ?TimerCounter
        ThisMessageBox.HAControl = ?HALink
  SELF.Open(window)                                        ! Open window
  locCallingWindowName = 0{PROP:Text}
  Do DefineListboxStyle
      IF (INSTRING('WINDOWS 7',UPPER(SYSTEM {PROP:WindowsVersion}),1,1) OR |
          INSTRING('WINDOWS VISTA',UPPER(SYSTEM{PROP:WindowsVersion}),1,1) OR |
          INSTRING('SERVER 2008',UPPER(SYSTEM{PROP:WindowsVersion}),1,1))
          0{prop:FontName} = 'Segoe UI'
          0{prop:FontSize} = 9
      ELSE
          0{prop:FontName} = 'Tahoma'
          0{prop:FontSize} = 8
      END
  ThisMessageBox.open(LMBD:MessageText,LMBD:HeadingText,LMBD:UseIcon,LMBD:Buttons,LMBD:Defaults,StylePar)         !CapeSoft MessageBox Code
      alert(EscKey)
      IF (0{prop:Width} < 150)
          0{prop:Width} = 150
      END
      0{PROP:Height} = 0{PROP:Height} + 2
  
      SETPENCOLOR(COLOR:BTNSHADOW)
      BOX(-1,-1,0{PROP:Width} + 2,0{PROP:Height} - 18,COLOR:White)
      IMAGE(?Image1{PROP:Xpos},?Image1{PROP:Ypos},?Image1{PROP:Width},?Image1{PROP:Height},?Image1{PROP:Text})
  
      ?Button1{PROP:Ypos} = ?Button1{PROP:Ypos} + 4
      ?Button2{PROP:Ypos} = ?Button2{PROP:Ypos} + 4
      ?Button3{PROP:Ypos} = ?Button3{PROP:Ypos} + 4
      ?Button4{PROP:Ypos} = ?Button4{PROP:Ypos} + 4
      ?Button5{PROP:Ypos} = ?Button5{PROP:Ypos} + 4
      ?Button6{PROP:Ypos} = ?Button6{PROP:Ypos} + 4
      ?Button7{PROP:Ypos} = ?Button7{PROP:Ypos} + 4
      ?Button8{PROP:Ypos} = ?Button8{PROP:Ypos} + 4
  
      IF (?Button8{PROP:Hide} = 0)
          b8# = ?Button8{PROP:Width} + 4
      END
      IF (?Button7{PROP:Hide} = 0)
          b7# = ?Button7{PROP:Width} + 4
      END
      IF (?Button6{PROP:Hide} = 0)
          b6# = ?Button6{PROP:Width} + 4
      END
      IF (?Button5{PROP:Hide} = 0)
          b5# = ?Button5{PROP:Width} + 4
      END
      IF (?Button4{PROP:Hide} = 0)
          b4# = ?Button4{PROP:Width} + 4
      END
      IF (?Button3{PROP:Hide} = 0)
          b3# = ?Button3{PROP:Width} + 4
      END
      IF (?Button2{PROP:Hide} = 0)
          b2# = ?Button2{PROP:Width} + 4
      END
      IF (?Button1{PROP:Hide} = 0)
          b1# = ?Button1{PROP:Width} + 4
      END
  
      ?Button8{PROP:Xpos} = 0{PROP:Width} - b8#
      ?Button7{PROP:Xpos} = 0{PROP:Width} - b8# - b7#
      ?Button6{PROP:Xpos} = 0{PROP:Width} - b8# - b7# - b6#
      ?Button5{PROP:Xpos} = 0{PROP:Width} - b8# - b7# - b6# - b5#
      ?Button4{PROP:Xpos} = 0{PROP:Width} - b8# - b7# - b6# - b5# - b4#
      ?Button3{PROP:Xpos} = 0{PROP:Width} - b8# - b7# - b6# - b5# - b4# - b3#
      ?Button2{PROP:Xpos} = 0{PROP:Width} - b8# - b7# - b6# - b5# - b4# - b3# - b2#
      ?Button1{PROP:Xpos} = 0{PROP:Width} - b8# - b7# - b6# - b5# - b4# - b3# - b2# - b1#
  
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
        IF (INSTRING('Try Again?',MessageTxt,1,1))
            ThisMessageBox.ReturnValue = 4
        END ! IF
              !CapeSoft MessageBox EndCode
    Loc:ButtonPressed = ThisMessageBox.ReturnValue
    ThisMessageBox.Close()
    if ThisMessageBox.MessagesUsed > 0 then ThisMessageBox.MessagesUsed -= 1 .
              !End of CapeSoft MessageBox EndCode
  AddToLog('Message Box Close',CLIP(MessageTxt),CLIP(locCallingWindowName),'','Button Pressed = ' & CLIP(LOC:ButtonPressed))
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
    ReturnValue = ThisMessageBox.SavedResponse           !CapeSoft MessageBox Code - Preserves the GlobalResponse
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ThisMessageBox.TakeEvent()                             !CapeSoft MessageBox Code
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

