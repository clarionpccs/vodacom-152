

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER039.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER038.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER040.INC'),ONCE        !Req'd for module callout resolution
                     END


JobEstimate          PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locEstAccBy          STRING(30)                            !
locEstAccCommunicationMethod STRING(30)                    !
locEstRejBy          STRING(30)                            !
locEstRejCommunicationMethod STRING(30)                    !
locEstRejReason      STRING(30)                            !
ErrorStr                    STRING(255)
FilesOpened     Long
AUDSTATS::State  USHORT
JOBS::State  USHORT
ESREJRES::State  USHORT
PARTS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
locEstRejReason_OptionView   View(ESREJRES)
                          Project(esr:RejectionReason)
                        End
  CODE
  GlobalErrors.SetProcedureName('JobEstimate')
  loc:formname = 'JobEstimate_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'JobEstimate',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('JobEstimate','')
    p_web._DivHeader('JobEstimate',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferJobEstimate',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobEstimate',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobEstimate',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_JobEstimate',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferJobEstimate',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_JobEstimate',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'JobEstimate',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(AUDSTATS)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(ESREJRES)
  p_web._OpenFile(PARTS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(AUDSTATS)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(ESREJRES)
  p_Web._CloseFile(PARTS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  ! Initi
      p_web.SSV('Hide:EstimateAccepted',1)
      p_web.SSV('Hide:EstimateRejected',1)
      p_web.SSV('Comment:EstimateReady','')
      p_web.SSV('Comment:EstimateAccepted','')
      p_web.SSV('Comment:EstimateRejected','')
  p_web.SetValue('JobEstimate_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:Estimate_If_Over')
    p_web.SetPicture('job:Estimate_If_Over','@n14.2')
  End
  p_web.SetSessionPicture('job:Estimate_If_Over','@n14.2')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('job:Estimate',job:Estimate)
  p_web.SetSessionValue('job:Estimate_If_Over',job:Estimate_If_Over)
  p_web.SetSessionValue('job:Estimate_Ready',job:Estimate_Ready)
  p_web.SetSessionValue('job:Estimate_Accepted',job:Estimate_Accepted)
  p_web.SetSessionValue('job:Estimate_Rejected',job:Estimate_Rejected)
  p_web.SetSessionValue('locEstAccBy',locEstAccBy)
  p_web.SetSessionValue('locEstAccCommunicationMethod',locEstAccCommunicationMethod)
  p_web.SetSessionValue('locEstRejBy',locEstRejBy)
  p_web.SetSessionValue('locEstRejCommunicationMethod',locEstRejCommunicationMethod)
  p_web.SetSessionValue('locEstRejReason',locEstRejReason)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('job:Estimate')
    job:Estimate = p_web.GetValue('job:Estimate')
    p_web.SetSessionValue('job:Estimate',job:Estimate)
  End
  if p_web.IfExistsValue('job:Estimate_If_Over')
    job:Estimate_If_Over = p_web.dformat(clip(p_web.GetValue('job:Estimate_If_Over')),'@n14.2')
    p_web.SetSessionValue('job:Estimate_If_Over',job:Estimate_If_Over)
  End
  if p_web.IfExistsValue('job:Estimate_Ready')
    job:Estimate_Ready = p_web.GetValue('job:Estimate_Ready')
    p_web.SetSessionValue('job:Estimate_Ready',job:Estimate_Ready)
  End
  if p_web.IfExistsValue('job:Estimate_Accepted')
    job:Estimate_Accepted = p_web.GetValue('job:Estimate_Accepted')
    p_web.SetSessionValue('job:Estimate_Accepted',job:Estimate_Accepted)
  End
  if p_web.IfExistsValue('job:Estimate_Rejected')
    job:Estimate_Rejected = p_web.GetValue('job:Estimate_Rejected')
    p_web.SetSessionValue('job:Estimate_Rejected',job:Estimate_Rejected)
  End
  if p_web.IfExistsValue('locEstAccBy')
    locEstAccBy = p_web.GetValue('locEstAccBy')
    p_web.SetSessionValue('locEstAccBy',locEstAccBy)
  End
  if p_web.IfExistsValue('locEstAccCommunicationMethod')
    locEstAccCommunicationMethod = p_web.GetValue('locEstAccCommunicationMethod')
    p_web.SetSessionValue('locEstAccCommunicationMethod',locEstAccCommunicationMethod)
  End
  if p_web.IfExistsValue('locEstRejBy')
    locEstRejBy = p_web.GetValue('locEstRejBy')
    p_web.SetSessionValue('locEstRejBy',locEstRejBy)
  End
  if p_web.IfExistsValue('locEstRejCommunicationMethod')
    locEstRejCommunicationMethod = p_web.GetValue('locEstRejCommunicationMethod')
    p_web.SetSessionValue('locEstRejCommunicationMethod',locEstRejCommunicationMethod)
  End
  if p_web.IfExistsValue('locEstRejReason')
    locEstRejReason = p_web.GetValue('locEstRejReason')
    p_web.SetSessionValue('locEstRejReason',locEstRejReason)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('JobEstimate_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locEstAccBy = p_web.RestoreValue('locEstAccBy')
 locEstAccCommunicationMethod = p_web.RestoreValue('locEstAccCommunicationMethod')
 locEstRejBy = p_web.RestoreValue('locEstRejBy')
 locEstRejCommunicationMethod = p_web.RestoreValue('locEstRejCommunicationMethod')
 locEstRejReason = p_web.RestoreValue('locEstRejReason')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('JobEstimate_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('JobEstimate_ChainTo')
    loc:formaction = p_web.GetSessionValue('JobEstimate_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="JobEstimate" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="JobEstimate" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="JobEstimate" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Estimate Details') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Estimate Details',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_JobEstimate">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_JobEstimate" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_JobEstimate')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_JobEstimate')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_JobEstimate'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.job:Estimate')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_JobEstimate')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobEstimate_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Estimate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Estimate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Estimate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Estimate_If_Over
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Estimate_If_Over
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Estimate_If_Over
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Estimate_Ready
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Estimate_Ready
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Estimate_Ready
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Estimate_Accepted
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Estimate_Accepted
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Estimate_Accepted
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Estimate_Rejected
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Estimate_Rejected
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Estimate_Rejected
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobEstimate_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::textEstimateAccepted
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::textEstimateAccepted
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEstAccBy
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEstAccBy
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEstAccBy
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEstAccCommunicationMethod
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEstAccCommunicationMethod
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEstAccCommunicationMethod
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_JobEstimate_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::textEstimateRejected
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::textEstimateRejected
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEstRejBy
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEstRejBy
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEstRejBy
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEstRejCommunicationMethod
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEstRejCommunicationMethod
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEstRejCommunicationMethod
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEstRejReason
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEstRejReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEstRejReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::job:Estimate  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Estimate Required')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Estimate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Estimate',p_web.GetValue('NewValue'))
    job:Estimate = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Estimate
    do Value::job:Estimate
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    p_web.SetSessionValue('job:Estimate',p_web.GetValue('Value'))
    job:Estimate = p_web.GetValue('Value')
  End
  if (p_web.GSV('job:Estimate') = 'YES')
      ! #11656 Set status when estimate = yes / no (Bryan: 23/08/2010)
      p_web.SSV('GetStatus:Type','JOB')
      p_web.SSV('GetStatus:StatusNumber',505)
      getStatus(505,0,'JOB',p_web)
  ELSE
      ! Get the previous Status
      if (p_web.GSV('job:Current_Status') = '505 ESTIMATE REQUIRED')
          Access:AUDSTATS.CLearkey(aus:StatusTypeRecordKey)
          aus:RefNumber = p_web.GSV('job:Ref_Number')
          aus:Type = 'JOB'
          Set(aus:StatusTypeRecordKey,aus:StatusTypeRecordKey)
          Loop Until Access:AUDSTATS.Next()
              if (aus:RefNumber <> p_web.GSV('job:Ref_Number'))
                  break
              end
              if (aus:Type <> 'JOB')
                  cycle
              end
              if (aus:NewStatus = '505 ESTIMATE REQUIRED')
                  p_web.SSV('GetStatus:Type','JOB')
                  p_web.SSV('GetStatus:StatusNumber',sub(aus:OldStatus,1,3))
                  getStatus(sub(aus:OldStatus,1,3),0,'JOB',p_web)
                  break
              end
          end
      end
  END
  do Value::job:Estimate
  do SendAlert
  do Prompt::job:Estimate_Accepted
  do Value::job:Estimate_Accepted  !1
  do Prompt::job:Estimate_If_Over
  do Value::job:Estimate_If_Over  !1
  do Prompt::job:Estimate_Ready
  do Value::job:Estimate_Ready  !1
  do Prompt::job:Estimate_Rejected
  do Value::job:Estimate_Rejected  !1

Value::job:Estimate  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- job:Estimate
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''job:Estimate'',''jobestimate_job:estimate_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Estimate')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('job:Estimate') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','job:Estimate',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('job:Estimate') & '_value')

Comment::job:Estimate  Routine
    loc:comment = ''
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Estimate_If_Over  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_If_Over') & '_prompt',Choose(p_web.GSV('job:Estimate') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Estimate If Over')
  If p_web.GSV('job:Estimate') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('job:Estimate_If_Over') & '_prompt')

Validate::job:Estimate_If_Over  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Estimate_If_Over',p_web.GetValue('NewValue'))
    job:Estimate_If_Over = p_web.GetValue('NewValue') !FieldType= REAL Field = job:Estimate_If_Over
    do Value::job:Estimate_If_Over
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Estimate_If_Over',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    job:Estimate_If_Over = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::job:Estimate_If_Over
  do SendAlert

Value::job:Estimate_If_Over  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_If_Over') & '_value',Choose(p_web.GSV('job:Estimate') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Estimate') <> 'YES')
  ! --- STRING --- job:Estimate_If_Over
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('job:Estimate_If_Over')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Estimate_If_Over'',''jobestimate_job:estimate_if_over_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Estimate_If_Over',p_web.GetSessionValue('job:Estimate_If_Over'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('job:Estimate_If_Over') & '_value')

Comment::job:Estimate_If_Over  Routine
      loc:comment = ''
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_If_Over') & '_comment',Choose(p_web.GSV('job:Estimate') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Estimate') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Estimate_Ready  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Ready') & '_prompt',Choose(p_web.GSV('job:Estimate') <> 'YES' OR p_web.GSV('job:Estimate_Ready') <> 'YES' or (p_web.GSV('job:Estimate_Ready') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES')),'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Estimate Ready')
  If p_web.GSV('job:Estimate') <> 'YES' OR p_web.GSV('job:Estimate_Ready') <> 'YES' or (p_web.GSV('job:Estimate_Ready') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES'))
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('job:Estimate_Ready') & '_prompt')

Validate::job:Estimate_Ready  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Estimate_Ready',p_web.GetValue('NewValue'))
    job:Estimate_Ready = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Estimate_Ready
    do Value::job:Estimate_Ready
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    p_web.SetSessionValue('job:Estimate_Ready',p_web.GetValue('Value'))
    job:Estimate_Ready = p_web.GetValue('Value')
  End
  do Value::job:Estimate_Ready
  do SendAlert
  do Prompt::job:Estimate_Ready

Value::job:Estimate_Ready  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Ready') & '_value',Choose(p_web.GSV('job:Estimate') <> 'YES' OR p_web.GSV('job:Estimate_Ready') <> 'YES' or (p_web.GSV('job:Estimate_Ready') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES')),'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Estimate') <> 'YES' OR p_web.GSV('job:Estimate_Ready') <> 'YES' or (p_web.GSV('job:Estimate_Ready') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES')))
  ! --- CHECKBOX --- job:Estimate_Ready
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''job:Estimate_Ready'',''jobestimate_job:estimate_ready_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Estimate_Ready')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('job:Estimate_Ready') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','job:Estimate_Ready',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('job:Estimate_Ready') & '_value')

Comment::job:Estimate_Ready  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:EstimateReady'))
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Ready') & '_comment',Choose(p_web.GSV('job:Estimate') <> 'YES' OR p_web.GSV('job:Estimate_Ready') <> 'YES' or (p_web.GSV('job:Estimate_Ready') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES')),'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Estimate') <> 'YES' OR p_web.GSV('job:Estimate_Ready') <> 'YES' or (p_web.GSV('job:Estimate_Ready') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES'))
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Estimate_Accepted  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Accepted') & '_prompt',Choose(p_web.GSV('job:Estimate') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Estimate Accepted')
  If p_web.GSV('job:Estimate') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('job:Estimate_Accepted') & '_prompt')

Validate::job:Estimate_Accepted  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Estimate_Accepted',p_web.GetValue('NewValue'))
    job:Estimate_Accepted = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Estimate_Accepted
    do Value::job:Estimate_Accepted
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    p_web.SetSessionValue('job:Estimate_Accepted',p_web.GetValue('Value'))
    job:Estimate_Accepted = p_web.GetValue('Value')
  End
  if (p_web.GSV('job:Estimate_Accepted') = 'YES')
      if (p_web.GSV('job:Estimate_Ready') <> 'YES')
          p_web.SSV('Comment:EstimateAccepted','Estimate has not been marked as ready')
          p_web.SSV('job:Estimate_Accepted','NO')
      else ! if (p_web.GSV('job:Estimate_Ready') <> 'YES')
          p_web.SSV('Comment:EstimateAccepted','')
          p_web.SSV('Hide:EstimateAccepted',0)
          p_web.SSV('job:Estimate_Rejected','NO')
      end !if (p_web.GSV('job:Estimate_Ready') <> 'YES')
  else ! if (p_web.GSV('job:Estimate_Accepted') = 'YES')
      ! Check for char parts
      found# = 0
      Access:PARTS.Clearkey(par:part_Number_Key)
      par:ref_Number    = p_web.GSV('job:Ref_Number')
      set(par:part_Number_Key,par:part_Number_Key)
      loop
          if (Access:PARTS.Next())
              Break
          end ! if (Access:PARTS.Next())
          if (par:ref_Number    <> p_web.GSV('job:Ref_Number'))
              Break
          end ! if (par:ref_Number    <> p_web.GSV('job:Ref_Number'))
          found# = 1
          break
      end ! loop
  
      if (found# = 1)
          p_web.SSV('Comment:EstimateAccepted','Error! There are Chargeable Parts on this job.')
          p_web.SSV('job:Estimate_Accepted','YES')
      else ! if (found# = 1)
          p_web.SSV('Comment:EstimateAccepted','')
          p_web.SSV('Hide:EstimateAccepted',1)
      end ! if (found# = 1)
      
  end ! if (p_web.GSV('job:Estimate_Accepted') = 'YES')
  do Value::job:Estimate_Accepted
  do SendAlert
  do Prompt::locEstAccBy
  do Value::locEstAccBy  !1
  do Prompt::locEstAccCommunicationMethod
  do Value::locEstAccCommunicationMethod  !1
  do Value::textEstimateAccepted  !1
  do Comment::job:Estimate_Accepted
  do Value::job:Estimate_Rejected  !1
  do Comment::job:Estimate_Rejected
  do Prompt::job:Estimate_Ready
  do Value::job:Estimate_Ready  !1

Value::job:Estimate_Accepted  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Accepted') & '_value',Choose(p_web.GSV('job:Estimate') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Estimate') <> 'YES')
  ! --- CHECKBOX --- job:Estimate_Accepted
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''job:Estimate_Accepted'',''jobestimate_job:estimate_accepted_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Estimate_Accepted')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('job:Estimate_Accepted') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','job:Estimate_Accepted',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('job:Estimate_Accepted') & '_value')

Comment::job:Estimate_Accepted  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:EstimateAccepted'))
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Accepted') & '_comment',Choose(p_web.GSV('job:Estimate') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Estimate') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('job:Estimate_Accepted') & '_comment')

Prompt::job:Estimate_Rejected  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Rejected') & '_prompt',Choose(p_web.GSV('job:Estimate') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Estimate Rejected')
  If p_web.GSV('job:Estimate') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('job:Estimate_Rejected') & '_prompt')

Validate::job:Estimate_Rejected  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Estimate_Rejected',p_web.GetValue('NewValue'))
    job:Estimate_Rejected = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Estimate_Rejected
    do Value::job:Estimate_Rejected
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    p_web.SetSessionValue('job:Estimate_Rejected',p_web.GetValue('Value'))
    job:Estimate_Rejected = p_web.GetValue('Value')
  End
  if (p_web.GSV('job:Estimate_Rejected') = 'YES')
      if (p_web.GSV('job:Estimate_Accepted') = 'YES')
          p_web.SSV('Comment:EstimateRejected','You must untick Estimate Accepted')
      else ! if (p_web.GSV('job:Estimate_Accepted') = 'YES')
          p_web.SSV('Hide:EstimateRejected',0)
          p_web.SSV('Comment:EstimateRejected','')
      end ! if (p_web.GSV('job:Estimate_Accepted') = 'YES')
  else ! if (p_web.GSV('job:Estimate_Rejected') = 'YES')
      p_web.SSV('Hide:EstimateRejected',1)
      p_web.SSV('Comment:EstimateRejected','')
  end ! if (p_web.GSV('job:Estimate_Rejected') = 'YES')
  do Value::job:Estimate_Rejected
  do SendAlert
  do Prompt::locEstRejBy
  do Value::locEstRejBy  !1
  do Prompt::locEstRejCommunicationMethod
  do Value::locEstRejCommunicationMethod  !1
  do Prompt::locEstRejReason
  do Value::locEstRejReason  !1
  do Comment::job:Estimate_Rejected

Value::job:Estimate_Rejected  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Rejected') & '_value',Choose(p_web.GSV('job:Estimate') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('job:Estimate') <> 'YES')
  ! --- CHECKBOX --- job:Estimate_Rejected
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''job:Estimate_Rejected'',''jobestimate_job:estimate_rejected_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Estimate_Rejected')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('job:Estimate_Rejected') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','job:Estimate_Rejected',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('job:Estimate_Rejected') & '_value')

Comment::job:Estimate_Rejected  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:EstimateRejected'))
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('job:Estimate_Rejected') & '_comment',Choose(p_web.GSV('job:Estimate') <> 'YES','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('job:Estimate') <> 'YES'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('job:Estimate_Rejected') & '_comment')

Validate::textEstimateAccepted  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textEstimateAccepted',p_web.GetValue('NewValue'))
    do Value::textEstimateAccepted
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textEstimateAccepted  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('textEstimateAccepted') & '_value',Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:EstimateAccepted') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Estimate Accepted',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('textEstimateAccepted') & '_value')

Comment::textEstimateAccepted  Routine
    loc:comment = ''
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('textEstimateAccepted') & '_comment',Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:EstimateAccepted') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locEstAccBy  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstAccBy') & '_prompt',Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Estimate Accepted By')
  If p_web.GSV('Hide:EstimateAccepted') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('locEstAccBy') & '_prompt')

Validate::locEstAccBy  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEstAccBy',p_web.GetValue('NewValue'))
    locEstAccBy = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEstAccBy
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEstAccBy',p_web.GetValue('Value'))
    locEstAccBy = p_web.GetValue('Value')
  End
    locEstAccBy = Upper(locEstAccBy)
    p_web.SetSessionValue('locEstAccBy',locEstAccBy)
  do Value::locEstAccBy
  do SendAlert

Value::locEstAccBy  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstAccBy') & '_value',Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:EstimateAccepted') = 1)
  ! --- STRING --- locEstAccBy
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locEstAccBy')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEstAccBy'',''jobestimate_locestaccby_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locEstAccBy',p_web.GetSessionValueFormat('locEstAccBy'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('locEstAccBy') & '_value')

Comment::locEstAccBy  Routine
      loc:comment = ''
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstAccBy') & '_comment',Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:EstimateAccepted') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locEstAccCommunicationMethod  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstAccCommunicationMethod') & '_prompt',Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Communication Method')
  If p_web.GSV('Hide:EstimateAccepted') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('locEstAccCommunicationMethod') & '_prompt')

Validate::locEstAccCommunicationMethod  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEstAccCommunicationMethod',p_web.GetValue('NewValue'))
    locEstAccCommunicationMethod = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEstAccCommunicationMethod
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEstAccCommunicationMethod',p_web.GetValue('Value'))
    locEstAccCommunicationMethod = p_web.GetValue('Value')
  End
    locEstAccCommunicationMethod = Upper(locEstAccCommunicationMethod)
    p_web.SetSessionValue('locEstAccCommunicationMethod',locEstAccCommunicationMethod)
  do Value::locEstAccCommunicationMethod
  do SendAlert

Value::locEstAccCommunicationMethod  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstAccCommunicationMethod') & '_value',Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:EstimateAccepted') = 1)
  ! --- STRING --- locEstAccCommunicationMethod
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locEstAccCommunicationMethod')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEstAccCommunicationMethod'',''jobestimate_locestacccommunicationmethod_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locEstAccCommunicationMethod',p_web.GetSessionValueFormat('locEstAccCommunicationMethod'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('locEstAccCommunicationMethod') & '_value')

Comment::locEstAccCommunicationMethod  Routine
      loc:comment = ''
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstAccCommunicationMethod') & '_comment',Choose(p_web.GSV('Hide:EstimateAccepted') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:EstimateAccepted') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::textEstimateRejected  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textEstimateRejected',p_web.GetValue('NewValue'))
    do Value::textEstimateRejected
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textEstimateRejected  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('textEstimateRejected') & '_value',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:EstimateRejected') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web.Translate('Estimate Rejected',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::textEstimateRejected  Routine
    loc:comment = ''
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('textEstimateRejected') & '_comment',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:EstimateRejected') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locEstRejBy  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstRejBy') & '_prompt',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Estimate Rejected By')
  If p_web.GSV('Hide:EstimateRejected') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('locEstRejBy') & '_prompt')

Validate::locEstRejBy  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEstRejBy',p_web.GetValue('NewValue'))
    locEstRejBy = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEstRejBy
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEstRejBy',p_web.GetValue('Value'))
    locEstRejBy = p_web.GetValue('Value')
  End
    locEstRejBy = Upper(locEstRejBy)
    p_web.SetSessionValue('locEstRejBy',locEstRejBy)
  do Value::locEstRejBy
  do SendAlert

Value::locEstRejBy  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstRejBy') & '_value',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:EstimateRejected') = 1)
  ! --- STRING --- locEstRejBy
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locEstRejBy')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEstRejBy'',''jobestimate_locestrejby_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locEstRejBy',p_web.GetSessionValueFormat('locEstRejBy'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('locEstRejBy') & '_value')

Comment::locEstRejBy  Routine
      loc:comment = ''
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstRejBy') & '_comment',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:EstimateRejected') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locEstRejCommunicationMethod  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstRejCommunicationMethod') & '_prompt',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Communication Method')
  If p_web.GSV('Hide:EstimateRejected') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('locEstRejCommunicationMethod') & '_prompt')

Validate::locEstRejCommunicationMethod  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEstRejCommunicationMethod',p_web.GetValue('NewValue'))
    locEstRejCommunicationMethod = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEstRejCommunicationMethod
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEstRejCommunicationMethod',p_web.GetValue('Value'))
    locEstRejCommunicationMethod = p_web.GetValue('Value')
  End
    locEstRejCommunicationMethod = Upper(locEstRejCommunicationMethod)
    p_web.SetSessionValue('locEstRejCommunicationMethod',locEstRejCommunicationMethod)
  do Value::locEstRejCommunicationMethod
  do SendAlert

Value::locEstRejCommunicationMethod  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstRejCommunicationMethod') & '_value',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:EstimateRejected') = 1)
  ! --- STRING --- locEstRejCommunicationMethod
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locEstRejCommunicationMethod')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEstRejCommunicationMethod'',''jobestimate_locestrejcommunicationmethod_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locEstRejCommunicationMethod',p_web.GetSessionValueFormat('locEstRejCommunicationMethod'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('locEstRejCommunicationMethod') & '_value')

Comment::locEstRejCommunicationMethod  Routine
      loc:comment = ''
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstRejCommunicationMethod') & '_comment',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:EstimateRejected') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locEstRejReason  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstRejReason') & '_prompt',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Rejection Reason')
  If p_web.GSV('Hide:EstimateRejected') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('locEstRejReason') & '_prompt')

Validate::locEstRejReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEstRejReason',p_web.GetValue('NewValue'))
    locEstRejReason = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEstRejReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEstRejReason',p_web.GetValue('Value'))
    locEstRejReason = p_web.GetValue('Value')
  End
  do Value::locEstRejReason
  do SendAlert

Value::locEstRejReason  Routine
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstRejReason') & '_value',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:EstimateRejected') = 1)
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locEstRejReason')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEstRejReason'',''jobestimate_locestrejreason_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locEstRejReason',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locEstRejReason') = 0
    p_web.SetSessionValue('locEstRejReason','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('locEstRejReason')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(AUDSTATS)
  bind(aus:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  p_web._OpenFile(ESREJRES)
  bind(esr:Record)
  p_web._OpenFile(PARTS)
  bind(par:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locEstRejReason_OptionView)
  locEstRejReason_OptionView{prop:order} = 'UPPER(esr:RejectionReason)'
  Set(locEstRejReason_OptionView)
  Loop
    Next(locEstRejReason_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locEstRejReason') = 0
      p_web.SetSessionValue('locEstRejReason',esr:RejectionReason)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,esr:RejectionReason,choose(esr:RejectionReason = p_web.getsessionvalue('locEstRejReason')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locEstRejReason_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(AUDSTATS)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(ESREJRES)
  p_Web._CloseFile(PARTS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('JobEstimate_' & p_web._nocolon('locEstRejReason') & '_value')

Comment::locEstRejReason  Routine
    loc:comment = ''
  p_web._DivHeader('JobEstimate_' & p_web._nocolon('locEstRejReason') & '_comment',Choose(p_web.GSV('Hide:EstimateRejected') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:EstimateRejected') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('JobEstimate_job:Estimate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Estimate
      else
        do Value::job:Estimate
      end
  of lower('JobEstimate_job:Estimate_If_Over_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Estimate_If_Over
      else
        do Value::job:Estimate_If_Over
      end
  of lower('JobEstimate_job:Estimate_Ready_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Estimate_Ready
      else
        do Value::job:Estimate_Ready
      end
  of lower('JobEstimate_job:Estimate_Accepted_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Estimate_Accepted
      else
        do Value::job:Estimate_Accepted
      end
  of lower('JobEstimate_job:Estimate_Rejected_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Estimate_Rejected
      else
        do Value::job:Estimate_Rejected
      end
  of lower('JobEstimate_locEstAccBy_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEstAccBy
      else
        do Value::locEstAccBy
      end
  of lower('JobEstimate_locEstAccCommunicationMethod_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEstAccCommunicationMethod
      else
        do Value::locEstAccCommunicationMethod
      end
  of lower('JobEstimate_locEstRejBy_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEstRejBy
      else
        do Value::locEstRejBy
      end
  of lower('JobEstimate_locEstRejCommunicationMethod_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEstRejCommunicationMethod
      else
        do Value::locEstRejCommunicationMethod
      end
  of lower('JobEstimate_locEstRejReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEstRejReason
      else
        do Value::locEstRejReason
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('JobEstimate_form:ready_',1)
  p_web.SetSessionValue('JobEstimate_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_JobEstimate',0)

PreCopy  Routine
  p_web.SetValue('JobEstimate_form:ready_',1)
  p_web.SetSessionValue('JobEstimate_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_JobEstimate',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('JobEstimate_form:ready_',1)
  p_web.SetSessionValue('JobEstimate_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('JobEstimate:Primed',0)

PreDelete       Routine
  p_web.SetValue('JobEstimate_form:ready_',1)
  p_web.SetSessionValue('JobEstimate_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('JobEstimate:Primed',0)
  p_web.setsessionvalue('showtab_JobEstimate',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
          If p_web.IfExistsValue('job:Estimate') = 0
            p_web.SetValue('job:Estimate','NO')
            job:Estimate = 'NO'
          End
      If(p_web.GSV('job:Estimate') <> 'YES' OR p_web.GSV('job:Estimate_Ready') <> 'YES' or (p_web.GSV('job:Estimate_Ready') = 'YES' and (p_web.GSV('job:Estimate_Accepted') = 'YES' or p_web.GSV('job:Estimate_Rejected') = 'YES')))
          If p_web.IfExistsValue('job:Estimate_Ready') = 0
            p_web.SetValue('job:Estimate_Ready','NO')
            job:Estimate_Ready = 'NO'
          End
      End
      If(p_web.GSV('job:Estimate') <> 'YES')
          If p_web.IfExistsValue('job:Estimate_Accepted') = 0
            p_web.SetValue('job:Estimate_Accepted','NO')
            job:Estimate_Accepted = 'NO'
          End
      End
      If(p_web.GSV('job:Estimate') <> 'YES')
          If p_web.IfExistsValue('job:Estimate_Rejected') = 0
            p_web.SetValue('job:Estimate_Rejected','NO')
            job:Estimate_Rejected = 'NO'
          End
      End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('JobEstimate_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('JobEstimate_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
      If not (p_web.GSV('Hide:EstimateAccepted') = 1)
          locEstAccBy = Upper(locEstAccBy)
          p_web.SetSessionValue('locEstAccBy',locEstAccBy)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:EstimateAccepted') = 1)
          locEstAccCommunicationMethod = Upper(locEstAccCommunicationMethod)
          p_web.SetSessionValue('locEstAccCommunicationMethod',locEstAccCommunicationMethod)
        If loc:Invalid <> '' then exit.
      End
  ! tab = 3
    loc:InvalidTab += 1
      If not (p_web.GSV('Hide:EstimateRejected') = 1)
          locEstRejBy = Upper(locEstRejBy)
          p_web.SetSessionValue('locEstRejBy',locEstRejBy)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:EstimateRejected') = 1)
          locEstRejCommunicationMethod = Upper(locEstRejCommunicationMethod)
          p_web.SetSessionValue('locEstRejCommunicationMethod',locEstRejCommunicationMethod)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:EstimateRejected') = 1)
            
        If loc:Invalid <> '' then exit.
      End
  ! The following fields are not on the form, but need to be checked anyway.
    if (p_web.GSV('job:estimate_rejected') = 'YES')
        if (p_web.GSV('Hide:EstimateRejected') = 0)
            p_web.SSV('GetStatus:Type','JOB')
            p_web.SSV('GetStatus:StatusNumber',540)
            getStatus(540,0,'JOB',p_web)
  
  !              p_web.SSV('AddToAudit:Type','JOB')
  !              p_web.SSV('AddToAudit:Action','ESTIMATE REJECTED FROM: ' & p_web.GSV('locEstRejBy'))
  !              p_web.SSV('AddToAudit:Notes','COMMUNICATION METHOD: ' & p_web.GSV('locEstRejCommunicationMethod') & |
  !                           '<13,10>REASON: ' & p_web.GSV('locEstRejReason'))
            addToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','ESTIMATE REJECTED FROM: ' & p_web.GSV('locEstRejBy'),'COMMUNICATION METHOD: ' & p_web.GSV('locEstRejCommunicationMethod') & |
                '<13,10>REASON: ' & p_web.GSV('locEstRejReason'))
        end ! if (p_web.GSV('Hide:EstimateRejected') = 0)
    end ! if (p_web.GSV('job:estimate_rejected') = 'YES')
  
    IF (p_web.GSV('job:Estimate_Accepted') = 'YES')
        IF (p_web.GSV('Hide:EstimateAccepted') = 0)
            IF (ConvertEstimateParts(p_web, ErrorStr))
                loc:alert = 'Unable to Accept Estimate - ' & CLIP(ErrorStr)
                loc:invalid = 'locEstAccBy'
                AddToLog('Debug','Job: ' & p_web.GSV('job:Ref_Number') & ' - ' & CLIP(ErrorStr),'Accept Estimate Job',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
                EXIT
            ELSE ! IF
    
                GetStatus(535,0,'JOB',p_web)
            
                AddToAudit(p_web,|
                    p_web.GSV('job:Ref_Number'),|
                    'JOB','ESTIMATE ACCEPTED FROM: ' & p_web.GSV('locEstAccBy'), |
                    'COMMUNICATION METHOD: ' & p_web.GSV('locEstAccCommunicationMethod'))
                    
                        ! Copy estimate costs to job
                    
                p_web.SSV('job:Courier_Cost',p_web.GSV('job:Courier_Cost_Estimate'))
                p_web.SSV('job:Labour_Cost',p_web.GSV('job:Labour_Cost_Estimate'))
                p_web.SSV('job:Parts_Cost',p_web.GSV('job:Parts_Cost_Estimate'))
                p_web.SSV('job:Ignore_Chargeable_Charges',p_web.GSV('job:Ignore_Estimate_Charges'))
                p_web.SSV('jobe:RRCCLabourCost',p_web.GSV('jobe:RRCELabourCost'))
                p_web.SSV('jobe:RRCCPartsCost',p_web.GSV('jobe:RRCEPartsCost'))
                p_web.SSV('jobe:IgnoreRRCChaCosts',p_web.GSV('jobe:IgnoreRRCEstCosts'))                    
                
            END ! IF
        END ! IF (p_web.GSV('Hide:EstimateAccepted') = 0)
    END ! IF (p_web.GSV('job:Estimate_Accepted') = 'YES')
  
  
  
  ! Delete Variables
      p_web.deleteSessionValue('locEstAccBy')
      p_web.deleteSessionValue('locEstAccCommunicationMethod')
      p_web.deleteSessionValue('locEstRejBy')
      p_web.deleteSessionValue('locEstRejCommunicationMethod')
      p_web.deleteSessionValue('locEstRejReason')
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('JobEstimate:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('locEstAccBy')
  p_web.StoreValue('locEstAccCommunicationMethod')
  p_web.StoreValue('')
  p_web.StoreValue('locEstRejBy')
  p_web.StoreValue('locEstRejCommunicationMethod')
  p_web.StoreValue('locEstRejReason')
