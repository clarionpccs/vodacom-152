

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER329.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! Report the JOBS File
!!! </summary>
StockCheckReport PROCEDURE (NetwebServerWorker p_web)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
tmp:PrintedBy        STRING(60)                            !
bar_code_temp        CSTRING(31)                           !
used_temp            LONG                                  !
tmp:FirstModel       STRING(30)                            !
quantity_total_temp  LONG                                  !
count_temp           LONG                                  !
Address:User_Name    STRING(30)                            !
address:Address_Line1 STRING(30)                           !
address:Address_Line2 STRING(30)                           !
address:Address_Line3 STRING(30)                           !
address:Post_Code    STRING(20)                            !
Address:Telephone_Number STRING(20)                        !
address:FaxNumber    STRING(30)                            !
address:Email        STRING(255)                           !
locTradeAccount      STRING(30)                            !
locSiteLocation      STRING(30)                            !
locManufacturer      STRING(30)                            !
locIncAccessories    STRING(3)                             !
locSuppressZeros     STRING(3)                             !
locPriceBand         STRING(30)                            !
locPrintedBy         STRING(60)                            !
Process:View         VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                     END
ProgressWindow       WINDOW('Report JOBS'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('Stock Value Report'),AT(396,2792,7521,8510),PRE(RPT),PAPER(PAPER:A4),FONT('Arial', |
  10,,FONT:regular),THOUS
                       HEADER,AT(385,781,7521,2094),USE(?unnamed:2)
                         STRING('Inc. Accessories:'),AT(4948,469),USE(?IncludeAccessories),FONT(,8),TRN
                         STRING('<<-- Date Stamp -->'),AT(5865,1125,927,135),USE(?ReportDateStamp:2),FONT('Arial',8, |
  ,FONT:bold),TRN
                         STRING(@s3),AT(5885,469),USE(locIncAccessories),FONT(,8,,FONT:bold),LEFT,TRN
                         STRING(@s3),AT(5885,625),USE(locSuppressZeros),FONT(,8,,FONT:bold),LEFT,TRN
                         STRING('Price Band:'),AT(4948,792),USE(?PriceBand),FONT(,8,COLOR:Black,,CHARSET:ANSI),TRN
                         STRING(@pPage <<#p),AT(5875,1333,700,135),USE(?PageCount:2),FONT('Arial',8,,FONT:bold),PAGENO
                         STRING(@s30),AT(5885,792),USE(locPriceBand),FONT(,8,COLOR:Black,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Suppress Zeros:'),AT(4948,625),USE(?SuppressZeros),FONT(,8),TRN
                         STRING(@s30),AT(5885,156),USE(locSiteLocation),FONT(,8,,FONT:bold),LEFT,TRN
                         STRING(@s30),AT(5885,313),USE(locManufacturer),FONT(,8,,FONT:bold),LEFT,TRN
                         STRING('Manufacturer:'),AT(4948,313),USE(?Manufacturer),FONT(,8),TRN
                         STRING('Site Location:'),AT(4948,156),USE(?String22:2),FONT(,8),TRN
                         STRING('Printed By:'),AT(4948,948),USE(?String27),FONT(,8),TRN
                         STRING(@s60),AT(5885,948),USE(locPrintedBy),FONT(,8,,FONT:bold),TRN
                         STRING('Report Date:'),AT(4948,1104),USE(?ReportDatePrompt),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                       END
DETAIL                 DETAIL,AT(0,0,,177),USE(?DetailBand)
                         STRING(@s15),AT(2344,0,1354,208),USE(bar_code_temp),FONT('C39 High 12pt LJ3',10,,,CHARSET:ANSI), |
  LEFT
                         STRING(@s12),AT(156,0),USE(sto:Shelf_Location),FONT(,7),TRN
                         STRING(@s12),AT(833,0),USE(sto:Second_Location),FONT(,7),TRN
                         STRING(@s15),AT(1510,0),USE(sto:Part_Number),FONT(,7),TRN
                         STRING(@s20),AT(4531,0),USE(sto:Description),FONT(,7),TRN
                         STRING(@s6),AT(6563,0),USE(sto:Quantity_Stock),FONT(,7),RIGHT,TRN
                         BOX,AT(6979,0,156,150),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING(@n10.2),AT(5625,0,521,104),USE(sto:Purchase_Cost),FONT('Arial',7,,FONT:regular,CHARSET:ANSI), |
  RIGHT,TRN
                         STRING(@s5),AT(6198,0),USE(used_temp),FONT(,7),RIGHT,TRN
                         STRING(@s14),AT(3750,0),USE(tmp:FirstModel),FONT('Arial',7,,FONT:regular,CHARSET:ANSI),TRN
                         LINE,AT(7187,104,208,0),USE(?Line2),COLOR(COLOR:Black)
                       END
Totals                 DETAIL,AT(0,0,,458),USE(?totals)
                         LINE,AT(208,52,7083,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Total Number Of Lines:'),AT(208,104),USE(?String26),FONT(,8,,FONT:bold),TRN
                         STRING(@s8),AT(6365,104),USE(quantity_total_temp),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING('Total Items:'),AT(5573,104),USE(?totalitems),FONT(,8,,FONT:bold),TRN
                         STRING(@s9),AT(2083,104),USE(count_temp),FONT(,8,,FONT:bold),TRN
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(10,31,7521,11156),USE(?Image1)
                         STRING(@s30),AT(156,0,3844,240),USE(Address:User_Name),FONT(,16,,FONT:bold),LEFT,TRN
                         STRING('STOCK CHECK REPORT'),AT(4271,0,3177,260),USE(?String20),FONT(,16,,FONT:bold),RIGHT, |
  TRN
                         STRING(@s30),AT(156,260,3844,156),USE(address:Address_Line1),FONT(,9),TRN
                         STRING(@s30),AT(156,417,3844,156),USE(address:Address_Line2),FONT(,9),TRN
                         STRING(@s30),AT(156,573,3844,156),USE(address:Address_Line3),FONT(,9),TRN
                         STRING(@s20),AT(156,729,1156,156),USE(address:Post_Code),FONT(,9),TRN
                         STRING('Tel:'),AT(156,1042),USE(?String16),FONT(,9),TRN
                         STRING(@s20),AT(521,1042),USE(Address:Telephone_Number),FONT(,9),TRN
                         STRING('Fax: '),AT(156,1198),USE(?String19),FONT(,9),TRN
                         STRING(@s30),AT(521,1198),USE(address:FaxNumber),FONT(,9),TRN
                         STRING(@s255),AT(521,1354,3844,198),USE(address:Email),FONT(,9),TRN
                         STRING('Email:'),AT(156,1354),USE(?String19:2),FONT(,9),TRN
                         STRING('2nd Loc'),AT(833,2083),USE(?String44),FONT(,7,,FONT:bold),TRN
                         STRING('Part Number'),AT(1510,2083),USE(?String44:3),FONT(,7,,FONT:bold),TRN
                         STRING('Description'),AT(4531,2083),USE(?String44:4),FONT(,7,,FONT:bold),TRN
                         STRING('Shelf Loc'),AT(156,2083),USE(?String44:2),FONT(,7,,FONT:bold),TRN
                         STRING('In Stock'),AT(6667,2083),USE(?Instock),FONT('Arial',7,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Count'),AT(7083,2083),USE(?String25:2),FONT('Arial',7,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Purch. Cost'),AT(5583,2083),USE(?String44:7),FONT(,7,,FONT:bold),TRN
                         STRING('Usage'),AT(6302,2083),USE(?Instock:2),FONT('Arial',7,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Part Number'),AT(2344,2083),USE(?String44:6),FONT(,7,,FONT:bold),TRN
                         STRING('1st Model'),AT(3750,2083),USE(?String44:5),FONT(,7,,FONT:bold),TRN
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR3               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepStringClass                       ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('StockCheckReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:PRIBAND.Open                                      ! File PRIBAND used by this procedure, so make sure it's RelationManager is open
  Relate:STOCK.SetOpenRelated()
  Relate:STOCK.Open                                        ! File STOCK used by this procedure, so make sure it's RelationManager is open
  Relate:TagFile.Open                                      ! File TagFile used by this procedure, so make sure it's RelationManager is open
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:STOHIST.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:STOMODEL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
        locTradeAccount = p_web.GSV('BookingAccount')  
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('StockCheckReport',ProgressWindow)          ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowAlpha+ScrollSort:AllowNumeric,ScrollBy:RunTime)
  ThisReport.Init(Process:View, Relate:TRADEACC, ?Progress:PctText, Progress:Thermometer, ProgressMgr, tra:Account_Number)
  ThisReport.CaseSensitiveValue = FALSE
  ThisReport.AddSortOrder(tra:Account_Number_Key)
  ThisReport.AddRange(tra:Account_Number,locTradeAccount)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:TRADEACC.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PRIBAND.Close
    Relate:STOCK.Close
    Relate:TagFile.Close
  END
  IF SELF.Opened
    INIMgr.Update('StockCheckReport',ProgressWindow)       ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'StockCheckReport',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
        SETTARGET(REPORT)
        IF (p_web.GSV('locShowStockQuantityOnReport') <> 1)
            ?Instock{PROP:Hide} = 1
            ?sto:Quantity_Stock{PROP:Hide} = 1
            ?totalitems{PROP:Hide} = 1
            ?quantity_total_temp{PROP:Hide} = 1
        END ! IF
        
        IF (p_web.GSV('locShowUsageDate') <> 1)
            ?used_temp{PROP:Hide} = 1
            ?Instock:2{PROP:Hide} = 1
        END ! IF
        SETTARGET()
        locSiteLocation = p_web.GSV('BookingSiteLocation')
  
        IF (p_web.GSV('locSuppressZerosOnReport') = 1)
            locSuppressZeros = 'YES'
        ELSE
            locSuppressZeros = 'NO'
        END
  
        IF (p_web.GSV('locAllManufacturers') <> 1)
            locManufacturer = p_web.GSV('locManufacturer')
        ELSE
            locManufacturer = 'Multiple'
        END ! IF
  
        IF (p_web.GSV('locAllPriceBands') <> 1)
            locPriceBand = p_web.GSV('prb:BandName')
        ELSE
            locPriceBand = 'Multiple'
        END ! IF
  
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = p_web.GSV('BookingUserCode')
        IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            locPrintedBy = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
        ELSE ! IF
        END ! IF
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.Init(Report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,Report{PROPPRINT:Paper},PAPER:USER), CHOOSE(Report{PROP:Thous}=True,PROP:Thous,CHOOSE(Report{PROP:MM}=True,PROP:MM,CHOOSE(Report{PROP:Points}=True,PROP:Points,0))), Report{PROPPRINT:PaperWidth}, Report{PROPPRINT:PaperHeight}, Report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetTitle('Stock Check Report')       !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR3.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetViewerPrefs(PDFXTR3:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetFontEmbedding(True, True, False, False, False)
    PDFXTR3.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp:2{PROP:Text} = FORMAT(TODAY(),@d06)
  END
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'StockCheckReport',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR3.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
locUsed                     LONG()	
locFirstModel               STRING(30)	
i                           LONG()
  CODE
  !region Print Process        
        address:User_Name        = tra:Company_Name
        address:Address_Line1    = tra:Address_Line1
        address:Address_Line2    = tra:Address_Line2
        address:Address_Line3    = tra:Address_Line3
        address:Post_code        = tra:Postcode
        address:Telephone_Number = tra:Telephone_Number
        address:FaxNumber        = tra:Fax_Number
        address:Email            = tra:EmailAddress
  
  
        Access:PRIBAND.ClearKey(prb:keyBandName)
        prb:BandName = p_web.GSV('locPriceBand')
        IF (Access:PRIBAND.TryFetch(prb:keyBandName) = Level:Benign)
            p_web.FileToSessionQueue(PRIBAND)
        ELSE ! IF
        END ! IF
        
        p_web.SSV('pBuildForAudit',0)
        p_web.SSV('pUpdateProgress',0)
        BuildStockAuditQueue(p_web)
        IF (p_web.GSV('recordCount') = 0)
            RETURN Level:Fatal
        END ! IF
        
  !        IF (BuildStockAuditQueue(p_web,0,0) = 0)
  !            RETURN Level:Fatal
  !        END ! IF
  		
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = p_web.GSV('BookingUserCode')
        IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
  			
        ELSE ! IF
        END ! IF
        
        IF (p_web.GSV('locReportOrder') = 1)
            SORT(qStockCheck,qStockCheck.SessionID,qStockCheck.Description)
        ELSE ! IF
            SORT(qStockCheck,qStockCheck.SessionID)
        END ! IF        
        
        CLEAR(qStockCheck)
        LOOP i = 1 TO RECORDS(qStockCheck)
            GET(qStockCheck,i)
            
            p_web.Noop
            
            IF (qStockCheck.SessionID <> p_web.SessionID)
                CYCLE
            END ! IF
            
            Access:STOCK.ClearKey(sto:Ref_Number_Key)
            sto:Ref_Number = qStockCheck.RefNumber	
            IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
  				
            ELSE ! IF
                CYCLE
            END ! IF
  			
            locFirstModel = ''
            Access:STOMODEL.ClearKey(stm:Model_Number_Key)
            stm:Ref_Number = sto:Ref_Number
            stm:Manufacturer = sto:Manufacturer
            SET(stm:Model_Number_Key,stm:Model_Number_Key)
            LOOP UNTIL Access:STOMODEL.Next() <> Level:Benign
                IF (stm:Ref_Number <> sto:Ref_Number OR |
                    stm:Manufacturer <> sto:Manufacturer)
                    BREAK
                END ! IF
                locFirstModel = stm:Model_Number
                BREAK
            END ! LOOP
            bar_code_temp = '*' & CLIP(sto:Part_Number) & '*'
            
            ReturnValue = PARENT.TakeRecord()
            
            used_temp = qStockCheck.Usage
            
            PRINT(rpt:Detail)
            count_temp += 1
            quantity_total_temp += sto:Quantity_Stock
        END ! LOOP
        PRINT(rpt:Totals)
        RETURN ReturnValue
  ! Override Below Code To Allow To Print From Q     
  !endregion        
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR3:rtn = PDFXTR3.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR3:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

