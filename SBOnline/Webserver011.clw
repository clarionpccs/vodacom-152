

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER011.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! Report the INVOICE File
!!! </summary>
WarrantyIncomeReport PROCEDURE (NetWebServerWorker p_web)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
AddressGroup         GROUP,PRE(address)                    !
CompanyName          STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
Postcode             STRING(30)                            !
TelephoneNumber      STRING(30)                            !
FaxNumber            STRING(30)                            !
EmailAddress         STRING(255)                           !
                     END                                   !
tmp:DateRange        STRING(60)                            !
tmp:printedby        STRING(60)                            !
TotalGroup           GROUP,PRE(total)                      !
Lines                LONG                                  !
HandlingFee          REAL                                  !
Labour               REAL                                  !
Parts                REAL                                  !
PartsSelling         REAL                                  !
SubTotal             REAL                                  !
VAT                  REAL                                  !
Total                REAL                                  !
                     END                                   !
Process:View         VIEW(INVOICE)
                       PROJECT(inv:Date_Created)
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Report INVOICE'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

report               REPORT('Status Report'),AT(458,2010,10938,5688),PRE(RPT),PAPER(PAPER:A4),LANDSCAPE,FONT('Arial', |
  10),THOUS
                       HEADER,AT(427,323,10865,1375),USE(?unnamed:2)
                         STRING(@s30),AT(94,52),USE(address:CompanyName),FONT(,12,,FONT:bold),TRN
                         STRING(@s30),AT(104,260,3531,198),USE(address:AddressLine1),FONT(,8),TRN
                         STRING('Date Printed:'),AT(7448,729),USE(?RunPrompt),FONT(,8),TRN
                         STRING(@s30),AT(104,365,3531,198),USE(address:AddressLine2),FONT(,8),TRN
                         STRING(@s30),AT(104,469,3531,198),USE(address:AddressLine3),FONT(,8),TRN
                         STRING(@s30),AT(104,573),USE(address:Postcode),FONT(,8),TRN
                         STRING('Printed By:'),AT(7448,573,625,208),USE(?String67),FONT(,8),TRN
                         STRING(@s60),AT(8438,573),USE(tmp:printedby),FONT(,8,,FONT:bold),TRN
                         STRING(@s60),AT(8438,417),USE(tmp:DateRange),FONT(,8,,FONT:bold),TRN
                         STRING('Date Range:'),AT(7448,417,625,208),USE(?String67:2),FONT(,8),TRN
                         STRING('Tel: '),AT(104,729),USE(?String15),FONT(,9),TRN
                         STRING(@s30),AT(573,729),USE(address:TelephoneNumber),FONT(,8),TRN
                         STRING('Fax:'),AT(104,833),USE(?String16),FONT(,9),TRN
                         STRING(@s30),AT(573,833),USE(address:FaxNumber),FONT(,8),TRN
                         STRING(@s255),AT(573,938,3531,198),USE(address:EmailAddress),FONT(,8),TRN
                         STRING('Email:'),AT(104,938),USE(?String16:2),FONT(,9),TRN
                         STRING('Page:'),AT(7448,885),USE(?PagePrompt),FONT(,8),TRN
                         STRING('<<-- Date Stamp -->'),AT(8417,729),USE(?ReportDateStamp),FONT(,8,,FONT:bold),TRN
                         STRING(@N3),AT(8417,885),USE(ReportPageNumber),FONT(,8,,FONT:bold),TRN
                       END
DETAIL                 DETAIL,AT(10,10,10875,167),USE(?DetailBand)
                         STRING(@s25),AT(4573,0,1146,156),USE(qWIncomeReportRep.ChargeType),FONT('Arial',7,COLOR:Black, |
  FONT:regular,CHARSET:ANSI),LEFT,TRN
                         STRING(@n10.2),AT(8010,0),USE(qWIncomeReportRep.PartsCost),FONT(,7),RIGHT,TRN
                         STRING(@n10.2),AT(9688,0),USE(qWIncomeReportRep.VAT),FONT(,7),RIGHT,TRN
                         STRING(@n10.2),AT(10260,0),USE(qWIncomeReportRep.Total),FONT(,7),RIGHT,TRN
                         STRING(@s3),AT(7240,0),USE(qWIncomeReportRep.Exchanged),FONT('Arial',7,COLOR:Black,FONT:regular, |
  CHARSET:ANSI),TRN
                         STRING(@s20),AT(5792,0),USE(qWIncomeReportRep.WarrantyStatus),FONT('Arial',7,COLOR:Black,FONT:regular, |
  CHARSET:ANSI),LEFT,TRN
                         STRING(@s3),AT(6906,0),USE(qWIncomeReportRep.Repair),FONT('Arial',7,COLOR:Black,FONT:regular, |
  CHARSET:ANSI),TRN
                         STRING(@s20),AT(3479,0,1146,156),USE(qWIncomeReportRep.ModelNumber),FONT('Arial',7,,FONT:regular, |
  CHARSET:ANSI),LEFT,TRN
                         STRING(@s20),AT(2396,0,,156),USE(qWIncomeReportRep.Manufacturer),FONT('Arial',7,,FONT:regular, |
  CHARSET:ANSI),LEFT,TRN
                         STRING(@s15),AT(146,0),USE(qWIncomeReportRep.RefNumber),FONT('Arial',7,,FONT:regular,CHARSET:ANSI), |
  LEFT,TRN
                         STRING(@s15),AT(1563,0),USE(qWIncomeReportRep.JobAccountNumber),FONT('Arial',7,,FONT:regular, |
  CHARSET:ANSI),LEFT,TRN
                         STRING(@n10.2),AT(7438,0),USE(qWIncomeReportRep.HandlingFee),FONT(,7),RIGHT,TRN
                         STRING(@n10.2),AT(9104,0),USE(qWIncomeReportRep.Labour),FONT(,7),RIGHT,TRN
                         STRING(@n10.2),AT(8563,0),USE(qWIncomeReportRep.PartsSelling),FONT(,7),RIGHT,TRN
                         STRING(@d6),AT(990,0),USE(qWIncomeReportRep.CompletedDate),FONT('Arial',7,,FONT:regular,CHARSET:ANSI), |
  RIGHT,TRN
                       END
DetailTotals           DETAIL,AT(0,0,10937,417),USE(?DetailTotals)
                         LINE,AT(198,52,10583,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Total Lines:'),AT(260,156),USE(?String68),FONT(,7,,FONT:bold),TRN
                         STRING(@s8),AT(885,156),USE(total:Lines),FONT(,7,,FONT:bold)
                         STRING(@n12.2),AT(7365,156),USE(total:HandlingFee),FONT(,7,,FONT:bold),RIGHT,TRN
                         STRING(@n14.2),AT(8510,156),USE(total:PartsSelling,,?total:PartsSelling:2),FONT(,7,,FONT:bold), |
  RIGHT,TRN
                         STRING(@n14.2),AT(9031,156),USE(total:Labour,,?total:Labour:2),FONT(,7,,FONT:bold),RIGHT,TRN
                         STRING(@n12.2),AT(9604,156),USE(total:VAT),FONT(,7,,FONT:bold),RIGHT,TRN
                         STRING(@n12.2),AT(10177,156),USE(total:Total),FONT(,7,,FONT:bold),RIGHT,TRN
                         STRING(@n14.2),AT(7938,156),USE(total:Parts),FONT(,7,,FONT:bold),RIGHT,TRN
                       END
                       FORM,AT(406,313,10969,7448),USE(?unnamed)
                         IMAGE('Rlistlan.gif'),AT(52,0,10938,7448),USE(?Image1)
                         STRING('WARRANTY INCOME REPORT'),AT(4156,52),USE(?ReportTitle),FONT(,12,,FONT:bold),TRN
                         STRING('Job No'),AT(208,1406),USE(?InvoiceNumberText),FONT(,7),TRN
                         STRING('Account No'),AT(1625,1406),USE(?String26:4),FONT(,7,,FONT:bold),TRN
                         STRING('Handling/'),AT(7760,1406),USE(?String26:7),FONT(,7,,FONT:bold),TRN
                         STRING('Labour'),AT(9479,1458),USE(?String26:8),FONT(,7,,FONT:bold),TRN
                         STRING('Parts'),AT(8750,1406),USE(?String26:9),FONT(,7,,FONT:bold),TRN
                         STRING('Repair'),AT(6917,1406),USE(?String58),FONT('Arial',7,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Exchange'),AT(7260,1406),USE(?String61),FONT('Arial',7,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Cost'),AT(8490,1510),USE(?String26:2),FONT(,7,,FONT:bold),TRN
                         STRING('Selling'),AT(8906,1510),USE(?String26:3),FONT(,7,,FONT:bold),TRN
                         STRING('Swap Fee'),AT(7760,1510),USE(?String26:14),FONT(,7,,FONT:bold),TRN
                         STRING('Completed'),AT(1104,1406),USE(?InvoiceDateText),FONT(,7),TRN
                         STRING('Model Number'),AT(3542,1406),USE(?String26:12),FONT(,7,,FONT:bold),TRN
                         STRING('Manufacturer'),AT(2458,1406),USE(?String26:5),FONT(,7,,FONT:bold),TRN
                         STRING('Total'),AT(10625,1458,260,156),USE(?String26:11),FONT(,7,,FONT:bold),TRN
                         STRING('Charge Type'),AT(4635,1406),USE(?String26:13),FONT(,7,,FONT:bold),TRN
                         STRING('Warranty Status'),AT(5854,1406),USE(?String57),FONT('Arial',7,COLOR:Black,FONT:bold, |
  CHARSET:ANSI),TRN
                         STRING('V.A.T.'),AT(10104,1458),USE(?String26:10),FONT(,7,,FONT:bold),TRN
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR3               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
EndReport              PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Next                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.EndReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
    PRINT(rpt:DetailTotals)
  ReturnValue = PARENT.EndReport()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('WarrantyIncomeReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:INVOICE.Open                                      ! File INVOICE used by this procedure, so make sure it's RelationManager is open
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('WarrantyIncomeReport',ProgressWindow)      ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:INVOICE, ?Progress:PctText, Progress:Thermometer, RECORDS(qWIncomeReportRep))
  ThisReport.AddSortOrder()
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
  END
  IF SELF.Opened
    INIMgr.Update('WarrantyIncomeReport',ProgressWindow)   ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'WarrantyIncomeReport',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Next PROCEDURE

ReturnValue          BYTE,AUTO

Progress BYTE,AUTO
  CODE
      ThisReport.RecordsProcessed+=1
      GET(qWIncomeReportRep,ThisReport.RecordsProcessed)
      IF ERRORCODE() THEN
         ReturnValue = Level:Notify
      ELSE
         ReturnValue = Level:Benign
      END
      IF ReturnValue = Level:Notify
          IF ThisReport.RecordsProcessed>RECORDS(qWIncomeReportRep)
             SELF.Response = RequestCompleted
             POST(EVENT:CloseWindow)
             RETURN Level:Notify
          ELSE
             SELF.Response = RequestCancelled
             POST(EVENT:CloseWindow)
             RETURN Level:Fatal
          END
      ELSE
         Progress = ThisReport.RecordsProcessed / ThisReport.RecordsToProcess*100
         IF Progress > 100 THEN Progress = 100.
         IF Progress <> Progress:Thermometer
           Progress:Thermometer = Progress
           DISPLAY()
         END
      END
      RETURN Level:Benign
  ReturnValue = PARENT.Next()
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
        SET(DEFAULTS)
        Access:DEFAULTS.Next()
        
        tmp:DateRange = FORMAT(p_web.GSV('locStartDate'),@d06) & ' to ' & FORMAT(p_web.GSV('locEndDate'),@d06)
        
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('BookingAccount')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            address:CompanyName     = tra:Company_Name
            address:AddressLine1    = tra:Address_Line1
            address:AddressLine2    = tra:Address_Line2
            address:AddressLine3    = tra:Address_Line3
            address:Postcode        = tra:Postcode
            address:TelephoneNumber = tra:Telephone_Number
            address:FaxNumber       = tra:Fax_Number
            address:EmailAddress    = tra:EmailAddress
        END ! IF
        
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = p_web.GSV('BookingUserCode')
        IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            tmp:printedby = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
        END ! IF
        
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.Init(report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,report{PROPPRINT:Paper},PAPER:USER), CHOOSE(report{PROP:Thous}=True,PROP:Thous,CHOOSE(report{PROP:MM}=True,PROP:MM,CHOOSE(report{PROP:Points}=True,PROP:Points,0))), report{PROPPRINT:PaperWidth}, report{PROPPRINT:PaperHeight}, report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetTitle('Warranty Income Report')   !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR3.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetViewerPrefs(PDFXTR3:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@d06)
  END
  IF ReturnValue = Level:Benign
    report$?ReportPageNumber{PROP:PageNo} = True
  END
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'WarrantyIncomeReport',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR3.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
        IF (qWIncomeReportRep.SessionID <> p_web.SessionID)
            RETURN Level:User
        END ! IF
        
  PRINT(RPT:DETAIL)
        total:Lines             += 1
        total:HandlingFee       += qWIncomeReportRep.HandlingFee
        total:Labour            += qWIncomeReportRep.Labour
        total:Parts             += qWIncomeReportRep.PartsCost
        total:PartsSelling      += qWIncomeReportRep.PartsSelling
        total:SubTotal          += (qWIncomeReportRep.PartsSelling + qWIncomeReportRep.Labour)
        total:VAT               += qWIncomeReportRep.Vat
        total:Total             += qWIncomeReportRep.Total   
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR3:rtn = PDFXTR3.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR3:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

