

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER252.INC'),ONCE        !Local module procedure declarations
                     END


WaybillProcessedPage PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:WaybillProcessedPage -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
loc:divname       string(255)
loc:parent        string(255)
FilesOpened     Long
WAYBPRO_Alias::State  USHORT
JOBS::State  USHORT
JOBSE::State  USHORT
                    MAP
ProcessedCounters       PROCEDURE()
                    END ! MAP
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
  CODE
  GlobalErrors.SetProcedureName('WaybillProcessedPage')
  loc:parent = p_web.GetValue('_ParentProc')
  If loc:parent <> ''
    loc:divname = 'WaybillProcessedPage' & '_' & loc:parent
  Else
    loc:divname = 'WaybillProcessedPage'
  End
  p_web._DivHeader(loc:divname,'adiv')
!----------- put your html code here -----------------------------------
!        DO OpenFiles
!        ProcessedCounters()
!        DO CloseFiles
        
        DO thePage
!----------- end of custom code ----------------------------------------
  do SendPacket
  p_web._DivFooter()
  if loc:parent
    p_web._RegisterDivEx(loc:divname,timer,'''_parentProc='&clip(loc:parent)&'''')
  else
    p_web._RegisterDivEx(loc:divname,timer)
  End
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
OpenFiles  ROUTINE
  p_web._OpenFile(WAYBPRO_Alias)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(JOBSE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(WAYBPRO_Alias)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(JOBSE)
     FilesOpened = False
  END
thePage  Routine
  packet = clip(packet) & |
    '<<div style="width:92px;text-align:center"><13,10>'&|
    '    <<span class="SubHeading">Processed<</span><13,10>'&|
    '    <<br/><13,10>'&|
    '    <<table><13,10>'&|
    '        <<tr><13,10>'&|
    ' {12}<<td><<b>NR / 48E<</b><</td><13,10>'&|
    '        <</tr><13,10>'&|
    '        <<tr><<td><</td><</tr><13,10>'&|
    '        <<tr><13,10>'&|
    ' {12}<<td><<!-- Net:s:locWaybillNR --><</td><13,10>'&|
    '        <</tr>  <13,10>'&|
    '        <<tr><<td><</td><</tr><13,10>'&|
    '        <<tr><13,10>'&|
    ' {12}<<td><<b>OBF<</b><</td><13,10>'&|
    '        <</tr> {10}<13,10>'&|
    '        <<tr><13,10>'&|
    ' {12}<<td><<!-- Net:s:locWaybillOBF --><</td><13,10>'&|
    '        <</tr> <13,10>'&|
    '    <</table><13,10>'&|
    '<</div><13,10>'&|
    ''
ProcessedCounters   PROCEDURE()
    CODE
        p_web.SSV('locWaybillOBF',0)
        p_web.SSV('locWaybillNR',0)

        Access:WAYBPRO_ALIAS.clearkey(wyp_ali:AccountJobNumberKey)
        wyp_ali:AccountNumber = p_web.GSV('Default:AccountNumber')
        Set(wyp_ali:AccountJobNumberKey,wyp_ali:AccountJobNumberKey)
        loop
            If Access:WAYBPRO_ALIAS.next() then
                break
            End
            If wyp_ali:AccountNumber <> p_web.GSV('Default:AccountNumber') then
                break
            End
        !now check whic counter to add this jb to

            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = wyp_ali:JobNumber
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !added by Paul 20/04/2010 - log no 10546
            !check the job type
            !open the jobse record
                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber = job:Ref_Number
                If Access:Jobse.fetch(jobe:RefNumberKey) = level:benign then
                    If jobe:OBFvalidated = 1 then
                        p_web.SSV('locWaybillOBF',p_web.GSV('locWaybillOBF') + 1)

                    Else
                        p_web.SSV('locWaybillNR',p_web.GSV('locWaybillNR') + 1)
                    End
                End

            Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !Error
            End !If

        End
