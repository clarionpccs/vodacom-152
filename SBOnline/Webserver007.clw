

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER007.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! Report the JOBS File
!!! </summary>
Waybill PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
locWaybillNumber     LONG                                  !
FromAddressGroup     GROUP,PRE(from)                       !
CompanyName          STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(39)                            !
TelephoneNumber      STRING(30)                            !
ContactName          STRING(60)                            !
EmailAddress         STRING(255)                           !
                     END                                   !
ToAddressGroup       GROUP,PRE(to)                         !
AccountNumber        STRING(30)                            !
CompanyName          STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
TelephoneNumber      STRING(30)                            !
ContactName          STRING(30)                            !
EmailAddress         STRING(255)                           !
                     END                                   !
locDateDespatched    DATE                                  !
locTimeDespatched    TIME                                  !
locCourier           STRING(30)                            !
locConsignmentNumber STRING(30)                            !
locBarCode           STRING(60)                            !
locJobNumber         STRING(30)                            !
locIMEINumber        STRING(30)                            !
locOrderNumber       STRING(30)                            !
locSecurityPackNumber STRING(30)                           !
locAccessories       STRING(255)                           !
locExchanged         STRING(1)                             !
Process:View         VIEW(WAYBILLJ)
                       PROJECT(waj:JobNumber)
                       PROJECT(waj:WayBillNumber)
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Report JOBS'),AT(,,142,59),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:DEFAULT), |
  DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT('JOBS Report'),AT(250,3875,7750,6594),PRE(RPT),PAPER(PAPER:A4),FONT('Tahoma',8,COLOR:Black, |
  FONT:regular,CHARSET:DEFAULT),THOUS
                       HEADER,AT(250,250,7750,3250),USE(?Header),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:DEFAULT)
                         STRING('Courier:'),AT(4896,781),USE(?String23),FONT(,8),TRN
                         STRING('Despatch Date:'),AT(4896,365),USE(?DespatchDate),FONT(,8),TRN
                         STRING(@d6),AT(6042,365),USE(locDateDespatched),FONT(,8,,FONT:bold),TRN
                         STRING(@t1b),AT(6042,573),USE(locTimeDespatched),FONT(,8,,FONT:bold),TRN
                         STRING('From Sender:'),AT(83,83),USE(?String16),FONT(,8),TRN
                         STRING('WAYBILL REJECTION'),AT(3750,31,3781),USE(?WaybillRejection),FONT(,16,,FONT:bold),RIGHT, |
  HIDE,TRN
                         STRING('Deliver To:'),AT(104,1667),USE(?String16:5),FONT(,8),TRN
                         STRING('Tel No:'),AT(104,1042),USE(?String16:2),FONT(,8),TRN
                         STRING('Page Number:'),AT(4896,990),USE(?String62),TRN
                         STRING(@s30),AT(938,1042),USE(from:TelephoneNumber),FONT(,8,,FONT:bold),TRN
                         STRING('Contact Name:'),AT(104,2813),USE(?String16:7),FONT(,8),TRN
                         STRING(@s60),AT(938,1198),USE(from:ContactName),FONT(,8,,FONT:bold),TRN
                         STRING(@s255),AT(938,1354),USE(from:EmailAddress),FONT(,8,,FONT:bold),TRN
                         STRING('Email:'),AT(104,1354),USE(?String16:4),FONT(,8),TRN
                         STRING('WAYBILL NUMBER'),AT(3490,1667,3906,260),USE(?String39),FONT(,12,,FONT:bold),CENTER, |
  TRN
                         STRING('Contact Name:'),AT(104,1198),USE(?String16:3),FONT(,8),TRN
                         STRING(@s30),AT(83,250,4687,365),USE(from:CompanyName),FONT(,12,,FONT:bold),TRN
                         STRING(@s30),AT(3490,1979,3906,260),USE(locBarCode),FONT('C39 High 12pt LJ3',12,COLOR:Black, |
  ,CHARSET:ANSI),CENTER,COLOR(COLOR:White)
                         STRING(@s30),AT(83,542),USE(from:AddressLine1),FONT(,8,,FONT:bold),TRN
                         STRING(@s30),AT(83,708),USE(from:AddressLine2),FONT(,8,,FONT:bold),TRN
                         STRING('Account Number:'),AT(104,1875),USE(?AccountNumber),FONT(,8),TRN
                         STRING(@s30),AT(1042,1875,1927,188),USE(to:AccountNumber),FONT(,8,,FONT:bold),TRN
                         STRING('Company Name:'),AT(104,2031),USE(?CompanyName),FONT(,8),TRN
                         STRING(@s30),AT(1042,2031),USE(to:CompanyName),FONT(,8,,FONT:bold),TRN
                         STRING('Address 1:'),AT(104,2188),USE(?Address1),FONT(,8),TRN
                         STRING(@s30),AT(1042,2188),USE(to:AddressLine1),FONT(,8,,FONT:bold),TRN
                         BOX,AT(52,2031,3177,156),USE(?Box1:2),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Address 2:'),AT(104,2344),USE(?Address2),FONT(,8),TRN
                         STRING(@s30),AT(1042,2344),USE(to:AddressLine2),FONT(,8,,FONT:bold),TRN
                         STRING('Suburb:'),AT(104,2500),USE(?Suburb),FONT(,8),TRN
                         STRING(@s30),AT(1042,2500),USE(to:AddressLine3),FONT(,8,,FONT:bold),TRN
                         STRING('Contact Name:'),AT(104,2813),USE(?ContactName),FONT(,8),TRN
                         STRING(@s30),AT(1042,2813),USE(to:ContactName),FONT(,8,,FONT:bold),TRN
                         LINE,AT(990,1875,0,1250),USE(?Line5),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2969,3177,156),USE(?Box1:8),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2656,3177,156),USE(?Box1:6),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Contact Number:'),AT(104,2656),USE(?ContactNumber),FONT(,8),TRN
                         STRING(@s30),AT(1042,2656),USE(to:TelephoneNumber),FONT(,8,,FONT:bold),TRN
                         BOX,AT(52,2813,3177,156),USE(?Box1:7),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2188,3177,156),USE(?Box1:3),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING('Email:'),AT(104,2969),USE(?Email),FONT(,8),TRN
                         STRING(@s255),AT(1042,2969),USE(to:EmailAddress),FONT(,8,,FONT:bold),TRN
                         STRING('Despatch Time:'),AT(4896,573),USE(?DespatchTime),FONT(,8),TRN
                         STRING(@s30),AT(83,875),USE(from:AddressLine3),FONT(,8,,FONT:bold),TRN
                         STRING(@s30),AT(3490,2292,3906,260),USE(locConsignmentNumber),FONT('Arial',12,,FONT:bold,CHARSET:ANSI), |
  CENTER,TRN
                         BOX,AT(52,2500,3177,156),USE(?Box1:5),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,2344,3177,156),USE(?Box1:4),COLOR(COLOR:Black),LINEWIDTH(1)
                         BOX,AT(52,1875,3177,156),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(1)
                         STRING(@s30),AT(6042,781),USE(locCourier),FONT(,,,FONT:bold)
                         STRING(@N3),AT(6042,990),USE(ReportPageNumber),FONT(,,,FONT:bold)
                       END
Detail                 DETAIL,AT(0,0,7750,229),USE(?Detail)
                         STRING(@s30),AT(104,0,1094),USE(locJobNumber)
                         STRING(@s30),AT(1260,0,1302),USE(locIMEINumber)
                         STRING(@s30),AT(2625,-10),USE(locOrderNumber),FONT(,7)
                         STRING(@s30),AT(4354,-10,958),USE(locSecurityPackNumber),FONT(,7)
                         TEXT,AT(5375,-10,2021,146),USE(locAccessories),RESIZE
                         STRING(@s1),AT(7458,-10),USE(locExchanged)
                       END
                       FOOTER,AT(250,10531,7750,1448),USE(?Footer)
                         TEXT,AT(125,42,7500,1333),USE(stt:Text),TRN
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:DEFAULT)
                         STRING('Job No'),AT(156,3333,,156),USE(?JobNo),FONT(,7),TRN
                         STRING('I.M.E.I. No'),AT(1313,3333),USE(?IMEINo),FONT(,7),TRN
                         STRING('Order No'),AT(2615,3333),USE(?OrderNo),FONT(,7),TRN
                         STRING('Accessories'),AT(5479,3333),USE(?Accessories),FONT(,7),TRN
                         STRING('Security Pack No'),AT(4438,3333),USE(?SecurityPackNo),FONT(,7),TRN
                         LINE,AT(104,3281,7292,0),USE(?Line6),COLOR(COLOR:Black)
                         LINE,AT(104,3542,7292,0),USE(?Line6:2),COLOR(COLOR:Black)
                         LINE,AT(219,10052,7292,0),USE(?Line6:3),COLOR(COLOR:Black)
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR2               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR2:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR2:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Waybill Variables
  ! p_web.SSV('locWaybillNumber',)
  ! p_web.SSV('Waybill:Courier,)
  ! p_web.SSV('Waybill:FromType,)
  ! p_web.SSV('Waybill:FromAccount,)
  ! p_web.SSV('Waybill:ToType,)
  ! p_web.SSV('Waybill:ToAccount,)
  GlobalErrors.SetProcedureName('Waybill')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:EXCHANGE.Open                                     ! File EXCHANGE used by this procedure, so make sure it's RelationManager is open
  Relate:JOBACC.SetOpenRelated()
  Relate:JOBACC.Open                                       ! File JOBACC used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:WAYBILLJ.SetOpenRelated()
  Relate:WAYBILLJ.Open                                     ! File WAYBILLJ used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:WAYBILLS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBS.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  locWaybillNumber = p_web.GSV('locWaybillNumber')
  Access:WAYBILLS.Clearkey(way:WayBillNumberKey)
  way:WayBillNumber = locWaybillNumber
  IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey))
  
  END
  SET(DEFAULTS,0)
  Access:DEFAULTS.Next()
  
  locCourier = p_web.GSV('Waybill:Courier')
  
  ! Set consignment number
  IF (way:FromAccount = p_web.GSV('ARC:AccountNumber'))
      IF (GETINI('PRINTING','SetWaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI') = 1)
          locConsignmentNumber = Clip(GETINI('PRINTING','WaybillPrefix',,Clip(Path()) & '\SB2KDEF.INI')) & Format(way:WaybillNumber,@n07)
      ELSE
          locConsignmentNumber = 'VDC' & Format(way:WaybillNumber,@n07)
      END
  ELSE
      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = way:FromAccount
      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          If clip(tra:RRCWaybillPrefix) <> ''
              locConsignmentNumber = clip(tra:RRCWaybillPrefix) & Format(way:WaybillNumber,@n07)
          ELSE
              locConsignmentNumber = 'VDC' & Format(way:WaybillNumber,@n07)
          END
      END
  END
  
  
  
  locDateDespatched = way:TheDate
  locTimeDespatched = way:TheTime
  
  ! Set addresses
  CASE p_web.GSV('Waybill:FromType')
  OF 'TRA'
      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = p_web.GSV('Waybill:FromAccount')
      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          from:CompanyName        = tra:Company_Name
          from:AddressLine1       = tra:Address_Line1
          from:AddressLine2       = tra:Address_Line2
          from:AddressLine3       = tra:Address_Line3
          from:TelephoneNumber    = tra:Telephone_Number
          from:ContactName        = tra:Contact_Name
          from:EmailAddress       = tra:EmailAddress        
      END
      
  OF 'SUB'
      Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
      sub:Account_Number = p_web.GSV('Waybill:FromAccount')
      IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
          from:CompanyName        = sub:Company_Name
          from:AddressLine1       = sub:Address_Line1
          from:AddressLine2       = sub:Address_Line2
          from:AddressLine3       = sub:Address_Line3
          from:TelephoneNumber    = sub:Telephone_Number
          from:ContactName        = sub:Contact_Name
          from:EmailAddress       = sub:EmailAddress
      END
      
  OF 'DEF'
      from:CompanyName        = def:User_Name
      from:AddressLine1       = def:Address_Line1
      from:AddressLine2       = def:Address_Line2
      from:AddressLine3       = def:Address_Line3
      from:TelephoneNumber    = def:Telephone_Number
      from:ContactName        = ''
      from:EmailAddress       = def:EmailAddress
  END
  
  CASE p_web.GSV('Waybill:ToType')
  OF 'TRA'
      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = p_web.GSV('Waybill:ToAccount')
      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          to:AccountNumber      = tra:Account_Number
          to:CompanyName        = tra:Company_Name
          to:AddressLine1       = tra:Address_Line1
          to:AddressLine2       = tra:Address_Line2
          to:AddressLine3       = tra:Address_Line3
          to:TelephoneNumber    = tra:Telephone_Number
          to:ContactName        = tra:Contact_Name
          to:EmailAddress       = tra:EmailAddress
      END
      
  OF 'SUB'
      Access:SUBTRACC.clearkey(sub:Account_Number_Key)
      sub:Account_Number = p_web.GSV('Waybill:ToAccount')
      IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
          to:AccountNumber      = sub:Account_Number
          to:CompanyName        = sub:Company_Name
          to:AddressLine1       = sub:Address_Line1
          to:AddressLine2       = sub:Address_Line2
          to:AddressLine3       = sub:Address_Line3
          to:TelephoneNumber    = sub:Telephone_Number
          to:ContactName        = sub:Contact_Name
          to:EmailAddress       = sub:EmailAddress
      END
      
  END
  
  
  IF (way:WaybillID <> 0)
      IF (way:WaybillID = 13)
          ! This is a rejection
          SETTARGET(Report)
          ?WaybillRejection{PROP:Hide} = FALSE
          SETTARGET()
      END
      
      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = way:FromAccount
      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          from:CompanyName    = tra:Company_Name
          from:AddressLine1   = tra:Address_Line1
          from:AddressLine2   = tra:Address_Line2
          from:AddressLine3   = tra:Address_Line3
          from:TelephoneNumber= tra:Telephone_Number
          from:ContactName    = tra:Contact_Name
          from:EmailAddress   = tra:EmailAddress
      END
      
      IF (way:WaybillID = 20) ! PUP to RRC. Get address from job
          Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
          sub:Account_Number = way:FromAccount
          IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
              from:CompanyName    = sub:Company_Name
              from:AddressLine1   = sub:Address_Line1
              from:AddressLine2   = sub:Address_Line2
              from:AddressLine3   = sub:Address_Line3
              from:TelephoneNumber= sub:Telephone_Number
              from:ContactName    = sub:Contact_Name
              from:EmailAddress   = sub:EmailAddress
          END
          
          IF (sub:VCPWaybillPrefix <> '')
              locConsignmentNumber = clip(sub:VCPWaybillPrefix) & Format(way:WayBillNumber,@n07)
          ELSE
              Access:TRADEACC.ClearKey(tra:Account_Number_Key)
              tra:Account_Number = sub:Main_Account_Number
              IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                  IF (tra:VCPWaybillPrefix <> '')
                      locConsignmentNumber = clip(tra:VCPWaybillPrefix) & Format(way:WayBillNumber,@n07)
                  END
                  
              END
          END
      END
      
      CASE way:WaybillID
      OF 1 OROF 5 OROF 6 OROF 11 OROF 12 OROF 13 OROF 20
          IF (way:ToAccount = 'CUSTOMER') ! PUP to Customer
              to:ContactName = ''
              Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
              sub:Account_Number = way:ToAccount
              IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:benign)
                  IF (sub:Use_Delivery_Address = 'YES')
                      to:ContactName = sub:Contact_Name
                  END
              END
              to:AccountNumber   = job:Account_Number
              to:CompanyName     = job:Company_Name
              to:AddressLine1    = job:Address_Line1
              to:AddressLine2    = job:Address_Line2
              to:AddressLine3    = job:Address_Line3
              to:TelephoneNumber = job:Telephone_Number
  
              to:EmailAddress    = jobe:EndUserEmailAddress
          ELSE
              Access:TRADEACC.ClearKey(tra:Account_Number_Key)
              tra:Account_Number = way:ToAccount
              IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                  to:AccountNumber  = tra:Account_Number
                  to:CompanyName    = tra:Company_Name
                  to:AddressLine1   = tra:Address_Line1
                  to:AddressLine2   = tra:Address_Line2
                  to:AddressLine3   = tra:Address_Line3
                  to:TelephoneNumber= tra:Telephone_Number
                  to:ContactName    = tra:Contact_Name
                  to:EmailAddress   = tra:EmailAddress
              END
              
          END
      OF 21 OROF 22 ! RRC to PUP
          Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
          sub:Account_Number = way:ToAccount
          IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
              to:CompanyName    = sub:Company_Name
              to:AddressLine1   = sub:Address_Line1
              to:AddressLine2   = sub:Address_Line2
              to:AddressLine3   = sub:Address_Line3
              to:TelephoneNumber= sub:Telephone_Number
              to:ContactName    = sub:Contact_Name
              to:EmailAddress   = sub:EmailAddress
          END   
      OF 300 ! Sundry Waybill
          ! Do this later.
      ELSE
          ! Get details from the job on the waybill
          Access:WAYBILLJ.ClearKey(waj:DescWaybillNoKey)
          waj:WayBillNumber = way:WayBillNumber
          IF (Access:WAYBILLJ.TryFetch(waj:DescWaybillNoKey) = Level:Benign)
              Access:JOBS.ClearKey(job:Ref_Number_Key)
              job:Ref_Number = waj:JobNumber
              IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                  
                  Access:JOBSE.ClearKey(jobe:RefNumberKey)
                  jobe:RefNumber = job:Ref_Number
                  IF (Access:JOBSE.tryfetch(jobe:RefNumberKey) = Level:Benign)
                  END
                  
                  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
                  sub:Account_Number = way:ToAccount
                  IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
                      IF (sub:UseCustDespAdd = 'YES')
                          to:AccountNumber   = job:Account_Number
                          to:CompanyName     = job:Company_Name
                          to:AddressLine1    = job:Address_Line1
                          to:AddressLine2    = job:Address_Line2
                          to:AddressLine3    = job:Address_Line3
                          to:TelephoneNumber = job:Telephone_Number
                          !changed bt Paul 28/04/2010 - log no 11419
                          if sub:Use_Delivery_Address = 'YES' then
                              to:ContactName     = clip(sub:Contact_Name)
                          Else
                              to:ContactName     = ''
                          End
                          !end change
                          to:EmailAddress    = jobe:EndUserEmailAddress                    
                      ELSE
                          to:AccountNumber   = job:Account_Number
                          to:CompanyName     = job:Company_Name_Delivery
                          to:AddressLine1    = job:Address_Line1_Delivery
                          to:AddressLine2    = job:Address_Line2_Delivery
                          to:AddressLine3    = job:Address_Line3_Delivery
                          to:TelephoneNumber = job:Telephone_Delivery
                          !changed bt Paul 28/04/2010 - log no 11419
                          if sub:Use_Delivery_Address = 'YES' then
                              to:ContactName     = clip(sub:Contact_Name)
                          Else
                              to:ContactName     = ''
                          End
                          !end change
                          to:EmailAddress    = jobe:EndUserEmailAddress                        
                      END
                  end
              END
          END
      END
  END
  
  locBarcode = '*' & CLIP(locConsignmentNumber) & '*'
  ! #12084 New standard texts (Bryan: 17/05/2011)
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'WAYBILL'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Waybill',ProgressWindow)                   ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:WAYBILLJ, ?Progress:PctText, Progress:Thermometer, ProgressMgr, waj:JobNumber)
  ThisReport.AddSortOrder(waj:JobNumberKey)
  ThisReport.AddRange(waj:WayBillNumber,locWaybillNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:WAYBILLJ.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:JOBACC.Close
    Relate:STANTEXT.Close
    Relate:WAYBILLJ.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('Waybill',ProgressWindow)                ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'Waybill',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR2.Init(Report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,Report{PROPPRINT:Paper},PAPER:USER), CHOOSE(Report{PROP:Thous}=True,PROP:Thous,CHOOSE(Report{PROP:MM}=True,PROP:MM,CHOOSE(Report{PROP:Points}=True,PROP:Points,0))), Report{PROPPRINT:PaperWidth}, Report{PROPPRINT:PaperHeight}, Report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetTitle('Waybill')                  !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR2.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR2:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR2:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR2:vpf = BOR(PDFXTR2:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR2:vpf = BOR(PDFXTR2:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR2:vpf = BOR(PDFXTR2:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR2:vpf = BOR(PDFXTR2:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR2:vpf = BOR(PDFXTR2:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR2:vpf = BOR(PDFXTR2:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetViewerPrefs(PDFXTR2:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR2.SetFontEmbedding(True, True, False, False, False)
    PDFXTR2.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  IF ReturnValue = Level:Benign
    Report$?ReportPageNumber{PROP:PageNo} = True
  END
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'Waybill',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR2.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = waj:JobNumber
        IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
            RETURN Level:User
        END
  
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
            RETURN Level:User
        END
        
        IF (wob:HeadAccountNumber <> p_web.GSV('BookingAccount'))
            ! #13593 Do not display jobs from other sites (DBH: 14/09/2015)
            RETURN Level:User
        END!  IF
  
        locExchanged = ''
        locJobNumber = CLIP(job:Ref_Number) & '-' & p_web.GSV('BookingBranchID') & CLIP(wob:JobNumber)
        
        IF (waj:WayBillNumber = job:Exchange_Consignment_Number)
            Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
            xch:Ref_Number = job:Exchange_Unit_Number
            IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
                locIMEINumber = waj:IMEINumber
            ELSE
                locIMEINumber = xch:ESN
            END
        ELSE
            locIMEINumber = waj:IMEINumber
            IF (job:Exchange_Unit_Number <> 0)
                locExchanged = 'E'
            END
        END
        locSecurityPackNumber = waj:SecurityPackNumber
        locAccessories = ''
        
        If job:Exchange_Unit_Number = 0 Or way:WaybillID = 3 Or way:WaybillID = 6 Or way:WayBillID = 8 Or |
            way:WaybillID = 10 Or way:WaybillID = 11 Or way:WaybillID = 12
  
            Access:JOBACC.ClearKey(jac:Ref_Number_Key)
            jac:Ref_Number = job:Ref_Number
            Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
            Loop
                If Access:JOBACC.NEXT()
                    Break
                End !If
                If jac:Ref_Number <> job:Ref_Number      |
                    Then Break.  ! End If
          ! Inserting (DBH 08/03/2007) # 8703 - Show all accessories if the isn't a 0 (to ARC) or 1 (to RRC)
                If way:WaybillType = 0 Or way:WaybillType = 1
              ! End (DBH 08/03/2007) #8703
              !Only show accessories that were sent to ARC - 4285 (DBH: 26-05-2004)
                    If jac:Attached <> True
                        Cycle
                    End !If jac:Attached <> True
                End ! If way:WaybillType <> 0 And way:WaybillType <> 1
                If locAccessories = ''
                    locAccessories = Clip(jac:Accessory)
                Else !If locAccessories = ''
                    locAccessories = Clip(locAccessories) & ', ' & Clip(jac:Accessory)
                End !If tmp:Accessories = ''
            End !Loop
        End !If waj:JobType = 'JOB' And job:Exchange_Unit_Number = 0
      
        locOrderNumber = job:Order_Number
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:Detail)
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR2:rtn = PDFXTR2.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR2:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

