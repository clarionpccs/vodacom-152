

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER230.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
CheckIfTagged        PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
ReturnValue          BYTE                                  !

  CODE
    ReturnValue = 0
    Relate:TagFile.Open()
    Access:TagFile.ClearKey(tag:keyTagged)
    tag:sessionID = p_web.SessionID
    SET(tag:keyTagged,tag:keyTagged)
    LOOP UNTIL Access:TagFile.Next()
        IF (tag:sessionID <> p_web.SessionID)
            BREAK
        END
        IF (tag:tagged = 1)
            ReturnValue = 1
            BREAK
        END
    END
    Relate:TagFile.Close()
    RETURN ReturnValue
