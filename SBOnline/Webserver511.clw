

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER511.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER010.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER347.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER512.INC'),ONCE        !Req'd for module callout resolution
                     END


CourierTATReportCriteria PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locStartDate         DATE                                  !
locEndDate           DATE                                  !
locTabOption         LONG                                  !
FilesOpened     Long
SUBTRACC::State  USHORT
SBO_GenericTagFile::State  USHORT
TRADEACC::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('CourierTATReportCriteria')
  loc:formname = 'CourierTATReportCriteria_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'CourierTATReportCriteria',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('CourierTATReportCriteria','')
    p_web._DivHeader('CourierTATReportCriteria',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferCourierTATReportCriteria',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCourierTATReportCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCourierTATReportCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_CourierTATReportCriteria',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferCourierTATReportCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_CourierTATReportCriteria',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'CourierTATReportCriteria',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(SBO_GenericTagFile)
  p_web._OpenFile(TRADEACC)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(SBO_GenericTagFile)
  p_Web._CloseFile(TRADEACC)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('CourierTATReportCriteria_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('locStartDate')
    p_web.SetPicture('locStartDate','@d06')
  End
  p_web.SetSessionPicture('locStartDate','@d06')
  If p_web.IfExistsValue('locEndDate')
    p_web.SetPicture('locEndDate','@d06')
  End
  p_web.SetSessionPicture('locEndDate','@d06')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  If p_web.GetValue('tab') = 1
    loc:TabNumber += 1
  End
  If p_web.GetValue('tab') = 2
    loc:TabNumber += 1
  End
  If p_web.GetValue('tab') = 3
    loc:TabNumber += 1
  End
  If p_web.GetValue('tab') = 4
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locStartDate',locStartDate)
  p_web.SetSessionValue('locEndDate',locEndDate)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locStartDate')
    locStartDate = p_web.dformat(clip(p_web.GetValue('locStartDate')),'@d06')
    p_web.SetSessionValue('locStartDate',locStartDate)
  End
  if p_web.IfExistsValue('locEndDate')
    locEndDate = p_web.dformat(clip(p_web.GetValue('locEndDate')),'@d06')
    p_web.SetSessionValue('locEndDate',locEndDate)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('CourierTATReportCriteria_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    IF (p_web.IfExistsValue('firsttime') AND p_web.GetValue('firsttime') = 1)
        ClearGenericTagFile(p_web)
        p_web.SSV('locStartDate',TODAY() - 5)
        p_web.SSV('locEndDate',TODAY())
        p_web.SSV('locAccountFromAll',0)
        p_web.SSV('locAccountToAll',0)
        p_web.SSV('locVCPFromAll',0)
        p_web.SSV('locVCPToAll',0)
    END ! IFi
    CASE p_web.GetValue('tab')
    OF 1
        p_web.SSV('TagTradeAccountsTypeValue','ACC1')
    OF 2
        p_web.SSV('TagTradeAccountsTypeValue','ACC2')
    OF 3
        p_web.SSV('TagSubAccountsType','VCP')
        p_web.SSV('TagSubAccountsTypeValue','VCP1')
    OF 4
        p_web.SSV('TagSubAccountsType','VCP')
        p_web.SSV('TagSubAccountsTypeValue','VCP2')
    END ! CASE
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locStartDate = p_web.RestoreValue('locStartDate')
 locEndDate = p_web.RestoreValue('locEndDate')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferCourierTATReportCriteria')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('CourierTATReportCriteria_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('CourierTATReportCriteria_ChainTo')
    loc:formaction = p_web.GetSessionValue('CourierTATReportCriteria_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'FormWaybillProcedureCriteria'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="CourierTATReportCriteria" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="CourierTATReportCriteria" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="CourierTATReportCriteria" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Courier TAT Report') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Courier TAT Report',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_CourierTATReportCriteria">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_CourierTATReportCriteria" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  do GenerateTab4
  do GenerateTab5
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_CourierTATReportCriteria')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Criteria') & ''''
        If p_web.GetValue('tab') = 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Account Sent From') & ''''
        End
        If p_web.GetValue('tab') = 2
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Account Sent To') & ''''
        End
        If p_web.GetValue('tab') = 3
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('VCP Sent From') & ''''
        End
        If p_web.GetValue('tab') = 4
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('VCP Sent To') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_CourierTATReportCriteria')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_CourierTATReportCriteria'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('CourierTATReportCriteria_TagTradeAccounts_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('CourierTATReportCriteria_TagTradeAccounts_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('CourierTATReportCriteria_TagSubAccounts_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('CourierTATReportCriteria_TagSubAccounts_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locStartDate')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab5'''
          If p_web.GetValue('tab') = 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          End
          If p_web.GetValue('tab') = 2
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          End
          If p_web.GetValue('tab') = 3
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          End
          If p_web.GetValue('tab') = 4
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab6'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_CourierTATReportCriteria')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab5'');'&CRLF
    if p_web.GetValue('tab') = 1
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    end
    if p_web.GetValue('tab') = 2
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    end
    if p_web.GetValue('tab') = 3
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
    end
    if p_web.GetValue('tab') = 4
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab6'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel5">'&CRLF &|
                                    '  <div id="panel5Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Criteria') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel5Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_CourierTATReportCriteria_5">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab5" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab5">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locStartDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locStartDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locStartDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEndDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEndDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEndDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::linkAccountFrom
      do Comment::linkAccountFrom
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::linkAccountTo
      do Comment::linkAccountTo
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::linkVCPFrom
      do Comment::linkVCPFrom
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::linkVCPTo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::linkVCPTo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
  If p_web.GetValue('tab') = 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Account Sent From') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_CourierTATReportCriteria_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Account Sent From')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Account Sent From')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Account Sent From')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Account Sent From')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwAccountFrom
      do Comment::brwAccountFrom
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnTagAll1
      do Comment::btnTagAll1
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnUnTagAll1
      do Comment::btnUnTagAll1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab2  Routine
  If p_web.GetValue('tab') = 2
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Account Sent To') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_CourierTATReportCriteria_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Account Sent To')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Account Sent To')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Account Sent To')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Account Sent To')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwAccountTo
      do Comment::brwAccountTo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnTagAll2
      do Comment::btnTagAll2
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnUnTagAll2
      do Comment::btnUnTagAll2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab3  Routine
  If p_web.GetValue('tab') = 3
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('VCP Sent From') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_CourierTATReportCriteria_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('VCP Sent From')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('VCP Sent From')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('VCP Sent From')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('VCP Sent From')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwVCPSentFrom
      do Comment::brwVCPSentFrom
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnTagAll3
      do Comment::btnTagAll3
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnUnTagAll3
      do Comment::btnUnTagAll3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab4  Routine
  If p_web.GetValue('tab') = 4
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('VCP Sent To') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_CourierTATReportCriteria_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('VCP Sent To')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('VCP Sent To')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('VCP Sent To')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('VCP Sent To')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwVCPSentTo
      do Comment::brwVCPSentTo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnTagAll4
      do Comment::btnTagAll4
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnUnTagAll4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnUnTagAll4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab5  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel6">'&CRLF &|
                                    '  <div id="panel6Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel6Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_CourierTATReportCriteria_6">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab6" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab6">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab6">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab6">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnCreateExport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnCreateExport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locStartDate  Routine
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('locStartDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Start Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locStartDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locStartDate',p_web.GetValue('NewValue'))
    locStartDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locStartDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locStartDate',p_web.dFormat(p_web.GetValue('Value'),'@d06'))
    locStartDate = p_web.Dformat(p_web.GetValue('Value'),'@d06') !
  End
  do Value::locStartDate
  do SendAlert

Value::locStartDate  Routine
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('locStartDate') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locStartDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locStartDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locStartDate'',''couriertatreportcriteria_locstartdate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locStartDate',p_web.GetSessionValue('locStartDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06',loc:javascript,,) & '<13,10>'
  do SendPacket
   packet = CLIP(packet) & |
     '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar' & |
     '(locStartDate,''dd/mm/yyyy'',this); Date.disabled=true;' & |
     'sv(''...'',''CourierTATReportCriteria_pickdate_value'',1,FieldValue(this,1));nextFocus(CourierTATReportCriteria_frm,'''',0);" ' & |
     'value="Select Date" name="Date" type="button">...</button>'
   DO SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CourierTATReportCriteria_' & p_web._nocolon('locStartDate') & '_value')

Comment::locStartDate  Routine
      loc:comment = ''
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('locStartDate') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locEndDate  Routine
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('locEndDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('End Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locEndDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEndDate',p_web.GetValue('NewValue'))
    locEndDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEndDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEndDate',p_web.dFormat(p_web.GetValue('Value'),'@d06'))
    locEndDate = p_web.Dformat(p_web.GetValue('Value'),'@d06') !
  End
  do Value::locEndDate
  do SendAlert

Value::locEndDate  Routine
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('locEndDate') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locEndDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locEndDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEndDate'',''couriertatreportcriteria_locenddate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locEndDate',p_web.GetSessionValue('locEndDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06',loc:javascript,,) & '<13,10>'
  do SendPacket
   packet = CLIP(packet) & |
     '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar' & |
     '(locEndDate,''dd/mm/yyyy'',this); Date.disabled=true;' & |
     'sv(''...'',''CourierTATReportCriteria_pickdate_value'',1,FieldValue(this,1));nextFocus(CourierTATReportCriteria_frm,'''',0);" ' & |
     'value="Select Date" name="Date" type="button">...</button>'
   DO SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CourierTATReportCriteria_' & p_web._nocolon('locEndDate') & '_value')

Comment::locEndDate  Routine
      loc:comment = ''
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('locEndDate') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::linkAccountFrom  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('linkAccountFrom',p_web.GetValue('NewValue'))
    do Value::linkAccountFrom
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::linkAccountFrom  Routine
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('linkAccountFrom') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.CreateHyperLink(p_web.Translate('Select Account Sent From'),'CourierTATReportCriteria?tab=1',,CHOOSE(p_web.GetValue('tab') = 1,'GreenBold',''),loc:javascript,,) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::linkAccountFrom  Routine
    loc:comment = ''
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('linkAccountFrom') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::linkAccountTo  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('linkAccountTo',p_web.GetValue('NewValue'))
    do Value::linkAccountTo
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::linkAccountTo  Routine
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('linkAccountTo') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.CreateHyperLink(p_web.Translate('Select Account Sent To'),'CourierTATReportCriteria?tab=2',,CHOOSE(p_web.GetValue('tab') = 2,'GreenBold',''),loc:javascript,,) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::linkAccountTo  Routine
    loc:comment = ''
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('linkAccountTo') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::linkVCPFrom  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('linkVCPFrom',p_web.GetValue('NewValue'))
    do Value::linkVCPFrom
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::linkVCPFrom  Routine
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('linkVCPFrom') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.CreateHyperLink(p_web.Translate('Select VCP Sent From'),'CourierTATReportCriteria?tab=3',,CHOOSE(p_web.GetValue('tab') = 3,'GreenBold',''),loc:javascript,,) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::linkVCPFrom  Routine
    loc:comment = ''
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('linkVCPFrom') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::linkVCPTo  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('linkVCPTo',p_web.GetValue('NewValue'))
    do Value::linkVCPTo
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::linkVCPTo  Routine
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('linkVCPTo') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.CreateHyperLink(p_web.Translate('Select VCP Sent To'),'CourierTATReportCriteria?tab=4',,CHOOSE(p_web.GetValue('tab') = 4,'GreenBold',''),loc:javascript,,) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::linkVCPTo  Routine
    loc:comment = ''
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('linkVCPTo') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::brwAccountFrom  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwAccountFrom',p_web.GetValue('NewValue'))
    do Value::brwAccountFrom
  Else
    p_web.StoreValue('tra:RecordNumber')
  End

Value::brwAccountFrom  Routine
  loc:extra = ''
  ! --- BROWSE ---  TagTradeAccounts --
  p_web.SetValue('TagTradeAccounts:NoForm',1)
  p_web.SetValue('TagTradeAccounts:FormName',loc:formname)
  p_web.SetValue('TagTradeAccounts:parentIs','Form')
  p_web.SetValue('_parentProc','CourierTATReportCriteria')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('CourierTATReportCriteria_TagTradeAccounts_embedded_div')&'"><!-- Net:TagTradeAccounts --></div><13,10>'
    p_web._DivHeader('CourierTATReportCriteria_' & lower('TagTradeAccounts') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('CourierTATReportCriteria_' & lower('TagTradeAccounts') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagTradeAccounts --><13,10>'
  end
  do SendPacket

Comment::brwAccountFrom  Routine
    loc:comment = ''
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('brwAccountFrom') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnTagAll1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnTagAll1',p_web.GetValue('NewValue'))
    do Value::btnTagAll1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    Access:TRADEACC.ClearKey(tra:RecordNumberKey)
    tra:RecordNumber = 1
    SET(tra:RecordNumberKey,tra:RecordNumberKey)
    LOOP UNTIL Access:TRADEACC.Next() <> Level:Benign
        IF (tra:Account_Number <> 'AA20')
            IF (tra:RemoteREpairCentre <> 1)
                CYCLE
            END ! IF
            IF (tra:Stop_Account = 'YES')
                CYCLE
            END ! IF
        END ! IF
    
        Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
        tagf:SessionID = p_web.SessionID
        tagf:TagType = 'ACC1'
        tagf:TaggedValue = tra:Account_Number
        IF (Access:SBO_GenericTagFile.TryFetch(tagf:KeyTagged) = Level:Benign)
            tagf:Tagged = 1
            Access:SBO_GenericTagFile.TryUpdate()
        ELSE ! IF
            IF (Access:SBO_GenericTagFile.PrimeRecord() = Level:Benign)
                tagf:SessionID = p_web.SessionID
                tagf:TagType = 'ACC1'
                tagf:TaggedValue = tra:Account_Number
                tagf:Tagged = 1
                IF (Access:SBO_GenericTagFile.TryInsert())
                    Access:SBO_GenericTagFile.CancelAutoInc()
                END ! IF
            END ! IF
        END ! IF
    END ! LOOP
  do SendAlert
  do Value::brwAccountFrom  !1

Value::btnTagAll1  Routine
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('btnTagAll1') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnTagAll1'',''couriertatreportcriteria_btntagall1_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnTagAll1','Tag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CourierTATReportCriteria_' & p_web._nocolon('btnTagAll1') & '_value')

Comment::btnTagAll1  Routine
    loc:comment = ''
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('btnTagAll1') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnUnTagAll1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnUnTagAll1',p_web.GetValue('NewValue'))
    do Value::btnUnTagAll1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    ClearGenericTagFile(p_web,'ACC1')  
  do SendAlert
  do Value::brwAccountFrom  !1

Value::btnUnTagAll1  Routine
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('btnUnTagAll1') & '_value',Choose(p_web.GSV('locAccountFromAll') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAccountFromAll') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnUnTagAll1'',''couriertatreportcriteria_btnuntagall1_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnUnTagAll1','UnTag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('CourierTATReportCriteria_' & p_web._nocolon('btnUnTagAll1') & '_value')

Comment::btnUnTagAll1  Routine
    loc:comment = ''
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('btnUnTagAll1') & '_comment',Choose(p_web.GSV('locAccountFromAll') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAccountFromAll') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::brwAccountTo  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwAccountTo',p_web.GetValue('NewValue'))
    do Value::brwAccountTo
  Else
    p_web.StoreValue('tra:RecordNumber')
  End

Value::brwAccountTo  Routine
  loc:extra = ''
  ! --- BROWSE ---  TagTradeAccounts --
  p_web.SetValue('TagTradeAccounts:NoForm',1)
  p_web.SetValue('TagTradeAccounts:FormName',loc:formname)
  p_web.SetValue('TagTradeAccounts:parentIs','Form')
  p_web.SetValue('_parentProc','CourierTATReportCriteria')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('CourierTATReportCriteria_TagTradeAccounts_embedded_div')&'"><!-- Net:TagTradeAccounts --></div><13,10>'
    p_web._DivHeader('CourierTATReportCriteria_' & lower('TagTradeAccounts') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('CourierTATReportCriteria_' & lower('TagTradeAccounts') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagTradeAccounts --><13,10>'
  end
  do SendPacket

Comment::brwAccountTo  Routine
    loc:comment = ''
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('brwAccountTo') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnTagAll2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnTagAll2',p_web.GetValue('NewValue'))
    do Value::btnTagAll2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    Access:TRADEACC.ClearKey(tra:RecordNumberKey)
    tra:RecordNumber = 1
    SET(tra:RecordNumberKey,tra:RecordNumberKey)
    LOOP UNTIL Access:TRADEACC.Next() <> Level:Benign
        IF (tra:Account_Number <> 'AA20')
            IF (tra:RemoteREpairCentre <> 1)
                CYCLE
            END ! IF
            IF (tra:Stop_Account = 'YES')
                CYCLE
            END ! IF
        END ! IF
    
        Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
        tagf:SessionID = p_web.SessionID
        tagf:TagType = 'ACC2'
        tagf:TaggedValue = tra:Account_Number
        IF (Access:SBO_GenericTagFile.TryFetch(tagf:KeyTagged) = Level:Benign)
            tagf:Tagged = 1
            Access:SBO_GenericTagFile.TryUpdate()
        ELSE ! IF
            IF (Access:SBO_GenericTagFile.PrimeRecord() = Level:Benign)
                tagf:SessionID = p_web.SessionID
                tagf:TagType = 'ACC2'
                tagf:TaggedValue = tra:Account_Number
                tagf:Tagged = 1
                IF (Access:SBO_GenericTagFile.TryInsert())
                    Access:SBO_GenericTagFile.CancelAutoInc()
                END ! IF
            END ! IF
        END ! IF
    END ! LOOP  
  do SendAlert
  do Value::brwAccountTo  !1

Value::btnTagAll2  Routine
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('btnTagAll2') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnTagAll2'',''couriertatreportcriteria_btntagall2_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnTagAll2','Tag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CourierTATReportCriteria_' & p_web._nocolon('btnTagAll2') & '_value')

Comment::btnTagAll2  Routine
    loc:comment = ''
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('btnTagAll2') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnUnTagAll2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnUnTagAll2',p_web.GetValue('NewValue'))
    do Value::btnUnTagAll2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ClearGenericTagFile(p_web,'ACC2')
  do SendAlert
  do Value::brwAccountTo  !1

Value::btnUnTagAll2  Routine
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('btnUnTagAll2') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnUnTagAll2'',''couriertatreportcriteria_btnuntagall2_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnUnTagAll2','UnTag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CourierTATReportCriteria_' & p_web._nocolon('btnUnTagAll2') & '_value')

Comment::btnUnTagAll2  Routine
    loc:comment = ''
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('btnUnTagAll2') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::brwVCPSentFrom  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwVCPSentFrom',p_web.GetValue('NewValue'))
    do Value::brwVCPSentFrom
  Else
    p_web.StoreValue('sub:RecordNumber')
  End

Value::brwVCPSentFrom  Routine
  loc:extra = ''
  ! --- BROWSE ---  TagSubAccounts --
  p_web.SetValue('TagSubAccounts:NoForm',1)
  p_web.SetValue('TagSubAccounts:FormName',loc:formname)
  p_web.SetValue('TagSubAccounts:parentIs','Form')
  p_web.SetValue('_parentProc','CourierTATReportCriteria')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('CourierTATReportCriteria_TagSubAccounts_embedded_div')&'"><!-- Net:TagSubAccounts --></div><13,10>'
    p_web._DivHeader('CourierTATReportCriteria_' & lower('TagSubAccounts') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('CourierTATReportCriteria_' & lower('TagSubAccounts') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagSubAccounts --><13,10>'
  end
  do SendPacket

Comment::brwVCPSentFrom  Routine
    loc:comment = ''
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('brwVCPSentFrom') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnTagAll3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnTagAll3',p_web.GetValue('NewValue'))
    do Value::btnTagAll3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = ''
    SET(sub:Account_Number_Key,sub:Account_Number_Key)
    LOOP UNTIL Access:SUBTRACC.Next() <> Level:Benign
        IF (sub:SIDJobBooking = 0)
            CYCLE
        END ! IF
        IF (sub:Stop_Account = 'YES')
            CYCLE
        END ! IF
    
        Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
        tagf:SessionID = p_web.SessionID
        tagf:TagType = 'VCP1'
        tagf:TaggedValue = sub:Account_Number
        IF (Access:SBO_GenericTagFile.TryFetch(tagf:KeyTagged) = Level:Benign)
            tagf:Tagged = 1
            Access:SBO_GenericTagFile.TryUpdate()
        ELSE ! IF
            IF (Access:SBO_GenericTagFile.PrimeRecord() = Level:Benign)
                tagf:SessionID = p_web.SessionID
                tagf:TagType = 'VCP1'
                tagf:TaggedValue = sub:Account_Number
                tagf:Tagged = 1
                IF (Access:SBO_GenericTagFile.TryInsert())
                    Access:SBO_GenericTagFile.CancelAutoInc()
                END ! IF
            END ! IF
        END ! IF
  
    END ! LOOP  
  do SendAlert
  do Value::brwVCPSentFrom  !1

Value::btnTagAll3  Routine
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('btnTagAll3') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnTagAll3'',''couriertatreportcriteria_btntagall3_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnTagAll3','Tag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CourierTATReportCriteria_' & p_web._nocolon('btnTagAll3') & '_value')

Comment::btnTagAll3  Routine
    loc:comment = ''
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('btnTagAll3') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnUnTagAll3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnUnTagAll3',p_web.GetValue('NewValue'))
    do Value::btnUnTagAll3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ClearGenericTagFile(p_web,'VCP1')
  do SendAlert
  do Value::brwVCPSentFrom  !1

Value::btnUnTagAll3  Routine
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('btnUnTagAll3') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnUnTagAll3'',''couriertatreportcriteria_btnuntagall3_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnUnTagAll3','UnTag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CourierTATReportCriteria_' & p_web._nocolon('btnUnTagAll3') & '_value')

Comment::btnUnTagAll3  Routine
    loc:comment = ''
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('btnUnTagAll3') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::brwVCPSentTo  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwVCPSentTo',p_web.GetValue('NewValue'))
    do Value::brwVCPSentTo
  Else
    p_web.StoreValue('sub:RecordNumber')
  End

Value::brwVCPSentTo  Routine
  loc:extra = ''
  ! --- BROWSE ---  TagSubAccounts --
  p_web.SetValue('TagSubAccounts:NoForm',1)
  p_web.SetValue('TagSubAccounts:FormName',loc:formname)
  p_web.SetValue('TagSubAccounts:parentIs','Form')
  p_web.SetValue('_parentProc','CourierTATReportCriteria')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('CourierTATReportCriteria_TagSubAccounts_embedded_div')&'"><!-- Net:TagSubAccounts --></div><13,10>'
    p_web._DivHeader('CourierTATReportCriteria_' & lower('TagSubAccounts') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('CourierTATReportCriteria_' & lower('TagSubAccounts') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagSubAccounts --><13,10>'
  end
  do SendPacket

Comment::brwVCPSentTo  Routine
    loc:comment = ''
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('brwVCPSentTo') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnTagAll4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnTagAll4',p_web.GetValue('NewValue'))
    do Value::btnTagAll4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = ''
    SET(sub:Account_Number_Key,sub:Account_Number_Key)
    LOOP UNTIL Access:SUBTRACC.Next() <> Level:Benign
        IF (sub:SIDJobBooking = 0)
            CYCLE
        END ! IF
        IF (sub:Stop_Account = 'YES')
            CYCLE
        END ! IF
    
        Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
        tagf:SessionID = p_web.SessionID
        tagf:TagType = 'VCP2'
        tagf:TaggedValue = sub:Account_Number
        IF (Access:SBO_GenericTagFile.TryFetch(tagf:KeyTagged) = Level:Benign)
            tagf:Tagged = 1
            Access:SBO_GenericTagFile.TryUpdate()
        ELSE ! IF
            IF (Access:SBO_GenericTagFile.PrimeRecord() = Level:Benign)
                tagf:SessionID = p_web.SessionID
                tagf:TagType = 'VCP2'
                tagf:TaggedValue = sub:Account_Number
                tagf:Tagged = 1
                IF (Access:SBO_GenericTagFile.TryInsert())
                    Access:SBO_GenericTagFile.CancelAutoInc()
                END ! IF
            END ! IF
        END ! IF
  
    END ! LOOP  
  do SendAlert
  do Value::brwVCPSentTo  !1

Value::btnTagAll4  Routine
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('btnTagAll4') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnTagAll4'',''couriertatreportcriteria_btntagall4_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnTagAll4','Tag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CourierTATReportCriteria_' & p_web._nocolon('btnTagAll4') & '_value')

Comment::btnTagAll4  Routine
    loc:comment = ''
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('btnTagAll4') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnUnTagAll4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnUnTagAll4',p_web.GetValue('NewValue'))
    do Value::btnUnTagAll4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ClearGenericTagFile(p_web,'VCP2')
  do SendAlert
  do Value::brwVCPSentTo  !1

Value::btnUnTagAll4  Routine
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('btnUnTagAll4') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnUnTagAll4'',''couriertatreportcriteria_btnuntagall4_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnUnTagAll4','UnTag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('CourierTATReportCriteria_' & p_web._nocolon('btnUnTagAll4') & '_value')

Comment::btnUnTagAll4  Routine
    loc:comment = ''
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('btnUnTagAll4') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnCreateExport  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnCreateExport',p_web.GetValue('NewValue'))
    do Value::btnCreateExport
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnCreateExport  Routine
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('btnCreateExport') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnCreateExport','Create Export','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=CourierTATExport')) & ''','''&clip('_blank')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()

Comment::btnCreateExport  Routine
    loc:comment = ''
  p_web._DivHeader('CourierTATReportCriteria_' & p_web._nocolon('btnCreateExport') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('CourierTATReportCriteria_locStartDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locStartDate
      else
        do Value::locStartDate
      end
  of lower('CourierTATReportCriteria_locEndDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEndDate
      else
        do Value::locEndDate
      end
  of lower('CourierTATReportCriteria_btnTagAll1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnTagAll1
      else
        do Value::btnTagAll1
      end
  of lower('CourierTATReportCriteria_btnUnTagAll1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnUnTagAll1
      else
        do Value::btnUnTagAll1
      end
  of lower('CourierTATReportCriteria_btnTagAll2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnTagAll2
      else
        do Value::btnTagAll2
      end
  of lower('CourierTATReportCriteria_btnUnTagAll2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnUnTagAll2
      else
        do Value::btnUnTagAll2
      end
  of lower('CourierTATReportCriteria_btnTagAll3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnTagAll3
      else
        do Value::btnTagAll3
      end
  of lower('CourierTATReportCriteria_btnUnTagAll3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnUnTagAll3
      else
        do Value::btnUnTagAll3
      end
  of lower('CourierTATReportCriteria_btnTagAll4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnTagAll4
      else
        do Value::btnTagAll4
      end
  of lower('CourierTATReportCriteria_btnUnTagAll4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnUnTagAll4
      else
        do Value::btnUnTagAll4
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('CourierTATReportCriteria_form:ready_',1)
  p_web.SetSessionValue('CourierTATReportCriteria_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_CourierTATReportCriteria',0)

PreCopy  Routine
  p_web.SetValue('CourierTATReportCriteria_form:ready_',1)
  p_web.SetSessionValue('CourierTATReportCriteria_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_CourierTATReportCriteria',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('CourierTATReportCriteria_form:ready_',1)
  p_web.SetSessionValue('CourierTATReportCriteria_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('CourierTATReportCriteria:Primed',0)

PreDelete       Routine
  p_web.SetValue('CourierTATReportCriteria_form:ready_',1)
  p_web.SetSessionValue('CourierTATReportCriteria_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('CourierTATReportCriteria:Primed',0)
  p_web.setsessionvalue('showtab_CourierTATReportCriteria',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GetValue('tab') = 1
  End
  If p_web.GetValue('tab') = 2
  End
  If p_web.GetValue('tab') = 3
  End
  If p_web.GetValue('tab') = 4
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('CourierTATReportCriteria_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('CourierTATReportCriteria_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 5
    loc:InvalidTab += 1
  ! tab = 1
  If p_web.GetValue('tab') = 1
    loc:InvalidTab += 1
  End
  ! tab = 2
  If p_web.GetValue('tab') = 2
    loc:InvalidTab += 1
  End
  ! tab = 3
  If p_web.GetValue('tab') = 3
    loc:InvalidTab += 1
  End
  ! tab = 4
  If p_web.GetValue('tab') = 4
    loc:InvalidTab += 1
  End
  ! tab = 6
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('CourierTATReportCriteria:Primed',0)
  p_web.StoreValue('locStartDate')
  p_web.StoreValue('locEndDate')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
