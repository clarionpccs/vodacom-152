

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER484.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER013.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER286.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseJobProgress    PROCEDURE  (NetWebServerWorker p_web)
locJobNumber         STRING(30)                            !
locLocation          STRING(30)                            !
locStatus            STRING(40)                            !
locCustomerName      STRING(60)                            !
locType              STRING(1)                             !
locJobProgressStartDate DATE                               !
locJobProgressEndDate DATE                                 !
                    MAP
FilterAccountNumber     PROCEDURE(),LONG
FilterDateBooked        PROCEDURE(),LONG
FilterLocation          PROCEDURE(),LONG
FilterModelNumber       PROCEDURE(),LONG
FilterStatus            PROCEDURE(),LONG
                    END ! MAP
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(SBO_GenericFile)
                      Project(sbogen:RecordNumber)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
TRADEACC::State  USHORT
AUDSTATS::State  USHORT
WEBJOB::State  USHORT
TagFile::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseJobProgress')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseJobProgress:NoForm')
      loc:NoForm = p_web.GetValue('BrowseJobProgress:NoForm')
      loc:FormName = p_web.GetValue('BrowseJobProgress:FormName')
    else
      loc:FormName = 'BrowseJobProgress_frm'
    End
    p_web.SSV('BrowseJobProgress:NoForm',loc:NoForm)
    p_web.SSV('BrowseJobProgress:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseJobProgress:NoForm')
    loc:FormName = p_web.GSV('BrowseJobProgress:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseJobProgress') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseJobProgress')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(SBO_GenericFile,sbogen:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'LOCJOBNUMBER') then p_web.SetValue('BrowseJobProgress_sort','12')
    ElsIf (loc:vorder = 'SBOGEN:LONG1') then p_web.SetValue('BrowseJobProgress_sort','12')
    ElsIf (loc:vorder = 'WOB:ORDERNUMBER') then p_web.SetValue('BrowseJobProgress_sort','19')
    ElsIf (loc:vorder = 'WOB:MOBILENUMBER') then p_web.SetValue('BrowseJobProgress_sort','20')
    ElsIf (loc:vorder = 'WOB:MODELNUMBER') then p_web.SetValue('BrowseJobProgress_sort','21')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseJobProgress:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseJobProgress:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseJobProgress:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseJobProgress:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseJobProgress:LookupField')
    loc:selecting = 0
  End
  

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Above
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
    ! #13541 Skip logging for next/previous (DBH: 15/10/2015)
    IF (~INSTRING('Refresh=previous',p_web.RequestData.DataString,1,1) AND |
        ~INSTRING('Refresh=next',p_web.RequestData.DataString,1,1) AND | 
        ~INSTRING('Refresh=first',p_web.RequestData.DataString,1,1) AND | 
        ~INSTRING('Refresh=last',p_web.RequestData.DataString,1,1))        
  ! Save Window Name
  AddToLog('NetWebBrowse',p_web.RequestData.DataString,'BrowseJobProgress',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseJobProgress_sort',net:DontEvaluate)
  If loc:vordernumber = 0
    loc:vordernumber = 12
  End
  p_web.SetSessionValue('BrowseJobProgress_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 12
    loc:vorder = Choose(Loc:SortDirection=1,'sbogen:Long1','-sbogen:Long1')
    Loc:LocateField = 'sbogen:Long1'
  of 19
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(wob:OrderNumber)','-UPPER(wob:OrderNumber)')
    Loc:LocateField = 'wob:OrderNumber'
  of 20
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(wob:MobileNumber)','-UPPER(wob:MobileNumber)')
    Loc:LocateField = 'wob:MobileNumber'
  of 21
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(wob:ModelNumber)','-UPPER(wob:ModelNumber)')
    Loc:LocateField = 'wob:ModelNumber'
  of 25
    Loc:LocateField = ''
  of 22
    Loc:LocateField = ''
  of 11
    Loc:LocateField = ''
  end
  If False ! add range fields to sort order
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('tag:tagged')
    loc:SortHeader = p_web.Translate('')
    p_web.SetSessionValue('BrowseJobProgress_LocatorPic','@n3')
  Of upper('locJobNumber')
  OrOf upper('sbogen:Long1')
    loc:SortHeader = p_web.Translate('Job Number')
    p_web.SetSessionValue('BrowseJobProgress_LocatorPic','@s30')
  Of upper('wob:SubAcountNumber')
    loc:SortHeader = p_web.Translate('Account')
    p_web.SetSessionValue('BrowseJobProgress_LocatorPic','@s30')
  Of upper('locCustomerName')
    loc:SortHeader = p_web.Translate('Customer Name')
    p_web.SetSessionValue('BrowseJobProgress_LocatorPic','@s60')
  Of upper('wob:IMEINumber')
    loc:SortHeader = p_web.Translate('I.M.E.I.')
    p_web.SetSessionValue('BrowseJobProgress_LocatorPic','@s30')
  Of upper('locStatus')
    loc:SortHeader = p_web.Translate('Status')
    p_web.SetSessionValue('BrowseJobProgress_LocatorPic','@s35')
  Of upper('job:Location')
    loc:SortHeader = p_web.Translate('Location')
    p_web.SetSessionValue('BrowseJobProgress_LocatorPic','@s30')
  Of upper('locType')
    loc:SortHeader = p_web.Translate('Type')
    p_web.SetSessionValue('BrowseJobProgress_LocatorPic','@s1')
  Of upper('wob:OrderNumber')
    loc:SortHeader = p_web.Translate('Order Number')
    p_web.SetSessionValue('BrowseJobProgress_LocatorPic','@s30')
  Of upper('wob:MobileNumber')
    loc:SortHeader = p_web.Translate('Mobile Number')
    p_web.SetSessionValue('BrowseJobProgress_LocatorPic','@s30')
  Of upper('wob:ModelNumber')
    loc:SortHeader = p_web.Translate('Model Number')
    p_web.SetSessionValue('BrowseJobProgress_LocatorPic','@s30')
  Of upper('wob:DespatchCourier')
    loc:SortHeader = p_web.Translate('Courier')
    p_web.SetSessionValue('BrowseJobProgress_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseJobProgress:LookupFrom')
  End!Else
    loc:formaction = 'ViewJob'
    loc:formactiontarget = '_self'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseJobProgress:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseJobProgress:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseJobProgress:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="SBO_GenericFile"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="sbogen:RecordNumberKey"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobProgress',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator fixedtd')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseJobProgress',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator fixedtd',,'onchange="BrowseJobProgress.locate(''Locator2BrowseJobProgress'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseJobProgress.cl(''BrowseJobProgress'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseJobProgress_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseJobProgress_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by tagged')&'">'&p_web.Translate('')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'12','BrowseJobProgress','Job Number',,,,,1)
        Else
          packet = clip(packet) & '<th>'&p_web.Translate('Job Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Sub Account Number')&'">'&p_web.Translate('Account')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&p_web.Translate('Customer Name')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by I.M.E.I. Number')&'">'&p_web.Translate('I.M.E.I.')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&p_web.Translate('Status')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Location')&'">'&p_web.Translate('Location')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&p_web.Translate('Type')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'19','BrowseJobProgress','Order Number','Click here to sort by Order Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Order Number')&'">'&p_web.Translate('Order Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'20','BrowseJobProgress','Mobile Number','Click here to sort by Mobile Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Mobile Number')&'">'&p_web.Translate('Mobile Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'21','BrowseJobProgress','Model Number','Click here to sort by Model Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Model Number')&'">'&p_web.Translate('Model Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Current Courier')&'">'&p_web.Translate('Courier')&'</th>'&CRLF
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  Despatch
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  CustServ
        do AddPacket
        loc:columns += 1
        packet = clip(packet) & '<th>'&NBSP&'</th>'&CRLF ! no heading for this column  Edit
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('sbogen:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and SBO_GenericFile{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'sbogen:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('sbogen:RecordNumber'),p_web.GetValue('sbogen:RecordNumber'),p_web.GetSessionValue('sbogen:RecordNumber'))
      loc:FilterWas = 'sbogen:SessionID = ' & p_web.SessionID
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobProgress',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseJobProgress_Filter')
    p_web.SetSessionValue('BrowseJobProgress_FirstValue','')
    p_web.SetSessionValue('BrowseJobProgress_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,SBO_GenericFile,sbogen:RecordNumberKey,loc:PageRows,'BrowseJobProgress',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If SBO_GenericFile{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(SBO_GenericFile,loc:firstvalue)
              Reset(ThisView,SBO_GenericFile)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If SBO_GenericFile{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(SBO_GenericFile,loc:lastvalue)
            Reset(ThisView,SBO_GenericFile)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      Access:WEBJOB.ClearKey(wob:RefNumberKey)
      wob:RefNumber = sbogen:Long1
      Access:WEBJOB.TryFetch(wob:RefNumberKey)
      
      Access:JOBS.ClearKey(job:Ref_Number_Key)
      job:Ref_Number = sbogen:Long1
      Access:JOBS.TryFetch(job:Ref_Number_Key)
      !Region Validate
        IF (FilterAccountNumber())
            CYCLE
        END ! IF
        
        IF (FilterLocation())
            CYCLE
        END ! IF
        
        IF (FilterStatus())
            CYCLE
        END ! IF
        
        IF (FilterModelNumber())
            CYCLE
        END ! IF
      !endregion        
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(sbogen:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
        
        p_web.SSV('sbogen:RecordNumber','')
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseJobProgress.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseJobProgress.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseJobProgress.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseJobProgress.last();',,loc:nextdisabled)
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseJobProgress',Net:Below)
     
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseJobProgress_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseJobProgress_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator fixedtd')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseJobProgress',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator fixedtd',,'onchange="BrowseJobProgress.locate(''Locator1BrowseJobProgress'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseJobProgress.cl(''BrowseJobProgress'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseJobProgress_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseJobProgress_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseJobProgress.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseJobProgress.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseJobProgress.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseJobProgress.last();',,loc:nextdisabled)
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    ! Fill In Browse Variables
    locJobNumber = CLIP(wob:RefNumber) & ' - ' & p_web.GSV('BookingBranchID') & CLIP(wob:JobNumber)
    
    locStatus = wob:Current_Status
    locCustomerName = job:Company_Name
    
    IF (job:Exchange_Unit_Number > 0)
        locStatus = 'E ' & CLIP(locStatus)
    ELSE
        locStatus = 'J ' & CLIP(locStatus)
    END ! IF
    
    locLocation = job:Location
    
    IF (wob:IMEINumber = '* IN PROGRESS *' AND job:Date_Booked < TODAY())
        locCustomerName = 'BOOKING ABORTED'
        locLocation = 'DESPATCHED'
    END 
    
    IF (job:Current_Status = '799 JOB CANCELLED')
        locLocation = 'DESPATCHED'
    END ! IF        
    
    IF (job:Warranty_Job = 'YES')
        IF (job:Chargeable_Job = 'YES')
            locType = 'S'
        ELSE ! IF
            locType = 'W'
        END ! IF
    ELSE ! IF
        IF (job:Chargeable_Job = 'YES')
            locType = 'C'
        ELSE ! IF
            locType = 'X'
        END ! IF
    END ! IF    
    
    
    Access:TAGFILE.ClearKey(tag:keyTagged)
    tag:SessionID = p_web.SessionID
    tag:TaggedValue = sbogen:Long1
    IF (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
    
    ELSE ! IF
        tag:Tagged = 0
    END ! IF
    loc:field = sbogen:RecordNumber
    p_web._thisrow = p_web._nocolon('sbogen:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseJobProgress:LookupField')) = sbogen:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((sbogen:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseJobProgress.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If SBO_GenericFile{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(SBO_GenericFile)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If SBO_GenericFile{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(SBO_GenericFile)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','sbogen:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BrowseJobProgress.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','sbogen:RecordNumber',clip(loc:field),,'checked',,,'onclick="BrowseJobProgress.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
      IF (loc:Checked = 'checked')
        p_web.SSV('sbogen:RecordNumber',sbogen:RecordNumber)
      END ! IF
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::tag:tagged
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locJobNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::wob:SubAcountNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locCustomerName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::wob:IMEINumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locStatus
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::job:Location
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::locType
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::wob:OrderNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::wob:MobileNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::wob:ModelNumber
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif wob:ReadyToDespatch <> 1
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            else
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            end
          end ! loc:eip = 0
          do value::wob:DespatchCourier
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
            if false
            elsif wob:ReadyToDespatch <> 1
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            else
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
            end
          end ! loc:eip = 0
          do value::Despatch
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::CustServ
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::Edit
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseJobProgress.omv(this);" onMouseOut="BrowseJobProgress.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseJobProgress=new browseTable(''BrowseJobProgress'','''&clip(loc:formname)&''','''&p_web._jsok('sbogen:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('sbogen:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''',''ViewJob'');<13,10>'&|
      'BrowseJobProgress.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseJobProgress.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseJobProgress')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseJobProgress')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseJobProgress')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseJobProgress')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(SBO_GenericFile)
  p_web._CloseFile(TRADEACC)
  p_web._CloseFile(AUDSTATS)
  p_web._CloseFile(WEBJOB)
  p_web._CloseFile(TagFile)
  p_web._CloseFile(JOBSE)
  p_web._CloseFile(JOBS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(SBO_GenericFile)
  Bind(sbogen:Record)
  Clear(sbogen:Record)
  NetWebSetSessionPics(p_web,SBO_GenericFile)
  p_web._OpenFile(TRADEACC)
  Bind(tra:Record)
  NetWebSetSessionPics(p_web,TRADEACC)
  p_web._OpenFile(AUDSTATS)
  Bind(aus:Record)
  NetWebSetSessionPics(p_web,AUDSTATS)
  p_web._OpenFile(WEBJOB)
  Bind(wob:Record)
  NetWebSetSessionPics(p_web,WEBJOB)
  p_web._OpenFile(TagFile)
  Bind(tag:Record)
  NetWebSetSessionPics(p_web,TagFile)
  p_web._OpenFile(JOBSE)
  Bind(jobe:Record)
  NetWebSetSessionPics(p_web,JOBSE)
  p_web._OpenFile(JOBS)
  Bind(job:Record)
  NetWebSetSessionPics(p_web,JOBS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('sbogen:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
    
! ----------------------------------------------------------------------------------------
SendAlert Routine
      ! Save Window Name
      IF (loc:alert <> '')
          AddToLog('Alert',loc:alert,'BrowseJobProgress',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
      END ! IF
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
  loc:eip = 1
  p_web._OpenFile(SBO_GenericFile)
  Case upper(p_web.GetValue('_EIPClm'))
  of upper('tag:tagged')
    do Validate::tag:tagged
  of upper('Despatch')
    do Validate::Despatch
  of upper('CustServ')
    do Validate::CustServ
  of upper('Edit')
    do Validate::Edit
  End
  p_web._CloseFile(SBO_GenericFile)
! ----------------------------------------------------------------------------------------
Validate::tag:tagged  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  sbogen:RecordNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  !#Bryan: Added embed to set extra key components for multi part keys
  loc:result = p_web._GetFile(SBO_GenericFile,sbogen:RecordNumberKey)
  p_web.FileToSessionQueue(SBO_GenericFile)
  loc:was = tag:tagged
  tag:tagged = Choose(p_web.GetValue('value') = 1,1,0)
        Access:TAGFILE.ClearKey(tag:KeyTagged)
        tag:SessionID = p_web.SessionID
        tag:TaggedValue = sbogen:Long1
        IF (Access:TAGFILE.TryFetch(tag:KeyTagged) = Level:Benign)
            tag:Tagged = CHOOSE(p_web.GetValue('value') = 1,TRUE,FALSE)
            Access:TAGFILE.TryUpdate()
        ELSE ! IF
            IF (Access:TAGFILE.PrimeRecord() = Level:Benign)
                tag:SessionID = p_web.SessionID
                tag:TaggedValue = sbogen:Long1
                tag:Tagged = CHOOSE(p_web.GetValue('value') = 1,TRUE,FALSE)
                IF (Access:TAGFILE.TryInsert())
                    Access:TAGFILE.CancelAutoInc()
                END ! IF
            END ! IF
        END ! IF  
  ! Validation goes here, if fails set loc:invalid & loc:alert
  do CheckForDuplicate
  If loc:invalid = '' ! save record
      p_web._updatefile(TagFile)
      p_web.FileToSessionQueue(TagFile)
  End
  do SendAlert
  if loc:alert <> ''
    tag:tagged = loc:was
    do Value::tag:tagged
  End
  ! updating other browse cells goes here.
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::tag:tagged   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If p_web.GetSessionLoggedIn() = 0 then loc:FormOk = 0.
    if false
    else
      packet = clip(packet) & p_web._DivHeader('tag:tagged_'&sbogen:RecordNumber,,net:crc)
      loc:extra = Choose(tag:tagged = 1,'checked','')
      packet = clip(packet) & p_web.CreateInput('checkbox','inp'&p_web.crc32('tag:tagged'&sbogen:RecordNumber),clip(1),,loc:extra & ' ' & loc:disabled,,,'onclick="BrowseJobProgress.eip(this,'''&p_web._jsok('tag:tagged')&''','''&p_web._jsok(loc:viewstate)&''');"',,) & '<13,10>'
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locJobNumber   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If p_web.GetSessionLoggedIn() = 0 then loc:FormOk = 0.
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locJobNumber_'&sbogen:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locJobNumber,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::wob:SubAcountNumber   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If p_web.GetSessionLoggedIn() = 0 then loc:FormOk = 0.
    if false
    else
      packet = clip(packet) & p_web._DivHeader('wob:SubAcountNumber_'&sbogen:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(wob:SubAcountNumber,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locCustomerName   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If p_web.GetSessionLoggedIn() = 0 then loc:FormOk = 0.
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locCustomerName_'&sbogen:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locCustomerName,'@s60')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::wob:IMEINumber   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If p_web.GetSessionLoggedIn() = 0 then loc:FormOk = 0.
    if false
    else
      packet = clip(packet) & p_web._DivHeader('wob:IMEINumber_'&sbogen:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(wob:IMEINumber,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locStatus   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If p_web.GetSessionLoggedIn() = 0 then loc:FormOk = 0.
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locStatus_'&sbogen:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locStatus,'@s35')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::job:Location   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If p_web.GetSessionLoggedIn() = 0 then loc:FormOk = 0.
    if false
    else
      packet = clip(packet) & p_web._DivHeader('job:Location_'&sbogen:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(job:Location,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::locType   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If p_web.GetSessionLoggedIn() = 0 then loc:FormOk = 0.
    if false
    else
      packet = clip(packet) & p_web._DivHeader('locType_'&sbogen:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(locType,'@s1')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::wob:OrderNumber   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If p_web.GetSessionLoggedIn() = 0 then loc:FormOk = 0.
    if false
    else
      packet = clip(packet) & p_web._DivHeader('wob:OrderNumber_'&sbogen:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(wob:OrderNumber,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::wob:MobileNumber   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If p_web.GetSessionLoggedIn() = 0 then loc:FormOk = 0.
    if false
    else
      packet = clip(packet) & p_web._DivHeader('wob:MobileNumber_'&sbogen:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(wob:MobileNumber,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::wob:ModelNumber   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If p_web.GetSessionLoggedIn() = 0 then loc:FormOk = 0.
    if false
    else
      packet = clip(packet) & p_web._DivHeader('wob:ModelNumber_'&sbogen:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(wob:ModelNumber,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::wob:DespatchCourier   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If p_web.GetSessionLoggedIn() = 0 then loc:FormOk = 0.
    if false
    elsif wob:ReadyToDespatch <> 1
      packet = clip(packet) & p_web._DivHeader('wob:DespatchCourier_'&sbogen:RecordNumber,,net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok('',0))
    else
      packet = clip(packet) & p_web._DivHeader('wob:DespatchCourier_'&sbogen:RecordNumber,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(wob:DespatchCourier,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
Validate::Despatch  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  sbogen:RecordNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  !#Bryan: Added embed to set extra key components for multi part keys
  loc:result = p_web._GetFile(SBO_GenericFile,sbogen:RecordNumberKey)
  p_web.FileToSessionQueue(SBO_GenericFile)
  do CheckForDuplicate
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::Despatch   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If p_web.GetSessionLoggedIn() = 0 then loc:FormOk = 0.
    if false
    elsif wob:ReadyToDespatch <> 1
      packet = clip(packet) & p_web._DivHeader('Despatch_'&sbogen:RecordNumber,,net:crc)
      packet = clip(packet) & p_web.CreateHyperLink(p_web._jsok('',0))
    else
      packet = clip(packet) & p_web._DivHeader('Despatch_'&sbogen:RecordNumber,,net:crc)
      If p_web.GetSessionLoggedIn()
      End
      loc:disabled = ''
      packet = clip(packet) & p_web.CreateButton('button','btnDespatch','Despatch','SmallButtonNoWidth',loc:formname,,,'window.open('''& p_web._MakeURL(clip('IndividualDespatch?locJobNumber=' & wob:RefNumber & '&idReturnURL=FormJobProgressCriteria')& '' ) & ''','''&clip('_self')&''')',,loc:disabled,,,,,) & '<13,10>' !3
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
Validate::CustServ  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  sbogen:RecordNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  !#Bryan: Added embed to set extra key components for multi part keys
  loc:result = p_web._GetFile(SBO_GenericFile,sbogen:RecordNumberKey)
  p_web.FileToSessionQueue(SBO_GenericFile)
  do CheckForDuplicate
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::CustServ   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If p_web.GetSessionLoggedIn() = 0 then loc:FormOk = 0.
    if false
    else
      packet = clip(packet) & p_web._DivHeader('CustServ_'&sbogen:RecordNumber,,net:crc)
      If p_web.GetSessionLoggedIn()
      End
      loc:disabled = ''
      If '_self'='_self' and Loc:Disabled = 0
        Loc:Disabled = -1
      End
      ! the button is set as a "submit" button, and this must have a URL
      packet = clip(packet) & p_web.CreateButton('submit','CustServ','Cust Serv','button-flat',loc:formname,p_web._MakeURL(clip('CustomerServicesForm?wob__RecordNumber=' & sbogen:Long2 & '&Change_btn=Change&Amend=0&FirstTime=1&CustServReturnURL=FormJobProgressCriteria')  & '&' & p_web._noColon('sbogen:RecordNumber')&'='& p_web.escape(sbogen:RecordNumber) & '&PressedButton=change_btn'),clip('_self'),,,loc:disabled,,,,,) & '<13,10>' !4
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
Validate::Edit  Routine
  data
loc:was                    Any
loc:result                 Long
  code
  do OpenFilesB
  loc:ViewState = p_web.GetValue('ViewState')
  sbogen:RecordNumber = p_web.Base64Decode(p_web._Unescape(loc:ViewState,Net:NoPlus))
  !#Bryan: Added embed to set extra key components for multi part keys
  loc:result = p_web._GetFile(SBO_GenericFile,sbogen:RecordNumberKey)
  p_web.FileToSessionQueue(SBO_GenericFile)
  do CheckForDuplicate
  do ClosefilesB
! ----------------------------------------------------------------------------------------
value::Edit   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
  If p_web.GetSessionLoggedIn() = 0 then loc:FormOk = 0.
    if false
    else
      packet = clip(packet) & p_web._DivHeader('Edit_'&sbogen:RecordNumber,,net:crc)
      If p_web.GetSessionLoggedIn()
      End
      loc:disabled = ''
      If '_self'='_self' and Loc:Disabled = 0
        Loc:Disabled = -1
      End
      ! the button is set as a "submit" button, and this must have a URL
      packet = clip(packet) & p_web.CreateButton('submit','Edit','Eng Screen','button-flat',loc:formname,p_web._MakeURL(clip('ViewJob?&wob__RecordNumber=' & sbogen:Long2 & '&Change_btn=Change&ViewJobReturnURL=FormJobProgressCriteria')& '&PressedButton=change_btn'),clip('_self'),,,loc:disabled,,,,,) & '<13,10>'  !5
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(AUDSTATS)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(TagFile)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(AUDSTATS)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = sbogen:RecordNumber
    
    p_web.SSV('sbogen:RecordNumber',sbogen:RecordNumber)

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('sbogen:RecordNumber',sbogen:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('sbogen:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('sbogen:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('sbogen:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
FilterAccountNumber PROCEDURE()!,LONG
    CODE
        IF (p_web.GSV('FranchiseAccount') > 0 AND p_web.GSV('locJobProgressAccountNumber') <> '')
            IF (wob:SubAcountNumber <> p_web.GSV('locJobProgressAccountNumber'))
                RETURN Level:Fatal
            END ! IF
        END ! IF
        RETURN Level:Benign
FilterDateBooked    PROCEDURE()!,LONG
    CODE
        RETURN Level:Benign
        IF (INRANGE(wob:DateBooked,p_web.GSV('locJobProgressStartDate'),p_web.GSV('locJobProgressEndDate')))
            RETURN Level:Benign
        ELSE
            RETURN Level:Fatal
        END 

FilterLocation      PROCEDURE()!,LONG
retValue    LONG(Level:Benign)
    CODE
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = wob:RefNumber
        IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
!                CYCLE
!                BHAddToDebugLog('In Loop - No Job')
        END! IF
        CASE p_web.GSV('locJobProgressLocation')
        OF 1 ! At RRC
            IF (job:Location <> 'AT FRANCHISE')
                retValue = Level:Fatal
            END ! IF
        OF 2 ! Not At RRC
            IF (job:Location = 'AT FRANCHISE')
                retValue = Level:Fatal
            END ! IF
        END ! CASE        
        
        RETURN retValue
FilterModelNumber   PROCEDURE()!,LONG
    CODE
        IF (p_web.GSV('locJobProgressModelNumberFilter') = 1 AND p_web.GSV('locJobProgressModelNumber') <> '')
            IF (wob:ModelNumber <> p_web.GSV('locJobProgressModelNumber'))
                RETURN Level:Fatal
            END ! IF
        END ! IF
        RETURN Level:Benign
FilterStatus        PROCEDURE()!,LONG
retValue    LONG(Level:Benign)
    CODE
        IF (p_web.GSV('locJobProgressStatus') <> '' AND p_web.GSV('locJobProgressStatusFilter') > 0)
            CASE p_web.GSV('locJobProgressStatusFilter')
            OF 1 ! Job Status
                IF (wob:Current_Status <> p_web.GSV('locJobProgressStatus'))
                    retValue = Level:Fatal
                END ! IF
            OF 2 ! Exchange Status
                IF (wob:Exchange_Status <> p_web.GSV('locJobProgressStatus'))
                    retValue = Level:Fatal
                END ! IF
            OF 3 ! Loan Status
                IF (wob:Loan_Status <> p_web.GSV('locJobProgressStatus'))
                    retValue = Level:Fatal
                END ! IF
            END ! CASE
        END ! IF
        RETURN retValue
