

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER193.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! Report the JOBS File
!!! </summary>
StatusReport PROCEDURE (NetWebServerWorker p_web)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
tmp:DefaultTelephone STRING(20)                            !
tmp:DefaultFax       STRING(20)                            !
value_temp           REAL                                  !
invoice_job_type_temp STRING(30)                           !
completed_job_type_temp STRING(30)                         !
Date_range_Type_Temp STRING(30)                            !
tmp:PrintedBy        STRING(60)                            !
job_type_temp        STRING(60)                            !
tmp:DespatchType     STRING(60)                            !
tmp:custname         STRING(20)                            !
tmp:OldStatus        STRING(30)                            !
tmp:StatusDate       DATE                                  !
tmp:StatusUser       STRING(3)                             !
tmp:status           STRING(30)                            !
tmp:StatusType       STRING(1)                             !
tmp:DaysInStatus     LONG                                  !
tmp:Loaned           STRING(3)                             !
tmp:DateRange        STRING(60)                            !
tmp:OSDays           LONG                                  !
tmp:Job_Number       STRING(20)                            !
tmp:JobType          STRING(1)                             !
tmp:StatusNo         BYTE                                  !
count_temp           LONG                                  !
DefaultAddress       GROUP,PRE(address)                    !
AddressName          STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
Postcode             STRING(30)                            !
Telephone            STRING(30)                            !
Fax                  STRING(30)                            !
EmailAddress         STRING(255)                           !
                     END                                   !
Process:View         VIEW(JOBS)
                       PROJECT(job:Account_Number)
                       PROJECT(job:ESN)
                       PROJECT(job:Engineer)
                       PROJECT(job:Location)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:date_booked)
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Report JOBS'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

report               REPORT('Status Report'),AT(458,2000,10937,5656),PRE(RPT),PAPER(PAPER:A4),LANDSCAPE,FONT('Arial', |
  10),THOUS
                       HEADER,AT(427,323,9740,1375),USE(?unnamed:2)
                         STRING(@s30),AT(94,52),USE(address:AddressName),FONT(,12,,FONT:bold),TRN
                         STRING(@s30),AT(104,260,3531,198),USE(address:AddressLine1),FONT(,8),TRN
                         STRING('Date Printed:'),AT(7448,583),USE(?RunPrompt),FONT(,8),TRN
                         STRING(@s30),AT(104,365,3531,198),USE(address:AddressLine2),FONT(,8),TRN
                         STRING(@s30),AT(104,469,3531,198),USE(address:AddressLine3),FONT(,8),TRN
                         STRING(@s30),AT(104,573),USE(address:Postcode),FONT(,8),TRN
                         STRING('Printed By:'),AT(7448,750,625,208),USE(?String67),FONT(,8),TRN
                         STRING('Date Range:'),AT(7448,417),USE(?RunPrompt:2),FONT(,8),TRN
                         STRING(@s60),AT(8438,417),USE(tmp:DateRange),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),LEFT, |
  TRN
                         STRING(@s60),AT(8437,750),USE(tmp:PrintedBy),FONT(,8,,FONT:bold),TRN
                         STRING('Tel: '),AT(104,729),USE(?String15),FONT(,9),TRN
                         STRING(@s30),AT(573,729),USE(address:Telephone),FONT(,8),TRN
                         STRING('Fax:'),AT(104,833),USE(?String16),FONT(,9),TRN
                         STRING(@s30),AT(573,833),USE(address:Fax),FONT(,8),TRN
                         STRING(@s255),AT(573,938,3531,198),USE(address:EmailAddress),FONT(,8),TRN
                         STRING('Email:'),AT(104,938),USE(?String16:2),FONT(,9),TRN
                         STRING('Page:'),AT(7448,917),USE(?PagePrompt),FONT(,8),TRN
                         STRING('<<-- Date Stamp -->'),AT(8458,583),USE(?ReportDateStamp),FONT(,8,,FONT:bold),TRN
                         STRING(@N3),AT(8417,917),USE(ReportPageNumber),FONT(,8,,FONT:bold),TRN
                       END
DETAIL                 DETAIL,AT(0,0,,146),USE(?detail)
                         STRING(@s3),AT(10302,0),USE(job:Engineer,,?job:Engineer:2),FONT(,7),TRN
                         STRING(@s1),AT(9688,0),USE(tmp:JobType),FONT('Arial',7,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING(@s4),AT(7917,0),USE(tmp:OSDays),FONT(,7),TRN
                         STRING(@s3),AT(9938,0),USE(tmp:Loaned),FONT(,7),TRN
                         STRING(@s25),AT(8281,0),USE(job:Location,,?job:Location:2),FONT(,7),TRN
                         STRING(@d6b),AT(63,0),USE(job:date_booked),FONT(,7),TRN
                         STRING(@s12),AT(563,0),USE(tmp:Job_Number),FONT(,7),LEFT,TRN
                         STRING(@s12),AT(1406,0),USE(job:Account_Number),FONT(,7),LEFT,TRN
                         STRING(@s15),AT(3385,0,990,156),USE(job:Model_Number),FONT(,7),LEFT,TRN
                         STRING(@s20),AT(2240,0,1042,156),USE(tmp:custname),FONT(,7),LEFT,TRN
                         STRING(@s16),AT(4479,0),USE(job:ESN),FONT(,7),LEFT,TRN
                         STRING(@s30),AT(5625,0),USE(tmp:status),FONT(,7),TRN
                         STRING(@s4),AT(7396,0),USE(tmp:DaysInStatus),FONT(,7),TRN
                         STRING(@s1),AT(5458,0),USE(tmp:StatusType),FONT(,7),TRN
                       END
detail1                DETAIL,AT(0,0,,146),USE(?detail1)
                         STRING(@d6b),AT(63,0),USE(job:date_booked,,?job:date_booked:2),FONT(,7),TRN
                         STRING(@s12),AT(563,0),USE(tmp:Job_Number,,?tmp:Job_Number:2),FONT(,7),LEFT,TRN
                         STRING(@s15),AT(1250,0,729,156),USE(job:Model_Number,,?job:Model_Number:2),FONT(,7),LEFT,TRN
                         STRING(@s30),AT(9167,0),USE(tmp:status,,?tmp:Status:2),FONT(,7),TRN
                         STRING(@s15),AT(3281,0),USE(job:Account_Number,,?job:Account_Number:2),FONT(,7),LEFT,TRN
                         STRING(@s20),AT(4375,0),USE(job:ESN,,?job:ESN:2),FONT(,7),LEFT,TRN
                         STRING(@s3),AT(8854,0),USE(tmp:StatusUser),FONT(,7),TRN
                         STRING(@d6),AT(8229,0),USE(tmp:StatusDate,,?tmp:StatusDate:2),FONT(,7),LEFT,TRN
                         STRING(@s30),AT(6510,0),USE(tmp:OldStatus),FONT(,7),TRN
                         STRING(@s3),AT(6042,0),USE(job:Engineer),FONT(,7),TRN
                         STRING(@s20),AT(2031,0),USE(job:Location),FONT(,7),TRN
                       END
detailTotals           DETAIL,AT(0,0,10937,458),USE(?detailTotal)
                         LINE,AT(198,52,7135,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Total Lines:'),AT(260,156),USE(?String68),FONT(,10,,FONT:bold),TRN
                         STRING(@s9),AT(1198,156),USE(count_temp),FONT(,10,,FONT:bold),TRN
                       END
                       FORM,AT(406,313,10969,7448),USE(?unnamed)
                         IMAGE('Rlistlan.gif'),AT(0,0,10938,7448),USE(?Image1)
                         STRING('STATUS REPORT'),AT(4625,52),USE(?String19),FONT(,14,,FONT:bold),TRN
                         GROUP,AT(52,1333,10729,365),USE(?MainTitle),BOXED,TRN
                           STRING('Booked'),AT(125,1490),USE(?String23),FONT(,7,,FONT:bold),TRN
                           STRING('Job No'),AT(615,1490),USE(?String20),FONT(,7,,FONT:bold),TRN
                           STRING('Account No'),AT(1458,1490),USE(?String21),FONT(,7,,FONT:bold),TRN
                           STRING('Model No'),AT(3437,1490),USE(?String25),FONT(,7,,FONT:bold),TRN
                           STRING('Cust Details'),AT(2292,1490),USE(?String27),FONT(,7,,FONT:bold),TRN
                           STRING('I.M.E.I. No'),AT(4531,1490),USE(?String29),FONT(,7,,FONT:bold),TRN
                           STRING('Days'),AT(7396,1385),USE(?CurrentStatus:3),FONT(,7,,FONT:bold),TRN
                           STRING('Total O/S'),AT(7812,1385),USE(?CurrentStatus:8),FONT(,7,,FONT:bold),TRN
                           STRING('In Status'),AT(7292,1490),USE(?CurrentStatus:7),FONT(,7,,FONT:bold),TRN
                           STRING('Job Location'),AT(8333,1490),USE(?CurrentStatus:4),FONT(,7,,FONT:bold),TRN
                           STRING('Loan'),AT(9990,1490),USE(?CurrentStatus:5),FONT(,7,,FONT:bold),TRN
                           STRING('Eng'),AT(10354,1490),USE(?CurrentStatus:6),FONT(,7,,FONT:bold),TRN
                           STRING('Type'),AT(9698,1490),USE(?CurrentStatus:10),FONT('Arial',7,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                           STRING('Days'),AT(7865,1490),USE(?CurrentStatus:9),FONT(,7,,FONT:bold),TRN
                           STRING('Status'),AT(5677,1479),USE(?CurrentStatus:2),FONT(,7,,FONT:bold),TRN
                         END
                         GROUP,AT(52,1333,10729,365),USE(?StatusTitle),BOXED,TRN
                           STRING('Booked'),AT(125,1490),USE(?String23:2),FONT(,7,,FONT:bold),TRN
                           STRING('Job No'),AT(615,1490),USE(?String20:2),FONT(,7,,FONT:bold),TRN
                           STRING('Model No'),AT(1302,1490),USE(?String25:2),FONT(,7,,FONT:bold),TRN
                           STRING('Location'),AT(2083,1490),USE(?Location),FONT(,7,,FONT:bold),TRN
                           STRING('Previous Status'),AT(6562,1490),USE(?String31:4),FONT(,7,,FONT:bold),TRN
                           STRING('Changed'),AT(8281,1490),USE(?String31:3),FONT(,7,,FONT:bold),TRN
                           STRING('Current Status'),AT(9219,1490),USE(?CurrentStatus),FONT(,7,,FONT:bold),TRN
                           STRING('Account No.'),AT(3333,1490),USE(?Location:3),FONT(,7,,FONT:bold),TRN
                           STRING('I.M.E.I. No.'),AT(4427,1490),USE(?Location:2),FONT(,7,,FONT:bold),TRN
                           STRING('User'),AT(8906,1490),USE(?String31:6),FONT(,7,,FONT:bold),TRN
                           STRING('Eng'),AT(6094,1490),USE(?String31:2),FONT(,7,,FONT:bold),TRN
                         END
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR3               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
EndReport              PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Next                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.EndReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
    PRINT(rpt:detailTotals)
  ReturnValue = PARENT.EndReport()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('StatusReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:AUDSTATS.SetOpenRelated()
  Relate:AUDSTATS.Open                                     ! File AUDSTATS used by this procedure, so make sure it's RelationManager is open
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('StatusReport',ProgressWindow)              ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, RECORDS(qStatusReport))
  ThisReport.AddSortOrder()
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDSTATS.Close
  END
  IF SELF.Opened
    INIMgr.Update('StatusReport',ProgressWindow)           ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'StatusReport',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Next PROCEDURE

ReturnValue          BYTE,AUTO

Progress BYTE,AUTO
  CODE
      ThisReport.RecordsProcessed+=1
      GET(qStatusReport,ThisReport.RecordsProcessed)
      IF ERRORCODE() THEN
         ReturnValue = Level:Notify
      ELSE
         ReturnValue = Level:Benign
      END
      IF ReturnValue = Level:Notify
          IF ThisReport.RecordsProcessed>RECORDS(qStatusReport)
             SELF.Response = RequestCompleted
             POST(EVENT:CloseWindow)
             RETURN Level:Notify
          ELSE
             SELF.Response = RequestCancelled
             POST(EVENT:CloseWindow)
             RETURN Level:Fatal
          END
      ELSE
         Progress = ThisReport.RecordsProcessed / ThisReport.RecordsToProcess*100
         IF Progress > 100 THEN Progress = 100.
         IF Progress <> Progress:Thermometer
           Progress:Thermometer = Progress
           DISPLAY()
         END
      END
      RETURN Level:Benign
  ReturnValue = PARENT.Next()
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('BookingAccount')
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            address:AddressName     = tra:Company_Name
            address:AddressLine1    = tra:Address_Line1
            address:AddressLine2    = tra:Address_Line2
            address:AddressLine3    = tra:Address_Line3
            address:Postcode        = tra:Postcode
            address:Telephone       = tra:Telephone_Number
            address:Fax             = tra:Fax_Number
            address:EmailAddress    = tra:EmailAddress
        END ! IF
        tmp:DateRange = FORMAT(p_web.GSV('locStartDate'),@d06) & ' to ' & FORMAT(p_web.GSV('locEndDate'),@d06)
        
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = p_web.GSV('BookingUserCode')
        IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            tmp:PrintedBy = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
        END ! IF
        
        
        SETTARGET(REPORT)
        ?MainTitle{PROP:Boxed} = 0
        ?StatusTitle{PROP:Boxed} = 0
        
        IF (p_web.GSV('locPaperReportType') = 2)
            ?StatusTitle{PROP:Hide} = 0
            ?MainTitle{PROP:Hide} = 1
        ELSE ! IF
            ?StatusTitle{PROP:Hide} = 1
            ?MainTitle{PROP:Hide} = 0
        END ! IF
        
        CASE p_web.GSV('locStatusType')
        OF 1
            ?CurrentStatus{prop:Text} = 'Job Status'
            ?CurrentStatus:2{PROP:Text} = 'Job Status'
        OF 2
            ?CurrentStatus{prop:Text} = 'Exchange Status'
            ?CurrentStatus:2{PROP:Text} = 'Exchange Status'
        OF 3
            ?CurrentStatus{prop:Text} = 'Loan Status'
            ?CurrentStatus:2{PROP:Text} = 'Loan Status'
        END !  IF
        SETTARGET()
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.Init(report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,report{PROPPRINT:Paper},PAPER:USER), CHOOSE(report{PROP:Thous}=True,PROP:Thous,CHOOSE(report{PROP:MM}=True,PROP:MM,CHOOSE(report{PROP:Points}=True,PROP:Points,0))), report{PROPPRINT:PaperWidth}, report{PROPPRINT:PaperHeight}, report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetTitle('Status Report')            !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR3.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetViewerPrefs(PDFXTR3:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@d06)
  END
  IF ReturnValue = Level:Benign
    report$?ReportPageNumber{PROP:PageNo} = True
  END
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'StatusReport',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR3.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  !region Process
        IF (qStatusReport.SessionID <> p_web.SessionID)
            RETURN Level:User
        END ! IF  
        
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = qStatusReport.JobNumber
        IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
            RETURN Level:User
        END ! IF
        
        tmp:Job_Number = job:Ref_Number!qStatusReport.FullJobNumber
        
        IF (job:Surname = '')
            Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
            sub:Account_Number = job:Account_Number
            IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
                Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                tra:Account_Number = sub:Main_Account_Number
                IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                    IF (tra:Use_Sub_Accounts = 'YES')
                        tmp:custname = sub:Company_Name
                    ELSE ! IF
                        tmp:custname = tra:Company_Name
                    END ! IF
                END ! IF
                
            END ! IF
        ELSE ! IF
            tmp:custname = job:Surname
        END ! IF
        
        tmp:StatusNo = p_web.GSV('locStatusType')
        
        IF (p_web.GSV('locShowCustomerStatus') = 1 AND p_web.GSV('locPaperReportType') = 0)
            IF (job:Exchange_Unit_Number > 0)
                tmp:StatusNo = 1
            END ! IF
        END ! IF
        
        tmp:OSDays = qStatusReport.OSDays
        
        Access:AUDSTATS.ClearKey(aus:DateChangedKey)
        aus:RefNumber  = job:Ref_Number
        
        CASE tmp:StatusNo
        OF 0 ! JOB
            aus:Type = 'JOB'
            
            tmp:Status = job:Current_Status
            tmp:StatusType = 'J'
            tmp:DaysInStatus = qStatusReport.JDaysInStatus
        OF 1 ! EXC
            aus:Type = 'EXC'
            
            tmp:Status = job:Exchange_Status
            tmp:StatusType = 'E'
            tmp:DaysInStatus = qStatusReport.EDaysInStatus
        OF 2 ! JOB
            aus:Type = 'LOA'
            
            tmp:Status = job:Loan_Status
            tmp:StatusType = 'L'
            tmp:DaysInStatus = qStatusReport.LDaysInStatus
        END ! CASE
        
        aus:DateChanged = TODAY()
        SET(aus:DateChangedKey,aus:DateChangedKey)
        LOOP UNTIL Access:AUDSTATS.Previous() <> Level:Benign
            IF (aus:RefNumber <> job:Ref_Number OR |
                aus:DateChanged > TODAY())
                BREAK
            END ! IF
            CASE tmp:StatusNo
            OF 0 ! JOB
                IF (aus:Type <> 'JOB')
                    BREAK
                END ! IF
                IF (aus:NewStatus <> job:Current_Status)
                    CYCLE
                END ! IF
            OF 1 ! EXC
                IF (aus:Type <> 'EXC')
                    BREAK
                END ! IF
                IF (aus:NewStatus <> job:Exchange_Status)
                    CYCLE
                END ! IF
            OF 2 ! LOA
                IF (aus:Type <> 'LOA')
                    BREAK
                END ! IF
                IF (aus:NewStatus <> job:Loan_Status)
                    CYCLE
                END ! IF
            END ! CASE
            tmp:OldStatus = aus:OldStatus
            tmp:StatusDate = aus:DateChanged
            tmp:StatusUser = aus:UserCode
            BREAK
  
        END ! LOOP
        IF (job:Loan_Unit_Number > 0)
            tmp:Loaned = 'YES'
        ELSE ! IF
            tmp:Loaned = 'NO'
        END ! IF
        IF (job:Chargeable_Job = 'YES' AND job:Warranty_Job = 'YES')
            tmp:JobType = 'S'
        ELSE
            IF (job:Chargeable_Job = 'YES')
                tmp:JobType = 'C'
            END ! IF
            IF (job:Warranty_Job = 'YES')   
                tmp:JobType = 'W'
            END ! IF
        END ! IF
        
        count_temp += 1
        
        IF (p_web.GSV('locPaperReportType') = 2)
            PRINT(rpt:Detail1)
        ELSE ! IF
            PRINT(rpt:Detail)
        END ! IF
  !endregion        
        
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR3:rtn = PDFXTR3.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR3:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

