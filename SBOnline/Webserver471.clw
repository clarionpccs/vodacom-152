

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER471.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
CreateTestRecord     PROCEDURE  (String f:Path)            ! Declare Procedure
tmp:TestRecord       STRING(255),STATIC                    !
TestRecord    File,Driver('ASCII'),Pre(test),Name(tmp:TestRecord),Create,Bindable,Thread
Record                  Record
TestLine                String(1)
                        End
                    End

  CODE
    ! Start - Create a small test file in the repository folder - TrkBs: 5110 (DBH: 30-08-2005)
    tmp:TestRecord = Clip(f:Path)

    If Sub(Clip(tmp:TestRecord),-1,1) = '\'
        tmp:TestRecord = Clip(tmp:TestRecord) & Random(1,9999) & Clock() & '.tmp'
    Else ! If Sub(Clip(tmp:TestRecord),-1,1) = '\'
        tmp:TestRecord = Clip(tmp:TestRecord) & '\' & Random(1,9999) & Clock() & '.tmp'
    End ! If Sub(Clip(tmp:TestRecord),-1,1) = '\'

    Remove(TestRecord)
    Create(TestRecord)
    If Error()
        Return False
    End ! If Error()
    Open(TestRecord)
    If Error()
        Return False
    End ! If Error()
    test:TestLine = '1'
    Add(TestRecord)
    If Error()
        Return False
    End ! If Error()
    Close(TestRecord)
    Remove(TestRecord)
    If Error()
        Return False
    End ! If Error()
    ! End   - Create a small test file in the repository folder - TrkBs: 5110 (DBH: 30-08-2005
    Return True
