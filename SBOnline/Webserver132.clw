

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER132.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! JOBS in SessionQ
!!! </summary>
LocationChange       PROCEDURE  (NetWebServerWorker p_web,STRING pNewLocation) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
Previous_Loc         STRING(30)                            !
FilesOpened     BYTE(0)

  CODE
    do openFiles
    IF (pNewLocation <> '')
        if (Access:LOCATLOG.PrimeRecord() = Level:Benign)
            lot:refNumber   = p_web.GSV('job:Ref_Number')
            lot:theDate     = today()
            lot:theTime     = clock()
            lot:userCode    = p_web.GSV('BookingUserCode')
            lot:PreviousLocation    = p_web.GSV('job:Location')
            if (lot:PreviousLocation = '')
                lot:PreviousLocation = 'N/A'
            end !if (lot:PreviousLocation = '')
            lot:NewLocation = pNewLocation
           
            if (Access:LOCATLOG.TryInsert() = Level:Benign)
                ! Inserted
            else ! if (Access:LOCATLOG.TryInsert() = Level:Benign)
                ! Error
                access:LOCATLOG.cancelautoinc()
            end ! if (Access:LOCATLOG.TryInsert() = Level:Benign)
        end ! if (Access:LOCATLOG.PrimeRecord() = Level:Benign)
    end ! if (p_web.GSV('LocationChange:Location') <> '')

    p_web.SSV('job:Location',pNewLocation)

    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:LOCATLOG.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOCATLOG.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:LOCATLOG.Close
     FilesOpened = False
  END
