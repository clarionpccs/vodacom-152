

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER386.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER046.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER048.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER372.INC'),ONCE        !Req'd for module callout resolution
                     END


FormReplaceLoanUnit  PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locNewIMEINumber     STRING(30)                            !
locNewMSN            STRING(30)                            !
locNewModelNumber    STRING(30)                            !
locNewManufacturer   STRING(30)                            !
                    MAP
IMEIModelRoutine        PROCEDURE(STRING pIMEINumber),LONG
ShowMSN                 PROCEDURE()
                    END ! MAP
FilesOpened     Long
LOANHIST::State  USHORT
MANUFACT::State  USHORT
MODELNUM::State  USHORT
LOAN::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('FormReplaceLoanUnit')
  loc:formname = 'FormReplaceLoanUnit_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormReplaceLoanUnit',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormReplaceLoanUnit','')
    p_web._DivHeader('FormReplaceLoanUnit',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormReplaceLoanUnit',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormReplaceLoanUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormReplaceLoanUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormReplaceLoanUnit',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormReplaceLoanUnit',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormReplaceLoanUnit',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormReplaceLoanUnit',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(LOANHIST)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(MODELNUM)
  p_web._OpenFile(LOAN)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(LOANHIST)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(MODELNUM)
  p_Web._CloseFile(LOAN)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormReplaceLoanUnit_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('loa:Ref_Number')
    p_web.SetPicture('loa:Ref_Number','@s8')
  End
  p_web.SetSessionPicture('loa:Ref_Number','@s8')
  If p_web.IfExistsValue('loa:Model_Number')
    p_web.SetPicture('loa:Model_Number','@s30')
  End
  p_web.SetSessionPicture('loa:Model_Number','@s30')
  If p_web.IfExistsValue('loa:ESN')
    p_web.SetPicture('loa:ESN','@s30')
  End
  p_web.SetSessionPicture('loa:ESN','@s30')
  If p_web.IfExistsValue('loa:MSN')
    p_web.SetPicture('loa:MSN','@s30')
  End
  p_web.SetSessionPicture('loa:MSN','@s30')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'locNewManufacturer'
    p_web.setsessionvalue('showtab_FormReplaceLoanUnit',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MANUFACT)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locNewModelNumber')
  Of 'locNewModelNumber'
    p_web.setsessionvalue('showtab_FormReplaceLoanUnit',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODELNUM)
    End
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('loa:Ref_Number',loa:Ref_Number)
  p_web.SetSessionValue('loa:Model_Number',loa:Model_Number)
  p_web.SetSessionValue('loa:ESN',loa:ESN)
  p_web.SetSessionValue('loa:MSN',loa:MSN)
  p_web.SetSessionValue('locNewIMEINumber',locNewIMEINumber)
  p_web.SetSessionValue('locNewMSN',locNewMSN)
  p_web.SetSessionValue('locNewManufacturer',locNewManufacturer)
  p_web.SetSessionValue('locNewModelNumber',locNewModelNumber)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('loa:Ref_Number')
    loa:Ref_Number = p_web.GetValue('loa:Ref_Number')
    p_web.SetSessionValue('loa:Ref_Number',loa:Ref_Number)
  End
  if p_web.IfExistsValue('loa:Model_Number')
    loa:Model_Number = p_web.GetValue('loa:Model_Number')
    p_web.SetSessionValue('loa:Model_Number',loa:Model_Number)
  End
  if p_web.IfExistsValue('loa:ESN')
    loa:ESN = p_web.GetValue('loa:ESN')
    p_web.SetSessionValue('loa:ESN',loa:ESN)
  End
  if p_web.IfExistsValue('loa:MSN')
    loa:MSN = p_web.GetValue('loa:MSN')
    p_web.SetSessionValue('loa:MSN',loa:MSN)
  End
  if p_web.IfExistsValue('locNewIMEINumber')
    locNewIMEINumber = p_web.GetValue('locNewIMEINumber')
    p_web.SetSessionValue('locNewIMEINumber',locNewIMEINumber)
  End
  if p_web.IfExistsValue('locNewMSN')
    locNewMSN = p_web.GetValue('locNewMSN')
    p_web.SetSessionValue('locNewMSN',locNewMSN)
  End
  if p_web.IfExistsValue('locNewManufacturer')
    locNewManufacturer = p_web.GetValue('locNewManufacturer')
    p_web.SetSessionValue('locNewManufacturer',locNewManufacturer)
  End
  if p_web.IfExistsValue('locNewModelNumber')
    locNewModelNumber = p_web.GetValue('locNewModelNumber')
    p_web.SetSessionValue('locNewModelNumber',locNewModelNumber)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormReplaceLoanUnit_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    p_web.SSV('Filter:Manufacturer','')
    p_web.SSV('Filter:ModelNumber','')
    IF (p_web.IfExistsValue('loa:Ref_Number'))
        Access:LOAN.ClearKey(loa:Ref_Number_Key)
        loa:Ref_Number = p_web.GetValue('loa:Ref_Number')
        IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
            p_web.FileToSessionQueue(LOAN)
        ELSE
            CreateScript(p_web,packet,'alert("Unable to find selected Loan Unit.")')
            DO SendPacket
            EXIT
        END ! IF
        p_web.SSV('locNewIMEINumber','')
        p_web.SSV('locNewMSN','')
        p_web.SSV('locNewModelNumber','')
        p_web.SSV('locNewManufacturer','')
        p_web.SSV('Hide:MSN',1)
    ELSE
        IF (p_web.GSV('locNewIMEINumber') <> '')
            IF (IMEIModelRoutine(p_web.GSV('locNewIMEINumber')) = 0)
                ! Add selected Manufacturer to the filter
                IF (p_web.GSV('locNewManufacturer') <> '')
                    p_web.SSV('Filter:ModelNumber',p_web.GSV('Filter:ModelNumber') & ' AND UPPER(mod:Manufacturer) = UPPER(''' & p_web.GSV('locNewManufacturer') & ''')')
                END ! IF
            END !IF
        END ! IF        
        
        ShowMSN()
    END ! IF
    
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locNewIMEINumber = p_web.RestoreValue('locNewIMEINumber')
 locNewMSN = p_web.RestoreValue('locNewMSN')
 locNewManufacturer = p_web.RestoreValue('locNewManufacturer')
 locNewModelNumber = p_web.RestoreValue('locNewModelNumber')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'FormAllLoanUnits'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormReplaceLoanUnit_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormReplaceLoanUnit_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormReplaceLoanUnit_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'FormAllLoanUnits'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormReplaceLoanUnit" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormReplaceLoanUnit" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormReplaceLoanUnit" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Replace Loan Unit') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Replace Loan Unit',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormReplaceLoanUnit">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormReplaceLoanUnit" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormReplaceLoanUnit')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Replace Loan Unit') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormReplaceLoanUnit')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormReplaceLoanUnit'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='MANUFACT'
            p_web.SetValue('SelectField',clip(loc:formname) & '.locNewModelNumber')
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locNewIMEINumber')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormReplaceLoanUnit')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Replace Loan Unit') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormReplaceLoanUnit_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Replace Loan Unit')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Replace Loan Unit')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Replace Loan Unit')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Replace Loan Unit')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::loa:Ref_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::loa:Ref_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::loa:Ref_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::loa:Model_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::loa:Model_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::loa:Model_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::loa:ESN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::loa:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::loa:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('MSN') <> '' AND p_web.GSV('loa:MSN') <> 'N/A'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::loa:MSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::loa:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::loa:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::_line
      do Comment::_line
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locNewIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locNewIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locNewIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locNewMSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locNewMSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locNewMSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locNewManufacturer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locNewManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locNewManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locNewModelNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locNewModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locNewModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::loa:Ref_Number  Routine
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('loa:Ref_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Loan Unit Number:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::loa:Ref_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('loa:Ref_Number',p_web.GetValue('NewValue'))
    loa:Ref_Number = p_web.GetValue('NewValue') !FieldType= LONG Field = loa:Ref_Number
    do Value::loa:Ref_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('loa:Ref_Number',p_web.dFormat(p_web.GetValue('Value'),'@s8'))
    loa:Ref_Number = p_web.GetValue('Value')
  End

Value::loa:Ref_Number  Routine
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('loa:Ref_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- loa:Ref_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('loa:Ref_Number'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::loa:Ref_Number  Routine
    loc:comment = ''
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('loa:Ref_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::loa:Model_Number  Routine
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('loa:Model_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Model Number:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::loa:Model_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('loa:Model_Number',p_web.GetValue('NewValue'))
    loa:Model_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = loa:Model_Number
    do Value::loa:Model_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('loa:Model_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    loa:Model_Number = p_web.GetValue('Value')
  End

Value::loa:Model_Number  Routine
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('loa:Model_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- loa:Model_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('loa:Model_Number'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::loa:Model_Number  Routine
    loc:comment = ''
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('loa:Model_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::loa:ESN  Routine
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('loa:ESN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('I.M.E.I. Number:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::loa:ESN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('loa:ESN',p_web.GetValue('NewValue'))
    loa:ESN = p_web.GetValue('NewValue') !FieldType= STRING Field = loa:ESN
    do Value::loa:ESN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('loa:ESN',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    loa:ESN = p_web.GetValue('Value')
  End

Value::loa:ESN  Routine
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('loa:ESN') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- loa:ESN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('loa:ESN'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::loa:ESN  Routine
    loc:comment = ''
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('loa:ESN') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::loa:MSN  Routine
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('loa:MSN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('M.S.N.:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::loa:MSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('loa:MSN',p_web.GetValue('NewValue'))
    loa:MSN = p_web.GetValue('NewValue') !FieldType= STRING Field = loa:MSN
    do Value::loa:MSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('loa:MSN',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    loa:MSN = p_web.GetValue('Value')
  End

Value::loa:MSN  Routine
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('loa:MSN') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- loa:MSN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('loa:MSN'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::loa:MSN  Routine
    loc:comment = ''
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('loa:MSN') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::_line  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('_line',p_web.GetValue('NewValue'))
    do Value::_line
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::_line  Routine
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('_line') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::_line  Routine
    loc:comment = ''
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('_line') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locNewIMEINumber  Routine
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('locNewIMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('New I.M.E.I. No')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locNewIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locNewIMEINumber',p_web.GetValue('NewValue'))
    locNewIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locNewIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locNewIMEINumber',p_web.GetValue('Value'))
    locNewIMEINumber = p_web.GetValue('Value')
  End
  If locNewIMEINumber = ''
    loc:Invalid = 'locNewIMEINumber'
    loc:alert = p_web.translate('New I.M.E.I. No') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locNewIMEINumber = Upper(locNewIMEINumber)
    p_web.SetSessionValue('locNewIMEINumber',locNewIMEINumber)
    IF (IMEIModelRoutine(p_web.GSV('locNewIMEINumber')))
        loc:Alert = 'The selected IMEI does not match an Model / Manufacturers'
        loc:invalid = 'locNewIMEINumber'
        p_web.SSV('locNewManufacturer','')
        p_web.SSV('locNewModelNumber','')
    ELSE
         ShowMSN()
    END ! IF
  do Value::locNewIMEINumber
  do SendAlert
  do Value::locNewManufacturer  !1
  do Value::locNewModelNumber  !1
  do Prompt::locNewMSN
  do Value::locNewMSN  !1

Value::locNewIMEINumber  Routine
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('locNewIMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locNewIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locNewIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locNewIMEINumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locNewIMEINumber'',''formreplaceloanunit_locnewimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locNewIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locNewIMEINumber',p_web.GetSessionValueFormat('locNewIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormReplaceLoanUnit_' & p_web._nocolon('locNewIMEINumber') & '_value')

Comment::locNewIMEINumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('locNewIMEINumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locNewMSN  Routine
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('locNewMSN') & '_prompt',Choose(p_web.GSV('Hide:MSN') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('M.S.N.')
  If p_web.GSV('Hide:MSN') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormReplaceLoanUnit_' & p_web._nocolon('locNewMSN') & '_prompt')

Validate::locNewMSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locNewMSN',p_web.GetValue('NewValue'))
    locNewMSN = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locNewMSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locNewMSN',p_web.GetValue('Value'))
    locNewMSN = p_web.GetValue('Value')
  End
  If locNewMSN = ''
    loc:Invalid = 'locNewMSN'
    loc:alert = p_web.translate('M.S.N.') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locNewMSN = Upper(locNewMSN)
    p_web.SetSessionValue('locNewMSN',locNewMSN)
  do Value::locNewMSN
  do SendAlert

Value::locNewMSN  Routine
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('locNewMSN') & '_value',Choose(p_web.GSV('Hide:MSN') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:MSN') = 1)
  ! --- STRING --- locNewMSN
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locNewMSN')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locNewMSN = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locNewMSN'',''formreplaceloanunit_locnewmsn_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locNewMSN',p_web.GetSessionValueFormat('locNewMSN'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormReplaceLoanUnit_' & p_web._nocolon('locNewMSN') & '_value')

Comment::locNewMSN  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('locNewMSN') & '_comment',Choose(p_web.GSV('Hide:MSN') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:MSN') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locNewManufacturer  Routine
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('locNewManufacturer') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Manufacturer')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locNewManufacturer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locNewManufacturer',p_web.GetValue('NewValue'))
    locNewManufacturer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locNewManufacturer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locNewManufacturer',p_web.GetValue('Value'))
    locNewManufacturer = p_web.GetValue('Value')
  End
  If locNewManufacturer = ''
    loc:Invalid = 'locNewManufacturer'
    loc:alert = p_web.translate('Manufacturer') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  p_Web.SetValue('lookupfield','locNewManufacturer')
  do AfterLookup
  do Value::locNewManufacturer
  do SendAlert
  do Comment::locNewManufacturer

Value::locNewManufacturer  Routine
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('locNewManufacturer') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locNewManufacturer
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locNewManufacturer')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locNewManufacturer = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locNewManufacturer'',''formreplaceloanunit_locnewmanufacturer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locNewManufacturer')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','locNewManufacturer',p_web.GetSessionValueFormat('locNewManufacturer'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('SelectManufacturers?LookupField=locNewManufacturer&Tab=0&ForeignField=man:Manufacturer&_sort=&Refresh=sort&LookupFrom=FormReplaceLoanUnit&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormReplaceLoanUnit_' & p_web._nocolon('locNewManufacturer') & '_value')

Comment::locNewManufacturer  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('locNewManufacturer') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormReplaceLoanUnit_' & p_web._nocolon('locNewManufacturer') & '_comment')

Prompt::locNewModelNumber  Routine
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('locNewModelNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Model Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locNewModelNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locNewModelNumber',p_web.GetValue('NewValue'))
    locNewModelNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locNewModelNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locNewModelNumber',p_web.GetValue('Value'))
    locNewModelNumber = p_web.GetValue('Value')
  End
  If locNewModelNumber = ''
    loc:Invalid = 'locNewModelNumber'
    loc:alert = p_web.translate('Model Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locNewModelNumber = Upper(locNewModelNumber)
    p_web.SetSessionValue('locNewModelNumber',locNewModelNumber)
  p_Web.SetValue('lookupfield','locNewModelNumber')
  do AfterLookup
  do Value::locNewModelNumber
  do SendAlert
  do Comment::locNewModelNumber

Value::locNewModelNumber  Routine
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('locNewModelNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locNewModelNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locNewModelNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locNewModelNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locNewModelNumber'',''formreplaceloanunit_locnewmodelnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locNewModelNumber')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','locNewModelNumber',p_web.GetSessionValueFormat('locNewModelNumber'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('SelectModelNumbers?LookupField=locNewModelNumber&Tab=0&ForeignField=mod:Model_Number&_sort=&Refresh=sort&LookupFrom=FormReplaceLoanUnit&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormReplaceLoanUnit_' & p_web._nocolon('locNewModelNumber') & '_value')

Comment::locNewModelNumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormReplaceLoanUnit_' & p_web._nocolon('locNewModelNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormReplaceLoanUnit_' & p_web._nocolon('locNewModelNumber') & '_comment')

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormReplaceLoanUnit_locNewIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locNewIMEINumber
      else
        do Value::locNewIMEINumber
      end
  of lower('FormReplaceLoanUnit_locNewMSN_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locNewMSN
      else
        do Value::locNewMSN
      end
  of lower('FormReplaceLoanUnit_locNewManufacturer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locNewManufacturer
      else
        do Value::locNewManufacturer
      end
  of lower('FormReplaceLoanUnit_locNewModelNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locNewModelNumber
      else
        do Value::locNewModelNumber
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormReplaceLoanUnit_form:ready_',1)
  p_web.SetSessionValue('FormReplaceLoanUnit_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormReplaceLoanUnit',0)

PreCopy  Routine
  p_web.SetValue('FormReplaceLoanUnit_form:ready_',1)
  p_web.SetSessionValue('FormReplaceLoanUnit_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormReplaceLoanUnit',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormReplaceLoanUnit_form:ready_',1)
  p_web.SetSessionValue('FormReplaceLoanUnit_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormReplaceLoanUnit:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormReplaceLoanUnit_form:ready_',1)
  p_web.SetSessionValue('FormReplaceLoanUnit_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormReplaceLoanUnit:Primed',0)
  p_web.setsessionvalue('showtab_FormReplaceLoanUnit',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormReplaceLoanUnit_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormReplaceLoanUnit_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
        If locNewIMEINumber = ''
          loc:Invalid = 'locNewIMEINumber'
          loc:alert = p_web.translate('New I.M.E.I. No') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locNewIMEINumber = Upper(locNewIMEINumber)
          p_web.SetSessionValue('locNewIMEINumber',locNewIMEINumber)
        If loc:Invalid <> '' then exit.
      If not (p_web.GSV('Hide:MSN') = 1)
        If locNewMSN = ''
          loc:Invalid = 'locNewMSN'
          loc:alert = p_web.translate('M.S.N.') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locNewMSN = Upper(locNewMSN)
          p_web.SetSessionValue('locNewMSN',locNewMSN)
        If loc:Invalid <> '' then exit.
      End
        If locNewManufacturer = ''
          loc:Invalid = 'locNewManufacturer'
          loc:alert = p_web.translate('Manufacturer') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
        If locNewModelNumber = ''
          loc:Invalid = 'locNewModelNumber'
          loc:alert = p_web.translate('Model Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locNewModelNumber = Upper(locNewModelNumber)
          p_web.SetSessionValue('locNewModelNumber',locNewModelNumber)
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
    foundLoan# = 0
    Access:LOAN.ClearKey(loa:ESN_Only_Key)
    loa:ESN = p_web.GSV('locNewIMEINumber')
    SET(loa:ESN_Only_Key,loa:ESN_Only_Key)
    LOOP UNTIL Access:LOAN.Next() <> Level:Benign
        IF (loa:ESN <> p_web.GSV('locNewIMEINumber'))
            BREAK
        END ! IF
        foundLoan# = 1
        BREAK
    END ! LOOP
  
    IF (foundLoan# = 1)
        loc:Alert = 'This I.M.E.I. Number already exists In Loan Stock'
        loc:Invalid = 'locNewIMEINumber'
        EXIT
    END ! IF
  
    Access:LOAN.ClearKey(loa:Ref_Number_Key)
    loa:Ref_Number = p_web.GSV('loa:Ref_Number')
    IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
        loa:ESN = p_web.GSV('locNewIMEINumber')
        loa:MSN = p_web.GSV('locNewMSN')
        loa:Model_Number = p_web.GSV('locNewModelNumber')
        IF (Access:LOAN.TryUpdate() = Level:Benign)
            IF (Access:LOANHIST.PrimeRecord() = Level:Benign)
                loh:Ref_Number = loa:Ref_Number
                loh:Date = TODAY()
                loh:Time = CLOCK()
                loh:User = p_web.GSV('BookingUserCode')
                loh:Status = 'REPLACE LOAN: PREV - ' & p_web.GSV('loa:ESN')
                IF (Access:LOANHIST.TryInsert())
                    Access:LOANHIST.CancelAutoInc()
                END ! IF
            END ! IF
        END ! IF
    ELSE ! IF
    END ! IF
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormReplaceLoanUnit:Primed',0)
  p_web.StoreValue('loa:Ref_Number')
  p_web.StoreValue('loa:Model_Number')
  p_web.StoreValue('loa:ESN')
  p_web.StoreValue('loa:MSN')
  p_web.StoreValue('')
  p_web.StoreValue('locNewIMEINumber')
  p_web.StoreValue('locNewMSN')
  p_web.StoreValue('locNewManufacturer')
  p_web.StoreValue('locNewModelNumber')
IMEIModelRoutine	PROCEDURE(STRING pIMEINumber)!,LONG   
qManufacturer       QUEUE(),PRE(qManufacturer)
Manufacturer            STRING(30)
                    END ! QUEUE
qModels                 QUEUE(),PRE(qModels)
Manufacturer                STRING(30)
ModelNumber             STRING(30)
                    END!  QUEUE
i   LONG
    CODE
	
        Free(glo:Q_ModelNumber)
        Clear(glo:Q_ModelNumber)

        If pIMEINumber <> 'N/A' And pIMEINumber <> ''
            ! The model number hasn't been filled yet (DBH: 13/10/2006)
                Access:ESNMODEL.Clearkey(esn:ESN_Only_Key)
                esn:ESN    = Sub(pIMEINumber,1,6)
                Set(esn:ESN_Only_Key,esn:ESN_Only_Key)
                Loop ! Begin Loop
                    If Access:ESNMODEL.Next()
                        Break
                    End ! If Access:ESNMODEL.Next()
                    If esn:ESN <> Sub(pIMEINumber,1,6)
                        Break
                    End ! If esn:ESN <> Sub(pMEI,1,6)
                !added by Paul 08/06/2010 - Log no 11466
                    If esn:Active <> 'Y' then
                        cycle
                    End
                !check to see if the model number selected is active
                    Access:ModelNum.clearkey(mod:Model_Number_Key)
                    mod:Model_Number = esn:Model_Number
                    If Access:ModelNum.fetch(mod:Model_Number_Key) = level:benign then
                        If mod:Active <> 'Y' then
                            cycle
                        End
                    End
                !End Addition
                !tb12488 Disable Manufacturers - is this manufacturer inactive
                    Access:Manufact.clearkey(man:Manufacturer_Key)
                    man:Manufacturer = mod:Manufacturer
                    if access:manufact.fetch(man:Manufacturer_Key) = level:Benign then
                        !if man:Notes[1:8] = 'INACTIVE' then
                        IF (man:Inactive = 1)
                            cycle
                        END !if inactive
                    END !if manufact.fetch

                    qManufacturer.Manufacturer = esn:Manufacturer
                    GET(qManufacturer,qManufacturer.Manufacturer)
                    IF (ERROR())
                        qManufacturer.Manufacturer = esn:Manufacturer
                        ADD(qManufacturer)
                    END ! IF
                    qModels.ModelNumber = esn:Model_Number
                    qModels.Manufacturer = esn:Manufacturer
                    ADD(qModels)
                End ! Loop
            ! Find any TAC codes that match the first 6 digits of the IMEI (DBH: 13/10/2006)

                Access:ESNMODEL.Clearkey(esn:ESN_Only_Key)
                esn:ESN = Sub(pIMEINumber,1,8)
                Set(esn:ESN_Only_Key,esn:ESN_Only_Key)
                Loop ! Begin Loop
                    If Access:ESNMODEL.Next()
                        Break
                    End ! If Access:ESNMODEL.Next()
                    If esn:ESN <> Sub(pIMEINumber,1,8)
                        Break
                    End ! If esn:ESN <> Sub(pIMEINumber,1,8)
                !added by Paul 08/06/2010 - Log no 11466
                    If esn:Active <> 'Y' then
                        cycle
                    End
                    Access:ModelNum.clearkey(mod:Model_Number_Key)
                    mod:Model_Number = esn:Model_Number
                    If Access:ModelNum.fetch(mod:Model_Number_Key) = level:benign then
                        If mod:Active <> 'Y' then
                            cycle
                        End
                    End
                !End Addition

                !tb12488 Disable Manufacturers - is this manufacturer inactive
                    Access:Manufact.clearkey(man:Manufacturer_Key)
                    man:Manufacturer = mod:Manufacturer
                    if access:manufact.fetch(man:Manufacturer_Key) = level:Benign then
                        if man:Notes[1:8] = 'INACTIVE' then
                            cycle
                        END !if inactive
                    END !if manufact.fetch
                !END 12488
				
                    qManufacturer.Manufacturer = esn:Manufacturer
                    GET(qManufacturer,qManufacturer.Manufacturer)
                    IF (ERROR())
                        qManufacturer.Manufacturer = esn:Manufacturer
                        ADD(qManufacturer)
                    END ! IF
				
                    qModels.ModelNumber = esn:Model_Number
                    GET(qModels,qModels.ModelNumber)
                    IF (ERROR())
                        qModels.ModelNumber = esn:Model_Number
                        qModels.Manufacturer = esn:Manufacturer
                        ADD(qModels)
                    END ! IF
                End ! Loop

            ! Find any TAC Codes that match the first 8 digits of the IMEI (DBH: 13/10/2006)


                Case Records(qModels)
                Of 0
                ! No TAC Codes have been found. So cannot return a Model Number (DBH: 13/10/2006)
                    RETURN Level:Fatal
                Of 1
                ! Only 1 TAC Code was found. Return that Model Number (DBH: 13/10/2006)
                    GET(qModels,1)
                    p_web.SSV('locNewManufacturer',qModels.Manufacturer)
                    p_web.SSV('locNewModelNumber',qModels.ModelNumber)
                Else
                ! Call browse showing the queue values.
                    LOOP i = 1 TO RECORDS(qModels)
                        GET(qModels,i)
                        p_web.SSV('Filter:ModelNumber',p_web.GSV('Filter:ModelNumber') & ' OR Upper(mod:Model_Number) = Upper(''' & Clip(qModels.ModelNumber) & ''')')
                    END ! LOOP
                    p_web.SSV('Filter:ModelNumber',SUB(p_web.GSV('Filter:ModelNumber'),5,1000))
				
                    IF (RECORDS(qManufacturer) = 1)
                        GET(qManufacturer,1)
                        p_web.SSV('Filter:Manufacturer','UPPER(man:Manufacturer) = UPPER(''' & qManufacturer.Manufacturer & ''')')
                    ELSE ! IF
                        LOOP i = 1 TO RECORDS(qManufacturer)
                            GET(qManufacturer,i)
                            p_web.SSV('Filter:Manufacturer',p_web.GSV('Filter:Manufacturer') & ' OR Upper(man:Manufacturer) = Upper(<39>' & Clip(qManufacturer.Manufacturer) & '<39>)')
                        END ! LOOP
                        p_web.SSV('Filter:Manufacturer',SUB(p_web.GSV('Filter:Manufacturer'),5,1000))
                    END ! IF

                End ! Case Records(ModelQUeue)
        
        End ! If pIMEINumber <> 'N/A' And pIMEINumber <> ''
        RETURN Level:Benign
ShowMSN		PROCEDURE()
	CODE
		p_web.SSV('Hide:MSN',1)
		Access:MANUFACT.ClearKey(man:Manufacturer_Key)
		man:Manufacturer = p_web.GSV('locNewManufacturer')
		IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
			IF (man:Use_MSN = 'YES')
				p_web.SSV('Hide:MSN',0)
			END ! IF
		END ! IF
