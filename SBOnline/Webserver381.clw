

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER381.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER010.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER382.INC'),ONCE        !Req'd for module callout resolution
                     END


FormReturnExchangeLoanProcess PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locReturnType        STRING(30)                            !
locIMEINumber        STRING(30)                            !
                    MAP
ProcessExchangeIMEI     PROCEDURE()
                    END ! MAP
FilesOpened     Long
LOAN::State  USHORT
EXCHHIST::State  USHORT
SBO_GenericFile::State  USHORT
EXCHANGE::State  USHORT
RETTYPES::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
locReturnType_OptionView   View(RETTYPES)
                          Project(rtt:Description)
                        End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormReturnExchangeLoanProcess')
  loc:formname = 'FormReturnExchangeLoanProcess_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormReturnExchangeLoanProcess',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormReturnExchangeLoanProcess','')
    p_web._DivHeader('FormReturnExchangeLoanProcess',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormReturnExchangeLoanProcess',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormReturnExchangeLoanProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormReturnExchangeLoanProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormReturnExchangeLoanProcess',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormReturnExchangeLoanProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormReturnExchangeLoanProcess',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormReturnExchangeLoanProcess',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(LOAN)
  p_web._OpenFile(EXCHHIST)
  p_web._OpenFile(SBO_GenericFile)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(RETTYPES)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(SBO_GenericFile)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(RETTYPES)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormReturnExchangeLoanProcess_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('OrderType') = 'E'
    loc:TabNumber += 1
  End
  If p_web.GSV('OrderType') = 'L'
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locReturnType',locReturnType)
  p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  p_web.SetSessionValue('locIMEINumber',locIMEINumber)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locReturnType')
    locReturnType = p_web.GetValue('locReturnType')
    p_web.SetSessionValue('locReturnType',locReturnType)
  End
  if p_web.IfExistsValue('locIMEINumber')
    locIMEINumber = p_web.GetValue('locIMEINumber')
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  End
  if p_web.IfExistsValue('locIMEINumber')
    locIMEINumber = p_web.GetValue('locIMEINumber')
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormReturnExchangeLoanProcess_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    IF (p_web.IfExistsValue('OrderType'))
        p_web.StoreValue('OrderType')
    END ! IF
    
    CASE p_web.GSV('OrderType')
    OF 'E'
        p_web.SSV('ReturnURL','FormAllExchangeUnits')
        p_web.SSV('FinalURL','simpleQuestion(''Return Units'',''Are you sure you want to return the selected units?'',''PageProcess?ProcessType=ReturnExchangeUnits&RedirectURL=FormAllExchangeUnits&ReturnURL=FormReturnExchangeLoanProcess'',''#'')')
    OF 'L'
        p_web.SSV('ReturnURL','FormAllLoanUnits')	
        p_web.SSV('FinalURL','simpleQuestion(''Return Units'',''Are you sure you want to return the selected units?'',''PageProcess?ProcessType=ReturnLoanUnits&RedirectURL=FormAllLoanUnits&ReturnURL=FormReturnExchangeLoanProcess'',''#'')')
    END ! CASE	  
    
    IF (p_web.GetValue('FirstTime') = 1)
        ClearSBOGenericFile(p_web)
        p_web.SSV('locIMEINumber','')
    END ! IF
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locReturnType = p_web.RestoreValue('locReturnType')
 locIMEINumber = p_web.RestoreValue('locIMEINumber')
 locIMEINumber = p_web.RestoreValue('locIMEINumber')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormReturnExchangeLoanProcess')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormReturnExchangeLoanProcess_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormReturnExchangeLoanProcess_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormReturnExchangeLoanProcess_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ReturnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormReturnExchangeLoanProcess" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormReturnExchangeLoanProcess" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormReturnExchangeLoanProcess" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Return Stock Process') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Return Stock Process',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormReturnExchangeLoanProcess">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormReturnExchangeLoanProcess" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormReturnExchangeLoanProcess')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GSV('OrderType') = 'E'
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Return Exchange Units') & ''''
        End
        If p_web.GSV('OrderType') = 'L'
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Return Loan Units') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('List Of IMEIs To Return') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormReturnExchangeLoanProcess')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormReturnExchangeLoanProcess'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormReturnExchangeLoanProcess_BrowseReturnExchangeLoan_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('OrderType') = 'E'
        p_web.SetValue('SelectField',clip(loc:formname) & '.locReturnType')
    ElsIf p_web.GSV('OrderType') = 'L'
        p_web.SetValue('SelectField',clip(loc:formname) & '.locIMEINumber')
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GSV('OrderType') = 'E'
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          End
          If p_web.GSV('OrderType') = 'L'
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormReturnExchangeLoanProcess')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GSV('OrderType') = 'E'
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    end
    if p_web.GSV('OrderType') = 'L'
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GSV('OrderType') = 'E'
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Return Exchange Units') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormReturnExchangeLoanProcess_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Return Exchange Units')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Return Exchange Units')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Return Exchange Units')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Return Exchange Units')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&220&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locReturnType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locReturnType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locReturnType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&220&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
  If p_web.GSV('OrderType') = 'L'
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Return Loan Units') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormReturnExchangeLoanProcess_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Return Loan Units')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Return Loan Units')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Return Loan Units')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Return Loan Units')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&220&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIMEINumberLoan
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIMEINumberLoan
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locIMEINumberLoan
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('List Of IMEIs To Return') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormReturnExchangeLoanProcess_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('List Of IMEIs To Return')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('List Of IMEIs To Return')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('List Of IMEIs To Return')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('List Of IMEIs To Return')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&220&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwUnits
      do Comment::brwUnits
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&220&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnReturnUnits
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnReturnUnits
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locReturnType  Routine
  p_web._DivHeader('FormReturnExchangeLoanProcess_' & p_web._nocolon('locReturnType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Return Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locReturnType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locReturnType',p_web.GetValue('NewValue'))
    locReturnType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locReturnType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locReturnType',p_web.GetValue('Value'))
    locReturnType = p_web.GetValue('Value')
  End
  If locReturnType = ''
    loc:Invalid = 'locReturnType'
    loc:alert = p_web.translate('Return Type') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locReturnType = Upper(locReturnType)
    p_web.SetSessionValue('locReturnType',locReturnType)
  do Value::locReturnType
  do SendAlert

Value::locReturnType  Routine
  p_web._DivHeader('FormReturnExchangeLoanProcess_' & p_web._nocolon('locReturnType') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locReturnType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locReturnType = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locReturnType'',''formreturnexchangeloanprocess_locreturntype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locReturnType',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locReturnType') = 0
    p_web.SetSessionValue('locReturnType','')
  end
    packet = clip(packet) & p_web.CreateOption('','',choose('' = p_web.getsessionvalue('locReturnType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(LOAN)
  bind(loa:Record)
  p_web._OpenFile(EXCHHIST)
  bind(exh:Record)
  p_web._OpenFile(SBO_GenericFile)
  bind(sbogen:Record)
  p_web._OpenFile(EXCHANGE)
  bind(xch:Record)
  p_web._OpenFile(RETTYPES)
  bind(rtt:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locReturnType_OptionView)
  locReturnType_OptionView{prop:filter} = 'rtt:Active = 1'
  locReturnType_OptionView{prop:order} = 'UPPER(rtt:Description)'
  Set(locReturnType_OptionView)
  Loop
    Next(locReturnType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locReturnType') = 0
      p_web.SetSessionValue('locReturnType',rtt:Description)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,rtt:Description,choose(rtt:Description = p_web.getsessionvalue('locReturnType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locReturnType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(SBO_GenericFile)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(RETTYPES)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormReturnExchangeLoanProcess_' & p_web._nocolon('locReturnType') & '_value')

Comment::locReturnType  Routine
    loc:comment = ''
  p_web._DivHeader('FormReturnExchangeLoanProcess_' & p_web._nocolon('locReturnType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locIMEINumber  Routine
  p_web._DivHeader('FormReturnExchangeLoanProcess_' & p_web._nocolon('locIMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('I.M.E.I. No To Return')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('NewValue'))
    locIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('Value'))
    locIMEINumber = p_web.GetValue('Value')
  End
  If locIMEINumber = ''
    loc:Invalid = 'locIMEINumber'
    loc:alert = p_web.translate('I.M.E.I. No To Return') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locIMEINumber = Upper(locIMEINumber)
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
    ProcessExchangeIMEI()  
  do Value::locIMEINumber
  do SendAlert
  do Value::brwUnits  !1
  do Value::locReturnType  !1

Value::locIMEINumber  Routine
  p_web._DivHeader('FormReturnExchangeLoanProcess_' & p_web._nocolon('locIMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locIMEINumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locIMEINumber'',''formreturnexchangeloanprocess_locimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locIMEINumber',p_web.GetSessionValueFormat('locIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormReturnExchangeLoanProcess_' & p_web._nocolon('locIMEINumber') & '_value')

Comment::locIMEINumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormReturnExchangeLoanProcess_' & p_web._nocolon('locIMEINumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locIMEINumberLoan  Routine
  p_web._DivHeader('FormReturnExchangeLoanProcess_' & p_web._nocolon('locIMEINumberLoan') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('I.M.E.I. Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIMEINumberLoan  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIMEINumberLoan',p_web.GetValue('NewValue'))
    locIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIMEINumberLoan
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('Value'))
    locIMEINumber = p_web.GetValue('Value')
  End
  If locIMEINumber = ''
    loc:Invalid = 'locIMEINumber'
    loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locIMEINumber = Upper(locIMEINumber)
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
    ProcessExchangeIMEI()  
  do Value::locIMEINumberLoan
  do SendAlert
  do Value::brwUnits  !1

Value::locIMEINumberLoan  Routine
  p_web._DivHeader('FormReturnExchangeLoanProcess_' & p_web._nocolon('locIMEINumberLoan') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locIMEINumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locIMEINumberLoan'',''formreturnexchangeloanprocess_locimeinumberloan_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locIMEINumber',p_web.GetSessionValueFormat('locIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormReturnExchangeLoanProcess_' & p_web._nocolon('locIMEINumberLoan') & '_value')

Comment::locIMEINumberLoan  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormReturnExchangeLoanProcess_' & p_web._nocolon('locIMEINumberLoan') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::brwUnits  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwUnits',p_web.GetValue('NewValue'))
    do Value::brwUnits
  Else
    p_web.StoreValue('sbogen:RecordNumber')
  End

Value::brwUnits  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseReturnExchangeLoan --
  p_web.SetValue('BrowseReturnExchangeLoan:NoForm',1)
  p_web.SetValue('BrowseReturnExchangeLoan:FormName',loc:formname)
  p_web.SetValue('BrowseReturnExchangeLoan:parentIs','Form')
  p_web.SetValue('_parentProc','FormReturnExchangeLoanProcess')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormReturnExchangeLoanProcess_BrowseReturnExchangeLoan_embedded_div')&'"><!-- Net:BrowseReturnExchangeLoan --></div><13,10>'
    p_web._DivHeader('FormReturnExchangeLoanProcess_' & lower('BrowseReturnExchangeLoan') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormReturnExchangeLoanProcess_' & lower('BrowseReturnExchangeLoan') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseReturnExchangeLoan --><13,10>'
  end
  do SendPacket

Comment::brwUnits  Routine
    loc:comment = ''
  p_web._DivHeader('FormReturnExchangeLoanProcess_' & p_web._nocolon('brwUnits') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnReturnUnits  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnReturnUnits',p_web.GetValue('NewValue'))
    do Value::btnReturnUnits
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnReturnUnits  Routine
  p_web._DivHeader('FormReturnExchangeLoanProcess_' & p_web._nocolon('btnReturnUnits') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip(p_web.GSV('FinalURL'))&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnReturnUnits','Return Units','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()

Comment::btnReturnUnits  Routine
    loc:comment = ''
  p_web._DivHeader('FormReturnExchangeLoanProcess_' & p_web._nocolon('btnReturnUnits') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormReturnExchangeLoanProcess_locReturnType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locReturnType
      else
        do Value::locReturnType
      end
  of lower('FormReturnExchangeLoanProcess_locIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIMEINumber
      else
        do Value::locIMEINumber
      end
  of lower('FormReturnExchangeLoanProcess_locIMEINumberLoan_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIMEINumberLoan
      else
        do Value::locIMEINumberLoan
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormReturnExchangeLoanProcess_form:ready_',1)
  p_web.SetSessionValue('FormReturnExchangeLoanProcess_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormReturnExchangeLoanProcess',0)

PreCopy  Routine
  p_web.SetValue('FormReturnExchangeLoanProcess_form:ready_',1)
  p_web.SetSessionValue('FormReturnExchangeLoanProcess_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormReturnExchangeLoanProcess',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormReturnExchangeLoanProcess_form:ready_',1)
  p_web.SetSessionValue('FormReturnExchangeLoanProcess_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormReturnExchangeLoanProcess:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormReturnExchangeLoanProcess_form:ready_',1)
  p_web.SetSessionValue('FormReturnExchangeLoanProcess_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormReturnExchangeLoanProcess:Primed',0)
  p_web.setsessionvalue('showtab_FormReturnExchangeLoanProcess',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('OrderType') = 'E'
  End
  If p_web.GSV('OrderType') = 'L'
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormReturnExchangeLoanProcess_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormReturnExchangeLoanProcess_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
  If p_web.GSV('OrderType') = 'E'
    loc:InvalidTab += 1
        If locReturnType = ''
          loc:Invalid = 'locReturnType'
          loc:alert = p_web.translate('Return Type') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locReturnType = Upper(locReturnType)
          p_web.SetSessionValue('locReturnType',locReturnType)
        If loc:Invalid <> '' then exit.
        If locIMEINumber = ''
          loc:Invalid = 'locIMEINumber'
          loc:alert = p_web.translate('I.M.E.I. No To Return') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locIMEINumber = Upper(locIMEINumber)
          p_web.SetSessionValue('locIMEINumber',locIMEINumber)
        If loc:Invalid <> '' then exit.
  End
  ! tab = 2
  If p_web.GSV('OrderType') = 'L'
    loc:InvalidTab += 1
        If locIMEINumber = ''
          loc:Invalid = 'locIMEINumber'
          loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locIMEINumber = Upper(locIMEINumber)
          p_web.SetSessionValue('locIMEINumber',locIMEINumber)
        If loc:Invalid <> '' then exit.
  End
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormReturnExchangeLoanProcess:Primed',0)
  p_web.StoreValue('locReturnType')
  p_web.StoreValue('locIMEINumber')
  p_web.StoreValue('locIMEINumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locReturnType',locReturnType) ! STRING(30)
     p_web.SSV('locIMEINumber',locIMEINumber) ! STRING(30)
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locReturnType = p_web.GSV('locReturnType') ! STRING(30)
     locIMEINumber = p_web.GSV('locIMEINumber') ! STRING(30)
ProcessExchangeIMEI		PROCEDURE()
locInvoiceNumber LONG()
locReceivedDate DATE()
locOrderNumber LONG()
locPrice                    REAL()
gExchOrd                    QUEUE(gExchangeOrders),PRE()
                            END
	CODE
		IF (p_web.GSV('locIMEINumber') = '')
			RETURN
		END ! IF
        LOOP 1 TIMES
            
            Access:SBO_GenericFile.ClearKey(sbogen:String1Key)
            sbogen:SessionID = p_web.SessionID
            sbogen:String1 = p_web.GSV('locIMEINumber')
            IF (Access:SBO_GenericFile.TryFetch(sbogen:String1Key) = Level:Benign)
                loc:alert = 'You have already entered the selected I.M.E.I. Number.'
                loc:invalid = 'locIMEINumber'
                BREAK
            END ! IF
            
            IF (p_web.GSV('OrderType') = 'E')
                IF (p_web.GSV('locReturnType') = '')
                    loc:alert = 'You must select a Return Type.'
                    loc:invalid = 'locReturnType'
                    BREAK
                END ! IF
			
                Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
                xch:ESN = p_web.GSV('locIMEINumber')
                IF (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key))
                    loc:alert = 'Unable to find the selected I.M.E.I. Number.'
                    loc:invalid = 'locIMEINumber'
                    BREAK
                END ! IF
			
                IF (xch:Location <> p_web.GSV('BookingSiteLocation'))
                    loc:alert = 'The selected I.M.E.I. is not in your location.'
                    loc:invalid = 'locIMEINumber'
                    BREAK
                END ! IF
			
                IF (xch:Available <> 'AVL')
                    loc:alert = 'The selected I.M.E.I. is not "Available".'
                    loc:invalid = 'locIMEINumber'
                    BREAK
                END ! IF
			
			! Is the unit already on a returns order?
                IF (VodacomClass.ReturnUnitAlreadyOnOrder(xch:Ref_Number,1))
                    loc:alert = 'Error! The selected unit is awaiting, or is already on a Returns Order.'
                    loc:invalid = 'locIMEINumber'
                    BREAK
                END ! IF
			
                VodacomClass.GetExchangeLoanOrderDetails(xch:Ref_Number,gExchOrd)
            END ! IF
            IF (p_web.GSV('OrderType') = 'L')
                Access:LOAN.ClearKey(loa:ESN_Only_Key)
                loa:ESN = p_web.GSV('locIMEINumber')
                IF (Access:LOAN.TryFetch(loa:ESN_Only_Key))
                    loc:alert = 'Unable to find the selected I.M.E.I. Number.'
                    loc:invalid = 'locIMEINumber'
                    BREAK
                END ! IF
                
                IF (loa:Location <> p_web.GSV('BookingSiteLocation'))
                    loc:alert = 'The selected I.M.E.I. is not in your location.'
                    loc:invalid = 'locIMEINumber'
                    BREAK
                END ! IF
			
                IF (loa:Available <> 'AVL')
                    loc:alert = 'The selected I.M.E.I. is not "Available".'
                    loc:invalid = 'locIMEINumber'
                    BREAK
                END ! IF
                
                ! Is the unit already on a returns order?
                IF (VodacomClass.ReturnUnitAlreadyOnOrder(loa:Ref_Number,2))
                    loc:alert = 'Error! The selected unit is awaiting, or is already on a Returns Order.'
                    loc:invalid = 'locIMEINumber'
                    BREAK
                END ! IF
			
                VodacomClass.GetExchangeLoanOrderDetails(loa:Ref_Number,gExchOrd,1)
            END ! IF
            
			
			locInvoiceNumber = gExchOrd.InvoiceNumber
			locReceivedDate = gExchOrd.ReceivedDate
			locOrderNumber = gExchOrd.OrderNumber
			locPrice = gExchOrd.Price
			
			IF (locInvoiceNumber = 0)
				loc:alert = 'Unable to find the original invoice for the selected I.M.E.I. Number.'
				loc:invalid = 'locIMEINumber'
				BREAK
			END ! IF
			
            IF (locReceivedDate > 0)
                IF (p_web.GSV('OrderType') = 'E')
                    Access:RETTYPES.ClearKey(rtt:DescriptionKey)
                    rtt:Description = p_web.GSV('locReturnType')
                    IF (Access:RETTYPES.TryFetch(rtt:DescriptionKey) = Level:Benign)
					
                    END ! IF
                    IF (rtt:UseReturnDays)
                        IF (rtt:ExchangeReturnDays + locReceivedDate < TODAY())
                            loc:alert = 'Exchange cannot be returned as was received at Franchise over the set period.'
                            loc:invalid = 'locIMEINumber'
                            BREAK
                        END ! IF
                    END ! IF
                END !I F
                IF (p_web.GSV('OrderType') = 'L')
                    IF (GETINI('STOCK','UseLoanReturnDays',,PATH() & '\SB2KDEF.INI') = 1)
                        IF (GETINI('STOCK','LoanReturnDays',0,PATH() & '\SB2KDEF.INI') + locReceivedDate < TODAY())
                            loc:alert = 'Loan cannot be returned as was received at Franchise over the set period.'
                            loc:invalid = 'locIMEINumber'
                            BREAK
                        END ! IF
                    END ! IF
                END ! IF
                
			END ! IF
			
			! Got this far? All ok then
			IF (Access:SBO_GenericFile.PrimeRecord() = Level:Benign)
				sbogen:SessionID = p_web.SessionID
				sbogen:String1 = p_web.GSV('locIMEINumber')
				sbogen:String2 = p_web.GSV('locReturnType')
				sbogen:Long1 = locInvoiceNumber
                sbogen:Long2 = locOrderNumber
                IF (p_web.GSV('OrderType') = 'E')
                    sbogen:Long3 = xch:Ref_Number
                END ! IF
                IF (p_web.GSV('OrderType') = 'L')
                    sbogen:Long3 = xch:Ref_Number
                END ! IF
				sbogen:Real1 = locPrice
				IF (Access:SBO_GenericFile.TryInsert())
					Access:SBO_GenericFile.CancelAutoInc()
				END ! IF
			END ! IF
			
			p_web.SSV('locIMEINumber','')
			
		END ! LOOP
