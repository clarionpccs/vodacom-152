

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER001.INC'),ONCE        !Local module procedure declarations
                     END


PreLoginForm         PROCEDURE  (NetWebServerWorker p_web)
loc:x          Long
packet              string(NET:MaxBinData)
packetlen           long
CRLF           String('<13,10>')
NBSP           String('&#160;')

  CODE
  GlobalErrors.SetProcedureName('PreLoginForm')
  p_web.SetValue('_parentPage','PreLoginForm')
  p_web.publicpage = 1
  if p_web.sessionId = 0 then p_web.NewSession().
  do Header
  packet = clip(packet) & p_web._jsBodyOnLoad('PageBody',,'PageBodyDiv')
        CreateScript(p_web,packet,'alert("Your session appears to have expired. You will now be returned to the login screen.")')
        CreateScript(p_web,packet,'window.open("LoginForm","_self")')
        Do sendpacket
  do Footer
  packet = clip(packet) & p_web.Popup()
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

SendPacket  Routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,Net:NoHeader)
    packet = ''
  end
Header Routine
  packet = p_web.w3Header()
  packet = clip(packet) & '<head>'&|
      '<title>'&clip(p_web.site.PageTitle)&'</title>'&|
      '<meta http-equiv="Content-Type" content="text/html; charset='&clip(p_web.site.HtmlCharset)&'" /><13,10>'
  packet =  clip(packet) & p_web.IncludeStyles()
  packet =  clip(packet) & p_web.IncludeScripts()
  packet = clip(packet) & '</head><13,10>'
  p_web.ParseHTML(packet,1,0,Net:SendHeader+Net:DontCache)
  packet = ''
Footer Routine
  packet = clip(packet) & '<!-- Net:SelectField --><13,10>' &|
                          '<script>bodyOnLoad();</script><13,10>' &|
                         '</div></body><13,10></html><13,10>'
