

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER311.INC'),ONCE        !Local module procedure declarations
                     END


FormAuditAddStock    PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locStockDespatchNote STRING(30)                            !
locStockPurchaseCost REAL                                  !
locStockPercentageMarkUp REAL                              !
locStockTradePrice   REAL                                  !
locStockRetailPrice  REAL                                  !
locStockDateReceived REAL                                  !
locStockADditionalNotes STRING(255)                        !
locStockQuantity     LONG                                  !
FilesOpened     Long
GENSHORT::State  USHORT
STOAUDIT::State  USHORT
STOCK::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('FormAuditAddStock')
  loc:formname = 'FormAuditAddStock_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormAuditAddStock',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormAuditAddStock','')
    p_web._DivHeader('FormAuditAddStock',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormAuditAddStock',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAuditAddStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAuditAddStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormAuditAddStock',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAuditAddStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormAuditAddStock',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormAuditAddStock',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(GENSHORT)
  p_web._OpenFile(STOAUDIT)
  p_web._OpenFile(STOCK)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(GENSHORT)
  p_Web._CloseFile(STOAUDIT)
  p_Web._CloseFile(STOCK)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormAuditAddStock_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('sto:Purchase_Cost')
    p_web.SetPicture('sto:Purchase_Cost','@n14.2')
  End
  p_web.SetSessionPicture('sto:Purchase_Cost','@n14.2')
  If p_web.IfExistsValue('sto:Percentage_Mark_Up')
    p_web.SetPicture('sto:Percentage_Mark_Up','@7.2')
  End
  p_web.SetSessionPicture('sto:Percentage_Mark_Up','@7.2')
  If p_web.IfExistsValue('sto:Sale_Cost')
    p_web.SetPicture('sto:Sale_Cost','@n14.2')
  End
  p_web.SetSessionPicture('sto:Sale_Cost','@n14.2')
  If p_web.IfExistsValue('sto:Retail_Cost')
    p_web.SetPicture('sto:Retail_Cost','@n14.2')
  End
  p_web.SetSessionPicture('sto:Retail_Cost','@n14.2')
  If p_web.IfExistsValue('locStockDateReceived')
    p_web.SetPicture('locStockDateReceived','@d06b')
  End
  p_web.SetSessionPicture('locStockDateReceived','@d06b')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locStockQuantity',locStockQuantity)
  p_web.SetSessionValue('locStockDespatchNote',locStockDespatchNote)
  p_web.SetSessionValue('sto:Purchase_Cost',sto:Purchase_Cost)
  p_web.SetSessionValue('sto:Percentage_Mark_Up',sto:Percentage_Mark_Up)
  p_web.SetSessionValue('sto:Sale_Cost',sto:Sale_Cost)
  p_web.SetSessionValue('sto:Retail_Cost',sto:Retail_Cost)
  p_web.SetSessionValue('locStockDateReceived',locStockDateReceived)
  p_web.SetSessionValue('locStockADditionalNotes',locStockADditionalNotes)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locStockQuantity')
    locStockQuantity = p_web.GetValue('locStockQuantity')
    p_web.SetSessionValue('locStockQuantity',locStockQuantity)
  End
  if p_web.IfExistsValue('locStockDespatchNote')
    locStockDespatchNote = p_web.GetValue('locStockDespatchNote')
    p_web.SetSessionValue('locStockDespatchNote',locStockDespatchNote)
  End
  if p_web.IfExistsValue('sto:Purchase_Cost')
    sto:Purchase_Cost = p_web.dformat(clip(p_web.GetValue('sto:Purchase_Cost')),'@n14.2')
    p_web.SetSessionValue('sto:Purchase_Cost',sto:Purchase_Cost)
  End
  if p_web.IfExistsValue('sto:Percentage_Mark_Up')
    sto:Percentage_Mark_Up = p_web.dformat(clip(p_web.GetValue('sto:Percentage_Mark_Up')),'@7.2')
    p_web.SetSessionValue('sto:Percentage_Mark_Up',sto:Percentage_Mark_Up)
  End
  if p_web.IfExistsValue('sto:Sale_Cost')
    sto:Sale_Cost = p_web.dformat(clip(p_web.GetValue('sto:Sale_Cost')),'@n14.2')
    p_web.SetSessionValue('sto:Sale_Cost',sto:Sale_Cost)
  End
  if p_web.IfExistsValue('sto:Retail_Cost')
    sto:Retail_Cost = p_web.dformat(clip(p_web.GetValue('sto:Retail_Cost')),'@n14.2')
    p_web.SetSessionValue('sto:Retail_Cost',sto:Retail_Cost)
  End
  if p_web.IfExistsValue('locStockDateReceived')
    locStockDateReceived = p_web.dformat(clip(p_web.GetValue('locStockDateReceived')),'@d06b')
    p_web.SetSessionValue('locStockDateReceived',locStockDateReceived)
  End
  if p_web.IfExistsValue('locStockADditionalNotes')
    locStockADditionalNotes = p_web.GetValue('locStockADditionalNotes')
    p_web.SetSessionValue('locStockADditionalNotes',locStockADditionalNotes)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormAuditAddStock_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    p_web.StoreValue('stoa:Internal_AutoNumber')
    p_web.StoreValue('AuditAction')
    
    Access:STOAUDIT.ClearKey(stoa:Internal_AutoNumber_Key)
    stoa:Internal_AutoNumber = p_web.GSV('stoa:Internal_AutoNumber')
    IF (Access:STOAUDIT.TryFetch(stoa:Internal_AutoNumber_Key) = Level:Benign)
        
        Access:STOCK.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = stoa:Stock_Ref_No
        IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
            
            p_web.SSV('locStockDespatchNote','STOCK AUDIT')
            p_web.SSV('locSTockDateReceived',TODAY())
            p_web.FileToSessionQueue(STOCK)
            p_web.FileToSessionQueue(STOAUDIT)
        ELSE ! IF
        END ! IF
        
        CASE p_web.GSV('AuditAction')
        OF 'add' OROF 'addvar' ! Increase 
            IF (STOA:New_Level <= STOA:Original_Level)
                CreateScript(p_web,packet,'alert("The audited quantity is lower than the stock level. Increase Stock is not a valid option for this part.");window.open("FormStockAuditProcess","_self")')
                DO SendPacket
                EXIT
            END ! IF
            
  		    p_web.SSV('locStockQuantity',STOA:New_Level-STOA:Original_Level)
  
        OF 'del' OROF 'delvar' ! Decrease
            IF (STOA:New_Level >= STOA:Original_Level)
                CreateScript(p_web,packet,'alert("The audited quantity is higher than the stock level. Decrease Stock is not a valid option for this part.");window.open("FormStockAuditProcess","_self")')
                DO SendPacket
                EXIT
            END ! IF
            
            p_web.SSV('locStockQuantity',STOA:Original_Level-STOA:New_Level)
            
        END ! IF
    ELSE ! IF
    END ! IF  
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locStockQuantity = p_web.RestoreValue('locStockQuantity')
 locStockDespatchNote = p_web.RestoreValue('locStockDespatchNote')
 locStockDateReceived = p_web.RestoreValue('locStockDateReceived')
 locStockADditionalNotes = p_web.RestoreValue('locStockADditionalNotes')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'FormStockAuditProcess'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormAuditAddStock_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormAuditAddStock_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormAuditAddStock_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'FormStockAuditProcess'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormAuditAddStock" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormAuditAddStock" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormAuditAddStock" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Add Stock') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Add Stock',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormAuditAddStock">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormAuditAddStock" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormAuditAddStock')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('General') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormAuditAddStock')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormAuditAddStock'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locStockADditionalNotes')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormAuditAddStock')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('General') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormAuditAddStock_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('General')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locStockQuantity
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locStockQuantity
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locStockQuantity
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If INLIST(p_web.GSV('AuditAction'),'add','addvar')
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locStockDespatchNote
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locStockDespatchNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locStockDespatchNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If INLIST(p_web.GSV('AuditAction'),'add','addvar')
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::STO:Purchase_Cost
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::STO:Purchase_Cost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::STO:Purchase_Cost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If INLIST(p_web.GSV('AuditAction'),'add','addvar')
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::STO:Percentage_Mark_Up
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::STO:Percentage_Mark_Up
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::STO:Percentage_Mark_Up
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If INLIST(p_web.GSV('AuditAction'),'add','addvar')
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::STO:Sale_Cost
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::STO:Sale_Cost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::STO:Sale_Cost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If INLIST(p_web.GSV('AuditAction'),'add','addvar')
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Retail_cost
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Retail_cost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sto:Retail_cost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If INLIST(p_web.GSV('AuditAction'),'add','addvar')
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locStockDateReceived
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locStockDateReceived
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locStockDateReceived
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locStockADditionalNotes
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locStockADditionalNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locStockADditionalNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locStockQuantity  Routine
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('locStockQuantity') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Quantity')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locStockQuantity  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locStockQuantity',p_web.GetValue('NewValue'))
    locStockQuantity = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locStockQuantity
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locStockQuantity',p_web.GetValue('Value'))
    locStockQuantity = p_web.GetValue('Value')
  End
  do Value::locStockQuantity
  do SendAlert

Value::locStockQuantity  Routine
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('locStockQuantity') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locStockQuantity
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locStockQuantity')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locStockQuantity'',''formauditaddstock_locstockquantity_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locStockQuantity',p_web.GetSessionValueFormat('locStockQuantity'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormAuditAddStock_' & p_web._nocolon('locStockQuantity') & '_value')

Comment::locStockQuantity  Routine
      loc:comment = ''
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('locStockQuantity') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locStockDespatchNote  Routine
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('locStockDespatchNote') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Despatch Note')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locStockDespatchNote  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locStockDespatchNote',p_web.GetValue('NewValue'))
    locStockDespatchNote = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locStockDespatchNote
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locStockDespatchNote',p_web.GetValue('Value'))
    locStockDespatchNote = p_web.GetValue('Value')
  End
  do Value::locStockDespatchNote
  do SendAlert

Value::locStockDespatchNote  Routine
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('locStockDespatchNote') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locStockDespatchNote
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locStockDespatchNote')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locStockDespatchNote'',''formauditaddstock_locstockdespatchnote_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locStockDespatchNote',p_web.GetSessionValueFormat('locStockDespatchNote'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormAuditAddStock_' & p_web._nocolon('locStockDespatchNote') & '_value')

Comment::locStockDespatchNote  Routine
      loc:comment = ''
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('locStockDespatchNote') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::STO:Purchase_Cost  Routine
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('STO:Purchase_Cost') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Purchase Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::STO:Purchase_Cost  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('STO:Purchase_Cost',p_web.GetValue('NewValue'))
    sto:Purchase_Cost = p_web.GetValue('NewValue') !FieldType= REAL Field = sto:Purchase_Cost
    do Value::STO:Purchase_Cost
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Purchase_Cost',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    sto:Purchase_Cost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::STO:Purchase_Cost
  do SendAlert

Value::STO:Purchase_Cost  Routine
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('STO:Purchase_Cost') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sto:Purchase_Cost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('sto:Purchase_Cost')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''STO:Purchase_Cost'',''formauditaddstock_sto:purchase_cost_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sto:Purchase_Cost',p_web.GetSessionValue('sto:Purchase_Cost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormAuditAddStock_' & p_web._nocolon('STO:Purchase_Cost') & '_value')

Comment::STO:Purchase_Cost  Routine
      loc:comment = ''
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('STO:Purchase_Cost') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::STO:Percentage_Mark_Up  Routine
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('STO:Percentage_Mark_Up') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Percentage Mark Up')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::STO:Percentage_Mark_Up  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('STO:Percentage_Mark_Up',p_web.GetValue('NewValue'))
    sto:Percentage_Mark_Up = p_web.GetValue('NewValue') !FieldType= REAL Field = sto:Percentage_Mark_Up
    do Value::STO:Percentage_Mark_Up
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Percentage_Mark_Up',p_web.dFormat(p_web.GetValue('Value'),'@7.2'))
    sto:Percentage_Mark_Up = p_web.Dformat(p_web.GetValue('Value'),'@7.2') !
  End
  do Value::STO:Percentage_Mark_Up
  do SendAlert

Value::STO:Percentage_Mark_Up  Routine
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('STO:Percentage_Mark_Up') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sto:Percentage_Mark_Up
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('sto:Percentage_Mark_Up')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''STO:Percentage_Mark_Up'',''formauditaddstock_sto:percentage_mark_up_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sto:Percentage_Mark_Up',p_web.GetSessionValue('sto:Percentage_Mark_Up'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@7.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormAuditAddStock_' & p_web._nocolon('STO:Percentage_Mark_Up') & '_value')

Comment::STO:Percentage_Mark_Up  Routine
      loc:comment = ''
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('STO:Percentage_Mark_Up') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::STO:Sale_Cost  Routine
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('STO:Sale_Cost') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Trade Price')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::STO:Sale_Cost  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('STO:Sale_Cost',p_web.GetValue('NewValue'))
    sto:Sale_Cost = p_web.GetValue('NewValue') !FieldType= REAL Field = sto:Sale_Cost
    do Value::STO:Sale_Cost
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Sale_Cost',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    sto:Sale_Cost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::STO:Sale_Cost
  do SendAlert

Value::STO:Sale_Cost  Routine
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('STO:Sale_Cost') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sto:Sale_Cost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('sto:Sale_Cost')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''STO:Sale_Cost'',''formauditaddstock_sto:sale_cost_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sto:Sale_Cost',p_web.GetSessionValue('sto:Sale_Cost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormAuditAddStock_' & p_web._nocolon('STO:Sale_Cost') & '_value')

Comment::STO:Sale_Cost  Routine
      loc:comment = ''
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('STO:Sale_Cost') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sto:Retail_cost  Routine
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('sto:Retail_cost') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Retail Price')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Retail_cost  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Retail_cost',p_web.GetValue('NewValue'))
    sto:Retail_Cost = p_web.GetValue('NewValue') !FieldType= REAL Field = sto:Retail_Cost
    do Value::sto:Retail_cost
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Retail_Cost',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    sto:Retail_Cost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End
  do Value::sto:Retail_cost
  do SendAlert

Value::sto:Retail_cost  Routine
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('sto:Retail_cost') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- sto:Retail_Cost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('sto:Retail_Cost')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''sto:Retail_cost'',''formauditaddstock_sto:retail_cost_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','sto:Retail_Cost',p_web.GetSessionValue('sto:Retail_Cost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormAuditAddStock_' & p_web._nocolon('sto:Retail_cost') & '_value')

Comment::sto:Retail_cost  Routine
      loc:comment = ''
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('sto:Retail_cost') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locStockDateReceived  Routine
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('locStockDateReceived') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Date Received')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locStockDateReceived  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locStockDateReceived',p_web.GetValue('NewValue'))
    locStockDateReceived = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locStockDateReceived
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locStockDateReceived',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    locStockDateReceived = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  do Value::locStockDateReceived
  do SendAlert

Value::locStockDateReceived  Routine
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('locStockDateReceived') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locStockDateReceived
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locStockDateReceived')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locStockDateReceived'',''formauditaddstock_locstockdatereceived_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locStockDateReceived',p_web.GetSessionValue('locStockDateReceived'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormAuditAddStock_' & p_web._nocolon('locStockDateReceived') & '_value')

Comment::locStockDateReceived  Routine
      loc:comment = ''
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('locStockDateReceived') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locStockADditionalNotes  Routine
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('locStockADditionalNotes') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Additional Notes')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locStockADditionalNotes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locStockADditionalNotes',p_web.GetValue('NewValue'))
    locStockADditionalNotes = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locStockADditionalNotes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locStockADditionalNotes',p_web.GetValue('Value'))
    locStockADditionalNotes = p_web.GetValue('Value')
  End
  If locStockADditionalNotes = ''
    loc:Invalid = 'locStockADditionalNotes'
    loc:alert = p_web.translate('Additional Notes') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locStockADditionalNotes = Upper(locStockADditionalNotes)
    p_web.SetSessionValue('locStockADditionalNotes',locStockADditionalNotes)
  do Value::locStockADditionalNotes
  do SendAlert

Value::locStockADditionalNotes  Routine
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('locStockADditionalNotes') & '_value','adiv')
  loc:extra = ''
  ! --- TEXT --- locStockADditionalNotes
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locStockADditionalNotes')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locStockADditionalNotes = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locStockADditionalNotes'',''formauditaddstock_locstockadditionalnotes_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('locStockADditionalNotes',p_web.GetSessionValue('locStockADditionalNotes'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locStockADditionalNotes),,Net:Web:Control) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormAuditAddStock_' & p_web._nocolon('locStockADditionalNotes') & '_value')

Comment::locStockADditionalNotes  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormAuditAddStock_' & p_web._nocolon('locStockADditionalNotes') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormAuditAddStock_locStockQuantity_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locStockQuantity
      else
        do Value::locStockQuantity
      end
  of lower('FormAuditAddStock_locStockDespatchNote_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locStockDespatchNote
      else
        do Value::locStockDespatchNote
      end
  of lower('FormAuditAddStock_STO:Purchase_Cost_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::STO:Purchase_Cost
      else
        do Value::STO:Purchase_Cost
      end
  of lower('FormAuditAddStock_STO:Percentage_Mark_Up_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::STO:Percentage_Mark_Up
      else
        do Value::STO:Percentage_Mark_Up
      end
  of lower('FormAuditAddStock_STO:Sale_Cost_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::STO:Sale_Cost
      else
        do Value::STO:Sale_Cost
      end
  of lower('FormAuditAddStock_sto:Retail_cost_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:Retail_cost
      else
        do Value::sto:Retail_cost
      end
  of lower('FormAuditAddStock_locStockDateReceived_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locStockDateReceived
      else
        do Value::locStockDateReceived
      end
  of lower('FormAuditAddStock_locStockADditionalNotes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locStockADditionalNotes
      else
        do Value::locStockADditionalNotes
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormAuditAddStock_form:ready_',1)
  p_web.SetSessionValue('FormAuditAddStock_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormAuditAddStock',0)

PreCopy  Routine
  p_web.SetValue('FormAuditAddStock_form:ready_',1)
  p_web.SetSessionValue('FormAuditAddStock_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormAuditAddStock',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormAuditAddStock_form:ready_',1)
  p_web.SetSessionValue('FormAuditAddStock_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormAuditAddStock:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormAuditAddStock_form:ready_',1)
  p_web.SetSessionValue('FormAuditAddStock_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormAuditAddStock:Primed',0)
  p_web.setsessionvalue('showtab_FormAuditAddStock',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormAuditAddStock_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
    Access:STOCK.ClearKey(sto:Ref_Number_Key)
    sto:Ref_Number = p_web.GSV('sto:Ref_number')
    IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
    END !I F
    
    Access:STOAUDIT.ClearKey(stoa:Internal_AutoNumber_Key)
    stoa:Internal_AutoNumber = p_web.GSV('stoa:Internal_AutoNumber')
    IF (Access:STOAUDIT.TryFetch(stoa:Internal_AutoNumber_Key) = Level:Benign)
    END ! IF
        
    CASE p_web.GSV('AuditAction')
    OF 'add' OROF 'addvar'
  
        sto:Quantity_Stock += p_web.GSV('locStockQuantity')
        IF (Access:STOCK.TryUpdate() = Level:Benign)
            IF (AddToStockHistory(sto:Ref_Number,| 										
                'ADD',|
                'STOCK AUDIT ' & p_web.GSV('stoa:Audit_Ref_No'),|
                0,|
                0,|
                p_web.GSV('locStockQuantity'),|
                sto:Purchase_Cost,|
                sto:Sale_Cost,|
                sto:Retail_Cost,|
                'STOCK ADDED',|
                p_web.GSV('locStockAdditionalNotes'),|
                p_web.GSV('BookingUserCode'),|
                sto:Quantity_Stock,|
                sto:AveragePurchaseCost,|
                sto:Purchase_Cost,|
                sto:Sale_Cost,|
                sto:Retail_Cost))
            END ! IF
        END!  IF
    OF 'del' OROF 'delvar'
        sto:Quantity_Stock -= p_web.GSV('locStockQuantity')
        IF (sto:Quantity_Stock < 0)
            sto:Quantity_Stock = 0
        END ! IF
        
        IF (Access:STOCK.TryUpdate() = Level:Benign)
            IF (AddToStockHistory(sto:Ref_Number,| 										
                'DEC',|
                'STOCK AUDIT ' & p_web.GSV('stoa:Audit_Ref_No'),|
                0,|
                0,|
                p_web.GSV('locStockQuantity'),|
                sto:Purchase_Cost,|
                sto:Sale_Cost,|
                sto:Retail_Cost,|
                'STOCK DECREMENTED',|
                p_web.GSV('locStockAdditionalNotes'),|
                p_web.GSV('BookingUserCode'),|
                sto:Quantity_Stock)) 
                    
            END ! IF
        END 
        
    END 
    
    CASE p_web.GSV('AuditAction')
    OF 'addvar'
        IF (Access:GENSHORT.PrimeRecord() = Level:Benign)
            gens:Audit_No = p_web.GSV('stoa:Audit_Ref_No')
            gens:Stock_Ref_No = sto:Ref_Number
            gens:Stock_Qty = (STOA:Original_Level-STOA:New_Level) * -1
            IF (Access:GENSHORT.TryInsert())
                Access:GENSHORT.CancelAutoInc()
            END ! IF
        END ! IF
    OF 'delvar'
        IF (Access:GENSHORT.PrimeRecord() = Level:Benign)
            gens:Audit_No = p_web.GSV('stoa:Audit_Ref_No')
            gens:Stock_Ref_No = sto:Ref_Number
            gens:Stock_Qty = stoa:New_Level - stoa:Original_Level
            IF (Access:GENSHORT.TryInsert())
                Access:GENSHORT.CancelAutoInc()
            END ! IF
        END ! IF
    OF 'add' OROF 'del'
        stoa:Original_Level = stoa:New_Level
        Access:STOAUDIT.TryUpdate()
                
        Access:GENSHORT.ClearKey(gens:Lock_Down_Key)
        gens:Audit_No = stoa:Audit_Ref_No
        gens:Stock_Ref_No = sto:Ref_Number
        IF (Access:GENSHORT.TryFetch(gens:Lock_Down_Key) = Level:Benign)
            Access:GENSHORT.DeleteRecord(0)
        END ! IF
        
    END ! IF
    
    
    
  p_web.DeleteSessionValue('FormAuditAddStock_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
        If locStockADditionalNotes = ''
          loc:Invalid = 'locStockADditionalNotes'
          loc:alert = p_web.translate('Additional Notes') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locStockADditionalNotes = Upper(locStockADditionalNotes)
          p_web.SetSessionValue('locStockADditionalNotes',locStockADditionalNotes)
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormAuditAddStock:Primed',0)
  p_web.StoreValue('locStockQuantity')
  p_web.StoreValue('locStockDespatchNote')
  p_web.StoreValue('sto:Purchase_Cost')
  p_web.StoreValue('sto:Percentage_Mark_Up')
  p_web.StoreValue('sto:Sale_Cost')
  p_web.StoreValue('sto:Retail_Cost')
  p_web.StoreValue('locStockDateReceived')
  p_web.StoreValue('locStockADditionalNotes')
