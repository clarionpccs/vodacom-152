

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER287.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
CheckPricing         PROCEDURE  (STRING pType)             ! Declare Procedure
ReturnByte           BYTE                                  !
FilesOpened     BYTE(0)

  CODE
    do OpenFiles
    ReturnByte = 0

    If job:chargeable_job = 'YES' and pType = 'C'
        If job:account_number <> '' And job:charge_type <> '' And |
            job:model_number <> '' And job:unit_type <> '' And job:repair_type <> ''

            !Look for standard charge - L945 (DBH: 04-09-2003)
            Access:STDCHRGE.ClearKey(sta:Model_Number_Charge_Key)
            sta:Model_Number = job:Model_Number
            sta:Charge_Type  = job:Charge_Type
            sta:Unit_Type    = job:Unit_Type
            sta:Repair_Type  = job:Repair_Type
            If Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign
                !Found
            Else !If Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign
                !Error

                Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                sub:Account_Number  = job:Account_Number
                If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Found

                Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Error
                End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                tra:Account_Number  = sub:Main_Account_Number
                If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Found

                Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Error
                End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

                Access:TRACHRGE.ClearKey(trc:Account_Charge_Key)
                trc:Account_Number = tra:Account_Number
                trc:Model_Number   = job:Model_Number
                trc:Charge_Type    = job:Charge_Type
                trc:Unit_Type      = job:Unit_Type
                trc:Repair_Type    = job:Repair_Type
                If Access:TRACHRGE.TryFetch(trc:Account_Charge_Key) = Level:Benign
                    !Found

                Else !If Access:TRACHRGE.TryFetch(trc:Account_Charge_Key) = Level:Benign
                    !Error
                    Access:SUBCHRGE.ClearKey(suc:Model_Repair_Type_Key)
                    suc:Account_Number = job:Account_Number
                    suc:Model_Number   = job:Model_Number
                    suc:Charge_Type    = job:Charge_Type
                    suc:Unit_Type      = job:Unit_Type
                    suc:Repair_Type    = job:Repair_Type
                    If Access:SUBCHRGE.TryFetch(suc:Model_Repair_Type_Key) = Level:Benign
                        !Found

                    Else !If Access:SUBCHRGE.TryFetch(suc:Model_Repair_Type_Key) = Level:Benign
                        !Error
                        glo:ErrorText = 'ERROR - No Pricing Structure. The selected combination of Trade Account, Charge Type, Model Number, Unit Type and Repair Type does not have a pricing structure setup.'
                        ReturnByte = 1
                    End !If Access:SUBCHRGE.TryFetch(suc:Model_Repair_Type_Key) = Level:Benign
                End !If Access:TRACHRGE.TryFetch(trc:Account_Charge_Key) = Level:Benign
            End !If Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign
        End !If job:account_number <> '' And job:charge_type <> '' And |
    End!If job:chargeable_job = 'YES'

    If job:warranty_job = 'YES' and pType = 'W' and ReturnByte = 0
        If job:account_number <> '' And job:warranty_charge_type <> '' And |
            job:model_number <> '' And job:unit_type <> '' And job:repair_type_warranty <> ''
            !Look for standard charge - L945 (DBH: 04-09-2003)
            Access:STDCHRGE.ClearKey(sta:Model_Number_Charge_Key)
            sta:Model_Number = job:Model_Number
            sta:Charge_Type  = job:Warranty_Charge_Type
            sta:Unit_Type    = job:Unit_Type
            sta:Repair_Type  = job:Repair_Type_Warranty
            If Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign
                !Found
            Else !If Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign
                !Error

                Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                sub:Account_Number  = job:Account_Number
                If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Found

                Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Error
                End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                tra:Account_Number  = sub:Main_Account_Number
                If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Found

                Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Error
                End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

                Access:TRACHRGE.ClearKey(trc:Account_Charge_Key)
                trc:Account_Number = tra:Account_Number
                trc:Model_Number   = job:Model_Number
                trc:Charge_Type    = job:Warranty_Charge_Type
                trc:Unit_Type      = job:Unit_Type
                trc:Repair_Type    = job:Repair_Type_Warranty
                If Access:TRACHRGE.TryFetch(trc:Account_Charge_Key) = Level:Benign
                    !Found

                Else !If Access:TRACHRGE.TryFetch(trc:Account_Charge_Key) = Level:Benign
                    !Error
                    Access:SUBCHRGE.ClearKey(suc:Model_Repair_Type_Key)
                    suc:Account_Number = job:Account_Number
                    suc:Model_Number   = job:Model_Number
                    suc:Charge_Type    = job:Warranty_Charge_Type
                    suc:Unit_Type      = job:Unit_Type
                    suc:Repair_Type    = job:Repair_Type_Warranty
                    If Access:SUBCHRGE.TryFetch(suc:Model_Repair_Type_Key) = Level:Benign
                        !Found

                    Else !If Access:SUBCHRGE.TryFetch(suc:Model_Repair_Type_Key) = Level:Benign
                        !Error
                        glo:ErrorText = 'ERROR - No Pricing Structure. The selected combination of Trade Account, Charge Type, Model Number, Unit Type and Repair Type does not have a pricing structure setup.'
                        ReturnByte = 1
                    End !If Access:SUBCHRGE.TryFetch(suc:Model_Repair_Type_Key) = Level:Benign
                End !If Access:TRACHRGE.TryFetch(trc:Account_Charge_Key) = Level:Benign
            End !If Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign
        End!If job:account_number <> '' And job:charge_type <> '' And |
    End!If job:chargeable_job = 'YES'

    do CloseFiles

    Return(ReturnByte)
!--------------------------------------
OpenFiles  ROUTINE
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBCHRGE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBCHRGE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRACHRGE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRACHRGE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:STDCHRGE.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STDCHRGE.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TRADEACC.Close
     Access:SUBTRACC.Close
     Access:SUBCHRGE.Close
     Access:TRACHRGE.Close
     Access:STDCHRGE.Close
     Access:JOBS.Close
     FilesOpened = False
  END
