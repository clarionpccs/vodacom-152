

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER142.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER040.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER111.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER112.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER120.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER139.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER143.INC'),ONCE        !Req'd for module callout resolution
                     END


BillingConfirmation  PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:CChargeType      STRING(30)                            !
tmp:CRepairType      STRING(30)                            !
tmp:CConfirmRepairType STRING(30)                          !
tmp:WChargeType      STRING(30)                            !
tmp:WRepairType      STRING(30)                            !
tmp:WConfirmRepairType BYTE                                !
tmp:Password         STRING(30)                            !
locRepairType        STRING(60)                            !
                    MAP
GetCost             PROCEDURE(STRING pChargeType,*STRING pCost),LONG
                    END ! MAP
FilesOpened     Long
TRACHRGE::State  USHORT
SUBCHRGE::State  USHORT
STDCHRGE::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
CHARTYPE::State  USHORT
REPTYDEF::State  USHORT
USERS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
tmp:CChargeType_OptionView   View(CHARTYPE)
                          Project(cha:Charge_Type)
                        End
tmp:CRepairType_OptionView   View(REPTYDEF)
                          Project(rtd:Repair_Type)
                        End
tmp:WChargeType_OptionView   View(CHARTYPE)
                          Project(cha:Charge_Type)
                        End
tmp:WRepairType_OptionView   View(REPTYDEF)
                          Project(rtd:Repair_Type)
                        End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('BillingConfirmation')
  loc:formname = 'BillingConfirmation_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'BillingConfirmation',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('BillingConfirmation','')
    p_web._DivHeader('BillingConfirmation',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferBillingConfirmation',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBillingConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBillingConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_BillingConfirmation',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferBillingConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_BillingConfirmation',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'BillingConfirmation',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(TRACHRGE)
  p_web._OpenFile(SUBCHRGE)
  p_web._OpenFile(STDCHRGE)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(CHARTYPE)
  p_web._OpenFile(REPTYDEF)
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(TRACHRGE)
  p_Web._CloseFile(SUBCHRGE)
  p_Web._CloseFile(STDCHRGE)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('BillingConfirmation_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('job:Chargeable_Job') = 'YES'
    loc:TabNumber += 1
  End
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:CChargeType'
    p_web.setsessionvalue('showtab_BillingConfirmation',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(CHARTYPE)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:CRepairType')
  End
  If p_web.GSV('job:Warranty_job') = 'YES'
    loc:TabNumber += 1
  End
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:WChargeType'
    p_web.setsessionvalue('showtab_BillingConfirmation',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(CHARTYPE)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:WRepairType')
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:CChargeType',tmp:CChargeType)
  p_web.SetSessionValue('tmp:CRepairType',tmp:CRepairType)
  p_web.SetSessionValue('tmp:CConfirmRepairType',tmp:CConfirmRepairType)
  p_web.SetSessionValue('tmp:WChargeType',tmp:WChargeType)
  p_web.SetSessionValue('tmp:WRepairType',tmp:WRepairType)
  p_web.SetSessionValue('tmp:WConfirmRepairType',tmp:WConfirmRepairType)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:CChargeType')
    tmp:CChargeType = p_web.GetValue('tmp:CChargeType')
    p_web.SetSessionValue('tmp:CChargeType',tmp:CChargeType)
  End
  if p_web.IfExistsValue('tmp:CRepairType')
    tmp:CRepairType = p_web.GetValue('tmp:CRepairType')
    p_web.SetSessionValue('tmp:CRepairType',tmp:CRepairType)
  End
  if p_web.IfExistsValue('tmp:CConfirmRepairType')
    tmp:CConfirmRepairType = p_web.GetValue('tmp:CConfirmRepairType')
    p_web.SetSessionValue('tmp:CConfirmRepairType',tmp:CConfirmRepairType)
  End
  if p_web.IfExistsValue('tmp:WChargeType')
    tmp:WChargeType = p_web.GetValue('tmp:WChargeType')
    p_web.SetSessionValue('tmp:WChargeType',tmp:WChargeType)
  End
  if p_web.IfExistsValue('tmp:WRepairType')
    tmp:WRepairType = p_web.GetValue('tmp:WRepairType')
    p_web.SetSessionValue('tmp:WRepairType',tmp:WRepairType)
  End
  if p_web.IfExistsValue('tmp:WConfirmRepairType')
    tmp:WConfirmRepairType = p_web.GetValue('tmp:WConfirmRepairType')
    p_web.SetSessionValue('tmp:WConfirmRepairType',tmp:WConfirmRepairType)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('BillingConfirmation_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  !region OpenScreen
  
    CalculateBilling(p_web) ! Calculate Job
  
    ! Check if anything changes
    p_web.SSV('tmp:CChargeType',p_web.GSV('job:Charge_Type'))
    p_web.SSV('tmp:WChargeType',p_web.GSV('job:Warranty_Charge_Type'))
    p_web.SSV('tmp:CConfirmRepairType',p_web.GSV('jobe:COverwriteRepairType'))
    p_web.SSV('tmp:WConfirmRepairType',p_web.GSV('jobe:WOverwriteRepairType'))
    p_web.SSV('tmp:CRepairType',p_web.GSV('job:Repair_Type'))
    p_web.SSV('tmp:WRepairType',p_web.GSV('job:Repair_Type_Warranty'))
    
    ! Does the user have access to change the details
    IF (DoesUserHaveAccess(p_web,'JOBS - AMEND REPAIR TYPES'))
        p_web.SSV('ReadOnly:RepairType',0)
    ELSE ! IF
        p_web.SSV('ReadOnly:RepairType',1)
    END ! IF
    
    ! Get the job's engineers skill level
    p_web.SSV('locEngineerSkillLevel',0)
    IF (p_web.GSV('job:Engineer') <> '')
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = p_web.GSV('job:Engineer')
        IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            p_web.SSV('locEngineerSkillLevel',use:SkillLevel)
        END ! IF
    END ! IF
    
    IF (p_web.IfExistsValue('JobCompleteProcess'))
        ! Called from "Complete Repair" button on job
        p_web.StoreValue('JobCompleteProcess')
    ELSE ! IF
        p_web.SSV('JobCompleteProcess','')
    END ! IF
    
    IF (p_web.GSV('JobCompleteProcess') = 1)
    
        p_web.SSV('nextURL','ViewJob') ! Return back to the job screen once done.
    
        IF (p_web.GSV('job:Estimate') = 'YES' AND p_web.GSV('job:Chargeable_Job') = 'YES')
            ! Are we completing the repair, or the estimate?
            IF (p_web.GSV('job:Estimate_Accepted') <> 'YES' AND |
                p_web.GSV('job:Estimate_Rejected') <> 'YES' AND |
                p_web.GSV('job:Estimate_Ready') <> 'YES')
  
                jobPricingRoutine(p_web)
                
                TotalPrice(p_web,'E',vat$,tot$,bal$)
                
                p_web.SSV('JobCompleteProcess',0)
  
                IF (tot$ < p_web.GSV('job:Estimate_IF_Over'))
                    p_web.SSV('nextURL','EstimateQuery') ! Go to estimate query screen next
                    
                ELSE ! IF (p_web.GSV('TotalRepair:Total') < p_web.GSV('job:Estimate_IF_Over'))
                    p_web.SSV('job:Courier_Cost',p_web.GSV('job:Courier_Cost_Estimate'))
                    p_web.SSV('job:Estimate_Ready','YES')
  
              
                    p_web.SSV('Hide:EstimateReady',0)
                    p_web.SSV('GetStatus:Type','JOB')
                    p_web.SSV('GetStatus:StatusNumber',510)
                    GetStatus(510,0,'JOB',p_web)
  
                END ! IF (p_web.GSV('TotalRepair:Total') < p_web.GSV('job:Estimate_IF_Over'))
            END ! IF (p_web.GSV('job:Estimate_Accepted') <> 'YES' AND |
        END ! IF (p_web.GSV('job:Estimate') = 'YES' AND p_web.GSV('job:Chargeable_Job') = 'YES
    ELSE ! IF (p_web.GSV('JobCompleteProcess') = 1)
        ! Not completing job/estimate so go to costs screen next
        p_web.SSV('nextURL','ViewCosts')
    
        IF (p_web.IfExistsValue('passedURL'))
            p_web.SSV('nextURL',p_web.GetValue('passedURL'))
        END ! IF        
        
    END ! IF (p_web.GSV('JobCompleteProcess') = 1)
  
  !endregion 
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:CChargeType = p_web.RestoreValue('tmp:CChargeType')
 tmp:CRepairType = p_web.RestoreValue('tmp:CRepairType')
 tmp:CConfirmRepairType = p_web.RestoreValue('tmp:CConfirmRepairType')
 tmp:WChargeType = p_web.RestoreValue('tmp:WChargeType')
 tmp:WRepairType = p_web.RestoreValue('tmp:WRepairType')
 tmp:WConfirmRepairType = p_web.RestoreValue('tmp:WConfirmRepairType')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('NextURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('BillingConfirmation_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('BillingConfirmation_ChainTo')
    loc:formaction = p_web.GetSessionValue('BillingConfirmation_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="BillingConfirmation" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="BillingConfirmation" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="BillingConfirmation" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Billing Confirmation') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Billing Confirmation',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_BillingConfirmation">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_BillingConfirmation" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_BillingConfirmation')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GSV('job:Chargeable_Job') = 'YES'
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Chargeable') & ''''
        End
        If p_web.GSV('job:Warranty_job') = 'YES'
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Warranty') & ''''
        End
        Loc:Tabnumber = p_web.getSessionValue('showtab_BillingConfirmation')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_BillingConfirmation'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('job:Chargeable_Job') = 'YES'
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:CChargeType')
    ElsIf p_web.GSV('job:Warranty_job') = 'YES'
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:WChargeType')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GSV('job:Chargeable_Job') = 'YES'
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          End
          If p_web.GSV('job:Warranty_job') = 'YES'
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          End
          Loc:Tabnumber = p_web.getSessionValue('showtab_BillingConfirmation')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GSV('job:Chargeable_Job') = 'YES'
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    end
    if p_web.GSV('job:Warranty_job') = 'YES'
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    end
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GSV('job:Chargeable_Job') = 'YES'
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Chargeable') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_BillingConfirmation_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Chargeable')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Chargeable')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Chargeable')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Chargeable')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:CChargeType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:CChargeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:CChargeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:CRepairType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:CRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:CRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:CConfirmRepairType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:CConfirmRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:CConfirmRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
  If p_web.GSV('job:Warranty_job') = 'YES'
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Warranty') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_BillingConfirmation_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Warranty')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Warranty')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Warranty')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Warranty')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:WChargeType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:WChargeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:WChargeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:WRepairType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:WRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:WRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:WConfirmRepairType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:WConfirmRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:WConfirmRepairType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end


Prompt::tmp:CChargeType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CChargeType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Charge Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:CChargeType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:CChargeType',p_web.GetValue('NewValue'))
    tmp:CChargeType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:CChargeType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:CChargeType',p_web.GetValue('Value'))
    tmp:CChargeType = p_web.GetValue('Value')
  End
    tmp:CChargeType = Upper(tmp:CChargeType)
    p_web.SetSessionValue('tmp:CChargeType',tmp:CChargeType)
  do Value::tmp:CChargeType
  do SendAlert
  do Value::tmp:CRepairType  !1

Value::tmp:CChargeType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CChargeType') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('tmp:CChargeType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:CChargeType'',''billingconfirmation_tmp:cchargetype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:CChargeType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:CChargeType',loc:fieldclass,loc:readonly,,200,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:CChargeType') = 0
    p_web.SetSessionValue('tmp:CChargeType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('tmp:CChargeType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(TRACHRGE)
  bind(trc:Record)
  p_web._OpenFile(SUBCHRGE)
  bind(suc:Record)
  p_web._OpenFile(STDCHRGE)
  bind(sta:Record)
  p_web._OpenFile(TRADEACC)
  bind(tra:Record)
  p_web._OpenFile(SUBTRACC)
  bind(sub:Record)
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(USERS)
  bind(use:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:CChargeType_OptionView)
  tmp:CChargeType_OptionView{prop:filter} = 'upper(cha:Warranty ) <> ''YES'''
  tmp:CChargeType_OptionView{prop:order} = 'UPPER(cha:Charge_Type)'
  Set(tmp:CChargeType_OptionView)
  Loop
    Next(tmp:CChargeType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:CChargeType') = 0
      p_web.SetSessionValue('tmp:CChargeType',cha:Charge_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cha:Charge_Type,choose(cha:Charge_Type = p_web.getsessionvalue('tmp:CChargeType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:CChargeType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(TRACHRGE)
  p_Web._CloseFile(SUBCHRGE)
  p_Web._CloseFile(STDCHRGE)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(USERS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('BillingConfirmation_' & p_web._nocolon('tmp:CChargeType') & '_value')

Comment::tmp:CChargeType  Routine
    loc:comment = ''
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CChargeType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:CRepairType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CRepairType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Repair Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:CRepairType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:CRepairType',p_web.GetValue('NewValue'))
    tmp:CRepairType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:CRepairType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:CRepairType',p_web.GetValue('Value'))
    tmp:CRepairType = p_web.GetValue('Value')
  End
    tmp:CRepairType = Upper(tmp:CRepairType)
    p_web.SetSessionValue('tmp:CRepairType',tmp:CRepairType)
  do Value::tmp:CRepairType
  do SendAlert

Value::tmp:CRepairType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CRepairType') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('tmp:CRepairType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:CRepairType'',''billingconfirmation_tmp:crepairtype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:RepairType') = 1 OR p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:CRepairType',loc:fieldclass,loc:readonly,,200,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:CRepairType') = 0
    p_web.SetSessionValue('tmp:CRepairType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('tmp:CRepairType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(TRACHRGE)
  bind(trc:Record)
  p_web._OpenFile(SUBCHRGE)
  bind(suc:Record)
  p_web._OpenFile(STDCHRGE)
  bind(sta:Record)
  p_web._OpenFile(TRADEACC)
  bind(tra:Record)
  p_web._OpenFile(SUBTRACC)
  bind(sub:Record)
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(USERS)
  bind(use:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:CRepairType_OptionView)
  tmp:CRepairType_OptionView{prop:filter} = 'upper(rtd:manufacturer) = upper(''' & p_web.GSV('job:manufacturer') & ''') and upper(rtd:chargeable) = ''YES'' and rtd:NotAvailable = 0 AND rtd:BER <> 1 and rtd:SkillLevel <= ' & p_web.GSV('locEngineerSkillLevel')
  tmp:CRepairType_OptionView{prop:order} = 'UPPER(rtd:Repair_Type)'
  Set(tmp:CRepairType_OptionView)
  Loop
    Next(tmp:CRepairType_OptionView)
    If ErrorCode() then Break.
        locRepairType = ''
        IF (GetCost(p_web.GSV('tmp:CChargeType'),locRepairType) = 0)
            CYCLE
        ELSE
            locRepairType = CLIP(rtd:Repair_Type) & ' --- (' & CLIP(LEFT(FORMAT(locRepairType,@n_14.2))) & ')'            
        END ! IF
        
    if p_web.IfExistsSessionValue('tmp:CRepairType') = 0
      p_web.SetSessionValue('tmp:CRepairType',rtd:Repair_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(locRepairType,rtd:Repair_Type,choose(rtd:Repair_Type = p_web.getsessionvalue('tmp:CRepairType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:CRepairType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(TRACHRGE)
  p_Web._CloseFile(SUBCHRGE)
  p_Web._CloseFile(STDCHRGE)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(USERS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('BillingConfirmation_' & p_web._nocolon('tmp:CRepairType') & '_value')

Comment::tmp:CRepairType  Routine
    loc:comment = ''
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CRepairType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:CConfirmRepairType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CConfirmRepairType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Confirm Repair Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:CConfirmRepairType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:CConfirmRepairType',p_web.GetValue('NewValue'))
    tmp:CConfirmRepairType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:CConfirmRepairType
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:CConfirmRepairType',p_web.GetValue('Value'))
    tmp:CConfirmRepairType = p_web.GetValue('Value')
  End
  do Value::tmp:CConfirmRepairType
  do SendAlert

Value::tmp:CConfirmRepairType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CConfirmRepairType') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- tmp:CConfirmRepairType
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:CConfirmRepairType'',''billingconfirmation_tmp:cconfirmrepairtype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  If p_web.GetSessionValue('tmp:CConfirmRepairType') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:CConfirmRepairType',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('BillingConfirmation_' & p_web._nocolon('tmp:CConfirmRepairType') & '_value')

Comment::tmp:CConfirmRepairType  Routine
    loc:comment = ''
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:CConfirmRepairType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:WChargeType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WChargeType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Charge Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:WChargeType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:WChargeType',p_web.GetValue('NewValue'))
    tmp:WChargeType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:WChargeType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:WChargeType',p_web.GetValue('Value'))
    tmp:WChargeType = p_web.GetValue('Value')
  End
    tmp:WChargeType = Upper(tmp:WChargeType)
    p_web.SetSessionValue('tmp:WChargeType',tmp:WChargeType)
  do Value::tmp:WChargeType
  do SendAlert
  do Value::tmp:WRepairType  !1

Value::tmp:WChargeType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WChargeType') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('tmp:WChargeType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:WChargeType'',''billingconfirmation_tmp:wchargetype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:WChargeType')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:WChargeType',loc:fieldclass,loc:readonly,,200,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:WChargeType') = 0
    p_web.SetSessionValue('tmp:WChargeType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('tmp:WChargeType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(TRACHRGE)
  bind(trc:Record)
  p_web._OpenFile(SUBCHRGE)
  bind(suc:Record)
  p_web._OpenFile(STDCHRGE)
  bind(sta:Record)
  p_web._OpenFile(TRADEACC)
  bind(tra:Record)
  p_web._OpenFile(SUBTRACC)
  bind(sub:Record)
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(USERS)
  bind(use:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:WChargeType_OptionView)
  tmp:WChargeType_OptionView{prop:filter} = 'cha:Warranty = ''YES'''
  tmp:WChargeType_OptionView{prop:order} = 'UPPER(cha:Charge_Type)'
  Set(tmp:WChargeType_OptionView)
  Loop
    Next(tmp:WChargeType_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('tmp:WChargeType') = 0
      p_web.SetSessionValue('tmp:WChargeType',cha:Charge_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cha:Charge_Type,choose(cha:Charge_Type = p_web.getsessionvalue('tmp:WChargeType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:WChargeType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(TRACHRGE)
  p_Web._CloseFile(SUBCHRGE)
  p_Web._CloseFile(STDCHRGE)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(USERS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('BillingConfirmation_' & p_web._nocolon('tmp:WChargeType') & '_value')

Comment::tmp:WChargeType  Routine
    loc:comment = ''
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WChargeType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:WRepairType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WRepairType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Repair Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:WRepairType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:WRepairType',p_web.GetValue('NewValue'))
    tmp:WRepairType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:WRepairType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:WRepairType',p_web.GetValue('Value'))
    tmp:WRepairType = p_web.GetValue('Value')
  End
    tmp:WRepairType = Upper(tmp:WRepairType)
    p_web.SetSessionValue('tmp:WRepairType',tmp:WRepairType)
  do Value::tmp:WRepairType
  do SendAlert

Value::tmp:WRepairType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WRepairType') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('tmp:WRepairType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:WRepairType'',''billingconfirmation_tmp:wrepairtype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('ReadOnly:RepairType') = 1 OR p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('tmp:WRepairType',loc:fieldclass,loc:readonly,,200,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('tmp:WRepairType') = 0
    p_web.SetSessionValue('tmp:WRepairType','')
  end
    packet = clip(packet) & p_web.CreateOption('-------------------------------------','',choose('' = p_web.getsessionvalue('tmp:WRepairType')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(TRACHRGE)
  bind(trc:Record)
  p_web._OpenFile(SUBCHRGE)
  bind(suc:Record)
  p_web._OpenFile(STDCHRGE)
  bind(sta:Record)
  p_web._OpenFile(TRADEACC)
  bind(tra:Record)
  p_web._OpenFile(SUBTRACC)
  bind(sub:Record)
  p_web._OpenFile(CHARTYPE)
  bind(cha:Record)
  p_web._OpenFile(REPTYDEF)
  bind(rtd:Record)
  p_web._OpenFile(USERS)
  bind(use:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(tmp:WRepairType_OptionView)
  tmp:WRepairType_OptionView{prop:filter} = 'upper(rtd:manufacturer) = upper(''' & p_web.GSV('job:manufacturer') & ''') and upper(rtd:warranty) = ''YES'' and rtd:NotAvailable = 0 and rtd:SkillLevel <= ' & p_web.GSV('locEngineerSkillLevel')
  tmp:WRepairType_OptionView{prop:order} = 'UPPER(rtd:Repair_Type)'
  Set(tmp:WRepairType_OptionView)
  Loop
    Next(tmp:WRepairType_OptionView)
    If ErrorCode() then Break.
        locRepairType = ''
        IF (GetCost(p_web.GSV('tmp:WChargeType'),locRepairType) = 0)
            CYCLE
        ELSE
            locRepairType = CLIP(rtd:Repair_Type) & ' --- (' & CLIP(LEFT(FORMAT(locRepairType,@n_14.2))) & ')'            
        END ! IF 
    if p_web.IfExistsSessionValue('tmp:WRepairType') = 0
      p_web.SetSessionValue('tmp:WRepairType',rtd:Repair_Type)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(locrepairType,rtd:Repair_Type,choose(rtd:Repair_Type = p_web.getsessionvalue('tmp:WRepairType')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(tmp:WRepairType_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(TRACHRGE)
  p_Web._CloseFile(SUBCHRGE)
  p_Web._CloseFile(STDCHRGE)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(CHARTYPE)
  p_Web._CloseFile(REPTYDEF)
  p_Web._CloseFile(USERS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('BillingConfirmation_' & p_web._nocolon('tmp:WRepairType') & '_value')

Comment::tmp:WRepairType  Routine
    loc:comment = ''
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WRepairType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:WConfirmRepairType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WConfirmRepairType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Confirm Repair Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:WConfirmRepairType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:WConfirmRepairType',p_web.GetValue('NewValue'))
    tmp:WConfirmRepairType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:WConfirmRepairType
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('tmp:WConfirmRepairType',p_web.GetValue('Value'))
    tmp:WConfirmRepairType = p_web.GetValue('Value')
  End
  do Value::tmp:WConfirmRepairType
  do SendAlert

Value::tmp:WConfirmRepairType  Routine
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WConfirmRepairType') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- tmp:WConfirmRepairType
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:WConfirmRepairType'',''billingconfirmation_tmp:wconfirmrepairtype_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Job:ViewOnly') = 1,'disabled','')
  If p_web.GetSessionValue('tmp:WConfirmRepairType') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','tmp:WConfirmRepairType',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('BillingConfirmation_' & p_web._nocolon('tmp:WConfirmRepairType') & '_value')

Comment::tmp:WConfirmRepairType  Routine
    loc:comment = ''
  p_web._DivHeader('BillingConfirmation_' & p_web._nocolon('tmp:WConfirmRepairType') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('BillingConfirmation_tmp:CChargeType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:CChargeType
      else
        do Value::tmp:CChargeType
      end
  of lower('BillingConfirmation_tmp:CRepairType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:CRepairType
      else
        do Value::tmp:CRepairType
      end
  of lower('BillingConfirmation_tmp:CConfirmRepairType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:CConfirmRepairType
      else
        do Value::tmp:CConfirmRepairType
      end
  of lower('BillingConfirmation_tmp:WChargeType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:WChargeType
      else
        do Value::tmp:WChargeType
      end
  of lower('BillingConfirmation_tmp:WRepairType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:WRepairType
      else
        do Value::tmp:WRepairType
      end
  of lower('BillingConfirmation_tmp:WConfirmRepairType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:WConfirmRepairType
      else
        do Value::tmp:WConfirmRepairType
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('BillingConfirmation_form:ready_',1)
  p_web.SetSessionValue('BillingConfirmation_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_BillingConfirmation',0)

PreCopy  Routine
  p_web.SetValue('BillingConfirmation_form:ready_',1)
  p_web.SetSessionValue('BillingConfirmation_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_BillingConfirmation',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('BillingConfirmation_form:ready_',1)
  p_web.SetSessionValue('BillingConfirmation_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('BillingConfirmation:Primed',0)

PreDelete       Routine
  p_web.SetValue('BillingConfirmation_form:ready_',1)
  p_web.SetSessionValue('BillingConfirmation_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('BillingConfirmation:Primed',0)
  p_web.setsessionvalue('showtab_BillingConfirmation',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('job:Chargeable_Job') = 'YES'
        If (p_web.GSV('Job:ViewOnly') = 1)
          If p_web.IfExistsValue('tmp:CConfirmRepairType') = 0
            p_web.SetValue('tmp:CConfirmRepairType',0)
            tmp:CConfirmRepairType = 0
          End
        End
  End
  If p_web.GSV('job:Warranty_job') = 'YES'
        If (p_web.GSV('Job:ViewOnly') = 1)
          If p_web.IfExistsValue('tmp:WConfirmRepairType') = 0
            p_web.SetValue('tmp:WConfirmRepairType',0)
            tmp:WConfirmRepairType = 0
          End
        End
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('BillingConfirmation_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  if (p_web.GSV('JobCompleteProcess') = 1)
      if (p_web.GSV('job:Chargeable_Job') = 'YES')
          if (p_web.GSV('tmp:CConfirmRepairType') = 0)
              loc:alert = 'You must confirm the chargeable repair type.'
              loc:invalid = 'tmp:CConfirmRepairType'
              exit
          end ! if (p_web.GSV('tmp:CConfirmRepairType') = 0)
      end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
  
      if (p_web.GSV('job:Warranty_Job') = 'YES')
          if (p_web.GSV('tmp:WConfirmRepairType') = 0)
              loc:alert = 'You must confirm the warranty repair type.'
              loc:invalid = 'tmp:WConfirmRepairType'
              exit
          end ! if (p_web.GSV('tmp:CConfirmRepairType') = 0)
      end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
  end ! if (p_web.GSV('JobCompleteProcess') = 1)
  
  IF (vod.RepairTypesNoParts(p_web.GSV('job:Chargeable_Job'), |
      p_web.GSV('job:Warranty_Job'), |
      p_web.GSV('job:Manufacturer'), |
      p_web.GSV('tmp:CRepairType'), |
      p_web.GSV('tmp:WRepairType')))
      ! #11762 Repair Type does not allow parts. 
      ! Check if there are any (Bryan: 24/11/2010)
      
      IF (vod.JobHasSparesAttached(p_web.GSV('job:Ref_Number',), |
          p_web.GSV('job:Estimate'), |
          p_web.GSV('job:Chargeable_Job'), |
          p_web.GSV('job:Warranty_Job')))
          loc:Invalid = 'tmp:ConfirmRepairType'
          loc:Alert = 'The selected Repair Type(s) require that there are no parts attached to the job.'
          EXIT
      END
  END
  
  
  p_web.DeleteSessionValue('BillingConfirmation_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
  If p_web.GSV('job:Chargeable_Job') = 'YES'
    loc:InvalidTab += 1
          tmp:CChargeType = Upper(tmp:CChargeType)
          p_web.SetSessionValue('tmp:CChargeType',tmp:CChargeType)
        If loc:Invalid <> '' then exit.
          tmp:CRepairType = Upper(tmp:CRepairType)
          p_web.SetSessionValue('tmp:CRepairType',tmp:CRepairType)
        If loc:Invalid <> '' then exit.
  End
  ! tab = 2
  If p_web.GSV('job:Warranty_job') = 'YES'
    loc:InvalidTab += 1
          tmp:WChargeType = Upper(tmp:WChargeType)
          p_web.SetSessionValue('tmp:WChargeType',tmp:WChargeType)
        If loc:Invalid <> '' then exit.
          tmp:WRepairType = Upper(tmp:WRepairType)
          p_web.SetSessionValue('tmp:WRepairType',tmp:WRepairType)
        If loc:Invalid <> '' then exit.
  End
  ! The following fields are not on the form, but need to be checked anyway.
    if (p_web.GSV('job:Chargeable_Job') = 'YES') 
        p_web.SSV('job:Charge_Type',p_web.GSV('tmp:CChargeType'))
        p_web.SSV('job:Repair_Type',p_web.GSV('tmp:CRepairType'))
          
        if (p_web.GSV('job:Estimate') <> 'YES') ! #11659 No need to set to estimate. If it already is. (Bryan: 31/08/2010)
            CASE vod.EstimateRequired(p_web.GSV('job:Charge_Type'),p_web.GSV('job:Account_Number'))
            OF 1 ! Make Esimate
                p_web.SSV('job:Estimate','YES')
            OF 2 ! Don't make esimate
                p_web.SSV('job:Estimate','NO')
            ELSE
            END        
        END
          
    end ! if (p_web.GSV('job:Chargeable_Job') = 'YES')
  
    if (p_web.GSV('job:Warranty_job') = 'YES')
        if (p_web.GSV('job:Warranty_Charge_Type') <> p_web.GSV('tmp:WChargeType') Or |
            p_web.GSV('job:Repair_Type_Warranty') <> p_web.GSV('tmp:WRepairType'))
  
            p_web.SSV('job:Warranty_Charge_Type',p_web.GSV('tmp:WChargeType'))
            p_web.SSV('job:Repair_Type_Warranty',p_web.GSV('tmp:WRepairType'))
            p_web.SSV('JobPricingRoutine:ForceWarranty',1)
            JobPricingRoutine(p_web)
        end !
    end ! if (p_web.GSV('job:Warranty_job') = 'YES')
  
    p_web.SSV('jobe:COverwriteRepairType',p_web.GSV('tmp:CConfirmRepairType'))
    p_web.SSV('jobe:WOverwriteRepairType',p_web.GSV('tmp:WConfirmRepairType'))
  
  
    CalculateBilling(p_web)
  
    p_web.DeleteSessionValue('passedURL') 
  
  
      
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('BillingConfirmation:Primed',0)
  p_web.StoreValue('tmp:CChargeType')
  p_web.StoreValue('tmp:CRepairType')
  p_web.StoreValue('tmp:CConfirmRepairType')
  p_web.StoreValue('tmp:WChargeType')
  p_web.StoreValue('tmp:WRepairType')
  p_web.StoreValue('tmp:WConfirmRepairType')
GetCost             PROCEDURE(STRING pChargeType,*STRING pCost)!,LONG
locCost                 STRING(30)
foundCost   LONG()
    CODE
        foundCost = 0
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = p_web.GSV('job:Account_Number')
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = sub:Main_Account_Number
            IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                IF (tra:Invoice_Sub_Accounts = 'YES' AND tra:Use_Sub_Accounts = 'YES')
                    Access:SUBCHRGE.ClearKey(suc:Model_Repair_Type_Key)
                    suc:Account_Number = sub:Account_Number
                    suc:Model_Number = p_web.GSV('job:Model_Number')
                    suc:Charge_Type = pChargeType
                    suc:Unit_Type = p_web.GSV('job:Unit_Type')
                    suc:Repair_Type = rtd:Repair_Type
                    IF (Access:SUBCHRGE.TryFetch(suc:Model_Repair_Type_Key) = Level:Benign)
                        pCost = suc:RRCRate
                        foundCost = 1
                    END ! IF
                END ! IF
                
                IF (found# = 0)
                    Access:TRACHRGE.ClearKey(trc:Model_Repair_Key)
                    trc:Account_Number = sub:Account_Number
                    trc:Model_Number = p_web.GSV('job:Model_Number')
                    trc:Charge_Type = pChargeType
                    trc:Unit_Type = p_web.GSV('job:Unit_Type')
                    trc:Repair_Type = rtd:Repair_Type
                    IF (Access:TRACHRGE.TryFetch(trc:Model_Repair_Key) = Level:Benign)
                        pCost = trc:RRCRate
                        foundCost = 1
                    ELSE ! IF
                    END ! IF        
                END ! IF
                
            END ! IF
        END ! IF

        IF (found# = 0)
            Access:STDCHRGE.ClearKey(sta:Model_Number_Charge_Key)
            sta:Model_Number = p_web.GSV('job:Model_Number')
            sta:Charge_Type = pChargeType
            sta:Unit_Type = p_web.GSV('job:Unit_Type')
            sta:Repair_Type = rtd:Repair_Type
            IF (Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign)
                pCost = sta:RRCRate
                foundCost = 1
            END ! IF
        END ! IF

        RETURN foundCost
