

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER213.INC'),ONCE        !Local module procedure declarations
                     END


FormDepleteStock     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
LocRefNumber         LONG                                  !
locQuantity          LONG                                  !
locAdditionalNotes   STRING(255)                           !
FilesOpened     Long
STOMODEL_ALIAS::State  USHORT
STOCK::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormDepleteStock')
  loc:formname = 'FormDepleteStock_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormDepleteStock',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormDepleteStock','')
    p_web._DivHeader('FormDepleteStock',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormDepleteStock',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormDepleteStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormDepleteStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormDepleteStock',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormDepleteStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormDepleteStock',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormDepleteStock',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(STOMODEL_ALIAS)
  p_web._OpenFile(STOCK)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(STOMODEL_ALIAS)
  p_Web._CloseFile(STOCK)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  !look up relevant stock record
  
  !attempt to get at stm:RefNumber (in "BrowseID") from BrowseStoModel - did not work 
  !p_web.ssv('BrowseID','STOMODEL') flags this has come from STOMODEL screen
  if p_web.gsv('BrowseID') = 'STOMODEL' THEN
      Access:StoModel_Alias.clearkey(stm_ali:RecordNumberKey)
      stm_ali:RecordNumber = p_web.gsv('BrowseID2')
      if access:StoModel_Alias.fetch(stm_ali:RecordNumberKey)
          !error
      END
      LocRefNumber = stm_ali:Ref_Number
  ELSE
      LocRefNumber = p_web.gsv('BrowseID')
  END
  
  
  !now we can look up the stock
  Access:Stock.ClearKey(sto:Ref_Number_Key)
  sto:Ref_Number = LocRefNumber       !was p_web.GSV('BrowseID')
  IF (Access:STOCK.TryFetch(sto:Ref_Number_Key))
  END
  p_web.FileToSessionQueue(STOCK)
  p_web.SSV('locQuantity',1)
  p_web.SSV('locAdditionalNotes','')
  p_web.SetValue('FormDepleteStock_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('sto:Part_Number')
    p_web.SetPicture('sto:Part_Number','@s30')
  End
  p_web.SetSessionPicture('sto:Part_Number','@s30')
  If p_web.IfExistsValue('sto:Description')
    p_web.SetPicture('sto:Description','@s30')
  End
  p_web.SetSessionPicture('sto:Description','@s30')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('sto:Part_Number',sto:Part_Number)
  p_web.SetSessionValue('sto:Description',sto:Description)
  p_web.SetSessionValue('locQuantity',locQuantity)
  p_web.SetSessionValue('locAdditionalNotes',locAdditionalNotes)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('sto:Part_Number')
    sto:Part_Number = p_web.GetValue('sto:Part_Number')
    p_web.SetSessionValue('sto:Part_Number',sto:Part_Number)
  End
  if p_web.IfExistsValue('sto:Description')
    sto:Description = p_web.GetValue('sto:Description')
    p_web.SetSessionValue('sto:Description',sto:Description)
  End
  if p_web.IfExistsValue('locQuantity')
    locQuantity = p_web.GetValue('locQuantity')
    p_web.SetSessionValue('locQuantity',locQuantity)
  End
  if p_web.IfExistsValue('locAdditionalNotes')
    locAdditionalNotes = p_web.GetValue('locAdditionalNotes')
    p_web.SetSessionValue('locAdditionalNotes',locAdditionalNotes)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormDepleteStock_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  ! Store return URL
    IF (p_web.IfExistsValue('ReturnURL'))
        p_web.StoreValue('ReturnURL')
    END
    
    ! #13128 Error if part not selected. (DBH: 18/03/2014)
    IF (p_web.GSV('BrowseID') = '')
        CreateScript(p_web,packet,'alert("Please select a part.");window.open("' & p_web.GSV('ReturnURL') & '","_self")')
        DO SendPacket
    END ! IF
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locQuantity = p_web.RestoreValue('locQuantity')
 locAdditionalNotes = p_web.RestoreValue('locAdditionalNotes')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('ReturnURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormDepleteStock_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormDepleteStock_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormDepleteStock_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ReturnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormDepleteStock" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormDepleteStock" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormDepleteStock" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Deplete Stock') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Deplete Stock',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormDepleteStock">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormDepleteStock" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormDepleteStock')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Details') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormDepleteStock')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormDepleteStock'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locQuantity')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormDepleteStock')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Part Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormDepleteStock_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Part_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Part_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sto:Part_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Description
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::sto:Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locQuantity
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locQuantity
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locQuantity
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAdditionalNotes
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAdditionalNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAdditionalNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::sto:Part_Number  Routine
  p_web._DivHeader('FormDepleteStock_' & p_web._nocolon('sto:Part_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Part Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Part_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Part_Number',p_web.GetValue('NewValue'))
    sto:Part_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = sto:Part_Number
    do Value::sto:Part_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Part_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sto:Part_Number = p_web.GetValue('Value')
  End

Value::sto:Part_Number  Routine
  p_web._DivHeader('FormDepleteStock_' & p_web._nocolon('sto:Part_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:Part_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('sto:Part_Number'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::sto:Part_Number  Routine
    loc:comment = ''
  p_web._DivHeader('FormDepleteStock_' & p_web._nocolon('sto:Part_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::sto:Description  Routine
  p_web._DivHeader('FormDepleteStock_' & p_web._nocolon('sto:Description') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Description')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Description  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Description',p_web.GetValue('NewValue'))
    sto:Description = p_web.GetValue('NewValue') !FieldType= STRING Field = sto:Description
    do Value::sto:Description
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Description',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sto:Description = p_web.GetValue('Value')
  End

Value::sto:Description  Routine
  p_web._DivHeader('FormDepleteStock_' & p_web._nocolon('sto:Description') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:Description
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('sto:Description'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::sto:Description  Routine
    loc:comment = ''
  p_web._DivHeader('FormDepleteStock_' & p_web._nocolon('sto:Description') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locQuantity  Routine
  p_web._DivHeader('FormDepleteStock_' & p_web._nocolon('locQuantity') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Quantity')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locQuantity  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locQuantity',p_web.GetValue('NewValue'))
    locQuantity = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locQuantity
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locQuantity',p_web.GetValue('Value'))
    locQuantity = p_web.GetValue('Value')
  End
  If locQuantity = ''
    loc:Invalid = 'locQuantity'
    loc:alert = p_web.translate('Quantity') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  If Numeric(locQuantity) = 0
    loc:Invalid = 'locQuantity'
    loc:alert = p_web.translate('Quantity') & ' ' & p_web.translate(p_web.site.NumericText)
  End
    locQuantity = Upper(locQuantity)
    p_web.SetSessionValue('locQuantity',locQuantity)
  do Value::locQuantity
  do SendAlert

Value::locQuantity  Routine
  p_web._DivHeader('FormDepleteStock_' & p_web._nocolon('locQuantity') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locQuantity
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locQuantity')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locQuantity = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
    If Numeric(locQuantity) = 0
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    Else
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locQuantity'',''formdepletestock_locquantity_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locQuantity',p_web.GetSessionValueFormat('locQuantity'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormDepleteStock_' & p_web._nocolon('locQuantity') & '_value')

Comment::locQuantity  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
    loc:comment = clip(loc:comment) & ' ' & p_web.translate(p_web.site.NumericText)
  p_web._DivHeader('FormDepleteStock_' & p_web._nocolon('locQuantity') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAdditionalNotes  Routine
  p_web._DivHeader('FormDepleteStock_' & p_web._nocolon('locAdditionalNotes') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Additional Notes')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAdditionalNotes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAdditionalNotes',p_web.GetValue('NewValue'))
    locAdditionalNotes = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAdditionalNotes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAdditionalNotes',p_web.GetValue('Value'))
    locAdditionalNotes = p_web.GetValue('Value')
  End
  do Value::locAdditionalNotes
  do SendAlert

Value::locAdditionalNotes  Routine
  p_web._DivHeader('FormDepleteStock_' & p_web._nocolon('locAdditionalNotes') & '_value','adiv')
  loc:extra = ''
  ! --- TEXT --- locAdditionalNotes
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locAdditionalNotes')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locAdditionalNotes'',''formdepletestock_locadditionalnotes_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('locAdditionalNotes',p_web.GetSessionValue('locAdditionalNotes'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locAdditionalNotes),,Net:Web:Control) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormDepleteStock_' & p_web._nocolon('locAdditionalNotes') & '_value')

Comment::locAdditionalNotes  Routine
      loc:comment = ''
  p_web._DivHeader('FormDepleteStock_' & p_web._nocolon('locAdditionalNotes') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormDepleteStock_locQuantity_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locQuantity
      else
        do Value::locQuantity
      end
  of lower('FormDepleteStock_locAdditionalNotes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAdditionalNotes
      else
        do Value::locAdditionalNotes
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormDepleteStock_form:ready_',1)
  p_web.SetSessionValue('FormDepleteStock_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormDepleteStock',0)

PreCopy  Routine
  p_web.SetValue('FormDepleteStock_form:ready_',1)
  p_web.SetSessionValue('FormDepleteStock_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormDepleteStock',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormDepleteStock_form:ready_',1)
  p_web.SetSessionValue('FormDepleteStock_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormDepleteStock:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormDepleteStock_form:ready_',1)
  p_web.SetSessionValue('FormDepleteStock_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormDepleteStock:Primed',0)
  p_web.setsessionvalue('showtab_FormDepleteStock',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormDepleteStock_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  Access:STOCK.Clearkey(sto:Ref_Number_Key)
  sto:Ref_Number  = p_web.GSV('BrowseID')
  If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
      !Found
  
      IF (p_web.GSV('locQuantity') > sto:Quantity_Stock)
          loc:Alert = 'You cannot decrement the stock level below zero.'
          loc:Invalid = 'locQuantity'
          EXIT
      END
      
      Pointer# = Pointer(STOCK)
      Hold(STOCK,1)
      Get(STOCK,Pointer#)
      If Errorcode() = 43
          loc:Alert = 'This stock item is currently in use. Please try again in a moment.'
          loc:Invalid = 'locQuantity'
          EXIT
      END
      sto:Quantity_Stock -= p_web.GSV('locQuantity')
      
      IF (sto:Quantity_Stock < 0)
          sto:Quantity_Stock = 0
      END
      
      IF (sto:Location <> VodacomClass.MainStoreLocation())
          IF (VodacomClass.MainStoreSuspended(sto:Part_Number))
              sto:Suspend = 1
              loc:Alert = 'There are no more items in stock. This part has been suspended.'
          END
      END
      IF (Access:STOCK.TryUpdate() = Level:Benign)
          If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
              'DEC', | ! Transaction_Type
              '', | ! Depatch_Note_Number
              0, | ! Job_Number
              0, | ! Sales_Number
              p_web.GSV('locQuantity'), | ! Quantity
              sto:Purchase_Cost, | ! Purchase_Cost
              sto:Sale_Cost, | ! Sale_Cost
              sto:Retail_Cost, | ! Retail_Cost
              'STOCK DECREMENTED', | ! Notes
              Clip(p_web.GSV('locAdditionalNotes')),|
              p_web.GSV('BookingUsercode'), |
              sto:Quantity_stock) ! Information
                            ! Added OK
          Else ! AddToStockHistory
                            ! Error
          End ! AddToStockHistory
          
          if (sto:Suspend)
              if (AddToStockHistory(sto:Ref_Number, |
                  'ADD',|
                  '',|
                  0, |
                  0, |
                  0, |
                  sto:Purchase_Cost,|
                  sto:Sale_Cost, |
                  sto:Retail_Cost, |
                  'PART SUSPENDED',|
                  '',|
                  p_web.GSV('BookingUsercode'), |
                  sto:Quantity_stock))
              end
          end
      END
      
  END
  p_web.DeleteSessionValue('FormDepleteStock_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
        If locQuantity = ''
          loc:Invalid = 'locQuantity'
          loc:alert = p_web.translate('Quantity') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If Numeric(locQuantity) = 0
          loc:Invalid = 'locQuantity'
          loc:alert = p_web.translate('Quantity') & ' ' & p_web.translate(p_web.site.NumericText)
        End
          locQuantity = Upper(locQuantity)
          p_web.SetSessionValue('locQuantity',locQuantity)
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormDepleteStock:Primed',0)
  p_web.StoreValue('sto:Part_Number')
  p_web.StoreValue('sto:Description')
  p_web.StoreValue('locQuantity')
  p_web.StoreValue('locAdditionalNotes')
