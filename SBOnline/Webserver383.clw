

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER383.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER010.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER195.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER377.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER385.INC'),ONCE        !Req'd for module callout resolution
                     END


FormOrderExchangeLoanUnits PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locOrderManufacturer STRING(30)                            !
locOrderModelNumber  STRING(30)                            !
locOrderQuantity     LONG                                  !
locOrderNotes        STRING(255)                           !
                    MAP 
ClearVariables          PROCEDURE()
AddToOrder              PROCEDURE()
                    END ! MAP
FilesOpened     Long
SBO_GenericFile::State  USHORT
MODEXCHA::State  USHORT
STOCK::State  USHORT
MODELNUM::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormOrderExchangeLoanUnits')
  loc:formname = 'FormOrderExchangeLoanUnits_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormOrderExchangeLoanUnits',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormOrderExchangeLoanUnits','')
    p_web._DivHeader('FormOrderExchangeLoanUnits',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormOrderExchangeLoanUnits',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormOrderExchangeLoanUnits',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormOrderExchangeLoanUnits',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormOrderExchangeLoanUnits',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormOrderExchangeLoanUnits',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormOrderExchangeLoanUnits',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormOrderExchangeLoanUnits',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(SBO_GenericFile)
  p_web._OpenFile(MODEXCHA)
  p_web._OpenFile(STOCK)
  p_web._OpenFile(MODELNUM)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(SBO_GenericFile)
  p_Web._CloseFile(MODEXCHA)
  p_Web._CloseFile(STOCK)
  p_Web._CloseFile(MODELNUM)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormOrderExchangeLoanUnits_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('locOrderQuantity')
    p_web.SetPicture('locOrderQuantity','@n_8')
  End
  p_web.SetSessionPicture('locOrderQuantity','@n_8')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'locOrderModelNumber'
    p_web.setsessionvalue('showtab_FormOrderExchangeLoanUnits',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODELNUM)
        p_web.setsessionvalue('locOrderManufacturer',mod:Manufacturer)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locOrderManufacturer')
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locOrderModelNumber',locOrderModelNumber)
  p_web.SetSessionValue('locOrderManufacturer',locOrderManufacturer)
  p_web.SetSessionValue('locOrderQuantity',locOrderQuantity)
  p_web.SetSessionValue('locOrderNotes',locOrderNotes)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locOrderModelNumber')
    locOrderModelNumber = p_web.GetValue('locOrderModelNumber')
    p_web.SetSessionValue('locOrderModelNumber',locOrderModelNumber)
  End
  if p_web.IfExistsValue('locOrderManufacturer')
    locOrderManufacturer = p_web.GetValue('locOrderManufacturer')
    p_web.SetSessionValue('locOrderManufacturer',locOrderManufacturer)
  End
  if p_web.IfExistsValue('locOrderQuantity')
    locOrderQuantity = p_web.dformat(clip(p_web.GetValue('locOrderQuantity')),'@n_8')
    p_web.SetSessionValue('locOrderQuantity',locOrderQuantity)
  End
  if p_web.IfExistsValue('locOrderNotes')
    locOrderNotes = p_web.GetValue('locOrderNotes')
    p_web.SetSessionValue('locOrderNotes',locOrderNotes)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormOrderExchangeLoanUnits_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    IF (p_web.GetValue('FirstTime') = 1)
        ClearVariables()
        ClearSBOGenericFile(p_web)
    END ! IF
    
    IF (p_web.IfExistsValue('OrderType'))
        p_web.StoreValue('OrderType')
    END ! IF
    
    IF (p_web.GSV('OrderType') = 'E')
        p_web.SSV('ReturnURL','FormAllExchangeUnits')
    END ! IF
    IF (p_web.GSV('OrderType') = 'L')
        p_web.SSV('ReturnURL','FormAllLoanUnits')
    END ! IF
      p_web.site.SaveButton.TextValue = 'Create Order'
      p_web.site.SaveButton.Class = 'button-entryfield'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locOrderModelNumber = p_web.RestoreValue('locOrderModelNumber')
 locOrderManufacturer = p_web.RestoreValue('locOrderManufacturer')
 locOrderQuantity = p_web.RestoreValue('locOrderQuantity')
 locOrderNotes = p_web.RestoreValue('locOrderNotes')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormOrderExchangeLoanUnits')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormOrderExchangeLoanUnits_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormOrderExchangeLoanUnits_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormOrderExchangeLoanUnits_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ReturnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormOrderExchangeLoanUnits" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormOrderExchangeLoanUnits" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormOrderExchangeLoanUnits" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Order Units') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Order Units',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormOrderExchangeLoanUnits">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormOrderExchangeLoanUnits" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormOrderExchangeLoanUnits')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Units Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Models To Order') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormOrderExchangeLoanUnits')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormOrderExchangeLoanUnits'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormOrderExchangeLoanUnits_BrowseExchangeLoanOrders_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='MODELNUM'
            p_web.SetValue('SelectField',clip(loc:formname) & '.locOrderQuantity')
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locOrderModelNumber')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormOrderExchangeLoanUnits')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Units Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormOrderExchangeLoanUnits_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Units Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Units Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Units Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Units Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locOrderModelNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locOrderModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locOrderModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locOrderManufacturer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locOrderManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locOrderManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locOrderQuantity
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locOrderQuantity
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locOrderQuantity
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locOrderNotes
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locOrderNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locOrderNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::btnAddToOrder
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnAddToOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnAddToOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Models To Order') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormOrderExchangeLoanUnits_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Models To Order')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Models To Order')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Models To Order')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Models To Order')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::brwOrders
      do Value::brwOrders
      do Comment::brwOrders
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::btnCreateOrder
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnCreateOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnCreateOrder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locOrderModelNumber  Routine
  p_web._DivHeader('FormOrderExchangeLoanUnits_' & p_web._nocolon('locOrderModelNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Model Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locOrderModelNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locOrderModelNumber',p_web.GetValue('NewValue'))
    locOrderModelNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locOrderModelNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locOrderModelNumber',p_web.GetValue('Value'))
    locOrderModelNumber = p_web.GetValue('Value')
  End
  If locOrderModelNumber = ''
    loc:Invalid = 'locOrderModelNumber'
    loc:alert = p_web.translate('Model Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locOrderModelNumber = Upper(locOrderModelNumber)
    p_web.SetSessionValue('locOrderModelNumber',locOrderModelNumber)
  p_Web.SetValue('lookupfield','locOrderModelNumber')
  do AfterLookup
  do Value::locOrderManufacturer
  do Value::locOrderModelNumber
  do SendAlert
  do Comment::locOrderModelNumber
  do Value::locOrderManufacturer  !1

Value::locOrderModelNumber  Routine
  p_web._DivHeader('FormOrderExchangeLoanUnits_' & p_web._nocolon('locOrderModelNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locOrderModelNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locOrderModelNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locOrderModelNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locOrderModelNumber'',''formorderexchangeloanunits_locordermodelnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locOrderModelNumber')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','locOrderModelNumber',p_web.GetSessionValueFormat('locOrderModelNumber'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('FormManModelNumbers?LookupField=locOrderModelNumber&Tab=1&ForeignField=mod:Model_Number&_sort=&Refresh=sort&LookupFrom=FormOrderExchangeLoanUnits&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormOrderExchangeLoanUnits_' & p_web._nocolon('locOrderModelNumber') & '_value')

Comment::locOrderModelNumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormOrderExchangeLoanUnits_' & p_web._nocolon('locOrderModelNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormOrderExchangeLoanUnits_' & p_web._nocolon('locOrderModelNumber') & '_comment')

Prompt::locOrderManufacturer  Routine
  p_web._DivHeader('FormOrderExchangeLoanUnits_' & p_web._nocolon('locOrderManufacturer') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Manufacturer:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormOrderExchangeLoanUnits_' & p_web._nocolon('locOrderManufacturer') & '_prompt')

Validate::locOrderManufacturer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locOrderManufacturer',p_web.GetValue('NewValue'))
    locOrderManufacturer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locOrderManufacturer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locOrderManufacturer',p_web.GetValue('Value'))
    locOrderManufacturer = p_web.GetValue('Value')
  End

Value::locOrderManufacturer  Routine
  p_web._DivHeader('FormOrderExchangeLoanUnits_' & p_web._nocolon('locOrderManufacturer') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locOrderManufacturer
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('bold green')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locOrderManufacturer'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormOrderExchangeLoanUnits_' & p_web._nocolon('locOrderManufacturer') & '_value')

Comment::locOrderManufacturer  Routine
    loc:comment = ''
  p_web._DivHeader('FormOrderExchangeLoanUnits_' & p_web._nocolon('locOrderManufacturer') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormOrderExchangeLoanUnits_' & p_web._nocolon('locOrderManufacturer') & '_comment')

Prompt::locOrderQuantity  Routine
  p_web._DivHeader('FormOrderExchangeLoanUnits_' & p_web._nocolon('locOrderQuantity') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Quantity Required')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locOrderQuantity  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locOrderQuantity',p_web.GetValue('NewValue'))
    locOrderQuantity = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locOrderQuantity
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locOrderQuantity',p_web.dFormat(p_web.GetValue('Value'),'@n_8'))
    locOrderQuantity = p_web.Dformat(p_web.GetValue('Value'),'@n_8') !
  End
  If locOrderQuantity = ''
    loc:Invalid = 'locOrderQuantity'
    loc:alert = p_web.translate('Quantity Required') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::locOrderQuantity
  do SendAlert

Value::locOrderQuantity  Routine
  p_web._DivHeader('FormOrderExchangeLoanUnits_' & p_web._nocolon('locOrderQuantity') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locOrderQuantity
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locOrderQuantity')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locOrderQuantity = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locOrderQuantity'',''formorderexchangeloanunits_locorderquantity_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locOrderQuantity',p_web.GetSessionValue('locOrderQuantity'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n_8',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormOrderExchangeLoanUnits_' & p_web._nocolon('locOrderQuantity') & '_value')

Comment::locOrderQuantity  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormOrderExchangeLoanUnits_' & p_web._nocolon('locOrderQuantity') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locOrderNotes  Routine
  p_web._DivHeader('FormOrderExchangeLoanUnits_' & p_web._nocolon('locOrderNotes') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Notes')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locOrderNotes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locOrderNotes',p_web.GetValue('NewValue'))
    locOrderNotes = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locOrderNotes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locOrderNotes',p_web.GetValue('Value'))
    locOrderNotes = p_web.GetValue('Value')
  End
    locOrderNotes = Upper(locOrderNotes)
    p_web.SetSessionValue('locOrderNotes',locOrderNotes)
  do Value::locOrderNotes
  do SendAlert

Value::locOrderNotes  Routine
  p_web._DivHeader('FormOrderExchangeLoanUnits_' & p_web._nocolon('locOrderNotes') & '_value','adiv')
  loc:extra = ''
  ! --- TEXT --- locOrderNotes
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locOrderNotes')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locOrderNotes'',''formorderexchangeloanunits_locordernotes_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('locOrderNotes',p_web.GetSessionValue('locOrderNotes'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,255,,Net:Web:Control) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormOrderExchangeLoanUnits_' & p_web._nocolon('locOrderNotes') & '_value')

Comment::locOrderNotes  Routine
      loc:comment = ''
  p_web._DivHeader('FormOrderExchangeLoanUnits_' & p_web._nocolon('locOrderNotes') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::btnAddToOrder  Routine
  p_web._DivHeader('FormOrderExchangeLoanUnits_' & p_web._nocolon('btnAddToOrder') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::btnAddToOrder  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnAddToOrder',p_web.GetValue('NewValue'))
    do Value::btnAddToOrder
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    AddToOrder()  
    
  do SendAlert
  do Value::btnAddToOrder  !1

Value::btnAddToOrder  Routine
  p_web._DivHeader('FormOrderExchangeLoanUnits_' & p_web._nocolon('btnAddToOrder') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnAddToOrder'',''formorderexchangeloanunits_btnaddtoorder_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnAddToOrder','Add To Order','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormOrderExchangeLoanUnits_' & p_web._nocolon('btnAddToOrder') & '_value')

Comment::btnAddToOrder  Routine
    loc:comment = ''
  p_web._DivHeader('FormOrderExchangeLoanUnits_' & p_web._nocolon('btnAddToOrder') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::brwOrders  Routine
  p_web._DivHeader('FormOrderExchangeLoanUnits_' & p_web._nocolon('brwOrders') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::brwOrders  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwOrders',p_web.GetValue('NewValue'))
    do Value::brwOrders
  Else
    p_web.StoreValue('sbogen:RecordNumber')
  End

Value::brwOrders  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseExchangeLoanOrders --
  p_web.SetValue('BrowseExchangeLoanOrders:NoForm',1)
  p_web.SetValue('BrowseExchangeLoanOrders:FormName',loc:formname)
  p_web.SetValue('BrowseExchangeLoanOrders:parentIs','Form')
  p_web.SetValue('_parentProc','FormOrderExchangeLoanUnits')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormOrderExchangeLoanUnits_BrowseExchangeLoanOrders_embedded_div')&'"><!-- Net:BrowseExchangeLoanOrders --></div><13,10>'
    p_web._DivHeader('FormOrderExchangeLoanUnits_' & lower('BrowseExchangeLoanOrders') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormOrderExchangeLoanUnits_' & lower('BrowseExchangeLoanOrders') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseExchangeLoanOrders --><13,10>'
  end
  do SendPacket

Comment::brwOrders  Routine
    loc:comment = ''
  p_web._DivHeader('FormOrderExchangeLoanUnits_' & p_web._nocolon('brwOrders') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::btnCreateOrder  Routine
  p_web._DivHeader('FormOrderExchangeLoanUnits_' & p_web._nocolon('btnCreateOrder') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::btnCreateOrder  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnCreateOrder',p_web.GetValue('NewValue'))
    do Value::btnCreateOrder
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnCreateOrder  Routine
  p_web._DivHeader('FormOrderExchangeLoanUnits_' & p_web._nocolon('btnCreateOrder') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('simpleQuestion(''Create Order'',''Are you sure you want to create the order?'',''PageProcess?ProcessType=CreateExchangeLoanOrder&ReturnURL=FormOrderExchangeLoanUnits&RedirectURL=PrintGenericDocument'',''#'')')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnCreateOrder','Create Order','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()

Comment::btnCreateOrder  Routine
    loc:comment = ''
  p_web._DivHeader('FormOrderExchangeLoanUnits_' & p_web._nocolon('btnCreateOrder') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormOrderExchangeLoanUnits_locOrderModelNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locOrderModelNumber
      else
        do Value::locOrderModelNumber
      end
  of lower('FormOrderExchangeLoanUnits_locOrderQuantity_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locOrderQuantity
      else
        do Value::locOrderQuantity
      end
  of lower('FormOrderExchangeLoanUnits_locOrderNotes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locOrderNotes
      else
        do Value::locOrderNotes
      end
  of lower('FormOrderExchangeLoanUnits_btnAddToOrder_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnAddToOrder
      else
        do Value::btnAddToOrder
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormOrderExchangeLoanUnits_form:ready_',1)
  p_web.SetSessionValue('FormOrderExchangeLoanUnits_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormOrderExchangeLoanUnits',0)

PreCopy  Routine
  p_web.SetValue('FormOrderExchangeLoanUnits_form:ready_',1)
  p_web.SetSessionValue('FormOrderExchangeLoanUnits_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormOrderExchangeLoanUnits',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormOrderExchangeLoanUnits_form:ready_',1)
  p_web.SetSessionValue('FormOrderExchangeLoanUnits_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormOrderExchangeLoanUnits:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormOrderExchangeLoanUnits_form:ready_',1)
  p_web.SetSessionValue('FormOrderExchangeLoanUnits_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormOrderExchangeLoanUnits:Primed',0)
  p_web.setsessionvalue('showtab_FormOrderExchangeLoanUnits',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormOrderExchangeLoanUnits_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormOrderExchangeLoanUnits_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
        If locOrderModelNumber = ''
          loc:Invalid = 'locOrderModelNumber'
          loc:alert = p_web.translate('Model Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locOrderModelNumber = Upper(locOrderModelNumber)
          p_web.SetSessionValue('locOrderModelNumber',locOrderModelNumber)
        If loc:Invalid <> '' then exit.
        If locOrderQuantity = ''
          loc:Invalid = 'locOrderQuantity'
          loc:alert = p_web.translate('Quantity Required') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
          locOrderNotes = Upper(locOrderNotes)
          p_web.SetSessionValue('locOrderNotes',locOrderNotes)
        If loc:Invalid <> '' then exit.
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormOrderExchangeLoanUnits:Primed',0)
  p_web.StoreValue('locOrderModelNumber')
  p_web.StoreValue('locOrderManufacturer')
  p_web.StoreValue('locOrderQuantity')
  p_web.StoreValue('locOrderNotes')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locOrderManufacturer',locOrderManufacturer) ! STRING(30)
     p_web.SSV('locOrderModelNumber',locOrderModelNumber) ! STRING(30)
     p_web.SSV('locOrderQuantity',locOrderQuantity) ! LONG
     p_web.SSV('locOrderNotes',locOrderNotes) ! STRING(255)
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locOrderManufacturer = p_web.GSV('locOrderManufacturer') ! STRING(30)
     locOrderModelNumber = p_web.GSV('locOrderModelNumber') ! STRING(30)
     locOrderQuantity = p_web.GSV('locOrderQuantity') ! LONG
     locOrderNotes = p_web.GSV('locOrderNotes') ! STRING(255)
AddToOrder          PROCEDURE()!
    CODE
        
        
        IF (p_web.GSV('locOrderModelNumber') = '')
            loc:alert = 'Model Number Required'
            loc:invalid = 'locOrderModelNumber'
            DO Value::locOrderModelNumber
            RETURN
        END ! IF
        IF (p_web.GSV('locOrderQuantity') = 0)
            loc:alert = 'Quantity Required'
            loc:invalid = 'locOrderQuantity'
            DO Value::locOrderQuantity
            RETURN
        END ! IF
		
        Access:SBO_GenericFile.ClearKey(sbogen:String1Key)
        sbogen:SessionID = p_web.SessionID
        sbogen:String1	 = p_web.GSV('locOrderModelNumber')
        IF (Access:SBO_GenericFile.TryFetch(sbogen:String1Key) = Level:Benign)
            loc:alert = 'Error. You have already requested this model'
            loc:invalid = 'locOrderModelNumber'
            DO Value::locOrderModelNumber
            RETURN
        END ! IF
		
        CASE p_web.GSV('OrderType')
        OF 'E'
            Access:STOCK.ClearKey(sto:ExchangeModelKey)
            sto:Location = 'MAIN STORE'
            sto:Manufacturer = p_web.GSV('locOrderManufacturer')
            sto:ExchangeModelNumber = p_web.GSV('locOrderModelNumber')
            IF (Access:STOCK.TryFetch(sto:ExchangeModelKey) = Level:Benign)
				
            ELSE ! IF
                loc:alert = 'Error. The selected Model is not available to be ordered.'
                loc:invalid = 'locOrderModelNumber'
                DO Value::locOrderModelNumber
                RETURN
            END ! IF
			
            Access:MODEXCHA.ClearKey(moa:AccountNumberKey)
            moa:AccountNumber = p_web.GSV('BookingAccount')
            moa:Manufacturer = p_web.GSV('locOrderManufacturer')
            moa:ModelNumber = p_web.GSV('locOrderModelNumber')
            IF (Access:MODEXCHA.TryFetch(moa:AccountNumberKey) = Level:Benign)
				
            ELSE ! IF
                loc:alert = 'Error! You have not been authorised to order this model number.'
                loc:invalid = 'locOrderModelNumber'
                DO Value::locOrderModelNumber
                RETURN
            END ! IF
			
            IF (sto:ExchangeOrderCap > 0 AND p_web.GSV('locOrderQuantity') > sto:ExchangeOrderCap)
                loc:alert = 'Error. The maximum quantity you can order for this model is ' & Clip(sto:ExchangeOrderCap) & '.'
                loc:invalid = 'locOrderModelNumber'
                DO Value::locOrderModelNumber
                RETURN
            END ! IF
			
        OF 'L'
            Access:STOCK.ClearKey(sto:LoanModelKey)
            sto:Location = 'MAIN STORE'
            sto:Manufacturer = p_web.GSV('locOrderManufacturer')
            sto:LoanModelNumber = p_web.GSV('locOrderModelNumber')
            IF (Access:STOCK.TryFetch(sto:LoanModelKey) = Level:Benign)
				
            ELSE ! IF
                loc:alert = 'Error. The selected Model is not available to be ordered.'
                loc:invalid = 'locOrderModelNumber'
                DO Value::locOrderModelNumber
                RETURN
            END ! IF
			
            Access:MODEXCHA.ClearKey(moa:AccountNumberKey)
            moa:AccountNumber = p_web.GSV('BookingAccount')
            moa:Manufacturer = p_web.GSV('locOrderManufacturer')
            moa:ModelNumber = p_web.GSV('locOrderModelNumber')
            IF (Access:MODEXCHA.TryFetch(moa:AccountNumberKey) = Level:Benign)
				
            ELSE ! IF
                loc:alert = 'Error! You have not been authorised to order this model number.'
                loc:invalid = 'locOrderModelNumber'
                DO Value::locOrderModelNumber
                RETURN
            END ! IF
			
            IF (mod:RRCOrderCap > 0 AND p_web.GSV('locOrderQuantity') > mod:RRCOrderCap)
                loc:alert = 'Error. The maximum quantity you can order for this model is ' & Clip(sto:ExchangeOrderCap) & '.'
                loc:invalid = 'locOrderModelNumber'
                DO Value::locOrderModelNumber
                RETURN
            END ! IF		
        END ! CASE
		
        IF (Access:SBO_GenericFile.PrimeRecord() = Level:Benign)
            sbogen:SessionID = p_web.SessionID
            sbogen:String1 = p_web.GSV('locOrderModelNumber')
            sbogen:String2 = p_web.GSV('locOrderManufacturer')
            sbogen:Long1 = p_web.GSV('locOrderQuantity')
            sbogen:Notes = p_web.GSV('locOrderNotes')
            IF (Access:SBO_GenericFile.TryInsert())
                Access:SBO_GenericFile.CancelAutoInc()
            END ! IF
            
            ClearVariables()
            
            DO Value::brwOrders
            DO Value::locOrderManufacturer
            DO Value::locOrderModelNumber
            DO Value::locOrderNotes
            DO Value::locOrderQuantity
        END ! IF
        


ClearVariables		PROCEDURE()!
    CODE
		p_web.SSV('locOrderModelNumber','')
        p_web.SSV('locOrderManufacturer','')
        p_web.SSV('locOrderQuantity',0)
        p_web.SSV('locOrderNotes','<13,10>')
