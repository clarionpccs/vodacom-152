

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER218.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
JobCard PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
tmp:JobNumber        LONG                                  !Job Number
tmp:PrintedBy        STRING(60)                            !
who_booked           STRING(50)                            !
Webmaster_Group      GROUP,PRE(tmp)                        !===============================================
Ref_number           STRING(20)                            !
BarCode              STRING(20)                            !
BranchIdentification STRING(2)                             !
                     END                                   !
save_job2_id         USHORT,AUTO                           !
tmp:accessories      STRING(255)                           !
RejectRecord         LONG,AUTO                             !
save_joo_id          USHORT,AUTO                           !
save_maf_id          USHORT,AUTO                           !
save_jac_id          USHORT,AUTO                           !
save_wpr_id          USHORT,AUTO                           !
save_par_id          USHORT,AUTO                           !
save_lac_id          USHORT                                !
save_loa_id          USHORT                                !
LocalRequest         LONG,AUTO                             !
LocalResponse        LONG,AUTO                             !
FilesOpened          LONG                                  !
WindowOpened         LONG                                  !
RecordsToProcess     LONG,AUTO                             !
RecordsProcessed     LONG,AUTO                             !
RecordsPerCycle      LONG,AUTO                             !
RecordsThisCycle     LONG,AUTO                             !
PercentProgress      BYTE                                  !
RecordStatus         BYTE,AUTO                             !
EndOfReport          BYTE,AUTO                             !
ReportRunDate        LONG,AUTO                             !
ReportRunTime        LONG,AUTO                             !
ReportPageNo         SHORT,AUTO                            !
FileOpensReached     BYTE                                  !
PartialPreviewReq    BYTE                                  !
DisplayProgress      BYTE                                  !
InitialPath          CSTRING(128)                          !
Progress:Thermometer BYTE                                  !
IniFileToUse         STRING(64)                            !
code_temp            BYTE                                  !
save_jea_id          USHORT,AUTO                           !
fault_code_field_temp STRING(30),DIM(12)                   !
option_temp          BYTE                                  !
Bar_code_string_temp CSTRING(21)                           !
Bar_Code_Temp        CSTRING(21)                           !
Bar_Code2_Temp       CSTRING(21)                           !
Address_Line1_Temp   STRING(30)                            !
Address_Line2_Temp   STRING(30)                            !
Address_Line3_Temp   STRING(30)                            !
Address_Line4_Temp   STRING(30)                            !
Invoice_Name_Temp    STRING(30)                            !
Delivery_Address1_Temp STRING(30)                          !
Delivery_address2_temp STRING(30)                          !
Delivery_address3_temp STRING(30)                          !
Delivery_address4_temp STRING(30)                          !
Delivery_Telephone_Temp STRING(30)                         !Delivery Telephone
InvoiceAddress_Group GROUP,PRE()                           !===============================================
Invoice_Company_Temp STRING(30)                            !
Invoice_address1_temp STRING(30)                           !
invoice_address2_temp STRING(30)                           !
invoice_address3_temp STRING(30)                           !
invoice_address4_temp STRING(30)                           !
invoice_EMail_Address STRING(255)                          !Email Address
                     END                                   !
accessories_temp     STRING(30),DIM(6)                     !
estimate_value_temp  STRING(70)                            !
despatched_user_temp STRING(40)                            !
vat_temp             REAL                                  !
total_temp           REAL                                  !
part_number_temp     STRING(30)                            !
line_cost_temp       REAL                                  !
job_number_temp      STRING(20)                            !
esn_temp             STRING(30)                            !ESN
charge_type_temp     STRING(22)                            !
repair_type_temp     STRING(22)                            !
labour_temp          REAL                                  !
parts_temp           REAL                                  !
courier_cost_temp    REAL                                  !
Quantity_temp        REAL                                  !
Description_temp     STRING(30)                            !
Cost_Temp            REAL                                  !
engineer_temp        STRING(30)                            !
part_type_temp       STRING(4)                             !
customer_name_temp   STRING(40)                            !
delivery_name_temp   STRING(40)                            !
exchange_unit_number_temp STRING(20)                       !
exchange_model_number STRING(30)                           !
exchange_manufacturer_temp STRING(30)                      !
exchange_unit_type_temp STRING(30)                         !
exchange_esn_temp    STRING(16)                            !
exchange_msn_temp    STRING(20)                            !
exchange_unit_title_temp STRING(15)                        !
invoice_company_name_temp STRING(30)                       !
invoice_telephone_number_temp STRING(15)                   !
invoice_fax_number_temp STRING(15)                         !
tmp:DefaultTelephone STRING(20)                            !
tmp:DefaultFax       STRING(20)                            !
tmp:bouncers         STRING(255)                           !
tmp:InvoiceText      STRING(255)                           !Invoice Text
tmp:LoanModel        STRING(30)                            !Loan Model Details
tmp:LoanIMEI         STRING(30)                            !Loan IMEI number
tmp:LoanAccessories  STRING(255)                           !Loan Accessories
tmp:LoanDepositPaid  REAL                                  !Loan Deposit Paid
DefaultAddress       GROUP,PRE(address)                    !
SiteName             STRING(40)                            !
Name                 STRING(40)                            !Name
Name2                STRING(40)                            !
Location             STRING(40)                            !
AddressLine1         STRING(40)                            !Address Line 1
AddressLine2         STRING(40)                            !Address Line 2
AddressLine3         STRING(40)                            !Address Line 3
AddressLine4         STRING(40)                            !Postcode
Telephone            STRING(30)                            !Telephone
RegistrationNo       STRING(40)                            !
VATNumber            STRING(40)                            !
Fax                  STRING(30)                            !Fax
EmailAddress         STRING(255)                           !Email Address
                     END                                   !
delivery_Company_Name_temp STRING(30)                      !Delivery Name
endUserTelNo         STRING(15)                            !
clientName           STRING(65)                            !
tmp:ReplacementValue REAL                                  !Replacement Value
tmp:FaultCodeDescription STRING(255),DIM(6)                !Fault Code Description
tmp:BookingOption    STRING(30)                            !Booking Option
tmp:ExportReport     BYTE                                  !
barcodeJobNumber     STRING(20)                            !
barcodeIMEINumber    STRING(20)                            !
locTermsText         STRING(255)                           !
Process:View         VIEW(JOBS)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Charge_Type)
                       PROJECT(job:Courier)
                       PROJECT(job:DOP)
                       PROJECT(job:Date_Completed)
                       PROJECT(job:Date_QA_Passed)
                       PROJECT(job:ESN)
                       PROJECT(job:Location)
                       PROJECT(job:MSN)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Repair_Type)
                       PROJECT(job:Repair_Type_Warranty)
                       PROJECT(job:Time_Completed)
                       PROJECT(job:Time_QA_Passed)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:Warranty_Charge_Type)
                       PROJECT(job:date_booked)
                       PROJECT(job:time_booked)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT('ORDERS Report'),AT(260,6458,7750,813),PRE(RPT),PAPER(PAPER:A4),FONT('Tahoma',8,,FONT:regular, |
  CHARSET:ANSI),THOUS
                       HEADER,AT(260,615,7750,8250),USE(?unnamed),FONT('Tahoma',7)
                         STRING(@s50),AT(6094,469),USE(who_booked),FONT(,8,,FONT:bold),TRN
                         STRING('Job Number: '),AT(5104,52),USE(?String25),FONT(,8),TRN
                         STRING(@s20),AT(6010,21),USE(tmp:Ref_number),FONT(,9,,FONT:bold),LEFT,TRN
                         STRING('Date Booked: '),AT(5104,313),USE(?String58),FONT(,8),TRN
                         STRING(@d6b),AT(6094,313),USE(job:date_booked),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING(@t1b),AT(6979,313),USE(job:time_booked),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING('Booked By:'),AT(5104,469),USE(?String158),FONT(,8),TRN
                         STRING(@d6b),AT(6094,781),USE(job:DOP),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING('Engineer:'),AT(5104,625),USE(?String96),FONT(,8),TRN
                         STRING(@s30),AT(6094,625),USE(engineer_temp),FONT(,8,,FONT:bold),TRN
                         STRING('Date Of Purchase:'),AT(5104,781),USE(?String96:2),FONT(,8),TRN
                         STRING('Model'),AT(177,3500),USE(?String40),FONT(,8,,FONT:bold),TRN
                         STRING('Location'),AT(177,2833),USE(?String40:3),FONT(,8,,FONT:bold),TRN
                         STRING('Warranty Type'),AT(4708,2833),USE(?warranty_Type),FONT(,8,,FONT:bold),TRN
                         STRING('Warranty Repair Type'),AT(6219,2833),USE(?warranty_repair_type),FONT(,8,,FONT:bold), |
  TRN
                         STRING('Chargeable Type'),AT(1688,2833),USE(?Chargeable_Type),FONT(,8,,FONT:bold),TRN
                         STRING('Chargeable Repair Type'),AT(3198,2833),USE(?Repair_Type),FONT(,8,,FONT:bold),TRN
                         STRING('Make'),AT(1688,3500),USE(?String41),FONT(,8,,FONT:bold),TRN
                         STRING('Unit Type'),AT(3198,3500),USE(?String42),FONT(,8,,FONT:bold),TRN
                         STRING('I.M.E.I. Number'),AT(4708,3510),USE(?String43),FONT(,8,,FONT:bold),TRN
                         STRING('M.S.N.'),AT(6219,3510),USE(?String44),FONT(,8,,FONT:bold),TRN
                         STRING(@s20),AT(1688,3073),USE(job:Charge_Type),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(3198,3073),USE(job:Repair_Type),FONT(,8),TRN
                         STRING(@s20),AT(177,3073),USE(job:Location),FONT(,8),TRN
                         STRING(@s30),AT(177,3698,1000,156),USE(job:Model_Number),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(1688,3698,1323,156),USE(job:Manufacturer),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(3198,3698,1396,156),USE(job:Unit_Type),FONT(,8),LEFT,TRN
                         STRING(@s15),AT(4708,3698,1396,156),USE(job:ESN),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(6219,3698),USE(job:MSN),FONT(,8),TRN
                         STRING(@s20),AT(6219,4010),USE(exchange_msn_temp),FONT(,8),TRN
                         STRING(@s16),AT(4708,4010,1396,156),USE(exchange_esn_temp),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(3198,4010,1396,156),USE(exchange_unit_type_temp),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(1688,4010,1323,156),USE(exchange_manufacturer_temp),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(177,4010,1000,156),USE(exchange_model_number),FONT(,8),LEFT,TRN
                         STRING(@s15),AT(177,3854),USE(exchange_unit_title_temp),FONT(,8,,FONT:bold),LEFT,TRN
                         STRING(@s20),AT(1354,3854),USE(exchange_unit_number_temp),LEFT,TRN
                         STRING('REPORTED FAULT'),AT(3562,4229,1062,156),USE(?String64),FONT(,8,,FONT:bold),TRN
                         TEXT,AT(4625,4219,2406,844),USE(jbn:Fault_Description),FONT(,7),TRN
                         STRING('ESTIMATE'),AT(156,4583,1354,156),USE(?Estimate),FONT(,8,,FONT:bold),HIDE,TRN
                         STRING(@s70),AT(1625,5031,5625,208),USE(estimate_value_temp),FONT(,8),TRN
                         STRING('ENGINEERS REPORT'),AT(156,5031,1354,156),USE(?String88),FONT(,8,,FONT:bold),TRN
                         STRING('REPAIR NOTES'),AT(156,4229),USE(?String88:3),FONT(,8,,FONT:bold),TRN
                         TEXT,AT(1615,4646,1927,417),USE(tmp:InvoiceText),FONT(,7),TRN
                         TEXT,AT(1615,5208,5417,313),USE(tmp:accessories),FONT(,7),TRN
                         STRING('ACCESSORIES'),AT(156,5208,1354,156),USE(?String100),FONT(,8,,FONT:bold),TRN
                         STRING('Type'),AT(521,5677),USE(?String98),FONT(,7,,FONT:bold),TRN
                         STRING('PARTS REQUIRED'),AT(156,5521,1135,156),USE(?String79),FONT(,8,,FONT:bold),TRN
                         STRING('Qty'),AT(156,5677),USE(?String80),FONT(,7,,FONT:bold),TRN
                         STRING('Part Number'),AT(885,5677),USE(?String81),FONT(,7,,FONT:bold),TRN
                         STRING('Description'),AT(2292,5677),USE(?String82),FONT(,7,,FONT:bold),TRN
                         STRING('Unit Cost'),AT(4063,5677),USE(?String83),FONT(,7,,FONT:bold),TRN
                         STRING('Line Cost'),AT(4948,5677),USE(?String89),FONT(,7,,FONT:bold),TRN
                         LINE,AT(156,5833,7083,0),USE(?Line1),COLOR(COLOR:Black)
                         LINE,AT(156,6667,7083,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('BOUNCER'),AT(156,6719,1354,156),USE(?BouncerTitle),FONT('Tahoma',8,,FONT:bold),HIDE, |
  TRN
                         TEXT,AT(1667,6719,5417,208),USE(tmp:bouncers),FONT(,8),HIDE,TRN
                         STRING('LOAN UNIT ISSUED'),AT(156,6927,1354,156),USE(?LoanUnitIssued),FONT('Tahoma',8,COLOR:Black, |
  FONT:bold,CHARSET:ANSI),HIDE,TRN
                         STRING(@s30),AT(1667,6927),USE(tmp:LoanModel),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  LEFT,HIDE,TRN
                         STRING('IMEI'),AT(3646,6927),USE(?IMEITitle),FONT('Tahoma',9,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  HIDE,TRN
                         STRING(@s16),AT(4010,6927),USE(tmp:LoanIMEI),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  LEFT,HIDE,TRN
                         STRING('CUSTOMER SIGNATURE'),AT(167,8021,1583,156),USE(?String820),FONT('Tahoma',8,,FONT:bold), |
  TRN
                         STRING('REPLACEMENT VALUE'),AT(5104,6927),USE(?LoanValueText),FONT('Tahoma',9,COLOR:Black, |
  FONT:bold,CHARSET:ANSI),HIDE,TRN
                         STRING(@n10.2),AT(6406,6927),USE(tmp:ReplacementValue),FONT(,8),RIGHT,HIDE,TRN
                         LINE,AT(4719,8156,1406,0),USE(?Line5),COLOR(COLOR:Black)
                         STRING('Date: {22}/ {23}/'),AT(4500,8031,1563,156),USE(?String830),FONT(,,,FONT:bold),TRN
                         LINE,AT(1594,8156,2917,0),USE(?Line5:2),COLOR(COLOR:Black)
                         STRING('LOAN ACCESSORIES'),AT(156,7083,1354,156),USE(?LoanAccessoriesTitle),FONT('Tahoma', |
  8,COLOR:Black,FONT:bold,CHARSET:ANSI),HIDE,TRN
                         STRING(@s255),AT(1667,7083,5417,208),USE(tmp:LoanAccessories),FONT('Tahoma',8,COLOR:Black, |
  FONT:regular,CHARSET:ANSI),LEFT,HIDE,TRN
                         STRING('EXTERNAL DAMAGE CHECK LIST'),AT(156,7219),USE(?ExternalDamageCheckList),FONT('Tahoma', |
  8,COLOR:Black,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Notes:'),AT(5781,7240),USE(?Notes),FONT(,7),TRN
                         TEXT,AT(6094,7240,1458,469),USE(jobe2:XNotes),FONT(,6),TRN
                         CHECK('None'),AT(5156,7240,625,156),USE(jobe2:XNone),TRN
                         CHECK('Keypad'),AT(156,7365,729,156),USE(jobe2:XKeypad),TRN
                         CHECK('Charger'),AT(2083,7365,781,156),USE(jobe2:XCharger),TRN
                         CHECK('Antenna'),AT(2083,7240,833,156),USE(jobe2:XAntenna),TRN
                         CHECK('Lens'),AT(2927,7240,573,156),USE(jobe2:XLens),TRN
                         CHECK('F/Cover'),AT(3552,7240,781,156),USE(jobe2:XFCover),TRN
                         CHECK('B/Cover'),AT(4437,7240,729,156),USE(jobe2:XBCover),TRN
                         CHECK('Battery'),AT(990,7365,729,156),USE(jobe2:XBattery),TRN
                         CHECK('LCD'),AT(2927,7365,625,156),USE(jobe2:XLCD),TRN
                         CHECK('System Connector'),AT(4437,7365,1250,156),USE(jobe2:XSystemConnector),TRN
                         CHECK('Sim Reader'),AT(3552,7365,885,156),USE(jobe2:XSimReader),TRN
                         STRING(@s30),AT(156,1927),USE(invoice_address4_temp),FONT(,8),TRN
                         STRING('Tel: '),AT(4167,2083),USE(?String32:2),FONT(,8),TRN
                         STRING(@s30),AT(4427,2083),USE(Delivery_Telephone_Temp),FONT(,8),TRN
                         STRING(@s50),AT(521,2083,3490,156),USE(invoice_EMail_Address),FONT(,8),LEFT,TRN
                         STRING('Email:'),AT(156,2083),USE(?unnamed:4),FONT(,8),TRN
                         STRING('Tel:'),AT(156,2240),USE(?String34:2),FONT(,8),TRN
                         STRING(@s15),AT(521,2240),USE(invoice_telephone_number_temp),FONT(,8),LEFT,TRN
                         STRING('Fax: '),AT(156,2396),USE(?String35:2),FONT(,8),TRN
                         STRING(@s15),AT(521,2396),USE(invoice_fax_number_temp),FONT(,8),LEFT,TRN
                         STRING(@s15),AT(2760,1302),USE(job:Account_Number),FONT(,8),TRN
                         STRING(@s20),AT(4708,3073),USE(job:Warranty_Charge_Type),FONT(,8),TRN
                         STRING(@s20),AT(6219,3073),USE(job:Repair_Type_Warranty),FONT(,8),TRN
                         STRING('Acc No:'),AT(2188,1302),USE(?String94),FONT(,8,COLOR:Black),TRN
                         STRING(@s30),AT(4167,1302),USE(delivery_Company_Name_temp),FONT(,8),TRN
                         STRING(@s30),AT(4167,1458,1917,156),USE(Delivery_Address1_Temp),FONT(,8),TRN
                         STRING(@s20),AT(2760,1458),USE(job:Order_Number),FONT(,8),TRN
                         STRING('Order No:'),AT(2188,1458),USE(?String140),FONT(,8),TRN
                         STRING(@s30),AT(4167,1615),USE(Delivery_address2_temp),FONT(,8),TRN
                         STRING(@s30),AT(4167,1771),USE(Delivery_address3_temp),FONT(,8),TRN
                         STRING(@s30),AT(4167,1927),USE(Delivery_address4_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1302),USE(invoice_company_name_temp),FONT(,8,COLOR:Black),TRN
                         STRING(@s30),AT(156,1458),USE(Invoice_address1_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1771),USE(invoice_address3_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1615),USE(invoice_address2_temp),FONT(,8),TRN
                         STRING(@s65),AT(4167,2396,2865,156),USE(clientName),FONT('Tahoma',8,COLOR:Black,FONT:regular, |
  CHARSET:ANSI),TRN
                         TEXT,AT(156,7781,7375,250),USE(stt:Text),TRN
                         TEXT,AT(156,7531,5885,260),USE(locTermsText)
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed:3)
DETAIL                   DETAIL,AT(0,0,,115),USE(?DetailBand)
                           STRING(@n8b),AT(-104,0),USE(Quantity_temp),FONT(,7),RIGHT,TRN
                           STRING(@s25),AT(885,0),USE(part_number_temp),FONT(,7),TRN
                           STRING(@s25),AT(2292,0),USE(Description_temp),FONT(,7),TRN
                           STRING(@n14.2b),AT(3802,0),USE(Cost_Temp),FONT(,7),RIGHT,TRN
                           STRING(@n14.2b),AT(4740,0),USE(line_cost_temp),FONT(,7),RIGHT,TRN
                           STRING(@s4),AT(521,0),USE(part_type_temp),FONT(,7),TRN
                         END
                         FOOTER,AT(396,8896,,2406),USE(?unnamed:2),ABSOLUTE,TOGETHER
                           STRING('Loan Deposit Paid'),AT(5313,21),USE(?LoanDepositPaidTitle),FONT(,8,,FONT:regular,CHARSET:ANSI), |
  HIDE,TRN
                           STRING(@n14.2b),AT(6396,21),USE(tmp:LoanDepositPaid),FONT(,8,,,CHARSET:ANSI),RIGHT,HIDE,TRN
                           STRING('Completed:'),AT(52,156),USE(?String66),FONT(,8),TRN
                           STRING(@D6b),AT(719,167),USE(job:Date_Completed),FONT(,8,,FONT:bold),RIGHT,TRN
                           STRING(@t1b),AT(1677,167),USE(job:Time_Completed),FONT(,8,,FONT:bold),RIGHT,TRN
                           STRING('QA Passed:'),AT(52,313),USE(?qa_passed),FONT(,8),HIDE,TRN
                           STRING('Labour:'),AT(5313,167),USE(?labour_string),FONT(,8),TRN
                           STRING(@n14.2b),AT(6396,167),USE(labour_temp),FONT(,8),RIGHT,TRN
                           STRING(@s20),AT(2500,260,2552,208),USE(barcodeJobNumber),FONT('C39 High 12pt LJ3',12),CENTER, |
  COLOR(COLOR:White)
                           STRING('Outgoing Courier:'),AT(52,573),USE(?String66:2),FONT(,8),TRN
                           STRING(@s20),AT(2917,469,1760,198),USE(job_number_temp),FONT('Tahoma',8,,FONT:bold),CENTER, |
  TRN
                           STRING('Carriage:'),AT(5313,458),USE(?carriage_string),FONT(,8),TRN
                           STRING(@n14.2b),AT(6396,313),USE(parts_temp),FONT(,8),RIGHT,TRN
                           STRING(@s20),AT(2500,729,2552,208),USE(barcodeIMEINumber),FONT('C39 High 12pt LJ3',12),CENTER, |
  COLOR(COLOR:White)
                           STRING('V.A.T.'),AT(5313,604),USE(?vat_String),FONT(,8),TRN
                           STRING(@n14.2b),AT(6396,458),USE(courier_cost_temp),FONT(,8),RIGHT,TRN
                           STRING(@d6b),AT(719,313),USE(job:Date_QA_Passed),FONT(,8,,FONT:bold),RIGHT,TRN
                           STRING('Total:'),AT(5313,781),USE(?total_string),FONT(,8,,FONT:bold),TRN
                           LINE,AT(6281,781,1000,0),USE(?line),COLOR(COLOR:Black)
                           STRING(@n14.2b),AT(6302,781),USE(total_temp),FONT(,8,,FONT:bold),RIGHT,TRN
                           STRING('<128>'),AT(6250,781),USE(?Euro),FONT(,8,,FONT:bold),HIDE,TRN
                           STRING('FAULT CODES'),AT(52,1146),USE(?String107),FONT(,8,,FONT:bold),TRN
                           STRING('Booking Option:'),AT(5313,938),USE(?BookingOption),FONT(,8,,FONT:bold),TRN
                           STRING(@s30),AT(6354,938,1406,156),USE(tmp:BookingOption),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI), |
  LEFT,TRN
                           STRING(@s30),AT(52,1302),USE(fault_code_field_temp[1]),FONT(,8),TRN
                           STRING(@s255),AT(2135,1302,4844,208),USE(tmp:FaultCodeDescription[1]),FONT(,8),TRN
                           STRING(@s30),AT(52,1458),USE(fault_code_field_temp[2]),FONT(,8),TRN
                           STRING(@s255),AT(2135,1458,4844,208),USE(tmp:FaultCodeDescription[2]),FONT(,8)
                           STRING(@s30),AT(52,1615),USE(fault_code_field_temp[3]),FONT(,8),TRN
                           STRING(@s255),AT(2135,1615,4844,208),USE(tmp:FaultCodeDescription[3]),FONT(,8)
                           STRING(@s30),AT(52,1771),USE(fault_code_field_temp[4]),FONT(,8),TRN
                           STRING(@s255),AT(2135,1771,4844,208),USE(tmp:FaultCodeDescription[4]),FONT(,8)
                           STRING(@s30),AT(52,1927),USE(fault_code_field_temp[5]),FONT(,8),TRN
                           STRING(@s255),AT(2135,1927,4844,208),USE(tmp:FaultCodeDescription[5]),FONT(,8)
                           STRING(@s30),AT(52,2083),USE(fault_code_field_temp[6]),FONT(,8),TRN
                           STRING(@s255),AT(2135,2083,4844,208),USE(tmp:FaultCodeDescription[6]),FONT(,8)
                           STRING(@n14.2b),AT(6396,604),USE(vat_temp),FONT(,8),RIGHT,TRN
                           STRING(@s13),AT(990,729,1406,156),USE(jobe2:IDNumber),FONT(,8,,FONT:bold),TRN
                           STRING('Customer ID No:'),AT(52,729),USE(?String66:3),FONT(,8),TRN
                           STRING(@s15),AT(990,573,1563,156),USE(job:Courier),FONT(,8,,FONT:bold),TRN
                           STRING(@t1b),AT(1677,313),USE(job:Time_QA_Passed),FONT(,8,,FONT:bold),RIGHT,TRN
                           STRING(@s30),AT(2760,938,2031,260),USE(esn_temp),FONT(,8,,FONT:bold),CENTER,TRN
                           STRING('Parts:'),AT(5313,313),USE(?parts_string),FONT(,8),TRN
                         END
                       END
                       FOOTER,AT(396,10271,7521,1156),USE(?Fault_Code9:2)
                       END
                       FORM,AT(250,250,7750,11188),USE(?Text:CurrencyItemCost:2),FONT('Tahoma',8,,FONT:regular)
                         STRING('JOB CARD'),AT(5521,0,1917,240),USE(?Chargeable_Repair_Type:2),FONT(,16,,FONT:bold), |
  RIGHT,TRN
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),FONT(,14,,FONT:bold),LEFT
                         STRING(@s40),AT(104,313,3073,208),USE(address:Name2),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,208,3073,208),USE(address:Name),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,417,2760,208),USE(address:Location),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING('REG NO:'),AT(104,573),USE(?stringREGNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(625,573,1667,208),USE(address:RegistrationNo),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('VAT NO: '),AT(104,677),USE(?stringVATNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s20),AT(625,677,1771,156),USE(address:VATNumber),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,833,2240,156),USE(address:AddressLine1),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,938,2240,156),USE(address:AddressLine2),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1042,2240,156),USE(address:AddressLine3),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1146),USE(address:AddressLine4),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('TEL:'),AT(104,1250),USE(?stringTEL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(469,1250,1458,208),USE(address:Telephone),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('FAX:'),AT(2083,1250,313,208),USE(?stringFAX),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(2396,1250,1510,188),USE(address:Fax),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('EMAIL:'),AT(104,1354,417,208),USE(?stringEMAIL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s255),AT(469,1354,3333,208),USE(address:EmailAddress),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('JOB DETAILS'),AT(5000,208),USE(?String57),FONT(,8,,FONT:bold),TRN
                         STRING('INVOICE ADDRESS'),AT(156,1510),USE(?String24),FONT(,8,,FONT:bold),TRN
                         STRING('DELIVERY ADDRESS'),AT(4115,1510,1677,156),USE(?DeliveryAddress),FONT(,8,,FONT:bold), |
  TRN
                         STRING('GENERAL DETAILS'),AT(156,2990),USE(?String91),FONT(,8,,FONT:bold),TRN
                         STRING('COMPLETION DETAILS'),AT(156,8563),USE(?String73),FONT(,8,,FONT:bold),TRN
                         STRING('CHARGE DETAILS'),AT(5365,8563),USE(?String74),FONT(,8,,FONT:bold),TRN
                         STRING('REPAIR DETAILS'),AT(156,3667),USE(?String50),FONT(,8,,FONT:bold),TRN
                         BOX,AT(5000,365,2646,1042),USE(?Box:TopDetails),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,1667,3604,1302),USE(?Box:Address1),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4063,1667,3604,1302),USE(?Box:Address2),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3125,1510,260),USE(?Box:Title1),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(1615,3125,1510,260),USE(?Box:Title2),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(3125,3125,1510,260),USE(?Box:Title3),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4635,3125,1510,260),USE(?Box:Title4),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(6146,3125,1510,260),USE(?Box:Title5),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3385,1510,260),USE(?Box:Title1a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(1615,3385,1510,260),USE(?Box:Title2a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(3125,3385,1510,260),USE(?Box:Title3a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4635,3385,1510,260),USE(?Box:Title4a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(6146,3385,1510,260),USE(?Box:Title5a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3802,7552,260),USE(?Box:Heading),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,4063,7552,4479),USE(?Box:Detail),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,8698,2344,1042),USE(?Box:Total1),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(5313,8698,2344,1042),USE(?Box:Total2),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR1               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('JobCard')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  tmp:JobNumber = p_web.GetSessionValue('tmp:JobNumber')
  
  Relate:EXCHANGE.Open                                     ! File EXCHANGE used by this procedure, so make sure it's RelationManager is open
  Relate:INVOICE.SetOpenRelated()
  Relate:INVOICE.Open                                      ! File INVOICE used by this procedure, so make sure it's RelationManager is open
  Relate:JOBEXACC.Open                                     ! File JOBEXACC used by this procedure, so make sure it's RelationManager is open
  Relate:JOBPAYMT.Open                                     ! File JOBPAYMT used by this procedure, so make sure it's RelationManager is open
  Relate:JOBS2_ALIAS.Open                                  ! File JOBS2_ALIAS used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:JOBNOTES.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBACC.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANUFACT.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAULO.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBOUTFL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOAN.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOANACC.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  ! #12084 New standard texts (Bryan: 17/05/2011)
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'JOB CARD'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  locTermsText = p_web.GSV('Default:TermsText')
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('JobCard',ProgressWindow)                   ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisReport.AddSortOrder(job:Ref_Number_Key)
  ThisReport.AddRange(job:Ref_Number,tmp:JobNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:INVOICE.Close
    Relate:JOBEXACC.Close
    Relate:JOBPAYMT.Close
    Relate:JOBS2_ALIAS.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('JobCard',ProgressWindow)                ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'JobCard',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
    job_Number_Temp = 'Job No: ' & clip(job:Ref_Number)
    esn_Temp = 'I.M.E.I.: ' & clip(job:ESN)
    barcodeJobNumber = '*' & clip(job:Ref_Number) & '*'
    barcodeIMEINumber = '*' & clip(job:ESN) & '*'

!  !Barcode Bit and setup refno
!  code_temp            = 1
!  option_temp          = 0
!  
!  bar_code_string_temp = Clip(job:ref_number)
!  !job_number_temp      = 'Job No: ' & Clip(job:ref_number)
!  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
!  
!  bar_code_string_temp = Clip(job:esn)
!  !esn_temp             = 'I.M.E.I.: ' & Clip(job:esn)
!  !SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
!  
!  job_number_temp = '*' & job:Ref_Number & '*'
!  
!  Settarget(Report)
!  Draw_JobNo.Init256()
!  Draw_JobNo.RenderWholeStrings = 1
!  Draw_JobNo.Resize(Draw_JobNo.Width * 5, Draw_JobNo.Height * 5)
!  Draw_JobNo.Blank(Color:White)
!  Draw_JobNo.FontName = 'C39 High 12pt LJ3'
!  Draw_JobNo.FontStyle = font:Regular
!  Draw_JobNo.FontSize = 24
!  Draw_JobNo.Show(0,0,Clip(job_Number_temp))
!  Draw_JobNo.Display()
!  Draw_JobNo.Kill256()
!  
!  drawer4.Init256()
!  drawer4.RenderWholeStrings = 1
!  drawer4.SetFontMode(Draw:NONANTIALIASED_QUALITY)
!  drawer4.Resize(drawer4.Width * 5, drawer4.Height * 5)
!  drawer4.Blank(Color:White)
!  drawer4.FontName = 'C39 High 12pt LJ3'
!  drawer4.FontStyle = font:Regular
!  drawer4.FontSize = 24
!  drawer4.Show(0,0,Clip(job_Number_temp))
!  drawer4.Display()
!  drawer4.Kill256()
!
!  drawer6.Init256()
!  drawer6.RenderWholeStrings = 1
!  drawer6.Resize(drawer6.Width * 5, drawer6.Height * 5)
!  drawer6.Blank(Color:White)
!  drawer6.FontName = 'C39 High 12pt LJ3'
!  drawer6.FontStyle = font:Regular
!  drawer6.FontSize = 24
!  drawer6.Show(0,0,Bar_Code_Temp)
!  drawer6.Display()
!  drawer6.Kill256()
!!  
!  drawer7.Init256()
!  drawer7.RenderWholeStrings = 1
!  drawer7.SetFontMode(Draw:NONANTIALIASED_QUALITY)
!  drawer7.Resize()
!  drawer7.Blank(Color:White)
!  drawer7.FontName = 'C39 High 12pt LJ3'
!  drawer7.FontStyle = font:Regular
!  drawer7.FontSize = 24
!  drawer7.Show(0,0,Clip(job_Number_temp))
!  drawer7.Display()
!  drawer7.Kill256()
!!  
!  drawer8.Init256()
!  drawer8.RenderWholeStrings = 1
!  !drawer8.SetFontMode(Draw:NONANTIALIASED_QUALITY)
!  drawer8.Resize()
!  drawer8.Blank(Color:White)
!  drawer8.FontName = 'C39 High 12pt LJ3'
!  drawer8.FontStyle = font:Regular
!  drawer8.FontSize = 24
!  drawer8.Show(0,0,Clip(job_Number_temp))
!  drawer8.Display()
!  drawer8.Kill256()
!!  
!!  drawer9.Init256()
!!  drawer9.RenderWholeStrings = 1
!!  drawer9.Resize(drawer9.Width * 5, drawer9.Height * 5)
!!  drawer9.Blank(Color:White)
!!  drawer9.FontName = 'C39 High 12pt LJ3'
!!  drawer9.FontStyle = font:Regular
!!  drawer9.FontSize = 22
!!  drawer9.Show(2,2,Clip(job_Number_temp))
!!  drawer9.Display()
!!  drawer9.Kill256()
!!
!!  !Draw_IMEI.Init256()
!!  !Draw_IMEI.RenderWholeStrings = 1
!!  !Draw_IMEI.Resize(Draw_IMEI.Width * 5, Draw_IMEI.Height * 5)
!!  !Draw_IMEI.Blank(Color:White)
!!  !Draw_IMEI.FontName = 'C39 High 12pt LJ3'
!!  !Draw_IMEI.FontStyle = font:Regular
!!  !Draw_IMEI.FontSize = 48
!!  !Draw_IMEI.Show(0,0,Bar_Code2_Temp)
!!  !Draw_IMEI.Display()
!!  !Draw_IMEI.Kill256()
!  SetTarget()
!  
!  
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.Init(Report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,Report{PROPPRINT:Paper},PAPER:USER), CHOOSE(Report{PROP:Thous}=True,PROP:Thous,CHOOSE(Report{PROP:MM}=True,PROP:MM,CHOOSE(Report{PROP:Points}=True,PROP:Points,0))), Report{PROPPRINT:PaperWidth}, Report{PROPPRINT:PaperHeight}, Report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetTitle('Job Card')                 !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR1.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetViewerPrefs(PDFXTR1:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetFontEmbedding(True, True, False, False, False)
    PDFXTR1.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'JobCard',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR1.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Found
      Access:TRADeACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = wob:HeadAccountNumber
      If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          !Found
          tmp:Ref_Number = job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
      Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
  
  Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
  
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = job:Ref_Number
  If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Found
  Else ! If AccESS:JOBE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBE.TryFetch(jobe:RefNumberKey) = Level:Benign
  
  Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
  jobe2:RefNumber = job:Ref_Number
  If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
  
  Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
  jbn:RefNumber = job:Ref_Number
  If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
  
  
  
  Access:DEFAULTS.Clearkey(def:RecordNumberKEy)
  def:Record_Number = 1
  Set(def:RecordNumberKEy,def:RecordNumberKEy)
  Loop ! Begin Loop
      If Access:DEFAULTS.Next()
          Break
      End ! If Access:DEFAULTS.Next()
      Break
  End ! Loop
  
  
  Access:USERS.ClearKey(use:User_Code_Key)
  use:User_Code = job:Engineer
  If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Found
      Engineer_Temp = Clip(use:Forename) & ' ' & Clip(use:Surname)
  Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Error
      Engineer_Temp = ''
  End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
  
  Access:USERS.ClearKey(use:User_Code_Key)
  use:User_Code = job:Who_Booked
  If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Found
      Who_Booked = Clip(use:Forename) & ' ' & Clip(use:Surname)
  Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Error
      Who_Booked = ''
  End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
  
  Despatched_User_Temp = ''
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  If (jobe:WebJob = 1)
      tra:Account_Number = wob:HeadAccountNumber
  ELSE ! If (glo:WebJob = 1)
      tra:Account_Number = GETINI('BOOKING','HeadAccount','AA20',CLIP(PATH())&'\SB2KDEF.INI')
  END ! If (glo:WebJob = 1)
  If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
      address:SiteName        = tra:Company_Name
      address:Name            = tra:coTradingName
      address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
      address:Location        = tra:coLocation
      address:RegistrationNo  = tra:coRegistrationNo
      address:AddressLine1    = tra:coAddressLine1
      address:AddressLine2    = tra:coAddressLine2
      address:AddressLine3    = tra:coAddressLine3
      address:AddressLine4    = tra:coAddressLine4
      address:Telephone       = tra:coTelephoneNumber
      address:Fax             = tra:coFaxNumber
      address:EmailAddress    = tra:coEmailAddress
      address:VatNumber       = tra:coVATNumber
  END
  Settarget(Report)
  
  ! Show Engineering Option If Set - 4285 (DBH: 10-06-2004)
  Case jobe:Engineer48HourOption
  Of 1
      tmp:BookingOption         = '48 Hour Exchange'
      ?BookingOPtion{prop:Text} = 'Engineer Option:'
  Of 2
      tmp:BookingOption         = 'ARC Repair'
      ?BookingOPtion{prop:Text} = 'Engineer Option:'
  Of 3
      tmp:BookingOption         = '7 Day TAT'
      ?BookingOPtion{prop:Text} = 'Engineer Option:'
  Of 4
      tmp:BookingOption         = 'Standard Repair'
      ?BookingOPtion{prop:Text} = 'Engineer Option:'
  Else
      Case jobe:Booking48HourOption
      Of 1
          tmp:BookingOption = '48 Hour Exchange'
      Of 2
          tmp:BookingOption = 'ARC Repair'
      Of 3
          tmp:BookingOption = '7 Day TAT'
      Else
          tmp:BookingOption = 'N/A'
      End ! Case jobe:Booking48HourOption
  End ! Case jobe:Engineer48HourOption
  
  If job:warranty_job <> 'YES'
      Hide(?job:warranty_charge_type)
      Hide(?job:repair_type_warranty)
      Hide(?warranty_type)
      Hide(?Warranty_repair_Type)
  End! If job:warranty_job <> 'YES'
  
  If job:chargeable_job <> 'YES'
      Hide(?job:charge_type)
      Hide(?job:repair_type)
      Hide(?Chargeable_type)
      Hide(?repair_Type)
  End! If job:chargeable_job <> 'YES'
  
  Access:MANUFACT.Clearkey(man:Manufacturer_Key)
  man:Manufacturer    = job:Manufacturer
  If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      ! Found
      If man:UseQA
          UnHide(?qa_passed)
      End! If def:qa_required <> 'YES'
  
  Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
  ! Error
  End ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
  
  
  If job:warranty_job = 'YES' And job:chargeable_job <> 'YES'
      Hide(?labour_string)
      Hide(?parts_string)
      Hide(?Carriage_string)
      Hide(?vat_string)
      Hide(?total_string)
      Hide(?line)
      Hide(?Euro)
  End! If job:chargeable_job = 'YES' And job:warranty_job = 'YES'
  
  If job:estimate = 'YES' and job:Chargeable_Job = 'YES'
      Unhide(?estimate)
      estimate_value_temp = 'ESTIMATE REQUIRED IF REPAIR COST EXCEEDS ' & Clip(Format(job:estimate_if_over, @n14.2)) & ' (Plus V.A.T.)'
  End ! If
  
  
  !------------------------------------------------------------------
  IF CLIP(job:title) = ''
      customer_name_temp = job:initial
  ELSIF CLIP(job:initial) = ''
      customer_name_temp = job:title
  ELSE
      customer_name_temp = CLIP(job:title) & ' ' & CLIP(job:initial)
  END ! IF
  !------------------------------------------------------------------
  IF CLIP(customer_name_temp) = ''
      customer_name_temp = job:surname
  ELSIF CLIP(job:surname) = ''
  ! customer_name_temp = customer_name_temp ! tautology
  ELSE
      customer_name_temp = CLIP(customer_name_temp) & ' ' & CLIP(job:surname)
  END ! IF
  !------------------------------------------------------------------
  Settarget()
  
  endUserTelNo = ''
  
  if jobe:EndUserTelNo <> ''
      endUserTelNo = clip(jobe:EndUserTelNo)
  end ! if
  
  if customer_name_temp <> ''
      clientName = 'Client: ' & clip(customer_name_temp) & '  Tel: ' & clip(endUserTelNo)
  else
      if endUserTelNo <> ''
          clientName = 'Tel: ' & clip(endUserTelNo)
      end ! if
  end ! if
  !**************************************************
  delivery_name_temp         = customer_name_temp
  delivery_company_Name_temp = job:Company_Name_Delivery
  
  delivery_address1_temp = job:address_line1_delivery
  delivery_address2_temp = job:address_line2_delivery
  If job:address_line3_delivery   = ''
      delivery_address3_temp = job:postcode_delivery
      delivery_address4_temp = ''
  Else
      delivery_address3_temp = job:address_line3_delivery
      delivery_address4_temp = job:postcode_delivery
  End ! If
  delivery_telephone_temp     = job:Telephone_Delivery
  !**************************************************
  
  
  !**************************************************
  
  ! If an RRC job, then put the RRC as the invoice address,
  
  !If jobe:WebJob
  !    SetTarget(Report)
  !    ?DeliveryAddress{prop:Text} = 'CUSTOMER ADDRESS'
  !    SetTarget()
  !    Access:WEBJOB.Clearkey(wob:RefNumberKey)
  !    wob:RefNumber   = job:Ref_Number
  !    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  !        ! Found
  !        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  !        tra:Account_Number  = wob:HeadAccountNumber
  !        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  !            ! Found
  !            invoice_address1_temp = tra:address_line1
  !            invoice_address2_temp = tra:address_line2
  !            If job:address_line3_delivery   = ''
  !                invoice_address3_temp = tra:postcode
  !                invoice_address4_temp = ''
  !            Else
  !                invoice_address3_temp = tra:address_line3
  !                invoice_address4_temp = tra:postcode
  !            End ! If
  !            invoice_company_name_temp     = tra:company_name
  !            invoice_telephone_number_temp = tra:telephone_number
  !            invoice_fax_number_temp       = tra:fax_number
  !            invoice_EMail_Address         = tra:EmailAddress
  !        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  !        ! Error
  !        End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  !    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  !    ! Error
  !    End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  !
  !Else ! If ~glo:WebJob And jobe:WebJob
  
  
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = job:account_number
      access:subtracc.fetch(sub:account_number_key)
  
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = sub:main_account_number
      access:tradeacc.fetch(tra:account_number_key)
      if tra:invoice_sub_accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
          If sub:invoice_customer_address = 'YES'
              invoice_address1_temp = job:address_line1
              invoice_address2_temp = job:address_line2
              If job:address_line3_delivery   = ''
                  invoice_address3_temp = job:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp = job:address_line3
                  invoice_address4_temp = job:postcode
              End ! If
              invoice_company_name_temp     = job:company_name
              invoice_telephone_number_temp = job:telephone_number
              invoice_fax_number_temp       = job:fax_number
              invoice_EMail_Address         = jobe:EndUserEmailAddress  ! '' ! tra:EmailAddress
          ! MEssage('case 1')
          Else! If sub:invoice_customer_address = 'YES'
              invoice_address1_temp = sub:address_line1
              invoice_address2_temp = sub:address_line2
              If job:address_line3_delivery   = ''
                  invoice_address3_temp = sub:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp = sub:address_line3
                  invoice_address4_temp = sub:postcode
              End ! If
              invoice_company_name_temp     = sub:company_name
              invoice_telephone_number_temp = sub:telephone_number
              invoice_fax_number_temp       = sub:fax_number
              invoice_EMail_Address         = sub:EmailAddress
  
          End! If sub:invoice_customer_address = 'YES'
  
      else! if tra:use_sub_accounts = 'YES'
          If tra:invoice_customer_address = 'YES'
              invoice_address1_temp = job:address_line1
              invoice_address2_temp = job:address_line2
              If job:address_line3_delivery   = ''
                  invoice_address3_temp = job:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp = job:address_line3
                  invoice_address4_temp = job:postcode
              End ! If
              invoice_company_name_temp     = job:company_name
              invoice_telephone_number_temp = job:telephone_number
              invoice_fax_number_temp       = job:fax_number
              invoice_EMail_Address         = jobe:EndUserEmailAddress ! '' ! tra:EmailAddress
  
          Else! If tra:invoice_customer_address = 'YES'
              !            UseAlternativeAddress# = 1
              invoice_address1_temp = tra:address_line1
              invoice_address2_temp = tra:address_line2
              If job:address_line3_delivery   = ''
                  invoice_address3_temp = tra:postcode
                  invoice_address4_temp = ''
              Else
                  invoice_address3_temp = tra:address_line3
                  invoice_address4_temp = tra:postcode
              End ! If
              invoice_company_name_temp     = tra:company_name
              invoice_telephone_number_temp = tra:telephone_number
              invoice_fax_number_temp       = tra:fax_number
              invoice_EMail_Address         = tra:EmailAddress
  
          End! If tra:invoice_customer_address = 'YES'
      end!!if tra:use_sub_accounts = 'YES'
  !End ! If ~glo:WebJob And jobe:WebJob
  
  !**************************************************
  
  
  !*************************set the display address to the head account if booked on as a web job***********************
  ! but only if this was not from a generic account
  
  
  !*********************************************************************************************************************
  
  
  !**************************************************
  If job:chargeable_job <> 'YES'
      labour_temp       = ''
      parts_temp        = ''
      courier_cost_temp = ''
      vat_temp          = ''
      total_temp        = ''
  Else! If job:chargeable_job <> 'YES'
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Found
  
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Error
      End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
      If job:invoice_number <> ''
          ! Total_Price('I',vat",total",balance")
          access:invoice.clearkey(inv:invoice_number_key)
          inv:invoice_number = job:invoice_number
          access:invoice.fetch(inv:invoice_number_key)
          if p_web.GSV('BookingSite') = 'RRC' then
              If inv:ExportedRRCOracle
                  Labour_Temp = jobe:InvRRCCLabourCost
                  Parts_Temp  = jobe:InvRRCCPartsCost
                  Vat_Temp    = jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour / 100 + |
                  jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts / 100 +                 |
                  job:Invoice_Courier_Cost * inv:Vat_Rate_Labour / 100
                  courier_cost_temp = job:invoice_courier_cost
              Else ! If inv:ExportedRRCOracle
                  Labour_Temp = jobe:RRCCLabourCost
                  Parts_Temp  = jobe:RRCCPartsCost
                  Vat_Temp    = jobe:RRCClabourCost * GetVATRate(job:Account_Number, 'L') / 100 + |
                  jobe:RRCCPartsCost * GetVATRate(job:Account_Number, 'P') / 100 +                |
                  job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100
                  courier_cost_temp = job:courier_cost
              End ! If inv:ExportedRRCOracle
          else
              labour_temp = job:invoice_labour_cost
              Parts_temp  = job:Invoice_Parts_Cost
              Vat_temp    = job:Invoice_Labour_Cost * inv:Vat_Rate_Labour / 100 + |
              job:Invoice_Parts_Cost * inv:Vat_Rate_Parts / 100 +                 |
              job:Invoice_Courier_Cost * inv:Vat_Rate_Labour / 100
              courier_cost_temp = job:invoice_courier_cost
          end ! if
  
      Else! If job:invoice_number <> ''
          If p_web.GSV('BookingSite') = 'RRC' Then
              Labour_Temp = jobe:RRCCLabourCost
              Parts_Temp  = jobe:RRCCPartsCost
              Vat_Temp    = jobe:RRCClabourCost * GetVATRate(job:Account_Number, 'L') / 100 + |
              jobe:RRCCPartsCost * GetVATRate(job:Account_Number, 'P') / 100 +                |
              job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100
          Else
              labour_temp = job:labour_cost
              parts_temp  = job:parts_cost
              Vat_Temp    = job:Labour_Cost * GetVATRate(job:Account_Number, 'L') / 100 + |
              job:Parts_Cost * GetVATRate(job:Account_Number, 'P') / 100 +                |
              job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100
          End ! If
  
          courier_cost_temp = job:courier_cost
      End! If job:invoice_number <> ''
      Total_Temp  = Labour_Temp + Parts_Temp + Courier_Cost_Temp + Vat_Temp
  
  End! If job:chargeable_job <> 'YES'
  !**************************************************
  
  setcursor(cursor:wait)
  
  FieldNumber# = 1
  save_maf_id  = access:manfault.savefile()
  access:manfault.clearkey(maf:field_number_key)
  maf:manufacturer = job:manufacturer
  set(maf:field_number_key, maf:field_number_key)
  loop
      if access:manfault.next()
          break
      end ! if
      if maf:manufacturer <> job:manufacturer      |
          then break   ! end if
      end ! if
      yldcnt# += 1
      if yldcnt# > 25
          yield()
          yldcnt# = 0
      end ! if
  
      If maf:Compulsory_At_Booking <> 'YES'
          Cycle
      End ! If maf:Compulsory_At_Booking <> 'YES'
  
      fault_code_field_temp[FieldNumber#] = maf:field_name
  
      Access:MANFAULO.ClearKey(mfo:Field_Key)
      mfo:Manufacturer = job:Manufacturer
      mfo:Field_Number = maf:Field_Number
  
  
      Case maf:Field_Number
      Of 1
          mfo:Field = job:Fault_Code1
      Of 2
          mfo:Field = job:Fault_Code2
      Of 3
          mfo:Field = job:Fault_Code3
      Of 4
          mfo:Field = job:Fault_Code4
      Of 5
          mfo:Field = job:Fault_Code5
      Of 6
          mfo:Field = job:Fault_Code6
      Of 7
          mfo:Field = job:Fault_Code7
      Of 8
          mfo:Field = job:Fault_Code8
      Of 9
          mfo:Field = job:Fault_Code9
      Of 10
          mfo:Field = job:Fault_Code10
      Of 11
          mfo:Field = job:Fault_Code11
      Of 12
          mfo:Field = job:Fault_Code12
      ! Inserting (DBH 21/04/2006) #7551 - Display the extra fault codes, if comp at booking
      Of 13
          mfo:Field = wob:FaultCode13
      Of 14
          mfo:Field = wob:FaultCode14
      Of 15
          mfo:Field = wob:FaultCode15
      Of 16
          mfo:Field = wob:FaultCode16
      Of 17
          mfo:Field = wob:FaultCode17
      Of 18
          mfo:Field = wob:FaultCode18
      Of 19
          mfo:Field = wob:FaultCode19
      Of 20
          mfo:Field = wob:FaultCode20
      End ! Case maf:Field_Number
      ! End (DBH 21/04/2006) #7551
  
      tmp:FaultCodeDescription[FieldNumber#] = mfo:Field
  
      If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
          ! Found
          tmp:FaultCodeDescription[FieldNumber#] = Clip(tmp:FaultCodeDescription[FieldNumber#]) & ' - ' & Clip(mfo:Description)
      Else! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
          ! Error
          ! Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
  
      FieldNumber# += 1
      If FieldNumber# > 6
          Break
      End ! If FieldNumber# > 6
  end ! loop
  access:manfault.restorefile(save_maf_id)
  setcursor()
  
  tmp:accessories = ''
  setcursor(cursor:wait)
  x#          = 0
  Save_jac_ID = Access:JOBACC.SaveFile()
  Access:JOBACC.ClearKey(jac:DamagedKey)
  jac:Ref_Number = job:Ref_Number
  jac:Damaged    = 0
  jac:Pirate     = 0
  Set(jac:DamagedPirateKey, jac:DamagedPirateKey)
  Loop
      If Access:JOBACC.NEXT()
          Break
      End ! If
      If jac:Ref_Number <> job:Ref_Number |
          Then Break
      End ! If
      yldcnt# += 1
      if yldcnt# > 25
          yield()
          yldcnt# = 0
      end ! if
      x# += 1
      If jac:Damaged
          jac:Accessory = 'DAMAGED ' & Clip(jac:Accessory)
      End ! If jac:Damaged
      If jac:Pirate
          jac:Accessory = 'PIRATE ' & Clip(jac:Accessory)
      End ! If jac:Pirate
      If tmp:accessories = ''
          tmp:accessories = jac:accessory
      Else! If tmp:accessories = ''
          tmp:accessories = Clip(tmp:accessories) & ',  ' & Clip(jac:accessory)
      End! If tmp:accessories = ''
  End ! loop
  access:jobacc.restorefile(save_jac_id)
  setcursor()
  
  
  If job:exchange_unit_number <> ''
      access:exchange.clearkey(xch:ref_number_key)
      xch:ref_number = job:exchange_unit_number
      if access:exchange.fetch(xch:ref_number_key) = Level:Benign
          exchange_unit_number_temp  = 'Unit: ' & Clip(job:exchange_unit_number)
          exchange_model_number      = xch:model_number
          exchange_manufacturer_temp = xch:manufacturer
          exchange_unit_type_temp    = 'N/A'
          exchange_esn_temp          = xch:esn
          exchange_unit_title_temp   = 'EXCHANGE UNIT'
          exchange_msn_temp          = xch:msn
      end! if access:exchange.fetch(xch:ref_number_key) = Level:Benign
  Else! If job:exchange_unit_number <> ''
      x#          = 0
      save_jea_id = access:jobexacc.savefile()
      access:jobexacc.clearkey(jea:part_number_key)
      jea:job_ref_number = job:ref_number
      set(jea:part_number_key, jea:part_number_key)
      loop
          if access:jobexacc.next()
              break
          end ! if
          if jea:job_ref_number <> job:ref_number      |
              then break   ! end if
          end ! if
          x# = 1
          Break
      end ! loop
      access:jobexacc.restorefile(save_jea_id)
      If x# = 1
          exchange_unit_number_temp  = ''
          exchange_model_number      = 'N/A'
          exchange_manufacturer_temp = job:manufacturer
          exchange_unit_type_temp    = 'ACCESSORY'
          exchange_esn_temp          = 'N/A'
          exchange_unit_title_temp   = 'EXCHANGE UNIT'
          exchange_msn_temp          = 'N/A'
      End! If x# = 1
  End! If job:exchange_unit_number <> ''
  
  ! Check For Bouncer
  access:manufact.clearkey(man:manufacturer_key)
  man:manufacturer = job:manufacturer
  access:manufact.tryfetch(man:manufacturer_key)
  
  tmp:bouncers = ''
  If job:ESN <> 'N/A' And job:ESN <> ''
      setcursor(cursor:wait)
      save_job2_id = access:jobs2_alias.savefile()
      access:jobs2_alias.clearkey(job2:esn_key)
      job2:esn = job:esn
      set(job2:esn_key, job2:esn_key)
      loop
          if access:jobs2_alias.next()
              break
          end ! if
          if job2:esn <> job:esn      |
              then break   ! end if
          end ! if
          yldcnt# += 1
          if yldcnt# > 25
              yield()
              yldcnt# = 0
          end ! if
          If job2:Cancelled = 'YES'
              Cycle
          End ! If job2:Cancelled = 'YES'
  
          If job2:ref_number <> job:ref_number
  !            If job2:date_booked + man:warranty_period > job:date_booked And job2:date_booked <= job:date_booked
                  If tmp:bouncers = ''
                      tmp:bouncers = 'Previous Job Number(s): ' & job2:ref_number
                  Else! If tmp:bouncer = ''
                      tmp:bouncers = CLip(tmp:bouncers) & ', ' & job2:ref_number
                  End! If tmp:bouncer = ''
  !            End! If job2:date_booked + man:warranty_period < Today()
          End! If job2:esn <> job2:ref_number
      end ! loop
      access:jobs2_alias.restorefile(save_job2_id)
      setcursor()
  End ! job:ESN <> 'N/A' And job:ESN <> ''
  
  If tmp:bouncers <> ''
      Settarget(report)
      Unhide(?bouncertitle)
      Unhide(?tmp:bouncers)
      Settarget()
  End! If CheckBouncer(job:ref_number)
  
  If man:UseInvTextForFaults
      tmp:InvoiceText = ''
      Save_joo_ID     = Access:JOBOUTFL.SaveFile()
      Access:JOBOUTFL.ClearKey(joo:LevelKey)
      joo:JobNumber = job:Ref_Number
      joo:Level     = 10
      Set(joo:LevelKey, joo:LevelKey)
      Loop
          If Access:JOBOUTFL.PREVIOUS()
              Break
          End ! If
            If joo:JobNumber <> job:Ref_Number      |
                Then Break   ! End If
            End ! If
            !do not include any 0 entry values
            If Instring('IMEI VALIDATION: ',joo:Description,1,1)
                Cycle
            End ! If Instring('IMEI VALIDATION: ',joo:Description,1,1)            
          If tmp:InvoiceText = ''
              tmp:InvoiceText = Clip(joo:FaultCode) & ' - ' & Clip(joo:Description)
          Else ! If tmp:InvoiceText = ''
              tmp:InvoiceText = Clip(tmp:InvoiceText) & '<13,10>' & Clip(joo:FaultCode) & ' - ' & Clip(joo:Description)
          End ! If tmp:InvoiceText = ''
      End ! Loop
      Access:JOBOUTFL.RestoreFile(Save_joo_ID)
  Else ! man:UseInvTextForFaults
      tmp:InvoiceText = jbn:Invoice_Text
  End ! man:UseInvTextForFaults
  
  ! Loan unit information (Added by Gary (7th Aug 2002))
  ! //------------------------------------------------------------------------
  tmp:LoanModel       = ''
  tmp:LoanIMEI        = ''
  tmp:LoanAccessories = ''
  
  if job:Loan_Unit_Number <> 0                                            ! Is a loan unit attached to this job ?
  
      save_loa_id = Access:Loan.SaveFile()
      Access:Loan.Clearkey(loa:Ref_Number_Key)
      loa:Ref_Number  = job:Loan_Unit_Number
      if not Access:Loan.TryFetch(loa:Ref_Number_Key)                         ! Fetch loan unit record
          tmp:LoanModel        = clip(loa:Manufacturer) & ' ' & loa:Model_Number
          tmp:LoanIMEI         = loa:ESN
          tmp:ReplacementValue = jobe:LoanReplacementValue
  
          save_lac_id = Access:LoanAcc.SaveFile()
          Access:LoanAcc.ClearKey(lac:ref_number_key)
          lac:ref_number = loa:ref_number
          set(lac:ref_number_key, lac:ref_number_key)
          loop until Access:LoanAcc.Next()                                    ! Fetch loan accessories
              if lac:ref_number <> loa:ref_number then break.
              if lac:Accessory_Status <> 'ISSUE' then cycle.                  ! Only display issued loan accessories
              if tmp:LoanAccessories = ''
                  tmp:LoanAccessories = lac:Accessory
              else
                  tmp:LoanAccessories = clip(tmp:LoanAccessories) & ', ' & lac:Accessory
              end ! if
  
          end ! loop
          Access:LoanAcc.RestoreFile(save_lac_id)
  
      end ! if
      Access:Loan.RestoreFile(save_loa_id)
  
  end ! if
  
  if tmp:LoanModel <> ''                                                      ! Display loan details on report
      SetTarget(report)
      Unhide(?LoanUnitIssued)
      Unhide(?tmp:LoanModel)
      Unhide(?IMEITitle)
      Unhide(?tmp:LoanIMEI)
      Unhide(?LoanAccessoriesTitle)
      Unhide(?tmp:LoanAccessories)
      Unhide(?tmp:ReplacementValue)
      Unhide(?LoanValueText)
      SetTarget()
  end ! if
  
  
  tmp:LoanDepositPaid = 0
  
  Access:JobPaymt.ClearKey(jpt:Loan_Deposit_Key)
  jpt:Ref_Number   = job:Ref_Number
  jpt:Loan_Deposit = True
  set(jpt:Loan_Deposit_Key, jpt:Loan_Deposit_Key)
  loop until Access:JobPaymt.Next()                                           ! Loop through payments attached to job
      if jpt:Ref_Number <> job:Ref_Number then break.
      if jpt:Loan_Deposit <> True then break.
      tmp:LoanDepositPaid += jpt:Amount                                       ! Check for deposit value
  end ! loop
  
  if tmp:LoanDepositPaid <> 0
      SetTarget(report)
      Unhide(?LoanDepositPaidTitle)
      Unhide(?tmp:LoanDepositPaid)                                            ! Display fields if a deposit was found
      SetTarget()
  end ! if
  ! //------------------------------------------------------------------------
  
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR1:rtn = PDFXTR1.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR1:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

