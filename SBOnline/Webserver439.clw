

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER439.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER227.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
BuildStockReceiveList PROCEDURE  (NetWebServerWorker p_web,LONG fRefNumber,BYTE fExchangeOrder,BYTE fLoanOrder) ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
    DO OpenFiles

    Access:RETSTOCK.Clearkey(res:Despatched_Key)
    res:Ref_Number = fRefNumber
    res:Despatched = 'YES'
    SET(res:Despatched_Key,res:Despatched_Key)
    LOOP UNTIL Access:RETSTOCK.Next()
   
        IF (res:Ref_Number <> fRefNumber OR |
            res:Despatched <> 'YES')
            BREAK
        END
        
        addNewPart# = 1
        IF (p_web.GSV('ret:ExchangeOrder') = 1 OR p_web.GSV('ret:LoanOrder') = 1)
            ! #12127 Group Parts If Exchange Order. Keep Received Seperate (Bryan: 05/07/2011)
            Access:STOCKRECEIVETMP.Clearkey(stotmp:ReceivedPartNumberKey)
            stotmp:SessionID = p_web.SessionID
            stotmp:Received = res:Received
            stotmp:PartNumber = res:Part_Number
            IF (Access:STOCKRECEIVETMP.TryFetch(stotmp:ReceivedPartNumberKey) = Level:Benign)
                stotmp:Quantity += 1
                stotmp:QuantityReceived += res:QuantityReceived
                Access:STOCKRECEIVETMP.TryUpdate()
                addNewPart# = 0
            END
        END ! IF (ret:ExchangeOrder = 1)
        
        IF (addNewPart# = 1)
            IF (Access:STOCKRECEIVETMP.PrimeRecord() = Level:Benign)
                stotmp:SessionID = p_web.SessionID
                stotmp:PartNumber = res:Part_Number
                stotmp:Description = res:Description
                stotmp:ItemCost = res:Item_Cost
                stotmp:Quantity = res:Quantity
                stotmp:QuantityReceived = res:QuantityReceived
                stotmp:RESRecordNumber = res:Record_Number
                stotmp:Received = res:Received
                stotmp:ExchangeOrder = fExchangeOrder
                stotmp:LoanOrder = fLoanOrder

                !IF (Access:STOCKRECEIVETMP.TryInsert())
                if Access:STOCKRECEIVETMP.TryUpdate()
                    Access:STOCKRECEIVETMP.CancelAutoInc()
                END
            END
            
        END ! IF (addNewPart# = 1)
        
    END !loop

    Do CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:STOCKRECEIVETMP.Open                              ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCKRECEIVETMP.UseFile                           ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:RETSTOCK.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:RETSTOCK.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:STOCKRECEIVETMP.Close
     Access:RETSTOCK.Close
     FilesOpened = False
  END
