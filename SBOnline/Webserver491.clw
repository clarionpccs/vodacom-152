

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER491.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
PreviousIMEI         PROCEDURE  (f:IMEI)                   ! Declare Procedure
save_jobs_alias_id   USHORT,AUTO                           !
save_exchange_id     USHORT,AUTO                           !
save_jobthird_id     USHORT,AUTO                           !

  CODE
    If Clip(f:IMEI) = '' Or f:IMEI = 'N/A'
        Return False
    End ! If Clip(f:IMEI) = '' Or f:IMEI = 'N/A'
    Found# = False
    Save_JOBS_ALIAS_ID = Access:JOBS_ALIAS.SaveFile()
    Access:JOBS_ALIAS.Clearkey(job_ali:ESN_Key)
    job_ali:ESN = f:IMEI
    Set(job_ali:ESN_Key,job_ali:ESN_Key)
    Loop ! Begin Loop
        If Access:JOBS_ALIAS.Next()
            Break
        End ! If Access:JOBS_ALIAS.Next()
        If job_ali:ESN <> f:IMEI
            Break
        End ! If job_ali:ESN <> f:IMEI
        Found# = True
        Break
    End ! Loop
    Access:JOBS_ALIAS.RestoreFile(Save_JOBS_ALIAS_ID)

    If Found# = True
        Return True
    End ! If Found# = True

    Save_JOBTHIRD_ID = Access:JOBTHIRD.SaveFile()
    Access:JOBTHIRD.Clearkey(jot:OriginalIMEIKey)
    jot:OriginalIMEI     = f:IMEI
    Set(jot:OriginalIMEIKey,jot:OriginalIMEIKey)
    Loop ! Begin Loop
        If Access:JOBTHIRD.Next()
            Break
        End ! If Access:JOBTHIRD.Next()
        If jot:OriginalIMEI <> f:IMEI
            Break
        End ! If jot:OriginalIMEI <> f:IMEI
        Found# = True
        Break
    End ! Loop
    Access:JOBTHIRD.RestoreFile(Save_JOBTHIRD_ID)

    If Found# = True
        Return True
    End ! If Found# = True

    Save_EXCHANGE_ID = Access:EXCHANGE.SaveFile()
    Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
    xch:ESN = f:IMEI
    Set(xch:ESN_Only_Key,xch:ESN_Only_Key)
    Loop ! Begin Loop
        If Access:EXCHANGE.Next()
            Break
        End ! If Access:EXCHANGE.Next()
        IF xch:ESN <> f:IMEI
            Break
        End ! IF xch:ESN <> f:IMEI
        If xch:Job_Number <> ''
            Found# = True
            Break
        End ! If xch:Job_Number <> ''
    End ! Loop
    Access:EXCHANGE.RestoreFile(Save_EXCHANGE_ID)

    If Found# = True
        Return True
    End ! If Found# = True

    Return False



