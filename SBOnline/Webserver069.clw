

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER069.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
SentToHub            PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
    do openFiles
    p_web.SSV('SentToHub',0)
    if (p_web.GSV('jobe:HubRepair') = 1 Or p_web.GSV('jobe:HubRepairDate') > 0)
        p_web.SSV('SentToHub',1)
    Else !If jobe:HubRepair
        Access:LOCATLOG.ClearKey(lot:NewLocationKey)
        lot:RefNumber   = p_web.GSV('job:Ref_Number')
        lot:NewLocation = p_web.GSV('Default:ARCLocation')
        If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
            p_web.SSV('SentToHub',1)
        End !If Access:LOCATLOG.TryFetch(lot:NewLocationKey)
    End !If jobe:HubRepair
    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:LOCATLOG.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOCATLOG.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:LOCATLOG.Close
     Access:JOBSE.Close
     FilesOpened = False
  END
