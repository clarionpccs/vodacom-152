

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER452.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER010.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER048.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER082.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER120.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER211.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER213.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER375.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER387.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER453.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER454.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER455.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER460.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER463.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER465.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER466.INC'),ONCE        !Req'd for module callout resolution
                     END


FormBrowseStock      PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
locFilterByPartNumber STRING(30)                           !
locPartNumber        STRING(30)                            !
Ans                  LONG                                  !
HideAddBtn           BYTE                                  !
HideDepleteBtn       BYTE                                  !
HideHistoryBtn       BYTE                                  !
HideOrderBtn         BYTE                                  !
HideReturnBtn        BYTE                                  !
HideSuspendBtn       BYTE                                  !
locOldPartNumber     STRING(30)                            !
locFilterByManufacturer BYTE                               !
locManufacturer      STRING(30)                            !
locFilterByDescription BYTE                                !
LocDescription       STRING(30)                            !
locFilterByShelfLocation BYTE                              !
LocShelfLocation     STRING(30)                            !
locFilterBySupplier  BYTE                                  !
LocSupplier          STRING(30)                            !
BrowseStockControl_sort LONG                               !
FilesOpened     Long
SUPPLIER::State  USHORT
LOCSHELF::State  USHORT
MANUFACT::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormBrowseStock')
  loc:formname = 'FormBrowseStock_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormBrowseStock',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormBrowseStock','')
    p_web._DivHeader('FormBrowseStock',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormBrowseStock',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormBrowseStock',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseStock',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormBrowseStock',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormBrowseStock',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
HideButtons         ROUTINE
    !default to showing them
    HideAddBtn      = false
    HideDepleteBtn  = false
    HideHistoryBtn  = false
    HideOrderBtn    = false
    HideReturnBtn   = false
    HideSuspendBtn  = false
    
    ! #13128 Always show the update buttons, unless the access prevents it (DBH: 18/03/2014)
!    if p_web.GSV('BrowseID')='' then 
!        !hide all BrowseButtons  
!        HideAddBtn      = true
!        HideDepleteBtn  = true
!        HideHistoryBtn  = true
!        HideOrderBtn    = true
!        HideReturnBtn   = true
!        HideSuspendBtn  = true
!        
!    ELSE
        !will depend on the permissions
        IF DoesUserHaveAccess(p_web,'STOCK - ADD STOCK') = FALSE
            HideAddBtn  = TRUE
        END
        IF DoesUserHaveAccess(p_web,'STOCK - DEPLETE STOCK') = FALSE
            HideDepleteBtn  = TRUE
        END
        IF DoesUserHaveAccess(p_web,'STOCK - STOCK HISTORY') = FALSE
            HideHistoryBtn  = TRUE
        END
        IF DoesUserHaveAccess(p_web,'STOCK - ORDER STOCK') = FALSE
            HideOrderBtn  = TRUE
        END
        IF DoesUserHaveAccess(p_web,'RRC/ARC RETURN STOCK') = FALSE
            HideReturnBtn  = TRUE
        END
        IF DoesUserHaveAccess(p_web,'SUSPEND PARTS') = FALSE
            HideSuspendBtn  = TRUE
        END
   ! END
    
    !set up the session variables to match
    p_web.SSV('HideAddBtn'      ,HideAddBtn)
    p_web.SSV('HideDepleteBtn'  ,HideDepleteBtn)
    p_web.SSV('HideHistoryBtn'  ,HideHistoryBtn)
    p_web.SSV('HideOrderBtn'    ,HideOrderBtn)
    p_web.SSV('HideReturnBtn'   ,HideReturnBtn)
    p_web.SSV('HideSuspendBtn'  ,HideSuspendBtn)
    
    EXIT
    
    !taken from BROWSE_STOCK in sbb01app.dll
    !
    ![Browse Buttons]
    !?Button_Group?
    !Add Stock		?Add_Stock		SecurityCheck('STOCK - ADD STOCK')
    !Deplete Stock	?Deplete_Stock	SecurityCheck('STOCK _ DEPLETE STOCK')
    !Stock History	?Stock_History	SecurityCheck('STOCK - STOCK HISTORY')
    !Order Stock	?Order_Stock	SecurityCheck('STOCK - ORDER STOCK')
    !Return Stock	?ReturnStock - only show if this is not the mainStoreLocation - 
    !               ALSO            SecurityCheck('RRC/ARC RETURN STOCK')    
    !
    !ShowHideReplicate       Routine
    !    If Location_Temp <> MainStoreLocation()
    !        ?DuplicateStock{prop:Hide} = 1
    !        ?ReturnStock{prop:Hide} = 0    ! #12151 Show Return Stock Button (Bryan: 01/09/2011)
    !        !?buttonReturnStockTracking{prop:Hide} = 0
    !    Else
    !        ?DuplicateStock{prop:Hide} = 0
    !        ?ReturnStock{prop:Hide} = 1      ! #12151 Show Return Stock Button (Bryan: 01/09/2011)
    !        !?buttonReturnStockTracking{prop:Hide} = 1
    !    End !If Location_Temp <> MainStoreLocation()
    !
    ![Procedure Buttons]
    !Outstanding Orders
    !Suspended Stock			SecurityCheck('SUSPEND PARTS')
    !Return Stock Tracking
    !Browse Model Stock
    !
    ![MENU]
    !Adjusted Web Order Receipts
    !Browse Retail Sales
    !Pending Orders
    !Stock Receive Procedure
    !Exchange    Allocation
OpenFiles  ROUTINE
  p_web._OpenFile(SUPPLIER)
  p_web._OpenFile(LOCSHELF)
  p_web._OpenFile(MANUFACT)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(SUPPLIER)
  p_Web._CloseFile(LOCSHELF)
  p_Web._CloseFile(MANUFACT)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormBrowseStock_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'locManufacturer'
    p_web.setsessionvalue('showtab_FormBrowseStock',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MANUFACT)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locFilterByShelfLocation')
  Of 'LocShelfLocation'
    p_web.setsessionvalue('showtab_FormBrowseStock',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(LOCSHELF)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locFilterBySupplier')
  Of 'LocSupplier'
    p_web.setsessionvalue('showtab_FormBrowseStock',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUPPLIER)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.BrowseStockControl_sort')
  End
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locOldPartNumber',locOldPartNumber)
  p_web.SetSessionValue('locFilterByPartNumber',locFilterByPartNumber)
  p_web.SetSessionValue('locPartNumber',locPartNumber)
  p_web.SetSessionValue('locFilterByDescription',locFilterByDescription)
  p_web.SetSessionValue('LocDescription',LocDescription)
  p_web.SetSessionValue('locFilterByManufacturer',locFilterByManufacturer)
  p_web.SetSessionValue('locManufacturer',locManufacturer)
  p_web.SetSessionValue('locFilterByShelfLocation',locFilterByShelfLocation)
  p_web.SetSessionValue('LocShelfLocation',LocShelfLocation)
  p_web.SetSessionValue('locFilterBySupplier',locFilterBySupplier)
  p_web.SetSessionValue('LocSupplier',LocSupplier)
  p_web.SetSessionValue('BrowseStockControl_sort',BrowseStockControl_sort)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locOldPartNumber')
    locOldPartNumber = p_web.GetValue('locOldPartNumber')
    p_web.SetSessionValue('locOldPartNumber',locOldPartNumber)
  End
  if p_web.IfExistsValue('locFilterByPartNumber')
    locFilterByPartNumber = p_web.GetValue('locFilterByPartNumber')
    p_web.SetSessionValue('locFilterByPartNumber',locFilterByPartNumber)
  End
  if p_web.IfExistsValue('locPartNumber')
    locPartNumber = p_web.GetValue('locPartNumber')
    p_web.SetSessionValue('locPartNumber',locPartNumber)
  End
  if p_web.IfExistsValue('locFilterByDescription')
    locFilterByDescription = p_web.GetValue('locFilterByDescription')
    p_web.SetSessionValue('locFilterByDescription',locFilterByDescription)
  End
  if p_web.IfExistsValue('LocDescription')
    LocDescription = p_web.GetValue('LocDescription')
    p_web.SetSessionValue('LocDescription',LocDescription)
  End
  if p_web.IfExistsValue('locFilterByManufacturer')
    locFilterByManufacturer = p_web.GetValue('locFilterByManufacturer')
    p_web.SetSessionValue('locFilterByManufacturer',locFilterByManufacturer)
  End
  if p_web.IfExistsValue('locManufacturer')
    locManufacturer = p_web.GetValue('locManufacturer')
    p_web.SetSessionValue('locManufacturer',locManufacturer)
  End
  if p_web.IfExistsValue('locFilterByShelfLocation')
    locFilterByShelfLocation = p_web.GetValue('locFilterByShelfLocation')
    p_web.SetSessionValue('locFilterByShelfLocation',locFilterByShelfLocation)
  End
  if p_web.IfExistsValue('LocShelfLocation')
    LocShelfLocation = p_web.GetValue('LocShelfLocation')
    p_web.SetSessionValue('LocShelfLocation',LocShelfLocation)
  End
  if p_web.IfExistsValue('locFilterBySupplier')
    locFilterBySupplier = p_web.GetValue('locFilterBySupplier')
    p_web.SetSessionValue('locFilterBySupplier',locFilterBySupplier)
  End
  if p_web.IfExistsValue('LocSupplier')
    LocSupplier = p_web.GetValue('LocSupplier')
    p_web.SetSessionValue('LocSupplier',LocSupplier)
  End
  if p_web.IfExistsValue('BrowseStockControl_sort')
    BrowseStockControl_sort = p_web.GetValue('BrowseStockControl_sort')
    p_web.SetSessionValue('BrowseStockControl_sort',BrowseStockControl_sort)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormBrowseStock_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    p_web.site.SaveButton.TextValue = 'Close'
    ClearTagFile(p_web)
        !p_web.SSV('js:RadioButton','window.open(''FormBrowseStock?Locator2BrowseStockControl=&Locator1BrowseStockControl=&BrowseStockControl_sort='' + this.value,''_self'')')
    p_web.SSV('js:RadioButton','BrowseStockControl.sort(this.value)') 
  ! Clear various variables
  
    p_web.SSV('locStockAllocationType','ALL')
  
    IF (p_web.GSV('FirstTime') = 1)
        p_web.SSV('FirstTime',0)
        locOldPartNumber        = ''
        locFilterByManufacturer = FALSE
        locManufacturer         = ''
        locFilterByDescription  = FALSE
        LocDescription          = ''
        locFilterByShelfLocation = FALSE
        LocShelfLocation        = ''
        locFilterBySupplier     = FALSE
        LocSupplier             = ''
          !sto:Part_Number         = ''
          !Loc:LocatorValue        = ''
          
        p_web.SSV('locOldPartNumber',locOldPartNumber)
        p_web.SSV('locFilterByManufacturer',locFilterByManufacturer)
        p_web.SSV('locManufacturer',locManufacturer)
        p_web.SSV('locFilterByDescription',locFilterByDescription)
        p_web.SSV('LocDescription',LocDescription)
        p_web.SSV('locFilterByShelfLocation',locFilterByShelfLocation)
        p_web.SSV('LocShelfLocation',LocShelfLocation)
        p_web.SSV('locFilterBySupplier',locFilterBySupplier)
        p_web.SSV('LocSupplier',LocSupplier)
        p_web.SSV('locFilterByPartNumber',0)
        p_web.SSV('locPartNumber','')
          !p_web.SetSessionValue('sto:Part_Number',sto:Part_Number)
              ! Set default sort order
        p_web.SSV('BrowseStockControl_sort',1)
    END
    do hideButtons
    
        
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locOldPartNumber = p_web.RestoreValue('locOldPartNumber')
 locFilterByPartNumber = p_web.RestoreValue('locFilterByPartNumber')
 locPartNumber = p_web.RestoreValue('locPartNumber')
 locFilterByDescription = p_web.RestoreValue('locFilterByDescription')
 LocDescription = p_web.RestoreValue('LocDescription')
 locFilterByManufacturer = p_web.RestoreValue('locFilterByManufacturer')
 locManufacturer = p_web.RestoreValue('locManufacturer')
 locFilterByShelfLocation = p_web.RestoreValue('locFilterByShelfLocation')
 LocShelfLocation = p_web.RestoreValue('LocShelfLocation')
 locFilterBySupplier = p_web.RestoreValue('locFilterBySupplier')
 LocSupplier = p_web.RestoreValue('LocSupplier')
 BrowseStockControl_sort = p_web.RestoreValue('BrowseStockControl_sort')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'IndexPage'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormBrowseStock_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormBrowseStock_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormBrowseStock_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormBrowseStock" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormBrowseStock" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormBrowseStock" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Browse Stock') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Browse Stock',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket
    do SendPacket
    Do Menu
    do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormBrowseStock">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormBrowseStock" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormBrowseStock')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Options') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Browse Stock') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormBrowseStock')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormBrowseStock'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormBrowseStock_BrowseStockControl_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormBrowseStock_BrowseStockControl_embedded_div')&''');'
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='MANUFACT'
          If Not (p_web.GSV('locFilterByShelfLocation') <> 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.LocShelfLocation')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='LOCSHELF'
          If Not (p_web.gsv('locFilterBySupplier')<>1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.LocSupplier')
          End
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locOldPartNumber')
    End
  End
  ! Set Select Field
    p_web.SetValue('SelectField',clip(loc:formname) & '.Locator2BrowseStockControl')
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormBrowseStock')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Options') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormBrowseStock_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Options')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Options')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Options')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Options')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&195&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locOldPartNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locOldPartNumber
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&195&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnOldPartNumberSearch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&195&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFilterByPartNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFilterByPartNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&195&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPartNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPartNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&195&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFilterByDescription
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFilterByDescription
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&195&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::LocDescription
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::LocDescription
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&195&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFilterByManufacturer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFilterByManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&195&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locManufacturer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&195&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFilterByShelfLocation
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFilterByShelfLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&195&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::LocShelfLocation
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::LocShelfLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&195&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFilterBySupplier
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFilterBySupplier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&195&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::LocSupplier
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::LocSupplier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Browse Stock') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormBrowseStock_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Stock')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Stock')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Stock')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Stock')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::BrowseStockControl_sort
      do Value::BrowseStockControl_sort
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseStockControl
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormBrowseStock_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnAddStock
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnDepleteStock
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnStockHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnOrderStock
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnReturnStock
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnBrowseOutstandingOrders
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnSuspendedStock
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnReturnStockTracking
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnBrowseModelStock
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locOldPartNumber  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('locOldPartNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('"Old" Part Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locOldPartNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locOldPartNumber',p_web.GetValue('NewValue'))
    locOldPartNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locOldPartNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locOldPartNumber',p_web.GetValue('Value'))
    locOldPartNumber = p_web.GetValue('Value')
  End
    locOldPartNumber = Upper(locOldPartNumber)
    p_web.SetSessionValue('locOldPartNumber',locOldPartNumber)
  do Value::locOldPartNumber
  do SendAlert

Value::locOldPartNumber  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('locOldPartNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locOldPartNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locOldPartNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locOldPartNumber'',''formbrowsestock_locoldpartnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locOldPartNumber',p_web.GetSessionValueFormat('locOldPartNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,30,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('locOldPartNumber') & '_value')


Validate::btnOldPartNumberSearch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnOldPartNumberSearch',p_web.GetValue('NewValue'))
    do Value::btnOldPartNumberSearch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnOldPartNumberSearch  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('btnOldPartNumberSearch') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnOldPartNoSearch','Search','SmallButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=OldPartNumber&ReturnURL=FormBrowseStock&RedirectURL=frmOldPartSearch')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Prompt::locFilterByPartNumber  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('locFilterByPartNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Filter By Part Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFilterByPartNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFilterByPartNumber',p_web.GetValue('NewValue'))
    locFilterByPartNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFilterByPartNumber
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locFilterByPartNumber',p_web.GetValue('Value'))
    locFilterByPartNumber = p_web.GetValue('Value')
  End
  do Value::locFilterByPartNumber
  do SendAlert
  do Prompt::locPartNumber
  do Value::locPartNumber  !1
  do Value::BrowseStockControl  !1

Value::locFilterByPartNumber  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('locFilterByPartNumber') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locFilterByPartNumber
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locFilterByPartNumber'',''formbrowsestock_locfilterbypartnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locFilterByPartNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locFilterByPartNumber') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locFilterByPartNumber',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('locFilterByPartNumber') & '_value')


Prompt::locPartNumber  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('locPartNumber') & '_prompt',Choose(p_web.GSV('locFilterByPartNumber') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Part Number Contains')
  If p_web.GSV('locFilterByPartNumber') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('locPartNumber') & '_prompt')

Validate::locPartNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPartNumber',p_web.GetValue('NewValue'))
    locPartNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPartNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPartNumber',p_web.GetValue('Value'))
    locPartNumber = p_web.GetValue('Value')
  End
    locPartNumber = Upper(locPartNumber)
    p_web.SetSessionValue('locPartNumber',locPartNumber)
  do Value::locPartNumber
  do SendAlert
  do Value::BrowseStockControl  !1

Value::locPartNumber  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('locPartNumber') & '_value',Choose(p_web.GSV('locFilterByPartNumber') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locFilterByPartNumber') <> 1)
  ! --- STRING --- locPartNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locPartNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPartNumber'',''formbrowsestock_locpartnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPartNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locPartNumber',p_web.GetSessionValueFormat('locPartNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('locPartNumber') & '_value')


Prompt::locFilterByDescription  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('locFilterByDescription') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Filter Browse by Description')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFilterByDescription  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFilterByDescription',p_web.GetValue('NewValue'))
    locFilterByDescription = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFilterByDescription
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locFilterByDescription',p_web.GetValue('Value'))
    locFilterByDescription = p_web.GetValue('Value')
  End
  do Value::locFilterByDescription
  do SendAlert
  do Prompt::LocDescription
  do Value::LocDescription  !1
  do Value::BrowseStockControl  !1

Value::locFilterByDescription  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('locFilterByDescription') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locFilterByDescription
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locFilterByDescription'',''formbrowsestock_locfilterbydescription_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locFilterByDescription')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locFilterByDescription') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locFilterByDescription',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('locFilterByDescription') & '_value')


Prompt::LocDescription  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('LocDescription') & '_prompt',Choose(p_web.GSV('locFilterByDescription') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Description Contains')
  If p_web.GSV('locFilterByDescription') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('LocDescription') & '_prompt')

Validate::LocDescription  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('LocDescription',p_web.GetValue('NewValue'))
    LocDescription = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::LocDescription
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('LocDescription',p_web.GetValue('Value'))
    LocDescription = p_web.GetValue('Value')
  End
    LocDescription = Upper(LocDescription)
    p_web.SetSessionValue('LocDescription',LocDescription)
  do Value::LocDescription
  do SendAlert
  do Value::BrowseStockControl  !1

Value::LocDescription  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('LocDescription') & '_value',Choose(p_web.GSV('locFilterByDescription') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locFilterByDescription') <> 1)
  ! --- STRING --- LocDescription
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('LocDescription')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''LocDescription'',''formbrowsestock_locdescription_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('LocDescription')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','LocDescription',p_web.GetSessionValueFormat('LocDescription'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,30,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('LocDescription') & '_value')


Prompt::locFilterByManufacturer  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('locFilterByManufacturer') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Filter Browse by Manufacturer')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFilterByManufacturer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFilterByManufacturer',p_web.GetValue('NewValue'))
    locFilterByManufacturer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFilterByManufacturer
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locFilterByManufacturer',p_web.GetValue('Value'))
    locFilterByManufacturer = p_web.GetValue('Value')
  End
  do Value::locFilterByManufacturer
  do SendAlert
  do Value::BrowseStockControl  !1
  do Prompt::locManufacturer
  do Value::locManufacturer  !1
  do Value::btnAddStock  !1
  do Value::btnDepleteStock  !1
  do Value::btnOrderStock  !1
  do Value::btnStockHistory  !1
  do Value::btnReturnStock  !1

Value::locFilterByManufacturer  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('locFilterByManufacturer') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locFilterByManufacturer
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locFilterByManufacturer'',''formbrowsestock_locfilterbymanufacturer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locFilterByManufacturer')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locFilterByManufacturer') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locFilterByManufacturer',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('locFilterByManufacturer') & '_value')


Prompt::locManufacturer  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('locManufacturer') & '_prompt',Choose(p_web.GSV('locFilterByManufacturer') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Manufacturer')
  If p_web.GSV('locFilterByManufacturer') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('locManufacturer') & '_prompt')

Validate::locManufacturer  Routine
    If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('Value')
      p_web.GetDescription(MANUFACT,man:Manufacturer_Key,,man:Manufacturer,man:Manufacturer,p_web.GetValue('Value'))
      loc:lookupdone = 1
    Else
      p_web.GetDescription(MANUFACT,man:Manufacturer_Key,,man:Manufacturer,man:Manufacturer,p_web.GetSessionValue('locManufacturer'))
    End
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locManufacturer',p_web.GetValue('NewValue'))
    locManufacturer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locManufacturer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locManufacturer',p_web.GetValue('Value'))
    locManufacturer = p_web.GetValue('Value')
  End
    locManufacturer = Upper(locManufacturer)
    p_web.SetSessionValue('locManufacturer',locManufacturer)
  p_Web.SetValue('lookupfield','locManufacturer')
  do AfterLookup
  do Value::locManufacturer
  do SendAlert
  do Value::BrowseStockControl  !1
  do Value::btnAddStock  !1
  do Value::btnDepleteStock  !1
  do Value::btnOrderStock  !1
  do Value::btnReturnStock  !1
  do Value::btnStockHistory  !1

Value::locManufacturer  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('locManufacturer') & '_value',Choose(p_web.GSV('locFilterByManufacturer') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locFilterByManufacturer') <> 1)
  ! --- STRING --- locManufacturer
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('locFilterByManufacturer') <> 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('locFilterByManufacturer') <> 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('locManufacturer')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locManufacturer'',''formbrowsestock_locmanufacturer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locManufacturer')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','locManufacturer',p_web.GetSessionValueFormat('locManufacturer'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,30,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('SelectManufacturers?LookupField=locManufacturer&Tab=2&ForeignField=man:Manufacturer&_sort=&Refresh=sort&LookupFrom=FormBrowseStock&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('locManufacturer') & '_value')


Prompt::locFilterByShelfLocation  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('locFilterByShelfLocation') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Filter Browse by Shelf Location')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFilterByShelfLocation  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFilterByShelfLocation',p_web.GetValue('NewValue'))
    locFilterByShelfLocation = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFilterByShelfLocation
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locFilterByShelfLocation',p_web.GetValue('Value'))
    locFilterByShelfLocation = p_web.GetValue('Value')
  End
  do Value::locFilterByShelfLocation
  do SendAlert
  do Prompt::LocShelfLocation
  do Value::LocShelfLocation  !1
  do Value::BrowseStockControl  !1

Value::locFilterByShelfLocation  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('locFilterByShelfLocation') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locFilterByShelfLocation
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locFilterByShelfLocation'',''formbrowsestock_locfilterbyshelflocation_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locFilterByShelfLocation')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locFilterByShelfLocation') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locFilterByShelfLocation',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('locFilterByShelfLocation') & '_value')


Prompt::LocShelfLocation  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('LocShelfLocation') & '_prompt',Choose(p_web.GSV('locFilterByShelfLocation') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Shelf Location')
  If p_web.GSV('locFilterByShelfLocation') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('LocShelfLocation') & '_prompt')

Validate::LocShelfLocation  Routine
    If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('Value')
      p_web.GetDescription(LOCSHELF,los:Shelf_Location_Key,,los:Site_Location,los:Shelf_Location,p_web.GetValue('Value'))
      loc:lookupdone = 1
    Else
      p_web.GetDescription(LOCSHELF,los:Shelf_Location_Key,,los:Site_Location,los:Shelf_Location,p_web.GetSessionValue('LocShelfLocation'))
    End
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('LocShelfLocation',p_web.GetValue('NewValue'))
    LocShelfLocation = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::LocShelfLocation
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('LocShelfLocation',p_web.GetValue('Value'))
    LocShelfLocation = p_web.GetValue('Value')
  End
    LocShelfLocation = Upper(LocShelfLocation)
    p_web.SetSessionValue('LocShelfLocation',LocShelfLocation)
  p_Web.SetValue('lookupfield','LocShelfLocation')
  do AfterLookup
  do Value::LocShelfLocation
  do SendAlert
  do Value::BrowseStockControl  !1

Value::LocShelfLocation  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('LocShelfLocation') & '_value',Choose(p_web.GSV('locFilterByShelfLocation') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locFilterByShelfLocation') <> 1)
  ! --- STRING --- LocShelfLocation
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('LocShelfLocation')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''LocShelfLocation'',''formbrowsestock_locshelflocation_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('LocShelfLocation')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','LocShelfLocation',p_web.GetSessionValueFormat('LocShelfLocation'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,30,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('SelectShelfLocation?LookupField=LocShelfLocation&Tab=2&ForeignField=los:Site_Location&_sort=&Refresh=sort&LookupFrom=FormBrowseStock&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('LocShelfLocation') & '_value')


Prompt::locFilterBySupplier  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('locFilterBySupplier') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Filter Browse by Supplier')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFilterBySupplier  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFilterBySupplier',p_web.GetValue('NewValue'))
    locFilterBySupplier = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFilterBySupplier
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locFilterBySupplier',p_web.GetValue('Value'))
    locFilterBySupplier = p_web.GetValue('Value')
  End
  do Value::locFilterBySupplier
  do SendAlert
  do Prompt::LocSupplier
  do Value::LocSupplier  !1
  do Value::BrowseStockControl  !1

Value::locFilterBySupplier  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('locFilterBySupplier') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locFilterBySupplier
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locFilterBySupplier'',''formbrowsestock_locfilterbysupplier_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locFilterBySupplier')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locFilterBySupplier') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locFilterBySupplier',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('locFilterBySupplier') & '_value')


Prompt::LocSupplier  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('LocSupplier') & '_prompt',Choose(p_web.gsv('locFilterBySupplier')<>1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Supplier')
  If p_web.gsv('locFilterBySupplier')<>1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('LocSupplier') & '_prompt')

Validate::LocSupplier  Routine
    If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('Value')
      p_web.GetDescription(SUPPLIER,sup:Company_Name_Key,,sup:Company_Name,sup:Company_Name,p_web.GetValue('Value'))
      loc:lookupdone = 1
    Else
      p_web.GetDescription(SUPPLIER,sup:Company_Name_Key,,sup:Company_Name,sup:Company_Name,p_web.GetSessionValue('LocSupplier'))
    End
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('LocSupplier',p_web.GetValue('NewValue'))
    LocSupplier = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::LocSupplier
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('LocSupplier',p_web.GetValue('Value'))
    LocSupplier = p_web.GetValue('Value')
  End
    LocSupplier = Upper(LocSupplier)
    p_web.SetSessionValue('LocSupplier',LocSupplier)
  p_Web.SetValue('lookupfield','LocSupplier')
  do AfterLookup
  do Value::LocSupplier
  do SendAlert
  do Value::BrowseStockControl  !1

Value::LocSupplier  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('LocSupplier') & '_value',Choose(p_web.gsv('locFilterBySupplier')<>1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.gsv('locFilterBySupplier')<>1)
  ! --- STRING --- LocSupplier
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('LocSupplier')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''LocSupplier'',''formbrowsestock_locsupplier_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('LocSupplier')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','LocSupplier',p_web.GetSessionValueFormat('LocSupplier'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,30,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('SelectSuppliers?LookupField=LocSupplier&Tab=2&ForeignField=sup:Company_Name&_sort=&Refresh=sort&LookupFrom=FormBrowseStock&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('LocSupplier') & '_value')


Prompt::BrowseStockControl_sort  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('BrowseStockControl_sort') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Choose Sort Order')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::BrowseStockControl_sort  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseStockControl_sort',p_web.GetValue('NewValue'))
    BrowseStockControl_sort = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::BrowseStockControl_sort
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('BrowseStockControl_sort',p_web.GetValue('Value'))
    BrowseStockControl_sort = p_web.GetValue('Value')
  End
  do Value::BrowseStockControl_sort
  do SendAlert

Value::BrowseStockControl_sort  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('BrowseStockControl_sort') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- BrowseStockControl_sort
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('BrowseStockControl_sort')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('BrowseStockControl_sort') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&clip(p_web.GSV('js:RadioButton'))&';'
    loc:javascript = clip(loc:javascript) & ' ' &p_web._nocolon('sv(''BrowseStockControl_sort'',''formbrowsestock_browsestockcontrol_sort_value'',1,'''&clip(1)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','BrowseStockControl_sort',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'BrowseStockControl_sort_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Part Number') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('BrowseStockControl_sort') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&clip(p_web.GSV('js:RadioButton'))&';'
    loc:javascript = clip(loc:javascript) & ' ' &p_web._nocolon('sv(''BrowseStockControl_sort'',''formbrowsestock_browsestockcontrol_sort_value'',1,'''&clip(2)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','BrowseStockControl_sort',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'BrowseStockControl_sort_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Description') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('BrowseStockControl_sort') = 3
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&clip(p_web.GSV('js:RadioButton'))&';'
    loc:javascript = clip(loc:javascript) & ' ' &p_web._nocolon('sv(''BrowseStockControl_sort'',''formbrowsestock_browsestockcontrol_sort_value'',1,'''&clip(3)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','BrowseStockControl_sort',clip(3),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'BrowseStockControl_sort_3') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Manufacturer') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('BrowseStockControl_sort') = 8
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&clip(p_web.GSV('js:RadioButton'))&';'
    loc:javascript = clip(loc:javascript) & ' ' &p_web._nocolon('sv(''BrowseStockControl_sort'',''formbrowsestock_browsestockcontrol_sort_value'',1,'''&clip(8)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','BrowseStockControl_sort',clip(8),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'BrowseStockControl_sort_4') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Shelf Location') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('BrowseStockControl_sort') = 4
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&clip(p_web.GSV('js:RadioButton'))&';'
    loc:javascript = clip(loc:javascript) & ' ' &p_web._nocolon('sv(''BrowseStockControl_sort'',''formbrowsestock_browsestockcontrol_sort_value'',1,'''&clip(4)&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','BrowseStockControl_sort',clip(4),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'BrowseStockControl_sort_5') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Supplier') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('BrowseStockControl_sort') & '_value')


Validate::BrowseStockControl  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseStockControl',p_web.GetValue('NewValue'))
    do Value::BrowseStockControl
  Else
    p_web.StoreValue('sto:Ref_Number')
  End
  do SendAlert
  do Value::btnAddStock  !1
  do Value::btnOrderStock  !1
  do Value::btnDepleteStock  !1
  do Value::btnStockHistory  !1
  do Value::btnReturnStock  !1
  do Value::btnSuspendedStock  !1
  do Value::BrowseStockControl_sort  !1

Value::BrowseStockControl  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseStockControl --
  p_web.SetValue('BrowseStockControl:NoForm',1)
  p_web.SetValue('BrowseStockControl:FormName',loc:formname)
  p_web.SetValue('BrowseStockControl:parentIs','Form')
  p_web.SetValue('_parentProc','FormBrowseStock')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormBrowseStock_BrowseStockControl_embedded_div')&'"><!-- Net:BrowseStockControl --></div><13,10>'
    p_web._DivHeader('FormBrowseStock_' & lower('BrowseStockControl') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormBrowseStock_' & lower('BrowseStockControl') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseStockControl --><13,10>'
  end
  do SendPacket


Validate::btnAddStock  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnAddStock',p_web.GetValue('NewValue'))
    do Value::btnAddStock
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnAddStock  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('btnAddStock') & '_value',Choose(p_web.gsv('HideAddBtn')='1','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.gsv('HideAddBtn')='1')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnAddStock','Add Stock','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormAddStock?' &'ReturnURL=FormBrowseStock')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('btnAddStock') & '_value')


Validate::btnDepleteStock  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnDepleteStock',p_web.GetValue('NewValue'))
    do Value::btnDepleteStock
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnDepleteStock  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('btnDepleteStock') & '_value',Choose(p_web.gsv('HideDepleteBtn')='1','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.gsv('HideDepleteBtn')='1')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnDepleteStock','Deplete Stock','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormDepleteStock?' &'ReturnURL=FormBrowseStock')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('btnDepleteStock') & '_value')


Validate::btnStockHistory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnStockHistory',p_web.GetValue('NewValue'))
    do Value::btnStockHistory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnStockHistory  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('btnStockHistory') & '_value',Choose(p_web.gsv('HideHistoryBtn')='1','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.gsv('HideHistoryBtn')='1')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnStockHistory','Stock History','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('BrowseStockHistory?' &'ReturnURL=FormBrowseStock')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('btnStockHistory') & '_value')


Validate::btnOrderStock  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnOrderStock',p_web.GetValue('NewValue'))
    do Value::btnOrderStock
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnOrderStock  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('btnOrderStock') & '_value',Choose(p_web.gsv('HideOrderBtn')='1','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.gsv('HideOrderBtn')='1')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnOrderStock','Order Stock','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormStockOrder?' &'ReturnURL=FormBrowseStock')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('btnOrderStock') & '_value')


Validate::btnReturnStock  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnReturnStock',p_web.GetValue('NewValue'))
    do Value::btnReturnStock
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnReturnStock  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('btnReturnStock') & '_value',Choose(p_web.gsv('HideReturnBtn')='1','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.gsv('HideReturnBtn')='1')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnReturnStock','Return Stock','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormReturnStock?' &'ReturnURL=FormBrowseStock')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('btnReturnStock') & '_value')


Validate::btnBrowseOutstandingOrders  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnBrowseOutstandingOrders',p_web.GetValue('NewValue'))
    do Value::btnBrowseOutstandingOrders
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnBrowseOutstandingOrders  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('btnBrowseOutstandingOrders') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('stockOutstandingOrders()')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnBrowseOutstandingOrders','Outstanding Orders','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnSuspendedStock  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnSuspendedStock',p_web.GetValue('NewValue'))
    do Value::btnSuspendedStock
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnSuspendedStock  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('btnSuspendedStock') & '_value',Choose(p_web.gsv('HideSuspendBtn')='1','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.gsv('HideSuspendBtn')='1')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnSuspendedStock','Suspended Stock','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormBrowseSuspendedStock')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseStock_' & p_web._nocolon('btnSuspendedStock') & '_value')


Validate::btnReturnStockTracking  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnReturnStockTracking',p_web.GetValue('NewValue'))
    do Value::btnReturnStockTracking
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnReturnStockTracking  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('btnReturnStockTracking') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnReturnStockTracking','Return Stock Tracking','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormBrowseReturnOrderTracking')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnBrowseModelStock  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnBrowseModelStock',p_web.GetValue('NewValue'))
    do Value::btnBrowseModelStock
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnBrowseModelStock  Routine
  p_web._DivHeader('FormBrowseStock_' & p_web._nocolon('btnBrowseModelStock') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnBrowseModelStock','Browse Model Stock','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormBrowseModelStock?' &'firsttime=1')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormBrowseStock_locOldPartNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locOldPartNumber
      else
        do Value::locOldPartNumber
      end
  of lower('FormBrowseStock_locFilterByPartNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locFilterByPartNumber
      else
        do Value::locFilterByPartNumber
      end
  of lower('FormBrowseStock_locPartNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPartNumber
      else
        do Value::locPartNumber
      end
  of lower('FormBrowseStock_locFilterByDescription_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locFilterByDescription
      else
        do Value::locFilterByDescription
      end
  of lower('FormBrowseStock_LocDescription_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::LocDescription
      else
        do Value::LocDescription
      end
  of lower('FormBrowseStock_locFilterByManufacturer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locFilterByManufacturer
      else
        do Value::locFilterByManufacturer
      end
  of lower('FormBrowseStock_locManufacturer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locManufacturer
      else
        do Value::locManufacturer
      end
  of lower('FormBrowseStock_locFilterByShelfLocation_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locFilterByShelfLocation
      else
        do Value::locFilterByShelfLocation
      end
  of lower('FormBrowseStock_LocShelfLocation_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::LocShelfLocation
      else
        do Value::LocShelfLocation
      end
  of lower('FormBrowseStock_locFilterBySupplier_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locFilterBySupplier
      else
        do Value::locFilterBySupplier
      end
  of lower('FormBrowseStock_LocSupplier_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::LocSupplier
      else
        do Value::LocSupplier
      end
  of lower('FormBrowseStock_BrowseStockControl_sort_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::BrowseStockControl_sort
      else
        do Value::BrowseStockControl_sort
      end
  of lower('FormBrowseStock_BrowseStockControl_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::BrowseStockControl
      else
        do Value::BrowseStockControl
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormBrowseStock_form:ready_',1)
  p_web.SetSessionValue('FormBrowseStock_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormBrowseStock',0)

PreCopy  Routine
  p_web.SetValue('FormBrowseStock_form:ready_',1)
  p_web.SetSessionValue('FormBrowseStock_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormBrowseStock',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormBrowseStock_form:ready_',1)
  p_web.SetSessionValue('FormBrowseStock_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormBrowseStock:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormBrowseStock_form:ready_',1)
  p_web.SetSessionValue('FormBrowseStock_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormBrowseStock:Primed',0)
  p_web.setsessionvalue('showtab_FormBrowseStock',0)

LoadRelatedRecords  Routine
    p_web.GetDescription(MANUFACT,man:Manufacturer_Key,,man:Manufacturer,,p_web.GetSessionValue('locManufacturer'))
    p_web.FileToSessionQueue(MANUFACT)
    p_web.GetDescription(LOCSHELF,los:Shelf_Location_Key,,los:Site_Location,,p_web.GetSessionValue('LocShelfLocation'))
    p_web.FileToSessionQueue(LOCSHELF)
    p_web.GetDescription(SUPPLIER,sup:Company_Name_Key,,sup:Company_Name,,p_web.GetSessionValue('LocSupplier'))
    p_web.FileToSessionQueue(SUPPLIER)
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
          If p_web.IfExistsValue('locFilterByPartNumber') = 0
            p_web.SetValue('locFilterByPartNumber',0)
            locFilterByPartNumber = 0
          End
          If p_web.IfExistsValue('locFilterByDescription') = 0
            p_web.SetValue('locFilterByDescription',0)
            locFilterByDescription = 0
          End
          If p_web.IfExistsValue('locFilterByManufacturer') = 0
            p_web.SetValue('locFilterByManufacturer',0)
            locFilterByManufacturer = 0
          End
          If p_web.IfExistsValue('locFilterByShelfLocation') = 0
            p_web.SetValue('locFilterByShelfLocation',0)
            locFilterByShelfLocation = 0
          End
          If p_web.IfExistsValue('locFilterBySupplier') = 0
            p_web.SetValue('locFilterBySupplier',0)
            locFilterBySupplier = 0
          End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormBrowseStock_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormBrowseStock_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 3
    loc:InvalidTab += 1
          locOldPartNumber = Upper(locOldPartNumber)
          p_web.SetSessionValue('locOldPartNumber',locOldPartNumber)
        If loc:Invalid <> '' then exit.
      If not (p_web.GSV('locFilterByPartNumber') <> 1)
          locPartNumber = Upper(locPartNumber)
          p_web.SetSessionValue('locPartNumber',locPartNumber)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('locFilterByDescription') <> 1)
          LocDescription = Upper(LocDescription)
          p_web.SetSessionValue('LocDescription',LocDescription)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('locFilterByManufacturer') <> 1)
          locManufacturer = Upper(locManufacturer)
          p_web.SetSessionValue('locManufacturer',locManufacturer)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('locFilterByShelfLocation') <> 1)
          LocShelfLocation = Upper(LocShelfLocation)
          p_web.SetSessionValue('LocShelfLocation',LocShelfLocation)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.gsv('locFilterBySupplier')<>1)
          LocSupplier = Upper(LocSupplier)
          p_web.SetSessionValue('LocSupplier',LocSupplier)
        If loc:Invalid <> '' then exit.
      End
  ! tab = 2
    loc:InvalidTab += 1
  ! tab = 4
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormBrowseStock:Primed',0)
  p_web.StoreValue('locOldPartNumber')
  p_web.StoreValue('')
  p_web.StoreValue('locFilterByPartNumber')
  p_web.StoreValue('locPartNumber')
  p_web.StoreValue('locFilterByDescription')
  p_web.StoreValue('LocDescription')
  p_web.StoreValue('locFilterByManufacturer')
  p_web.StoreValue('locManufacturer')
  p_web.StoreValue('locFilterByShelfLocation')
  p_web.StoreValue('LocShelfLocation')
  p_web.StoreValue('locFilterBySupplier')
  p_web.StoreValue('LocSupplier')
  p_web.StoreValue('BrowseStockControl_sort')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
Menu  Routine
  packet = clip(packet) & |
    '<<!-- Net:MenuStockControl --><13,10>'&|
    ''
