

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER361.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER317.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER520.INC'),ONCE        !Req'd for module callout resolution
                     END


FormExchangePhoneCriteria PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locAllStockTypes     LONG                                  !
locAVL               STRING(3)                             !
locEXC               STRING(3)                             !
locINC               STRING(3)                             !
locFAU               STRING(3)                             !
locNOA               STRING(3)                             !
locREP               STRING(3)                             !
locSUS               STRING(3)                             !
locDES               STRING(3)                             !
locQA1               STRING(3)                             !
locQA2               STRING(3)                             !
locQAF               STRING(3)                             !
locRTS               STRING(3)                             !
locINR               STRING(3)                             !
locESC               STRING(3)                             !
locIT4               STRING(3)                             !
locITM               STRING(3)                             !
locITR               STRING(3)                             !
locITP               STRING(3)                             !
locStartDate         DATE                                  !
locEndDate           DATE                                  !
locAllStatus         LONG                                  !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormExchangePhoneCriteria')
  loc:formname = 'FormExchangePhoneCriteria_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormExchangePhoneCriteria',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormExchangePhoneCriteria','')
    p_web._DivHeader('FormExchangePhoneCriteria',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormExchangePhoneCriteria',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormExchangePhoneCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormExchangePhoneCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormExchangePhoneCriteria',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormExchangePhoneCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormExchangePhoneCriteria',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormExchangePhoneCriteria',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormExchangePhoneCriteria_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('locStartDate')
    p_web.SetPicture('locStartDate','@d06')
  End
  p_web.SetSessionPicture('locStartDate','@d06')
  If p_web.IfExistsValue('locEndDate')
    p_web.SetPicture('locEndDate','@d06')
  End
  p_web.SetSessionPicture('locEndDate','@d06')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locAllStockTypes',locAllStockTypes)
  p_web.SetSessionValue('locAllStatus',locAllStatus)
  p_web.SetSessionValue('locAVL',locAVL)
  p_web.SetSessionValue('locEXC',locEXC)
  p_web.SetSessionValue('locINC',locINC)
  p_web.SetSessionValue('locFAU',locFAU)
  p_web.SetSessionValue('locNOA',locNOA)
  p_web.SetSessionValue('locREP',locREP)
  p_web.SetSessionValue('locSUS',locSUS)
  p_web.SetSessionValue('locDES',locDES)
  p_web.SetSessionValue('locQA1',locQA1)
  p_web.SetSessionValue('locQA2',locQA2)
  p_web.SetSessionValue('locQAF',locQAF)
  p_web.SetSessionValue('locRTS',locRTS)
  p_web.SetSessionValue('locINR',locINR)
  p_web.SetSessionValue('locESC',locESC)
  p_web.SetSessionValue('locIT4',locIT4)
  p_web.SetSessionValue('locITM',locITM)
  p_web.SetSessionValue('locITR',locITR)
  p_web.SetSessionValue('locITP',locITP)
  p_web.SetSessionValue('locStartDate',locStartDate)
  p_web.SetSessionValue('locEndDate',locEndDate)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locAllStockTypes')
    locAllStockTypes = p_web.GetValue('locAllStockTypes')
    p_web.SetSessionValue('locAllStockTypes',locAllStockTypes)
  End
  if p_web.IfExistsValue('locAllStatus')
    locAllStatus = p_web.dformat(clip(p_web.GetValue('locAllStatus')),1)
    p_web.SetSessionValue('locAllStatus',locAllStatus)
  End
  if p_web.IfExistsValue('locAVL')
    locAVL = p_web.GetValue('locAVL')
    p_web.SetSessionValue('locAVL',locAVL)
  End
  if p_web.IfExistsValue('locEXC')
    locEXC = p_web.GetValue('locEXC')
    p_web.SetSessionValue('locEXC',locEXC)
  End
  if p_web.IfExistsValue('locINC')
    locINC = p_web.GetValue('locINC')
    p_web.SetSessionValue('locINC',locINC)
  End
  if p_web.IfExistsValue('locFAU')
    locFAU = p_web.GetValue('locFAU')
    p_web.SetSessionValue('locFAU',locFAU)
  End
  if p_web.IfExistsValue('locNOA')
    locNOA = p_web.GetValue('locNOA')
    p_web.SetSessionValue('locNOA',locNOA)
  End
  if p_web.IfExistsValue('locREP')
    locREP = p_web.GetValue('locREP')
    p_web.SetSessionValue('locREP',locREP)
  End
  if p_web.IfExistsValue('locSUS')
    locSUS = p_web.GetValue('locSUS')
    p_web.SetSessionValue('locSUS',locSUS)
  End
  if p_web.IfExistsValue('locDES')
    locDES = p_web.GetValue('locDES')
    p_web.SetSessionValue('locDES',locDES)
  End
  if p_web.IfExistsValue('locQA1')
    locQA1 = p_web.GetValue('locQA1')
    p_web.SetSessionValue('locQA1',locQA1)
  End
  if p_web.IfExistsValue('locQA2')
    locQA2 = p_web.GetValue('locQA2')
    p_web.SetSessionValue('locQA2',locQA2)
  End
  if p_web.IfExistsValue('locQAF')
    locQAF = p_web.GetValue('locQAF')
    p_web.SetSessionValue('locQAF',locQAF)
  End
  if p_web.IfExistsValue('locRTS')
    locRTS = p_web.GetValue('locRTS')
    p_web.SetSessionValue('locRTS',locRTS)
  End
  if p_web.IfExistsValue('locINR')
    locINR = p_web.GetValue('locINR')
    p_web.SetSessionValue('locINR',locINR)
  End
  if p_web.IfExistsValue('locESC')
    locESC = p_web.GetValue('locESC')
    p_web.SetSessionValue('locESC',locESC)
  End
  if p_web.IfExistsValue('locIT4')
    locIT4 = p_web.GetValue('locIT4')
    p_web.SetSessionValue('locIT4',locIT4)
  End
  if p_web.IfExistsValue('locITM')
    locITM = p_web.GetValue('locITM')
    p_web.SetSessionValue('locITM',locITM)
  End
  if p_web.IfExistsValue('locITR')
    locITR = p_web.GetValue('locITR')
    p_web.SetSessionValue('locITR',locITR)
  End
  if p_web.IfExistsValue('locITP')
    locITP = p_web.GetValue('locITP')
    p_web.SetSessionValue('locITP',locITP)
  End
  if p_web.IfExistsValue('locStartDate')
    locStartDate = p_web.dformat(clip(p_web.GetValue('locStartDate')),'@d06')
    p_web.SetSessionValue('locStartDate',locStartDate)
  End
  if p_web.IfExistsValue('locEndDate')
    locEndDate = p_web.dformat(clip(p_web.GetValue('locEndDate')),'@d06')
    p_web.SetSessionValue('locEndDate',locEndDate)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormExchangePhoneCriteria_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    ClearTagFile(p_web)  
    p_web.SSV('locStartDate',TODAY())
    p_web.SSV('locEndDate',TODAY())
      p_web.site.SaveButton.TextValue = 'Export'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locAllStockTypes = p_web.RestoreValue('locAllStockTypes')
 locAllStatus = p_web.RestoreValue('locAllStatus')
 locAVL = p_web.RestoreValue('locAVL')
 locEXC = p_web.RestoreValue('locEXC')
 locINC = p_web.RestoreValue('locINC')
 locFAU = p_web.RestoreValue('locFAU')
 locNOA = p_web.RestoreValue('locNOA')
 locREP = p_web.RestoreValue('locREP')
 locSUS = p_web.RestoreValue('locSUS')
 locDES = p_web.RestoreValue('locDES')
 locQA1 = p_web.RestoreValue('locQA1')
 locQA2 = p_web.RestoreValue('locQA2')
 locQAF = p_web.RestoreValue('locQAF')
 locRTS = p_web.RestoreValue('locRTS')
 locINR = p_web.RestoreValue('locINR')
 locESC = p_web.RestoreValue('locESC')
 locIT4 = p_web.RestoreValue('locIT4')
 locITM = p_web.RestoreValue('locITM')
 locITR = p_web.RestoreValue('locITR')
 locITP = p_web.RestoreValue('locITP')
 locStartDate = p_web.RestoreValue('locStartDate')
 locEndDate = p_web.RestoreValue('locEndDate')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'RunReport?RT=ExchangePhoneExport'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormExchangePhoneCriteria_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormExchangePhoneCriteria_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormExchangePhoneCriteria_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'FormReports'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormExchangePhoneCriteria" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormExchangePhoneCriteria" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormExchangePhoneCriteria" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Exchange Phone Export Criteria') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Exchange Phone Export Criteria',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormExchangePhoneCriteria">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormExchangePhoneCriteria" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormExchangePhoneCriteria')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Stock Types') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Exchange Status') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Date Range') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormExchangePhoneCriteria')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormExchangePhoneCriteria'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormExchangePhoneCriteria_BrowseTagStockType_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormExchangePhoneCriteria_BrowseTagStockType_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormExchangePhoneCriteria_BrowseTagStockType_embedded_div')&''');'
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locAllStockTypes')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormExchangePhoneCriteria')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Stock Types') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormExchangePhoneCriteria_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Stock Types')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Stock Types')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Stock Types')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Stock Types')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAllStockTypes
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&150&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAllStockTypes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAllStockTypes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwStockTypes
      do Comment::brwStockTypes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnUnTagAll
      do Comment::btnUnTagAll
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Exchange Status') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormExchangePhoneCriteria_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Exchange Status')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Exchange Status')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Exchange Status')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Exchange Status')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAllStatus
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&150&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAllStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAllStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAVL
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&150&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAVL
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAVL
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEXC
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&150&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEXC
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEXC
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locINC
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&150&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locINC
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locINC
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFAU
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&150&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFAU
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFAU
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locNOA
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&150&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locNOA
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locNOA
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locREP
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&150&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locREP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locREP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSUS
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&150&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSUS
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locSUS
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locDES
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&150&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locDES
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locDES
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locQA1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&150&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locQA1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locQA1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locQA2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&150&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locQA2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locQA2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locQAF
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&150&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locQAF
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locQAF
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locRTS
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&150&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locRTS
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locRTS
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locINR
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&150&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locINR
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locINR
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locESC
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&150&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locESC
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locESC
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIT4
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&150&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIT4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locIT4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locITM
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&150&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locITM
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locITM
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locITR
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&150&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locITR
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locITR
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locITP
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&150&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locITP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locITP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Date Range') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormExchangePhoneCriteria_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Date Range')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Date Range')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Date Range')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Date Range')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locStartDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&150&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locStartDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locStartDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEndDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&150&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEndDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEndDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locAllStockTypes  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locAllStockTypes') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('All Stock Types')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAllStockTypes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAllStockTypes',p_web.GetValue('NewValue'))
    locAllStockTypes = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAllStockTypes
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locAllStockTypes',p_web.GetValue('Value'))
    locAllStockTypes = p_web.GetValue('Value')
  End
  do Value::locAllStockTypes
  do SendAlert
  do Value::btnUnTagAll  !1
  do Value::brwStockTypes  !1

Value::locAllStockTypes  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locAllStockTypes') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locAllStockTypes
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locAllStockTypes'',''formexchangephonecriteria_locallstocktypes_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locAllStockTypes')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locAllStockTypes') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locAllStockTypes',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('locAllStockTypes') & '_value')

Comment::locAllStockTypes  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locAllStockTypes') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::brwStockTypes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwStockTypes',p_web.GetValue('NewValue'))
    do Value::brwStockTypes
  Else
    p_web.StoreValue('stp:Stock_Type')
  End

Value::brwStockTypes  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('locAllStockTypes') = 1,1,0))
  ! --- BROWSE ---  BrowseTagStockType --
  p_web.SetValue('BrowseTagStockType:NoForm',1)
  p_web.SetValue('BrowseTagStockType:FormName',loc:formname)
  p_web.SetValue('BrowseTagStockType:parentIs','Form')
  p_web.SetValue('_parentProc','FormExchangePhoneCriteria')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormExchangePhoneCriteria_BrowseTagStockType_embedded_div')&'"><!-- Net:BrowseTagStockType --></div><13,10>'
    p_web._DivHeader('FormExchangePhoneCriteria_' & lower('BrowseTagStockType') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormExchangePhoneCriteria_' & lower('BrowseTagStockType') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseTagStockType --><13,10>'
  end
  do SendPacket

Comment::brwStockTypes  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('brwStockTypes') & '_comment',Choose(p_web.GSV('locAllStockTypes') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAllStockTypes') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnUnTagAll  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnUnTagAll',p_web.GetValue('NewValue'))
    do Value::btnUnTagAll
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    ClearTagFile(p_web)  
  do SendAlert
  do Value::brwStockTypes  !1

Value::btnUnTagAll  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('btnUnTagAll') & '_value',Choose(p_web.GSV('locAllStockTypes') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllStockTypes') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnUnTagAll'',''formexchangephonecriteria_btnuntagall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnUnTagAll','UnTag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('btnUnTagAll') & '_value')

Comment::btnUnTagAll  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('btnUnTagAll') & '_comment',Choose(p_web.GSV('locAllStockTypes') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAllStockTypes') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAllStatus  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locAllStatus') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('All Statuses')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAllStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAllStatus',p_web.GetValue('NewValue'))
    locAllStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAllStatus
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locAllStatus',p_web.GetValue('Value'))
    locAllStatus = p_web.GetValue('Value')
  End
    DO Prompt::locAVL               
    DO Prompt::locEXC               
    DO Prompt::locINC
    DO Prompt::locFAU               
    DO Prompt::locNOA               
    DO Prompt::locREP               
    DO Prompt::locSUS               
    DO Prompt::locDES               
    DO Prompt::locQA1               
    DO Prompt::locQA2               
    DO Prompt::locQAF               
    DO Prompt::locRTS               
    DO Prompt::locINR               
    DO Prompt::locESC               
    DO Prompt::locIT4               
    DO Prompt::locITM               
    DO Prompt::locITR               
    DO Prompt::locITP
  
    DO Value::locAVL               
    DO Value::locEXC               
    DO Value::locINC               
    DO Value::locFAU               
    DO Value::locNOA               
    DO Value::locREP               
    DO Value::locSUS               
    DO Value::locDES               
    DO Value::locQA1               
    DO Value::locQA2               
    DO Value::locQAF               
    DO Value::locRTS               
    DO Value::locINR               
    DO Value::locESC               
    DO Value::locIT4               
    DO Value::locITM               
    DO Value::locITR               
    DO Value::locITP  
  do Value::locAllStatus
  do SendAlert

Value::locAllStatus  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locAllStatus') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locAllStatus
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locAllStatus'',''formexchangephonecriteria_locallstatus_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locAllStatus')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locAllStatus') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locAllStatus',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('locAllStatus') & '_value')

Comment::locAllStatus  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locAllStatus') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAVL  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locAVL') & '_prompt',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Available')
  If p_web.GSV('locAllStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAVL  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAVL',p_web.GetValue('NewValue'))
    locAVL = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAVL
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locAVL',p_web.GetValue('Value'))
    locAVL = p_web.GetValue('Value')
  End
  do Value::locAVL
  do SendAlert

Value::locAVL  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locAVL') & '_value',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllStatus') = 1)
  ! --- CHECKBOX --- locAVL
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locAVL'',''formexchangephonecriteria_locavl_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locAVL') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locAVL',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('locAVL') & '_value')

Comment::locAVL  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locAVL') & '_comment',Choose(p_web.GSV('locAllStatus') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAllStatus') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locEXC  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locEXC') & '_prompt',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Exchanged')
  If p_web.GSV('locAllStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locEXC  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEXC',p_web.GetValue('NewValue'))
    locEXC = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEXC
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locEXC',p_web.GetValue('Value'))
    locEXC = p_web.GetValue('Value')
  End
  do Value::locEXC
  do SendAlert

Value::locEXC  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locEXC') & '_value',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllStatus') = 1)
  ! --- CHECKBOX --- locEXC
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locEXC'',''formexchangephonecriteria_locexc_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locEXC') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locEXC',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('locEXC') & '_value')

Comment::locEXC  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locEXC') & '_comment',Choose(p_web.GSV('locAllStatus') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAllStatus') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locINC  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locINC') & '_prompt',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Incoming Transit')
  If p_web.GSV('locAllStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locINC  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locINC',p_web.GetValue('NewValue'))
    locINC = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locINC
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locINC',p_web.GetValue('Value'))
    locINC = p_web.GetValue('Value')
  End
  do Value::locINC
  do SendAlert

Value::locINC  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locINC') & '_value',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllStatus') = 1)
  ! --- CHECKBOX --- locINC
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locINC'',''formexchangephonecriteria_locinc_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locINC') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locINC',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('locINC') & '_value')

Comment::locINC  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locINC') & '_comment',Choose(p_web.GSV('locAllStatus') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAllStatus') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFAU  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locFAU') & '_prompt',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Faulty')
  If p_web.GSV('locAllStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFAU  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFAU',p_web.GetValue('NewValue'))
    locFAU = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFAU
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locFAU',p_web.GetValue('Value'))
    locFAU = p_web.GetValue('Value')
  End
  do Value::locFAU
  do SendAlert

Value::locFAU  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locFAU') & '_value',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllStatus') = 1)
  ! --- CHECKBOX --- locFAU
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locFAU'',''formexchangephonecriteria_locfau_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locFAU') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locFAU',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('locFAU') & '_value')

Comment::locFAU  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locFAU') & '_comment',Choose(p_web.GSV('locAllStatus') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAllStatus') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locNOA  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locNOA') & '_prompt',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Not Available')
  If p_web.GSV('locAllStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locNOA  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locNOA',p_web.GetValue('NewValue'))
    locNOA = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locNOA
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locNOA',p_web.GetValue('Value'))
    locNOA = p_web.GetValue('Value')
  End
  do Value::locNOA
  do SendAlert

Value::locNOA  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locNOA') & '_value',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllStatus') = 1)
  ! --- CHECKBOX --- locNOA
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locNOA'',''formexchangephonecriteria_locnoa_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locNOA') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locNOA',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('locNOA') & '_value')

Comment::locNOA  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locNOA') & '_comment',Choose(p_web.GSV('locAllStatus') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAllStatus') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locREP  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locREP') & '_prompt',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('In Repair')
  If p_web.GSV('locAllStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locREP  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locREP',p_web.GetValue('NewValue'))
    locREP = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locREP
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locREP',p_web.GetValue('Value'))
    locREP = p_web.GetValue('Value')
  End
  do Value::locREP
  do SendAlert

Value::locREP  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locREP') & '_value',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllStatus') = 1)
  ! --- CHECKBOX --- locREP
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locREP'',''formexchangephonecriteria_locrep_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locREP') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locREP',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('locREP') & '_value')

Comment::locREP  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locREP') & '_comment',Choose(p_web.GSV('locAllStatus') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAllStatus') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locSUS  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locSUS') & '_prompt',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Suspended')
  If p_web.GSV('locAllStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locSUS  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSUS',p_web.GetValue('NewValue'))
    locSUS = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSUS
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locSUS',p_web.GetValue('Value'))
    locSUS = p_web.GetValue('Value')
  End
  do Value::locSUS
  do SendAlert

Value::locSUS  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locSUS') & '_value',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllStatus') = 1)
  ! --- CHECKBOX --- locSUS
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSUS'',''formexchangephonecriteria_locsus_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locSUS') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locSUS',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('locSUS') & '_value')

Comment::locSUS  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locSUS') & '_comment',Choose(p_web.GSV('locAllStatus') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAllStatus') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locDES  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locDES') & '_prompt',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Despatched')
  If p_web.GSV('locAllStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locDES  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locDES',p_web.GetValue('NewValue'))
    locDES = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locDES
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locDES',p_web.GetValue('Value'))
    locDES = p_web.GetValue('Value')
  End
  do Value::locDES
  do SendAlert

Value::locDES  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locDES') & '_value',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllStatus') = 1)
  ! --- CHECKBOX --- locDES
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locDES'',''formexchangephonecriteria_locdes_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locDES') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locDES',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('locDES') & '_value')

Comment::locDES  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locDES') & '_comment',Choose(p_web.GSV('locAllStatus') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAllStatus') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locQA1  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locQA1') & '_prompt',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Electronic QA Required')
  If p_web.GSV('locAllStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locQA1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locQA1',p_web.GetValue('NewValue'))
    locQA1 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locQA1
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locQA1',p_web.GetValue('Value'))
    locQA1 = p_web.GetValue('Value')
  End
  do Value::locQA1
  do SendAlert

Value::locQA1  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locQA1') & '_value',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllStatus') = 1)
  ! --- CHECKBOX --- locQA1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locQA1'',''formexchangephonecriteria_locqa1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locQA1') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locQA1',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('locQA1') & '_value')

Comment::locQA1  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locQA1') & '_comment',Choose(p_web.GSV('locAllStatus') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAllStatus') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locQA2  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locQA2') & '_prompt',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Manual QA Required')
  If p_web.GSV('locAllStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locQA2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locQA2',p_web.GetValue('NewValue'))
    locQA2 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locQA2
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locQA2',p_web.GetValue('Value'))
    locQA2 = p_web.GetValue('Value')
  End
  do Value::locQA2
  do SendAlert

Value::locQA2  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locQA2') & '_value',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllStatus') = 1)
  ! --- CHECKBOX --- locQA2
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locQA2'',''formexchangephonecriteria_locqa2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locQA2') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locQA2',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('locQA2') & '_value')

Comment::locQA2  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locQA2') & '_comment',Choose(p_web.GSV('locAllStatus') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAllStatus') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locQAF  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locQAF') & '_prompt',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('QA Failed')
  If p_web.GSV('locAllStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locQAF  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locQAF',p_web.GetValue('NewValue'))
    locQAF = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locQAF
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locQAF',p_web.GetValue('Value'))
    locQAF = p_web.GetValue('Value')
  End
  do Value::locQAF
  do SendAlert

Value::locQAF  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locQAF') & '_value',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllStatus') = 1)
  ! --- CHECKBOX --- locQAF
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locQAF'',''formexchangephonecriteria_locqaf_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locQAF') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locQAF',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('locQAF') & '_value')

Comment::locQAF  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locQAF') & '_comment',Choose(p_web.GSV('locAllStatus') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAllStatus') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locRTS  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locRTS') & '_prompt',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Return To Stock')
  If p_web.GSV('locAllStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locRTS  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locRTS',p_web.GetValue('NewValue'))
    locRTS = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locRTS
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locRTS',p_web.GetValue('Value'))
    locRTS = p_web.GetValue('Value')
  End
  do Value::locRTS
  do SendAlert

Value::locRTS  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locRTS') & '_value',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllStatus') = 1)
  ! --- CHECKBOX --- locRTS
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locRTS'',''formexchangephonecriteria_locrts_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locRTS') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locRTS',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('locRTS') & '_value')

Comment::locRTS  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locRTS') & '_comment',Choose(p_web.GSV('locAllStatus') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAllStatus') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locINR  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locINR') & '_prompt',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Exchange Repair')
  If p_web.GSV('locAllStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locINR  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locINR',p_web.GetValue('NewValue'))
    locINR = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locINR
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locINR',p_web.GetValue('Value'))
    locINR = p_web.GetValue('Value')
  End
  do Value::locINR
  do SendAlert

Value::locINR  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locINR') & '_value',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllStatus') = 1)
  ! --- CHECKBOX --- locINR
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locINR'',''formexchangephonecriteria_locinr_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locINR') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locINR',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('locINR') & '_value')

Comment::locINR  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locINR') & '_comment',Choose(p_web.GSV('locAllStatus') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAllStatus') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locESC  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locESC') & '_prompt',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Exchange Scrap Confirmed')
  If p_web.GSV('locAllStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locESC  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locESC',p_web.GetValue('NewValue'))
    locESC = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locESC
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locESC',p_web.GetValue('Value'))
    locESC = p_web.GetValue('Value')
  End
  do Value::locESC
  do SendAlert

Value::locESC  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locESC') & '_value',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllStatus') = 1)
  ! --- CHECKBOX --- locESC
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locESC'',''formexchangephonecriteria_locesc_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locESC') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locESC',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('locESC') & '_value')

Comment::locESC  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locESC') & '_comment',Choose(p_web.GSV('locAllStatus') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAllStatus') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locIT4  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locIT4') & '_prompt',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('In Transit To 48HR Stores')
  If p_web.GSV('locAllStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIT4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIT4',p_web.GetValue('NewValue'))
    locIT4 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIT4
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locIT4',p_web.GetValue('Value'))
    locIT4 = p_web.GetValue('Value')
  End
  do Value::locIT4
  do SendAlert

Value::locIT4  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locIT4') & '_value',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllStatus') = 1)
  ! --- CHECKBOX --- locIT4
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locIT4'',''formexchangephonecriteria_locit4_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locIT4') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locIT4',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('locIT4') & '_value')

Comment::locIT4  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locIT4') & '_comment',Choose(p_web.GSV('locAllStatus') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAllStatus') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locITM  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locITM') & '_prompt',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('In Transit From Main Store')
  If p_web.GSV('locAllStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locITM  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locITM',p_web.GetValue('NewValue'))
    locITM = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locITM
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locITM',p_web.GetValue('Value'))
    locITM = p_web.GetValue('Value')
  End
  do Value::locITM
  do SendAlert

Value::locITM  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locITM') & '_value',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllStatus') = 1)
  ! --- CHECKBOX --- locITM
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locITM'',''formexchangephonecriteria_locitm_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locITM') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locITM',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('locITM') & '_value')

Comment::locITM  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locITM') & '_comment',Choose(p_web.GSV('locAllStatus') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAllStatus') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locITR  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locITR') & '_prompt',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('In Transit To Scrapping')
  If p_web.GSV('locAllStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locITR  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locITR',p_web.GetValue('NewValue'))
    locITR = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locITR
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locITR',p_web.GetValue('Value'))
    locITR = p_web.GetValue('Value')
  End
  do Value::locITR
  do SendAlert

Value::locITR  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locITR') & '_value',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllStatus') = 1)
  ! --- CHECKBOX --- locITR
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locITR'',''formexchangephonecriteria_locitr_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locITR') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locITR',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('locITR') & '_value')

Comment::locITR  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locITR') & '_comment',Choose(p_web.GSV('locAllStatus') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAllStatus') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locITP  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locITP') & '_prompt',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Phantom In Transit')
  If p_web.GSV('locAllStatus') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locITP  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locITP',p_web.GetValue('NewValue'))
    locITP = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locITP
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locITP',p_web.GetValue('Value'))
    locITP = p_web.GetValue('Value')
  End
  do Value::locITP
  do SendAlert

Value::locITP  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locITP') & '_value',Choose(p_web.GSV('locAllStatus') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locAllStatus') = 1)
  ! --- CHECKBOX --- locITP
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locITP'',''formexchangephonecriteria_locitp_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locITP') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locITP',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('locITP') & '_value')

Comment::locITP  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locITP') & '_comment',Choose(p_web.GSV('locAllStatus') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locAllStatus') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locStartDate  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locStartDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Status Start Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locStartDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locStartDate',p_web.GetValue('NewValue'))
    locStartDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locStartDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locStartDate',p_web.dFormat(p_web.GetValue('Value'),'@d06'))
    locStartDate = p_web.Dformat(p_web.GetValue('Value'),'@d06') !
  End
  If locStartDate = ''
    loc:Invalid = 'locStartDate'
    loc:alert = p_web.translate('Status Start Date') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locStartDate = Upper(locStartDate)
    p_web.SetSessionValue('locStartDate',locStartDate)
  do Value::locStartDate
  do SendAlert

Value::locStartDate  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locStartDate') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locStartDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locStartDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locStartDate = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locStartDate'',''formexchangephonecriteria_locstartdate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locStartDate',p_web.GetSessionValue('locStartDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06',loc:javascript,,) & '<13,10>'
  do SendPacket
   packet = CLIP(packet) & |
     '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar' & |
     '(locStartDate,''dd/mm/yyyy'',this); Date.disabled=true;' & |
     'sv(''...'',''FormExchangePhoneCriteria_pickdate_value'',1,FieldValue(this,1));nextFocus(FormExchangePhoneCriteria_frm,'''',0);" ' & |
     'value="Select Date" name="Date" type="button">...</button>'
   DO SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('locStartDate') & '_value')

Comment::locStartDate  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locStartDate') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locEndDate  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locEndDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Status End Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locEndDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEndDate',p_web.GetValue('NewValue'))
    locEndDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEndDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEndDate',p_web.dFormat(p_web.GetValue('Value'),'@d06'))
    locEndDate = p_web.Dformat(p_web.GetValue('Value'),'@d06') !
  End
  If locEndDate = ''
    loc:Invalid = 'locEndDate'
    loc:alert = p_web.translate('Status End Date') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locEndDate = Upper(locEndDate)
    p_web.SetSessionValue('locEndDate',locEndDate)
  do Value::locEndDate
  do SendAlert

Value::locEndDate  Routine
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locEndDate') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locEndDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locEndDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locEndDate = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEndDate'',''formexchangephonecriteria_locenddate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locEndDate',p_web.GetSessionValue('locEndDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06',loc:javascript,,) & '<13,10>'
  do SendPacket
   packet = CLIP(packet) & |
     '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar' & |
     '(locEndDate,''dd/mm/yyyy'',this); Date.disabled=true;' & |
     'sv(''...'',''FormExchangePhoneCriteria_pickdate_value'',1,FieldValue(this,1));nextFocus(FormExchangePhoneCriteria_frm,'''',0);" ' & |
     'value="Select Date" name="Date" type="button">...</button>'
   DO SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangePhoneCriteria_' & p_web._nocolon('locEndDate') & '_value')

Comment::locEndDate  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormExchangePhoneCriteria_' & p_web._nocolon('locEndDate') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormExchangePhoneCriteria_locAllStockTypes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAllStockTypes
      else
        do Value::locAllStockTypes
      end
  of lower('FormExchangePhoneCriteria_btnUnTagAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnUnTagAll
      else
        do Value::btnUnTagAll
      end
  of lower('FormExchangePhoneCriteria_locAllStatus_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAllStatus
      else
        do Value::locAllStatus
      end
  of lower('FormExchangePhoneCriteria_locAVL_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAVL
      else
        do Value::locAVL
      end
  of lower('FormExchangePhoneCriteria_locEXC_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEXC
      else
        do Value::locEXC
      end
  of lower('FormExchangePhoneCriteria_locINC_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locINC
      else
        do Value::locINC
      end
  of lower('FormExchangePhoneCriteria_locFAU_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locFAU
      else
        do Value::locFAU
      end
  of lower('FormExchangePhoneCriteria_locNOA_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locNOA
      else
        do Value::locNOA
      end
  of lower('FormExchangePhoneCriteria_locREP_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locREP
      else
        do Value::locREP
      end
  of lower('FormExchangePhoneCriteria_locSUS_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSUS
      else
        do Value::locSUS
      end
  of lower('FormExchangePhoneCriteria_locDES_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locDES
      else
        do Value::locDES
      end
  of lower('FormExchangePhoneCriteria_locQA1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locQA1
      else
        do Value::locQA1
      end
  of lower('FormExchangePhoneCriteria_locQA2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locQA2
      else
        do Value::locQA2
      end
  of lower('FormExchangePhoneCriteria_locQAF_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locQAF
      else
        do Value::locQAF
      end
  of lower('FormExchangePhoneCriteria_locRTS_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locRTS
      else
        do Value::locRTS
      end
  of lower('FormExchangePhoneCriteria_locINR_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locINR
      else
        do Value::locINR
      end
  of lower('FormExchangePhoneCriteria_locESC_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locESC
      else
        do Value::locESC
      end
  of lower('FormExchangePhoneCriteria_locIT4_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIT4
      else
        do Value::locIT4
      end
  of lower('FormExchangePhoneCriteria_locITM_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locITM
      else
        do Value::locITM
      end
  of lower('FormExchangePhoneCriteria_locITR_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locITR
      else
        do Value::locITR
      end
  of lower('FormExchangePhoneCriteria_locITP_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locITP
      else
        do Value::locITP
      end
  of lower('FormExchangePhoneCriteria_locStartDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locStartDate
      else
        do Value::locStartDate
      end
  of lower('FormExchangePhoneCriteria_locEndDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEndDate
      else
        do Value::locEndDate
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormExchangePhoneCriteria_form:ready_',1)
  p_web.SetSessionValue('FormExchangePhoneCriteria_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormExchangePhoneCriteria',0)

PreCopy  Routine
  p_web.SetValue('FormExchangePhoneCriteria_form:ready_',1)
  p_web.SetSessionValue('FormExchangePhoneCriteria_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormExchangePhoneCriteria',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormExchangePhoneCriteria_form:ready_',1)
  p_web.SetSessionValue('FormExchangePhoneCriteria_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormExchangePhoneCriteria:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormExchangePhoneCriteria_form:ready_',1)
  p_web.SetSessionValue('FormExchangePhoneCriteria_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormExchangePhoneCriteria:Primed',0)
  p_web.setsessionvalue('showtab_FormExchangePhoneCriteria',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
          If p_web.IfExistsValue('locAllStockTypes') = 0
            p_web.SetValue('locAllStockTypes',0)
            locAllStockTypes = 0
          End
          If p_web.IfExistsValue('locAllStatus') = 0
            p_web.SetValue('locAllStatus',0)
            locAllStatus = 0
          End
      If(p_web.GSV('locAllStatus') = 1)
          If p_web.IfExistsValue('locAVL') = 0
            p_web.SetValue('locAVL',0)
            locAVL = 0
          End
      End
      If(p_web.GSV('locAllStatus') = 1)
          If p_web.IfExistsValue('locEXC') = 0
            p_web.SetValue('locEXC',0)
            locEXC = 0
          End
      End
      If(p_web.GSV('locAllStatus') = 1)
          If p_web.IfExistsValue('locINC') = 0
            p_web.SetValue('locINC',0)
            locINC = 0
          End
      End
      If(p_web.GSV('locAllStatus') = 1)
          If p_web.IfExistsValue('locFAU') = 0
            p_web.SetValue('locFAU',0)
            locFAU = 0
          End
      End
      If(p_web.GSV('locAllStatus') = 1)
          If p_web.IfExistsValue('locNOA') = 0
            p_web.SetValue('locNOA',0)
            locNOA = 0
          End
      End
      If(p_web.GSV('locAllStatus') = 1)
          If p_web.IfExistsValue('locREP') = 0
            p_web.SetValue('locREP',0)
            locREP = 0
          End
      End
      If(p_web.GSV('locAllStatus') = 1)
          If p_web.IfExistsValue('locSUS') = 0
            p_web.SetValue('locSUS',0)
            locSUS = 0
          End
      End
      If(p_web.GSV('locAllStatus') = 1)
          If p_web.IfExistsValue('locDES') = 0
            p_web.SetValue('locDES',0)
            locDES = 0
          End
      End
      If(p_web.GSV('locAllStatus') = 1)
          If p_web.IfExistsValue('locQA1') = 0
            p_web.SetValue('locQA1',0)
            locQA1 = 0
          End
      End
      If(p_web.GSV('locAllStatus') = 1)
          If p_web.IfExistsValue('locQA2') = 0
            p_web.SetValue('locQA2',0)
            locQA2 = 0
          End
      End
      If(p_web.GSV('locAllStatus') = 1)
          If p_web.IfExistsValue('locQAF') = 0
            p_web.SetValue('locQAF',0)
            locQAF = 0
          End
      End
      If(p_web.GSV('locAllStatus') = 1)
          If p_web.IfExistsValue('locRTS') = 0
            p_web.SetValue('locRTS',0)
            locRTS = 0
          End
      End
      If(p_web.GSV('locAllStatus') = 1)
          If p_web.IfExistsValue('locINR') = 0
            p_web.SetValue('locINR',0)
            locINR = 0
          End
      End
      If(p_web.GSV('locAllStatus') = 1)
          If p_web.IfExistsValue('locESC') = 0
            p_web.SetValue('locESC',0)
            locESC = 0
          End
      End
      If(p_web.GSV('locAllStatus') = 1)
          If p_web.IfExistsValue('locIT4') = 0
            p_web.SetValue('locIT4',0)
            locIT4 = 0
          End
      End
      If(p_web.GSV('locAllStatus') = 1)
          If p_web.IfExistsValue('locITM') = 0
            p_web.SetValue('locITM',0)
            locITM = 0
          End
      End
      If(p_web.GSV('locAllStatus') = 1)
          If p_web.IfExistsValue('locITR') = 0
            p_web.SetValue('locITR',0)
            locITR = 0
          End
      End
      If(p_web.GSV('locAllStatus') = 1)
          If p_web.IfExistsValue('locITP') = 0
            p_web.SetValue('locITP',0)
            locITP = 0
          End
      End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
    !Validate
    tagged# = 0
    IF (p_web.GSV('locAllStockTypes') <> 1)
        Access:TagFile.ClearKey(tag:KeyTagged)
        tag:SessionID = p_web.SessionID
        SET(tag:KeyTagged,tag:KeyTagged)
        LOOP UNTIL Access:TagFile.Next() <> Level:Benign
            IF (tag:SessionID <> p_web.SessionID)
                BREAK
            END ! IF
            IF (tag:tagged = 0)
                CYCLE
            END ! IF
            tagged# = 1
            BREAK
        END ! LOOP
        IF (tagged# = 0)
            loc:alert = 'You have not tagged any stock types!'
            loc:invalid = 'locAllStockTypes'
        END ! IF
    END ! IF 
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormExchangePhoneCriteria_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormExchangePhoneCriteria_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! tab = 3
    loc:InvalidTab += 1
        If locStartDate = ''
          loc:Invalid = 'locStartDate'
          loc:alert = p_web.translate('Status Start Date') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locStartDate = Upper(locStartDate)
          p_web.SetSessionValue('locStartDate',locStartDate)
        If loc:Invalid <> '' then exit.
        If locEndDate = ''
          loc:Invalid = 'locEndDate'
          loc:alert = p_web.translate('Status End Date') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locEndDate = Upper(locEndDate)
          p_web.SetSessionValue('locEndDate',locEndDate)
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormExchangePhoneCriteria:Primed',0)
  p_web.StoreValue('locAllStockTypes')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locAllStatus')
  p_web.StoreValue('locAVL')
  p_web.StoreValue('locEXC')
  p_web.StoreValue('locINC')
  p_web.StoreValue('locFAU')
  p_web.StoreValue('locNOA')
  p_web.StoreValue('locREP')
  p_web.StoreValue('locSUS')
  p_web.StoreValue('locDES')
  p_web.StoreValue('locQA1')
  p_web.StoreValue('locQA2')
  p_web.StoreValue('locQAF')
  p_web.StoreValue('locRTS')
  p_web.StoreValue('locINR')
  p_web.StoreValue('locESC')
  p_web.StoreValue('locIT4')
  p_web.StoreValue('locITM')
  p_web.StoreValue('locITR')
  p_web.StoreValue('locITP')
  p_web.StoreValue('locStartDate')
  p_web.StoreValue('locEndDate')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locAllStockTypes',locAllStockTypes) ! LONG
     p_web.SSV('locAVL',locAVL) ! STRING(3)
     p_web.SSV('locEXC',locEXC) ! STRING(3)
     p_web.SSV('locINC',locINC) ! STRING(3)
     p_web.SSV('locFAU',locFAU) ! STRING(3)
     p_web.SSV('locNOA',locNOA) ! STRING(3)
     p_web.SSV('locREP',locREP) ! STRING(3)
     p_web.SSV('locSUS',locSUS) ! STRING(3)
     p_web.SSV('locDES',locDES) ! STRING(3)
     p_web.SSV('locQA1',locQA1) ! STRING(3)
     p_web.SSV('locQA2',locQA2) ! STRING(3)
     p_web.SSV('locQAF',locQAF) ! STRING(3)
     p_web.SSV('locRTS',locRTS) ! STRING(3)
     p_web.SSV('locINR',locINR) ! STRING(3)
     p_web.SSV('locESC',locESC) ! STRING(3)
     p_web.SSV('locIT4',locIT4) ! STRING(3)
     p_web.SSV('locITM',locITM) ! STRING(3)
     p_web.SSV('locITR',locITR) ! STRING(3)
     p_web.SSV('locITP',locITP) ! STRING(3)
     p_web.SSV('locStartDate',locStartDate) ! DATE
     p_web.SSV('locEndDate',locEndDate) ! DATE
     p_web.SSV('locAllStatus',locAllStatus) ! LONG
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locAllStockTypes = p_web.GSV('locAllStockTypes') ! LONG
     locAVL = p_web.GSV('locAVL') ! STRING(3)
     locEXC = p_web.GSV('locEXC') ! STRING(3)
     locINC = p_web.GSV('locINC') ! STRING(3)
     locFAU = p_web.GSV('locFAU') ! STRING(3)
     locNOA = p_web.GSV('locNOA') ! STRING(3)
     locREP = p_web.GSV('locREP') ! STRING(3)
     locSUS = p_web.GSV('locSUS') ! STRING(3)
     locDES = p_web.GSV('locDES') ! STRING(3)
     locQA1 = p_web.GSV('locQA1') ! STRING(3)
     locQA2 = p_web.GSV('locQA2') ! STRING(3)
     locQAF = p_web.GSV('locQAF') ! STRING(3)
     locRTS = p_web.GSV('locRTS') ! STRING(3)
     locINR = p_web.GSV('locINR') ! STRING(3)
     locESC = p_web.GSV('locESC') ! STRING(3)
     locIT4 = p_web.GSV('locIT4') ! STRING(3)
     locITM = p_web.GSV('locITM') ! STRING(3)
     locITR = p_web.GSV('locITR') ! STRING(3)
     locITP = p_web.GSV('locITP') ! STRING(3)
     locStartDate = p_web.GSV('locStartDate') ! DATE
     locEndDate = p_web.GSV('locEndDate') ! DATE
     locAllStatus = p_web.GSV('locAllStatus') ! LONG
