

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER125.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER120.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER126.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER127.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER128.INC'),ONCE        !Req'd for module callout resolution
                     END


AmendAddress         PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
VariablesGroup       GROUP,PRE(tmp)                        !
CompanyName          STRING(30)                            !TempCompanyName
AddressLine1         STRING(30)                            !TempAddressLine1
AddressLine2         STRING(30)                            !TempAddressLine2
Suburb               STRING(30)                            !TempSuburb
Postcode             STRING(30)                            !TempPostcode
TelephoneNumber      STRING(30)                            !TempTelephoneNumber
FaxNumber            STRING(30)                            !TempFaxNumber
EmailAddress         STRING(255)                           !Email Address
EndUserTelephoneNumber STRING(30)                          !End User Tel No
IDNumber             STRING(30)                            !ID Number
SMSNotification      BYTE(0)                               !SMS Notification
NotificationMobileNumber STRING(30)                        !NotificationMobileNumber
EmailNotification    BYTE(0)                               !EmailNotification
NotificationEmailAddress STRING(255)                       !Notification Email Address
CompanyNameDelivery  STRING(30)                            !tmp:CompanyNameDelivery
AddressLine1Delivery STRING(30)                            !tmp:AddressLine1Delivery
AddressLine2Delivery STRING(30)                            !AddressLine2Delivery
SuburbDelivery       STRING(30)                            !SubrubDelivery
PostcodeDelivery     STRING(30)                            !PostcodeDelivery
TelephoneNumberDelivery STRING(30)                         !TelephoneNumberDelivery
CompanyNameCollection STRING(30)                           !CompanyNameCollection
AddressLine1Collection STRING(30)                          !AddressLine1Collection
AddressLine2Collection STRING(30)                          !AddressLine2Collection
SuburbCollection     STRING(30)                            !SuburbCollection
PostcodeCollection   STRING(30)                            !PostcodeCollection
TelephoneNumberCollection STRING(30)                       !TelephoneNumberCollection
HubCustomer          STRING(30)                            !Hub
HubCollection        STRING(30)                            !Hub
HubDelivery          STRING(30)                            !Hub
                     END                                   !
ReplicateCustomerToDelivery BYTE(0)                        !ReplicateCustomerToDelivery
ReplicateCustomerToCollection BYTE(0)                      !ReplicateCustomerToCollection
ReplicateCollectionToDelivery BYTE(0)                      !ReplicateCollectionToDelivery
ReplicateDeliveryToCollection BYTE(0)                      !ReplicateDeliveryToCollection
FilesOpened     Long
C3DMONIT::State  USHORT
SUBTRACC::State  USHORT
JOBSE3::State  USHORT
COURIER::State  USHORT
JOBNOTES::State  USHORT
SUBACCAD::State  USHORT
SUBURB::State  USHORT
JOBS::State  USHORT
JOBSE::State  USHORT
WEBJOB::State  USHORT
JOBSE2::State  USHORT
TRANTYPE::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('AmendAddress')
  loc:formname = 'AmendAddress_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'AmendAddress',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('AmendAddress','')
    p_web._DivHeader('AmendAddress',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferAmendAddress',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAmendAddress',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAmendAddress',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_AmendAddress',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferAmendAddress',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_AmendAddress',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'AmendAddress',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(C3DMONIT)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(JOBSE3)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(JOBNOTES)
  p_web._OpenFile(SUBACCAD)
  p_web._OpenFile(SUBURB)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(TRANTYPE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(C3DMONIT)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(JOBSE3)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(JOBNOTES)
  p_Web._CloseFile(SUBACCAD)
  p_Web._CloseFile(SUBURB)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(TRANTYPE)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('AmendAddress_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('FromURL') <> 'CustomerServicesForm'
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:Address_Line3'
    p_web.setsessionvalue('showtab_AmendAddress',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBURB)
        p_web.setsessionvalue('job:Postcode',sur:Postcode)
        p_web.setsessionvalue('jobe2:HubCustomer',sur:Hub)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.jobe2:HubCustomer')
  Of 'job:Courier'
    p_web.setsessionvalue('showtab_AmendAddress',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(COURIER)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.jobe2:CourierWaybillNumber')
  End
  If p_web.GSV('Hide:AmendC3DButton') = 0
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'job:Address_Line3_Delivery'
    p_web.setsessionvalue('showtab_AmendAddress',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBURB)
        p_web.setsessionvalue('job:Postcode_Delivery',sur:Postcode)
        p_web.setsessionvalue('jobe2:HubDelivery',sur:Hub)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.job:Address_Line3_Collection')
  Of 'job:Address_Line3_Collection'
    p_web.setsessionvalue('showtab_AmendAddress',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBURB)
        p_web.setsessionvalue('job:Postcode_Collection',sur:Postcode)
        p_web.setsessionvalue('jobe2:HubCollection',sur:Hub)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.jobe2:HubDelivery')
  End
  If p_web.GSV('Hide:SubSubAccount') <> 1
    loc:TabNumber += 1
  End
  Case p_Web.GetValue('lookupfield')
  Of 'jobe:Sub_Sub_Account'
    p_web.setsessionvalue('showtab_AmendAddress',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBACCAD)
        p_web.setsessionvalue('job:Postcode_Delivery',sua:Postcode)
        p_web.setsessionvalue('job:Address_Line1_Delivery',sua:addressline1)
        p_web.setsessionvalue('job:Address_Line2_Delivery',sua:addressline2)
        p_web.setsessionvalue('job:Address_Line3_Delivery',sua:addressline3)
        p_web.setsessionvalue('job:Telephone_Delivery',sua:TelephoneNumber)
        p_web.setsessionvalue('job:Company_Name_Delivery',sua:CompanyName)
    End
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('job:Company_Name',job:Company_Name)
  p_web.SetSessionValue('job:Address_Line1',job:Address_Line1)
  p_web.SetSessionValue('job:Address_Line2',job:Address_Line2)
  p_web.SetSessionValue('job:Address_Line3',job:Address_Line3)
  p_web.SetSessionValue('jobe2:HubCustomer',jobe2:HubCustomer)
  p_web.SetSessionValue('job:Postcode',job:Postcode)
  p_web.SetSessionValue('job:Telephone_Number',job:Telephone_Number)
  p_web.SetSessionValue('job:Fax_Number',job:Fax_Number)
  p_web.SetSessionValue('jobe:EndUserEmailAddress',jobe:EndUserEmailAddress)
  p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
  p_web.SetSessionValue('jobe:VatNumber',jobe:VatNumber)
  p_web.SetSessionValue('jobe2:IDNumber',jobe2:IDNumber)
  p_web.SetSessionValue('jobe2:SMSNotification',jobe2:SMSNotification)
  p_web.SetSessionValue('jobe2:SMSAlertNumber',jobe2:SMSAlertNumber)
  p_web.SetSessionValue('jobe2:EmailNotification',jobe2:EmailNotification)
  p_web.SetSessionValue('jobe2:EmailAlertAddress',jobe2:EmailAlertAddress)
  p_web.SetSessionValue('job:Courier',job:Courier)
  p_web.SetSessionValue('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber)
  p_web.SetSessionValue('jobe3:LoyaltyStatus',jobe3:LoyaltyStatus)
  p_web.SetSessionValue('job:Company_Name_Delivery',job:Company_Name_Delivery)
  p_web.SetSessionValue('job:Company_Name_Collection',job:Company_Name_Collection)
  p_web.SetSessionValue('job:Address_Line1_Delivery',job:Address_Line1_Delivery)
  p_web.SetSessionValue('job:Address_Line1_Collection',job:Address_Line1_Collection)
  p_web.SetSessionValue('job:Address_Line2_Delivery',job:Address_Line2_Delivery)
  p_web.SetSessionValue('job:Address_Line2_Collection',job:Address_Line2_Collection)
  p_web.SetSessionValue('job:Address_Line3_Delivery',job:Address_Line3_Delivery)
  p_web.SetSessionValue('job:Address_Line3_Collection',job:Address_Line3_Collection)
  p_web.SetSessionValue('jobe2:HubDelivery',jobe2:HubDelivery)
  p_web.SetSessionValue('jobe2:HubCollection',jobe2:HubCollection)
  p_web.SetSessionValue('job:Postcode_Delivery',job:Postcode_Delivery)
  p_web.SetSessionValue('job:Postcode_Collection',job:Postcode_Collection)
  p_web.SetSessionValue('job:Telephone_Delivery',job:Telephone_Delivery)
  p_web.SetSessionValue('job:Telephone_Collection',job:Telephone_Collection)
  p_web.SetSessionValue('jbn:Delivery_Text',jbn:Delivery_Text)
  p_web.SetSessionValue('jbn:Collection_Text',jbn:Collection_Text)
  p_web.SetSessionValue('jbn:DelContactName',jbn:DelContactName)
  p_web.SetSessionValue('jbn:ColContatName',jbn:ColContatName)
  p_web.SetSessionValue('jbn:DelDepartment',jbn:DelDepartment)
  p_web.SetSessionValue('jbn:ColDepartment',jbn:ColDepartment)
  p_web.SetSessionValue('jobe:Sub_Sub_Account',jobe:Sub_Sub_Account)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('job:Company_Name')
    job:Company_Name = p_web.GetValue('job:Company_Name')
    p_web.SetSessionValue('job:Company_Name',job:Company_Name)
  End
  if p_web.IfExistsValue('job:Address_Line1')
    job:Address_Line1 = p_web.GetValue('job:Address_Line1')
    p_web.SetSessionValue('job:Address_Line1',job:Address_Line1)
  End
  if p_web.IfExistsValue('job:Address_Line2')
    job:Address_Line2 = p_web.GetValue('job:Address_Line2')
    p_web.SetSessionValue('job:Address_Line2',job:Address_Line2)
  End
  if p_web.IfExistsValue('job:Address_Line3')
    job:Address_Line3 = p_web.GetValue('job:Address_Line3')
    p_web.SetSessionValue('job:Address_Line3',job:Address_Line3)
  End
  if p_web.IfExistsValue('jobe2:HubCustomer')
    jobe2:HubCustomer = p_web.GetValue('jobe2:HubCustomer')
    p_web.SetSessionValue('jobe2:HubCustomer',jobe2:HubCustomer)
  End
  if p_web.IfExistsValue('job:Postcode')
    job:Postcode = p_web.GetValue('job:Postcode')
    p_web.SetSessionValue('job:Postcode',job:Postcode)
  End
  if p_web.IfExistsValue('job:Telephone_Number')
    job:Telephone_Number = p_web.GetValue('job:Telephone_Number')
    p_web.SetSessionValue('job:Telephone_Number',job:Telephone_Number)
  End
  if p_web.IfExistsValue('job:Fax_Number')
    job:Fax_Number = p_web.GetValue('job:Fax_Number')
    p_web.SetSessionValue('job:Fax_Number',job:Fax_Number)
  End
  if p_web.IfExistsValue('jobe:EndUserEmailAddress')
    jobe:EndUserEmailAddress = p_web.GetValue('jobe:EndUserEmailAddress')
    p_web.SetSessionValue('jobe:EndUserEmailAddress',jobe:EndUserEmailAddress)
  End
  if p_web.IfExistsValue('jobe:EndUserTelNo')
    jobe:EndUserTelNo = p_web.GetValue('jobe:EndUserTelNo')
    p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
  End
  if p_web.IfExistsValue('jobe:VatNumber')
    jobe:VatNumber = p_web.GetValue('jobe:VatNumber')
    p_web.SetSessionValue('jobe:VatNumber',jobe:VatNumber)
  End
  if p_web.IfExistsValue('jobe2:IDNumber')
    jobe2:IDNumber = p_web.GetValue('jobe2:IDNumber')
    p_web.SetSessionValue('jobe2:IDNumber',jobe2:IDNumber)
  End
  if p_web.IfExistsValue('jobe2:SMSNotification')
    jobe2:SMSNotification = p_web.GetValue('jobe2:SMSNotification')
    p_web.SetSessionValue('jobe2:SMSNotification',jobe2:SMSNotification)
  End
  if p_web.IfExistsValue('jobe2:SMSAlertNumber')
    jobe2:SMSAlertNumber = p_web.GetValue('jobe2:SMSAlertNumber')
    p_web.SetSessionValue('jobe2:SMSAlertNumber',jobe2:SMSAlertNumber)
  End
  if p_web.IfExistsValue('jobe2:EmailNotification')
    jobe2:EmailNotification = p_web.GetValue('jobe2:EmailNotification')
    p_web.SetSessionValue('jobe2:EmailNotification',jobe2:EmailNotification)
  End
  if p_web.IfExistsValue('jobe2:EmailAlertAddress')
    jobe2:EmailAlertAddress = p_web.GetValue('jobe2:EmailAlertAddress')
    p_web.SetSessionValue('jobe2:EmailAlertAddress',jobe2:EmailAlertAddress)
  End
  if p_web.IfExistsValue('job:Courier')
    job:Courier = p_web.GetValue('job:Courier')
    p_web.SetSessionValue('job:Courier',job:Courier)
  End
  if p_web.IfExistsValue('jobe2:CourierWaybillNumber')
    jobe2:CourierWaybillNumber = p_web.GetValue('jobe2:CourierWaybillNumber')
    p_web.SetSessionValue('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber)
  End
  if p_web.IfExistsValue('jobe3:LoyaltyStatus')
    jobe3:LoyaltyStatus = p_web.GetValue('jobe3:LoyaltyStatus')
    p_web.SetSessionValue('jobe3:LoyaltyStatus',jobe3:LoyaltyStatus)
  End
  if p_web.IfExistsValue('job:Company_Name_Delivery')
    job:Company_Name_Delivery = p_web.GetValue('job:Company_Name_Delivery')
    p_web.SetSessionValue('job:Company_Name_Delivery',job:Company_Name_Delivery)
  End
  if p_web.IfExistsValue('job:Company_Name_Collection')
    job:Company_Name_Collection = p_web.GetValue('job:Company_Name_Collection')
    p_web.SetSessionValue('job:Company_Name_Collection',job:Company_Name_Collection)
  End
  if p_web.IfExistsValue('job:Address_Line1_Delivery')
    job:Address_Line1_Delivery = p_web.GetValue('job:Address_Line1_Delivery')
    p_web.SetSessionValue('job:Address_Line1_Delivery',job:Address_Line1_Delivery)
  End
  if p_web.IfExistsValue('job:Address_Line1_Collection')
    job:Address_Line1_Collection = p_web.GetValue('job:Address_Line1_Collection')
    p_web.SetSessionValue('job:Address_Line1_Collection',job:Address_Line1_Collection)
  End
  if p_web.IfExistsValue('job:Address_Line2_Delivery')
    job:Address_Line2_Delivery = p_web.GetValue('job:Address_Line2_Delivery')
    p_web.SetSessionValue('job:Address_Line2_Delivery',job:Address_Line2_Delivery)
  End
  if p_web.IfExistsValue('job:Address_Line2_Collection')
    job:Address_Line2_Collection = p_web.GetValue('job:Address_Line2_Collection')
    p_web.SetSessionValue('job:Address_Line2_Collection',job:Address_Line2_Collection)
  End
  if p_web.IfExistsValue('job:Address_Line3_Delivery')
    job:Address_Line3_Delivery = p_web.GetValue('job:Address_Line3_Delivery')
    p_web.SetSessionValue('job:Address_Line3_Delivery',job:Address_Line3_Delivery)
  End
  if p_web.IfExistsValue('job:Address_Line3_Collection')
    job:Address_Line3_Collection = p_web.GetValue('job:Address_Line3_Collection')
    p_web.SetSessionValue('job:Address_Line3_Collection',job:Address_Line3_Collection)
  End
  if p_web.IfExistsValue('jobe2:HubDelivery')
    jobe2:HubDelivery = p_web.GetValue('jobe2:HubDelivery')
    p_web.SetSessionValue('jobe2:HubDelivery',jobe2:HubDelivery)
  End
  if p_web.IfExistsValue('jobe2:HubCollection')
    jobe2:HubCollection = p_web.GetValue('jobe2:HubCollection')
    p_web.SetSessionValue('jobe2:HubCollection',jobe2:HubCollection)
  End
  if p_web.IfExistsValue('job:Postcode_Delivery')
    job:Postcode_Delivery = p_web.GetValue('job:Postcode_Delivery')
    p_web.SetSessionValue('job:Postcode_Delivery',job:Postcode_Delivery)
  End
  if p_web.IfExistsValue('job:Postcode_Collection')
    job:Postcode_Collection = p_web.GetValue('job:Postcode_Collection')
    p_web.SetSessionValue('job:Postcode_Collection',job:Postcode_Collection)
  End
  if p_web.IfExistsValue('job:Telephone_Delivery')
    job:Telephone_Delivery = p_web.GetValue('job:Telephone_Delivery')
    p_web.SetSessionValue('job:Telephone_Delivery',job:Telephone_Delivery)
  End
  if p_web.IfExistsValue('job:Telephone_Collection')
    job:Telephone_Collection = p_web.GetValue('job:Telephone_Collection')
    p_web.SetSessionValue('job:Telephone_Collection',job:Telephone_Collection)
  End
  if p_web.IfExistsValue('jbn:Delivery_Text')
    jbn:Delivery_Text = p_web.GetValue('jbn:Delivery_Text')
    p_web.SetSessionValue('jbn:Delivery_Text',jbn:Delivery_Text)
  End
  if p_web.IfExistsValue('jbn:Collection_Text')
    jbn:Collection_Text = p_web.GetValue('jbn:Collection_Text')
    p_web.SetSessionValue('jbn:Collection_Text',jbn:Collection_Text)
  End
  if p_web.IfExistsValue('jbn:DelContactName')
    jbn:DelContactName = p_web.GetValue('jbn:DelContactName')
    p_web.SetSessionValue('jbn:DelContactName',jbn:DelContactName)
  End
  if p_web.IfExistsValue('jbn:ColContatName')
    jbn:ColContatName = p_web.GetValue('jbn:ColContatName')
    p_web.SetSessionValue('jbn:ColContatName',jbn:ColContatName)
  End
  if p_web.IfExistsValue('jbn:DelDepartment')
    jbn:DelDepartment = p_web.GetValue('jbn:DelDepartment')
    p_web.SetSessionValue('jbn:DelDepartment',jbn:DelDepartment)
  End
  if p_web.IfExistsValue('jbn:ColDepartment')
    jbn:ColDepartment = p_web.GetValue('jbn:ColDepartment')
    p_web.SetSessionValue('jbn:ColDepartment',jbn:ColDepartment)
  End
  if p_web.IfExistsValue('jobe:Sub_Sub_Account')
    jobe:Sub_Sub_Account = p_web.GetValue('jobe:Sub_Sub_Account')
    p_web.SetSessionValue('jobe:Sub_Sub_Account',jobe:Sub_Sub_Account)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('AmendAddress_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  !region Prime
    p_web.SSV('Hide:DeliveryAddress',1)
    p_web.SSV('Hide:CollectionAddress',1)
    Access:TRANTYPE.Clearkey(trt:transit_Type_Key)
    trt:transit_Type    = p_web.GSV('job:transit_Type')
    if (Access:TRANTYPE.TryFetch(trt:transit_Type_Key) = Level:Benign)
      ! Found
        if (trt:Delivery_Address = 'YES')
            p_web.SSV('Hide:DeliveryAddress',0)
        end ! if (trt:DeliveryAddress = 'YES')
  
        if (trt:Collection_Address = 'YES')
            p_web.SSV('Hide:CollectionAddress',0)
        end ! if (trt:Collection_Address = 'YES')
  
    else ! if (Access:TRANTYPE.TryFetch(trt:transit_Type_Key) = Level:Benign)
      ! Error
    end ! if (Access:TRANTYPE.TryFetch(trt:transit_Type_Key) = Level:Benign)
  
    p_web.StoreValue('FromURL')
  
  
    p_web.SSV('Comment:TelephoneNumber','')
    p_web.SSV('Comment:FaxNumber','')
    p_web.SSV('Comment:SMSAlertNumber','')
    p_web.SSV('Comment:TelephoneNumberDelivery','')
    p_web.SSV('Comment:TelephoneNumberCollection','')
  
    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = p_web.GSV('job:Account_Number')
    IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
        IF (sub:UseAlternativeAdd)
            p_web.SSV('Hide:SubSubAccount',0)
        ELSE
            p_web.SSV('Hide:SubSubAccount',1)
        END
          ! Start - Only show VAT Number if the sub account is "overridden" and blank - TrkBs: 5364 (DBH: 26-04-2005)
        If sub:Vat_Number = '' AND sub:OverrideHeadVATNo = True
            p_web.SSV('Hide:VatNumber',0)
        Else ! sub:Vat_Number = '' AND tra:Vat_Number = ''
            p_web.SSV('Hide:VatNumber',1)
        End ! sub:Vat_Number = '' AND tra:Vat_Number = ''
          ! End   - Only show VAT Number if the sub account is "overridden" and blank - TrkBs: 5364 (DBH: 26-04-2005)
    END
  
    p_web.SSV('Hide:AmendC3DButton',1)
    ! #13498 Missed out from original spec (13224) (DBH: 03/03/2015)
    Access:C3DMONIT.ClearKey(c3d:KeyRefNumber)
    IF (p_web.GSV('job:Ref_Number') > 0)
        c3d:RefNumber = p_web.GSV('job:Ref_Number')
    ELSIF (p_web.GSV('SiebelRefNo') > 0)
        ! Here if it's a new job booking (DBH: 05/03/2015)
        C3D:RefNumber = p_web.GSV('SiebelRefNo')
    END ! IF
    IF (C3D:RefNumber > 0)
        SET(c3d:KeyRefNumber,c3d:KeyRefNumber)
        LOOP UNTIL Access:C3DMONIT.Next() <> Level:Benign
            IF (p_web.GSV('job:Ref_Number') > 0)
                IF (c3d:RefNumber <> p_web.GSV('job:Ref_Number'))
                    BREAK
                END ! IIF
            ELSIF (p_web.GSV('SiebelRefNo') > 0)
                IF (C3D:RefNumber <> p_web.GSV('SiebelRefNo'))
                    BREAK
                END ! IF
            END ! IF
            IF (c3d:ErrorCode = 'OK')
                p_web.SSV('Hide:AmendC3DButton',0)
                BREAK
            END ! IF
        END ! LOOP
    END ! IF
    
  !endregion  
  !
  ! EMBED ID:           %GenerateForm
  ! EMBED Description:  Generate Form
  ! EMBED Parameters:   
  ! CW Version:         8000
  !
  ! #AT(%GenerateForm,%ActiveTemplateInstance')
  ! #ENDAT
  !
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('FromURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('AmendAddress_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('AmendAddress_ChainTo')
    loc:formaction = p_web.GetSessionValue('AmendAddress_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="AmendAddress" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="AmendAddress" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="AmendAddress" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Amend Addresses') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Amend Addresses',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_AmendAddress">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_AmendAddress" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  do GenerateTab4
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_AmendAddress')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GSV('FromURL') <> 'CustomerServicesForm'
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Browse') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('End User Details') & ''''
        If p_web.GSV('Hide:AmendC3DButton') = 0
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Collection / Delivery Details') & ''''
        If p_web.GSV('Hide:SubSubAccount') <> 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Other Details') & ''''
        End
        Loc:Tabnumber = p_web.getSessionValue('showtab_AmendAddress')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_AmendAddress'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='SUBURB'
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Telephone_Number')
    End
    If upper(p_web.getvalue('LookupFile'))='COURIER'
            p_web.SetValue('SelectField',clip(loc:formname) & '.jobe2:CourierWaybillNumber')
    End
    If upper(p_web.getvalue('LookupFile'))='SUBURB'
          If Not (p_web.GSV('Hide:CollectionAddress') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Address_Line3_Collection')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='SUBURB'
          If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.job:Telephone_Delivery')
          End
    End
  Else
    If False
    ElsIf p_web.GSV('FromURL') <> 'CustomerServicesForm'
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.job:Company_Name')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GSV('FromURL') <> 'CustomerServicesForm'
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab5'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          If p_web.GSV('Hide:AmendC3DButton') = 0
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab6'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          If p_web.GSV('Hide:SubSubAccount') <> 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          End
          Loc:Tabnumber = p_web.getSessionValue('showtab_AmendAddress')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GSV('FromURL') <> 'CustomerServicesForm'
      packet = clip(packet) & 'roundCorners(''tab5'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    if p_web.GSV('Hide:AmendC3DButton') = 0
      packet = clip(packet) & 'roundCorners(''tab6'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    if p_web.GSV('Hide:SubSubAccount') <> 1
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
    end
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GSV('FromURL') <> 'CustomerServicesForm'
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel5">'&CRLF &|
                                    '  <div id="panel5Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Browse') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel5Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_AmendAddress_5">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab5" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab5">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Browse')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Link:AuditTrail
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Link:AuditTrail
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Link:EngineerHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Link:EngineerHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Link:StatusChanges
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Link:StatusChanges
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::link:BrowseLocationHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::link:BrowseLocationHistory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('End User Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_AmendAddress_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('End User Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('End User Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('End User Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('End User Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::temp:Title
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::temp:Title
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::temp:Title
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Company_Name
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Company_Name
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Company_Name
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:HubCustomer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:HubCustomer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:HubCustomer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Postcode
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Postcode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Postcode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Telephone_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Telephone_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Telephone_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Fax_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Fax_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Fax_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:EndUserEmailAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:EndUserEmailAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe:EndUserEmailAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:EndUserTelNo
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:EndUserTelNo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe:EndUserTelNo
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('Hide:VatNumber') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:VatNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:VatNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe:VatNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:IDNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:IDNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:IDNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:SMSNotification
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:SMSNotification
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:SMSNotification
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:SMSAlertNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:SMSAlertNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:SMSAlertNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:EmailNotification
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:EmailNotification
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:EmailNotification
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:EmailAlertAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:EmailAlertAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:EmailAlertAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Courier
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:CourierWaybillNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:CourierWaybillNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:CourierWaybillNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe3:LoyaltyStatus
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe3:LoyaltyStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe3:LoyaltyStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
  If p_web.GSV('Hide:AmendC3DButton') = 0
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel6">'&CRLF &|
                                    '  <div id="panel6Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel6Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_AmendAddress_6">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab6" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab6">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab6">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab6">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnAmendC3D
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnAmendC3D
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Collection / Delivery Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_AmendAddress_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Collection / Delivery Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Collection / Delivery Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Collection / Delivery Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Collection / Delivery Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::DeliveryAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::DeliveryAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::DeliveryAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::CollectionAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::CollectionAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::CollectionAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Company_Name_Delivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Company_Name_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Company_Name_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Company_Name_Collection
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Company_Name_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Company_Name_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line1_Delivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line1_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line1_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line1_Collection
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line1_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line1_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line2_Delivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line2_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line2_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line2_Collection
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line2_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line2_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line3_Delivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line3_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line3_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Address_Line3_Collection
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Address_Line3_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Address_Line3_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:HubDelivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:HubDelivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:HubDelivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe2:HubCollection
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe2:HubCollection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe2:HubCollection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Postcode_Delivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Postcode_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Postcode_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Postcode_Collection
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Postcode_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Postcode_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Telephone_Delivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Telephone_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Telephone_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Telephone_Collection
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Telephone_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Telephone_Collection
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Button:CopyEndUserAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Button:CopyEndUserAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::Button:CopyEndUserAddress1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Button:CopyEndUserAddress1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Button:CopyEndUserAddress1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:Delivery_Text
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:Delivery_Text
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jbn:Delivery_Text
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:Collection_Text
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:Collection_Text
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jbn:Collection_Text
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:DelContactName
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:DelContactName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jbn:DelContactName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:ColContatName
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:ColContatName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jbn:ColContatName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:DelDepartment
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:DelDepartment
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jbn:DelDepartment
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:ColDepartment
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:ColDepartment
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jbn:ColDepartment
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab4  Routine
  If p_web.GSV('Hide:SubSubAccount') <> 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Other Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_AmendAddress_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Other Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Other Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Other Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Other Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&140&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jobe:Sub_Sub_Account
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jobe:Sub_Sub_Account
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::jobe:Sub_Sub_Account
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end


Validate::Link:AuditTrail  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Link:AuditTrail',p_web.GetValue('NewValue'))
    do Value::Link:AuditTrail
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::Link:AuditTrail  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('Link:AuditTrail') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.CreateHyperLink(p_web.Translate('Audit Trail'),'BrowseAuditFilter','_self',,loc:javascript,,0) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::Link:AuditTrail  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('Link:AuditTrail') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::Link:EngineerHistory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Link:EngineerHistory',p_web.GetValue('NewValue'))
    do Value::Link:EngineerHistory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::Link:EngineerHistory  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('Link:EngineerHistory') & '_value',Choose(p_web.GSV('Hide:EngineerHistory') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:EngineerHistory') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.CreateHyperLink(p_web.Translate('Engineer History'),'FormBrowseEngineerHistory','_self',,loc:javascript,,0) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::Link:EngineerHistory  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('Link:EngineerHistory') & '_comment',Choose(p_web.GSV('Hide:EngineerHistory') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:EngineerHistory') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::Link:StatusChanges  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Link:StatusChanges',p_web.GetValue('NewValue'))
    do Value::Link:StatusChanges
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::Link:StatusChanges  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('Link:StatusChanges') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.CreateHyperLink(p_web.Translate('Status Changes'),'BrowseStatusFilter','_self',,loc:javascript,,0) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::Link:StatusChanges  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('Link:StatusChanges') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::link:BrowseLocationHistory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('link:BrowseLocationHistory',p_web.GetValue('NewValue'))
    do Value::link:BrowseLocationHistory
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::link:BrowseLocationHistory  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('link:BrowseLocationHistory') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.CreateHyperLink(p_web.Translate('Location History'),'FormBrowseLocationHistory','_Self',,loc:javascript,,0) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::link:BrowseLocationHistory  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('link:BrowseLocationHistory') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::temp:Title  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('temp:Title') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::temp:Title  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('temp:Title',p_web.GetValue('NewValue'))
    do Value::temp:Title
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::temp:Title  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('temp:Title') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('BlueBold')&'">' & p_web.Translate('End User Address',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::temp:Title  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('temp:Title') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Company_Name  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Customer Name')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Company_Name  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Company_Name',p_web.GetValue('NewValue'))
    job:Company_Name = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Company_Name
    do Value::job:Company_Name
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Company_Name',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Company_Name = p_web.GetValue('Value')
  End
    job:Company_Name = Upper(job:Company_Name)
    p_web.SetSessionValue('job:Company_Name',job:Company_Name)
  do Value::job:Company_Name
  do SendAlert

Value::job:Company_Name  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Company_Name
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('Hide:AmendC3DButton') = 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('Hide:AmendC3DButton') = 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Company_Name')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Company_Name'',''amendaddress_job:company_name_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Company_Name',p_web.GetSessionValueFormat('job:Company_Name'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Company_Name') & '_value')

Comment::job:Company_Name  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Address_Line1  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Address')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Address_Line1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line1',p_web.GetValue('NewValue'))
    job:Address_Line1 = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line1
    do Value::job:Address_Line1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line1',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line1 = p_web.GetValue('Value')
  End
    job:Address_Line1 = Upper(job:Address_Line1)
    p_web.SetSessionValue('job:Address_Line1',job:Address_Line1)
  do Value::job:Address_Line1
  do SendAlert

Value::job:Address_Line1  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Address_Line1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('Hide:AmendC3DButton') = 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('Hide:AmendC3DButton') = 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Address_Line1')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line1'',''amendaddress_job:address_line1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line1',p_web.GetSessionValueFormat('job:Address_Line1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line1') & '_value')

Comment::job:Address_Line1  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Address_Line2  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Address_Line2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line2',p_web.GetValue('NewValue'))
    job:Address_Line2 = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line2
    do Value::job:Address_Line2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line2',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line2 = p_web.GetValue('Value')
  End
    job:Address_Line2 = Upper(job:Address_Line2)
    p_web.SetSessionValue('job:Address_Line2',job:Address_Line2)
  do Value::job:Address_Line2
  do SendAlert

Value::job:Address_Line2  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Address_Line2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('Hide:AmendC3DButton') = 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('Hide:AmendC3DButton') = 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Address_Line2')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line2'',''amendaddress_job:address_line2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line2',p_web.GetSessionValueFormat('job:Address_Line2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line2') & '_value')

Comment::job:Address_Line2  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Address_Line3  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Suburb')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Address_Line3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line3',p_web.GetValue('NewValue'))
    job:Address_Line3 = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line3
    do Value::job:Address_Line3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line3',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line3 = p_web.GetValue('Value')
  End
    job:Address_Line3 = Upper(job:Address_Line3)
    p_web.SetSessionValue('job:Address_Line3',job:Address_Line3)
      Access:SUBURB.ClearKey(sur:SuburbKey)
      sur:Suburb = p_web.GSV('job:Address_Line3')
      IF (Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign)
          p_web.SSV('job:Postcode',sur:Postcode)
          p_web.SSV('jobe2:HubCustomer',sur:Hub)
      END
  
  p_Web.SetValue('lookupfield','job:Address_Line3')
  do AfterLookup
  do Value::job:Postcode
  do Value::jobe2:HubCustomer
  do Value::job:Address_Line3
  do SendAlert
  do Comment::job:Address_Line3
  do Value::jobe2:HubCustomer  !1
  do Value::job:Postcode  !1

Value::job:Address_Line3  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Address_Line3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('Hide:AmendC3DButton') = 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('Hide:AmendC3DButton') = 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Address_Line3')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line3'',''amendaddress_job:address_line3_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Address_Line3')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Address_Line3',p_web.GetSessionValue('job:Address_Line3'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('LookupSuburbs')&'?LookupField=job:Address_Line3&Tab=4&ForeignField=sur:Suburb&_sort=&Refresh=sort&LookupFrom=AmendAddress&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line3') & '_value')

Comment::job:Address_Line3  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line3') & '_comment')

Prompt::jobe2:HubCustomer  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubCustomer') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Hub')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubCustomer') & '_prompt')

Validate::jobe2:HubCustomer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:HubCustomer',p_web.GetValue('NewValue'))
    jobe2:HubCustomer = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:HubCustomer
    do Value::jobe2:HubCustomer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:HubCustomer',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe2:HubCustomer = p_web.GetValue('Value')
  End
    jobe2:HubCustomer = Upper(jobe2:HubCustomer)
    p_web.SetSessionValue('jobe2:HubCustomer',jobe2:HubCustomer)
  do Value::jobe2:HubCustomer
  do SendAlert

Value::jobe2:HubCustomer  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubCustomer') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe2:HubCustomer
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('jobe2:HubCustomer')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:HubCustomer'',''amendaddress_jobe2:hubcustomer_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:HubCustomer',p_web.GetSessionValueFormat('jobe2:HubCustomer'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Hub') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubCustomer') & '_value')

Comment::jobe2:HubCustomer  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubCustomer') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubCustomer') & '_comment')

Prompt::job:Postcode  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Postcode')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode') & '_prompt')

Validate::job:Postcode  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Postcode',p_web.GetValue('NewValue'))
    job:Postcode = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Postcode
    do Value::job:Postcode
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Postcode',p_web.dFormat(p_web.GetValue('Value'),'@s10'))
    job:Postcode = p_web.GetValue('Value')
  End
  do Value::job:Postcode
  do SendAlert

Value::job:Postcode  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Postcode
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('job:Postcode')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Postcode'',''amendaddress_job:postcode_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Postcode',p_web.GetSessionValueFormat('job:Postcode'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s10'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode') & '_value')

Comment::job:Postcode  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode') & '_comment')

Prompt::job:Telephone_Number  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Telephone Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Telephone_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Telephone_Number',p_web.GetValue('NewValue'))
    job:Telephone_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Telephone_Number
    do Value::job:Telephone_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Telephone_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Telephone_Number = p_web.GetValue('Value')
  End
    job:Telephone_Number = Upper(job:Telephone_Number)
    p_web.SetSessionValue('job:Telephone_Number',job:Telephone_Number)
      If PassMobileNumberFormat(p_web.GSV('job:Telephone_Number')) = 0
          p_web.SSV('job:Telephone_Number','')
          p_web.SSV('Comment:TelephoneNumber','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:TelephoneNumber','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::job:Telephone_Number
  do SendAlert
  do Comment::job:Telephone_Number

Value::job:Telephone_Number  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Telephone_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('Hide:AmendC3DButton') = 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('Hide:AmendC3DButton') = 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Telephone_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Telephone_Number'',''amendaddress_job:telephone_number_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Telephone_Number')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Telephone_Number',p_web.GetSessionValueFormat('job:Telephone_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Telephone_Number') & '_value')

Comment::job:Telephone_Number  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:TelephoneNumber'))
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Number') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Telephone_Number') & '_comment')

Prompt::job:Fax_Number  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Fax_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Fax Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Fax_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Fax_Number',p_web.GetValue('NewValue'))
    job:Fax_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Fax_Number
    do Value::job:Fax_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Fax_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Fax_Number = p_web.GetValue('Value')
  End
    job:Fax_Number = Upper(job:Fax_Number)
    p_web.SetSessionValue('job:Fax_Number',job:Fax_Number)
      If PassMobileNumberFormat(p_web.GSV('job:Fax_Number')) = 0
          p_web.SSV('job:Fax_Number','')
          p_web.SSV('Comment:FaxNumber','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:FaxNumber','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::job:Fax_Number
  do SendAlert
  do Comment::job:Fax_Number

Value::job:Fax_Number  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Fax_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Fax_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('Hide:AmendC3DButton') = 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('Hide:AmendC3DButton') = 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Fax_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Fax_Number'',''amendaddress_job:fax_number_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Fax_Number')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Fax_Number',p_web.GetSessionValueFormat('job:Fax_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Fax_Number') & '_value')

Comment::job:Fax_Number  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:FaxNumber'))
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Fax_Number') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Fax_Number') & '_comment')

Prompt::jobe:EndUserEmailAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:EndUserEmailAddress') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Email Address')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe:EndUserEmailAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:EndUserEmailAddress',p_web.GetValue('NewValue'))
    jobe:EndUserEmailAddress = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe:EndUserEmailAddress
    do Value::jobe:EndUserEmailAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:EndUserEmailAddress',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jobe:EndUserEmailAddress = p_web.GetValue('Value')
  End
  do Value::jobe:EndUserEmailAddress
  do SendAlert

Value::jobe:EndUserEmailAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:EndUserEmailAddress') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe:EndUserEmailAddress
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('Hide:AmendC3DButton') = 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('Hide:AmendC3DButton') = 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('jobe:EndUserEmailAddress')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:EndUserEmailAddress'',''amendaddress_jobe:enduseremailaddress_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe:EndUserEmailAddress',p_web.GetSessionValueFormat('jobe:EndUserEmailAddress'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s255'),'Email Address') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe:EndUserEmailAddress') & '_value')

Comment::jobe:EndUserEmailAddress  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:EndUserEmailAddress') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe:EndUserTelNo  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:EndUserTelNo') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('End User Tel No')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe:EndUserTelNo  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:EndUserTelNo',p_web.GetValue('NewValue'))
    jobe:EndUserTelNo = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe:EndUserTelNo
    do Value::jobe:EndUserTelNo
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:EndUserTelNo',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    jobe:EndUserTelNo = p_web.GetValue('Value')
  End
    jobe:EndUserTelNo = Upper(jobe:EndUserTelNo)
    p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
  do Value::jobe:EndUserTelNo
  do SendAlert

Value::jobe:EndUserTelNo  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:EndUserTelNo') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe:EndUserTelNo
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jobe:EndUserTelNo')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:EndUserTelNo'',''amendaddress_jobe:endusertelno_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe:EndUserTelNo',p_web.GetSessionValueFormat('jobe:EndUserTelNo'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe:EndUserTelNo') & '_value')

Comment::jobe:EndUserTelNo  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:EndUserTelNo') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe:VatNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:VatNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Vat Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe:VatNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:VatNumber',p_web.GetValue('NewValue'))
    jobe:VatNumber = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe:VatNumber
    do Value::jobe:VatNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:VatNumber',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe:VatNumber = p_web.GetValue('Value')
  End
    jobe:VatNumber = Upper(jobe:VatNumber)
    p_web.SetSessionValue('jobe:VatNumber',jobe:VatNumber)
  do Value::jobe:VatNumber
  do SendAlert

Value::jobe:VatNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:VatNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe:VatNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jobe:VatNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:VatNumber'',''amendaddress_jobe:vatnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe:VatNumber',p_web.GetSessionValueFormat('jobe:VatNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Vat Number') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe:VatNumber') & '_value')

Comment::jobe:VatNumber  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:VatNumber') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe2:IDNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:IDNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('ID Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe2:IDNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:IDNumber',p_web.GetValue('NewValue'))
    jobe2:IDNumber = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:IDNumber
    do Value::jobe2:IDNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:IDNumber',p_web.dFormat(p_web.GetValue('Value'),'@s13'))
    jobe2:IDNumber = p_web.GetValue('Value')
  End
    jobe2:IDNumber = Upper(jobe2:IDNumber)
    p_web.SetSessionValue('jobe2:IDNumber',jobe2:IDNumber)
  do Value::jobe2:IDNumber
  do SendAlert

Value::jobe2:IDNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:IDNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe2:IDNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jobe2:IDNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:IDNumber'',''amendaddress_jobe2:idnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:IDNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:IDNumber',p_web.GetSessionValueFormat('jobe2:IDNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s13'),'I.D. Number') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:IDNumber') & '_value')

Comment::jobe2:IDNumber  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:IDNumber') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe2:SMSNotification  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:SMSNotification') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('SMS Notification')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe2:SMSNotification  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:SMSNotification',p_web.GetValue('NewValue'))
    jobe2:SMSNotification = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe2:SMSNotification
    do Value::jobe2:SMSNotification
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe2:SMSNotification',p_web.GetValue('Value'))
    jobe2:SMSNotification = p_web.GetValue('Value')
  End
  do Value::jobe2:SMSNotification
  do SendAlert
  do Prompt::jobe2:SMSAlertNumber
  do Value::jobe2:SMSAlertNumber  !1

Value::jobe2:SMSNotification  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:SMSNotification') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- jobe2:SMSNotification
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:SMSNotification'',''amendaddress_jobe2:smsnotification_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:SMSNotification')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:SMSNotification') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:SMSNotification',clip(1),,loc:readonly,,,loc:javascript,,'SMS Notification') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:SMSNotification') & '_value')

Comment::jobe2:SMSNotification  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:SMSNotification') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe2:SMSAlertNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:SMSAlertNumber') & '_prompt',Choose(p_web.GSV('jobe2:SMSNotification') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Mobile No')
  If p_web.GSV('jobe2:SMSNotification') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:SMSAlertNumber') & '_prompt')

Validate::jobe2:SMSAlertNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:SMSAlertNumber',p_web.GetValue('NewValue'))
    jobe2:SMSAlertNumber = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:SMSAlertNumber
    do Value::jobe2:SMSAlertNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:SMSAlertNumber',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe2:SMSAlertNumber = p_web.GetValue('Value')
  End
    jobe2:SMSAlertNumber = Upper(jobe2:SMSAlertNumber)
    p_web.SetSessionValue('jobe2:SMSAlertNumber',jobe2:SMSAlertNumber)
      If PassMobileNumberFormat(p_web.GSV('jobe2:SMSAlertNumber')) = 0
          p_web.SSV('jobe2:SMSAlertNumber','')
          p_web.SSV('Comment:SMSAlertNumber','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:SMSAlertNumber','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::jobe2:SMSAlertNumber
  do SendAlert
  do Comment::jobe2:SMSAlertNumber

Value::jobe2:SMSAlertNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:SMSAlertNumber') & '_value',Choose(p_web.GSV('jobe2:SMSNotification') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('jobe2:SMSNotification') = 0)
  ! --- STRING --- jobe2:SMSAlertNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jobe2:SMSAlertNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:SMSAlertNumber'',''amendaddress_jobe2:smsalertnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:SMSAlertNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:SMSAlertNumber',p_web.GetSessionValueFormat('jobe2:SMSAlertNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'SMS Alert Mobile Number') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:SMSAlertNumber') & '_value')

Comment::jobe2:SMSAlertNumber  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:SMSAlertNumber'))
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:SMSAlertNumber') & '_comment',Choose(p_web.GSV('jobe2:SMSNotification') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('jobe2:SMSNotification') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:SMSAlertNumber') & '_comment')

Prompt::jobe2:EmailNotification  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:EmailNotification') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Email Notification')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe2:EmailNotification  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:EmailNotification',p_web.GetValue('NewValue'))
    jobe2:EmailNotification = p_web.GetValue('NewValue') !FieldType= BYTE Field = jobe2:EmailNotification
    do Value::jobe2:EmailNotification
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('jobe2:EmailNotification',p_web.GetValue('Value'))
    jobe2:EmailNotification = p_web.GetValue('Value')
  End
  do Value::jobe2:EmailNotification
  do SendAlert
  do Prompt::jobe2:EmailAlertAddress
  do Value::jobe2:EmailAlertAddress  !1

Value::jobe2:EmailNotification  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:EmailNotification') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- jobe2:EmailNotification
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''jobe2:EmailNotification'',''amendaddress_jobe2:emailnotification_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:EmailNotification')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('jobe2:EmailNotification') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','jobe2:EmailNotification',clip(1),,loc:readonly,,,loc:javascript,,'Email Notification') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:EmailNotification') & '_value')

Comment::jobe2:EmailNotification  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:EmailNotification') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe2:EmailAlertAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:EmailAlertAddress') & '_prompt',Choose(p_web.GSV('jobe2:EmailNotification') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Email Address')
  If p_web.GSV('jobe2:EmailNotification') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:EmailAlertAddress') & '_prompt')

Validate::jobe2:EmailAlertAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:EmailAlertAddress',p_web.GetValue('NewValue'))
    jobe2:EmailAlertAddress = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:EmailAlertAddress
    do Value::jobe2:EmailAlertAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:EmailAlertAddress',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jobe2:EmailAlertAddress = p_web.GetValue('Value')
  End
  do Value::jobe2:EmailAlertAddress
  do SendAlert

Value::jobe2:EmailAlertAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:EmailAlertAddress') & '_value',Choose(p_web.GSV('jobe2:EmailNotification') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('jobe2:EmailNotification') = 0)
  ! --- STRING --- jobe2:EmailAlertAddress
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('jobe2:EmailAlertAddress')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:EmailAlertAddress'',''amendaddress_jobe2:emailalertaddress_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:EmailAlertAddress',p_web.GetSessionValueFormat('jobe2:EmailAlertAddress'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s255'),'Email Alert Address') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:EmailAlertAddress') & '_value')

Comment::jobe2:EmailAlertAddress  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:EmailAlertAddress') & '_comment',Choose(p_web.GSV('jobe2:EmailNotification') = 0,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('jobe2:EmailNotification') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Courier  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Courier') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Outgoing Courier')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Courier  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Courier',p_web.GetValue('NewValue'))
    job:Courier = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Courier
    do Value::job:Courier
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Courier',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Courier = p_web.GetValue('Value')
  End
  If job:Courier = '' and ForceCourier('B')
    loc:Invalid = 'job:Courier'
    loc:alert = p_web.translate('Outgoing Courier') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  p_Web.SetValue('lookupfield','job:Courier')
  do AfterLookup
  do Value::job:Courier
  do SendAlert
  do Comment::job:Courier

Value::job:Courier  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Courier') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- job:Courier
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If (ForceCourier('B'))
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('job:Courier')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If job:Courier = '' and (ForceCourier('B'))
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Courier'',''amendaddress_job:courier_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Courier')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Courier',p_web.GetSessionValue('job:Courier'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),'Outgoing Courier') & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('SelectCouriers?LookupField=job:Courier&Tab=4&ForeignField=cou:Courier&_sort=cou:Courier&Refresh=sort&LookupFrom=AmendAddress&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Courier') & '_value')

Comment::job:Courier  Routine
    If ForceCourier('B')
      loc:comment = p_web.translate(p_web.site.RequiredText)
    End
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Courier') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Courier') & '_comment')

Prompt::jobe2:CourierWaybillNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:CourierWaybillNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Courier Waybill Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe2:CourierWaybillNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:CourierWaybillNumber',p_web.GetValue('NewValue'))
    jobe2:CourierWaybillNumber = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:CourierWaybillNumber
    do Value::jobe2:CourierWaybillNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:CourierWaybillNumber',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe2:CourierWaybillNumber = p_web.GetValue('Value')
  End
    jobe2:CourierWaybillNumber = Upper(jobe2:CourierWaybillNumber)
    p_web.SetSessionValue('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber)
  do Value::jobe2:CourierWaybillNumber
  do SendAlert

Value::jobe2:CourierWaybillNumber  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:CourierWaybillNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe2:CourierWaybillNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jobe2:CourierWaybillNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:CourierWaybillNumber'',''amendaddress_jobe2:courierwaybillnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:CourierWaybillNumber',p_web.GetSessionValueFormat('jobe2:CourierWaybillNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Courier Waybill Number') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:CourierWaybillNumber') & '_value')

Comment::jobe2:CourierWaybillNumber  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:CourierWaybillNumber') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe3:LoyaltyStatus  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe3:LoyaltyStatus') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Loyalty Status:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe3:LoyaltyStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe3:LoyaltyStatus',p_web.GetValue('NewValue'))
    jobe3:LoyaltyStatus = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe3:LoyaltyStatus
    do Value::jobe3:LoyaltyStatus
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe3:LoyaltyStatus',p_web.dFormat(p_web.GetValue('Value'),'@s250'))
    jobe3:LoyaltyStatus = p_web.GetValue('Value')
  End

Value::jobe3:LoyaltyStatus  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe3:LoyaltyStatus') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- jobe3:LoyaltyStatus
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('jobe3:LoyaltyStatus'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::jobe3:LoyaltyStatus  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe3:LoyaltyStatus') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnAmendC3D  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnAmendC3D',p_web.GetValue('NewValue'))
    do Value::btnAmendC3D
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    IF (DoesUserHaveAccess(p_web,'CHANGE SIEBEL CUSTOMER INFO'))
        p_web.SSV('Hide:AmendC3DButton',1) ! #13498 Missed out from original spec (13224) (DBH: 03/03/2015)
    ELSE
        loc:Alert = 'You do not have access to this option.'
    END ! IF
    
  do SendAlert
  do Value::btnAmendC3D  !1
  do Value::job:Company_Name  !1
  do Value::job:Address_Line1  !1
  do Value::job:Address_Line2  !1
  do Value::job:Address_Line3  !1
  do Value::job:Telephone_Number  !1
  do Value::job:Fax_Number  !1
  do Value::jobe:EndUserEmailAddress  !1

Value::btnAmendC3D  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('btnAmendC3D') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnAmendC3D'',''amendaddress_btnamendc3d_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnAmendC3D','Amend C3D Cust Details','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('btnAmendC3D') & '_value')

Comment::btnAmendC3D  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('btnAmendC3D') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::DeliveryAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('DeliveryAddress') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::DeliveryAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('DeliveryAddress',p_web.GetValue('NewValue'))
    do Value::DeliveryAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::DeliveryAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('DeliveryAddress') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('BlueBold')&'">' & p_web.Translate('Delivery Address',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::DeliveryAddress  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('DeliveryAddress') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::CollectionAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('CollectionAddress') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::CollectionAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('CollectionAddress',p_web.GetValue('NewValue'))
    do Value::CollectionAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::CollectionAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('CollectionAddress') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('BlueBold')&'">' & p_web.Translate('Collection Address',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::CollectionAddress  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('CollectionAddress') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Company_Name_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Customer Name')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Company_Name_Delivery') & '_prompt')

Validate::job:Company_Name_Delivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Company_Name_Delivery',p_web.GetValue('NewValue'))
    job:Company_Name_Delivery = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Company_Name_Delivery
    do Value::job:Company_Name_Delivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Company_Name_Delivery',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Company_Name_Delivery = p_web.GetValue('Value')
  End
    job:Company_Name_Delivery = Upper(job:Company_Name_Delivery)
    p_web.SetSessionValue('job:Company_Name_Delivery',job:Company_Name_Delivery)
  do Value::job:Company_Name_Delivery
  do SendAlert

Value::job:Company_Name_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name_Delivery') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Company_Name_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Company_Name_Delivery')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Company_Name_Delivery'',''amendaddress_job:company_name_delivery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Company_Name_Delivery',p_web.GetSessionValueFormat('job:Company_Name_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Company_Name_Delivery') & '_value')

Comment::job:Company_Name_Delivery  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name_Delivery') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Company_Name_Delivery') & '_comment')

Prompt::job:Company_Name_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Company Name')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Company_Name_Collection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Company_Name_Collection',p_web.GetValue('NewValue'))
    job:Company_Name_Collection = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Company_Name_Collection
    do Value::job:Company_Name_Collection
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Company_Name_Collection',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Company_Name_Collection = p_web.GetValue('Value')
  End
  do Value::job:Company_Name_Collection
  do SendAlert

Value::job:Company_Name_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name_Collection') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Company_Name_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Company_Name_Collection')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Company_Name_Collection'',''amendaddress_job:company_name_collection_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Company_Name_Collection',p_web.GetSessionValueFormat('job:Company_Name_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Company_Name_Collection') & '_value')

Comment::job:Company_Name_Collection  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Company_Name_Collection') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Address_Line1_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Address')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line1_Delivery') & '_prompt')

Validate::job:Address_Line1_Delivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line1_Delivery',p_web.GetValue('NewValue'))
    job:Address_Line1_Delivery = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line1_Delivery
    do Value::job:Address_Line1_Delivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line1_Delivery',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line1_Delivery = p_web.GetValue('Value')
  End
    job:Address_Line1_Delivery = Upper(job:Address_Line1_Delivery)
    p_web.SetSessionValue('job:Address_Line1_Delivery',job:Address_Line1_Delivery)
  do Value::job:Address_Line1_Delivery
  do SendAlert

Value::job:Address_Line1_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1_Delivery') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Address_Line1_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Address_Line1_Delivery')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line1_Delivery'',''amendaddress_job:address_line1_delivery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line1_Delivery',p_web.GetSessionValueFormat('job:Address_Line1_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line1_Delivery') & '_value')

Comment::job:Address_Line1_Delivery  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1_Delivery') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line1_Delivery') & '_comment')

Prompt::job:Address_Line1_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Address')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Address_Line1_Collection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line1_Collection',p_web.GetValue('NewValue'))
    job:Address_Line1_Collection = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line1_Collection
    do Value::job:Address_Line1_Collection
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line1_Collection',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line1_Collection = p_web.GetValue('Value')
  End
    job:Address_Line1_Collection = Upper(job:Address_Line1_Collection)
    p_web.SetSessionValue('job:Address_Line1_Collection',job:Address_Line1_Collection)
  do Value::job:Address_Line1_Collection
  do SendAlert

Value::job:Address_Line1_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1_Collection') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Address_Line1_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Address_Line1_Collection')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line1_Collection'',''amendaddress_job:address_line1_collection_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line1_Collection',p_web.GetSessionValueFormat('job:Address_Line1_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line1_Collection') & '_value')

Comment::job:Address_Line1_Collection  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line1_Collection') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Address_Line2_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line2_Delivery') & '_prompt')

Validate::job:Address_Line2_Delivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line2_Delivery',p_web.GetValue('NewValue'))
    job:Address_Line2_Delivery = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line2_Delivery
    do Value::job:Address_Line2_Delivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line2_Delivery',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line2_Delivery = p_web.GetValue('Value')
  End
    job:Address_Line2_Delivery = Upper(job:Address_Line2_Delivery)
    p_web.SetSessionValue('job:Address_Line2_Delivery',job:Address_Line2_Delivery)
  do Value::job:Address_Line2_Delivery
  do SendAlert

Value::job:Address_Line2_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2_Delivery') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Address_Line2_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Address_Line2_Delivery')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line2_Delivery'',''amendaddress_job:address_line2_delivery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line2_Delivery',p_web.GetSessionValueFormat('job:Address_Line2_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line2_Delivery') & '_value')

Comment::job:Address_Line2_Delivery  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2_Delivery') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line2_Delivery') & '_comment')

Prompt::job:Address_Line2_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Address_Line2_Collection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line2_Collection',p_web.GetValue('NewValue'))
    job:Address_Line2_Collection = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line2_Collection
    do Value::job:Address_Line2_Collection
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line2_Collection',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line2_Collection = p_web.GetValue('Value')
  End
    job:Address_Line2_Collection = Upper(job:Address_Line2_Collection)
    p_web.SetSessionValue('job:Address_Line2_Collection',job:Address_Line2_Collection)
  do Value::job:Address_Line2_Collection
  do SendAlert

Value::job:Address_Line2_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2_Collection') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Address_Line2_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Address_Line2_Collection')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line2_Collection'',''amendaddress_job:address_line2_collection_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Address_Line2_Collection',p_web.GetSessionValueFormat('job:Address_Line2_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line2_Collection') & '_value')

Comment::job:Address_Line2_Collection  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line2_Collection') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Address_Line3_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Suburb')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line3_Delivery') & '_prompt')

Validate::job:Address_Line3_Delivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line3_Delivery',p_web.GetValue('NewValue'))
    job:Address_Line3_Delivery = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line3_Delivery
    do Value::job:Address_Line3_Delivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line3_Delivery',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line3_Delivery = p_web.GetValue('Value')
  End
    job:Address_Line3_Delivery = Upper(job:Address_Line3_Delivery)
    p_web.SetSessionValue('job:Address_Line3_Delivery',job:Address_Line3_Delivery)
      Access:SUBURB.ClearKey(sur:SuburbKey)
      sur:Suburb = p_web.GSV('job:Address_Line3_Delivery')
      IF (Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign)
          p_web.SSV('job:Postcode_Delivery',sur:postcode)
          p_web.SSV('jobe2:HubDelivery',sur:hub)
      END
  p_Web.SetValue('lookupfield','job:Address_Line3_Delivery')
  do AfterLookup
  do Value::job:Postcode_Delivery
  do Value::jobe2:HubDelivery
  do Value::job:Address_Line3_Delivery
  do SendAlert
  do Comment::job:Address_Line3_Delivery
  do Value::jobe2:HubDelivery  !1

Value::job:Address_Line3_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3_Delivery') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Address_Line3_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Address_Line3_Delivery')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line3_Delivery'',''amendaddress_job:address_line3_delivery_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Address_Line3_Delivery')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Address_Line3_Delivery',p_web.GetSessionValue('job:Address_Line3_Delivery'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('LookupSuburbs')&'?LookupField=job:Address_Line3_Delivery&Tab=4&ForeignField=sur:Suburb&_sort=&Refresh=sort&LookupFrom=AmendAddress&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line3_Delivery') & '_value')

Comment::job:Address_Line3_Delivery  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3_Delivery') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line3_Delivery') & '_comment')

Prompt::job:Address_Line3_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Suburb')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Address_Line3_Collection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Address_Line3_Collection',p_web.GetValue('NewValue'))
    job:Address_Line3_Collection = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Address_Line3_Collection
    do Value::job:Address_Line3_Collection
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Address_Line3_Collection',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Address_Line3_Collection = p_web.GetValue('Value')
  End
    job:Address_Line3_Collection = Upper(job:Address_Line3_Collection)
    p_web.SetSessionValue('job:Address_Line3_Collection',job:Address_Line3_Collection)
      Access:SUBURB.ClearKey(sur:SuburbKey)
      sur:Suburb = p_web.GSV('job:Address_Line3_Collection')
      IF (Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign)
          p_web.SSV('job:Postcode_Collection',sur:postcode)
          p_web.SSV('jobe2:HubCollection',sur:hub)
      END
  p_Web.SetValue('lookupfield','job:Address_Line3_Collection')
  do AfterLookup
  do Value::job:Postcode_Collection
  do Value::jobe2:HubCollection
  do Value::job:Address_Line3_Collection
  do SendAlert
  do Comment::job:Address_Line3_Collection
  do Value::jobe2:HubCollection  !1

Value::job:Address_Line3_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3_Collection') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Address_Line3_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Address_Line3_Collection')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Address_Line3_Collection'',''amendaddress_job:address_line3_collection_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Address_Line3_Collection')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','job:Address_Line3_Collection',p_web.GetSessionValue('job:Address_Line3_Collection'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s30',loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('LookupSuburbs')&'?LookupField=job:Address_Line3_Collection&Tab=4&ForeignField=sur:Suburb&_sort=&Refresh=sort&LookupFrom=AmendAddress&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line3_Collection') & '_value')

Comment::job:Address_Line3_Collection  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Address_Line3_Collection') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Address_Line3_Collection') & '_comment')

Prompt::jobe2:HubDelivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubDelivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Hub')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubDelivery') & '_prompt')

Validate::jobe2:HubDelivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:HubDelivery',p_web.GetValue('NewValue'))
    jobe2:HubDelivery = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:HubDelivery
    do Value::jobe2:HubDelivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:HubDelivery',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe2:HubDelivery = p_web.GetValue('Value')
  End
    jobe2:HubDelivery = Upper(jobe2:HubDelivery)
    p_web.SetSessionValue('jobe2:HubDelivery',jobe2:HubDelivery)
  do Value::jobe2:HubDelivery
  do SendAlert
  do Value::job:Postcode_Delivery  !1

Value::jobe2:HubDelivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubDelivery') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- jobe2:HubDelivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('jobe2:HubDelivery')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:HubDelivery'',''amendaddress_jobe2:hubdelivery_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:HubDelivery')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:HubDelivery',p_web.GetSessionValueFormat('jobe2:HubDelivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Hub') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubDelivery') & '_value')

Comment::jobe2:HubDelivery  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubDelivery') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubDelivery') & '_comment')

Prompt::jobe2:HubCollection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubCollection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Hub')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubCollection') & '_prompt')

Validate::jobe2:HubCollection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe2:HubCollection',p_web.GetValue('NewValue'))
    jobe2:HubCollection = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe2:HubCollection
    do Value::jobe2:HubCollection
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe2:HubCollection',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jobe2:HubCollection = p_web.GetValue('Value')
  End
    jobe2:HubCollection = Upper(jobe2:HubCollection)
    p_web.SetSessionValue('jobe2:HubCollection',jobe2:HubCollection)
  do Value::jobe2:HubCollection
  do SendAlert
  do Value::job:Postcode_Collection  !1

Value::jobe2:HubCollection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubCollection') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- jobe2:HubCollection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('jobe2:HubCollection')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe2:HubCollection'',''amendaddress_jobe2:hubcollection_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe2:HubCollection')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jobe2:HubCollection',p_web.GetSessionValueFormat('jobe2:HubCollection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Hub') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubCollection') & '_value')

Comment::jobe2:HubCollection  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe2:HubCollection') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe2:HubCollection') & '_comment')

Prompt::job:Postcode_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Postcode')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode_Delivery') & '_prompt')

Validate::job:Postcode_Delivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Postcode_Delivery',p_web.GetValue('NewValue'))
    job:Postcode_Delivery = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Postcode_Delivery
    do Value::job:Postcode_Delivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Postcode_Delivery',p_web.dFormat(p_web.GetValue('Value'),'@s10'))
    job:Postcode_Delivery = p_web.GetValue('Value')
  End
  do Value::job:Postcode_Delivery
  do SendAlert

Value::job:Postcode_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode_Delivery') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Postcode_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('job:Postcode_Delivery')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Postcode_Delivery'',''amendaddress_job:postcode_delivery_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Postcode_Delivery',p_web.GetSessionValueFormat('job:Postcode_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s10'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode_Delivery') & '_value')

Comment::job:Postcode_Delivery  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode_Delivery') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode_Delivery') & '_comment')

Prompt::job:Postcode_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Postcode')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode_Collection') & '_prompt')

Validate::job:Postcode_Collection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Postcode_Collection',p_web.GetValue('NewValue'))
    job:Postcode_Collection = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Postcode_Collection
    do Value::job:Postcode_Collection
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Postcode_Collection',p_web.dFormat(p_web.GetValue('Value'),'@s10'))
    job:Postcode_Collection = p_web.GetValue('Value')
  End
  do Value::job:Postcode_Collection
  do SendAlert

Value::job:Postcode_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode_Collection') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Postcode_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('job:Postcode_Collection')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Postcode_Collection'',''amendaddress_job:postcode_collection_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Postcode_Collection',p_web.GetSessionValueFormat('job:Postcode_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s10'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode_Collection') & '_value')

Comment::job:Postcode_Collection  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Postcode_Collection') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Postcode_Collection') & '_comment')

Prompt::job:Telephone_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Delivery') & '_prompt',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Telephone Number')
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Telephone_Delivery') & '_prompt')

Validate::job:Telephone_Delivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Telephone_Delivery',p_web.GetValue('NewValue'))
    job:Telephone_Delivery = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Telephone_Delivery
    do Value::job:Telephone_Delivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Telephone_Delivery',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Telephone_Delivery = p_web.GetValue('Value')
  End
    job:Telephone_Delivery = Upper(job:Telephone_Delivery)
    p_web.SetSessionValue('job:Telephone_Delivery',job:Telephone_Delivery)
      If PassMobileNumberFormat(p_web.GSV('job:Telephone_Delivery')) = 0
          p_web.SSV('job:Telephone_Delivery','')
          p_web.SSV('Comment:TelephoneNumberDelivery','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:TelephoneNumberDelivery','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::job:Telephone_Delivery
  do SendAlert
  do Comment::job:Telephone_Delivery

Value::job:Telephone_Delivery  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Delivery') & '_value',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:DeliveryAddress') = 1)
  ! --- STRING --- job:Telephone_Delivery
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:DeliveryAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Telephone_Delivery')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Telephone_Delivery'',''amendaddress_job:telephone_delivery_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Telephone_Delivery')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Telephone_Delivery',p_web.GetSessionValueFormat('job:Telephone_Delivery'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Telephone_Delivery') & '_value')

Comment::job:Telephone_Delivery  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:TelephoneNumberDelivery'))
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Delivery') & '_comment',Choose(p_web.GSV('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Telephone_Delivery') & '_comment')

Prompt::job:Telephone_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Collection') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Telephone Number')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Telephone_Collection  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Telephone_Collection',p_web.GetValue('NewValue'))
    job:Telephone_Collection = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Telephone_Collection
    do Value::job:Telephone_Collection
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Telephone_Collection',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Telephone_Collection = p_web.GetValue('Value')
  End
    job:Telephone_Collection = Upper(job:Telephone_Collection)
    p_web.SetSessionValue('job:Telephone_Collection',job:Telephone_Collection)
      If PassMobileNumberFormat(p_web.GSV('job:Telephone_Collection')) = 0
          p_web.SSV('job:Telephone_Collection','')
          p_web.SSV('Comment:TelephoneNumberCollection','Number Format/Length Incorrect.')
      Else ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
          p_web.SSV('Comment:TelephoneNumberCollection','')
      End ! If PassMobileNumberFormat(p_web.GSV('job:Mobile_Number')) = 0
  do Value::job:Telephone_Collection
  do SendAlert
  do Comment::job:Telephone_Collection

Value::job:Telephone_Collection  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Collection') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- STRING --- job:Telephone_Collection
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GetSessionValue('ReadOnly:CollectionAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('job:Telephone_Collection')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Telephone_Collection'',''amendaddress_job:telephone_collection_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('job:Telephone_Collection')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:Telephone_Collection',p_web.GetSessionValueFormat('job:Telephone_Collection'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Telephone_Collection') & '_value')

Comment::job:Telephone_Collection  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:TelephoneNumberCollection'))
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('job:Telephone_Collection') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('job:Telephone_Collection') & '_comment')

Validate::Button:CopyEndUserAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:CopyEndUserAddress',p_web.GetValue('NewValue'))
    do Value::Button:CopyEndUserAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  p_web.SSV('job:Company_Name_Delivery',p_web.GSV('job:Company_Name'))
  p_web.SSV('job:Address_Line1_Delivery',p_web.GSV('job:Address_Line1'))
  p_web.SSV('job:Address_Line2_Delivery',p_web.GSV('job:Address_Line2'))
  p_web.SSV('job:Address_Line3_Delivery',p_web.GSV('job:Address_Line3'))
  p_web.SSV('job:Postcode_Delivery',p_web.GSV('job:Postcode'))
  p_web.SSV('jobe2:HubDelivery',p_web.GSV('jobe2:HubCustomer'))
  p_web.SSV('job:Telephone_Delivery',p_web.GSV('job:Telephone_Number'))
  do Value::Button:CopyEndUserAddress
  do SendAlert
  do Value::job:Address_Line1_Delivery  !1
  do Value::job:Address_Line2_Delivery  !1
  do Value::job:Address_Line3_Delivery  !1
  do Value::job:Company_Name_Delivery  !1
  do Value::job:Telephone_Delivery  !1
  do Value::jobe2:HubDelivery  !1
  do Value::job:Postcode_Delivery  !1

Value::Button:CopyEndUserAddress  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('Button:CopyEndUserAddress') & '_value',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon(''&clip('CopyEndUserAddress')&'.disabled=true;sv(''Button:CopyEndUserAddress'',''amendaddress_button:copyenduseraddress_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CopyEndUserAddress','Copy / Clear Address','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ClearAddressDetails&AddType=D')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('Button:CopyEndUserAddress') & '_value')

Comment::Button:CopyEndUserAddress  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('Button:CopyEndUserAddress') & '_comment',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::Button:CopyEndUserAddress1  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('Button:CopyEndUserAddress1') & '_prompt',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::Button:CopyEndUserAddress1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:CopyEndUserAddress1',p_web.GetValue('NewValue'))
    do Value::Button:CopyEndUserAddress1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  p_web.SSV('job:Company_Name_Collection',p_web.GSV('job:Company_Name'))
  p_web.SSV('job:Address_Line1_Collection',p_web.GSV('job:Address_Line1'))
  p_web.SSV('job:Address_Line2_Collection',p_web.GSV('job:Address_Line2'))
  p_web.SSV('job:Address_Line3_Collection',p_web.GSV('job:Address_Line3'))
  p_web.SSV('job:Postcode_Collection',p_web.GSV('job:Postcode'))
  p_web.SSV('jobe2:HubCollection',p_web.GSV('jobe2:HubCustomer'))
  p_web.SSV('job:Telephone_Collection',p_web.GSV('job:Telephone_Number'))
  do Value::Button:CopyEndUserAddress1
  do SendAlert
  do Value::job:Address_Line1_Collection  !1
  do Value::job:Address_Line2_Collection  !1
  do Value::job:Address_Line3_Collection  !1
  do Value::job:Company_Name_Collection  !1
  do Value::job:Telephone_Collection  !1
  do Value::jobe2:HubCollection  !1
  do Value::job:Postcode_Collection  !1

Value::Button:CopyEndUserAddress1  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('Button:CopyEndUserAddress1') & '_value',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CollectionAddress') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon(''&clip('CopyEndUserAddress1')&'.disabled=true;sv(''Button:CopyEndUserAddress1'',''amendaddress_button:copyenduseraddress1_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CopyEndUserAddress1','Copy / Clear Address','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('ClearAddressDetails&AddType=C')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('Button:CopyEndUserAddress1') & '_value')

Comment::Button:CopyEndUserAddress1  Routine
    loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('Button:CopyEndUserAddress1') & '_comment',Choose(p_web.GSV('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jbn:Delivery_Text  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:Delivery_Text') & '_prompt',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Delivery Text')
  If p_web.GetSessionValue('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:Delivery_Text  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:Delivery_Text',p_web.GetValue('NewValue'))
    jbn:Delivery_Text = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:Delivery_Text
    do Value::jbn:Delivery_Text
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:Delivery_Text',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jbn:Delivery_Text = p_web.GetValue('Value')
  End
    jbn:Delivery_Text = Upper(jbn:Delivery_Text)
    p_web.SetSessionValue('jbn:Delivery_Text',jbn:Delivery_Text)
  do Value::jbn:Delivery_Text
  do SendAlert

Value::jbn:Delivery_Text  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:Delivery_Text') & '_value',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
  ! --- TEXT --- jbn:Delivery_Text
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jbn:Delivery_Text')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:Delivery_Text'',''amendaddress_jbn:delivery_text_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('jbn:Delivery_Text',p_web.GetSessionValue('jbn:Delivery_Text'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,255,,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jbn:Delivery_Text') & '_value')

Comment::jbn:Delivery_Text  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:Delivery_Text') & '_comment',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jbn:Collection_Text  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:Collection_Text') & '_prompt',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Collection Text')
  If p_web.GetSessionValue('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:Collection_Text  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:Collection_Text',p_web.GetValue('NewValue'))
    jbn:Collection_Text = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:Collection_Text
    do Value::jbn:Collection_Text
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:Collection_Text',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jbn:Collection_Text = p_web.GetValue('Value')
  End
    jbn:Collection_Text = Upper(jbn:Collection_Text)
    p_web.SetSessionValue('jbn:Collection_Text',jbn:Collection_Text)
  do Value::jbn:Collection_Text
  do SendAlert

Value::jbn:Collection_Text  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:Collection_Text') & '_value',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
  ! --- TEXT --- jbn:Collection_Text
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jbn:Collection_Text')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:Collection_Text'',''amendaddress_jbn:collection_text_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('jbn:Collection_Text',p_web.GetSessionValue('jbn:Collection_Text'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(jbn:Collection_Text),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jbn:Collection_Text') & '_value')

Comment::jbn:Collection_Text  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:Collection_Text') & '_comment',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jbn:DelContactName  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:DelContactName') & '_prompt',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Contact Name')
  If p_web.GetSessionValue('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:DelContactName  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:DelContactName',p_web.GetValue('NewValue'))
    jbn:DelContactName = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:DelContactName
    do Value::jbn:DelContactName
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:DelContactName',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jbn:DelContactName = p_web.GetValue('Value')
  End
    jbn:DelContactName = Upper(jbn:DelContactName)
    p_web.SetSessionValue('jbn:DelContactName',jbn:DelContactName)
  do Value::jbn:DelContactName
  do SendAlert

Value::jbn:DelContactName  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:DelContactName') & '_value',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
  ! --- STRING --- jbn:DelContactName
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jbn:DelContactName')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:DelContactName'',''amendaddress_jbn:delcontactname_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jbn:DelContactName',p_web.GetSessionValueFormat('jbn:DelContactName'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Contact Name') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jbn:DelContactName') & '_value')

Comment::jbn:DelContactName  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:DelContactName') & '_comment',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jbn:ColContatName  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:ColContatName') & '_prompt',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Contact Name')
  If p_web.GetSessionValue('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:ColContatName  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:ColContatName',p_web.GetValue('NewValue'))
    jbn:ColContatName = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:ColContatName
    do Value::jbn:ColContatName
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:ColContatName',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jbn:ColContatName = p_web.GetValue('Value')
  End
    jbn:ColContatName = Upper(jbn:ColContatName)
    p_web.SetSessionValue('jbn:ColContatName',jbn:ColContatName)
  do Value::jbn:ColContatName
  do SendAlert

Value::jbn:ColContatName  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:ColContatName') & '_value',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
  ! --- STRING --- jbn:ColContatName
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jbn:ColContatName')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:ColContatName'',''amendaddress_jbn:colcontatname_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jbn:ColContatName',p_web.GetSessionValueFormat('jbn:ColContatName'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Contact Name') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jbn:ColContatName') & '_value')

Comment::jbn:ColContatName  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:ColContatName') & '_comment',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jbn:DelDepartment  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:DelDepartment') & '_prompt',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Department')
  If p_web.GetSessionValue('Hide:DeliveryAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:DelDepartment  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:DelDepartment',p_web.GetValue('NewValue'))
    jbn:DelDepartment = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:DelDepartment
    do Value::jbn:DelDepartment
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:DelDepartment',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jbn:DelDepartment = p_web.GetValue('Value')
  End
    jbn:DelDepartment = Upper(jbn:DelDepartment)
    p_web.SetSessionValue('jbn:DelDepartment',jbn:DelDepartment)
  do Value::jbn:DelDepartment
  do SendAlert

Value::jbn:DelDepartment  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:DelDepartment') & '_value',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
  ! --- STRING --- jbn:DelDepartment
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jbn:DelDepartment')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:DelDepartment'',''amendaddress_jbn:deldepartment_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jbn:DelDepartment',p_web.GetSessionValueFormat('jbn:DelDepartment'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Department') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jbn:DelDepartment') & '_value')

Comment::jbn:DelDepartment  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:DelDepartment') & '_comment',Choose(p_web.GetSessionValue('Hide:DeliveryAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:DeliveryAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jbn:ColDepartment  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:ColDepartment') & '_prompt',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Department')
  If p_web.GetSessionValue('Hide:CollectionAddress') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:ColDepartment  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:ColDepartment',p_web.GetValue('NewValue'))
    jbn:ColDepartment = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:ColDepartment
    do Value::jbn:ColDepartment
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:ColDepartment',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    jbn:ColDepartment = p_web.GetValue('Value')
  End
    jbn:ColDepartment = Upper(jbn:ColDepartment)
    p_web.SetSessionValue('jbn:ColDepartment',jbn:ColDepartment)
  do Value::jbn:ColDepartment
  do SendAlert

Value::jbn:ColDepartment  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:ColDepartment') & '_value',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
  ! --- STRING --- jbn:ColDepartment
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jbn:ColDepartment')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn:ColDepartment'',''amendaddress_jbn:coldepartment_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','jbn:ColDepartment',p_web.GetSessionValueFormat('jbn:ColDepartment'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),'Department') & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jbn:ColDepartment') & '_value')

Comment::jbn:ColDepartment  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jbn:ColDepartment') & '_comment',Choose(p_web.GetSessionValue('Hide:CollectionAddress') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Hide:CollectionAddress') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::jobe:Sub_Sub_Account  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:Sub_Sub_Account') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jobe:Sub_Sub_Account  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jobe:Sub_Sub_Account',p_web.GetValue('NewValue'))
    jobe:Sub_Sub_Account = p_web.GetValue('NewValue') !FieldType= STRING Field = jobe:Sub_Sub_Account
    do Value::jobe:Sub_Sub_Account
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jobe:Sub_Sub_Account',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    jobe:Sub_Sub_Account = p_web.GetValue('Value')
  End
  p_Web.SetValue('lookupfield','jobe:Sub_Sub_Account')
  do AfterLookup
  do Value::job:Postcode_Delivery
  do Value::job:Address_Line1_Delivery
  do Value::job:Address_Line2_Delivery
  do Value::job:Address_Line3_Delivery
  do Value::job:Telephone_Delivery
  do Value::job:Company_Name_Delivery
  do Value::jobe:Sub_Sub_Account
  do SendAlert
  do Comment::jobe:Sub_Sub_Account

Value::jobe:Sub_Sub_Account  Routine
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:Sub_Sub_Account') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- jobe:Sub_Sub_Account
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('jobe:Sub_Sub_Account')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jobe:Sub_Sub_Account'',''amendaddress_jobe:sub_sub_account_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('jobe:Sub_Sub_Account')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','jobe:Sub_Sub_Account',p_web.GetSessionValue('jobe:Sub_Sub_Account'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@s15',loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseSubAddresses')&'?LookupField=jobe:Sub_Sub_Account&Tab=4&ForeignField=sua:AccountNumber&_sort=sua:CompanyName&Refresh=sort&LookupFrom=AmendAddress&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe:Sub_Sub_Account') & '_value')

Comment::jobe:Sub_Sub_Account  Routine
      loc:comment = ''
  p_web._DivHeader('AmendAddress_' & p_web._nocolon('jobe:Sub_Sub_Account') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('AmendAddress_' & p_web._nocolon('jobe:Sub_Sub_Account') & '_comment')

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('AmendAddress_job:Company_Name_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Company_Name
      else
        do Value::job:Company_Name
      end
  of lower('AmendAddress_job:Address_Line1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line1
      else
        do Value::job:Address_Line1
      end
  of lower('AmendAddress_job:Address_Line2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line2
      else
        do Value::job:Address_Line2
      end
  of lower('AmendAddress_job:Address_Line3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line3
      else
        do Value::job:Address_Line3
      end
  of lower('AmendAddress_jobe2:HubCustomer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:HubCustomer
      else
        do Value::jobe2:HubCustomer
      end
  of lower('AmendAddress_job:Postcode_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Postcode
      else
        do Value::job:Postcode
      end
  of lower('AmendAddress_job:Telephone_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Telephone_Number
      else
        do Value::job:Telephone_Number
      end
  of lower('AmendAddress_job:Fax_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Fax_Number
      else
        do Value::job:Fax_Number
      end
  of lower('AmendAddress_jobe:EndUserEmailAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:EndUserEmailAddress
      else
        do Value::jobe:EndUserEmailAddress
      end
  of lower('AmendAddress_jobe:EndUserTelNo_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:EndUserTelNo
      else
        do Value::jobe:EndUserTelNo
      end
  of lower('AmendAddress_jobe:VatNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:VatNumber
      else
        do Value::jobe:VatNumber
      end
  of lower('AmendAddress_jobe2:IDNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:IDNumber
      else
        do Value::jobe2:IDNumber
      end
  of lower('AmendAddress_jobe2:SMSNotification_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:SMSNotification
      else
        do Value::jobe2:SMSNotification
      end
  of lower('AmendAddress_jobe2:SMSAlertNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:SMSAlertNumber
      else
        do Value::jobe2:SMSAlertNumber
      end
  of lower('AmendAddress_jobe2:EmailNotification_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:EmailNotification
      else
        do Value::jobe2:EmailNotification
      end
  of lower('AmendAddress_jobe2:EmailAlertAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:EmailAlertAddress
      else
        do Value::jobe2:EmailAlertAddress
      end
  of lower('AmendAddress_job:Courier_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Courier
      else
        do Value::job:Courier
      end
  of lower('AmendAddress_jobe2:CourierWaybillNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:CourierWaybillNumber
      else
        do Value::jobe2:CourierWaybillNumber
      end
  of lower('AmendAddress_btnAmendC3D_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnAmendC3D
      else
        do Value::btnAmendC3D
      end
  of lower('AmendAddress_job:Company_Name_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Company_Name_Delivery
      else
        do Value::job:Company_Name_Delivery
      end
  of lower('AmendAddress_job:Company_Name_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Company_Name_Collection
      else
        do Value::job:Company_Name_Collection
      end
  of lower('AmendAddress_job:Address_Line1_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line1_Delivery
      else
        do Value::job:Address_Line1_Delivery
      end
  of lower('AmendAddress_job:Address_Line1_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line1_Collection
      else
        do Value::job:Address_Line1_Collection
      end
  of lower('AmendAddress_job:Address_Line2_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line2_Delivery
      else
        do Value::job:Address_Line2_Delivery
      end
  of lower('AmendAddress_job:Address_Line2_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line2_Collection
      else
        do Value::job:Address_Line2_Collection
      end
  of lower('AmendAddress_job:Address_Line3_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line3_Delivery
      else
        do Value::job:Address_Line3_Delivery
      end
  of lower('AmendAddress_job:Address_Line3_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Address_Line3_Collection
      else
        do Value::job:Address_Line3_Collection
      end
  of lower('AmendAddress_jobe2:HubDelivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:HubDelivery
      else
        do Value::jobe2:HubDelivery
      end
  of lower('AmendAddress_jobe2:HubCollection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe2:HubCollection
      else
        do Value::jobe2:HubCollection
      end
  of lower('AmendAddress_job:Postcode_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Postcode_Delivery
      else
        do Value::job:Postcode_Delivery
      end
  of lower('AmendAddress_job:Postcode_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Postcode_Collection
      else
        do Value::job:Postcode_Collection
      end
  of lower('AmendAddress_job:Telephone_Delivery_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Telephone_Delivery
      else
        do Value::job:Telephone_Delivery
      end
  of lower('AmendAddress_job:Telephone_Collection_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Telephone_Collection
      else
        do Value::job:Telephone_Collection
      end
  of lower('AmendAddress_Button:CopyEndUserAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::Button:CopyEndUserAddress
      else
        do Value::Button:CopyEndUserAddress
      end
  of lower('AmendAddress_Button:CopyEndUserAddress1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::Button:CopyEndUserAddress1
      else
        do Value::Button:CopyEndUserAddress1
      end
  of lower('AmendAddress_jbn:Delivery_Text_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:Delivery_Text
      else
        do Value::jbn:Delivery_Text
      end
  of lower('AmendAddress_jbn:Collection_Text_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:Collection_Text
      else
        do Value::jbn:Collection_Text
      end
  of lower('AmendAddress_jbn:DelContactName_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:DelContactName
      else
        do Value::jbn:DelContactName
      end
  of lower('AmendAddress_jbn:ColContatName_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:ColContatName
      else
        do Value::jbn:ColContatName
      end
  of lower('AmendAddress_jbn:DelDepartment_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:DelDepartment
      else
        do Value::jbn:DelDepartment
      end
  of lower('AmendAddress_jbn:ColDepartment_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn:ColDepartment
      else
        do Value::jbn:ColDepartment
      end
  of lower('AmendAddress_jobe:Sub_Sub_Account_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jobe:Sub_Sub_Account
      else
        do Value::jobe:Sub_Sub_Account
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('AmendAddress_form:ready_',1)
  p_web.SetSessionValue('AmendAddress_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_AmendAddress',0)

PreCopy  Routine
  p_web.SetValue('AmendAddress_form:ready_',1)
  p_web.SetSessionValue('AmendAddress_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_AmendAddress',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('AmendAddress_form:ready_',1)
  p_web.SetSessionValue('AmendAddress_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('AmendAddress:Primed',0)

PreDelete       Routine
  p_web.SetValue('AmendAddress_form:ready_',1)
  p_web.SetSessionValue('AmendAddress_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('AmendAddress:Primed',0)
  p_web.setsessionvalue('showtab_AmendAddress',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('FromURL') <> 'CustomerServicesForm'
  End
          If p_web.IfExistsValue('jobe2:SMSNotification') = 0
            p_web.SetValue('jobe2:SMSNotification',0)
            jobe2:SMSNotification = 0
          End
          If p_web.IfExistsValue('jobe2:EmailNotification') = 0
            p_web.SetValue('jobe2:EmailNotification',0)
            jobe2:EmailNotification = 0
          End
  If p_web.GSV('Hide:AmendC3DButton') = 0
  End
  If p_web.GSV('Hide:SubSubAccount') <> 1
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('AmendAddress_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('AmendAddress_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 5
  If p_web.GSV('FromURL') <> 'CustomerServicesForm'
    loc:InvalidTab += 1
  End
  ! tab = 1
    loc:InvalidTab += 1
          job:Company_Name = Upper(job:Company_Name)
          p_web.SetSessionValue('job:Company_Name',job:Company_Name)
        If loc:Invalid <> '' then exit.
          job:Address_Line1 = Upper(job:Address_Line1)
          p_web.SetSessionValue('job:Address_Line1',job:Address_Line1)
        If loc:Invalid <> '' then exit.
          job:Address_Line2 = Upper(job:Address_Line2)
          p_web.SetSessionValue('job:Address_Line2',job:Address_Line2)
        If loc:Invalid <> '' then exit.
          job:Address_Line3 = Upper(job:Address_Line3)
          p_web.SetSessionValue('job:Address_Line3',job:Address_Line3)
        If loc:Invalid <> '' then exit.
          jobe2:HubCustomer = Upper(jobe2:HubCustomer)
          p_web.SetSessionValue('jobe2:HubCustomer',jobe2:HubCustomer)
        If loc:Invalid <> '' then exit.
          job:Telephone_Number = Upper(job:Telephone_Number)
          p_web.SetSessionValue('job:Telephone_Number',job:Telephone_Number)
        If loc:Invalid <> '' then exit.
          job:Fax_Number = Upper(job:Fax_Number)
          p_web.SetSessionValue('job:Fax_Number',job:Fax_Number)
        If loc:Invalid <> '' then exit.
          jobe:EndUserTelNo = Upper(jobe:EndUserTelNo)
          p_web.SetSessionValue('jobe:EndUserTelNo',jobe:EndUserTelNo)
        If loc:Invalid <> '' then exit.
    If p_web.GSV('Hide:VatNumber') <> 1
          jobe:VatNumber = Upper(jobe:VatNumber)
          p_web.SetSessionValue('jobe:VatNumber',jobe:VatNumber)
        If loc:Invalid <> '' then exit.
    End
          jobe2:IDNumber = Upper(jobe2:IDNumber)
          p_web.SetSessionValue('jobe2:IDNumber',jobe2:IDNumber)
        If loc:Invalid <> '' then exit.
      If not (p_web.GSV('jobe2:SMSNotification') = 0)
          jobe2:SMSAlertNumber = Upper(jobe2:SMSAlertNumber)
          p_web.SetSessionValue('jobe2:SMSAlertNumber',jobe2:SMSAlertNumber)
        If loc:Invalid <> '' then exit.
      End
        If job:Courier = '' and ForceCourier('B')
          loc:Invalid = 'job:Courier'
          loc:alert = p_web.translate('Outgoing Courier') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
          jobe2:CourierWaybillNumber = Upper(jobe2:CourierWaybillNumber)
          p_web.SetSessionValue('jobe2:CourierWaybillNumber',jobe2:CourierWaybillNumber)
        If loc:Invalid <> '' then exit.
  ! tab = 6
  If p_web.GSV('Hide:AmendC3DButton') = 0
    loc:InvalidTab += 1
  End
  ! tab = 2
    loc:InvalidTab += 1
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
          job:Company_Name_Delivery = Upper(job:Company_Name_Delivery)
          p_web.SetSessionValue('job:Company_Name_Delivery',job:Company_Name_Delivery)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
          job:Address_Line1_Delivery = Upper(job:Address_Line1_Delivery)
          p_web.SetSessionValue('job:Address_Line1_Delivery',job:Address_Line1_Delivery)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
          job:Address_Line1_Collection = Upper(job:Address_Line1_Collection)
          p_web.SetSessionValue('job:Address_Line1_Collection',job:Address_Line1_Collection)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
          job:Address_Line2_Delivery = Upper(job:Address_Line2_Delivery)
          p_web.SetSessionValue('job:Address_Line2_Delivery',job:Address_Line2_Delivery)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
          job:Address_Line2_Collection = Upper(job:Address_Line2_Collection)
          p_web.SetSessionValue('job:Address_Line2_Collection',job:Address_Line2_Collection)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
          job:Address_Line3_Delivery = Upper(job:Address_Line3_Delivery)
          p_web.SetSessionValue('job:Address_Line3_Delivery',job:Address_Line3_Delivery)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
          job:Address_Line3_Collection = Upper(job:Address_Line3_Collection)
          p_web.SetSessionValue('job:Address_Line3_Collection',job:Address_Line3_Collection)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
          jobe2:HubDelivery = Upper(jobe2:HubDelivery)
          p_web.SetSessionValue('jobe2:HubDelivery',jobe2:HubDelivery)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
          jobe2:HubCollection = Upper(jobe2:HubCollection)
          p_web.SetSessionValue('jobe2:HubCollection',jobe2:HubCollection)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:DeliveryAddress') = 1)
          job:Telephone_Delivery = Upper(job:Telephone_Delivery)
          p_web.SetSessionValue('job:Telephone_Delivery',job:Telephone_Delivery)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:CollectionAddress') = 1)
          job:Telephone_Collection = Upper(job:Telephone_Collection)
          p_web.SetSessionValue('job:Telephone_Collection',job:Telephone_Collection)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
          jbn:Delivery_Text = Upper(jbn:Delivery_Text)
          p_web.SetSessionValue('jbn:Delivery_Text',jbn:Delivery_Text)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
          jbn:Collection_Text = Upper(jbn:Collection_Text)
          p_web.SetSessionValue('jbn:Collection_Text',jbn:Collection_Text)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
          jbn:DelContactName = Upper(jbn:DelContactName)
          p_web.SetSessionValue('jbn:DelContactName',jbn:DelContactName)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
          jbn:ColContatName = Upper(jbn:ColContatName)
          p_web.SetSessionValue('jbn:ColContatName',jbn:ColContatName)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('Hide:DeliveryAddress') = 1)
          jbn:DelDepartment = Upper(jbn:DelDepartment)
          p_web.SetSessionValue('jbn:DelDepartment',jbn:DelDepartment)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('Hide:CollectionAddress') = 1)
          jbn:ColDepartment = Upper(jbn:ColDepartment)
          p_web.SetSessionValue('jbn:ColDepartment',jbn:ColDepartment)
        If loc:Invalid <> '' then exit.
      End
  ! tab = 4
  If p_web.GSV('Hide:SubSubAccount') <> 1
    loc:InvalidTab += 1
  End
  ! The following fields are not on the form, but need to be checked anyway.
  !Validation
  !p_web.SetSessionValue('job:Company_Name',p_web.GetSessionValue('tmp:CompanyName'))
  !p_web.SetSessionValue('job:Address_Line1',p_web.GetSessionValue('tmp:AddressLine1'))
  !p_web.SetSessionValue('job:Address_Line2',p_web.GetSessionValue('tmp:AddressLine2'))
  !p_web.SetSessionValue('job:Address_Line3',p_web.GetSessionValue('tmp:Suburb'))
  !p_web.SetSessionValue('jobe2:HubCustomer',p_web.GetSessionValue('tmp:HubCustomer'))
  !p_web.SetSessionValue('job:Telephone_Number',p_web.GetSessionValue('tmp:TelephoneNumber'))
  !p_web.SetSessionValue('job:Fax_Number',p_web.GetSessionValue('tmp:FaxNumber'))
  !p_web.SetSessionValue('jobe:EndUserEmailAddress',p_web.GetSessionValue('tmp:EmailAddress'))
  !p_web.SetSessionValue('jobe:EndUserTelNo',p_web.GetSessionValue('tmp:EndUserTelephoneNumber'))
  !p_web.SetSessionValue('jobe2:IDNumber',p_web.GetSessionValue('tmp:IDNumber'))
  !p_web.SetSessionValue('jobe2:SMSNotification',p_web.GetSessionValue('tmp:SMSNotification'))
  !p_web.SetSessionValue('jobe2:SMSAlertNumber',p_web.GetSessionValue('tmp:NotificationMobileNumber'))
  !p_web.SetSessionValue('jobe2:EmailNotification',p_web.GetSessionValue('tmp:EmailNotification'))
  !p_web.SetSessionValue('jobe2:EmailAlertAddress',p_web.GetSessionValue('tmp:NotificationEmailAddress'))
  !
  !p_web.SetSessionValue('job:Company_Name_Delivery',p_web.GetSessionValue('tmp:CompanyNameDelivery'))
  !p_web.SetSessionValue('job:Company_Name_Collection',p_web.GetSessionValue('tmp:CompanyNameCollection'))
  !p_web.SetSessionValue('job:Address_Line1_Delivery',p_web.GetSessionValue('tmp:AddressLine1Delivery'))
  !p_web.SetSessionValue('job:Address_Line1_Collection',p_web.GetSessionValue('tmp:AddressLine1Collection'))
  !p_web.SetSessionValue('job:Address_Line3_Delivery',p_web.GetSessionValue('tmp:SuburbDelivery'))
  !p_web.SetSessionValue('job:Address_Line3_Collection',p_web.GetSessionValue('tmp:SuburbCollection'))
  !p_web.SetSessionValue('jobe2:HubDelivery',p_web.GetSessionValue('tmp:HubDelivery'))
  !p_web.SetSessionValue('jobe2:HubCollection',p_web.GetSessionValue('tmp:HubCollection'))
  !p_web.SetSessionValue('job:Telephone_Delivery',p_web.GetSessionValue('tmp:TelephoneNumberDelivery'))
  !p_web.SetSessionValue('job:Telephone_Collection',p_web.GetSessionValue('tmp:TelephoneNumberCollection'))
  !
  !
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('AmendAddress:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('job:Company_Name')
  p_web.StoreValue('job:Address_Line1')
  p_web.StoreValue('job:Address_Line2')
  p_web.StoreValue('job:Address_Line3')
  p_web.StoreValue('job:Postcode')
  p_web.StoreValue('job:Telephone_Number')
  p_web.StoreValue('job:Fax_Number')
  p_web.StoreValue('jobe:EndUserEmailAddress')
  p_web.StoreValue('jobe:EndUserTelNo')
  p_web.StoreValue('jobe:VatNumber')
  p_web.StoreValue('job:Courier')
  p_web.StoreValue('jobe3:LoyaltyStatus')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('job:Company_Name_Delivery')
  p_web.StoreValue('job:Company_Name_Collection')
  p_web.StoreValue('job:Address_Line1_Delivery')
  p_web.StoreValue('job:Address_Line1_Collection')
  p_web.StoreValue('job:Address_Line2_Delivery')
  p_web.StoreValue('job:Address_Line2_Collection')
  p_web.StoreValue('job:Address_Line3_Delivery')
  p_web.StoreValue('job:Address_Line3_Collection')
  p_web.StoreValue('job:Postcode_Delivery')
  p_web.StoreValue('job:Postcode_Collection')
  p_web.StoreValue('job:Telephone_Delivery')
  p_web.StoreValue('job:Telephone_Collection')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('jbn:Delivery_Text')
  p_web.StoreValue('jbn:Collection_Text')
  p_web.StoreValue('jbn:DelContactName')
  p_web.StoreValue('jbn:ColContatName')
  p_web.StoreValue('jbn:DelDepartment')
  p_web.StoreValue('jbn:ColDepartment')
  p_web.StoreValue('jobe:Sub_Sub_Account')
