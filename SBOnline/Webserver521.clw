

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER521.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! Validate Accessories (uses TaggingFile)
!!! </summary>
ValidateAccessories  PROCEDURE  (NetwebserverWorker p_web, STRING pType, LONG pRefNumber)!,LONG ! Declare Procedure
RetValue    LONG(0)
FilesOpened     BYTE(0)

  CODE
    !region ProcessedCode
        DO OpenFiles
        
        CASE pType
        OF 'LOA'
            Access:LOANACC.ClearKey(lac:Ref_Number_Key)
            lac:Ref_Number = pRefNumber
            SET(lac:Ref_Number_Key,lac:Ref_Number_Key)
            LOOP UNTIL Access:LOANACC.Next() <> Level:Benign
                IF (lac:Ref_Number <> pRefNumber)
                    BREAK
                END ! IF

                Access:Tagging.ClearKey(tgg:SessionIDKey)
                tgg:SessionID = p_web.SessionID
                tgg:TaggedValue = lac:Accessory
                IF (Access:Tagging.TryFetch(tgg:SessionIDKey) = Level:Benign)
                    IF (tgg:Tagged <> 1)        
                        RetValue = 1
                        BREAK
                    END ! IF (tgg:Tagged <> 1  )        
                ELSE ! IF (Access:Tagging.TryFetch(tgg:SessionIDKey) = Level:Benign)
                    RetValue = 1
                    BREAK
                END ! IF (Access:Tagging.TryFetch(tgg:SessionIDKey) = Level:Benign)
            END ! LOOP
            IF (RetValue = 0)
                Access:Tagging.ClearKey(tgg:SessionIDKey)
                tgg:SessionID = p_web.SessionID
                SET(tgg:SessionIDKey,tgg:SessionIDKey)
                LOOP UNTIL Access:Tagging.Next() <> Level:Benign
                    IF (tgg:SessionID <> p_web.SessionID)
                        BREAK
                    END ! IF

                    IF (tgg:Tagged = 0)
                        CYCLE
                    END ! IF        
                    Access:LOANACC.ClearKey(lac:Ref_Number_Key)
                    lac:Ref_Number = pRefNumber 
                    lac:Accessory = tgg:TaggedValue
                    IF (Access:LOANACC.TryFetch(lac:Ref_Number_Key) = Level:Benign)
        
                    ELSE ! IF (Access:LOANACC.TryFetch(lac:Ref_Number_Key) = Level:Benign)
                        RetValue = 1
                        BREAK
                    END ! IF (Access:LOANACC.TryFetch(lac:Ref_Number_Key) = Level:Benign)
                END ! LOOP
            END ! IF (RetValue = 0)

        ELSE ! 
            Access:JOBACC.ClearKey(jac:Ref_Number_Key)
            jac:Ref_Number = pRefNumber
            SET(jac:Ref_Number_Key,jac:Ref_Number_Key)
            LOOP UNTIL Access:JOBACC.Next() <> Level:Benign
                IF (jac:Ref_Number <> pRefNumber)
                    BREAK
                END ! IF
                IF (p_web.GSV('BookingSite') <> 'RRC')
                    IF (~jac:Attached)
                        CYCLE
                    END ! IF
                END ! IF
                
                Access:Tagging.ClearKey(tgg:SessionIDKey)
                tgg:SessionID = p_web.SessionID
                tgg:TaggedValue = jac:Accessory
                IF (Access:Tagging.TryFetch(tgg:SessionIDKey) = Level:Benign)
                    IF (tgg:Tagged <> 1)
                        RetValue = 1
                        BREAK
                    ELSE
                        IF (p_web.GSV('BookingSite') <> 'RRC' AND ~jac:Attached)
                            RetValue = 1
                            BREAK
                        END ! IF
                    END ! IF
                ELSE ! IF
                    RetValue = 1
                    BREAK
                END ! IF
            END ! LOOP
            
            IF (RetValue = 0)
                Access:Tagging.ClearKey(tgg:SessionIDKey)
                tgg:SessionID = p_web.SessionID
                SET(tgg:SessionIDKey,tgg:SessionIDKey)
                LOOP UNTIL Access:Tagging.Next() <> Level:Benign
                    IF (tgg:SessionID <> p_web.SessionID)
                        BREAK
                    END ! IF
                    IF (tgg:Tagged = 0)
                        CYCLE
                    END ! IF
                    Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                    jac:Ref_Number = pRefNumber
                    jac:Accessory = tgg:TaggedValue
                    IF (Access:JOBACC.TryFetch(jac:Ref_Number_Key) = Level:Benign)
                        
                    ELSE ! IF
                        RetValue = 2
                        BREAk
                    END ! IF
                END ! LOOP
            END ! IF
        END ! CASE
        
        DO CloseFiles
        
        RETURN RetValue
        
        ! 1 = Missing
        ! 2 = Mismatch
        ! Else = All Fine
    
    !endregion    
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBACC.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBACC.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:Tagging.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:Tagging.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:LOANACC.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOANACC.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBACC.Close
     Access:Tagging.Close
     Access:LOANACC.Close
     FilesOpened = False
  END
