

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER425.INC'),ONCE        !Local module procedure declarations
                     END


UJS_BrowseContactHistory PROCEDURE  (NetWebServerWorker p_web)
SentRefNumber        LONG                                  !
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(CONTHIST)
                      Project(cht:Ref_Number)
                      Project(cht:Date)
                      Project(cht:Time)
                      Project(cht:Action)
                      Project(cht:Ref_Number)
                      Project(cht:Date)
                      Project(cht:Time)
                      Project(cht:User)
                      Project(cht:Action)
                      Project(cht:Notes)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
  CODE
    SentRefNumber = p_web.GetSessionValue('NewJobRef')
    p_web.SetSessionValue('SentRefNumber',SentRefNumber)
  GlobalErrors.SetProcedureName('UJS_BrowseContactHistory')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('UJS_BrowseContactHistory:NoForm')
      loc:NoForm = p_web.GetValue('UJS_BrowseContactHistory:NoForm')
      loc:FormName = p_web.GetValue('UJS_BrowseContactHistory:FormName')
    else
      loc:FormName = 'UJS_BrowseContactHistory_frm'
    End
    p_web.SSV('UJS_BrowseContactHistory:NoForm',loc:NoForm)
    p_web.SSV('UJS_BrowseContactHistory:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('UJS_BrowseContactHistory:NoForm')
    loc:FormName = p_web.GSV('UJS_BrowseContactHistory:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('UJS_BrowseContactHistory') & '_' & lower(loc:parent)
  else
    loc:divname = lower('UJS_BrowseContactHistory')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('adiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(CONTHIST,cht:Ref_Number_Key,loc:vorder)
    If False
    ElsIf (loc:vorder = 'CHT:REF_NUMBER') then p_web.SetValue('UJS_BrowseContactHistory_sort','1')
    ElsIf (loc:vorder = 'CHT:DATE') then p_web.SetValue('UJS_BrowseContactHistory_sort','2')
    ElsIf (loc:vorder = 'CHT:TIME') then p_web.SetValue('UJS_BrowseContactHistory_sort','3')
    ElsIf (loc:vorder = 'CHT:USER') then p_web.SetValue('UJS_BrowseContactHistory_sort','4')
    ElsIf (loc:vorder = 'CHT:ACTION') then p_web.SetValue('UJS_BrowseContactHistory_sort','5')
    ElsIf (loc:vorder = 'CHT:NOTES') then p_web.SetValue('UJS_BrowseContactHistory_sort','6')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('UJS_BrowseContactHistory:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('UJS_BrowseContactHistory:LookupFrom','LookupFrom')
    p_web.StoreValue('UJS_BrowseContactHistory:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('UJS_BrowseContactHistory:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('UJS_BrowseContactHistory:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:PageLoad
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:None
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  ! Save Window Name
  AddToLog('NetWebBrowse',p_web.RequestData.DataString,'UJS_BrowseContactHistory',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 15
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('UJS_BrowseContactHistory_sort',net:DontEvaluate)
  p_web.SetSessionValue('UJS_BrowseContactHistory_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'cht:Ref_Number','-cht:Ref_Number')
    Loc:LocateField = 'cht:Ref_Number'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'cht:Date','-cht:Date')
    Loc:LocateField = 'cht:Date'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'cht:Time','-cht:Time')
    Loc:LocateField = 'cht:Time'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(cht:User)','-UPPER(cht:User)')
    Loc:LocateField = 'cht:User'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(cht:Action)','-UPPER(cht:Action)')
    Loc:LocateField = 'cht:Action'
  of 6
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(cht:Notes)','-UPPER(cht:Notes)')
    Loc:LocateField = 'cht:Notes'
  end
  if loc:vorder = ''
    loc:vorder = '+cht:Ref_Number,-cht:Date,-cht:Time,+UPPER(cht:Action)'
  end
  If False ! add range fields to sort order
  Else
    If Instring('CHT:REF_NUMBER',upper(loc:vOrder),1,1) = 0
      loc:vOrder = 'cht:Ref_Number,' & loc:vorder
    End
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('cht:Ref_Number')
    loc:SortHeader = p_web.Translate('Job Number')
    p_web.SetSessionValue('UJS_BrowseContactHistory_LocatorPic','@s8')
  Of upper('cht:Date')
    loc:SortHeader = p_web.Translate('Date')
    p_web.SetSessionValue('UJS_BrowseContactHistory_LocatorPic','@d6b')
  Of upper('cht:Time')
    loc:SortHeader = p_web.Translate('Time')
    p_web.SetSessionValue('UJS_BrowseContactHistory_LocatorPic','@t1')
  Of upper('cht:User')
    loc:SortHeader = p_web.Translate('User')
    p_web.SetSessionValue('UJS_BrowseContactHistory_LocatorPic','@s3')
  Of upper('cht:Action')
    loc:SortHeader = p_web.Translate('Action')
    p_web.SetSessionValue('UJS_BrowseContactHistory_LocatorPic','@s80')
  Of upper('cht:Notes')
    loc:SortHeader = p_web.Translate('Notes')
    p_web.SetSessionValue('UJS_BrowseContactHistory_LocatorPic','@s255')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('UJS_BrowseContactHistory:LookupFrom')
  End!Else
  loc:CloseAction = 'UpdateJobsStatus'
  loc:formaction = 'UJS_BrowseContactHistory'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="UJS_BrowseContactHistory:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="UJS_BrowseContactHistory:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('UJS_BrowseContactHistory:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="CONTHIST"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="cht:Ref_Number_Key"></input><13,10>'
  end
  If p_web.Translate('Contact History') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Contact History',0)&'</span>'&CRLF
  End
  If clip('Contact History') <> ''
    !packet = clip(packet) & p_web.br !Bryan. Why is this here?
    packet = clip(packet) & p_web.br
  End
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'UJS_BrowseContactHistory',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2UJS_BrowseContactHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="UJS_BrowseContactHistory.locate(''Locator2UJS_BrowseContactHistory'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'UJS_BrowseContactHistory.cl(''UJS_BrowseContactHistory'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="UJS_BrowseContactHistory_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="UJS_BrowseContactHistory_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','UJS_BrowseContactHistory','Job Number','Click here to sort by Job Number',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Job Number')&'">'&p_web.Translate('Job Number')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','UJS_BrowseContactHistory','Date','Click here to sort by Date',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Date')&'">'&p_web.Translate('Date')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','UJS_BrowseContactHistory','Time','Click here to sort by Time',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Time')&'">'&p_web.Translate('Time')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','UJS_BrowseContactHistory','User','Click here to sort by User',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by User')&'">'&p_web.Translate('User')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','UJS_BrowseContactHistory','Action','Click here to sort by Action',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Action')&'">'&p_web.Translate('Action')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'6','UJS_BrowseContactHistory','Notes','Click here to sort by Notes',,,,1)
        Else
          packet = clip(packet) & '<th Title="'&p_web.Translate('Click here to sort by Notes')&'">'&p_web.Translate('Notes')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'cht:Date' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,15,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('cht:ref_number',lower(Thisview{prop:order}),1,1) = 0 !and CONTHIST{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'cht:Ref_Number'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('cht:Ref_Number'),p_web.GetValue('cht:Ref_Number'),p_web.GetSessionValue('cht:Ref_Number'))
  If Instring('cht:date',lower(Thisview{prop:order}),1,1) = 0 !and CONTHIST{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'cht:Date'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('cht:Date'),p_web.GetValue('cht:Date'),p_web.GetSessionValue('cht:Date'))
  If Instring('cht:time',lower(Thisview{prop:order}),1,1) = 0 !and CONTHIST{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'cht:Time'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('cht:Time'),p_web.GetValue('cht:Time'),p_web.GetSessionValue('cht:Time'))
  If Instring('cht:action',lower(Thisview{prop:order}),1,1) = 0 !and CONTHIST{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'cht:Action'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('cht:Action'),p_web.GetValue('cht:Action'),p_web.GetSessionValue('cht:Action'))
    SentRefNumber = p_web.RestoreValue('SentRefNumber')
    loc:FilterWas = 'cht:Ref_Number = ' & SentRefNumber
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'UJS_BrowseContactHistory',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('UJS_BrowseContactHistory_Filter')
    p_web.SetSessionValue('UJS_BrowseContactHistory_FirstValue','')
    p_web.SetSessionValue('UJS_BrowseContactHistory_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,CONTHIST,cht:Ref_Number_Key,loc:PageRows,'UJS_BrowseContactHistory',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If CONTHIST{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(CONTHIST,loc:firstvalue)
              Reset(ThisView,CONTHIST)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If CONTHIST{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(CONTHIST,loc:lastvalue)
            Reset(ThisView,CONTHIST)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(cht:Action)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'UJS_BrowseContactHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'UJS_BrowseContactHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'UJS_BrowseContactHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'UJS_BrowseContactHistory.last();',,loc:nextdisabled)
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'UJS_BrowseContactHistory',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('UJS_BrowseContactHistory_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('UJS_BrowseContactHistory_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1UJS_BrowseContactHistory',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="UJS_BrowseContactHistory.locate(''Locator1UJS_BrowseContactHistory'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'UJS_BrowseContactHistory.cl(''UJS_BrowseContactHistory'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('UJS_BrowseContactHistory_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('UJS_BrowseContactHistory_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'UJS_BrowseContactHistory.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'UJS_BrowseContactHistory.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'UJS_BrowseContactHistory.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'UJS_BrowseContactHistory.last();',,loc:nextdisabled)
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If loc:selecting = 0 and loc:parent = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:Formname,loc:CloseAction)
      do SendPacket
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    loc:field = cht:Action
    p_web._thisrow = p_web._nocolon('cht:Action')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('UJS_BrowseContactHistory:LookupField')) = cht:Action and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((cht:Action = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="UJS_BrowseContactHistory.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If CONTHIST{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(CONTHIST)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If CONTHIST{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(CONTHIST)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','cht:Action',clip(loc:field),,loc:checked,,,'onclick="UJS_BrowseContactHistory.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','cht:Action',clip(loc:field),,'checked',,,'onclick="UJS_BrowseContactHistory.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::cht:Ref_Number
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::cht:Date
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::cht:Time
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::cht:User
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::cht:Action
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td>'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::cht:Notes
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="UJS_BrowseContactHistory.omv(this);" onMouseOut="UJS_BrowseContactHistory.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var UJS_BrowseContactHistory=new browseTable(''UJS_BrowseContactHistory'','''&clip(loc:formname)&''','''&p_web._jsok('cht:Ref_Number',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',1,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('cht:Ref_Number')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'UJS_BrowseContactHistory.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'UJS_BrowseContactHistory.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2UJS_BrowseContactHistory')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1UJS_BrowseContactHistory')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1UJS_BrowseContactHistory')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2UJS_BrowseContactHistory')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(CONTHIST)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(CONTHIST)
  Bind(cht:Record)
  Clear(cht:Record)
  NetWebSetSessionPics(p_web,CONTHIST)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('cht:Ref_Number',loc:default)
    p_web.SetValue('cht:Date',loc:default)
    p_web.SetValue('cht:Time',loc:default)
    p_web.SetValue('cht:Action',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
      ! Save Window Name
      IF (loc:alert <> '')
          AddToLog('Alert',loc:alert,'UJS_BrowseContactHistory',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
      END ! IF
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::cht:Ref_Number   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('cht:Ref_Number_'&cht:Action,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(cht:Ref_Number,'@s8')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::cht:Date   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('cht:Date_'&cht:Action,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(cht:Date,'@d6b')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::cht:Time   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('cht:Time_'&cht:Action,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(cht:Time,'@t1')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::cht:User   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('cht:User_'&cht:Action,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(cht:User,'@s3')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::cht:Action   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('cht:Action_'&cht:Action,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(cht:Action,'@s80')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::cht:Notes   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('cht:Notes_'&cht:Action,,net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(cht:Notes,'@s255')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = cht:Ref_Number
  loc:default = cht:Date
  loc:default = cht:Time
  loc:default = cht:Action

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('cht:Ref_Number',cht:Ref_Number)
  p_web.SetSessionValue('cht:Date',cht:Date)
  p_web.SetSessionValue('cht:Time',cht:Time)
  p_web.SetSessionValue('cht:Action',cht:Action)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('cht:Ref_Number',loc:default)
    p_web.SetSessionValue('cht:Date',loc:default)
    p_web.SetSessionValue('cht:Time',loc:default)
    p_web.SetSessionValue('cht:Action',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('cht:Ref_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('cht:Ref_Number'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
