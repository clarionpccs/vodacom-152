

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER107.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
CheckLength          PROCEDURE  (STRING pType,STRING pModelNumber,STRING pNumber) ! Declare Procedure
tmp:LengthFrom       LONG                                  !Length From
tmp:LengthTo         LONG                                  !Length To
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
MODELNUM::State  USHORT
FilesOpened     BYTE(0)

  CODE
        IF (pType = 'MOBILE')
            tmp:LengthFrom = GETINI('COMPULSORY','MobileLengthFrom',,Clip(Path()) & '\SB2KDEF.INI')
            tmp:LengthTo   = GETINI('COMPULSORY','MobileLengthTo',,Clip(Path()) & '\SB2KDEF.INI')
            IF Len(Clip(pNumber)) < tmp:LengthFrom Or |
                Len(Clip(pNumber)) > tmp:LengthTo

                RETURN Level:Fatal
            END ! Len(Clip(f_Number)) > tmp:LengthTo
        ELSE ! IF
        !Return The Correct Length Of A Model Number
            Return# = 0
            Do OpenFiles
            Do SaveFiles
            access:modelnum.clearkey(mod:model_number_key)
            mod:model_number = pModelNumber
            if access:modelnum.tryfetch(mod:model_number_key) = Level:Benign
                If pNumber <> 'N/A'
                    Case pType
                    Of 'IMEI'
                        If Len(Clip(pNumber)) < mod:esn_length_from Or Len(Clip(pNumber)) > mod:esn_length_to
                            Return# = 1
                        End!If Clip(Len(f_number)) < mod:esn_length_from Or Clip(Len(f_number)) > mod:esn_length_to


                    Of 'MSN'
                        If Len(Clip(pNumber)) < mod:msn_length_from Or len(clip(pNumber)) > mod:msn_length_to
                            Return# = 1
                        End!If Clip(Len(f_number)) < mod:esn_length_from
                    End!Case f_type
                End!If f_number <> 'N/A'

            End!if access:modelnum.tryfetch(mod:model_number_key) = Level:Benign
            Do RestoreFiles
            Do CloseFiles
            RETURN Return#
        END ! IF

    RETURN Level:Benign
!--------------------------------------
OpenFiles  ROUTINE
  Access:MODELNUM.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MODELNUM.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:MODELNUM.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  MODELNUM::State = Access:MODELNUM.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF MODELNUM::State <> 0
    Access:MODELNUM.RestoreFile(MODELNUM::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
