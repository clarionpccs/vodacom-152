

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER101.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ForceCustomerName    PROCEDURE  (func:AccountNumber,func:Type) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !

  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Customer Name
    If (def:Customer_Name = 'B' and func:Type = 'B') Or |
         (def:Customer_Name <> 'I' and func:Type = 'C')
         If CustomerNameRequired(func:AccountNumber) = Level:Benign
            Return Level:Fatal
         End!If CustomerNameRequired(job:Account_Number) = Level:Benign
    End!If def:Customer_Name = 'B'
    Return Level:Benign
