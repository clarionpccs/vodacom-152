

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER496.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
CallSiebelExt        PROCEDURE  (NetwebServerWorker p_web) ! Declare Procedure
    MAP
trace	PROCEDURE(STRING pText)	
    END
!region Local File and Variables Setup
!Output file from Brian's programme - pipe deliniated
SFileName                   STRING(255), STATIC
LFileName                   STRING(255), STATIC
SiebelFile                  FILE, DRIVER('BASIC', '/COMMA=124 /ALWAYSQUOTE=OFF'), PRE(SBF), NAME(SFileName), CREATE, BINDABLE, THREAD
SFRec                           RECORD
SFField1                            STRING(80)
SFField2                            STRING(80)
SFField3                            STRING(80)
SFField4                            STRING(80)
SFField5                            STRING(80)
SFField6                            STRING(80)
SFField7                            STRING(80)
SFField8                            STRING(80)
SFField9                            STRING(250)    !large to hold the loyalty status
                                END ! record
                            END ! file

!logging file for errors
LoggingFile                 FILE, DRIVER('BASIC'), PRE(LOUT), NAME(LFileName), CREATE
LFRec                           RECORD
LFField1                            STRING(80)
LFField2                            STRING(80)
LFField3                            STRING(80)
LFField4                            STRING(80)
LFField5                            STRING(80)
LFField6                            STRING(80)
LFField7                            STRING(80)
LFField8                            STRING(80)
                                END ! record
                            END ! file
locErrorCode                LONG	
locErrorDescription         STRING(255)
locErrorCodeDescription     STRING(80)

FMWUserName                 STRING(100)
FMWPassword                 STRING(100)
CustomerLookup              STRING(1)
IdentifyStack               STRING(1)
CustomerExceptionFolder     STRING(255)
CallUsing                   STRING(10)
PathToSiebel                STRING(255)
PathToSiebel2               STRING(255)
PathToExe                   STRING(255)
SB2KDEFINI                  STRING(255)

ReadField1                  String(80)
ReadField2                  String(80)
ReadField3                  String(80)
ReadField4                  String(80)
ReadField5                  String(80)
ReadField6                  String(80)
ReadField7                  String(80)
ReadField8                  String(80)
ReadField9                  String(255)

locSoapAction               STRING(80)

SiebelRefNo                 STRING(30) ! id to reference the Siebel entries (can't use job no)
!endregion

  CODE
!region ProcessedCode

        trace('CallSiebel')
	
        Relate:C3DMONIT.Open()
        Relate:SOAPERRS.Open()
        Relate:SMSMAIL.Open()
        Relate:SUBURB.Open()
	
        SB2KDEFINI = PATH() & '\SB2KDEF.INI'
	
        CustomerLookup = GETINI('C3D','CustomerLookup','N',SB2KDEFINI)
		
        p_web.SSV('SiebelRefNo','') ! Clear Var incase it fails
		
        IF (CustomerLookup = 'N')
            
        ELSE ! IF (CustomerLookup = 'N')
            SiebelRefno = RANDOM(1,1000000) ! Hopefully this is random enough
            p_web.SSV('SiebelRefNo',SiebelRefNo) ! Store for retrieving when job no is known
				
            FMWUserName             = GETINI('C3D','FMWUserName',    '',    SB2KDEFINI)
            FMWPassword             = GETINI('C3D','FMWPassword',    '',    SB2KDEFINI)
            IdentifyStack           = GETINI('C3D','IdentifyStack',  '',    SB2KDEFINI)
            CustomerExceptionFolder = GETINI('C3D','ExceptionFolder','',    SB2KDEFINI)
            CallUsing               = GETINI('C3D','CallUsing',      '',    SB2KDEFINI)
            PathToSiebel            = GETINI('C3D','PathToSiebel',   '',    SB2KDEFINI)
            PathToExe               = GETINI('C3D','PathToExe',      '',    SB2KDEFINI)
            PathToSiebel2           = GETINI('C3D','PathToSiebel2',  '',    SB2KDEFINI)			
		
			! Check everything is filled in
            locErrorCode = 0
            locErrorDescription = ''
            IF (CLIP(FMWUserName) 				= '') THEN locErrorCode += 1.
            IF (CLIP(FMWPassword) 				= '') THEN locErrorCode += 2.
            IF (CLIP(CustomerLookup) 			= '') THEN locErrorCode += 4.
            IF (CLIP(IdentifyStack) 			= '') THEN locErrorCode += 8.
            IF (CLIP(CustomerExceptionFolder) 	= '') THEN locErrorCode += 16.
            IF (CLIP(CallUsing) 				= '') THEN locErrorCode += 32.
            IF (CLIP(PathToSiebel) 				= '') THEN locErrorCode += 64.
            IF (CLIP(PathToExe) 				= '') THEN locErrorCode += 128.
			
            IF (~EXISTS(PathToExe)) 				  THEN locErrorCode += 256.
            IF (~EXISTS(CLIP(PathToExe) & '\SiebelCom.exe')) THEN locErrorCode += 512.
			
            IF (locErrorCode > 0)
                locErrorCodeDescription = 'SETUP'
                locErrorDescription = 'CODE =' & locErrorCode
            ELSE ! IF (locErrorCode > 0)
                rtn# = RUNDOS(CLIP(PathToExe)&'\SiebelCom.exe '&|
                    '  "Username:' & CLIP(FMWUserName) & |
                    '" "Password:' & CLIP(FMWPassword) & |
                    '" "URL:'      & CLIP(PathToSiebel) & |
                    '" "URL2:'     & CLIP(PathToSiebel2) & |
                    '" "MSISDN:'   & p_web.GSV('job:Mobile_Number') & |
                    '" "Output:'& CLIP(PathToExe) & '\S1' & CLIP(SiebelRefno) & '.in"',1)
								
                trace('After SiebelCom. SiebelRefNo = ' & SiebelRefNo)
								
				! Look for incoming data file
                SFileName = CLIP(PathToExe) & '\S1' & CLIP(SiebelRefno) & '.in'
				
                IF (~EXISTS(SFileName))
                    locErrorCodeDescription = 'SYSTEM'
                    locErrorDescription = 'RETURNING FILE NOT FOUND ' & CLIP(SFilename)
                ELSE ! IF (~EXISTS(SFileName)
				
                    OPEN(SiebelFile)
                    SET(SiebelFile)
                    NEXT(SiebelFile)
                    IF (ERRORCODE())
						! Could not get first record
                        locErrorCodeDescription = 'SYSTEM'
                        locErrorDescription = 'UNABLE TO READ RETURNED FILE'
                    ELSE ! IF (ERRORCODE())	
                        IF (UPPER(CLIP(SBF:SFField1)) = 'ERROR')
							! Returned an error
                            NEXT(SiebelFile)
							! Read error from next line
                            IF CLIP(SBF:SFField1) = '' THEN SBF:SFField1 = 'SYSTEM'.
                            locErrorCodeDescription = SBF:SFField1
                            locErrorDescription = SBF:SFField2
                        ELSE ! IF (UPPER(CLIP(SBF:SFField1)) = 'ERROR')
                            NEXT(SiebelFile) ! Move to send line
                            IF (ERRORCODE())
								! Could not get second record
                                locErrorCodeDescription = 'SYSTEM2'
                                locErrorDescription = 'UNABLE TO READ SECOND LINE'
                            ELSE ! IF (ERRORCODE())
                                locErrorCodeDescription = 'OK'
								
                                ReadField1 = SBF:SFField1       !Migration Status
                                ReadField2 = SBF:SFField2       !Account ID
                                ReadField3 = ''                  !'TITLE'
                                ReadField4 = SBF:SFField4       !'FORENAME'
                                ReadField5 = SBF:SFField5       !'SURNAME'
                                ReadField6 = SBF:SFField6       !'LINE1'
                                ReadField7 = SBF:SFField7       !'LINE2'
                                ReadField8 = SBF:SFField8       !'LINE3'
                                ReadField9 = SBF:SFField9		! Loyalty Status
                            END ! IF (ERRORCODE())
                        END ! IF (UPPER(CLIP(SBF:SFField1)) = 'ERROR')
                    END ! IF (ERRORCODE())
					
                    CLOSE(SiebelFile)
                    REMOVE(SiebelFile)
					
                    IF (locErrorCodeDescription = '' OR locErrorCodeDescription = 'OK') ! aside: I don't think a blank is possible
                        IF (ReadField1 = 'N')
                            locErrorCodeDescription = 'NOT MIGRATED'
                        END ! IF
                    END ! IF
					
                    IF (ReadField9 <> '')
						! If Loyalty Status Filled
                        p_web.SSV('jobe3:LoyaltyStatus',ReadField9)
                    END ! IF
                END ! IF (~EXISTS(SFileName)
            END ! IF (locErrorCode > 0)
			
            trace('Add To C3DMonit: ' & CLIP(locErrorCodeDescription))
			
			! Always write to C3DMONIT ?? Maybe do this at the end
            IF (Access:C3DMONIT.PrimeRecord() = Level:Benign)
                C3D:RefNumber = SiebelRefNo
                C3D:MSISDN    = p_web.GSV('job:Mobile_Number')
                C3D:CallDate  = TODAY()
                C3D:CallTime  = CLOCK()
                C3D:ErrorCode = locErrorCodeDescription
                IF (Access:C3DMONIT.TryInsert())
                    Access:C3DMONIT.CancelAutoInc()
                END ! IF
            END ! IF
			
			!look up the required action AUTO/MANUAL/MANUAL PLUS/RETRY
            Access:SOAPERRS.ClearKey(sop:KeyErrorCode)
            sop:ErrorCode = locErrorCodeDescription
            IF (Access:SOAPERRS.TryFetch(sop:KeyErrorCode))
                sop:Action = 'ERROR NOT FOUND'
            END ! IF
			
            trace('Soap Action: ' & CLIP(sop:Action))
			
			!All "errors" must be logged in an exception file together with the MSISDN that was queried. The log file will contain the following information:
            IF (locErrorCodeDescription <> 'OK')
                LFileName = CLIP(CustomerExceptionFolder) & '\MSISDN_Query_Exception_' &  FORMAT(TODAY(),@d10-) & '.csv'
				
                IF (~EXISTS(LFileName))
                    CREATE(LoggingFile)
                    SHARE(LoggingFile)
                    Lout:LFField1 = 'DATE'
                    Lout:LFField2 = 'TIME'
                    Lout:LFField3 = 'MOBILE'
                    Lout:LFField4 = 'ERROR'
                    Lout:LFField5 = 'FAULT'
                    Lout:LFField6 = 'TYPE'
                    Lout:LFField7 = 'DESCRIPTION'
                    Lout:LFField8 = 'DETAILS'
                    ADD(LoggingFile)						
                ELSE ! IF
                    SHARE(LoggingFile) ! File already exists. Open it and add to it
                END ! IF
				
                Lout:LFField1 = FORMAT(TODAY(),@d06)
                Lout:LFField2 = FORMAT(CLOCK(),@t04)
                Lout:LFField3 = p_web.GSV('job:Mobile_Number')
                Lout:LFField4 = CLIP(locErrorCodeDescription)
                Lout:LFField5 = CLIP(SOP:SoapFault)
                Lout:LFField6 = CLIP(SOP:ErrorType)
                Lout:LFField7 = CLIP(SOP:Description)
                Lout:LFField8 = locErrorDescription
                ADD(LoggingFile)
                CLOSE(LoggingFile)					
                
                trace('Error: ' & CLIP(locErrorDescription))
				
				!for some transactions failed an email must be sent to servicebase.support@vodacom.co.za with a copy of exception file.
                IF (sop:Action = 'MANUAL PLUS')
                    IF (Access:SMSMAIL.PrimeRecord() = Level:Benign)
                        sms:SendToSMS       = 'F'
                        sms:SendToEmail     = 'T'
                        sms:SMSSent         = 'F'
                        sms:EmailSent       = 'F'
                        sms:DateInserted    = TODAY()
                        sms:TimeInserted    = CLOCK()
                        sms:EmailSubject    = CLIP(SOP:Subject)
                        sms:EmailAddress    = 'servicebase.support@vodacom.co.za'
                        sms:PathToEstimate  = CLIP(LFileName)
                        sms:MSG             = CLIP(SOP:Body) &|
                            '<13,10>Date: '&FORMAT(TODAY(),@D06)&|
                            '<13,10>Time: '&FORMAT(CLOCK(),@t04)							
                        IF (Access:SMSMAIL.TryInsert())
                            Access:SMSMAIL.CancelAutoInc()
                        END ! IF
                    END ! IF
                END ! IF
				
            END ! IF (locErrorCodeDescription <> 'OK')
			
            IF (sop:Action = 'AUTO')
                p_web.SSV('job:Initial',ReadField4)
                p_web.SSV('job:Surname',ReadField5)
                p_web.SSV('job:Company_Name',CLIP(ReadField4) & ' ' & CLIP(ReadField5))
                p_web.SSV('job:Address_Line1',ReadField6)
                p_web.SSV('job:Address_Line2',ReadField7)
                p_web.SSV('job:Address_Line3',ReadField8)
				
                Access:SUBURB.ClearKey(sur:SuburbKey)
                sur:Suburb = p_web.GSV('job:Address_Line3')
                IF (Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign)
                    p_web.SSV('job:Postcode',sur:Postcode)
                    p_web.SSV('jobe2:HubCustomer',sur:Hub)
                END ! IF					
            END ! IF
        END ! IF (CustomerLookup = 'N')
		
        Relate:SUBURB.Close()
        Relate:SMSMAIL.Close()
        Relate:SOAPERRS.Close()
        Relate:C3DMONIT.Close()
!endregion
trace	PROCEDURE(STRING pText)
outString	CSTRING(1000)
    CODE
        RETURN ! Logging Disabled
