

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER273.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER265.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER268.INC'),ONCE        !Req'd for module callout resolution
                     END


FormAddToBatch       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locCompanyDetails    STRING(60)                            !
locHeadAccountNumber STRING(30)                            !
locExistingBatchNumber LONG                                !
locBatchStatusText   STRING(100)                           !
locBatchAccountNumber STRING(30)                           !
locBatchCourier      STRING(30)                            !
locNumberInBatch     LONG                                  !
locBatchAccountName  STRING(30)                            !
locWaybillNumber     STRING(30)                            !
FilesOpened     Long
MULDESPJ::State  USHORT
MULDESP::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
JOBS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormAddToBatch')
  loc:formname = 'FormAddToBatch_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormAddToBatch',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormAddToBatch','')
    p_web._DivHeader('FormAddToBatch',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormAddToBatch',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAddToBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAddToBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormAddToBatch',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormAddToBatch',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormAddToBatch',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormAddToBatch',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
DeleteSessionValues ROUTINE
    p_web.DeleteSessionValue('locCompanyDetails')
    p_web.DeleteSessionValue('locHeadAccountNumber')
    p_web.DeleteSessionValue('locExistingBatchNumber')
    p_web.DeleteSessionValue('locBatchStatusText')
    p_web.DeleteSessionValue('locBatchAccountNumber')
    p_web.DeleteSessionValue('locBatchCourier')
    p_web.DeleteSessionValue('locNumberInBatch')
    p_web.DeleteSessionValue('locBatchAccountName')
    p_web.DeleteSessionValue('Hide:CreateNewBatchButton')
    p_web.DeleteSessionvalue('Hide:ConsignmentNumber')
    p_web.DeleteSessionValue('locWaybillNumber')
    
OpenFiles  ROUTINE
  p_web._OpenFile(MULDESPJ)
  p_web._OpenFile(MULDESP)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MULDESPJ)
  p_Web._CloseFile(MULDESP)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormAddToBatch_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  DO DeleteSessionValues

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:Ref_Number')
    p_web.SetPicture('job:Ref_Number','@s8')
  End
  p_web.SetSessionPicture('job:Ref_Number','@s8')
  If p_web.IfExistsValue('job:Charge_Type')
    p_web.SetPicture('job:Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Charge_Type','@s30')
  If p_web.IfExistsValue('job:Warranty_Charge_Type')
    p_web.SetPicture('job:Warranty_Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Warranty_Charge_Type','@s30')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  If p_web.GSV('locExistingBatchNumber') > 0
    loc:TabNumber += 1
  End
  If p_web.GSV('locExistingBatchNumber') = 0
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locCompanyDetails',locCompanyDetails)
  p_web.SetSessionValue('job:Ref_Number',job:Ref_Number)
  p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  p_web.SetSessionValue('locBatchStatusText',locBatchStatusText)
  p_web.SetSessionValue('locBatchAccountNumber',locBatchAccountNumber)
  p_web.SetSessionValue('locBatchAccountName',locBatchAccountName)
  p_web.SetSessionValue('locBatchCourier',locBatchCourier)
  p_web.SetSessionValue('locNumberInBatch',locNumberInBatch)
  p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locCompanyDetails')
    locCompanyDetails = p_web.GetValue('locCompanyDetails')
    p_web.SetSessionValue('locCompanyDetails',locCompanyDetails)
  End
  if p_web.IfExistsValue('job:Ref_Number')
    job:Ref_Number = p_web.GetValue('job:Ref_Number')
    p_web.SetSessionValue('job:Ref_Number',job:Ref_Number)
  End
  if p_web.IfExistsValue('job:Charge_Type')
    job:Charge_Type = p_web.GetValue('job:Charge_Type')
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  End
  if p_web.IfExistsValue('job:Warranty_Charge_Type')
    job:Warranty_Charge_Type = p_web.GetValue('job:Warranty_Charge_Type')
    p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  End
  if p_web.IfExistsValue('locBatchStatusText')
    locBatchStatusText = p_web.GetValue('locBatchStatusText')
    p_web.SetSessionValue('locBatchStatusText',locBatchStatusText)
  End
  if p_web.IfExistsValue('locBatchAccountNumber')
    locBatchAccountNumber = p_web.GetValue('locBatchAccountNumber')
    p_web.SetSessionValue('locBatchAccountNumber',locBatchAccountNumber)
  End
  if p_web.IfExistsValue('locBatchAccountName')
    locBatchAccountName = p_web.GetValue('locBatchAccountName')
    p_web.SetSessionValue('locBatchAccountName',locBatchAccountName)
  End
  if p_web.IfExistsValue('locBatchCourier')
    locBatchCourier = p_web.GetValue('locBatchCourier')
    p_web.SetSessionValue('locBatchCourier',locBatchCourier)
  End
  if p_web.IfExistsValue('locNumberInBatch')
    locNumberInBatch = p_web.GetValue('locNumberInBatch')
    p_web.SetSessionValue('locNumberInBatch',locNumberInBatch)
  End
  if p_web.IfExistsValue('locWaybillNumber')
    locWaybillNumber = p_web.GetValue('locWaybillNumber')
    p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormAddToBatch_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  ! Start
  p_web.SSV('Hide:AddToBatch',0)
  p_web.SSV('Hide:CreateNewBatchButton',0)
  p_web.SSV('Hide:ConsignmentNumber',1)
  p_web.SSV('locWaybillNumber','')
  
  Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
  sub:Account_Number = p_web.GSV('job:Account_Number')
  IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
      p_web.SSV('locCompanyDetails',p_web.GSV('job:Account_Number') & ' - ' & Clip(sub:Company_Name))
  END
  
  locHeadAccountNumber = p_web.GSV('job:Account_Number')
  
  IF (sub:Generic_Account)
      IF (p_web.GSV('BookingSite') = 'RRC' OR | 
          (p_web.GSV('BookingSite') = 'ARC' AND p_web.GSV('jobe:WebJob') <> 1))
      ELSE
          locHeadAccountNumber = p_web.GSV('wob:HeadAccountNumber')
      END
      
  ELSE
      IF (p_web.GSV('BookingSite') = 'ARC' AND p_web.GSV('jobe:WebJob') = 1)
          locHeadAccountNumber = p_web.GSV('wob:HeadAccountNumber')
      END
  END
  
  ! Is there an existing batch?
  VIRTUAL# = 0
  locExistingBatchNumber = 0
  IF (p_web.GSV('BookingSite') = 'ARC')
      Access:MULDESP.ClearKey(muld:HeadAccountKey)
      muld:HeadAccountNumber = p_web.GSV('BookingAccount')
      muld:AccountNumber = locHeadAccountNumber
      If GETINI('DESPATCH','GroupVirtualBatches',,CLIP(PATH())&'\SB2KDEF.INI') = 1
          IF (NOT sub:Generic_Account)
              IF (vod.RemoteAccount(p_web.GSV('job:Account_Number')))
                  muld:AccountNumber = tra:Account_Number
                  VIRTUAL# = 1
              END
          END
          
      END
      
  ELSE
      IF (p_web.GSV('jobe:Sub_Sub_Account') = '')
          Access:MULDESP.ClearKey(muld:AccountNumberKey)
          muld:HeadAccountNumber = p_web.GSV('BookingAccount')
          muld:AccountNumber = locHeadAccountNumber
      ELSE
          locHeadAccountNumber = p_web.GSV('jobe:Sub_Sub_Account')
          Access:MULDESP.ClearKey(muld:AccountNumberKey)
          muld:HeadAccountNumber = p_web.GSV('BookingAccount')
          muld:AccountNumber = locHeadAccountNumber
      END
      
  END
  SET(muld:HeadAccountKey,muld:HeadAccountKey)
  LOOP UNTIL Access:MULDESP.Next()
      IF (muld:HeadAccountNumber <> p_web.GSV('BookingAccount'))
          BREAK
      END
      IF (VIRTUAL# = 1)
          IF (muld:AccountNumber <> tra:Account_Number)
              BREAK
          END
          IF (muld:Courier = tra:Courier_Outgoing)
              locExistingBatchNumber = muld:RecordNumber
          END
      ELSE
          IF (muld:AccountNumber <> locHeadAccountNumber)
              BREAK
          END
          IF (p_web.GSV('BookingSite') = 'ARC')
              CASE p_web.GSV('job:Despatch_Type')
              OF 'JOB'
                  IF (p_web.GSV('job:Courier') = muld:Courier)
                      locExistingBatchNumber = muld:RecordNumber
                      BREAK
                  END
                  
              OF 'EXC'
                  IF (p_web.GSV('job:Exchange_Courier') = muld:Courier)
                      locExistingBatchNumber = muld:RecordNumber
                      BREAK
                  END
              OF 'LOA'
                  IF (p_web.GSV('job:Loan_Courier') = muld:Courier)
                      locExistingBatchNumber = muld:RecordNumber
                      BREAK
                  END
              END
              
          ELSE
              CASE p_web.GSV('jobe:DespatchType')
              OF 'JOB'
                  IF (p_web.GSV('job:Courier') = muld:Courier)
                      locExistingBatchNumber = muld:RecordNumber
                      BREAK
                  END
                  
              OF 'EXC'
                  IF (p_web.GSV('job:Exchange_Courier') = muld:Courier)
                      locExistingBatchNumber = muld:RecordNumber
                      BREAK
                  END
              OF 'LOA'
                  IF (p_web.GSV('job:Loan_Courier') = muld:Courier)
                      locExistingBatchNumber = muld:RecordNumber
                      BREAK
                  END
              END
          END
          
      END
  END
  
  IF (locExistingBatchNumber = 0)
      p_web.SSV('locBatchStatusText','No Batch Found')
      p_web.SSV('Hide:AddToBatch',1)
  ELSE
      ! Found an existing batch, fill in the details
      Access:MULDESP.ClearKey(muld:RecordNumberKey)
      muld:RecordNumber = locExistingBatchNumber
      IF (Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign)
          p_web.SSV('locBatchAccountNumber',muld:AccountNumber)
          p_web.SSV('locBatchCourier',muld:Courier)
          
          locNumberInBatch = 0
          Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
          mulj:RefNumber = muld:RecordNumber
          SET(mulj:JobNumberKey,mulj:JobNumberKey)
          LOOP UNTIL Access:MULDESPJ.Next()
              IF (mulj:RefNumber <> muld:RecordNumber)
                  BREAK
              END
              locNumberInBatch += 1
          END
          p_web.SSV('locNumberInBatch',locNumberInBatch)
          
          ! Get The Account Name
          IF (muld:BatchType = 'TRA')
              Access:TRADEACC.ClearKey(tra:Account_Number_Key)
              tra:Account_Number = muld:AccountNumber
              IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                  p_web.SSV('locBatchAccountName',tra:Company_Name)
              END
          ELSE
              Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
              sub:Account_Number = muld:AccountNumber
              IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
                  p_web.SSV('locBatchAccountName',sub:Company_Name)
              END
  
          END
          
      END
      
      p_web.SSV('locBatchStatusText','Existing Batch Found')
      p_web.SSV('Hide:AddToBatch',0)
  END
  p_web.SSV('locExistingBatchNumber',locExistingBatchNumber)
  
  !  ! Show Consignment Number? ! I don't think this is needed here?!
  !  IF (p_web.GSV('cou:PrintWaybill') = 1)
  !  ELSE
  !      IF (p_web.GSV('cou:AutoConsignmentNo') = 1)
  !            
  !      ELSE
  !          p_web.SSV('Hide:CreateNewBatchButton',1)
  !          p_web.SSV('Hide:ConsignmentNumber',0)
  !      END
  !  END
          
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locCompanyDetails = p_web.RestoreValue('locCompanyDetails')
 locBatchStatusText = p_web.RestoreValue('locBatchStatusText')
 locBatchAccountNumber = p_web.RestoreValue('locBatchAccountNumber')
 locBatchAccountName = p_web.RestoreValue('locBatchAccountName')
 locBatchCourier = p_web.RestoreValue('locBatchCourier')
 locNumberInBatch = p_web.RestoreValue('locNumberInBatch')
 locWaybillNumber = p_web.RestoreValue('locWaybillNumber')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'MultipleBatchDespatch'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormAddToBatch_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormAddToBatch_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormAddToBatch_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'MultipleBatchDespatch'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormAddToBatch" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormAddToBatch" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormAddToBatch" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Add To Batch') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Add To Batch',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormAddToBatch">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormAddToBatch" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormAddToBatch')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Current Job Details') & ''''
        If p_web.GSV('locExistingBatchNumber') > 0
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Existing Batch Details') & ''''
        End
        If p_web.GSV('locExistingBatchNumber') = 0
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Create New Batch') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Force Single Despatch') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormAddToBatch')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormAddToBatch'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          If p_web.GSV('locExistingBatchNumber') > 0
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          End
          If p_web.GSV('locExistingBatchNumber') = 0
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab5'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormAddToBatch')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    if p_web.GSV('locExistingBatchNumber') > 0
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
    end
    if p_web.GSV('locExistingBatchNumber') = 0
      packet = clip(packet) & 'roundCorners(''tab5'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Current Job Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormAddToBatch_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Current Job Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Current Job Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Current Job Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Current Job Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCompanyDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Ref_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Ref_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('job:Chargeable_Job') = 'YES'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('job:warranty_job') = 'YES'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Warranty_Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Warranty_Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
  If p_web.GSV('locExistingBatchNumber') > 0
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Existing Batch Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormAddToBatch_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Existing Batch Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Existing Batch Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Existing Batch Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Existing Batch Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locBatchStatusText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('locBatchAccountNumber') <> ''
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locBatchAccountNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locBatchAccountNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('locBatchAccountName') <> ''
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locBatchAccountName
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locBatchAccountName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('locBatchCourier') <> ''
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locBatchCourier
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locBatchCourier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('locNumberInBatch') <> ''
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locNumberInBatch
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locNumberInBatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonAddToExistingBatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::textCreateNewBatchMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab2  Routine
  If p_web.GSV('locExistingBatchNumber') = 0
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel5">'&CRLF &|
                                    '  <div id="panel5Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Create New Batch') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel5Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormAddToBatch_5">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Create New Batch')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab5" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab5">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Create New Batch')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Create New Batch')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab5">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Create New Batch')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    If p_web.GSV('Hide:ConsignmentNumber') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::textEnterConsingmentNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:ConsignmentNumber') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWaybillNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWaybillNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonCreateNewBatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Force Single Despatch') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormAddToBatch_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Force Single Despatch')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Force Single Despatch')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Force Single Despatch')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Force Single Despatch')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonForceSingleDespatch
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::locCompanyDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCompanyDetails',p_web.GetValue('NewValue'))
    locCompanyDetails = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCompanyDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCompanyDetails',p_web.GetValue('Value'))
    locCompanyDetails = p_web.GetValue('Value')
  End

Value::locCompanyDetails  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locCompanyDetails') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locCompanyDetails
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('BlueBoldLarge')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locCompanyDetails'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::job:Ref_Number  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('job:Ref_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Ref_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Ref_Number',p_web.GetValue('NewValue'))
    job:Ref_Number = p_web.GetValue('NewValue') !FieldType= LONG Field = job:Ref_Number
    do Value::job:Ref_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Ref_Number',p_web.dFormat(p_web.GetValue('Value'),'@s8'))
    job:Ref_Number = p_web.GetValue('Value')
  End

Value::job:Ref_Number  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('job:Ref_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Ref_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Ref_Number'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::job:Charge_Type  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('job:Charge_Type') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Chargeable Charge Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Charge_Type',p_web.GetValue('NewValue'))
    job:Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Charge_Type
    do Value::job:Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Charge_Type = p_web.GetValue('Value')
  End

Value::job:Charge_Type  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('job:Charge_Type') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::job:Warranty_Charge_Type  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('job:Warranty_Charge_Type') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Warranty Charge Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Warranty_Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Warranty_Charge_Type',p_web.GetValue('NewValue'))
    job:Warranty_Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Warranty_Charge_Type
    do Value::job:Warranty_Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Warranty_Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Warranty_Charge_Type = p_web.GetValue('Value')
  End

Value::job:Warranty_Charge_Type  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('job:Warranty_Charge_Type') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Warranty_Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Warranty_Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::locBatchStatusText  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locBatchStatusText',p_web.GetValue('NewValue'))
    locBatchStatusText = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locBatchStatusText
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locBatchStatusText',p_web.GetValue('Value'))
    locBatchStatusText = p_web.GetValue('Value')
  End

Value::locBatchStatusText  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locBatchStatusText') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locBatchStatusText
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('BlueBoldLarge')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locBatchStatusText'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locBatchAccountNumber  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locBatchAccountNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locBatchAccountNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locBatchAccountNumber',p_web.GetValue('NewValue'))
    locBatchAccountNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locBatchAccountNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locBatchAccountNumber',p_web.GetValue('Value'))
    locBatchAccountNumber = p_web.GetValue('Value')
  End

Value::locBatchAccountNumber  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locBatchAccountNumber') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locBatchAccountNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locBatchAccountNumber'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locBatchAccountName  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locBatchAccountName') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account Name')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locBatchAccountName  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locBatchAccountName',p_web.GetValue('NewValue'))
    locBatchAccountName = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locBatchAccountName
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locBatchAccountName',p_web.GetValue('Value'))
    locBatchAccountName = p_web.GetValue('Value')
  End

Value::locBatchAccountName  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locBatchAccountName') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locBatchAccountName
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locBatchAccountName'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locBatchCourier  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locBatchCourier') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Courier')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locBatchCourier  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locBatchCourier',p_web.GetValue('NewValue'))
    locBatchCourier = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locBatchCourier
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locBatchCourier',p_web.GetValue('Value'))
    locBatchCourier = p_web.GetValue('Value')
  End

Value::locBatchCourier  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locBatchCourier') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locBatchCourier
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locBatchCourier'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locNumberInBatch  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locNumberInBatch') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Units In Batch')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locNumberInBatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locNumberInBatch',p_web.GetValue('NewValue'))
    locNumberInBatch = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locNumberInBatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locNumberInBatch',p_web.GetValue('Value'))
    locNumberInBatch = p_web.GetValue('Value')
  End

Value::locNumberInBatch  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locNumberInBatch') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locNumberInBatch
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locNumberInBatch'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::buttonAddToExistingBatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonAddToExistingBatch',p_web.GetValue('NewValue'))
    do Value::buttonAddToExistingBatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonAddToExistingBatch  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('buttonAddToExistingBatch') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AddToBatch','Add To Batch','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('MultipleBatchDespatch?' &'Action=AddToBatch&BatchNumber=' & p_web.GSV('locExistingBatchNumber'))) & ''','''&clip('_self')&''')',loc:javascript,0,'images/packinsert.png',,,,)

  do SendPacket
  p_web._DivFooter()


Validate::textCreateNewBatchMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textCreateNewBatchMessage',p_web.GetValue('NewValue'))
    do Value::textCreateNewBatchMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textCreateNewBatchMessage  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('textCreateNewBatchMessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('BlueBold')&'">' & p_web.Translate(p_web.br & '(Note: If you wish to create a New Batch, you must despatch/delete the current batch first.)',1) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::textEnterConsingmentNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textEnterConsingmentNumber',p_web.GetValue('NewValue'))
    do Value::textEnterConsingmentNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textEnterConsingmentNumber  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('textEnterConsingmentNumber') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web.Translate('Enter A Consignment Number, then press [TAB]',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locWaybillNumber  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locWaybillNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Consignment Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locWaybillNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWaybillNumber',p_web.GetValue('NewValue'))
    locWaybillNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWaybillNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locWaybillNumber',p_web.GetValue('Value'))
    locWaybillNumber = p_web.GetValue('Value')
  End
  If locWaybillNumber = ''
    loc:Invalid = 'locWaybillNumber'
    loc:alert = p_web.translate('Consignment Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locWaybillNumber = Upper(locWaybillNumber)
    p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)
  IF (p_web.GSV('locWaybillNumber') > 0)
      p_web.SSV('Hide:CreateNewBatchButton',0)
  ELSE
      p_web.SSV('Hide:CreateNewBatchButton',1)
  END
  do Value::locWaybillNumber
  do SendAlert
  do Value::buttonCreateNewBatch  !1

Value::locWaybillNumber  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('locWaybillNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locWaybillNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locWaybillNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locWaybillNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWaybillNumber'',''formaddtobatch_locwaybillnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locWaybillNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locWaybillNumber',p_web.GetSessionValueFormat('locWaybillNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormAddToBatch_' & p_web._nocolon('locWaybillNumber') & '_value')


Validate::buttonCreateNewBatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonCreateNewBatch',p_web.GetValue('NewValue'))
    do Value::buttonCreateNewBatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonCreateNewBatch  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('buttonCreateNewBatch') & '_value',Choose(p_web.GSV('Hide:CreateNewBatchButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CreateNewBatchButton') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CreateNewBatch','Create New Batch','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('CreateNewBatch')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/new.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormAddToBatch_' & p_web._nocolon('buttonCreateNewBatch') & '_value')


Validate::buttonForceSingleDespatch  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonForceSingleDespatch',p_web.GetValue('NewValue'))
    do Value::buttonForceSingleDespatch
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonForceSingleDespatch  Routine
  p_web._DivHeader('FormAddToBatch_' & p_web._nocolon('buttonForceSingleDespatch') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ForceSingleDespatch','Individual Despatch','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('IndividualDespatch?' &'PassedJobNumber=' & p_web.GSV('locJobNumber') & '&PassedIMEINumber=' & p_web.GSV('locIMEINumber'))) & ''','''&clip('_self')&''')',loc:javascript,0,'images/package.png',,,,)

  do SendPacket
  p_web._DivFooter()


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormAddToBatch_locWaybillNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWaybillNumber
      else
        do Value::locWaybillNumber
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormAddToBatch_form:ready_',1)
  p_web.SetSessionValue('FormAddToBatch_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormAddToBatch',0)

PreCopy  Routine
  p_web.SetValue('FormAddToBatch_form:ready_',1)
  p_web.SetSessionValue('FormAddToBatch_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormAddToBatch',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormAddToBatch_form:ready_',1)
  p_web.SetSessionValue('FormAddToBatch_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormAddToBatch:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormAddToBatch_form:ready_',1)
  p_web.SetSessionValue('FormAddToBatch_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormAddToBatch:Primed',0)
  p_web.setsessionvalue('showtab_FormAddToBatch',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('locExistingBatchNumber') > 0
  End
  If p_web.GSV('locExistingBatchNumber') = 0
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormAddToBatch_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormAddToBatch_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
  ! tab = 3
  If p_web.GSV('locExistingBatchNumber') > 0
    loc:InvalidTab += 1
  End
  ! tab = 5
  If p_web.GSV('locExistingBatchNumber') = 0
    loc:InvalidTab += 1
    If p_web.GSV('Hide:ConsignmentNumber') <> 1
        If locWaybillNumber = ''
          loc:Invalid = 'locWaybillNumber'
          loc:alert = p_web.translate('Consignment Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locWaybillNumber = Upper(locWaybillNumber)
          p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)
        If loc:Invalid <> '' then exit.
    End
  End
  ! tab = 4
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormAddToBatch:Primed',0)
  p_web.StoreValue('locCompanyDetails')
  p_web.StoreValue('job:Ref_Number')
  p_web.StoreValue('job:Charge_Type')
  p_web.StoreValue('job:Warranty_Charge_Type')
  p_web.StoreValue('locBatchStatusText')
  p_web.StoreValue('locBatchAccountNumber')
  p_web.StoreValue('locBatchAccountName')
  p_web.StoreValue('locBatchCourier')
  p_web.StoreValue('locNumberInBatch')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locWaybillNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
