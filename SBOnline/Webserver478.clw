

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER478.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
JobReceipt PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
tmp:JobNumber        LONG                                  !Job Number
tmp:PrintedBy        STRING(60)                            !
who_booked           STRING(50)                            !
Webmaster_Group      GROUP,PRE(tmp)                        !===============================================
Ref_number           STRING(20)                            !
BarCode              STRING(20)                            !
BranchIdentification STRING(2)                             !
                     END                                   !
save_job2_id         USHORT,AUTO                           !
tmp:accessories      STRING(255)                           !
RejectRecord         LONG,AUTO                             !
save_joo_id          USHORT,AUTO                           !
save_maf_id          USHORT,AUTO                           !
save_jac_id          USHORT,AUTO                           !
save_wpr_id          USHORT,AUTO                           !
save_par_id          USHORT,AUTO                           !
save_lac_id          USHORT                                !
save_loa_id          USHORT                                !
LocalRequest         LONG,AUTO                             !
LocalResponse        LONG,AUTO                             !
FilesOpened          LONG                                  !
WindowOpened         LONG                                  !
RecordsToProcess     LONG,AUTO                             !
RecordsProcessed     LONG,AUTO                             !
RecordsPerCycle      LONG,AUTO                             !
RecordsThisCycle     LONG,AUTO                             !
PercentProgress      BYTE                                  !
RecordStatus         BYTE,AUTO                             !
EndOfReport          BYTE,AUTO                             !
ReportRunDate        LONG,AUTO                             !
ReportRunTime        LONG,AUTO                             !
ReportPageNo         SHORT,AUTO                            !
FileOpensReached     BYTE                                  !
PartialPreviewReq    BYTE                                  !
DisplayProgress      BYTE                                  !
InitialPath          CSTRING(128)                          !
Progress:Thermometer BYTE                                  !
IniFileToUse         STRING(64)                            !
code_temp            BYTE                                  !
save_jea_id          USHORT,AUTO                           !
fault_code_field_temp STRING(30),DIM(12)                   !
option_temp          BYTE                                  !
Bar_code_string_temp CSTRING(21)                           !
Bar_Code_Temp        CSTRING(21)                           !
Bar_Code2_Temp       CSTRING(21)                           !
Address_Line1_Temp   STRING(30)                            !
Address_Line2_Temp   STRING(30)                            !
Address_Line3_Temp   STRING(30)                            !
Address_Line4_Temp   STRING(30)                            !
Invoice_Name_Temp    STRING(30)                            !
Delivery_Address1_Temp STRING(30)                          !
Delivery_address2_temp STRING(30)                          !
Delivery_address3_temp STRING(30)                          !
Delivery_address4_temp STRING(30)                          !
Delivery_Telephone_Temp STRING(30)                         !Delivery Telephone
InvoiceAddress_Group GROUP,PRE()                           !===============================================
Invoice_Company_Temp STRING(30)                            !
Invoice_address1_temp STRING(30)                           !
invoice_address2_temp STRING(30)                           !
invoice_address3_temp STRING(30)                           !
invoice_address4_temp STRING(30)                           !
invoice_EMail_Address STRING(255)                          !Email Address
                     END                                   !
accessories_temp     STRING(30),DIM(6)                     !
estimate_value_temp  STRING(70)                            !
despatched_user_temp STRING(40)                            !
vat_temp             REAL                                  !
total_temp           REAL                                  !
part_number_temp     STRING(30)                            !
line_cost_temp       REAL                                  !
job_number_temp      STRING(20)                            !
esn_temp             STRING(30)                            !ESN
charge_type_temp     STRING(22)                            !
repair_type_temp     STRING(22)                            !
labour_temp          REAL                                  !
parts_temp           REAL                                  !
courier_cost_temp    REAL                                  !
Quantity_temp        REAL                                  !
Description_temp     STRING(30)                            !
Cost_Temp            REAL                                  !
engineer_temp        STRING(30)                            !
part_type_temp       STRING(4)                             !
customer_name_temp   STRING(40)                            !
delivery_name_temp   STRING(40)                            !
exchange_unit_number_temp STRING(20)                       !
exchange_model_number STRING(30)                           !
exchange_manufacturer_temp STRING(30)                      !
exchange_unit_type_temp STRING(30)                         !
exchange_esn_temp    STRING(16)                            !
exchange_msn_temp    STRING(15)                            !
exchange_unit_title_temp STRING(15)                        !
invoice_company_name_temp STRING(30)                       !
invoice_telephone_number_temp STRING(15)                   !
invoice_fax_number_temp STRING(15)                         !
tmp:DefaultTelephone STRING(20)                            !
tmp:DefaultFax       STRING(20)                            !
tmp:bouncers         STRING(255)                           !
tmp:InvoiceText      STRING(255)                           !Invoice Text
tmp:LoanModel        STRING(30)                            !Loan Model Details
tmp:LoanIMEI         STRING(30)                            !Loan IMEI number
tmp:LoanAccessories  STRING(255)                           !Loan Accessories
tmp:LoanDepositPaid  REAL                                  !Loan Deposit Paid
DefaultAddress       GROUP,PRE(address)                    !
SiteName             STRING(40)                            !
Name                 STRING(40)                            !Name
Name2                STRING(40)                            !
Location             STRING(40)                            !
RegistrationNo       STRING(40)                            !
AddressLine1         STRING(40)                            !Address Line 1
AddressLine2         STRING(40)                            !Address Line 2
AddressLine3         STRING(40)                            !Address Line 3
AddressLine4         STRING(40)                            !
VatNumber            STRING(30)                            !
Telephone            STRING(30)                            !Telephone
Fax                  STRING(30)                            !Fax
EmailAddress         STRING(255)                           !Email Address
                     END                                   !
delivery_Company_Name_temp STRING(30)                      !Delivery Name
endUserTelNo         STRING(15)                            !
clientName           STRING(65)                            !
tmp:ReplacementValue REAL                                  !Replacement Value
tmp:FaultCodeDescription STRING(255),DIM(6)                !Fault Code Description
tmp:BookingOption    STRING(30)                            !Booking Option
tmp:ExportReport     BYTE                                  !
Received_By_Temp     STRING(60)                            !Received_By_Temp
Amount_Received_Temp REAL                                  !Amount_Received_Temp
tmp:StandardText     STRING(255)                           !tmp:StandardText
barcodeJobNumber     STRING(20)                            !
barcodeIMEINumber    STRING(20)                            !
locTermsText         STRING(255)                           !
Process:View         VIEW(JOBS)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Charge_Type)
                       PROJECT(job:ESN)
                       PROJECT(job:Incoming_Consignment_Number)
                       PROJECT(job:Incoming_Courier)
                       PROJECT(job:Incoming_Date)
                       PROJECT(job:MSN)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Repair_Type)
                       PROJECT(job:Repair_Type_Warranty)
                       PROJECT(job:Transit_Type)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:Warranty_Charge_Type)
                       PROJECT(job:date_booked)
                       PROJECT(job:time_booked)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT('Job Receipt'),AT(250,4323,7750,5813),PRE(RPT),PAPER(PAPER:A4),FONT('Tahoma',8,,,CHARSET:ANSI), |
  THOUS
                       HEADER,AT(323,438,7521,4115),USE(?unnamed),FONT('Tahoma')
                         STRING('Job Number: '),AT(5052,365),USE(?String25),FONT(,8),TRN
                         STRING(@s20),AT(5990,365),USE(tmp:Ref_number),FONT(,9,,FONT:bold),LEFT,TRN
                         STRING('Date Booked: '),AT(5052,677),USE(?String58),FONT(,8),TRN
                         STRING(@d6b),AT(5990,677),USE(job:date_booked),FONT(,8,,FONT:bold),TRN
                         STRING(@t1),AT(6875,677),USE(job:time_booked),FONT(,8,,FONT:bold),TRN
                         STRING(@s20),AT(1604,3240),USE(job:Charge_Type),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(4635,3240),USE(job:Warranty_Charge_Type),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(3125,3240),USE(job:Repair_Type),FONT(,8),TRN
                         STRING(@s20),AT(6146,3240),USE(job:Repair_Type_Warranty),FONT(,8),TRN
                         STRING('Model'),AT(156,3646),USE(?String40),FONT(,8,,FONT:bold),TRN
                         STRING('Make'),AT(1604,3646),USE(?String41),FONT(,8,,FONT:bold),TRN
                         STRING('Unit Type'),AT(3125,3646),USE(?String42),FONT(,8,,FONT:bold),TRN
                         STRING('I.M.E.I. Number'),AT(4635,3646),USE(?String43),FONT(,8,,FONT:bold),TRN
                         STRING('M.S.N.'),AT(6146,3646),USE(?String44),FONT(,8,,FONT:bold),TRN
                         STRING(@s30),AT(156,2188),USE(invoice_address4_temp),FONT(,8),TRN
                         STRING('Tel: '),AT(4083,2344),USE(?String32:2),FONT(,8),TRN
                         STRING(@s30),AT(4323,2344),USE(Delivery_Telephone_Temp),FONT(,8)
                         STRING(@s65),AT(4063,2500,2865,156),USE(clientName),FONT('Arial',8,,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING('Order Number'),AT(156,3000),USE(?String40:2),FONT(,8,,FONT:bold),TRN
                         STRING('Chargeable Type'),AT(1604,3000),USE(?String40:3),FONT(,8,,FONT:bold),TRN
                         STRING('Chargeable Repair Type'),AT(3125,3000),USE(?String40:4),FONT(,8,,FONT:bold),TRN
                         STRING('Warranty Type'),AT(4635,3000),USE(?String40:5),FONT(,8,,FONT:bold),TRN
                         STRING('Warranty Repair Type'),AT(6146,3000),USE(?String40:6),FONT(,8,,FONT:bold),TRN
                         STRING('Tel:'),AT(2198,2031),USE(?String34:3),FONT(,8),TRN
                         STRING('Acc No:'),AT(2188,1563),USE(?String78),FONT(,8),TRN
                         STRING('Email:'),AT(156,2344),USE(?String34:2),FONT(,8),TRN
                         STRING(@s15),AT(2667,2031),USE(invoice_telephone_number_temp),FONT(,8),LEFT,TRN
                         STRING('Fax: '),AT(2198,2188),USE(?String35:2),FONT(,8),TRN
                         STRING(@s15),AT(2667,2188),USE(invoice_fax_number_temp),FONT(,8),LEFT,TRN
                         STRING(@s15),AT(2656,1563),USE(job:Account_Number,,?job:Account_Number:2),FONT(,8),LEFT,TRN
                         STRING(@s15),AT(156,3240),USE(job:Order_Number),FONT(,8),TRN
                         STRING(@s30),AT(4083,1563),USE(delivery_Company_Name_temp),FONT(,8),TRN
                         STRING(@s50),AT(469,2344,3125,208),USE(invoice_EMail_Address),FONT(,8),TRN
                         STRING(@s30),AT(4083,1719,1917,156),USE(Delivery_Address1_Temp),FONT(,8),TRN
                         STRING(@s30),AT(4083,1875),USE(Delivery_address2_temp),FONT(,8),TRN
                         STRING(@s30),AT(4083,2031),USE(Delivery_address3_temp),FONT(,8),TRN
                         STRING(@s30),AT(4083,2188),USE(Delivery_address4_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1563),USE(invoice_company_name_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1719),USE(Invoice_address1_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,2031),USE(invoice_address3_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1875),USE(invoice_address2_temp),FONT(,8),TRN
                       END
EndOfReportBreak       BREAK(EndOfReport),USE(?unnamed:4)
DETAIL                   DETAIL,AT(0,0,,5646),USE(?DetailBand),FONT('Tahoma',8),TOGETHER
                           STRING(@s30),AT(229,0,1000,156),USE(job:Model_Number),FONT(,8),LEFT,TRN
                           STRING(@s30),AT(1677,0,1323,156),USE(job:Manufacturer),FONT(,8),LEFT,TRN
                           STRING(@s30),AT(3198,0,1396,156),USE(job:Unit_Type),FONT(,8),LEFT,TRN
                           STRING(@s30),AT(4708,0,1396,156),USE(job:ESN),FONT(,8),LEFT,TRN
                           STRING(@s30),AT(6219,0),USE(job:MSN),FONT(,8),TRN
                           STRING(@s15),AT(6219,365),USE(exchange_msn_temp),FONT(,8),TRN
                           STRING(@s16),AT(4708,365,1396,156),USE(exchange_esn_temp),FONT(,8),LEFT,TRN
                           STRING(@s30),AT(3198,365,1396,156),USE(exchange_unit_type_temp),FONT(,8),LEFT,TRN
                           STRING(@s30),AT(1677,365,1323,156),USE(exchange_manufacturer_temp),FONT(,8),LEFT,TRN
                           STRING(@s30),AT(229,365,1000,156),USE(exchange_model_number),FONT(,8),LEFT,TRN
                           STRING(@s15),AT(229,208),USE(exchange_unit_title_temp),FONT(,8,,FONT:bold),LEFT,TRN
                           STRING(@s20),AT(1354,208),USE(exchange_unit_number_temp),LEFT,TRN
                           STRING('ACCESSORIES'),AT(156,1042),USE(?String63),FONT(,8,,FONT:bold),TRN
                           TEXT,AT(1927,1042,5313,365),USE(tmp:accessories),FONT(,7),TRN
                           STRING('EXTERNAL DAMAGE CHECK LIST'),AT(156,1458),USE(?ExternalDamageCheckList),FONT('Tahoma', |
  8,COLOR:Black,FONT:bold,CHARSET:ANSI),TRN
                           STRING('Notes:'),AT(5990,1458),USE(?Notes),FONT(,7),TRN
                           TEXT,AT(6302,1458,1250,677),USE(jobe2:XNotes),FONT(,6),TRN
                           CHECK('None'),AT(5365,1458,625,156),USE(jobe2:XNone),TRN
                           CHECK('Keypad'),AT(156,1615,729,156),USE(jobe2:XKeypad),TRN
                           CHECK('Charger'),AT(2146,1615,781,156),USE(jobe2:XCharger),TRN
                           CHECK('Antenna'),AT(2146,1448,833,156),USE(jobe2:XAntenna),TRN
                           CHECK('Lens'),AT(2938,1448,573,156),USE(jobe2:XLens),TRN
                           CHECK('F/Cover'),AT(3563,1448,781,156),USE(jobe2:XFCover),TRN
                           CHECK('B/Cover'),AT(4448,1448,729,156),USE(jobe2:XBCover),TRN
                           CHECK('Battery'),AT(990,1615,729,156),USE(jobe2:XBattery),TRN
                           CHECK('LCD'),AT(2938,1615,625,156),USE(jobe2:XLCD),TRN
                           CHECK('System Connector'),AT(4448,1615,1250,156),USE(jobe2:XSystemConnector),TRN
                           CHECK('Sim Reader'),AT(3563,1604,885,156),USE(jobe2:XSimReader),TRN
                           STRING('Initial Transit Type: '),AT(156,4740,990,156),USE(?String66),FONT(,8),TRN
                           STRING(@s20),AT(1198,4740),USE(job:Transit_Type),FONT(,8,,FONT:bold),LEFT,TRN
                           STRING('Courier: '),AT(156,4896),USE(?String67),FONT(,8),TRN
                           STRING(@s15),AT(1198,4896,1302,188),USE(job:Incoming_Courier),FONT(,8,,FONT:bold),TRN
                           STRING('Consignment No:'),AT(156,5052),USE(?String70),FONT(,8),TRN
                           STRING(@s15),AT(1198,5052,1302,188),USE(job:Incoming_Consignment_Number),FONT(,8,,FONT:bold), |
  TRN
                           STRING('Customer ID No:'),AT(5573,5156),USE(?CustomerIDNo),FONT(,8),TRN
                           STRING('Date Received: '),AT(156,5208),USE(?String68),FONT(,8),TRN
                           STRING(@d6b),AT(1198,5208),USE(job:Incoming_Date),FONT(,8,,FONT:bold),TRN
                           STRING('Received By: '),AT(156,5365),USE(?String69),FONT(,8),TRN
                           STRING(@s15),AT(1198,5365),USE(Received_By_Temp),FONT(,8,,FONT:bold),TRN
                           STRING('REPORTED FAULT'),AT(156,573),USE(?String64),FONT(,8,,FONT:bold),TRN
                           TEXT,AT(1927,573,5313,417),USE(jbn:Fault_Description),FONT('Arial',7,,,CHARSET:ANSI),TRN
                           STRING(@s24),AT(3281,5365,1760,240),USE(esn_temp),FONT(,8,,FONT:bold),CENTER,TRN
                           STRING('Booking Option:'),AT(5573,5313),USE(?String122),FONT(,8,,FONT:bold),TRN
                           STRING(@s30),AT(6563,5313,1563,156),USE(tmp:BookingOption),FONT(,8),TRN
                           STRING(@s13),AT(6563,5156),USE(jobe2:IDNumber),FONT(,8),TRN
                           STRING(@s20),AT(3281,4958,1760,198),USE(job_number_temp),FONT(,8,,FONT:bold),CENTER,TRN
                           STRING(@s20),AT(2865,5156,2552,208),USE(barcodeIMEINumber),FONT('C39 High 12pt LJ3',12),CENTER, |
  COLOR(COLOR:White),TRN
                           STRING(@s20),AT(2865,4740,2552,208),USE(barcodeJobNumber),FONT('C39 High 12pt LJ3',12),CENTER, |
  COLOR(COLOR:White)
                           STRING('Amount:'),AT(5573,4948),USE(?amount_text),FONT(,8),TRN
                           STRING('<128>'),AT(6510,4948,52,208),USE(?Euro),FONT(,8,,FONT:bold),HIDE,TRN
                           STRING(@n-14.2b),AT(6615,4948),USE(Amount_Received_Temp),FONT(,8,,FONT:bold),TRN
                           STRING('Loan Deposit Paid'),AT(5573,4740),USE(?LoanDepositPaidTitle),FONT(,8,,,CHARSET:ANSI), |
  HIDE,TRN
                           STRING(@n-14.2b),AT(6667,4740),USE(tmp:LoanDepositPaid),FONT(,8,,FONT:bold,CHARSET:ANSI),HIDE, |
  TRN
                           TEXT,AT(208,2125,7333,2417),USE(stt:Text),TRN
                         END
                       END
                       FOOTER,AT(396,10156,7521,1156),USE(?unnamed:2)
                         STRING('Estimate Required If Repair Costs Exceed:'),AT(156,63),USE(?estimate_text),FONT(,8), |
  HIDE,TRN
                         STRING(@s70),AT(2344,52,4115,260),USE(estimate_value_temp),FONT(,8),HIDE,TRN
                         TEXT,AT(83,83,10,10),USE(tmp:StandardText),FONT(,8),HIDE
                         STRING('LOAN UNIT ISSUED'),AT(156,271),USE(?LoanUnitIssued),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  HIDE,TRN
                         STRING(@s30),AT(1906,271),USE(tmp:LoanModel),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),LEFT, |
  HIDE,TRN
                         STRING('IMEI'),AT(3969,271),USE(?IMEITitle),FONT('Arial',9,,FONT:bold,CHARSET:ANSI),HIDE,TRN
                         STRING(@s30),AT(4292,271),USE(tmp:LoanIMEI),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),LEFT, |
  HIDE,TRN
                         STRING('LOAN ACCESSORIES'),AT(156,458),USE(?LoanAccessoriesTitle),FONT('Arial',8,,FONT:bold, |
  CHARSET:ANSI),HIDE,TRN
                         STRING(@s255),AT(1906,458,5417,208),USE(tmp:LoanAccessories),FONT('Arial',8,,FONT:regular, |
  CHARSET:ANSI),LEFT,HIDE,TRN
                         STRING('Customer Signature'),AT(156,875),USE(?String82),FONT(,,,FONT:bold),TRN
                         STRING('Date: {17}/ {18}/'),AT(5135,875,2156),USE(?String83),FONT(,,,FONT:bold),TRN
                         LINE,AT(1406,1042,3677,0),USE(?Line1),COLOR(COLOR:Black)
                         LINE,AT(5573,1042,1521,0),USE(?Line2),COLOR(COLOR:Black)
                         TEXT,AT(156,615,5885,260),USE(locTermsText),FONT(,7)
                       END
                       FORM,AT(250,250,7750,11188),USE(?Text:CurrencyItemCost:2),FONT('Tahoma',8,,FONT:regular)
                         STRING('JOB RECEIPT'),AT(5917,0,1521,240),USE(?String20),FONT(,16,,FONT:bold),RIGHT,TRN
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),FONT(,14,,FONT:bold),LEFT
                         STRING(@s40),AT(104,313,3073,208),USE(address:Name2),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,208,3073,208),USE(address:Name),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,417,2760,208),USE(address:Location),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING('REG NO:'),AT(104,573),USE(?stringREGNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(625,573,1667,208),USE(address:RegistrationNo),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('VAT NO: '),AT(104,677),USE(?stringVATNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s20),AT(625,677,1771,156),USE(address:VatNumber),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,833,2240,156),USE(address:AddressLine1),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,938,2240,156),USE(address:AddressLine2),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1042,2240,156),USE(address:AddressLine3),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1146),USE(address:AddressLine4),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('TEL:'),AT(104,1250),USE(?stringTEL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(469,1250,1458,208),USE(address:Telephone),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('FAX:'),AT(2083,1250,313,208),USE(?stringFAX),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(2396,1250,1510,188),USE(address:Fax),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('EMAIL:'),AT(104,1354,417,208),USE(?stringEMAIL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s255),AT(469,1354,3333,208),USE(address:EmailAddress),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('JOB DETAILS'),AT(5000,208),USE(?String57),FONT(,8,,FONT:bold),TRN
                         STRING('INVOICE ADDRESS'),AT(156,1510),USE(?String24),FONT(,8,,FONT:bold),TRN
                         STRING('DELIVERY ADDRESS'),AT(4115,1510,1677,156),USE(?DeliveryAddress),FONT(,8,,FONT:bold), |
  TRN
                         STRING('GOODS RECEIVED DETAILS'),AT(156,8563),USE(?String73),FONT(,8,,FONT:bold),TRN
                         STRING('PAYMENT RECEIVED (INC. V.A.T.)'),AT(5552,8563),USE(?String73:2),FONT(,8,,FONT:bold), |
  TRN
                         STRING('REPAIR DETAILS'),AT(156,3677),USE(?String50),FONT(,8,,FONT:bold),TRN
                         BOX,AT(5000,365,2646,1042),USE(?Box:TopDetails),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,1667,3604,1302),USE(?Box:Address1),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4063,1667,3604,1302),USE(?Box:Address2),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3125,1510,260),USE(?Box:Title1),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(1615,3125,1510,260),USE(?Box:Title2),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(3125,3125,1510,260),USE(?Box:Title3),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4635,3125,1510,260),USE(?Box:Title4),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(6146,3125,1510,260),USE(?Box:Title5),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3385,1510,260),USE(?Box:Title1a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(1615,3385,1510,260),USE(?Box:Title2a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(3125,3385,1510,260),USE(?Box:Title3a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4635,3385,1510,260),USE(?Box:Title4a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(6146,3385,1510,260),USE(?Box:Title5a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3802,7552,260),USE(?Box:Heading),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,4063,7552,4479),USE(?Box:Detail),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,8698,2552,1042),USE(?Box:Total1),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(5521,8698,2135,1042),USE(?Box:Total2),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR1               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('JobReceipt')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  tmp:JobNumber = p_web.GetSessionValue('tmp:JobNumber')
  Relate:EXCHANGE.Open                                     ! File EXCHANGE used by this procedure, so make sure it's RelationManager is open
  Relate:INVOICE.SetOpenRelated()
  Relate:INVOICE.Open                                      ! File INVOICE used by this procedure, so make sure it's RelationManager is open
  Relate:JOBEXACC.Open                                     ! File JOBEXACC used by this procedure, so make sure it's RelationManager is open
  Relate:JOBPAYMT.Open                                     ! File JOBPAYMT used by this procedure, so make sure it's RelationManager is open
  Relate:JOBS2_ALIAS.Open                                  ! File JOBS2_ALIAS used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:JOBNOTES.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBACC.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANUFACT.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:MANFAULO.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBOUTFL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOAN.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOANACC.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  ! #12084 New standard texts (Bryan: 17/05/2011)
  Access:STANTEXT.Clearkey(stt:Description_Key)
  stt:Description = 'JOB RECEIPT'
  IF (Access:STANTEXT.TryFetch(stt:Description_Key))
  END
  locTermsText = p_web.GSV('Default:TermsText')
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('JobReceipt',ProgressWindow)                ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisReport.AddSortOrder(job:Ref_Number_Key)
  ThisReport.AddRange(job:Ref_Number,tmp:JobNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:INVOICE.Close
    Relate:JOBEXACC.Close
    Relate:JOBPAYMT.Close
    Relate:JOBS2_ALIAS.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('JobReceipt',ProgressWindow)             ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'JobReceipt',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.Init(Report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,Report{PROPPRINT:Paper},PAPER:USER), CHOOSE(Report{PROP:Thous}=True,PROP:Thous,CHOOSE(Report{PROP:MM}=True,PROP:MM,CHOOSE(Report{PROP:Points}=True,PROP:Points,0))), Report{PROPPRINT:PaperWidth}, Report{PROPPRINT:PaperHeight}, Report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetTitle('Job Receipt')              !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR1.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetViewerPrefs(PDFXTR1:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetFontEmbedding(True, True, False, False, False)
    PDFXTR1.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'JobReceipt',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR1.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = job:Ref_Number
  If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Found
      Access:TRADeACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = wob:HeadAccountNumber
      If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          !Found
          tmp:Ref_Number = job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
      Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
  
  Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
  
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = job:Ref_Number
  If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Found
  Else ! If AccESS:JOBE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBE.TryFetch(jobe:RefNumberKey) = Level:Benign
  
  Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
  jobe2:RefNumber = job:Ref_Number
  If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
  
  Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
  jbn:RefNumber = job:Ref_Number
  If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
  
   Access:STANTEXT.ClearKey(stt:Description_Key)
   stt:Description = 'JOB RECEIPT'
   If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
       !Found
       tmp:StandardText = stt:Text
   Else ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
       !Error
   End ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
  
  Access:DEFAULTS.Clearkey(def:RecordNumberKEy)
  def:Record_Number = 1
  Set(def:RecordNumberKEy,def:RecordNumberKEy)
  Loop ! Begin Loop
      If Access:DEFAULTS.Next()
          Break
      End ! If Access:DEFAULTS.Next()
      Break
  End ! Loop
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  If (jobe:WebJob = 1)
      tra:Account_Number = wob:HeadAccountNumber
  ELSE ! If (glo:WebJob = 1)
      tra:Account_Number = GETINI('BOOKING','HeadAccount','AA20',CLIP(PATH())&'\SB2KDEF.INI')
  END ! If (glo:WebJob = 1)
  If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
      address:SiteName        = tra:Company_Name
      address:Name            = tra:coTradingName
      address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
      address:Location        = tra:coLocation
      address:RegistrationNo  = tra:coRegistrationNo
      address:AddressLine1    = tra:coAddressLine1
      address:AddressLine2    = tra:coAddressLine2
      address:AddressLine3    = tra:coAddressLine3
      address:AddressLine4    = tra:coAddressLine4
      address:Telephone       = tra:coTelephoneNumber
      address:Fax             = tra:coFaxNumber
      address:EmailAddress    = tra:coEmailAddress
      address:VatNumber       = tra:coVATNumber
  END
  
  Access:USERS.ClearKey(use:Password_Key)
  use:Password = job:Despatch_User
  If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
      !Found
      Received_By_Temp = CLip(use:Forename) & ' ' & Clip(use:Surname)
  Else ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
      !Error
  End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
  
  Case jobe:Booking48HourOption
  Of 1
      tmp:BookingOption = '48 Hour Exchange'
  Of 2
      tmp:BookingOption = 'ARC Repair'
  Of 3
      tmp:BookingOption = '7 Day TAT'
  Else
      tmp:BookingOption = 'N/A'
  End ! Case jobe:Booking48HourOption
  
  If Clip(job:Title) = '' And Clip(job:Initial) = ''
      Customer_Name_Temp = job:Surname
  End ! If Clip(job:Title) = '' And Clip(job:Initial) = ''
  If Clip(job:Title) = '' And Clip(job:Initial) <> ''
      CUstomer_Name_Temp = Clip(job:Initial) & ' ' & Clip(job:Surname)
  End ! If Clip(job:Title) = '' And Clip(job:Initial) <> ''
  If Clip(job:Title) <> '' ANd CLip(job:Initial) <> ''
      Customer_Name_Temp = CLip(job:Title) & ' ' & Clip(job:Initial) & ' ' & Clip(job:Surname)
  End ! If Clip(job:Title) <> '' ANd CLip(job:Initial) <> ''
  
  If Customer_Name_Temp <> ''
      ClientName = 'Client: ' & Clip(CUstomer_Name_Temp)
      If jobe:EndUserTelNo <> ''
          CLientName = Clip(ClientName) & '  Tel: ' & jobe:EndUSerTelNo
      End ! If jobe:EndUserTelNo <> ''
  ELse
      If jobe:EndUserTelNo <> ''
          CLientName = 'Tel: ' & jobe:EndUSerTelNo
      End ! If jobe:EndUserTelNo <> ''
  End ! If Customer_Name_Temp <> ''
  
  delivery_Company_Name_temp = job:Company_Name_Delivery
  delivery_address1_temp     = job:address_line1_delivery
  delivery_address2_temp     = job:address_line2_delivery
  If job:address_line3_delivery   = ''
      delivery_address3_temp = job:postcode_delivery
      delivery_address4_temp = ''
  Else
      delivery_address3_temp = job:address_line3_delivery
      delivery_address4_temp = job:postcode_delivery
  End ! If
  delivery_Telephone_temp = job:Telephone_Delivery
  
  ! ---- Not relevant ----
  ! Deleting: DBH 29/01/2009 #10661
  !If jobe:WebJob
  !    SetTarget(Report)
  !    ?DeliveryAddress{prop:Text} = 'CUSTOMER ADDRESS'
  !    SetTarget()
  !    Invoice_Address1_Temp = tra:Address_Line1
  !    Invoice_ADdress2_Temp = tra:Address_Line2
  !    Invoice_Address3_Temp = tra:Address_Line3
  !    Invoice_Address4_temp = tra:Postcode
  !    Invoice_Company_Name_Temp = tra:Company_Name
  !    Invoice_Telephone_Number_Temp = tra:Telephone_Number
  !    Invoice_Fax_Number_Temp = tra:Fax_Number
  !    Invoice_EMail_Address = tra:EmailAddress
  !Else ! If jobe:WebJob
  
  ! End: DBH 29/01/2009 #10661
  ! -----------------------------------------
      Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
      sub:Account_Number = job:Account_Number
      If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
          !Found
          Access:TRADEACC.ClearKey(tra:Account_Number_Key)
          tra:Account_Number = sub:Main_Account_Number
          If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
              !Found
              If tra:Invoice_Sub_Accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
                  If sub:Invoice_Customer_Address= 'YES'
                      Invoice_Address1_Temp = job:Address_Line1
                      Invoice_ADdress2_Temp = job:Address_Line2
                      Invoice_Address3_Temp = job:Address_Line3
                      Invoice_Address4_temp = job:Postcode
                      Invoice_Company_Name_Temp = job:Company_Name
                      Invoice_Telephone_Number_Temp = job:Telephone_Number
                      Invoice_Fax_Number_Temp = job:Fax_Number
                      Invoice_EMail_Address = jobe:EndUserEmailAddress
                  Else ! If sub:Invoice_Customer_Address= 'YES'
                      Invoice_Address1_Temp = sub:Address_Line1
                      Invoice_ADdress2_Temp = sub:Address_Line2
                      Invoice_Address3_Temp = sub:Address_Line3
                      Invoice_Address4_temp = sub:Postcode
                      Invoice_Company_Name_Temp = sub:Company_Name
                      Invoice_Telephone_Number_Temp = sub:Telephone_Number
                      Invoice_Fax_Number_Temp = sub:Fax_Number
                      Invoice_EMail_Address = sub:EmailAddress
                  End ! If sub:Invoice_Customer_Address= 'YES'
              Else ! If tra:Invoice_Sub_Accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
                  If tra:invoice_Customer_Address = 'YES'
                      Invoice_Address1_Temp = job:Address_Line1
                      Invoice_ADdress2_Temp = job:Address_Line2
                      Invoice_Address3_Temp = job:Address_Line3
                      Invoice_Address4_temp = job:Postcode
                      Invoice_Company_Name_Temp = job:Company_Name
                      Invoice_Telephone_Number_Temp = job:Telephone_Number
                      Invoice_Fax_Number_Temp = job:Fax_Number
                      Invoice_EMail_Address = jobe:EndUserEmailAddress
                  Else ! If sub:Invoice_Customer_Address= 'YES'
                      Invoice_Address1_Temp = tra:Address_Line1
                      Invoice_ADdress2_Temp = tra:Address_Line2
                      Invoice_Address3_Temp = tra:Address_Line3
                      Invoice_Address4_temp = tra:Postcode
                      Invoice_Company_Name_Temp = tra:Company_Name
                      Invoice_Telephone_Number_Temp = tra:Telephone_Number
                      Invoice_Fax_Number_Temp = tra:Fax_Number
                      Invoice_EMail_Address = tra:EmailAddress
                  End ! If tra:invoice_Customer_Address = 'YES'
              End ! If tra:Invoice_Sub_Accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
          Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
              !Error
          End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
      Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
          !Error
      End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
  
  !End ! If jobe:WebJob
  
  If job:Exchange_Unit_Number > 0
      Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
      xch:Ref_Number = job:Exchange_Unit_Number
      If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
          !Found
          Exchange_Unit_Number_Temp = 'Unit: ' & job:Exchange_Unit_Number
          Exchange_Model_Number    = xch:Model_NUmber
          Exchange_Manufacturer_Temp = xch:Manufacturer
          Exchange_Unit_Type_Temp = 'N/A'
          Exchange_ESN_Temp = xch:ESN
          Exchange_Unit_Title_Temp = 'EXCHANGE UNIT'
          Exchange_MSN_Temp = xch:MSN
      Else ! If Access:EXCHANGE.TryFetch(xch:Ref_NumberKey) = Level:Benign
          !Error
      End ! If Access:EXCHANGE.TryFetch(xch:Ref_NumberKey) = Level:Benign
  End ! If job:Exchange_Unit_Number > 0
  
  tmp:Accessories = ''
  Access:JOBACC.Clearkey(jac:DamagedKey)
  jac:Ref_Number = job:Ref_Number
  jac:Damaged = 0
  Set(jac:DamagedKey,jac:DamagedKey)
  Loop ! Begin Loop
      If Access:JOBACC.Next()
          Break
      End ! If Access:JOBACC.Next()
      If jac:Ref_Number <> job:Ref_Number
          Break
      End ! If jac:Ref_Number <> job:Ref_Number
  
      If jac:Damaged = 1
          jac:Accessory = 'DAMAGED ' & Clip(jac:Accessory)
      End ! If jac:Damaged = 1
  
      If tmp:Accessories = ''
          tmp:Accessories = jac:Accessory
      Else ! If tmp:Accessories = ''
          tmp:Accessories = Clip(tmp:Accessories) & ', ' & jac:Accessory
      End ! If tmp:Accessories = ''
  End ! Loop
  
  If job:Estimate = 'YES'
      Settarget(Report)
      ?Estimate_Text{prop:Hide} = 0
      ?Estimate_Value_Temp{prop:Hide} = 0
      Estimate_Value_Temp = Clip(Format(job:Estimate_If_Over,@n14.2)) & ' (Plus VAT)'
      SetTarget()
  End ! If job:Estimate = 'YES'
  
  If job:Chargeable_Job = 'YES'
      Access:JOBPAYMT.Clearkey(jpt:All_Date_Key)
      jpt:Ref_Number = job:Ref_Number
      Set(jpt:All_Date_Key,jpt:All_Date_Key)
      Loop ! Begin Loop
          If Access:JOBPAYMT.Next()
              Break
          End ! If Access:JOBPAYMT.Next()
          If jpt:Ref_Number <> job:Ref_Number
              Break
          End ! If jpt:Ref_Number <> job:Ref_Number
          Amount_Received_Temp += jpt:Amount
      End ! Loop
  Else ! If job:Chargeable_Job = 'YES'
      SetTarget(Report)
      ?Amount_Text{prop:Hide} = 1
      ?job:Charge_Type{prop:Hide} = 1
      ?String40:3{prop:Hide} = 1
      SetTarget()
  End ! If job:Chargeable_Job = 'YES'
  
  tmp:LoanModel = ''
  tmp:LoanIMEI = ''
  tmp:LoanAccessories = ''
  
  If job:Loan_Unit_Number > 0
      Access:LOAN.ClearKey(loa:Ref_Number_Key)
      loa:Ref_Number = job:Loan_Unit_Number
      If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
          !Found
          tmp:LoanModel = Clip(loa:Manufacturer) & ' ' & loa:Model_Number
          tmp:LoanIMEI = loa:ESN
  
          Access:LOANACC.Clearkey(lac:Ref_Number_Key)
          lac:Ref_Number = loa:Ref_Number
          Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
          Loop ! Begin Loop
              If Access:LOANACC.Next()
                  Break
              End ! If Access:LOANACC.Next()
              If lac:Ref_Number <> loa:Ref_Number
                  Break
              End ! If lac:Ref_Number <> loa:Ref_Number
              If lac:Accessory_Status <> 'ISSUE'
                  Cycle
              End ! If lac:Accessory_Status <> 'ISSUE'
              If tmp:LoanAccessories = ''
                  tmp:LoanAccessories = lac:Accessory
              Else ! If tmp:LoanAccessories = ''
                  tmp:LoanAccessories = Clip(tmp:LoanAccessories) & ', ' & lac:Accessory
              End ! If tmp:LoanAccessories = ''
          End ! Loop
  
          Access:STANTEXT.ClearKey(stt:Description_Key)
          stt:Description = 'LOAN DISCLAIMER'
          If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
              !Found
              tmp:StandardText = Clip(tmp:StandardText) & '<13,10>' & stt:Text
          Else ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
              !Error
          End ! If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
  
      Else ! If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
          !Error
      End ! If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
  
  End ! If job:Loan_Unit_Number > 0
  
  If tmp:LoanModel <> ''
      SetTarget(Report)
      ?LoanUnitIssued{prop:Hide} = 0
      ?tmp:LoanModel{prop:Hide} = 0
      ?IMEITitle{prop:Hide} = 0
      ?tmp:LoanIMEI{prop:Hide} = 0
      ?LoanAccessoriesTitle{prop:Hide} = 0
      ?tmp:LOanAccessories{prop:Hide} = 0
  End ! If tmp:LoanModel <> ''
  
  tmp:LoanDepositPaid = 0
  
  Access:JOBPAYMT.Clearkey(jpt:Loan_Deposit_Key)
  jpt:Ref_Number = job:Ref_Number
  jpt:Loan_Deposit = 1
  Set(jpt:Loan_Deposit_Key,jpt:Loan_Deposit_Key)
  Loop ! Begin Loop
      If Access:JOBPAYMT.Next()
          Break
      End ! If Access:JOBPAYMT.Next()
      If jpt:Ref_Number <> job:Ref_Number
          Break
      End ! If jpt:Ref_Number <> job:Ref_Number
      If jpt:Loan_Deposit <> 1
          Break
      End ! If jpt:Loan_Deposit <> 1
      tmp:LoanDepositPaid += jpt:Amount
  End ! Loop
  
  If tmp:LoanDepositPaid > 0
      SetTarget(Report)
      ?LoanDepositPaidTitle{prop:Hide} = 0
      ?tmp:LoanDepositPaid{prop:Hide} = 0
      SetTarget()
  End ! If tmp:LoanDepositPaid > 0
  
  job_number_temp      = 'Job No: ' & job:ref_number
  esn_temp             = 'I.M.E.I.: ' & Clip(job:esn)
  barcodeJobNumber = '*' & clip(job:Ref_Number) & '*'
  barcodeIMEINumber = '*' & clip(job:ESN) & '*'
  
  !!Barcode Bit
  !code_temp   = 3
  !option_temp = 0
  !
  !bar_code_string_temp = job:ref_number
  !job_number_temp      = 'Job No: ' & job:ref_number
  !SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  !
  !bar_code_string_temp = Clip(job:esn)
  !esn_temp             = 'I.M.E.I.: ' & Clip(job:esn)
  !SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  !
  !SetTarget(Report)
  !DR_JobNo.Init256()
  !DR_JobNo.RenderWholeStrings = 1
  !DR_JobNo.Resize(DR_JobNo.Width * 5, DR_JobNo.Height * 5)
  !DR_JobNo.Blank(Color:White)
  !DR_JobNo.FontName = 'C128 High 12pt LJ3'
  !DR_JobNo.FontStyle = font:Regular
  !DR_JobNo.FontSize = 48
  !DR_JobNo.Show(0,0,Bar_Code_Temp)
  !DR_JobNo.Display()
  !DR_JobNo.Kill256()
  !
  !DR_IMEI.Init256()
  !DR_IMEI.RenderWholeStrings = 1
  !DR_IMEI.Resize(DR_IMEI.Width * 5, DR_IMEI.Height * 5)
  !DR_IMEI.Blank(Color:White)
  !DR_IMEI.FontName = 'C128 High 12pt LJ3'
  !DR_IMEI.FontStyle = font:Regular
  !DR_IMEI.FontSize = 48
  !DR_IMEI.Show(0,0,Bar_Code2_Temp)
  !DR_IMEI.Display()
  !DR_IMEI.Kill256()
  !SetTarget()
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR1:rtn = PDFXTR1.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR1:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

