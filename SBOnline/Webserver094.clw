

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER094.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ForceIncomingCourier PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !

  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Incoming Courier
    If (def:Force_Incoming_Courier = 'B' And func:Type = 'B') Or |
        (def:Force_Incoming_Courier <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Incoming_Courier = 'B'
    Return Level:Benign
