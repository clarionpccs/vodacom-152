

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER153.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
AccessoryCheck       PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
    !Validate Accessories
    ! Variables Passed:
    ! AccessoryCheck:Type
    ! AccessoryCheck:RefNumber
    
    ! Returned Value
! AccessoryCheck:Return

    do OpenFiles
    Case p_web.GSV('AccessoryCheck:Type')
    of 'LOA'
        error# = 0
        Access:LOANACC.Clearkey(lac:Ref_Number_Key)
        lac:Ref_Number    = p_web.GSV('AccessoryCheck:RefNumber')
        set(lac:Ref_Number_Key,lac:Ref_Number_Key)
        loop
            if (Access:LOANACC.Next())
                Break
            end ! if (Access:LOANACC.Next())
            if (lac:Ref_Number    <> p_web.GSV('AccessoryCheck:RefNumber'))
                Break
            end ! if (lac:Ref_Number    <> p_web.GSV('tmp:LoanUnitNumber'))
    
    
            Access:TAGFILE.Clearkey(tag:keyTagged)
            tag:sessionID    = p_web.sessionID
            tag:taggedValue  = lac:accessory
            if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
                ! Found
                if (tag:tagged <> 1)
                    error# = 1
                    break
                end ! if (tag:tagged <> 1)
            else ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
                ! Error
                error# = 1
                break
            end ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
        end ! loop
    
        if (error# = 0)
            Access:TAGFILE.Clearkey(tag:keyTagged)
            tag:sessionID    = p_web.sessionID
            set(tag:keyTagged,tag:keyTagged)
            loop
                if (Access:TAGFILE.Next())
                    Break
                end ! if (Access:TAGFILE.Next())
                if (tag:sessionID    <> p_web.sessionID)
                    Break
                end ! if (tag:sessionID    <> p_web.sessionID)
                if (tag:Tagged = 0)
                    cycle
                end ! if (tag:Tagged = 0)
                Access:LOANACC.Clearkey(lac:ref_number_Key)
                lac:ref_number    = p_web.GSV('AccessoryCheck:RefNumber')
                lac:accessory    = tag:taggedValue
                if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
                    ! Found
                else ! if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
                    ! Error
                    error# = 2
                    break
                end ! if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
            end ! loop
        end ! if (error# = 0)
    Else
        error# = 0
        Access:JOBACC.Clearkey(jac:Ref_Number_Key)
        jac:Ref_Number    = p_web.GSV('AccessoryCheck:RefNumber')
        set(jac:Ref_Number_Key,jac:Ref_Number_Key)
        loop
            if (Access:JOBACC.Next())
                Break
            end ! if (Access:LOANACC.Next())
            if (jac:Ref_Number    <> p_web.GSV('AccessoryCheck:RefNumber'))
                Break
            end ! if (lac:Ref_Number    <> p_web.GSV('tmp:LoanUnitNumber'))
    
            if (p_web.GSV('BookingSite') <> 'RRC')
                if (NOT jac:Attached)
                    Cycle
                End
            End
    
            Access:TAGFILE.Clearkey(tag:keyTagged)
            tag:sessionID    = p_web.sessionID
            tag:taggedValue  = jac:accessory
            if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
                ! Found
                if (tag:tagged <> 1)
                    error# = 1
                    break
                else
                    if (p_web.GSV('BookingSite') <> 'RRC' AND NOT jac:Attached)
                        error# = 1
                        break
                    End
                end ! if (tag:tagged <> 1)
            else ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
                ! Error
                error# = 1
                break
            end ! if (Access:TAGFILE.TryFetch(tag:keyTagged) = Level:Benign)
        end ! loop
    
        if (error# = 0)
            Access:TAGFILE.Clearkey(tag:keyTagged)
            tag:sessionID    = p_web.sessionID
            set(tag:keyTagged,tag:keyTagged)
            loop
                if (Access:TAGFILE.Next())
                    Break
                end ! if (Access:TAGFILE.Next())
                if (tag:sessionID    <> p_web.sessionID)
                    Break
                end ! if (tag:sessionID    <> p_web.sessionID)
                if (tag:Tagged = 0)
                    cycle
                end ! if (tag:Tagged = 0)
                Access:JOBACC.Clearkey(jac:ref_number_Key)
                jac:ref_number    = p_web.GSV('AccessoryCheck:RefNumber')
                jac:accessory    = tag:taggedValue
                if (Access:JOBACC.TryFetch(jac:ref_number_Key) = Level:Benign)
                    ! Found
                else ! if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
                    ! Error
                    error# = 2
                    break
                end ! if (Access:LOANACC.TryFetch(lac:ref_number_Key) = Level:Benign)
            end ! loop
        end ! if (error# = 0)
    End
    
    do CloseFiles

    p_web.SSV('AccessoryCheck:Return',error#)
    ! 1 = Missing
    ! 2 = Mismatch
    ! Else = All Fine
!--------------------------------------
OpenFiles  ROUTINE
  Access:TagFile.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TagFile.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBACC.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBACC.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:LOANACC.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOANACC.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TagFile.Close
     Access:JOBACC.Close
     Access:LOANACC.Close
     FilesOpened = False
  END
