

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER412.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER409.INC'),ONCE        !Req'd for module callout resolution
                     END


WarrantyClaimPaidCriteria PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locWarrantyManufacturer STRING(30)                         !
locPaidDateRange     LONG                                  !
locPaidStartDate     DATE                                  !
locPaidEndDate       DATE                                  !
FilesOpened     Long
MANUFACT::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
locWarrantyManufacturer_OptionView   View(MANUFACT)
                          Project(man:Manufacturer)
                          Project(man:Manufacturer)
                        End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('WarrantyClaimPaidCriteria')
  loc:formname = 'WarrantyClaimPaidCriteria_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'WarrantyClaimPaidCriteria',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('WarrantyClaimPaidCriteria','')
    p_web._DivHeader('WarrantyClaimPaidCriteria',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferWarrantyClaimPaidCriteria',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWarrantyClaimPaidCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWarrantyClaimPaidCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_WarrantyClaimPaidCriteria',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWarrantyClaimPaidCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_WarrantyClaimPaidCriteria',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'WarrantyClaimPaidCriteria',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(MANUFACT)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(MANUFACT)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('WarrantyClaimPaidCriteria_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('locPaidStartDate')
    p_web.SetPicture('locPaidStartDate','@d06b')
  End
  p_web.SetSessionPicture('locPaidStartDate','@d06b')
  If p_web.IfExistsValue('locPaidEndDate')
    p_web.SetPicture('locPaidEndDate','@d06b')
  End
  p_web.SetSessionPicture('locPaidEndDate','@d06b')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locWarrantyManufacturer',locWarrantyManufacturer)
  p_web.SetSessionValue('locPaidDateRange',locPaidDateRange)
  p_web.SetSessionValue('locPaidStartDate',locPaidStartDate)
  p_web.SetSessionValue('locPaidEndDate',locPaidEndDate)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locWarrantyManufacturer')
    locWarrantyManufacturer = p_web.GetValue('locWarrantyManufacturer')
    p_web.SetSessionValue('locWarrantyManufacturer',locWarrantyManufacturer)
  End
  if p_web.IfExistsValue('locPaidDateRange')
    locPaidDateRange = p_web.GetValue('locPaidDateRange')
    p_web.SetSessionValue('locPaidDateRange',locPaidDateRange)
  End
  if p_web.IfExistsValue('locPaidStartDate')
    locPaidStartDate = p_web.dformat(clip(p_web.GetValue('locPaidStartDate')),'@d06b')
    p_web.SetSessionValue('locPaidStartDate',locPaidStartDate)
  End
  if p_web.IfExistsValue('locPaidEndDate')
    locPaidEndDate = p_web.dformat(clip(p_web.GetValue('locPaidEndDate')),'@d06b')
    p_web.SetSessionValue('locPaidEndDate',locPaidEndDate)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('WarrantyClaimPaidCriteria_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    p_web.SSV('locPaidStartDate',DATE(MONTH(TODAY()),1,YEAR(TODAY())))
    p_web.SSV('locPaidEndDate',TODAY())
    IF (p_web.GSV('locWarrantyManufacturerFilter') <> 1)
        ! Don't prefill manufacturer if filter hasn't been sent previously
        p_web.SSV('locWarrantyManufacturer','')
    END ! IF
    p_web.SSV('locPaidDateRange',0)
    
    ClearTagFile(p_web)
    
    p_web.SSV('jow:RecordNumber',0) ! Clear record pointer
      p_web.site.SaveButton.TextValue = 'OK'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locWarrantyManufacturer = p_web.RestoreValue('locWarrantyManufacturer')
 locPaidDateRange = p_web.RestoreValue('locPaidDateRange')
 locPaidStartDate = p_web.RestoreValue('locPaidStartDate')
 locPaidEndDate = p_web.RestoreValue('locPaidEndDate')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'WarrantyBatchProcessing'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('WarrantyClaimPaidCriteria_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('WarrantyClaimPaidCriteria_ChainTo')
    loc:formaction = p_web.GetSessionValue('WarrantyClaimPaidCriteria_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'WarrantyClaims'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="WarrantyClaimPaidCriteria" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="WarrantyClaimPaidCriteria" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="WarrantyClaimPaidCriteria" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Select Accepted Date Range') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Select Accepted Date Range',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_WarrantyClaimPaidCriteria">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_WarrantyClaimPaidCriteria" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_WarrantyClaimPaidCriteria')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Criteria') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_WarrantyClaimPaidCriteria')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_WarrantyClaimPaidCriteria'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locWarrantyManufacturer')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_WarrantyClaimPaidCriteria')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Criteria') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_WarrantyClaimPaidCriteria_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWarrantyManufacturer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWarrantyManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locWarrantyManufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPaidDateRange
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPaidDateRange
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locPaidDateRange
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPaidStartDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPaidStartDate
      do Comment::locPaidStartDate
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::lookup_startdate
      do Comment::lookup_startdate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPaidEndDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPaidEndDate
      do Comment::locPaidEndDate
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::lookup_enddate
      do Comment::lookup_enddate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locWarrantyManufacturer  Routine
  p_web._DivHeader('WarrantyClaimPaidCriteria_' & p_web._nocolon('locWarrantyManufacturer') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Manufacturer')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locWarrantyManufacturer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWarrantyManufacturer',p_web.GetValue('NewValue'))
    locWarrantyManufacturer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWarrantyManufacturer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locWarrantyManufacturer',p_web.GetValue('Value'))
    locWarrantyManufacturer = p_web.GetValue('Value')
  End
  If locWarrantyManufacturer = ''
    loc:Invalid = 'locWarrantyManufacturer'
    loc:alert = p_web.translate('Manufacturer') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::locWarrantyManufacturer
  do SendAlert

Value::locWarrantyManufacturer  Routine
  p_web._DivHeader('WarrantyClaimPaidCriteria_' & p_web._nocolon('locWarrantyManufacturer') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locWarrantyManufacturer')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locWarrantyManufacturer = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locWarrantyManufacturer'',''warrantyclaimpaidcriteria_locwarrantymanufacturer_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locWarrantyManufacturer',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locWarrantyManufacturer') = 0
    p_web.SetSessionValue('locWarrantyManufacturer','')
  end
    packet = clip(packet) & p_web.CreateOption('','',choose('' = p_web.getsessionvalue('locWarrantyManufacturer')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locWarrantyManufacturer_OptionView)
  locWarrantyManufacturer_OptionView{prop:order} = 'UPPER(man:Manufacturer)'
  Set(locWarrantyManufacturer_OptionView)
  Loop
    Next(locWarrantyManufacturer_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locWarrantyManufacturer') = 0
      p_web.SetSessionValue('locWarrantyManufacturer',man:Manufacturer)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(man:Manufacturer,man:Manufacturer,choose(man:Manufacturer = p_web.getsessionvalue('locWarrantyManufacturer')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locWarrantyManufacturer_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(MANUFACT)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaimPaidCriteria_' & p_web._nocolon('locWarrantyManufacturer') & '_value')

Comment::locWarrantyManufacturer  Routine
    loc:comment = p_web.Translate('Required')
  p_web._DivHeader('WarrantyClaimPaidCriteria_' & p_web._nocolon('locWarrantyManufacturer') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locPaidDateRange  Routine
  p_web._DivHeader('WarrantyClaimPaidCriteria_' & p_web._nocolon('locPaidDateRange') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Select Date Range')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locPaidDateRange  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPaidDateRange',p_web.GetValue('NewValue'))
    locPaidDateRange = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPaidDateRange
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPaidDateRange',p_web.GetValue('Value'))
    locPaidDateRange = p_web.GetValue('Value')
  End
  do Value::locPaidDateRange
  do SendAlert
  do Prompt::locPaidStartDate
  do Value::locPaidStartDate  !1
  do Comment::locPaidStartDate
  do Value::lookup_startdate  !1

Value::locPaidDateRange  Routine
  p_web._DivHeader('WarrantyClaimPaidCriteria_' & p_web._nocolon('locPaidDateRange') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locPaidDateRange
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locPaidDateRange')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locPaidDateRange') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locPaidDateRange'',''warrantyclaimpaidcriteria_locpaiddaterange_value'',1,'''&clip(0)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPaidDateRange')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locPaidDateRange',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locPaidDateRange_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Jobs Between Date Range') & '<13,10>'
    packet = clip(packet) & p_web.br
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locPaidDateRange') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locPaidDateRange'',''warrantyclaimpaidcriteria_locpaiddaterange_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPaidDateRange')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locPaidDateRange',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locPaidDateRange_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Jobs Before Date') & '<13,10>'
    packet = clip(packet) & p_web.br
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaimPaidCriteria_' & p_web._nocolon('locPaidDateRange') & '_value')

Comment::locPaidDateRange  Routine
    loc:comment = ''
  p_web._DivHeader('WarrantyClaimPaidCriteria_' & p_web._nocolon('locPaidDateRange') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locPaidStartDate  Routine
  p_web._DivHeader('WarrantyClaimPaidCriteria_' & p_web._nocolon('locPaidStartDate') & '_prompt',Choose(p_web.GSV('locPaidDateRange') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Start Date')
  If p_web.GSV('locPaidDateRange') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaimPaidCriteria_' & p_web._nocolon('locPaidStartDate') & '_prompt')

Validate::locPaidStartDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPaidStartDate',p_web.GetValue('NewValue'))
    locPaidStartDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPaidStartDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPaidStartDate',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    locPaidStartDate = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  If locPaidStartDate = ''
    loc:Invalid = 'locPaidStartDate'
    loc:alert = p_web.translate('Start Date') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::locPaidStartDate
  do SendAlert

Value::locPaidStartDate  Routine
  p_web._DivHeader('WarrantyClaimPaidCriteria_' & p_web._nocolon('locPaidStartDate') & '_value',Choose(p_web.GSV('locPaidDateRange') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locPaidDateRange') = 1)
  ! --- STRING --- locPaidStartDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locPaidStartDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locPaidStartDate = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPaidStartDate'',''warrantyclaimpaidcriteria_locpaidstartdate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locPaidStartDate',p_web.GetSessionValue('locPaidStartDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaimPaidCriteria_' & p_web._nocolon('locPaidStartDate') & '_value')

Comment::locPaidStartDate  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('WarrantyClaimPaidCriteria_' & p_web._nocolon('locPaidStartDate') & '_comment',Choose(p_web.GSV('locPaidDateRange') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locPaidDateRange') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaimPaidCriteria_' & p_web._nocolon('locPaidStartDate') & '_comment')

Validate::lookup_startdate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('lookup_startdate',p_web.GetValue('NewValue'))
    do Value::lookup_startdate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::locPaidStartDate  !1

Value::lookup_startdate  Routine
  p_web._DivHeader('WarrantyClaimPaidCriteria_' & p_web._nocolon('lookup_startdate') & '_value',Choose(p_web.GSV('locPaidDateRange') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locPaidDateRange') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('displayCalendar(locPaidStartDate,''dd/mm/yyyy'',this)')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','lookup_startdate','...','LookupButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaimPaidCriteria_' & p_web._nocolon('lookup_startdate') & '_value')

Comment::lookup_startdate  Routine
    loc:comment = ''
  p_web._DivHeader('WarrantyClaimPaidCriteria_' & p_web._nocolon('lookup_startdate') & '_comment',Choose(p_web.GSV('locPaidDateRange') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locPaidDateRange') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locPaidEndDate  Routine
  p_web._DivHeader('WarrantyClaimPaidCriteria_' & p_web._nocolon('locPaidEndDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('End Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locPaidEndDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPaidEndDate',p_web.GetValue('NewValue'))
    locPaidEndDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPaidEndDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPaidEndDate',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    locPaidEndDate = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  If locPaidEndDate = ''
    loc:Invalid = 'locPaidEndDate'
    loc:alert = p_web.translate('End Date') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::locPaidEndDate
  do SendAlert

Value::locPaidEndDate  Routine
  p_web._DivHeader('WarrantyClaimPaidCriteria_' & p_web._nocolon('locPaidEndDate') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locPaidEndDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locPaidEndDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locPaidEndDate = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPaidEndDate'',''warrantyclaimpaidcriteria_locpaidenddate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locPaidEndDate',p_web.GetSessionValue('locPaidEndDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaimPaidCriteria_' & p_web._nocolon('locPaidEndDate') & '_value')

Comment::locPaidEndDate  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('WarrantyClaimPaidCriteria_' & p_web._nocolon('locPaidEndDate') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::lookup_enddate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('lookup_enddate',p_web.GetValue('NewValue'))
    do Value::lookup_enddate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::locPaidEndDate  !1

Value::lookup_enddate  Routine
  p_web._DivHeader('WarrantyClaimPaidCriteria_' & p_web._nocolon('lookup_enddate') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('displayCalendar(locPaidEndDate,''dd/mm/yyyy'',this)')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','lookup_enddate','...','LookupButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WarrantyClaimPaidCriteria_' & p_web._nocolon('lookup_enddate') & '_value')

Comment::lookup_enddate  Routine
    loc:comment = ''
  p_web._DivHeader('WarrantyClaimPaidCriteria_' & p_web._nocolon('lookup_enddate') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('WarrantyClaimPaidCriteria_locWarrantyManufacturer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWarrantyManufacturer
      else
        do Value::locWarrantyManufacturer
      end
  of lower('WarrantyClaimPaidCriteria_locPaidDateRange_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPaidDateRange
      else
        do Value::locPaidDateRange
      end
  of lower('WarrantyClaimPaidCriteria_locPaidStartDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPaidStartDate
      else
        do Value::locPaidStartDate
      end
  of lower('WarrantyClaimPaidCriteria_lookup_startdate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::lookup_startdate
      else
        do Value::lookup_startdate
      end
  of lower('WarrantyClaimPaidCriteria_locPaidEndDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPaidEndDate
      else
        do Value::locPaidEndDate
      end
  of lower('WarrantyClaimPaidCriteria_lookup_enddate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::lookup_enddate
      else
        do Value::lookup_enddate
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('WarrantyClaimPaidCriteria_form:ready_',1)
  p_web.SetSessionValue('WarrantyClaimPaidCriteria_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_WarrantyClaimPaidCriteria',0)

PreCopy  Routine
  p_web.SetValue('WarrantyClaimPaidCriteria_form:ready_',1)
  p_web.SetSessionValue('WarrantyClaimPaidCriteria_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_WarrantyClaimPaidCriteria',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('WarrantyClaimPaidCriteria_form:ready_',1)
  p_web.SetSessionValue('WarrantyClaimPaidCriteria_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('WarrantyClaimPaidCriteria:Primed',0)

PreDelete       Routine
  p_web.SetValue('WarrantyClaimPaidCriteria_form:ready_',1)
  p_web.SetSessionValue('WarrantyClaimPaidCriteria_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('WarrantyClaimPaidCriteria:Primed',0)
  p_web.setsessionvalue('showtab_WarrantyClaimPaidCriteria',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('WarrantyClaimPaidCriteria_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('WarrantyClaimPaidCriteria_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
        If locWarrantyManufacturer = ''
          loc:Invalid = 'locWarrantyManufacturer'
          loc:alert = p_web.translate('Manufacturer') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      If not (p_web.GSV('locPaidDateRange') = 1)
        If locPaidStartDate = ''
          loc:Invalid = 'locPaidStartDate'
          loc:alert = p_web.translate('Start Date') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
        If locPaidEndDate = ''
          loc:Invalid = 'locPaidEndDate'
          loc:alert = p_web.translate('End Date') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
    IF (p_web.GSV('locPaidDateRange') = 0)
        IF (p_web.GSV('locPaidStartDate') > p_web.GSV('locPaidEndDate'))
            loc:Alert = 'Invalid Date Range'
            loc:Invalid = 'locPaidEndDate'
        END ! IF
    ELSE
        p_web.SSV('locPaidStartDate',DEFORMAT('01/01/2000',@d06))
    END!  IF 
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('WarrantyClaimPaidCriteria:Primed',0)
  p_web.StoreValue('locWarrantyManufacturer')
  p_web.StoreValue('locPaidDateRange')
  p_web.StoreValue('locPaidStartDate')
  p_web.StoreValue('')
  p_web.StoreValue('locPaidEndDate')
  p_web.StoreValue('')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locWarrantyManufacturer',locWarrantyManufacturer) ! STRING(30)
     p_web.SSV('locPaidDateRange',locPaidDateRange) ! LONG
     p_web.SSV('locPaidStartDate',locPaidStartDate) ! DATE
     p_web.SSV('locPaidEndDate',locPaidEndDate) ! DATE
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locWarrantyManufacturer = p_web.GSV('locWarrantyManufacturer') ! STRING(30)
     locPaidDateRange = p_web.GSV('locPaidDateRange') ! LONG
     locPaidStartDate = p_web.GSV('locPaidStartDate') ! DATE
     locPaidEndDate = p_web.GSV('locPaidEndDate') ! DATE
