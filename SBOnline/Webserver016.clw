

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER016.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Line500_XML          PROCEDURE  (NetwebServerWorker p_web, STRING pType) ! Declare Procedure
                    MAP
CreateXML               PROCEDURE(STRING pType, STRING pRepoDir)
                    END

MESSAGE_DATA          GROUP(),PRE(riv)				
TRANS_DESC                      STRING(50)
MAIN_RETAIL_AC                  STRING(20)
SUB_RETAIL_AC                   STRING(20)
FRANCHISE_BRANCH            STRING(20)
SB_REPAIR_NO                STRING(20)
BILL_AC_NO                  STRING(10)
SB_INV_NO                       STRING(10)
AMT_EXCL_VAT                    STRING(19)
VAT_RATE                        STRING(19)
COST                            STRING(19)
INV_DATE                        STRING(10)
INV_DUE_DATE                    STRING(10)
DESPATCH_DATE                   STRING(10)
DATE_REQUIRED                   STRING(10)
SERVICE_CODE                    STRING(20)
RESEND_YN                   STRING(1)

                            END!  GROUP


RIVCode                     STRING(3)				

RepositoryDir               CSTRING(255)
seq                         LONG
FilesOpened     BYTE(0)
    MAP
trace	PROCEDURE(STRING pText)	
    END

  CODE
!region ProcessedCode
        IF (GETINI('XML','CreateLine500',,Clip(Path()) & '\SB2KDEF.INI') <> 1)
            RETURN
        END ! IF

        RepositoryDir = GETINI('XML', 'RepositoryFolder',, Clip(Path()) & '\SB2KDEF.INI')
        
        trace('Create Line 500: ' & pType)
        trace('Create Line 500: ' & RepositoryDir)        

        IF ~(EXISTS(RepositoryDir))
            RETURN
        END ! IF

		! Get default for Service Code
        RIVCode = GETINI('XML','RIVDescription','RIV',Clip(Path()) & '\SB2KDEF.INI')

        DO OpenFiles

        LOOP 1 TIMES ! Break Loop
            
            IF (pType = 'RIV')

                Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
                inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
                IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign)
			
                ELSE ! IF
                    BREAK
                END !IF

                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = p_web.GSV('job:Ref_Number')
                IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
			
                ELSE ! IF
                END !IF

                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                jobe:RefNumber = job:Ref_Number
                IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
			
                ELSE ! IF
                END !IF

                Access:WEBJOB.ClearKey(wob:RefNumberKey)
                wob:RefNumber = job:Ref_Number
                IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
			
                ELSE ! IF
                END !IF

                Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
                sub:Account_Number = job:Account_Number
                IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
                    IF (sub:Generic_Account <> TRUE)
					! This is not a generic account. So the only message is for "Company Owned" franchises
					! and this produces an AOW
                        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                        tra:Account_Number = wob:HeadAccountNumber
                        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                            IF (tra:CompanyOwned = TRUE)
                                DO CreateAOWFile
                                BREAK
                            END ! IF
                        END !IF
                        BREAK
                    END ! IF
                ELSE ! IF
                END !IF

                DO CreateRIVFile
            END ! IF
            
        END ! Break Loop

        DO CloseFiles
!endregion        
CreateRIVFile       ROUTINE
    CLEAR(MESSAGE_DATA)		
    
    MESSAGE_DATA.TRANS_DESC = 'Retailer Invoice'

    Access:TRADEACC.ClearKey(tra:Account_Number_Key)
    tra:Account_Number = sub:Main_Account_Number
    IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
        MESSAGE_DATA.MAIN_RETAIL_AC = tra:Line500AccountNumber
    ELSE ! IF
    END !IF

    MESSAGE_DATA.SUB_RETAIL_AC = job:Account_Number
    MESSAGE_DATA.FRANCHISE_BRANCH = wob:HeadAccountNumber

    MESSAGE_DATA.SB_INV_NO = 'Z' & FORMAT(CLIP(inv:Invoice_Number) & CLIP(tra:BranchIdentification),@n09)
 
    MESSAGE_DATA.AMT_EXCL_VAT = LEFT(CLIP(ROUND(job:Invoice_Courier_Cost + | ! RRC Invoice Sub Total
        jobe:InvRRCCLabourCost +   |
        jobe:InvRRCCPartsCost, .01)))
    MESSAGE_DATA.VAT_RATE = LEFT(CLIP(ROUND(inv:Vat_Rate_Labour,.01)))
    MESSAGE_DATA.COST = LEFT(CLIP(ROUND(MESSAGE_DATA.AMT_EXCL_VAT + |
        (job:Invoice_Courier_Cost * (inv:Vat_Rate_Labour / 100) + |
        jobe:InvRRCCLabourCost * (inv:Vat_Rate_Parts / 100) +    |
        jobe:InvRRCCPartsCost * (inv:Vat_Rate_Labour / 100)), .01)))

    MESSAGE_DATA.INV_DATE = FORMAT(inv:RRCInvoiceDate,@d06b)
    MESSAGE_DATA.INV_DUE_DATE = FORMAT(inv:RRCInvoiceDate,@d06b)
    MESSAGE_DATA.DESPATCH_DATE = FORMAT(inv:RRCInvoiceDate,@d06b)
    MESSAGE_DATA.DATE_REQUIRED = FORMAT(inv:RRCInvoiceDate,@d06b)
    MESSAGE_DATA.SERVICE_CODE = RIVCode
    MESSAGE_DATA.RESEND_YN = 'N'
    
    CreateXML('RIV',RepositoryDir)
    
CreateAOWFile       ROUTINE
    CLEAR(MESSAGE_DATA)		
    
    MESSAGE_DATA.TRANS_DESC = 'ARC Out Of Warranty Repair'

    Access:TRADEACC.ClearKey(tra:Account_Number_Key)
    tra:Account_Number = wob:HeadAccountNumber
    IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
        MESSAGE_DATA.SB_INV_NO = 'Z' & FORMAT(CLIP(inv:Invoice_Number) & CLIP(tra:BranchIdentification),@n09)
        MESSAGE_DATA.SB_REPAIR_NO = CLIP(job:Ref_Number) & '-' & CLIP(tra:BranchIdentification) & CLIP(wob:JobNumber)
    END !IF		
    
    MESSAGE_DATA.BILL_AC_NO = SUB(sub:Line500AccountNumber, 1, 10) ! Booking Account Number
    
    MESSAGE_DATA.AMT_EXCL_VAT = LEFT(CLIP(ROUND(job:Invoice_Courier_Cost + | ! RRC Invoice Sub Total
        jobe:InvRRCCLabourCost +   |
        jobe:InvRRCCPartsCost, .01)))
    MESSAGE_DATA.VAT_RATE = LEFT(CLIP(ROUND(inv:Vat_Rate_Labour,.01)))
    MESSAGE_DATA.COST = LEFT(CLIP(ROUND(MESSAGE_DATA.AMT_EXCL_VAT + |
        (job:Invoice_Courier_Cost * (inv:Vat_Rate_Labour / 100) + | ! RRC Invoice VAT
        jobe:InvRRCCLabourCost * (inv:Vat_Rate_Parts / 100) +    |
        jobe:InvRRCCPartsCost * (inv:Vat_Rate_Labour / 100)), .01)))

    MESSAGE_DATA.INV_DATE = FORMAT(inv:RRCInvoiceDate,@d06b)
    MESSAGE_DATA.INV_DUE_DATE = FORMAT(inv:RRCInvoiceDate,@d06b)
    MESSAGE_DATA.DESPATCH_DATE = FORMAT(inv:RRCInvoiceDate,@d06b)
    MESSAGE_DATA.DATE_REQUIRED = FORMAT(inv:RRCInvoiceDate,@d06b)
    MESSAGE_DATA.SERVICE_CODE = 'AOW'
    MESSAGE_DATA.RESEND_YN = 'N'
    
    CreateXML('AOW',RepositoryDir)
!--------------------------------------
OpenFiles  ROUTINE
  Access:RETSALES.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:RETSALES.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:WEBJOB.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:WEBJOB.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:INVOICE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:INVOICE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:RETSALES.Close
     Access:WEBJOB.Close
     Access:JOBSE.Close
     Access:JOBS.Close
     Access:TRADEACC.Close
     Access:SUBTRACC.Close
     Access:INVOICE.Close
     FilesOpened = False
  END
CreateXML           PROCEDURE(STRING pT, STRING pRepoDir)
TheDate                 DATE
TheTime                 TIME
MsgID                   STRING(24)
    CODE
        TheDate = TODAY()
        TheTime = CLOCK()
        MsgID = 'MSG' & FORMAT(TheDate, @D012) & FORMAT(TheTime, @T05) & FORMAT(TheTime % 100, @N02) & FORMAT(seq, @N05)
        seq += 1
        IF (seq > 99999)	
            seq = 1
        END ! IF

        IF (XML:CreateXMLFile(CLIP(pRepoDir) & '\' & pT & '\' & MsgID & '.xml','1.0','UTF-8'))
			! ERROR
            RETURN
        END 
        XML:CreateParent('VODACOM_MESSAGE')
        XML:CreateAttribute('version','1.0')
        XML:AddParent()

        XML:CreateParent('MESSAGE_HEADER')		
        XML:CreateAttribute('direction','REQ')
        XML:AddParent()

        XML:AddElement('SRC_SYSTEM','')
        XML:AddElement('SRC_APPLICATION','')
        XML:AddElement('SERVICING_APPLICATION','')
        XML:AddElement('ACTION_CODE','UPD')
        XML:AddElement('SERVICE_CODE',pT)
        XML:AddElement('MESSAGE_ID','')
        XML:AddElement('USER_NAME','')
        XML:AddElement('TOKEN','')
        XML:CloseParent() ! MESSAGE_HEADER

        XML:CreateParent('MESSAGE_BODY')
        XML:AddParent()
        
        XML:CreateParent(CLIP(pT) & '_UPDATE_REQUEST')
        XML:CreateAttribute('version','1.0')
        XML:AddParent()
        
        XML:AddElement('TRANS_DESC',MESSAGE_DATA.TRANS_DESC)
        CASE pT
        OF 'AOW'
            XML:AddElement('BILL_AC_NO',MESSAGE_DATA.BILL_AC_NO)
            XML:AddElement('SB_REPAIR_NO',MESSAGE_DATA.SB_REPAIR_NO)
        OF 'RIV'
            XML:AddElement('MAIN_RETAIL_AC',MESSAGE_DATA.MAIN_RETAIL_AC)
            XML:AddElement('SUB_RETAIL_AC',MESSAGE_DATA.SUB_RETAIL_AC)
            XML:AddElement('FRANCHISE_BRANCH',MESSAGE_DATA.FRANCHISE_BRANCH)
        END ! CASE
        
        XML:AddElement('SB_INV_NO',MESSAGE_DATA.SB_INV_NO)
        XML:AddElement('AMT_EXCL_VAT',MESSAGE_DATA.AMT_EXCL_VAT)
        XML:AddElement('VAT_RATE',MESSAGE_DATA.VAT_RATE)
        XML:AddElement('COST',MESSAGE_DATA.COST)
        XML:AddElement('INV_DATE',MESSAGE_DATA.INV_DATE)
        XML:AddElement('INV_DUE_DATE',MESSAGE_DATA.INV_DUE_DATE)
        XML:AddElement('DESPATCH_DATE',MESSAGE_DATA.DESPATCH_DATE)
        XML:AddElement('DATE_REQUIRED',MESSAGE_DATA.DATE_REQUIRED)
        XML:AddElement('SERVICE_CODE',MESSAGE_DATA.SERVICE_CODE)
        XML:AddElement('RESEND_YN',MESSAGE_DATA.RESEND_YN)
        
        XML:CloseParent() ! MESSAGE_DATA
    
        XML:CloseParent() ! MESSAGE_BODY
        XML:CloseParent() ! VODACOM_MESSAGE

        XML:CloseXMLFile()
trace	PROCEDURE(STRING pText)
outString	CSTRING(1000)
    CODE
        RETURN ! Logging Disabled
