

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER437.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER227.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER230.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER438.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER439.INC'),ONCE        !Req'd for module callout resolution
                     END


frmExchangeReceiveProcess PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locIMEINumber        STRING(30)                            !
locScannedIMEINumber STRING(1000)                          !
qIMEI                QUEUE,PRE(qi)                         !
SessionID            LONG                                  !
IMEINumber           STRING(30)                            !
                     END                                   !
FilesOpened     Long
LOANHIST::State  USHORT
LOAN::State  USHORT
RETSALES::State  USHORT
EXCHHIST::State  USHORT
EXCHANGE::State  USHORT
TagFile::State  USHORT
RETSTOCK::State  USHORT
STOCKRECEIVETMP::State  USHORT
SBO_OutParts::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
locScannedIMEINumber_OptionView   View(SBO_OutParts)
                          Project(sout:PartNumber)
                          Project(sout:PartNumber)
                        End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('frmExchangeReceiveProcess')
  loc:formname = 'frmExchangeReceiveProcess_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'frmExchangeReceiveProcess',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('frmExchangeReceiveProcess','')
    p_web._DivHeader('frmExchangeReceiveProcess',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferfrmExchangeReceiveProcess',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmExchangeReceiveProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmExchangeReceiveProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_frmExchangeReceiveProcess',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferfrmExchangeReceiveProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_frmExchangeReceiveProcess',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'frmExchangeReceiveProcess',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(LOANHIST)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(RETSALES)
  p_web._OpenFile(EXCHHIST)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(TagFile)
  p_web._OpenFile(RETSTOCK)
  p_web._OpenFile(STOCKRECEIVETMP)
  p_web._OpenFile(SBO_OutParts)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(LOANHIST)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(RETSALES)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(RETSTOCK)
  p_Web._CloseFile(STOCKRECEIVETMP)
  p_Web._CloseFile(SBO_OutParts)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('frmExchangeReceiveProcess_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  p_web.SetSessionValue('locScannedIMEINumber',locScannedIMEINumber)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locIMEINumber')
    locIMEINumber = p_web.GetValue('locIMEINumber')
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  End
  if p_web.IfExistsValue('locScannedIMEINumber')
    locScannedIMEINumber = p_web.GetValue('locScannedIMEINumber')
    p_web.SetSessionValue('locScannedIMEINumber',locScannedIMEINumber)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('frmExchangeReceiveProcess_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  p_web.SSV('Error',0)
  p_web.SSV('locIMEINumber','')
  p_web.SSV('Comment:IMEINumber','')
  p_web.SSV('txtFinishedMessage','')
  
  IF (p_web.IfExistsValue('fLoan'))
        p_web.StoreValue('fLoan') ! Receive Loan Units
    ELSE
        p_web.SSV('fLoan','') ! Clear Variable incase it has already been set
  END
  
  
  ClearSBOPartsList(p_web)
    IF (CheckIfTagged(p_web) = 0)
        CreateScript(p_web,packet,'alert("You have not tagged any parts");window.open("FormStockReceive","_self")')
        DO SendPacket
        EXIT
      !p_web.SSV('Error',1)
  ELSE
      found# = 0
  
      ! Add items to be scanned to temp file (DBH: 29/11/2011)
        Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
        ret:Invoice_Number = p_web.GSV('locInvoiceNumber')
        IF (Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign)
            IF (ret:Account_Number <> p_web.GSV('BookingStoresAccount'))
                CreateScript(p_web,packet,'alert("The selected invoice is not for you site");window.open("FormStockReceive","_self")')
                DO SendPacket
                EXIT
            END ! IF
        ELSE ! IF
            CreateScript(p_web,packet,'alert("Unable to find the selected Invoice Number");window.open("FormStockReceive","_self")')
            DO SendPacket
            EXIT
        END ! IF   
        
        p_web.FileToSessionQueue(RETSALES)
  
      Access:RETSTOCK.Clearkey(res:DespatchedPartKey)
      res:Ref_Number = p_web.GSV('ret:Ref_Number')
      res:Despatched = 'YES'
      SET(res:DespatchedPartKey,res:DespatchedPartKey)
      LOOP UNTIL Access:RETSTOCK.Next()
          IF (res:Ref_Number <> p_web.GSV('ret:Ref_Number') OR |
              res:Despatched <> 'YES')
              BREAK
          END
          IF (res:Received = 1)
              CYCLE
          END
  
          Access:TagFile.ClearKey(tag:keyTagged)
          tag:sessionID = p_web.SessionID
          SET(tag:keyTagged,tag:keyTagged)
          LOOP UNTIL Access:TagFile.Next()
              IF (tag:sessionID <> p_web.SessionID)
                  BREAK
              END
              IF (tag:tagged = 1)
                  Access:STOCKRECEIVETMP.Clearkey(stotmp:RecordNumberKey)
                  stotmp:RecordNumber = tag:taggedValue
                  IF (Access:STOCKRECEIVETMP.TryFetch(stotmp:RecordNumberKey) = Level:Benign)
                      IF (stotmp:PartNumber = res:Part_Number)
                          IF (p_web.GSV('fLoan') = 1)
                              Access:LOAN.ClearKey(loa:Ref_Number_Key)
                              loa:Ref_Number = res:LoanRefNumber
                              IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
                                  IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
                                      sout:SessionID = p_web.SessionID
                                      sout:PartNumber = loa:ESN
                                      sout:Description = loa:Ref_Number
                                      sout:LineType = ''
                                      IF (Access:SBO_OutParts.TryInsert())
                                          Access:SBO_OutParts.CancelAutoInc()
                                      END
                                      found# = 1
                                  END
                              END
  
                          ELSE
  
                              Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                              xch:Ref_Number = res:ExchangeRefNumber
                              IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                                  IF (Access:SBO_OutParts.PrimeRecord() = Level:Benign)
                                      sout:SessionID = p_web.SessionID
                                      sout:PartNumber = xch:ESN
                                      sout:Description = xch:Ref_Number
                                      sout:LineType = ''
                                      IF (Access:SBO_OutParts.TryInsert())
                                          Access:SBO_OutParts.CancelAutoInc()
                                      END
                                      found# = 1
                                  END
                              END ! IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                          END
  
                      END ! IF (stotmp:PartNumber = res:Part_Number)
                  END !IF (Access:STOCKRECEIVETMP.TryFetch(stotmp:RecordNumberKey) = Level:Benign)
              END
          END ! Loop i# = 1 TO RECORDS(glo:Queue)
      END
  
  END
  
    IF (found# = 0)
        CreateScript(p_web,packet,'alert("No items were processed.");window.open("FormStockReceive","_self")')
        DO SendPacket
        EXIT
        p_web.SSV('Error',1)
    END
  
      p_web.site.SaveButton.TextValue = 'OK'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locIMEINumber = p_web.RestoreValue('locIMEINumber')
 locScannedIMEINumber = p_web.RestoreValue('locScannedIMEINumber')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'PageProcess?ProcessType=BuildStockReceiveList&ReturnURL=FormStockReceive'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('frmExchangeReceiveProcess_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('frmExchangeReceiveProcess_ChainTo')
    loc:formaction = p_web.GetSessionValue('frmExchangeReceiveProcess_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'FormStockReceive'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="frmExchangeReceiveProcess" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="frmExchangeReceiveProcess" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="frmExchangeReceiveProcess" ></input><13,10>'
  end

  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_frmExchangeReceiveProcess">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_frmExchangeReceiveProcess" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_frmExchangeReceiveProcess')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Scanned IMEIs To Receive') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_frmExchangeReceiveProcess')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_frmExchangeReceiveProcess'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locIMEINumber')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_frmExchangeReceiveProcess')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Scanned IMEIs To Receive') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_frmExchangeReceiveProcess_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Scanned IMEIs To Receive')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Scanned IMEIs To Receive')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Scanned IMEIs To Receive')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Scanned IMEIs To Receive')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locScannedIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locScannedIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locScannedIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::txtFinishedMessage
      do Comment::txtFinishedMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locIMEINumber  Routine
  p_web._DivHeader('frmExchangeReceiveProcess_' & p_web._nocolon('locIMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('IMEI Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('NewValue'))
    locIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('Value'))
    locIMEINumber = p_web.GetValue('Value')
  End
    locIMEINumber = Upper(locIMEINumber)
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
    !p_web.SSV('Comment:IMEINumber','')
    Access:SBO_OutParts.ClearKey(sout:PartNumberKey)
    sout:SessionID = p_web.SessionID
    sout:LineType = 'P' ! Already processed
    sout:PartNumber = p_web.GSV('locIMEINumber')
    IF (Access:SBO_OutParts.TryFetch(sout:PartNumberKey) = Level:Benign)
        
        !p_web.SSV('Comment:IMEINumber','The selected IMEI Number has already been processed')
        loc:alert = 'The selected IMEI Number has already been processed'
        loc:invalid = 'locIMEINumber'
    ELSE
        Access:SBO_OutParts.ClearKey(sout:PartNumberKey)
        sout:SessionID = p_web.SessionID
        sout:LineType = '' ! Already processed
        sout:PartNumber = p_web.GSV('locIMEINumber')
        IF (Access:SBO_OutParts.TryFetch(sout:PartNumberKey))
            !p_web.SSV('Comment:IMEINumber','The selected IMEI Number has not been tagged')
            loc:alert = 'The selected I.M.E.I. does not match any of the tagged items on this invoice.'
            loc:invalid = 'locIMEINumber'
        ELSE
            IF (p_web.GSV('fLoan') = 1)
                Access:LOAN.Clearkey(loa:Ref_Number_Key)
                loa:Ref_Number  = sout:Description
                IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
                    loa:Available = 'AVL'
                    loa:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                    IF (Access:LOAN.TryUpdate() = Level:Benign)
                        Access:RETSTOCK.Clearkey(res:LoanRefNumberKey)
                        res:Ref_Number = p_web.GSV('ret:Ref_Number')
                        res:LoanRefNumber = loa:Ref_Number
                        IF (Access:RETSTOCK.Tryfetch(res:ExchangeRefNumberKey) = Level:Benign)
                            res:Received = 1
                            res:DateReceived = TODAY()
                            IF (Access:RETSTOCK.TryUpdate() = Level:Benign)
  
                            END !IF (Access:RETSTOCK.TryUpdate() = Level:Benign)
                        END ! IF (Access:RETSTOCK.Tryfetch(res:ExchangeRefNumberKey) = Level:Benign)
                        IF (Access:LOANHIST.PrimeRecord() = Level:Benign)
                            loh:Ref_Number     = xch:Ref_Number
                            loh:Date        = TODAY()
                            loh:Time        = CLOCK()
  !                            Access:USERS.Clearkey(use:Password_Key)
  !                            use:Password = glo:Password
  !                            IF (Access:USERS.TryFetch(use:Password_Key))
  !                            END
                            loh:User        = p_web.GSV('BookingUserCode') ! Do not use Global Vars!! (DBH: 21/11/2014) use:User_Code
                            loh:Status        = 'UNIT RECEIVED'
                            loh:Notes        = 'ORDER NUMBER: ' & CLIP(res:Purchase_Order_Number) & |
                                '<13,10>RETAIL INVOICE NUMBER: ' & p_web.GSV('ret:Invoice_Number') & |
                                '<13,10>PRICE: ' & Format(p_web.GSV('res:Item_Cost'),@n14.2)
  
                            IF (Access:LOANHIST.TryInsert())
                                Access:LOANHIST.CancelAutoInc()
                            END
                        END ! IF (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                        sout:LineType = 'P' ! Processed
                        Access:SBO_OutParts.TryUpdate()
                        p_web.SSV('locIMEINumber','')
                    END ! IF (Access:EXCHANGE.TryUpdate() = Level:Benign)
                END ! IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
            ELSE
                Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                xch:Ref_Number  = sout:Description
                IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                    xch:Available = 'AVL'
                    xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                    IF (Access:EXCHANGE.TryUpdate() = Level:Benign)
                        Access:RETSTOCK.Clearkey(res:ExchangeRefNumberKey)
                        res:Ref_Number = p_web.GSV('ret:Ref_Number')
                        res:ExchangeRefNumber = xch:Ref_Number
                        IF (Access:RETSTOCK.Tryfetch(res:ExchangeRefNumberKey) = Level:Benign)
                            res:Received = 1
                            res:DateReceived = TODAY()
                            IF (Access:RETSTOCK.TryUpdate() = Level:Benign)
  
                            END !IF (Access:RETSTOCK.TryUpdate() = Level:Benign)
                        END ! IF (Access:RETSTOCK.Tryfetch(res:ExchangeRefNumberKey) = Level:Benign)
                        IF (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                            exh:Ref_Number     = xch:Ref_Number
                            exh:Date        = TODAY()
                            exh:Time        = CLOCK()
  !                            Access:USERS.Clearkey(use:Password_Key)
  !                            use:Password = glo:Password
  !                            IF (Access:USERS.TryFetch(use:Password_Key))
  !                            END
                            exh:User        = p_web.GSV('BookingUserCode') ! Do not use Global Vars!! (DBH: 21/11/2014) use:User_Code
                            exh:Status        = 'UNIT RECEIVED'
                            exh:Notes        = 'ORDER NUMBER: ' & CLIP(res:Purchase_Order_Number) & |
                                '<13,10>RETAIL INVOICE NUMBER: ' & p_web.GSV('ret:Invoice_Number') & |
                                '<13,10>PRICE: ' & Format(p_web.GSV('res:Item_Cost'),@n14.2)
  
                            IF (Access:EXCHHIST.TryInsert())
                                Access:EXCHHIST.CancelAutoInc()
                            END
                        END ! IF (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                        sout:LineType = 'P' ! Processed
                        Access:SBO_OutParts.TryUpdate()
                        p_web.SSV('locIMEINumber','')
                    END ! IF (Access:EXCHANGE.TryUpdate() = Level:Benign)
                END ! IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
            END
  
        END
    END
  
  ! Any IMEI's left?
    found# = 0
    Access:SBO_OutParts.ClearKey(sout:PartNumberKey)
    sout:SessionID = p_web.SessionID
    sout:LineType = ''
    SET(sout:PartNumberKey,sout:PartNumberKey)
    LOOP UNTIL Access:SBO_OutParts.Next()
        IF (sout:SessionID <> p_web.SessionID OR |
            sout:LineType <> '')
            BREAK
        END
        found# = 1
        BREAK
    END
    IF (found# = 0)
        p_web.SSV('txtFinishedMessage','All I.M.E.I. Numbers Have Been Processed')
    END
  
  do Value::locIMEINumber
  do SendAlert
  do Value::locScannedIMEINumber  !1
  do Comment::locIMEINumber
  do Value::txtFinishedMessage  !1

Value::locIMEINumber  Routine
  p_web._DivHeader('frmExchangeReceiveProcess_' & p_web._nocolon('locIMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locIMEINumber'',''frmexchangereceiveprocess_locimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locIMEINumber',p_web.GetSessionValueFormat('locIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,30,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('frmExchangeReceiveProcess_' & p_web._nocolon('locIMEINumber') & '_value')

Comment::locIMEINumber  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:IMEINumber'))
  p_web._DivHeader('frmExchangeReceiveProcess_' & p_web._nocolon('locIMEINumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('frmExchangeReceiveProcess_' & p_web._nocolon('locIMEINumber') & '_comment')

Prompt::locScannedIMEINumber  Routine
  p_web._DivHeader('frmExchangeReceiveProcess_' & p_web._nocolon('locScannedIMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Processed IMEI Number(s)')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locScannedIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locScannedIMEINumber',p_web.GetValue('NewValue'))
    locScannedIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locScannedIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locScannedIMEINumber',p_web.GetValue('Value'))
    locScannedIMEINumber = p_web.GetValue('Value')
  End

Value::locScannedIMEINumber  Routine
  p_web._DivHeader('frmExchangeReceiveProcess_' & p_web._nocolon('locScannedIMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locScannedIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locScannedIMEINumber',loc:fieldclass,loc:readonly,10,174,0,loc:javascript,)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(LOANHIST)
  bind(loh:Record)
  p_web._OpenFile(LOAN)
  bind(loa:Record)
  p_web._OpenFile(RETSALES)
  bind(ret:Record)
  p_web._OpenFile(EXCHHIST)
  bind(exh:Record)
  p_web._OpenFile(EXCHANGE)
  bind(xch:Record)
  p_web._OpenFile(TagFile)
  bind(tag:Record)
  p_web._OpenFile(RETSTOCK)
  bind(res:Record)
  p_web._OpenFile(STOCKRECEIVETMP)
  bind(stotmp:Record)
  p_web._OpenFile(SBO_OutParts)
  bind(sout:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locScannedIMEINumber_OptionView)
  locScannedIMEINumber_OptionView{prop:filter} = 'sout:SessionID = ' & p_web.SessionID & ' AND UPPER(sout:LineType) = ''P'''
  locScannedIMEINumber_OptionView{prop:order} = 'UPPER(-sout:RecordNumber)'
  Set(locScannedIMEINumber_OptionView)
  Loop
    Next(locScannedIMEINumber_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locScannedIMEINumber') = 0
      p_web.SetSessionValue('locScannedIMEINumber',sout:PartNumber)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(sout:PartNumber,sout:PartNumber,choose(sout:PartNumber = p_web.getsessionvalue('locScannedIMEINumber')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locScannedIMEINumber_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(LOANHIST)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(RETSALES)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(RETSTOCK)
  p_Web._CloseFile(STOCKRECEIVETMP)
  p_Web._CloseFile(SBO_OutParts)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('frmExchangeReceiveProcess_' & p_web._nocolon('locScannedIMEINumber') & '_value')

Comment::locScannedIMEINumber  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('frmExchangeReceiveProcess_' & p_web._nocolon('locScannedIMEINumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::txtFinishedMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtFinishedMessage',p_web.GetValue('NewValue'))
    do Value::txtFinishedMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtFinishedMessage  Routine
  p_web._DivHeader('frmExchangeReceiveProcess_' & p_web._nocolon('txtFinishedMessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold large')&'">' & p_web.Translate(p_web.GSV('txtFinishedMessage'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('frmExchangeReceiveProcess_' & p_web._nocolon('txtFinishedMessage') & '_value')

Comment::txtFinishedMessage  Routine
    loc:comment = ''
  p_web._DivHeader('frmExchangeReceiveProcess_' & p_web._nocolon('txtFinishedMessage') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('frmExchangeReceiveProcess_locIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIMEINumber
      else
        do Value::locIMEINumber
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('frmExchangeReceiveProcess_form:ready_',1)
  p_web.SetSessionValue('frmExchangeReceiveProcess_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_frmExchangeReceiveProcess',0)

PreCopy  Routine
  p_web.SetValue('frmExchangeReceiveProcess_form:ready_',1)
  p_web.SetSessionValue('frmExchangeReceiveProcess_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_frmExchangeReceiveProcess',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('frmExchangeReceiveProcess_form:ready_',1)
  p_web.SetSessionValue('frmExchangeReceiveProcess_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('frmExchangeReceiveProcess:Primed',0)

PreDelete       Routine
  p_web.SetValue('frmExchangeReceiveProcess_form:ready_',1)
  p_web.SetSessionValue('frmExchangeReceiveProcess_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('frmExchangeReceiveProcess:Primed',0)
  p_web.setsessionvalue('showtab_frmExchangeReceiveProcess',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('frmExchangeReceiveProcess_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  !  ClearStocKReceiveList(p_web)
  !  ClearTagFile(p_web)
  !  BuildStockReceiveList(p_web,p_web.GSV('ret:Ref_Number'),p_web.GSV('ret:ExchangeOrder'),p_web.GSV('ret:LoanOrder'))
  p_web.DeleteSessionValue('frmExchangeReceiveProcess_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
          locIMEINumber = Upper(locIMEINumber)
          p_web.SetSessionValue('locIMEINumber',locIMEINumber)
        If loc:Invalid <> '' then exit.
          locScannedIMEINumber = Upper(locScannedIMEINumber)
          p_web.SetSessionValue('locScannedIMEINumber',locScannedIMEINumber)
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('frmExchangeReceiveProcess:Primed',0)
  p_web.StoreValue('locIMEINumber')
  p_web.StoreValue('locScannedIMEINumber')
  p_web.StoreValue('')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locIMEINumber',locIMEINumber) ! STRING(30)
     p_web.SSV('locScannedIMEINumber',locScannedIMEINumber) ! STRING(1000)
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locIMEINumber = p_web.GSV('locIMEINumber') ! STRING(30)
     locScannedIMEINumber = p_web.GSV('locScannedIMEINumber') ! STRING(1000)
