

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER270.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Despatch:Job         PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
    DO OpenFiles
    if (p_web.GSV('BookingSite') = 'RRC')
        p_web.SSV('GetStatus:Type','JOB')
        IF (p_web.GSV('job:Who_Booked') = 'WEB')
            p_web.SSV('GetStatus:StatusNumber',Sub(GETINI('RRC','StatusSentToPUP','464 DESPATCHED TO PUP',Clip(Path()) & '\SB2KDEF.INI'),1,3))                                        
                        
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'RRC'
                joc:DespatchTo = 'PUP'
                joc:Courier = p_web.GSV('job:Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'JOB'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
                        
            p_web.SSV('LocationChange:Location',Clip(GETINI('RRC','InTransitToPUPLocation','IN TRANSIT TO PUP',CLIP(PATH())&'\SB2KDEF.INI')))
        ELSE ! IF (p_web.GSV('job:Who_Booked') = 'WEB')
            IF (p_web.GSV('job:Paid') = 'YES' OR (p_web.GSV('job:Chargeable_Job') = 'YES' AND p_web.GSV('jobe:RRCCSubTotal') = 0))
                p_web.SSV('GetStatus:StatusNumber','910') 
            ELSE 
                p_web.SSV('GetStatus:StatusNumber','905')
            END
                        
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'RRC'
                joc:DespatchTo = 'CUSTOMER'
                joc:Courier = p_web.GSV('job:Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'JOB'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
            p_web.SSV('LocationChange:Location',Clip(GETINI('RRC','DespatchToCustomer','DESPATCHED',CLIP(PATH())&'\SB2KDEF.INI')))
        END
                    
        GetStatus(p_web.GSV('GetStatus:StatusNumber'),0,'JOB',p_web)
        LocationChange(p_web,p_web.GSV('LocationChange:Location'))
                    
        IF (p_web.GSV('job:Loan_Unit_Number') <> 0)
            p_web.SSV('GetStatus:StatusNumber',812)
            p_web.SSV('GetStatus:Type','LOA')
                        
            ! Loan Collection Note???
            ! MissingFor Noe
            !! 
        END
    
        ! Update Job
                
        p_web.SSV('wob:DateJobDespatched',TODAY())
        p_web.SSV('jobe:DespatchType','')
        p_web.SSV('jobe:Despatched','')
        p_web.SSV('wob:ReadyToDespatch',0)                
                
        ! Update Files
                   
        IF (p_web.GSV('jobe:VSACustomer') = 1)
            CID_XML(p_web.GSV('job:Mobile_Number'),p_web.GSV('wob:HeadAccountNumber'),2)                    
        END
                    
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = p_web.GSV('job:Account_Number')
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = sub:Main_Account_Number
            IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                IF (tra:Use_Sub_Accounts = 'YES')
                    IF (sub:Print_Despatch_Despatch = 'YES' AND |
                        sub:Despatch_Note_Per_Item = 'YES')
                        p_web.SSV('Hide:PrintDespatchNote',0)
                    END
                                    
                ELSE
                    IF (tra:Print_Despatch_Despatch = 'YES' AND |
                        tra:Despatch_Note_Per_Item = 'YES')
                        p_web.SSV('Hide:PrintDespatchNote',0)
                    END
                END
            END
        END
                
        p_web.SSV('AddToAudit:Action','DESPATCH FROM RRC')
        p_web.SSV('AddToAudit:Notes','COURIER: ' & p_web.GSV('job:Courier') & |
            '<13,10>WAYBILL NO: ' & p_web.GSV('locWaybillNumber'))
    
    ELSE ! if (p_web.GSV('BookingSite') = 'RRC')
        p_web.SSV('job:Date_Despatched',Today())
        p_web.SSV('job:Despatched','YES')
        p_web.SSV('job:Consignment_Number',p_web.GSV('locWaybillNumber'))
        if (p_web.GSV('jobe:WebJob') = 1)
            p_web.SSV('GetStatus:StatusNumber',Sub(GETINI('RRC','StatusDespatchedToRRC','454 DESPATCHED TO RRC',CLIP(PATH())&'\SB2KDEF.INI'),1,3))
            p_web.SSV('GetStatus:Type','JOB')
            GetStatus(Sub(GETINI('RRC','StatusDespatchedToRRC','454 DESPATCHED TO RRC',CLIP(PATH())&'\SB2KDEF.INI'),1,3),0,'JOB',p_web)
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'ARC'
                joc:DespatchTo = 'RRC'
                joc:Courier = p_web.GSV('job:Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'JOB'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
        ELSE !if (p_web.GSV('jobe:WebJob') = 1)
            ! Add To COnsignment History
            IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
                joc:RefNumber = p_web.GSV('job:Ref_Number')
                joc:TheDate = TODAY()
                joc:TheTime = CLOCK()
                joc:UserCode = p_web.GSV('BookingUserCode')
                joc:DespatchFrom = 'ARC'
                joc:DespatchTo = 'RRC'
                joc:Courier = p_web.GSV('job:Courier')
                joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
                joc:DespatchType = 'JOB'
                IF (Access:JOBSCONS.TryInsert())
                    Access:JOBSCONS.CancelAutoInc()
                END
            END
                    
            paid# = 1
            if (p_web.GSV('job:Chargeable_Job') = 'YES' AND p_web.GSV('job:Paid') <> 'YES')
                paid# = 0
            END
            IF (paid# = 1 AND p_web.GSV('job:Warranty_Job') = 'YES' AND p_web.GSV('job:Paid_Warranty') <> 'YES')
                paid# = 0
            END
            IF (paid# = 1)
                p_web.SSV('GetStatus:StatusNumber',910) ! Despatch Paid
                p_web.SSV('GetStatus:Type','JOB')
                GetStatus(910,0,'JOB',p_web) ! DEspatch Paid
            ELSE
                p_web.SSV('GetStatus:StatusNumber',905) ! Despatch UnPaid
                p_web.SSV('GetStatus:Type','JOB')
                GetStatus(905,0,'JOB',p_web) ! Despatch Unpaid
            END
                    
            if (p_web.GSV('job:Loan_Unit_Number') <> 0)
                p_web.SSV('GetStatus:StatusNumber',812)
                p_web.SSV('GetStatus:Type','LOA')
                GetStatus(812,0,'LOA',p_web)
            END
                    
                    
        end
        p_web.SSV('AddToAudit:Action','JOB DESPATCHED VIA ' & p_web.GSV('job:Courier'))
        p_web.SSV('AddToAudit:Notes','CONSIGNMENT NUMBER: ' & Clip(p_web.GSV('locWaybillNumber')))
    END ! if (p_web.GSV('BookingSite') = 'RRC')
            
    p_web.SSV('jobe:JobSecurityPackNo',p_web.GSV('locSecurityPackID'))
            
    ! Add To Audit
    p_web.SSV('AddToAudit:Type','JOB')
    IF (p_web.GSV('locSecurityPackID') <> '')
        p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & |
            '<13,10>SECURITY PACK NO: ' & p_web.GSV('locSecurityPackID'))
    END
    AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB',p_web.GSV('AddToAudit:Action'),p_web.GSV('AddToAudit:Notes'))
    
    DO CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TRADEACC.Close
     Access:SUBTRACC.Close
     Access:JOBSCONS.Close
     FilesOpened = False
  END
