

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER092.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ForceLocation        PROCEDURE  (func:TransitType,func:Workshop,func:Type) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !

  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Location
    access:trantype.clearkey(trt:transit_type_key)
    trt:transit_type = func:TransitType
    if access:trantype.fetch(trt:transit_type_key) = Level:Benign
        If trt:force_location = 'YES' and func:Workshop = 'YES'
            Return Level:Fatal
        End!If trt:force_location = 'YES' and job:location = ''
    end
    Return Level:Benign
