

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER492.INC'),ONCE        !Local module procedure declarations
                     END


FormChangeDOP        PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:DOP              DATE                                  !
tmp:NewDOP           DATE                                  !
tmp:ChangeReason     STRING(255)                           !
tmp:UserPassword     STRING(30)                            !
FilesOpened     Long
USERS::State  USHORT
ACCAREAS::State  USHORT
JOBS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormChangeDOP')
  loc:formname = 'FormChangeDOP_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormChangeDOP',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormChangeDOP','')
    p_web._DivHeader('FormChangeDOP',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormChangeDOP',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormChangeDOP',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormChangeDOP',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormChangeDOP',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormChangeDOP',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormChangeDOP',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormChangeDOP',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  p_web._OpenFile(ACCAREAS)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(ACCAREAS)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SSV('Comment:NewDOP','Required')
  p_web.SSV('Comment:UserPassword','Enter Password and press TAB')
  p_web.SSV('tmp:UserPassword','')
  p_web.SSV('Hide:ChangeDOPButton',1)
  p_web.SSV('local:PasswordValidated','')
  p_web.SSV('tmp:NewDOP','')
  p_web.SSV('tmp:ChangeReason','')
  
  if p_web.IfExistsValue('frompage')
      p_web.StoreValue('frompage')
  end ! if p_web.IfExistsValue('frompage')
  p_web.SetValue('FormChangeDOP_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:DOP')
    p_web.SetPicture('job:DOP','@d06b')
  End
  p_web.SetSessionPicture('job:DOP','@d06b')
  If p_web.IfExistsValue('tmp:NewDOP')
    p_web.SetPicture('tmp:NewDOP','@d06b')
  End
  p_web.SetSessionPicture('tmp:NewDOP','@d06b')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:UserPassword',tmp:UserPassword)
  p_web.SetSessionValue('job:DOP',job:DOP)
  p_web.SetSessionValue('tmp:NewDOP',tmp:NewDOP)
  p_web.SetSessionValue('tmp:ChangeReason',tmp:ChangeReason)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:UserPassword')
    tmp:UserPassword = p_web.GetValue('tmp:UserPassword')
    p_web.SetSessionValue('tmp:UserPassword',tmp:UserPassword)
  End
  if p_web.IfExistsValue('job:DOP')
    job:DOP = p_web.dformat(clip(p_web.GetValue('job:DOP')),'@d06b')
    p_web.SetSessionValue('job:DOP',job:DOP)
  End
  if p_web.IfExistsValue('tmp:NewDOP')
    tmp:NewDOP = p_web.dformat(clip(p_web.GetValue('tmp:NewDOP')),'@d06b')
    p_web.SetSessionValue('tmp:NewDOP',tmp:NewDOP)
  End
  if p_web.IfExistsValue('tmp:ChangeReason')
    tmp:ChangeReason = p_web.GetValue('tmp:ChangeReason')
    p_web.SetSessionValue('tmp:ChangeReason',tmp:ChangeReason)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormChangeDOP_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:UserPassword = p_web.RestoreValue('tmp:UserPassword')
 tmp:NewDOP = p_web.RestoreValue('tmp:NewDOP')
 tmp:ChangeReason = p_web.RestoreValue('tmp:ChangeReason')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('frompage')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormChangeDOP_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormChangeDOP_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormChangeDOP_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('frompage')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormChangeDOP" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormChangeDOP" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormChangeDOP" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Override DOP') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Override DOP',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormChangeDOP">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormChangeDOP" class="'&clip('MyTabs')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormChangeDOP')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Confirm User') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Details') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormChangeDOP')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormChangeDOP'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:UserPassword')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormChangeDOP')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Confirm User') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormChangeDOP_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm User')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm User')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm User')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Confirm User')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:UserPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:UserPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:UserPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormChangeDOP_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:DOP
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:DOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:DOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:NewDOP
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:NewDOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:NewDOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ChangeReason
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&250&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ChangeReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ChangeReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::tmp:UserPassword  Routine
  p_web._DivHeader('FormChangeDOP_' & p_web._nocolon('tmp:UserPassword') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Confirm Password')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:UserPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:UserPassword',p_web.GetValue('NewValue'))
    tmp:UserPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:UserPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:UserPassword',p_web.GetValue('Value'))
    tmp:UserPassword = p_web.GetValue('Value')
  End
  If tmp:UserPassword = ''
    loc:Invalid = 'tmp:UserPassword'
    loc:alert = 'You do not have access. Click Cancel'
  End
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = p_web.GSV('tmp:UserPassword')
      if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
          ! Found
          Access:ACCAREAS.Clearkey(acc:Access_Level_Key)
          acc:User_Level    = use:User_Level
          acc:Access_Area    = 'JOB - DOP OVERRIDE'
          if (Access:ACCAREAS.TryFetch(acc:Access_Level_Key) = Level:Benign)
              ! Found
              p_web.SSV('Comment:UserPassword','Password Accepted')
              p_web.SSV('Hide:ChangeDOPButton',0)
              p_web.SSV('local:PasswordValidated',1)
          else ! if (Access:ACCAREAS.TryFetch(acc:Access_Level_Key) = Level:Benign)
              ! Error
              p_web.SSV('Comment:UserPassword','User does not have access')
              p_web.SSV('Hide:ChangeDOPButton',1)
              p_web.SSV('tmp:UserPassword','')
              p_web.SSV('local:PasswordValidated',0)
          end ! if (Access:ACCAREAS.TryFetch(acc:Access_Level_Key) = Level:Benign)
  
      else ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
          ! Error
          p_web.SSV('Comment:UserPassword','Invalid User')
          p_web.SSV('Hide:ChangeDOPButton',1)
          p_web.SSV('tmp:UserPassword','')
          p_web.SSV('local:PasswordValidated',0)
      end ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
  
  do Value::tmp:UserPassword
  do SendAlert
  do Prompt::tmp:ChangeReason
  do Value::tmp:ChangeReason  !1
  do Comment::tmp:ChangeReason
  do Prompt::tmp:NewDOP
  do Value::tmp:NewDOP  !1
  do Comment::tmp:NewDOP
  do Prompt::job:DOP
  do Value::job:DOP  !1
  do Comment::job:DOP
  do Comment::tmp:UserPassword

Value::tmp:UserPassword  Routine
  p_web._DivHeader('FormChangeDOP_' & p_web._nocolon('tmp:UserPassword') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:UserPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:UserPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:UserPassword = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:UserPassword'',''formchangedop_tmp:userpassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:UserPassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','tmp:UserPassword',p_web.GetSessionValueFormat('tmp:UserPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChangeDOP_' & p_web._nocolon('tmp:UserPassword') & '_value')

Comment::tmp:UserPassword  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:UserPassword'))
  p_web._DivHeader('FormChangeDOP_' & p_web._nocolon('tmp:UserPassword') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChangeDOP_' & p_web._nocolon('tmp:UserPassword') & '_comment')

Prompt::job:DOP  Routine
  p_web._DivHeader('FormChangeDOP_' & p_web._nocolon('job:DOP') & '_prompt',Choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Previous DOP')
  If p_web.GSV('Hide:ChangeDOPButton') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChangeDOP_' & p_web._nocolon('job:DOP') & '_prompt')

Validate::job:DOP  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:DOP',p_web.GetValue('NewValue'))
    job:DOP = p_web.GetValue('NewValue') !FieldType= DATE Field = job:DOP
    do Value::job:DOP
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:DOP',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    job:DOP = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  do Value::job:DOP
  do SendAlert

Value::job:DOP  Routine
  p_web._DivHeader('FormChangeDOP_' & p_web._nocolon('job:DOP') & '_value',Choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ChangeDOPButton') = 1)
  ! --- STRING --- job:DOP
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('job:DOP')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:DOP'',''formchangedop_job:dop_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','job:DOP',p_web.GetSessionValue('job:DOP'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChangeDOP_' & p_web._nocolon('job:DOP') & '_value')

Comment::job:DOP  Routine
      loc:comment = ''
  p_web._DivHeader('FormChangeDOP_' & p_web._nocolon('job:DOP') & '_comment',Choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:ChangeDOPButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChangeDOP_' & p_web._nocolon('job:DOP') & '_comment')

Prompt::tmp:NewDOP  Routine
  p_web._DivHeader('FormChangeDOP_' & p_web._nocolon('tmp:NewDOP') & '_prompt',Choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('New DOP')
  If p_web.GSV('Hide:ChangeDOPButton') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChangeDOP_' & p_web._nocolon('tmp:NewDOP') & '_prompt')

Validate::tmp:NewDOP  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:NewDOP',p_web.GetValue('NewValue'))
    tmp:NewDOP = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:NewDOP
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:NewDOP',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    tmp:NewDOP = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  If tmp:NewDOP = ''
    loc:Invalid = 'tmp:NewDOP'
    loc:alert = p_web.translate('New DOP') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  if (p_web.GSV('tmp:NewDOP') > Today() Or p_web.GSV('tmp:NewDOP') = p_web.GSV('job:DOP'))
      p_web.SSV('tmp:NewDOP',0)
      p_web.SSV('Comment:NewDOP','Invalid Date.')
  else ! if (p_web.GSV('tmp:NewDOP') > Today())
      p_web.SSV('Comment:NewDOP','Required')
  end ! if (p_web.GSV('tmp:NewDOP') > Today())
  do Value::tmp:NewDOP
  do SendAlert
  do Comment::tmp:NewDOP

Value::tmp:NewDOP  Routine
  p_web._DivHeader('FormChangeDOP_' & p_web._nocolon('tmp:NewDOP') & '_value',Choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ChangeDOPButton') = 1)
  ! --- STRING --- tmp:NewDOP
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:NewDOP')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:NewDOP = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:NewDOP'',''formchangedop_tmp:newdop_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:NewDOP')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:NewDOP',p_web.GetSessionValue('tmp:NewDOP'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
  !Handcode Date Lookup Button
  packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar(tmp__NewDOP,''dd/mm/yyyy'',this); Date.disabled=true;sv(''...'',''newjobbooking_pickdate_value'',1,FieldValue(this,1));nextFocus(NewJobBooking_frm,'''',0);" value="Select Date Of Purchase" name="Date" type="button">...</button>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChangeDOP_' & p_web._nocolon('tmp:NewDOP') & '_value')

Comment::tmp:NewDOP  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:NEWDOP'))
  p_web._DivHeader('FormChangeDOP_' & p_web._nocolon('tmp:NewDOP') & '_comment',Choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:ChangeDOPButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChangeDOP_' & p_web._nocolon('tmp:NewDOP') & '_comment')

Prompt::tmp:ChangeReason  Routine
  p_web._DivHeader('FormChangeDOP_' & p_web._nocolon('tmp:ChangeReason') & '_prompt',Choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Change Reason')
  If p_web.GSV('Hide:ChangeDOPButton') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChangeDOP_' & p_web._nocolon('tmp:ChangeReason') & '_prompt')

Validate::tmp:ChangeReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ChangeReason',p_web.GetValue('NewValue'))
    tmp:ChangeReason = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ChangeReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ChangeReason',p_web.GetValue('Value'))
    tmp:ChangeReason = p_web.GetValue('Value')
  End
  If tmp:ChangeReason = ''
    loc:Invalid = 'tmp:ChangeReason'
    loc:alert = p_web.translate('Change Reason') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::tmp:ChangeReason
  do SendAlert

Value::tmp:ChangeReason  Routine
  p_web._DivHeader('FormChangeDOP_' & p_web._nocolon('tmp:ChangeReason') & '_value',Choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ChangeDOPButton') = 1)
  ! --- TEXT --- tmp:ChangeReason
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('Hide:ChangeDOPButton') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:ChangeReason')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:ChangeReason = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ChangeReason'',''formchangedop_tmp:changereason_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('tmp:ChangeReason',p_web.GetSessionValue('tmp:ChangeReason'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(tmp:ChangeReason),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChangeDOP_' & p_web._nocolon('tmp:ChangeReason') & '_value')

Comment::tmp:ChangeReason  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('FormChangeDOP_' & p_web._nocolon('tmp:ChangeReason') & '_comment',Choose(p_web.GSV('Hide:ChangeDOPButton') = 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GSV('Hide:ChangeDOPButton') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormChangeDOP_' & p_web._nocolon('tmp:ChangeReason') & '_comment')

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormChangeDOP_tmp:UserPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:UserPassword
      else
        do Value::tmp:UserPassword
      end
  of lower('FormChangeDOP_job:DOP_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:DOP
      else
        do Value::job:DOP
      end
  of lower('FormChangeDOP_tmp:NewDOP_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:NewDOP
      else
        do Value::tmp:NewDOP
      end
  of lower('FormChangeDOP_tmp:ChangeReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ChangeReason
      else
        do Value::tmp:ChangeReason
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormChangeDOP_form:ready_',1)
  p_web.SetSessionValue('FormChangeDOP_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormChangeDOP',0)

PreCopy  Routine
  p_web.SetValue('FormChangeDOP_form:ready_',1)
  p_web.SetSessionValue('FormChangeDOP_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormChangeDOP',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormChangeDOP_form:ready_',1)
  p_web.SetSessionValue('FormChangeDOP_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormChangeDOP:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormChangeDOP_form:ready_',1)
  p_web.SetSessionValue('FormChangeDOP_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormChangeDOP:Primed',0)
  p_web.setsessionvalue('showtab_FormChangeDOP',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormChangeDOP_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  if (p_web.GSV('local:PasswordValidated') = 0)
      loc:Invalid = 'tmp:UserPassword'
      loc:Alert = 'Your password has not yet been validated'
      p_web.SSV('tmp:UserPassword','')
      exit
  end ! if (p_web.GSV('local:PasswordValidated') = 0)
  p_web.DeleteSessionValue('FormChangeDOP_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
        If tmp:UserPassword = ''
          loc:Invalid = 'tmp:UserPassword'
          loc:alert = 'You do not have access. Click Cancel'
        End
        If loc:Invalid <> '' then exit.
  ! tab = 1
    loc:InvalidTab += 1
      If not (p_web.GSV('Hide:ChangeDOPButton') = 1)
        If tmp:NewDOP = ''
          loc:Invalid = 'tmp:NewDOP'
          loc:alert = p_web.translate('New DOP') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GSV('Hide:ChangeDOPButton') = 1)
        If tmp:ChangeReason = ''
          loc:Invalid = 'tmp:ChangeReason'
          loc:alert = p_web.translate('Change Reason') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
      p_web.SSV('tmp:OldDOP',p_web.GSV('job:DOP'))
      p_web.SSV('job:DOP',p_web.GSV('tmp:NewDOP'))
      p_web.SSV('tmp:DateOfPurchase',p_web.GSV('tmp:NewDOP'))
  p_web.SetSessionValue('FormChangeDOP:Primed',0)
  p_web.StoreValue('tmp:UserPassword')
  p_web.StoreValue('job:DOP')
  p_web.StoreValue('tmp:NewDOP')
  p_web.StoreValue('tmp:ChangeReason')
