

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER089.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ForceMSN             PROCEDURE  (func:Manufacturer,func:Type) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
DEFAULTS::State  USHORT
FilesOpened     BYTE(0)

  CODE
    Do OpenFiles
    Do SaveFiles
    Access:DEFAULTS.Clearkey(def:RecordNumberKey)
    def:Record_Number = 1
    Set(def:RecordNumberKey,def:RecordNumberKey)
    Loop ! Begin Loop
        If Access:DEFAULTS.Next()
            Break
        End ! If Access:DEFAULTS.Next()
        Break
    End ! Loop

    Return# = 0
    If (def:Force_MSN = 'B' And func:Type = 'B') Or |
        (def:Force_MSN <> 'I' And func:Type = 'C')
        if (MSNRequired(func:Manufacturer))
            Return# = 1
        end !if (MSNRequired(func:Manufacturer) = 0)
    End!If def:Force_MSN = 'B'

    Do RestoreFiles
    Do CloseFiles
    Return Return#
!--------------------------------------
OpenFiles  ROUTINE
  Access:DEFAULTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:DEFAULTS.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  DEFAULTS::State = Access:DEFAULTS.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF DEFAULTS::State <> 0
    Access:DEFAULTS.RestoreFile(DEFAULTS::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
