

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER077.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
CreateTheInvoice     PROCEDURE  (NetWebServerWorker p_web,*STRING pError)!,LONG ! Declare Procedure
locLabourVatRate     REAL                                  !
locPartsVatRate      REAL                                  !
locPaid              BYTE                                  !
FilesOpened     BYTE(0)
    MAP
trace	PROCEDURE(STRING pText)	
    END

  CODE
!region ProcessedCode
        LOOP 1 TIMES ! Break Loop
            IF (p_web.GSV('job:Invoice_Number') > 0)
                ! Do nothing the job is already invoiced
                BREAK
            END

            IF (JobInUse(p_web.GSV('job:Ref_Number')) = 1)
                pError = 'The selected job is in use by another station'
                BREAK
            END

            DO OpenFiles

            SET(DEFAULTS,0)
            Access:DEFAULTS.Next()

            IF (vod.InvoiceTheSubAccount(p_web.GSV('job:Account_Number')))
                Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
                sub:Account_Number = p_web.GSV('job:Account_Number')
                IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key))
                    pError = 'Cannot find the Sub Account details'
                    BREAK
                END
                Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                tra:Account_Number = sub:Main_Account_Number
                IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
                    pError = 'Cannot find the Head Account details'
                    BREAK
                END        
        
                Access:VATCODE.ClearKey(vat:Vat_code_Key)
                vat:Vat_Code = sub:Labour_Vat_Code
                IF (Access:VATCODE.TryFetch(vat:Vat_code_Key))
                    pError = 'Cannot find the Labour VAT Code for the Sub Account'
                    BREAK
                END
        
                locLabourVatRate = vat:VAT_Rate
        
                Access:VATCODE.ClearKey(vat:Vat_code_Key)
                vat:Vat_Code = sub:Parts_Vat_Code
                IF (Access:VATCODE.TryFetch(vat:Vat_code_Key))
                    pError = 'Cannot find the Part VAT Code for the Sub Account'
                    BREAK
                END        
            
                locPartsVatRate = vat:VAT_Rate
        
      
            ELSE
                Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
                sub:Account_Number = p_web.GSV('job:Account_Number')
                IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key))
                    pError = 'Cannot find the Sub Account details'
                    BREAK
                END
                Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                tra:Account_Number = sub:Main_Account_Number
                IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
                    pError = 'Cannot find the Head Account details'
                    BREAK
                END  
        
                Access:VATCODE.ClearKey(vat:Vat_code_Key)
                vat:Vat_Code = tra:Labour_Vat_Code
                IF (Access:VATCODE.TryFetch(vat:Vat_code_Key))
                    pError = 'Cannot find the Labour VAT Code for the Sub Account'
                    BREAK
                END
        
                locLabourVatRate = vat:VAT_Rate
        
                Access:VATCODE.ClearKey(vat:Vat_code_Key)
                vat:Vat_Code = tra:Parts_Vat_Code
                IF (Access:VATCODE.TryFetch(vat:Vat_code_Key))
                    pError = 'Cannot find the Part VAT Code for the Sub Account'
                    BREAK
                END        
            
                locPartsVatRate = vat:VAT_Rate       
            END

            JobPricingRoutine(p_web)

            IF (Access:INVOICE.PrimeRecord() = Level:Benign)
                inv:Invoice_Type = 'SIN'
                inv:Job_Number = p_web.GSV('job:Ref_Number')
                inv:Date_Created = TODAY()
                inv:Account_Number = p_web.GSV('job:Account_Number')
                IF (tra:Invoice_Sub_Accounts = 'YES')
                    inv:AccountType = 'SUB'
                ELSE
                    inv:AccountType = 'MAI'
                END
                inv:Total = p_web.GSV('job:Sub_Total')
                inv:RRCVatRateLabour = locLabourVatRate
                inv:RRCVatRateParts = locPartsVatRate
                inv:RRCVatRateRetail = locLabourVatRate
                inv:Vat_Rate_Labour = locLabourVatRate
                inv:Vat_Rate_Parts = locPartsVatRate
                inv:Vat_Rate_Retail = locLabourVatRate
                inv:VAT_Number = def:Vat_Number
                inv:Courier_Paid = p_web.GSV('job:Courier_Cost')
                inv:Parts_Paid = p_web.GSV('job:Parts_Cost')
                inv:Labour_Paid = p_web.GSV('job:Labour_Cost')
        
!        IF (p_web.GSV('BookingSite') = 'RRC')
!            ! An RRC Created Invoice
            ! #13532 Make sure invoice date is always filled in. Although I don't know why this would fail (DBH: 07/05/2015)
                inv:RRCInvoiceDate = TODAY()
                inv:ExportedRRCOracle = TRUE
!        END
        
                IF (Access:INVOICE.TryInsert())
                    Access:INVOICE.CancelAutoInc()
                    pError = 'Unable to create invoice'
                    BREAK
                END
        
                locPaid = FALSE
                IF (p_web.GSV('job:Warranty_Job') = 'YES')
                    IF (p_web.GSV('job:EDI') = 'FIN')
                        locPaid = TRUE
                    END
                END
                IF (p_web.GSV('job:Chargeable_Job') = 'YES')
                    locPaid = FALSE
                    TotalPrice(p_web,'C',vat$,tot$,bal$)
                    IF (tot$ = 0 OR bal$ <= 0)
                        locPaid = TRUE
                    END
                END
        
                Access:COURIER.ClearKey(cou:Courier_Key)
                cou:Courier = job:Courier
                IF (Access:COURIER.TryFetch(cou:Courier_Key))
                END
        
                IF (p_web.GSV('BookingSite') = 'RRC')
                    IF (p_web.GSV('jobe:Despatched') <> 'REA')
                        IF (locPaid = TRUE)
                            IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                                p_web.SSV('GetStatus:StatusNumber',910) ! Despatched Paid
                                p_web.SSV('GetSTatus:Type','JOB')
                                GetStatus(910,0,'JOB',p_web)
                            END
                    
                        ELSE
                            IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                                p_web.SSV('GetStatus:StatusNumber',905) ! Despatched UnPaid
                                p_web.SSV('GetSTatus:Type','JOB')
                                GetStatus(905,0,'JOB',p_web)
                            END
                        END
                    ELSE
                        IF (locPaid = true)
                            IF (cou:CustomerCollection)
                                IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                                    p_web.SSV('GetStatus:StatusNumber',915) ! Paid Awaiting Collection
                                    p_web.SSV('GetSTatus:Type','JOB')
                                    GetStatus(915,0,'JOB',p_web)
                                END                        
                            ELSE
                                IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                                    p_web.SSV('GetStatus:StatusNumber',916) ! Paid Awaiting Despatch
                                    p_web.SSV('GetSTatus:Type','JOB')
                                    GetStatus(916,0,'JOB',p_web)
                                END                                                
                            END
                    
                        ELSE
                            IF (cou:CustomerCollection)
                                IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                                    p_web.SSV('GetStatus:StatusNumber',805) ! Ready To Collection
                                    p_web.SSV('GetSTatus:Type','JOB')
                                    GetStatus(805,0,'JOB',p_web)
                                END                        
                            ELSE
                                IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                                    p_web.SSV('GetStatus:StatusNumber',810) ! Ready To Despatch
                                    p_web.SSV('GetSTatus:Type','JOB')
                                    GetStatus(810,0,'JOB',p_web)
                                END                                                
                            END                    
                        END
                    END
                ELSE
            !ARC Invoice
                    IF (p_web.GSV('job:Despatched') <> 'REA')
                        IF (locPaid = TRUE)
                            IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                                p_web.SSV('GetStatus:StatusNumber',910) ! Despatched Paid
                                p_web.SSV('GetSTatus:Type','JOB')
                                GetStatus(910,0,'JOB',p_web)
                            END
                    
                        ELSE
                            IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                                p_web.SSV('GetStatus:StatusNumber',905) ! Despatched UnPaid
                                p_web.SSV('GetSTatus:Type','JOB')
                                GetStatus(905,0,'JOB',p_web)
                            END
                        END
                    ELSE
                        IF (locPaid = true)
                            IF (cou:CustomerCollection)
                                IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                                    p_web.SSV('GetStatus:StatusNumber',915) ! Paid Awaiting Collection
                                    p_web.SSV('GetSTatus:Type','JOB')
                                    GetStatus(915,0,'JOB',p_web)
                                END                        
                            ELSE
                                IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                                    p_web.SSV('GetStatus:StatusNumber',916) ! Paid Awaiting Despatch
                                    p_web.SSV('GetSTatus:Type','JOB')
                                    GetStatus(916,0,'JOB',p_web)
                                END                                                
                            END
                    
                        ELSE
                            IF (cou:CustomerCollection)
                                IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                                    p_web.SSV('GetStatus:StatusNumber',805) ! Ready To Collection
                                    p_web.SSV('GetSTatus:Type','JOB')
                                    GetStatus(805,0,'JOB',p_web)
                                END                        
                            ELSE
                                IF (p_web.GSV('job:Exchange_Unit_Number') = '')
                                    p_web.SSV('GetStatus:StatusNumber',810) ! Ready To Despatch
                                    p_web.SSV('GetSTatus:Type','JOB')
                                    GetStatus(810,0,'JOB',p_web)
                                END                                                
                            END                    
                        END
                        IF (p_web.GSV('job:Despatch_Type') = 'JOB' AND | 
                            p_web.GSV('jobe:WebJob') = 1)
                            p_web.SSV('GetStatus:StatusNumber',p_web.GSV('Default:StatusSentToRRC'))
                            p_web.SSV('GetSTatus:Type','JOB')
                            GetStatus(p_web.GSV('Default:StatusSentToRRC'),0,'JOB',p_web)   
                        END
                
                    END
           
                END
        
        
                p_web.SSV('job:Invoice_Number',inv:Invoice_Number)
                p_web.SSV('job:Invoice_Date',TODAY())
                p_web.SSV('job:Invoice_Labour_Cost',p_web.GSV('job:Labour_Cost'))
                p_web.SSV('job:Invoice_Courier_Cost',p_web.GSV('job:Courier_Cost'))
                p_web.SSV('job:Invoice_Parts_Cost',p_web.GSV('job:Parts_Cost'))
                p_web.SSV('job:Invoice_Sub_Total',p_web.GSV('job:Sub_Total'))
        
                p_web.SSV('jobe:InvRRCCLabourCost',p_web.GSV('jobe:RRCCLabourCost'))
                p_web.SSV('jobe:InvRRCCPartsCost',p_web.GSV('jobe:RRCCPartsCost'))
                p_web.SSV('jobe:InvRRCCSubTotal', p_web.GSV('jobe:RRCCSubTotal'))
                p_web.SSV('jobe:InvoiceHandlingFee',p_web.GSV('jobe:HandlingFee'))
                p_web.SSV('jobe:InvoiceExchangeRate',p_web.GSV('jobe:ExchangeRate') )
                p_web.SSV('jobe2:InvDiscountAmnt',p_web.GSV('jobe2:JobDiscountAmnt'))
     
                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = p_web.GSV('job:Ref_Number')
                IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                    p_web.SessionQueueToFile(JOBS)
                    IF (Access:JOBS.TryUpdate())
                        ! #13532 Extra checking to warn the user is something has gone wrong. (DBH: 08/05/2015)
                        pError = 'An error occurred updating the job record. \nNB: Invoice No ' & inv:Invoice_Number & ' may have still been created. Please remove this invoice before you try again'
                        BREAK
                    END ! IF
                    
                END
                
                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                jobe:RefNumber = p_web.GSV('job:Ref_Number')
                IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                    p_web.SessionQueueToFile(JOBSE)
                    Access:JOBSE.TryUpdate()
                END
        
                
        
                Access:JOBSE2.CLearkey(jobe2:RefNumberKey)
                jobe2:RefNumber = p_web.GSV('job:Ref_Number')
                If (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
                    p_web.SessionQueueToFile(JOBSE2)
                    Access:JOBSE2.TryUpdate()
                end
        
        
                IF (p_web.GSV('BookingSite') = 'RRC')
                    Line500_XML(p_web,'RIV')
                END
        
            END

        END ! Break Loop
    
        trace('Error Message: ' & CLIP(pError))
        IF (pError <> '')
            RETURN Level:Fatal
        ELSE
            RETURN Level:Benign
        END ! IF
!endregion
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBSE2.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE2.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:COURIER.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:COURIER.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:INVOICE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:INVOICE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSE.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULTS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:DEFAULTS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBSE2.Close
     Access:VATCODE.Close
     Access:COURIER.Close
     Access:INVOICE.Close
     Access:SUBTRACC.Close
     Access:TRADEACC.Close
     Access:JOBSE.Close
     Access:JOBS.Close
     Access:DEFAULTS.Close
     FilesOpened = False
  END
trace	PROCEDURE(STRING pText)
outString	CSTRING(1000)
    CODE
        RETURN ! Logging Disabled
