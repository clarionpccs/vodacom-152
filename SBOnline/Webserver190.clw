

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER190.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
UpdateProgress       PROCEDURE  (NetWebServerWorker p_web,LONG fRecords,STRING fReturnURL,<STRING fTitleText>,LONG pHidePercent=0) ! Declare Procedure
p_packet            STRING(255)
packetlen           long

  CODE
    p_packet =     '<div id="waitframe" class="nt-process">' & |
        '<p class="nt-process-text">Please wait while ServiceBase searches for matching records<br /><br/>'& |
        'This may take a few seconds....<br/><br/>'

    p_packet = CLIP(p_packet) & CLIP(fTitleText) & '<br/>'

    pushpack(p_web,CLIP(p_packet) & |
        '<br/><script type="text/javascript">drawProgressBar(''#ff0000'', 250, ' & fRecords & ',' & pHidePercent & ');</script>' & |
        '</p>'& |
        '<p>' & |
        '<a href="IndexPage">Cancel Process</a>' & |
        '</p>' & |
        '</div>')



