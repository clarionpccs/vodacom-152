

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER022.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
TotalPrice           PROCEDURE  (NetWebServerWorker p_web,STRING priceType,*REAL vat,*REAL total,*REAL balance) ! Declare Procedure
paid_chargeable_temp REAL                                  !
paid_warranty_temp   REAL                                  !
labour_rate_temp     REAL                                  !
parts_rate_temp      REAL                                  !
FilesOpened     BYTE(0)

  CODE
!region !How much has been paid?
    DO OpenFiles
		
    vat = 0
    total = 0
    balance = 0
		
    Paid_Warranty_Temp = 0
    Paid_Chargeable_Temp = 0
    Access:JOBPAYMT_ALIAS.Clearkey(jpt_ali:All_Date_Key)
    jpt_ali:Ref_Number = p_web.GSV('job:Ref_Number')
    Set(jpt_ali:All_Date_Key,jpt_ali:All_Date_Key)
    Loop ! Begin Loop
        If Access:JOBPAYMT_ALIAS.Next()
            Break
        End ! If Access:JOBPAYMT_ALIAS.Next()
        If jpt_ali:Ref_Number <> p_web.GSV('job:Ref_Number')
            Break
        End ! If jpt_ali:Ref_Number <> p_web.GSV('job:Ref_Number

        Paid_Chargeable_Temp += jpt_ali:Amount
    End ! Loop

    !Look up VAT Rates
    Labour_Rate_Temp = 0
    Parts_Rate_Temp = 0
	    
    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = p_web.GSV('job:Account_Number')
    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Invoice_Sub_Accounts = 'YES'
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:VAT_Code = sub:Labour_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    Labour_Rate_Temp = vat:VAT_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:VAT_Code = sub:Parts_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    Parts_Rate_Temp = vat:VAT_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
            Else ! If tra:Invoice_Sub_Accounts = 'YES'
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:VAT_Code = tra:Labour_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    Labour_Rate_Temp = vat:VAT_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:VAT_Code = tra:Parts_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    Parts_Rate_Temp = vat:VAT_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
            End ! If tra:Invoice_Sub_Accounts = 'YES'
        Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign

    CASE priceType
    Of 'C' !Chargeable
        If p_web.GSV('BookingSite') = 'RRC'
            vat = p_web.GSV('jobe:RRCCLabourCost') * (labour_rate_temp/100) + |
                            p_web.GSV('jobe:RRCCPartsCost') * (parts_rate_temp/100) + |
                            p_web.GSV('job:courier_cost') * (labour_rate_temp/100)

            total = p_web.GSV('jobe:RRCCLabourCost') + |
                            p_web.GSV('jobe:RRCCPartsCost') + p_web.GSV('job:courier_cost') + vat

            balance = total - paid_chargeable_temp

        Else !If glo:WebJob
            vat = p_web.GSV('job:labour_cost') * (labour_rate_temp/100) + |
                            p_web.GSV('job:parts_cost') * (parts_rate_temp/100) + |
                            p_web.GSV('job:courier_cost') * (labour_rate_temp/100)

            total = p_web.GSV('job:labour_cost') + |
                            p_web.GSV('job:parts_cost') + p_web.GSV('job:courier_cost') + vat

            balance = total - paid_chargeable_temp
        End !If glo:WebJob
    Of 'W' ! Warranty
        If p_web.GSV('BookingSite') = 'RRC'
            vat = p_web.GSV('jobe:RRCWLabourCost') * (labour_rate_temp/100) + |
                            p_web.GSV('jobe:RRCWPartsCost') * (parts_rate_temp/100) + |
                            p_web.GSV('job:courier_cost_warranty') * (labour_rate_temp/100)

            total = p_web.GSV('jobe:RRCWLabourCost') + |
                            p_web.GSV('jobe:RRCWPartsCost') + p_web.GSV('job:courier_cost_warranty') + vat

        Else !If glo:WebJob
            vat = p_web.GSV('job:labour_cost_warranty') * (labour_rate_temp/100) + |
                            p_web.GSV('job:parts_cost_warranty') * (parts_rate_temp/100) + |
                            p_web.GSV('job:courier_cost_warranty') * (labour_rate_temp/100)

            total = p_web.GSV('job:labour_cost_warranty') + |
                            p_web.GSV('job:parts_cost_warranty') + p_web.GSV('job:courier_cost_warranty') + vat

        End !If glo:WebJob
    Of 'E' !Estimate
        If p_web.GSV('jobe:WebJob') = 1
            vat = p_web.GSV('jobe:RRCELabourCost') * (labour_rate_temp/100) + |
                            p_web.GSV('jobe:RRCEPartsCost') * (parts_rate_temp/100) + |
                            p_web.GSV('job:courier_cost_estimate') * (labour_rate_temp/100)

            total = p_web.GSV('jobe:RRCELabourCost') + |
                            p_web.GSV('jobe:RRCEPartsCost') + p_web.GSV('job:courier_cost_estimate') + vat

        Else !If p_web.GSV('jobe:WebJob
            vat = p_web.GSV('job:labour_cost_estimate') * (labour_rate_temp/100) + |
                            p_web.GSV('job:parts_cost_estimate') * (parts_rate_temp/100) + |
                            p_web.GSV('job:courier_cost_estimate') * (labour_rate_temp/100)

            total = p_web.GSV('job:labour_cost_estimate') + |
                            p_web.GSV('job:parts_cost_estimate') + p_web.GSV('job:courier_cost_estimate') + vat

        End !If p_web.GSV('jobe:WebJob
    Of 'I' ! Chargealbe Invoice
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
        If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Found
            If p_web.GSV('BookingSite') = 'RRC'
                vat = p_web.GSV('jobe:InvRRCCLabourCost') * (inv:vat_rate_labour/100) + |
                                p_web.GSV('jobe:InvRRCCPartsCost') * (inv:vat_rate_parts/100) + |
                                p_web.GSV('job:invoice_courier_cost') * (inv:vat_rate_labour/100)

                total = p_web.GSV('jobe:InvRRCCLabourCost') + |
                                p_web.GSV('jobe:InvRRCCPartsCost') + p_web.GSV('job:invoice_courier_cost') + vat

                balance = total - paid_chargeable_temp
            Else !If glo:webJOb
                vat = p_web.GSV('job:invoice_labour_cost') * (inv:vat_rate_labour/100) + |
                                p_web.GSV('job:invoice_parts_cost') * (inv:vat_rate_parts/100) + |
                                p_web.GSV('job:invoice_courier_cost') * (inv:vat_rate_labour/100)

                total = p_web.GSV('job:invoice_labour_cost') + |
                                p_web.GSV('job:invoice_parts_cost') + p_web.GSV('job:invoice_courier_cost') + vat

                balance = total - paid_chargeable_temp
            End !If glo:webJOb

        Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Error
        End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
    Of 'V' ! Warranty Invoice
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = p_web.GSV('job:Invoice_Number_Warranty')
        If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Found
            If p_web.GSV('BookingSite') = 'RRC'
                vat = p_web.GSV('jobe:InvRRCWLabourCost') * (inv:vat_rate_labour/100) + |
                                p_web.GSV('jobe:InvRRCWPartsCost') * (inv:vat_rate_parts/100) + |
                                p_web.GSV('job:Winvoice_courier_cost') * (inv:vat_rate_labour/100)
                total = p_web.GSV('jobe:InvRRCWLabourCost') + p_web.GSV('jobe:InvRRCWPartsCost') + |
                                p_web.GSV('job:Winvoice_courier_cost') + vat

                balance = total - paid_chargeable_temp
            Else !If glo:WebJob
                vat = p_web.GSV('job:Winvoice_labour_cost') * (inv:vat_rate_labour/100) + |
                                p_web.GSV('job:Winvoice_parts_cost') * (inv:vat_rate_parts/100) + |
                                p_web.GSV('job:Winvoice_courier_cost') * (inv:vat_rate_labour/100)
                total = p_web.GSV('job:Winvoice_labour_cost') + p_web.GSV('job:Winvoice_parts_cost') + |
                                p_web.GSV('job:Winvoice_courier_cost') + vat

                balance = total - paid_chargeable_temp
            End !If glo:WebJob
        Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Error
        End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
    End ! Case f:Type

    vat = Round(vat,.01)
    total = Round(total,.01)
    balance = Round(balance,.01)
		
    Do CloseFiles
!endregion	
!--------------------------------------
OpenFiles  ROUTINE
  Access:SUBTRACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SUBTRACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRADEACC.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:VATCODE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBPAYMT_ALIAS.Open                               ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBPAYMT_ALIAS.UseFile                            ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:SUBTRACC.Close
     Access:TRADEACC.Close
     Access:VATCODE.Close
     Access:JOBPAYMT_ALIAS.Close
     FilesOpened = False
  END
