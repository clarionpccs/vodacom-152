

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER205.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
GetOutFaults         PROCEDURE  (*gGetOutFaults G,<STRING pJobRecord>,<NetWebServerWorker p_web>) ! Declare Procedure
                    MAP
ProcessPartsW           PROCEDURE()
ProcessPartsE           PROCEDURE()
ProcessPartsC           PROCEDURE()
                    END ! MAP

cFaultCode                  STRING(255)
cFaultDescription           STRING(60)
wFaultDescription           STRING(60)
cIndex                      LONG()
cRepairType                 STRING(30)
wFaultCode                  STRING(255)
wIndex                      LONG()
wRepairType                 STRING(30)
keyRepair                   LONG()
faultCode                   STRING(255)

rjob                        GROUP(JOB:Record),PRE(rjob)
                            END ! 

  CODE
!Region ProcessedCode
        IF (pJobRecord <> '')
            rjob = pJobRecord
        ELSE
            rjob.Manufacturer = p_web.GSV('job:Manufacturer')
            rjob.Ref_Number = p_web.GSV('job:Ref_Number')
            rjob.Estimate = p_web.GSV('job:Estimate')
            rjob.Chargeable_Job = p_web.GSV('job:Chargeable_Job')
            rjob.Estimate_Accepted = p_web.GSV('job:Estimate_Accepted')
            rjob.Estimate_Rejected = p_web.GSV('job:Estimate_Rejected')            
        END ! IF
        
        Relate:ESTPARTS.Open()
        Relate:JOBOUTFL.Open()
        Relate:MANFAULO.Open()
        Relate:MANFAULT.Open()
        Relate:MANFAUPA.Open()
        Relate:MANUFACT.Open()
        Relate:PARTS.Open()
        Relate:WARPARTS.Open()
    
        Access:MANUFACT.ClearKey(man:Manufacturer_Key)
        man:Manufacturer = rjob.Manufacturer
        IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
            IF (man:UseInvTextForFaults = TRUE)
                IF (man:AutoRepairType = TRUE)
                    Access:MANFAULT.ClearKey(maf:MainFaultKey)
                    maf:Manufacturer = rjob.Manufacturer
                    maf:MainFault = 1
                    IF (Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign)
                        Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
                        joo:JobNumber = rjob.Ref_Number
                        SET(joo:JobNumberKey,joo:JobNumberKey)
                        LOOP UNTIL Access:JOBOUTFL.Next() <> Level:Benign
                            IF (joo:JobNumber <> rjob.Ref_Number)
                                BREAK
                            END ! IF
                            Access:MANFAULO.ClearKey(mfo:Field_Key)
                            mfo:Manufacturer = rjob.Manufacturer
                            mfo:Field_Number = maf:Field_Number
                            mfo:Field = joo:FaultCode
                            IF (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                                IF (mfo:RepairType <> '')
                                    IF (mfo:ImportanceLevel > cIndex)
                                        cFaultCode = mfo:Field
                                        cFaultDescription = mfo:Description
                                        cIndex = mfo:ImportanceLevel
                                        cRepairType = mfo:RepairType
                                    END ! IF
                                END ! IF
                                IF (mfo:RepairTypeWarranty <> '')
                                    IF (mfo:ImportanceLevel > wIndex)
                                        wFaultCode = mfo:Field
                                        wFaultDescription = mfo:Description
                                        wIndex = mfo:ImportanceLevel
                                        wRepairType = mfo:RepairTypeWarranty
                                    END ! IF
                                END ! IF
                            END ! IF
                        END ! LOOP
                        
                        Access:MANFAUPA.ClearKey(map:KeyRepairKey)
                        map:Manufacturer = rjob.Manufacturer
                        map:KeyRepair = 1
                        IF (Access:MANFAUPA.TryFetch(map:KeyRepairKey) = Level:Benign)
                            keyRepair = map:Field_Number
                        ELSE ! IF
                            keyRepair = 0
                        END ! IF
                        
                        Access:MANFAUPA.ClearKey(map:MainFaultKey)
                        map:Manufacturer = rjob.Manufacturer
                        map:MainFault = 1
                        SET(map:MainFaultKey,map:MainFaultKey)
                        LOOP UNTIL Access:MANFAUPA.Next() <> Level:Benign
                            IF (map:Manufacturer <> rjob.Manufacturer OR |
                                map:MainFault <> 1)
                               BREAK
                            END ! IF
                                
                            ProcessPartsW()
                                
                            IF (rjob.Estimate = 'YES' AND rjob.Chargeable_Job = 'YES' AND rjob.Estimate_Accepted <> 'YES' AND rjob.Estimate_Rejected <> 'YES')
                                ProcessPartsE()
                            ELSE
                                ProcessPartsC()
                            END ! IF
                        END ! LOOP
                    END ! IF
                END ! IF
            END ! IF
        ELSE ! IF
        END ! IF

        Relate:WARPARTS.Close()
        Relate:PARTS.Close()
        Relate:MANUFACT.Close()
        Relate:MANFAUPA.Close()
        Relate:MANFAULT.Close()
        Relate:MANFAULO.Close()
        Relate:JOBOUTFL.Close()
        Relate:ESTPARTS.Close()
    
        G.cFaultCode = cFaultCode
        G.cIndex = cIndex
        G.cRepairType = cRepairType
        G.wFaultCode = wFaultCode
        G.wIndex = wIndex
        G.wRepairType = wRepairType     
        G.wFaultDescription = wFaultDescription 
        G.cFaultDescription = cFaultDescription 
!Endregion
ProcessPartsW           PROCEDURE()
    CODE
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number = rjob.Ref_Number
        SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
        LOOP UNTIL Access:WARPARTS.Next() <> Level:Benign
            IF (wpr:Ref_Number <> rjob.Ref_Number)
                BREAK
            END ! IF
            
            CASE keyRepair
            OF 1
                IF (wpr:Fault_Code1 <> 1)
                    CYCLE
                END ! IF
            OF 2
                IF (wpr:Fault_Code2 <> 1)
                    CYCLE
                END ! IF
            OF 3
                IF (wpr:Fault_Code3 <> 1)
                    CYCLE
                END ! IF
            OF 4
                IF (wpr:Fault_Code4 <> 1)
                    CYCLE
                END ! IF
            OF 5
                IF (wpr:Fault_Code5 <> 1)
                    CYCLE
                END ! IF
            OF 6
                IF (wpr:Fault_Code6 <> 1)
                    CYCLE
                END ! IF
            OF 7
                IF (wpr:Fault_Code7 <> 1)
                    CYCLE
                END ! IF
            OF 8
                IF (wpr:Fault_Code8 <> 1)
                    CYCLE
                END ! IF
            OF 9
                IF (wpr:Fault_Code9 <> 1)
                    CYCLE
                END ! IF
            OF 10
                IF (wpr:Fault_Code10 <> 1)
                    CYCLE
                END ! IF
            OF 11
                IF (wpr:Fault_Code11 <> 1)
                    CYCLE
                END ! IF
            OF 12
                IF (wpr:Fault_Code12 <> 1)
                    CYCLE
                END ! IF
            END ! CASE
            
            Access:MANFAULO.ClearKey(mfo:Field_Key)
            mfo:Manufacturer = rjob.Manufacturer
            mfo:Field_NUmber = maf:Field_Number
            CASE map:Field_Number
            OF 1
                mfo:Field = wpr:Fault_Code1
            OF 2
                mfo:Field = wpr:Fault_Code2
            OF 3
                mfo:Field = wpr:Fault_Code3
            OF 4
                mfo:Field = wpr:Fault_Code4
            OF 5
                mfo:Field = wpr:Fault_Code5
            OF 6
                mfo:Field = wpr:Fault_Code6
            OF 7
                mfo:Field = wpr:Fault_Code7
            OF 8
                mfo:Field = wpr:Fault_Code8
            OF 9
                mfo:Field = wpr:Fault_Code9
            OF 10
                mfo:Field = wpr:Fault_Code10
            OF 11
                mfo:Field = wpr:Fault_Code11
            OF 12
                mfo:Field = wpr:Fault_Code12
            END!  IF
            IF (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                IF (mfo:RepairType <> '')
                    IF (mfo:ImportanceLevel > cIndex)
                        cFaultCode = mfo:Field
                        cFaultDescription = mfo:Description
                        cIndex = mfo:ImportanceLevel
                        cRepairType = mfo:RepairType
                    END ! IF
                END ! IF
                IF (mfo:RepairTypeWarranty <> '')
                    IF (mfo:ImportanceLevel > wIndex)
                        wFaultCode = mfo:Field
                        wFaultDescription = mfo:Description
                        wIndex = mfo:ImportanceLevel
                        wRepairType = mfo:RepairTypeWarranty
                    END ! IF
                END ! IF                                    
            ELSE ! IF
            END ! IF
        END ! LOOP
        
ProcessPartsE       PROCEDURE()
    CODE
        Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
        epr:Ref_Number = rjob.Ref_Number
        SET(epr:Part_Number_Key,epr:Part_Number_Key)
        LOOP UNTIL Access:ESTPARTS.Next() <> Level:Benign
            IF (epr:Ref_Number <> rjob.Ref_Number)
                BREAK
            END ! IF
            
            CASE keyRepair
            OF 1
                IF (epr:Fault_Code1 <> 1)
                    CYCLE
                END ! IF
            OF 2
                IF (epr:Fault_Code2 <> 1)
                    CYCLE
                END ! IF
            OF 3
                IF (epr:Fault_Code3 <> 1)
                    CYCLE
                END ! IF
            OF 4
                IF (epr:Fault_Code4 <> 1)
                    CYCLE
                END ! IF
            OF 5
                IF (epr:Fault_Code5 <> 1)
                    CYCLE
                END ! IF
            OF 6
                IF (epr:Fault_Code6 <> 1)
                    CYCLE
                END ! IF
            OF 7
                IF (epr:Fault_Code7 <> 1)
                    CYCLE
                END ! IF
            OF 8
                IF (epr:Fault_Code8 <> 1)
                    CYCLE
                END ! IF
            OF 9
                IF (epr:Fault_Code9 <> 1)
                    CYCLE
                END ! IF
            OF 10
                IF (epr:Fault_Code10 <> 1)
                    CYCLE
                END ! IF
            OF 11
                IF (epr:Fault_Code11 <> 1)
                    CYCLE
                END ! IF
            OF 12
                IF (epr:Fault_Code12 <> 1)
                    CYCLE
                END ! IF
            END ! CASE
            
            Access:MANFAULO.ClearKey(mfo:Field_Key)
            mfo:Manufacturer = rjob.Manufacturer
            mfo:Field_NUmber = maf:Field_Number
            CASE map:Field_Number
            OF 1
                mfo:Field = epr:Fault_Code1
            OF 2
                mfo:Field = epr:Fault_Code2
            OF 3
                mfo:Field = epr:Fault_Code3
            OF 4
                mfo:Field = epr:Fault_Code4
            OF 5
                mfo:Field = epr:Fault_Code5
            OF 6
                mfo:Field = epr:Fault_Code6
            OF 7
                mfo:Field = epr:Fault_Code7
            OF 8
                mfo:Field = epr:Fault_Code8
            OF 9
                mfo:Field = epr:Fault_Code9
            OF 10
                mfo:Field = epr:Fault_Code10
            OF 11
                mfo:Field = epr:Fault_Code11
            OF 12
                mfo:Field = epr:Fault_Code12
            END!  IF
            IF (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                IF (mfo:RepairType <> '')
                    IF (mfo:ImportanceLevel > cIndex)
                        cFaultCode = mfo:Field
                        cFaultDescription = mfo:Description
                        cIndex = mfo:ImportanceLevel
                        cRepairType = mfo:RepairType
                    END ! IF
                END ! IF
                IF (mfo:RepairTypeWarranty <> '')
                    IF (mfo:ImportanceLevel > wIndex)
                        wFaultCode = mfo:Field
                        wFaultDescription = mfo:Description
                        wIndex = mfo:ImportanceLevel
                        wRepairType = mfo:RepairTypeWarranty
                    END ! IF
                END ! IF                                    
            ELSE ! IF
            END ! IF
        END ! LOOP
        
ProcessPartsC       PROCEDURE()
    CODE
        Access:PARTS.ClearKey(par:Part_Number_Key)
        par:Ref_Number = rjob.Ref_Number
        SET(par:Part_Number_Key,par:Part_Number_Key)
        LOOP UNTIL Access:PARTS.Next() <> Level:Benign
            IF (par:Ref_Number <> rjob.Ref_Number)
                BREAK
            END ! IF
            
            CASE keyRepair
            OF 1
                IF (par:Fault_Code1 <> 1)
                    CYCLE
                END ! IF
            OF 2
                IF (par:Fault_Code2 <> 1)
                    CYCLE
                END ! IF
            OF 3
                IF (par:Fault_Code3 <> 1)
                    CYCLE
                END ! IF
            OF 4
                IF (par:Fault_Code4 <> 1)
                    CYCLE
                END ! IF
            OF 5
                IF (par:Fault_Code5 <> 1)
                    CYCLE
                END ! IF
            OF 6
                IF (par:Fault_Code6 <> 1)
                    CYCLE
                END ! IF
            OF 7
                IF (par:Fault_Code7 <> 1)
                    CYCLE
                END ! IF
            OF 8
                IF (par:Fault_Code8 <> 1)
                    CYCLE
                END ! IF
            OF 9
                IF (par:Fault_Code9 <> 1)
                    CYCLE
                END ! IF
            OF 10
                IF (par:Fault_Code10 <> 1)
                    CYCLE
                END ! IF
            OF 11
                IF (par:Fault_Code11 <> 1)
                    CYCLE
                END ! IF
            OF 12
                IF (par:Fault_Code12 <> 1)
                    CYCLE
                END ! IF
            END ! CASE
            
            Access:MANFAULO.ClearKey(mfo:Field_Key)
            mfo:Manufacturer = rjob.Manufacturer
            mfo:Field_NUmber = maf:Field_Number
            CASE map:Field_Number
            OF 1
                mfo:Field = par:Fault_Code1
            OF 2
                mfo:Field = par:Fault_Code2
            OF 3
                mfo:Field = par:Fault_Code3
            OF 4
                mfo:Field = par:Fault_Code4
            OF 5
                mfo:Field = par:Fault_Code5
            OF 6
                mfo:Field = par:Fault_Code6
            OF 7
                mfo:Field = par:Fault_Code7
            OF 8
                mfo:Field = par:Fault_Code8
            OF 9
                mfo:Field = par:Fault_Code9
            OF 10
                mfo:Field = par:Fault_Code10
            OF 11
                mfo:Field = par:Fault_Code11
            OF 12
                mfo:Field = par:Fault_Code12
            END!  IF
            IF (Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign)
                IF (mfo:RepairType <> '')
                    IF (mfo:ImportanceLevel > cIndex)
                        cFaultCode = mfo:Field
                        cFaultDescription = mfo:Description
                        cIndex = mfo:ImportanceLevel
                        cRepairType = mfo:RepairType
                    END ! IF
                END ! IF
                IF (mfo:RepairTypeWarranty <> '')
                    IF (mfo:ImportanceLevel > wIndex)
                        wFaultCode = mfo:Field
                        wFaultDescription = mfo:Description
                        wIndex = mfo:ImportanceLevel
                        wRepairType = mfo:RepairTypeWarranty
                    END ! IF
                END ! IF                                    
            ELSE ! IF
            END ! IF
        END ! LOOP        
