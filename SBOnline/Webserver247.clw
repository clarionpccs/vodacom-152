

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER247.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER010.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER062.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER248.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER249.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER250.INC'),ONCE        !Req'd for module callout resolution
                     END


WaybillConfirmation  PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locWaybillNumber     STRING(30)                            !
locJobNumber         STRING(30)                            !
locIMEINumber        STRING(30)                            !
locFilterBrowse      LONG                                  !
locWaybillFound      LONG                                  !
locSundryWaybillFound LONG                                 !
locJobFound          LONG                                  !
locEngineerOption    STRING(30)                            !
locModelNumber       STRING(30)                            !
locJobType           STRING(60)                            !
locChargeType        STRING(60)                            !
locDOP               STRING(30)                            !
locEngineerNotes     STRING(255)                           !
                    MAP
CountUnprocessed        PROCEDURE(),LONG
StickyNotes             PROCEDURE(),LONG
                    END ! MAP
FilesOpened     Long
WAYAUDIT::State  USHORT
CONTHIST::State  USHORT
JOBNOTES::State  USHORT
JOBS::State  USHORT
WAYBILLJ::State  USHORT
EXCHANGE::State  USHORT
WAYBILLS::State  USHORT
JOBSE::State  USHORT
SBO_GenericFile::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('WaybillConfirmation')
  loc:formname = 'WaybillConfirmation_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'WaybillConfirmation',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('WaybillConfirmation','')
    p_web._DivHeader('WaybillConfirmation',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferWaybillConfirmation',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWaybillConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWaybillConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_WaybillConfirmation',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWaybillConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_WaybillConfirmation',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'WaybillConfirmation',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
ClearTempFile       ROUTINE
    ClearSBOGenericFile(p_web)
!    Access:SBO_GenericFile.ClearKey(sbogen:Long1Key)
!    sbogen:SessionID = p_web.SessionID
!    SET(sbogen:Long1Key,sbogen:Long1Key)
!    LOOP UNTIL Access:SBO_GenericFile.Next() <> Level:Benign
!        IF (sbogen:SessionID <> p_web.SessionID)
!            BREAK
!        END ! IF
!        Access:SBO_GenericFile.DeleteRecord(0)
!    END ! LOOp
ResetAllFields      ROUTINE
    do Value::WaybillBrowseConfItems  !1
    do Prompt::locFilterBrowse  !1
    do Value::locFilterBrowse  !1
    do Value::WaybillBrowseConfItems  !1
    do Prompt::locIMEINumber
    do Value::locIMEINumber  !1
    do Prompt::locJobNumber
    do Value::locJobNumber  !1
    do Prompt::jbn:Engineers_Notes  !1    
    do Value::jbn:Engineers_Notes  !1    
    DO Prompt::locModelNumber
    DO Value::locModelNumber
    DO Prompt::locJobType
    DO Value::locJobType
    DO Prompt::locChargeType
    DO Value::locChargeType
    DO Prompt::locDOP
    DO Value::locDOP
    do Value::txtJobsOnSelectedWaybill  !1
    do Value::txtItemsOnSundryWauybill  !1
    do Value::txtProcessJobsOnWaybill  !1
    Do Value::locEngineerOption
    DO Value::WaybillConfAccessories
    DO Value::btnClearJob
    DO Value::btnClearWaybill
    !DO Value::locWaybillNumber
    DO Value::btnCompleteWaybill
    DO Value::btnProcessJob
    DO Value::txtWaybillReceived
    DO Value::WaybillConfSundryItems
    DO Value::btnReceiveSundryWaybill
DeleteSessionValues ROUTINE ! Auto generated by Bryan's Template
    ! Local Variables
    ! p_web.DeleteSessionValue('Ans') ! Excluded
    p_web.DeleteSessionValue('locWaybillNumber')
    p_web.DeleteSessionValue('locJobNumber')
    p_web.DeleteSessionValue('locIMEINumber')
    p_web.DeleteSessionValue('locFilterBrowse')
    p_web.DeleteSessionValue('locWaybillFound')
    p_web.DeleteSessionValue('locSundryWaybillFound')
    p_web.DeleteSessionValue('locJobFound')
    p_web.DeleteSessionValue('locEngineerOption')
    p_web.DeleteSessionValue('locModelNumber')
    p_web.DeleteSessionValue('locJobType')
    p_web.DeleteSessionValue('locChargeType')
    p_web.DeleteSessionValue('locDOP')
    p_web.DeleteSessionValue('locEngineerNotes')

    ! Other Variables
OpenFiles  ROUTINE
  p_web._OpenFile(WAYAUDIT)
  p_web._OpenFile(CONTHIST)
  p_web._OpenFile(JOBNOTES)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(WAYBILLJ)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(WAYBILLS)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(SBO_GenericFile)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(WAYAUDIT)
  p_Web._CloseFile(CONTHIST)
  p_Web._CloseFile(JOBNOTES)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WAYBILLJ)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(WAYBILLS)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(SBO_GenericFile)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('WaybillConfirmation_form:inited_',1)
  do RestoreMem

CancelForm  Routine
    DO DeleteSessionValues  

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('locDOP')
    p_web.SetPicture('locDOP','@d06b')
  End
  p_web.SetSessionPicture('locDOP','@d06b')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)
  p_web.SetSessionValue('locFilterBrowse',locFilterBrowse)
  p_web.SetSessionValue('locJobNumber',locJobNumber)
  p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  p_web.SetSessionValue('locEngineerOption',locEngineerOption)
  p_web.SetSessionValue('locModelNumber',locModelNumber)
  p_web.SetSessionValue('locJobType',locJobType)
  p_web.SetSessionValue('locChargeType',locChargeType)
  p_web.SetSessionValue('locDOP',locDOP)
  p_web.SetSessionValue('locEngineerNotes',locEngineerNotes)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locWaybillNumber')
    locWaybillNumber = p_web.GetValue('locWaybillNumber')
    p_web.SetSessionValue('locWaybillNumber',locWaybillNumber)
  End
  if p_web.IfExistsValue('locFilterBrowse')
    locFilterBrowse = p_web.GetValue('locFilterBrowse')
    p_web.SetSessionValue('locFilterBrowse',locFilterBrowse)
  End
  if p_web.IfExistsValue('locJobNumber')
    locJobNumber = p_web.GetValue('locJobNumber')
    p_web.SetSessionValue('locJobNumber',locJobNumber)
  End
  if p_web.IfExistsValue('locIMEINumber')
    locIMEINumber = p_web.GetValue('locIMEINumber')
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  End
  if p_web.IfExistsValue('locEngineerOption')
    locEngineerOption = p_web.GetValue('locEngineerOption')
    p_web.SetSessionValue('locEngineerOption',locEngineerOption)
  End
  if p_web.IfExistsValue('locModelNumber')
    locModelNumber = p_web.GetValue('locModelNumber')
    p_web.SetSessionValue('locModelNumber',locModelNumber)
  End
  if p_web.IfExistsValue('locJobType')
    locJobType = p_web.GetValue('locJobType')
    p_web.SetSessionValue('locJobType',locJobType)
  End
  if p_web.IfExistsValue('locChargeType')
    locChargeType = p_web.GetValue('locChargeType')
    p_web.SetSessionValue('locChargeType',locChargeType)
  End
  if p_web.IfExistsValue('locDOP')
    locDOP = p_web.dformat(clip(p_web.GetValue('locDOP')),'@d06b')
    p_web.SetSessionValue('locDOP',locDOP)
  End
  if p_web.IfExistsValue('locEngineerNotes')
    locEngineerNotes = p_web.GetValue('locEngineerNotes')
    p_web.SetSessionValue('locEngineerNotes',locEngineerNotes)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('WaybillConfirmation_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    IF (p_web.GetValue('clearvars') = 1)
        p_web.SSV('locFilterBrowse',0)
        DO DeleteSessionValues
        p_web.DeleteValue('clearvars')
  !    ELSE
  !        DO ResetAllFields
    END ! IF    
      p_web.site.CancelButton.TextValue = 'Finish'
      p_web.site.CancelButton.Image = 'images/psave.png'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locWaybillNumber = p_web.RestoreValue('locWaybillNumber')
 locFilterBrowse = p_web.RestoreValue('locFilterBrowse')
 locJobNumber = p_web.RestoreValue('locJobNumber')
 locIMEINumber = p_web.RestoreValue('locIMEINumber')
 locEngineerOption = p_web.RestoreValue('locEngineerOption')
 locModelNumber = p_web.RestoreValue('locModelNumber')
 locJobType = p_web.RestoreValue('locJobType')
 locChargeType = p_web.RestoreValue('locChargeType')
 locDOP = p_web.RestoreValue('locDOP')
 locEngineerNotes = p_web.RestoreValue('locEngineerNotes')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferWaybillConfirmation')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('WaybillConfirmation_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('WaybillConfirmation_ChainTo')
    loc:formaction = p_web.GetSessionValue('WaybillConfirmation_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="WaybillConfirmation" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="WaybillConfirmation" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="WaybillConfirmation" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Waybill Confirmation') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Waybill Confirmation',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_WaybillConfirmation">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_WaybillConfirmation" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_WaybillConfirmation')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Enter Waybill Number') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_WaybillConfirmation')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_WaybillConfirmation'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('WaybillConfirmation_WaybillBrowseConfItems_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('WaybillConfirmation_WaybillConfAccessories_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('WaybillConfirmation_WaybillConfSundryItems_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locWaybillNumber')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_WaybillConfirmation')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Enter Waybill Number') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_WaybillConfirmation_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Waybill Number')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Waybill Number')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Waybill Number')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Enter Waybill Number')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locWaybillNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locWaybillNumber
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnClearWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtWaybillReceived
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_WaybillConfirmation_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtJobsOnSelectedWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFilterBrowse
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFilterBrowse
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::WaybillBrowseConfItems
      do Value::WaybillBrowseConfItems
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnCompleteWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_WaybillConfirmation_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtProcessJobsOnWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIMEINumber
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnClearJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEngineerOption
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEngineerOption
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locModelNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td rowspan="7" colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::WaybillConfAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locJobType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locJobType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locChargeType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locChargeType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locDOP
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locDOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:Engineers_Notes
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:Engineers_Notes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnProcessJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_WaybillConfirmation_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtItemsOnSundryWauybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::WaybillConfSundryItems
      do Value::WaybillConfSundryItems
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hiddenSundry
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::btnReceiveSundryWaybill
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnReceiveSundryWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locWaybillNumber  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('locWaybillNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Waybill Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locWaybillNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locWaybillNumber',p_web.GetValue('NewValue'))
    locWaybillNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locWaybillNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locWaybillNumber',p_web.GetValue('Value'))
    locWaybillNumber = p_web.GetValue('Value')
  End
  !  !region Get Waybill
  !    !sbogen:Long1 = Processed
  !    !sbogen:Long2 = Job Number
  !    !sbogen:String1 = IMEI Number
  !    !sbogen:String2 = Model Number
  !    !sbogen:String3 = Date Booked
  !    !sbogen:Byte1 = Exchange(1)/Job(0)
  !    
  !    p_web.SSV('txtWaybillReceived','')
  !    
  !    DO ClearTempFile
  !    
  !  
  !    p_web.SSV('locWaybillFound',0)
  !    p_web.SSV('locSundryWaybillFound',0)
  !    
  !    Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
  !    way:WayBillNumber = p_web.GSV('locWayBillNumber')
  !    IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey))
  !        loc:Alert = 'Waybill Number Not Found'
  !        loc:invalid = 'locWaybillNumber'
  !    ELSE
  !        
  !        p_web.FileToSessionQueue(WAYBILLS)                    
  !        
  !        wayError# = 0
  !        IF (p_web.GSV('BookingSite') = 'RRC')
  !            IF (way:WayBillType <> 1)
  !                ! ERROR 
  !                wayError# = 1
  !            ELSE
  !                IF (way:AccountNumber <> p_web.GSV('BookingAccount'))
  !                    wayError# = 1
  !                END ! IF
  !            END ! IF
  !        ELSE
  !            IF (way:WayBillType <> 0)
  !                ! ERROR 
  !                wayError# = 1
  !            END ! IF
  !        END !IF
  !        
  !        IF (wayError# = 1)
  !            loc:Alert = 'Waybill Number Not Found For This Site'
  !            loc:invalid = 'locWaybillNumber'
  !        ELSE
  !            IF (way:Received)
  !                p_web.SSV('txtWaybillReceived','Waybill Received')
  !            END ! IF
  !            
  !            IF (way:WaybillID = 300)
  !                p_web.SSV('locSundryWaybillFound',1)
  !            ELSE
  !                p_web.SSV('locWaybillFound',1) 
  !                
  !                packet = CLIP(packet) & 'document.getElementByID(''progressframe'').css.style = ''visibility:visible'''
  !                DO SendPacket
  !                
  !                Access:WAYBILLJ.ClearKey(waj:JobNumberKey)
  !                waj:WayBillNumber = way:WayBillNumber
  !                SET(waj:JobNumberKey,waj:JobNumberKey)
  !                LOOP UNTIL Access:WAYBILLJ.Next() <> Level:Benign
  !                    IF (waj:WayBillNumber <> way:WayBillNumber)
  !                        BREAK
  !                    END ! IF
  !                    
  !                    Access:JOBS.ClearKey(job:Ref_Number_Key)
  !                    job:Ref_Number = waj:JobNumber
  !                    IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
  !                    END ! IF
  !                    
  !                    IF (Access:SBO_GenericFile.PrimeRecord() = Level:Benign)
  !                        sbogen:SessionID = p_web.SessionID
  !                        sbogen:Long2 = waj:JobNumber
  !                        sbogen:String1 = waj:IMEINumber
  !                        sbogen:String3 = job:date_booked
  !                        
  !                        CASE waj:JobType
  !                        OF 'JOB'
  !                            sbogen:Byte1 = 0                            
  !                            sbogen:String2 = job:Model_Number
  !                            IF (p_web.GSV('BookingSite') = 'RRC')
  !                                IF (job:Location = p_web.GSV('Default:InTransitRRC'))
  !                                    sbogen:Long1 = 0
  !                                ELSE
  !                                    sbogen:Long1 = 1
  !                                END ! IF
  !                            ELSE ! IF
  !                                IF (job:Location = p_web.GSV('Default:InTransitARC'))
  !                                    sbogen:Long1 = 0
  !                                ELSE
  !                                    sbogen:Long1 = 1
  !                                END ! IF
  !                            END ! IF
  !                            
  !                            
  !                        OF 'EXC'
  !                            sbogen:Byte1 = 1
  !                            Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
  !                            xch:Ref_Number = job:Exchange_Unit_Number
  !                            IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
  !                            END !I F
  !                            sbogen:String2 = xch:Model_Number
  !                            
  !                            If Sub(job:Exchange_Status,1,3) = Sub(GETINI('RRC','ExchangeStatusDespatchToRRC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3)
  !                                sbogen:Long1 = 0
  !                            ELSE ! IF
  !                                sbogen:Long1 = 1
  !                            END !IF
  !                        END ! CASE
  !                        IF (Access:SBO_GenericFile.TryInsert())
  !                            Access:SBO_GenericFile.CancelAutoInc()
  !                        END ! IF
  !                    END ! IF
  !                END!  LOOp
  !                packet = CLIP(packet) & 'document.getElementByID(''progressframe'').css.style = ''visibility:hidden'''
  !                DO SendPacket
  !            END ! IF
  !        END ! IF
  !    END ! IF
  !    DO ResetAllFields
  !   
  !  !endregion
  do Value::locWaybillNumber
  do SendAlert

Value::locWaybillNumber  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('locWaybillNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locWaybillNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('locWaybillFound') = 1 OR p_web.GSV('locSundryWaybillFound') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('locWaybillFound') = 1 OR p_web.GSV('locSundryWaybillFound') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('locWaybillNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&clip('window.open(''PageProcess?ProcessType=WaybillConfirmation&ReturnURL=WaybillConfirmation&RedirectURL=WaybillConfirmation&locWaybillNumber='' + document.getElementById(''locWaybillNumber'').value,''_self'')')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locWaybillNumber',p_web.GetSessionValueFormat('locWaybillNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillConfirmation_' & p_web._nocolon('locWaybillNumber') & '_value')


Validate::btnClearWaybill  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnClearWaybill',p_web.GetValue('NewValue'))
    do Value::btnClearWaybill
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    p_web.SSV('locWaybillFound',0)
    p_web.SSV('locSundryWaybillFound',0)
    p_web.SSV('locJobFound',0)
    p_web.SSV('locWaybillNumber','')
    p_web.SSV('txtWaybillReceived','')
    DO ClearTempFile
    DO ResetAllFields
  do SendAlert
  do Value::locWaybillNumber  !1

Value::btnClearWaybill  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('btnClearWaybill') & '_value',Choose(p_web.GSV('locWaybillFound') = 0 AND p_web.GSV('locSundryWaybillFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locWaybillFound') = 0 AND p_web.GSV('locSundryWaybillFound') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnClearWaybill'',''waybillconfirmation_btnclearwaybill_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnClearWaybill','Clear Field','button-flat',loc:formname,,,'window.open('''& p_web._MakeURL(clip('WaybillConfirmation?' &'clearvars=1')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillConfirmation_' & p_web._nocolon('btnClearWaybill') & '_value')


Validate::hidden  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden',p_web.GetValue('NewValue'))
    do Value::hidden
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('hidden') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::txtWaybillReceived  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtWaybillReceived',p_web.GetValue('NewValue'))
    do Value::txtWaybillReceived
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtWaybillReceived  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('txtWaybillReceived') & '_value',Choose(p_web.GSV('locWaybillFound') = 0 AND p_web.GSV('locSundryWaybillFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locWaybillFound') = 0 AND p_web.GSV('locSundryWaybillFound') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web.Translate(p_web.GSV('txtWaybillReceived'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Validate::txtJobsOnSelectedWaybill  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtJobsOnSelectedWaybill',p_web.GetValue('NewValue'))
    do Value::txtJobsOnSelectedWaybill
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtJobsOnSelectedWaybill  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('txtJobsOnSelectedWaybill') & '_value',Choose(p_web.GSV('locWaybillFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locWaybillFound') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('LargeText bold')&'">' & p_web.Translate('Jobs On Selected Waybill',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::locFilterBrowse  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('locFilterBrowse') & '_prompt',Choose(p_web.GSV('locWaybillFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Filter Browse')
  If p_web.GSV('locWaybillFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFilterBrowse  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFilterBrowse',p_web.GetValue('NewValue'))
    locFilterBrowse = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFilterBrowse
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFilterBrowse',p_web.GetValue('Value'))
    locFilterBrowse = p_web.GetValue('Value')
  End
  do Value::locFilterBrowse
  do SendAlert
  do Value::WaybillBrowseConfItems  !1

Value::locFilterBrowse  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('locFilterBrowse') & '_value',Choose(p_web.GSV('locWaybillFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locWaybillFound') = 0)
  ! --- RADIO --- locFilterBrowse
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locFilterBrowse')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locFilterBrowse') = 0
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locFilterBrowse'',''waybillconfirmation_locfilterbrowse_value'',1,'''&clip(0)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locFilterBrowse')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locFilterBrowse',clip(0),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locFilterBrowse_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Un-processed Jobs') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locFilterBrowse') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locFilterBrowse'',''waybillconfirmation_locfilterbrowse_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locFilterBrowse')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locFilterBrowse',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locFilterBrowse_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Processed Jobs') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillConfirmation_' & p_web._nocolon('locFilterBrowse') & '_value')


Validate::hidden2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden2',p_web.GetValue('NewValue'))
    do Value::hidden2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden2  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('hidden2') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::WaybillBrowseConfItems  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('WaybillBrowseConfItems') & '_prompt',Choose(p_web.GSV('locWaybillFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locWaybillFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::WaybillBrowseConfItems  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('WaybillBrowseConfItems',p_web.GetValue('NewValue'))
    do Value::WaybillBrowseConfItems
  Else
    p_web.StoreValue('sbogen:RecordNumber')
  End

Value::WaybillBrowseConfItems  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('locWaybillFound') = 0,1,0))
  ! --- BROWSE ---  WaybillBrowseConfItems --
  p_web.SetValue('WaybillBrowseConfItems:NoForm',1)
  p_web.SetValue('WaybillBrowseConfItems:FormName',loc:formname)
  p_web.SetValue('WaybillBrowseConfItems:parentIs','Form')
  p_web.SetValue('_parentProc','WaybillConfirmation')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('WaybillConfirmation_WaybillBrowseConfItems_embedded_div')&'"><!-- Net:WaybillBrowseConfItems --></div><13,10>'
    p_web._DivHeader('WaybillConfirmation_' & lower('WaybillBrowseConfItems') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('WaybillConfirmation_' & lower('WaybillBrowseConfItems') & '_value')
  else
    packet = clip(packet) & '<!-- Net:WaybillBrowseConfItems --><13,10>'
  end
  do SendPacket


Validate::btnCompleteWaybill  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnCompleteWaybill',p_web.GetValue('NewValue'))
    do Value::btnCompleteWaybill
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do SendAlert
  do Value::btnCompleteWaybill  !1

Value::btnCompleteWaybill  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('btnCompleteWaybill') & '_value',Choose(p_web.GSV('locWaybillFound') = 0 OR p_web.GSV('locJobFound') = 1 OR p_web.GSV('way:Received') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locWaybillFound') = 0 OR p_web.GSV('locJobFound') = 1 OR p_web.GSV('way:Received') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('completeWaybill(' & CountUnprocessed() & ')')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnCompleteWaybill','Complete Waybill','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillConfirmation_' & p_web._nocolon('btnCompleteWaybill') & '_value')


Validate::txtProcessJobsOnWaybill  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtProcessJobsOnWaybill',p_web.GetValue('NewValue'))
    do Value::txtProcessJobsOnWaybill
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtProcessJobsOnWaybill  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('txtProcessJobsOnWaybill') & '_value',Choose(p_web.GSV('locWaybillFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locWaybillFound') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('LargeText bold')&'">' & p_web.Translate('Process Jobs On Waybill',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::locJobNumber  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('locJobNumber') & '_prompt',Choose(p_web.GSV('locWaybillFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Job Number')
  If p_web.GSV('locWaybillFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locJobNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobNumber',p_web.GetValue('NewValue'))
    locJobNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobNumber',p_web.GetValue('Value'))
    locJobNumber = p_web.GetValue('Value')
  End
  do Value::locJobNumber
  do SendAlert

Value::locJobNumber  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('locJobNumber') & '_value',Choose(p_web.GSV('locWaybillFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locWaybillFound') = 0)
  ! --- STRING --- locJobNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locJobNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locJobNumber'',''waybillconfirmation_locjobnumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locJobNumber',p_web.GetSessionValueFormat('locJobNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillConfirmation_' & p_web._nocolon('locJobNumber') & '_value')


Validate::hidden3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden3',p_web.GetValue('NewValue'))
    do Value::hidden3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden3  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('hidden3') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locIMEINumber  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('locIMEINumber') & '_prompt',Choose(p_web.GSV('locWaybillFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('I.M.E.I. Number')
  If p_web.GSV('locWaybillFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('NewValue'))
    locIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('Value'))
    locIMEINumber = p_web.GetValue('Value')
  End
    !Validate Job/IMEI
    p_web.SSV('locJobFound',0)
    IF (p_web.GSV('locWaybillNumber') > 0 AND |
        p_web.GSV('locJobNumber') > 0 AND |
        p_web.GSV('locIMEINumber') > '')
        
        Access:SBO_GenericFile.ClearKey(sbogen:Long2Key)
        sbogen:SessionID = p_web.SessionID
        sbogen:Long2 = p_web.GSV('locJobNumber')
        IF (Access:SBO_GenericFile.TryFetch(sbogen:Long2Key))
            ! ERROR
            loc:alert = 'Unable to find the selected job number.'
            loc:invalid = 'locJobNumber'
        ELSE !
            IF (sbogen:String1 <> p_web.GSV('locIMEINumber'))
                ! ERROR
                loc:alert = 'Unable to find the selected IMEI Number'
                loc:invalid = 'locIMEINumber'
            ELSE ! IF 
                p_web.SSV('locJobFound',1)
                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = p_web.GSV('locJobNumber')
                IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                    p_web.FileToSessionQueue(JOBS)
                    
                    IF (job:Warranty_Job = 'YES' AND job:Chargeable_Job = 'YES')
                        p_web.SSV('locJobType','WARRANTY/CHARGEABLE')
                        p_web.SSV('locChargeType',CLIP(job:Warranty_Charge_Type) & '/' & CLIP(job:Charge_Type))
                    ELSE
                        IF (job:Warranty_Job = 'YES')
                            p_web.SSV('locJobType','WARRANTY')
                            p_web.SSV('locChargeType',CLIP(job:Warranty_Charge_Type))
  
                        ELSE ! IF
                            p_web.SSV('locJobType','CHARGEABLE')
                            p_web.SSV('locChargeType',CLIP(job:Charge_Type))
                        END! IF
                    END ! IF
                    
                    p_web.SSV('locDOP',job:DOP)
                    
                END ! IF
                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                jobe:RefNumber = job:Ref_Number
                IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                    p_web.FileToSessionQueue(JOBSE)
                    
                    CASE jobe:Engineer48HourOption
                    OF 1
                        p_web.SSV('locEngineerOption','48 HOUR EXCH')
                    OF 2
                        p_web.SSV('locEngineerOption','ARC REPAIR')
                    OF 3
                        p_web.SSV('locEngineerOption','7 DAY TAT')
                    OF 4
                        p_web.SSV('locEngineerOption','STANDARD REPAIR')
                    ELSE
                        p_web.SSV('locEngineerOption','')
                    END ! CASE
                END !I F
                Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
                jbn:RefNumber = job:Ref_Number
                IF (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
                    !p_web.FileToSessionQueue(JOBNOTES)
                    locEngineerNotes = VodacomClass.EncodeHTML(jbn:Engineers_Notes)
                    p_web.SSV('locEngineerNotes',locEngineerNotes)
                END ! IF
  
                p_web.SSV('locModelNumber',job:Model_Number)
                
                IF (sbogen:Byte1 = 1)
                    Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                    xch:Ref_Number = job:Exchange_Unit_Number
                    IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                        p_web.SSV('locModelNumber',xch:Model_Number)
                    END ! IF
                END ! IF                    
                
                DO ResetAllFields
            END ! IF
        END ! 
        
    END ! IF
    
  do Value::locIMEINumber
  do SendAlert

Value::locIMEINumber  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('locIMEINumber') & '_value',Choose(p_web.GSV('locWaybillFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locWaybillFound') = 0)
  ! --- STRING --- locIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locIMEINumber'',''waybillconfirmation_locimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locIMEINumber',p_web.GetSessionValueFormat('locIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillConfirmation_' & p_web._nocolon('locIMEINumber') & '_value')


Validate::btnClearJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnClearJob',p_web.GetValue('NewValue'))
    do Value::btnClearJob
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    p_web.SSV('locJobNumber','')
    p_web.SSV('locIMEINumber','')
    p_web.SSV('locJobFound',0)
    DO ResetAllFields
  do SendAlert
  do Value::btnClearJob  !1

Value::btnClearJob  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('btnClearJob') & '_value',Choose(p_web.GSV('locWaybillFound') = 0 OR p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locWaybillFound') = 0 OR p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnClearJob'',''waybillconfirmation_btnclearjob_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnClearJob','Clear Fields','button-flat',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillConfirmation_' & p_web._nocolon('btnClearJob') & '_value')


Prompt::locEngineerOption  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('locEngineerOption') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locEngineerOption  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEngineerOption',p_web.GetValue('NewValue'))
    locEngineerOption = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEngineerOption
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEngineerOption',p_web.GetValue('Value'))
    locEngineerOption = p_web.GetValue('Value')
  End

Value::locEngineerOption  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('locEngineerOption') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- locEngineerOption
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('locEngineerOption'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::locModelNumber  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('locModelNumber') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Model Number')
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locModelNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locModelNumber',p_web.GetValue('NewValue'))
    locModelNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locModelNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locModelNumber',p_web.GetValue('Value'))
    locModelNumber = p_web.GetValue('Value')
  End

Value::locModelNumber  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('locModelNumber') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- locModelNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('locModelNumber'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Validate::WaybillConfAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('WaybillConfAccessories',p_web.GetValue('NewValue'))
    do Value::WaybillConfAccessories
  Else
    p_web.StoreValue('jac:Ref_Number')
  End

Value::WaybillConfAccessories  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('locJobFound') = 0,1,0))
  ! --- BROWSE ---  WaybillConfAccessories --
  p_web.SetValue('WaybillConfAccessories:NoForm',1)
  p_web.SetValue('WaybillConfAccessories:FormName',loc:formname)
  p_web.SetValue('WaybillConfAccessories:parentIs','Form')
  p_web.SetValue('_parentProc','WaybillConfirmation')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('WaybillConfirmation_WaybillConfAccessories_embedded_div')&'"><!-- Net:WaybillConfAccessories --></div><13,10>'
    p_web._DivHeader('WaybillConfirmation_' & lower('WaybillConfAccessories') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('WaybillConfirmation_' & lower('WaybillConfAccessories') & '_value')
  else
    packet = clip(packet) & '<!-- Net:WaybillConfAccessories --><13,10>'
  end
  do SendPacket


Prompt::locJobType  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('locJobType') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Job Type')
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locJobType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locJobType',p_web.GetValue('NewValue'))
    locJobType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locJobType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locJobType',p_web.GetValue('Value'))
    locJobType = p_web.GetValue('Value')
  End

Value::locJobType  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('locJobType') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- locJobType
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('locJobType'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::locChargeType  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('locChargeType') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Charge Type')
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locChargeType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locChargeType',p_web.GetValue('NewValue'))
    locChargeType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locChargeType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locChargeType',p_web.GetValue('Value'))
    locChargeType = p_web.GetValue('Value')
  End

Value::locChargeType  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('locChargeType') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- locChargeType
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('locChargeType'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::locDOP  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('locDOP') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('DOP')
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locDOP  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locDOP',p_web.GetValue('NewValue'))
    locDOP = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locDOP
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locDOP',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    locDOP = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End

Value::locDOP  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('locDOP') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- locDOP
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(format(p_web.GetSessionValue('locDOP'),'@d06b')) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::jbn:Engineers_Notes  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('jbn:Engineers_Notes') & '_prompt',Choose(p_web.GSV('locJobFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Engineers Notes')
  If p_web.GSV('locJobFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::jbn:Engineers_Notes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:Engineers_Notes',p_web.GetValue('NewValue'))
    locEngineerNotes = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::jbn:Engineers_Notes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEngineerNotes',p_web.GetValue('Value'))
    locEngineerNotes = p_web.GetValue('Value')
  End

Value::jbn:Engineers_Notes  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('jbn:Engineers_Notes') & '_value',Choose(p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locJobFound') = 0)
  ! --- TEXT --- locEngineerNotes
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locEngineerNotes')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
  packet = clip(packet) & p_web.CreateTextArea('locEngineerNotes',p_web.GetSessionValue('locEngineerNotes'),8,40,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(locEngineerNotes),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Validate::btnProcessJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnProcessJob',p_web.GetValue('NewValue'))
    do Value::btnProcessJob
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ! Process Job  
  do SendAlert
  do Value::btnProcessJob  !1

Value::btnProcessJob  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('btnProcessJob') & '_value',Choose(p_web.GSV('locWaybillFound') = 0 OR p_web.GSV('locJobFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locWaybillFound') = 0 OR p_web.GSV('locJobFound') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('waybillProcessJob(' & StickyNotes() & ')')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnProcessJob','Process Job > ','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillConfirmation_' & p_web._nocolon('btnProcessJob') & '_value')


Validate::txtItemsOnSundryWauybill  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtItemsOnSundryWauybill',p_web.GetValue('NewValue'))
    do Value::txtItemsOnSundryWauybill
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtItemsOnSundryWauybill  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('txtItemsOnSundryWauybill') & '_value',Choose(p_web.GSV('locSundryWaybillFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locSundryWaybillFound') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('bold')&'">' & p_web.Translate('Items On Sundry Waybill',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::WaybillConfSundryItems  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('WaybillConfSundryItems') & '_prompt',Choose(p_web.GSV('locSundryWaybillFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locSundryWaybillFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::WaybillConfSundryItems  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('WaybillConfSundryItems',p_web.GetValue('NewValue'))
    do Value::WaybillConfSundryItems
  Else
    p_web.StoreValue('was:RecordNumber')
  End

Value::WaybillConfSundryItems  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('locSundryWaybillFound') = 0,1,0))
  ! --- BROWSE ---  WaybillConfSundryItems --
  p_web.SetValue('WaybillConfSundryItems:NoForm',1)
  p_web.SetValue('WaybillConfSundryItems:FormName',loc:formname)
  p_web.SetValue('WaybillConfSundryItems:parentIs','Form')
  p_web.SetValue('_parentProc','WaybillConfirmation')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('WaybillConfirmation_WaybillConfSundryItems_embedded_div')&'"><!-- Net:WaybillConfSundryItems --></div><13,10>'
    p_web._DivHeader('WaybillConfirmation_' & lower('WaybillConfSundryItems') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('WaybillConfirmation_' & lower('WaybillConfSundryItems') & '_value')
  else
    packet = clip(packet) & '<!-- Net:WaybillConfSundryItems --><13,10>'
  end
  do SendPacket


Validate::hiddenSundry  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hiddenSundry',p_web.GetValue('NewValue'))
    do Value::hiddenSundry
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hiddenSundry  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('hiddenSundry') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::btnReceiveSundryWaybill  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('btnReceiveSundryWaybill') & '_prompt',Choose(p_web.GSV('locSundryWaybillFound') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locSundryWaybillFound') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::btnReceiveSundryWaybill  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnReceiveSundryWaybill',p_web.GetValue('NewValue'))
    do Value::btnReceiveSundryWaybill
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    ! Receive Sundry Waybill
    Access:WAYBILLS.ClearKey(way:RecordNumberKey)
    way:RecordNumber = p_web.GSV('way:RecordNumber')
    IF (Access:WAYBILLS.TryFetch(way:RecordNumberKey) = Level:Benign)
        way:Received = TRUE
        IF (Access:WAYBILLS.TryUpdate() = Level:Benign)
            If Access:WAYAUDIT.PrimeRecord() = Level:Benign
                waa:WAYBILLSRecordNumber = way:RecordNumber
                waa:UserCode = p_web.GSV('BookingUserCode')
                waa:TheDate = Today()
                waa:TheTime = Clock()
                waa:Action = 'WAYBILL RECEIVED'
                If Access:WAYAUDIT.TryInsert() = Level:Benign
                        !Insert
                Else ! If Access:WAYAUDIT.TryInsert() = Level:Benign
                    Access:WAYAUDIT.CancelAutoInc()
                End ! If Access:WAYAUDIT.TryInsert() = Level:Benign
            End ! If Access.WAYAUDIT.PrimeRecord() = Level:Benign            
            loc:Alert = 'Waybill Received'
            p_web.SSV('locWaybillFound',0)
            p_web.SSV('locSundryWaybillFound',0)
            p_web.SSV('locWaybillNumber','')
            DO ResetAllFields
        END ! IF
    END ! IF
  do SendAlert
  do Value::btnReceiveSundryWaybill  !1

Value::btnReceiveSundryWaybill  Routine
  p_web._DivHeader('WaybillConfirmation_' & p_web._nocolon('btnReceiveSundryWaybill') & '_value',Choose(p_web.GSV('locSundryWaybillFound') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locSundryWaybillFound') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnReceiveSundryWaybill'',''waybillconfirmation_btnreceivesundrywaybill_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnReceiveSundryWaybill','Receive Waybill','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillConfirmation_' & p_web._nocolon('btnReceiveSundryWaybill') & '_value')


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('WaybillConfirmation_locWaybillNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locWaybillNumber
      else
        do Value::locWaybillNumber
      end
  of lower('WaybillConfirmation_btnClearWaybill_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnClearWaybill
      else
        do Value::btnClearWaybill
      end
  of lower('WaybillConfirmation_locFilterBrowse_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locFilterBrowse
      else
        do Value::locFilterBrowse
      end
  of lower('WaybillConfirmation_btnCompleteWaybill_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnCompleteWaybill
      else
        do Value::btnCompleteWaybill
      end
  of lower('WaybillConfirmation_locJobNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locJobNumber
      else
        do Value::locJobNumber
      end
  of lower('WaybillConfirmation_locIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIMEINumber
      else
        do Value::locIMEINumber
      end
  of lower('WaybillConfirmation_btnClearJob_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnClearJob
      else
        do Value::btnClearJob
      end
  of lower('WaybillConfirmation_btnProcessJob_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnProcessJob
      else
        do Value::btnProcessJob
      end
  of lower('WaybillConfirmation_btnReceiveSundryWaybill_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnReceiveSundryWaybill
      else
        do Value::btnReceiveSundryWaybill
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('WaybillConfirmation_form:ready_',1)
  p_web.SetSessionValue('WaybillConfirmation_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_WaybillConfirmation',0)

PreCopy  Routine
  p_web.SetValue('WaybillConfirmation_form:ready_',1)
  p_web.SetSessionValue('WaybillConfirmation_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_WaybillConfirmation',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('WaybillConfirmation_form:ready_',1)
  p_web.SetSessionValue('WaybillConfirmation_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('WaybillConfirmation:Primed',0)

PreDelete       Routine
  p_web.SetValue('WaybillConfirmation_form:ready_',1)
  p_web.SetSessionValue('WaybillConfirmation_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('WaybillConfirmation:Primed',0)
  p_web.setsessionvalue('showtab_WaybillConfirmation',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('WaybillConfirmation_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('WaybillConfirmation_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! tab = 3
    loc:InvalidTab += 1
  ! tab = 4
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('WaybillConfirmation:Primed',0)
  p_web.StoreValue('locWaybillNumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locFilterBrowse')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locJobNumber')
  p_web.StoreValue('')
  p_web.StoreValue('locIMEINumber')
  p_web.StoreValue('')
  p_web.StoreValue('locEngineerOption')
  p_web.StoreValue('locModelNumber')
  p_web.StoreValue('')
  p_web.StoreValue('locJobType')
  p_web.StoreValue('locChargeType')
  p_web.StoreValue('locDOP')
  p_web.StoreValue('locEngineerNotes')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locWaybillNumber',locWaybillNumber) ! STRING(30)
     p_web.SSV('locJobNumber',locJobNumber) ! STRING(30)
     p_web.SSV('locIMEINumber',locIMEINumber) ! STRING(30)
     p_web.SSV('locFilterBrowse',locFilterBrowse) ! LONG
     p_web.SSV('locWaybillFound',locWaybillFound) ! LONG
     p_web.SSV('locSundryWaybillFound',locSundryWaybillFound) ! LONG
     p_web.SSV('locJobFound',locJobFound) ! LONG
     p_web.SSV('locEngineerOption',locEngineerOption) ! STRING(30)
     p_web.SSV('locModelNumber',locModelNumber) ! STRING(30)
     p_web.SSV('locJobType',locJobType) ! STRING(60)
     p_web.SSV('locChargeType',locChargeType) ! STRING(60)
     p_web.SSV('locDOP',locDOP) ! STRING(30)
     p_web.SSV('locEngineerNotes',locEngineerNotes) ! STRING(255)
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locWaybillNumber = p_web.GSV('locWaybillNumber') ! STRING(30)
     locJobNumber = p_web.GSV('locJobNumber') ! STRING(30)
     locIMEINumber = p_web.GSV('locIMEINumber') ! STRING(30)
     locFilterBrowse = p_web.GSV('locFilterBrowse') ! LONG
     locWaybillFound = p_web.GSV('locWaybillFound') ! LONG
     locSundryWaybillFound = p_web.GSV('locSundryWaybillFound') ! LONG
     locJobFound = p_web.GSV('locJobFound') ! LONG
     locEngineerOption = p_web.GSV('locEngineerOption') ! STRING(30)
     locModelNumber = p_web.GSV('locModelNumber') ! STRING(30)
     locJobType = p_web.GSV('locJobType') ! STRING(60)
     locChargeType = p_web.GSV('locChargeType') ! STRING(60)
     locDOP = p_web.GSV('locDOP') ! STRING(30)
     locEngineerNotes = p_web.GSV('locEngineerNotes') ! STRING(255)
CountUnprocessed    PROCEDURE()!,LONG
retValue LONG(0)
    CODE
       ! Process Job
        Access:SBO_GenericFile.ClearKey(sbogen:Long2Key)
        sbogen:SessionID = p_web.SessionID
        SET(sbogen:Long2Key,sbogen:Long2Key)
        LOOP UNTIL Access:SBO_GenericFile.Next() <> Level:Benign
            IF (sbogen:SessionID <> p_web.SessionID)
                BREAK
            END ! IF
            IF (sbogen:Long1 = 0)
            ! Unprocesed!
                retValue = 1
                BREAK
            END ! IF
        END ! LOOP
        
        RETURN retValue
StickyNotes         PROCEDURE()!,LONG
retValue                LONG(0)
    CODE
        Access:CONTHIST.ClearKey(cht:KeyRefSticky)
        cht:Ref_Number = p_web.GSV('locJobNumber')
        cht:SN_StickyNote = 'Y'
        SET(cht:KeyRefSticky,cht:KeyRefSticky)
        LOOP UNTIL Access:CONTHIST.Next() <> Level:Benign
            IF (cht:Ref_Number <> p_web.GSV('locJobNumber') OR  |
                cht:SN_StickyNote <> 'Y')
                BREAK
            END ! IF
            IF (cht:SN_Completed <> 'Y' AND cht:SN_WaybillConf = 'Y')
                retValue = 1
                BREAK
            END ! IF            
        END ! LOOP
        
        RETURN retValue
