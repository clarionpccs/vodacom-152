

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER235.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ClearSBOGenericFile  PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
        DO OpenFiles
        STREAM(SBO_GenericFile)
    Access:SBO_GenericFile.Clearkey(sbogen:Long1Key)
    sbogen:SessionID = p_web.SessionID
    SET(sbogen:Long1Key,sbogen:Long1Key)
    LOOP UNTIL Access:SBO_GenericFile.Next()
        IF (sbogen:SessionID <> p_web.SessionID)
            BREAK
        END

        Access:SBO_GenericFile.DeleteRecord(0)
    END
        FLUSH(SBO_GenericFile)
        
    DO CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:SBO_GenericFile.Open                              ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SBO_GenericFile.UseFile                           ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:SBO_GenericFile.Close
     FilesOpened = False
  END
