

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER239.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER010.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER120.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER127.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER241.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER245.INC'),ONCE        !Req'd for module callout resolution
                     END


WaybillCreateSundry  PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locDestination       STRING(30)                            !
locTransport         STRING(30)                            !
locSecurityPackNumber STRING(30)                           !
locAccountNumber     STRING(30)                            !
locCompanyName       STRING(30)                            !
locAddress1          STRING(30)                            !
locAddress2          STRING(30)                            !
locAddress3          STRING(30)                            !
locPostcode          STRING(30)                            !
locHub               STRING(30)                            !
locTelephoneNumber   STRING(30)                            !
locContactName       STRING(30)                            !
locEmailAddress      STRING(255)                           !
locUserNotes         STRING(255)                           !
locSendToOtherAddress LONG                                 !
                    MAP
CountItems              PROCEDURE(),LONG
                    END ! MAP
FilesOpened     Long
SBO_GenericFile::State  USHORT
SUBURB::State  USHORT
COURIER::State  USHORT
TRADEACC::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('WaybillCreateSundry')
  loc:formname = 'WaybillCreateSundry_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'WaybillCreateSundry',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('WaybillCreateSundry','')
    p_web._DivHeader('WaybillCreateSundry',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferWaybillCreateSundry',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWaybillCreateSundry',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWaybillCreateSundry',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_WaybillCreateSundry',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWaybillCreateSundry',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_WaybillCreateSundry',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'WaybillCreateSundry',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
DeleteSessionValues ROUTINE ! Auto generated by Bryan's Template
    ! Local Variables
    ! p_web.DeleteSessionValue('Ans') ! Excluded
    p_web.DeleteSessionValue('locDestination')
    p_web.DeleteSessionValue('locTransport')
    p_web.DeleteSessionValue('locSecurityPackNumber')
    p_web.DeleteSessionValue('locAccountNumber')
    p_web.DeleteSessionValue('locCompanyName')
    p_web.DeleteSessionValue('locAddress1')
    p_web.DeleteSessionValue('locAddress2')
    p_web.DeleteSessionValue('locAddress3')
    p_web.DeleteSessionValue('locPostcode')
    p_web.DeleteSessionValue('locHub')
    p_web.DeleteSessionValue('locTelephoneNumber')
    p_web.DeleteSessionValue('locContactName')
    p_web.DeleteSessionValue('locEmailAddress')
    p_web.DeleteSessionValue('locUserNotes')
    p_web.DeleteSessionValue('locSendToOtherAddress')

    ! Other Variables
!DeleteSessionValues Continued.....
    Access:SBO_GenericFile.ClearKey(sbogen:Long1Key)
    sbogen:SessionID = p_web.SessionID
    SET(sbogen:Long1Key,sbogen:Long1Key)
    LOOP UNTIL Access:SBO_GenericFile.Next() <> Level:Benign
        IF (sbogen:SessionID <> p_web.SessionID)
            BREAK
        END ! IF
        Access:SBO_GenericFile.DeleteRecord(0)
    END ! LOOP

OpenFiles  ROUTINE
  p_web._OpenFile(SBO_GenericFile)
  p_web._OpenFile(SUBURB)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(TRADEACC)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(SBO_GenericFile)
  p_Web._CloseFile(SUBURB)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(TRADEACC)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
        IF (p_web.GetValue('clearvars') = 1)
            DO DeleteSessionValues
        END ! IF
  p_web.SetValue('WaybillCreateSundry_form:inited_',1)
  do RestoreMem

CancelForm  Routine
    DO DeleteSessionValues  

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'locDestination'
    p_web.setsessionvalue('showtab_WaybillCreateSundry',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(TRADEACC)
        p_web.setsessionvalue('locAccountNumber',tra:Account_Number)
        p_web.setsessionvalue('locCompanyName',tra:Company_Name)
        p_web.setsessionvalue('locAddress1',tra:Address_Line1)
        p_web.setsessionvalue('locAddress2',tra:Address_Line2)
        p_web.setsessionvalue('locAddress3',tra:Address_Line3)
        p_web.setsessionvalue('locPostcode',tra:Postcode)
        p_web.setsessionvalue('locHub',tra:Hub)
        p_web.setsessionvalue('locTelephoneNumber',tra:Telephone_Number)
        p_web.setsessionvalue('locContactName',tra:Contact_Name)
        p_web.setsessionvalue('locEmailAddress',tra:EmailAddress)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.')
  Of 'locTransport'
    p_web.setsessionvalue('showtab_WaybillCreateSundry',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(COURIER)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locSecurityPackNumber')
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'locAddress3'
    p_web.setsessionvalue('showtab_WaybillCreateSundry',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(SUBURB)
        p_web.setsessionvalue('locPostcode',sur:Postcode)
        p_web.setsessionvalue('locHub',sur:Hub)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locPostcode')
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locSendToOtherAddress',locSendToOtherAddress)
  p_web.SetSessionValue('locDestination',locDestination)
  p_web.SetSessionValue('locTransport',locTransport)
  p_web.SetSessionValue('locSecurityPackNumber',locSecurityPackNumber)
  p_web.SetSessionValue('locAccountNumber',locAccountNumber)
  p_web.SetSessionValue('locCompanyName',locCompanyName)
  p_web.SetSessionValue('locAddress1',locAddress1)
  p_web.SetSessionValue('locAddress2',locAddress2)
  p_web.SetSessionValue('locAddress3',locAddress3)
  p_web.SetSessionValue('locPostcode',locPostcode)
  p_web.SetSessionValue('locHub',locHub)
  p_web.SetSessionValue('locTelephoneNumber',locTelephoneNumber)
  p_web.SetSessionValue('locContactName',locContactName)
  p_web.SetSessionValue('locEmailAddress',locEmailAddress)
  p_web.SetSessionValue('locUserNotes',locUserNotes)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locSendToOtherAddress')
    locSendToOtherAddress = p_web.GetValue('locSendToOtherAddress')
    p_web.SetSessionValue('locSendToOtherAddress',locSendToOtherAddress)
  End
  if p_web.IfExistsValue('locDestination')
    locDestination = p_web.GetValue('locDestination')
    p_web.SetSessionValue('locDestination',locDestination)
  End
  if p_web.IfExistsValue('locTransport')
    locTransport = p_web.GetValue('locTransport')
    p_web.SetSessionValue('locTransport',locTransport)
  End
  if p_web.IfExistsValue('locSecurityPackNumber')
    locSecurityPackNumber = p_web.GetValue('locSecurityPackNumber')
    p_web.SetSessionValue('locSecurityPackNumber',locSecurityPackNumber)
  End
  if p_web.IfExistsValue('locAccountNumber')
    locAccountNumber = p_web.GetValue('locAccountNumber')
    p_web.SetSessionValue('locAccountNumber',locAccountNumber)
  End
  if p_web.IfExistsValue('locCompanyName')
    locCompanyName = p_web.GetValue('locCompanyName')
    p_web.SetSessionValue('locCompanyName',locCompanyName)
  End
  if p_web.IfExistsValue('locAddress1')
    locAddress1 = p_web.GetValue('locAddress1')
    p_web.SetSessionValue('locAddress1',locAddress1)
  End
  if p_web.IfExistsValue('locAddress2')
    locAddress2 = p_web.GetValue('locAddress2')
    p_web.SetSessionValue('locAddress2',locAddress2)
  End
  if p_web.IfExistsValue('locAddress3')
    locAddress3 = p_web.GetValue('locAddress3')
    p_web.SetSessionValue('locAddress3',locAddress3)
  End
  if p_web.IfExistsValue('locPostcode')
    locPostcode = p_web.GetValue('locPostcode')
    p_web.SetSessionValue('locPostcode',locPostcode)
  End
  if p_web.IfExistsValue('locHub')
    locHub = p_web.GetValue('locHub')
    p_web.SetSessionValue('locHub',locHub)
  End
  if p_web.IfExistsValue('locTelephoneNumber')
    locTelephoneNumber = p_web.GetValue('locTelephoneNumber')
    p_web.SetSessionValue('locTelephoneNumber',locTelephoneNumber)
  End
  if p_web.IfExistsValue('locContactName')
    locContactName = p_web.GetValue('locContactName')
    p_web.SetSessionValue('locContactName',locContactName)
  End
  if p_web.IfExistsValue('locEmailAddress')
    locEmailAddress = p_web.GetValue('locEmailAddress')
    p_web.SetSessionValue('locEmailAddress',locEmailAddress)
  End
  if p_web.IfExistsValue('locUserNotes')
    locUserNotes = p_web.GetValue('locUserNotes')
    p_web.SetSessionValue('locUserNotes',locUserNotes)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('WaybillCreateSundry_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    IF (DoesUserHaveAccess(p_web,'RAPID - CREATE SUNDRY WAYBILL') = FALSE)
        !packet = CLIP(packet) & '<script>alert("You do not have access to this option");window.open("IndexPage","_self");</script>'
        CreateScript(p_web,packet,'alert("ou do not have access to this option")')
        CreateScript(p_web,packet,'window.open("IndexPage","_self")')
        DO SendPacket
        EXIT
    END ! IF  
      p_web.site.CancelButton.TextValue = 'Close'
      p_web.site.CancelButton.Image = 'images/psave.png'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locSendToOtherAddress = p_web.RestoreValue('locSendToOtherAddress')
 locDestination = p_web.RestoreValue('locDestination')
 locTransport = p_web.RestoreValue('locTransport')
 locSecurityPackNumber = p_web.RestoreValue('locSecurityPackNumber')
 locAccountNumber = p_web.RestoreValue('locAccountNumber')
 locCompanyName = p_web.RestoreValue('locCompanyName')
 locAddress1 = p_web.RestoreValue('locAddress1')
 locAddress2 = p_web.RestoreValue('locAddress2')
 locAddress3 = p_web.RestoreValue('locAddress3')
 locPostcode = p_web.RestoreValue('locPostcode')
 locHub = p_web.RestoreValue('locHub')
 locTelephoneNumber = p_web.RestoreValue('locTelephoneNumber')
 locContactName = p_web.RestoreValue('locContactName')
 locEmailAddress = p_web.RestoreValue('locEmailAddress')
 locUserNotes = p_web.RestoreValue('locUserNotes')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferWaybillCreateSundry')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('WaybillCreateSundry_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('WaybillCreateSundry_ChainTo')
    loc:formaction = p_web.GetSessionValue('WaybillCreateSundry_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="WaybillCreateSundry" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="WaybillCreateSundry" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="WaybillCreateSundry" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Create Sundry Waybill') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Create Sundry Waybill',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_WaybillCreateSundry">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_WaybillCreateSundry" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_WaybillCreateSundry')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Waybill Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Destination Address') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_WaybillCreateSundry')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_WaybillCreateSundry'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('WaybillCreateSundry_WaybillBrowseSundryItems_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='TRADEACC'
            p_web.SetValue('SelectField',clip(loc:formname) & '.locTransport')
    End
    If upper(p_web.getvalue('LookupFile'))='COURIER'
            p_web.SetValue('SelectField',clip(loc:formname) & '.locSecurityPackNumber')
    End
    If upper(p_web.getvalue('LookupFile'))='SUBURB'
            p_web.SetValue('SelectField',clip(loc:formname) & '.locTelephoneNumber')
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locSendToOtherAddress')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_WaybillCreateSundry')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Waybill Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_WaybillCreateSundry_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Waybill Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Waybill Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Waybill Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Waybill Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSendToOtherAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSendToOtherAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locSendToOtherAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locDestination
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locDestination
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locDestination
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::hidden
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locTransport
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locTransport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locTransport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSecurityPackNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSecurityPackNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locSecurityPackNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Destination Address') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_WaybillCreateSundry_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Destination Address')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Destination Address')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Destination Address')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Destination Address')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAccountNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAccountNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAccountNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::hidden2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCompanyName
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCompanyName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCompanyName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAddress1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAddress1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAddress1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAddress2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAddress2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAddress2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAddress3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAddress3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locAddress3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPostcode
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPostcode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locPostcode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locHub
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locHub
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locHub
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locTelephoneNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locTelephoneNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locTelephoneNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locContactName
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locContactName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locContactName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEmailAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEmailAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locEmailAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_WaybillCreateSundry_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::WaybillBrowseSundryItems
      do Comment::WaybillBrowseSundryItems
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locUserNotes
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locUserNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locUserNotes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&280&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnGenerateWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnGenerateWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locSendToOtherAddress  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locSendToOtherAddress') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Send To Other Address')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locSendToOtherAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSendToOtherAddress',p_web.GetValue('NewValue'))
    locSendToOtherAddress = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSendToOtherAddress
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locSendToOtherAddress',p_web.GetValue('Value'))
    locSendToOtherAddress = p_web.GetValue('Value')
  End
    IF (p_web.GSV('locSendTootherAddress') = 0)
        p_web.SSV('locDestination','')
    ELSE
        p_web.SSV('locDestination','OTHER')
    END ! 
    p_web.SSV('locAccountNumber','')
    p_web.SSV('locCompanyName','')
    p_web.SSV('locAddress1','')
    p_web.SSV('locAddress2','')
    p_web.SSV('locAddress3','')
    p_web.SSV('locPostcode','')
    p_web.SSV('locHub','')
    p_web.SSV('locTelephoneNumber','')
    p_web.SSV('locContactName','')
    p_web.SSV('locEmailAddress','')    
  do Value::locSendToOtherAddress
  do SendAlert
  do Value::locAccountNumber  !1
  do Value::locCompanyName  !1
  do Value::locAddress1  !1
  do Value::locAddress2  !1
  do Value::locAddress3  !1
  do Value::locPostcode  !1
  do Value::locHub  !1
  do Value::locTelephoneNumber  !1
  do Value::locContactName  !1
  do Value::locEmailAddress  !1
  do Value::locDestination  !1
  do Value::btnGenerateWaybill  !1

Value::locSendToOtherAddress  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locSendToOtherAddress') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locSendToOtherAddress
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locSendToOtherAddress'',''waybillcreatesundry_locsendtootheraddress_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSendToOtherAddress')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locSendToOtherAddress') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locSendToOtherAddress',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locSendToOtherAddress') & '_value')

Comment::locSendToOtherAddress  Routine
    loc:comment = ''
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locSendToOtherAddress') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locDestination  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locDestination') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Destination')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locDestination  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locDestination',p_web.GetValue('NewValue'))
    locDestination = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locDestination
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locDestination',p_web.GetValue('Value'))
    locDestination = p_web.GetValue('Value')
  End
  If locDestination = ''
    loc:Invalid = 'locDestination'
    loc:alert = p_web.translate('Destination') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locDestination = Upper(locDestination)
    p_web.SetSessionValue('locDestination',locDestination)
  p_Web.SetValue('lookupfield','locDestination')
  do AfterLookup
  do Value::locAccountNumber
  do Value::locCompanyName
  do Value::locAddress1
  do Value::locAddress2
  do Value::locAddress3
  do Value::locPostcode
  do Value::locHub
  do Value::locTelephoneNumber
  do Value::locContactName
  do Value::locEmailAddress
  do Value::locDestination
  do SendAlert
  do Comment::locDestination
  do Value::locAccountNumber  !1
  do Value::locCompanyName  !1

Value::locDestination  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locDestination') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locDestination
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('locSendToOtherAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('locSendToOtherAddress') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locDestination')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locDestination = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locDestination'',''waybillcreatesundry_locdestination_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locDestination')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','locDestination',p_web.GetSessionValueFormat('locDestination'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('LookupRRCHeadAccounts?LookupField=locDestination&Tab=2&ForeignField=tra:Account_Number&_sort=tra:Account_Number&Refresh=sort&LookupFrom=WaybillCreateSundry&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locDestination') & '_value')

Comment::locDestination  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locDestination') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locDestination') & '_comment')

Validate::hidden  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden',p_web.GetValue('NewValue'))
    do Value::hidden
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('hidden') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::hidden  Routine
    loc:comment = ''
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('hidden') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locTransport  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locTransport') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Mode Of Transport')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locTransport  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locTransport',p_web.GetValue('NewValue'))
    locTransport = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locTransport
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locTransport',p_web.GetValue('Value'))
    locTransport = p_web.GetValue('Value')
  End
  If locTransport = ''
    loc:Invalid = 'locTransport'
    loc:alert = p_web.translate('Mode Of Transport') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locTransport = Upper(locTransport)
    p_web.SetSessionValue('locTransport',locTransport)
  p_Web.SetValue('lookupfield','locTransport')
  do AfterLookup
  do Value::locTransport
  do SendAlert
  do Comment::locTransport
  do Value::btnGenerateWaybill  !1

Value::locTransport  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locTransport') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locTransport
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locTransport')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locTransport = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locTransport'',''waybillcreatesundry_loctransport_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locTransport')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','locTransport',p_web.GetSessionValueFormat('locTransport'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('SelectCouriers?LookupField=locTransport&Tab=2&ForeignField=cou:Courier&_sort=cou:Courier&Refresh=sort&LookupFrom=WaybillCreateSundry&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locTransport') & '_value')

Comment::locTransport  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locTransport') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locTransport') & '_comment')

Prompt::locSecurityPackNumber  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locSecurityPackNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Security Pack No')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locSecurityPackNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSecurityPackNumber',p_web.GetValue('NewValue'))
    locSecurityPackNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSecurityPackNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locSecurityPackNumber',p_web.GetValue('Value'))
    locSecurityPackNumber = p_web.GetValue('Value')
  End
  If locSecurityPackNumber = ''
    loc:Invalid = 'locSecurityPackNumber'
    loc:alert = p_web.translate('Security Pack No') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locSecurityPackNumber = Upper(locSecurityPackNumber)
    p_web.SetSessionValue('locSecurityPackNumber',locSecurityPackNumber)
  do Value::locSecurityPackNumber
  do SendAlert
  do Value::btnGenerateWaybill  !1

Value::locSecurityPackNumber  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locSecurityPackNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locSecurityPackNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locSecurityPackNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locSecurityPackNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locSecurityPackNumber'',''waybillcreatesundry_locsecuritypacknumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locSecurityPackNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locSecurityPackNumber',p_web.GetSessionValueFormat('locSecurityPackNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locSecurityPackNumber') & '_value')

Comment::locSecurityPackNumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locSecurityPackNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locAccountNumber  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locAccountNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locAccountNumber') & '_prompt')

Validate::locAccountNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAccountNumber',p_web.GetValue('NewValue'))
    locAccountNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAccountNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAccountNumber',p_web.GetValue('Value'))
    locAccountNumber = p_web.GetValue('Value')
  End
    locAccountNumber = Upper(locAccountNumber)
    p_web.SetSessionValue('locAccountNumber',locAccountNumber)
  do Value::locAccountNumber
  do SendAlert
  do Value::btnGenerateWaybill  !1

Value::locAccountNumber  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locAccountNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locAccountNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('locSendToOtherAddress') = 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('locSendToOtherAddress') = 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('locAccountNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locAccountNumber'',''waybillcreatesundry_locaccountnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locAccountNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locAccountNumber',p_web.GetSessionValueFormat('locAccountNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locAccountNumber') & '_value')

Comment::locAccountNumber  Routine
      loc:comment = ''
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locAccountNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locAccountNumber') & '_comment')

Validate::hidden2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden2',p_web.GetValue('NewValue'))
    do Value::hidden2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden2  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('hidden2') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::hidden2  Routine
    loc:comment = ''
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('hidden2') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locCompanyName  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locCompanyName') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Company Name')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locCompanyName') & '_prompt')

Validate::locCompanyName  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCompanyName',p_web.GetValue('NewValue'))
    locCompanyName = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCompanyName
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCompanyName',p_web.GetValue('Value'))
    locCompanyName = p_web.GetValue('Value')
  End
  If locCompanyName = ''
    loc:Invalid = 'locCompanyName'
    loc:alert = p_web.translate('Company Name') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locCompanyName = Upper(locCompanyName)
    p_web.SetSessionValue('locCompanyName',locCompanyName)
  do Value::locCompanyName
  do SendAlert
  do Value::btnGenerateWaybill  !1

Value::locCompanyName  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locCompanyName') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locCompanyName
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('locSendToOtherAddress') = 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('locSendToOtherAddress') = 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locCompanyName')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locCompanyName = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locCompanyName'',''waybillcreatesundry_loccompanyname_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locCompanyName')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locCompanyName',p_web.GetSessionValueFormat('locCompanyName'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locCompanyName') & '_value')

Comment::locCompanyName  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locCompanyName') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locCompanyName') & '_comment')

Prompt::locAddress1  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locAddress1') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Address')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locAddress1') & '_prompt')

Validate::locAddress1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAddress1',p_web.GetValue('NewValue'))
    locAddress1 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAddress1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAddress1',p_web.GetValue('Value'))
    locAddress1 = p_web.GetValue('Value')
  End
  If locAddress1 = '' and p_web.GSV('locSendToOtherAddress') = 1
    loc:Invalid = 'locAddress1'
    loc:alert = p_web.translate('Address') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locAddress1 = Upper(locAddress1)
    p_web.SetSessionValue('locAddress1',locAddress1)
  do Value::locAddress1
  do SendAlert
  do Value::btnGenerateWaybill  !1

Value::locAddress1  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locAddress1') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locAddress1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('locSendToOtherAddress') = 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('locSendToOtherAddress') = 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GSV('locSendToOtherAddress') = 1)
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('locAddress1')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locAddress1 = '' and (p_web.GSV('locSendToOtherAddress') = 1)
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locAddress1'',''waybillcreatesundry_locaddress1_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locAddress1')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locAddress1',p_web.GetSessionValueFormat('locAddress1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locAddress1') & '_value')

Comment::locAddress1  Routine
    If p_web.GSV('locSendToOtherAddress') = 1
      loc:comment = p_web.translate(p_web.site.RequiredText)
    End
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locAddress1') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locAddress1') & '_comment')

Prompt::locAddress2  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locAddress2') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locAddress2') & '_prompt')

Validate::locAddress2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAddress2',p_web.GetValue('NewValue'))
    locAddress2 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAddress2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAddress2',p_web.GetValue('Value'))
    locAddress2 = p_web.GetValue('Value')
  End
    locAddress2 = Upper(locAddress2)
    p_web.SetSessionValue('locAddress2',locAddress2)
  do Value::locAddress2
  do SendAlert

Value::locAddress2  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locAddress2') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locAddress2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('locSendToOtherAddress') = 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('locSendToOtherAddress') = 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('locAddress2')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locAddress2'',''waybillcreatesundry_locaddress2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locAddress2',p_web.GetSessionValueFormat('locAddress2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locAddress2') & '_value')

Comment::locAddress2  Routine
      loc:comment = ''
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locAddress2') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locAddress2') & '_comment')

Prompt::locAddress3  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locAddress3') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Suburb')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locAddress3') & '_prompt')

Validate::locAddress3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAddress3',p_web.GetValue('NewValue'))
    locAddress3 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAddress3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAddress3',p_web.GetValue('Value'))
    locAddress3 = p_web.GetValue('Value')
  End
    locAddress3 = Upper(locAddress3)
    p_web.SetSessionValue('locAddress3',locAddress3)
  p_Web.SetValue('lookupfield','locAddress3')
  do AfterLookup
  do Value::locPostcode
  do Value::locHub
  do Value::locAddress3
  do SendAlert
  do Comment::locAddress3
  do Value::btnGenerateWaybill  !1

Value::locAddress3  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locAddress3') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locAddress3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('locSendToOtherAddress') = 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('locSendToOtherAddress') = 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('locAddress3')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locAddress3'',''waybillcreatesundry_locaddress3_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locAddress3')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','locAddress3',p_web.GetSessionValueFormat('locAddress3'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('LookupSuburbs?LookupField=locAddress3&Tab=2&ForeignField=sur:Suburb&_sort=&Refresh=sort&LookupFrom=WaybillCreateSundry&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locAddress3') & '_value')

Comment::locAddress3  Routine
      loc:comment = ''
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locAddress3') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locAddress3') & '_comment')

Prompt::locPostcode  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locPostcode') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Postcode')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locPostcode') & '_prompt')

Validate::locPostcode  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPostcode',p_web.GetValue('NewValue'))
    locPostcode = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPostcode
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPostcode',p_web.GetValue('Value'))
    locPostcode = p_web.GetValue('Value')
  End
    locPostcode = Upper(locPostcode)
    p_web.SetSessionValue('locPostcode',locPostcode)
  do Value::locPostcode
  do SendAlert
  do Value::btnGenerateWaybill  !1

Value::locPostcode  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locPostcode') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locPostcode
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('locSendToOtherAddress') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locPostcode')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPostcode'',''waybillcreatesundry_locpostcode_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPostcode')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locPostcode',p_web.GetSessionValueFormat('locPostcode'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locPostcode') & '_value')

Comment::locPostcode  Routine
      loc:comment = ''
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locPostcode') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locPostcode') & '_comment')

Prompt::locHub  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locHub') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Hub')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locHub') & '_prompt')

Validate::locHub  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locHub',p_web.GetValue('NewValue'))
    locHub = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locHub
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locHub',p_web.GetValue('Value'))
    locHub = p_web.GetValue('Value')
  End
  do Value::locHub
  do SendAlert
  do Value::btnGenerateWaybill  !1

Value::locHub  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locHub') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locHub
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locHub')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locHub'',''waybillcreatesundry_lochub_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locHub')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locHub',p_web.GetSessionValueFormat('locHub'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locHub') & '_value')

Comment::locHub  Routine
      loc:comment = ''
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locHub') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locHub') & '_comment')

Prompt::locTelephoneNumber  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locTelephoneNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Telephone Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locTelephoneNumber') & '_prompt')

Validate::locTelephoneNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locTelephoneNumber',p_web.GetValue('NewValue'))
    locTelephoneNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locTelephoneNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locTelephoneNumber',p_web.GetValue('Value'))
    locTelephoneNumber = p_web.GetValue('Value')
  End
  If locTelephoneNumber = ''
    loc:Invalid = 'locTelephoneNumber'
    loc:alert = p_web.translate('Telephone Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::locTelephoneNumber
  do SendAlert
  do Value::btnGenerateWaybill  !1

Value::locTelephoneNumber  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locTelephoneNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locTelephoneNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('locSendToOtherAddress') = 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('locSendToOtherAddress') = 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locTelephoneNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locTelephoneNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locTelephoneNumber'',''waybillcreatesundry_loctelephonenumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locTelephoneNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locTelephoneNumber',p_web.GetSessionValueFormat('locTelephoneNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locTelephoneNumber') & '_value')

Comment::locTelephoneNumber  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locTelephoneNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locTelephoneNumber') & '_comment')

Prompt::locContactName  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locContactName') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Contact Name')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locContactName') & '_prompt')

Validate::locContactName  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locContactName',p_web.GetValue('NewValue'))
    locContactName = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locContactName
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locContactName',p_web.GetValue('Value'))
    locContactName = p_web.GetValue('Value')
  End
  If locContactName = ''
    loc:Invalid = 'locContactName'
    loc:alert = p_web.translate('Contact Name') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::locContactName
  do SendAlert
  do Value::btnGenerateWaybill  !1

Value::locContactName  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locContactName') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locContactName
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('locSendToOtherAddress') = 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('locSendToOtherAddress') = 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locContactName')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locContactName = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locContactName'',''waybillcreatesundry_loccontactname_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locContactName')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locContactName',p_web.GetSessionValueFormat('locContactName'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locContactName') & '_value')

Comment::locContactName  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locContactName') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locContactName') & '_comment')

Prompt::locEmailAddress  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locEmailAddress') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Email Address')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locEmailAddress') & '_prompt')

Validate::locEmailAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEmailAddress',p_web.GetValue('NewValue'))
    locEmailAddress = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEmailAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEmailAddress',p_web.GetValue('Value'))
    locEmailAddress = p_web.GetValue('Value')
  End
  do Value::locEmailAddress
  do SendAlert

Value::locEmailAddress  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locEmailAddress') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locEmailAddress
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('locSendToOtherAddress') = 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('locSendToOtherAddress') = 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('locEmailAddress')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locEmailAddress'',''waybillcreatesundry_locemailaddress_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locEmailAddress',p_web.GetSessionValueFormat('locEmailAddress'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,255,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locEmailAddress') & '_value')

Comment::locEmailAddress  Routine
      loc:comment = ''
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locEmailAddress') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locEmailAddress') & '_comment')

Validate::WaybillBrowseSundryItems  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('WaybillBrowseSundryItems',p_web.GetValue('NewValue'))
    do Value::WaybillBrowseSundryItems
  Else
    p_web.StoreValue('sbogen:RecordNumber')
  End

Value::WaybillBrowseSundryItems  Routine
  loc:extra = ''
  ! --- BROWSE ---  WaybillBrowseSundryItems --
  p_web.SetValue('WaybillBrowseSundryItems:NoForm',1)
  p_web.SetValue('WaybillBrowseSundryItems:FormName',loc:formname)
  p_web.SetValue('WaybillBrowseSundryItems:parentIs','Form')
  p_web.SetValue('_parentProc','WaybillCreateSundry')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('WaybillCreateSundry_WaybillBrowseSundryItems_embedded_div')&'"><!-- Net:WaybillBrowseSundryItems --></div><13,10>'
    p_web._DivHeader('WaybillCreateSundry_' & lower('WaybillBrowseSundryItems') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('WaybillCreateSundry_' & lower('WaybillBrowseSundryItems') & '_value')
  else
    packet = clip(packet) & '<!-- Net:WaybillBrowseSundryItems --><13,10>'
  end
  do SendPacket

Comment::WaybillBrowseSundryItems  Routine
    loc:comment = ''
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('WaybillBrowseSundryItems') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locUserNotes  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locUserNotes') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('User Notes')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locUserNotes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locUserNotes',p_web.GetValue('NewValue'))
    locUserNotes = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locUserNotes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locUserNotes',p_web.GetValue('Value'))
    locUserNotes = p_web.GetValue('Value')
  End
    locUserNotes = Upper(locUserNotes)
    p_web.SetSessionValue('locUserNotes',locUserNotes)
  do Value::locUserNotes
  do SendAlert

Value::locUserNotes  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locUserNotes') & '_value','adiv')
  loc:extra = ''
  ! --- TEXT --- locUserNotes
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('locUserNotes')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locUserNotes'',''waybillcreatesundry_locusernotes_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('locUserNotes',p_web.GetSessionValue('locUserNotes'),5,40,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,255,,Net:Web:Control) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('locUserNotes') & '_value')

Comment::locUserNotes  Routine
      loc:comment = ''
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('locUserNotes') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnGenerateWaybill  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnGenerateWaybill',p_web.GetValue('NewValue'))
    do Value::btnGenerateWaybill
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  !    IF (p_web.GSV('locDestination') = '')
  !        loc:alert = 'Destination Required'
  !        loc:invalid = 'locDestination'
  !    ELSIF (p_web.GSV('locTransport') = '')
  !        loc:alert = 'Mode Of Transport Required'
  !        loc:invalid = 'locTransport'
  !    ELSIF (p_web.GSV('locSecurityPackNumber') = '')
  !        loc:alert = 'Security Pack Number Required'
  !        loc:invalid = 'locSecurityPackNumber'
  !    ELSIF (p_web.GSV('locSendToOtherAddress') = 1)
  !        IF (p_web.GSV('locCompanyName') = '')
  !            loc:alert = 'Company Name Required'
  !            loc:invalid = 'locCompanyName'
  !        ELSIF (p_web.GSV('locAddress1') = '')
  !            loc:alert = 'Address Required'
  !            loc:invalid = 'locAddress1'
  !        ELSIF (p_web.GSV('locContactName') = '')
  !            loc:alert = 'Contact Name Required'
  !            loc:invalid = 'locContactName'
  !        ELSIF (p_web.GSV('locTelephoneNumber') = '')
  !            loc:alert = 'Telephone Number Required'
  !            loc:invalid = 'locTelephoneNumber'
  !        ELSIF (p_web.GSV('locHub') = '')
  !            loc:alert = 'Hub Required'
  !            loc:invalid = 'locHub'
  !        END ! IF
  !    END 
  !    
  !    IF (loc:alert = '')
  !          packet = '<script>window.open("PageProcess?ProcessType=WaybillSundryGeneration&ReturnURL=WaybillCreateSundry&RedirectURL=WaybillCreate&wayType=Sundry","_self");</script>'
  !          DO SendPacket
  !    END ! IF
    
  do Value::btnGenerateWaybill
  do SendAlert

Value::btnGenerateWaybill  Routine
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('btnGenerateWaybill') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('generateSundryWaybill(' & CountItems() & ',''' & p_web.GSV('locSendToOtherAddress') & ''',''' & p_web.GSV('locDestination') & ''',''' & p_web.GSV('locTransport') & ''',''' & p_web.GSV('locSecurityPackNumber') & ''',''' & p_web.GSV('locCompanyName') & ''',''' & p_web.GSV('locTelephoneNumber') & ''',''' & p_web.GSV('locContactName') & ''',''' & p_web.GSV('locAddress1') & ''',''' & p_web.GSV('locHub') & ''')')&';'
  loc:javascript = clip(loc:javascript) & ' ' &p_web._nocolon('sv(''btnGenerateWaybill'',''waybillcreatesundry_btngeneratewaybill_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnGenerateWaybill','Generate Waybill','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillCreateSundry_' & p_web._nocolon('btnGenerateWaybill') & '_value')

Comment::btnGenerateWaybill  Routine
    loc:comment = ''
  p_web._DivHeader('WaybillCreateSundry_' & p_web._nocolon('btnGenerateWaybill') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('WaybillCreateSundry_locSendToOtherAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSendToOtherAddress
      else
        do Value::locSendToOtherAddress
      end
  of lower('WaybillCreateSundry_locDestination_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locDestination
      else
        do Value::locDestination
      end
  of lower('WaybillCreateSundry_locTransport_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locTransport
      else
        do Value::locTransport
      end
  of lower('WaybillCreateSundry_locSecurityPackNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locSecurityPackNumber
      else
        do Value::locSecurityPackNumber
      end
  of lower('WaybillCreateSundry_locAccountNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAccountNumber
      else
        do Value::locAccountNumber
      end
  of lower('WaybillCreateSundry_locCompanyName_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locCompanyName
      else
        do Value::locCompanyName
      end
  of lower('WaybillCreateSundry_locAddress1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAddress1
      else
        do Value::locAddress1
      end
  of lower('WaybillCreateSundry_locAddress2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAddress2
      else
        do Value::locAddress2
      end
  of lower('WaybillCreateSundry_locAddress3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAddress3
      else
        do Value::locAddress3
      end
  of lower('WaybillCreateSundry_locPostcode_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPostcode
      else
        do Value::locPostcode
      end
  of lower('WaybillCreateSundry_locHub_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locHub
      else
        do Value::locHub
      end
  of lower('WaybillCreateSundry_locTelephoneNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locTelephoneNumber
      else
        do Value::locTelephoneNumber
      end
  of lower('WaybillCreateSundry_locContactName_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locContactName
      else
        do Value::locContactName
      end
  of lower('WaybillCreateSundry_locEmailAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locEmailAddress
      else
        do Value::locEmailAddress
      end
  of lower('WaybillCreateSundry_locUserNotes_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locUserNotes
      else
        do Value::locUserNotes
      end
  of lower('WaybillCreateSundry_btnGenerateWaybill_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnGenerateWaybill
      else
        do Value::btnGenerateWaybill
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('WaybillCreateSundry_form:ready_',1)
  p_web.SetSessionValue('WaybillCreateSundry_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_WaybillCreateSundry',0)

PreCopy  Routine
  p_web.SetValue('WaybillCreateSundry_form:ready_',1)
  p_web.SetSessionValue('WaybillCreateSundry_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_WaybillCreateSundry',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('WaybillCreateSundry_form:ready_',1)
  p_web.SetSessionValue('WaybillCreateSundry_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('WaybillCreateSundry:Primed',0)

PreDelete       Routine
  p_web.SetValue('WaybillCreateSundry_form:ready_',1)
  p_web.SetSessionValue('WaybillCreateSundry_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('WaybillCreateSundry:Primed',0)
  p_web.setsessionvalue('showtab_WaybillCreateSundry',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
          If p_web.IfExistsValue('locSendToOtherAddress') = 0
            p_web.SetValue('locSendToOtherAddress',0)
            locSendToOtherAddress = 0
          End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('WaybillCreateSundry_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('WaybillCreateSundry_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
        If locDestination = ''
          loc:Invalid = 'locDestination'
          loc:alert = p_web.translate('Destination') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locDestination = Upper(locDestination)
          p_web.SetSessionValue('locDestination',locDestination)
        If loc:Invalid <> '' then exit.
        If locTransport = ''
          loc:Invalid = 'locTransport'
          loc:alert = p_web.translate('Mode Of Transport') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locTransport = Upper(locTransport)
          p_web.SetSessionValue('locTransport',locTransport)
        If loc:Invalid <> '' then exit.
        If locSecurityPackNumber = ''
          loc:Invalid = 'locSecurityPackNumber'
          loc:alert = p_web.translate('Security Pack No') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locSecurityPackNumber = Upper(locSecurityPackNumber)
          p_web.SetSessionValue('locSecurityPackNumber',locSecurityPackNumber)
        If loc:Invalid <> '' then exit.
  ! tab = 2
    loc:InvalidTab += 1
          locAccountNumber = Upper(locAccountNumber)
          p_web.SetSessionValue('locAccountNumber',locAccountNumber)
        If loc:Invalid <> '' then exit.
        If locCompanyName = ''
          loc:Invalid = 'locCompanyName'
          loc:alert = p_web.translate('Company Name') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locCompanyName = Upper(locCompanyName)
          p_web.SetSessionValue('locCompanyName',locCompanyName)
        If loc:Invalid <> '' then exit.
        If locAddress1 = '' and p_web.GSV('locSendToOtherAddress') = 1
          loc:Invalid = 'locAddress1'
          loc:alert = p_web.translate('Address') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locAddress1 = Upper(locAddress1)
          p_web.SetSessionValue('locAddress1',locAddress1)
        If loc:Invalid <> '' then exit.
          locAddress2 = Upper(locAddress2)
          p_web.SetSessionValue('locAddress2',locAddress2)
        If loc:Invalid <> '' then exit.
          locAddress3 = Upper(locAddress3)
          p_web.SetSessionValue('locAddress3',locAddress3)
        If loc:Invalid <> '' then exit.
          locPostcode = Upper(locPostcode)
          p_web.SetSessionValue('locPostcode',locPostcode)
        If loc:Invalid <> '' then exit.
        If locTelephoneNumber = ''
          loc:Invalid = 'locTelephoneNumber'
          loc:alert = p_web.translate('Telephone Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
        If locContactName = ''
          loc:Invalid = 'locContactName'
          loc:alert = p_web.translate('Contact Name') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
  ! tab = 3
    loc:InvalidTab += 1
          locUserNotes = Upper(locUserNotes)
          p_web.SetSessionValue('locUserNotes',locUserNotes)
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('WaybillCreateSundry:Primed',0)
  p_web.StoreValue('locSendToOtherAddress')
  p_web.StoreValue('locDestination')
  p_web.StoreValue('')
  p_web.StoreValue('locTransport')
  p_web.StoreValue('locSecurityPackNumber')
  p_web.StoreValue('locAccountNumber')
  p_web.StoreValue('')
  p_web.StoreValue('locCompanyName')
  p_web.StoreValue('locAddress1')
  p_web.StoreValue('locAddress2')
  p_web.StoreValue('locAddress3')
  p_web.StoreValue('locPostcode')
  p_web.StoreValue('locHub')
  p_web.StoreValue('locTelephoneNumber')
  p_web.StoreValue('locContactName')
  p_web.StoreValue('locEmailAddress')
  p_web.StoreValue('')
  p_web.StoreValue('locUserNotes')
  p_web.StoreValue('')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locDestination',locDestination) ! STRING(30)
     p_web.SSV('locTransport',locTransport) ! STRING(30)
     p_web.SSV('locSecurityPackNumber',locSecurityPackNumber) ! STRING(30)
     p_web.SSV('locAccountNumber',locAccountNumber) ! STRING(30)
     p_web.SSV('locCompanyName',locCompanyName) ! STRING(30)
     p_web.SSV('locAddress1',locAddress1) ! STRING(30)
     p_web.SSV('locAddress2',locAddress2) ! STRING(30)
     p_web.SSV('locAddress3',locAddress3) ! STRING(30)
     p_web.SSV('locPostcode',locPostcode) ! STRING(30)
     p_web.SSV('locHub',locHub) ! STRING(30)
     p_web.SSV('locTelephoneNumber',locTelephoneNumber) ! STRING(30)
     p_web.SSV('locContactName',locContactName) ! STRING(30)
     p_web.SSV('locEmailAddress',locEmailAddress) ! STRING(255)
     p_web.SSV('locUserNotes',locUserNotes) ! STRING(255)
     p_web.SSV('locSendToOtherAddress',locSendToOtherAddress) ! LONG
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locDestination = p_web.GSV('locDestination') ! STRING(30)
     locTransport = p_web.GSV('locTransport') ! STRING(30)
     locSecurityPackNumber = p_web.GSV('locSecurityPackNumber') ! STRING(30)
     locAccountNumber = p_web.GSV('locAccountNumber') ! STRING(30)
     locCompanyName = p_web.GSV('locCompanyName') ! STRING(30)
     locAddress1 = p_web.GSV('locAddress1') ! STRING(30)
     locAddress2 = p_web.GSV('locAddress2') ! STRING(30)
     locAddress3 = p_web.GSV('locAddress3') ! STRING(30)
     locPostcode = p_web.GSV('locPostcode') ! STRING(30)
     locHub = p_web.GSV('locHub') ! STRING(30)
     locTelephoneNumber = p_web.GSV('locTelephoneNumber') ! STRING(30)
     locContactName = p_web.GSV('locContactName') ! STRING(30)
     locEmailAddress = p_web.GSV('locEmailAddress') ! STRING(255)
     locUserNotes = p_web.GSV('locUserNotes') ! STRING(255)
     locSendToOtherAddress = p_web.GSV('locSendToOtherAddress') ! LONG
CountItems          PROCEDURE()!,LONG
retValue                LONG(0)
    CODE
        Access:SBO_GenericFile.ClearKey(sbogen:String1Key)
        sbogen:SessionID = p_web.SessionID
        SET(sbogen:String1Key,sbogen:String1Key)
        LOOP UNTIL Access:SBO_GenericFile.Next() <> Level:Benign
            IF (sbogen:SessionID <> p_web.SessionID)
                BREAK
            END ! IF
            retValue = 1
            BREAK
        END ! LOOP
        
        RETURN retValue
