

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER097.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ForceFaultCodes      PROCEDURE  (func:ChargeableJob,func:WarrantyJob,func:CChargeType,func:WChargeType,func:CRepairType,func:WRepairType,func:Type,fManufacturer) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !

  CODE
    !(func:ChargeableJob,func:WarrantyJob,func:CChargeType,func:WChargeType,func:CRepairType,func:WRepairType,fManufacturer)

    If func:Type = 'C' or func:type = 'X'
        If func:ChargeableJob = 'YES'
            Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
            cha:Charge_Type = func:CChargeType
            If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Found
                If cha:Force_Warranty = 'YES'
                    If func:CRepairType <> ''
                        Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
                        rtd:Manufacturer = fManufacturer
                        rtd:Chargeable   = 'YES'
                        rtd:Repair_Type  = func:CRepairType
                        If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                            !Found
                            If rtd:CompFaultCoding
! Deleted (DBH 20/05/2006) #6733 - Do not check the manufacturer for char fault codes
!                                 Access:MANUFACT.Clearkey(man:Manufacturer_Key)
!                                 man:Manufacturer    = fManufacturer
!                                 If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
!                                     !Found
!                                     If man:ForceCharFaultCodes
!                                         Return Level:Fatal
!                                     End !If man:ForceCharFaultCodes
!                                 Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
!                                     !Error
!                                 End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
! End (DBH 20/05/2006) #6733
                                Return Level:Fatal
                            End !If rtd:CompFaultCoding
                        Else ! If Access:REPTYDEF.Tryfetch(rtd:Chargeable_Key) = Level:Benign
                            !Error
                        End !If Access:REPTYDEF.Tryfetch(rtd:Chargeable_Key) = Level:Benign
                    Else !If func:CRepairType <> ''
                        Return Level:Fatal
                    End !If func:CRepairType <> ''
                End !If cha:Force_Warranty = 'YES'
            Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Error
            End !If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        End !If func:Chargeable_Job = 'YES'
    End !If func:Type = 'C' or func:type = 'X'

    If func:Type = 'W' or func:Type = 'X'
        If func:WarrantyJob = 'YES'
            Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
            cha:Charge_Type = func:WChargeType
            If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Found
                If cha:Force_Warranty = 'YES'
                    If func:WRepairType <> ''
                        Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
                        rtd:Manufacturer = fManufacturer
                        rtd:Warranty     = 'YES'
                        rtd:Repair_Type  = func:WRepairType
                        If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                            !Found
                            If rtd:CompFaultCoding
                                Return Level:Fatal
                            End !If rtd:CompFaultCoding
                        Else ! If Access:REPTYDEF.Tryfetch(rtd:Warrranty_Key) = Level:Benign
                            !Error
                        End !If Access:REPTYDEF.Tryfetch(rtd:Warrranty_Key) = Level:Benign
                    Else !If WRetairType <> ''
                        Return Level:Fatal
                    End !If WRetairType <> ''
                End !If cha:Force_Warranty = 'YES'

            Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                !Error
            End !If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        End !If func:Warranty_Job = 'YES'
    End !If func:Type = 'W' or func:Type = 'X'
    Return Level:Benign
