

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER042.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
AddToStockAllocation PROCEDURE  (LONG func:PartRecordNumber,STRING func:PartType,LONG func:Quantity,STRING func:Status,STRING func:Engineer,<NetWebServerWorker p_web>) ! Declare Procedure

  CODE
    VodacomClass.AddPartToStockAllocation(func:PartRecordNumber,func:PartType,func:Quantity,func:Status,func:Engineer,,p_web)
!    !p_web.SSV('AddToStockAllocation:Type','WAR')
!    !p_web.SSV('AddToStockAllocation:Status','STatus?')
!    !p_web.SSV('AddToSTockAllocation:Qty',?)
!    
!    do openFiles
!    do saveFiles
!    Access:STOCKALL.Clearkey(stl:PartRecordNumberKey)
!    stl:Location = p_web.GSV('BookingSiteLocation')
!    stl:PartType = p_web.GSV('AddToStockAllocation:Type')
!    Case stl:PartType
!    of 'CHA'
!        stl:PartRecordNumber = p_web.GSV('par:Record_Number')
!                
!        IF (p_web.GSV('par:Correction') = 1)
!            DO ExitProc
!                
!        END
!        
!    of 'WAR'
!        stl:PartRecordNumber = p_web.GSV('wpr:Record_Number') 
!                    
!        IF (p_web.GSV('wpr:Correction') = 1)
!            DO ExitProc
!        END 
!                
!    of 'EST'
!        stl:PartRecordNumber = p_web.GSV('epr:Record_Number')
!        IF (p_web.GSV('epr:Correction') = 1)
!            DO ExitProc
!        END 
!                
!    end
!    if (Access:STOCKALL.TryFetch(stl:PartRecordNumberKey) = Level:Benign)
!        Do WriteRecord
!    else
!        If (Access:STOCKALL.PrimeRecord() = Level:Benign)
!            Do WriteRecord
!            if (Access:STOCKALL.TryInsert())
!            End
!        End
!    end
!    
!    DO ExitProc
!              
