

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER045.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER024.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
GetStatus            PROCEDURE  (LONG f_number,BYTE f_audit,STRING f_type,<NetWebServerWorker p_web>) ! Declare Procedure

  CODE
        !TB12477
        !Change needed for Webmaster so that despatch of a job to VCP does not change the status to "811 READY TO DESPATCH (LOAN)"
        !but rather "463 SEND TO PUP".
        IF (f_number = 811)
            IF NOT p_web &= NULL
                IF (p_web.GSV('job:Who_Booked') = 'WEB')
                    f_number = 463
                END
            ELSE
                IF (job:who_booked = 'WEB')
                    f_number = 463
                END ! IF
            END ! IF
        END ! IF

        ! #13123 Ok, we've already assumed that jobs is open. So I will continue with that to make life easier.
        ! Don't change job to 810 READY TO DESPATCH, or 916 PAID AWAITNG DESPATCH
        ! if at 453 SEND TO PUP (DBH: 29/07/2013)
        IF (f_Number = 810 OR f_Number = 916)
            IF NOT p_web &= NULL
                IF (p_web.GSV('job:Who_Booked') = 'WEB' AND SUB(p_web.GSV('job:Current_Status'),1,3) = '463')
                    ! Do Nothing
                    RETURN
                END ! IF
            ELSE
                IF (job:Who_Booked = 'WEB' AND SUB(job:Current_Status,1,3) = '463')
                ! Do nothing
                    RETURN
                END ! IF (job:Who_Booked = 'WEB' AND SUB(job:Current_Status,1,3) = '463')
            END !IF
        END ! IF (f_Number = 810 OR f_Number = 916)
        
        
        VodacomClass.SetStatus(f_number,f_audit,f_type,p_web)
        
        ! #13203 Hideous bit of hardcoding to stop SMS being sent 
        ! when "DESPATCH EXCH UNIT' set on PUP jobs. (DBH: 11/11/2013)
        IF (f_Number = 110 AND f_type = 'EXC')
            IF NOT p_web &= NULL
                IF (p_web.GSV('job:Who_Booked') = 'WEB')
                    RETURN
                END ! IF
            ELSE
                IF (job:Who_Booked = 'WEB')
                    RETURN
                END ! IF
            END ! IF
        END ! IF

        SendSMSText(f_type[1],'Y','N',p_web)
