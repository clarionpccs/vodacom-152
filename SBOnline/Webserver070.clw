

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER070.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! Check for saved entry. pAdd to add if missing. pDelete to delete any matching.
!!! </summary>
DuplicateTabCheckDelete PROCEDURE  (NetwebServerWorker p_web,STRING pType,STRING pValue) ! Declare Procedure
RetValue                    LONG(Level:Benign)
FilesOpened     BYTE(0)

  CODE
        !region ProcessedCode
    DO OpenFiles
        
    Access:SBO_DupCheck.ClearKey(sbodup:ValueKey)
    sbodup:SessionID = p_web.SessionID
    sbodup:DupType = pType
    sbodup:ValueField = pValue
    SET(sbodup:ValueKey,sbodup:ValueKey)
    LOOP UNTIL Access:SBO_DupCheck.Next() <> Level:Benign
        IF (sbodup:SessionID <> p_web.SessionID)
            BREAK
        END ! IF
        IF (pType <> '')
            IF (sbodup:DupType <> pType)
                BREAK
            END ! IF
        END ! IF        
        IF (pValue <> '')
            IF (sbodup:ValueField <> pValue)
                BREAK
            END ! IF
        END !I F
        Access:SBO_DupCheck.DeleteRecord(0)    
    END ! LOOP
        
    DO CloseFiles        
    !endregion
!--------------------------------------
OpenFiles  ROUTINE
  Access:SBO_DupCheck.Open                                 ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SBO_DupCheck.UseFile                              ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:SBO_DupCheck.Close
     FilesOpened = False
  END
