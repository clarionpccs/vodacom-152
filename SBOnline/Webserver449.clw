

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER449.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER448.INC'),ONCE        !Req'd for module callout resolution
                     END


FormRetailSales      PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locRaisedBy          STRING(60)                            !
locPaymentStatus     STRING(30)                            !
locAddressLine1      STRING(30)                            !
locRetailStockType   STRING(1)                             !
FilesOpened     Long
USERS::State  USHORT
RETSALES::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormRetailSales')
  loc:formname = 'FormRetailSales_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormRetailSales',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormRetailSales','')
    p_web._DivHeader('FormRetailSales',clip('fdiv') & ' ' & clip('FormContent'))
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormRetailSales',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormRetailSales',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormRetailSales',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormRetailSales',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If loc:act = InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormRetailSales',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormRetailSales',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormRetailSales',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  p_web._OpenFile(RETSALES)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(RETSALES)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormRetailSales_form:inited_',1)
  p_web.SetValue('UpdateFile','RETSALES')
  p_web.SetValue('UpdateKey','ret:Ref_Number_Key')
  p_web.SetValue('IDField','ret:Ref_Number')
  do RestoreMem

CancelForm  Routine
  IF p_web.GetSessionValue('FormRetailSales:Primed') = 1
    p_web._deleteFile(RETSALES)
    p_web.SetSessionValue('FormRetailSales:Primed',0)
  End

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','RETSALES')
  p_web.SetValue('UpdateKey','ret:Ref_Number_Key')
  If p_web.IfExistsValue('ret:Account_Number')
    p_web.SetPicture('ret:Account_Number','@s15')
  End
  p_web.SetSessionPicture('ret:Account_Number','@s15')
  If p_web.IfExistsValue('ret:Contact_Name')
    p_web.SetPicture('ret:Contact_Name','@s30')
  End
  p_web.SetSessionPicture('ret:Contact_Name','@s30')
  If p_web.IfExistsValue('ret:Company_Name_Delivery')
    p_web.SetPicture('ret:Company_Name_Delivery','@s30')
  End
  p_web.SetSessionPicture('ret:Company_Name_Delivery','@s30')
  If p_web.IfExistsValue('ret:Despatch_Number')
    p_web.SetPicture('ret:Despatch_Number','@s8')
  End
  p_web.SetSessionPicture('ret:Despatch_Number','@s8')
  If p_web.IfExistsValue('ret:Address_Line2_Delivery')
    p_web.SetPicture('ret:Address_Line2_Delivery','@s30')
  End
  p_web.SetSessionPicture('ret:Address_Line2_Delivery','@s30')
  If p_web.IfExistsValue('ret:Delivery_Text')
    p_web.SetPicture('ret:Delivery_Text','@s255')
  End
  p_web.SetSessionPicture('ret:Delivery_Text','@s255')
  If p_web.IfExistsValue('ret:Address_Line3_Delivery')
    p_web.SetPicture('ret:Address_Line3_Delivery','@s30')
  End
  p_web.SetSessionPicture('ret:Address_Line3_Delivery','@s30')
  If p_web.IfExistsValue('ret:Date_Despatched')
    p_web.SetPicture('ret:Date_Despatched','@d6b')
  End
  p_web.SetSessionPicture('ret:Date_Despatched','@d6b')
  If p_web.IfExistsValue('ret:Postcode_Delivery')
    p_web.SetPicture('ret:Postcode_Delivery','@s10')
  End
  p_web.SetSessionPicture('ret:Postcode_Delivery','@s10')
  If p_web.IfExistsValue('ret:Consignment_Number')
    p_web.SetPicture('ret:Consignment_Number','@s30')
  End
  p_web.SetSessionPicture('ret:Consignment_Number','@s30')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locRaisedBy',locRaisedBy)
  p_web.SetSessionValue('locPaymentStatus',locPaymentStatus)
  p_web.SetSessionValue('locAddressLine1',locAddressLine1)
  p_web.SetSessionValue('locRetailStockType',locRetailStockType)

RestoreMem       Routine
  !FormSource=File
  if p_web.IfExistsValue('locRaisedBy')
    locRaisedBy = p_web.GetValue('locRaisedBy')
    p_web.SetSessionValue('locRaisedBy',locRaisedBy)
  End
  if p_web.IfExistsValue('locPaymentStatus')
    locPaymentStatus = p_web.GetValue('locPaymentStatus')
    p_web.SetSessionValue('locPaymentStatus',locPaymentStatus)
  End
  if p_web.IfExistsValue('locAddressLine1')
    locAddressLine1 = p_web.GetValue('locAddressLine1')
    p_web.SetSessionValue('locAddressLine1',locAddressLine1)
  End
  if p_web.IfExistsValue('locRetailStockType')
    locRetailStockType = p_web.GetValue('locRetailStockType')
    p_web.SetSessionValue('locRetailStockType',locRetailStockType)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormRetailSales_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  IF (p_web.GSV('ret:Building_Name_Delivery') = '')
      p_web.SSV('locAddressLine1',p_web.GSV('ret:Address_Line1_Delivery'))
  ELSE
      p_web.SSV('locAddressLine1',p_web.GSV('ret:Building_Name_Delivery') & ' ' & p_web.GSV('ret:Address_Line1_Delivery'))
  END
  
  Access:USERS.ClearKey(use:User_Code_Key)
  use:User_Code = p_web.GSV('ret:Who_Booked')
  IF (Access:USERS.TryFetch(use:User_Code_Key))
      p_web.SSV('locRaisedBy','Unknown User')
  ELSE
      p_web.SSV('locRaisedBy',CLIP(use:Forename) & ' ' & CLIP(use:Surname))
  END
  
  Case p_web.GSV('ret:payment_method')
  OF 'TRA'
      p_web.SSV('locPaymentStatus','Trade')
  Of 'CAS'
      p_web.SSV('locPaymentStatus','Cash')
  Of 'EXC'
      p_web.SSV('locPaymentStatus','Exchange Accessory')
  End!Case ret:payment_method
  
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locRaisedBy = p_web.RestoreValue('locRaisedBy')
 locPaymentStatus = p_web.RestoreValue('locPaymentStatus')
 locAddressLine1 = p_web.RestoreValue('locAddressLine1')
 locRetailStockType = p_web.RestoreValue('locRetailStockType')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormRetailSales')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormRetailSales_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormRetailSales_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormRetailSales_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  packet = clip(packet) & '<input type="hidden" name="RETSALES__FileAction" value="'&p_web.getSessionValue('RETSALES:FileAction')&'" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="file" value="RETSALES" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateFile" value="RETSALES" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateKey" value="ret:Ref_Number_Key" ></input><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormRetailSales" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormRetailSales" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormRetailSales" ></input><13,10>'
  end

  do SendPacket
  packet = clip(packet) & p_web.CreateInput('hidden','ret:Ref_Number',p_web._jsok(p_web.getSessionValue('ret:Ref_Number'))) & '<13,10>'
  If p_web.Translate('View Retail Sale') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('View Retail Sale',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormRetailSales">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormRetailSales" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormRetailSales')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Sale Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormRetailSales')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormRetailSales'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormRetailSales_BrowseRetailSalesItems_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormRetailSales_BrowseRetailSalesItems_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormRetailSales_BrowseRetailSalesItems_embedded_div')&''');'
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormRetailSales')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Sale Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormRetailSales_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Sale Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Sale Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Sale Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Sale Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::ret:Account_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ret:Account_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::txtDateRaised
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtDateRaised
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::ret:Contact_Name
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ret:Contact_Name
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locRaisedBy
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locRaisedBy
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::txtDeliveryAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtDeliveryAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPaymentStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::ret:Company_Name_Delivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ret:Company_Name_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::ret:Despatch_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ret:Despatch_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAddressLine1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAddressLine1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::txtInvoiceNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtInvoiceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::ret:Address_Line2_Delivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ret:Address_Line2_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::ret:Delivery_Text
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ret:Delivery_Text
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::ret:Address_Line3_Delivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ret:Address_Line3_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::ret:Date_Despatched
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ret:Date_Despatched
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::ret:Postcode_Delivery
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ret:Postcode_Delivery
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::ret:Consignment_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ret:Consignment_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormRetailSales_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locRetailStockType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locRetailStockType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseRetailSalesItems
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::ret:Account_Number  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('ret:Account_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::ret:Account_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ret:Account_Number',p_web.GetValue('NewValue'))
    ret:Account_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = ret:Account_Number
    do Value::ret:Account_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('ret:Account_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    ret:Account_Number = p_web.GetValue('Value')
  End

Value::ret:Account_Number  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('ret:Account_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- ret:Account_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('ret:Account_Number'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::txtDateRaised  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('txtDateRaised') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Date Raised')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::txtDateRaised  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtDateRaised',p_web.GetValue('NewValue'))
    do Value::txtDateRaised
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtDateRaised  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('txtDateRaised') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(Format(p_web.GSV('ret:date_booked'),@d06b) & ' ' & Format(p_web.GSV('ret:Time_Booked'),@t01b),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::ret:Contact_Name  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('ret:Contact_Name') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Contact Name')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::ret:Contact_Name  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ret:Contact_Name',p_web.GetValue('NewValue'))
    ret:Contact_Name = p_web.GetValue('NewValue') !FieldType= STRING Field = ret:Contact_Name
    do Value::ret:Contact_Name
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('ret:Contact_Name',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    ret:Contact_Name = p_web.GetValue('Value')
  End

Value::ret:Contact_Name  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('ret:Contact_Name') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- ret:Contact_Name
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('ret:Contact_Name'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locRaisedBy  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('locRaisedBy') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Raised By')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locRaisedBy  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locRaisedBy',p_web.GetValue('NewValue'))
    locRaisedBy = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locRaisedBy
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locRaisedBy',p_web.GetValue('Value'))
    locRaisedBy = p_web.GetValue('Value')
  End

Value::locRaisedBy  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('locRaisedBy') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locRaisedBy
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locRaisedBy'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::txtDeliveryAddress  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('txtDeliveryAddress') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Delivery Address:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::txtDeliveryAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtDeliveryAddress',p_web.GetValue('NewValue'))
    do Value::txtDeliveryAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtDeliveryAddress  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('txtDeliveryAddress') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::locPaymentStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPaymentStatus',p_web.GetValue('NewValue'))
    locPaymentStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPaymentStatus
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPaymentStatus',p_web.GetValue('Value'))
    locPaymentStatus = p_web.GetValue('Value')
  End

Value::locPaymentStatus  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('locPaymentStatus') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locPaymentStatus
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locPaymentStatus'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::ret:Company_Name_Delivery  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('ret:Company_Name_Delivery') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Company Name')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::ret:Company_Name_Delivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ret:Company_Name_Delivery',p_web.GetValue('NewValue'))
    ret:Company_Name_Delivery = p_web.GetValue('NewValue') !FieldType= STRING Field = ret:Company_Name_Delivery
    do Value::ret:Company_Name_Delivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('ret:Company_Name_Delivery',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    ret:Company_Name_Delivery = p_web.GetValue('Value')
  End

Value::ret:Company_Name_Delivery  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('ret:Company_Name_Delivery') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- ret:Company_Name_Delivery
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('ret:Company_Name_Delivery'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::ret:Despatch_Number  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('ret:Despatch_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Despatch_Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::ret:Despatch_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ret:Despatch_Number',p_web.GetValue('NewValue'))
    ret:Despatch_Number = p_web.GetValue('NewValue') !FieldType= LONG Field = ret:Despatch_Number
    do Value::ret:Despatch_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('ret:Despatch_Number',p_web.dFormat(p_web.GetValue('Value'),'@s8'))
    ret:Despatch_Number = p_web.GetValue('Value')
  End

Value::ret:Despatch_Number  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('ret:Despatch_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- ret:Despatch_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('ret:Despatch_Number'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locAddressLine1  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('locAddressLine1') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Address')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAddressLine1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAddressLine1',p_web.GetValue('NewValue'))
    locAddressLine1 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAddressLine1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAddressLine1',p_web.GetValue('Value'))
    locAddressLine1 = p_web.GetValue('Value')
  End

Value::locAddressLine1  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('locAddressLine1') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locAddressLine1
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAddressLine1'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::txtInvoiceNumber  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('txtInvoiceNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Invoice Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::txtInvoiceNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtInvoiceNumber',p_web.GetValue('NewValue'))
    do Value::txtInvoiceNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtInvoiceNumber  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('txtInvoiceNumber') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate(p_web.GSV('ret:Invoice_Number') & ' ' & Format(p_web.GSV('ret:Invoice_Date'),@d06b),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::ret:Address_Line2_Delivery  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('ret:Address_Line2_Delivery') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::ret:Address_Line2_Delivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ret:Address_Line2_Delivery',p_web.GetValue('NewValue'))
    ret:Address_Line2_Delivery = p_web.GetValue('NewValue') !FieldType= STRING Field = ret:Address_Line2_Delivery
    do Value::ret:Address_Line2_Delivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('ret:Address_Line2_Delivery',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    ret:Address_Line2_Delivery = p_web.GetValue('Value')
  End

Value::ret:Address_Line2_Delivery  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('ret:Address_Line2_Delivery') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- ret:Address_Line2_Delivery
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('ret:Address_Line2_Delivery'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::ret:Delivery_Text  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('ret:Delivery_Text') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Delivery Text')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::ret:Delivery_Text  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ret:Delivery_Text',p_web.GetValue('NewValue'))
    ret:Delivery_Text = p_web.GetValue('NewValue') !FieldType= STRING Field = ret:Delivery_Text
    do Value::ret:Delivery_Text
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('ret:Delivery_Text',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    ret:Delivery_Text = p_web.GetValue('Value')
  End

Value::ret:Delivery_Text  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('ret:Delivery_Text') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- ret:Delivery_Text
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('ret:Delivery_Text'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::ret:Address_Line3_Delivery  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('ret:Address_Line3_Delivery') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::ret:Address_Line3_Delivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ret:Address_Line3_Delivery',p_web.GetValue('NewValue'))
    ret:Address_Line3_Delivery = p_web.GetValue('NewValue') !FieldType= STRING Field = ret:Address_Line3_Delivery
    do Value::ret:Address_Line3_Delivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('ret:Address_Line3_Delivery',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    ret:Address_Line3_Delivery = p_web.GetValue('Value')
  End

Value::ret:Address_Line3_Delivery  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('ret:Address_Line3_Delivery') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- ret:Address_Line3_Delivery
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('ret:Address_Line3_Delivery'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::ret:Date_Despatched  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('ret:Date_Despatched') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Date Despatched')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::ret:Date_Despatched  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ret:Date_Despatched',p_web.GetValue('NewValue'))
    ret:Date_Despatched = p_web.GetValue('NewValue') !FieldType= DATE Field = ret:Date_Despatched
    do Value::ret:Date_Despatched
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('ret:Date_Despatched',p_web.dFormat(p_web.GetValue('Value'),'@d6b'))
    ret:Date_Despatched = p_web.GetValue('Value')
  End

Value::ret:Date_Despatched  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('ret:Date_Despatched') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- ret:Date_Despatched
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('ret:Date_Despatched'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::ret:Postcode_Delivery  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('ret:Postcode_Delivery') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Postcode')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::ret:Postcode_Delivery  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ret:Postcode_Delivery',p_web.GetValue('NewValue'))
    ret:Postcode_Delivery = p_web.GetValue('NewValue') !FieldType= STRING Field = ret:Postcode_Delivery
    do Value::ret:Postcode_Delivery
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('ret:Postcode_Delivery',p_web.dFormat(p_web.GetValue('Value'),'@s10'))
    ret:Postcode_Delivery = p_web.GetValue('Value')
  End

Value::ret:Postcode_Delivery  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('ret:Postcode_Delivery') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- ret:Postcode_Delivery
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('ret:Postcode_Delivery'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::ret:Consignment_Number  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('ret:Consignment_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Consignment Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::ret:Consignment_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ret:Consignment_Number',p_web.GetValue('NewValue'))
    ret:Consignment_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = ret:Consignment_Number
    do Value::ret:Consignment_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('ret:Consignment_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    ret:Consignment_Number = p_web.GetValue('Value')
  End

Value::ret:Consignment_Number  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('ret:Consignment_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- ret:Consignment_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('ret:Consignment_Number'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locRetailStockType  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('locRetailStockType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Stock Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locRetailStockType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locRetailStockType',p_web.GetValue('NewValue'))
    locRetailStockType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locRetailStockType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locRetailStockType',p_web.GetValue('Value'))
    locRetailStockType = p_web.GetValue('Value')
  End
  do Value::locRetailStockType
  do SendAlert
  do Value::BrowseRetailSalesItems  !1

Value::locRetailStockType  Routine
  p_web._DivHeader('FormRetailSales_' & p_web._nocolon('locRetailStockType') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locRetailStockType
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locRetailStockType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locRetailStockType') = ''
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locRetailStockType'',''formretailsales_locretailstocktype_value'',1,'''&clip('')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locRetailStockType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locRetailStockType',clip(''),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locRetailStockType_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Picked') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locRetailStockType') = 'B'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locRetailStockType'',''formretailsales_locretailstocktype_value'',1,'''&clip('B')&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locRetailStockType')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locRetailStockType',clip('B'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locRetailStockType_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Back Orders') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRetailSales_' & p_web._nocolon('locRetailStockType') & '_value')


Validate::BrowseRetailSalesItems  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseRetailSalesItems',p_web.GetValue('NewValue'))
    do Value::BrowseRetailSalesItems
  Else
    p_web.StoreValue('res:Record_Number')
  End

Value::BrowseRetailSalesItems  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseRetailSalesItems --
  p_web.SetValue('BrowseRetailSalesItems:NoForm',1)
  p_web.SetValue('BrowseRetailSalesItems:FormName',loc:formname)
  p_web.SetValue('BrowseRetailSalesItems:parentIs','Form')
  p_web.SetValue('_parentProc','FormRetailSales')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormRetailSales_BrowseRetailSalesItems_embedded_div')&'"><!-- Net:BrowseRetailSalesItems --></div><13,10>'
    p_web._DivHeader('FormRetailSales_' & lower('BrowseRetailSalesItems') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormRetailSales_' & lower('BrowseRetailSalesItems') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseRetailSalesItems --><13,10>'
  end
  do SendPacket


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormRetailSales_locRetailStockType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locRetailStockType
      else
        do Value::locRetailStockType
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormRetailSales_form:ready_',1)
  p_web.SetSessionValue('FormRetailSales_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormRetailSales',0)
  Access:RETSALES.PrimeRecord()
  Ans = ChangeRecord
  p_web.SetSessionValue('FormRetailSales:Primed',1)

PreCopy  Routine
  p_web.SetValue('FormRetailSales_form:ready_',1)
  p_web.SetSessionValue('FormRetailSales_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormRetailSales',0)
  Access:RETSALES.PrimeRecord()
  Ans = ChangeRecord
  p_web.SetSessionValue('FormRetailSales:Primed',1)
  p_web._PreCopyRecord(RETSALES,ret:Ref_Number_Key,Net:Web:Autonumbered)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormRetailSales_form:ready_',1)
  p_web.SetSessionValue('FormRetailSales_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormRetailSales:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormRetailSales_form:ready_',1)
  p_web.SetSessionValue('FormRetailSales_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormRetailSales:Primed',0)
  p_web.setsessionvalue('showtab_FormRetailSales',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormRetailSales_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormRetailSales_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST
PostInsert      Routine

PostCopy        Routine
  p_web.SetSessionValue('FormRetailSales:Primed',0)

PostUpdate      Routine
  p_web.SetSessionValue('FormRetailSales:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('locRaisedBy')
  p_web.StoreValue('')
  p_web.StoreValue('locPaymentStatus')
  p_web.StoreValue('locAddressLine1')
  p_web.StoreValue('')
  p_web.StoreValue('locRetailStockType')
  p_web.StoreValue('')

PostDelete      Routine
