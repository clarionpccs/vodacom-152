

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER113.INC'),ONCE        !Local module procedure declarations
                     END


BrowseStatusChanges  PROCEDURE  (NetWebServerWorker p_web)
tmp:StatusTypeFilter STRING(3)                             !Status Type Filer
tmp:UserName         STRING(60)                            !User Name
CRLF                string('<13,10>')
NBSP                string('&#160;')
packet              string(NET:MaxBinData)
packetlen           long
TableQueue  Queue,pre(TableQueue)
kind          Long
row           String(size(packet))
id            String(256)
sub           Long
            End
loc:total               decimal(31,15),dim(200)
loc:RowNumber           Long
loc:section             Long
loc:found               Long
loc:FirstRow            String(256)
loc:FirstRowID          String(256)
loc:RowsHigh            Long(1)
loc:RowsIn              Long
loc:vordernumber        Long
loc:vorder              String(256)
loc:pagename            String(256)
loc:ButtonPosition      Long
loc:SelectionMethod     Long
loc:FileLoading         Long
loc:Sorting             Long
loc:LocatorPosition     Long
loc:LocatorBlank        Long
loc:LocatorType         Long
loc:LocatorSearchButton Long
loc:LocatorClearButton  Long
loc:LocatorValue        String(256)
Loc:NoForm              Long
Loc:FormName            String(256)
Loc:Class               String(256)
Loc:Skip                Long
loc:ViewOnly            Long
loc:invalid             String(100)
loc:ViewPosWorkaround   String(255)
ThisView            View(AUDSTATS)
                      Project(aus:RecordNumber)
                      Project(aus:DateChanged)
                      Project(aus:TimeChanged)
                      Project(aus:OldStatus)
                      Project(aus:NewStatus)
                    END ! of ThisView
!-- drop

loc:formaction        String(256)
loc:formactiontarget  String(256)
loc:SelectAction      String(256)
loc:CloseAction       String(256)
loc:extra             String(256)
loc:LiveClick         String(256)
loc:Selecting         Long
loc:rowcount          Long ! counts the total rows printed to screen
loc:pagerows          Long ! the number of rows per page
loc:columns           Long
loc:selectionexists   Long
loc:checked           String(10)
loc:direction         Long(2) ! + = forwards, - = backwards
loc:FillBack          Long(1)
loc:field             string(1024)
loc:first             Long
loc:FirstValue        String(1024)
loc:LastValue         String(1024)
Loc:LocateField       String(256)
Loc:SortHeader        String(256)
Loc:SortDirection     Long(1)
loc:disabled          Long
loc:RowStyle          String(512)
loc:MultiRowStyle     String(256)
loc:SelectColumnClass String(256)
loc:x                 Long
loc:y                 Long
loc:options           Long
loc:RowStarted        Long
loc:nextdisabled      Long
loc:previousdisabled  Long
loc:divname           String(256)
loc:FilterWas         String(1024)
loc:selected          String(256)
loc:default           String(256)
loc:parent            String(64)
loc:IsChange          Long
loc:Silent            Long ! children of this browse should go into Silent mode
loc:ParentSilent      Long ! this browse should go into silent mode (reads value of _Silent on start).
loc:eip               Long
loc:eipRest           like(packet)
loc:alert             String(256)
loc:tabledata         String(50)
loc:SkipHeader        Long
loc:ViewState         String(1024)
Loc:NoBuffer          Long(1)         ! buffer is causing a problem with sql engines, and resetting the position in the view, so default to off.
FilesOpened     Long
USERS::State  USHORT
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return
  End
  GlobalErrors.SetProcedureName('BrowseStatusChanges')


  If p_web.RequestAjax = 0
    if p_web.IfExistsValue('BrowseStatusChanges:NoForm')
      loc:NoForm = p_web.GetValue('BrowseStatusChanges:NoForm')
      loc:FormName = p_web.GetValue('BrowseStatusChanges:FormName')
    else
      loc:FormName = 'BrowseStatusChanges_frm'
    End
    p_web.SSV('BrowseStatusChanges:NoForm',loc:NoForm)
    p_web.SSV('BrowseStatusChanges:FormName',loc:FormName)
  else
    loc:NoForm = p_web.GSV('BrowseStatusChanges:NoForm')
    loc:FormName = p_web.GSV('BrowseStatusChanges:FormName')
  end
  loc:parent = p_web.GetValue('_ParentProc')
  if loc:parent <> ''
    loc:divname = lower('BrowseStatusChanges') & '_' & lower(loc:parent)
  else
    loc:divname = lower('BrowseStatusChanges')
  end
  loc:ParentSilent = p_web.GetValue('_Silent')

  if p_web.IfExistsValue('_EIPClm')
    do CallEIP
  elsif p_web.IfExistsValue('_Clicked')
    do CallClicked
  else
    do CallBrowse
  End
  GlobalErrors.SetProcedureName()
  Return

CallBrowse  Routine
  If p_web.RequestAjax = 0
    p_web.Message('alert',,,Net:Send)   ! these 2 should have been done by here, but this handles cases
    p_web.Busy(Net:Send)                ! where they are not done.
  End
  p_web._DivHeader(loc:divname,clip('fdiv') & ' ' & clip('BrowseContent'))
  if loc:ParentSilent = 0
    do GenerateBrowse
  elsIf p_web.RequestAjax = 0
    do OpenFilesB
    do LookupAbility
    do CloseFilesB
  End
  p_web._DivFooter()
  p_web._RegisterDivEx(loc:divname,)
  do Children
  do ClosingScripts
  do SendPacket

LookupAbility  Routine
  If p_web.IfExistsValue('Lookup_Btn')
    loc:vorder = upper(p_web.GetValue('_sort'))
    p_web.PrimeForLookup(AUDSTATS,aus:RecordNumberKey,loc:vorder)
    If False
    ElsIf (loc:vorder = 'AUS:DATECHANGED') then p_web.SetValue('BrowseStatusChanges_sort','1')
    ElsIf (loc:vorder = 'AUS:TIMECHANGED') then p_web.SetValue('BrowseStatusChanges_sort','2')
    ElsIf (loc:vorder = 'TMP:USERNAME') then p_web.SetValue('BrowseStatusChanges_sort','4')
    ElsIf (loc:vorder = 'AUS:OLDSTATUS') then p_web.SetValue('BrowseStatusChanges_sort','5')
    ElsIf (loc:vorder = 'AUS:NEWSTATUS') then p_web.SetValue('BrowseStatusChanges_sort','3')
    End
  End
  If p_web.IfExistsValue('LookupField') and p_web.GetValue('BrowseStatusChanges:parentIs') <> 'Browse'
    loc:selecting = 1
    p_web.StoreValue('BrowseStatusChanges:LookupFrom','LookupFrom')
    p_web.StoreValue('BrowseStatusChanges:LookupField','LookupField')
  elsif p_web.RequestAjax > 0 and p_web.IfExistsSessionValue('BrowseStatusChanges:LookupField') > 0
    loc:selecting = 1
  else
    p_web.DeleteSessionValue('BrowseStatusChanges:LookupField')
    loc:selecting = 0
  End

GenerateBrowse Routine
  ! Set general Browse options
  loc:ButtonPosition   = Net:Below
  loc:SelectionMethod  = Net:Highlight
  loc:FileLoading      = Net:FileLoad
  loc:FillBack         = 0
  loc:Sorting          = Net:ServerSort
  ! Set Locator Options
  loc:LocatorPosition  = Net:Below
  loc:LocatorBlank = 0
  loc:LocatorType = Net:Position
  loc:LocatorSearchButton = false
  loc:LocatorClearButton = false
  do OpenFilesB
  do LookupAbility ! browse lookup initialization
  ! Save Window Name
  AddToLog('NetWebBrowse',p_web.RequestData.DataString,'BrowseStatusChanges',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  If loc:FileLoading = Net:PageLoad
    loc:pagerows = 10
  End
  loc:selectionexists = 0
  loc:pagename        = p_web.PageName
  ! Set Sort Order Options
  loc:vordernumber    = p_web.RestoreValue('BrowseStatusChanges_sort',net:DontEvaluate)
  p_web.SetSessionValue('BrowseStatusChanges_sort',loc:vordernumber)
  Loc:SortDirection = choose(loc:vordernumber < 0,-1,1)
  case abs(loc:vordernumber)
  of 1
    loc:vorder = Choose(Loc:SortDirection=1,'aus:DateChanged','-aus:DateChanged')
    Loc:LocateField = 'aus:DateChanged'
  of 2
    loc:vorder = Choose(Loc:SortDirection=1,'aus:TimeChanged','-aus:TimeChanged')
    Loc:LocateField = 'aus:TimeChanged'
  of 4
    loc:vorder = Choose(Loc:SortDirection=1,'tmp:UserName','-tmp:UserName')
    Loc:LocateField = 'tmp:UserName'
  of 5
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(aus:OldStatus)','-UPPER(aus:OldStatus)')
    Loc:LocateField = 'aus:OldStatus'
  of 3
    loc:vorder = Choose(Loc:SortDirection=1,'UPPER(aus:NewStatus)','-UPPER(aus:NewStatus)')
    Loc:LocateField = 'aus:NewStatus'
  end
  if loc:vorder = ''
    loc:vorder = '-aus:RecordNumber'
  end
  If False ! add range fields to sort order
  ElsIf (p_web.GSV('tmp:StatusTypeFilter') <> '')
    If Instring('AUS:TYPE',upper(loc:vOrder),1,1) = 0
      loc:vOrder = 'UPPER(aus:Type),' & loc:vorder
    End
  ElsIf (p_web.GSV('tmp:StatusTypeFilter') = '')
  End

  Case Left(Upper(Loc:LocateField))
  Of upper('aus:DateChanged')
    loc:SortHeader = p_web.Translate('Date Changed')
    p_web.SetSessionValue('BrowseStatusChanges_LocatorPic','@d6')
  Of upper('aus:TimeChanged')
    loc:SortHeader = p_web.Translate('Time Changed')
    p_web.SetSessionValue('BrowseStatusChanges_LocatorPic','@t1b')
  Of upper('tmp:UserName')
    loc:SortHeader = p_web.Translate('Username')
    p_web.SetSessionValue('BrowseStatusChanges_LocatorPic','@s60')
  Of upper('aus:OldStatus')
    loc:SortHeader = p_web.Translate('Old Status')
    p_web.SetSessionValue('BrowseStatusChanges_LocatorPic','@s30')
  Of upper('aus:NewStatus')
    loc:SortHeader = p_web.Translate('New Status')
    p_web.SetSessionValue('BrowseStatusChanges_LocatorPic','@s30')
  End
  If loc:selecting = 1
    loc:selectaction = p_web.GetSessionValue('BrowseStatusChanges:LookupFrom')
  End!Else
  loc:formaction = 'BrowseStatusChanges'
  do SendPacket
  loc:rowcount = 0
  ! in this section packets are added to the Queue using AddPacket, not sent using SendPacket
  If Loc:NoForm = 0
    packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" target="'&clip(loc:formactiontarget)&'" method="post" name="'&clip(loc:FormName)&'" id="'&clip(loc:FormName)&'"><13,10>'
  Else
    packet = clip(packet) & '<input type="hidden" name="BrowseStatusChanges:NoForm" value="1"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="BrowseStatusChanges:FormName" value="'&clip(loc:formname)&'"></input><13,10>'
  End
  If loc:Selecting = 1
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web.GetSessionValue('BrowseStatusChanges:LookupField')&'"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupFile" value="AUDSTATS"></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="LookupKey" value="aus:RecordNumberKey"></input><13,10>'
  end
  TableQueue.Kind = Net:BeforeTable
  do AddPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStatusChanges',Net:Above)
  IF(((loc:LocatorPosition=Net:Above) or (loc:LocatorPosition = Net:Both)) and (loc:FileLoading = Net:PageLoad)) and loc:sortheader <> ''
      packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
      '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
      '<td>' & p_web.CreateInput('text','Locator2BrowseStatusChanges',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseStatusChanges.locate(''Locator2BrowseStatusChanges'',this.value);" ') & '</td>'
      If loc:LocatorSearchButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
      End
      If loc:LocatorClearButton
        packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseStatusChanges.cl(''BrowseStatusChanges'');') & '</td>'
      End
      packet = clip(packet) & '</tr></table><13,10>'
    TableQueue.Kind = Net:Locator
    do AddPacket
  END
  TableQueue.Kind = Net:JustBeforeTable
  do AddPacket
  TableQueue.Kind = Net:RowTable
  If(loc:Sorting=Net:ClientSort)
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="sortable" id="BrowseStatusChanges_tbl">'&CRLF
  Else
    packet = clip(packet) & '<div class="'&clip('')&'"><table class="'&clip('BrowseTable')&'" id="BrowseStatusChanges_tbl">'&CRLF
  End
  do AddPacket
  TableQueue.Kind = Net:RowHeader
  packet = '<tr>'&CRLF
  loc:SelectColumnClass = ' class="selectcolumn"'
  If loc:SelectionMethod = Net:Radio
    packet = clip(packet) & '<th'&clip(loc:SelectColumnClass)&'><!-- Radio Select Column -->'&NBSP&'</th>'&CRLF
  End
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'1','BrowseStatusChanges','Date Changed','Click here to sort by Date Changed',,'RightJustify',100,1)
        Else
          packet = clip(packet) & '<th class="RightJustify" width="'&clip(100)&'" Title="'&p_web.Translate('Click here to sort by Date Changed')&'">'&p_web.Translate('Date Changed')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'2','BrowseStatusChanges','Time Changed','Click here to sort by Time Changed',,'RightJustify',100,1)
        Else
          packet = clip(packet) & '<th class="RightJustify" width="'&clip(100)&'" Title="'&p_web.Translate('Click here to sort by Time Changed')&'">'&p_web.Translate('Time Changed')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'4','BrowseStatusChanges','Username',,,'LeftJustify',200,1)
        Else
          packet = clip(packet) & '<th class="LeftJustify" width="'&clip(200)&'">'&p_web.Translate('Username')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'5','BrowseStatusChanges','Old Status','Click here to sort by Old Status',,'LeftJustify',200,1)
        Else
          packet = clip(packet) & '<th class="LeftJustify" width="'&clip(200)&'" Title="'&p_web.Translate('Click here to sort by Old Status')&'">'&p_web.Translate('Old Status')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
        If(loc:Sorting = Net:ServerSort)
          packet = clip(packet) & p_web._CreateSortHeader(loc:vordernumber,'3','BrowseStatusChanges','New Status','Click here to sort by New Status',,'LeftJustify',200,1)
        Else
          packet = clip(packet) & '<th class="LeftJustify" width="'&clip(200)&'" Title="'&p_web.Translate('Click here to sort by New Status')&'">'&p_web.Translate('New Status')&'</th>'&CRLF
        End
        do AddPacket
        loc:columns += 1
  packet = clip(packet) & '</tr>'&CRLF
  loc:rowstarted = 0
  do AddPacket
  TableQueue.Kind = Net:RowData
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  Open(ThisView)
  If Loc:LocateField = 'aus:DateChanged' then Loc:NoBuffer = 1.
  If Loc:NoBuffer = 0
    Buffer(ThisView,10,0,0,0) ! causes sorting error in ODBC, when sorting by Decimal fields.
  End
  ThisView{prop:order} = clip(loc:vorder)
  If Instring('aus:recordnumber',lower(Thisview{prop:order}),1,1) = 0 !and AUDSTATS{prop:SQLDriver} = 1
    ThisView{prop:order} = clip(loc:vorder) & ',' & 'aus:RecordNumber'
  End
  Loc:Selected = Choose(p_web.IfExistsValue('aus:RecordNumber'),p_web.GetValue('aus:RecordNumber'),p_web.GetSessionValue('aus:RecordNumber'))
  If False  ! Generate Filter
  ElsIf (p_web.GSV('tmp:StatusTypeFilter') <> '')
      tmp:StatusTypeFilter = p_web.RestoreValue('tmp:StatusTypeFilter')
      loc:FilterWas = 'aus:Type = ''' & clip(tmp:StatusTypeFilter) &''''
      If 'aus:RefNumber = ' & p_web.GetSessionValue('job:Ref_Number') <> ''
        loc:FilterWas = clip(loc:FilterWas) & ' AND ' & 'aus:RefNumber = ' & p_web.GetSessionValue('job:Ref_Number')
      End
  ElsIf (p_web.GSV('tmp:StatusTypeFilter') = '')
      loc:FilterWas = 'aus:RefNumber = ' & p_web.GetSessionValue('job:Ref_Number')
  Else
  End
  ThisView{prop:Filter} = loc:FilterWas
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStatusChanges',Net:Both)
  loc:FilterWas = ThisView{prop:filter}
  if loc:filterwas <> p_web.GetSessionValue('BrowseStatusChanges_Filter')
    p_web.SetSessionValue('BrowseStatusChanges_FirstValue','')
    p_web.SetSessionValue('BrowseStatusChanges_Filter',loc:filterwas)
  end
  p_web._SetView(ThisView,AUDSTATS,aus:RecordNumberKey,loc:PageRows,'BrowseStatusChanges',left(Loc:LocateField),loc:FileLoading,loc:LocatorType,clip(loc:LocatorValue),Loc:SortDirection,Loc:Options,Loc:FillBack,Loc:Direction,loc:NextDisabled,loc:PreviousDisabled)
  If loc:LocatorBlank = 0 or Loc:LocatorValue <> '' or loc:LocatorType = Net:Position or loc:LocatorType = Net:None
    Loop
      If loc:direction > 0 Then Next(ThisView) else Previous(ThisView).
      if errorcode() = 0                                ! in some cases the first record in the
        if loc:ViewPosWorkaround = Position(ThisView)   ! view is fetched twice after the SET(view,file)
          Cycle                                         ! 4.31 PR 18
        End
        loc:ViewPosWorkaround = Position(ThisView)
      End
      If ErrorCode()
        loc:ViewPosWorkaround = ''
        if loc:rowstarted
          packet = clip(packet) & '</tr>'&CRLF
          do AddPacket
          loc:rowstarted = 0
        End
        If loc:direction = -1
          loc:previousdisabled = 1
          Break
        End
        If loc:direction = 1
          loc:nextdisabled = 1
          Break
        End
        If loc:fillback = 0
          loc:nextdisabled = 1
          Break
        end
        if loc:direction = 2
          If loc:LocatorType = Net:Position
            ThisView{prop:Filter} = loc:FilterWas
          End
          If loc:first = 0
            Set(thisView)
          Else
            p_web._bounceView(ThisView)
            If AUDSTATS{prop:sqldriver}
              Reset(ThisView,loc:firstvalue)
            Else
              Reget(AUDSTATS,loc:firstvalue)
              Reset(ThisView,AUDSTATS)
            End
          End
          Previous(ThisView)
          loc:direction = -1
          loc:nextdisabled = 1
          Cycle
        End
        if loc:direction = -2
          p_web._bounceView(ThisView)
          If AUDSTATS{prop:sqldriver}
            !Set(ThisView) ! workaround to RESET bug ! done in BounceView
            Reset(ThisView,loc:lastvalue)
          Else
            Reget(AUDSTATS,loc:lastvalue)
            Reset(ThisView,AUDSTATS)
          End
          Next(ThisView)
          loc:direction = 1
          loc:previousdisabled = 1
          Cycle
        End
      End
      If(loc:FileLoading=Net:PageLoad)
        If loc:rowcount >= loc:pagerows !and loc:page > 1
          if loc:rowstarted
            packet = clip(packet) & '</tr>'&CRLF
            do AddPacket
            loc:rowstarted = 0
          End
          Break
        End
      End
      loc:viewstate = p_web.escape(p_web.Base64Encode(clip(aus:RecordNumber)))
      do BrowseRow
    end ! loop
  else
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('Enter a search term in the locator')&'</td></tr>'&CRLF
    do AddPacket
  end
  p_web._thisrow = ''
  p_web._thisvalue = ''
  if loc:found = 0
    packet = clip(packet) & '<tr><13,10><td>'&p_web.Translate('No Records found')&'</td></tr>'&CRLF
    do AddPacket
    loc:firstvalue = ''
    loc:lastvalue = ''
  end
  loc:direction = 1
  do MakeFooter
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0)) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseStatusChanges.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseStatusChanges.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseStatusChanges.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseStatusChanges.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        TableQueue.Kind = Net:BeforeTable
        do AddPacket
  End
  If (loc:ButtonPosition=Net:Above or (loc:ButtonPosition=Net:Both and loc:found > 0))
  End
  do SendQueue
  ! now back to calling SendPacket
  Loc:LocatorValue = p_web.GetLocatorValue(Loc:LocatorType,'BrowseStatusChanges',Net:Below)
  if(loc:FileLoading=Net:PageLoad)
    p_web.SetSessionValue('BrowseStatusChanges_FirstValue',loc:firstvalue)
    p_web.SetSessionValue('BrowseStatusChanges_LastValue',loc:lastvalue)
    If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
      If((Loc:LocatorPosition=2) or (Loc:LocatorPosition = 3)) and loc:sortheader <> ''
          packet = clip(packet) & '<table><tr class="'&clip('Locator')&'">' &|
          '<td>'& p_web.translate(p_web.site.LocatePromptText)& ' ' & clip(Loc:SortHeader) & ': </td>' &|
          '<td>' & p_web.CreateInput('text','Locator1BrowseStatusChanges',Choose(loc:LocatorType = Net:Position or loc:LocatorType = Net:None,'',clip(Loc:LocatorValue)),'Locator',,'onchange="BrowseStatusChanges.locate(''Locator1BrowseStatusChanges'',this.value);" ') & '</td>'
          If loc:LocatorSearchButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:LocateButton) & '</td>'
          End
          If loc:LocatorClearButton
            packet = clip(packet) & '<td>' & p_web.CreateStdButton('button',Net:Web:ClearButton,,,,'BrowseStatusChanges.cl(''BrowseStatusChanges'');') & '</td>'
          End
          packet = clip(packet) & '</tr></table><13,10>'
      End
    End
  End
  p_web.SetSessionValue('BrowseStatusChanges_prop:Order',ThisView{prop:order})
  p_web.SetSessionValue('BrowseStatusChanges_prop:Filter',ThisView{prop:filter})
  Close(ThisView)
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  do CloseFilesB
  If (loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both) and loc:FileLoading=Net:PageLoad
        if loc:previousdisabled = 0 or loc:nextdisabled = 0
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:FirstButton,,,,'BrowseStatusChanges.first();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:PreviousButton,,,,'BrowseStatusChanges.previous();',,loc:previousdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:NextButton,,,,'BrowseStatusChanges.next();',,loc:nextdisabled)
          packet = clip(packet) &  p_web.CreateStdButton('button',Net:Web:LastButton,,,,'BrowseStatusChanges.last();',,loc:nextdisabled)
          packet = clip(packet) & p_web.br
        end
        do SendPacket
  End
  If loc:ButtonPosition=Net:Below or loc:ButtonPosition=Net:Both
  End
  If Loc:NoForm = 0
    packet = clip(packet) & '</form>'&CRLF
  End
  do SendPacket

BrowseRow  routine
    Access:USERS.ClearKey(use:User_Code_Key)
    use:User_Code = aus:UserCode
    If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
        !Found
        tmp:UserName   = Clip(use:Surname) & ', ' & Clip(use:Forename)
    Else!If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
        !Error
        IF (aus:UserCode = '&SB')
            tmp:UserName = 'SYSTEM GENERATED'
        ELSE
            tmp:UserName = '* Unknown User Code: ' & Clip(aus:UserCode) & ' *'
        END ! IF
    End!If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
    
    loc:field = aus:RecordNumber
    p_web._thisrow = p_web._nocolon('aus:RecordNumber')
    p_web._thisvalue = p_web._jsok(loc:field)
    if loc:eip = 0
      if loc:selecting = 1
        loc:checked = Choose(p_web.getsessionvalue(p_web.GetSessionValue('BrowseStatusChanges:LookupField')) = aus:RecordNumber and loc:selectionexists = 0,'checked','')
        if loc:checked <> '' then do SetSelection.
      else
        if Loc:LocatorValue <> '' and loc:selectionexists = 0
           loc:checked = 'checked'
           do SetSelection
        else
          loc:checked = Choose((aus:RecordNumber = loc:selected) and loc:selectionexists = 0,'checked','')
          if loc:checked <> '' then do SetSelection.
        end
      end
      If(loc:SelectionMethod  = Net:Radio)
      ElsIf(loc:SelectionMethod  = Net:Highlight)
        loc:rowstyle = 'onclick="BrowseStatusChanges.cr(this,''' & p_web._jsok(loc:field,Net:Parameter) &''','&loc:ParentSilent&'); '
        loc:rowstyle = clip(loc:rowstyle) & '"'
      End
      do StartRowHTML
      do StartRow
      loc:RowsIn = 0
        if(loc:FileLoading=Net:PageLoad)
          if loc:first = 0 or loc:direction < 0
            If AUDSTATS{prop:SqlDriver} = 1
              loc:firstvalue = Position(ThisView)
            Else
              loc:firstvalue = Position(AUDSTATS)
            End
            if loc:first = 0 then loc:first = records(TableQueue) + 1.
            if loc:SelectionExists = 0
              do PushDefaultSelection
            else
              loc:SelectionExists += 1
            end
          end
          if loc:direction > 0 or loc:lastvalue = ''
            If AUDSTATS{prop:SqlDriver} = 1
              loc:lastvalue = Position(ThisView)
            Else
              loc:lastvalue = Position(AUDSTATS)
            End
          end
        Else
          If loc:first = 0
            loc:first = records(TableQueue) + 1
            if loc:SelectionExists = 0 then do PushDefaultSelection.
          End
        End
        If loc:checked then do SetSelection.
        If(loc:SelectionMethod  = Net:Radio)
          packet = clip(packet) & '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','aus:RecordNumber',clip(loc:field),,loc:checked,,,'onclick="BrowseStatusChanges.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'&CRLF
          if loc:FirstRowId = ''
            loc:FirstRow = '<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here-->'&p_web.CreateInput('radio','aus:RecordNumber',clip(loc:field),,'checked',,,'onclick="BrowseStatusChanges.value='''&p_web._jsok(loc:field,Net:Parameter)&'''"')&'</td>'
            loc:FirstRowID = loc:field
          end
        ElsIf(loc:SelectionMethod  = Net:Highlight)
          if loc:FirstRowId = '' or loc:direction < 0
            loc:FirstRowID = loc:field
          end
        End
        loc:tabledata = '<!--here-->'
    end ! loc:eip = 0
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="RightJustify" width="'&clip(100)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::aus:DateChanged
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="RightJustify" width="'&clip(100)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::aus:TimeChanged
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="LeftJustify" width="'&clip(200)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::tmp:UserName
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="LeftJustify" width="'&clip(200)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::aus:OldStatus
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
          If Loc:Eip = 0
              packet = clip(packet) & '<td class="LeftJustify" width="'&clip(200)&'">'&clip(loc:tabledata)&'<13,10>'
              loc:tabledata = ''
          end ! loc:eip = 0
          do value::aus:NewStatus
          If loc:eip = 0
            packet = clip(packet) & '</td><13,10>'
          End
      loc:found = 1

StartRowHTML  Routine
  If loc:rowstarted
    packet = clip(packet) & '</tr>'&CRLF
    do AddPacket
  End
  packet = clip(packet) & '<tr onMouseOver="BrowseStatusChanges.omv(this);" onMouseOut="BrowseStatusChanges.omt(this);" '&clip(loc:rowstyle)&'>'&CRLF
  loc:rowstarted = 1

StartRow     Routine
  loc:rowcount += 1
  TableQueue.Id = loc:field

ClosingScripts  Routine
  If p_web.RequestAjax = 0
    packet = clip(packet) &'<script type="text/javascript" defer="defer">'&|
      'var BrowseStatusChanges=new browseTable(''BrowseStatusChanges'','''&clip(loc:formname)&''','''&p_web._jsok('aus:RecordNumber',Net:Parameter)&''',1,'''&clip(loc:divname)&''',1,1,1,'''&clip(loc:parent)&''','''&clip(loc:formaction)&''','''&clip(loc:formactiontarget)&''',0,'''&p_web.Translate('Are you sure you want to delete this record?')&''','''&p_web.GSV('aus:RecordNumber')&''','''&clip(loc:selectaction)&''','''&clip(loc:formactiontarget)&''','''');<13,10>'&|
      'BrowseStatusChanges.setGreenBar('''&p_web.GetWebColor(p_web.Site.BrowseHighlightColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOneColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseTwoColor)&''','''&p_web.GetWebColor(p_web.Site.BrowseOverColor)&''');<13,10>' &|
      'BrowseStatusChanges.greenBar();<13,10>'&|
      '</script>'&CRLF
    do SendPacket
    If loc:sortheader <> '' and loc:FileLoading=Net:PageLoad and p_web.GetValue('SelectField') = ''
      If loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
        case loc:LocatorPosition
        of 1
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseStatusChanges')
        of 2
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseStatusChanges')
        of 3
          if loc:LocatorValue
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator1BrowseStatusChanges')
          else
            p_web.SetValue('SelectField',clip(loc:FormName)&'.Locator2BrowseStatusChanges')
          end
        End
      End
    End
    do SendPacket
  End

CloseFilesB  Routine
  p_web._CloseFile(AUDSTATS)
  p_web._CloseFile(USERS)
  popbind()

OpenFilesB  Routine
  pushbind()
  p_web._OpenFile(AUDSTATS)
  Bind(aus:Record)
  Clear(aus:Record)
  NetWebSetSessionPics(p_web,AUDSTATS)
  p_web._OpenFile(USERS)
  Bind(use:Record)
  NetWebSetSessionPics(p_web,USERS)

Children Routine
  If p_web.RequestAjax = 0
    do StartChildren
  Else
    do AjaxChildren
  End

AjaxChildren  Routine
    p_web.SetValue('aus:RecordNumber',loc:default)
    p_web.SetValue('refresh','first')

StartChildren  Routine
! ----------------------------------------------------------------------------------------
CallClicked  Routine
  p_web.SetSessionValue(p_web.GetValue('id'),p_web.GetValue('value'))
! ----------------------------------------------------------------------------------------
SendAlert Routine
      ! Save Window Name
      IF (loc:alert <> '')
          AddToLog('Alert',loc:alert,'BrowseStatusChanges',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
      END ! IF
  p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

CallEip  Routine
! ----------------------------------------------------------------------------------------
value::aus:DateChanged   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('aus:DateChanged_'&aus:RecordNumber,'RightJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(aus:DateChanged,'@d6')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::aus:TimeChanged   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('aus:TimeChanged_'&aus:RecordNumber,'RightJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(aus:TimeChanged,'@t1b')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::tmp:UserName   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('tmp:UserName_'&aus:RecordNumber,'LeftJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(tmp:UserName,'@s60')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::aus:OldStatus   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('aus:OldStatus_'&aus:RecordNumber,'LeftJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(aus:OldStatus,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
! ----------------------------------------------------------------------------------------
value::aus:NewStatus   Routine
  data
loc:extra    string(256)
loc:disabled string(20)
loc:FormOk   Long(1)
  code
    if false
    else
      packet = clip(packet) & p_web._DivHeader('aus:NewStatus_'&aus:RecordNumber,'LeftJustify',net:crc)
      packet = clip(packet) & p_web._jsok(Left(Format(aus:NewStatus,'@s30')),0)
    End
    packet = clip(packet) & p_web._DivFooter(0)
    if loc:eip = 1
      do SendPacket
    end
OpenFiles  ROUTINE
  p_web._OpenFile(USERS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(USERS)
     FilesOpened = False
  END
  return
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

CheckForDuplicate  Routine
PushDefaultSelection  Routine
  loc:default = aus:RecordNumber

SetSelection  Routine
  loc:selectionexists = loc:rowCount
  do PushDefaultSelection
  p_web.SetSessionValue('aus:RecordNumber',aus:RecordNumber)


MakeFooter  Routine

AddPacket  Routine
  If packet = '' then exit.
  TableQueue.Row = packet
  TableQueue.Sub = loc:RowsIn
  if loc:direction > 0
    add(TableQueue)
  else
    add(TableQueue,loc:first + loc:RowsIn)
  end
  packet = ''
  If TableQueue.Kind = Net:RowData
    Loc:RowNumber += 1
  End

SendQueue  Routine
  data
ix  long
iy  long
  code

  If loc:selectionexists = 0
    loc:default = loc:FirstRowID
    p_web.SetSessionValue('aus:RecordNumber',loc:default)
  End
  If loc:FirstRowID <> ''
    TableQueue.Id = loc:FirstRowID
    TableQueue.Sub = 0
    get(TableQueue,TableQueue.Id,TableQueue.Sub)
    If(loc:SelectionMethod  = Net:Highlight)
      If loc:selectionexists = 0 then loc:selectionexists = 1.
      ix = instring('<!--here-->',TableQueue.Row,1,1)
      TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,'&loc:SelectionExists&',rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('aus:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
      Put(TableQueue)
    ElsIf(loc:SelectionMethod  = Net:Radio)
      if loc:selectionexists = 0
        loc:selectionexists = 1
        ix = instring('<td'&clip(loc:MultiRowStyle)&clip(loc:SelectColumnClass)&'><!--here--><input type="radio"',TableQueue.Row,1,1)
        iy = instring('</td>',TableQueue.Row,1,ix)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & clip(loc:firstrow) & sub(TableQueue.Row,iy+5,size(TableQueue.Row))
        ix = instring('<!--here-->',TableQueue.Row,1,1)
        TableQueue.Row = sub(TableQueue.Row,1,ix-1) & '<!--selected,0,rows,'&loc:RowsHigh&',value,'&p_web._jsok(p_web.GSV('aus:RecordNumber'),Net:Parameter)&'-->' & sub(TableQueue.Row,ix+11,size(TableQueue.Row))
        Put(TableQueue)
      end
    End
  End

  Loop ix = 1 to records(TableQueue)
    get(TableQueue,ix)
    if TableQueue.Kind = Net:BeforeTable
      iy = Instring('__::__',TableQueue.Row,1,1)
      if iy > 0
        TableQueue.Row = sub(TableQueue.Row,1,iy-1) & p_web._jsok(loc:default) & sub(TableQueue.Row,iy+6,size(TableQueue.Row))
        put(TableQueue)
        break
      end
    end
  end

  loc:section = Net:BeforeTable
  do SendSection

  if loc:previousdisabled = 0 or loc:nextdisabled = 0 or loc:LocatorType = Net:Range or loc:LocatorType = Net:Contains
    loc:section = Net:Locator
    do SendSection
  end

  loc:section = Net:JustBeforeTable
  do SendSection

  loc:section = Net:RowTable
  do SendSection
  if loc:found
    packet = '<thead><13,10>'
    loc:section = Net:RowHeader
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</thead><13,10>'
      do SendPacket
    end
    packet = '<tfoot><13,10>'
    loc:section = Net:RowFooter
    do SendSection
    if packet = ''                   ! if it comes back blank, it's been sent, so send the closing tag.
      packet = '</tfoot><13,10>'
      do SendPacket
    end
  end
  packet = '<tbody><13,10>'
  do SendPacket
  loc:section = Net:RowData
  do SendSection
  packet = '</tbody></table></div><13,10>'
  do SendPacket

SendSection Routine
  DATA
loc:counter  Long
loc:r  Long
  CODE
  Loop loc:counter = 1 to records(TableQueue)
    get(TableQueue,loc:counter)
    if TableQueue.Kind = loc:section
      if loc:r = 0 then do SendPacket. ! <head> and <foot> come in "suspended"
      packet = TableQueue.Row
      do SendPacket
      loc:r += 1
    end
  End
  if(loc:FileLoading=Net:PageLoad)
  End
