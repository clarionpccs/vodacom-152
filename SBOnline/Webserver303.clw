

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER303.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER195.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER305.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER308.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER312.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER314.INC'),ONCE        !Req'd for module callout resolution
                     END


AuditProcess         PROCEDURE  (NetWebServerWorker p_web)
                    MAP
ExchangeCompleteAudit   PROCEDURE()
ExchangeNewAudit        PROCEDURE()
ExchangeContinueAudit           PROCEDURE()
ExchangeSuspendAudit    PROCEDURE()
LoanAuditPickStockType  PROCEDURE()
LoanCompleteAudit       PROCEDURE()
LoanContinueAudit       PROCEDURE()
LoanNewAudit            PROCEDURE()
LoanSuspendAudit        PROCEDURE()
StockAuditNextPart      PROCEDURE()
StockCompleteAudit		PROCEDURE()
StockNewAudit           PROCEDURE()
StockNewAuditStart      PROCEDURE()
StockSuspendAudit       PROCEDURE()
WIPCompleteAudit        PROCEDURE()
WIPFinishScan           PROCEDURE()
WIPOthersScanning   PROCEDURE(LONG pAuditNumber),LONG
WIPSuspendAudit PROCEDURE()
                    END ! MAP


ProgressBarWeb              CLASS
RecordCount                     LONG()
TotalRecords                    LONG()
SkipValue                       LONG(),PRIVATE
Skip                            LONG(),PRIVATE
DisplayText                     STRING(100),PRIVATE
PercentageValue                 LONG(),PRIVATE
Initialized                     LONG(),PRIVATE
Init                            PROCEDURE(LONG pTotalRecords,LONG pSkipValue=100)
Update                          PROCEDURE(<STRING pDisplayText>,LONG pForceUpdate=0)  
UpdateProgressBar               PROCEDURE(),PRIVATE
                            END ! CASE
loc:x          Long
packet              string(NET:MaxBinData)
packetlen           long
CRLF           String('<13,10>')
NBSP           String('&#160;')

  CODE
  GlobalErrors.SetProcedureName('AuditProcess')
  p_web.SetValue('_parentPage','AuditProcess')
  p_web.publicpage = 1
  if p_web.sessionId = 0 then p_web.NewSession().
        p_web.StoreValue('ProcessType')
        p_web.StoreValue('okURL')
        p_web.StoreValue('errorURL')
        IF (p_web.IfExistsValue('PrintType'))
            p_web.StoreValue('PrintType')
        END ! IF
  do Header
        DO SendPacket
        DO processing
        DO SendPacket
  packet = clip(packet) & p_web._jsBodyOnLoad('PageBody',,'PageBodyDiv')
    do SendPacket
    Do redirect
    do SendPacket
  do Footer
  packet = clip(packet) & p_web.Popup()
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

SendPacket  Routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,Net:NoHeader)
    packet = ''
  end
Header Routine
  packet = p_web.w3Header()
  packet = clip(packet) & '<head>'&|
      '<title>'&clip(p_web.site.PageTitle)&'</title>'&|
      '<meta http-equiv="Content-Type" content="text/html; charset='&clip(p_web.site.HtmlCharset)&'" /><13,10>'
  packet =  clip(packet) & p_web.IncludeStyles()
  packet =  clip(packet) & p_web.IncludeScripts()
  packet = clip(packet) & p_web.AddScript('progressbar.js')
    Do heading
    Do decProgressBar
  packet = clip(packet) & '</head><13,10>'
  p_web.ParseHTML(packet,1,0,Net:SendHeader+Net:DontCache)
  packet = ''
Footer Routine
  packet = clip(packet) & '<!-- Net:SelectField --><13,10>' &|
                          '<script>bodyOnLoad();</script><13,10>' &|
                         '</div></body><13,10></html><13,10>'
heading  Routine
  packet = clip(packet) & |
    '<<!-- Net:BannerBlank --><13,10>'&|
    ''
decProgressBar  Routine
  packet = clip(packet) & |
    '<<div id="progressframe" class="nt-process"><13,10>'&|
    '<<p class="nt-process-text">Please wait while ServiceBase searches for matching records<<br/><<br/>This may take a few seconds...<</br><<br/><</p><13,10>'&|
    '<<p id="progtitle" class="nt-process-text"><<br/><<br/><13,10>'&|
    '<<div id="progbarwrapper" class="smallish-progress-wrapper"><13,10>'&|
    '<<div id="progbar" class="smallish-progress-bar"><</div><13,10>'&|
    '<<div id="progtext" class="smallish-progress-text"><</div><13,10>'&|
    '<</div><13,10>'&|
    '<<br/><<a href="<<!-- Net:s:returnURL-->">Cancel Process<</a><13,10>'&|
    '<</p><13,10>'&|
    '<</div><13,10>'&|
    ''
processing  Routine
  packet = clip(packet) & |
    '<13,10>'&|
    ''
    DO SendPacket
    CASE p_web.GSV('ProcessType')
    OF 'ExchangeCompleteAudit'
        ExchangeCompleteAudit()
    OF 'ExchangeContinueAudit'
        ExchangeContinueAudit()
    OF 'ExchangeNewAudit'
        ExchangeNewAudit()
    OF 'ExchangeSuspendAudit'
        ExchangeSuspendAudit()
    OF 'LoanAuditPickStockType'
        LoanAuditPickStockType()
    OF 'LoanCompleteAudit'
        LoanCompleteAudit()
    OF 'LoanContinueAudit'  
        LoanContinueAudit()
    OF 'LoanNewAudit'
        LoanNewAudit()
    OF 'LoanSuspendAudit'
        LoanSuspendAudit()
    OF 'StockAuditNextPart'
        StockAuditNextPart()
    OF 'StockCompleteAudit'
        StockCompleteAudit()
    OF 'StockNewAudit'
        StockNewAudit()
    OF 'StockNewAuditStart'
        StockNewAuditStart()
    OF 'StockSuspendAudit'
        StockSuspendAudit()
    OF 'WIPCompleteAudit'
        WIPCompleteAudit()
    OF 'WIPFinishScan'
        WIPFinishScan()
    OF 'WIPSuspendAudit'
        WIPSuspendAudit()
    ELSE
        DO returnfail
    END ! CASE
    DO redirect        
    
redirect  Routine
  packet = clip(packet) & |
    '<13,10>'&|
    ''
    IF (p_web.GSV('okURL') <> '')
        packet = CLIP(packet) & |
            '<script>window.open("' & p_web.GSV('okURL') & '","_self");</script>'
    END ! IF   
returnfail  Routine
  packet = clip(packet) & |
    '<13,10>'&|
    ''
    IF (p_web.GSV('errorURL') <> '')
        CreateScript(p_web,packet,'window.open("' & p_web.GSV('errorURL') & '","_self")')
    ELSE
        CreateScript(p_web,packet,'window.close()')
        
    END ! IF
    
nothing             ROUTINE
    ! Don't redirect
ExchangeCompleteAudit       PROCEDURE()
    CODE
		! Whole load of stuff in the real code that doesn't work.
		! So will not include it here.
        IF (p_web.GSV('emf:Complete_Flag') = 1)
            CreateScript(p_web,packet,'alert("This audit has already been completed.")')
            Do ReturnFail
            RETURN
        END ! IF
		
        Relate:EXCHAMF.Open()
        Access:EXCHAMF.ClearKey(emf:Audit_Number_Key)
        emf:Audit_Number = p_web.GSV('emf:Audit_Number')
        IF (Access:EXCHAMF.TryFetch(emf:Audit_Number_Key) = Level:Benign)
            p_web.SessionQueueToFile(EXCHAMF)
            emf:Complete_Flag = 1
            emf:Status = 'AUDIT COMPLETED'
            Access:EXCHAMF.TryUpdate()
            p_web.FileToSessionQueue(EXCHAMF)
        ELSE ! IF
        END ! IF
        Relate:EXCHAMF.Close()

    CreateScript(p_web,packet,'window.open("PrintGenericDocument?PrintType=CompleteExchangeAudit","_self")')
    Do SendPacket

    DO nothing
ExchangeContinueAudit       PROCEDURE()
    CODE
        Relate:EXCHAMF.Open()
        Access:EXCHAMF.ClearKey(emf:Audit_Number_Key)
        emf:Audit_Number = p_web.GetValue('emf:Audit_Number')
        IF (Access:EXCHAMF.TryFetch(emf:Audit_Number_Key) = Level:Benign)
            p_web.FileToSessionQueue(EXCHAMF)
        END ! IF
        
!Remove because the code is wrong in the real program
! and to fix it here would stop the audits working
        
!        Relate:STOAUUSE.Open()
!        Access:STOAUUSE.ClearKey(stou:KeyAuditTypeNoUser)
!        stou:AuditType = 'E'
!        stou:Audit_No = p_web.GSV('emf:Audit_Number')
!        stou:User_Code = p_web.GSV('BookingUserCode')
!        SET(stou:KeyAuditTypeNoUser,stou:KeyAuditTypeNoUser)
!        LOOP UNTIL Access:STOAUUSE.Next() <> Level:Benign
!            IF (stou:AuditType <> 'E' OR |
!                stou:Audit_No <> p_web.GSV('emf:Audit_Number') OR |
!                stou:User_Code <> p_web.GSV('BookingUserCode'))
!                BREAK
!            END ! IF
!    
!            IF (stou:Current = 'Y')
!                packet = CLIP(packet) & '<script>simpleConfirm("Records show that you are already '& |
!                    'currently working within this audit.\n\n If you are using a shared log-in then ' & |
!                    'you are warned that this will cause problems, and will render this audit worthless.\n\nClick OK to continue and re-join this audit.","'& p_web.GSV('okURL') &'","' & p_web.GSV('errorURL') & '");</script>'
!            ELSE
!                BREAK
!            END ! IF
!        END ! LOOP
!        Relate:STOAUUSE.Close()        
        Relate:EXCHAMF.Open()

ExchangeNewAudit       PROCEDURE()
totalRecords    LONG()
    CODE
        Relate:EXCHAMF.Open()
        Relate:EXCHANGE.Open()
        Relate:EXCHAUI.Open()
    
        IF (Access:EXCHAMF.PrimeRecord() = Level:Benign)
            emf:Date    = TODAY()
            emf:Time    = CLOCK()
            emf:User    = p_web.GSV('BookingUserCode')
            emf:Complete_Flag = 0
            emf:Status = 'AUDIT IN PROGRESS'
            emf:Site_Location = p_web.GSV('BookingSiteLocation')
            IF (Access:EXCHAMF.TryInsert())
                Access:EXCHAMF.CancelAutoInc()
            END ! IF
            p_web.FileToSessionQueue(EXCHAMF)
        END ! IF
        
        totalRecords = 0
        Access:EXCHANGE.ClearKey(xch:AvailLocRef)
        xch:Available = 'AVL'
        xch:Location = p_web.GSV('BookingSiteLocation')
        SET(xch:AvailLocRef,xch:AvailLocRef)
        LOOP UNTIL Access:EXCHANGE.Next() <> Level:Benign
            IF (xch:Available <> 'AVL' OR |
                xch:Location <> p_web.GSV('BookingSiteLocation'))
                BREAK
            END ! IF
            totalRecords += 1
        END ! LOOP
        
        ProgressBarWeb.Init(totalRecords)
        
        Access:EXCHANGE.ClearKey(xch:AvailLocRef)
        xch:Available = 'AVL'
        xch:Location = p_web.GSV('BookingSiteLocation')
        SET(xch:AvailLocRef,xch:AvailLocRef)
        LOOP UNTIL Access:EXCHANGE.Next() <> Level:Benign
            IF (xch:Available <> 'AVL' OR |
                xch:Location <> p_web.GSV('BookingSiteLocation'))
                BREAK
            END ! IF
            
            ProgressBarWeb.Update('Building List Of Units..')
            
            IF (Access:EXCHAUI.PrimeRecord() = Level:Benign)
                eau:Audit_Number = emf:Audit_Number
                eau:Site_Location = xch:Location
                eau:Ref_Number = xch:Ref_Number
                eau:Exists = 'N'
                eau:Stock_Type = xch:Stock_Type
                eau:Shelf_Location = xch:Shelf_Location
                eau:Location = xch:Location
                eau:New_IMEI = FALSE
                eau:IMEI_Number = xch:ESN
                IF (Access:EXCHAUI.TryInsert())
                    Access:EXCHAUI.CancelAutoInc()
                END ! IF
            END ! IF
        END ! LOOP        
        
        Relate:EXCHAMF.Close()
        Relate:EXCHANGE.Close()
        Relate:EXCHAUI.Close()
        
        !DO Redirect

ExchangeSuspendAudit        PROCEDURE()
errorFound LONG(0)
    CODE
        Relate:STOAUUSE.Open()
        Relate:EXCHAMF.Open()

        LOOP 1 TIMES
! Remove because the real code is broken
! and the include it here would break the audits
            
!            Access:STOAUUSE.ClearKey(stou:KeyAuditTypeNo)
!            stou:AuditType = 'E'
!            stou:Audit_No = p_web.GSV('emf:Audit_Number')
!            SET(stou:KeyAuditTypeNo,stou:KeyAuditTypeNo)
!            LOOP UNTIL Access:STOAUUSE.Next() <> Level:Benign
!                IF (stou:AuditType <> 'E' OR |
!                    stou:Audit_No <> p_web.GSV('emf:Audit_Number'))
!                    BREAK
!                END ! IF
!                IF (stou:User_Code <> p_web.GSV('BookingUserCode') AND stou:Current = 'Y')
!                    packet = CLIP(packet) & '<script>alert("You cannot proceed with this request as user ' & CLIP(stou:User_Code) & ' is still recorded as being joined to the audit.");</script>'
!                    errorFound = 1
!                    BREAK
!                    
!                END ! IF
!            END ! LOOP
!            
!            IF (errorFound = 1)
!                DO ReturnFail
!                BREAK
!            END ! IF
    
            Access:EXCHAMF.ClearKey(emf:Audit_Number_Key)
            emf:Audit_Number = p_web.GSV('emf:Audit_Number')
            IF (Access:EXCHAMF.TryFetch(emf:Audit_Number_Key) = Level:Benign)
                p_web.SessionQueueToFile(EXCHAMF)
                emf:Status = 'SUSPENDED'
                Access:EXCHAMF.TryUpdate()
            END ! IF
        
        END ! LOOP
        
        Relate:STOAUUSE.Close()
        Relate:EXCHAMF.Close()
LoanAuditPickStockType		PROCEDURE()
    CODE
        IF (p_web.GSV('locStockType') = '')
            DO ReturnFail
        ELSE
            Relate:LOANAUI.Open()
            Relate:LOANALC.Open()
            p_web.SSV('locInStock',0)
            p_web.SSV('locAddedIn',0)
            Access:LOANAUI.ClearKey(lau:Main_Browse_Key)
            lau:Audit_Number = p_web.GSV('lmf:Audit_Number')
            lau:Confirmed = 0
            lau:Site_Location = p_web.GSV('BookingSiteLocation')
            lau:Stock_Type = p_web.GSV('locStockType')
            SET(lau:Main_Browse_Key,lau:Main_Browse_Key)
            LOOP UNTIL Access:LOANAUI.Next() <> Level:Benign
                IF (lau:Audit_Number <> p_web.GSV('lmf:Audit_Number') OR |
                    lau:Confirmed <> 0 OR |
                    lau:Site_Location <> p_web.GSV('BookingSiteLocation') OR |
                    lau:Stock_Type <> p_web.GSV('locStockType'))
                    BREAK
                END ! IF
                p_web.SSV('locInStock',p_web.GSV('locInStock') + 1)
            END ! LOOP
			
            p_web.SSV('locCounted',0)
			
            Access:LOANAUI.ClearKey(lau:Main_Browse_Key)
            lau:Audit_Number = p_web.GSV('lmf:Audit_Number')
            lau:Confirmed = 1
            lau:Site_Location = p_web.GSV('BookingSiteLocation')
            lau:Stock_Type = p_web.GSV('locStockType')
            SET(lau:Main_Browse_Key,lau:Main_Browse_Key)
            LOOP UNTIL Access:LOANAUI.Next() <> Level:Benign
                IF (lau:Audit_Number <> p_web.GSV('lmf:Audit_Number') OR |
                    lau:Confirmed <> 1 OR |
                    lau:Site_Location <> p_web.GSV('BookingSiteLocation') OR |
                    lau:Stock_Type <> p_web.GSV('locStockType'))
                    BREAK
                END ! IF
                IF (lau:New_IMEI = 1)
                    p_web.SSV('locAddedIn',p_web.GSV('locAddedIn') + 1)
                END ! IF
                p_web.SSV('locCounted',p_web.GSV('locCounted') + 1)
            END ! LOOP
			
            p_web.SSV('locInStock',p_web.GSV('locInStock') + p_web.GSV('locCounted') - p_web.GSV('locAddedIn'))
			
            Access:LOANALC.ClearKey(lac1:Locate_Key)
            lac1:Audit_Number = p_web.GSV('lmf:Audit_Number')
            lac1:Location = p_web.GSV('BookingSiteLocation')
            lac1:Stock_Type = p_web.GSV('locStockType')
            IF (Access:LOANALC.TryFetch(lac1:Locate_Key) = Level:Benign)
                IF (lac1:Confirmed = 1)
                    p_web.SSV('locStatus',p_web.GSV('locStockType') & ': STOCK TYPE AUDIT COMPLETE')
                ELSE ! IF
                    p_web.SSV('locStatus',p_web.GSV('locStockType') & ': PRELIMINARY AUDIT')
                END ! IF
            ELSE ! IF
                packet = CLIP(packet) & '<script>simpleConfirm("Do you wish to audit this stock type?","FormLoanAuditProcess?setprogress=startaudit","FormLoanAuditProcess?setprogress=startauditno");</script>'
            END ! IF
			
            Relate:LOANALC.Close()
            Relate:LOANAUI.Close()
        END ! IF
        ! Make sure the the loan browse is cleared
        p_web.SSV('BrowseAuditedLoanUnits_FirstValue','')
LoanCompleteAudit   PROCEDURE()
allComplete             LONG()
countRecords            LONG()
    CODE
        
            
        Relate:LOANALC.Open()
        Relate:LOANAUI.Open()
        Relate:LOAN.Open()
        Relate:LOANHIST.Open()
        Relate:LOANAMF.Open()
		
        ProgressBarWeb.Init(countRecords)
	
        countRecords = 0
		
        IF (p_web.GSV('lmf:Complete_Flag') = 1)
            
            CreateScript(p_web,packet,'alert("Error! The selected audit is already complete.")')
            
            Do ReturnFail	
        ELSE ! IF
            allComplete = 1
            Access:LOANALC.ClearKey(lac1:Locate_Key)
            lac1:Audit_Number = p_web.GSV('lmf:Audit_Number')
            SET(lac1:Locate_Key,lac1:Locate_Key)
            LOOP UNTIL Access:LOANALC.Next() <> Level:Benign
                IF (lac1:Audit_Number <> p_web.GSV('lmf:Audit_Number'))
                    BREAK
                END ! IF
                countRecords += 1
                IF (lac1:Confirmed = 0)
                    allComplete = 0
                END ! IF
            END ! LOOP
			
            ProgressBarWeb.Init(countRecords)
			
            IF (allComplete = 0)
                CreateScript(p_web,packet,'alert("There are unconfirmed stock type audits. Please check you have completed the audit correctly.")')
                
                Do ReturnFail
            ELSE ! IF
                Access:LOANALC.ClearKey(lac1:Locate_Key)
                lac1:Audit_Number = p_web.GSV('lmf:Audit_Number')
                SET(lac1:Locate_Key,lac1:Locate_Key)
                LOOP UNTIL Access:LOANALC.Next() <> Level:Benign
                    IF (lac1:Audit_Number <> p_web.GSV('lmf:Audit_Number'))
                        BREAK
                    END ! IF
					
                    ProgressBarWeb.Update('Completing Audit...')
					
                    IF (lmf:Complete_Flag = 0)
						! How the f are we here?
                        CYCLE
                    END ! IF
                    Access:LOANAUI.ClearKey(lau:Main_Browse_Key)
                    lau:Audit_Number = lac1:Audit_Number
                    lau:Confirmed = 0
                    lau:Site_Location = p_web.GSV('BookingSiteLocation')
                    lau:Stock_Type = lac1:Stock_Type
                    SET(lau:Main_Browse_Key,lau:Main_Browse_Key)
                    LOOP UNTIL Access:LOANAUI.Next() <> Level:Benign
                        IF (lau:Audit_Number <> lac1:Audit_Number OR |
                            lau:Confirmed <> 0 OR |
                            lau:Site_Location <> p_web.GSV('BookingSiteLocation') OR |
                            lau:Stock_Type <> lac1:Stock_Type)
                            BREAK
                        END ! IF
                        Access:LOAN.ClearKey(loa:Ref_Number_Key)
                        loa:Ref_Number = lau:Ref_Number
                        IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
                            loa:Available = 'AUS'
                            loa:StatusChangeDate = TODAY()
                            IF (Access:LOAN.TryUpdate() = Level:Benign)
                                IF (Access:LOANHIST.PrimeRecord() = Level:Benign)
                                    loh:Ref_Number = loa:Ref_Number
                                    loh:Date = TODAY()
                                    loh:Time = CLOCK()
                                    loh:User = p_web.GSV('BookingUserCode')
                                    loh:Status = 'AUDIT SHORTAGE'
                                    IF (Access:LOANHIST.TryInsert())
                                        Access:LOANHIST.CancelAutoInc()
                                    END ! IF
                                END ! IF
                            END ! IF
                        ELSE ! IF
                        END ! IF
                    END ! LOOP
					
                END ! LOOP
            END ! IF
			
            Access:LOANAMF.ClearKey(lmf:Audit_Number_Key)
            lmf:Audit_Number = p_web.GSV('lmf:Audit_Number')
            IF (Access:LOANAMF.TryFetch(lmf:Audit_Number_Key) = Level:Benign)
                lmf:Complete_Flag = 1
                lmf:Status = 'AUDIT COMPLETED'
                Access:LOANAMF.TryUpdate()
                
            ELSE ! IF
            END ! IF
			
        END ! IF
		
        Relate:LOAN.Close()
        Relate:LOANAUI.Close()
        Relate:LOANALC.Close()
        Relate:LOANHIST.Close()
        Relate:LOANAMF.Close()
        
        
        
    CreateScript(p_web,packet,'window.open("PrintGenericDocument?PrintType=CompleteLoanAudit","_self")')
    Do SendPacket

    DO nothing

LoanContinueAudit		PROCEDURE()
	CODE
		Relate:LOANAMF.Open()
		Access:LOANAMF.ClearKey(lmf:Audit_Number_Key)
		lmf:Audit_Number = p_web.GetValue('lmf:Audit_Number')
		IF (Access:LOANAMF.TryFetch(lmf:Audit_Number_Key) = Level:Benign)
			p_web.FileToSessionQueue(LOANAMF)
        ELSE ! IF
            DO ReturnFail
		END ! IF
		
		Relate:LOANAMF.Close()
LoanNewAudit		PROCEDURE()
	CODE
	    ! Clear Vars
        p_web.SSV('locInStock','')
        p_web.SSV('locCounted','')
        p_web.SSV('locVariance','')
        p_web.SSV('locStockType','')
        p_web.SSV('locStatus','')
        p_web.SSV('locIMEINumber','')
        
		ProgressBarWeb.Init(RECORDS(LOAN)/20)
		
		Relate:LOANAMF.Open()
		Relate:LOAN.Open()
		Relate:LOANAUI.Open()
		
		
        IF (Access:LOANAMF.PrimeRecord() = Level:Benign)
            lmf:Date = TODAY()
            lmf:Time = CLOCK()
            lmf:User = p_web.GSV('BookingUserCode')
            lmf:Complete_Flag = 0
            lmf:Status = 'AUDIT IN PROGRESS'
            lmf:Site_Location = p_web.GSV('BookingSiteLocation')
            IF (Access:LOANAMF.TryInsert())
                Access:LOANAMF.CancelAutoInc()
            END ! IF
        END ! IF
        p_web.FileToSessionQueue(LOANAMF)
		
		Access:LOAN.ClearKey(loa:LocRefKey)
		loa:Location = p_web.GSV('BookingSiteLocation')
		SET(loa:LocRefKey,loa:LocRefKey)
		LOOP UNTIL Access:LOAN.Next() <> Level:Benign
			IF (loa:Location <> p_web.GSV('BookingSiteLocation'))
				BREAK
			END ! IF
			
			ProgressBarWeb.Update('Building Loan List..')
			IF (Access:LOANAUI.PrimeRecord() = Level:Benign)
				lau:Audit_Number = lmf:Audit_Number
				lau:Site_Location = loa:Location
				lau:Ref_Number = loa:Ref_Number
				lau:Exists = 'N'
				lau:Stock_Type = loa:Stock_Type
				lau:Shelf_Location = loa:Shelf_Location
				lau:Location = loa:Location
				lau:New_IMEI = FALSE
				lau:IMEI_number = loa:ESN
				IF (Access:LOANAUI.TryInsert())
					Access:LOANAUI.CancelAutoInc()
				END ! IF
			END ! IF
		END ! LOOP
		
		Relate:LOANAUI.Close()
		Relate:LOANAMF.Close()
		Relate:LOAN.Close()
LoanSuspendAudit    PROCEDURE()
    CODE
        Access:LOANAMF.ClearKey(lmf:Audit_Number_Key)
        lmf:Audit_Number = p_web.GSV('lmf:Audit_Number')
        IF (Access:LOANAMF.TryFetch(lmf:Audit_Number_Key) = Level:Benign)
            p_web.SessionQueueToFile(LOANAMF)
            lmf:Status = 'SUSPENDED'
            Access:LOANAMF.TryUpdate()
        ELSE ! IF
        END ! IF
StockAuditNextPart	PROCEDURE()
	CODE
		Relate:STOAUDIT.Open()
		
		Access:STOAUDIT.ClearKey(stoa:Internal_AutoNumber_Key)
		stoa:Internal_AutoNumber = p_web.GSV('stoa:Internal_AutoNumber')
		IF (Access:STOAUDIT.TryFetch(stoa:Internal_AutoNumber_Key) = Level:Benign)
			stoa:New_Level = p_web.GSV('locCounted')
			stoa:Confirmed = 'Y'
            Access:STOAUDIT.TryUpdate()
            
            p_web.SSV('locCounted','')
            
			IF (stoa:New_Level <> stoa:Original_Level)
				IF (stoa:New_Level < stoa:Original_Level)
					! REdirect to delete part screen
					CreateScript(p_web,packet,'window.open("FormAuditAddStock?stoa__Internal_AutoNumber=' & stoa:Internal_AutoNumber & '&AuditAction=delvar","_self")')
					
				ELSE
					! REdirect to add part screen
					CreateScript(p_web,packet,'window.open("FormAuditAddStock?stoa__Internal_AutoNumber=' & stoa:Internal_AutoNumber & '&AuditAction=addvar","_self")')
				END !IF
			END ! IF
		ELSE ! IF
		END ! IF
		
        Relate:STOAUDIT.Close()
        
        p_web.SSV('BrowseAuditedStockUnits_FirstValue','')
StockCompleteAudit		PROCEDURE()
foundUser	LONG()
errorFound	LONG()
	CODE
		Relate:STOAUUSE.Open()
		Relate:STOAUDIT.Open()
		

		LOOP 1 TIMES
			foundUser = 0
			Access:STOAUUSE.ClearKey(stou:KeyAuditTypeNo)
			stou:AuditType = 'S'
			stou:Audit_no	 = p_web.GSV('stom:Audit_No')
			SET(stou:KeyAuditTypeNo,stou:KeyAuditTypeNo)
			LOOP UNTIL Access:STOAUUSE.Next() <> Level:Benign
				IF (stou:AuditType <> 'S' OR |
					stou:Audit_No <> p_web.GSV('stom:Audit_No'))
					BREAK
				END ! IF
				IF (stou:User_Code = p_web.GSV('BookingUserCode'))
					CYCLE
				END ! IF
				
				IF (stou:Current= 'Y')
					CreateScript(p_web,packet,'alert("You cannot proceed with this request as user '&clip(STOU:User_code)&' is still recorded as being joined to the audit.")')
					foundUser = 1
					BREAK
				END !IF
			END ! LOOP
			
			IF (foundUser = 1)
				errorFound = 1
				BREAK
			END ! IF
		
			IF (p_web.GSV('stom:CompleteType') = 'S')
				Access:STOAUDIT.ClearKey(stoa:Audit_Ref_No_Key)
				stoa:Audit_Ref_No = p_web.GSV('stom:Audit_No')
				SET(stoa:Audit_Ref_No_Key,stoa:Audit_Ref_No_Key)
				LOOP UNTIL Access:STOAUDIT.Next() <> Level:Benign
					IF (stoa:Audit_Ref_No <> p_web.GSV('stom:Audit_No'))
						BREAK
					END ! IF
					
					IF (stoa:Confirmed <> 'Y')
						stoa:Confirmed = 'Y'
						Access:STOAUDIT.TryUpdate()
						
						! General shortages need to be kept up to date
						IF (stoa:New_Level = stoa:Original_Level)
							Access:GENSHORT.ClearKey(gens:Lock_Down_Key)
							gens:Audit_No = stoa:Audit_Ref_No
							gens:Stock_Ref_No = stoa:Stock_Ref_no
							IF (Access:GENSHORT.TryFetch(gens:Lock_Down_Key) = Level:Benign)
								
							ELSE ! IF
								Access:GENSHORT.DeleteRecord(0)
							END ! IF
						ELSE ! IF
							! Stock Levels Are Different - Record This
							Access:GENSHORT.ClearKey(gens:Lock_Down_Key)
							gens:Audit_No = stoa:Audit_Ref_No
							gens:Stock_Ref_No = stoa:Stock_Ref_no
							IF (Access:GENSHORT.TryFetch(gens:Lock_Down_Key) = Level:Benign)
								
							ELSE ! IF
								IF (Access:GENSHORT.PrimeRecord() = Level:Benign)
									gens:Audit_No = stoa:Audit_Ref_No
									gens:Stock_Ref_No = stoa:Stock_Ref_No
									IF (Access:GENSHORT.TryInsert())
										Access:GENSHORT.CancelAutoInc()
									END ! IF
								END ! IF
							END ! IF							
							gens:Stock_Qty = stoa:New_Level - stoa:Original_Level
							Access:GENSHORT.TryUpdate()
						END ! IF
					ELSE ! IF
					END!  IF
					
				END ! LOOP
			ELSE ! IF
			END ! IF
			
			Access:STMASAUD.ClearKey(stom:AutoIncrement_Key)
			stom:Audit_No = p_web.GSV('stom:Audit_No')
			IF (Access:STMASAUD.TryFetch(stom:AutoIncrement_Key) = Level:Benign)
				p_web.SessionQUeueToFile(STMASAUD)
				stom:Complete = 'Y'
				IF (Access:STMASAUD.TryUpdate() = Level:Benign)
					Access:STOAUUSE.ClearKey(stou:KeyAuditTypeNo)
					stou:AuditType  = 'S'
					stou:Audit_No = stom:Audit_No
					SET(stou:KeyAuditTypeNo,stou:KeyAuditTypeNo)
					LOOP UNTIL Access:STOAUUSE.Next() <> Level:Benign
						IF (stou:AuditType  <> 'S' OR |
							stou:Audit_No <> stom:Audit_No)
							BREAK
						END ! IF
						Access:STOAUUSE.DeleteRecord(0)
					END ! LOOP
				END ! IF
			ELSE ! IF
			END ! IF
		
		END ! LOOP
		
		Relate:STOAUUSE.Close()
		Relate:STOAUDIT.Close()
		
		IF (errorFound)
			Do ReturnFail
		END ! IF
		
		
		
StockNewAudit		PROCEDURE()
recordCount	LONG()
    CODE
        p_web.StoreValue('AuditType')
        
        p_web.SSV('pBuildForAudit',1)
        p_web.SSV('pUpdateProgress',1)
        BuildStockAuditQueue(p_web)
        recordCount = p_web.GSV('recordCount')
!        recordCount = BuildStockAuditQueue(p_web,1)
        
		IF (recordCount > 0)
			CreateScript(p_web,packet,'confirmStockAuditStart(' & recordCount & ')')
			!DO ReturnFail
		END ! IF
StockNewAuditStart  PROCEDURE()
i                       LONG()
	CODE
		Relate:STMASAUD.Open()
		Relate:STOAUDIT.Open()
        Relate:STOCK.Open()
        Relate:STOAUUSE.Open()
		
        !ProgressBarWeb.Init(RECORDS(TempStockAuditQueue))
        ProgressBarWeb.Init(RECORDS(qStockCheck))
        
        
		
		IF (Access:STMASAUD.PrimeRecord() = Level:Benign)
			stom:Audit_Date = TODAY()
			stom:Audit_Time = CLOCK()
			stom:Audit_User = p_web.GSV('BookingUserCode')
			stom:Complete = 'N'
			stom:Branch = p_web.GSV('BookingSiteLocation')
			stom:CompleteType = p_web.GSV('AuditType')
            IF (Access:STMASAUD.TryInsert())
                Access:STMASAUD.CancelAutoInc()
            END ! IF
            
            p_web.FileToSessionQueue(STMASAUD)
			
            LOOP i = 1 TO RECORDS(qStockCheck)
                GET(qStockCheck,i)
                IF (qStockCheck.SessionID <> p_web.SessionID)
                    CYCLE
                END ! IF
            
!            
!            CLEAR(tmpsto:Record)
!            tmpsto:SessionID = p_web.SessionID
!            SET(tmpsto:KeySessionID,tmpsto:KeySessionID)
!            LOOP UNTIL EOF(TempStockAuditQueue)
!                NEXT(TempStockAuditQueue)
!                
!                IF (ERROR())
!                    BREAK
!                END ! IF
!                IF (tmpsto:SessionID <> p_web.SessionID)
!                    BREAK
!                END ! IF
				
                ProgressBarWeb.Update('Adding Records To Audit..')
                
				
                Access:STOCK.ClearKey(sto:Ref_Number_Key)
                sto:Ref_Number = qStockCheck.RefNumber!tmpsto:RefNumber
                IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
					
                ELSE ! IF
                    CYCLE
                END ! IF
				
                IF (Access:STOAUDIT.PrimeRecord() = Level:Benign)
                    stoa:Audit_Ref_No = p_web.GSV('stom:Audit_No')
                    stoa:Site_Location = p_web.GSV('BookingSiteLocation')
                    stoa:Stock_Ref_No = qStockCheck.RefNumber!tmpsto:RefNumber
                    stoa:Original_Level = sto:Quantity_Stock
                    stoa:New_Level = 0
                    stoa:Audit_Reason = ''
                    stoa:Preliminary  = ''
                    stoa:Confirmed = 'N'
                    stoa:Shelf_Location = sto:Shelf_Location
                    stoa:Second_Location = sto:Second_Location
                    IF (Access:STOAUDIT.TryInsert())
                        Access:STOAUDIT.CancelAutoInc()
                    END ! IF
                END ! IF
				
            END ! LOOP
            
            IF (Access:STOAUUSE.PrimeRecord() = Level:Benign)
                STOU:AuditType       = 'S'
                STOU:Audit_No        = p_web.GSV('stom:Audit_No')
                STOU:User_code       = p_web.GSV('BookingUserCode')
                STOU:Current         = 'Y'
                STOU:StockPartNumber = 'PRIMARY LOG-IN'
                IF (Access:STOAUUSE.TryInsert())
                    Access:STOAUUSE.CancelAutoInc()
                END ! IF
            END ! IF
			
		END ! IF
		
		Relate:STMASAUD.Close()
		Relate:STOAUDIT.Close()
        Relate:STOCK.Close()
        Relate:STOAUUSE.Close()
StockSuspendAudit   PROCEDURE()
    CODE
        Relate:STMASAUD.Open()
        Access:STMASAUD.ClearKey(stom:AutoIncrement_Key)
        stom:Audit_No = p_web.GSV('stom:Audit_No')
        IF (Access:STMASAUD.TryFetch(stom:AutoIncrement_Key) = Level:Benign)
            p_web.SessionQueueToFile(STMASAUD)
            stom:Send = 'S'
            Access:STMASAUD.TryUpdate()
        ELSE ! IF
        END ! IF
        Relate:STMASAUD.Close()	
WIPCompleteAudit    PROCEDURE()
otherScanners   LONG(0)
    CODE
        IF (WIPOthersScanning(p_web.GSV('wim:Audit_Number')))
            DO ReturnFail
        ELSE ! IF 
            Relate:WIPAMF.Open()
            
            Access:WIPAMF.ClearKey(wim:Audit_Number_Key)
            wim:Audit_Number = p_web.GSV('wim:Audit_Number')
            IF (Access:WIPAMF.TryFetch(wim:Audit_Number_Key) = Level:Benign)
                p_web.SessionQueueToFile(WIPAMF)
                wim:Status = 'AUDIT COMPLETED'
                wim:Date_Completed = TODAY()
                wim:Time_Completed = CLOCK()
                wim:Complete_Flag = 1
                Access:WIPAMF.TryUpdate()
                p_web.FileToSessionQueue(WIPAMF) ! Reget 
                ! TODO: CALL THE REPORT
            END ! IF
            
            Relate:WIPAMF.Close()
        END ! IF
WIPFinishScan       PROCEDURE()
    CODE
        
        Relate:WIPSCAN.Open()
        
        Access:WIPSCAN.ClearKey(wsc:Completed_Audit_Key)
        wsc:Completed = 0
        wsc:Audit_Number = p_web.GSV('wim:Audit_Number')
        SET(wsc:Completed_Audit_Key,wsc:Completed_Audit_Key)
        LOOP UNTIL Access:WIPSCAN.Next() <> Level:Benign
            IF (wsc:Completed <> 0 OR |
                wsc:Audit_Number <> p_web.GSV('wim:Audit_Number'))
                BREAK
            END ! IF
            IF (wsc:User = p_web.GSV('BookingUsedCode'))
                wsc:Completed = 1
                Access:WIPSCAN.TryUpdate()
                BREAK
            END ! IF
        END ! LOOP           

        Relate:WIPSCAN.Close()
WIPOthersScanning   PROCEDURE(LONG pAuditNumber)!,LONG
found   LONG(0)
    CODE
        Relate:WIPSCAN.Open()

        Access:WIPSCAN.ClearKey(wsc:Completed_Audit_Key)
        wsc:Completed = 0
        wsc:Audit_Number = pAuditNumber
        SET(wsc:Completed_Audit_Key,wsc:Completed_Audit_Key)
        LOOP UNTIL Access:WIPSCAN.Next() <> Level:Benign
            IF (wsc:Completed <> 0 OR |
                wsc:Audit_Number <> pAuditNumber)
                BREAK
            END ! IF
            found = 1
            CreateScript(p_web,packet,'alert("Other users are still joined in the audit scanning. Please recheck.")')
            BREAK
        END ! LOOP 
        Relate:WIPSCAN.Close()
        
        RETURN found
WIPSuspendAudit     PROCEDURE()
    CODE
        Relate:WIPAMF.Open()
        
        
        IF (WIPOthersScanning(p_web.GSV('wim:Audit_Number')))
            DO ReturnFail
        ELSE ! 
            Access:WIPAMF.ClearKey(wim:Audit_Number_Key)
            wim:Audit_Number = p_web.GSV('wim:Audit_Number')
            IF (Access:WIPAMF.TryFetch(wim:Audit_Number_Key) = Level:Benign)
                p_web.SessionQueueToFile(WIPAMF)
                wim:Status = 'SUSPENDED'
                Access:WIPAMF.TryUpdate()
            ELSE ! IF
            END ! IF
        END ! 
        
        Relate:WIPAMF.Close()
ProgressBarWeb.Init PROCEDURE(LONG pTotalRecords,LONG pSkipValue=100)
    CODE
        SELF.TotalRecords = pTotalRecords
        SELF.RecordCount = 0
        SELF.Skip = 0
        SELF.SkipValue = pSkipValue
        
ProgressBarWeb.Update       PROCEDURE(<STRING pDisplayText>,LONG pForceUpdate=0)  
    CODE
        SELF.RecordCount += 1
        IF (pDisplayText <> '')
            SELF.DisplayText = pDisplayText
        ELSE ! IF
            SELF.DisplayText = ''
        END ! IF
        
        
        IF (SELF.Skip > SELF.SkipValue OR SELF.Skip = 0)
            SELF.Skip = 0

            SELF.PercentageValue = INT((SELF.RecordCount / SELF.TotalRecords) * 100)
        
            IF (SELF.PercentageValue => 100)
                SELF.RecordCount = 0
                SELF.PercentageValue = 0
            END ! IF

            SELF.UpdateProgressBar()
        END ! IF
        IF (pForceUpdate)
            SELF.UpdateProgressBar()
        END !I F
        
        SELF.Skip += 1
ProgressBarWeb.UpdateProgressBar    PROCEDURE()
    CODE
        packet = CLIP(packet) & '<script>updateProgressBar("' & CLIP(SELF.DisplayText) & '",' & SELF.PercentageValue & ');</script>'
        DO SendPacket 
