

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER130.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER021.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER131.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER134.INC'),ONCE        !Req'd for module callout resolution
                     END


ReceiptFromPUP       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locIMEINumber        STRING(30)                            !
locMSN               STRING(30)                            !
locNetwork           STRING(30)                            !
locInFault           STRING(30)                            !
locIMEIMessage       STRING(100)                           !
locPassword          STRING(30)                            !
locValidationMessage STRING(100)                           !
                    MAP
BouncerCheck            PROCEDURE(STRING pIMEI, *STRING pError),LONG   
                    END ! MAP
FilesOpened     Long
DEFAULTS::State  USHORT
JOBS_ALIAS::State  USHORT
Tagging::State  USHORT
JOBSE3::State  USHORT
WEBJOB::State  USHORT
NETWORKS::State  USHORT
MANFAULT::State  USHORT
JOBACC::State  USHORT
JOBS::State  USHORT
JOBSE::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('ReceiptFromPUP')
  loc:formname = 'ReceiptFromPUP_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'ReceiptFromPUP',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('ReceiptFromPUP','')
    p_web._DivHeader('ReceiptFromPUP',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferReceiptFromPUP',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferReceiptFromPUP',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferReceiptFromPUP',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ReceiptFromPUP',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferReceiptFromPUP',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_ReceiptFromPUP',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'ReceiptFromPUP',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
deleteVariables         routine
    p_web.deleteSessionValue('locIMEINumber')
    p_web.deleteSessionValue('locIMEIMessage')
    p_web.deleteSessionValue('locIMEIAccepted')
    p_web.deleteSessionValue('locMSN')
    p_web.deleteSessionValue('locNetwork')
    p_web.deleteSessionValue('locInFault')
    p_web.deleteSessionValue('locPassword')
    p_web.deleteSessionValue('PasswordRequired')
    p_web.deleteSessionValue('Comment:IMEINumber')
    p_web.deleteSessionValue('Comment:Password')
    p_web.deleteSessionValue('tmp:LoanModelNumber')
    p_web.deleteSessionValue('locValidationMessage')
saveChanges         routine
    data
locNotes        String(255)
    code
        if (p_web.GSV('locIMEINumber') <> p_web.GSV('job:ESN'))
            locNotes = '<13,10>OLD I.M.E.I.: ' & p_web.GSV('job:ESN') & |
                '<13,10>NEW I.M.E.I.: ' & p_web.GSV('locIMEINumber')
            p_web.SSV('job:ESN',p_web.GSV('locIMEINumber'))
        end ! if (p_web.GSV('locIMEINumber') <> p_web.GSV('job:ESN'))

        if (p_web.GSV('locMSN') <> p_web.GSV('job:MSN'))
            locNotes = clip(locNotes) & |
                '<13,10>OLD M.S.N.: ' & p_web.GSV('job:MSN') & |
                '<13,10>NEW M.S.N.: ' & p_web.GSV('locMSN')
            p_web.SSV('job:MSN',p_web.GSV('locMSN'))
        end ! if (p_web.GSV('locMSN') <> p_web.GSV('job:MSN'))

        if (p_web.GSV('locNetwork') <> p_web.GSV('jobe:Network'))
            locNotes = clip(locNotes) & |
                '<13,10>OLD NETWORK: ' & p_web.GSV('jobe:Network') & |
                '<13,10>NEW NETWORK: ' & p_web.GSV('locNetwork')
            p_web.SSV('jobe:Network',p_web.GSV('locNetwork'))
        end ! if (p_web.GSV('locNetwork') <> p_web.GSV('job:Network'))

        Access:MANFAULT.Clearkey(maf:inFaultKey)
        maf:manufacturer    = p_web.GSV('job:manufacturer')
        maf:inFault    = 1
        if (Access:MANFAULT.TryFetch(maf:inFaultKey) = Level:Benign)
        ! Found
        else ! if (Access:MANFAULT.TryFetch(maf:inFaultKey) = Level:Benign)
        ! Error
        end ! if (Access:MANFAULT.TryFetch(maf:inFaultKey) = Level:Benign)

        if (maf:Field_Number < 13)
            if (p_web.GSV('job:Fault_Code' & maf:Field_Number) <> p_web.GSV('locInFault'))
                locNotes = clip(locNotes) & |
                    '<13,10>OLD IN FAULT: ' & p_web.GSV('job:Fault_Code' & maf:Field_Number) & |
                    '<13,10>NEW IN FAULT: ' & p_web.GSV('locInFault')
                p_web.SSV('job:Fault_Code' & maf:Field_Number,p_web.GSV('locInFault'))
            end ! if (p_web.GSV('job:Fault_Code' & maf:Field_Number) <> p_web.GSV('locInFault'))
        else ! if (maf:Field_Number < 13)
            if (p_web.GSV('wob:FaultCode' & maf:Field_Number) <> p_web.GSV('locInFault'))
                locNotes = clip(locNotes) & |
                    '<13,10>OLD IN FAULT: ' & p_web.GSV('wob:FaultCode' & maf:Field_Number) & |
                    '<13,10>NEW IN FAULT: ' & p_web.GSV('locInFault')
                p_web.SSV('wob:FaultCode' & maf:Field_Number,p_web.GSV('locInFault'))
            end ! if (p_web.GSV('job:Fault_Code' & maf:Field_Number) <> p_web.GSV('locInFault'))
        end ! if (maf:Field_Number < 13)

        if (clip(locNotes) <> '')
!            p_web.SSV('AddToAudit:Type','JOB')
!            p_web.SSV('AddToAudit:Notes','DETAILS CHANGED:' & clip(locNotes))
!            p_web.SSV('AddToAudit:Action','PUP VALIDATION')
            addToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','PUP VALIDATION','DETAILS CHANGED:' & clip(locNotes))
        end ! if (clip(locNotes) <> '')

        locNotes = ''
        if (instring('MISMATCH',upper(p_web.GSV('locValidationMessage')),1,1))
            locNotes = 'ACCESSORY MISMATCH. BOOKED AT PUP:-'
            Access:JOBACC.Clearkey(jac:ref_Number_Key)
            jac:ref_Number    = p_web.GSV('job:Ref_Number')
            set(jac:ref_Number_Key,jac:ref_Number_Key)
            loop
                if (Access:JOBACC.Next())
                    Break
                end ! if (Access:JOBACC.Next())
                if (jac:ref_Number    <> p_web.GSV('job:Ref_Number'))
                    Break
                end ! if (jac:ref_Number    <> p_web.GSV('job:Ref_Number'))
                locNotes = clip(locNotes) & |
                    '<13,10>' & clip(jac:accessory)
            end ! loop

            locNotes = clip(locNotes) & |
                '<13,10,13,0>ACCESSORIES RECEIVED:-'
            
            Access:Tagging.ClearKey(tgg:SessionIDKey)
            tgg:SessionID = p_web.SessionID
            SET(tgg:SessionIDKey,tgg:SessionIDKey)
            LOOP UNTIL Access:Tagging.Next() <> Level:Benign
                IF (tgg:SessionID <> p_web.SessionID)
                    BREAK
                END ! IF
                IF (tgg:Tagged = 0)
                    CYCLE
                END ! IF
                locNotes = clip(locNotes) & |
                    '<13,10>' & clip(tgg:taggedValue)   
            END ! LOOP

        end ! if (instring('MISMATCH',upper(p_web.GSV('locValidationMessage')),1,1)

!        p_web.SSV('AddToAudit:Type','JOB')
!        p_web.SSV('AddToAudit:Notes',clip(locNotes))
!        p_web.SSV('AddToAudit:Action','UNIT RECEIVED AT ' & p_web.GSV('BookingSite') & ' FROM PUP')
        addToAudit(p_web,p_web.GSV('job:Ref_Number'),'JOB','UNIT RECEIVED AT ' & p_web.GSV('BookingSite') & ' FROM PUP',clip(locNotes))

        !p_web.SSV('LocationChange:Location',p_web.GSV('Default:' & p_web.GSV('BookingSite') & 'Location'))
        locationChange(p_web,p_web.GSV('Default:' & p_web.GSV('BookingSite') & 'Location'))

        p_web.SSV('job:Workshop','YES')

        p_web.SSV('GetStatus:Type','JOB')
        p_web.SSV('GetStatus:StatusNumber',sub(p_web.GSV('Default:StatusReceivedFromPUP'),1,3))
        getStatus(sub(p_web.GSV('Default:StatusReceivedFromPUP'),1,3),0,'JOB',p_web)

        Access:JOBS.Clearkey(job:ref_Number_Key)
        job:ref_Number    = p_web.GSV('job:Ref_Number')
        if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
        ! Found
            p_web.SessionQueueToFile(JOBS)
            access:JOBS.tryUpdate()
            updateDateTimeStamp(job:Ref_Number)
        else ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
        ! Error
        end ! if (Access:JOBS.TryFetch(job:ref_Number_Key) = Level:Benign)
        
        
        IF (p_web.GSV('jobe3:SpecificNeeds') = 1)
            ! #13499 Send specific needs email when job is received from PUP (DBH: 02/04/2015)
            SendSpecificNeedsEmail(p_web)
        END ! IF
        
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = p_web.GSV('job:Ref_Number')
        if (Access:WEBJOB.TryFetch(wob:RefNumberKey)= Level:Benign)
            p_web.SessionQueueToFile(WEBJOB)
            access:WEBJOB.TryUpdate()
        END
        
        Access:JOBSE.Clearkey(jobe:refNumberKey)
        jobe:refNumber    = p_web.GSV('job:Ref_Number')
        if (Access:JOBSE.TryFetch(jobe:refNumberKey) = Level:Benign)
        ! Found
            p_web.SessionQueueToFile(JOBSE)
            access:JOBSE.tryUpdate()
        else ! if (Access:JOBSE.TryFetch(jobe:refNumberKey) = Level:Benign)
        ! Error
        end ! if (Access:JOBSE.TryFetch(jobe:refNumberKey) = Level:Benign)
OpenFiles  ROUTINE
  p_web._OpenFile(DEFAULTS)
  p_web._OpenFile(JOBS_ALIAS)
  p_web._OpenFile(Tagging)
  p_web._OpenFile(JOBSE3)
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(NETWORKS)
  p_web._OpenFile(MANFAULT)
  p_web._OpenFile(JOBACC)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(JOBSE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(DEFAULTS)
  p_Web._CloseFile(JOBS_ALIAS)
  p_Web._CloseFile(Tagging)
  p_Web._CloseFile(JOBSE3)
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(NETWORKS)
  p_Web._CloseFile(MANFAULT)
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(JOBSE)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('ReceiptFromPUP_form:inited_',1)
  do RestoreMem

CancelForm  Routine
    DO DeleteVariables

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'locNetwork'
    p_web.setsessionvalue('showtab_ReceiptFromPUP',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(NETWORKS)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locInFault')
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  p_web.SetSessionValue('locIMEIMessage',locIMEIMessage)
  p_web.SetSessionValue('locPassword',locPassword)
  p_web.SetSessionValue('locMSN',locMSN)
  p_web.SetSessionValue('locNetwork',locNetwork)
  p_web.SetSessionValue('locInFault',locInFault)
  p_web.SetSessionValue('locValidationMessage',locValidationMessage)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locIMEINumber')
    locIMEINumber = p_web.GetValue('locIMEINumber')
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  End
  if p_web.IfExistsValue('locIMEIMessage')
    locIMEIMessage = p_web.GetValue('locIMEIMessage')
    p_web.SetSessionValue('locIMEIMessage',locIMEIMessage)
  End
  if p_web.IfExistsValue('locPassword')
    locPassword = p_web.GetValue('locPassword')
    p_web.SetSessionValue('locPassword',locPassword)
  End
  if p_web.IfExistsValue('locMSN')
    locMSN = p_web.GetValue('locMSN')
    p_web.SetSessionValue('locMSN',locMSN)
  End
  if p_web.IfExistsValue('locNetwork')
    locNetwork = p_web.GetValue('locNetwork')
    p_web.SetSessionValue('locNetwork',locNetwork)
  End
  if p_web.IfExistsValue('locInFault')
    locInFault = p_web.GetValue('locInFault')
    p_web.SetSessionValue('locInFault',locInFault)
  End
  if p_web.IfExistsValue('locValidationMessage')
    locValidationMessage = p_web.GetValue('locValidationMessage')
    p_web.SetSessionValue('locValidationMessage',locValidationMessage)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('ReceiptFromPUP_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
        !INIT
    if (MSNRequired(p_web.GSV('job:Manufacturer')))
        p_web.SSV('Hide:MSN',0)
    else
        p_web.SSV('Hide:MSN',1)
    end ! if (MSNRequired(p_web.GSV('job:Manufacturer')))
  
    p_web.SSV('Comment:IMEINumber','Enter I.M.E.I. and press [TAB] to accept')
  
      ! Used for the tag browse
    p_web.SSV('tmp:LoanModelNumber',p_web.GSV('job:Model_Number'))
  
    ClearTaggingFile(p_web)
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locIMEINumber = p_web.RestoreValue('locIMEINumber')
 locIMEIMessage = p_web.RestoreValue('locIMEIMessage')
 locPassword = p_web.RestoreValue('locPassword')
 locMSN = p_web.RestoreValue('locMSN')
 locNetwork = p_web.RestoreValue('locNetwork')
 locInFault = p_web.RestoreValue('locInFault')
 locValidationMessage = p_web.RestoreValue('locValidationMessage')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'ViewJob'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('ReceiptFromPUP_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('ReceiptFromPUP_ChainTo')
    loc:formaction = p_web.GetSessionValue('ReceiptFromPUP_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'ViewJob'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="ReceiptFromPUP" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="ReceiptFromPUP" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="ReceiptFromPUP" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Receipt From PUP') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Receipt From PUP',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_ReceiptFromPUP">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_ReceiptFromPUP" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_ReceiptFromPUP')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('PUP Unit Validation') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_ReceiptFromPUP')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_ReceiptFromPUP'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ReceiptFromPUP_TagValidateLoanAccessories_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ReceiptFromPUP_TagValidateLoanAccessories_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('ReceiptFromPUP_TagValidateLoanAccessories_embedded_div')&''');'
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='NETWORKS'
            p_web.SetValue('SelectField',clip(loc:formname) & '.locInFault')
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locIMEINumber')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_ReceiptFromPUP')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('PUP Unit Validation') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ReceiptFromPUP_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('PUP Unit Validation')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('PUP Unit Validation')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('PUP Unit Validation')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('PUP Unit Validation')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIMEIMessage
      do Value::locIMEIMessage
      do Comment::locIMEIMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPassword
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locPassword
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ReceiptFromPUP_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    If p_web.GSV('Hide:MSN') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locMSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locMSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locMSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locNetwork
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locNetwork
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locNetwork
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locInFault
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locInFault
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locInFault
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ReceiptFromPUP_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'25%'&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::TagValidateLoanAccessories
      do Comment::TagValidateLoanAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonValidateAccessories
      do Comment::buttonValidateAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::locValidationMessage
      do Comment::locValidationMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locIMEINumber  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('I.M.E.I. Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('NewValue'))
    locIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('Value'))
    locIMEINumber = p_web.GetValue('Value')
  End
  If locIMEINumber = ''
    loc:Invalid = 'locIMEINumber'
    loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locIMEINumber = Upper(locIMEINumber)
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
    if (p_web.GSV('locIMEINumber') <> p_web.GSV('job:ESN'))
        IF (BouncerCheck(p_web.GSV('locIMEINumber'),loc:alert))
            p_web.SSV('locIMEINumber','')
        ELSE
            loc:alert = 'IMEI Number does not match IMEI from booking. Enter password to accept change and continue.'
           
  !            p_web.SSV('locIMEIMessage','IMEI Number does not match IMEI from booking. Enter password to accept change and continue.')
            p_web.SSV('PasswordRequired',1)  
        END ! IF
          
  !        p_web.SSV('locIMEIAccepted',0)
      else!
  !        p_web.SSV('locIMEIAccepted',1)
          p_web.SSV('PasswordRequired',0)
          p_web.SSV('locIMEIMessage','')
      end ! if (p_web.GSV('locIMEINumber') <> p_web.GSV('job:ESN'))
  do Value::locIMEINumber
  do SendAlert
  do Comment::locIMEINumber
  do Prompt::locPassword
  do Value::locPassword  !1
  do Comment::locPassword
  do Prompt::locInFault
  do Value::locInFault  !1
  do Prompt::locMSN
  do Value::locMSN  !1
  do Prompt::locNetwork
  do Value::locNetwork  !1
  do Value::locIMEIMessage  !1

Value::locIMEINumber  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locIMEINumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locIMEINumber'',''receiptfrompup_locimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locIMEINumber',p_web.GetSessionValueFormat('locIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locIMEINumber') & '_value')

Comment::locIMEINumber  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:IMEINumber'))
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEINumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locIMEINumber') & '_comment')

Prompt::locIMEIMessage  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEIMessage') & '_prompt',Choose(p_web.GSV('locIMEIMessage') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GSV('locIMEIMessage') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIMEIMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIMEIMessage',p_web.GetValue('NewValue'))
    locIMEIMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIMEIMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locIMEIMessage',p_web.GetValue('Value'))
    locIMEIMessage = p_web.GetValue('Value')
  End

Value::locIMEIMessage  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEIMessage') & '_value',Choose(p_web.GSV('locIMEIMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locIMEIMessage') = '')
  ! --- DISPLAY --- locIMEIMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('RedBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locIMEIMessage'),0) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locIMEIMessage') & '_value')

Comment::locIMEIMessage  Routine
    loc:comment = ''
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locIMEIMessage') & '_comment',Choose(p_web.GSV('locIMEIMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locIMEIMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locPassword  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locPassword') & '_prompt',Choose(p_web.GSV('PasswordRequired') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Enter Password')
  If p_web.GSV('PasswordRequired') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locPassword') & '_prompt')

Validate::locPassword  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPassword',p_web.GetValue('NewValue'))
    locPassword = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPassword
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPassword',p_web.GetValue('Value'))
    locPassword = p_web.GetValue('Value')
  End
  If locPassword = ''
    loc:Invalid = 'locPassword'
    loc:alert = p_web.translate('Enter Password') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locPassword = Upper(locPassword)
    p_web.SetSessionValue('locPassword',locPassword)
  Access:users.Clearkey(use:Password_Key)
  use:Password    = p_web.GSV('locPassword')
  if (Access:users.TryFetch(use:Password_Key) = Level:Benign)
      ! Found
      Access:ACCAREAS.Clearkey(acc:access_Level_Key)
      acc:user_Level    = use:user_Level
      acc:access_Area    = 'PUP RECEIPT - CHANGE IMEI'
      if (Access:ACCAREAS.TryFetch(acc:access_Level_Key) = Level:Benign)
          ! Found
          p_web.SSV('Comment:Password','Password Accepted')
      else ! if (Access:ACCAREAS.TryFetch(acc:access_Level_Key) = Level:Benign)
          ! Error
            loc:alert = 'You do not have access to change the IMEI'
          !p_web.SSV('Comment:Password','You do not have access to change the IMEI')
          p_web.SSV('locPassword','')
      end ! if (Access:ACCAREAS.TryFetch(acc:access_Level_Key) = Level:Benign)
  else ! if (Access:users.TryFetch(use:Password_Key) = Level:Benign)
      ! Error
        loc:alert = 'Invalid Password'
      !p_web.SSV('Comment:Password','Invalid Password')
      p_web.SSV('locPassword','')
  
  end ! if (Access:users.TryFetch(use:Password_Key) = Level:Benign)
  do Value::locPassword
  do SendAlert
  do Value::locIMEINumber  !1
  do Comment::locIMEINumber
  do Prompt::locPassword
  do Comment::locPassword

Value::locPassword  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locPassword') & '_value',Choose(p_web.GSV('PasswordRequired') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('PasswordRequired') <> 1)
  ! --- STRING --- locPassword
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locPassword')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locPassword = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locPassword'',''receiptfrompup_locpassword_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locPassword')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('password','locPassword',p_web.GetSessionValueFormat('locPassword'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locPassword') & '_value')

Comment::locPassword  Routine
    loc:comment = p_web.Translate(p_web.GSV('Comment:Password'))
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locPassword') & '_comment',Choose(p_web.GSV('PasswordRequired') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('PasswordRequired') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locPassword') & '_comment')

Prompt::locMSN  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locMSN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('M.S.N.')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locMSN') & '_prompt')

Validate::locMSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locMSN',p_web.GetValue('NewValue'))
    locMSN = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locMSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locMSN',p_web.GetValue('Value'))
    locMSN = p_web.GetValue('Value')
  End
  If locMSN = ''
    loc:Invalid = 'locMSN'
    loc:alert = p_web.translate('M.S.N.') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locMSN = Upper(locMSN)
    p_web.SetSessionValue('locMSN',locMSN)
  do Value::locMSN
  do SendAlert

Value::locMSN  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locMSN') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locMSN
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locMSN')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locMSN = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locMSN'',''receiptfrompup_locmsn_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locMSN',p_web.GetSessionValueFormat('locMSN'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locMSN') & '_value')

Comment::locMSN  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locMSN') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locNetwork  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locNetwork') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Network')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locNetwork') & '_prompt')

Validate::locNetwork  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locNetwork',p_web.GetValue('NewValue'))
    locNetwork = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locNetwork
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locNetwork',p_web.GetValue('Value'))
    locNetwork = p_web.GetValue('Value')
  End
  If locNetwork = ''
    loc:Invalid = 'locNetwork'
    loc:alert = p_web.translate('Network') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locNetwork = Upper(locNetwork)
    p_web.SetSessionValue('locNetwork',locNetwork)
  p_Web.SetValue('lookupfield','locNetwork')
  do AfterLookup
  do Value::locNetwork
  do SendAlert
  do Comment::locNetwork

Value::locNetwork  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locNetwork') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locNetwork
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locNetwork')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locNetwork = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locNetwork'',''receiptfrompup_locnetwork_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locNetwork')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','locNetwork',p_web.GetSessionValueFormat('locNetwork'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectNetworks')&'?LookupField=locNetwork&Tab=2&ForeignField=net:Network&_sort=net:Network&Refresh=sort&LookupFrom=ReceiptFromPUP&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locNetwork') & '_value')

Comment::locNetwork  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locNetwork') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locNetwork') & '_comment')

Prompt::locInFault  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locInFault') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('In Fault')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locInFault') & '_prompt')

Validate::locInFault  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locInFault',p_web.GetValue('NewValue'))
    locInFault = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locInFault
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locInFault',p_web.GetValue('Value'))
    locInFault = p_web.GetValue('Value')
  End
  If locInFault = ''
    loc:Invalid = 'locInFault'
    loc:alert = p_web.translate('In Fault') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locInFault = Upper(locInFault)
    p_web.SetSessionValue('locInFault',locInFault)
  do Value::locInFault
  do SendAlert

Value::locInFault  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locInFault') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locInFault
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locInFault')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locInFault = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locInFault'',''receiptfrompup_locinfault_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locInFault',p_web.GetSessionValueFormat('locInFault'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
          Access:MANFAULT.Clearkey(maf:InFaultKey)
          maf:manufacturer    = p_web.GSV('job:manufacturer')
          maf:inFault    = 1
          if (Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign)
              ! Found
              packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('BrowseJobFaultCodeLookup')&|
                          '?LookupField=locInFault&Tab=2&ForeignField=mfo:Field&_sort=mfo:Field&Refresh=' & |
                          'sort&LookupFrom=ReceiptFromPUP&' & |
                          'fieldNumber=' & maf:Field_Number & '&partType=&partMainFault='),) !lookupextra
  
          else ! if (Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign)
              ! Error
          end ! if (Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign)
  
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locInFault') & '_value')

Comment::locInFault  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locInFault') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::TagValidateLoanAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('TagValidateLoanAccessories',p_web.GetValue('NewValue'))
    do Value::TagValidateLoanAccessories
  Else
    p_web.StoreValue('acr:Accessory')
  End

Value::TagValidateLoanAccessories  Routine
  loc:extra = ''
  ! --- BROWSE ---  TagValidateLoanAccessories --
  p_web.SetValue('TagValidateLoanAccessories:NoForm',1)
  p_web.SetValue('TagValidateLoanAccessories:FormName',loc:formname)
  p_web.SetValue('TagValidateLoanAccessories:parentIs','Form')
  p_web.SetValue('_parentProc','ReceiptFromPUP')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('ReceiptFromPUP_TagValidateLoanAccessories_embedded_div')&'"><!-- Net:TagValidateLoanAccessories --></div><13,10>'
    p_web._DivHeader('ReceiptFromPUP_' & lower('TagValidateLoanAccessories') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('ReceiptFromPUP_' & lower('TagValidateLoanAccessories') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagValidateLoanAccessories --><13,10>'
  end
  do SendPacket

Comment::TagValidateLoanAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('TagValidateLoanAccessories') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonValidateAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonValidateAccessories',p_web.GetValue('NewValue'))
    do Value::buttonValidateAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  !Validate Accessories
      error# = 0
  
      Access:JOBACC.Clearkey(jac:ref_Number_Key)
      jac:ref_Number    = p_web.GSV('job:Ref_Number')
      set(jac:ref_Number_Key,jac:ref_Number_Key)
      loop
          if (Access:JOBACC.Next())
              Break
          end ! if (Access:JOBACC.Next())
          if (jac:ref_Number    <> p_web.GSV('job:Ref_Number'))
              Break
          end ! if (jac:ref_Number    <> p_web.GSV('job:Ref_Number'))
            Access:Tagging.ClearKey(tgg:SessionIDKey)
            tgg:SessionID = p_web.SessionID
            tgg:TaggedValue = jac:Accessory
            IF (Access:Tagging.TryFetch(tgg:SessionIDKey) = Level:Benign)
                IF (tgg:Tagged <> 1)
                    error# = 1
                    BREAK
                END ! IF
            ELSE ! IF
                error# = 1
                BREAK
            END ! IF
  
      end ! loop
  
      if (error# = 0)
            Access:Tagging.ClearKey(tgg:SessionIDKey)
            tgg:SessionID = p_web.SessionID
            SET(tgg:SessionIDKey,tgg:SessionIDKey)
            LOOP UNTIL Access:Tagging.Next() <> Level:Benign
                IF (tgg:SessionID <> p_web.SessionID)
                    BREAK
                END ! IF
                IF (tgg:Tagged = 0)
                    CYCLE
                END ! IF
                Access:JOBACC.Clearkey(jac:ref_Number_Key)
                jac:ref_Number    = p_web.GSV('job:Ref_Number')
                jac:accessory    = tgg:TaggedValue
                if (Access:JOBACC.TryFetch(jac:ref_Number_Key) = Level:Benign)
                  ! Found
                else ! if (Access:JOBSACC.TryFetch(jac:ref_Number_Key) = Level:Benign)
                  ! Error
                    error# = 1
                    break
                end ! if (Access:JOBSACC.TryFetch(jac:ref_Number_Key) = Level:Benign)    
            END ! LOOP
      end ! if (error# = 0)
  
      case error#
      of 1
          p_web.SSV('locValidationMessage','<span class="RedBold">Accessories Validated. There is a mismatch.</span>')
      else
          p_web.SSV('locValidationMessage','<span class="GreenBold">Accessories Validated</span>')
      end
  do Value::buttonValidateAccessories
  do SendAlert
  do Value::locValidationMessage  !1
  do Value::locValidationMessage  !1

Value::buttonValidateAccessories  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('buttonValidateAccessories') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonValidateAccessories'',''receiptfrompup_buttonvalidateaccessories_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ValidateAccessories','Validate Accessories','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('buttonValidateAccessories') & '_value')

Comment::buttonValidateAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('buttonValidateAccessories') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locValidationMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locValidationMessage',p_web.GetValue('NewValue'))
    locValidationMessage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locValidationMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locValidationMessage',p_web.GetValue('Value'))
    locValidationMessage = p_web.GetValue('Value')
  End

Value::locValidationMessage  Routine
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locValidationMessage') & '_value',Choose(p_web.GSV('locValidationMessage') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locValidationMessage') = '')
  ! --- DISPLAY --- locValidationMessage
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('locValidationMessage'),1) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('ReceiptFromPUP_' & p_web._nocolon('locValidationMessage') & '_value')

Comment::locValidationMessage  Routine
    loc:comment = ''
  p_web._DivHeader('ReceiptFromPUP_' & p_web._nocolon('locValidationMessage') & '_comment',Choose(p_web.GSV('locValidationMessage') = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locValidationMessage') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('ReceiptFromPUP_locIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIMEINumber
      else
        do Value::locIMEINumber
      end
  of lower('ReceiptFromPUP_locPassword_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locPassword
      else
        do Value::locPassword
      end
  of lower('ReceiptFromPUP_locMSN_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locMSN
      else
        do Value::locMSN
      end
  of lower('ReceiptFromPUP_locNetwork_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locNetwork
      else
        do Value::locNetwork
      end
  of lower('ReceiptFromPUP_locInFault_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locInFault
      else
        do Value::locInFault
      end
  of lower('ReceiptFromPUP_buttonValidateAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonValidateAccessories
      else
        do Value::buttonValidateAccessories
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('ReceiptFromPUP_form:ready_',1)
  p_web.SetSessionValue('ReceiptFromPUP_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_ReceiptFromPUP',0)

PreCopy  Routine
  p_web.SetValue('ReceiptFromPUP_form:ready_',1)
  p_web.SetSessionValue('ReceiptFromPUP_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_ReceiptFromPUP',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('ReceiptFromPUP_form:ready_',1)
  p_web.SetSessionValue('ReceiptFromPUP_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('ReceiptFromPUP:Primed',0)

PreDelete       Routine
  p_web.SetValue('ReceiptFromPUP_form:ready_',1)
  p_web.SetSessionValue('ReceiptFromPUP_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('ReceiptFromPUP:Primed',0)
  p_web.setsessionvalue('showtab_ReceiptFromPUP',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('ReceiptFromPUP_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
    ! Validation
    if (p_web.GSV('locValidationMessage') = '')
        loc:Alert = 'You must validate the accessories'
        loc:invalid = 'buttonValidateAccessories'
        exit
    end ! if (p_web.GSV('locValidationMessage') = '')
  p_web.DeleteSessionValue('ReceiptFromPUP_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
    loc:InvalidTab += 1
        If locIMEINumber = ''
          loc:Invalid = 'locIMEINumber'
          loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locIMEINumber = Upper(locIMEINumber)
          p_web.SetSessionValue('locIMEINumber',locIMEINumber)
        If loc:Invalid <> '' then exit.
      If not (p_web.GSV('PasswordRequired') <> 1)
        If locPassword = ''
          loc:Invalid = 'locPassword'
          loc:alert = p_web.translate('Enter Password') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locPassword = Upper(locPassword)
          p_web.SetSessionValue('locPassword',locPassword)
        If loc:Invalid <> '' then exit.
      End
  ! tab = 1
    loc:InvalidTab += 1
    If p_web.GSV('Hide:MSN') <> 1
        If locMSN = ''
          loc:Invalid = 'locMSN'
          loc:alert = p_web.translate('M.S.N.') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locMSN = Upper(locMSN)
          p_web.SetSessionValue('locMSN',locMSN)
        If loc:Invalid <> '' then exit.
    End
        If locNetwork = ''
          loc:Invalid = 'locNetwork'
          loc:alert = p_web.translate('Network') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locNetwork = Upper(locNetwork)
          p_web.SetSessionValue('locNetwork',locNetwork)
        If loc:Invalid <> '' then exit.
        If locInFault = ''
          loc:Invalid = 'locInFault'
          loc:alert = p_web.translate('In Fault') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locInFault = Upper(locInFault)
          p_web.SetSessionValue('locInFault',locInFault)
        If loc:Invalid <> '' then exit.
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('ReceiptFromPUP:Primed',0)
  p_web.StoreValue('locIMEINumber')
  p_web.StoreValue('locIMEIMessage')
  p_web.StoreValue('locPassword')
  p_web.StoreValue('locMSN')
  p_web.StoreValue('locNetwork')
  p_web.StoreValue('locInFault')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locValidationMessage')
  !Post Update
  do saveChanges


  do deleteVariables
BouncerCheck        PROCEDURE(STRING pIMEI, *STRING pError)!,LONG 
liveBouncer             LONG
bouncers                 LONG
RetValue                LONG(Level:Benign)
    CODE
        IF (pIMEI = '')
            RETURN Level:Benign
        END ! IF
        
        Access:JOBS_ALIAS.ClearKey(job_ali:ESN_Key)
        job_ali:ESN = pIMEI
        SET(job_ali:ESN_Key,job_ali:ESN_Key)
        LOOP UNTIL Access:JOBS_ALIAS.Next() <> Level:Benign
            IF (job_ali:ESN <> pIMEI)
                BREAK
            END ! IF
            IF (job_ali:Cancelled = 'YES')
                CYCLE
            END ! IF
            IF (job_ali:Date_Completed = '')
                liveBouncer += 1
            END ! IF
            bouncers += 1
        END ! LOOP

        Access:DEFAULTS.ClearKey(def:RecordNumberKey)
        def:record_number = 1
        SET(def:RecordNumberKey,def:RecordNumberKey)
        Access:DEFAULTS.Next()

        IF (def:Allow_Bouncer <> 'YES')
            IF (bouncers > 0)
                pError = 'IMEI Number has been previously entered within the default period.' 
                RetValue =  Level:Fatal
            END ! IF
        ELSE ! IF (def:Allow_Bouncer <> 'YES')
            IF (GETINI('BOUNCER','StopLive',,Clip(Path()) & '\SB2KDEF.INI') = 1)
                IF (liveBouncer > 0)
                    pError = 'IMEI Number already exists on a Live Job.'
                    RetValue =  Level:Fatal
                END ! IF (liveBouncer > 0)
            END ! IF (GETINI('BOUNCER','StopLive',,Clip(Path()) & '\SB2KDEF.INI') = 1)
        END ! IF (def:Allow_Bouncer <> 'YES')

        RETURN RetValue
        
