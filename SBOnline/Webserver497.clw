

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER497.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
MQProcess            PROCEDURE  (String f:IMEINumber,*String f:POP,*Date f:DOP,*Byte f:Error,*String f:ErrorMessage,String f:OneYearWarranty,*Byte f:SpecificNeeds)!,Byte ! Declare Procedure
    include('MQProcess.inc','Local Data')

  CODE
        include('MQProcess.inc','Processed Code')
