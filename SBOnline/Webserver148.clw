

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER148.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
RemoveFromStockAllocation PROCEDURE  (func:PartRecordNumber,func:PartType) ! Declare Procedure
save_sto_id          USHORT,AUTO                           !
save_par_id          USHORT,AUTO                           !
save_wpr_id          USHORT,AUTO                           !
save_epr_id          USHORT,AUTO                           !
save_tra_id          USHORT,AUTO                           !
tmp:CurrentAccount   STRING(30)                            !Current Account Number
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
STOCKALL::State  USHORT
FilesOpened     BYTE(0)

  CODE
    !Part Record Number
    !Part Type
    !Quantity

    DO openfiles
    DO Savefiles
    Access:STOCKALL.ClearKey(stl:PartRecordTypeKey)
    stl:PartType         = func:PartType
    stl:PartRecordNumber = func:PartRecordNumber
    If Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign
        !Found
        Relate:STOCKALL.Delete(0)
    Else !If Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign
        !Error
    End !If Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign

    DO restorefiles
    DO closefiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:STOCKALL.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCKALL.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:STOCKALL.Close
     FilesOpened = False
  END
SaveFiles  ROUTINE
  STOCKALL::State = Access:STOCKALL.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF STOCKALL::State <> 0
    Access:STOCKALL.RestoreFile(STOCKALL::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
