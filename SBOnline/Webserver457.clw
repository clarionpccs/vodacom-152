

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER457.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER456.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER458.INC'),ONCE        !Req'd for module callout resolution
                     END


FormStockControl     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
loc0_7               LONG                                  !
loc0_30              LONG                                  !
loc31_60             LONG                                  !
loc61_90             LONG                                  !
locAverageValue      LONG                                  !
locAverageText       STRING(30)                            !
FilesOpened     Long
LOCSHELF::State  USHORT
STOCK::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormStockControl')
  loc:formname = 'FormStockControl_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormStockControl',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormStockControl','Change')
    p_web._DivHeader('FormStockControl',clip('fdiv') & ' ' & clip('FormContent'))
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormStockControl',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockControl',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockControl',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormStockControl',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If loc:act = InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormStockControl',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormStockControl',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormStockControl',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(LOCSHELF)
  p_web._OpenFile(STOCK)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(LOCSHELF)
  p_Web._CloseFile(STOCK)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  VodacomClass.StockUsage(p_web.GSV('sto:Ref_Number'),loc0_7,loc0_30, |
      loc31_60,loc61_90,locAverageValue)
  
  If locAverageValue < 1
      locAverageText = '< 1'
  Else!If average_temp < 1
      locAverageText = Int(locAverageValue)
  End!If average_temp < 1    
  
  p_web.SSV('loc0_7',loc0_7)
  p_web.SSV('loc0_30',loc0_30)
  p_web.SSV('loc31_60',loc31_60)
  p_web.SSV('loc61_90',loc61_90)
  p_web.SSV('locAverageText',locAverageText)
  p_web.SetValue('FormStockControl_form:inited_',1)
  p_web.SetValue('UpdateFile','STOCK')
  p_web.SetValue('UpdateKey','sto:Ref_Number_Key')
  p_web.SetValue('IDField','sto:Ref_Number')
  do RestoreMem

CancelForm  Routine
  IF p_web.GetSessionValue('FormStockControl:Primed') = 1
    p_web._deleteFile(STOCK)
    p_web.SetSessionValue('FormStockControl:Primed',0)
  End

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','STOCK')
  p_web.SetValue('UpdateKey','sto:Ref_Number_Key')
  If p_web.IfExistsValue('sto:DateBooked')
    p_web.SetPicture('sto:DateBooked','@d6')
  End
  p_web.SetSessionPicture('sto:DateBooked','@d6')
  If p_web.IfExistsValue('sto:Part_Number')
    p_web.SetPicture('sto:Part_Number','@s30')
  End
  p_web.SetSessionPicture('sto:Part_Number','@s30')
  If p_web.IfExistsValue('sto:Location')
    p_web.SetPicture('sto:Location','@s30')
  End
  p_web.SetSessionPicture('sto:Location','@s30')
  If p_web.IfExistsValue('sto:Description')
    p_web.SetPicture('sto:Description','@s30')
  End
  p_web.SetSessionPicture('sto:Description','@s30')
  If p_web.IfExistsValue('sto:Shelf_Location')
    p_web.SetPicture('sto:Shelf_Location','@s30')
  End
  p_web.SetSessionPicture('sto:Shelf_Location','@s30')
  If p_web.IfExistsValue('sto:Supplier')
    p_web.SetPicture('sto:Supplier','@s30')
  End
  p_web.SetSessionPicture('sto:Supplier','@s30')
  If p_web.IfExistsValue('sto:Second_Location')
    p_web.SetPicture('sto:Second_Location','@s30')
  End
  p_web.SetSessionPicture('sto:Second_Location','@s30')
  If p_web.IfExistsValue('sto:AveragePurchaseCost')
    p_web.SetPicture('sto:AveragePurchaseCost','@n14.2')
  End
  p_web.SetSessionPicture('sto:AveragePurchaseCost','@n14.2')
  If p_web.IfExistsValue('sto:Percentage_Mark_Up')
    p_web.SetPicture('sto:Percentage_Mark_Up','@n6.2')
  End
  p_web.SetSessionPicture('sto:Percentage_Mark_Up','@n6.2')
  If p_web.IfExistsValue('sto:Purchase_Cost')
    p_web.SetPicture('sto:Purchase_Cost','@n14.2')
  End
  p_web.SetSessionPicture('sto:Purchase_Cost','@n14.2')
  If p_web.IfExistsValue('sto:PurchaseMarkUp')
    p_web.SetPicture('sto:PurchaseMarkUp','@n14.2')
  End
  p_web.SetSessionPicture('sto:PurchaseMarkUp','@n14.2')
  If p_web.IfExistsValue('sto:Sale_Cost')
    p_web.SetPicture('sto:Sale_Cost','@n14.2')
  End
  p_web.SetSessionPicture('sto:Sale_Cost','@n14.2')
  If p_web.IfExistsValue('sto:Quantity_Stock')
    p_web.SetPicture('sto:Quantity_Stock','@N8')
  End
  p_web.SetSessionPicture('sto:Quantity_Stock','@N8')
  If p_web.IfExistsValue('sto:ExchangeOrderCap')
    p_web.SetPicture('sto:ExchangeOrderCap','@n-14')
  End
  p_web.SetSessionPicture('sto:ExchangeOrderCap','@n-14')
  If p_web.IfExistsValue('sto:RepairLevel')
    p_web.SetPicture('sto:RepairLevel','@n8')
  End
  p_web.SetSessionPicture('sto:RepairLevel','@n8')
  If p_web.IfExistsValue('sto:SkillLevel')
    p_web.SetPicture('sto:SkillLevel','@n8')
  End
  p_web.SetSessionPicture('sto:SkillLevel','@n8')
  If p_web.IfExistsValue('sto:Manufacturer')
    p_web.SetPicture('sto:Manufacturer','@s30')
  End
  p_web.SetSessionPicture('sto:Manufacturer','@s30')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'sto:Shelf_Location'
    p_web.setsessionvalue('showtab_FormStockControl',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(LOCSHELF)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.sto:Supplier')
  End
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('loc0_7',loc0_7)
  p_web.SetSessionValue('loc0_30',loc0_30)
  p_web.SetSessionValue('loc31_60',loc31_60)
  p_web.SetSessionValue('loc61_90',loc61_90)
  p_web.SetSessionValue('locAverageText',locAverageText)

RestoreMem       Routine
  !FormSource=File
  if p_web.IfExistsValue('loc0_7')
    loc0_7 = p_web.GetValue('loc0_7')
    p_web.SetSessionValue('loc0_7',loc0_7)
  End
  if p_web.IfExistsValue('loc0_30')
    loc0_30 = p_web.GetValue('loc0_30')
    p_web.SetSessionValue('loc0_30',loc0_30)
  End
  if p_web.IfExistsValue('loc31_60')
    loc31_60 = p_web.GetValue('loc31_60')
    p_web.SetSessionValue('loc31_60',loc31_60)
  End
  if p_web.IfExistsValue('loc61_90')
    loc61_90 = p_web.GetValue('loc61_90')
    p_web.SetSessionValue('loc61_90',loc61_90)
  End
  if p_web.IfExistsValue('locAverageText')
    locAverageText = p_web.GetValue('locAverageText')
    p_web.SetSessionValue('locAverageText',locAverageText)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormStockControl_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 loc0_7 = p_web.RestoreValue('loc0_7')
 loc0_30 = p_web.RestoreValue('loc0_30')
 loc31_60 = p_web.RestoreValue('loc31_60')
 loc61_90 = p_web.RestoreValue('loc61_90')
 locAverageText = p_web.RestoreValue('locAverageText')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormStockControl')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormStockControl_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormStockControl_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormStockControl_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = loc:FormAction
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  packet = clip(packet) & '<input type="hidden" name="STOCK__FileAction" value="'&p_web.getSessionValue('STOCK:FileAction')&'" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="file" value="STOCK" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateFile" value="STOCK" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateKey" value="sto:Ref_Number_Key" ></input><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormStockControl" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormStockControl" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormStockControl" ></input><13,10>'
  end

  do SendPacket
  packet = clip(packet) & p_web.CreateInput('hidden','sto:Ref_Number',p_web._jsok(p_web.getSessionValue('sto:Ref_Number'))) & '<13,10>'
  If p_web.Translate('Edit Stock') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Edit Stock',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormStockControl">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormStockControl" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormStockControl')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Cost / Qty / Usage Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Part Defaults') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Models This Part Can Be Used For') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormStockControl')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormStockControl'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormStockControl_FormStockModels_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormStockControl')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Part Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormStockControl_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:DateBooked
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:DateBooked
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Part_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Part_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Location
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Location
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Description
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Shelf_Location
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Shelf_Location
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Supplier
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Supplier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Second_Location
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Second_Location
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Accessory
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Accessory
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnAmendLocations
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Cost / Qty / Usage Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormStockControl_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Cost / Qty / Usage Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Cost / Qty / Usage Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Cost / Qty / Usage Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Cost / Qty / Usage Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:AveragePurchaseCost
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:AveragePurchaseCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::txtUsage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtUsage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Percentage_Mark_Up
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Percentage_Mark_Up
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::loc0_7
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::loc0_7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Purchase_Cost
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Purchase_Cost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::loc0_30
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::loc0_30
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:PurchaseMarkUp
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:PurchaseMarkUp
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::loc31_60
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::loc31_60
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Sale_Cost
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Sale_Cost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::loc61_90
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::loc61_90
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Quantity_Stock
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Quantity_Stock
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAverageText
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAverageText
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Part Defaults') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormStockControl_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Defaults')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Part Defaults')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Defaults')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Part Defaults')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Sundry_Item
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Sundry_Item
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Suspend
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:Suspend
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:ExchangeUnit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:ExchangeUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:ExchangeOrderCap
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:ExchangeOrderCap
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:AllowDuplicate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:AllowDuplicate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:ReturnFaultySpare
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:ReturnFaultySpare
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:ChargeablePartOnly
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:ChargeablePartOnly
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:AttachBySolder
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:AttachBySolder
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:E1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:E1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:E2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:E2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:E3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:E3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:RepairLevel
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:RepairLevel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:SkillLevel
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::sto:SkillLevel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Models This Part Can Be Used For') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormStockControl_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Models This Part Can Be Used For')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Models This Part Can Be Used For')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Models This Part Can Be Used For')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Models This Part Can Be Used For')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::sto:Manufacturer
      do Value::sto:Manufacturer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::FormStockModels
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::sto:DateBooked  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:DateBooked') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Date Created')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:DateBooked  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:DateBooked',p_web.GetValue('NewValue'))
    sto:DateBooked = p_web.GetValue('NewValue') !FieldType= DATE Field = sto:DateBooked
    do Value::sto:DateBooked
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:DateBooked',p_web.dFormat(p_web.GetValue('Value'),'@d6'))
    sto:DateBooked = p_web.GetValue('Value')
  End

Value::sto:DateBooked  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:DateBooked') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:DateBooked
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:DateBooked'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::sto:Part_Number  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Part_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Part Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Part_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Part_Number',p_web.GetValue('NewValue'))
    sto:Part_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = sto:Part_Number
    do Value::sto:Part_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Part_Number',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sto:Part_Number = p_web.GetValue('Value')
  End

Value::sto:Part_Number  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Part_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:Part_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Part_Number'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::sto:Location  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Location') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Location')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Location  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Location',p_web.GetValue('NewValue'))
    sto:Location = p_web.GetValue('NewValue') !FieldType= STRING Field = sto:Location
    do Value::sto:Location
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Location',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sto:Location = p_web.GetValue('Value')
  End

Value::sto:Location  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Location') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:Location
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Location'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::sto:Description  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Description') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Description')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Description  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Description',p_web.GetValue('NewValue'))
    sto:Description = p_web.GetValue('NewValue') !FieldType= STRING Field = sto:Description
    do Value::sto:Description
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Description',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sto:Description = p_web.GetValue('Value')
  End

Value::sto:Description  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Description') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:Description
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Description'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::sto:Shelf_Location  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Shelf_Location') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Shelf Location')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Shelf_Location  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Shelf_Location',p_web.GetValue('NewValue'))
    sto:Shelf_Location = p_web.GetValue('NewValue') !FieldType= STRING Field = sto:Shelf_Location
    do Value::sto:Shelf_Location
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Shelf_Location',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sto:Shelf_Location = p_web.GetValue('Value')
  End

Value::sto:Shelf_Location  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Shelf_Location') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:Shelf_Location
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Shelf_Location'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::sto:Supplier  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Supplier') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Supplier')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Supplier  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Supplier',p_web.GetValue('NewValue'))
    sto:Supplier = p_web.GetValue('NewValue') !FieldType= STRING Field = sto:Supplier
    do Value::sto:Supplier
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Supplier',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sto:Supplier = p_web.GetValue('Value')
  End

Value::sto:Supplier  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Supplier') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:Supplier
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Supplier'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::sto:Second_Location  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Second_Location') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('2nd Location')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Second_Location  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Second_Location',p_web.GetValue('NewValue'))
    sto:Second_Location = p_web.GetValue('NewValue') !FieldType= STRING Field = sto:Second_Location
    do Value::sto:Second_Location
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Second_Location',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sto:Second_Location = p_web.GetValue('Value')
  End

Value::sto:Second_Location  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Second_Location') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:Second_Location
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Second_Location'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::sto:Accessory  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Accessory') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Accessory')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Accessory  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Accessory',p_web.GetValue('NewValue'))
    sto:Accessory = p_web.GetValue('NewValue') !FieldType= STRING Field = sto:Accessory
    do Value::sto:Accessory
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    p_web.SetSessionValue('sto:Accessory',p_web.GetValue('Value'))
    sto:Accessory = p_web.GetValue('Value')
  End
  do Value::sto:Accessory
  do SendAlert

Value::sto:Accessory  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Accessory') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- sto:Accessory
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sto:Accessory'',''formstockcontrol_sto:accessory_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('sto:Accessory') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sto:Accessory',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockControl_' & p_web._nocolon('sto:Accessory') & '_value')


Validate::btnAmendLocations  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnAmendLocations',p_web.GetValue('NewValue'))
    do Value::btnAmendLocations
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnAmendLocations  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('btnAmendLocations') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnAmendLocations','Amend Locations','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormUpdateStockLocations')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Prompt::sto:AveragePurchaseCost  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:AveragePurchaseCost') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Average Purchase Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:AveragePurchaseCost  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:AveragePurchaseCost',p_web.GetValue('NewValue'))
    sto:AveragePurchaseCost = p_web.GetValue('NewValue') !FieldType= REAL Field = sto:AveragePurchaseCost
    do Value::sto:AveragePurchaseCost
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:AveragePurchaseCost',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    sto:AveragePurchaseCost = p_web.GetValue('Value')
  End

Value::sto:AveragePurchaseCost  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:AveragePurchaseCost') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:AveragePurchaseCost
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:AveragePurchaseCost'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::txtUsage  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('txtUsage') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Usage')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::txtUsage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtUsage',p_web.GetValue('NewValue'))
    do Value::txtUsage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtUsage  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('txtUsage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::sto:Percentage_Mark_Up  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Percentage_Mark_Up') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Percentage Mark Up')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Percentage_Mark_Up  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Percentage_Mark_Up',p_web.GetValue('NewValue'))
    sto:Percentage_Mark_Up = p_web.GetValue('NewValue') !FieldType= REAL Field = sto:Percentage_Mark_Up
    do Value::sto:Percentage_Mark_Up
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Percentage_Mark_Up',p_web.dFormat(p_web.GetValue('Value'),'@n6.2'))
    sto:Percentage_Mark_Up = p_web.GetValue('Value')
  End

Value::sto:Percentage_Mark_Up  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Percentage_Mark_Up') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:Percentage_Mark_Up
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Percentage_Mark_Up'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::loc0_7  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('loc0_7') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('0 - 7 Days')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::loc0_7  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('loc0_7',p_web.GetValue('NewValue'))
    loc0_7 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::loc0_7
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('loc0_7',p_web.GetValue('Value'))
    loc0_7 = p_web.GetValue('Value')
  End

Value::loc0_7  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('loc0_7') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- loc0_7
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('loc0_7'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::sto:Purchase_Cost  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Purchase_Cost') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('In Warranty Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Purchase_Cost  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Purchase_Cost',p_web.GetValue('NewValue'))
    sto:Purchase_Cost = p_web.GetValue('NewValue') !FieldType= REAL Field = sto:Purchase_Cost
    do Value::sto:Purchase_Cost
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Purchase_Cost',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    sto:Purchase_Cost = p_web.GetValue('Value')
  End

Value::sto:Purchase_Cost  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Purchase_Cost') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:Purchase_Cost
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Purchase_Cost'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::loc0_30  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('loc0_30') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('0 - 30 Days')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::loc0_30  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('loc0_30',p_web.GetValue('NewValue'))
    loc0_30 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::loc0_30
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('loc0_30',p_web.GetValue('Value'))
    loc0_30 = p_web.GetValue('Value')
  End

Value::loc0_30  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('loc0_30') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- loc0_30
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('loc0_30'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::sto:PurchaseMarkUp  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:PurchaseMarkUp') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Percentage Mark Up')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:PurchaseMarkUp  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:PurchaseMarkUp',p_web.GetValue('NewValue'))
    sto:PurchaseMarkUp = p_web.GetValue('NewValue') !FieldType= REAL Field = sto:PurchaseMarkUp
    do Value::sto:PurchaseMarkUp
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:PurchaseMarkUp',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    sto:PurchaseMarkUp = p_web.GetValue('Value')
  End

Value::sto:PurchaseMarkUp  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:PurchaseMarkUp') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:PurchaseMarkUp
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:PurchaseMarkUp'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::loc31_60  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('loc31_60') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('31 - 60 Days')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::loc31_60  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('loc31_60',p_web.GetValue('NewValue'))
    loc31_60 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::loc31_60
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('loc31_60',p_web.GetValue('Value'))
    loc31_60 = p_web.GetValue('Value')
  End

Value::loc31_60  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('loc31_60') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- loc31_60
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('loc31_60'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::sto:Sale_Cost  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Sale_Cost') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Out Warranty Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Sale_Cost  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Sale_Cost',p_web.GetValue('NewValue'))
    sto:Sale_Cost = p_web.GetValue('NewValue') !FieldType= REAL Field = sto:Sale_Cost
    do Value::sto:Sale_Cost
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Sale_Cost',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    sto:Sale_Cost = p_web.GetValue('Value')
  End

Value::sto:Sale_Cost  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Sale_Cost') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:Sale_Cost
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Sale_Cost'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::loc61_90  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('loc61_90') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('61 - 90 Days')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::loc61_90  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('loc61_90',p_web.GetValue('NewValue'))
    loc61_90 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::loc61_90
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('loc61_90',p_web.GetValue('Value'))
    loc61_90 = p_web.GetValue('Value')
  End

Value::loc61_90  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('loc61_90') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- loc61_90
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('loc61_90'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::sto:Quantity_Stock  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Quantity_Stock') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Quantity In Stock')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Quantity_Stock  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Quantity_Stock',p_web.GetValue('NewValue'))
    sto:Quantity_Stock = p_web.GetValue('NewValue') !FieldType= REAL Field = sto:Quantity_Stock
    do Value::sto:Quantity_Stock
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Quantity_Stock',p_web.dFormat(p_web.GetValue('Value'),'@N8'))
    sto:Quantity_Stock = p_web.GetValue('Value')
  End

Value::sto:Quantity_Stock  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Quantity_Stock') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:Quantity_Stock
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Quantity_Stock'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locAverageText  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('locAverageText') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Average Daily Use')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAverageText  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAverageText',p_web.GetValue('NewValue'))
    locAverageText = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAverageText
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locAverageText',p_web.GetValue('Value'))
    locAverageText = p_web.GetValue('Value')
  End

Value::locAverageText  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('locAverageText') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locAverageText
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locAverageText'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::sto:Sundry_Item  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Sundry_Item') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Sundry Item')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Sundry_Item  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Sundry_Item',p_web.GetValue('NewValue'))
    sto:Sundry_Item = p_web.GetValue('NewValue') !FieldType= STRING Field = sto:Sundry_Item
    do Value::sto:Sundry_Item
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    p_web.SetSessionValue('sto:Sundry_Item',p_web.GetValue('Value'))
    sto:Sundry_Item = p_web.GetValue('Value')
  End
  do Value::sto:Sundry_Item
  do SendAlert

Value::sto:Sundry_Item  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Sundry_Item') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- sto:Sundry_Item
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sto:Sundry_Item'',''formstockcontrol_sto:sundry_item_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('sto:Sundry_Item') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sto:Sundry_Item',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockControl_' & p_web._nocolon('sto:Sundry_Item') & '_value')


Prompt::sto:Suspend  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Suspend') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Suspend Part')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Suspend  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Suspend',p_web.GetValue('NewValue'))
    sto:Suspend = p_web.GetValue('NewValue') !FieldType= BYTE Field = sto:Suspend
    do Value::sto:Suspend
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('sto:Suspend',p_web.GetValue('Value'))
    sto:Suspend = p_web.GetValue('Value')
  End
  do Value::sto:Suspend
  do SendAlert

Value::sto:Suspend  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Suspend') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- sto:Suspend
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sto:Suspend'',''formstockcontrol_sto:suspend_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('sto:Suspend') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sto:Suspend',clip(1),,loc:readonly,,,loc:javascript,,'Suspend Part') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockControl_' & p_web._nocolon('sto:Suspend') & '_value')


Prompt::sto:ExchangeUnit  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:ExchangeUnit') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Exchange Unit')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:ExchangeUnit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:ExchangeUnit',p_web.GetValue('NewValue'))
    sto:ExchangeUnit = p_web.GetValue('NewValue') !FieldType= STRING Field = sto:ExchangeUnit
    do Value::sto:ExchangeUnit
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value','NO')
    end
    p_web.SetSessionValue('sto:ExchangeUnit',p_web.GetValue('Value'))
    sto:ExchangeUnit = p_web.GetValue('Value')
  End
  do Value::sto:ExchangeUnit
  do SendAlert

Value::sto:ExchangeUnit  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:ExchangeUnit') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- sto:ExchangeUnit
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sto:ExchangeUnit'',''formstockcontrol_sto:exchangeunit_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('sto:ExchangeUnit') = 'YES'
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sto:ExchangeUnit',clip('YES'),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockControl_' & p_web._nocolon('sto:ExchangeUnit') & '_value')


Prompt::sto:ExchangeOrderCap  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:ExchangeOrderCap') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Exchange Order Cap')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:ExchangeOrderCap  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:ExchangeOrderCap',p_web.GetValue('NewValue'))
    sto:ExchangeOrderCap = p_web.GetValue('NewValue') !FieldType= LONG Field = sto:ExchangeOrderCap
    do Value::sto:ExchangeOrderCap
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:ExchangeOrderCap',p_web.dFormat(p_web.GetValue('Value'),'@n-14'))
    sto:ExchangeOrderCap = p_web.GetValue('Value')
  End

Value::sto:ExchangeOrderCap  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:ExchangeOrderCap') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:ExchangeOrderCap
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('sto:ExchangeOrderCap'),) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::sto:AllowDuplicate  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:AllowDuplicate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Allow Duplicate Part On Jobs')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:AllowDuplicate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:AllowDuplicate',p_web.GetValue('NewValue'))
    sto:AllowDuplicate = p_web.GetValue('NewValue') !FieldType= BYTE Field = sto:AllowDuplicate
    do Value::sto:AllowDuplicate
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('sto:AllowDuplicate',p_web.GetValue('Value'))
    sto:AllowDuplicate = p_web.GetValue('Value')
  End
  do Value::sto:AllowDuplicate
  do SendAlert

Value::sto:AllowDuplicate  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:AllowDuplicate') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- sto:AllowDuplicate
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sto:AllowDuplicate'',''formstockcontrol_sto:allowduplicate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('sto:AllowDuplicate') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sto:AllowDuplicate',clip(1),,loc:readonly,,,loc:javascript,,'<<tab.') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockControl_' & p_web._nocolon('sto:AllowDuplicate') & '_value')


Prompt::sto:ReturnFaultySpare  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:ReturnFaultySpare') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Return Faulty Spare')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:ReturnFaultySpare  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:ReturnFaultySpare',p_web.GetValue('NewValue'))
    sto:ReturnFaultySpare = p_web.GetValue('NewValue') !FieldType= BYTE Field = sto:ReturnFaultySpare
    do Value::sto:ReturnFaultySpare
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('sto:ReturnFaultySpare',p_web.GetValue('Value'))
    sto:ReturnFaultySpare = p_web.GetValue('Value')
  End
  do Value::sto:ReturnFaultySpare
  do SendAlert

Value::sto:ReturnFaultySpare  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:ReturnFaultySpare') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- sto:ReturnFaultySpare
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sto:ReturnFaultySpare'',''formstockcontrol_sto:returnfaultyspare_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('sto:ReturnFaultySpare') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sto:ReturnFaultySpare',clip(1),,loc:readonly,,,loc:javascript,,'Return Faulty Spare') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockControl_' & p_web._nocolon('sto:ReturnFaultySpare') & '_value')


Prompt::sto:ChargeablePartOnly  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:ChargeablePartOnly') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Chargeable Part Only')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:ChargeablePartOnly  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:ChargeablePartOnly',p_web.GetValue('NewValue'))
    sto:ChargeablePartOnly = p_web.GetValue('NewValue') !FieldType= BYTE Field = sto:ChargeablePartOnly
    do Value::sto:ChargeablePartOnly
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('sto:ChargeablePartOnly',p_web.GetValue('Value'))
    sto:ChargeablePartOnly = p_web.GetValue('Value')
  End
  do Value::sto:ChargeablePartOnly
  do SendAlert

Value::sto:ChargeablePartOnly  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:ChargeablePartOnly') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- sto:ChargeablePartOnly
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sto:ChargeablePartOnly'',''formstockcontrol_sto:chargeablepartonly_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('sto:ChargeablePartOnly') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sto:ChargeablePartOnly',clip(1),,loc:readonly,,,loc:javascript,,'Chargeable Part Only') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockControl_' & p_web._nocolon('sto:ChargeablePartOnly') & '_value')


Prompt::sto:AttachBySolder  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:AttachBySolder') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Attached By Solder')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:AttachBySolder  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:AttachBySolder',p_web.GetValue('NewValue'))
    sto:AttachBySolder = p_web.GetValue('NewValue') !FieldType= BYTE Field = sto:AttachBySolder
    do Value::sto:AttachBySolder
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('sto:AttachBySolder',p_web.GetValue('Value'))
    sto:AttachBySolder = p_web.GetValue('Value')
  End
  do Value::sto:AttachBySolder
  do SendAlert

Value::sto:AttachBySolder  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:AttachBySolder') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- sto:AttachBySolder
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sto:AttachBySolder'',''formstockcontrol_sto:attachbysolder_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('sto:AttachBySolder') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sto:AttachBySolder',clip(1),,loc:readonly,,,loc:javascript,,'Attach By Solder') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockControl_' & p_web._nocolon('sto:AttachBySolder') & '_value')


Prompt::sto:E1  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:E1') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Access Level 1')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:E1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:E1',p_web.GetValue('NewValue'))
    sto:E1 = p_web.GetValue('NewValue') !FieldType= BYTE Field = sto:E1
    do Value::sto:E1
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('sto:E1',p_web.GetValue('Value'))
    sto:E1 = p_web.GetValue('Value')
  End
  do Value::sto:E1
  do SendAlert

Value::sto:E1  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:E1') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- sto:E1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sto:E1'',''formstockcontrol_sto:e1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('sto:E1') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sto:E1',clip(1),,loc:readonly,,,loc:javascript,,'E1') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockControl_' & p_web._nocolon('sto:E1') & '_value')


Prompt::sto:E2  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:E2') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Access Level 2')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:E2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:E2',p_web.GetValue('NewValue'))
    sto:E2 = p_web.GetValue('NewValue') !FieldType= BYTE Field = sto:E2
    do Value::sto:E2
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('sto:E2',p_web.GetValue('Value'))
    sto:E2 = p_web.GetValue('Value')
  End
  do Value::sto:E2
  do SendAlert

Value::sto:E2  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:E2') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- sto:E2
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sto:E2'',''formstockcontrol_sto:e2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('sto:E2') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sto:E2',clip(1),,loc:readonly,,,loc:javascript,,'E2') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockControl_' & p_web._nocolon('sto:E2') & '_value')


Prompt::sto:E3  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:E3') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Access Level 3')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:E3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:E3',p_web.GetValue('NewValue'))
    sto:E3 = p_web.GetValue('NewValue') !FieldType= BYTE Field = sto:E3
    do Value::sto:E3
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('sto:E3',p_web.GetValue('Value'))
    sto:E3 = p_web.GetValue('Value')
  End
  do Value::sto:E3
  do SendAlert

Value::sto:E3  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:E3') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- sto:E3
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''sto:E3'',''formstockcontrol_sto:e3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'disabled'
  If p_web.GetSessionValue('sto:E3') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','sto:E3',clip(1),,loc:readonly,,,loc:javascript,,'E3') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormStockControl_' & p_web._nocolon('sto:E3') & '_value')


Prompt::sto:RepairLevel  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:RepairLevel') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Repair Index')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:RepairLevel  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:RepairLevel',p_web.GetValue('NewValue'))
    sto:RepairLevel = p_web.GetValue('NewValue') !FieldType= LONG Field = sto:RepairLevel
    do Value::sto:RepairLevel
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:RepairLevel',p_web.dFormat(p_web.GetValue('Value'),'@n8'))
    sto:RepairLevel = p_web.GetValue('Value')
  End

Value::sto:RepairLevel  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:RepairLevel') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:RepairLevel
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:RepairLevel'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::sto:SkillLevel  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:SkillLevel') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Skill Level')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:SkillLevel  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:SkillLevel',p_web.GetValue('NewValue'))
    sto:SkillLevel = p_web.GetValue('NewValue') !FieldType= LONG Field = sto:SkillLevel
    do Value::sto:SkillLevel
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:SkillLevel',p_web.dFormat(p_web.GetValue('Value'),'@n8'))
    sto:SkillLevel = p_web.GetValue('Value')
  End

Value::sto:SkillLevel  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:SkillLevel') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:SkillLevel
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:SkillLevel'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::sto:Manufacturer  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Manufacturer') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Manufacturer')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::sto:Manufacturer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('sto:Manufacturer',p_web.GetValue('NewValue'))
    sto:Manufacturer = p_web.GetValue('NewValue') !FieldType= STRING Field = sto:Manufacturer
    do Value::sto:Manufacturer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('sto:Manufacturer',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    sto:Manufacturer = p_web.GetValue('Value')
  End

Value::sto:Manufacturer  Routine
  p_web._DivHeader('FormStockControl_' & p_web._nocolon('sto:Manufacturer') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- sto:Manufacturer
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('sto:Manufacturer'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::FormStockModels  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('FormStockModels',p_web.GetValue('NewValue'))
    do Value::FormStockModels
  Else
    p_web.StoreValue('stm:RecordNumber')
  End

Value::FormStockModels  Routine
  loc:extra = ''
  ! --- BROWSE ---  FormStockModels --
  p_web.SetValue('FormStockModels:NoForm',1)
  p_web.SetValue('FormStockModels:FormName',loc:formname)
  p_web.SetValue('FormStockModels:parentIs','Form')
  p_web.SetValue('_parentProc','FormStockControl')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormStockControl_FormStockModels_embedded_div')&'"><!-- Net:FormStockModels --></div><13,10>'
    p_web._DivHeader('FormStockControl_' & lower('FormStockModels') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormStockControl_' & lower('FormStockModels') & '_value')
  else
    packet = clip(packet) & '<!-- Net:FormStockModels --><13,10>'
  end
  do SendPacket


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormStockControl_sto:Accessory_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:Accessory
      else
        do Value::sto:Accessory
      end
  of lower('FormStockControl_sto:Sundry_Item_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:Sundry_Item
      else
        do Value::sto:Sundry_Item
      end
  of lower('FormStockControl_sto:Suspend_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:Suspend
      else
        do Value::sto:Suspend
      end
  of lower('FormStockControl_sto:ExchangeUnit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:ExchangeUnit
      else
        do Value::sto:ExchangeUnit
      end
  of lower('FormStockControl_sto:AllowDuplicate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:AllowDuplicate
      else
        do Value::sto:AllowDuplicate
      end
  of lower('FormStockControl_sto:ReturnFaultySpare_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:ReturnFaultySpare
      else
        do Value::sto:ReturnFaultySpare
      end
  of lower('FormStockControl_sto:ChargeablePartOnly_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:ChargeablePartOnly
      else
        do Value::sto:ChargeablePartOnly
      end
  of lower('FormStockControl_sto:AttachBySolder_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:AttachBySolder
      else
        do Value::sto:AttachBySolder
      end
  of lower('FormStockControl_sto:E1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:E1
      else
        do Value::sto:E1
      end
  of lower('FormStockControl_sto:E2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:E2
      else
        do Value::sto:E2
      end
  of lower('FormStockControl_sto:E3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::sto:E3
      else
        do Value::sto:E3
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormStockControl_form:ready_',1)
  p_web.SetSessionValue('FormStockControl_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormStockControl',0)
  Access:STOCK.PrimeRecord()
  Ans = ChangeRecord
  p_web.SetSessionValue('FormStockControl:Primed',1)

PreCopy  Routine
  p_web.SetValue('FormStockControl_form:ready_',1)
  p_web.SetSessionValue('FormStockControl_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormStockControl',0)
  Access:STOCK.PrimeRecord()
  Ans = ChangeRecord
  p_web.SetSessionValue('FormStockControl:Primed',1)
  p_web._PreCopyRecord(STOCK,sto:Ref_Number_Key,Net:Web:Autonumbered)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormStockControl_form:ready_',1)
  p_web.SetSessionValue('FormStockControl_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormStockControl:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormStockControl_form:ready_',1)
  p_web.SetSessionValue('FormStockControl_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormStockControl:Primed',0)
  p_web.setsessionvalue('showtab_FormStockControl',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormStockControl_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormStockControl_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! tab = 3
    loc:InvalidTab += 1
  ! tab = 4
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
  ! Automatic Dictionary Validation
    If sto:LoanUnit <> '1' and sto:LoanUnit <> '0'
      loc:Invalid = 'sto:LoanUnit'
      loc:Alert = clip('sto:LoanUnit') & ' ' & clip(p_web.site.OneOfText) & ' ' & '1' & ' / ' & '0'
    End
  If Loc:Invalid <> '' then exit.
! NET:WEB:StagePOST
PostInsert      Routine

PostCopy        Routine
  p_web.SetSessionValue('FormStockControl:Primed',0)

PostUpdate      Routine
  p_web.SetSessionValue('FormStockControl:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('loc0_7')
  p_web.StoreValue('loc0_30')
  p_web.StoreValue('loc31_60')
  p_web.StoreValue('loc61_90')
  p_web.StoreValue('locAverageText')
  p_web.StoreValue('')

PostDelete      Routine
