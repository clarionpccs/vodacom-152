

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER018.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER017.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
getPartStatus        PROCEDURE  (fType)                    ! Declare Procedure

  CODE
!Assume in part, or warpart record
    colourFlag# = 0

    case fType
    of 'C' ! Chargeable part

        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number  = par:Part_Ref_Number
        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Found
        !    tmp:CShelfLocation  = Clip(sto:Shelf_Location) & ' / ' & Clip(sto:Second_Location)
        Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Error
        !    tmp:CShelfLocation  = ''
        End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign


        If par:Part_Number = 'EXCH'
            Case par:Status
                Of 'ORD'    !Order
                    colourFlag# = 6
                Of 'REQ'    !Allocate
                    colourFlag# = 1
                Of 'RTS'
                    colourFlag# = 7
            End !Case par:Status
        Else !par:Part_Number = 'EXCH'
            If par:WebOrder = 1
                colourFlag# = 6
            Else

                If par:PartAllocated Or ~(rapidLocation(sto:Location) And sto:Location <> '')
                    If par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = ''
                        colourFlag# = 1 !Green
                    End !If par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = ''

                    If par:order_number <> '' And par:date_received =''
                        colourFlag# = 3 !Red
                    End !If par:order_number <> '' And par:date_received =''

                    If par:Order_Number <> '' And par:Date_Received > 0
                        colourFlag# = 4 !Gray
                    End !If par:Order_Number <> '' And par:Date_Received <> ''
                Else !par:PartAllocated
                    If par:Status = ''
                        colourFlag# = 1 !Green
                    End !If par:Status = ''

                    Case par:Status
                        Of 'PRO'
                            colourFlag# = 5 !Yellow
                        Of 'PIK'
                            colourFlag# = 2 !Navy
                        Of 'RET'
                            colourFlag# = 7 !Pink?
                    End !Case par:Status
                End !par:PartAllocated

            End !par:WebOrder = 1
        End !par:Part_Number = 'EXCH'

    of 'W'
        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number  = wpr:Part_Ref_Number
        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Found
        !    tmp:CShelfLocation  = Clip(sto:Shelf_Location) & ' / ' & Clip(sto:Second_Location)
        Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Error
        !    tmp:CShelfLocation  = ''
        End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign


        If wpr:Part_Number = 'EXCH'
            Case wpr:Status
                Of 'ORD'    !Order
                    colourFlag# = 6
                Of 'REQ'    !Allocate
                    colourFlag# = 1
                Of 'RTS'
                    colourFlag# = 7
            End !Case wpr:Status
        Else !wpr:Part_Number = 'EXCH'
            If wpr:WebOrder = 1
                colourFlag# = 6
            Else

                If wpr:PartAllocated Or ~(rapidLocation(sto:Location) And sto:Location <> '')
                    If wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = ''
                        colourFlag# = 1 !Green
                    End !If wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = ''

                    If wpr:order_number <> '' And wpr:date_received =''
                        colourFlag# = 3 !Red
                    End !If wpr:order_number <> '' And wpr:date_received =''

                    If wpr:Order_Number <> '' And wpr:Date_Received > 0
                        colourFlag# = 4 !Gray
                    End !If wpr:Order_Number <> '' And wpr:Date_Received <> ''
                Else !wpr:PartAllocated
                    If wpr:Status = ''
                        colourFlag# = 1 !Green
                    End !If wpr:Status = ''

                    Case wpr:Status
                        Of 'PRO'
                            colourFlag# = 5 !Yellow
                        Of 'PIK'
                            colourFlag# = 2 !Navy
                        Of 'RET'
                            colourFlag# = 7 !Pink?
                    End !Case wpr:Status
                End !wpr:PartAllocated

            End !wpr:WebOrder = 1
        End !wpr:Part_Number = 'EXCH'


    !1 = green
    !2 = navy
    !3 = red
    !4 = gray
    !5 = fuschia
    !6 = purple
    !7 = 08080FFH

    end ! case fType


    case colourFlag#
    of 1
        return 'Requested'
    of 2
        return 'Picked'
    of 3
        return 'On Order'
    of 4
        !tmp:partStatus = 'Requested'
    of 5
        return 'Awaiting Picking'
    of 6
        return 'On Order'
    of 7
        return 'Awaiting Return'
    else
        return 'Allocated'
    end ! case colourFlag#
    return ''
