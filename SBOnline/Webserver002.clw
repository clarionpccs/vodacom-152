

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('NetWeb.inc'),ONCE
   INCLUDE('VSA_SYSU.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER002.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER501.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! Don't Forget To Update Version Number
!!! </summary>
Main PROCEDURE 

WebLog               GROUP,PRE(web)                        !
EnableLogging        LONG(1)                               !
LastGet              STRING(4096)                          !
LastPost             STRING(4096)                          !
StartDate            LONG                                  !
StartTime            LONG                                  !
PagesServed          LONG                                  !
                     END                                   !
LogQueue             QUEUE,PRE(LogQ)                       !
Port                 LONG                                  !
Date                 LONG                                  !
Time                 LONG                                  !
Desc                 STRING(4096)                          !
                     END                                   !
tmp:DataPath         STRING(255)                           !Data Path
Path:Scripts         STRING(255)                           !Path:Scripts
Path:Web             STRING(255)                           !Path:Web
Path:Styles          STRING(255)                           !Path:Styles
AmendDetails         BYTE                                  !
tmp:OpenError        BYTE(0)                               !Open Error
tmp:VersionNumber    STRING(30)                            !Version Number
s_web              &NetWebServer
Net:ShortInit      Long
loc:RequestData    Group(NetWebServerRequestDataType).
loc:OverString     String(size(loc:RequestData)),over(loc:RequestData)
Window               WINDOW('ServiceBase 3g Online'),AT(0,0,352,276),FONT('Tahoma',8,,FONT:regular),DOUBLE,ICONIZE, |
  ICON('cellular3g.ico'),GRAY,IMM
                       SHEET,AT(4,2,344,254),USE(?Sheet1),COLOR(COLOR:White),SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Verison Number:'),AT(8,20,336,14),USE(?VersionNumber:Prompt),FONT(,12,,FONT:bold), |
  CENTER,TRN
                           IMAGE('sbonline.jpg'),AT(76,34),USE(?Image1),CENTERED
                           CHECK('Amend Defaults'),AT(144,130),USE(AmendDetails),VALUE('1','0'),TRN
                           PROMPT('Server Port'),AT(28,142),USE(?Prompt1),TRN
                           TEXT,AT(104,142,64,10),USE(glo:WebserverPort),SINGLE,BOXED,FLAT
                           TEXT,AT(104,158,200,10),USE(glo:LocalPath),SINGLE,BOXED,DISABLE,FLAT
                           PROMPT('Local Path'),AT(28,158),USE(?PROMPT2),TRN
                           TEXT,AT(104,174,200,10),USE(glo:DataPath),SINGLE,BOXED,FLAT
                           BUTTON('...'),AT(308,174,12,10),USE(?LookupFile)
                           TEXT,AT(104,190,200,10),USE(Path:Web),SINGLE,BOXED,FLAT
                           BUTTON('...'),AT(308,190,12,10),USE(?LookupFile:2)
                           PROMPT('Path To "Web" Folder'),AT(28,190),USE(?Prompt1:2),TRN
                           BUTTON('Save Changes And Close WebServer'),AT(104,204,148,14),USE(?Button4),LEFT,ICON('psave.ico')
                           PROMPT('Data Path'),AT(28,174),USE(?Prompt1:3),TRN
                         END
                         TAB('Logging'),USE(?Tab2)
                           GROUP('Logging Group'),AT(8,16,336,218),USE(?LogGroup),TRN
                             LIST,AT(8,20,332,98),USE(?LogQueue),VSCROLL,COLOR(,COLOR:Black,00F0F0F0h),GRID(00F0F0F0h), |
  FORMAT('51L(2)|M~Port~@n5@51L(2)|M~Date~@D17B@36L(2)|M~Time~@T4B@1020L(2)|M~Description~@s255@'), |
  FROM(LogQueue)
                             TEXT,AT(8,122,168,84),USE(web:LastGet),VSCROLL,BOXED
                             TEXT,AT(180,122,160,84),USE(web:LastPost),VSCROLL,BOXED
                             OPTION('Log:'),AT(8,209,141,22),USE(web:EnableLogging),BOXED,TRN
                               RADIO('No'),AT(16,220),USE(?Option1:Radio1),VALUE('0'),TRN
                               RADIO('Screen'),AT(44,220),USE(?Option1:Radio2),VALUE('1'),TRN
                               RADIO('Screen && Disk'),AT(88,220),USE(?Option1:Radio3),VALUE('2'),TRN
                             END
                             BUTTON('Clear'),AT(149,217,45,16),USE(?Clear)
                           END
                           CHECK('Enable Procedure Logging'),AT(8,241),USE(glo:EnableProcedureLogging),TRN,VALUE('1', |
  '0')
                         END
                       END
                       STRING('Started At:'),AT(5,260),USE(?StartedAt),TRN
                       STRING(@d1),AT(45,260),USE(web:StartDate),TRN
                       STRING(@t1),AT(89,260),USE(web:StartTime)
                       STRING('Pages:'),AT(161,260),USE(?Pages),TRN
                       STRING(@n14),AT(189,260),USE(web:PagesServed),TRN
                       BUTTON('Close WebServer'),AT(269,258,80,14),USE(?Cancel),LEFT,ICON('pcancel.ico'),STD(STD:Close)
                     END

SI vsSystemUtilClass                                       ! ValuesSystemInformation (ABC Free)
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Local Data Classes
p_web                CLASS(NetWebServer)                   ! Generated by NetTalk Extension (Class Definition)
AddLog                 PROCEDURE(String p_Data,<string p_ip>),DERIVED
StartNewThread         PROCEDURE(NetWebServerRequestDataType p_RequestData),DERIVED
TakeEvent              PROCEDURE(),DERIVED

                     END

FileLookup1          SelectFileClass
FileLookup4          SelectFileClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
CheckOmittedWeb  routine

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

!s_web    &NetWebServer
locTempFolder       CSTRING(255)
ipAddress STRING(30)
  CODE
  ! Get Defaults
        glo:LocalPath = Clip(LongPath(Path()))
        tmp:OpenError = 0
        Path:Web = GETINI('DEFAULTS','pathtoweb',0,Clip(glo:LocalPath) & '\WEBSERVER.INI')
        glo:EnableProcedureLogging = GETINI('DEFAULTS','proclogging','web',Clip(glo:LocalPath) & '\WEBSERVER.INI')
        If GETINI('DEFAULTS','PathToData',,Clip(Path()) & '\WEBSERVER.INI') <> ''
            glo:WebserverPort = GETINI('DEFAULTS','port',,Clip(glo:LocalPath) & '\WEBSERVER.INI')
            glo:DataPath = GETINI('DEFAULTS','pathtodata',,Clip(glo:LocalPath) & '\WEBSERVER.INI')
            
            web:EnableLogging = GETINI('DEFAULTS','enablelogging',0,CLIP(glo:LocalPath) & '\WEBSERVER.INI')
            
            ! Create "Logging" folder
            glo:LogComputerName = CLIP(SI.GetComputerName())
            gloIpAddress = CLIP(SI.GetComputerName()) & ' : ' & glo:WebserverPort       
            
            glo:LogFilePath = CLIP(glo:DataPath) & '\_Logging\' & CLIP(glo:LogComputerName)
            IF (NOT EXISTS(glo:LogFilePath))
                rtn# = MkDir(glo:LogFilePath)
            END ! IF            
            
            
            SetPath(Clip(glo:DataPath))
            tmp:DataPath = Path()
            
            !NetOptions(net:getcompletename,computername)
  
            ! Create the "temp" folder
            
            ! #13590 Create temp folder to hold criteria files, away from data folder (DBH: 12/10/2015)
            locTempFolder = CLIP(tmp:DataPath) & '\temp'
            IF (NOT EXISTS(locTempFolder))  
                rtn# = MKDir(locTempFolder)
            END
            
            locTempFolder = CLIP(Path:Web) & '\temp'
            IF (NOT EXISTS(locTempFolder))  
                rtn# = MKDir(locTempFolder)
            END
  
            glo:TagFile = clip(Path:Web) & '\temp\tagfile' & clock() & '.tmp'
            glo:SBO_GenericTagFile = clip(Path:Web) & '\temp\GenTagFile' & clock() & '.tmp'
            glo:SBO_DupCheck = CLIP(Path:Web) & '\temp\dupcheck' & CLOCK() & '.tmp'
            glo:sbo_outfaultparts = clip(Path:Web) & '\temp\outfaultparts' & clock() & '.tmp'
            glo:sbo_outparts = CLIP(Path:Web) & '\temp\outparts' & CLOCK() & '.tmp'
            glo:tempFaultCodes = clip(Path:Web) & '\temp\faultcodes' & clock() & '.tmp'
            glo:tempAuditQueue = clip(Path:Web) & '\temp\auditqueue' & clock() & '.tmp'
            glo:StockReceiveTmp = CLIP(Path:Web) & '\temp\stockreceive' & CLOCK() & '.tmp'
            glo:SBO_GenericFile = CLIP(Path:Web) & '\temp\genericfile' & CLOCK() & '.tmp'
            glo:TempStockAuditQueue = CLIP(Path:Web) & '\temp\stockauditqueue' & CLOCK() & '.tmp'
            glo:SBO_WarrantyClaims = CLIP(Path:Web) & '\temp\warrantyclaims' & CLOCK() & '.tmp'
            CREATE(SBO_WarrantyClaims)
            SHARE(SBO_WarrantyClaims)
            create(TempFaultCodes)
            share(TempFaultCodes)
            create(TempAuditQueue)
            share(TempAuditQueue)
            CREATE(TempStockAuditQueue)
            SHARE(TempStockAuditQueue)
            
            
  
            
        Else ! If GETINI('DEFAULTS','PathToData',,Clip(Path()) & '\WEBSERVER.INI') = ''
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('No defaults have been setup for this WebServer.'&|
                            '|You must ensure they are setup correctly before this WebServer can function correctly.','Amend Details',|
                            icon:Hand,'&OK',1,1)
            Of 1 ! &OK Button
            End!Case Message
            tmp:OpenError = 1
        End ! If GETINI('DEFAULTS','PathToData',,Clip(Path()) & '\WEBSERVER.INI') = ''
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?VersionNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:STOCK.SetOpenRelated()
  Relate:STOCK.Open                                        ! File STOCK used by this procedure, so make sure it's RelationManager is open
  Relate:Tagging.Open                                      ! File Tagging used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  AddToLog('SBOnline Open',CLIP(glo:LocalPath),'Main',,kVersionNumber & '.' & kWebserverCurrentVersion)
        ! Clear Tagging Q
        SETCURSOR(CURSOR:Wait)
        Access:Tagging.ClearKey(tgg:EntryDateKey)
        tgg:EntryDate = 0
        SET(tgg:EntryDateKey,tgg:EntryDateKey)
        LOOP UNTIL Access:Tagging.Next() <> Level:Benign
            IF (tgg:EntryDate = TODAY())
                BREAK
            END ! IF
            Access:Tagging.DeleteRecord(0)
        END ! LOOP
        SETCURSOR()  
  SELF.Open(Window)                                        ! Open window
                                               ! Generated by NetTalk Extension (Start)
  do CheckOmittedWeb
    s_web &= p_web
    p_web.SuppressErrorMsg = 1         ! No Object Generated Error Messages ! Generated by NetTalk Extension
    p_web.init()
    p_web.Port = glo:WebserverPort
    p_web.Open()
    Get(p_web._SitesQueue,1)
  !---------------------------------
  s_web._SitesQueue.Defaults.WebHandlerProcName = 'WebHandler'
  s_web._SitesQueue.Defaults.DefaultPage = 'LoginForm'
  s_web._SitesQueue.Defaults.SessionExpiryAfterHS = 90001
  s_web._SitesQueue.Defaults.LoginPage = 'PreLoginForm'
  s_web._SitesQueue.Defaults.LoginPageIsControl = 0
  s_web._SitesQueue.Defaults.WebFolderPath = Path:Web
  If instring('\',s_web._SitesQueue.Defaults.WebFolderPath,1,1) = 0
    s_web._SitesQueue.Defaults.WebFolderPath = clip(s_web._SitesQueue.defaults.appPath) & s_web._SitesQueue.Defaults.WebFolderPath
  End
  s_web._SitesQueue.Defaults.UploadsPath = clip(s_web._SitesQueue.Defaults.WebFolderPath) & '\uploads'
  s_web._SitesQueue.Defaults.HtmlCharset = 'ISO-8859-1'
  s_web._SitesQueue.Defaults.LocatePromptText = s_web.Translate('Locate')
  s_web._SitesQueue.Defaults.scriptsdir = 'scripts'
  s_web._SitesQueue.Defaults.stylesdir  = 'styles'
  s_web.MakeFolders()
  s_web._SitesQueue.defaults.AllowAjax = 1
  s_web._SitesQueue.defaults._CheckForParseHeader = 1         ! Check for Parse Header String
  s_web._SitesQueue.defaults._CheckForParseHeaderSize = 1000  ! Check for the Parse Header in the first x bytes
  s_web._SitesQueue.defaults._CheckParseHeader = '<!-- NetWebServer --><13,10>'
  s_web._SitesQueue.defaults.securedir = 'secure'
  s_web._SitesQueue.defaults.loggedindir = 'loggedin'
  s_web._SitesQueue.defaults.InsertPromptText = s_web.Translate('Insert')
  s_web._SitesQueue.defaults.CopyPromptText = s_web.Translate('Copy')
  s_web._SitesQueue.defaults.ChangePromptText = s_web.Translate('Change')
  s_web._SitesQueue.defaults.ViewPromptText   = s_web.Translate('View')
  s_web._SitesQueue.defaults.DeletePromptText = s_web.Translate('Delete')
  s_web._SitesQueue.defaults.RequiredText = s_web.Translate('Required')
  s_web._SitesQueue.defaults.NumericText = s_web.Translate('A Number')
  s_web._SitesQueue.defaults.MoreThanText = s_web.Translate('More than or equal to')
  s_web._SitesQueue.defaults.LessThanText = s_web.Translate('Less than or equal to')
  s_web._SitesQueue.defaults.NotZeroText = s_web.Translate('Must not be Zero or Blank')
  s_web._SitesQueue.defaults.OneOfText = s_web.Translate('Must be one of')
  s_web._SitesQueue.defaults.InListText = s_web.Translate('Must be one of')
  s_web._SitesQueue.defaults.InFileText = s_web.Translate('Must be in table')
  s_web._SitesQueue.defaults.DuplicateText = s_web.Translate('Creates Duplicate Record on')
  s_web._SitesQueue.defaults.RestrictText = s_web.Translate('Unable to Delete, Child records exist in table')
  s_web._SitesQueue.Defaults.DatePicture = '@D6'
  s_web._SitesQueue.Defaults.PageHeaderTag = '<!-- Net:BannerBlank -->'
  s_web._SitesQueue.Defaults.PageFooterTag = '<!-- Net:PageFooter -->'
  s_web._SitesQueue.Defaults.PageTitle = 'ServiceBase Online'
  s_web._SitesQueue.Defaults.WebFormStyle = Net:Web:Plain
  s_web._SitesQueue.Defaults.BrowseClass = 'BrowseTable'
  s_web._SitesQueue.Defaults.PromptClass = 'FormPrompt'
  s_web._SitesQueue.Defaults.CommentClass = 'FormComments'
  s_web._SitesQueue.Defaults.BodyClass = 'PageBody'
  s_web._SitesQueue.Defaults.BodyDivClass = 'PageBodyDiv'
  s_web._SitesQueue.Defaults.BrowseHighlightColor = 314111
  s_web._SitesQueue.Defaults.BrowseOverColor = -1
  s_web._SitesQueue.Defaults.BrowseOneColor = 16777215
  s_web._SitesQueue.Defaults.BrowseTwoColor = 16315633
  s_web._SitesQueue.Defaults.UploadButton.Name = 'upload_btn'
  s_web._SitesQueue.Defaults.UploadButton.TextValue = s_web.Translate('Upload')
  s_web._SitesQueue.Defaults.UploadButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.UploadButton.ToolTip = s_web.Translate('Click here to Upload the file')
  s_web._SitesQueue.Defaults.LookupButton.Name = 'lookup_btn'
  s_web._SitesQueue.Defaults.LookupButton.TextValue = s_web.Translate('...')
  s_web._SitesQueue.Defaults.LookupButton.Class = 'LookupButton'
  s_web._SitesQueue.Defaults.LookupButton.ToolTip = s_web.Translate('Click here to Search for a value')
  s_web._SitesQueue.Defaults.SaveButton.Name = 'save_btn'
  s_web._SitesQueue.Defaults.SaveButton.TextValue = s_web.Translate('Save')
  s_web._SitesQueue.Defaults.SaveButton.Image = '/images/psave.png'
  s_web._SitesQueue.Defaults.SaveButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.SaveButton.ToolTip = s_web.Translate('Click on this to Save the form')
  s_web._SitesQueue.Defaults.CancelButton.Name = 'cancel_btn'
  s_web._SitesQueue.Defaults.CancelButton.TextValue = s_web.Translate('Cancel')
  s_web._SitesQueue.Defaults.CancelButton.Image = '/images/pcancel.png'
  s_web._SitesQueue.Defaults.CancelButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.CancelButton.ToolTip = s_web.Translate('Click on this to Cancel the form')
  s_web._SitesQueue.Defaults.DeletefButton.Name = 'deletef_btn'
  s_web._SitesQueue.Defaults.DeletefButton.TextValue = s_web.Translate('Delete')
  s_web._SitesQueue.Defaults.DeletefButton.Image = '/images/pdelete.png'
  s_web._SitesQueue.Defaults.DeletefButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.DeletefButton.ToolTip = s_web.Translate('Click here to Delete this record')
  s_web._SitesQueue.Defaults.CloseButton.Name = 'close_btn'
  s_web._SitesQueue.Defaults.CloseButton.TextValue = s_web.Translate('Close')
  s_web._SitesQueue.Defaults.CloseButton.Image = '/images/pcancel.png'
  s_web._SitesQueue.Defaults.CloseButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.CloseButton.ToolTip = s_web.Translate('Click here to Close this form')
  s_web._SitesQueue.Defaults.InsertButton.Name = 'insert_btn'
  s_web._SitesQueue.Defaults.InsertButton.TextValue = s_web.Translate('Insert')
  s_web._SitesQueue.Defaults.InsertButton.Image = '/images/pinsert.png'
  s_web._SitesQueue.Defaults.InsertButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.InsertButton.ToolTip = s_web.Translate('Click here to Insert a new record')
  s_web._SitesQueue.Defaults.ChangeButton.Name = 'change_btn'
  s_web._SitesQueue.Defaults.ChangeButton.TextValue = s_web.Translate('Change')
  s_web._SitesQueue.Defaults.ChangeButton.Image = '/images/pchange.png'
  s_web._SitesQueue.Defaults.ChangeButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.ChangeButton.ToolTip = s_web.Translate('Click here to Change the highlighted record')
  s_web._SitesQueue.Defaults.ViewButton.Name = 'view_btn'
  s_web._SitesQueue.Defaults.ViewButton.TextValue = s_web.Translate('View')
  s_web._SitesQueue.Defaults.ViewButton.Image = '/images/psearch.png'
  s_web._SitesQueue.Defaults.ViewButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.ViewButton.ToolTip = s_web.Translate('Click here to view details of the highlighted record')
  s_web._SitesQueue.Defaults.DeletebButton.Name = 'deleteb_btn'
  s_web._SitesQueue.Defaults.DeletebButton.TextValue = s_web.Translate('Delete')
  s_web._SitesQueue.Defaults.DeletebButton.Image = '/images/pdelete.png'
  s_web._SitesQueue.Defaults.DeletebButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.DeletebButton.ToolTip = s_web.Translate('Click here to Delete the highlighted record')
  s_web._SitesQueue.Defaults.SelectButton.Name = 'select_btn'
  s_web._SitesQueue.Defaults.SelectButton.TextValue = s_web.Translate('Select')
  s_web._SitesQueue.Defaults.SelectButton.Image = '/images/pselect.png'
  s_web._SitesQueue.Defaults.SelectButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.SelectButton.ToolTip = s_web.Translate('Click here to Select the highlighted record')
  s_web._SitesQueue.Defaults.BrowseCancelButton.Name = 'browsecancel_btn'
  s_web._SitesQueue.Defaults.BrowseCancelButton.TextValue = s_web.Translate('Cancel')
  s_web._SitesQueue.Defaults.BrowseCancelButton.Image = '/images/pcancel.png'
  s_web._SitesQueue.Defaults.BrowseCancelButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.BrowseCancelButton.ToolTip = s_web.Translate('Click here to return without selecting anything')
  s_web._SitesQueue.Defaults.BrowseCloseButton.Name = 'browseclose_btn'
  s_web._SitesQueue.Defaults.BrowseCloseButton.TextValue = s_web.Translate('Close')
  s_web._SitesQueue.Defaults.BrowseCloseButton.Image = '/images/pcancel.png'
  s_web._SitesQueue.Defaults.BrowseCloseButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.BrowseCloseButton.ToolTip = s_web.Translate('Click here to Close this browse')
  s_web._SitesQueue.Defaults.SmallInsertButton.Name = 'insert_btn'
  s_web._SitesQueue.Defaults.SmallInsertButton.TextValue = s_web.Translate('Insert')
  s_web._SitesQueue.Defaults.SmallInsertButton.Class = 'SmallButtonIcon'
  s_web._SitesQueue.Defaults.SmallInsertButton.ToolTip = s_web.Translate('Click here to Insert a new record')
  s_web._SitesQueue.Defaults.SmallChangeButton.Name = 'change_btn'
  s_web._SitesQueue.Defaults.SmallChangeButton.TextValue = s_web.Translate('Change')
  s_web._SitesQueue.Defaults.SmallChangeButton.Class = 'SmallButtonIcon'
  s_web._SitesQueue.Defaults.SmallChangeButton.ToolTip = s_web.Translate('Click here to Change this record')
  s_web._SitesQueue.Defaults.SmallViewButton.Name = 'view_btn'
  s_web._SitesQueue.Defaults.SmallViewButton.TextValue = s_web.Translate('View')
  s_web._SitesQueue.Defaults.SmallViewButton.Class = 'SmallButtonIcon'
  s_web._SitesQueue.Defaults.SmallViewButton.ToolTip = s_web.Translate('Click here to view details of this record')
  s_web._SitesQueue.Defaults.SmallDeleteButton.Name = 'deleteb_btn'
  s_web._SitesQueue.Defaults.SmallDeleteButton.TextValue = s_web.Translate('Delete')
  s_web._SitesQueue.Defaults.SmallDeleteButton.Class = 'SmallButtonIcon'
  s_web._SitesQueue.Defaults.SmallDeleteButton.ToolTip = s_web.Translate('Click here to Delete this record')
  s_web._SitesQueue.Defaults.SmallSelectButton.Name = 'select_btn'
  s_web._SitesQueue.Defaults.SmallSelectButton.TextValue = s_web.Translate('Select')
  s_web._SitesQueue.Defaults.SmallSelectButton.Class = 'SmallButtonIcon'
  s_web._SitesQueue.Defaults.SmallSelectButton.ToolTip = s_web.Translate('Click here to Select this record')
  s_web._SitesQueue.Defaults.LocateButton.Name = 'locate_btn'
  s_web._SitesQueue.Defaults.LocateButton.TextValue = s_web.Translate('Search')
  s_web._SitesQueue.Defaults.LocateButton.Image = '/images/psearch.png'
  s_web._SitesQueue.Defaults.LocateButton.Class = 'MainButtonIcon'
  s_web._SitesQueue.Defaults.LocateButton.ToolTip = s_web.Translate('Click here to start the Search')
  s_web._SitesQueue.Defaults.FirstButton.Name = 'first_btn'
  s_web._SitesQueue.Defaults.FirstButton.TextValue = s_web.Translate('')
  s_web._SitesQueue.Defaults.FirstButton.Image = '/images/listtop.png'
  s_web._SitesQueue.Defaults.FirstButton.ImageWidth = '16'
  s_web._SitesQueue.Defaults.FirstButton.ImageHeight = '16'
  s_web._SitesQueue.Defaults.FirstButton.ImageAlt = s_web.Translate('')
  s_web._SitesQueue.Defaults.FirstButton.Class = 'browsenavbuttons'
  s_web._SitesQueue.Defaults.FirstButton.ToolTip = s_web.Translate('Click here to go to the First page in the list')
  s_web._SitesQueue.Defaults.PreviousButton.Name = 'previous_btn'
  s_web._SitesQueue.Defaults.PreviousButton.TextValue = s_web.Translate('')
  s_web._SitesQueue.Defaults.PreviousButton.Image = '/images/listback.png'
  s_web._SitesQueue.Defaults.PreviousButton.ImageWidth = '16'
  s_web._SitesQueue.Defaults.PreviousButton.ImageHeight = '16'
  s_web._SitesQueue.Defaults.PreviousButton.ImageAlt = s_web.Translate('')
  s_web._SitesQueue.Defaults.PreviousButton.Class = 'browsenavbuttons'
  s_web._SitesQueue.Defaults.PreviousButton.ToolTip = s_web.Translate('Click here to go to the Previous page in the list')
  s_web._SitesQueue.Defaults.NextButton.Name = 'next_btn'
  s_web._SitesQueue.Defaults.NextButton.TextValue = s_web.Translate('')
  s_web._SitesQueue.Defaults.NextButton.Image = '/images/listnext.png'
  s_web._SitesQueue.Defaults.NextButton.ImageWidth = '16'
  s_web._SitesQueue.Defaults.NextButton.ImageHeight = '16'
  s_web._SitesQueue.Defaults.NextButton.ImageAlt = s_web.Translate('')
  s_web._SitesQueue.Defaults.NextButton.Class = 'browsenavbuttons'
  s_web._SitesQueue.Defaults.NextButton.ToolTip = s_web.Translate('Click here to go to the Next page in the list')
  s_web._SitesQueue.Defaults.LastButton.Name = 'last_btn'
  s_web._SitesQueue.Defaults.LastButton.TextValue = s_web.Translate('')
  s_web._SitesQueue.Defaults.LastButton.Image = '/images/listbottom.png'
  s_web._SitesQueue.Defaults.LastButton.ImageWidth = '16'
  s_web._SitesQueue.Defaults.LastButton.ImageHeight = '16'
  s_web._SitesQueue.Defaults.LastButton.ImageAlt = s_web.Translate('')
  s_web._SitesQueue.Defaults.LastButton.Class = 'browsenavbuttons'
  s_web._SitesQueue.Defaults.LastButton.ToolTip = s_web.Translate('Click here to go to the Last page in the list')
  s_web._SitesQueue.Defaults.PrintButton.Name = 'print_btn'
  s_web._SitesQueue.Defaults.PrintButton.TextValue = s_web.Translate('Print')
  s_web._SitesQueue.Defaults.PrintButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.PrintButton.ToolTip = s_web.Translate('Click here to Print this page')
  s_web._SitesQueue.Defaults.StartButton.Name = 'start_btn'
  s_web._SitesQueue.Defaults.StartButton.TextValue = s_web.Translate('Start')
  s_web._SitesQueue.Defaults.StartButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.StartButton.ToolTip = s_web.Translate('Click here to Start the report')
  s_web._SitesQueue.Defaults.DateLookupButton.Name = 'lookup_btn'
  s_web._SitesQueue.Defaults.DateLookupButton.TextValue = s_web.Translate('...')
  s_web._SitesQueue.Defaults.DateLookupButton.Class = 'LookupButton'
  s_web._SitesQueue.Defaults.DateLookupButton.ToolTip = s_web.Translate('Click here to select a date')
  s_web._SitesQueue.Defaults.WizPreviousButton.Name = 'wizprevious_btn'
  s_web._SitesQueue.Defaults.WizPreviousButton.TextValue = s_web.Translate('Previous')
  s_web._SitesQueue.Defaults.WizPreviousButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.WizPreviousButton.ToolTip = s_web.Translate('Click here to go back to the Previous step')
  s_web._SitesQueue.Defaults.WizNextButton.Name = 'wiznext_btn'
  s_web._SitesQueue.Defaults.WizNextButton.TextValue = s_web.Translate('Next')
  s_web._SitesQueue.Defaults.WizNextButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.WizNextButton.ToolTip = s_web.Translate('Click here to go to the Next step')
  s_web._SitesQueue.Defaults.SmallOtherButton.Name = 'other_btn'
  s_web._SitesQueue.Defaults.SmallOtherButton.TextValue = s_web.Translate('Other')
  s_web._SitesQueue.Defaults.SmallOtherButton.Class = 'SmallButton'
  s_web._SitesQueue.Defaults.SmallOtherButton.ToolTip = s_web.Translate('')
  s_web._SitesQueue.Defaults.SmallPrintButton.Name = 'print_btn'
  s_web._SitesQueue.Defaults.SmallPrintButton.TextValue = s_web.Translate('Print')
  s_web._SitesQueue.Defaults.SmallPrintButton.Class = 'SmallButton'
  s_web._SitesQueue.Defaults.SmallPrintButton.ToolTip = s_web.Translate('Click here to print this record')
  s_web._SitesQueue.Defaults.CopyButton.Name = 'copy_btn'
  s_web._SitesQueue.Defaults.CopyButton.TextValue = s_web.Translate('Copy')
  s_web._SitesQueue.Defaults.CopyButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.CopyButton.ToolTip = s_web.Translate('Click here to copy the highlighted record')
  s_web._SitesQueue.Defaults.SmallCopyButton.Name = 'copy_btn'
  s_web._SitesQueue.Defaults.SmallCopyButton.TextValue = s_web.Translate('Copy')
  s_web._SitesQueue.Defaults.SmallCopyButton.Class = 'SmallButton'
  s_web._SitesQueue.Defaults.SmallCopyButton.ToolTip = s_web.Translate('Click here to copy this record')
  s_web._SitesQueue.Defaults.ClearButton.Name = 'clear_btn'
  s_web._SitesQueue.Defaults.ClearButton.TextValue = s_web.Translate('Clear')
  s_web._SitesQueue.Defaults.ClearButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.ClearButton.ToolTip = s_web.Translate('Click here to clear the locator')
  s_web._SitesQueue.Defaults.LogoutButton.Name = 'logout_btn'
  s_web._SitesQueue.Defaults.LogoutButton.TextValue = s_web.Translate('Logout')
  s_web._SitesQueue.Defaults.LogoutButton.Class = 'MainButton'
  s_web._SitesQueue.Defaults.LogoutButton.ToolTip = s_web.Translate('Click here to logout')
  s_web._SitesQueue.Defaults.PreCompressed = 0
  s_web._SitesQueue.Defaults.HtmlCommonScripts = |
    s_web.AddScript('prototype.js') &|
    s_web.AddScript('rico.js') &|
    s_web.AddScript('chrome.js') &|
    s_web.AddScript('netweb.js') &|
    s_web.AddScript('tabs.js') &|
    s_web.AddScript('tabsxp.js') &|
    s_web.AddScript('xptaskpanel.js') &|
    s_web.AddScript('calendar2.js') &|
    s_web.AddScript('calendar6.js') &|
    s_web.AddScript('sorttable.js') &|
    s_web.AddScript('dhtmlgoodies_calendar.js') &|
    s_web.AddScript('pccsMessageBox.js') &|
  ''
  s_web._SitesQueue.Defaults.HtmlMSIE6Scripts = |
    s_web.AddScript('msie6.js') &|
  ''
  s_web._SitesQueue.Defaults.HtmlCommonStyles = |
    s_web.AddStyle('accordion.css') &|
    s_web.AddStyle('chromemenu.css') &|
    s_web.AddStyle('sorttable.css') &|
    s_web.AddStyle('tabs.css') &|
    s_web.AddStyle('xptabs.css') &|
    s_web.AddStyle('xptaskpanel.css') &|
    s_web.AddStyle('netweb.css') &|
    s_web.AddStyle('pccs.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlMSIE6Styles = |
    s_web.AddStyle('msie6.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlMSIEStyles = |
    s_web.AddStyle('msie.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlFireFoxStyles = |
    s_web.AddStyle('firefox.css') &|
    s_web.AddStyle('pccsfirefox.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlSafariStyles = |
    s_web.AddStyle('firefox.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlChromeStyles = |
    s_web.AddStyle('firefox.css') &|
    s_web.AddStyle('pccschrome.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlOperaStyles = |
    s_web.AddStyle('firefox.css') &|
  ''
  s_web._SitesQueue.Defaults.HtmlMozillaStyles = |
    s_web.AddStyle('firefox.css') &|
  ''
  Put(s_web._SitesQueue)
  If Net:ShortInit
    ReturnValue = Level:Notify
  End
  !--------------------------------------------------------------
  if p_web.error <> 0
    ! Put code in here to handle if the object does not initialise properly
  end
        !! Change Version Here To Determine Which Phase This Program is !!
        tmp:VersionNumber = 'Version Number: ' & kVersionNumber & '.' & kWebserverCurrentVersion
        ?VersionNumber:Prompt{prop:Text} = tmp:VersionNumber
        glo:VersionNumber = tmp:VersionNumber
  
        ! Do not start logging (DBH: 02/03/2009)
        ! web:EnableLogging = 0
  
        p_web.AddLog('Webserver Started: ' & LONGPATH())
  Do DefineListboxStyle
  IF ?AmendDetails{Prop:Checked}
    UNHIDE(?LookupFile)
    UNHIDE(?Button4)
    UNHIDE(?LookupFile:2)
    ENABLE(?glo:WebserverPort)
    ENABLE(?glo:DataPath)
    ENABLE(?Path:Web)
  END
  IF NOT ?AmendDetails{PROP:Checked}
    HIDE(?LookupFile)
    HIDE(?Button4)
    HIDE(?LookupFile:2)
    DISABLE(?glo:WebserverPort)
    DISABLE(?glo:DataPath)
    DISABLE(?Path:Web)
  END
  FileLookup1.Init
  FileLookup1.ClearOnCancel = True
  FileLookup1.Flags=BOR(FileLookup1.Flags,FILE:LongName)   ! Allow long filenames
  FileLookup1.Flags=BOR(FileLookup1.Flags,FILE:Directory)  ! Allow Directory Dialog
  FileLookup1.SetMask('All Files','*.*')                   ! Set the file mask
  FileLookup4.Init
  FileLookup4.ClearOnCancel = True
  FileLookup4.Flags=BOR(FileLookup4.Flags,FILE:LongName)   ! Allow long filenames
  FileLookup4.Flags=BOR(FileLookup4.Flags,FILE:Directory)  ! Allow Directory Dialog
  FileLookup4.SetMask('All Files','*.*')                   ! Set the file mask
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

AllFiles            QUEUE(File:Queue),PRE(fil)
                    END !
i                   LONG()
  CODE
  p_web.Kill()                              ! Generated by NetTalk Extension
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STOCK.Close
    Relate:Tagging.Close
  END
  AddToLog('SBOnline Close',CLIP(glo:LocalPath),'Main',,kVersionNumber & '.' & kWebserverCurrentVersion)
  GlobalErrors.SetProcedureName
        remove(TagFile)
        remove(SBO_GenericTagFile)
        remove(SBO_OutFaultParts)
        REMOVE(StockReceiveTmp)
        REMOVE(sbo_outparts)
        close(tempFaultCodes)
        remove(tempFaultCodes)
        close(tempAuditQueue)
        remove(tempAuditQueue)
        REMOVE(SBO_GenericFile)
        CLOSE(TempStockAuditQueue)
        REMOVE(TempStockAuditQueue)
        CLOSE(SBO_WarrantyClaims)        
        REMOVE(SBO_DupCheck)
        ! CLear Temp Folder
        FREE(AllFiles)
        DIRECTORY(AllFiles,CLIP(Path:Web) & '\temp\*.*',ff_:DIRECTORY)
        LOOP i = 1 TO RECORDS(AllFiles)
            GET(AllFiles,i)
            IF (fil:ShortName <> '..' AND fil:ShortName <> '.')
                REMOVE(CLIP(Path:Web) & '\temp\' & CLIP(fil:Name))
            END ! IF
            
        END ! LOOP	
        
        ! Remove logs over 90 days
        FREE(AllFiles)
        DIRECTORY(allFiles,PATH() & '\_Logging\*SBOnline*.log',ff_:Directory)
        LOOP x# = 1 TO RECORDS(allFiles)
            GET(allFiles,x#)
            !STOP(fil:Name)
            IF (DEFORMAT(SUB(fil:Name,1,8),@d012) < TODAY() - 90)
                
                REMOVE(PATH() & '\_Logging\' & CLIP(fil:Name))
            END
        END ! LOOP  
  
        p_web.AddLog('Webserver Stopped: ' & LONGPATH())
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?AmendDetails
      IF ?AmendDetails{PROP:Checked}
        UNHIDE(?LookupFile)
        UNHIDE(?Button4)
        UNHIDE(?LookupFile:2)
        ENABLE(?glo:WebserverPort)
        ENABLE(?glo:DataPath)
        ENABLE(?Path:Web)
      END
      IF NOT ?AmendDetails{PROP:Checked}
        HIDE(?LookupFile)
        HIDE(?Button4)
        HIDE(?LookupFile:2)
        DISABLE(?glo:WebserverPort)
        DISABLE(?glo:DataPath)
        DISABLE(?Path:Web)
      END
      ThisWindow.Reset
      If ~0{prop:AcceptAll}
          If AmendDetails = 1
              Beep(Beep:SystemExclamation)  ;  Yield()
              Case Message('After any changes are made, the program will quit. '&|
                  '|Anyone connected to this websever at the time will lose information.'&|
                  '|'&|
                  '|Are you sure you want to continue?','Amend Details',|
                             icon:exclamation,'&Yes|&No',2,2)
                  Of 1 ! &Yes Button
                      ?AmendDetails{prop:Disable} = 1
                      ?Cancel{prop:Disable} = 1
                  Of 2 ! &No Button
                      AmendDetails = 0
                      Display()
                      Cycle
              End!Case Message
          End
      End ! If ~0{prop:AcceptAll}
    OF ?LookupFile
      ThisWindow.Update
      glo:DataPath = FileLookup1.Ask(1)
      DISPLAY
    OF ?LookupFile:2
      ThisWindow.Update
      Path:Web = FileLookup4.Ask(1)
      DISPLAY
    OF ?Button4
      ThisWindow.Update
      If glo:WebServerPort <> '' And glo:DataPath <> '' And Path:Web <> ''
          PUTINI('DEFAULTS','port',glo:WebserverPort,Clip(glo:LocalPath) & '\WEBSERVER.INI')
          PUTINI('DEFAULTS','pathtodata',Clip(glo:DataPath),Clip(glo:LocalPath) & '\WEBSERVER.INI')
          PUTINI('DEFAULTS','pathtoweb',Clip(Path:Web),Clip(glo:LocalPath) & '\WEBSERVER.INI')
          Halt()
      End !If glo:WebServerPort <> '' And glo:DatPath <> ''
    OF ?web:EnableLogging
        PUTINI('DEFAULTS','enablelogging',web:EnableLogging,CLIP(glo:LocalPath) & '\WEBSERVER.INI')      
    OF ?glo:EnableProcedureLogging
      PUTINI('DEFAULTS','proclogging',glo:EnableProcedureLogging,Clip(glo:LocalPath) & '\WEBSERVER.INI')      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    p_web.TakeEvent()                 ! Generated by NetTalk Extension
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      If glo:webserverport = ''
          0{prop:Iconize} = 0
      End ! If glo:webserverpost = ''
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


p_web.AddLog PROCEDURE(String p_Data,<string p_ip>)


  CODE
        IF (SUB(p_Data,1,10) <> 'DELSESSION')
            SL_AddToSBOnlineLog(CLIP(p_Data),,web:EnableLogging)
        END  
        
    self._wait()
    If web:EnableLogging > 0
      clear(LogQueue)
      LogQueue.Port = Self.Port
      LogQueue.Date = today()
      LogQueue.Time = clock()
      LogQueue.Desc = p_Data
      Add(LogQueue,1)
      Loop While Records(LogQueue) > 500
        Get(LogQueue,501)
        Delete(LogQueue)
      End
    End
    self._release()
  PARENT.AddLog(p_Data,p_ip)


p_web.StartNewThread PROCEDURE(NetWebServerRequestDataType p_RequestData)

!loc:RequestData    Group(NetWebServerRequestDataType).
!loc:OverString     String(size(loc:RequestData)),over(loc:RequestData)

  CODE
    loc:RequestData :=: p_RequestData
    web:PagesServed = self._PagesServed + 1
    if p_RequestData.DataStringLen >= 4
      case (upper(p_RequestData.DataString[1 : 4]))
      of 'POST'
        web:LastPost = p_RequestData.DataString[1 : p_RequestData.DataStringLen]
        display (?web:LastPost)
      of 'GET '
        web:LastGet = p_RequestData.DataString[1 : p_RequestData.DataStringLen]
        display (?web:LastGet)
      else
        web:LastGet = p_RequestData.DataString[1 : p_RequestData.DataStringLen]
        display (?web:LastGet)
      end
    end
    self.AddLog(p_RequestData.DataString,p_RequestData.FromIP)
    START (WebHandler, 35000, loc:OverString)
    RETURN ! Don't call parent
  PARENT.StartNewThread(p_RequestData)


p_web.TakeEvent PROCEDURE


  CODE
  PARENT.TakeEvent
    if event() = Event:openWindow
      web:StartDate = Today()
      web:StartTime = Clock()
    End
    If Event() = Event:Accepted and Field() = ?Clear
      Free(LogQueue)
      web:LastGet = ''
      web:LastPost = ''
      display()
    End
    If Field() = ?LogQueue and Event() = Event:NewSelection
      Get(LogQueue,Choice(?LogQueue))
      If ErrorCode() = 0
        Case Upper(Sub(LogQueue.Desc,1,3))
        Of 'POS'
          web:LastPost = LogQueue.Desc
        Of 'GET'
          web:LastGet = LogQueue.Desc
        Else
          web:LastGet = LogQueue.Desc
        End
        Display()
      End
    End

