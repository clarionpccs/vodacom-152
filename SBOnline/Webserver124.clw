

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER124.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
PassMobileNumberFormat PROCEDURE  (String f:MobileNumber)  ! Declare Procedure
tmp:Format           STRING(30),AUTO                       !Format
tmp:StartQuotes      BYTE(0)                               !Start Quotes

  CODE
   If f:MobileNumber = ''
        Return True
    End ! If Clip(f:MobileNumber) = ''

    Case GETINI('MOBILENUMBER','FormatType',,Clip(Path()) & '\SB2KDEF.INI')
    Of 0 ! Mobile Number Length
        If CheckLength('MOBILE','',f:MobileNumber)
            Return False
        End ! If CheckLength('MOBILE','',f:MobileNumber)
        Return True
    Of 1 ! Mobile Number Format
    ! End (DBH 22/06/2006) #7597

        tmp:Format  = GETINI('MOBILENUMBER','Format',,Clip(Path()) & '\SB2KDEF.INI')

        If tmp:Format = ''
            Return True
        End ! If Clip(tmp:Format) = ''

        ! Inserting (DBH 22/06/2006) #7597 - Check the length of the mobile before starting the format check
        Len# = 0
        Loop x# = 1 To Len(tmp:Format)
            If Sub(tmp:Format,x#,1) <> '*' And Sub(tmp:Format,x#,1) <> ''
                Len# += 1
            End ! If Sub(tmp:Format,x#,1) <> '*'
        End ! Loop x# = 1 To Len(tmp:Format)
        MobLen# = 0
        Loop x# = 1 To Len(f:MobileNumber)
            If Sub(f:MobileNumber,x#,1) <> ''
                MobLen# += 1
            End ! If Sub(f:MobileNumber,x#,1) <> ''
        End ! Loop x# = 1 To Len(f:MobileNumber)

        If MobLen# <> Len#
            Return False
        End ! If Len(f:MobileNumber) <> Len#
        ! End (DBH 22/06/2006) #7597


        Error# = 0
        y# = 1
        Loop x# = 1 To Len(tmp:Format)
            If tmp:StartQuotes
                If Sub(tmp:Format,x#,1) = '*'
                    tmp:StartQuotes = 0
                    Cycle
                End ! If Sub(tmp:Format,x#,1) = '"'
                If Sub(tmp:Format,x#,1) <> Sub(f:MobileNumber,y#,1)
                    Error# = 1
                    Break
                Else ! If Sub(tmp:Format,x#,1) <> Sub(f:MobileNumber,y#,1)
                    y# += 1
                    Cycle
                End ! If Sub(tmp:Format,x#,1) <> Sub(f:MobileNumber,y#,1)
            End ! If tmp:StartQuotes
            If Sub(tmp:Format,x#,1) = '0'
                If Val(Sub(f:MobileNumber,y#,1)) < 48 Or Val(Sub(f:MobileNumber,y#,1)) > 57
                    Error# = 1
                    Break
                End ! If Val(Sub(f:MobileNumber,y#,1)) < 48 Or Val(Sub(f:MobileNumber,y#,1)) > 57
            End ! If Sub(tmp:Format,x#,1) = '0'

            If Sub(tmp:Format,x#,1) = '*'
                tmp:StartQuotes = 1
                Cycle
            End ! If Sub(tmp:Format,x#,1) = '"'

            y# += 1
        End ! Loop x# = 1 To Len(Clip(tmp:Format))

        If Error# = 0
            Return True
        Else ! If Error# = 0
            Return False
        End ! If Error# = 0
    Else
        Return True
    End ! Case GETINI('MOBILENUMBER','FormatType',,Clip(Path()) & '\SB2KDEF.INI')
