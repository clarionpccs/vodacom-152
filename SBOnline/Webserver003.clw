

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER003.INC'),ONCE        !Local module procedure declarations
                     END


PageFooter           PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:PageFooter -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
loc:divname       string(255)
loc:parent        string(255)
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
  CODE
  GlobalErrors.SetProcedureName('PageFooter')
  loc:parent = p_web.GetValue('_ParentProc')
  If loc:parent <> ''
    loc:divname = 'PageFooter' & '_' & loc:parent
  Else
    loc:divname = 'PageFooter'
  End
  p_web._DivHeader(loc:divname,'adiv nt-left nt-width-100 nt-site-footer')
!----------- put your html code here -----------------------------------
!        packet = CLIP(packet) & |
!            '<script>' & |
!            'startCountDown(' & INT(p_web.site.SessionExpiryAfterHS/100) & ',"LoginForm","countdown");' & |
!            '</script>'
!        DO SendPacket
!        
!        
!        if (p_web.GetSessionLoggedIn() and p_web.PageName <> 'LoginForm')
!    ! parameter 1 is the session time
!    ! parameter 2 is the name of the login page.
!    ! parameter 3 is the id of the <div> in the html.
!    packet = clip(packet) & p_web.Script('startCountDown('& int(p_web.site.SessionExpiryAfterHS/100) &',"'& clip(p_web.site.LoginPage) &'","countdown");')
!!        packet = clip(packet) & p_web.Script('startCountDown(30,"'& clip(p_web.site.LoginPage) &'","countdown");')
!    do SendPacket
!  end

    DO Footer
!----------- end of custom code ----------------------------------------
  do SendPacket
  p_web._DivFooter()
  if loc:parent
    p_web._RegisterDivEx(loc:divname,timer,'''_parentProc='&clip(loc:parent)&'''')
  else
    p_web._RegisterDivEx(loc:divname,timer)
  End
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
footer  Routine
  packet = clip(packet) & |
    '<<div id="_VersionText" class="BottomBannerText"><13,10>'&|
    '    <<span class="SmallText"><13,10>'&|
    '    <<!-- Net:s:LoginDetails1 --><13,10>'&|
    '    <<br/><<!-- Net:s:LoginDetails2 --><13,10>'&|
    '    <<br/><<!-- Net:s:VersionNumber --><13,10>'&|
    '<</div><13,10>'&|
    ''
