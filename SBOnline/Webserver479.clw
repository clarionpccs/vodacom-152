

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER479.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER125.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER480.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
DespatchNote PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
noOfCopies          LONG(1)
priceDespatch       LONG()
priceFirstCopy      LONG()
exchangeNote        LONG(0)
foundThirdPartyIMEI         LONG(0)
Progress:Thermometer BYTE                                  !
locJobNumber         LONG                                  !
locFullJobNumber     STRING(30)                            !
locChargeType        STRING(30)                            !
locRepairType        STRING(30)                            !
locOriginalIMEI      STRING(30)                            !
locFinalIMEI         STRING(30)                            !
locEngineerReport    STRING(255)                           !
locAccessories       STRING(255)                           !
locLoanExchangeUnit  STRING(100)                           !
locIDNumber          STRING(20)                            !
locQuantity          LONG                                  !
locPartNumber        STRING(30)                            !
locDescription       STRING(30)                            !
locUnitCost          REAL                                  !
locLineCost          REAL                                  !
locLoanReplacementValue REAL                               !
locLabour            REAL                                  !
locParts             REAL                                  !
locCarriage          REAL                                  !
locVAT               REAL                                  !
locTotal             REAL                                  !
locTerms             STRING(1000)                          !
locBarCodeJobNumber  STRING(30)                            !
locBarCodeIMEINumber STRING(30)                            !
locDisplayJobNumber  STRING(30)                            !
locDisplayIMEINumber STRING(30)                            !
locCustomerName      STRING(60)                            !
locEndUserTelNo      STRING(60)                            !
locDespatchUser      STRING(60)                            !
locClientName        STRING(60)                            !
qOutFaults           QUEUE,PRE()                           !
Description          STRING(255)                           !
                     END                                   !
DefaultAddress       GROUP,PRE(address)                    !
Name                 STRING(40)                            !Name
SiteName             STRING(40)                            !
Name2                STRING(40)                            !
Location             STRING(40)                            !
RegistrationNo       STRING(40)                            !
VATNumber            STRING(40)                            !
AddressLine1         STRING(40)                            !Address Line 1
AddressLine2         STRING(40)                            !Address Line 2
AddressLine3         STRING(40)                            !Address Line 3
AddressLine4         STRING(40)                            !Postcode
Telephone            STRING(30)                            !Telephone
Fax                  STRING(30)                            !Fax
EmailAddress         STRING(255)                           !Email Address
                     END                                   !
InvoiceAddress       GROUP,PRE(invoice)                    !
Name                 STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
AddressLine4         STRING(30)                            !
TelephoneNumber      STRING(30)                            !
FaxNumber            STRING(30)                            !
MobileNumber         STRING(30)                            !
                     END                                   !
DeliveryAddress      GROUP,PRE(delivery)                   !
Name                 STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
AddressLine4         STRING(30)                            !
TelephoneNumber      STRING(30)                            !
                     END                                   !
locTermsText         STRING(255)                           !
Process:View         VIEW(JOBS)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Authority_Number)
                       PROJECT(job:Consignment_Number)
                       PROJECT(job:Courier)
                       PROJECT(job:Date_Completed)
                       PROJECT(job:Despatch_Number)
                       PROJECT(job:MSN)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:date_booked)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),DOUBLE,CENTER, |
  GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT('ORDERS Report'),AT(250,6792,7760,1417),PRE(RPT),PAPER(PAPER:A4),FONT('Tahoma',8,,FONT:regular, |
  CHARSET:ANSI),THOUS
                       HEADER,AT(250,292,7750,10000),USE(?Header),FONT('Tahoma',8,,FONT:regular)
                         STRING('Job Number:'),AT(5125,375),USE(?STRING1),TRN
                         STRING('Despach Batch No:'),AT(5125,625,1042,167),USE(?STRING1:2),TRN
                         STRING('Date Booked:'),AT(5125,875),USE(?STRING2),TRN
                         STRING('Date Completed:'),AT(5125,1125),USE(?STRING3),TRN
                         STRING(@s8),AT(6250,625),USE(job:Despatch_Number),LEFT(1),TRN
                         STRING(@d6b),AT(6250,875),USE(job:date_booked),LEFT,TRN
                         STRING(@D6b),AT(6250,1125),USE(job:Date_Completed),LEFT,TRN
                         STRING(@s30),AT(167,1708,2250),USE(invoice:Name),TRN
                         STRING(@s30),AT(167,1875,2208),USE(invoice:AddressLine1),TRN
                         STRING(@s30),AT(167,2042,2208),USE(invoice:AddressLine2),TRN
                         STRING(@s30),AT(167,2208,2250),USE(invoice:AddressLine3),TRN
                         STRING(@s30),AT(167,2375,2208),USE(invoice:AddressLine4),TRN
                         STRING('Tel:'),AT(2500,1875),USE(?STRING4),TRN
                         STRING('Fax:'),AT(2500,2042,302,167),USE(?STRING4:2),TRN
                         STRING('Mobile:'),AT(2500,2208,417,167),USE(?STRING4:3),TRN
                         STRING(@s30),AT(2958,1875,927),USE(invoice:TelephoneNumber),TRN
                         STRING(@s30),AT(2958,2042,927),USE(invoice:FaxNumber),TRN
                         STRING(@s30),AT(2958,2208,927),USE(invoice:MobileNumber),TRN
                         STRING(@s30),AT(4125,1708,2250),USE(delivery:Name),TRN
                         STRING(@s30),AT(4125,1875,2250),USE(delivery:AddressLine1),TRN
                         STRING(@s30),AT(4125,2042,2250),USE(delivery:AddressLine2),TRN
                         STRING(@s30),AT(4125,2208,2250),USE(delivery:AddressLine3),TRN
                         STRING(@s30),AT(4125,2375,2250),USE(delivery:AddressLine4),TRN
                         STRING('Tel:'),AT(4125,2542,198,167),USE(?STRING4:4),TRN
                         STRING(@s30),AT(4375,2542,2250),USE(delivery:TelephoneNumber),TRN
                         STRING(@s30),AT(6250,375,1375),USE(locFullJobNumber),TRN
                         STRING(@s15),AT(156,3406),USE(job:Account_Number),LEFT,TRN
                         STRING(@s30),AT(1677,3406,1125),USE(job:Order_Number),LEFT,TRN
                         STRING(@s30),AT(3187,3406,1323),USE(job:Authority_Number),LEFT,TRN
                         STRING(@s30),AT(4698,3406,1437),USE(locChargeType),TRN
                         STRING(@s30),AT(6208,3406,1500),USE(locRepairType),TRN
                         STRING(@s30),AT(156,4104,1344),USE(job:Model_Number),LEFT,TRN
                         STRING(@s30),AT(1562,4104,1323),USE(job:Manufacturer),LEFT,TRN
                         STRING(@s30),AT(2958,4104,1042),USE(job:Unit_Type),LEFT,TRN
                         STRING(@s20),AT(4115,4104,1042),USE(locOriginalIMEI),LEFT,TRN
                         STRING(@s30),AT(5219,4104,1125),USE(locFinalIMEI),HIDE,TRN
                         STRING(@s20),AT(6479,4104,1187),USE(job:MSN),LEFT,TRN
                         STRING('REPORTED FAULT'),AT(167,4333),USE(?STRING5),FONT(,,,FONT:bold)
                         TEXT,AT(1490,4333,5948,427),USE(jbn:Fault_Description),TRN
                         STRING('ENGINEER REPORT'),AT(167,4792),USE(?STRING6),FONT(,,,FONT:bold)
                         TEXT,AT(1500,4792,5948,677),USE(locEngineerReport),TRN
                         STRING('ACCESSORIES'),AT(167,5500),USE(?STRING7),FONT(,,,FONT:bold)
                         TEXT,AT(1500,5500,5948,302),USE(locAccessories),FONT(,,,FONT:regular+FONT:underline)
                         STRING('EXCHANGE UNIT'),AT(167,5833),USE(?strExchangeUnit),FONT(,,,FONT:bold),HIDE
                         STRING(@s100),AT(1500,5833,5948),USE(locLoanExchangeUnit),HIDE,TRN
                         STRING('PARTS USED'),AT(167,6292),USE(?STRING9),FONT(,,,FONT:bold)
                         STRING('Qty'),AT(1500,6250),USE(?STRING10),FONT(,,,FONT:bold)
                         STRING('Part Number'),AT(2000,6250),USE(?STRING11),FONT(,,,FONT:bold)
                         STRING('ID NUMBER'),AT(167,6083),USE(?STRING12),FONT(,,,FONT:bold)
                         STRING(@s20),AT(1500,6042),USE(locIDNumber),TRN
                         STRING('Description'),AT(3750,6250),USE(?STRING13),FONT(,,,FONT:bold)
                         STRING('Unit Cost'),AT(5833,6250),USE(?STRING14),FONT(,,,FONT:bold)
                         STRING('Line Cost'),AT(6792,6250),USE(?STRING15),FONT(,,,FONT:bold)
                         LINE,AT(1510,6469,5875,0),USE(?LINE1)
                         LINE,AT(1510,8385,5875,0),USE(?lineTerms)
                         STRING('Customer Signature'),AT(167,8208),USE(?strTerms2),FONT(,,,FONT:bold)
                         STRING('Date'),AT(6000,8208),USE(?strTerms3),FONT(,,,FONT:bold)
                         STRING('Courier:'),AT(167,8917,792,167),USE(?STRING21:2),TRN
                         STRING(@s30),AT(1083,8917,1625),USE(job:Courier),FONT(,,,FONT:bold),TRN
                         STRING('Despatch Date:'),AT(167,8750),USE(?ReportDatePrompt),TRN
                         STRING('<<-- Date Stamp -->'),AT(1083,8750),USE(?ReportDateStamp),FONT(,,,FONT:bold),TRN
                         STRING('Consignment No:'),AT(167,9083),USE(?STRING21),TRN
                         STRING(@s30),AT(1083,9083,1625,167),USE(job:Consignment_Number),FONT(,,,FONT:bold),TRN
                         STRING('Loan Replacement Value:'),AT(167,9333),USE(?strLoanReplacementValue),HIDE
                         STRING(@n10.2),AT(1500,9333),USE(locLoanReplacementValue),FONT(,,,FONT:bold),RIGHT(2),HIDE, |
  TRN
                         STRING('Vodacom Repairs Loan Phone Terms And Conditions'),AT(167,9844),USE(?strLoanTerms), |
  FONT(,7,,FONT:bold+FONT:underline),HIDE
                         STRING('Labour:'),AT(5458,8750),USE(?strLabour),TRN
                         STRING('Parts:'),AT(5458,8917),USE(?strParts),TRN
                         STRING('Carriage:'),AT(5458,9083),USE(?strCarriage),TRN
                         STRING('V.A.T.:'),AT(5458,9250),USE(?strVAT),TRN
                         STRING('Total:'),AT(5458,9500),USE(?strTotal),FONT(,,,FONT:bold),TRN
                         LINE,AT(6344,9469,1000,0),USE(?LINE2)
                         STRING(@n14.2b),AT(6292,8750),USE(locLabour),RIGHT(2),TRN
                         STRING(@n14.2b),AT(6292,8917),USE(locParts),RIGHT(2),TRN
                         STRING(@n14.2b),AT(6292,9083),USE(locCarriage),RIGHT(2),TRN
                         STRING(@n14.2b),AT(6292,9250),USE(locVAT),RIGHT(2),TRN
                         STRING(@n14.2b),AT(6156,9500),USE(locTotal),FONT(,,,FONT:bold),RIGHT(2),TRN
                         STRING(@s20),AT(2573,8750),USE(locBarCodeJobNumber),FONT('C39 High 12pt LJ3',12),CENTER
                         STRING(@s20),AT(2573,9167,2594,198),USE(locBarCodeIMEINumber),FONT('C39 High 12pt LJ3',12), |
  CENTER
                         STRING(@s30),AT(2729,8917),USE(locDisplayJobNumber),FONT(,,,FONT:bold),CENTER
                         STRING(@s30),AT(2729,9333),USE(locDisplayIMEINumber),FONT(,,,FONT:bold),CENTER
                         TEXT,AT(156,7917,5885,260),USE(locTermsText),FONT(,7)
                       END
Detail                 DETAIL,AT(0,0,7750,208),USE(?Detail)
                         STRING(@n-14b),AT(667,0),USE(locQuantity),RIGHT(2)
                         STRING(@s30),AT(2000,0),USE(locPartNumber)
                         STRING(@s30),AT(3750,0),USE(locDescription)
                         STRING(@n14.2b),AT(5115,0),USE(locUnitCost),RIGHT(2)
                         STRING(@n14.2b),AT(6229,0),USE(locLineCost),RIGHT(2)
                       END
NewPage                DETAIL,AT(0,0,7750,83),USE(?detNewPage),PAGEAFTER(1)
                       END
                       FOOTER,AT(260,10208,7750,1167),USE(?Footer)
                         TEXT,AT(125,42,7458,1000),USE(stt:Text)
                       END
                       FORM,AT(250,250,7750,11188),USE(?Text:CurrencyItemCost:2),FONT('Tahoma',8,,FONT:regular)
                         STRING('DESPATCH NOTE'),AT(3833,-42,3792,240),USE(?strTitle),FONT(,14,,FONT:bold),RIGHT,TRN
                         STRING(@s40),AT(104,0,4167,260),USE(address:SiteName),FONT(,14,,FONT:bold),LEFT
                         STRING(@s40),AT(104,313,3073,208),USE(address:Name2),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,208,3073,208),USE(address:Name),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,417,2760,208),USE(address:Location),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING('REG NO:'),AT(104,573),USE(?stringREGNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(625,573,1667,208),USE(address:RegistrationNo),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('VAT NO: '),AT(104,677),USE(?stringVATNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s20),AT(625,677,1771,156),USE(address:VATNumber),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,833,2240,156),USE(address:AddressLine1),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,938,2240,156),USE(address:AddressLine2),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1042,2240,156),USE(address:AddressLine3),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1146),USE(address:AddressLine4),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('TEL:'),AT(104,1250),USE(?stringTEL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(469,1250,1458,208),USE(address:Telephone),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('FAX:'),AT(2083,1250,313,208),USE(?stringFAX),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(2396,1250,1510,188),USE(address:Fax),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('EMAIL:'),AT(104,1354,417,208),USE(?stringEMAIL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s255),AT(469,1354,3333,208),USE(address:EmailAddress),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('JOB DETAILS'),AT(5000,208),USE(?String57),FONT(,8,,FONT:bold),TRN
                         STRING('CUSTOMER ADDRESS'),AT(156,1510),USE(?String24),FONT(,8,,FONT:bold),TRN
                         STRING('DELIVERY ADDRESS'),AT(4115,1510,1677,156),USE(?DeliveryAddress),FONT(,8,,FONT:bold), |
  TRN
                         STRING('GENERAL DETAILS'),AT(156,2990),USE(?String91),FONT(,8,,FONT:bold),TRN
                         STRING('DELIVERY DETAILS'),AT(156,8563),USE(?String73),FONT(,8,,FONT:bold),TRN
                         STRING('CHARGE DETAILS'),AT(5365,8563),USE(?String74),FONT(,8,,FONT:bold),TRN
                         STRING('REPAIR DETAILS'),AT(156,3667),USE(?String50),FONT(,8,,FONT:bold),TRN
                         BOX,AT(5000,365,2646,1042),USE(?Box:TopDetails),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,1667,3604,1302),USE(?Box:Address1),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4063,1667,3604,1302),USE(?Box:Address2),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3125,1510,260),USE(?Box:Title1),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(1615,3125,1510,260),USE(?Box:Title2),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(3125,3125,1510,260),USE(?Box:Title3),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4635,3125,1510,260),USE(?Box:Title4),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(6146,3125,1510,260),USE(?Box:Title5),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3385,1510,260),USE(?Box:Title1a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(1615,3385,1510,260),USE(?Box:Title2a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(3125,3385,1510,260),USE(?Box:Title3a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(4635,3385,1510,260),USE(?Box:Title4a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(6146,3385,1510,260),USE(?Box:Title5a),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,3802,7552,260),USE(?Box:Heading),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,4063,7552,4479),USE(?Box:Detail),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1), |
  ROUND
                         BOX,AT(104,8698,2344,1042),USE(?Box:Total1),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         BOX,AT(5313,8698,2344,1042),USE(?Box:Total2),COLOR(COLOR:Black),FILL(00C2E3F3h),LINEWIDTH(1), |
  ROUND
                         STRING('Account Number'),AT(156,3156,1042,167),USE(?String91:2),FONT(,8,,FONT:bold),TRN
                         STRING('Order Number'),AT(1677,3156,1042,167),USE(?String91:3),FONT(,8,,FONT:bold),TRN
                         STRING('Authority Number'),AT(3187,3156,1177,167),USE(?String91:4),FONT(,8,,FONT:bold),TRN
                         STRING('Chargeable Type'),AT(4698,3156,1042,167),USE(?strChargeableType),FONT(,8,,FONT:bold), |
  TRN
                         STRING('Chargeable Repair Type'),AT(6208,3156,1437),USE(?strRepairType),FONT(,8,,FONT:bold), |
  TRN
                         STRING('Model'),AT(156,3833,1042,167),USE(?String91:7),FONT(,8,,FONT:bold),TRN
                         STRING('Make'),AT(1562,3833,1042,167),USE(?String91:8),FONT(,8,,FONT:bold),TRN
                         STRING('Unit Type'),AT(2958,3833,1042,167),USE(?String91:9),FONT(,8,,FONT:bold),TRN
                         STRING('I.M.E.I. Number'),AT(4115,3833,1042,167),USE(?strIMEINumber),FONT(,8,,FONT:bold),TRN
                         STRING('Exch IMEI No'),AT(5219,3833,1042,167),USE(?strExchangeIMEINumber),FONT(,8,,FONT:bold), |
  HIDE,TRN
                         STRING('M.S.N.'),AT(6479,3833,1042,167),USE(?String91:12),FONT(,8,,FONT:bold),TRN
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR1               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END

                    MAP
ShowHidePrices  PROCEDURE(LONG pHide)
                    END ! MAP

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('DespatchNote')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:DEFAULT2.Open                                     ! File DEFAULT2 used by this procedure, so make sure it's RelationManager is open
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:EXCHANGE.Open                                     ! File EXCHANGE used by this procedure, so make sure it's RelationManager is open
  Relate:INVOICE.SetOpenRelated()
  Relate:INVOICE.Open                                      ! File INVOICE used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:JOBTHIRD.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOAN.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBOUTFL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBACC.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:WARPARTS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PARTS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBNOTES.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
        ! Load Data Files
        locJobNumber = p_web.GSV('locJobNumber')
  
        SET(DEFAULT2)
        Access:DEFAULT2.Next()
        SET(DEFAULTS)
        Access:DEFAULTS.Next()
  
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = locJobNumber
        IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        
        ELSE ! IF
        END ! IF
  
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
        
        ELSE ! IF
        END ! IF
  
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
        
        ELSE ! IF
        END ! IF
  
        Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
        jbn:RefNumber = job:Ref_Number
        IF (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
        
        ELSE ! IF
        END ! IF  
    
  Do DefineListboxStyle
  INIMgr.Fetch('DespatchNote',ProgressWindow)              ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisReport.AddSortOrder(job:Ref_Number_Key)
  ThisReport.AddRange(job:Ref_Number,locJobNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:INVOICE.Close
    Relate:STANTEXT.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('DespatchNote',ProgressWindow)           ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'DespatchNote',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
  !region Get Trade Defaults
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = wob:HeadAccountNumber
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            delivery:Name = tra:Company_Name
            delivery:AddressLine1 = tra:Address_Line1
            delivery:AddressLine2 = tra:Address_Line2
            delivery:AddressLine3 = tra:Address_Line3
            delivery:AddressLine4 = tra:Postcode
            delivery:TelephoneNumber = tra:Telephone_Number
          
            IF (tra:Num_Despatch_Note_Copies > 0)
                noOfCopies = tra:Num_Despatch_Note_Copies
                
            END ! IF
            IF (tra:Price_Despatch = 'YES')
                priceDespatch = TRUE
                IF (tra:Price_First_Copy_Only = TRUE)
                    priceFirstCopy = TRUE
                END ! IF
            END ! IF        
        ELSE ! IF
        END ! IF
      
        locFullJobNumber = job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
      
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        If (p_web.GSV('BookingSite') = 'RRC')
            tra:Account_Number = p_web.GSV('wob:HeadAccountNumber')
        ELSE ! If (glo:WebJob = 1)
            tra:Account_Number = GETINI('BOOKING','HeadAccount','AA20',CLIP(PATH())&'\SB2KDEF.INI')
        END ! If (glo:WebJob = 1)
        If (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            address:SiteName        = tra:Company_Name
            address:Name            = tra:coTradingName
            address:Name2           = tra:coTradingName2  ! #12079 New address fields. (Bryan: 13/04/2011)
            address:Location        = tra:coLocation
            address:RegistrationNo  = tra:coRegistrationNo
            address:AddressLine1    = tra:coAddressLine1
            address:AddressLine2    = tra:coAddressLine2
            address:AddressLine3    = tra:coAddressLine3
            address:AddressLine4    = tra:coAddressLine4
            address:Telephone       = tra:coTelephoneNumber
            address:Fax             = tra:coFaxNumber
            address:EmailAddress    = tra:coEmailAddress
            address:VatNumber       = tra:coVATNumber
        END    
      
      
      ! Get Report Data
      
        IF (job:Warranty_Job = 'YES' AND job:Chargeable_Job <> 'YES')
            locChargeType = job:Warranty_Charge_Type
            locRepairType = job:Repair_Type_Warranty
        ELSE ! IF
            locChargeType = job:Charge_Type
            locRepairType = job:Repair_Type    
        END ! IF
      
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
          
        ELSE ! IF
        END ! IF
      
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          
        ELSE ! IF
        END ! IF
  
        IF (tra:Invoice_Sub_Accounts = 'YES')
            IF (sub:UseCustDespAdd = 'YES')
                invoice:AddressLine1 = job:Address_Line1
                invoice:AddressLine2 = job:Address_Line2
                invoice:AddressLine3 = job:Address_Line3
                invoice:AddressLine4 = job:Postcode
                invoice:Name = job:Company_Name
                invoice:TelephoneNumber = job:Telephone_Number
                invoice:FaxNumber = job:Fax_Number
            
            ELSE
                invoice:AddressLine1 = sub:Address_Line1
                invoice:AddressLine2 = sub:Address_Line2
                invoice:AddressLine3 = sub:Address_Line3
                invoice:AddressLine4 = sub:Postcode
                invoice:TelephoneNumber = sub:Telephone_Number
                invoice:Name = sub:Company_Name
                invoice:FaxNumber = sub:Fax_Number
            END        
        ELSE ! IF
            IF (tra:UseCustDespAdd = 'YES')
                invoice:AddressLine1 = job:Address_Line1
                invoice:AddressLine2 = job:Address_Line2
                invoice:AddressLine3 = job:Address_Line3
                invoice:AddressLine4 = job:Postcode
                invoice:Name = job:Company_Name
                invoice:TelephoneNumber = job:Telephone_Number
                invoice:FaxNumber = job:Fax_Number        
            ELSE
                invoice:AddressLine1 = tra:Address_Line1
                invoice:AddressLine2 = tra:Address_Line2
                invoice:AddressLine3 = tra:Address_Line3
                invoice:AddressLine4 = tra:Postcode
                invoice:TelephoneNumber = tra:Telephone_Number
                invoice:Name = tra:Company_Name
                invoice:FaxNumber = tra:Fax_Number
            END        
        END ! IF
      
        locIDNumber = jobe2:IDNumber
    
        IF (job:Title = '')
            locCustomerName = job:Initial
        ELSIF (job:Initial = '')
            locCustomerName = job:Title
        ELSE
            locCustomerName = CLIP(job:Title) & ' ' & CLIP(job:Initial)
        END
    
        IF (locCustomerName = '')
            locCustomerName = job:Surname
        ELSIF(job:Surname = '')
        ELSE
            locCustomerName = CLIP(locCustomerName) & ' ' & CLIP(locCustomerName)
        END
    
        locEndUserTelNo = CLIP(jobe:EndUserTelNo)
    
        IF (locCustomerName <> '')
            locClientName = 'Client: ' & CLIP(locCustomerName) & '    Tel: ' & CLIP(locEndUserTelNo)
        ELSE
            IF (locEndUserTelNo <> '')
                locClientName = 'Tel: ' & CLIP(locEndUserTelNo)
            END
        END
    
        IF NOT (p_web.GSV('BookingSite') <> 'RRC' AND jobe:WebJob = 1)
            delivery:Name = job:Company_Name_Delivery
            delivery:AddressLine1 = job:Address_Line1_Delivery
            delivery:AddressLine2 = job:Address_Line2_Delivery
            delivery:AddressLine3 = job:Address_Line3_Delivery
            delivery:AddressLine4 = job:Postcode_Delivery
            delivery:TelephoneNumber = job:Telephone_Delivery
        END
    
        IF (job:Chargeable_Job = 'YES')
        
            IF (job:Invoice_Number <> '')
                Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
                inv:Invoice_Number = job:Invoice_Number
                IF (Access:INVOICE.TryFetch(inv:INvoice_Number_Key))
                END
        
                IF (p_web.GSV('BookingSite') = 'RRC')
                    IF (inv:ExportedRRCOracle)
                        locLabour = jobe:InvRRCCLabourCost
                        locParts = jobe:InvRRCCPartsCost
                        locCarriage = job:Invoice_Courier_Cost
                        locVAT = (jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour / 100) + |
                            (jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts / 100) + |
                            (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour / 100)
                    ELSE
                        locLabour = jobe:RRCCLabourCost
                        locParts = jobe:RRCCPartsCost
                        locCarriage = job:Courier_Cost
                        locVAT = (jobe:RRCCLabourCost * GetVATRate(job:Account_Number, 'L') / 100) + |
                            (jobe:RRCCPartsCost * GetVATRate(job:Account_Number, 'P') / 100) + |
                            (job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100)
                    END
                ELSE
                    locLabour = job:Invoice_Labour_Cost
                    locParts = job:Invoice_Parts_Cost
                    locCarriage = job:Invoice_Courier_Cost
                    locVAT = (job:Invoice_Labour_Cost * inv:Vat_Rate_Labour / 100) + |
                        (job:Invoice_Parts_Cost * inv:Vat_Rate_Parts / 100) + |
                        (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour / 100)
               
                END
        
            ELSE
                IF (p_web.GSV('BookingSite') = 'RRC')
                    locLabour = jobe:RRCCLabourCost
                    locParts = jobe:RRCCPartsCost
                    locCarriage = job:Courier_Cost
                    locVAT = (jobe:RRCCLabourCost * GetVATRate(job:Account_Number, 'L') / 100) + |
                        (jobe:RRCCPartsCost * GetVATRate(job:Account_Number, 'P') / 100) + |
                        (job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100)
                ELSE
                    locLabour = job:Labour_Cost
                    locParts = job:Parts_Cost
                    locCarriage = job:Courier_Cost
                    locVAT = (job:Labour_Cost * GetVATRate(job:Account_Number, 'L') / 100) + |
                        (job:Parts_Cost * GetVATRate(job:Account_Number, 'P') / 100) + |
                        (job:Courier_Cost * GetVATRate(job:Account_Number, 'L') / 100)
                END
        
            END
        
        END
    
        IF (job:Warranty_Job = 'YES')
            IF (job:Invoice_Number_Warranty <> '')
                Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
                inv:Invoice_Number = job:Invoice_Number_Warranty
                IF (Access:INVOICE.TryFetch(inv:Invoice_NUmber_Key))
                END
            
                IF (p_web.GSV('BookingSite') = 'RRC')
                    locLabour       = jobe:InvRRCWLabourCost
                    locParts        = jobe:InvRRCWPartsCost
                    locCarriage = job:WInvoice_Courier_Cost
                    locVAT          = (jobe:InvRRCWLabourCost * inv:Vat_Rate_Labour / 100) + |
                        (jobe:InvRRCWPartsCost * inv:Vat_Rate_Parts / 100) + |
                        (job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour / 100)   
                ELSE
                    locLabour       = jobe:InvoiceClaimValue
                    locParts        = job_ali:Winvoice_parts_cost
                    locCarriage = job_ali:Winvoice_courier_cost
                    locVAT          = (jobe:InvoiceClaimValue * inv:Vat_Rate_Labour / 100) + |
                        (job:WInvoice_Parts_Cost * inv:Vat_Rate_Parts / 100) +                 |
                        (job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour / 100)
                END
            
            ELSE
                IF (p_web.GSV('BookingSite') = 'RRC')
                    locLabour       = jobe:RRCWLabourCost
                    locParts        = jobe:RRCWPartsCost
                    locCarriage = job_ali:Courier_Cost_Warranty
                    locVAT          = (jobe:RRCWLabourCost * GetVATRate(job:Account_Number, 'L') / 100) + |
                        (jobe:RRCWPartsCost * GetVATRate(job:Account_Number, 'P') / 100) + |
                        (job:Courier_Cost_Warranty * GetVATRate(job:Account_Number, 'L') / 100)
    
                ELSE
                    locLabour       = jobe:ClaimValue
                    locParts        = job_ali:Parts_Cost_Warranty
                    locCarriage = job_ali:Courier_Cost_Warranty
                    locVAT          = (jobe:ClaimValue * GetVATRate(job:Account_Number, 'L') / 100) + |
                        (job:Parts_Cost_Warranty * GetVATRate(job:Account_Number, 'P') / 100) +         |
                        (job:Courier_Cost_Warranty * GetVATRate(job:Account_Number, 'L') / 100)
    
                END
            
            END
        
        
        END
    
        locTotal = locLabour + locParts + locCarriage + locVAT
    
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = job:Despatch_User
        IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            locDespatchUser = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
        END
  !endregion  
  !region Accessories & Engineer Report
        locAccessories = ''
        Access:JOBACC.ClearKey(jac:Ref_Number_Key)
        jac:Ref_Number = job:Ref_Number
        SET(jac:Ref_Number_Key,jac:Ref_Number_Key)
        LOOP UNTIL Access:JOBACC.Next()
            IF (jac:Ref_Number <> job:Ref_Number)
                BREAK
            END
      
            IF (locAccessories = '')
                locAccessories = jac:Accessory
            ELSE
                locAccessories = CLIP(locAccessories) & ', ' & CLIP(jac:Accessory)
            END
        END
  
        FREE(qOutfaults)
        Access:JOBOUTFL.ClearKey(joo:LevelKey)
        joo:JobNumber = job:Ref_Number
        SET(joo:LevelKey,joo:LevelKey)
        LOOP UNTIL Access:JOBOUTFL.Next()
            IF (joo:JobNumber <> job:Ref_Number)
                BREAK
            END
            qOutfaults.Description = joo:Description
            GET(qOutfaults,qOutfaults.Description)
            IF (ERROR())
                locEngineerReport = CLIP(locEngineerReport) & ' ' & CLIP(joo:Description)
                qOutFaults.Description = joo:Description
                Add(qOutFaults)
            END
        END
        FREE(qOutfaults)
  !endregion  
  !region Exchange/Loan/Third Party Note
        Access:STANTEXT.ClearKey(stt:Description_Key)
        stt:Description = 'DESPATCH NOTE'
        IF (Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign)
        
        ELSE ! IF
        END ! IF
    
        IF (job:Loan_Unit_Number > 0)
            SETTARGET(Report)
            ?strExchangeUnit{prop:Text} = 'LOAN UNIT'
            locIDNumber = jobe2:LoanIDNumber
            ?strTitle{prop:Text} = 'LOAN DESPATCH NOTE'
            ?strExchangeUnit{prop:Hide} = 0
            ?locLoanExchangeUnit{prop:Hide} = 0
        
            Access:LOAN.ClearKey(loa:Ref_Number_Key)
            loa:Ref_Number = job:Loan_Unit_Number
            IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
                locLoanExchangeUnit = '(' & loa:Ref_Number & ') ' & CLIP(loa:Manufacturer) & | 
                    ' ' & CLIP(loa:Model_Number) & ' - ' & CLIP(loa:ESN)
            ELSE ! IF
            END ! IF
            SETTARGET()
            Access:STANTEXT.ClearKey(stt:Description_Key)
            stt:Description = 'LOAN DESPATCH NOTE'
            IF (Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign)
            
            ELSE ! IF
            END ! IF
        END ! IF
    
        locOriginalIMEI = job:ESN
    
        ! Send To Third Party?
        foundThirdPartyIMEI = FALSE
        IF (job:Third_Party_Site <> '')
            Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
            jot:RefNumber = job:Ref_Number
            SET(jot:RefNumberKey,jot:RefNumberKey)
            LOOP UNTIL Access:JOBTHIRD.Next() <> Level:Benign
                IF (jot:RefNumber <> job:Ref_Number)
                    BREAK
                END ! IF
                IF (jot:OriginalIMEI <> job:ESN)
                    foundThirdPartyIMEI = TRUE
                END ! IF
            END ! LOOP
        END ! IF
    
        IF (foundThirdPartyIMEI = TRUE)
            SETTARGET(REPORT)
            locOriginalIMEI = jot:OriginalIMEI
            locFinalIMEI = job:ESN
            ?strIMEINumber{prop:Text} = 'Orig I.M.E.I. Number'
            ?strExchangeIMEINumber{prop:Hide} = 0
            ?locFinalIMEI{prop:Hide} = 0
            SETTARGET()
        END ! IF
        
    
        exchangeNote = FALSE
    
        IF (job:Exchange_Unit_Number > 0)
            exchangeNote = TRUE
        END ! IF
    
    ! Look for Exchange Part
        IF (exchangeNote = FALSE)
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number = job:Ref_Number
            SET(par:Part_Number_Key,par:Part_Number_Key)
            LOOP UNTIL Access:PARTS.Next() <> Level:Benign
                IF (par:Ref_Number <> job:Ref_Number)
                    BREAK
                END ! IF
                Access:STOCK.ClearKey(sto:Ref_Number_Key)
                sto:Ref_Number = par:Part_Ref_Number
                IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                    IF (sto:ExchangeUnit = 'YES')
                        exchangeNote = TRUE
                        BREAK
                    END ! IF
                ELSE ! IF
                END ! IF
            END ! LOOP
        END ! IF
        IF (exchangeNote = FALSE)
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number = job:Ref_Number
            SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
            LOOP UNTIL Access:WARPARTS.Next() <> Level:Benign
                IF (wpr:Ref_Number <> job:Ref_Number)
                    BREAK
                END ! IF
                Access:STOCK.ClearKey(sto:Ref_Number_Key)
                sto:Ref_Number = wpr:Part_Ref_Number
                IF (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                    IF (sto:ExchangeUnit = 'YES')
                        exchangeNote = TRUE
                        BREAK
                    END ! IF
                ELSE ! IF
                END ! IF
            END ! LOOP
        END ! IF    
    
        IF (exchangeNote = TRUE)
        ! Don't put exchange unit repair on a despatch note if it's failed assessment
            IF (jobe:WebJob = 1 AND p_web.GSV('BookingSite') <> 'RRC' AND jobe:Engineer48HourOption = 1)
                IF (PassExchangeAssessment(job:Ref_Number) = FALSE)
                    exchangeNote = FALSE
                END ! IF
            END ! IF
        END ! IF    
    
        IF (exchangeNote = TRUE)
            SETTARGET(REPORT)
            ?strExchangeUnit{prop:Text} = 'EXCHANGE UNIT'
            ?strTitle{prop:Text} = 'EXCHANGE DESPATCH NOTE'
            ?locLoanExchangeUnit{prop:Hide} = 0
            ?strExchangeUnit{prop:Hide} = 0
            ?locPartNumber{prop:Hide} = 1
            ?locDescription{prop:Hide} = 1
            ?locQuantity{prop:Hide} = 1
            ?locUnitCost{prop:Hide} = 1
            ?locLineCost{prop:Hide} = 1
            Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
            xch:Ref_Number = job:Exchange_Unit_Number
            IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                locLoanExchangeUnit = '(' & Clip(xch:Ref_number) & ') ' & CLip(xch:manufacturer) & ' ' & CLip(xch:model_number) & |
                    ' - ' & CLip(xch:esn)
                locOriginalIMEI = job:ESN
                locFinalIMEI = xch:ESN
                ?locFinalIMEI{PROP:Hide} = 0
                ?strExchangeIMEINumber{PROP:Hide} = 0        
                
            END ! IF
            SETTARGET()
        END ! IF
        
  !endregion
  !region Terms
  !        locTerms = ''
  !        Access:STANTEXT.ClearKey(stt:Description_Key)
  !        stt:Description = 'DESPATCH NOTE'
  !        IF (Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign)
        ! #13581 Do not need to lookup standard text again (DBH: 11/08/2015)
        
            
  !        END
  
        locTerms = stt:Text
        
        IF (job:Loan_Unit_Number <> '' AND job:Exchange_Unit_Number = 0)
            SETTARGET(Report)
            ?locLoanReplacementValue{PROP:Hide} = 0
            ?strLoanReplacementValue{PROP:Hide} = 0
            locLoanReplacementValue = jobe:LoanReplacementValue
            ?strLoanTerms{PROP:Hide} = 0
            !?strTerms1{PROP:Hide} = 0
            ?strTerms2{PROP:Hide} = 0
            ?strTerms3{PROP:Hide} = 0
            ?lineTerms{PROP:Hide} = 0
            SETTARGET()
            locTerms = '1) The loan phone and its accessories remain the property of Vodacom Service Provider Company and is given to the customer to use while his / her phone is being repaired. It is the customers responsibility to return the loan phone and its accessories in proper working condition.<13,10>'
            locTerms = Clip(locTerms) & '2) In the event of damage to the loan phone and/or its accessories, the customer will be liable for any costs incurred to repair or replace the loan phone and/or its accessories. <13,10>'
            locTerms = Clip(locTerms) & '3) In the event of loss or theft of the loan phone and/or its accessories the customer will be liable for the replacement cost of the loan phone and/or its accessories or he/she may replace the lost/stolen phone and/or its accessories with new ones of the same or similar make and model, or same replacement value.<13,10>'
            locTerms = Clip(locTerms) & '4) Any phone replaced by the customer must be able to operate on the Vodacom network. <13,10>'
            locTerms = Clip(locTerms) & '5) The customers repaired phone will not be returned until the loan phone and its accessories has been returned, repaired or replaced. '
        END
        locTermsText = p_web.GSV('Default:TermsText')
  !endregion  
  !region Barcode Bit
        locBarCodeJobNumber = '*' & job:Ref_Number & '*'
        locBarCodeIMEINumber = '*' & job:ESN & '*'
        locDisplayIMEINumber = 'I.M.E.I. No: ' & job:ESN
        locDisplayJobNumber = 'Job No: ' & job:Ref_Number
  !endregion  
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.Init(Report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,Report{PROPPRINT:Paper},PAPER:USER), CHOOSE(Report{PROP:Thous}=True,PROP:Thous,CHOOSE(Report{PROP:MM}=True,PROP:MM,CHOOSE(Report{PROP:Points}=True,PROP:Points,0))), Report{PROPPRINT:PaperWidth}, Report{PROPPRINT:PaperHeight}, Report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetTitle('Despatch Note')            !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR1.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetViewerPrefs(PDFXTR1:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetFontEmbedding(True, True, False, False, False)
    PDFXTR1.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@d06)
  END
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'DespatchNote',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR1.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !report modifications go in here ...
      !hide despatch number if zero
      If job:Despatch_Number = 0 THEN
          !TB13096 don't show the despatch number (0) or the title- JC 04/109/13
          SETTARGET(Report)
          ?String1:2{PROP:Hide} = 1
          ?job:Despatch_Number{PROP:Hide} = 1
          SETTARGET()
      END !if despatch number exists
  
      IF (job:Warranty_Job = 'YES' AND job:Chargeable_Job <> 'YES')
          SETTARGET(Report)
          ?strChargeableType{PROP:Text} = 'Warranty Type'
          ?strRepairType{PROP:Text} = 'Warranty Repair Type'
          SETTARGET()
      END
  
      !hide prices if defaults set
      IF (tra:Price_Despatch <> 'YES' AND sub:PriceDespatchNotes <> 1)
          SETTARGET(Report)
          ?locUnitCost{PROP:Hide} = 1
          ?locLineCost{PROP:Hide} = 1
          !TB13096 - also hide the titles - JC 04/109/13
          ?String14{Prop:hide} = 1
          ?String15{Prop:hide} = 1
          !end TB13096     
          ?locLabour{PROP:Hide} = 1
          ?locParts{PROP:Hide} = 1
          ?locCarriage{PROP:Hide} = 1
          ?locVAT{PROP:Hide} = 1
          ?locTotal{PROP:Hide} = 1
          ?strLabour{PROP:Hide} = 1
          ?strParts{PROP:Hide} = 1
          ?strCarriage{PROP:Hide} = 1
          ?strVAT{PROP:Hide} = 1
          ?strTotal{PROP:Hide} = 1
          !TB13096 - also hide the amounts - JC 04/09/13
          ?locLabour{PROP:Hide} = 1
          ?locParts{PROP:Hide} = 1
          ?locCarriage{PROP:Hide} = 1
          ?locVAT{PROP:Hide} = 1
          ?locTotal{PROP:Hide} = 1
          ?LINE2{PROP:Hide} = 1
          ?String74{PROP:Hide} = 1
          !End TB13096
          SETTARGET()
      END
  
  
      IF (hideDespatchAddress# = 1)
          SETTARGET(Report)
          ?delivery:Name{prop:Hide} = 1
          ?delivery:AddressLine1{PROP:Hide} = 1
          ?delivery:AddressLine2{PROP:Hide} = 1
          ?delivery:AddressLine3{PROP:Hide} = 1
          ?delivery:AddressLine4{PROP:Hide} = 1
          ?delivery:TelephoneNumber{prop:Hide} = 1
          SETTARGET()
      END
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue

ShowHidePrices      PROCEDURE(LONG pHide)
    CODE
        SETTARGET(Report)
        ?locUnitCost{PROP:Hide} = pHide
        ?locLineCost{PROP:Hide} = pHide
        ?String14{Prop:hide} = pHide
        ?String15{Prop:hide} = pHide
        ?locLabour{PROP:Hide} = pHide
        ?locParts{PROP:Hide} = pHide
        ?locCarriage{PROP:Hide} = pHide
        ?locVAT{PROP:Hide} = pHide
        ?locTotal{PROP:Hide} = pHide
        ?strLabour{PROP:Hide} = pHide
        ?strParts{PROP:Hide} = pHide
        ?strCarriage{PROP:Hide} = pHide
        ?strVAT{PROP:Hide} = pHide
        ?strTotal{PROP:Hide} = pHide
        ?locLabour{PROP:Hide} = pHide
        ?locParts{PROP:Hide} = pHide
        ?locCarriage{PROP:Hide} = pHide
        ?locVAT{PROP:Hide} = pHide
        ?locTotal{PROP:Hide} = pHide
        ?LINE2{PROP:Hide} = pHide
        SETTARGET()

ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

i                           LONG()
SkipDetails BYTE
  CODE
        LOOP i = 1 TO noOfCopies
            IF (i > 1)
                PRINT(rpt:NewPage)
            END ! IF
            IF (priceDespatch)
                ! Show Prices
                ShowHidePrices(0)
                IF (priceFirstCopy)
                    ! Don't Show Prices
                    priceFirstCopy = 0
                    ShowHidePrices(1)
                END ! IF
            ELSE
                ShowHidePrices(1)
            END ! IF
            
            countParts# = 0
            IF (job:Warranty_Job = 'YES')
                Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                wpr:Ref_Number = job:Ref_Number
                SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
                LOOP UNTIL Access:WARPARTS.Next()
                    IF (wpr:Ref_Number <> job:Ref_Number)
                        BREAK
                    END
                    locPartNumber = wpr:Part_Number
                    locDescription = wpr:Description
                    locQuantity = wpr:Quantity
                    locUnitCost = wpr:Purchase_Cost
                    locLineCost = wpr:Quantity * wpr:Purchase_Cost
                    Print(rpt:Detail)
                    countParts# += 1
                END
            END
  
            !TB13096 - this needs chargeable parts as well as warranty parts
            Access:parts.clearkey(par:Part_Number_Key)
            par:Ref_Number = job:Ref_Number
            set(par:Part_Number_Key,par:Part_Number_Key)
            loop until access:parts.next()
                if par:Ref_Number <> job:Ref_Number then break.
                locPartNumber = par:Part_Number
                locDescription = par:Description
                locQuantity = par:Quantity
                locUnitCost = par:Purchase_Cost
                locLineCost = par:Quantity * par:Purchase_Cost
                Print(rpt:Detail)
                countParts# += 1
            end !loop            
  
            IF (countParts# = 0)
                locPartNumber = ''
                Print(rpt:Detail)
            END
        END
        
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR1:rtn = PDFXTR1.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR1:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

