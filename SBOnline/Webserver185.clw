

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER185.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
IsJobInvoiced        PROCEDURE  (STRING invNumber,NetWebServerWorker p_web,<STRING invType>) ! Declare Procedure
retValue    BYTE(FALSE)
FilesOpened     BYTE(0)

  CODE
!region ProcessedCode
        Do OpenFiles

        LOOP 1 TIMES
            
            Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
            inv:Invoice_Number = invNumber
            IF (Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign)
                IF (invType <> '')
                    ! Only look for certain invoice types
                    IF (inv:Invoice_Type <> invType)
                        retValue = FALSE
                        BREAK
                    END
                END
        
        
                IF (p_web.GSV('BookingSite') = 'RRC')
                    IF (inv:RRCInvoiceDate > 0)
                        retValue = TRUE
                    END
                ELSE ! IF
                    IF (inv:ARCInvoiceDate > 0)
                        retValue = TRUE
                    END
                END ! IF
            ELSE ! IF
            END ! IF
            
        END ! LOOP
        

        Do CloseFiles
		
        RETURN retValue
!endregion
!--------------------------------------
OpenFiles  ROUTINE
  Access:INVOICE.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:INVOICE.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:INVOICE.Close
     FilesOpened = False
  END
