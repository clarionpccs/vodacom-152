

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER227.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ClearStockReceiveList PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
        DO OpenFiles
        STREAM(STOCKRECEIVETMP)

    Access:STOCKRECEIVETMP.Clearkey(stotmp:PartNumberKey)
    stotmp:SessionID = p_web.SessionID
    SET(stotmp:PartNumberKey,stotmp:PartNumberKey)
        LOOP UNTIL Access:STOCKRECEIVETMP.Next()
            IF (stotmp:SessionID <> p_web.SessionID)
                BREAK
            END

            Access:STOCKRECEIVETMP.DeleteRecord(0)
        END
        
        FLUSH(STOCKRECEIVETMP)

    DO CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:STOCKRECEIVETMP.Open                              ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:STOCKRECEIVETMP.UseFile                           ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:STOCKRECEIVETMP.Close
     FilesOpened = False
  END
