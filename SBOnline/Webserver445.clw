

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER445.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
PendingWebOrderReport PROCEDURE (NetWebServerWorker p_web)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
LineCost             DECIMAL(10,2)                         !
TotalLineCost        DECIMAL(10,2)                         !
Address:User_Name    STRING(30)                            !
address:Address_Line1 STRING(30)                           !
address:Address_Line2 STRING(30)                           !
address:Address_Line3 STRING(30)                           !
address:Post_Code    STRING(20)                            !
address:Telephone_Number STRING(20)                        !
address:Fax_No       STRING(20)                            !
address:Email        STRING(50)                            !
Total_No_Of_Lines    LONG                                  !
tmp:printedby        STRING(60)                            !
tmp:TelephoneNumber  STRING(20)                            !
tmp:FaxNumber        STRING(20)                            !
locAccountFilter     STRING(30)                            !
Process:View         VIEW(ORDWEBPR)
                       PROJECT(orw:AccountNumber)
                       PROJECT(orw:Description)
                       PROJECT(orw:ItemCost)
                       PROJECT(orw:PartNumber)
                       PROJECT(orw:Quantity)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

Report               REPORT,AT(396,2802,7521,7740),PRE(RPT),PAPER(PAPER:A4),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(385,531,7521,1802),USE(?unnamed:2)
                         STRING(@s30),AT(52,646),USE(address:Address_Line3),FONT('Arial',8,,,CHARSET:ANSI),TRN
                         STRING(@s20),AT(52,833),USE(address:Post_Code),FONT('Arial',8,,,CHARSET:ANSI),TRN
                         STRING('Date Printed:'),AT(4948,1302),USE(?String16),FONT('Arial',8,,,CHARSET:ANSI),TRN
                         STRING('Email:'),AT(52,1302),USE(?String33),FONT('Arial',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING(@s50),AT(365,1302),USE(address:Email),FONT('Arial',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING(@s20),AT(365,990),USE(address:Telephone_Number),FONT('Arial',8,,,CHARSET:ANSI),TRN
                         STRING('Fax:'),AT(52,1146),USE(?String34),FONT('Arial',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING(@s20),AT(365,1146),USE(address:Fax_No),FONT('Arial',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING('Tel:'),AT(52,990),USE(?String32),FONT('Arial',8,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING('Page Number:'),AT(4948,1458),USE(?String16:2),FONT('Arial',8,,,CHARSET:ANSI),TRN
                         STRING(@N3),AT(6250,1458),USE(?PageNo),FONT(,8,,FONT:bold),PAGENO,TRN
                         STRING(@s30),AT(52,271),USE(address:Address_Line1),FONT('Arial',8,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(52,458),USE(address:Address_Line2),FONT('Arial',8,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(52,0),USE(Address:User_Name),FONT('Arial',14,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('dd/mm/yyyy'),AT(5792,1292),USE(?ReportDateStamp),FONT(,8,,FONT:bold),TRN
                         STRING('Ordered:'),AT(4958,917),USE(?ReportDatePrompt),FONT(,8),TRN
                         STRING('dd/mm/yyyy'),AT(5792,917),USE(?ReportDateStamp:2),FONT(,8,,FONT:bold),TRN
                         STRING('hh:mm'),AT(6542,917),USE(?ReportTimeStamp),FONT(,8,,FONT:bold),TRN
                       END
DETAIL                 DETAIL,AT(0,0,,167),USE(?DetailBand)
                         STRING(@s8),AT(104,0),USE(orw:Quantity),FONT('Arial',8,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(781,0),USE(orw:PartNumber),FONT('Arial',8,,,CHARSET:ANSI),TRN
                         STRING(@n14.2),AT(4740,0),USE(orw:ItemCost),FONT('Arial',8),RIGHT(1),TRN
                         STRING(@n-14.2),AT(5781,0),USE(LineCost),FONT('Arial',8),RIGHT,TRN
                         STRING(@s30),AT(2760,0,,208),USE(orw:Description),FONT('Arial',8),TRN
                       END
                       FOOTER,AT(385,10562,7521,417),USE(?unnamed:3)
                         LINE,AT(125,31,2146,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING('Total'),AT(4385,94),USE(?String28),FONT('Arial',8,,FONT:bold),TRN
                         LINE,AT(4385,42,2250,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING(@n-14.2),AT(5792,94),USE(TotalLineCost),FONT('Arial',8,,FONT:bold),RIGHT,TRN
                         STRING('Total Number Of Lines:'),AT(115,83),USE(?String29),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING(@n-14),AT(1635,83),USE(Total_No_Of_Lines),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),RIGHT(1), |
  TRN
                       END
                       FORM,AT(365,510,7521,10802),USE(?unnamed)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,10802),USE(?Image1)
                         STRING('STOCK ORDER'),AT(5781,52),USE(?String3),FONT('Arial',16,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Quantity'),AT(104,1979),USE(?String5),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Part Number'),AT(781,1979),USE(?String6),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Description'),AT(2760,1979),USE(?String31),FONT('Arial',8,,FONT:bold),TRN
                         STRING('Item Cost'),AT(5000,1979),USE(?String8),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Line Cost'),AT(6042,1979),USE(?String9:2),FONT('Arial',8,,FONT:bold,CHARSET:ANSI), |
  TRN
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR1               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepStringClass                       ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('PendingWebOrderReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:GENSHORT.Open                                     ! File GENSHORT used by this procedure, so make sure it's RelationManager is open
  Relate:ORDWEBPR.Open                                     ! File ORDWEBPR used by this procedure, so make sure it's RelationManager is open
  Relate:STOAUDIT.Open                                     ! File STOAUDIT used by this procedure, so make sure it's RelationManager is open
  Relate:TagFile.Open                                      ! File TagFile used by this procedure, so make sure it's RelationManager is open
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  locAccountFilter = p_web.GSV('BookingStoresAccount')
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number = p_web.GSV('BookingAccount')
        IF Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            address:User_Name        = tra:company_name
            address:Address_Line1    = tra:address_line1
            address:Address_Line2    = tra:address_line2
            address:Address_Line3    = tra:address_line3
            address:Post_code        = tra:postcode
            address:Telephone_Number = tra:telephone_number
            address:Fax_No           = tra:Fax_Number
            address:Email            = tra:EmailAddress
        ELSE ! If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
        END !If Access:TRADACC.Tryfetch(tra:Account_Number_Key) = Level:Benign  
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('PendingWebOrderReport',ProgressWindow)     ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowAlpha+ScrollSort:AllowNumeric,ScrollBy:RunTime)
  ThisReport.Init(Process:View, Relate:ORDWEBPR, ?Progress:PctText, Progress:Thermometer, ProgressMgr, orw:PartNumber)
  ThisReport.CaseSensitiveValue = FALSE
  ThisReport.AddSortOrder(orw:PartNumberKey)
  ThisReport.AddRange(orw:AccountNumber,locAccountFilter)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:ORDWEBPR.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:GENSHORT.Close
    Relate:ORDWEBPR.Close
    Relate:STOAUDIT.Close
    Relate:TagFile.Close
  END
  IF SELF.Opened
    INIMgr.Update('PendingWebOrderReport',ProgressWindow)  ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'PendingWebOrderReport',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.Init(Report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,Report{PROPPRINT:Paper},PAPER:USER), CHOOSE(Report{PROP:Thous}=True,PROP:Thous,CHOOSE(Report{PROP:MM}=True,PROP:MM,CHOOSE(Report{PROP:Points}=True,PROP:Points,0))), Report{PROPPRINT:PaperWidth}, Report{PROPPRINT:PaperHeight}, Report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetTitle('Stock Order')              !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR1.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetViewerPrefs(PDFXTR1:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@d06)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp:2{PROP:Text} = FORMAT(TODAY(),@d06)
  END
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportTimeStamp{PROP:Text} = FORMAT(CLOCK(),@T7)
  END
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'PendingWebOrderReport',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR1.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
        Access:TagFile.ClearKey(tag:keyTagged)
        tag:sessionID = p_web.sessionid
        tag:taggedValue = orw:PartNumber
        IF (Access:TagFile.TryFetch(tag:keyTagged))
           RETURN Level:Benign
           !was RETURN Level:Notify
        END
  
        LineCost = orw:ItemCost * orw:Quantity
        TotalLineCost += LineCost
        
        Total_No_Of_Lines += 1
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR1:rtn = PDFXTR1.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR1:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

