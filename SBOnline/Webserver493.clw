

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER493.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER070.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER492.INC'),ONCE        !Req'd for module callout resolution
                     END


OBFValidation        PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
Error:OBF            STRING(255)                           !Error OBF
Error:Validation     STRING(255)                           !Error:Generic
tmp:IMEINumber       STRING(30)                            !IMEI Number
tmp:BOXIMEINumber    STRING(30)                            !BOX IMEI Number
tmp:Network          STRING(30)                            !Network
tmp:DOP              DATE                                  !Date Of Purchase
tmp:DateOfPurchase   DATE                                  !
tmp:ReturnDate       DATE                                  !Return Date
tmp:TalkTime         STRING(30)                            !Talk Time
tmp:OriginalDealer   STRING(255)                           !Original Dealer
tmp:BranchOfReturn   STRING(30)                            !Branch Of Return
tmp:StoreReferenceNumber STRING(30)                        !Store Reference Number
tmp:ProofOfPurchase  BYTE(0)                               !Proof Of Purchase
tmp:OriginalPackaging BYTE(0)                              !Original Packaging
tmp:OriginalAccessories BYTE(0)                            !Original Accessories
tmp:OriginalManuals  BYTE(0)                               !Original Manuals
tmp:PhysicalDamage   BYTE(0)                               !Physical Damage
tmp:LAccountNumber   STRING(30)                            !LAccount Number
tmp:ReplacementIMEINumber STRING(30)                       !Replacement I.M.E.I. Number
tmp:Replacement      BYTE(0)                               !Replacement
tmp:TransitType      STRING(30)                            !Transit Type
FilesOpened     Long
NETWORKS::State  USHORT
TRANTYPE::State  USHORT
JOBSE::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('OBFValidation')
  loc:formname = 'OBFValidation_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'OBFValidation',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('OBFValidation','Insert')
    p_web._DivHeader('OBFValidation',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferOBFValidation',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferOBFValidation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferOBFValidation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_OBFValidation',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferOBFValidation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_OBFValidation',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'OBFValidation',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
ValidationFailed        Routine
    p_web.SSV('OBFValidation','Failed')
    p_web.SSV('tmp:TransitType','')
    If p_web.GSV('BookingSite') = 'ARC'
        p_web.SSV('filter:TransitType','trt:ARC = 1 AND trt:OBF <> 1')
    Else ! If p_web.GSV('BookingSite') = 'ARC'
        p_web.SSV('filter:TransitType','trt:RRC = 1 AND trt:OBF <> 1')
    End ! If p_web.GSV('BookingSite') = 'ARC'

ValidateOBF         Routine
Data
local:Error             CString(255)
local:ReturnDate        Date()
local:DOP               Date()
Code
    If p_web.GSV('OBFValidation') = 'Failed'
        Exit
    End ! If p_web.GSV('OBFValidation') = 'Failed'


!    local:ReturnDate = Deformat(p_web.GSV('tmp:ReturnDate'),'@d06')
!    local:DOP = Deformat(p_web.GSV('tmp:DateOfPurchase'),'@d06')
    local:ReturnDate = p_web.GSV('tmp:ReturnDate')
    local:DOP = p_web.GSV('tmp:DateOfPurchase')

    If local:ReturnDate > 0 And local:DOP > 0
        If local:ReturnDate > local:DOP + 7
            p_web.SSV('Error:Validation','Handset has not returned within 7 days of purchase.')
            Do ValidationFailed
            Exit
        End ! If tmp:ReturnDate > tmp:DateOfPurchase + 7
    End ! If tmp:ReturnDate > 0 And tmp:DateOfPurchase > 0

    tmp:ProofOfPurchase = p_web.GSV('tmp:ProofOfPurchase')
    If tmp:ProofOfPurchase = 2
        p_web.SSV('Error:Validation','Handset Proof Of Purchase Missing.')
        Do ValidationFailed
        Exit
    End ! If tmp:ProofOfPurchase = 2

    tmp:OriginalPackaging = p_web.GSV('tmp:OriginalPackaging')
    If tmp:OriginalPackaging = 2
        p_web.SSV('Error:Validation','Handset Original Packaging Missing.')
        Do ValidationFailed
        Exit
    End ! If tmp:OriginalPackaging = 2

    tmp:OriginalAccessories = p_web.GSV('tmp:OriginalAccessories')
    If tmp:OriginalAccessories = 2!
        p_web.SSV('Error:Validation','Original Accessories Missing.')
        Do ValidationFailed
        Exit
    End ! If tmp:OriginalAccessores = 2

    tmp:OriginalMAnuals = p_web.GSV('tmp:OriginalManuals')
    If tmp:OriginalManuals = 2
        p_web.SSV('Error:Validation','Original Manuals Missing.')
        Do ValidationFailed
        Exit
    End ! If tmp:OriginalManuals = 2

    tmp:IMEINumber = p_web.GSV('tmp:IMEINumber')
    tmp:BOXIMEINumber = p_web.GSV('tmp:BOXIMEINumber')
    If tmp:IMEINumber <> '' And tmp:BOXIMEINumber <> ''
        If tmp:IMEINumber <> tmp:BOXIMEINumber
            p_web.SSV('Error:Validation','Handset IMEI Number does not match the BOX IMEI Number.')
            Do ValidationFailed
            Exit
        End ! If tmp:IMEINumber <> tmp:BOXIMEINumber
    End ! If tmp:IMEINumber <> '' And tmp:BOXIMEINumber <> ''

    tmp:PhysicalDamage = p_web.GSV('tmp:PhysicalDamage')
    If tmp:PhysicalDamage = 1
        p_web.SSV('Error:Validation','Handset has signs of physical damage.')
        Do ValidationFailed
        Exit
    End ! If tmp:PhysicalDamage = 1
!
    tmp:Network = p_web.GSV('tmp:Network')
    If tmp:Network <> '' And tmp:Network <> '- Select Network -' And tmp:Network <> '082 VODACOM'
        p_web.SSV('Error:Validation','Handset not supplied by Vodacom Service Provider within the last 12 months.')
        Do ValidationFailed
        Exit
    End ! If tmp:Network <> '' And tmp:Network <> '082 VODACOM'

    tmp:TalkTime = p_web.GSV('tmp:TalkTime')
    tmp:OriginalDealer = p_web.GSV('tmp:OriginalDealer')
    tmp:BranchOfReturn = p_web.GSV('tmp:BranchOfReturn')
    tmp:Replacement = p_web.GSV('tmp:Replacement')
    tmp:LAccountNumber = p_web.GSV('tmp:LAccountNumber')
    tmp:ReplacementIMEINumber = p_web.GSV('tmp:ReplacementIMEINumber')

    If tmp:BOXIMEINumber <> '' And |
        tmp:Network <> '' And |
        tmp:Network <> '- Select Network -' And |
        local:DOP > 0 And |
        local:ReturnDate > 0 And |
        tmp:BranchOfReturn <> '' And |
        tmp:ProofOfPurchase <> 0 And |
        tmp:OriginalAccessories <> 0 And |
        tmp:PhysicalDamage <> 0 And |
        tmp:OriginalPackaging <> 0 And |
        tmp:PhysicalDamage <> 0 And |
        tmp:OriginalManuals <> 0 And |
        (tmp:Replacement = 2 Or (tmp:Replacement = 1 And tmp:LAccountNumber <> '' And tmp:ReplacementIMEINumber <> ''))
        p_web.SSV('OBFValidation','Passed')
        p_web.SSV('filter:TransitType','trt:Transit_Type = ''' & p_web.GSV('save:TransitType') & '''')
        p_web.SSV('Comment:TransitType','OBF Passed')
    End ! If tmp:BOXIMEINumber <> ''
OpenFiles  ROUTINE
  p_web._OpenFile(NETWORKS)
  p_web._OpenFile(TRANTYPE)
  p_web._OpenFile(JOBSE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(NETWORKS)
  p_Web._CloseFile(TRANTYPE)
  p_Web._CloseFile(JOBSE)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('OBFValidation_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  p_web.SSV('Hide:TransitType','')
    DuplicateTabCheckDelete(p_web,'NEWJOBBOOKING','') ! Clear SessionID
    DuplicateTabCheckDelete(p_web,'NEWJOBBOOKINGIMEI','')

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('tmp:DateOfPurchase')
    p_web.SetPicture('tmp:DateOfPurchase','@d06b')
  End
  p_web.SetSessionPicture('tmp:DateOfPurchase','@d06b')
  If p_web.IfExistsValue('tmp:ReturnDate')
    p_web.SetPicture('tmp:ReturnDate','@d06b')
  End
  p_web.SetSessionPicture('tmp:ReturnDate','@d06b')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:Network'
    p_web.setsessionvalue('showtab_OBFValidation',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(NETWORKS)
      p_web.SSV('tmp:Network',net:Network)
      Do ValidateOBF
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.')
  End
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'tmp:TransitType'
    p_web.setsessionvalue('showtab_OBFValidation',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(TRANTYPE)
    End
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tmp:IMEINumber',tmp:IMEINumber)
  p_web.SetSessionValue('tmp:BOXIMEINumber',tmp:BOXIMEINumber)
  p_web.SetSessionValue('tmp:Network',tmp:Network)
  p_web.SetSessionValue('tmp:DateOfPurchase',tmp:DateOfPurchase)
  p_web.SetSessionValue('tmp:ReturnDate',tmp:ReturnDate)
  p_web.SetSessionValue('tmp:TalkTime',tmp:TalkTime)
  p_web.SetSessionValue('tmp:OriginalDealer',tmp:OriginalDealer)
  p_web.SetSessionValue('tmp:BranchOfReturn',tmp:BranchOfReturn)
  p_web.SetSessionValue('tmp:StoreReferenceNumber',tmp:StoreReferenceNumber)
  p_web.SetSessionValue('tmp:ProofOfPurchase',tmp:ProofOfPurchase)
  p_web.SetSessionValue('tmp:OriginalPackaging',tmp:OriginalPackaging)
  p_web.SetSessionValue('tmp:OriginalAccessories',tmp:OriginalAccessories)
  p_web.SetSessionValue('tmp:OriginalManuals',tmp:OriginalManuals)
  p_web.SetSessionValue('tmp:PhysicalDamage',tmp:PhysicalDamage)
  p_web.SetSessionValue('tmp:Replacement',tmp:Replacement)
  p_web.SetSessionValue('tmp:LAccountNumber',tmp:LAccountNumber)
  p_web.SetSessionValue('tmp:ReplacementIMEINumber',tmp:ReplacementIMEINumber)
  p_web.SetSessionValue('Error:Validation',Error:Validation)
  p_web.SetSessionValue('tmp:TransitType',tmp:TransitType)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('tmp:IMEINumber')
    tmp:IMEINumber = p_web.GetValue('tmp:IMEINumber')
    p_web.SetSessionValue('tmp:IMEINumber',tmp:IMEINumber)
  End
  if p_web.IfExistsValue('tmp:BOXIMEINumber')
    tmp:BOXIMEINumber = p_web.GetValue('tmp:BOXIMEINumber')
    p_web.SetSessionValue('tmp:BOXIMEINumber',tmp:BOXIMEINumber)
  End
  if p_web.IfExistsValue('tmp:Network')
    tmp:Network = p_web.GetValue('tmp:Network')
    p_web.SetSessionValue('tmp:Network',tmp:Network)
  End
  if p_web.IfExistsValue('tmp:DateOfPurchase')
    tmp:DateOfPurchase = p_web.dformat(clip(p_web.GetValue('tmp:DateOfPurchase')),'@d06b')
    p_web.SetSessionValue('tmp:DateOfPurchase',tmp:DateOfPurchase)
  End
  if p_web.IfExistsValue('tmp:ReturnDate')
    tmp:ReturnDate = p_web.dformat(clip(p_web.GetValue('tmp:ReturnDate')),'@d06b')
    p_web.SetSessionValue('tmp:ReturnDate',tmp:ReturnDate)
  End
  if p_web.IfExistsValue('tmp:TalkTime')
    tmp:TalkTime = p_web.GetValue('tmp:TalkTime')
    p_web.SetSessionValue('tmp:TalkTime',tmp:TalkTime)
  End
  if p_web.IfExistsValue('tmp:OriginalDealer')
    tmp:OriginalDealer = p_web.GetValue('tmp:OriginalDealer')
    p_web.SetSessionValue('tmp:OriginalDealer',tmp:OriginalDealer)
  End
  if p_web.IfExistsValue('tmp:BranchOfReturn')
    tmp:BranchOfReturn = p_web.GetValue('tmp:BranchOfReturn')
    p_web.SetSessionValue('tmp:BranchOfReturn',tmp:BranchOfReturn)
  End
  if p_web.IfExistsValue('tmp:StoreReferenceNumber')
    tmp:StoreReferenceNumber = p_web.GetValue('tmp:StoreReferenceNumber')
    p_web.SetSessionValue('tmp:StoreReferenceNumber',tmp:StoreReferenceNumber)
  End
  if p_web.IfExistsValue('tmp:ProofOfPurchase')
    tmp:ProofOfPurchase = p_web.GetValue('tmp:ProofOfPurchase')
    p_web.SetSessionValue('tmp:ProofOfPurchase',tmp:ProofOfPurchase)
  End
  if p_web.IfExistsValue('tmp:OriginalPackaging')
    tmp:OriginalPackaging = p_web.GetValue('tmp:OriginalPackaging')
    p_web.SetSessionValue('tmp:OriginalPackaging',tmp:OriginalPackaging)
  End
  if p_web.IfExistsValue('tmp:OriginalAccessories')
    tmp:OriginalAccessories = p_web.GetValue('tmp:OriginalAccessories')
    p_web.SetSessionValue('tmp:OriginalAccessories',tmp:OriginalAccessories)
  End
  if p_web.IfExistsValue('tmp:OriginalManuals')
    tmp:OriginalManuals = p_web.GetValue('tmp:OriginalManuals')
    p_web.SetSessionValue('tmp:OriginalManuals',tmp:OriginalManuals)
  End
  if p_web.IfExistsValue('tmp:PhysicalDamage')
    tmp:PhysicalDamage = p_web.GetValue('tmp:PhysicalDamage')
    p_web.SetSessionValue('tmp:PhysicalDamage',tmp:PhysicalDamage)
  End
  if p_web.IfExistsValue('tmp:Replacement')
    tmp:Replacement = p_web.GetValue('tmp:Replacement')
    p_web.SetSessionValue('tmp:Replacement',tmp:Replacement)
  End
  if p_web.IfExistsValue('tmp:LAccountNumber')
    tmp:LAccountNumber = p_web.GetValue('tmp:LAccountNumber')
    p_web.SetSessionValue('tmp:LAccountNumber',tmp:LAccountNumber)
  End
  if p_web.IfExistsValue('tmp:ReplacementIMEINumber')
    tmp:ReplacementIMEINumber = p_web.GetValue('tmp:ReplacementIMEINumber')
    p_web.SetSessionValue('tmp:ReplacementIMEINumber',tmp:ReplacementIMEINumber)
  End
  if p_web.IfExistsValue('Error:Validation')
    Error:Validation = p_web.GetValue('Error:Validation')
    p_web.SetSessionValue('Error:Validation',Error:Validation)
  End
  if p_web.IfExistsValue('tmp:TransitType')
    tmp:TransitType = p_web.GetValue('tmp:TransitType')
    p_web.SetSessionValue('tmp:TransitType',tmp:TransitType)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('OBFValidation_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  ! Prime
  If p_web.GSV('ReadyForNewJobBooking') = ''
      loc:Invalid = 'job:ESN'
      loc:Alert = 'An Error Has Occurred. Please "Quit Booking" and try again!'
  End ! If p_web.GSV('ReadyForNewJobBooking') = ''
  
  p_web.site.SaveButton.TextValue = 'Save'
  p_web.site.CancelButton.TextValue = 'Quit'
  
  p_web.SSV('tmp:IMEINumber',p_web.GSV('tmp:ESN'))
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 tmp:IMEINumber = p_web.RestoreValue('tmp:IMEINumber')
 tmp:BOXIMEINumber = p_web.RestoreValue('tmp:BOXIMEINumber')
 tmp:Network = p_web.RestoreValue('tmp:Network')
 tmp:DateOfPurchase = p_web.RestoreValue('tmp:DateOfPurchase')
 tmp:ReturnDate = p_web.RestoreValue('tmp:ReturnDate')
 tmp:TalkTime = p_web.RestoreValue('tmp:TalkTime')
 tmp:OriginalDealer = p_web.RestoreValue('tmp:OriginalDealer')
 tmp:BranchOfReturn = p_web.RestoreValue('tmp:BranchOfReturn')
 tmp:StoreReferenceNumber = p_web.RestoreValue('tmp:StoreReferenceNumber')
 tmp:ProofOfPurchase = p_web.RestoreValue('tmp:ProofOfPurchase')
 tmp:OriginalPackaging = p_web.RestoreValue('tmp:OriginalPackaging')
 tmp:OriginalAccessories = p_web.RestoreValue('tmp:OriginalAccessories')
 tmp:OriginalManuals = p_web.RestoreValue('tmp:OriginalManuals')
 tmp:PhysicalDamage = p_web.RestoreValue('tmp:PhysicalDamage')
 tmp:Replacement = p_web.RestoreValue('tmp:Replacement')
 tmp:LAccountNumber = p_web.RestoreValue('tmp:LAccountNumber')
 tmp:ReplacementIMEINumber = p_web.RestoreValue('tmp:ReplacementIMEINumber')
 Error:Validation = p_web.RestoreValue('Error:Validation')
 tmp:TransitType = p_web.RestoreValue('tmp:TransitType')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'NewJobBooking?&Insert_btn=Insert&'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('OBFValidation_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('OBFValidation_ChainTo')
    loc:formaction = p_web.GetSessionValue('OBFValidation_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="OBFValidation" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="OBFValidation" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="OBFValidation" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('OBF Validation') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('OBF Validation',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_OBFValidation">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_OBFValidation" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_OBFValidation')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('OBF Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Validation Result') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_OBFValidation')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_OBFValidation'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='NETWORKS'
            p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:DateOfPurchase')
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.tmp:BOXIMEINumber')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_OBFValidation')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('OBF Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_OBFValidation_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('OBF Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('OBF Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('OBF Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('OBF Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:IMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:IMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:IMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:BOXIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:BOXIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:BOXIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:Network
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:Network
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:Network
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('Hide:ChangeDOP') = 0
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::text_DOPOverride
      do Comment::text_DOPOverride
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('Hide:ChangeDOP') = 0
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::text_DOPOverride2
      do Comment::text_DOPOverride2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:DateOfPurchase
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:DateOfPurchase
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:DateOfPurchase
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('Hide:ChangeDOP') = 0
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::button_ChangeDOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::button_ChangeDOP
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ReturnDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ReturnDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ReturnDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:TalkTime
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:TalkTime
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:TalkTime
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:OriginalDealer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:OriginalDealer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:OriginalDealer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:BranchOfReturn
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:BranchOfReturn
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:BranchOfReturn
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:StoreReferenceNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:StoreReferenceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:StoreReferenceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ProofOfPurchase
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ProofOfPurchase
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ProofOfPurchase
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:OriginalPackaging
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:OriginalPackaging
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:OriginalPackaging
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:OriginalAccessories
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:OriginalAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:OriginalAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:OriginalManuals
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:OriginalManuals
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:OriginalManuals
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:PhysicalDamage
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:PhysicalDamage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:PhysicalDamage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:Replacement
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:Replacement
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:Replacement
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:LAccountNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:LAccountNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:LAccountNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:ReplacementIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:ReplacementIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:ReplacementIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::Button:FailValidation
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Button:FailValidation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Button:FailValidation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Validation Result') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_OBFValidation_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Validation Result')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Validation Result')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Validation Result')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Validation Result')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::Passed:Validation
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Passed:Validation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Passed:Validation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::Error:Validation
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::Error:Validation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::Error:Validation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tmp:TransitType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tmp:TransitType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::tmp:TransitType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::tmp:IMEINumber  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:IMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Handset I.M.E.I. No')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:IMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:IMEINumber',p_web.GetValue('NewValue'))
    tmp:IMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:IMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:IMEINumber',p_web.GetValue('Value'))
    tmp:IMEINumber = p_web.GetValue('Value')
  End
  do Value::tmp:IMEINumber
  do SendAlert

Value::tmp:IMEINumber  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:IMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:IMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tmp:IMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:IMEINumber'',''obfvalidation_tmp:imeinumber_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:IMEINumber',p_web.GetSessionValueFormat('tmp:IMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:IMEINumber') & '_value')

Comment::tmp:IMEINumber  Routine
      loc:comment = ''
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:IMEINumber') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:BOXIMEINumber  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:BOXIMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Box I.M.E.I. Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:BOXIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:BOXIMEINumber',p_web.GetValue('NewValue'))
    tmp:BOXIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:BOXIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:BOXIMEINumber',p_web.GetValue('Value'))
    tmp:BOXIMEINumber = p_web.GetValue('Value')
  End
  If tmp:BOXIMEINumber = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
    loc:Invalid = 'tmp:BOXIMEINumber'
    loc:alert = p_web.translate('Box I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
      Do ValidateOBF
  do Value::tmp:BOXIMEINumber
  do SendAlert
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

Value::tmp:BOXIMEINumber  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:BOXIMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:BOXIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If (p_web.GetSessionValue('OBFValidation') <> 'Failed')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:BOXIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:BOXIMEINumber = '' and (p_web.GetSessionValue('OBFValidation') <> 'Failed')
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:BOXIMEINumber'',''obfvalidation_tmp:boximeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:BOXIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:BOXIMEINumber',p_web.GetSessionValueFormat('tmp:BOXIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:BOXIMEINumber') & '_value')

Comment::tmp:BOXIMEINumber  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:BOXIMEINumber') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:Network  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:Network') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Network')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:Network  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:Network',p_web.GetValue('NewValue'))
    tmp:Network = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:Network
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:Network',p_web.GetValue('Value'))
    tmp:Network = p_web.GetValue('Value')
  End
  If tmp:Network = ''
    loc:Invalid = 'tmp:Network'
    loc:alert = p_web.translate('Network') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:Network = Upper(tmp:Network)
    p_web.SetSessionValue('tmp:Network',tmp:Network)
  Access:NETWORKS.Clearkey(net:NetworkKey)
  net:Network    = p_web.GSV('tmp:Network')
  if (Access:NETWORKS.TryFetch(net:NetworkKey) = Level:Benign)
      ! Found
      p_web.SSV('comment:Network','')
      Do ValidateOBF
  else ! if (Access:NETWORKS.TryFetch(net:NetworkKey) = Level:Benign)
      ! Error
      p_web.SSV('tmp:Network','')
      p_web.SSV('comment:Network','Invalid Network')
  end ! if (Access:NETWORKS.TryFetch(net:NetworkKey) = Level:Benign)
  p_Web.SetValue('lookupfield','tmp:Network')
  do AfterLookup
  do Value::tmp:Network
  do SendAlert
  do Comment::tmp:Network
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

Value::tmp:Network  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:Network') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:Network
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:Network')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:Network = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(25) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:Network'',''obfvalidation_tmp:network_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:Network')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','tmp:Network',p_web.GetSessionValueFormat('tmp:Network'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectNetworks')&'?LookupField=tmp:Network&Tab=1&ForeignField=net:Network&_sort=&Refresh=sort&LookupFrom=OBFValidation&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:Network') & '_value')

Comment::tmp:Network  Routine
    loc:comment = p_web.Translate(p_web.GSV('comment:Network'))
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:Network') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:Network') & '_comment')

Validate::text_DOPOverride  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text_DOPOverride',p_web.GetValue('NewValue'))
    do Value::text_DOPOverride
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text_DOPOverride  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('text_DOPOverride') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Unit booked in previously.',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::text_DOPOverride  Routine
    loc:comment = ''
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('text_DOPOverride') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::text_DOPOverride2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('text_DOPOverride2',p_web.GetValue('NewValue'))
    do Value::text_DOPOverride2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::text_DOPOverride2  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('text_DOPOverride2') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('The Date Of Purchase will be filled identically to the previous job. ',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::text_DOPOverride2  Routine
    loc:comment = ''
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('text_DOPOverride2') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:DateOfPurchase  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:DateOfPurchase') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Date Of Purchase')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:DateOfPurchase  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:DateOfPurchase',p_web.GetValue('NewValue'))
    tmp:DateOfPurchase = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:DateOfPurchase
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:DateOfPurchase',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    tmp:DateOfPurchase = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  If tmp:DateOfPurchase = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
    loc:Invalid = 'tmp:DateOfPurchase'
    loc:alert = p_web.translate('Date Of Purchase') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  If p_web.GSV('tmp:DateOfPurchase') > Today() Or p_web.GSV('tmp:DateOfPurchase') < Deformat('01/01/1980',@d06)
      p_web.SSV('tmp:DateOfPurchase','')
  Else ! If tmp:DateOfPurchase > Today() Or tmp:DateOfPurchase < Deformat('01/01/1980',@d06)
      Do ValidateOBF
  End ! If tmp:DateOfPurchase > Today() Or tmp:DateOfPurchase < Deformat('01/01/1980',@d06)
  
  !If IsDateFormatInvalid(p_web.GSV('tmp:DateOfPurchase'))
  !    p_web.SSV('tmp:DateOfPurchase','')
  !Else ! If IsDateFormatInvalid(p_web.GSV('tmp:DateOfPurchase'))
  !    If Deformat(p_web.GSV('tmp:DateOfPurchase'),@d06) > Today() Or |
  !        Deformat(p_web.GSV('tmp:DateOfPurchase'),@d06) < Deformat('01/01/1980',@d06)
  !        p_web.SSV('tmp:DateOfPurchase','')
  !    End ! Deformat(p_web.GSV('tmp:DOP'),'@d06') < Deformat('01/01/1980','@d06')
      
  !End ! If IsDateFormatInvalid(p_web.GSV('tmp:DateOfPurchase'))
  do Value::tmp:DateOfPurchase
  do SendAlert
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

Value::tmp:DateOfPurchase  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:DateOfPurchase') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:DateOfPurchase
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('Hide:ChangeDOP') = 0,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('Hide:ChangeDOP') = 0
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If (p_web.GetSessionValue('OBFValidation') <> 'Failed')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:DateOfPurchase')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:DateOfPurchase = '' and (p_web.GetSessionValue('OBFValidation') <> 'Failed')
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:DateOfPurchase'',''obfvalidation_tmp:dateofpurchase_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:DateOfPurchase')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:DateOfPurchase',p_web.GetSessionValue('tmp:DateOfPurchase'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
  !Handcode Date Lookup Button
  if (p_web.GSV('Hide:ChangeDOP') = 1)
      packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar(tmp__DateOfPurchase,''dd/mm/yyyy'',this); Date.disabled=true;sv(''...'',''newjobbooking_pickdate_value'',1,FieldValue(this,1));nextFocus(NewJobBooking_frm,'''',0);" value="Select Date Of Purchase" name="Date" type="button">...</button>'
      do SendPacket
  end ! if (p_web.GSV('Hide:ChangeDOP') = 0)
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:DateOfPurchase') & '_value')

Comment::tmp:DateOfPurchase  Routine
    loc:comment = p_web.Translate('dd/mm/yyyy')
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:DateOfPurchase') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::button_ChangeDOP  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('button_ChangeDOP',p_web.GetValue('NewValue'))
    do Value::button_ChangeDOP
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::button_ChangeDOP  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('button_ChangeDOP') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','Override DOP','Override DOP','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormChangeDOP?frompage=' & p_web.PageName)) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()

Comment::button_ChangeDOP  Routine
    loc:comment = ''
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('button_ChangeDOP') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ReturnDate  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:ReturnDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Return Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ReturnDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ReturnDate',p_web.GetValue('NewValue'))
    tmp:ReturnDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ReturnDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ReturnDate',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    tmp:ReturnDate = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  If tmp:ReturnDate = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
    loc:Invalid = 'tmp:ReturnDate'
    loc:alert = p_web.translate('Return Date') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  If p_web.GSV('tmp:ReturnDate') > Today() Or p_web.GSV('tmp:ReturnDate') < Deformat('01/01/1980',@d06) Or p_web.GSV('tmp:ReturnDate') < p_web.GSV('tmp:DateOfPurchase')
      p_web.SSV('tmp:ReturnDate','')
  Else ! If p_web.GSV('tmp:ReturnDate') > Today() Or p_web.GSV('tmp:ReturnDate') < Deformat('01/01/1980',@d06)
      Do ValidateOBF
  End ! If p_web.GSV('tmp:ReturnDate') > Today() Or p_web.GSV('tmp:ReturnDate') < Deformat('01/01/1980',@d06)
  
  !If IsDateFormatInvalid(p_web.GSV('tmp:ReturnDate'))
  !    p_web.SSV('tmp:ReturnDate','')
  !Else ! If IsDateFormatInvalid(p_web.GSV('tmp:ReturnDate'))
  !    If Deformat(p_web.GSV('tmp:ReturnDate'),@d06) > Today() Or |
  !        Deformat(p_web.GSV('tmp:ReturnDate'),@d06) < Deformat('01/01/1980',@d06)
  !        p_web.SSV('tmp:ReturnDate','')
  !    End ! Deformat(p_web.GSV('tmp:DOP'),'@d06') < Deformat('01/01/1980','@d06')
      
  !End ! If IsDateFormatInvalid(p_web.GSV('tmp:ReturnDate'))
  do Value::tmp:ReturnDate
  do SendAlert
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Value::tmp:BranchOfReturn  !1
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

Value::tmp:ReturnDate  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:ReturnDate') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:ReturnDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If (p_web.GetSessionValue('OBFValidation') <> 'Failed')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:ReturnDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:ReturnDate = '' and (p_web.GetSessionValue('OBFValidation') <> 'Failed')
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ReturnDate'',''obfvalidation_tmp:returndate_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ReturnDate')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ReturnDate',p_web.GetSessionValue('tmp:ReturnDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
  !Handcode Date Lookup Button
  packet = clip(packet) & '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar(tmp__ReturnDate,''dd/mm/yyyy'',this); Date.disabled=true;sv(''...'',''newjobbooking_pickdate_value'',1,FieldValue(this,1));nextFocus(NewJobBooking_frm,'''',0);" value="Select Date Of Purchase" name="Date" type="button">...</button>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:ReturnDate') & '_value')

Comment::tmp:ReturnDate  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:ReturnDate') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:TalkTime  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:TalkTime') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Talk Time')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:TalkTime  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:TalkTime',p_web.GetValue('NewValue'))
    tmp:TalkTime = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:TalkTime
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:TalkTime',p_web.GetValue('Value'))
    tmp:TalkTime = p_web.GetValue('Value')
  End
  If tmp:TalkTime = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
    loc:Invalid = 'tmp:TalkTime'
    loc:alert = p_web.translate('Talk Time') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:TalkTime = Upper(tmp:TalkTime)
    p_web.SetSessionValue('tmp:TalkTime',tmp:TalkTime)
      Do ValidateOBF
  do Value::tmp:TalkTime
  do SendAlert
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

Value::tmp:TalkTime  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:TalkTime') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:TalkTime
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GetSessionValue('OBFValidation') <> 'Failed')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:TalkTime')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:TalkTime = '' and (p_web.GetSessionValue('OBFValidation') <> 'Failed')
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:TalkTime'',''obfvalidation_tmp:talktime_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:TalkTime')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:TalkTime',p_web.GetSessionValueFormat('tmp:TalkTime'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:TalkTime') & '_value')

Comment::tmp:TalkTime  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:TalkTime') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:OriginalDealer  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalDealer') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Original Dealer Address')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:OriginalDealer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:OriginalDealer',p_web.GetValue('NewValue'))
    tmp:OriginalDealer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:OriginalDealer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:OriginalDealer',p_web.GetValue('Value'))
    tmp:OriginalDealer = p_web.GetValue('Value')
  End
  If tmp:OriginalDealer = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
    loc:Invalid = 'tmp:OriginalDealer'
    loc:alert = p_web.translate('Original Dealer Address') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:OriginalDealer = Upper(tmp:OriginalDealer)
    p_web.SetSessionValue('tmp:OriginalDealer',tmp:OriginalDealer)
      Do ValidateOBF
      if (sub(p_web.GSV('tmp:OriginalDealer'),1,1 ) = ' ')
          p_web.SSV('tmp:OriginalDealer',sub(p_web.GSV('tmp:OriginalDealer'),2,255))
      end !if (sub(p_web.GSV('tmp:OriginalDealer'),1,1 ) = ' ')
  
  do Value::tmp:OriginalDealer
  do SendAlert
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

Value::tmp:OriginalDealer  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalDealer') & '_value','adiv')
  loc:extra = ''
  ! --- TEXT --- tmp:OriginalDealer
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GetSessionValue('OBFValidation') <> 'Failed')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:OriginalDealer')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:OriginalDealer = '' and (p_web.GetSessionValue('OBFValidation') <> 'Failed')
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:OriginalDealer'',''obfvalidation_tmp:originaldealer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OriginalDealer')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('tmp:OriginalDealer',p_web.GetSessionValue('tmp:OriginalDealer'),3,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(tmp:OriginalDealer),,Net:Web:Control) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:OriginalDealer') & '_value')

Comment::tmp:OriginalDealer  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalDealer') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:BranchOfReturn  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:BranchOfReturn') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Branch Of Return')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:BranchOfReturn  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:BranchOfReturn',p_web.GetValue('NewValue'))
    tmp:BranchOfReturn = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:BranchOfReturn
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:BranchOfReturn',p_web.GetValue('Value'))
    tmp:BranchOfReturn = p_web.GetValue('Value')
  End
  If tmp:BranchOfReturn = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
    loc:Invalid = 'tmp:BranchOfReturn'
    loc:alert = p_web.translate('Branch Of Return') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:BranchOfReturn = Upper(tmp:BranchOfReturn)
    p_web.SetSessionValue('tmp:BranchOfReturn',tmp:BranchOfReturn)
      Do ValidateOBF
  
  do Value::tmp:BranchOfReturn
  do SendAlert
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

Value::tmp:BranchOfReturn  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:BranchOfReturn') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:BranchOfReturn
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GetSessionValue('OBFValidation') <> 'Failed')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:BranchOfReturn')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:BranchOfReturn = '' and (p_web.GetSessionValue('OBFValidation') <> 'Failed')
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:BranchOfReturn'',''obfvalidation_tmp:branchofreturn_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:BranchOfReturn')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:BranchOfReturn',p_web.GetSessionValueFormat('tmp:BranchOfReturn'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:BranchOfReturn') & '_value')

Comment::tmp:BranchOfReturn  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:BranchOfReturn') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:StoreReferenceNumber  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:StoreReferenceNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Store Ref Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:StoreReferenceNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:StoreReferenceNumber',p_web.GetValue('NewValue'))
    tmp:StoreReferenceNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:StoreReferenceNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:StoreReferenceNumber',p_web.GetValue('Value'))
    tmp:StoreReferenceNumber = p_web.GetValue('Value')
  End
    tmp:StoreReferenceNumber = Upper(tmp:StoreReferenceNumber)
    p_web.SetSessionValue('tmp:StoreReferenceNumber',tmp:StoreReferenceNumber)
      Do ValidateOBF
  
  do Value::tmp:StoreReferenceNumber
  do SendAlert
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

Value::tmp:StoreReferenceNumber  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:StoreReferenceNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tmp:StoreReferenceNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('tmp:StoreReferenceNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:StoreReferenceNumber'',''obfvalidation_tmp:storereferencenumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:StoreReferenceNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:StoreReferenceNumber',p_web.GetSessionValueFormat('tmp:StoreReferenceNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:StoreReferenceNumber') & '_value')

Comment::tmp:StoreReferenceNumber  Routine
    loc:comment = p_web.Translate('(CCV, RTV, Repair Order No)')
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:StoreReferenceNumber') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ProofOfPurchase  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:ProofOfPurchase') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Proof Of Purchase')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:ProofOfPurchase  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ProofOfPurchase',p_web.GetValue('NewValue'))
    tmp:ProofOfPurchase = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ProofOfPurchase
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ProofOfPurchase',p_web.GetValue('Value'))
    tmp:ProofOfPurchase = p_web.GetValue('Value')
  End
      Do ValidateOBF
  do Value::tmp:ProofOfPurchase
  do SendAlert
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

Value::tmp:ProofOfPurchase  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:ProofOfPurchase') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- tmp:ProofOfPurchase
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:ProofOfPurchase')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:ProofOfPurchase') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:ProofOfPurchase'',''obfvalidation_tmp:proofofpurchase_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ProofOfPurchase')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:ProofOfPurchase',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:ProofOfPurchase_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:ProofOfPurchase') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:ProofOfPurchase'',''obfvalidation_tmp:proofofpurchase_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ProofOfPurchase')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:ProofOfPurchase',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:ProofOfPurchase_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:ProofOfPurchase') & '_value')

Comment::tmp:ProofOfPurchase  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:ProofOfPurchase') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:OriginalPackaging  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalPackaging') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Original Packaging')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:OriginalPackaging  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:OriginalPackaging',p_web.GetValue('NewValue'))
    tmp:OriginalPackaging = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:OriginalPackaging
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:OriginalPackaging',p_web.GetValue('Value'))
    tmp:OriginalPackaging = p_web.GetValue('Value')
  End
      Do ValidateOBF
  do Value::tmp:OriginalPackaging
  do SendAlert
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

Value::tmp:OriginalPackaging  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalPackaging') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- tmp:OriginalPackaging
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:OriginalPackaging')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:OriginalPackaging') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:OriginalPackaging'',''obfvalidation_tmp:originalpackaging_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OriginalPackaging')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:OriginalPackaging',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:OriginalPackaging_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:OriginalPackaging') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:OriginalPackaging'',''obfvalidation_tmp:originalpackaging_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OriginalPackaging')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:OriginalPackaging',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:OriginalPackaging_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:OriginalPackaging') & '_value')

Comment::tmp:OriginalPackaging  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalPackaging') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:OriginalAccessories  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalAccessories') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Original Accessories')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:OriginalAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:OriginalAccessories',p_web.GetValue('NewValue'))
    tmp:OriginalAccessories = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:OriginalAccessories
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:OriginalAccessories',p_web.GetValue('Value'))
    tmp:OriginalAccessories = p_web.GetValue('Value')
  End
      Do ValidateOBF
  do Value::tmp:OriginalAccessories
  do SendAlert
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

Value::tmp:OriginalAccessories  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalAccessories') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- tmp:OriginalAccessories
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:OriginalAccessories')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:OriginalAccessories') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:OriginalAccessories'',''obfvalidation_tmp:originalaccessories_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OriginalAccessories')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:OriginalAccessories',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:OriginalAccessories_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:OriginalAccessories') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:OriginalAccessories'',''obfvalidation_tmp:originalaccessories_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OriginalAccessories')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:OriginalAccessories',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:OriginalAccessories_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:OriginalAccessories') & '_value')

Comment::tmp:OriginalAccessories  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalAccessories') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:OriginalManuals  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalManuals') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Original Manuals')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:OriginalManuals  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:OriginalManuals',p_web.GetValue('NewValue'))
    tmp:OriginalManuals = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:OriginalManuals
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:OriginalManuals',p_web.GetValue('Value'))
    tmp:OriginalManuals = p_web.GetValue('Value')
  End
      Do ValidateOBF
  do Value::tmp:OriginalManuals
  do SendAlert
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

Value::tmp:OriginalManuals  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalManuals') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- tmp:OriginalManuals
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:OriginalManuals')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:OriginalManuals') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:OriginalManuals'',''obfvalidation_tmp:originalmanuals_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OriginalManuals')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:OriginalManuals',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:OriginalManuals_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:OriginalManuals') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:OriginalManuals'',''obfvalidation_tmp:originalmanuals_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:OriginalManuals')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:OriginalManuals',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:OriginalManuals_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:OriginalManuals') & '_value')

Comment::tmp:OriginalManuals  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:OriginalManuals') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:PhysicalDamage  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:PhysicalDamage') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Physical Damage')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:PhysicalDamage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:PhysicalDamage',p_web.GetValue('NewValue'))
    tmp:PhysicalDamage = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:PhysicalDamage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:PhysicalDamage',p_web.GetValue('Value'))
    tmp:PhysicalDamage = p_web.GetValue('Value')
  End
      Do ValidateOBF
  do Value::tmp:PhysicalDamage
  do SendAlert
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

Value::tmp:PhysicalDamage  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:PhysicalDamage') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- tmp:PhysicalDamage
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:PhysicalDamage')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:PhysicalDamage') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:PhysicalDamage'',''obfvalidation_tmp:physicaldamage_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:PhysicalDamage')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:PhysicalDamage',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:PhysicalDamage_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:PhysicalDamage') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:PhysicalDamage'',''obfvalidation_tmp:physicaldamage_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:PhysicalDamage')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:PhysicalDamage',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:PhysicalDamage_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:PhysicalDamage') & '_value')

Comment::tmp:PhysicalDamage  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:PhysicalDamage') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:Replacement  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:Replacement') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Replacement')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tmp:Replacement  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:Replacement',p_web.GetValue('NewValue'))
    tmp:Replacement = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:Replacement
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:Replacement',p_web.GetValue('Value'))
    tmp:Replacement = p_web.GetValue('Value')
  End
      Do ValidateOBF
  do Value::tmp:Replacement
  do SendAlert
  do Prompt::tmp:LAccountNumber
  do Value::tmp:LAccountNumber  !1
  do Prompt::tmp:ReplacementIMEINumber
  do Value::tmp:ReplacementIMEINumber  !1
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

Value::tmp:Replacement  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:Replacement') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- tmp:Replacement
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('tmp:Replacement')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:Replacement') = 1
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:Replacement'',''obfvalidation_tmp:replacement_value'',1,'''&clip(1)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:Replacement')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:Replacement',clip(1),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:Replacement_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('tmp:Replacement') = 2
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''tmp:Replacement'',''obfvalidation_tmp:replacement_value'',1,'''&clip(2)&''')')&';'
    loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:Replacement')&''',0);'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','tmp:Replacement',clip(2),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'tmp:Replacement_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:Replacement') & '_value')

Comment::tmp:Replacement  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:Replacement') & '_comment',clip('FormComments') & ' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:LAccountNumber  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:LAccountNumber') & '_prompt',Choose(p_web.GetSessionValue('tmp:Replacement') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('L/Account Number')
  If p_web.GetSessionValue('tmp:Replacement') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:LAccountNumber') & '_prompt')

Validate::tmp:LAccountNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:LAccountNumber',p_web.GetValue('NewValue'))
    tmp:LAccountNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:LAccountNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:LAccountNumber',p_web.GetValue('Value'))
    tmp:LAccountNumber = p_web.GetValue('Value')
  End
  If tmp:LAccountNumber = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
    loc:Invalid = 'tmp:LAccountNumber'
    loc:alert = p_web.translate('L/Account Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:LAccountNumber = Upper(tmp:LAccountNumber)
    p_web.SetSessionValue('tmp:LAccountNumber',tmp:LAccountNumber)
      Do ValidateOBF
  do Value::tmp:LAccountNumber
  do SendAlert
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

Value::tmp:LAccountNumber  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:LAccountNumber') & '_value',Choose(p_web.GetSessionValue('tmp:Replacement') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('tmp:Replacement') <> 1)
  ! --- STRING --- tmp:LAccountNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GetSessionValue('OBFValidation') <> 'Failed')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:LAccountNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:LAccountNumber = '' and (p_web.GetSessionValue('OBFValidation') <> 'Failed')
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:LAccountNumber'',''obfvalidation_tmp:laccountnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:LAccountNumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:LAccountNumber',p_web.GetSessionValueFormat('tmp:LAccountNumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:LAccountNumber') & '_value')

Comment::tmp:LAccountNumber  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:LAccountNumber') & '_comment',Choose(p_web.GetSessionValue('tmp:Replacement') <> 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('tmp:Replacement') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:ReplacementIMEINumber  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:ReplacementIMEINumber') & '_prompt',Choose(p_web.GetSessionValue('tmp:Replacement') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Replacement IMEI No')
  If p_web.GetSessionValue('tmp:Replacement') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:ReplacementIMEINumber') & '_prompt')

Validate::tmp:ReplacementIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:ReplacementIMEINumber',p_web.GetValue('NewValue'))
    tmp:ReplacementIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:ReplacementIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:ReplacementIMEINumber',p_web.GetValue('Value'))
    tmp:ReplacementIMEINumber = p_web.GetValue('Value')
  End
  If tmp:ReplacementIMEINumber = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
    loc:Invalid = 'tmp:ReplacementIMEINumber'
    loc:alert = p_web.translate('Replacement IMEI No') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    tmp:ReplacementIMEINumber = Upper(tmp:ReplacementIMEINumber)
    p_web.SetSessionValue('tmp:ReplacementIMEINumber',tmp:ReplacementIMEINumber)
      Do ValidateOBF
  do Value::tmp:ReplacementIMEINumber
  do SendAlert
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Comment::tmp:TransitType
  do Prompt::Button:FailValidation
  do Value::Button:FailValidation  !1
  do Comment::Button:FailValidation
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

Value::tmp:ReplacementIMEINumber  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:ReplacementIMEINumber') & '_value',Choose(p_web.GetSessionValue('tmp:Replacement') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('tmp:Replacement') <> 1)
  ! --- STRING --- tmp:ReplacementIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If (p_web.GetSessionValue('OBFValidation') <> 'Failed')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  End
  If lower(loc:invalid) = lower('tmp:ReplacementIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:ReplacementIMEINumber = '' and (p_web.GetSessionValue('OBFValidation') <> 'Failed')
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:ReplacementIMEINumber'',''obfvalidation_tmp:replacementimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:ReplacementIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tmp:ReplacementIMEINumber',p_web.GetSessionValueFormat('tmp:ReplacementIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:ReplacementIMEINumber') & '_value')

Comment::tmp:ReplacementIMEINumber  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:ReplacementIMEINumber') & '_comment',Choose(p_web.GetSessionValue('tmp:Replacement') <> 1,'hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('tmp:Replacement') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::Button:FailValidation  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('Button:FailValidation') & '_prompt',Choose(p_web.GetSessionValue('OBFValidation') = 'Passed','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Fail Validation')
  If p_web.GetSessionValue('OBFValidation') = 'Passed'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('Button:FailValidation') & '_prompt')

Validate::Button:FailValidation  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Button:FailValidation',p_web.GetValue('NewValue'))
    do Value::Button:FailValidation
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  p_web.SSV('Error:Validation','Validation Manually Failed')
  Do ValidationFailed
  do Value::Button:FailValidation
  do SendAlert
  do Prompt::Error:Validation
  do Value::Error:Validation  !1
  do Prompt::tmp:TransitType
  do Value::tmp:TransitType  !1
  do Prompt::Passed:Validation
  do Value::Passed:Validation  !1
  do Comment::Passed:Validation

Value::Button:FailValidation  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('Button:FailValidation') & '_value',Choose(p_web.GetSessionValue('OBFValidation') = 'Passed','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('OBFValidation') = 'Passed')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon(''&clip('ForceValidationFail')&'.disabled=true;sv(''Button:FailValidation'',''obfvalidation_button:failvalidation_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ForceValidationFail','Force Validation Fail','button-entryfield',loc:formname,,,,loc:javascript,0,'images/pcancel.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('Button:FailValidation') & '_value')

Comment::Button:FailValidation  Routine
    loc:comment = ''
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('Button:FailValidation') & '_comment',Choose(p_web.GetSessionValue('OBFValidation') = 'Passed','hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('OBFValidation') = 'Passed'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('Button:FailValidation') & '_comment')

Prompt::Passed:Validation  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('Passed:Validation') & '_prompt',Choose(p_web.GetSessionValue('OBFValidation') <> 'Passed','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('')
  If p_web.GetSessionValue('OBFValidation') <> 'Passed'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('Passed:Validation') & '_prompt')

Validate::Passed:Validation  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Passed:Validation',p_web.GetValue('NewValue'))
    do Value::Passed:Validation
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::Passed:Validation  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('Passed:Validation') & '_value',Choose(p_web.GetSessionValue('OBFValidation') <> 'Passed','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('OBFValidation') <> 'Passed')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBoldLarge')&'">' & p_web.Translate('Passed Validation',) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('Passed:Validation') & '_value')

Comment::Passed:Validation  Routine
    loc:comment = ''
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('Passed:Validation') & '_comment',Choose(p_web.GetSessionValue('OBFValidation') <> 'Passed','hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('OBFValidation') <> 'Passed'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('Passed:Validation') & '_comment')

Prompt::Error:Validation  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('Error:Validation') & '_prompt',Choose(p_web.GetSessionValue('Error:Validation') = '','hdiv','' & clip('RedRegular') & ''))
  loc:prompt = p_web.Translate('Validation Failed')
  If p_web.GetSessionValue('Error:Validation') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('Error:Validation') & '_prompt')

Validate::Error:Validation  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('Error:Validation',p_web.GetValue('NewValue'))
    Error:Validation = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::Error:Validation
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('Error:Validation',p_web.GetValue('Value'))
    Error:Validation = p_web.GetValue('Value')
  End
  do Value::Error:Validation
  do SendAlert

Value::Error:Validation  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('Error:Validation') & '_value',Choose(p_web.GetSessionValue('Error:Validation') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('Error:Validation') = '')
  ! --- TEXT --- Error:Validation
  loc:fieldclass = Choose(sub('RedRegular',1,1) = ' ',clip('FormEntry') & 'RedRegular','RedRegular')
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('Error:Validation')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''Error:Validation'',''obfvalidation_error:validation_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
  packet = clip(packet) & p_web.CreateTextArea('Error:Validation',p_web.GetSessionValue('Error:Validation'),3,60,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(Error:Validation),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('Error:Validation') & '_value')

Comment::Error:Validation  Routine
      loc:comment = ''
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('Error:Validation') & '_comment',Choose(p_web.GetSessionValue('Error:Validation') = '','hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('Error:Validation') = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::tmp:TransitType  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:TransitType') & '_prompt',Choose(p_web.GetSessionValue('OBFValidation') <> 'Failed','hdiv','' & clip('RedRegular') & ''))
  loc:prompt = p_web.Translate('Select Transit Type')
  If p_web.GetSessionValue('OBFValidation') <> 'Failed'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:TransitType') & '_prompt')

Validate::tmp:TransitType  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tmp:TransitType',p_web.GetValue('NewValue'))
    tmp:TransitType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::tmp:TransitType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tmp:TransitType',p_web.GetValue('Value'))
    tmp:TransitType = p_web.GetValue('Value')
  End
  If tmp:TransitType = ''
    loc:Invalid = 'tmp:TransitType'
    loc:alert = p_web.translate('Select Transit Type') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  p_Web.SetValue('lookupfield','tmp:TransitType')
  do AfterLookup
  do Value::tmp:TransitType
  do SendAlert
  do Comment::tmp:TransitType

Value::tmp:TransitType  Routine
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:TransitType') & '_value',Choose(p_web.GetSessionValue('OBFValidation') <> 'Failed','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GetSessionValue('OBFValidation') <> 'Failed')
  ! --- STRING --- tmp:TransitType
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('tmp:TransitType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If tmp:TransitType = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tmp:TransitType'',''obfvalidation_tmp:transittype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('tmp:TransitType')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','tmp:TransitType',p_web.GetSessionValueFormat('tmp:TransitType'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL(clip('SelectTransitTypes')&'?LookupField=tmp:TransitType&Tab=1&ForeignField=trt:Transit_Type&_sort=&Refresh=sort&LookupFrom=OBFValidation&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:TransitType') & '_value')

Comment::tmp:TransitType  Routine
    loc:comment = p_web.Translate('')
  p_web._DivHeader('OBFValidation_' & p_web._nocolon('tmp:TransitType') & '_comment',Choose(p_web.GetSessionValue('OBFValidation') <> 'Failed','hdiv',clip('FormComments') &' adiv'))
  If p_web.GetSessionValue('OBFValidation') <> 'Failed'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('OBFValidation_' & p_web._nocolon('tmp:TransitType') & '_comment')

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('OBFValidation_tmp:IMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:IMEINumber
      else
        do Value::tmp:IMEINumber
      end
  of lower('OBFValidation_tmp:BOXIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:BOXIMEINumber
      else
        do Value::tmp:BOXIMEINumber
      end
  of lower('OBFValidation_tmp:Network_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:Network
      else
        do Value::tmp:Network
      end
  of lower('OBFValidation_tmp:DateOfPurchase_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:DateOfPurchase
      else
        do Value::tmp:DateOfPurchase
      end
  of lower('OBFValidation_tmp:ReturnDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ReturnDate
      else
        do Value::tmp:ReturnDate
      end
  of lower('OBFValidation_tmp:TalkTime_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:TalkTime
      else
        do Value::tmp:TalkTime
      end
  of lower('OBFValidation_tmp:OriginalDealer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:OriginalDealer
      else
        do Value::tmp:OriginalDealer
      end
  of lower('OBFValidation_tmp:BranchOfReturn_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:BranchOfReturn
      else
        do Value::tmp:BranchOfReturn
      end
  of lower('OBFValidation_tmp:StoreReferenceNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:StoreReferenceNumber
      else
        do Value::tmp:StoreReferenceNumber
      end
  of lower('OBFValidation_tmp:ProofOfPurchase_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ProofOfPurchase
      else
        do Value::tmp:ProofOfPurchase
      end
  of lower('OBFValidation_tmp:OriginalPackaging_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:OriginalPackaging
      else
        do Value::tmp:OriginalPackaging
      end
  of lower('OBFValidation_tmp:OriginalAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:OriginalAccessories
      else
        do Value::tmp:OriginalAccessories
      end
  of lower('OBFValidation_tmp:OriginalManuals_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:OriginalManuals
      else
        do Value::tmp:OriginalManuals
      end
  of lower('OBFValidation_tmp:PhysicalDamage_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:PhysicalDamage
      else
        do Value::tmp:PhysicalDamage
      end
  of lower('OBFValidation_tmp:Replacement_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:Replacement
      else
        do Value::tmp:Replacement
      end
  of lower('OBFValidation_tmp:LAccountNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:LAccountNumber
      else
        do Value::tmp:LAccountNumber
      end
  of lower('OBFValidation_tmp:ReplacementIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:ReplacementIMEINumber
      else
        do Value::tmp:ReplacementIMEINumber
      end
  of lower('OBFValidation_Button:FailValidation_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::Button:FailValidation
      else
        do Value::Button:FailValidation
      end
  of lower('OBFValidation_Error:Validation_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::Error:Validation
      else
        do Value::Error:Validation
      end
  of lower('OBFValidation_tmp:TransitType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tmp:TransitType
      else
        do Value::tmp:TransitType
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('OBFValidation_form:ready_',1)
  p_web.SetSessionValue('OBFValidation_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_OBFValidation',0)

PreCopy  Routine
  p_web.SetValue('OBFValidation_form:ready_',1)
  p_web.SetSessionValue('OBFValidation_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_OBFValidation',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('OBFValidation_form:ready_',1)
  p_web.SetSessionValue('OBFValidation_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('OBFValidation:Primed',0)

PreDelete       Routine
  p_web.SetValue('OBFValidation_form:ready_',1)
  p_web.SetSessionValue('OBFValidation_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('OBFValidation:Primed',0)
  p_web.setsessionvalue('showtab_OBFValidation',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord
  !Field Validation
      If p_web.GSV('ReadyForNewJobBooking') = ''
          loc:Invalid = 'job:ESN'
          loc:Alert = 'An Error Has Occurred. Please "Quit Booking" and try again!'
          Exit
      End ! If p_web.GSV('ReadyForNewJobBooking') = ''
  
      If p_web.GSV('OBFValidation') = 'Pending'
          loc:Invalid = 'tmp:IMEINumber'
          loc:Alert = 'You must complete all of the fields, or click "Force Validation Fail".'
          Exit
      Elsif p_web.GSV('OBFValidation') = 'Failed'
          If p_web.GSV('tmp:TransitType') = ''
              loc:Invalid = 'tmp:TransitType'
              loc:Alert = 'You must select a new Transit Type.'
              Exit
          End ! If p_web.GSV('OBFValidation') = 'Failed' And p_web.GSV('tmp:TransitType') = ''
      Else
          If p_web.GSV('tmp:Replacement') = 1
              If Sub(p_web.GSV('tmp:LAccountNumber'),1,1) <> 'L'
                  loc:Invalid = 'tmp:LAccountNumber'
                  loc:Alert = 'L/Account Number must begin with the letter "L".'
                  Exit
              End ! If Sub(p_web.GSV('tmp:LAccountNumber'),1,1) <> 'L'
          End ! If p_web.GSV('tmp:Replacement')
      End ! If p_web.GSV('OBFValidation') <> 'Failed'
  
  ! Inserting (DBH 06/03/2008) # 9836 - Automatically add accessories
      If p_web.GSV('OBFValidation') = 'Passed'
          p_web.SSV('tmp:TheJobAccessory','|;ORIGINAL PACKAGING;|;MANUALS')
      End ! If p_web.GSV('OBFValidation') = 'Passed'
  ! End (DBH 06/03/2008) #9836
  
      ! Force The Transit Type (DBH: 27/03/2008)
      p_web.SSV('Filter:TransitType','trt:Transit_Type = <39>' & p_web.GSV('job:Transit_Type') & '<39>')
      p_web.SSV('Comment:TransitType','OBF Validation: ' & p_web.GSV('OBFValidation'))
      p_web.SSV('ReadOnly:IMEINumber',1)
  
      p_web.SSV('ReadyForNewJobBooking',3)
  
      p_web.SSV('tmp:DOP',p_web.GSV('tmp:DateOfPurchase'))

ValidateDelete  Routine
  p_web.DeleteSessionValue('OBFValidation_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('OBFValidation_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
        If tmp:BOXIMEINumber = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
          loc:Invalid = 'tmp:BOXIMEINumber'
          loc:alert = p_web.translate('Box I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
        If tmp:Network = ''
          loc:Invalid = 'tmp:Network'
          loc:alert = p_web.translate('Network') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:Network = Upper(tmp:Network)
          p_web.SetSessionValue('tmp:Network',tmp:Network)
        If loc:Invalid <> '' then exit.
        If tmp:DateOfPurchase = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
          loc:Invalid = 'tmp:DateOfPurchase'
          loc:alert = p_web.translate('Date Of Purchase') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
        If tmp:ReturnDate = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
          loc:Invalid = 'tmp:ReturnDate'
          loc:alert = p_web.translate('Return Date') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
        If tmp:TalkTime = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
          loc:Invalid = 'tmp:TalkTime'
          loc:alert = p_web.translate('Talk Time') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:TalkTime = Upper(tmp:TalkTime)
          p_web.SetSessionValue('tmp:TalkTime',tmp:TalkTime)
        If loc:Invalid <> '' then exit.
        If tmp:OriginalDealer = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
          loc:Invalid = 'tmp:OriginalDealer'
          loc:alert = p_web.translate('Original Dealer Address') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:OriginalDealer = Upper(tmp:OriginalDealer)
          p_web.SetSessionValue('tmp:OriginalDealer',tmp:OriginalDealer)
        If loc:Invalid <> '' then exit.
        If tmp:BranchOfReturn = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
          loc:Invalid = 'tmp:BranchOfReturn'
          loc:alert = p_web.translate('Branch Of Return') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:BranchOfReturn = Upper(tmp:BranchOfReturn)
          p_web.SetSessionValue('tmp:BranchOfReturn',tmp:BranchOfReturn)
        If loc:Invalid <> '' then exit.
          tmp:StoreReferenceNumber = Upper(tmp:StoreReferenceNumber)
          p_web.SetSessionValue('tmp:StoreReferenceNumber',tmp:StoreReferenceNumber)
        If loc:Invalid <> '' then exit.
      If not (p_web.GetSessionValue('tmp:Replacement') <> 1)
        If tmp:LAccountNumber = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
          loc:Invalid = 'tmp:LAccountNumber'
          loc:alert = p_web.translate('L/Account Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:LAccountNumber = Upper(tmp:LAccountNumber)
          p_web.SetSessionValue('tmp:LAccountNumber',tmp:LAccountNumber)
        If loc:Invalid <> '' then exit.
      End
      If not (p_web.GetSessionValue('tmp:Replacement') <> 1)
        If tmp:ReplacementIMEINumber = '' and p_web.GetSessionValue('OBFValidation') <> 'Failed'
          loc:Invalid = 'tmp:ReplacementIMEINumber'
          loc:alert = p_web.translate('Replacement IMEI No') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          tmp:ReplacementIMEINumber = Upper(tmp:ReplacementIMEINumber)
          p_web.SetSessionValue('tmp:ReplacementIMEINumber',tmp:ReplacementIMEINumber)
        If loc:Invalid <> '' then exit.
      End
  ! tab = 2
    loc:InvalidTab += 1
      If not (p_web.GetSessionValue('OBFValidation') <> 'Failed')
        If tmp:TransitType = ''
          loc:Invalid = 'tmp:TransitType'
          loc:alert = p_web.translate('Select Transit Type') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
      End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('OBFValidation:Primed',0)
  p_web.StoreValue('tmp:IMEINumber')
  p_web.StoreValue('tmp:BOXIMEINumber')
  p_web.StoreValue('tmp:Network')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:DateOfPurchase')
  p_web.StoreValue('')
  p_web.StoreValue('tmp:ReturnDate')
  p_web.StoreValue('tmp:TalkTime')
  p_web.StoreValue('tmp:OriginalDealer')
  p_web.StoreValue('tmp:BranchOfReturn')
  p_web.StoreValue('tmp:StoreReferenceNumber')
  p_web.StoreValue('tmp:ProofOfPurchase')
  p_web.StoreValue('tmp:OriginalPackaging')
  p_web.StoreValue('tmp:OriginalAccessories')
  p_web.StoreValue('tmp:OriginalManuals')
  p_web.StoreValue('tmp:PhysicalDamage')
  p_web.StoreValue('tmp:Replacement')
  p_web.StoreValue('tmp:LAccountNumber')
  p_web.StoreValue('tmp:ReplacementIMEINumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('Error:Validation')
  p_web.StoreValue('tmp:TransitType')
