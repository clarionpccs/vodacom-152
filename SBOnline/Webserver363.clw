

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER363.INC'),ONCE        !Local module procedure declarations
                     END


FormCompareBouncerFaultCodes PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locFaultCode1        STRING(30)                            !
locFaultCode2        STRING(30)                            !
locFaultCode3        STRING(30)                            !
locFaultCode4        STRING(30)                            !
locFaultCode5        STRING(30)                            !
locFaultCode6        STRING(30)                            !
locFaultCode7        STRING(30)                            !
locFaultCode8        STRING(30)                            !
locFaultCode9        STRING(30)                            !
locFaultCode10       STRING(30)                            !
locFaultCode11       STRING(30)                            !
locFaultCode12       STRING(30)                            !
locFaultCodeH1       STRING(30)                            !
locFaultCodeH2       STRING(30)                            !
locFaultCodeH3       STRING(30)                            !
locFaultCodeH4       STRING(30)                            !
locFaultCodeH5       STRING(30)                            !
locFaultCodeH6       STRING(30)                            !
locFaultCodeH7       STRING(30)                            !
locFaultCodeH8       STRING(30)                            !
locFaultCodeH9       STRING(30)                            !
locFaultCodeH10      STRING(30)                            !
locFaultCodeH11      STRING(30)                            !
locFaultCodeH12      STRING(30)                            !
locCurrentJobNumber  LONG                                  !
locHistoricJobNumber LONG                                  !
FilesOpened     Long
JOBS_ALIAS::State  USHORT
JOBS::State  USHORT
MANFAULT::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('FormCompareBouncerFaultCodes')
  loc:formname = 'FormCompareBouncerFaultCodes_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormCompareBouncerFaultCodes',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormCompareBouncerFaultCodes','')
    p_web._DivHeader('FormCompareBouncerFaultCodes',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormCompareBouncerFaultCodes',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormCompareBouncerFaultCodes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormCompareBouncerFaultCodes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormCompareBouncerFaultCodes',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormCompareBouncerFaultCodes',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormCompareBouncerFaultCodes',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormCompareBouncerFaultCodes',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(JOBS_ALIAS)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(MANFAULT)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBS_ALIAS)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(MANFAULT)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormCompareBouncerFaultCodes_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locCurrentJobNumber',locCurrentJobNumber)
  p_web.SetSessionValue('locHistoricJobNumber',locHistoricJobNumber)
  p_web.SetSessionValue('locFaultCode1',locFaultCode1)
  p_web.SetSessionValue('locFaultCodeH1',locFaultCodeH1)
  p_web.SetSessionValue('locFaultCode2',locFaultCode2)
  p_web.SetSessionValue('locFaultCodeH2',locFaultCodeH2)
  p_web.SetSessionValue('locFaultCode3',locFaultCode3)
  p_web.SetSessionValue('locFaultCodeH3',locFaultCodeH3)
  p_web.SetSessionValue('locFaultCode4',locFaultCode4)
  p_web.SetSessionValue('locFaultCodeH4',locFaultCodeH4)
  p_web.SetSessionValue('locFaultCode5',locFaultCode5)
  p_web.SetSessionValue('locFaultCodeH5',locFaultCodeH5)
  p_web.SetSessionValue('locFaultCode6',locFaultCode6)
  p_web.SetSessionValue('locFaultCodeH6',locFaultCodeH6)
  p_web.SetSessionValue('locFaultCode7',locFaultCode7)
  p_web.SetSessionValue('locFaultCodeH7',locFaultCodeH7)
  p_web.SetSessionValue('locFaultCode8',locFaultCode8)
  p_web.SetSessionValue('locFaultCodeH8',locFaultCodeH8)
  p_web.SetSessionValue('locFaultCode9',locFaultCode9)
  p_web.SetSessionValue('locFaultCodeH9',locFaultCodeH9)
  p_web.SetSessionValue('locFaultCode10',locFaultCode10)
  p_web.SetSessionValue('locFaultCodeH10',locFaultCodeH10)
  p_web.SetSessionValue('locFaultCode11',locFaultCode11)
  p_web.SetSessionValue('locFaultCodeH11',locFaultCodeH11)
  p_web.SetSessionValue('locFaultCode12',locFaultCode12)
  p_web.SetSessionValue('locFaultCodeH12',locFaultCodeH12)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locCurrentJobNumber')
    locCurrentJobNumber = p_web.GetValue('locCurrentJobNumber')
    p_web.SetSessionValue('locCurrentJobNumber',locCurrentJobNumber)
  End
  if p_web.IfExistsValue('locHistoricJobNumber')
    locHistoricJobNumber = p_web.GetValue('locHistoricJobNumber')
    p_web.SetSessionValue('locHistoricJobNumber',locHistoricJobNumber)
  End
  if p_web.IfExistsValue('locFaultCode1')
    locFaultCode1 = p_web.GetValue('locFaultCode1')
    p_web.SetSessionValue('locFaultCode1',locFaultCode1)
  End
  if p_web.IfExistsValue('locFaultCodeH1')
    locFaultCodeH1 = p_web.GetValue('locFaultCodeH1')
    p_web.SetSessionValue('locFaultCodeH1',locFaultCodeH1)
  End
  if p_web.IfExistsValue('locFaultCode2')
    locFaultCode2 = p_web.GetValue('locFaultCode2')
    p_web.SetSessionValue('locFaultCode2',locFaultCode2)
  End
  if p_web.IfExistsValue('locFaultCodeH2')
    locFaultCodeH2 = p_web.GetValue('locFaultCodeH2')
    p_web.SetSessionValue('locFaultCodeH2',locFaultCodeH2)
  End
  if p_web.IfExistsValue('locFaultCode3')
    locFaultCode3 = p_web.GetValue('locFaultCode3')
    p_web.SetSessionValue('locFaultCode3',locFaultCode3)
  End
  if p_web.IfExistsValue('locFaultCodeH3')
    locFaultCodeH3 = p_web.GetValue('locFaultCodeH3')
    p_web.SetSessionValue('locFaultCodeH3',locFaultCodeH3)
  End
  if p_web.IfExistsValue('locFaultCode4')
    locFaultCode4 = p_web.GetValue('locFaultCode4')
    p_web.SetSessionValue('locFaultCode4',locFaultCode4)
  End
  if p_web.IfExistsValue('locFaultCodeH4')
    locFaultCodeH4 = p_web.GetValue('locFaultCodeH4')
    p_web.SetSessionValue('locFaultCodeH4',locFaultCodeH4)
  End
  if p_web.IfExistsValue('locFaultCode5')
    locFaultCode5 = p_web.GetValue('locFaultCode5')
    p_web.SetSessionValue('locFaultCode5',locFaultCode5)
  End
  if p_web.IfExistsValue('locFaultCodeH5')
    locFaultCodeH5 = p_web.GetValue('locFaultCodeH5')
    p_web.SetSessionValue('locFaultCodeH5',locFaultCodeH5)
  End
  if p_web.IfExistsValue('locFaultCode6')
    locFaultCode6 = p_web.GetValue('locFaultCode6')
    p_web.SetSessionValue('locFaultCode6',locFaultCode6)
  End
  if p_web.IfExistsValue('locFaultCodeH6')
    locFaultCodeH6 = p_web.GetValue('locFaultCodeH6')
    p_web.SetSessionValue('locFaultCodeH6',locFaultCodeH6)
  End
  if p_web.IfExistsValue('locFaultCode7')
    locFaultCode7 = p_web.GetValue('locFaultCode7')
    p_web.SetSessionValue('locFaultCode7',locFaultCode7)
  End
  if p_web.IfExistsValue('locFaultCodeH7')
    locFaultCodeH7 = p_web.GetValue('locFaultCodeH7')
    p_web.SetSessionValue('locFaultCodeH7',locFaultCodeH7)
  End
  if p_web.IfExistsValue('locFaultCode8')
    locFaultCode8 = p_web.GetValue('locFaultCode8')
    p_web.SetSessionValue('locFaultCode8',locFaultCode8)
  End
  if p_web.IfExistsValue('locFaultCodeH8')
    locFaultCodeH8 = p_web.GetValue('locFaultCodeH8')
    p_web.SetSessionValue('locFaultCodeH8',locFaultCodeH8)
  End
  if p_web.IfExistsValue('locFaultCode9')
    locFaultCode9 = p_web.GetValue('locFaultCode9')
    p_web.SetSessionValue('locFaultCode9',locFaultCode9)
  End
  if p_web.IfExistsValue('locFaultCodeH9')
    locFaultCodeH9 = p_web.GetValue('locFaultCodeH9')
    p_web.SetSessionValue('locFaultCodeH9',locFaultCodeH9)
  End
  if p_web.IfExistsValue('locFaultCode10')
    locFaultCode10 = p_web.GetValue('locFaultCode10')
    p_web.SetSessionValue('locFaultCode10',locFaultCode10)
  End
  if p_web.IfExistsValue('locFaultCodeH10')
    locFaultCodeH10 = p_web.GetValue('locFaultCodeH10')
    p_web.SetSessionValue('locFaultCodeH10',locFaultCodeH10)
  End
  if p_web.IfExistsValue('locFaultCode11')
    locFaultCode11 = p_web.GetValue('locFaultCode11')
    p_web.SetSessionValue('locFaultCode11',locFaultCode11)
  End
  if p_web.IfExistsValue('locFaultCodeH11')
    locFaultCodeH11 = p_web.GetValue('locFaultCodeH11')
    p_web.SetSessionValue('locFaultCodeH11',locFaultCodeH11)
  End
  if p_web.IfExistsValue('locFaultCode12')
    locFaultCode12 = p_web.GetValue('locFaultCode12')
    p_web.SetSessionValue('locFaultCode12',locFaultCode12)
  End
  if p_web.IfExistsValue('locFaultCodeH12')
    locFaultCodeH12 = p_web.GetValue('locFaultCodeH12')
    p_web.SetSessionValue('locFaultCodeH12',locFaultCodeH12)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormCompareBouncerFaultCodes_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    IF (p_web.IfExistsValue('curjobno'))
        p_web.SSV('locCurrentJobNumber',p_web.GetValue('curjobno'))
    END ! IF
    IF (p_web.IfExistsValue('hisjobno'))
        p_web.SSV('locHistoricJobNumber',p_web.GetValue('hisjobno'))
    END ! IF
    
    Access:JOBS.ClearKey(job:Ref_Number_Key)
    job:Ref_Number = p_web.GSV('locCurrentJobNumber')
    IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
  
        LOOP x# = 1 TO 12
            p_web.SSV('Hide' & x#,1)
        END ! LOOP
  
        Access:MANFAULT.ClearKey(maf:Field_Number_Key)
        maf:Manufacturer = job:Manufacturer
        SET(maf:Field_Number_Key,maf:Field_Number_Key)
        LOOP UNTIL Access:MANFAULT.Next() <> Level:Benign
            IF (maf:Manufacturer <> job:Manufacturer)
                BREAK
            END ! IF
            p_web.SSV('Hide' & maf:Field_Number,0)
            p_web.SSV('Prompt' & maf:Field_Number,maf:Field_Name)
            CASE maf:Field_Number
            OF 1
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCode1',FORMAT(job:Fault_Code1,@d06))
                ELSE
                    p_web.SSV('locFaultCode1',job:Fault_Code1)
                END ! IF
            OF 2
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCode2',FORMAT(job:Fault_Code2,@d06))
                ELSE
                    p_web.SSV('locFaultCode2',job:Fault_Code2)
                END ! IF
            OF 3
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCode3',FORMAT(job:Fault_Code3,@d06))
                ELSE
                    p_web.SSV('locFaultCode3',job:Fault_Code3)
                END ! IF
            OF 4
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCode4',FORMAT(job:Fault_Code4,@d06))
                ELSE
                    p_web.SSV('locFaultCode4',job:Fault_Code4)
                END ! IF
            OF 5
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCode5',FORMAT(job:Fault_Code5,@d06))
                ELSE
                    p_web.SSV('locFaultCode5',job:Fault_Code5)
                END ! IF
            OF 6
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCode6',FORMAT(job:Fault_Code6,@d06))
                ELSE
                    p_web.SSV('locFaultCode6',job:Fault_Code6)
                END ! IF
            OF 7
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCode7',FORMAT(job:Fault_Code7,@d06))
                ELSE
                    p_web.SSV('locFaultCode7',job:Fault_Code7)
                END ! IF
            OF 8
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCode8',FORMAT(job:Fault_Code8,@d06))
                ELSE
                    p_web.SSV('locFaultCode8',job:Fault_Code8)
                END ! IF
            OF 9
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCode9',FORMAT(job:Fault_Code9,@d06))
                ELSE
                    p_web.SSV('locFaultCode9',job:Fault_Code9)
                END ! IF
            OF 10
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCode10',FORMAT(job:Fault_Code10,@d06))
                ELSE
                    p_web.SSV('locFaultCode10',job:Fault_Code10)
                END ! IF
            OF 11
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCode11',FORMAT(job:Fault_Code11,@d06))
                ELSE
                    p_web.SSV('locFaultCode11',job:Fault_Code11)
                END ! IF
            OF 12
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCode12',FORMAT(job:Fault_Code12,@d06))
                ELSE
                    p_web.SSV('locFaultCode12',job:Fault_Code12)
                END ! IF
            END!  CASE
            
        END ! LOOP
  
  	
    END ! IF
  
    Access:JOBS_ALIAS.ClearKey(job_ali:Ref_Number_Key)
    job_ali:Ref_Number = p_web.GSV('locHistoricJobNumber')
    IF (Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign)
  
        LOOP x# = 1 TO 12
            p_web.SSV('HideH' & x#,1)
        END ! LOOP
  
        Access:MANFAULT.ClearKey(maf:Field_Number_Key)
        maf:Manufacturer = job_ali:Manufacturer
        SET(maf:Field_Number_Key,maf:Field_Number_Key)
        LOOP UNTIL Access:MANFAULT.Next() <> Level:Benign
            IF (maf:Manufacturer <> job_ali:Manufacturer)
                BREAK
            END ! IF
            p_web.SSV('HideH' & maf:Field_Number,0)
            p_web.SSV('PromptH' & maf:Field_Number,maf:Field_Name)
            
            CASE maf:Field_Number
            OF 1
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCodeH1',FORMAT(job_ali:Fault_Code1,@d06))
                ELSE
                    p_web.SSV('locFaultCodeH1',job_ali:Fault_Code1)
                END ! IF
            OF 2
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCodeH2',FORMAT(job_ali:Fault_Code2,@d06))
                ELSE
                    p_web.SSV('locFaultCodeH2',job_ali:Fault_Code2)
                END ! IF
            OF 3
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCodeH3',FORMAT(job_ali:Fault_Code3,@d06))
                ELSE
                    p_web.SSV('locFaultCodeH3',job_ali:Fault_Code3)
                END ! IF
            OF 4
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCodeH4',FORMAT(job_ali:Fault_Code4,@d06))
                ELSE
                    p_web.SSV('locFaultCodeH4',job_ali:Fault_Code4)
                END ! IF
            OF 5
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCodeH5',FORMAT(job_ali:Fault_Code5,@d06))
                ELSE
                    p_web.SSV('locFaultCodeH5',job_ali:Fault_Code5)
                END ! IF
            OF 6
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCodeH6',FORMAT(job_ali:Fault_Code6,@d06))
                ELSE
                    p_web.SSV('locFaultCodeH6',job_ali:Fault_Code6)
                END ! IF
            OF 7
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCodeH7',FORMAT(job_ali:Fault_Code7,@d06))
                ELSE
                    p_web.SSV('locFaultCodeH7',job_ali:Fault_Code7)
                END ! IF
            OF 8
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCodeH8',FORMAT(job_ali:Fault_Code8,@d06))
                ELSE
                    p_web.SSV('locFaultCodeH8',job_ali:Fault_Code8)
                END ! IF
            OF 9
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCodeH9',FORMAT(job_ali:Fault_Code9,@d06))
                ELSE
                    p_web.SSV('locFaultCodeH9',job_ali:Fault_Code9)
                END ! IF
            OF 10
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCodeH10',FORMAT(job_ali:Fault_Code10,@d06))
                ELSE
                    p_web.SSV('locFaultCodeH10',job_ali:Fault_Code10)
                END ! IF
            OF 11
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCodeH11',FORMAT(job_ali:Fault_Code11,@d06))
                ELSE
                    p_web.SSV('locFaultCodeH11',job_ali:Fault_Code11)
                END ! IF
            OF 12
                IF (maf:Field_Type = 'DATE')
                    p_web.SSV('locFaultCodeH12',FORMAT(job_ali:Fault_Code12,@d06))
                ELSE
                    p_web.SSV('locFaultCodeH12',job_ali:Fault_Code12)
                END ! IF
            END!  CASE
        END ! LOOP
  	
  	
    END ! IF
      p_web.site.CancelButton.TextValue = 'Close'
      p_web.site.CancelButton.Image = '/images/psave.png'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locCurrentJobNumber = p_web.RestoreValue('locCurrentJobNumber')
 locHistoricJobNumber = p_web.RestoreValue('locHistoricJobNumber')
 locFaultCode1 = p_web.RestoreValue('locFaultCode1')
 locFaultCodeH1 = p_web.RestoreValue('locFaultCodeH1')
 locFaultCode2 = p_web.RestoreValue('locFaultCode2')
 locFaultCodeH2 = p_web.RestoreValue('locFaultCodeH2')
 locFaultCode3 = p_web.RestoreValue('locFaultCode3')
 locFaultCodeH3 = p_web.RestoreValue('locFaultCodeH3')
 locFaultCode4 = p_web.RestoreValue('locFaultCode4')
 locFaultCodeH4 = p_web.RestoreValue('locFaultCodeH4')
 locFaultCode5 = p_web.RestoreValue('locFaultCode5')
 locFaultCodeH5 = p_web.RestoreValue('locFaultCodeH5')
 locFaultCode6 = p_web.RestoreValue('locFaultCode6')
 locFaultCodeH6 = p_web.RestoreValue('locFaultCodeH6')
 locFaultCode7 = p_web.RestoreValue('locFaultCode7')
 locFaultCodeH7 = p_web.RestoreValue('locFaultCodeH7')
 locFaultCode8 = p_web.RestoreValue('locFaultCode8')
 locFaultCodeH8 = p_web.RestoreValue('locFaultCodeH8')
 locFaultCode9 = p_web.RestoreValue('locFaultCode9')
 locFaultCodeH9 = p_web.RestoreValue('locFaultCodeH9')
 locFaultCode10 = p_web.RestoreValue('locFaultCode10')
 locFaultCodeH10 = p_web.RestoreValue('locFaultCodeH10')
 locFaultCode11 = p_web.RestoreValue('locFaultCode11')
 locFaultCodeH11 = p_web.RestoreValue('locFaultCodeH11')
 locFaultCode12 = p_web.RestoreValue('locFaultCode12')
 locFaultCodeH12 = p_web.RestoreValue('locFaultCodeH12')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormCompareBouncerFaultCodes')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormCompareBouncerFaultCodes_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormCompareBouncerFaultCodes_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormCompareBouncerFaultCodes_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'FormBrowseBouncers'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormCompareBouncerFaultCodes" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormCompareBouncerFaultCodes" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormCompareBouncerFaultCodes" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Compare Fault Codes') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Compare Fault Codes',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormCompareBouncerFaultCodes">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormCompareBouncerFaultCodes" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormCompareBouncerFaultCodes')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Fault Codes') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormCompareBouncerFaultCodes')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormCompareBouncerFaultCodes'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormCompareBouncerFaultCodes')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Fault Codes') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormCompareBouncerFaultCodes_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Fault Codes')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Fault Codes')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Fault Codes')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Fault Codes')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCurrentJobNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCurrentJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCurrentJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locHistoricJobNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locHistoricJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locHistoricJobNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaultCode1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCode1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCode1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaultCodeH1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCodeH1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCodeH1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaultCode2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCode2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCode2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaultCodeH2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCodeH2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCodeH2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaultCode3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCode3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCode3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaultCodeH3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCodeH3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCodeH3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaultCode4
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCode4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCode4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaultCodeH4
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCodeH4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCodeH4
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCode5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCode5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCodeH5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCodeH5
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaultCode6
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCode6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCode6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaultCodeH6
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCodeH6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCodeH6
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaultCode7
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCode7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCode7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaultCodeH7
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCodeH7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCodeH7
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaultCode8
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCode8
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCode8
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaultCodeH8
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCodeH8
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCodeH8
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaultCode9
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCode9
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCode9
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaultCodeH9
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCodeH9
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCodeH9
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaultCode10
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCode10
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCode10
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaultCodeH10
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCodeH10
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCodeH10
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaultCode11
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCode11
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCode11
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaultCodeH11
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCodeH11
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCodeH11
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaultCode12
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCode12
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCode12
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locFaultCodeH12
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locFaultCodeH12
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locFaultCodeH12
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locCurrentJobNumber  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locCurrentJobNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Current Job No')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locCurrentJobNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCurrentJobNumber',p_web.GetValue('NewValue'))
    locCurrentJobNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCurrentJobNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCurrentJobNumber',p_web.GetValue('Value'))
    locCurrentJobNumber = p_web.GetValue('Value')
  End

Value::locCurrentJobNumber  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locCurrentJobNumber') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locCurrentJobNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('red bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locCurrentJobNumber'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locCurrentJobNumber  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locCurrentJobNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locHistoricJobNumber  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locHistoricJobNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Historic Job No')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locHistoricJobNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locHistoricJobNumber',p_web.GetValue('NewValue'))
    locHistoricJobNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locHistoricJobNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locHistoricJobNumber',p_web.GetValue('Value'))
    locHistoricJobNumber = p_web.GetValue('Value')
  End

Value::locHistoricJobNumber  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locHistoricJobNumber') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locHistoricJobNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('red bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locHistoricJobNumber'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locHistoricJobNumber  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locHistoricJobNumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaultCode1  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode1') & '_prompt',Choose(p_web.GSV('Hide1') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt1'))
  If p_web.GSV('Hide1') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCode1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCode1',p_web.GetValue('NewValue'))
    locFaultCode1 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCode1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCode1',p_web.GetValue('Value'))
    locFaultCode1 = p_web.GetValue('Value')
  End

Value::locFaultCode1  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode1') & '_value',Choose(p_web.GSV('Hide1') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide1') = 1)
  ! --- DISPLAY --- locFaultCode1
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web._jsok(p_web.GetSessionValueFormat('locFaultCode1'),) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCode1  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode1') & '_comment',Choose(p_web.GSV('Hide1') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide1') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaultCodeH1  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH1') & '_prompt',Choose(p_web.GSV('HideH1') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('PromptH1'))
  If p_web.GSV('HideH1') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCodeH1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCodeH1',p_web.GetValue('NewValue'))
    locFaultCodeH1 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCodeH1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCodeH1',p_web.GetValue('Value'))
    locFaultCodeH1 = p_web.GetValue('Value')
  End

Value::locFaultCodeH1  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH1') & '_value',Choose(p_web.GSV('HideH1') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('HideH1') = 1)
  ! --- DISPLAY --- locFaultCodeH1
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCodeH1'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCodeH1  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH1') & '_comment',Choose(p_web.GSV('HideH1') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('HideH1') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaultCode2  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode2') & '_prompt',Choose(p_web.GSV('Hide2') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt2'))
  If p_web.GSV('Hide2') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCode2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCode2',p_web.GetValue('NewValue'))
    locFaultCode2 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCode2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCode2',p_web.GetValue('Value'))
    locFaultCode2 = p_web.GetValue('Value')
  End

Value::locFaultCode2  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode2') & '_value',Choose(p_web.GSV('Hide2') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide2') = 1)
  ! --- DISPLAY --- locFaultCode2
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCode2'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCode2  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode2') & '_comment',Choose(p_web.GSV('Hide2') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide2') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaultCodeH2  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH2') & '_prompt',Choose(p_web.GSV('HideH2') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('PromptH2'))
  If p_web.GSV('HideH2') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCodeH2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCodeH2',p_web.GetValue('NewValue'))
    locFaultCodeH2 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCodeH2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCodeH2',p_web.GetValue('Value'))
    locFaultCodeH2 = p_web.GetValue('Value')
  End

Value::locFaultCodeH2  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH2') & '_value',Choose(p_web.GSV('HideH2') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('HideH2') = 1)
  ! --- DISPLAY --- locFaultCodeH2
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCodeH2'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCodeH2  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH2') & '_comment',Choose(p_web.GSV('HideH2') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('HideH2') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaultCode3  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode3') & '_prompt',Choose(p_web.GSV('Hide3') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt3'))
  If p_web.GSV('Hide3') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCode3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCode3',p_web.GetValue('NewValue'))
    locFaultCode3 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCode3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCode3',p_web.GetValue('Value'))
    locFaultCode3 = p_web.GetValue('Value')
  End

Value::locFaultCode3  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode3') & '_value',Choose(p_web.GSV('Hide3') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide3') = 1)
  ! --- DISPLAY --- locFaultCode3
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCode3'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCode3  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode3') & '_comment',Choose(p_web.GSV('Hide3') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide3') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaultCodeH3  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH3') & '_prompt',Choose(p_web.GSV('HideH3') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('PromptH3'))
  If p_web.GSV('HideH3') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCodeH3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCodeH3',p_web.GetValue('NewValue'))
    locFaultCodeH3 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCodeH3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCodeH3',p_web.GetValue('Value'))
    locFaultCodeH3 = p_web.GetValue('Value')
  End

Value::locFaultCodeH3  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH3') & '_value',Choose(p_web.GSV('HideH3') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('HideH3') = 1)
  ! --- DISPLAY --- locFaultCodeH3
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCodeH3'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCodeH3  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH3') & '_comment',Choose(p_web.GSV('HideH3') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('HideH3') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaultCode4  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode4') & '_prompt',Choose(p_web.GSV('Hide4') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt4'))
  If p_web.GSV('Hide4') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCode4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCode4',p_web.GetValue('NewValue'))
    locFaultCode4 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCode4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCode4',p_web.GetValue('Value'))
    locFaultCode4 = p_web.GetValue('Value')
  End

Value::locFaultCode4  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode4') & '_value',Choose(p_web.GSV('Hide4') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide4') = 1)
  ! --- DISPLAY --- locFaultCode4
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCode4'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCode4  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode4') & '_comment',Choose(p_web.GSV('Hide4') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide4') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaultCodeH4  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH4') & '_prompt',Choose(p_web.GSV('HideH4') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('PromptH4'))
  If p_web.GSV('HideH4') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCodeH4  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCodeH4',p_web.GetValue('NewValue'))
    locFaultCodeH4 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCodeH4
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCodeH4',p_web.GetValue('Value'))
    locFaultCodeH4 = p_web.GetValue('Value')
  End

Value::locFaultCodeH4  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH4') & '_value',Choose(p_web.GSV('HideH4') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('HideH4') = 1)
  ! --- DISPLAY --- locFaultCodeH4
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCodeH4'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCodeH4  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH4') & '_comment',Choose(p_web.GSV('HideH4') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('HideH4') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCode5  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCode5',p_web.GetValue('NewValue'))
    locFaultCode5 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCode5
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCode5',p_web.GetValue('Value'))
    locFaultCode5 = p_web.GetValue('Value')
  End

Value::locFaultCode5  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode5') & '_value',Choose(p_web.GSV('Hide5') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide5') = 1)
  ! --- DISPLAY --- locFaultCode5
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCode5'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCode5  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode5') & '_comment',Choose(p_web.GSV('Hide5') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide5') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCodeH5  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCodeH5',p_web.GetValue('NewValue'))
    locFaultCodeH5 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCodeH5
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCodeH5',p_web.GetValue('Value'))
    locFaultCodeH5 = p_web.GetValue('Value')
  End

Value::locFaultCodeH5  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH5') & '_value',Choose(p_web.GSV('HideH5') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('HideH5') = 1)
  ! --- DISPLAY --- locFaultCodeH5
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCodeH5'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCodeH5  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH5') & '_comment',Choose(p_web.GSV('HideH5') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('HideH5') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaultCode6  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode6') & '_prompt',Choose(p_web.GSV('Hide6') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt6'))
  If p_web.GSV('Hide6') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCode6  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCode6',p_web.GetValue('NewValue'))
    locFaultCode6 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCode6
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCode6',p_web.GetValue('Value'))
    locFaultCode6 = p_web.GetValue('Value')
  End

Value::locFaultCode6  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode6') & '_value',Choose(p_web.GSV('Hide6') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide6') = 1)
  ! --- DISPLAY --- locFaultCode6
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCode6'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCode6  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode6') & '_comment',Choose(p_web.GSV('Hide6') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide6') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaultCodeH6  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH6') & '_prompt',Choose(p_web.GSV('HideH6') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('PromptH6'))
  If p_web.GSV('HideH6') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCodeH6  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCodeH6',p_web.GetValue('NewValue'))
    locFaultCodeH6 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCodeH6
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCodeH6',p_web.GetValue('Value'))
    locFaultCodeH6 = p_web.GetValue('Value')
  End

Value::locFaultCodeH6  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH6') & '_value',Choose(p_web.GSV('HideH6') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('HideH6') = 1)
  ! --- DISPLAY --- locFaultCodeH6
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCodeH6'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCodeH6  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH6') & '_comment',Choose(p_web.GSV('HideH6') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('HideH6') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaultCode7  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode7') & '_prompt',Choose(p_web.GSV('Hide7') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt7'))
  If p_web.GSV('Hide7') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCode7  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCode7',p_web.GetValue('NewValue'))
    locFaultCode7 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCode7
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCode7',p_web.GetValue('Value'))
    locFaultCode7 = p_web.GetValue('Value')
  End

Value::locFaultCode7  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode7') & '_value',Choose(p_web.GSV('Hide7') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide7') = 1)
  ! --- DISPLAY --- locFaultCode7
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCode7'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCode7  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode7') & '_comment',Choose(p_web.GSV('Hide7') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide7') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaultCodeH7  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH7') & '_prompt',Choose(p_web.GSV('HideH7') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('PromptH7'))
  If p_web.GSV('HideH7') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCodeH7  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCodeH7',p_web.GetValue('NewValue'))
    locFaultCodeH7 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCodeH7
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCodeH7',p_web.GetValue('Value'))
    locFaultCodeH7 = p_web.GetValue('Value')
  End

Value::locFaultCodeH7  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH7') & '_value',Choose(p_web.GSV('HideH7') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('HideH7') = 1)
  ! --- DISPLAY --- locFaultCodeH7
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCodeH7'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCodeH7  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH7') & '_comment',Choose(p_web.GSV('HideH7') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('HideH7') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaultCode8  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode8') & '_prompt',Choose(p_web.GSV('Hide8') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt8'))
  If p_web.GSV('Hide8') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCode8  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCode8',p_web.GetValue('NewValue'))
    locFaultCode8 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCode8
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCode8',p_web.GetValue('Value'))
    locFaultCode8 = p_web.GetValue('Value')
  End

Value::locFaultCode8  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode8') & '_value',Choose(p_web.GSV('Hide8') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide8') = 1)
  ! --- DISPLAY --- locFaultCode8
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCode8'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCode8  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode8') & '_comment',Choose(p_web.GSV('Hide8') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide8') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaultCodeH8  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH8') & '_prompt',Choose(p_web.GSV('HideH8') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('PromptH8'))
  If p_web.GSV('HideH8') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCodeH8  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCodeH8',p_web.GetValue('NewValue'))
    locFaultCodeH8 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCodeH8
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCodeH8',p_web.GetValue('Value'))
    locFaultCodeH8 = p_web.GetValue('Value')
  End

Value::locFaultCodeH8  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH8') & '_value',Choose(p_web.GSV('HideH8') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('HideH8') = 1)
  ! --- DISPLAY --- locFaultCodeH8
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCodeH8'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCodeH8  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH8') & '_comment',Choose(p_web.GSV('HideH8') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('HideH8') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaultCode9  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode9') & '_prompt',Choose(p_web.GSV('Hide9') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt9'))
  If p_web.GSV('Hide9') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCode9  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCode9',p_web.GetValue('NewValue'))
    locFaultCode9 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCode9
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCode9',p_web.GetValue('Value'))
    locFaultCode9 = p_web.GetValue('Value')
  End

Value::locFaultCode9  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode9') & '_value',Choose(p_web.GSV('Hide9') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide9') = 1)
  ! --- DISPLAY --- locFaultCode9
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCode9'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCode9  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode9') & '_comment',Choose(p_web.GSV('Hide9') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide9') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaultCodeH9  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH9') & '_prompt',Choose(p_web.GSV('HideH9') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('PromptH9'))
  If p_web.GSV('HideH9') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCodeH9  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCodeH9',p_web.GetValue('NewValue'))
    locFaultCodeH9 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCodeH9
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCodeH9',p_web.GetValue('Value'))
    locFaultCodeH9 = p_web.GetValue('Value')
  End

Value::locFaultCodeH9  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH9') & '_value',Choose(p_web.GSV('HideH9') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('HideH9') = 1)
  ! --- DISPLAY --- locFaultCodeH9
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCodeH9'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCodeH9  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH9') & '_comment',Choose(p_web.GSV('HideH9') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('HideH9') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaultCode10  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode10') & '_prompt',Choose(p_web.GSV('Hide10') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt10'))
  If p_web.GSV('Hide10') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCode10  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCode10',p_web.GetValue('NewValue'))
    locFaultCode10 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCode10
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCode10',p_web.GetValue('Value'))
    locFaultCode10 = p_web.GetValue('Value')
  End

Value::locFaultCode10  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode10') & '_value',Choose(p_web.GSV('Hide10') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide10') = 1)
  ! --- DISPLAY --- locFaultCode10
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCode10'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCode10  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode10') & '_comment',Choose(p_web.GSV('Hide10') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide10') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaultCodeH10  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH10') & '_prompt',Choose(p_web.GSV('HideH10') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('PromptH10'))
  If p_web.GSV('HideH10') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCodeH10  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCodeH10',p_web.GetValue('NewValue'))
    locFaultCodeH10 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCodeH10
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCodeH10',p_web.GetValue('Value'))
    locFaultCodeH10 = p_web.GetValue('Value')
  End

Value::locFaultCodeH10  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH10') & '_value',Choose(p_web.GSV('HideH10') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('HideH10') = 1)
  ! --- DISPLAY --- locFaultCodeH10
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCodeH10'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCodeH10  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH10') & '_comment',Choose(p_web.GSV('HideH10') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('HideH10') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaultCode11  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode11') & '_prompt',Choose(p_web.GSV('Hide11') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt11'))
  If p_web.GSV('Hide11') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCode11  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCode11',p_web.GetValue('NewValue'))
    locFaultCode11 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCode11
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCode11',p_web.GetValue('Value'))
    locFaultCode11 = p_web.GetValue('Value')
  End

Value::locFaultCode11  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode11') & '_value',Choose(p_web.GSV('Hide11') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide11') = 1)
  ! --- DISPLAY --- locFaultCode11
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCode11'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCode11  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode11') & '_comment',Choose(p_web.GSV('Hide11') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide11') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaultCodeH11  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH11') & '_prompt',Choose(p_web.GSV('HideH11') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('PromptH11'))
  If p_web.GSV('HideH11') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCodeH11  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCodeH11',p_web.GetValue('NewValue'))
    locFaultCodeH11 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCodeH11
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCodeH11',p_web.GetValue('Value'))
    locFaultCodeH11 = p_web.GetValue('Value')
  End

Value::locFaultCodeH11  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH11') & '_value',Choose(p_web.GSV('HideH11') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('HideH11') = 1)
  ! --- DISPLAY --- locFaultCodeH11
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCodeH11'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCodeH11  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH11') & '_comment',Choose(p_web.GSV('HideH11') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('HideH11') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaultCode12  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode12') & '_prompt',Choose(p_web.GSV('Hide12') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('Prompt12'))
  If p_web.GSV('Hide12') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCode12  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCode12',p_web.GetValue('NewValue'))
    locFaultCode12 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCode12
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCode12',p_web.GetValue('Value'))
    locFaultCode12 = p_web.GetValue('Value')
  End

Value::locFaultCode12  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode12') & '_value',Choose(p_web.GSV('Hide12') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide12') = 1)
  ! --- DISPLAY --- locFaultCode12
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCode12'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCode12  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCode12') & '_comment',Choose(p_web.GSV('Hide12') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide12') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locFaultCodeH12  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH12') & '_prompt',Choose(p_web.GSV('HideH12') = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate(p_web.GSV('PromptH12'))
  If p_web.GSV('HideH12') = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locFaultCodeH12  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locFaultCodeH12',p_web.GetValue('NewValue'))
    locFaultCodeH12 = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locFaultCodeH12
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locFaultCodeH12',p_web.GetValue('Value'))
    locFaultCodeH12 = p_web.GetValue('Value')
  End

Value::locFaultCodeH12  Routine
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH12') & '_value',Choose(p_web.GSV('HideH12') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('HideH12') = 1)
  ! --- DISPLAY --- locFaultCodeH12
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locFaultCodeH12'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locFaultCodeH12  Routine
    loc:comment = ''
  p_web._DivHeader('FormCompareBouncerFaultCodes_' & p_web._nocolon('locFaultCodeH12') & '_comment',Choose(p_web.GSV('HideH12') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('HideH12') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormCompareBouncerFaultCodes_form:ready_',1)
  p_web.SetSessionValue('FormCompareBouncerFaultCodes_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormCompareBouncerFaultCodes',0)

PreCopy  Routine
  p_web.SetValue('FormCompareBouncerFaultCodes_form:ready_',1)
  p_web.SetSessionValue('FormCompareBouncerFaultCodes_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormCompareBouncerFaultCodes',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormCompareBouncerFaultCodes_form:ready_',1)
  p_web.SetSessionValue('FormCompareBouncerFaultCodes_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormCompareBouncerFaultCodes:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormCompareBouncerFaultCodes_form:ready_',1)
  p_web.SetSessionValue('FormCompareBouncerFaultCodes_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormCompareBouncerFaultCodes:Primed',0)
  p_web.setsessionvalue('showtab_FormCompareBouncerFaultCodes',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormCompareBouncerFaultCodes_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormCompareBouncerFaultCodes_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormCompareBouncerFaultCodes:Primed',0)
  p_web.StoreValue('locCurrentJobNumber')
  p_web.StoreValue('locHistoricJobNumber')
  p_web.StoreValue('locFaultCode1')
  p_web.StoreValue('locFaultCodeH1')
  p_web.StoreValue('locFaultCode2')
  p_web.StoreValue('locFaultCodeH2')
  p_web.StoreValue('locFaultCode3')
  p_web.StoreValue('locFaultCodeH3')
  p_web.StoreValue('locFaultCode4')
  p_web.StoreValue('locFaultCodeH4')
  p_web.StoreValue('locFaultCode5')
  p_web.StoreValue('locFaultCodeH5')
  p_web.StoreValue('locFaultCode6')
  p_web.StoreValue('locFaultCodeH6')
  p_web.StoreValue('locFaultCode7')
  p_web.StoreValue('locFaultCodeH7')
  p_web.StoreValue('locFaultCode8')
  p_web.StoreValue('locFaultCodeH8')
  p_web.StoreValue('locFaultCode9')
  p_web.StoreValue('locFaultCodeH9')
  p_web.StoreValue('locFaultCode10')
  p_web.StoreValue('locFaultCodeH10')
  p_web.StoreValue('locFaultCode11')
  p_web.StoreValue('locFaultCodeH11')
  p_web.StoreValue('locFaultCode12')
  p_web.StoreValue('locFaultCodeH12')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locFaultCode1',locFaultCode1) ! STRING(30)
     p_web.SSV('locFaultCode2',locFaultCode2) ! STRING(30)
     p_web.SSV('locFaultCode3',locFaultCode3) ! STRING(30)
     p_web.SSV('locFaultCode4',locFaultCode4) ! STRING(30)
     p_web.SSV('locFaultCode5',locFaultCode5) ! STRING(30)
     p_web.SSV('locFaultCode6',locFaultCode6) ! STRING(30)
     p_web.SSV('locFaultCode7',locFaultCode7) ! STRING(30)
     p_web.SSV('locFaultCode8',locFaultCode8) ! STRING(30)
     p_web.SSV('locFaultCode9',locFaultCode9) ! STRING(30)
     p_web.SSV('locFaultCode10',locFaultCode10) ! STRING(30)
     p_web.SSV('locFaultCode11',locFaultCode11) ! STRING(30)
     p_web.SSV('locFaultCode12',locFaultCode12) ! STRING(30)
     p_web.SSV('locFaultCodeH1',locFaultCodeH1) ! STRING(30)
     p_web.SSV('locFaultCodeH2',locFaultCodeH2) ! STRING(30)
     p_web.SSV('locFaultCodeH3',locFaultCodeH3) ! STRING(30)
     p_web.SSV('locFaultCodeH4',locFaultCodeH4) ! STRING(30)
     p_web.SSV('locFaultCodeH5',locFaultCodeH5) ! STRING(30)
     p_web.SSV('locFaultCodeH6',locFaultCodeH6) ! STRING(30)
     p_web.SSV('locFaultCodeH7',locFaultCodeH7) ! STRING(30)
     p_web.SSV('locFaultCodeH8',locFaultCodeH8) ! STRING(30)
     p_web.SSV('locFaultCodeH9',locFaultCodeH9) ! STRING(30)
     p_web.SSV('locFaultCodeH10',locFaultCodeH10) ! STRING(30)
     p_web.SSV('locFaultCodeH11',locFaultCodeH11) ! STRING(30)
     p_web.SSV('locFaultCodeH12',locFaultCodeH12) ! STRING(30)
     p_web.SSV('locCurrentJobNumber',locCurrentJobNumber) ! LONG
     p_web.SSV('locHistoricJobNumber',locHistoricJobNumber) ! LONG
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locFaultCode1 = p_web.GSV('locFaultCode1') ! STRING(30)
     locFaultCode2 = p_web.GSV('locFaultCode2') ! STRING(30)
     locFaultCode3 = p_web.GSV('locFaultCode3') ! STRING(30)
     locFaultCode4 = p_web.GSV('locFaultCode4') ! STRING(30)
     locFaultCode5 = p_web.GSV('locFaultCode5') ! STRING(30)
     locFaultCode6 = p_web.GSV('locFaultCode6') ! STRING(30)
     locFaultCode7 = p_web.GSV('locFaultCode7') ! STRING(30)
     locFaultCode8 = p_web.GSV('locFaultCode8') ! STRING(30)
     locFaultCode9 = p_web.GSV('locFaultCode9') ! STRING(30)
     locFaultCode10 = p_web.GSV('locFaultCode10') ! STRING(30)
     locFaultCode11 = p_web.GSV('locFaultCode11') ! STRING(30)
     locFaultCode12 = p_web.GSV('locFaultCode12') ! STRING(30)
     locFaultCodeH1 = p_web.GSV('locFaultCodeH1') ! STRING(30)
     locFaultCodeH2 = p_web.GSV('locFaultCodeH2') ! STRING(30)
     locFaultCodeH3 = p_web.GSV('locFaultCodeH3') ! STRING(30)
     locFaultCodeH4 = p_web.GSV('locFaultCodeH4') ! STRING(30)
     locFaultCodeH5 = p_web.GSV('locFaultCodeH5') ! STRING(30)
     locFaultCodeH6 = p_web.GSV('locFaultCodeH6') ! STRING(30)
     locFaultCodeH7 = p_web.GSV('locFaultCodeH7') ! STRING(30)
     locFaultCodeH8 = p_web.GSV('locFaultCodeH8') ! STRING(30)
     locFaultCodeH9 = p_web.GSV('locFaultCodeH9') ! STRING(30)
     locFaultCodeH10 = p_web.GSV('locFaultCodeH10') ! STRING(30)
     locFaultCodeH11 = p_web.GSV('locFaultCodeH11') ! STRING(30)
     locFaultCodeH12 = p_web.GSV('locFaultCodeH12') ! STRING(30)
     locCurrentJobNumber = p_web.GSV('locCurrentJobNumber') ! LONG
     locHistoricJobNumber = p_web.GSV('locHistoricJobNumber') ! LONG
