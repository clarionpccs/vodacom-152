

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER093.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ForceInvoiceText     PROCEDURE  (func:Type)                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !

  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Invoice Text
    If (def:Force_Invoice_Text = 'B' And func:Type = 'B') Or |
        (def:Force_Invoice_Text <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Invoice_Text = 'B'
    Return Level:Benign
