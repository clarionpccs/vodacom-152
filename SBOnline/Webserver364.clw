

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER364.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER010.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER363.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER365.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER366.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER367.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER368.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER369.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER370.INC'),ONCE        !Req'd for module callout resolution
                     END


FormBrowseBouncers   PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locEngineer          STRING(60)                            !
locExchangeUnit      STRING(60)                            !
locEngineerHistoric  STRING(60)                            !
locExchangeUnitHistoric STRING(60)                         !
                    MAP
LoadJob                 PROCEDURE()
LoadJobHistoric         PROCEDURE()
                    END ! MAP
FilesOpened     Long
BOUNCER::State  USHORT
JOBS_ALIAS::State  USHORT
EXCHANGE::State  USHORT
USERS::State  USHORT
JOBNOTES::State  USHORT
JOBNOTES_ALIAS::State  USHORT
JOBS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormBrowseBouncers')
  loc:formname = 'FormBrowseBouncers_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormBrowseBouncers',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormBrowseBouncers','')
    p_web._DivHeader('FormBrowseBouncers',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormBrowseBouncers',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseBouncers',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseBouncers',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormBrowseBouncers',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormBrowseBouncers',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormBrowseBouncers',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormBrowseBouncers',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(BOUNCER)
  p_web._OpenFile(JOBS_ALIAS)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(USERS)
  p_web._OpenFile(JOBNOTES)
  p_web._OpenFile(JOBNOTES_ALIAS)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(BOUNCER)
  p_Web._CloseFile(JOBS_ALIAS)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(JOBNOTES)
  p_Web._CloseFile(JOBNOTES_ALIAS)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormBrowseBouncers_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:Charge_Type')
    p_web.SetPicture('job:Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Charge_Type','@s30')
  If p_web.IfExistsValue('job:Warranty_Charge_Type')
    p_web.SetPicture('job:Warranty_Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Warranty_Charge_Type','@s30')
  If p_web.IfExistsValue('job:Repair_Type')
    p_web.SetPicture('job:Repair_Type','@s30')
  End
  p_web.SetSessionPicture('job:Repair_Type','@s30')
  If p_web.IfExistsValue('job:Repair_Type_Warranty')
    p_web.SetPicture('job:Repair_Type_Warranty','@s30')
  End
  p_web.SetSessionPicture('job:Repair_Type_Warranty','@s30')
  If p_web.IfExistsValue('job:ESN')
    p_web.SetPicture('job:ESN','@s20')
  End
  p_web.SetSessionPicture('job:ESN','@s20')
  If p_web.IfExistsValue('job:MSN')
    p_web.SetPicture('job:MSN','@s20')
  End
  p_web.SetSessionPicture('job:MSN','@s20')
  If p_web.IfExistsValue('job:Third_Party_Site')
    p_web.SetPicture('job:Third_Party_Site','@s30')
  End
  p_web.SetSessionPicture('job:Third_Party_Site','@s30')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  p_web.SetSessionValue('jbn:Fault_Description',jbn:Fault_Description)
  p_web.SetSessionValue('job:Repair_Type',job:Repair_Type)
  p_web.SetSessionValue('job:Repair_Type_Warranty',job:Repair_Type_Warranty)
  p_web.SetSessionValue('job:ESN',job:ESN)
  p_web.SetSessionValue('locEngineer',locEngineer)
  p_web.SetSessionValue('job:MSN',job:MSN)
  p_web.SetSessionValue('job:Third_Party_Site',job:Third_Party_Site)
  p_web.SetSessionValue('locExchangeUnit',locExchangeUnit)
  p_web.SetSessionValue('job_ali:Charge_Type',job_ali:Charge_Type)
  p_web.SetSessionValue('job_ali:Warranty_Charge_Type',job_ali:Warranty_Charge_Type)
  p_web.SetSessionValue('jbn_ali:Fault_Description',jbn_ali:Fault_Description)
  p_web.SetSessionValue('job_ali:Repair_Type',job_ali:Repair_Type)
  p_web.SetSessionValue('job_ali:Repair_Type_Warranty',job_ali:Repair_Type_Warranty)
  p_web.SetSessionValue('job_ali:ESN',job_ali:ESN)
  p_web.SetSessionValue('locEngineerHistoric',locEngineerHistoric)
  p_web.SetSessionValue('job_ali:MSN',job_ali:MSN)
  p_web.SetSessionValue('job_ali:Third_Party_Site',job_ali:Third_Party_Site)
  p_web.SetSessionValue('locExchangeUnitHistoric',locExchangeUnitHistoric)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('job:Charge_Type')
    job:Charge_Type = p_web.GetValue('job:Charge_Type')
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  End
  if p_web.IfExistsValue('job:Warranty_Charge_Type')
    job:Warranty_Charge_Type = p_web.GetValue('job:Warranty_Charge_Type')
    p_web.SetSessionValue('job:Warranty_Charge_Type',job:Warranty_Charge_Type)
  End
  if p_web.IfExistsValue('jbn:Fault_Description')
    jbn:Fault_Description = p_web.GetValue('jbn:Fault_Description')
    p_web.SetSessionValue('jbn:Fault_Description',jbn:Fault_Description)
  End
  if p_web.IfExistsValue('job:Repair_Type')
    job:Repair_Type = p_web.GetValue('job:Repair_Type')
    p_web.SetSessionValue('job:Repair_Type',job:Repair_Type)
  End
  if p_web.IfExistsValue('job:Repair_Type_Warranty')
    job:Repair_Type_Warranty = p_web.GetValue('job:Repair_Type_Warranty')
    p_web.SetSessionValue('job:Repair_Type_Warranty',job:Repair_Type_Warranty)
  End
  if p_web.IfExistsValue('job:ESN')
    job:ESN = p_web.GetValue('job:ESN')
    p_web.SetSessionValue('job:ESN',job:ESN)
  End
  if p_web.IfExistsValue('locEngineer')
    locEngineer = p_web.GetValue('locEngineer')
    p_web.SetSessionValue('locEngineer',locEngineer)
  End
  if p_web.IfExistsValue('job:MSN')
    job:MSN = p_web.GetValue('job:MSN')
    p_web.SetSessionValue('job:MSN',job:MSN)
  End
  if p_web.IfExistsValue('job:Third_Party_Site')
    job:Third_Party_Site = p_web.GetValue('job:Third_Party_Site')
    p_web.SetSessionValue('job:Third_Party_Site',job:Third_Party_Site)
  End
  if p_web.IfExistsValue('locExchangeUnit')
    locExchangeUnit = p_web.GetValue('locExchangeUnit')
    p_web.SetSessionValue('locExchangeUnit',locExchangeUnit)
  End
  if p_web.IfExistsValue('job_ali:Charge_Type')
    job_ali:Charge_Type = p_web.GetValue('job_ali:Charge_Type')
    p_web.SetSessionValue('job_ali:Charge_Type',job_ali:Charge_Type)
  End
  if p_web.IfExistsValue('job_ali:Warranty_Charge_Type')
    job_ali:Warranty_Charge_Type = p_web.GetValue('job_ali:Warranty_Charge_Type')
    p_web.SetSessionValue('job_ali:Warranty_Charge_Type',job_ali:Warranty_Charge_Type)
  End
  if p_web.IfExistsValue('jbn_ali:Fault_Description')
    jbn_ali:Fault_Description = p_web.GetValue('jbn_ali:Fault_Description')
    p_web.SetSessionValue('jbn_ali:Fault_Description',jbn_ali:Fault_Description)
  End
  if p_web.IfExistsValue('job_ali:Repair_Type')
    job_ali:Repair_Type = p_web.GetValue('job_ali:Repair_Type')
    p_web.SetSessionValue('job_ali:Repair_Type',job_ali:Repair_Type)
  End
  if p_web.IfExistsValue('job_ali:Repair_Type_Warranty')
    job_ali:Repair_Type_Warranty = p_web.GetValue('job_ali:Repair_Type_Warranty')
    p_web.SetSessionValue('job_ali:Repair_Type_Warranty',job_ali:Repair_Type_Warranty)
  End
  if p_web.IfExistsValue('job_ali:ESN')
    job_ali:ESN = p_web.GetValue('job_ali:ESN')
    p_web.SetSessionValue('job_ali:ESN',job_ali:ESN)
  End
  if p_web.IfExistsValue('locEngineerHistoric')
    locEngineerHistoric = p_web.GetValue('locEngineerHistoric')
    p_web.SetSessionValue('locEngineerHistoric',locEngineerHistoric)
  End
  if p_web.IfExistsValue('job_ali:MSN')
    job_ali:MSN = p_web.GetValue('job_ali:MSN')
    p_web.SetSessionValue('job_ali:MSN',job_ali:MSN)
  End
  if p_web.IfExistsValue('job_ali:Third_Party_Site')
    job_ali:Third_Party_Site = p_web.GetValue('job_ali:Third_Party_Site')
    p_web.SetSessionValue('job_ali:Third_Party_Site',job_ali:Third_Party_Site)
  End
  if p_web.IfExistsValue('locExchangeUnitHistoric')
    locExchangeUnitHistoric = p_web.GetValue('locExchangeUnitHistoric')
    p_web.SetSessionValue('locExchangeUnitHistoric',locExchangeUnitHistoric)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormBrowseBouncers_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
      p_web.site.CancelButton.TextValue = 'Close'
      p_web.site.CancelButton.Image = '/images/psave.png'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locEngineer = p_web.RestoreValue('locEngineer')
 locExchangeUnit = p_web.RestoreValue('locExchangeUnit')
 locEngineerHistoric = p_web.RestoreValue('locEngineerHistoric')
 locExchangeUnitHistoric = p_web.RestoreValue('locExchangeUnitHistoric')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormBrowseBouncers')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormBrowseBouncers_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormBrowseBouncers_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormBrowseBouncers_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormBrowseBouncers" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormBrowseBouncers" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormBrowseBouncers" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Browse Bouncers') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Browse Bouncers',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormBrowseBouncers">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormBrowseBouncers" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormBrowseBouncers')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Browse Current Jobs') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Browse Historic Jobs') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormBrowseBouncers')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormBrowseBouncers'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormBrowseBouncers_BrowseBouncerCurrentJob_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormBrowseBouncers_BrowseSimpleChaPartsList_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormBrowseBouncers_BrowseSimpleWarPartsList_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormBrowseBouncers_BrowseBouncerHistoryJob_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormBrowseBouncers_BrowseSimpleChaAliasPartsList_embedded_div')&''');'
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormBrowseBouncers_BrowseSimpleWarAliasPartsList_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormBrowseBouncers')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Browse Current Jobs') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormBrowseBouncers_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Current Jobs')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Current Jobs')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Current Jobs')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Current Jobs')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwJob
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      LoadJob()
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormBrowseBouncers_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Warranty_Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Warranty_Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& ' rowspan="5">'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn:Fault_Description
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' valign="top" rowspan="5">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn:Fault_Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Repair_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Repair_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Repair_Type_Warranty
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Repair_Type_Warranty
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:ESN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEngineer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEngineer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:MSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Third_Party_Site
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Third_Party_Site
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExchangeUnit
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="4">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExchangeUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwCParts
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwWParts
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnCountBouncers
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnCompareFaultCodes
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Browse Historic Jobs') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormBrowseBouncers_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Historic Jobs')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Historic Jobs')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Historic Jobs')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Browse Historic Jobs')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwHistoric
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      LoadJobHistoric()
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormBrowseBouncers_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job_ali:Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job_ali:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job_ali:Warranty_Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job_ali:Warranty_Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& ' rowspan="5">'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::jbn_ali:Fault_Description
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' valign="top" rowspan="5">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::jbn_ali:Fault_Description
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job_ali:Repair_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job_ali:Repair_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job_ali:Repair_Type_Warranty
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job_ali:Repair_Type_Warranty
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job_ali:ESN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job_ali:ESN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locEngineerHistoric
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locEngineerHistoric
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job_ali:MSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job_ali:MSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job_ali:Third_Party_Site
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job_ali:Third_Party_Site
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locExchangeUnitHistoric
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&' colspan="4">'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locExchangeUnitHistoric
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwPartsAlias
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwWPartsAlias
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::brwJob  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwJob',p_web.GetValue('NewValue'))
    do Value::brwJob
  Else
    p_web.StoreValue('job:Ref_Number')
  End
    LoadJob()
    
    do Prompt::job:Charge_Type
    do Value::job:Charge_Type  !1
    do Prompt::job:Repair_Type
    do Value::job:Repair_Type  !1
    do Prompt::job:Repair_Type_Warranty
    do Value::job:Repair_Type_Warranty  !1
    do Prompt::job:Warranty_Charge_Type
    do Value::job:Warranty_Charge_Type  !1
    DO Prompt::job:ESN
    do Value::job:ESN  !1
    do Prompt::job:MSN
    do Value::job:MSN  !1
    do Prompt::job:Third_Party_Site
    do Value::job:Third_Party_Site  !1
    do Value::locEngineer  !1
    do Prompt::locExchangeUnit
    do Value::locExchangeUnit  !1
    do Value::brwCParts  !1
    do Value::brwWParts  !1
    do Value::brwHistoric  !1
    
    LoadJobHistoric()
    
    DO Prompt::jbn_ali:Fault_Description
    DO Value::jbn_ali:Fault_Description
    DO Prompt::job_ali:Charge_Type
    DO Value::job_ali:Charge_Type
    DO Prompt::job_ali:Warranty_Charge_Type
    DO Value::job_ali:Warranty_Charge_Type
    DO Prompt::job_ali:Repair_Type
    DO Value::job_ali:Repair_Type
    DO Prompt::job_ali:Repair_Type_Warranty
    DO Value::job_ali:Repair_Type_Warranty
    DO Prompt::job_ali:ESN
    DO Value::job_ali:ESN
    DO Prompt::job_ali:MSN
    DO Value::job_ali:MSN
    DO Value::locEngineerHistoric
    DO Prompt::job_ali:Third_Party_Site
    DO Value::job_ali:Third_Party_Site
    DO Prompt::locExchangeUnitHistoric
    DO Value::locExchangeUnitHistoric 	
    DO Value::brwPartsAlias
    DO Value::brwWPartsAlias
    DO Value::btnCountBouncers
    DO Value::btnCompareFaultCodes
  	
    
  do SendAlert
  do Prompt::jbn:Fault_Description
  do Value::jbn:Fault_Description  !1

Value::brwJob  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseBouncerCurrentJob --
  p_web.SetValue('BrowseBouncerCurrentJob:NoForm',1)
  p_web.SetValue('BrowseBouncerCurrentJob:FormName',loc:formname)
  p_web.SetValue('BrowseBouncerCurrentJob:parentIs','Form')
  p_web.SetValue('_parentProc','FormBrowseBouncers')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormBrowseBouncers_BrowseBouncerCurrentJob_embedded_div')&'"><!-- Net:BrowseBouncerCurrentJob --></div><13,10>'
    p_web._DivHeader('FormBrowseBouncers_' & lower('BrowseBouncerCurrentJob') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormBrowseBouncers_' & lower('BrowseBouncerCurrentJob') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseBouncerCurrentJob --><13,10>'
  end
  do SendPacket


Prompt::job:Charge_Type  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job:Charge_Type') & '_prompt',Choose(p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Cha Charge Type:')
  If p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:Chargeable_Job') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Charge_Type',p_web.GetValue('NewValue'))
    job:Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Charge_Type
    do Value::job:Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Charge_Type = p_web.GetValue('Value')
  End

Value::job:Charge_Type  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job:Charge_Type') & '_value',Choose(p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:Chargeable_Job') <> 'YES')
  ! --- DISPLAY --- job:Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::job:Warranty_Charge_Type  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job:Warranty_Charge_Type') & '_prompt',Choose(p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:Warranty_Job') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Warr Charge Type:')
  If p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Warranty_Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Warranty_Charge_Type',p_web.GetValue('NewValue'))
    job:Warranty_Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Warranty_Charge_Type
    do Value::job:Warranty_Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Warranty_Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Warranty_Charge_Type = p_web.GetValue('Value')
  End

Value::job:Warranty_Charge_Type  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job:Warranty_Charge_Type') & '_value',Choose(p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:Warranty_Job') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:Warranty_Job') <> 'YES')
  ! --- DISPLAY --- job:Warranty_Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Warranty_Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::jbn:Fault_Description  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('jbn:Fault_Description') & '_prompt',Choose(p_web.GSV('jbn:Fault_Description') = '' OR p_web.GSV('JobFound') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Fault Description:')
  If p_web.GSV('jbn:Fault_Description') = '' OR p_web.GSV('JobFound') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseBouncers_' & p_web._nocolon('jbn:Fault_Description') & '_prompt')

Validate::jbn:Fault_Description  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn:Fault_Description',p_web.GetValue('NewValue'))
    jbn:Fault_Description = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn:Fault_Description
    do Value::jbn:Fault_Description
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn:Fault_Description',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jbn:Fault_Description = p_web.GetValue('Value')
  End

Value::jbn:Fault_Description  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('jbn:Fault_Description') & '_value',Choose(p_web.GSV('jbn:Fault_Description') = '' OR p_web.GSV('JobFound') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('jbn:Fault_Description') = '' OR p_web.GSV('JobFound') <> 1)
  ! --- TEXT --- jbn:Fault_Description
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('jbn:Fault_Description')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('jbn:Fault_Description',p_web.GetSessionValue('jbn:Fault_Description'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(jbn:Fault_Description),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseBouncers_' & p_web._nocolon('jbn:Fault_Description') & '_value')


Prompt::job:Repair_Type  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job:Repair_Type') & '_prompt',Choose(p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Cha Repair Type:')
  If p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:Chargeable_Job') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Repair_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Repair_Type',p_web.GetValue('NewValue'))
    job:Repair_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Repair_Type
    do Value::job:Repair_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Repair_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Repair_Type = p_web.GetValue('Value')
  End

Value::job:Repair_Type  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job:Repair_Type') & '_value',Choose(p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:Chargeable_Job') <> 'YES')
  ! --- DISPLAY --- job:Repair_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Repair_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::job:Repair_Type_Warranty  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job:Repair_Type_Warranty') & '_prompt',Choose(p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:Warranty_Job') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Warr Repair Type:')
  If p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Repair_Type_Warranty  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Repair_Type_Warranty',p_web.GetValue('NewValue'))
    job:Repair_Type_Warranty = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Repair_Type_Warranty
    do Value::job:Repair_Type_Warranty
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Repair_Type_Warranty',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Repair_Type_Warranty = p_web.GetValue('Value')
  End

Value::job:Repair_Type_Warranty  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job:Repair_Type_Warranty') & '_value',Choose(p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:Warranty_Job') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:Warranty_Job') <> 'YES')
  ! --- DISPLAY --- job:Repair_Type_Warranty
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Repair_Type_Warranty'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::job:ESN  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job:ESN') & '_prompt',Choose(p_web.GSV('JobFound') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('I.M.E.I. Number:')
  If p_web.GSV('JobFound') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:ESN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:ESN',p_web.GetValue('NewValue'))
    job:ESN = p_web.GetValue('NewValue') !FieldType= STRING Field = job:ESN
    do Value::job:ESN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:ESN',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    job:ESN = p_web.GetValue('Value')
  End

Value::job:ESN  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job:ESN') & '_value',Choose(p_web.GSV('JobFound') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('JobFound') <> 1)
  ! --- DISPLAY --- job:ESN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:ESN'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::locEngineer  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('locEngineer') & '_prompt',Choose(p_web.GSV('locEngineer') = '' OR p_web.GSV('JobFound') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Engineer:')
  If p_web.GSV('locEngineer') = '' OR p_web.GSV('JobFound') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locEngineer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEngineer',p_web.GetValue('NewValue'))
    locEngineer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEngineer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEngineer',p_web.GetValue('Value'))
    locEngineer = p_web.GetValue('Value')
  End

Value::locEngineer  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('locEngineer') & '_value',Choose(p_web.GSV('locEngineer') = '' OR p_web.GSV('JobFound') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locEngineer') = '' OR p_web.GSV('JobFound') <> 1)
  ! --- DISPLAY --- locEngineer
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locEngineer'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::job:MSN  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job:MSN') & '_prompt',Choose(p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:MSN') = '' OR p_web.GSV('job:MSN') = 'N/A','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('M.S.N.:')
  If p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:MSN') = '' OR p_web.GSV('job:MSN') = 'N/A'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:MSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:MSN',p_web.GetValue('NewValue'))
    job:MSN = p_web.GetValue('NewValue') !FieldType= STRING Field = job:MSN
    do Value::job:MSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:MSN',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    job:MSN = p_web.GetValue('Value')
  End

Value::job:MSN  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job:MSN') & '_value',Choose(p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:MSN') = '' OR p_web.GSV('job:MSN') = 'N/A','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:MSN') = '' OR p_web.GSV('job:MSN') = 'N/A')
  ! --- DISPLAY --- job:MSN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:MSN'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::job:Third_Party_Site  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job:Third_Party_Site') & '_prompt',Choose(p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:Third_Party_Site') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Third Party Site:')
  If p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:Third_Party_Site') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Third_Party_Site  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Third_Party_Site',p_web.GetValue('NewValue'))
    job:Third_Party_Site = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Third_Party_Site
    do Value::job:Third_Party_Site
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Third_Party_Site',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Third_Party_Site = p_web.GetValue('Value')
  End

Value::job:Third_Party_Site  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job:Third_Party_Site') & '_value',Choose(p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:Third_Party_Site') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:Third_Party_Site') = '')
  ! --- DISPLAY --- job:Third_Party_Site
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Third_Party_Site'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::locExchangeUnit  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('locExchangeUnit') & '_prompt',Choose(p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:exchange_unit_Number') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Exchange Unit:')
  If p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:exchange_unit_Number') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locExchangeUnit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExchangeUnit',p_web.GetValue('NewValue'))
    locExchangeUnit = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExchangeUnit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locExchangeUnit',p_web.GetValue('Value'))
    locExchangeUnit = p_web.GetValue('Value')
  End

Value::locExchangeUnit  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('locExchangeUnit') & '_value',Choose(p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:exchange_unit_Number') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:exchange_unit_Number') = 0)
  ! --- DISPLAY --- locExchangeUnit
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locExchangeUnit'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Validate::brwCParts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwCParts',p_web.GetValue('NewValue'))
    do Value::brwCParts
  Else
    p_web.StoreValue('par:Record_Number')
  End

Value::brwCParts  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:Chargeable_Job') <> 'YES',1,0))
  ! --- BROWSE ---  BrowseSimpleChaPartsList --
  p_web.SetValue('BrowseSimpleChaPartsList:NoForm',1)
  p_web.SetValue('BrowseSimpleChaPartsList:FormName',loc:formname)
  p_web.SetValue('BrowseSimpleChaPartsList:parentIs','Form')
  p_web.SetValue('_parentProc','FormBrowseBouncers')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormBrowseBouncers_BrowseSimpleChaPartsList_embedded_div')&'"><!-- Net:BrowseSimpleChaPartsList --></div><13,10>'
    p_web._DivHeader('FormBrowseBouncers_' & lower('BrowseSimpleChaPartsList') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormBrowseBouncers_' & lower('BrowseSimpleChaPartsList') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseSimpleChaPartsList --><13,10>'
  end
  do SendPacket


Validate::brwWParts  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwWParts',p_web.GetValue('NewValue'))
    do Value::brwWParts
  Else
    p_web.StoreValue('wpr:Record_Number')
  End

Value::brwWParts  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('JobFound') <> 1 OR p_web.GSV('job:Warranty_Job') <> 'YES',1,0))
  ! --- BROWSE ---  BrowseSimpleWarPartsList --
  p_web.SetValue('BrowseSimpleWarPartsList:NoForm',1)
  p_web.SetValue('BrowseSimpleWarPartsList:FormName',loc:formname)
  p_web.SetValue('BrowseSimpleWarPartsList:parentIs','Form')
  p_web.SetValue('_parentProc','FormBrowseBouncers')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormBrowseBouncers_BrowseSimpleWarPartsList_embedded_div')&'"><!-- Net:BrowseSimpleWarPartsList --></div><13,10>'
    p_web._DivHeader('FormBrowseBouncers_' & lower('BrowseSimpleWarPartsList') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormBrowseBouncers_' & lower('BrowseSimpleWarPartsList') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseSimpleWarPartsList --><13,10>'
  end
  do SendPacket


Validate::btnCountBouncers  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnCountBouncers',p_web.GetValue('NewValue'))
    do Value::btnCountBouncers
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnCountBouncers  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('btnCountBouncers') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnCountBouncers','Count Bouncers','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=CountBouncers')) & ''','''&clip('_blank')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Validate::btnCompareFaultCodes  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnCompareFaultCodes',p_web.GetValue('NewValue'))
    do Value::btnCompareFaultCodes
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnCompareFaultCodes  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('btnCompareFaultCodes') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnCompareFaultCodes','Compare Fault Codes','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormCompareBouncerFaultCodes?' &'curjobno=' & p_web.GSV('job:Ref_Number') & '&hisjobno=' & p_web.GSV('job_ali:Ref_Number'))) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()


Validate::brwHistoric  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwHistoric',p_web.GetValue('NewValue'))
    do Value::brwHistoric
  Else
    p_web.StoreValue('bou:Record_Number')
  End
    LoadJobHistoric()
    
    DO Prompt::job_ali:Charge_Type
    DO Value::job_ali:Charge_Type
    DO Prompt::job_ali:Warranty_Charge_Type
    DO Value::job_ali:Warranty_Charge_Type
    DO Prompt::job_ali:Repair_Type
    DO Value::job_ali:Repair_Type
    DO Prompt::job_ali:Repair_Type_Warranty
    DO Value::job_ali:Repair_Type_Warranty
    DO Prompt::job_ali:ESN
    DO Value::job_ali:ESN
    DO Prompt::job_ali:MSN
    DO Value::job_ali:MSN
    DO Value::locEngineerHistoric
    DO Prompt::job_ali:Third_Party_Site
    DO Value::job_ali:Third_Party_Site
    DO Prompt::locExchangeUnitHistoric
    DO Value::locExchangeUnitHistoric  	
    DO Value::brwPartsAlias
    DO Value::brwWPartsAlias
    DO Value::btnCountBouncers
    DO Value::btnCompareFaultCodes
    
  do SendAlert
  do Prompt::jbn_ali:Fault_Description
  do Value::jbn_ali:Fault_Description  !1

Value::brwHistoric  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseBouncerHistoryJob --
  p_web.SetValue('BrowseBouncerHistoryJob:NoForm',1)
  p_web.SetValue('BrowseBouncerHistoryJob:FormName',loc:formname)
  p_web.SetValue('BrowseBouncerHistoryJob:parentIs','Form')
  p_web.SetValue('_parentProc','FormBrowseBouncers')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormBrowseBouncers_BrowseBouncerHistoryJob_embedded_div')&'"><!-- Net:BrowseBouncerHistoryJob --></div><13,10>'
    p_web._DivHeader('FormBrowseBouncers_' & lower('BrowseBouncerHistoryJob') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormBrowseBouncers_' & lower('BrowseBouncerHistoryJob') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseBouncerHistoryJob --><13,10>'
  end
  do SendPacket


Prompt::job_ali:Charge_Type  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job_ali:Charge_Type') & '_prompt',Choose(p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job_ali:Chargeable_Job') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Cha Charge Type:')
  If p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job_ali:Chargeable_Job') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job_ali:Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job_ali:Charge_Type',p_web.GetValue('NewValue'))
    job_ali:Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job_ali:Charge_Type
    do Value::job_ali:Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job_ali:Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job_ali:Charge_Type = p_web.GetValue('Value')
  End

Value::job_ali:Charge_Type  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job_ali:Charge_Type') & '_value',Choose(p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job_ali:Chargeable_Job') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job_ali:Chargeable_Job') <> 'YES')
  ! --- DISPLAY --- job_ali:Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job_ali:Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::job_ali:Warranty_Charge_Type  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job_ali:Warranty_Charge_Type') & '_prompt',Choose(p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job:Warranty_Job') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Warr Charge Type:')
  If p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job_ali:Warranty_Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job_ali:Warranty_Charge_Type',p_web.GetValue('NewValue'))
    job_ali:Warranty_Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job_ali:Warranty_Charge_Type
    do Value::job_ali:Warranty_Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job_ali:Warranty_Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job_ali:Warranty_Charge_Type = p_web.GetValue('Value')
  End

Value::job_ali:Warranty_Charge_Type  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job_ali:Warranty_Charge_Type') & '_value',Choose(p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job:Warranty_Job') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job:Warranty_Job') <> 'YES')
  ! --- DISPLAY --- job_ali:Warranty_Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job_ali:Warranty_Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::jbn_ali:Fault_Description  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('jbn_ali:Fault_Description') & '_prompt',Choose(p_web.GSV('jbn_ali:Fault_Description') = '' OR p_web.GSV('JobFoundHistoric') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Fault Description:')
  If p_web.GSV('jbn_ali:Fault_Description') = '' OR p_web.GSV('JobFoundHistoric') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseBouncers_' & p_web._nocolon('jbn_ali:Fault_Description') & '_prompt')

Validate::jbn_ali:Fault_Description  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('jbn_ali:Fault_Description',p_web.GetValue('NewValue'))
    jbn_ali:Fault_Description = p_web.GetValue('NewValue') !FieldType= STRING Field = jbn_ali:Fault_Description
    do Value::jbn_ali:Fault_Description
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('jbn_ali:Fault_Description',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    jbn_ali:Fault_Description = p_web.GetValue('Value')
  End
    jbn_ali:Fault_Description = Upper(jbn_ali:Fault_Description)
    p_web.SetSessionValue('jbn_ali:Fault_Description',jbn_ali:Fault_Description)
  do Value::jbn_ali:Fault_Description
  do SendAlert

Value::jbn_ali:Fault_Description  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('jbn_ali:Fault_Description') & '_value',Choose(p_web.GSV('jbn_ali:Fault_Description') = '' OR p_web.GSV('JobFoundHistoric') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('jbn_ali:Fault_Description') = '' OR p_web.GSV('JobFoundHistoric') <> 1)
  ! --- TEXT --- jbn_ali:Fault_Description
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('jbn_ali:Fault_Description')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''jbn_ali:Fault_Description'',''formbrowsebouncers_jbn_ali:fault_description_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = 'readonly'
  packet = clip(packet) & p_web.CreateTextArea('jbn_ali:Fault_Description',p_web.GetSessionValue('jbn_ali:Fault_Description'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,size(jbn_ali:Fault_Description),,Net:Web:Control) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormBrowseBouncers_' & p_web._nocolon('jbn_ali:Fault_Description') & '_value')


Prompt::job_ali:Repair_Type  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job_ali:Repair_Type') & '_prompt',Choose(p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Cha Repair Type:')
  If p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job:Chargeable_Job') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job_ali:Repair_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job_ali:Repair_Type',p_web.GetValue('NewValue'))
    job_ali:Repair_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job_ali:Repair_Type
    do Value::job_ali:Repair_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job_ali:Repair_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job_ali:Repair_Type = p_web.GetValue('Value')
  End

Value::job_ali:Repair_Type  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job_ali:Repair_Type') & '_value',Choose(p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job:Chargeable_Job') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job:Chargeable_Job') <> 'YES')
  ! --- DISPLAY --- job_ali:Repair_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job_ali:Repair_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::job_ali:Repair_Type_Warranty  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job_ali:Repair_Type_Warranty') & '_prompt',Choose(p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job:Warranty_Job') <> 'YES','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Warr Repair Type:')
  If p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job:Warranty_Job') <> 'YES'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job_ali:Repair_Type_Warranty  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job_ali:Repair_Type_Warranty',p_web.GetValue('NewValue'))
    job_ali:Repair_Type_Warranty = p_web.GetValue('NewValue') !FieldType= STRING Field = job_ali:Repair_Type_Warranty
    do Value::job_ali:Repair_Type_Warranty
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job_ali:Repair_Type_Warranty',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job_ali:Repair_Type_Warranty = p_web.GetValue('Value')
  End

Value::job_ali:Repair_Type_Warranty  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job_ali:Repair_Type_Warranty') & '_value',Choose(p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job:Warranty_Job') <> 'YES','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job:Warranty_Job') <> 'YES')
  ! --- DISPLAY --- job_ali:Repair_Type_Warranty
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job_ali:Repair_Type_Warranty'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::job_ali:ESN  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job_ali:ESN') & '_prompt',Choose(p_web.GSV('JobFoundHistoric') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('I.M.E.I. Number:')
  If p_web.GSV('JobFoundHistoric') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job_ali:ESN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job_ali:ESN',p_web.GetValue('NewValue'))
    job_ali:ESN = p_web.GetValue('NewValue') !FieldType= STRING Field = job_ali:ESN
    do Value::job_ali:ESN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job_ali:ESN',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    job_ali:ESN = p_web.GetValue('Value')
  End

Value::job_ali:ESN  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job_ali:ESN') & '_value',Choose(p_web.GSV('JobFoundHistoric') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('JobFoundHistoric') <> 1)
  ! --- DISPLAY --- job_ali:ESN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job_ali:ESN'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::locEngineerHistoric  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('locEngineerHistoric') & '_prompt',Choose(p_web.GSV('locEngineerHistoric') = '' OR p_web.GSV('JobFoundHistoric') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Engineer:')
  If p_web.GSV('locEngineerHistoric') = '' OR p_web.GSV('JobFoundHistoric') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locEngineerHistoric  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locEngineerHistoric',p_web.GetValue('NewValue'))
    locEngineerHistoric = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locEngineerHistoric
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locEngineerHistoric',p_web.GetValue('Value'))
    locEngineerHistoric = p_web.GetValue('Value')
  End

Value::locEngineerHistoric  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('locEngineerHistoric') & '_value',Choose(p_web.GSV('locEngineerHistoric') = '' OR p_web.GSV('JobFoundHistoric') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locEngineerHistoric') = '' OR p_web.GSV('JobFoundHistoric') <> 1)
  ! --- DISPLAY --- locEngineerHistoric
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locEngineerHistoric'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::job_ali:MSN  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job_ali:MSN') & '_prompt',Choose(p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job_ali:MSN') = '' OR p_web.GSV('job_ali:MSN')  = 'N/A','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('M.S.N.:')
  If p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job_ali:MSN') = '' OR p_web.GSV('job_ali:MSN')  = 'N/A'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job_ali:MSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job_ali:MSN',p_web.GetValue('NewValue'))
    job_ali:MSN = p_web.GetValue('NewValue') !FieldType= STRING Field = job_ali:MSN
    do Value::job_ali:MSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job_ali:MSN',p_web.dFormat(p_web.GetValue('Value'),'@s20'))
    job_ali:MSN = p_web.GetValue('Value')
  End

Value::job_ali:MSN  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job_ali:MSN') & '_value',Choose(p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job_ali:MSN') = '' OR p_web.GSV('job_ali:MSN')  = 'N/A','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job_ali:MSN') = '' OR p_web.GSV('job_ali:MSN')  = 'N/A')
  ! --- DISPLAY --- job_ali:MSN
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job_ali:MSN'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::job_ali:Third_Party_Site  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job_ali:Third_Party_Site') & '_prompt',Choose(p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job_ali:Third_Party_Site') = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Third Party Site:')
  If p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job_ali:Third_Party_Site') = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job_ali:Third_Party_Site  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job_ali:Third_Party_Site',p_web.GetValue('NewValue'))
    job_ali:Third_Party_Site = p_web.GetValue('NewValue') !FieldType= STRING Field = job_ali:Third_Party_Site
    do Value::job_ali:Third_Party_Site
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job_ali:Third_Party_Site',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job_ali:Third_Party_Site = p_web.GetValue('Value')
  End

Value::job_ali:Third_Party_Site  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('job_ali:Third_Party_Site') & '_value',Choose(p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job_ali:Third_Party_Site') = '','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job_ali:Third_Party_Site') = '')
  ! --- DISPLAY --- job_ali:Third_Party_Site
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job_ali:Third_Party_Site'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Prompt::locExchangeUnitHistoric  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('locExchangeUnitHistoric') & '_prompt',Choose(p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job_ali:Exchange_Unit_Number') = 0,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Exchange Unit:')
  If p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job_ali:Exchange_Unit_Number') = 0
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locExchangeUnitHistoric  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locExchangeUnitHistoric',p_web.GetValue('NewValue'))
    locExchangeUnitHistoric = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locExchangeUnitHistoric
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locExchangeUnitHistoric',p_web.GetValue('Value'))
    locExchangeUnitHistoric = p_web.GetValue('Value')
  End

Value::locExchangeUnitHistoric  Routine
  p_web._DivHeader('FormBrowseBouncers_' & p_web._nocolon('locExchangeUnitHistoric') & '_value',Choose(p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job_ali:Exchange_Unit_Number') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job_ali:Exchange_Unit_Number') = 0)
  ! --- DISPLAY --- locExchangeUnitHistoric
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locExchangeUnitHistoric'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()


Validate::brwPartsAlias  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwPartsAlias',p_web.GetValue('NewValue'))
    do Value::brwPartsAlias
  Else
    p_web.StoreValue('par:Record_Number')
  End

Value::brwPartsAlias  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job_ali:Chargeable_job') <> 'YES',1,0))
  ! --- BROWSE ---  BrowseSimpleChaAliasPartsList --
  p_web.SetValue('BrowseSimpleChaAliasPartsList:NoForm',1)
  p_web.SetValue('BrowseSimpleChaAliasPartsList:FormName',loc:formname)
  p_web.SetValue('BrowseSimpleChaAliasPartsList:parentIs','Form')
  p_web.SetValue('_parentProc','FormBrowseBouncers')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormBrowseBouncers_BrowseSimpleChaAliasPartsList_embedded_div')&'"><!-- Net:BrowseSimpleChaAliasPartsList --></div><13,10>'
    p_web._DivHeader('FormBrowseBouncers_' & lower('BrowseSimpleChaAliasPartsList') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormBrowseBouncers_' & lower('BrowseSimpleChaAliasPartsList') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseSimpleChaAliasPartsList --><13,10>'
  end
  do SendPacket


Validate::brwWPartsAlias  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwWPartsAlias',p_web.GetValue('NewValue'))
    do Value::brwWPartsAlias
  Else
    p_web.StoreValue('wpr:Record_Number')
  End

Value::brwWPartsAlias  Routine
  loc:extra = ''
  p_web.SetValue('_Silent',Choose(p_web.GSV('JobFoundHistoric') <> 1 OR p_web.GSV('job_ali:Warranty_Job') <> 'YES',1,0))
  ! --- BROWSE ---  BrowseSimpleWarAliasPartsList --
  p_web.SetValue('BrowseSimpleWarAliasPartsList:NoForm',1)
  p_web.SetValue('BrowseSimpleWarAliasPartsList:FormName',loc:formname)
  p_web.SetValue('BrowseSimpleWarAliasPartsList:parentIs','Form')
  p_web.SetValue('_parentProc','FormBrowseBouncers')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormBrowseBouncers_BrowseSimpleWarAliasPartsList_embedded_div')&'"><!-- Net:BrowseSimpleWarAliasPartsList --></div><13,10>'
    p_web._DivHeader('FormBrowseBouncers_' & lower('BrowseSimpleWarAliasPartsList') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormBrowseBouncers_' & lower('BrowseSimpleWarAliasPartsList') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseSimpleWarAliasPartsList --><13,10>'
  end
  do SendPacket


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormBrowseBouncers_BrowseBouncerCurrentJob_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::brwJob
      else
        do Value::brwJob
      end
  of lower('FormBrowseBouncers_BrowseBouncerHistoryJob_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::brwHistoric
      else
        do Value::brwHistoric
      end
  of lower('FormBrowseBouncers_jbn_ali:Fault_Description_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::jbn_ali:Fault_Description
      else
        do Value::jbn_ali:Fault_Description
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormBrowseBouncers_form:ready_',1)
  p_web.SetSessionValue('FormBrowseBouncers_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormBrowseBouncers',0)

PreCopy  Routine
  p_web.SetValue('FormBrowseBouncers_form:ready_',1)
  p_web.SetSessionValue('FormBrowseBouncers_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormBrowseBouncers',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormBrowseBouncers_form:ready_',1)
  p_web.SetSessionValue('FormBrowseBouncers_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormBrowseBouncers:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormBrowseBouncers_form:ready_',1)
  p_web.SetSessionValue('FormBrowseBouncers_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormBrowseBouncers:Primed',0)
  p_web.setsessionvalue('showtab_FormBrowseBouncers',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormBrowseBouncers_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormBrowseBouncers_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
      If not (p_web.GSV('jbn:Fault_Description') = '' OR p_web.GSV('JobFound') <> 1)
          jbn:Fault_Description = Upper(jbn:Fault_Description)
          p_web.SetSessionValue('jbn:Fault_Description',jbn:Fault_Description)
        If loc:Invalid <> '' then exit.
      End
  ! tab = 3
    loc:InvalidTab += 1
  ! tab = 4
    loc:InvalidTab += 1
      If not (p_web.GSV('jbn_ali:Fault_Description') = '' OR p_web.GSV('JobFoundHistoric') <> 1)
          jbn_ali:Fault_Description = Upper(jbn_ali:Fault_Description)
          p_web.SetSessionValue('jbn_ali:Fault_Description',jbn_ali:Fault_Description)
        If loc:Invalid <> '' then exit.
      End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormBrowseBouncers:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('job:Charge_Type')
  p_web.StoreValue('job:Warranty_Charge_Type')
  p_web.StoreValue('jbn:Fault_Description')
  p_web.StoreValue('job:Repair_Type')
  p_web.StoreValue('job:Repair_Type_Warranty')
  p_web.StoreValue('job:ESN')
  p_web.StoreValue('locEngineer')
  p_web.StoreValue('job:MSN')
  p_web.StoreValue('job:Third_Party_Site')
  p_web.StoreValue('locExchangeUnit')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('job_ali:Charge_Type')
  p_web.StoreValue('job_ali:Warranty_Charge_Type')
  p_web.StoreValue('jbn_ali:Fault_Description')
  p_web.StoreValue('job_ali:Repair_Type')
  p_web.StoreValue('job_ali:Repair_Type_Warranty')
  p_web.StoreValue('job_ali:ESN')
  p_web.StoreValue('locEngineerHistoric')
  p_web.StoreValue('job_ali:MSN')
  p_web.StoreValue('job_ali:Third_Party_Site')
  p_web.StoreValue('locExchangeUnitHistoric')
  p_web.StoreValue('')
  p_web.StoreValue('')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locEngineer',locEngineer) ! STRING(60)
     p_web.SSV('locExchangeUnit',locExchangeUnit) ! STRING(60)
     p_web.SSV('locEngineerHistoric',locEngineerHistoric) ! STRING(60)
     p_web.SSV('locExchangeUnitHistoric',locExchangeUnitHistoric) ! STRING(60)
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locEngineer = p_web.GSV('locEngineer') ! STRING(60)
     locExchangeUnit = p_web.GSV('locExchangeUnit') ! STRING(60)
     locEngineerHistoric = p_web.GSV('locEngineerHistoric') ! STRING(60)
     locExchangeUnitHistoric = p_web.GSV('locExchangeUnitHistoric') ! STRING(60)
LoadJob             PROCEDURE()
    CODE
        p_web.SSV('JobFound',0)
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = p_web.GSV('job:Ref_Number')
        IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
            p_web.FileToSessionQueue(JOBS)
            p_web.SSV('JobFound',1)
        
            Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
            jbn:RefNumber = job:Ref_Number
            IF (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
                p_web.FileToSessionQueue(JOBNOTES)
            END ! IF
            IF (job:Engineer = '')
                p_web.SSV('locEngineer','')
            ELSE
                Access:USERS.ClearKey(use:User_Code_Key)
                use:User_Code = job:Engineer
                IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
                    p_web.SSV('locEngineer',CLIP(use:Forename) & ' ' & CLIP(use:Surname))
                ELSE ! IF
                END ! IF
            END !IF

            IF (job:Exchange_Unit_Number = 0)
                p_web.SSV('locExchangeUnit','')
            ELSE ! IF
                Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                xch:Ref_Number = job:Exchange_Unit_Number	
                IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                    p_web.SSV('locExchangeUnit',CLIP(xch:Model_Number) & '  -  ' & CLIP(xch:ESN))
                    IF (xch:MSN <> '')
                        p_web.SSV('locExchangeUnit',p_web.GSV('locExchangeUnit') & ' - ' & CLIP(xch:MSN))
                    END ! IF
                ELSE ! IF
                    p_web.SSV('locExchangeUnit','Cannot Find Unit!')
                END ! IF
            END ! IF
            
        END ! IF
        
LoadJobHistoric             PROCEDURE()
    CODE
        p_web.SSV('JobFoundHistoric',0)
        
        
        
        Access:BOUNCER.ClearKey(bou:Record_Number_Key)
        bou:Record_Number = p_web.GSV('bou:Record_Number')
        IF (Access:BOUNCER.TryFetch(bou:Record_Number_Key) = Level:Benign)
            
            Access:JOBS_ALIAS.ClearKey(job_ali:Ref_Number_Key)
            job_ali:Ref_Number = bou:Bouncer_Job_Number
            IF (Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign)
                
                p_web.SSV('JobFoundHistoric',1)
                
                p_web.FileToSessionQueue(JOBS_ALIAS)
            END ! IF
            
            Access:JOBNOTES_ALIAS.ClearKey(jbn_ali:RefNumberKey)
            jbn_ali:RefNumber = job_ali:Ref_Number
            IF (Access:JOBNOTES_ALIAS.TryFetch(jbn_ali:RefNumberKey) = Level:Benign)
                p_web.FileToSessionQueue(JOBNOTES_ALIAS)
            END ! IF
            IF (job_ali:Engineer = '')
                p_web.SSV('locEngineerHistoric','')
            ELSE
                Access:USERS.ClearKey(use:User_Code_Key)
                use:User_Code = job_ali:Engineer
                IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
                    p_web.SSV('locEngineerHistoric',CLIP(use:Forename) & ' ' & CLIP(use:Surname))
                ELSE ! IF
                END ! IF
            END !IF

            IF (job_ali:Exchange_Unit_Number = 0)
                p_web.SSV('locExchangeUnitHistoric','')
            ELSE ! IF
                Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                xch:Ref_Number = job_ali:Exchange_Unit_Number	
                IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                    p_web.SSV('locExchangeUnitHistoric',CLIP(xch:Model_Number) & '  -  ' & CLIP(xch:ESN))
                    IF (xch:MSN <> '')
                        p_web.SSV('locExchangeUnitHistoric',p_web.GSV('locExchangeUnitHistoric') & ' - ' & CLIP(xch:MSN))
                    END ! IF
                ELSE ! IF
                    p_web.SSV('locExchangeUnitHistoric','Cannot Find Unit!')
                END ! IF
            END ! IF
        END ! IF
        
