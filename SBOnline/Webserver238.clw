

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER238.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
AddToConsignmentHistory PROCEDURE  (NetWebServerWorker p_web,LONG pJobNumber,STRING pDespatchFrom,STRING pDespatchTo, STRING pCourier,STRING pConsNo,STRING pType) ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
    DO OpenFiles

    If Access:JOBSCONS.PrimeRecord() = Level:Benign
        joc:UserCode          = p_web.GSV('BookingUserCode')
        joc:RefNumber         = pJobNumber
        joc:DespatchFrom      = pDespatchFrom
        joc:DespatchTo        = pDespatchTo
        joc:Courier           = pCourier
        joc:ConsignmentNumber = pConsNo
        joc:DespatchType      = pType
        If Access:JOBSCONS.TryInsert() = Level:Benign
            !Insert Successful

        Else !If Access:JOBSCONS.TryInsert() = Level:Benign
            !Insert Failed
            Access:JOBSCONS.CancelAutoInc()
        End !If Access:JOBSCONS.TryInsert() = Level:Benign
    End !If Access:JOBSCONS.PrimeRecord() = Level:Benign

    DO CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBSCONS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBSCONS.Close
     FilesOpened = False
  END
