

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER473.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
CID_XML              PROCEDURE  (String f:Mobile,String f:Account,Byte f:Type) ! Declare Procedure
! ================================================================
! Xml Export FILE structure
fileXmlExport   FILE,DRIVER('ASCII'),CREATE,BINDABLE,THREAD
Record              RECORD
recbuff                 STRING(256)
                    END
                END
! Xml Export Class Instance
objXmlExport XmlExport
! ================================================================
seq                  LONG                                  !
RepositoryDir        CSTRING('C:\ServiceBaseMQ\MQ_Repository<0>{224}') !
save_invoice_id      USHORT,AUTO                           !
save_tradeacc_id     USHORT,AUTO                           !
save_subtracc_id     USHORT,AUTO                           !
tmp:STFMessage       STRING(30)                            !STF Message
tmp:AOWMessage       STRING(30)                            !AOW Message
tmp:RIVMessage       STRING(30)                            !RIV Message
XmlData group,type
ServiceCode string(3)
CID         group
MSISDN_OR_ICCID         string(20)
CALL_ID                 string(7)
ACCOUNT_ID              string(9)
SOURCE                  string(60)
TOPIC_CODE              string(60)
COMMENT                 string(255)
            end
        end

    map
SendXML         procedure(const *XmlData aXmlData, string aRepositoryDir)
    end

XmlValues GROUP(XmlData)
           END

  CODE
! ================================================================
! Initialise Xml Export Object
  objXmlExport.FInit(fileXmlExport)
! ================================================================
    ! Check default to see if should create messages - TrkBs: 6141 (DBH: 20-07-2005)
    If GETINI('XML','CreateCID',,Clip(Path()) & '\SB2KDEF.INI') <> 1
        Return
    End ! If GETINI('XML','CreateLine500',,Clip(Path()) & '\SB2KDEF.INI') <> True

    ! Lookup General Default for XML repository folder - TrkBs: 5110 (DBH: 29-06-2005)
    RepositoryDir               = GETINI('XML', 'RepositoryFolder',, Clip(Path()) & '\SB2KDEF.INI')

    If CreateTestRecord(RepositoryDir) = False
        Return
    End ! If CreateTestRecord(RepositoryDir) = False

    Do CreateCID
CreateCID                  Routine
    Access:TRADEACC_ALIAS.Open()
    Access:TRADEACC_ALIAS.UseFile()
    Access:TRADEACC_ALIAS.ClearKey(tra_ali:Account_Number_Key)
    tra_ali:Account_Number = f:Account
    If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
    ! Found

    Else ! If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
    ! Error
    End ! If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign

    clear(XmlValues)
    XmlValues.ServiceCode         = 'CID'
    XmlValues.CID.MSISDN_OR_ICCID = f:Mobile
    XmlValues.CID.CALL_ID         = ''
    XmlValues.CID.ACCOUNT_ID      = ''
    XmlValues.CID.SOURCE          = 'ServiceBase'
    Case f:Type
    Of 1 !Booked In
        XmlValues.CID.TOPIC_CODE      = 'Repair Booked In'
        XmlValues.CID.COMMENT         = 'Repair Booked In At ' & Clip(tra_ali:Company_Name)
    Of 2 !Despatched
        XmlValues.CID.TOPIC_CODE      = 'Repair Collected'
        XmlValues.CID.COMMENT         = 'Repair Collected From ' & Clip(tra_ali:Company_Name)
    End ! Case f:Type

    SendXml(XmlValues, RepositoryDir)

    Access:TRADEACC_ALIAS.Close()
SendXML         procedure(aXmlData, aRepositoryDir)

TheDate date
TheTime time
MsgId   string(24)

    code

    TheDate = today()
    TheTime = clock()
    MsgId = 'MSG' & format(TheDate, @d012) & format(TheTime, @t05) & format(TheTime % 100, @n02) & format(seq, @n05)
    seq += 1
    if seq > 99999 then
        seq = 1
    end
    if objXmlExport.FOpen(clip(aRepositoryDir) & '\' & clip(aXmlData.ServiceCode) & '\' & MsgId & '.xml', true) = level:benign then
        objXmlExport.OpenTag('VODACOM_MESSAGE', 'version="1.0"')
            objXmlExport.OpenTag('MESSAGE_HEADER', 'direction="REQ"')
                objXmlExport.WriteTag('SRC_SYSTEM', '')
                objXmlExport.WriteTag('SRC_APPLICATION', '')
                objXmlExport.WriteTag('SERVICING_APPLICATION', '')
                objXmlExport.WriteTag('ACTION_CODE', 'INS')
                objXmlExport.WriteTag('SERVICE_CODE', clip(aXmlData.ServiceCode))
                objXmlExport.WriteTag('MESSAGE_ID', '')
                objXmlExport.OpenTag('MESSAGE_EXPIRY')
                objXmlExport.WriteTag('VALIDITY','84600000','unit="millisecond"')
                objXmlExport.WriteTag('ACTION','','type="DISCARD"')
                objXmlExport.WriteTag('RESPONSE_REQUIRED','Y')
                objXmlExport.CloseTag('MESSAGE_EXPIRY')
                objXmlExport.WriteTag('USER_NAME', '')
                objXmlExport.WriteTag('TOKEN', '')
            objXmlExport.CloseTag('MESSAGE_HEADER')
            objXmlExport.OpenTag('MESSAGE_BODY')

            objXmlExport.OpenTag('CUSTOMER_INTERACTION_DETAIL_REQUEST')
                objXmlExport.WriteTag('MSISDN_OR_ICCID', clip(aXmlData.CID.MSISDN_OR_ICCID))
                objXmlExport.WriteTag('CALL_ID', clip(aXmlData.CID.CALL_ID))
                objXmlExport.WriteTag('ACCOUNT_ID', clip(aXmlData.CID.ACCOUNT_ID))
                objXmlExport.WriteTag('SOURCE', clip(aXmlData.CID.SOURCE))
                objXmlExport.WriteTag('TOPIC_CODE', clip(aXmlData.CID.TOPIC_CODE))
                objXmlExport.WriteTag('COMMENT', clip(aXmlData.CID.COMMENT))
             objXmlExport.CloseTag('CUSTOMER_INTERACTION_DETAIL_REQUEST')

            objXmlExport.CloseTag('MESSAGE_BODY')
        objXmlExport.CloseTag('VODACOM_MESSAGE')
        objXmlExport.FClose();
    end





