

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER314.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER303.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER315.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER319.INC'),ONCE        !Req'd for module callout resolution
                     END


FormExchangeAuditProcess PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locCurrentStatus     STRING(30)                            !
locNewLevel          LONG                                  !
locIMEINumber        STRING(30)                            !
                    MAP
GetNewLevel             PROCEDURE()
ProcessIMEI             PROCEDURE()
                    END ! MAP
FilesOpened     Long
LOAN::State  USHORT
EXCHANGE::State  USHORT
EXCHAUI::State  USHORT
EXCHAMF::State  USHORT
STOAUUSE::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormExchangeAuditProcess')
  loc:formname = 'FormExchangeAuditProcess_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormExchangeAuditProcess',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormExchangeAuditProcess','')
    p_web._DivHeader('FormExchangeAuditProcess',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormExchangeAuditProcess',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormExchangeAuditProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormExchangeAuditProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormExchangeAuditProcess',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormExchangeAuditProcess',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormExchangeAuditProcess',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormExchangeAuditProcess',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(LOAN)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(EXCHAUI)
  p_web._OpenFile(EXCHAMF)
  p_web._OpenFile(STOAUUSE)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(EXCHAUI)
  p_Web._CloseFile(EXCHAMF)
  p_Web._CloseFile(STOAUUSE)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormExchangeAuditProcess_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('emf:Status',emf:Status)
  p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  p_web.SetSessionValue('locNewLevel',locNewLevel)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('emf:Status')
    emf:Status = p_web.GetValue('emf:Status')
    p_web.SetSessionValue('emf:Status',emf:Status)
  End
  if p_web.IfExistsValue('locIMEINumber')
    locIMEINumber = p_web.GetValue('locIMEINumber')
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  End
  if p_web.IfExistsValue('locNewLevel')
    locNewLevel = p_web.GetValue('locNewLevel')
    p_web.SetSessionValue('locNewLevel',locNewLevel)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormExchangeAuditProcess_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    p_web.SSV('Hide:FinishScan',1)
    p_web.SSV('ReadOnly:IMEINumber',1)
    p_web.SSV('locIMEINumber','')
    
  ! Have remove this code because 
  ! 1) It doesn't work properly in the real code
  ! 2) If I fix it, the audit becomes unusable.
    
  !    !set all records to active
  !    FoundUser# = 0
  !    Access:StoAuUse.clearkey(STOU:KeyAuditTypeNoUser)
  !    STOU:AuditType = 'E'
  !    STOU:Audit_No  = p_web.GSV('emf:Audit_Number')
  !    STOU:User_code = p_web.GSV('BookingUserCode')
  !    set(STOU:KeyAuditTypeNoUser,STOU:KeyAuditTypeNoUser)
  !    Loop
  !  
  !        If access:StoAuUse.next() then break.
  !        If STOU:AuditType <> 'E' then break.
  !        If STOU:Audit_No  <> p_web.GSV('emf:Audit_Number') then break.
  !        if STOU:User_code <> p_web.GSV('BookingUserCode') then break.
  !        FoundUser# = 1
  !        STOU:Current = 'Y'
  !        Access:StoAuUse.update()
  !    END !loop through all StoAuUse
  !    IF (FoundUser# = 0)
  !        !first time in
  !        Access:StoAuUse.primerecord()
  !        STOU:AuditType       = 'E'
  !        STOU:Audit_No        = p_web.GSV('emf:Audit_Number')
  !        STOU:User_code       = p_web.GSV('BookingUserCode')
  !        STOU:Current         = 'Y'
  !        STOU:StockPartNumber = 'PRIMARY LOG-IN'
  !        Access:StoAuUse.update()
  !    END
  
    IF (p_web.GSV('emf:User') <> p_web.GSV('BookingUserCode'))
        p_web.SSV('Hide:ContinueAudit',1)
        p_web.SSV('Hide:SuspendAudit',1)
        p_web.SSV('Hide:FinishScan',0)
        p_web.SSV('locCurrentStatus','JOINED AUDIT')
        p_web.SSV('ReadOnly:IMEINumber',0)
    ELSE ! IF 
        p_web.SSV('locCurrentStatus',p_web.GSV('emf:Status'))
        IF (p_web.GSV('emf:Status') = 'SUSPENDED')
            p_web.SSV('Hide:ContinueAudit',0)
            p_web.SSV('Hide:SuspendAudit',1)
        ELSE ! IF
            p_web.SSV('Hide:ContinueAudit',1)
            p_web.SSV('Hide:SuspendAudit',0)
            p_web.SSV('ReadOnly:IMEINumber',0)
        END ! IF
    END ! IF
    
    
    GetNewLevel()
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locIMEINumber = p_web.RestoreValue('locIMEINumber')
 locNewLevel = p_web.RestoreValue('locNewLevel')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormExchangeAuditProcess')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormExchangeAuditProcess_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormExchangeAuditProcess_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormExchangeAuditProcess_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'FormExchangeAudits'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormExchangeAuditProcess" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormExchangeAuditProcess" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormExchangeAuditProcess" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Exchange Audit Procedure') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Exchange Audit Procedure',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormExchangeAuditProcess">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormExchangeAuditProcess" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormExchangeAuditProcess')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Scanning IMEI') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Actions') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormExchangeAuditProcess')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormExchangeAuditProcess'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locIMEINumber')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormExchangeAuditProcess')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
    do SendPacket
    Do spacer
    do SendPacket
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Scanning IMEI') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormExchangeAuditProcess_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Scanning IMEI')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Scanning IMEI')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Scanning IMEI')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Scanning IMEI')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::display2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::display2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::emf:Status
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::emf:Status
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::emf:Status
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locIMEINumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locNewLevel
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ''
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locNewLevel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locNewLevel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::___Line
      do Comment::___Line
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwAudits
      do Comment::brwAudits
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Actions') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormExchangeAuditProcess_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Actions')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnFinishScan
      do Comment::btnFinishScan
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnCompleteAudit
      do Comment::btnCompleteAudit
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnContinueAudit
      do Comment::btnContinueAudit
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td>'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnSuspendAudit
      do Comment::btnSuspendAudit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::display2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('display2',p_web.GetValue('NewValue'))
    do Value::display2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::display2  Routine
  p_web._DivHeader('FormExchangeAuditProcess_' & p_web._nocolon('display2') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold')&'">' & p_web.Translate('Audit Number: ' & p_web.GSV('emf:Audit_Number'),0) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::display2  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangeAuditProcess_' & p_web._nocolon('display2') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::emf:Status  Routine
  p_web._DivHeader('FormExchangeAuditProcess_' & p_web._nocolon('emf:Status') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::emf:Status  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('emf:Status',p_web.GetValue('NewValue'))
    emf:Status = p_web.GetValue('NewValue') !FieldType= STRING Field = emf:Status
    do Value::emf:Status
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('emf:Status',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    emf:Status = p_web.GetValue('Value')
  End

Value::emf:Status  Routine
  p_web._DivHeader('FormExchangeAuditProcess_' & p_web._nocolon('emf:Status') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- emf:Status
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('red bold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('emf:Status'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangeAuditProcess_' & p_web._nocolon('emf:Status') & '_value')

Comment::emf:Status  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangeAuditProcess_' & p_web._nocolon('emf:Status') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locIMEINumber  Routine
  p_web._DivHeader('FormExchangeAuditProcess_' & p_web._nocolon('locIMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('IMEI Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangeAuditProcess_' & p_web._nocolon('locIMEINumber') & '_prompt')

Validate::locIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('NewValue'))
    locIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('Value'))
    locIMEINumber = p_web.GetValue('Value')
  End
    ProcessIMEI()  
  do Value::locIMEINumber
  do SendAlert
  do Value::locNewLevel  !1
  do Value::brwAudits  !1

Value::locIMEINumber  Routine
  p_web._DivHeader('FormExchangeAuditProcess_' & p_web._nocolon('locIMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('ReadOnly:IMEINumber') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('ReadOnly:IMEINumber') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('locIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locIMEINumber'',''formexchangeauditprocess_locimeinumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locIMEINumber')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locIMEINumber',p_web.GetSessionValueFormat('locIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangeAuditProcess_' & p_web._nocolon('locIMEINumber') & '_value')

Comment::locIMEINumber  Routine
      loc:comment = ''
  p_web._DivHeader('FormExchangeAuditProcess_' & p_web._nocolon('locIMEINumber') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locNewLevel  Routine
  p_web._DivHeader('FormExchangeAuditProcess_' & p_web._nocolon('locNewLevel') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Counted:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locNewLevel  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locNewLevel',p_web.GetValue('NewValue'))
    locNewLevel = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locNewLevel
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locNewLevel',p_web.GetValue('Value'))
    locNewLevel = p_web.GetValue('Value')
  End

Value::locNewLevel  Routine
  p_web._DivHeader('FormExchangeAuditProcess_' & p_web._nocolon('locNewLevel') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locNewLevel
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green gold LargeText')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locNewLevel'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangeAuditProcess_' & p_web._nocolon('locNewLevel') & '_value')

Comment::locNewLevel  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangeAuditProcess_' & p_web._nocolon('locNewLevel') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::___Line  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('___Line',p_web.GetValue('NewValue'))
    do Value::___Line
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::___Line  Routine
  p_web._DivHeader('FormExchangeAuditProcess_' & p_web._nocolon('___Line') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::___Line  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangeAuditProcess_' & p_web._nocolon('___Line') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::brwAudits  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwAudits',p_web.GetValue('NewValue'))
    do Value::brwAudits
  Else
    p_web.StoreValue('eau:Internal_No')
  End

Value::brwAudits  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseAuditedExchangeUnits --
  p_web.SetValue('BrowseAuditedExchangeUnits:NoForm',1)
  p_web.SetValue('BrowseAuditedExchangeUnits:FormName',loc:formname)
  p_web.SetValue('BrowseAuditedExchangeUnits:parentIs','Form')
  p_web.SetValue('_parentProc','FormExchangeAuditProcess')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormExchangeAuditProcess_BrowseAuditedExchangeUnits_embedded_div')&'"><!-- Net:BrowseAuditedExchangeUnits --></div><13,10>'
    p_web._DivHeader('FormExchangeAuditProcess_' & lower('BrowseAuditedExchangeUnits') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormExchangeAuditProcess_' & lower('BrowseAuditedExchangeUnits') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseAuditedExchangeUnits --><13,10>'
  end
  do SendPacket

Comment::brwAudits  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangeAuditProcess_' & p_web._nocolon('brwAudits') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnFinishScan  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnFinishScan',p_web.GetValue('NewValue'))
    do Value::btnFinishScan
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnFinishScan  Routine
  p_web._DivHeader('FormExchangeAuditProcess_' & p_web._nocolon('btnFinishScan') & '_value',Choose(p_web.GSV('Hide:FinishScan') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:FinishScan') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnFinishScan','Finish Scan','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormExchangeAudits')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::btnFinishScan  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangeAuditProcess_' & p_web._nocolon('btnFinishScan') & '_comment',Choose(p_web.GSV('Hide:FinishScan') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:FinishScan') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnCompleteAudit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnCompleteAudit',p_web.GetValue('NewValue'))
    do Value::btnCompleteAudit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnCompleteAudit  Routine
  p_web._DivHeader('FormExchangeAuditProcess_' & p_web._nocolon('btnCompleteAudit') & '_value',Choose(p_web.GSV('Hide:CompleteAudit') = 1 OR p_web.GSV('emf:Complete_Flag') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:CompleteAudit') = 1 OR p_web.GSV('emf:Complete_Flag') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&clip('exchangeAuditCompleteConfirm()')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnCompleteAudit','Complete Audit','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::btnCompleteAudit  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangeAuditProcess_' & p_web._nocolon('btnCompleteAudit') & '_comment',Choose(p_web.GSV('Hide:CompleteAudit') = 1 OR p_web.GSV('emf:Complete_Flag') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:CompleteAudit') = 1 OR p_web.GSV('emf:Complete_Flag') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnContinueAudit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnContinueAudit',p_web.GetValue('NewValue'))
    do Value::btnContinueAudit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    p_web.SSV('locCurrentStatus','AUDIT IN PROGRESS')
    p_web.SSV('Hide:ContinueAudit',1)
    p_web.SSV('Hide:SuspendAudit',0)
    p_web.SSV('ReadOnly:IMEINumber',0)
    Access:EXCHAMF.ClearKey(emf:Audit_Number_Key)
    emf:Audit_Number = p_web.GSV('emf:Audit_Number')
    IF (Access:EXCHAMF.TryFetch(emf:Audit_Number_Key) = Level:Benign)
        p_web.SessionQueueToFile(EXCHAMF)
        emf:Status = 'AUDIT IN PROGRESS'
        p_web.SSV('emf:Status',emf:Status)
        Access:EXCHAMF.TryUpdate()
    END ! IF
    
    
        
  do SendAlert
  do Value::btnContinueAudit  !1
  do Value::btnSuspendAudit  !1
  do Value::emf:Status  !1
  do Prompt::locIMEINumber
  do Value::locIMEINumber  !1

Value::btnContinueAudit  Routine
  p_web._DivHeader('FormExchangeAuditProcess_' & p_web._nocolon('btnContinueAudit') & '_value',Choose(p_web.GSV('Hide:ContinueAudit') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:ContinueAudit') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnContinueAudit'',''formexchangeauditprocess_btncontinueaudit_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnContinueAudit','Continue Audit','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangeAuditProcess_' & p_web._nocolon('btnContinueAudit') & '_value')

Comment::btnContinueAudit  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangeAuditProcess_' & p_web._nocolon('btnContinueAudit') & '_comment',Choose(p_web.GSV('Hide:ContinueAudit') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:ContinueAudit') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnSuspendAudit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnSuspendAudit',p_web.GetValue('NewValue'))
    do Value::btnSuspendAudit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnSuspendAudit  Routine
  p_web._DivHeader('FormExchangeAuditProcess_' & p_web._nocolon('btnSuspendAudit') & '_value',Choose(p_web.GSV('Hide:SuspendAudit') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:SuspendAudit') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnSuspendAudit','Suspend Audit','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('AuditProcess?' &'ProcessType=ExchangeSuspendAudit&okURL=FormExchangeAudits&errorURL=FormExchangeAuditProcess')) & ''','''&clip('_self')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormExchangeAuditProcess_' & p_web._nocolon('btnSuspendAudit') & '_value')

Comment::btnSuspendAudit  Routine
    loc:comment = ''
  p_web._DivHeader('FormExchangeAuditProcess_' & p_web._nocolon('btnSuspendAudit') & '_comment',Choose(p_web.GSV('Hide:SuspendAudit') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:SuspendAudit') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormExchangeAuditProcess_locIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIMEINumber
      else
        do Value::locIMEINumber
      end
  of lower('FormExchangeAuditProcess_btnContinueAudit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnContinueAudit
      else
        do Value::btnContinueAudit
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormExchangeAuditProcess_form:ready_',1)
  p_web.SetSessionValue('FormExchangeAuditProcess_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormExchangeAuditProcess',0)

PreCopy  Routine
  p_web.SetValue('FormExchangeAuditProcess_form:ready_',1)
  p_web.SetSessionValue('FormExchangeAuditProcess_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormExchangeAuditProcess',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormExchangeAuditProcess_form:ready_',1)
  p_web.SetSessionValue('FormExchangeAuditProcess_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormExchangeAuditProcess:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormExchangeAuditProcess_form:ready_',1)
  p_web.SetSessionValue('FormExchangeAuditProcess_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormExchangeAuditProcess:Primed',0)
  p_web.setsessionvalue('showtab_FormExchangeAuditProcess',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormExchangeAuditProcess_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormExchangeAuditProcess_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormExchangeAuditProcess:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('emf:Status')
  p_web.StoreValue('locIMEINumber')
  p_web.StoreValue('locNewLevel')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
spacer  Routine
  packet = clip(packet) & |
    '<<br/><13,10>'&|
    '<<br/><13,10>'&|
    '<<br/><13,10>'&|
    '<<br/><13,10>'&|
    ''
GetNewLevel	PROCEDURE()
newLevel	LONG()
	CODE
		Access:EXCHAUI.ClearKey(eau:Audit_Number_Key)
		eau:Audit_Number = p_web.GSV('emf:Audit_Number')
		SET(eau:Audit_Number_Key,eau:Audit_Number_Key)
		LOOP UNTIL Access:EXCHAUI.Next() <> Level:Benign
			IF (eau:Audit_Number <> p_web.GSV('emf:Audit_Number'))
				BREAK
			END ! IF
			newLevel += eau:Confirmed
		END ! LOOP
		
		p_web.SSV('locNewLevel',newLevel)
ProcessIMEI	PROCEDURE()
	CODE
		Access:EXCHAUI.ClearKey(eau:Locate_IMEI_Key)
		eau:Audit_Number = p_web.GSV('emf:Audit_Number')
		eau:Site_Location = p_web.GSV('BookingSiteLocation')
		eau:IMEI_Number = p_web.GSV('locIMEINumber')
		IF (Access:EXCHAUI.TryFetch(eau:Locate_IMEI_Key) = Level:Benign)
			! Check if the IMEI exists in another location
			Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
			xch:ESN = p_web.GSV('locIMEINumber')
			IF (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)
				IF (xch:Location <> p_web.GSV('BookingSiteLocation'))
					loc:alert = 'This unit exists in another location. Please ensure it is in the correct location.'
                    loc:invalid = 'locIMEINumber'
                    RETURN
                END ! IF
			ELSE ! IF
				Access:LOAN.ClearKey(loa:ESN_Only_Key)
				loa:ESN = p_web.GSV('locIMEINumber')
				IF (Access:LOAN.TryFetch(loa:ESN_Only_Key) = Level:Benign)
					loc:alert = 'This unit exists in the Loan Database. It must be counted as part of a Loan Audit,'
					loc:invalid = 'locIMEINumber'
					RETURN
                ELSE ! IF
                    loc:alert = 'This I.M.E.I. Number is not recognised in the exchange or loan databases.'
                    loc:invalid = 'locIMEINumber'
                    RETURN
				END ! IF
			END ! IF
			IF (eau:Confirmed = TRUE)
				loc:alert = 'The selected I.M.E.I. Number has already been scanned.'
				loc:invalid = 'locIMEINumber'
				RETURN
			ELSE ! IF
				eau:Confirmed = TRUE
				Access:EXCHAUI.TryUpdate()
			END ! IF
		ELSE ! IF
			loc:alert = 'This I.M.E.I. Number is not recognised in the exchange or loan databases.'
			loc:invalid = 'locIMEINumber'
			RETURN
		END ! IF
		
        GetNewLevel()
        
        p_web.SSV('locIMEINumber','')
