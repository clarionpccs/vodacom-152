

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER498.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
CopyDOPFromBouncer   PROCEDURE  (String f:Manufacturer, Date f:PreviousDOP, *Date f:DOP,  String f:IMEINumber, String f:Bouncer, *Byte f:Found) ! Declare Procedure
    include('CopyDOPFromBouncer.inc','Local Data')

  CODE
    include('CopyDOPFromBouncer.inc','Processed Code')
