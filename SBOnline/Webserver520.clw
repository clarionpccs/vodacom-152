

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER520.INC'),ONCE        !Local module procedure declarations
                     END


RunReport            PROCEDURE  (NetWebServerWorker p_web)
                    MAP
AvailableExchangeExport PROCEDURE()
ChargeableIncomeReport  PROCEDURE()
CreditReport            PROCEDURE()
ExchangeLoanDeviceSoldReport    PROCEDURE()
ExchangePhoneExport     PROCEDURE()
ExchangeStockValuationExport    PROCEDURE()
ExchangeLoanUnitsReturnedToMainStore    PROCEDURE()
HandlingFeeReport       PROCEDURE()
InsuranceReport         PROCEDURE
LoanPhoneExport         PROCEDURE()
SMSResponseReport       PROCEDURE()
StatusReport            PROCEDURE()
StockValueReport        PROCEDURE()
WarrantyIncomeReport    PROCEDURE()
DeleteReport            PROCEDURE()
DeleteAllReports        PROCEDURE()
                    END

RepQ                        CLASS
CriteriaFilename                CSTRING(255)
FullCriteriaFilename            CSTRING(255)
ReportEXE                       CSTRING(255)
CommandLine                     CSTRING(255)
ReportName                      CSTRING(255)
Init                            PROCEDURE(STRING pReportName, STRING pReportEXE),LONG
Error                           PROCEDURE(STRING pErrorText)
Message                         PROCEDURE(STRING pTitle, STRING pText)
SaveCriteriaAndRun              PROCEDURE(),LONG
Finished                        PROCEDURE()
                            END !  CLASS
loc:x          Long
packet              string(NET:MaxBinData)
packetlen           long
CRLF           String('<13,10>')
NBSP           String('&#160;')

  CODE
  GlobalErrors.SetProcedureName('RunReport')
  p_web.SetValue('_parentPage','RunReport')
  p_web.publicpage = 1
  if p_web.sessionId = 0 then p_web.NewSession().
  do Header
        p_web.SSV('myalertHeading','')	
        p_web.SSV('myalertText','')
        p_web.SSV('myalertButton2URL','')
        p_web.SSV('myalertButton3URL','')
        p_web.SSV('myalertButton2Text','')
        p_web.SSV('myalertButton3Text','')
        p_web.SSV('myalertButton1Text','OK')
        p_web.SSV('myalertButton1URL','window.open(''FormReports'',''_self'')')	
        ! Workaround to fix the refresh issue when returning to browse
        p_web.SSV('BrowseReportQ_FirstValue','')
        Do SendPacket
        Do Procedures
        DO SendPacket
        DO MyAlert
        Do SendPacket 
  packet = clip(packet) & p_web._jsBodyOnLoad('PageBody',,'PageBodyDiv')
  do Footer
  packet = clip(packet) & p_web.Popup()
  do SendPacket
  GlobalErrors.SetProcedureName()
  Return

SendPacket  Routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,Net:NoHeader)
    packet = ''
  end
Header Routine
  packet = p_web.w3Header()
  packet = clip(packet) & '<head>'&|
      '<title>'&clip(p_web.site.PageTitle)&'</title>'&|
      '<meta http-equiv="Content-Type" content="text/html; charset='&clip(p_web.site.HtmlCharset)&'" /><13,10>'
  packet =  clip(packet) & p_web.IncludeStyles()
  packet =  clip(packet) & p_web.IncludeScripts()
    Do Heading
  packet = clip(packet) & '</head><13,10>'
  p_web.ParseHTML(packet,1,0,Net:SendHeader+Net:DontCache)
  packet = ''
Footer Routine
  packet = clip(packet) & '<!-- Net:SelectField --><13,10>' &|
                          '<script>bodyOnLoad();</script><13,10>' &|
                         '</div></body><13,10></html><13,10>'
Heading  Routine
  packet = clip(packet) & |
    '<<div class="PageBodyDiv" style="<13,10>'&|
    '    background-color: red;<13,10>'&|
    '    margin-top: 15px;<13,10>'&|
    '    height: 45px;<13,10>'&|
    '    padding-top: 3px;<13,10>'&|
    '    margin-left: 47px;<13,10>'&|
    '    margin-right: 47px;<13,10>'&|
    '    padding-left: 3px;<13,10>'&|
    '"><13,10>'&|
    '  <<img src="/images/topbanner.gif" width="800" height="40"><13,10>'&|
    '<</div><13,10>'&|
    ''
Procedures  Routine
  AddToLog('RunReport Start',p_web.RequestData.DataString,p_web.GetValue('RT'),p_web.SessionID,,p_web.GSV('BookingUserCode'))
  packet = clip(packet) & |
    '<13,10>'&|
    ''
    CASE p_web.GetValue('RT')
    OF 'AvailableExchangeExport'
        AvailableExchangeExport()
    OF 'ChargeableIncomeReport'
        ChargeableIncomeReport()
    OF 'CreditReport'
        CreditReport()
    OF 'ExchangeLoanDeviceSoldReport'
        ExchangeLoanDeviceSoldReport()
    OF 'ExchangePhoneExport'
        ExchangePhoneExport()
    OF 'ExchangeStockValuationExport'
        ExchangeStockValuationExport()
    OF 'ExchangeLoanUnitsReturnedToMainStore'
        ExchangeLoanUnitsReturnedToMainStore()
    OF 'HandlingFeeReport'
        HandlingFeeReport()
    OF 'InsuranceReport'
        InsuranceReport()
    OF 'LoanPhoneExport'
        LoanPhoneExport()
    OF 'SMSResponseReport'
        SMSResponseReport()
    OF 'StatusReport'
        StatusReport()
    OF 'StockValueReport'
        StockValueReport()
    OF 'WarrantyIncomeReport'
        WarrantyIncomeReport()
    OF 'DeleteReport'
        DeleteReport()
    OF 'DeleteAllReports'
        DeleteAllReports()
    ELSE
        RepQ.Message('Program Error','<br/>No Report called')
    END ! CASE  
   AddToLog('RunReport End',p_web.RequestData.DataString,p_web.GetValue('RT'),p_web.SessionID,,p_web.GSV('BookingUserCode'))
MyAlert  Routine
    packet = CLIP(packet) & |
        '<div id="myalert-background">' & CRLF & |
        '<p class="SubHeading">' & p_web.GSV('myalertHeading') & '</p>' & CRLF & |
        '<p id="myalert-text">' & p_web.GSV('myalertText') & '</p>' & CRLF & |
        '<br/>' & CRLF & |
        '<div id="myalert-buttons">' & CRLF & |
        '<a onclick="' & p_web.GSV('myalertButton1URL') & '" class="MessageBoxButton">' & p_web.GSV('myalertButton1Text') & '</a>' & CRLF
    IF (p_web.GSV('myalertButton2Text') <> '')	
        packet = CLIP(packet) & |
            '<a onclick="' & p_web.GSV('myalertButton2URL') & '" class="MessageBoxButton">' & p_web.GSV('myalertButton2Text') & '</a>' & CRLF
    END ! IF
    IF (p_web.GSV('myalertButton3Text') <> '')	
        packet = CLIP(packet) & |
            '<a onclick="' & p_web.GSV('myalertButton3URL') & '" class="MessageBoxButton">' & p_web.GSV('myalertButton3Text') & '</a>' & CRLF
    END ! IF
    packet = CLIP(packet) & |
        '</div></div>' & CRLF  
  packet = clip(packet) & |
    '<13,10>'&|
    ''
! Available Exchange Export (Summary/Detailed)
AvailableExchangeExport     PROCEDURE()
countTagged	LONG
i                           LONG()
    CODE
        IF (RepQ.Init('Available Exchange Units Report ' & CHOOSE(p_web.GSV('locExportType') = 1,'(Summary)','(Detailed)'),'VODR0104.EXE'))
            RETURN
        END ! IF
        
        IF (~XML:CreateXMLFile(RepQ.FullCriteriaFilename))
            XML:CreateParent('Criteria'); XML:AddParent()
            XML:AddElement('ReportName',RepQ.ReportName)
            XML:AddElement('ReportType',p_web.GSV('locExportType'))
            XML:AddElement('AllStockTypes',p_web.GSV('locAllStockTypes'))
            
            IF (p_web.GSV('locAllStockTypes') <> 1)
                Relate:TagFile.Open()
	            XML:CreateParent('StockTypes'); XML:AddParent()
                Access:TagFile.ClearKey(tag:KeyTagged)
                tag:SessionID = p_web.SessionID
                SET(tag:KeyTagged,tag:KeyTagged)
                LOOP UNTIL Access:TagFile.Next() <> Level:Benign
                    IF (tag:SessionID <> p_web.SessionID)
                        BREAK
                    END ! IF
                    IF (tag:tagged = 0)
                        CYCLE
                    END ! IF
                    XML:AddElement('StockType',tag:taggedValue)
                    countTagged += 1
                END ! LOOP
                XML:CloseParent('StockTypes')
                Relate:TagFile.Close()
            END ! IF            
            
            XML:CloseParent('Criteria')
            
            XML:CloseXMLFile()
            XML:Free()

            IF (RepQ.SaveCriteriaAndRun())
            END ! IF
        END !

        RETURN
! ChargeableIncomeReport
ChargeableIncomeReport      PROCEDURE()
    CODE
        IF (RepQ.Init('Chargeable Income Report (' & CHOOSE(p_web.GSV('locReportStyle') = 'I','Invoiced','Non-Invoiced') &')','VODR0063.EXE'))
            RETURN
        END ! IF
        IF (~XML:CreateXMLFile(RepQ.FullCriteriaFilename))
            XML:CreateParent('Criteria'); XML:AddParent()
            XML:AddElement('ReportName', RepQ.ReportName)
            XML:CreateParent('Defaults'); XML:AddParent()
            XML:AddElement('ReportStyle', p_web.GSV('locReportStyle')) !I or C
            XML:AddElement('ReportOrder', p_web.GSV('locReportOrder')) !0 or 1
            XML:AddElement('ReportType', p_web.GSV('ReportType')) !E or P
            XML:AddElement('GenericAccounts', p_web.GSV('locGenericAccounts')) !0 or 1
            XML:AddElement('AllAccounts', p_web.GSV('locAllACcounts')) !0 or 1
            XML:AddElement('AllChargeTypes',p_web.GSV('locAllChargeTypes')) ! 0 or 1
            XML:AddElement('StartDate', p_web.GSV('locStartDate'))
            XML:AddElement('EndDate', p_web.GSV('locEndDate'))
            XML:AddElement('ZeroSuppression', p_web.GSV('locZeroSupression'))
            XML:CloseParent('Defaults')
            IF (p_web.GSV('locAllAccounts') <> 1)
                XML:CreatePArent('Accounts'); XML:AddParent()
                Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
                tagf:SessionID = p_web.SessionID
                tagf:TagType = CHOOSE(p_web.GSV('locGenericAccounts') = 1,kGenericAccount,kSubAccount)
                SET(tagf:KeyTagged,tagf:KeyTagged)
                LOOP UNTIL Access:SBO_GenericTagFile.Next() <> Level:Benign
                    IF (tagf:SessionID <> p_web.SessionID OR |
                    tagf:TagType <> CHOOSE(p_web.GSV('locGenericAccounts') = 1,kGenericAccount,kSubAccount))
                        BREAK
                    END ! IF
                    IF (tagf:Tagged = 1)
                        XML:AddElement('AccountNumber', tagf:TaggedValue)
                    END! I F
                END ! LOOP
                XML:CloseParent('Accounts')
            END ! IF
            IF (p_web.GSV('locAllChargeTypes') <> 1)
                XML:CreateParent('ChargeTypes'); XML:AddParent()
                Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
                tagf:SessionID = p_web.SessionID
                tagf:TagType = 'C'
        
                SET(tagf:KeyTagged,tagf:KeyTagged)
                LOOP UNTIL Access:SBO_GenericTagFile.Next() <> Level:Benign
                    IF (tagf:SessionID <> p_web.SessionID OR |
                        tagf:TagType <> 'C')
                        BREAK
                    END ! IF
                    IF (tagf:Tagged = 1)
                        XML:AddElement('ChargeType',tagf:TaggedValue)
                    END !I F
                END ! LOOP
                XML:CloseParent('ChargeTypes')
            END ! IF
            
            XML:CloseParent('Criteria')
            
        
            XML:CloseXMLFile()
            XML:Free()

            IF (RepQ.SaveCriteriaAndRun())
            END ! IF
        END !

        RETURN
! CreditReport
CreditReport        PROCEDURE()
    CODE
        IF (RepQ.Init('Credit Report','VODR0094.EXE'))
            RETURN
        END ! IF
        IF (~XML:CreateXMLFile(RepQ.FullCriteriaFilename))
            XML:CreateParent('Criteria'); XML:AddParent()
            XML:AddElement('ReportName', RepQ.ReportName)
            XML:CreateParent('Defaults'); XML:AddParent()
            XML:AddElement('StartDate',p_web.GSV('locStartDate'))
            XML:AddElement('EndDate',p_web.GSV('locEndDate'))
            XML:CloseParent('Defaults')
            XML:CloseParent('Criteria')
            XML:CloseXMLFile()
            XML:Free()

            IF (RepQ.SaveCriteriaAndRun())
            END ! IF
        END ! IF
        RETURN
! ExchangeLoanDeviceSoldReport
ExchangeLoanDeviceSoldReport        PROCEDURE()
ReportDescription                       CSTRING(20)
ReportType                              CSTRING(20)
                                      LONG()
    CODE
        
        ReportDescription = CHOOSE(p_web.GSV('Type') = 'E','Exchange','Loan')
        ReportType = CHOOSE(p_web.GSV('Kind') = 'D','Detailed','Summary')
		
        IF (RepQ.Init(ReportDescription & ' Device Sold Report (' & ReportType & ')',CHOOSE(p_web.GSV('Type') = 'E','VODR0106.EXE','VODR0109.EXE')))
            RETURN
        END! IF
        
        IF (~XML:CreateXMLFile(RepQ.FullCriteriaFilename))
            XML:CreateParent('Criteria');XML:AddParent()
            XML:AddElement('ReportName',RepQ.ReportName)
            XML:AddElement('ReportKind',p_web.GSV('Kind'))  
            XML:AddElement('StartDate',p_web.GSV('locStartDate'))
            XML:AddElement('EndDate',p_web.GSV('locEndDate'))
            XML:CloseParent('Criteria')
            
            XML:CloseXMLFile()
            XML:Free()
            
            IF (RepQ.SaveCriteriaAndRun())
            END ! IF
            
        END ! IF
        
        RETURN
! ExchangeLoanUnitsReturnedToMainStore
ExchangeLoanUnitsReturnedToMainStore        PROCEDURE()
ReportDescription                               CSTRING(60)
    CODE
        ReportDescription = CHOOSE(p_web.GSV('Kind') = 'E','Exchange','Loan') & | 
            ' Units Returned To Main Store Report (' & CHOOSE(p_web.GSV('ReportType') = 1,'Received','In Transit') & |
                ')'

        IF (RepQ.Init(ReportDescription,CHOOSE(p_web.GSV('Kind') = 'E','VODR0108.EXE','VODR0110.EXE')))
            RETURN
        END ! IF
        
        IF (~XML:CreateXMLFile(RepQ.FullCriteriaFilename))
            XML:CreateParent('Criteria'); XML:AddParent()
            XML:CreateParent('Defaults'); XML:AddParent()
            XML:AddElement('ReportType',p_web.GSV('ReportType'))
            XML:AddElement('StartDate',p_web.GSV('locStartDate'))
            XML:AddElement('EndDate',p_web.GSV('locEndDate'))
            XML:CloseParent('Defaults')
            XML:CloseParent('Criteria')
        
            XML:CloseXMLFile()
        
            XML:Free()
        
            IF (RepQ.SaveCriteriaAndRun())
            END!  IF
        END ! IF
        
        RETURN

        
! ExchangePhoneExport
ExchangePhoneExport PROCEDURE()!
countTagged LONG()
    CODE
        
        IF (RepQ.Init('Exchange Phone Report','VODR0107.EXE'))
            RETURN
        END ! IF
        
        IF (~XML:CreateXMLFile(RepQ.FullCriteriaFilename))
            XML:CreateParent('Criteria'); XML:AddParent()
            XML:CreateParent('Defaults'); XML:AddParent()
            XML:AddElement('AllStockTypes',p_web.GSV('locAllStockTypes'))
            XML:AddElement('AllStatuses',p_web.GSV('locAllStatus'))
            XML:AddElement('StartDate',p_web.GSV('locStartDate'))
            XML:AddElement('EndDate',p_web.GSV('locEndDate'))
            XML:CloseParent('Defaults')
            XML:CreateParent('Statuses'); XML:AddParent()
            IF (p_web.GSV('locAVL') = 1)
                XML:AddElement('Status','AVL')
            END ! IF
            IF (p_web.GSV('locEXC') = 1)
                XML:AddElement('Status','EXC')
            END ! IF            
            IF (p_web.GSV('locINC') = 1)
                XML:AddElement('Status','INC')
            END ! IF  
            IF (p_web.GSV('locFAU') = 1)
                XML:AddElement('Status','FAU')
            END ! IF  
            IF (p_web.GSV('locNOA') = 1)
                XML:AddElement('Status','NOA')
            END ! IF  
            IF (p_web.GSV('locREP') = 1)
                XML:AddElement('Status','REP')
            END ! IF  
            IF (p_web.GSV('locSUS') = 1)
                XML:AddElement('Status','SUS')
            END ! IF  
            IF (p_web.GSV('locDES') = 1)
                XML:AddElement('Status','DES')
            END ! IF  
            IF (p_web.GSV('locQA1') = 1)
                XML:AddElement('Status','QA1')
            END ! IF  
            IF (p_web.GSV('locQA2') = 1)
                XML:AddElement('Status','QA2')
            END ! IF  
            IF (p_web.GSV('locQAF') = 1)
                XML:AddElement('Status','QAF')
            END ! IF  
            IF (p_web.GSV('locRTS') = 1)
                XML:AddElement('Status','RTS')
            END ! IF  
            IF (p_web.GSV('locINR') = 1)
                XML:AddElement('Status','INR')
            END ! IF  
            IF (p_web.GSV('locESC') = 1)
                XML:AddElement('Status','ESC')
            END ! IF  
            IF (p_web.GSV('locIT4') = 1)
                XML:AddElement('Status','IT4')
            END ! IF  
            IF (p_web.GSV('locITM') = 1)
                XML:AddElement('Status','ITM')
            END ! IF  
            IF (p_web.GSV('locITR') = 1)
                XML:AddElement('Status','ITR')
            END ! IF  
            IF (p_web.GSV('locITP') = 1)
                XML:AddElement('Status','ITP')
            END ! IF  
            XML:CloseParent('Statuses')

            
            XML:CreateParent('StockTypes'); XML:AddParent()
            IF (p_web.GSV('locAllStockTypes') <> 1)
                Relate:TagFile.Open()
                Access:TagFile.ClearKey(tag:KeyTagged)
                tag:SessionID = p_web.SessionID
                SET(tag:KeyTagged,tag:KeyTagged)
                LOOP UNTIL Access:TagFile.Next() <> Level:Benign
                    IF (tag:SessionID <> p_web.SessionID)
                        BREAK
                    END ! IF
                    IF (tag:Tagged = 0)
                        CYCLE
                    END ! IF
                    XML:AddElement('StockType',tag:taggedValue)
                END ! LOOP 
                Relate:TagFile.Close()
            END ! IF
            XML:CloseParent('StockTypes')
            XML:CloseParent('Criteria')
            XML:CloseXMLFile()
            
            XML:Free()
            
            IF (RepQ.SaveCriteriaAndRun())
            END!  IF
            
        END ! IF
       
        
        RETURN
! ExchangeStockValuationExport
ExchangeStockValuationExport        PROCEDURE()
countTagged                             LONG
    CODE
        IF (RepQ.Init('Exchange Stock Valuation Report','VODR0105.EXE'))
            RETURN
        END ! IF
        
        IF (~XML:CreateXMLFile(RepQ.FullCriteriaFilename))
            XML:CreateParent('Criteria'); XML:AddParent()
            XML:AddElement('ReportName',RepQ.ReportName)
            XML:CreateParent('Defaults'); XML:AddParent()
            XML:AddElement('AllStockTypes',p_web.GSV('locAllStockTypes'))
            XML:CloseParent('Defaults')
            
            IF (p_web.GSV('locAllStockTypes') <> 1)
                Relate:TagFile.Open()
                XML:CreateParent('StockTypes'); XML:AddParent()
                Access:TagFile.ClearKey(tag:KeyTagged)
                tag:SessionID = p_web.SessionID
                SET(tag:KeyTagged,tag:KeyTagged)
                LOOP UNTIL Access:TagFile.Next() <> Level:Benign
                    IF (tag:SessionID <> p_web.SessionID)
                        BREAK
                    END ! IF
                    IF (tag:tagged = 0)
                        CYCLE
                    END ! IF
                    XML:AddElement('StockType',tag:taggedValue)
                    countTagged += 1
                END ! LOOP
                XML:CloseParent('StockTypes')
                Relate:TagFile.Close()
            END ! IF            
            
            XML:CloseParent('Criteria')
            
            XML:CloseXMLFile()
            XML:Free()

            IF (RepQ.SaveCriteriaAndRun())
            END ! IF
        END !

        RETURN
! HandlingFeeReport
HandlingFeeReport   PROCEDURE()
    CODE
        IF (RepQ.Init('Handling And Exchange Fee Report' ,'VODR0062.EXE'))
            RETURN
        END ! IF
        IF (~XML:CreateXMLFile(RepQ.FullCriteriaFilename))
            XML:CreateParent('Criteria'); XML:AddParent()
            XML:AddElement('ReportName', RepQ.ReportName)
            XML:CreateParent('Defaults'); XML:AddParent()
            XML:AddElement('AllAccounts',p_web.GSV('locAllAccounts'))
            XML:AddElement('GenericAccounts',p_web.GSV('locGenericAccounts'))
            XML:AddElement('AllChargeTypes',p_web.GSV('locAllChargeTypes'))
            XML:AddElement('AllManufacturers',p_web.GSV('locAllManufacturers'))
            XML:AddElement('StartDate',p_web.GSV('locStartDate'))
            XML:AddElement('EndDate',p_web.GSV('locEndDate'))
            XML:AddElement('ReportOrder',p_web.GSV('locReportOrder')) !0 (job), 1 (action), 2 (man)
            XML:CloseParent('Defaults')
            XML:GotoTop()
            IF (p_web.GSV('locAllAccounts') <> 1)
                XML:CreatePArent('Accounts'); XML:AddParent()
                Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
                tagf:SessionID = p_web.SessionID
                tagf:TagType = CHOOSE(p_web.GSV('locGenericAccounts') = 1,kGenericAccount,kSubAccount)
                SET(tagf:KeyTagged,tagf:KeyTagged)
                LOOP UNTIL Access:SBO_GenericTagFile.Next() <> Level:Benign
                    IF (tagf:SessionID <> p_web.SessionID OR |
                        tagf:TagType <> CHOOSE(p_web.GSV('locGenericAccounts') = 1,kGenericAccount,kSubAccount))
                        BREAK
                    END ! IF
                    IF (tagf:Tagged = 1)
                        XML:AddElement('AccountNumber', tagf:TaggedValue)
                    END! I F
                END ! LOOP
                XML:CloseParent('Accounts')
            END ! IF
            XML:GotoTop()
            IF (p_web.GSV('locAllChargeTypes') <> 1)
                XML:CreateParent('ChargeTypes'); XML:AddParent()
                Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
                tagf:SessionID = p_web.SessionID
                tagf:TagType = 'C'
        
                SET(tagf:KeyTagged,tagf:KeyTagged)
                LOOP UNTIL Access:SBO_GenericTagFile.Next() <> Level:Benign
                    IF (tagf:SessionID <> p_web.SessionID OR |
                        tagf:TagType <> 'C')
                        BREAK
                    END ! IF
                    IF (tagf:Tagged = 1)
                        XML:AddElement('ChargeType',tagf:TaggedValue)
                    END !I F
                END ! LOOP
                XML:CloseParent('ChargeTypes')
            END ! IF
            XML:GotoTop()
            IF (p_web.GSV('locAllManufacturers') <> 1)
                XML:CreateParent('Manufacturers'); XML:AddParent()
                Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
                tagf:SessionID = p_web.SessionID
                tagf:TagType = 'M'
        
                SET(tagf:KeyTagged,tagf:KeyTagged)
                LOOP UNTIL Access:SBO_GenericTagFile.Next() <> Level:Benign
                    IF (tagf:SessionID <> p_web.SessionID OR |
                        tagf:TagType <> 'M')
                        BREAK
                    END ! IF
                    IF (tagf:Tagged = 1)
                        XML:AddElement('Manufacturer',tagf:TaggedValue)
                    END !I F
                END ! LOOP
                XML:CloseParent('Manufacturers')
            END ! IF            
            XML:CloseParent('Criteria')
            
        
            XML:CloseXMLFile()
            XML:Free()

            IF (RepQ.SaveCriteriaAndRun())
            END ! IF
        END !

        RETURN
! InsuranceReport
InsuranceReport        PROCEDURE()
    CODE
        IF (RepQ.Init('Insurance Claim Report','VODR0115.EXE'))
            RETURN
        END ! IF
        IF (~XML:CreateXMLFile(RepQ.FullCriteriaFilename))
            XML:CreateParent('Criteria'); XML:AddParent()
            XML:AddElement('ReportName', RepQ.ReportName)
            XML:CreateParent('Defaults'); XML:AddParent()
            XML:AddElement('StartDate',p_web.GSV('locStartDate'))
            XML:AddElement('EndDate',p_web.GSV('locEndDate'))
            XML:CloseParent('Defaults')
            XML:CloseParent('Criteria')
            XML:CloseXMLFile()
            XML:Free()

            IF (RepQ.SaveCriteriaAndRun())
            END ! IF
        END ! IF
        RETURN
! LoanPhoneExport
LoanPhoneExport     PROCEDURE()
    CODE
        IF (RepQ.Init('Loan Phone Report','VODR0117.EXE'))
            RETURN
        END ! IF
        
        IF (~XML:CreateXMLFile(RepQ.FullCriteriaFilename))
            XML:CreateParent('Criteria'); XML:AddParent()
            XML:CreateParent('Defaults'); XML:AddParent()
            XML:AddElement('LoanStatus',p_web.GSV('locLoanStatus'))
            XML:AddElement('StockType',p_web.GSV('locStockType')) 
            XML:AddElement('StartDate',p_web.GSV('locStartDate'))
            XML:AddElement('EndDate',p_web.GSV('locEndDate'))
            XML:CloseParent('Defaults')
            XML:CloseParent('Criteria')
        
            XML:CloseXMLFile()
        
            XML:Free()
        
            IF (RepQ.SaveCriteriaAndRun())
            END!  IF
        END ! IF
        
        RETURN
! SMSResponseReport
SMSResponseReport        PROCEDURE()
    CODE
        IF (RepQ.Init('SMS Response Report','VODR0112.EXE'))
            RETURN
        END ! IF
        IF (~XML:CreateXMLFile(RepQ.FullCriteriaFilename))
            XML:CreateParent('Criteria'); XML:AddParent()
            XML:AddElement('ReportName', RepQ.ReportName)
            XML:CreateParent('Defaults'); XML:AddParent()
            XML:AddElement('StartDate',p_web.GSV('locStartDate'))
            XML:AddElement('EndDate',p_web.GSV('locEndDate'))
            XML:CloseParent('Defaults')
            XML:CloseParent('Criteria')
            XML:CloseXMLFile()
            XML:Free()

            IF (RepQ.SaveCriteriaAndRun())
            END ! IF
        END ! IF
        RETURN
! StatusReport
StatusReport        PROCEDURE()
i LONG
    CODE
        
        LOOP 4 TIMES
            i += 1
            CASE i
            OF 1
                IF (p_web.GSV('locExportJobs') <> 1)
                    CYCLE 
                END ! IF
            OF 2
                IF (p_web.GSV('locExportAudit') <> 1)
                    CYCLE 
                END ! IF
            OF 3
                IF (p_web.GSV('locExportCParts') <> 1)
                    CYCLE 
                END ! IF
            OF 4
                IF (p_web.GSV('locExportWParts') <> 1)
                    CYCLE 
                END ! IF
            END ! END
            
            IF (RepQ.Init('Status Report (' & CHOOSE(i,'Jobs','Audit','Chargeable Parts','Warranty Parts') & ')' ,'VODR0116.EXE'))
                RETURN
            END ! IF
            IF (~XML:CreateXMLFile(RepQ.FullCriteriaFilename))
                XML:CreateParent('Criteria'); XML:AddParent()
                XML:AddElement('ReportName', RepQ.ReportName)
                XML:CreateParent('Defaults'); XML:AddParent()
                IF (i = 1)
                    XML:AddElement('ExportJobs',p_web.GSV('locExportJobs'))
                END !I F
                IF (i = 2)
                    XML:AddElement('ExportAudit',p_web.GSV('locExportAudit'))
                END ! IF
                IF (i = 3)
                    XML:AddElement('ExportCParts',p_web.GSV('locExportCParts'))
                END ! IF
                IF (i = 4)
                    XML:AddElement('ExportWParts',p_web.GSV('locExportWParts'))
                END ! IF
                
                XML:AddElement('ExportType',p_web.GSV('locExportType'))
                XML:AddElement('AllSubAccounts',p_web.GSV('locTagAllSubAccounts'))
                XML:AddElement('AllGenericAccounts',p_web.GSV('locTagAllGenericAccounts'))
                XML:AddElement('AllStatusTypes',p_web.GSV('locTagAllStatusTypes'))
                XML:AddElement('StatusType',p_web.GSV('locStatusType'))
                XML:AddElement('ShowCustomerStatus',p_web.GSV('locShowCustomerStatus'))
                XML:AddElement('AllLocations',p_web.GSV('locTagAllLocations'))
                XML:AddElement('ReportOrder',p_web.GSV('locReportOrder'))
                XML:AddElement('DateRangeType',p_web.GSV('locDateRangeType'))
                XML:AddElement('StartDate',p_web.GSV('locStartDate'))
                XML:AddElement('EndDate',p_web.GSV('locEndDate'))
            
                XML:CloseParent('Defaults')
                XML:GotoTop()
                IF (p_web.GSV('locTagAllSubAccounts') <> 1)
                    XML:CreatePArent('SubAccounts'); XML:AddParent()
                    Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
                    tagf:SessionID = p_web.SessionID
                    tagf:TagType = kSubAccount
                    SET(tagf:KeyTagged,tagf:KeyTagged)
                    LOOP UNTIL Access:SBO_GenericTagFile.Next() <> Level:Benign
                        IF (tagf:SessionID <> p_web.SessionID OR |
                            tagf:TagType <> kSubAccount)
                        END ! IF
                        IF (tagf:Tagged = 1)
                            XML:AddElement('AccountNumber', tagf:TaggedValue)
                        END! I F
                    END ! LOOP
                    XML:CloseParent('SubAccounts')
                END ! IF
                XML:GotoTop()
                IF (p_web.GSV('locTagAllGenericAccounts') <> 1)
                    XML:CreatePArent('GenericAccounts'); XML:AddParent()
                    Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
                    tagf:SessionID = p_web.SessionID
                    tagf:TagType = kGenericAccount
                    SET(tagf:KeyTagged,tagf:KeyTagged)
                    LOOP UNTIL Access:SBO_GenericTagFile.Next() <> Level:Benign
                        IF (tagf:SessionID <> p_web.SessionID OR |
                            tagf:TagType <> kGenericAccount)
                        END ! IF
                        IF (tagf:Tagged = 1)
                            XML:AddElement('AccountNumber', tagf:TaggedValue)
                        END! I F
                    END ! LOOP
                    XML:CloseParent('GenericAccounts')
                END ! IF  
                XML:GotoTop()
                IF (p_web.GSV('locTagAllStatusTypes') <> 1)
                    XML:CreatePArent('StatusTypes'); XML:AddParent()
                    Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
                    tagf:SessionID = p_web.SessionID
                    tagf:TagType = kStatusTypes
                    SET(tagf:KeyTagged,tagf:KeyTagged)
                    LOOP UNTIL Access:SBO_GenericTagFile.Next() <> Level:Benign
                        IF (tagf:SessionID <> p_web.SessionID OR |
                            tagf:TagType <> kStatusTypes)
                        END ! IF
                        IF (tagf:Tagged = 1)
                            XML:AddElement('StatusType', tagf:TaggedValue)
                        END! I F
                    END ! LOOP
                    XML:CloseParent('StatusTypes')
                END ! IF   
                XML:GotoTop()
                IF (p_web.GSV('locTagAllLocations') <> 1)
                    XML:CreatePArent('Locations'); XML:AddParent()
                    Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
                    tagf:SessionID = p_web.SessionID
                    tagf:TagType = kInternalLocation
                    SET(tagf:KeyTagged,tagf:KeyTagged)
                    LOOP UNTIL Access:SBO_GenericTagFile.Next() <> Level:Benign
                        IF (tagf:SessionID <> p_web.SessionID OR |
                            tagf:TagType <> kInternalLocation)
                        END ! IF
                        IF (tagf:Tagged = 1)
                            XML:AddElement('Location', tagf:TaggedValue)
                        END! I F
                    END ! LOOP
                    XML:CloseParent('Locations')
                END ! IF              
                       
                XML:CloseParent('Criteria')
            
        
                XML:CloseXMLFile()
                XML:Free()

                IF (RepQ.SaveCriteriaAndRun())
                END ! IF
            END !
        END ! IF
        

        RETURN
! StockValueReport
StockValueReport    PROCEDURE()
    CODE
        IF (RepQ.Init('Stock Value Report (' & CHOOSE(p_web.GSV('locReportType') = 1,'Detailed','Summary') & ')' ,'VODR0095.EXE'))
            RETURN
        END ! IF
        IF (~XML:CreateXMLFile(RepQ.FullCriteriaFilename))
            XML:CreateParent('Criteria'); XML:AddParent()
            XML:AddElement('ReportName', RepQ.ReportName)
            XML:CreateParent('Defaults'); XML:AddParent()
            XML:AddElement('ReportType',p_web.GSV('locReportType'))
            XML:AddElement('AllShelfLocations',p_web.GSV('locAllShelfLocations'))
            XML:AddElement('IncludeAccessories',p_web.GSV('locIncludeAccessories'))
            XML:AddElement('SuppressZeros',p_web.GSV('locSuppressZeros'))
            XML:CloseParent('Defaults')
            XML:GotoTop()
            IF (p_web.GSV('locAllShelfLocations') <> 1)
                XML:CreateParent('ShelfLocations'); XML:AddParent()
                Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
                tagf:SessionID = p_web.SessionID
                tagf:TagType = kShelfLocation
                SET(tagf:KeyTagged,tagf:KeyTagged)
                LOOP UNTIL Access:SBO_GenericTagFile.Next() <> Level:Benign
                    IF (tagf:SessionID <> p_web.SessionID OR |
                        tagf:TagType <> kShelfLocation)
                    END ! IF
                    IF (tagf:Tagged = 1)
                        XML:AddElement('ShelfLocation', tagf:TaggedValue)
                    END! I F
                END ! LOOP
                XML:CloseParent('ShelfLocations')
            END ! IF  
            XML:CloseParent('Criteria')
            
        
            XML:CloseXMLFile()
            XML:Free()

            IF (RepQ.SaveCriteriaAndRun())
            END ! IF
        END !

        RETURN          
! WarrantyIncomeReport
WarrantyIncomeReport      PROCEDURE()
    CODE
        IF (RepQ.Init('Warranty Income Report' ,'VODR0064.EXE'))
            RETURN
        END ! IF
        IF (~XML:CreateXMLFile(RepQ.FullCriteriaFilename))
            XML:CreateParent('Criteria'); XML:AddParent()
            XML:AddElement('ReportName', RepQ.ReportName)
            XML:CreateParent('Defaults'); XML:AddParent()
            XML:AddElement('ReportOrder', p_web.GSV('locReportOrder')) !0 (man) or 1 (job no) or 2 (date comp)
            XML:AddElement('ReportType', p_web.GSV('ReportType')) !E or P
            XML:AddElement('GenericAccounts', p_web.GSV('locGenericAccounts')) !0 or 1
            XML:AddElement('AllAccounts', p_web.GSV('locAllACcounts')) !0 or 1
            XML:AddElement('AllManufacturers',p_web.GSV('locAllManufacturers')) !0 or 1
            XML:AddElement('AllChargeTypes',p_web.GSV('locAllChargeTypes')) ! 0 to 1
            XML:AddElement('StartDate', p_web.GSV('locStartDate'))
            XML:AddElement('EndDate', p_web.GSV('locEndDate'))
            XML:AddElement('ZeroSuppression', p_web.GSV('locZeroSupression'))
            XML:AddElement('AllWarrantyStatus',p_web.GSV('locAllWarrantyStatus')) !0 or 1
            XML:AddElement('Submitted',p_web.GSV('locSubmitted'))
            XML:AddElement('Resubmitted',p_web.GSV('locResubmitted'))
            XML:AddElement('Rejected',p_web.GSV('locRejected'))
            XML:AddElement('RejectionAcknowledged',p_web.GSV('locRejectionAcknowledged'))
            XML:AddElement('Reconciled',p_web.GSV('locReconciled'))
            XML:AddElement('ExcludedOnly',p_web.GSV('locExcludedOnly'))
            XML:AddElement('DateRangeType',p_web.GSV('locDateRangeType')) !0 (completed) or 1 (submitted) or 2 (accepted)
            XML:AddElement('IncludeARCRepairedJobs',p_web.GSV('locIncludeARCRepairedJobs'))
            
            XML:CloseParent('Defaults')
            XML:GotoTop()
            IF (p_web.GSV('locAllAccounts') <> 1)
                XML:CreatePArent('Accounts'); XML:AddParent()
                Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
                tagf:SessionID = p_web.SessionID
                tagf:TagType = CHOOSE(p_web.GSV('locGenericAccounts') = 1,kGenericAccount,kSubAccount)
                SET(tagf:KeyTagged,tagf:KeyTagged)
                LOOP UNTIL Access:SBO_GenericTagFile.Next() <> Level:Benign
                    IF (tagf:SessionID <> p_web.SessionID OR |
                        tagf:TagType <> CHOOSE(p_web.GSV('locGenericAccounts') = 1,kGenericAccount,kSubAccount))
                        BREAK
                    END ! IF
                    IF (tagf:Tagged = 1)
                        XML:AddElement('AccountNumber', tagf:TaggedValue)
                    END! I F
                END ! LOOP
                XML:CloseParent('Accounts')
            END ! IF
            XML:GotoTop()
            IF (p_web.GSV('locAllChargeTypes') <> 1)
                XML:CreateParent('ChargeTypes'); XML:AddParent()
                Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
                tagf:SessionID = p_web.SessionID
                tagf:TagType = 'C'
        
                SET(tagf:KeyTagged,tagf:KeyTagged)
                LOOP UNTIL Access:SBO_GenericTagFile.Next() <> Level:Benign
                    IF (tagf:SessionID <> p_web.SessionID OR |
                        tagf:TagType <> 'C')
                        BREAK
                    END ! IF
                    IF (tagf:Tagged = 1)
                        XML:AddElement('ChargeType',tagf:TaggedValue)
                    END !I F
                END ! LOOP
                XML:CloseParent('ChargeTypes')
            END ! IF
            XML:GotoTop()
            IF (p_web.GSV('locAllManufacturers') <> 1)
                XML:CreateParent('Manufacturers'); XML:AddParent()
                Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
                tagf:SessionID = p_web.SessionID
                tagf:TagType = 'M'
        
                SET(tagf:KeyTagged,tagf:KeyTagged)
                LOOP UNTIL Access:SBO_GenericTagFile.Next() <> Level:Benign
                    IF (tagf:SessionID <> p_web.SessionID OR |
                        tagf:TagType <> 'M')
                        BREAK
                    END ! IF
                    IF (tagf:Tagged = 1)
                        XML:AddElement('Manufacturer',tagf:TaggedValue)
                    END !I F
                END ! LOOP
                XML:CloseParent('Manufacturers')
            END ! IF            
            XML:CloseParent('Criteria')
            
        
            XML:CloseXMLFile()
            XML:Free()

            IF (RepQ.SaveCriteriaAndRun())
            END ! IF
        END !

        RETURN
RepQ.Init           PROCEDURE(STRING pReportName, STRING pReportEXE)!,LONG
RetValue                LONG(Level:Benign)
    CODE
        Relate:REPORTQ.Open()
        Access:REPORTQ.ClearKey(rpq:UserReportDescriptionKey)
        rpq:AccountNumber = BHStripForFilename(p_web.GSV('BookingAccount')) ! Used for folder name
        rpq:Usercode = p_web.GSV('BookingUsercode')
        rpq:ReportDescription = pReportName
        SET(rpq:UserReportDescriptionKey,rpq:UserReportDescriptionKey)
        LOOP UNTIL Access:REPORTQ.Next() <> Level:Benign
            IF (rpq:AccountNumber <> BHStripForFilename(p_web.GSV('BookingAccount')) OR |
                rpq:Usercode <> p_web.GSV('BookingUsercode') OR |
                rpq:ReportDescription <> pReportName)
                BREAK
            END ! IF
            
            IF (rpq:Status <> 'Finished')
                SELF.Error('Error! You are already running this report!<br/><br/>Please check the Report Queue to determined when the report has finished.')
                RetValue = Level:Fatal
                BREAK
            END ! IF
        END ! IF
        
        Relate:REPORTQ.Close()
        
        
        SELF.CriteriaFilename = FORMAT(RANDOM(10000000,99999999),@n08) & '.xml'
        
        SELF.FullCriteriaFilename = PATH() & '\temp\' & CLIP(SELF.CriteriaFilename)
        
        SELF.ReportName = pReportName
        SELF.CommandLine = ''
        SELF.ReportEXE = pReportEXE
        
        RETURN RetValue
        
        
RepQ.SaveCriteriaAndRun PROCEDURE()
RetValue    LONG(Level:Benign)
    CODE
        Relate:REPORTQ.Open()
        IF (Access:REPORTQ.PrimeRecord() = Level:Benign)
            rpq:AccountNumber = BHStripForFilename(p_web.GSV('BookingAccount'))  !Strip non "filename" characters as will be used for folder name
            rpq:ReportDescription = SELF.ReportName
            rpq:Usercode = p_web.GSV('BookingUserCode')
            rpq:StartDate = TODAY()
            rpq:StartTime = CLOCK()
            rpq:CriteriaXMLFile = SELF.CriteriaFilename
            rpq:Status = 'Loading'
            IF (Access:REPORTQ.TryInsert())
                Access:REPORTQ.CancelAutoInc()
                RetValue = Level:Fatal
            END ! IF
            SELF.CommandLine = CLIP(SELF.ReportEXE) & ' /SBO' & FORMAT(rpq:RecordNumber,@n010)
        ELSE
            RetValue = Level:Fatal
        END ! IF
        
        Relate:REPORTQ.Close()
        
        IF (RetValue = Level:Benign)
            RUN(SELF.CommandLine)
            SELF.Finished()
        ELSE
            SELF.Error('An error occurred creating your report. Please try again later.')
        END ! IF
        
        RETURN RetValue
        
RepQ.Error          PROCEDURE(STRING pErrorText)
    CODE
        SELF.Message('Report Error','<span class="RedBold">' & CLIP(pErrorText) & '</span>')
         
RepQ.Message        PROCEDURE(STRING pTitle, STRING pText)
    CODE
        p_web.SSV('myalertHeading',CLIP(pTitle))
        p_web.SSV('myalertText',CLIP(pText))  

RepQ.Finished       PROCEDURE()
    CODE
        SELF.Message('Report Running','The report is now running.<br/><br/>Check the Report Queue. You will be able to download the report once it has finished.')
        
DeleteReport        PROCEDURE()
ReportName STRING(60)
    CODE
        IF (p_web.IfExistsValue('rpq:RecordNumber'))
            Relate:REPORTQ.Open()
            Access:REPORTQ.ClearKey(rpq:RecordNumberKey)
            rpq:RecordNumber = p_web.GetValue('rpq:RecordNumber')
            IF (Access:REPORTQ.TryFetch(rpq:RecordNumberKey) = Level:Benign)
                ReportName = rpq:ReportDescription
                REMOVE(PATH() & '\downloads\' & CLIP(rpq:AccountNumber) & '\' & CLIP(rpq:FinalURL))
                Access:REPORTQ.DeleteRecord(0)             
                
            END ! IF            
            Relate:REPORTQ.Close()
                        
            RepQ.Message('Delete Report',CLIP(ReportName) & '<br/><br/>The selected report has been deleted.')
                    
        END ! IF
        
DeleteAllReports        PROCEDURE()
ReportName STRING(60)
    CODE
            Relate:REPORTQ.Open()
            Access:REPORTQ.ClearKey(rpq:UserReportDescriptionKey)
            rpq:AccountNumber = p_web.GSV('BookingAccount')
            rpq:Usercode = p_web.GSV('BookingUsercode')
            SET(rpq:UserReportDescriptionKey,rpq:UserReportDescriptionKey)
            LOOP UNTIL Access:REPORTQ.Next() <> Level:Benign
                IF (rpq:AccountNumber <> p_web.GSV('BookingAccount') OR |
                    rpq:Usercode <> p_web.GSV('BookingUsercode'))
                    BREAK
                END!  IF
                REMOVE(PATH() & '\downloads\' & CLIP(rpq:AccountNumber) & '\' & CLIP(rpq:FinalURL))
                Access:REPORTQ.DeleteRecord(0)  
                    
            END ! LOOP
            
            RepQ.Message('Delete Reports','All Reports Deleted.')
                    
        
