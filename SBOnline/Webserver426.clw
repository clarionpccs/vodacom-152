

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER426.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER260.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER419.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER424.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER425.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER428.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER429.INC'),ONCE        !Req'd for module callout resolution
                     END


UpdateJobsStatus     PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
NewEngineerName      STRING(50)                            !
ShowStickyNoteHistory STRING(1)                            !
SendingResult        STRING(255)                           !
NewLocation          STRING(30)                            !
NewEngineer          STRING(30)                            !
NewSkillLevel        STRING(20)                            !
NewStatus            STRING(20)                            !
NewJobRef            STRING(20)                            !
Hide_Location        BYTE                                  !
Hide_SkillLevel      BYTE                                  !
Hide_Status          BYTE                                  !
Save_joe_ID          USHORT                                !
ReportCompleted      STRING(4)                             !
ReportRequired       BYTE                                  !
                    MAP
UpdateJobDetails        PROCEDURE(*STRING pError)
                    END!  MAP
FilesOpened     Long
JOBSE::State  USHORT
JOBS::State  USHORT
AUDIT::State  USHORT
JOBSE2::State  USHORT
JOBSENG::State  USHORT
LOCATLOG::State  USHORT
TagFile::State  USHORT
CONTHIST::State  USHORT
DEFAULT2::State  USHORT
DEFAULTS::State  USHORT
TRADEACC::State  USHORT
ACCSTAT::State  USHORT
LOCINTER::State  USHORT
USERS::State  USHORT
STATUS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('UpdateJobsStatus')
  loc:formname = 'UpdateJobsStatus_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'UpdateJobsStatus',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('UpdateJobsStatus','')
    p_web._DivHeader('UpdateJobsStatus',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferUpdateJobsStatus',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferUpdateJobsStatus',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferUpdateJobsStatus',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_UpdateJobsStatus',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferUpdateJobsStatus',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_UpdateJobsStatus',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'UpdateJobsStatus',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
JobUpdate           ROUTINE
    
    Do JobUpdateDetails     !in subroutine so I can use "exit"

    !show the message ...
    p_web.Message('Alert',clip(SendingResult),p_web._MessageClass,Net:Send)
    !Objections: "Do not show the action string if sucessful, it wastes the users time clicking [OK]"
    !reason for keeping message
    !if you don't show the message you lose the focus and the user has to click back in the "newjobref" field
    !Harvey and J agree that having the message and having to touch [enter] is easier than using mouse to get focus back
    !so the message stays!!    
    
    
    !Set the CompletedJobs in tagfile to show in list
    if SendingResult[1:5] = 'ERROR' THEN
        !dont update the completed jobs list ...
    ELSE 
        
        Access:Tagfile.PrimeRecord()
        tag:sessionID = p_web.SessionID
        tag:taggedValue = clip(p_web.gsv('newJobRef'))
        tag:tagged = 1
        access:Tagfile.insert()

    END !if sendingresult was an error

    !reset NewJobRef
    p_web.ssv('NewJobRef','')
    NewJobRef = ''
    
    p_web.ssv('ReportRequired','Y')

    EXIT

    
    
JobUpdateDetails    Routine    
    
    !check for missing / incorrect fields   
    if clip(p_web.gsv('NewJobRef')) = '' THEN
        SendingResult = 'ERROR - You must give a job reference number'
        EXIT    
    END !if job ref blank
    SendingResult = 'Job Number: '&clip(p_web.gsv('NewJobRef'))& ' updated successfully'

    if not NUMERIC(p_web.gsv('NewJobRef')) THEN
        SendingResult = 'ERROR - The job reference number '&clip(p_web.gsv('NewJobRef'))& ' must contain only numbers'
        EXIT    
    END !if job ref contains non numberic characters
    !No change to successful "sending result"

    if INSTRING('.',p_web.gsv('NewJobRef'),1,1) THEN
        SendingResult = 'ERROR - The job reference number '&clip(p_web.gsv('NewJobRef'))& ' must not contain decimals'
        EXIT    
    END !if job ref contains '.'
    !No change to successful "sending result"  
    
    if clip(p_web.gsv('NewEngineer')) = '' THEN
        SendingResult = 'ERROR - You must select an engineer.'
        EXIT                
    End !if engineer blank
    !SendingResult = clip(SendingResult)&'<13,10>Change engineer to: '&p_web.gsv('NewEngineer')

    if p_web.gsv('Hide_Location') = false THEN
        !change location
        if clip(p_web.gsv('NewLocation')) = '' THEN
            SendingResult = 'ERROR - You must select a new location for the job.' 
            EXIT                    
        END 
        !SendingResult = clip(SendingResult)&'<13,10>Change location to: '&p_web.gsv('NewLocation')               
    END !if not hide location

    if p_web.gsv('Hide_SkillLevel') = false THEN 
        !change skill level
        if clip(p_web.gsv('NewSkillLevel')) = '' then  
            SendingResult = 'ERROR - you must supply a new skill level for the job.' 
            EXIT  
        END 
        !SendingResult = clip(SendingResult) & '<13,10>Change Skill to: '&p_web.gsv('NewSkillLevel') 
    END !if not hide skill level
                
    if p_web.gsv('Hide_Status') = FALSE
        !change status
        if clip(p_web.gsv('NewStatus')) = '' THEN
            SendingResult = 'ERROR - You must give a new status for this job' 
            EXIT
        END 
        !SendingResult = clip(SendingResult)&'<13,10>Change status to: '&clip(p_web.gsv('NewStatus'))        
    END !if not hide status
                 
!    !TB12540 - Show contact history if there is a sticky note
!    if p_web.gsv('ShowStickyNoteHistory') = '0' !this has not been looked at before
!        !need to find one that has not been cancelled, and is valid for this request 
!        Access:conthist.clearkey(cht:KeyRefSticky)
!        cht:Ref_Number = deformat(p_web.gsv('NewJobRef'))
!        cht:SN_StickyNote = 'Y'
!        Set(cht:KeyRefSticky,cht:KeyRefSticky)      
!        Loop
!            if access:Conthist.next() then break.
!            IF cht:Ref_Number <> deformat(p_web.gsv('NewJobRef')) then break.
!            if cht:SN_StickyNote <> 'Y' then break.
!            if cht:SN_Completed <> 'Y' and cht:SN_EngAlloc = 'Y' then
!                !UJS_BrowseContactHistory(p_web)
!                SendingResult = 'ERROR - There is a StickyNote in the contact history you should view this before commiting. ' 
!                p_Web.ssv('SaveJobNumber',p_web.gsv('NewJobRef'))
!                !ShowStickyNoteHistory = '1'
!                p_web.ssv('ShowStickyNoteHistory','1')     !so it does not force it again
!                BREAK
!            END !if not completed and engAlloc = y
!        END !loop thrugh conthist        
!    END !if showStickyNoteHistory was false
!    if p_web.gsv('ShowStickyNoteHistory') = '1' then exit.
   
    !Can we open jobs ect? 
    access:jobs.clearkey(job:ref_number_key)   
    job:ref_number = deformat(p_web.gsv('NewJobRef'))
    if access:jobs.fetch(job:ref_number_key) THEN
        SendingResult = 'ERROR - Unable to find job '&clip(p_web.gsv('NewJobRef'))& '. Please check Job Number and try again.'
        EXIT
    END
    
    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    If Access:JOBSE.Fetch(jobe:RefNumberKey) THEN
        SendingResult = 'ERROR - Unable to find JOBS extention file '&clip(p_web.gsv('NewJobRef'))& ' , please check Job NUmber and try again.'
        EXIT
    END
    
    Access:WEBJOB.Clearkey(wob:RefNumberKey)
    wob:RefNumber = job:Ref_Number
    If Access:WEBJOB.fetch(wob:RefNumberKey) 
        !not found
        SendingResult = 'ERROR - Unable to find WEBJOB record '&clip(p_web.gsv('NewJobRef'))& ' . Please check Job Number and try again.'
        EXIT
    End
    
    !account number is 'AA20' for this to be the ARC
    !tra:Account_Number <> p_web.GSV('loc:BranchID')
    if p_web.gsv('Loc:BranchID') = 'AA20' THEN
        !If at the ARC, should only be able to allocate if the job is AT the ARC - 3474 (DBH: 06-01-2004)
        If jobe:HubRepair <> True
            SendingResult = 'ERROR - The selected job '&clip(p_web.gsv('NewJobRef'))& ' is not at the ARC.'
            EXIT
        End !If jobe:HubRepair <> True

        !Check the job is at the ARC Location - L921 (DBH: 13-08-2003)
        Access:LOCATLOG.ClearKey(lot:NewLocationKey)
        lot:RefNumber   = job:Ref_Number
        lot:NewLocation = Clip(GETINI('RRC','ARCLocation','RECEIVED AT ARC',CLIP(PATH())&'\SB2KDEF.INI'))
        If Access:LOCATLOG.TryFetch(lot:NewLocationKey) 
            !Error
            SendingResult =  'ERROR - This job '&clip(p_web.gsv('NewJobRef'))& ' is not at the ARC Location. Please ensure that the Waybill Confirmation procedure has been completed.'
            EXIT
        End !If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign       
        
    ELSE
        !this is at rrc
        !If at the RRC, should only be able to allocate a job from the SAME RRC  
        If wob:HeadAccountNumber <> p_web.gsv('Loc:BranchID')
            SendingResult = 'ERROR - The selected job '&clip(p_web.gsv('NewJobRef'))& ' was booked by a different RRC.'
            EXIT            
        End !If wob:HeadAccountNumber <> ClarioNET:Global.Param2

        If jobe:HubRepair
            SendingResult = 'ERROR - Cannot update '&clip(p_web.gsv('NewJobRef'))& ' . The selected job is not at the RRC.'
            EXIT            
        End !If jobe:HubRepair
    End
    
!========= from here is where the changes start ===============================================================================    
      
    If job:workshop <> 'YES'
        If NewEngineer <> ''
            SendingResult = 'ERROR - This job '&clip(p_web.gsv('NewJobRef'))& ' is not in the workshop. You cannot assign an engineer to it.'
            EXIT
        End!If engineer_temp <> ''
        If NewLocation <> '' And Hide_Location = FALSE
            SendingResult = 'ERROR - This job '&clip(p_web.gsv('NewJobRef'))& ' is not in the workshop. You cannot assign an location to it.'
            EXIT            
        End!If location_temp <> ''
    End!If job:workshop <> 'YES'

    If job:date_completed > 0 and VodacomClass.JobSentToHub(job:Ref_Number) = 0
        !been completed and not gone to ARC
        SendingResult = 'ERROR - The selected job '&clip(p_web.gsv('NewJobRef'))& ' has been completed'
        EXIT
    END
    
    if p_web.gsv('NewEngineer') = job:Engineer THEN
        SendingResult = 'ERROR - The job '&clip(p_web.gsv('NewJobRef'))& ' is already allocated to the selected engineer'
        EXIT
    END
 
    !always updating the status
    GetStatus(sub(p_web.gsv('NewStatus'),1,3),1,'JOB')

    !Updating the engineer?
    If job:workshop = 'YES'
        If p_web.gsv('NewEngineer') <> ''
            !Check current engineer
            If job:Engineer <> ''
                !Lookup current engineer and mark as escalated
                Save_joe_ID = Access:JOBSENG.SaveFile()
                Access:JOBSENG.ClearKey(joe:UserCodeKey)
                joe:JobNumber     = job:Ref_Number
                joe:UserCode      = job:Engineer
                joe:DateAllocated = Today()
                Set(joe:UserCodeKey,joe:UserCodeKey)
                Loop
                    If Access:JOBSENG.PREVIOUS()
                       Break
                    End !If
                    If joe:JobNumber     <> job:Ref_Number       |
                    Or joe:UserCode      <> job:Engineer      |
                    Or joe:DateAllocated > Today()       |
                        Then Break.  ! End If
                    If joe:Status = 'ALLOCATED'
                        joe:Status = 'ESCALATED'
                        joe:StatusDate = Today()
                        joe:StatusTime = Clock()
                        Access:JOBSENG.Update()
                    End !If joe:Status = 'ALLOCATED'
                    Break
                End !Loop
                Access:JOBSENG.RestoreFile(Save_joe_ID)
            Else !If job:Engineer <> ''
                ! Inserting (DBH 11/05/2006) #7597 - First allocation of engineer. Send SMS/Email Alert
                !If glo:WebJob = 1 Or (glo:WebJob = 0 And jobe:WebJob = 0)
                    ! Do not send if ARC engineer being assign to RRC job (DBH: 11-05-2006)
                    Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
                    jobe2:RefNumber = job:Ref_Number
                    If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                        ! Found
                        If jobe2:SMSNotification
                            AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'2ENG','SMS',jobe2:SMSAlertNumber,'',0,'')
                        End ! If jobe2:SMSNotification

                        If jobe2:EmailNotification
                            AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'2ENG','EMAIL','',jobe2:EmailAlertAddress,0,'')
                        End ! If jobe2:EmailNotification
                    Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                        ! Error
                    End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                !End ! If glo:WebJob = 1 Or (glo:WebJob = 0 And jobe:WebJob = 0)
                ! End (DBH 11/05/2006) #7597
            End !If job:Engineer <> ''
            
            job:engineer       = p_web.gsv('NewEngineer')
            
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code   = job:Engineer
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    ! Found

            Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    ! Error
            End ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
            
            AddToAudit(p_web,job:Ref_Number,'JOB','ENGINEER ALLOCATED: ' & Clip(use:User_Code), |
                Clip(use:Forename) & ' ' & Clip(use:Surname) & ' - ' & Clip(use:Location) & ' - LEVEL ' & Clip(use:SkillLevel))
            
            ! Inserting (DBH 16/12/2005) #6633 - Record an entry in the Audit Trail for the engineer allocation
!            If Access:AUDIT.PrimeRecord() = Level:Benign
!                aud:Ref_Number  = job:Ref_Number
!                aud:Date        = Today()
!                aud:Time        = Clock()
!                Access:USERS.Clearkey(use:Password_Key)
!                use:Password    = glo:Password
!                If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
!                    ! Found
!
!                Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
!                    ! Error
!                End ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
!                aud:User        = use:User_Code
!                ! Inserting (DBH 18/01/2006) #6633 - Reget the engineer's details
!                Access:USERS.Clearkey(use:User_Code_Key)
!                use:User_Code   = job:Engineer
!                If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!                    ! Found
!
!                Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!                    ! Error
!                End ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
!                ! End (DBH 18/01/2006) #6633
!                aud:Action      = 'ENGINEER ALLOCATED: ' & Clip(use:User_Code)
!                aud:Type        = 'JOB'
!                aud:Notes       = Clip(use:Forename) & ' ' & Clip(use:Surname) & ' - ' & Clip(use:Location) & ' - LEVEL ' & Clip(use:SkillLevel)
!                If Access:AUDIT.TryInsert() = Level:Benign
!                    ! Insert Successful
!
!                Else ! If Access:AUDIT.TryInsert() = Level:Benign
!                    ! Insert Failed
!                    Access:AUDIT.CancelAutoInc()
!                End ! If Access:AUDIT.TryInsert() = Level:Benign
!            End !If Access:AUDIT.PrimeRecord() = Level:Benign
            ! End (DBH 16/12/2005) #6633
        End!If NewEngineer <> ''

        If Access:JOBSENG.PrimeRecord() = Level:Benign
            joe:JobNumber     = p_web.gsv('NewJobRef')      !job:Ref_Number
            joe:UserCode      = p_web.gsv('NewEngineer')    !job:Engineer
            joe:DateAllocated = Today()
            joe:TimeAllocated = Clock()
            joe:AllocatedBy   = glo:UserCode
            access:users.clearkey(use:User_Code_Key)
            use:User_Code   = p_web.gsv('NewEngineer')      !job:Engineer
            access:users.fetch(use:User_Code_Key)

            joe:EngSkillLevel = use:SkillLevel
            joe:JobSkillLevel = jobe:SkillLevel
            !if jobe:HubRepairDate = '' THEN
            IF (VodacomClass.JobSentToHub(job:Ref_Number) = 0)
                joe:Status = 'ALLOCATED'        !this has never been to the ARC
            ELSE
                joe:Status = 'ENGINEER QA'      !this has been to the ARC
            END            
            joe:StatusDate  = Today()
            joe:StatusTime  = Clock()            
            Access:JOBSENG.Insert()
                        
        End !If Access:JOBSENG.PrimeRecord() = Level:Benign
    END !updating the engineer?

    !update skilllevel?
    if p_web.gsv('Hide_SkillLevel') = false
        If Hide_SkillLevel = 0
            jobe:SkillLevel = p_web.gsv('NewSkillLevel')
        End !If Hide_SkillLevel = 0
    END !if not hidden
    
    
    
    !Start of update location
    if P_WEB.GSv('Hide_Location') = FALSE
        IF p_web.gsv(NewLocation) <> ''THEN
            
            !Add To Old Location
            access:locinter.clearkey(loi:location_key)
            loi:location = job:location
            If access:locinter.fetch(loi:location_key) = Level:Benign
                If loi:allocate_spaces = 'YES'
                    loi:current_spaces+= 1
                    loi:location_available = 'YES'
                    access:locinter.update()
                End
            end !if fetch()
            
            !Take From New Location
            !Write To Log
            If Access:LOCATLOG.PrimeRecord() = Level:Benign
                lot:RefNumber   = job:Ref_Number
!                Access:USERS.Clearkey(use:Password_Key)
!                use:Password    = glo:Password
!                If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                    !Found
                    lot:UserCode    = p_web.GSV('BookingUserCode') ! Do not use Global Vars!! (DBH: 21/11/2014) use:User_code
!                End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign

                lot:PreviousLocation    = job:Location
                lot:NewLocation         = p_web.gsv('NewLocation')
                Access:LOCATLOG.TryInsert() 
                
            End !If Access:LOCATLOG.PrimeRecord() = Level:Benign
            
            !update the job record
            job:location = p_web.gsv('NewLocation')
            
            !record new location
            access:locinter.clearkey(loi:location_key)
            loi:location = p_web.gsv('NewLocation')         !job:location
            If access:locinter.fetch(loi:location_key) = Level:Benign
                If loi:allocate_spaces = 'YES'
                    loi:current_spaces -= 1
                    If loi:current_spaces< 1
                        loi:current_spaces = 0
                        loi:location_available = 'NO'
                    End
                    access:locinter.update()
                End
            end !if fetch

        End!If NewLocation <> ''
    End!If not hiding location

    !update all files
    Access:jobs.update()
    Access:JOBSE.Update()
    !Access:jobse2.update() - nothing to update - only looked up if this is first job allocation (to allow sms/email)
    

    EXIT
    
OpenFiles  ROUTINE
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(AUDIT)
  p_web._OpenFile(JOBSE2)
  p_web._OpenFile(JOBSENG)
  p_web._OpenFile(LOCATLOG)
  p_web._OpenFile(TagFile)
  p_web._OpenFile(CONTHIST)
  p_web._OpenFile(DEFAULT2)
  p_web._OpenFile(DEFAULTS)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(ACCSTAT)
  p_web._OpenFile(LOCINTER)
  p_web._OpenFile(USERS)
  p_web._OpenFile(STATUS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(AUDIT)
  p_Web._CloseFile(JOBSE2)
  p_Web._CloseFile(JOBSENG)
  p_Web._CloseFile(LOCATLOG)
  p_Web._CloseFile(TagFile)
  p_Web._CloseFile(CONTHIST)
  p_Web._CloseFile(DEFAULT2)
  p_Web._CloseFile(DEFAULTS)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(ACCSTAT)
  p_Web._CloseFile(LOCINTER)
  p_Web._CloseFile(USERS)
  p_Web._CloseFile(STATUS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
      !Set variable used in view job if called
      p_web.ssv('ReturnURL','UpdateJobsStatus')
      !set variable used by SelectEngineesr when called
      !p_web.ssv('FilterByEngineer','YES')
      
      p_web.SSV('filter:Engineer','Upper(use:Active) = <39>YES<39> And Upper(use:Location) = Upper(<39>' & Clip(use:location) & '<39>) And Upper(use:User_Type) = <39>ENGINEER<39>')
  p_web.SetValue('UpdateJobsStatus_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  Case p_Web.GetValue('lookupfield')
  Of 'NewLocation'
    p_web.setsessionvalue('showtab_UpdateJobsStatus',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(LOCINTER)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.NewEngineer')
  Of 'NewEngineer'
    p_web.setsessionvalue('showtab_UpdateJobsStatus',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(USERS)
        p_web.setsessionvalue('NewEngineerName',use:Forename & Use:surname)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.NewEngineerName')
  Of 'NewStatus'
    p_web.setsessionvalue('showtab_UpdateJobsStatus',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(ACCSTAT)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.NewJobRef')
  End
  loc:TabNumber += 1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('NewSkillLevel',NewSkillLevel)
  p_web.SetSessionValue('NewLocation',NewLocation)
  p_web.SetSessionValue('NewEngineer',NewEngineer)
  p_web.SetSessionValue('NewEngineerName',NewEngineerName)
  p_web.SetSessionValue('NewStatus',NewStatus)
  p_web.SetSessionValue('NewJobRef',NewJobRef)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('NewSkillLevel')
    NewSkillLevel = p_web.GetValue('NewSkillLevel')
    p_web.SetSessionValue('NewSkillLevel',NewSkillLevel)
  End
  if p_web.IfExistsValue('NewLocation')
    NewLocation = p_web.GetValue('NewLocation')
    p_web.SetSessionValue('NewLocation',NewLocation)
  End
  if p_web.IfExistsValue('NewEngineer')
    NewEngineer = p_web.GetValue('NewEngineer')
    p_web.SetSessionValue('NewEngineer',NewEngineer)
  End
  if p_web.IfExistsValue('NewEngineerName')
    NewEngineerName = p_web.GetValue('NewEngineerName')
    p_web.SetSessionValue('NewEngineerName',NewEngineerName)
  End
  if p_web.IfExistsValue('NewStatus')
    NewStatus = p_web.GetValue('NewStatus')
    p_web.SetSessionValue('NewStatus',NewStatus)
  End
  if p_web.IfExistsValue('NewJobRef')
    NewJobRef = p_web.GetValue('NewJobRef')
    p_web.SetSessionValue('NewJobRef',NewJobRef)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('UpdateJobsStatus_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
     !set up the screen and the defaults
    !some things are done every time - a few defaults are only done the first time ...
  
    !read files needed
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    Set(DEFAULT2)
    Access:DEFAULT2.Next()
  
      !  set up the filter so can only allocate to users at the same location
      !  p_web.ssv('FilterLocation',use:Location)
      !  FilterLocation = use:location
      !  changing to use selectengineers
      !  p_web.SSV('filter:Engineer','Upper(use:Active) = <39>YES<39> And Upper(use:Location) = Upper(<39>' & Clip(use:Location) & '<39>) And Upper(use:User_Type) = <39>ENGINEER<39>')
    
    p_web.SSV('filter:Engineer','UPPER(use:Active) = ''YES'' AND ' & | 
        'UPPER(use:Location) = UPPER(''' & p_web.GSV('BookingSiteLocation') & ''') AND ' & | 
        'UPPER(use:User_Type) = ''ENGINEER''')
    
  
    !Hide and show things - set the variables to say hidden
    If def:HideLocation Or GETINI('RAPIDSTATUS','HideLocation',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        Hide_Location = TRUE
    ELSE
        if p_web.gsv('NewUpdateType') = '' THEN !new update type not selected yet
            hide_location = TRUE
        ELSE
            Hide_location = FALSE
        END
    END  !default Hide Location
    p_web.ssv('Hide_Location',Hide_location)
  
    If GETINI('HIDE','SkillLevel',,CLIP(PATH())&'\SB2KDEF.INI') = 1 Or GETINI('RAPIDSTATUS','HideSkillLevel',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        Hide_SkillLevel = TRUE
    ELSE
        if p_web.gsv('NewUpdateType') = '' THEN !new update type not selected yet
            Hide_SkillLevel = TRUE
        ELSE
            Hide_skillLevel = false
        END
    END !default hide skill level
    p_web.ssv('Hide_SkillLevel',Hide_SkillLevel)
  
    If GETINI('RAPIDSTATUS','HideStatus',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        Hide_Status = TRUE
    ELSE
        Hide_status = FALSE
    END !default hide status
    p_web.ssv('Hide_Status',Hide_Status)
  
         
    !default job number may be reset depending on what has been happening
    if p_Web.gsv('SaveJobNumber') <> '' THEN
      !this does not blank
        NewJobRef = p_web.gsv('SaveJobNumber')
        p_web.ssv('SaveJobNumber','') 
        p_web.ssv('ShowStickyNoteHistory','1')      !shows it has been done and will not do it again
    ELSE
        NewJobRef = ''
        p_web.ssv('ShowStickyNoteHistory','0')      !so it will force it next time
    END
    P_web.ssv('NewJobRef',NewJobRef)
  
  
  !following defaults are only set up if this is the first time it has been called
  !check the first call variable (set true at the previous window)
    if p_web.gsv('AllocationFirstCall') = TRUE THEN
      
      !reset the first call variable so this does not get used again
        p_web.ssv('AllocationFirstCall',False)
  
      !default new status
        If GETINI('RAPIDSTATUS','SetStatus',,CLIP(PATH())&'\SB2KDEF.INI') = 1
            NewStatus = GETINI('RAPIDSTATUS','Status',,CLIP(PATH())&'\SB2KDEF.INI')    
        ELSE
            NewStatus = ''
        END
        p_web.SetSessionValue('NewStatus',NewStatus)
  
      !Default  location
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = GETINI('BOOKING','HeadAccount','AA20',CLIP(PATH())&'\SB2KDEF.INI')
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
            NewLocation = tra:SiteLocation    
        ELSE
            NewLocation = ''
        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        p_web.SetSessionValue('NewLocation',NewLocation)
  
      !Allocations Report stuff
      !ReportRequired = getini('RAPIDSTATUS','AutoPrint',0,clip(path()) & '\SB2KDEF.INI')
      !Case ReportRequired
      !of 0 
      !    p_web.SetSessionValue('ReportRequired','N')    !will only be required if something is allocated
      !ELSE
      !    p_web.ssv('ReportRequired','Y' |
      !END
        p_web.SetSessionValue('ReportRequired','N')    !will only be required if something is allocated
        p_web.ssv('ReportPrinted','N')
        
        ! #13575 Clear fields when first launched (DBH: 28/07/2015)
        ClearTagFile(p_web,1)
            
        p_web.SSV('NewJobRef','')
        p_web.SSV('NewEngineer','')
        p_web.SSV('NewEngineerName','')
      
    END !if this is the first call to the window
  
  
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 NewSkillLevel = p_web.RestoreValue('NewSkillLevel')
 NewLocation = p_web.RestoreValue('NewLocation')
 NewEngineer = p_web.RestoreValue('NewEngineer')
 NewEngineerName = p_web.RestoreValue('NewEngineerName')
 NewStatus = p_web.RestoreValue('NewStatus')
 NewJobRef = p_web.RestoreValue('NewJobRef')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'IndexPage'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('UpdateJobsStatus_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('UpdateJobsStatus_ChainTo')
    loc:formaction = p_web.GetSessionValue('UpdateJobsStatus_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndexPage'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="UpdateJobsStatus" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="UpdateJobsStatus" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="UpdateJobsStatus" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Rapid Job Status Update / Job Allocation') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Rapid Job Status Update / Job Allocation',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_UpdateJobsStatus">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_UpdateJobsStatus" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_UpdateJobsStatus')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Job Allocation') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Jobs Updated') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_UpdateJobsStatus')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_UpdateJobsStatus'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='LOCINTER'
            p_web.SetValue('SelectField',clip(loc:formname) & '.NewEngineer')
    End
    If upper(p_web.getvalue('LookupFile'))='USERS'
          If Not (Hide_Status = 1)
            p_web.SetValue('SelectField',clip(loc:formname) & '.NewStatus')
          End
    End
    If upper(p_web.getvalue('LookupFile'))='ACCSTAT'
            p_web.SetValue('SelectField',clip(loc:formname) & '.NewJobRef')
    End
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.NewSkillLevel')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab5'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab6'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_UpdateJobsStatus')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
    do SendPacket
    Do endofpagespacer
    do SendPacket
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab5'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab6'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Job Allocation') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_UpdateJobsStatus_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Allocation')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Job Allocation')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Allocation')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Job Allocation')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::NewSkillLevel
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::NewSkillLevel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::NewSkillLevel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::NewLocation
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::NewLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::NewLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::NewEngineer
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::NewEngineer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::NewEngineer
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::NewEngineerName
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::NewEngineerName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::NewEngineerName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::NewStatus
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::NewStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::NewStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::NewJobRef
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::NewJobRef
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::NewJobRef
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Jobs Updated') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_UpdateJobsStatus_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Updated')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Updated')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Updated')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Jobs Updated')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::BrowseTagFile
      do Comment::BrowseTagFile
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel5">'&CRLF &|
                                    '  <div id="panel5Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel5Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_UpdateJobsStatus_5">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab5" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab5">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab5">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab5">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::AllocationReport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::AllocationReport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::ReportDisplay
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ReportDisplay
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::ReportDisplay
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel6">'&CRLF &|
                                    '  <div id="panel6Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel6Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_UpdateJobsStatus_6">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab6" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab6">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab6">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab6">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ButtonCancel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::ButtonCancel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'30%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ButtonFinish
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'40%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::ButtonFinish
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::NewSkillLevel  Routine
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('NewSkillLevel') & '_prompt',Choose(Hide_SkillLevel = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('New Skill Level')
  If Hide_SkillLevel = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::NewSkillLevel  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('NewSkillLevel',p_web.GetValue('NewValue'))
    NewSkillLevel = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::NewSkillLevel
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('NewSkillLevel',p_web.GetValue('Value'))
    NewSkillLevel = p_web.GetValue('Value')
  End
  do Value::NewSkillLevel
  do SendAlert

Value::NewSkillLevel  Routine
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('NewSkillLevel') & '_value',Choose(Hide_SkillLevel = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (Hide_SkillLevel = 1)
  ! --- STRING --- NewSkillLevel
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('NewSkillLevel')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''NewSkillLevel'',''updatejobsstatus_newskilllevel_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','NewSkillLevel',p_web.GetSessionValueFormat('NewSkillLevel'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateJobsStatus_' & p_web._nocolon('NewSkillLevel') & '_value')

Comment::NewSkillLevel  Routine
      loc:comment = ''
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('NewSkillLevel') & '_comment',Choose(Hide_SkillLevel = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If Hide_SkillLevel = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::NewLocation  Routine
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('NewLocation') & '_prompt',Choose(Hide_Location = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('New Location')
  If Hide_Location = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::NewLocation  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('NewLocation',p_web.GetValue('NewValue'))
    NewLocation = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::NewLocation
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('NewLocation',p_web.GetValue('Value'))
    NewLocation = p_web.GetValue('Value')
  End
  p_Web.SetValue('lookupfield','NewLocation')
  do AfterLookup
  do Value::NewLocation
  do SendAlert
  do Comment::NewLocation

Value::NewLocation  Routine
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('NewLocation') & '_value',Choose(Hide_Location = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (Hide_Location = 1)
  ! --- STRING --- NewLocation
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('NewLocation')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''NewLocation'',''updatejobsstatus_newlocation_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('NewLocation')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','NewLocation',p_web.GetSessionValueFormat('NewLocation'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,'Select the new location using the button to the right') & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('UJS_SelectLocation?LookupField=NewLocation&Tab=3&ForeignField=loi:Location&_sort=loi:Location&Refresh=sort&LookupFrom=UpdateJobsStatus&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateJobsStatus_' & p_web._nocolon('NewLocation') & '_value')

Comment::NewLocation  Routine
      loc:comment = ''
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('NewLocation') & '_comment',Choose(Hide_Location = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If Hide_Location = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateJobsStatus_' & p_web._nocolon('NewLocation') & '_comment')

Prompt::NewEngineer  Routine
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('NewEngineer') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('New Engineer')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::NewEngineer  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('NewEngineer',p_web.GetValue('NewValue'))
    NewEngineer = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::NewEngineer
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('NewEngineer',p_web.GetValue('Value'))
    NewEngineer = p_web.GetValue('Value')
  End
  p_Web.SetValue('lookupfield','NewEngineer')
  do AfterLookup
  do Value::NewEngineerName
  do Value::NewEngineer
  do SendAlert
  do Comment::NewEngineer
  do Prompt::NewEngineerName
  do Value::NewEngineerName  !1

Value::NewEngineer  Routine
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('NewEngineer') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- NewEngineer
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('NewEngineer') <> '','readonly','')
  loc:fieldclass = 'FormEntry'
  If p_web.GSV('NewEngineer') <> ''
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  If lower(loc:invalid) = lower('NewEngineer')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''NewEngineer'',''updatejobsstatus_newengineer_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('NewEngineer')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','NewEngineer',p_web.GetSessionValueFormat('NewEngineer'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,'Select the allocated Engineer') & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('SelectEngineers?LookupField=NewEngineer&Tab=3&ForeignField=use:User_Code&_sort=&Refresh=sort&LookupFrom=UpdateJobsStatus&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateJobsStatus_' & p_web._nocolon('NewEngineer') & '_value')

Comment::NewEngineer  Routine
      loc:comment = ''
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('NewEngineer') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateJobsStatus_' & p_web._nocolon('NewEngineer') & '_comment')

Prompt::NewEngineerName  Routine
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('NewEngineerName') & '_prompt',Choose(NewEngineerName = '','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('New Engineer Name')
  If NewEngineerName = ''
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateJobsStatus_' & p_web._nocolon('NewEngineerName') & '_prompt')

Validate::NewEngineerName  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('NewEngineerName',p_web.GetValue('NewValue'))
    NewEngineerName = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::NewEngineerName
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('NewEngineerName',p_web.GetValue('Value'))
    NewEngineerName = p_web.GetValue('Value')
  End

Value::NewEngineerName  Routine
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('NewEngineerName') & '_value',Choose(NewEngineerName = '','hdiv','adiv'))
  loc:extra = ''
  If Not (NewEngineerName = '')
  ! --- DISPLAY --- NewEngineerName
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.Translate(NewEngineerName,) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateJobsStatus_' & p_web._nocolon('NewEngineerName') & '_value')

Comment::NewEngineerName  Routine
    loc:comment = ''
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('NewEngineerName') & '_comment',Choose(NewEngineerName = '','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If NewEngineerName = ''
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateJobsStatus_' & p_web._nocolon('NewEngineerName') & '_comment')

Prompt::NewStatus  Routine
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('NewStatus') & '_prompt',Choose(Hide_Status = 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('New Status')
  If Hide_Status = 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::NewStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('NewStatus',p_web.GetValue('NewValue'))
    NewStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::NewStatus
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('NewStatus',p_web.GetValue('Value'))
    NewStatus = p_web.GetValue('Value')
  End
  !      p_web.ssv('NewStatus',NewStatus)
  p_Web.SetValue('lookupfield','NewStatus')
  do AfterLookup
  do Value::NewStatus
  do SendAlert
  do Comment::NewStatus

Value::NewStatus  Routine
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('NewStatus') & '_value',Choose(Hide_Status = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (Hide_Status = 1)
  ! --- STRING --- NewStatus
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('NewStatus')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ' onfocus="'&p_web._nocolon('sv('''',''updatejobsstatus_newstatus_value'',257,FieldValue(this,1))')&'"'
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''NewStatus'',''updatejobsstatus_newstatus_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('NewStatus')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = 'readonly'
    packet = clip(packet) & p_web.CreateInput('text','NewStatus',p_web.GetSessionValueFormat('NewStatus'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,'Select new status using the button on the right') & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('UJS_SelectStatus?LookupField=NewStatus&Tab=3&ForeignField=acs:Status&_sort=acs:Status&Refresh=sort&LookupFrom=UpdateJobsStatus&'),) !lookupextra
    End
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateJobsStatus_' & p_web._nocolon('NewStatus') & '_value')

Comment::NewStatus  Routine
      loc:comment = ''
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('NewStatus') & '_comment',Choose(Hide_Status = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If Hide_Status = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateJobsStatus_' & p_web._nocolon('NewStatus') & '_comment')

Prompt::NewJobRef  Routine
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('NewJobRef') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Job Reference Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::NewJobRef  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('NewJobRef',p_web.GetValue('NewValue'))
    NewJobRef = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::NewJobRef
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('NewJobRef',p_web.GetValue('Value'))
    NewJobRef = p_web.GetValue('Value')
  End
  !      p_web.ssv('NewJobRef',NewJobRef)
  !  
  !      !now use the details set:
  !      Do JobUpdate        !in shared routine
    
    UpdateJobDetails(loc:alert)
  do Value::NewJobRef
  do SendAlert
  do Value::BrowseTagFile  !1
  do Comment::BrowseTagFile
  do Value::ButtonCancel  !1
  do Value::ButtonFinish  !1
  do Value::ReportDisplay  !1
  do Value::AllocationReport  !1

Value::NewJobRef  Routine
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('NewJobRef') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- NewJobRef
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('NewJobRef')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''NewJobRef'',''updatejobsstatus_newjobref_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon(NewJobRef)&''',2);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','NewJobRef',p_web.GetSessionValueFormat('NewJobRef'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,'Enter Job Number and press [TAB]') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateJobsStatus_' & p_web._nocolon('NewJobRef') & '_value')

Comment::NewJobRef  Routine
    loc:comment = p_web.Translate('Enter Job Number and press [TAB]')
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('NewJobRef') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::BrowseTagFile  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('BrowseTagFile',p_web.GetValue('NewValue'))
    do Value::BrowseTagFile
  Else
    p_web.StoreValue('tag:sessionID')
  End

Value::BrowseTagFile  Routine
  loc:extra = ''
  ! --- BROWSE ---  BrowseTagFile --
  p_web.SetValue('BrowseTagFile:NoForm',1)
  p_web.SetValue('BrowseTagFile:FormName',loc:formname)
  p_web.SetValue('BrowseTagFile:parentIs','Form')
  p_web.SetValue('_parentProc','UpdateJobsStatus')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('UpdateJobsStatus_BrowseTagFile_embedded_div')&'"><!-- Net:BrowseTagFile --></div><13,10>'
    p_web._DivHeader('UpdateJobsStatus_' & lower('BrowseTagFile') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('UpdateJobsStatus_' & lower('BrowseTagFile') & '_value')
  else
    packet = clip(packet) & '<!-- Net:BrowseTagFile --><13,10>'
  end
  do SendPacket

Comment::BrowseTagFile  Routine
    loc:comment = ''
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('BrowseTagFile') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateJobsStatus_' & p_web._nocolon('BrowseTagFile') & '_comment')

Validate::AllocationReport  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('AllocationReport',p_web.GetValue('NewValue'))
    do Value::AllocationReport
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      UpdateUniqueBitText(p_web)
      p_web.ssv('ReportPrinted','Y')
      
  do Value::AllocationReport
  do SendAlert
  do Value::ButtonFinish  !1

Value::AllocationReport  Routine
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('AllocationReport') & '_value',Choose(p_web.gsv('ReportRequired')='N','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.gsv('ReportRequired')='N')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''AllocationReport'',''updatejobsstatus_allocationreport_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','AllocationReport','Allocation Report','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('AllocationReport?' &'rnd=' & RANDOM(1,1000))) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateJobsStatus_' & p_web._nocolon('AllocationReport') & '_value')

Comment::AllocationReport  Routine
    loc:comment = ''
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('AllocationReport') & '_comment',Choose(p_web.gsv('ReportRequired')='N','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.gsv('ReportRequired')='N'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::ReportDisplay  Routine
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('ReportDisplay') & '_prompt',Choose(p_web.gsv('ReportRequired')='N','hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('WARNING:')
  If p_web.gsv('ReportRequired')='N'
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::ReportDisplay  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ReportDisplay',p_web.GetValue('NewValue'))
    do Value::ReportDisplay
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::ReportDisplay  Routine
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('ReportDisplay') & '_value',Choose(p_web.gsv('ReportRequired')='N','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.gsv('ReportRequired')='N')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.Translate('You must print the Allocation Report before you leave this page',) & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateJobsStatus_' & p_web._nocolon('ReportDisplay') & '_value')

Comment::ReportDisplay  Routine
    loc:comment = ''
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('ReportDisplay') & '_comment',Choose(p_web.gsv('ReportRequired')='N','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.gsv('ReportRequired')='N'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::ButtonCancel  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ButtonCancel',p_web.GetValue('NewValue'))
    do Value::ButtonCancel
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::ButtonCancel  Routine
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('ButtonCancel') & '_value',Choose(p_web.gsv('ReportRequired')='Y','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.gsv('ReportRequired')='Y')
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ButtonCancel','Cancel','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('SubmenuAllocations')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/pcancel.png',,,,'Cancel Process')

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateJobsStatus_' & p_web._nocolon('ButtonCancel') & '_value')

Comment::ButtonCancel  Routine
    loc:comment = ''
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('ButtonCancel') & '_comment',Choose(p_web.gsv('ReportRequired')='Y','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.gsv('ReportRequired')='Y'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::ButtonFinish  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ButtonFinish',p_web.GetValue('NewValue'))
    do Value::ButtonFinish
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  !finishing code here
  !clear all sessionvariables as we cancel the form
  p_web.ssv('SendingResult','')
  p_web.ssv('NewLocation','')
  p_web.ssv('NewEngineer','')
  p_web.ssv('NewSkillLevel','')
  p_web.ssv('NewStatus','')
  !p_web.ssv('FilterLocation','')
  p_web.ssv('NewJobRef','')
  p_web.ssv('NewEngineerName','')
  p_web.ssv('Hide_Location',FALSE)
  p_web.ssv('Hide_SkillLevel',FALSE)
  p_web.ssv('Hide_Status',FALSE)
  
  !and the local variables
  SendingResult = ''
  NewLocation = ''
  NewEngineer = ''
  NewSkillLevel = ''
  NewStatus = ''
  NewJobRef = ''
  NewEngineerName = ''
  Hide_Location = false 
  Hide_SkillLevel = false 
  Hide_Status = false 
  
  !clear out the tag file used to hold jobs updated
    ClearTagFile(p_web)
  !  LOOP
  !      Access:TagFile.clearkey(tag:keyTagged)
  !      tag:sessionID = p_web.SessionID
  !      set(tag:keyTagged,tag:keyTagged)
  !      if Access:TagFile.next() then break.
  !      if tag:sessionID <> p_web.SessionID then break.
  !      Access:TagFile.DeleteRecord(0)
  !  END !loop
  do Value::ButtonFinish
  do SendAlert

Value::ButtonFinish  Routine
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('ButtonFinish') & '_value',Choose(p_web.gsv('ReportRequired')='N' or p_web.gsv('ReportPrinted')='N','hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.gsv('ReportRequired')='N' or p_web.gsv('ReportPrinted')='N')
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''ButtonFinish'',''updatejobsstatus_buttonfinish_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ButtonFinish','Finish','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('SubmenuAllocations')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/psave.png',,,,'Return to Index Page')

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('UpdateJobsStatus_' & p_web._nocolon('ButtonFinish') & '_value')

Comment::ButtonFinish  Routine
    loc:comment = ''
  p_web._DivHeader('UpdateJobsStatus_' & p_web._nocolon('ButtonFinish') & '_comment',Choose(p_web.gsv('ReportRequired')='N' or p_web.gsv('ReportPrinted')='N','hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.gsv('ReportRequired')='N' or p_web.gsv('ReportPrinted')='N'
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('UpdateJobsStatus_NewSkillLevel_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::NewSkillLevel
      else
        do Value::NewSkillLevel
      end
  of lower('UpdateJobsStatus_NewLocation_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::NewLocation
      else
        do Value::NewLocation
      end
  of lower('UpdateJobsStatus_NewEngineer_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::NewEngineer
      else
        do Value::NewEngineer
      end
  of lower('UpdateJobsStatus_NewEngineer_comment')
    do Comment::NewEngineer
  of lower('UpdateJobsStatus_NewStatus_value')
      case p_web.GetValue('event')
      of event:selected !257
        do Value::NewStatus
      of event:accepted !1
        do Validate::NewStatus
      else
        do Value::NewStatus
      end
  of lower('UpdateJobsStatus_NewJobRef_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::NewJobRef
      else
        do Value::NewJobRef
      end
  of lower('UpdateJobsStatus_AllocationReport_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::AllocationReport
      else
        do Value::AllocationReport
      end
  of lower('UpdateJobsStatus_ButtonFinish_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::ButtonFinish
      else
        do Value::ButtonFinish
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('UpdateJobsStatus_form:ready_',1)
  p_web.SetSessionValue('UpdateJobsStatus_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_UpdateJobsStatus',0)

PreCopy  Routine
  p_web.SetValue('UpdateJobsStatus_form:ready_',1)
  p_web.SetSessionValue('UpdateJobsStatus_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_UpdateJobsStatus',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('UpdateJobsStatus_form:ready_',1)
  p_web.SetSessionValue('UpdateJobsStatus_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('UpdateJobsStatus:Primed',0)

PreDelete       Routine
  p_web.SetValue('UpdateJobsStatus_form:ready_',1)
  p_web.SetSessionValue('UpdateJobsStatus_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('UpdateJobsStatus:Primed',0)
  p_web.setsessionvalue('showtab_UpdateJobsStatus',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('UpdateJobsStatus_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('UpdateJobsStatus_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 4
    loc:InvalidTab += 1
  ! tab = 5
    loc:InvalidTab += 1
  ! tab = 6
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('UpdateJobsStatus:Primed',0)
  p_web.StoreValue('NewSkillLevel')
  p_web.StoreValue('NewLocation')
  p_web.StoreValue('NewEngineer')
  p_web.StoreValue('NewEngineerName')
  p_web.StoreValue('NewStatus')
  p_web.StoreValue('NewJobRef')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
endofpagespacer  Routine
  packet = clip(packet) & |
    '<<br/><13,10>'&|
    '<<br/><13,10>'&|
    '<<br/><13,10>'&|
    ''
UpdateJobDetails    PROCEDURE(*STRING pError)
    CODE
        LOOP 1 TIMES
            IF (p_web.GSV('NewJobRef') = '')
                pError = 'ERROR - You must give a job reference number'
                BREAK
            END ! IF
            
            IF (~NUMERIC(p_web.GSV('NewJobRef')))
                pError = 'ERROR - The job reference number '&clip(p_web.gsv('NewJobRef'))& ' must contain only numbers'
                BREAK
            END ! IF
            
            IF (INSTRING('.',p_web.GSV('NewJobRef'),1,1))
                pError = 'ERROR - The job reference number '&clip(p_web.gsv('NewJobRef'))& ' must not contain decimals'
                BREAK
            END ! IF
            
            IF (p_web.GSV('NewEngineer') = '')
                pError = 'ERROR - You must select an engineer.'
                BREAK
            END ! IF
            
            IF (p_web.GSV('Hide_Location') = FALSE)
                IF (p_web.GSV('NewLocation') = '')
                    pError = 'ERROR - You must select a new location for the job.' 
                    BREAK
                END ! IF
            END ! IF
            
            IF (p_web.GSV('Hide_SKillLevel') = FALSE)
                IF (p_web.GSV('NewSkillLevel') = '')
                    pError = 'ERROR - you must supply a new skill level for the job.' 
                    BREAK
                END !  IF
            END ! IF
            
            IF (p_web.GSV('Hide_Status') = FALSE)
                IF (p_web.GSV('NewStatus') = '')
                    pError = 'ERROR - You must give a new status for this job' 
                    BREAK
                END ! IF
            END ! IF
            
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = p_web.GSV('NewJobRef')
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                
            ELSE ! IF
                pError = 'ERROR - Unable to find job '&clip(p_web.gsv('NewJobRef'))& '. Please check Job Number and try again.'
                BREAK
            END ! IF
            
            Access:JOBSE.ClearKey(jobe:RefNumberKEy)
            jobe:RefNumber = job:Ref_Number
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKEy) = Level:Benign)
                
            ELSE ! IF
                pError = 'ERROR - Unable to find JOBS extention file '&clip(p_web.gsv('NewJobRef'))& ' , please check Job NUmber and try again.'
                BREAK
            END ! IF
            
            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                
            ELSE ! IF
                pError = 'ERROR - Unable to find WEBJOB record '&clip(p_web.gsv('NewJobRef'))& ' . Please check Job Number and try again.'
                BREAK
            END ! IF
            
            IF (wob:HeadAccountNumber <> p_web.GSV('BookingAccount'))
                pError = 'ERROR - The selected job '&clip(p_web.gsv('NewJobRef'))& ' was booked by a different RRC.'
                BREAK
            END ! IF
            
            IF (jobe:HubRepair)
                pError = 'ERROR - Cannot update '&clip(p_web.gsv('NewJobRef'))& ' . The selected job is not at the RRC.'
                BREAK
            END ! IF
            
            IF (job:Workshop <> 'YES')
                IF (p_web.GSV('NewEngineer') <> '') 
                    pError = 'ERROR - This job '&clip(p_web.gsv('NewJobRef'))& ' is not in the workshop. You cannot assign an engineer to it.'
                    BREAK
                END ! IF
                
                IF (p_web.GSV('NewLocation') <> '' AND p_web.GSV('Hide_Location') = FALSE)
                    pError = 'ERROR - This job '&clip(p_web.gsv('NewJobRef'))& ' is not in the workshop. You cannot assign an location to it.'
                    BREAK
                END ! IF                
            END ! IF
            
            IF (job:Date_Completed > 0 AND VodacomClass.JobSentToHub(job:Ref_Number) = 0) !jobe:HubRepairDate = '')
                pError = 'ERROR - The selected job '&clip(p_web.gsv('NewJobRef'))& ' has been completed'
                BREAK
            END ! IF
            
            IF (p_web.GSV('NewEngineer') = job:Engineer)
                pError = 'ERROR - The job '&clip(p_web.gsv('NewJobRef'))& ' is already allocated to the selected engineer'
                BREAK
            END ! IF
            
            GetStatus(SUB(p_web.GSV('NewStatus'),1,3),1,'JOB')
            
            ! Update Engineer
            IF (job:Workshop = 'YES')
                IF (p_web.GSV('NewEngineer') <> '')
                    IF (job:Engineer <> '')
                        ! Lookup current engineer and escalate
                        Access:JOBSENG.ClearKey(joe:UserCodeKey)
                        joe:JobNumber = job:Ref_Number
                        joe:UserCode = job:Engineer
                        joe:DateAllocated = TODAY()
                        SET(joe:UserCodeKey,joe:UserCodeKey)
                        LOOP UNTIL Access:JOBSENG.Previous() <> Level:Benign
                            IF (joe:JobNumber <> job:Ref_Number OR |
                                joe:UserCode <> job:Engineer OR |
                                joe:DateAllocated > TODAY())
                                BREAK
                            END ! IF
                            IF (joe:Status = 'ALLOCATED')
                                joe:Status = 'ESCALATED'
                                joe:StatusDate = TODAY()
                                joe:StatusTime = CLOCK()
                                Access:JOBSENG.TryUpdate()
                            END ! IF
                            BREAK
                        END ! LOOP
                    ELSE ! IF
                        Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
                        jobe2:RefNumber = job:Ref_Number    
                        IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
                            IF jobe2:SMSNotification
                                AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'2ENG','SMS',jobe2:SMSAlertNumber,'',0,'')
                            END ! If jobe2:SMSNotification

                            IF jobe2:EmailNotification
                                AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'2ENG','EMAIL','',jobe2:EmailAlertAddress,0,'')
                            END ! If jobe2:EmailNotification                            
                        ELSE ! IF
                        END ! IF
                    END! IF
                    
                    job:Engineer = p_web.GSV('NewEngineer')
                    
                    Access:USERS.ClearKey(use:User_Code_Key)
                    use:User_Code = job:Engineer
                    IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
                        
                    ELSE ! IF
                    END ! IF
                    
                    AddToAudit(p_web,job:Ref_Number,'JOB','ENGINEER ALLOCATED: ' & Clip(use:User_Code), |
                Clip(use:Forename) & ' ' & Clip(use:Surname) & ' - ' & Clip(use:Location) & ' - LEVEL ' & Clip(use:SkillLevel))
                
                    If Access:JOBSENG.PrimeRecord() = Level:Benign
                        joe:JobNumber     = p_web.gsv('NewJobRef')      !job:Ref_Number
                        joe:UserCode      = p_web.gsv('NewEngineer')    !job:Engineer
                        joe:DateAllocated = Today()
                        joe:TimeAllocated = Clock()
                        joe:AllocatedBy   = p_web.GSV('BookingUserCode')
                        access:users.clearkey(use:User_Code_Key)
                        use:User_Code   = p_web.gsv('NewEngineer')      !job:Engineer
                        access:users.fetch(use:User_Code_Key)

                        joe:EngSkillLevel = use:SkillLevel
                        joe:JobSkillLevel = jobe:SkillLevel
                        !if jobe:HubRepairDate = '' THEN
                        IF (VodacomClass.JobSentToHub(job:Ref_Number) = 0)
                            joe:Status = 'ALLOCATED'        !this has never been to the ARC
                        ELSE
                            joe:Status = 'ENGINEER QA'      !this has been to the ARC
                        END            
                        joe:StatusDate  = Today()
                        joe:StatusTime  = Clock()            
                        Access:JOBSENG.Insert()
                                    
                    End !If Access:JOBSENG.PrimeRecord() = Level:Benign
                
                END ! IF
                
                ! Update Skill Level?
                IF (p_web.GSV('Hide_SkillLevel') = FALSE)
                    jobe:SkillLevel = p_web.GSV('NewSkillLevel')
                END ! IF
                
                ! Update Location
                IF (p_web.GSV('Hide_Location') = FALSE)
                    IF (p_web.GSV('NewLocation') <> '')
                        Access:LOCINTER.ClearKey(loi:Location_Key)
                        loi:Location = job:Location
                        IF (Access:LOCINTER.TryFetch(loi:Location_Key) = Level:Benign)
                            IF (loi:Allocate_Spaces = 'YES')
                                loi:Current_Spaces += 1
                                loi:Location_Available = 'YES'
                                Access:LOCINTER.TryUpdate()
                            END ! IF
                        ELSE ! IF
                        END ! IF
                        
                        IF (Access:LOCATLOG.PrimeRecord() = Level:Benign)
                            lot:RefNumber = job:Ref_Number
                            lot:UserCode = p_web.GSV('BookingUserCode')
                            lot:PreviousLocation = job:Location
                            lot:NewLocation = p_web.GSV('NewLocation')
                            lot:TheDate = TODAY()
                            lot:TheTime = CLOCK()
                            IF (Access:LOCATLOG.TryInsert())
                                Access:LOCATLOG.CancelAutoInc()
                            END ! IF
                        END ! IF
                        
                        job:Location = p_web.GSV('NewLocation')
                        
                        Access:LOCINTER.ClearKey(loi:Location_Key)
                        loi:LOcation = p_web.GSV('NewLocation')
                        IF (Access:LOCINTER.TryFetch(loi:Location_Key) = Level:Benign)
                            IF (loi:Allocate_Spaces = 'YES')
                                loi:Current_Spaces -= 1
                                IF (loi:Current_Spaces < 0)
                                    loi:Current_Spaces = 0
                                    loi:Location_Available = 'NO'
                                END ! IF
                                Access:LOCINTER.TryUpdate()
                            END ! IF
                        ELSE ! IF
                        END ! IF
                    END ! IF
                END ! IF
                
            END ! IF
            
            IF (Access:JOBS.TryUpdate())
                pError = 'ERROR - Unable to update the job. Please try again.'
                BREAK
            END ! IF
            IF (Access:JOBSE.TryUpdate())
            END ! IF
            
            IF (Access:TagFile.PrimeRecord() = Level:Benign)
                tag:SessionID   = p_web.SessionID
                tag:TaggedValue = p_web.GSV('NewJobRef')
                tag:Tagged = 1
                IF (Access:TagFile.TryInsert())
                    Access:TagFile.CancelAutoInc()
                END ! IF
            END ! IF
            
            p_web.SSV('NewJobRef','')
            p_web.SSV('ReportRequired','Y')
            
        END ! Break Loop
