

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER516.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER010.INC'),ONCE        !Req'd for module callout resolution
                     END


VodacomPortalExportCriteria PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locCriteriaStartDate DATE                                  !
locCriteriaEndDate   DATE                                  !
locIncludeReceivedJobs LONG                                !
locIncludeNotReceivedJobs LONG                             !
FilesOpened     Long
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('VodacomPortalExportCriteria')
  loc:formname = 'VodacomPortalExportCriteria_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'VodacomPortalExportCriteria',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('VodacomPortalExportCriteria','')
    p_web._DivHeader('VodacomPortalExportCriteria',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferVodacomPortalExportCriteria',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferVodacomPortalExportCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferVodacomPortalExportCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_VodacomPortalExportCriteria',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferVodacomPortalExportCriteria',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_VodacomPortalExportCriteria',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'VodacomPortalExportCriteria',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('VodacomPortalExportCriteria_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('locCriteriaStartDate')
    p_web.SetPicture('locCriteriaStartDate','@d06b')
  End
  p_web.SetSessionPicture('locCriteriaStartDate','@d06b')
  If p_web.IfExistsValue('locCriteriaEndDate')
    p_web.SetPicture('locCriteriaEndDate','@d06b')
  End
  p_web.SetSessionPicture('locCriteriaEndDate','@d06b')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locCriteriaStartDate',locCriteriaStartDate)
  p_web.SetSessionValue('locCriteriaEndDate',locCriteriaEndDate)
  p_web.SetSessionValue('locIncludeReceivedJobs',locIncludeReceivedJobs)
  p_web.SetSessionValue('locIncludeNotReceivedJobs',locIncludeNotReceivedJobs)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locCriteriaStartDate')
    locCriteriaStartDate = p_web.dformat(clip(p_web.GetValue('locCriteriaStartDate')),'@d06b')
    p_web.SetSessionValue('locCriteriaStartDate',locCriteriaStartDate)
  End
  if p_web.IfExistsValue('locCriteriaEndDate')
    locCriteriaEndDate = p_web.dformat(clip(p_web.GetValue('locCriteriaEndDate')),'@d06b')
    p_web.SetSessionValue('locCriteriaEndDate',locCriteriaEndDate)
  End
  if p_web.IfExistsValue('locIncludeReceivedJobs')
    locIncludeReceivedJobs = p_web.GetValue('locIncludeReceivedJobs')
    p_web.SetSessionValue('locIncludeReceivedJobs',locIncludeReceivedJobs)
  End
  if p_web.IfExistsValue('locIncludeNotReceivedJobs')
    locIncludeNotReceivedJobs = p_web.GetValue('locIncludeNotReceivedJobs')
    p_web.SetSessionValue('locIncludeNotReceivedJobs',locIncludeNotReceivedJobs)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('VodacomPortalExportCriteria_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    IF (p_web.IfExistsValue('firsttime') )
        p_web.SSV('locCriteriaStartDate',DATE(MONTH(TODAY()),1,YEAR(TODAY())))
        p_web.SSV('locCriteriaEndDate',TODAY())
        p_web.SSV('locIncludeReceivedJobs',1)
        p_web.SSV('locIncludeNotReceivedJobs',1)
    END ! IF
    
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locCriteriaStartDate = p_web.RestoreValue('locCriteriaStartDate')
 locCriteriaEndDate = p_web.RestoreValue('locCriteriaEndDate')
 locIncludeReceivedJobs = p_web.RestoreValue('locIncludeReceivedJobs')
 locIncludeNotReceivedJobs = p_web.RestoreValue('locIncludeNotReceivedJobs')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'FormPreJobCriteria'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('VodacomPortalExportCriteria_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('VodacomPortalExportCriteria_ChainTo')
    loc:formaction = p_web.GetSessionValue('VodacomPortalExportCriteria_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'FormPreJobCriteria'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="VodacomPortalExportCriteria" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="VodacomPortalExportCriteria" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="VodacomPortalExportCriteria" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Vodacom Portal Booking') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Vodacom Portal Booking',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_VodacomPortalExportCriteria">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_VodacomPortalExportCriteria" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_VodacomPortalExportCriteria')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Criteria') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_VodacomPortalExportCriteria')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_VodacomPortalExportCriteria'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locCriteriaStartDate')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_VodacomPortalExportCriteria')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Criteria') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_VodacomPortalExportCriteria_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Criteria')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCriteriaStartDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCriteriaStartDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCriteriaStartDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::hidden
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCriteriaEndDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCriteriaEndDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCriteriaEndDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIncludeReceivedJobs
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIncludeReceivedJobs
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locIncludeReceivedJobs
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIncludeNotReceivedJobs
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIncludeNotReceivedJobs
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locIncludeNotReceivedJobs
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&250&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnCreateExport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnCreateExport
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locCriteriaStartDate  Routine
  p_web._DivHeader('VodacomPortalExportCriteria_' & p_web._nocolon('locCriteriaStartDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Start Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locCriteriaStartDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCriteriaStartDate',p_web.GetValue('NewValue'))
    locCriteriaStartDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCriteriaStartDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCriteriaStartDate',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    locCriteriaStartDate = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  do Value::locCriteriaStartDate
  do SendAlert

Value::locCriteriaStartDate  Routine
  p_web._DivHeader('VodacomPortalExportCriteria_' & p_web._nocolon('locCriteriaStartDate') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locCriteriaStartDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locCriteriaStartDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locCriteriaStartDate'',''vodacomportalexportcriteria_loccriteriastartdate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locCriteriaStartDate',p_web.GetSessionValue('locCriteriaStartDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
   packet = CLIP(packet) & |
     '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar' & |
     '(locCriteriaStartDate,''dd/mm/yyyy'',this); Date.disabled=true;' & |
     'sv(''...'',''VodacomPortalExportCriteria_pickdate_value'',1,FieldValue(this,1));nextFocus(VodacomPortalExportCriteria_frm,'''',0);" ' & |
     'value="Select Date" name="Date" type="button">...</button>'
   DO SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('VodacomPortalExportCriteria_' & p_web._nocolon('locCriteriaStartDate') & '_value')

Comment::locCriteriaStartDate  Routine
      loc:comment = ''
  p_web._DivHeader('VodacomPortalExportCriteria_' & p_web._nocolon('locCriteriaStartDate') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::hidden  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden',p_web.GetValue('NewValue'))
    do Value::hidden
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden  Routine
  p_web._DivHeader('VodacomPortalExportCriteria_' & p_web._nocolon('hidden') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::hidden  Routine
    loc:comment = ''
  p_web._DivHeader('VodacomPortalExportCriteria_' & p_web._nocolon('hidden') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locCriteriaEndDate  Routine
  p_web._DivHeader('VodacomPortalExportCriteria_' & p_web._nocolon('locCriteriaEndDate') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('End Date')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locCriteriaEndDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCriteriaEndDate',p_web.GetValue('NewValue'))
    locCriteriaEndDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCriteriaEndDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCriteriaEndDate',p_web.dFormat(p_web.GetValue('Value'),'@d06b'))
    locCriteriaEndDate = p_web.Dformat(p_web.GetValue('Value'),'@d06b') !
  End
  do Value::locCriteriaEndDate
  do SendAlert

Value::locCriteriaEndDate  Routine
  p_web._DivHeader('VodacomPortalExportCriteria_' & p_web._nocolon('locCriteriaEndDate') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locCriteriaEndDate
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locCriteriaEndDate')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locCriteriaEndDate'',''vodacomportalexportcriteria_loccriteriaenddate_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locCriteriaEndDate',p_web.GetSessionValue('locCriteriaEndDate'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@d06b',loc:javascript,,) & '<13,10>'
  do SendPacket
   packet = CLIP(packet) & |
     '<button id="lookup_btn" class="LookupButton" onclick="displayCalendar' & |
     '(locCriteriaEndDate,''dd/mm/yyyy'',this); Date.disabled=true;' & |
     'sv(''...'',''VodacomPortalExportCriteria_pickdate_value'',1,FieldValue(this,1));nextFocus(VodacomPortalExportCriteria_frm,'''',0);" ' & |
     'value="Select Date" name="Date" type="button">...</button>'
   DO SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('VodacomPortalExportCriteria_' & p_web._nocolon('locCriteriaEndDate') & '_value')

Comment::locCriteriaEndDate  Routine
      loc:comment = ''
  p_web._DivHeader('VodacomPortalExportCriteria_' & p_web._nocolon('locCriteriaEndDate') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locIncludeReceivedJobs  Routine
  p_web._DivHeader('VodacomPortalExportCriteria_' & p_web._nocolon('locIncludeReceivedJobs') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Include Received Jobs')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIncludeReceivedJobs  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIncludeReceivedJobs',p_web.GetValue('NewValue'))
    locIncludeReceivedJobs = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIncludeReceivedJobs
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locIncludeReceivedJobs',p_web.GetValue('Value'))
    locIncludeReceivedJobs = p_web.GetValue('Value')
  End
  do Value::locIncludeReceivedJobs
  do SendAlert
  do Value::btnCreateExport  !1

Value::locIncludeReceivedJobs  Routine
  p_web._DivHeader('VodacomPortalExportCriteria_' & p_web._nocolon('locIncludeReceivedJobs') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locIncludeReceivedJobs
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locIncludeReceivedJobs'',''vodacomportalexportcriteria_locincludereceivedjobs_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locIncludeReceivedJobs')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locIncludeReceivedJobs') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locIncludeReceivedJobs',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('VodacomPortalExportCriteria_' & p_web._nocolon('locIncludeReceivedJobs') & '_value')

Comment::locIncludeReceivedJobs  Routine
    loc:comment = ''
  p_web._DivHeader('VodacomPortalExportCriteria_' & p_web._nocolon('locIncludeReceivedJobs') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locIncludeNotReceivedJobs  Routine
  p_web._DivHeader('VodacomPortalExportCriteria_' & p_web._nocolon('locIncludeNotReceivedJobs') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Include Not Received Jobs')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIncludeNotReceivedJobs  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIncludeNotReceivedJobs',p_web.GetValue('NewValue'))
    locIncludeNotReceivedJobs = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIncludeNotReceivedJobs
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locIncludeNotReceivedJobs',p_web.GetValue('Value'))
    locIncludeNotReceivedJobs = p_web.GetValue('Value')
  End
  do Value::locIncludeNotReceivedJobs
  do SendAlert
  do Value::btnCreateExport  !1

Value::locIncludeNotReceivedJobs  Routine
  p_web._DivHeader('VodacomPortalExportCriteria_' & p_web._nocolon('locIncludeNotReceivedJobs') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locIncludeNotReceivedJobs
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locIncludeNotReceivedJobs'',''vodacomportalexportcriteria_locincludenotreceivedjobs_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locIncludeNotReceivedJobs')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locIncludeNotReceivedJobs') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locIncludeNotReceivedJobs',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('VodacomPortalExportCriteria_' & p_web._nocolon('locIncludeNotReceivedJobs') & '_value')

Comment::locIncludeNotReceivedJobs  Routine
    loc:comment = ''
  p_web._DivHeader('VodacomPortalExportCriteria_' & p_web._nocolon('locIncludeNotReceivedJobs') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnCreateExport  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnCreateExport',p_web.GetValue('NewValue'))
    do Value::btnCreateExport
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnCreateExport  Routine
  p_web._DivHeader('VodacomPortalExportCriteria_' & p_web._nocolon('btnCreateExport') & '_value',Choose(p_web.GSV('locIncludeReceivedJobs') = 0 AND p_web.GSV('locIncludeNotReceivedJobs') = 0,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('locIncludeReceivedJobs') = 0 AND p_web.GSV('locIncludeNotReceivedJobs') = 0)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnCreateExport','Create Export','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('PageProcess?' &'ProcessType=PreJobExport')) & ''','''&clip('_blank')&''')',loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('VodacomPortalExportCriteria_' & p_web._nocolon('btnCreateExport') & '_value')

Comment::btnCreateExport  Routine
    loc:comment = ''
  p_web._DivHeader('VodacomPortalExportCriteria_' & p_web._nocolon('btnCreateExport') & '_comment',Choose(p_web.GSV('locIncludeReceivedJobs') = 0 AND p_web.GSV('locIncludeNotReceivedJobs') = 0,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('locIncludeReceivedJobs') = 0 AND p_web.GSV('locIncludeNotReceivedJobs') = 0
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('VodacomPortalExportCriteria_locCriteriaStartDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locCriteriaStartDate
      else
        do Value::locCriteriaStartDate
      end
  of lower('VodacomPortalExportCriteria_locCriteriaEndDate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locCriteriaEndDate
      else
        do Value::locCriteriaEndDate
      end
  of lower('VodacomPortalExportCriteria_locIncludeReceivedJobs_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIncludeReceivedJobs
      else
        do Value::locIncludeReceivedJobs
      end
  of lower('VodacomPortalExportCriteria_locIncludeNotReceivedJobs_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIncludeNotReceivedJobs
      else
        do Value::locIncludeNotReceivedJobs
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('VodacomPortalExportCriteria_form:ready_',1)
  p_web.SetSessionValue('VodacomPortalExportCriteria_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_VodacomPortalExportCriteria',0)

PreCopy  Routine
  p_web.SetValue('VodacomPortalExportCriteria_form:ready_',1)
  p_web.SetSessionValue('VodacomPortalExportCriteria_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_VodacomPortalExportCriteria',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('VodacomPortalExportCriteria_form:ready_',1)
  p_web.SetSessionValue('VodacomPortalExportCriteria_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('VodacomPortalExportCriteria:Primed',0)

PreDelete       Routine
  p_web.SetValue('VodacomPortalExportCriteria_form:ready_',1)
  p_web.SetSessionValue('VodacomPortalExportCriteria_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('VodacomPortalExportCriteria:Primed',0)
  p_web.setsessionvalue('showtab_VodacomPortalExportCriteria',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
          If p_web.IfExistsValue('locIncludeReceivedJobs') = 0
            p_web.SetValue('locIncludeReceivedJobs',0)
            locIncludeReceivedJobs = 0
          End
          If p_web.IfExistsValue('locIncludeNotReceivedJobs') = 0
            p_web.SetValue('locIncludeNotReceivedJobs',0)
            locIncludeNotReceivedJobs = 0
          End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('VodacomPortalExportCriteria_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('VodacomPortalExportCriteria_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('VodacomPortalExportCriteria:Primed',0)
  p_web.StoreValue('locCriteriaStartDate')
  p_web.StoreValue('')
  p_web.StoreValue('locCriteriaEndDate')
  p_web.StoreValue('locIncludeReceivedJobs')
  p_web.StoreValue('locIncludeNotReceivedJobs')
  p_web.StoreValue('')
