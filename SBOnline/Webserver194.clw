

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER194.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! Report the JOBS File
!!! </summary>
QAFailed PROCEDURE (NetWebServerWorker p_web)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
tmp:DefaultTelephone STRING(20)                            !
tmp:DefaultFax       STRING(20)                            !
tmp:PrintedBy        STRING(60)                            !
code_temp            BYTE                                  !
option_temp          BYTE                                  !
bar_code_string_temp CSTRING(21)                           !
bar_code_temp        CSTRING(21)                           !
qa_reason_temp       STRING(255)                           !
engineer_temp        STRING(60)                            !
jobNumber            LONG                                  !
Process:View         VIEW(JOBS)
                       PROJECT(job:Charge_Type)
                       PROJECT(job:ESN)
                       PROJECT(job:MSN)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Repair_Type)
                       PROJECT(job:Repair_Type_Warranty)
                       PROJECT(job:Third_Party_Site)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:Warranty_Charge_Type)
                       PROJECT(job:date_booked)
                       PROJECT(job:who_booked)
                     END
ProgressWindow       WINDOW('Report JOBS'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT,AT(406,1042,7521,9781),PRE(rpt),PAPER(PAPER:A4),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(0,0,,583),USE(?unnamed)
                         STRING('3rd Party Repair'),AT(2604,0),USE(?Third_Party_Repair),FONT(,22,,FONT:bold),HIDE,TRN
                       END
Detail                 DETAIL,AT(0,0,,9115),USE(?detailband)
                         STRING('Job Number:'),AT(313,208),USE(?String2),FONT(,20,,FONT:bold,CHARSET:ANSI),TRN
                         STRING(@s20),AT(2917,313,1771,208),USE(bar_code_temp),FONT('C39 High 12pt LJ3',12,,FONT:regular, |
  CHARSET:ANSI),LEFT
                         STRING(@s8),AT(4792,313),USE(job:Ref_Number,,?JOB_ALI:Ref_Number:2),FONT(,12,,FONT:bold),LEFT, |
  TRN
                         STRING('Unit Details:'),AT(313,656),USE(?String2:2),FONT(,20,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('Date Booked:'),AT(2917,760),USE(?String6),FONT(,12,,FONT:bold),TRN
                         STRING(@d6b),AT(4792,760),USE(job:date_booked),FONT(,12,,,CHARSET:ANSI),LEFT
                         STRING(@s3),AT(5781,760),USE(job:who_booked),FONT(,12)
                         STRING('Manufacturer:'),AT(2917,1073),USE(?String6:2),FONT(,12,,FONT:bold),TRN
                         STRING(@s30),AT(4792,1073),USE(job:Manufacturer),FONT(,12,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Model Number:'),AT(2917,1385),USE(?String6:3),FONT(,12,,FONT:bold),TRN
                         STRING(@s30),AT(4792,1385),USE(job:Model_Number),FONT(,12,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Unit Type:'),AT(2917,1698),USE(?String6:4),FONT(,12,,FONT:bold),TRN
                         STRING(@s30),AT(4792,1698),USE(job:Unit_Type),FONT(,12,,,CHARSET:ANSI),LEFT,TRN
                         STRING('I.M.E.I. Number:'),AT(2917,2010),USE(?String6:5),FONT(,12,,FONT:bold),TRN
                         STRING(@s16),AT(4792,2010),USE(job:ESN),FONT(,12,,,CHARSET:ANSI),LEFT,TRN
                         STRING('M.S.N.'),AT(2917,2323),USE(?String6:6),FONT(,12,,FONT:bold),TRN
                         STRING(@s16),AT(4792,2323),USE(job:MSN),FONT(,12,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Chargeable Type:'),AT(2917,2740),USE(?String6:7),FONT(,12,,FONT:bold),TRN
                         STRING(@s30),AT(4792,2740),USE(job:Charge_Type),FONT(,12,,,CHARSET:ANSI),LEFT,TRN
                         STRING(@s30),AT(4792,3000),USE(job:Repair_Type),FONT(,12,,,CHARSET:ANSI),LEFT
                         STRING('Repair Type:'),AT(2917,3000),USE(?String6:8),FONT(,12,,FONT:bold),TRN
                         STRING(@s30),AT(4792,3281),USE(job:Warranty_Charge_Type),FONT(,12,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Warranty Type:'),AT(2917,3281),USE(?String6:9),FONT(,12,,FONT:bold),TRN
                         STRING('Repair Type:'),AT(2917,3542),USE(?String6:10),FONT(,12,,FONT:bold),TRN
                         STRING(@s30),AT(4792,3542),USE(job:Repair_Type_Warranty),FONT(,12,,,CHARSET:ANSI),LEFT
                         STRING(@s60),AT(4792,4552),USE(tmp:PrintedBy),FONT(,12)
                         STRING(@s60),AT(4792,4292),USE(engineer_temp),FONT(,12),TRN
                         STRING(@s30),AT(4792,3927),USE(job:Third_Party_Site),FONT(,12),HIDE,TRN
                         STRING('3rd Party Agent:'),AT(2917,3927),USE(?Third_Party_Agent),FONT(,12,,FONT:bold),HIDE, |
  TRN
                         STRING('Original Engineer:'),AT(2917,4292),USE(?String6:13),FONT(,12,,FONT:bold),TRN
                         STRING('Inspector:'),AT(2917,4552),USE(?String6:11),FONT(,12,,FONT:bold),TRN
                         STRING('Failure Reason:'),AT(2917,4813),USE(?String6:12),FONT(,12,,FONT:bold),TRN
                         TEXT,AT(4792,4844,2552,4167),USE(qa_reason_temp),FONT(,8)
                         STRING('Repair Details:'),AT(260,2688),USE(?String2:3),FONT(,20,,FONT:bold,CHARSET:ANSI),TRN
                         STRING('QA Details:'),AT(313,4292),USE(?String2:4),FONT(,20,,FONT:bold,CHARSET:ANSI),TRN
                       END
                       FORM,AT(396,479,7521,10552),USE(?unnamed:3)
                         STRING('QA FAILURE'),AT(2031,52,3177,417),USE(?string20),FONT(,24,,FONT:bold),CENTER,TRN
                         BOX,AT(104,521,7292,9844),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(1)
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR3               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR3:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('QAFailed')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:AUDIT.SetOpenRelated()
  Relate:AUDIT.Open                                        ! File AUDIT used by this procedure, so make sure it's RelationManager is open
  Relate:TagFile.Open                                      ! File TagFile used by this procedure, so make sure it's RelationManager is open
  Access:JOBACC.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:AUDIT2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
        jobNumber = p_web.GSV('job:Ref_Number')        
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('QAFailed',ProgressWindow)                  ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisReport.AddSortOrder(job:Ref_Number_Key)
  ThisReport.AddRange(job:Ref_Number,jobNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:TagFile.Close
  END
  IF SELF.Opened
    INIMgr.Update('QAFailed',ProgressWindow)               ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'QAFailed',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.Init(Report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,Report{PROPPRINT:Paper},PAPER:USER), CHOOSE(Report{PROP:Thous}=True,PROP:Thous,CHOOSE(Report{PROP:MM}=True,PROP:MM,CHOOSE(Report{PROP:Points}=True,PROP:Points,0))), Report{PROPPRINT:PaperWidth}, Report{PROPPRINT:PaperHeight}, Report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetTitle('QA Failure Report')        !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR3.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR3:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR3:vpf = BOR(PDFXTR3:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetViewerPrefs(PDFXTR3:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR3.SetFontEmbedding(True, True, False, False, False)
    PDFXTR3.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'QAFailed',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR3.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  !barcode bit
        bar_Code_Temp = '*' & CLIP(job:Ref_Number) & '*'
  
  !        ! #13445 Get the rejection reason from the audit trail entry. (DBH: 17/12/2014)
  !        Access:AUDIT.ClearKey(aud:Ref_Number_Key)
  !        aud:Ref_Number = job:Ref_Number
  !        SET(aud:Ref_Number_Key,aud:Ref_Number_Key)
  !        LOOP UNTIL Access:AUDIT.Next() <> Level:Benign
  !            IF (aud:Ref_Number <> job:Ref_Number)
  !                BREAK
  !            END ! IF
  !            IF (INSTRING('QA REJECTION',UPPER(aud:Action),1,1))
  !                Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKEy)
  !                aud2:AUDRecordNumber = aud:Record_Number    
  !                IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKEy) = Level:Benign)
  !                    rejStart# = INSTRING(':',aud2:Notes,1,1)
  !                    qa_Reason_Temp = CLIP(SUB(aud2:Notes,rejStart# + 2,255))
  !                    BREAK
  !                END ! IF
  !            END ! IF
  !        END ! LOOP
        IF (p_web.GSV('FailReason') = 'ACCESSORIES MISSING')
            qa_Reason_Temp = 'ACCESSORIES BOOKED IN:-'
            Access:JOBACC.ClearKey(jac:Ref_Number_Key)
            jac:Ref_Number = p_web.GSV('job:Ref_Number')
            SET(jac:Ref_Number_Key,jac:Ref_Number_Key)
            LOOP UNTIL Access:JOBACC.Next() <> Level:Benign
                IF (jac:Ref_Number <> p_web.GSV('job:Ref_Number'))
                    BREAK
                END ! IF
                qa_Reason_Temp = CLIP(qa_Reason_Temp) & '<13,10>' & CLIP(jac:Accessory)
            END ! LOOP
  
            qa_Reason_Temp = CLIP(qa_reason_temp) & '<13,10,13,10>ACCESSORIES BOOKED OUT:-'
  
            Access:TagFile.ClearKey(tag:keyTagged)
            tag:sessionID = p_web.SessionID
            SET(tag:keyTagged,tag:keyTagged)
            LOOP UNTIL Access:TagFile.Next() <> Level:Benign
                IF (tag:sessionID <> p_web.SessionID)
                    BREAK
                END ! IF
                IF (tag:tagged = 1)
                    qa_Reason_Temp = CLIP(qa_Reason_Temp) & '<13,10>' & CLIP(tag:taggedValue)
                END ! IF
            END ! LOOP
  
  !            LOOP x# = 1 TO 1000
  !                IF (SUB(p_web.GSV('FoundAccessories'),x#,2) = '|;')
  !                    start# = x# + 2
  !                    CYCLE
  !                END ! IF
  !  
  !                IF (start# > 0)
  !                    IF (SUB(p_web.GSV('FoundAccessories'),x#,2) = ';|')
  !                        qa_Reason_Temp = CLIP(qa_Reason_Temp) & '<13,10>' & SUB(p_web.GSV('FoundAccessories'),start#,x# - start#)
  !                        start# = 0
  !                    END ! IF
  !                END ! IF
  !  
  !            END ! LOOP
  !            IF (start# > 0)
  !                qa_Reason_Temp = CLIP(qa_Reason_Temp) & '<13,10>' & SUB(p_web.GSV('FoundAccessories'),start#,30)
  !            END ! IF
  
            qa_reason_temp = CLIP(qa_reason_temp) & '<13,10,13,10>' & p_web.GSV('FailNotes')
  
  
        ELSE ! 
            qa_reason_temp = p_web.GSV('FailReason') ! Got from QA Failure Reason
        END ! IF
        
        qa_reason_temp = UPPER(qa_reason_temp)
  
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = job:Engineer
        IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            engineer_Temp = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
        END ! IF
  
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = p_web.GSV('BookingUserCode')
        IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            tmp:PrintedBy = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
        END ! IF
  
  !   Looks like this was never called, as the original used the jobs file, not the alias file        
  !        If job_ali:third_party_site <> ''
  !            Settarget(report)
  !            Unhide(?third_party_repair)
  !            Unhide(?third_party_agent)
  !            Unhide(?job:third_party_site)
  !            Settarget()
  !        End!If job_ali:third_party_site <> ''
  ReturnValue = PARENT.TakeRecord()
  PRINT(rpt:Detail)
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR3:rtn = PDFXTR3.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR3:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

