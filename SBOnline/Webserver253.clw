

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER253.INC'),ONCE        !Local module procedure declarations
                     END


WaybillDeleteFromProcessing PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locReason            STRING(255)                           !
locStatus            STRING(30)                            !
locPreviousStatus    STRING(30)                            !
FilesOpened     Long
WEBJOB::State  USHORT
WAYBPRO::State  USHORT
STATUS::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
WAYBAWT::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
locStatus_OptionView   View(STATUS)
                          Project(sts:Status)
                          Project(sts:Status)
                        End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('WaybillDeleteFromProcessing')
  loc:formname = 'WaybillDeleteFromProcessing_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'WaybillDeleteFromProcessing',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('WaybillDeleteFromProcessing','')
    p_web._DivHeader('WaybillDeleteFromProcessing',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferWaybillDeleteFromProcessing',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWaybillDeleteFromProcessing',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWaybillDeleteFromProcessing',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_WaybillDeleteFromProcessing',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferWaybillDeleteFromProcessing',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_WaybillDeleteFromProcessing',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'WaybillDeleteFromProcessing',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(WAYBPRO)
  p_web._OpenFile(STATUS)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(WAYBAWT)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(WAYBPRO)
  p_Web._CloseFile(STATUS)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WAYBAWT)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
        p_web.SSV('locStatus','')
        p_web.SSV('locReason','')
        ! Delete From Processing Browse
        IF (p_web.IfExistsValue('wayID'))
            p_web.StoreValue('wayID')
        END ! IF
        
        ! Delete From Processed Browse
        IF (p_web.IfExistsValue('wayType'))
            p_web.StoreValue('wayType')
        END ! IF
        
  p_web.SetValue('WaybillDeleteFromProcessing_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locStatus',locStatus)
  p_web.SetSessionValue('locReason',locReason)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locStatus')
    locStatus = p_web.GetValue('locStatus')
    p_web.SetSessionValue('locStatus',locStatus)
  End
  if p_web.IfExistsValue('locReason')
    locReason = p_web.GetValue('locReason')
    p_web.SetSessionValue('locReason',locReason)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('WaybillDeleteFromProcessing_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
      p_web.site.SaveButton.TextValue = 'Remove'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locStatus = p_web.RestoreValue('locStatus')
 locReason = p_web.RestoreValue('locReason')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'WaybillGeneration'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('WaybillDeleteFromProcessing_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('WaybillDeleteFromProcessing_ChainTo')
    loc:formaction = p_web.GetSessionValue('WaybillDeleteFromProcessing_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'WaybillGeneration'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="WaybillDeleteFromProcessing" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="WaybillDeleteFromProcessing" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="WaybillDeleteFromProcessing" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Remove Job From Waybill Processing') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Remove Job From Waybill Processing',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_WaybillDeleteFromProcessing">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_WaybillDeleteFromProcessing" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_WaybillDeleteFromProcessing')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Select New Status For Removed Job') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_WaybillDeleteFromProcessing')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_WaybillDeleteFromProcessing'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
        p_web.SetValue('SelectField',clip(loc:formname) & '.locStatus')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_WaybillDeleteFromProcessing')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Select New Status For Removed Job') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_WaybillDeleteFromProcessing_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select New Status For Removed Job')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Select New Status For Removed Job')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select New Status For Removed Job')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Select New Status For Removed Job')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::txtWarning
      do Comment::txtWarning
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locStatus
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locStatus
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&180&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locReason
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&220&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locReason
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Validate::txtWarning  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtWarning',p_web.GetValue('NewValue'))
    do Value::txtWarning
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtWarning  Routine
  p_web._DivHeader('WaybillDeleteFromProcessing_' & p_web._nocolon('txtWarning') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    p_web.Translate('Note: Job will be removed from Waybill Processing, and placed back in RRC Control',) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::txtWarning  Routine
    loc:comment = ''
  p_web._DivHeader('WaybillDeleteFromProcessing_' & p_web._nocolon('txtWarning') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locStatus  Routine
  p_web._DivHeader('WaybillDeleteFromProcessing_' & p_web._nocolon('locStatus') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('New Status')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locStatus  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locStatus',p_web.GetValue('NewValue'))
    locStatus = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locStatus
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locStatus',p_web.GetValue('Value'))
    locStatus = p_web.GetValue('Value')
  End
  If locStatus = ''
    loc:Invalid = 'locStatus'
    loc:alert = p_web.translate('New Status') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
  do Value::locStatus
  do SendAlert

Value::locStatus  Routine
  p_web._DivHeader('WaybillDeleteFromProcessing_' & p_web._nocolon('locStatus') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locStatus')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locStatus = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locStatus'',''waybilldeletefromprocessing_locstatus_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locStatus',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locStatus') = 0
    p_web.SetSessionValue('locStatus','')
  end
    packet = clip(packet) & p_web.CreateOption('','',choose('' = p_web.getsessionvalue('locStatus')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(WEBJOB)
  bind(wob:Record)
  p_web._OpenFile(WAYBPRO)
  bind(wyp:Record)
  p_web._OpenFile(STATUS)
  bind(sts:Record)
  p_web._OpenFile(JOBSE)
  bind(jobe:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  p_web._OpenFile(WAYBAWT)
  bind(wya:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locStatus_OptionView)
  locStatus_OptionView{prop:filter} = 'sts:Job = ''YES'''
  locStatus_OptionView{prop:order} = 'UPPER(sts:Ref_Number)'
  Set(locStatus_OptionView)
  Loop
    Next(locStatus_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locStatus') = 0
      p_web.SetSessionValue('locStatus',sts:Status)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(sts:Status,sts:Status,choose(sts:Status = p_web.getsessionvalue('locStatus')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locStatus_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(WAYBPRO)
  p_Web._CloseFile(STATUS)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(WAYBAWT)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillDeleteFromProcessing_' & p_web._nocolon('locStatus') & '_value')

Comment::locStatus  Routine
    loc:comment = p_web.Translate('Required')
  p_web._DivHeader('WaybillDeleteFromProcessing_' & p_web._nocolon('locStatus') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locReason  Routine
  p_web._DivHeader('WaybillDeleteFromProcessing_' & p_web._nocolon('locReason') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Reason')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locReason  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locReason',p_web.GetValue('NewValue'))
    locReason = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locReason
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locReason',p_web.GetValue('Value'))
    locReason = p_web.GetValue('Value')
  End
  If locReason = ''
    loc:Invalid = 'locReason'
    loc:alert = p_web.translate('Reason') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locReason = Upper(locReason)
    p_web.SetSessionValue('locReason',locReason)
  do Value::locReason
  do SendAlert

Value::locReason  Routine
  p_web._DivHeader('WaybillDeleteFromProcessing_' & p_web._nocolon('locReason') & '_value','adiv')
  loc:extra = ''
  ! --- TEXT --- locReason
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locReason')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locReason = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locReason'',''waybilldeletefromprocessing_locreason_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'readonly','')
  packet = clip(packet) & p_web.CreateTextArea('locReason',p_web.GetSessionValue('locReason'),5,30,loc:fieldclass,loc:readonly,loc:extra,loc:javascript,255,,Net:Web:Control) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('WaybillDeleteFromProcessing_' & p_web._nocolon('locReason') & '_value')

Comment::locReason  Routine
    loc:comment = p_web.translate(p_web.site.RequiredText)
  p_web._DivHeader('WaybillDeleteFromProcessing_' & p_web._nocolon('locReason') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('WaybillDeleteFromProcessing_locStatus_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locStatus
      else
        do Value::locStatus
      end
  of lower('WaybillDeleteFromProcessing_locReason_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locReason
      else
        do Value::locReason
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('WaybillDeleteFromProcessing_form:ready_',1)
  p_web.SetSessionValue('WaybillDeleteFromProcessing_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_WaybillDeleteFromProcessing',0)

PreCopy  Routine
  p_web.SetValue('WaybillDeleteFromProcessing_form:ready_',1)
  p_web.SetSessionValue('WaybillDeleteFromProcessing_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_WaybillDeleteFromProcessing',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('WaybillDeleteFromProcessing_form:ready_',1)
  p_web.SetSessionValue('WaybillDeleteFromProcessing_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('WaybillDeleteFromProcessing:Primed',0)

PreDelete       Routine
  p_web.SetValue('WaybillDeleteFromProcessing_form:ready_',1)
  p_web.SetSessionValue('WaybillDeleteFromProcessing_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('WaybillDeleteFromProcessing:Primed',0)
  p_web.setsessionvalue('showtab_WaybillDeleteFromProcessing',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('WaybillDeleteFromProcessing_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('WaybillDeleteFromProcessing_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
        If locStatus = ''
          loc:Invalid = 'locStatus'
          loc:alert = p_web.translate('New Status') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
        If loc:Invalid <> '' then exit.
        If locReason = ''
          loc:Invalid = 'locReason'
          loc:alert = p_web.translate('Reason') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locReason = Upper(locReason)
          p_web.SetSessionValue('locReason',locReason)
        If loc:Invalid <> '' then exit.
  ! The following fields are not on the form, but need to be checked anyway.
    IF (p_web.GSV('wayType') = 'PRE')
        Access:WAYBAWT.ClearKey(wya:WAYBAWTIDKey)
        wya:WAYBAWTID = p_web.GSV('wayID')
        IF (Access:WAYBAWT.TryFetch(wya:WAYBAWTIDKey) = Level:Benign)
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = wya:JobNumber
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
            END ! IF
            
        ELSE
            loc:Invalid = 'locStatus'
            loc:alert = 'Cannot Delete. Record Not Found'
            EXIT
        END ! IF
    END ! IF
    
    IF (p_web.GSV('wayType') = 'PRO')
        Access:WAYBPRO.ClearKey(wyp:WAYBPROIDKey)
        wyp:WAYBPROID = p_web.GSV('wayID')
        IF (Access:WAYBPRO.TryFetch(wyp:WAYBPROIDKey) = Level:Benign)
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = wyp:JobNumber
            IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
            END !I F
            
        ELSE
            loc:Invalid = 'locStatus'
            loc:alert = 'Cannot Delete. Record Not Found'
            EXIT
        END ! IF
        
    END ! IF
                        
    IF (JobInUse(job:Ref_Number))
        loc:Invalid = 'locStatus'
        loc:Alert = 'Cannot Remove. The selected job is in use'
        EXIT
    END ! IF
                        
    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
        jobe:HubRepair = 0
        jobe:HubRepairDate = 0
        jobe:HubRepairTime = 0
        Access:JOBSE.TryUpdate()
        
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
            
        END ! IF
        
        locPreviousStatus = job:Current_Status
        
        ! Change Status
        p_web.FileToSessionQueue(JOBS)
        p_web.FileToSessionQueue(WEBJOB)
        p_web.FileToSessionQueue(JOBSE)
                            
        GetStatus(SUB(p_web.GSV('locStatus'),1,3),0,'JOB',p_web)
                            
        p_web.SessionQueueToFile(JOBS)
        p_web.SessionQueueToFile(WEBJOB)
        p_web.SessionQueueToFile(JOBSE)
                            
        IF (Access:JOBS.TryUpdate() = Level:Benign)
            Access:WEBJOB.TryUpdate()
            ! Add the "manual status change" audit trail entry
            AddToAudit(p_web,job:Ref_Number,'JOB','MANUAL STATUS CHANGE TO: ' & p_web.GSV('locStatus'), |
                'PREVIOUS STATUS: ' & CLIP(locPreviousStatus) & '<13,10>NEW STATUS: ' & p_web.GSV('locStatus') & '<13,10>REASON:<13,10>' & p_web.GSV('locReason'))
        END ! IF
        
    ELSE
  
    END ! IF
                        
    IF (p_web.GSV('wayType') = 'PRE')
        Access:WAYBAWT.DeleteRecord(0)    
    END ! IF
    
    IF (p_web.GSV('wayType') = 'PRO')
        Access:WAYBPRO.DeleteRecord(0)    
    END!  IF 
          
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('WaybillDeleteFromProcessing:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('locStatus')
  p_web.StoreValue('locReason')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locReason',locReason) ! STRING(255)
     p_web.SSV('locStatus',locStatus) ! STRING(30)
     p_web.SSV('locPreviousStatus',locPreviousStatus) ! STRING(30)
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locReason = p_web.GSV('locReason') ! STRING(255)
     locStatus = p_web.GSV('locStatus') ! STRING(30)
     locPreviousStatus = p_web.GSV('locPreviousStatus') ! STRING(30)
