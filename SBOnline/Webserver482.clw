

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER482.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! Report the JOBS File
!!! </summary>
LoanCollectionNote PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
locJobNumber         LONG                                  !
tmp:Ref_number       STRING(20)                            !
tmp:PrintedBy        STRING(255)                           !
code_temp            BYTE                                  !
option_temp          BYTE                                  !
Bar_code_string_temp CSTRING(21)                           !
Bar_Code_Temp        CSTRING(21)                           !
Bar_Code2_Temp       CSTRING(21)                           !
Address_Line1_Temp   STRING(30)                            !
Address_Line2_Temp   STRING(30)                            !
Address_Line3_Temp   STRING(30)                            !
Address_Line4_Temp   STRING(30)                            !
Invoice_Name_Temp    STRING(30)                            !
Delivery_Address1_Temp STRING(30)                          !
Delivery_address2_temp STRING(30)                          !
Delivery_address3_temp STRING(30)                          !
Delivery_address4_temp STRING(30)                          !
Invoice_Company_Temp STRING(30)                            !
Invoice_address1_temp STRING(30)                           !
invoice_address2_temp STRING(30)                           !
invoice_address3_temp STRING(30)                           !
invoice_address4_temp STRING(30)                           !
accessories_temp     STRING(30),DIM(6)                     !
estimate_value_temp  STRING(40)                            !
despatched_user_temp STRING(40)                            !
vat_temp             REAL                                  !
total_temp           REAL                                  !
part_number_temp     STRING(30)                            !
line_cost_temp       REAL                                  !
job_number_temp      STRING(20)                            !
esn_temp             STRING(30)                            !
charge_type_temp     STRING(22)                            !
repair_type_temp     STRING(22)                            !
labour_temp          REAL                                  !
parts_temp           REAL                                  !
courier_cost_temp    REAL                                  !
Quantity_temp        REAL                                  !
Description_temp     STRING(30)                            !
Cost_Temp            REAL                                  !
customer_name_temp   STRING(60)                            !
sub_total_temp       REAL                                  !
invoice_company_name_temp STRING(30)                       !
invoice_telephone_number_temp STRING(15)                   !
invoice_fax_number_temp STRING(15)                         !
tmp:LoanExchangeUnit STRING(100)                           !
tmp:DefaultTelephone STRING(20)                            !
tmp:DefaultFax       STRING(20)                            !
tmp:accessories      STRING(255)                           !
tmp:OriginalIMEI     STRING(30)                            !
tmp:FinalIMEI        STRING(20)                            !
tmp:DefaultEmailAddress STRING(255)                        !
tmp:EngineerReport   STRING(255)                           !
OutFaults_Q          QUEUE,PRE(ofq)                        !
OutFaultsDescription STRING(60)                            !
                     END                                   !
DeliveryAddress      GROUP,PRE(delivery)                   !
CustomerName         STRING(30)                            !
CompanyName          STRING(30)                            !
AddressLine1         STRING(30)                            !
AddressLine2         STRING(30)                            !
AddressLine3         STRING(30)                            !
Postcode             STRING(30)                            !
TelephoneNumber      STRING(30)                            !
                     END                                   !
endUserTelNo         STRING(15)                            !
clientName           STRING(65)                            !
tmp:Terms            STRING(1000)                          !
DefaultAddress       GROUP,PRE(address)                    !
SiteName             STRING(40)                            !
Name                 STRING(40)                            !
Name2                STRING(40)                            !
Location             STRING(40)                            !
AddressLine1         STRING(40)                            !
AddressLine2         STRING(40)                            !
AddressLine3         STRING(40)                            !
AddressLine4         STRING(40)                            !
Telephone            STRING(40)                            !
RegistrationNo       STRING(40)                            !
Fax                  STRING(30)                            !
EmailAddress         STRING(255)                           !
VatNumber            STRING(30)                            !
                     END                                   !
locTermsText         STRING(255)                           !
Process:View         VIEW(JOBS)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Authority_Number)
                       PROJECT(job:Consignment_Number)
                       PROJECT(job:Courier)
                       PROJECT(job:Date_Completed)
                       PROJECT(job:Despatch_Number)
                       PROJECT(job:MSN)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:Mobile_Number)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Time_Completed)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:date_booked)
                       PROJECT(job:time_booked)
                     END
ProgressWindow       WINDOW('Report JOBS'),AT(,,142,59),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT),DOUBLE, |
  CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Report'), |
  TIP('Cancel Report')
                     END

Report               REPORT,AT(396,6875,7521,1188),PRE(RPT),PAPER(PAPER:LETTER),FONT('Arial',10,,FONT:regular), |
  THOUS
                       HEADER,AT(417,333,7667,8125),USE(?unnamed)
                         STRING('Job Number: '),AT(4917,396),USE(?String25),FONT(,8),TRN
                         STRING(@s20),AT(6156,396),USE(tmp:Ref_number),FONT(,8,,FONT:bold),LEFT,TRN
                         STRING('Date Booked: '),AT(4917,719),USE(?String58),FONT(,8),TRN
                         STRING(@d6b),AT(6156,719),USE(job:date_booked),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING(@t1b),AT(6760,719),USE(job:time_booked),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING('Date Completed:'),AT(4917,875),USE(?String85),FONT(,8),TRN
                         STRING('Despatch Batch No:'),AT(4917,563),USE(?String86),FONT(,8),TRN
                         STRING(@d6b),AT(6156,875,635,198),USE(job:Date_Completed),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING(@t1b),AT(6760,875),USE(job:Time_Completed),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING(@s10b),AT(6156,563),USE(job:Despatch_Number),FONT(,8,,FONT:bold),LEFT,TRN
                         STRING(@s22),AT(1604,3240),USE(job:Order_Number,,?job:Order_Number:2),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(4531,3229),USE(charge_type_temp),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(3156,3240),USE(job:Authority_Number),FONT(,8),TRN
                         STRING(@s20),AT(6000,3240),USE(repair_type_temp),FONT(,8),TRN
                         STRING(@s20),AT(156,3906,1000,156),USE(job:Model_Number),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(5052,3906,1094,156),USE(tmp:FinalIMEI),FONT(,8),LEFT,HIDE,TRN
                         STRING(@s20),AT(1250,3906,1323,156),USE(job:Manufacturer),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(2604,3906,1396,156),USE(job:Unit_Type),FONT(,8),LEFT,TRN
                         STRING(@s30),AT(3906,3906,1094,156),USE(tmp:OriginalIMEI),FONT(,8),LEFT,TRN
                         STRING(@s20),AT(6198,3906),USE(job:MSN),FONT(,8),TRN
                         STRING('REPORTED FAULT'),AT(156,4167),USE(?String64),FONT(,9,,FONT:bold),TRN
                         TEXT,AT(1615,4167,5573,417),USE(jbn:Fault_Description),FONT('Arial',8,,,CHARSET:ANSI),TRN
                         STRING('ENGINEER REPORT'),AT(156,4688),USE(?String88),FONT(,9,,FONT:bold),TRN
                         TEXT,AT(1615,4688,5573,781),USE(tmp:EngineerReport),FONT('Arial',8,,,CHARSET:ANSI),TRN
                         STRING('EXCHANGE UNIT'),AT(156,5885),USE(?Exchange_Unit),FONT(,9,,FONT:bold),HIDE,TRN
                         STRING(@s100),AT(1615,5885,5781,208),USE(tmp:LoanExchangeUnit),FONT('Arial',8,,FONT:regular, |
  CHARSET:ANSI),HIDE,TRN
                         STRING('ACCESSORIES'),AT(156,5583),USE(?String105),FONT(,9,,FONT:bold),TRN
                         TEXT,AT(1615,5583,5573,260),USE(tmp:accessories),FONT('Arial',8,,FONT:regular,CHARSET:ANSI), |
  TRN
                         STRING('PARTS USED'),AT(156,6094),USE(?Parts_Used),FONT(,9,,FONT:bold),TRN
                         STRING('Qty'),AT(1521,6094),USE(?Qty),FONT(,8,,FONT:bold),TRN
                         STRING('Part Number'),AT(1917,6094),USE(?Part_Number),FONT(,8,,FONT:bold),TRN
                         STRING('Description'),AT(3698,6094),USE(?description),FONT(,8,,FONT:bold),TRN
                         STRING('Unit Cost'),AT(5719,6094),USE(?Unit_Cost),FONT(,8,,FONT:bold),HIDE,TRN
                         STRING('Line Cost'),AT(6677,6094),USE(?Line_Cost),FONT(,8,,FONT:bold),HIDE,TRN
                         LINE,AT(1406,6250,6042,0),USE(?Line1),COLOR(COLOR:Black)
                         TEXT,AT(156,7448,5885,260),USE(locTermsText),FONT(,7),TRN
                         STRING('Customer Signature'),AT(156,7813),USE(?Terms2),FONT(,8,,FONT:bold),TRN
                         STRING('Date: {13}/ {14}/'),AT(5104,7813),USE(?Terms3),FONT(,8,,FONT:bold),TRN
                         LINE,AT(1406,7969,5313,0),USE(?SigLine),COLOR(COLOR:Black)
                         STRING(@s30),AT(156,2188),USE(invoice_address4_temp),FONT(,8),TRN
                         STRING('Tel: '),AT(4115,2344),USE(?String32:2),FONT(,8),TRN
                         STRING(@s30),AT(4323,2344),USE(delivery:TelephoneNumber),FONT(,8)
                         STRING(@s65),AT(4115,2813,2865,156),USE(clientName),FONT('Arial',8,COLOR:Black,FONT:regular, |
  CHARSET:ANSI),TRN
                         STRING(@s15),AT(2781,2031),USE(job:Mobile_Number),FONT(,8),LEFT,TRN
                         STRING('Mobile:'),AT(2229,2031),USE(?String35:3),FONT(,8),TRN
                         STRING('Tel:'),AT(2229,1719),USE(?String34:2),FONT(,8),TRN
                         STRING(@s15),AT(2781,1719),USE(invoice_telephone_number_temp),FONT(,8),LEFT,TRN
                         STRING('Fax: '),AT(2229,1875),USE(?String35:2),FONT(,8),TRN
                         STRING(@s15),AT(2781,1875),USE(invoice_fax_number_temp),FONT(,8),LEFT,TRN
                         STRING(@s15),AT(156,3240),USE(job:Account_Number),FONT(,8),TRN
                         STRING(@s30),AT(4115,1563),USE(delivery:CompanyName),FONT(,8),TRN
                         STRING(@s30),AT(4115,1719,1917,156),USE(delivery:AddressLine1),FONT(,8),TRN
                         STRING(@s30),AT(4115,1875),USE(delivery:AddressLine2),FONT(,8),TRN
                         STRING(@s30),AT(4115,2031),USE(delivery:AddressLine3),FONT(,8),TRN
                         STRING(@s30),AT(4115,2188),USE(delivery:Postcode),FONT(,8),TRN
                         STRING(@s30),AT(156,1563),USE(invoice_company_name_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1719),USE(Invoice_address1_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,2031),USE(invoice_address3_temp),FONT(,8),TRN
                         STRING(@s30),AT(156,1875),USE(invoice_address2_temp),FONT(,8),TRN
                       END
DETAIL                 DETAIL,AT(0,0,,115),USE(?DetailBand)
                         STRING(@n8b),AT(1198,0),USE(Quantity_temp),FONT(,7),RIGHT,TRN
                         STRING(@s25),AT(1917,0),USE(part_number_temp),FONT(,7),TRN
                         STRING(@s25),AT(3677,0),USE(Description_temp),FONT(,7),TRN
                         STRING(@n14.2b),AT(5531,0),USE(Cost_Temp),FONT(,7),RIGHT,HIDE,TRN
                         STRING(@n14.2b),AT(6521,0),USE(line_cost_temp),FONT(,7),RIGHT,HIDE,TRN
                       END
                       FOOTER,AT(406,8698,7521,2167),USE(?unnamed:3)
                         STRING('Vodacom Repairs Loan Phone Terms And Conditions'),AT(208,1000),USE(?String108),FONT(, |
  7,,FONT:bold+FONT:underline),TRN
                         STRING('Despatch Date:'),AT(177,94),USE(?String66),FONT(,8),TRN
                         TEXT,AT(208,1125,7031,969),USE(stt:Text),FONT(,8),TRN
                         STRING('Courier: '),AT(177,250),USE(?String67),FONT(,8),TRN
                         STRING(@s20),AT(1177,250),USE(job:Courier),FONT(,8,,FONT:bold),TRN
                         STRING('Labour:'),AT(4865,94),USE(?labour_string),FONT(,8),TRN
                         STRING(@n14.2b),AT(6417,94),USE(labour_temp),FONT(,8),RIGHT,TRN
                         STRING('Consignment No:'),AT(177,406),USE(?String70),FONT(,8),TRN
                         STRING(@s20),AT(1177,406),USE(job:Consignment_Number),FONT(,8,,FONT:bold),TRN
                         STRING(@s20),AT(3146,292),USE(job_number_temp),FONT(,8,,FONT:bold),CENTER,TRN
                         STRING(@s8),AT(2896,94,1760,198),USE(Bar_Code_Temp),FONT('C128 High 12pt LJ3',12),CENTER
                         STRING('Carriage:'),AT(4865,406),USE(?carriage_string),FONT(,8),TRN
                         STRING(@n14.2b),AT(6417,250),USE(parts_temp),FONT(,8),RIGHT,TRN
                         STRING('V.A.T.'),AT(4865,573),USE(?vat_String),FONT(,8),TRN
                         STRING(@n14.2b),AT(6417,406),USE(courier_cost_temp),FONT(,8),RIGHT,TRN
                         STRING(@s13),AT(1167,562),USE(jobe2:LoanIDNumber),FONT(,8,,FONT:bold),TRN
                         STRING('ID Number:'),AT(177,562),USE(?String70:2),FONT(,8),TRN
                         STRING('Total:'),AT(4865,740),USE(?total_string),FONT(,8,,FONT:bold),TRN
                         LINE,AT(6302,729,1000,0),USE(?line),COLOR(COLOR:Black)
                         STRING(@n14.2b),AT(6417,740),USE(total_temp),FONT(,8,,FONT:bold),RIGHT,TRN
                         STRING(@s16),AT(2885,531,1760,198),USE(Bar_Code2_Temp),FONT('C128 High 12pt LJ3',12),CENTER
                         STRING(@n14.2b),AT(6417,573),USE(vat_temp),FONT(,8),RIGHT,TRN
                         STRING('<128>'),AT(6271,740,156,208),USE(?Euro),FONT(,8,,FONT:bold),TRN
                         STRING(@s30),AT(2896,729,1760,240),USE(esn_temp),FONT(,8,,FONT:bold),CENTER,TRN
                         STRING('Parts:'),AT(4865,250),USE(?parts_string),FONT(,8),TRN
                         STRING('<<-- Date Stamp -->'),AT(1167,83),USE(?ReportDateStamp),FONT(,8,,FONT:bold),TRN
                       END
                       FORM,AT(396,292,7510,11375),USE(?unnamed:4)
                         IMAGE('RINVDET.GIF'),AT(0,0,7552,10781),USE(?Image1)
                         STRING('LOAN COLLECTION NOTE'),AT(4531,0,2865,260),USE(?Title),FONT(,14,,FONT:bold),RIGHT, |
  TRN
                         STRING(@s40),AT(104,0,4469,260),USE(address:SiteName),FONT(,14,,FONT:bold),LEFT
                         STRING(@s40),AT(104,365,3073,208),USE(address:Name2),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,208,3073,208),USE(address:Name),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING(@s40),AT(104,521,2760,208),USE(address:Location),FONT(,8,COLOR:Black,FONT:bold),TRN
                         STRING('REG NO:'),AT(104,677),USE(?stringREGNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(625,677,1667,146),USE(address:RegistrationNo),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('VAT NO: '),AT(104,781),USE(?stringVATNO),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s20),AT(625,781,1771,156),USE(address:VatNumber),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,885,2240,156),USE(address:AddressLine1),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,990,2240,156),USE(address:AddressLine2),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1094,2240,156),USE(address:AddressLine3),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s40),AT(104,1198),USE(address:AddressLine4),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('TEL:'),AT(2385,885),USE(?stringTEL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(2625,885,1458,208),USE(address:Telephone),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('FAX:'),AT(2385,990,313,208),USE(?stringFAX),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s30),AT(2625,990,1510,188),USE(address:Fax),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('EMAIL:'),AT(104,1323,417,208),USE(?stringEMAIL),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING(@s255),AT(625,1313,3333,208),USE(address:EmailAddress),FONT(,7,,,CHARSET:ANSI),TRN
                         STRING('JOB DETAILS'),AT(4844,198),USE(?String57),FONT(,9,,FONT:bold),TRN
                         STRING('CUSTOMER ADDRESS'),AT(156,1458),USE(?String24),FONT(,9,,FONT:bold),TRN
                         STRING('DELIVERY ADDRESS'),AT(4010,1458,1677,156),USE(?String28),FONT(,9,,FONT:bold),TRN
                         STRING('GENERAL DETAILS'),AT(156,2865),USE(?String91),FONT(,9,,FONT:bold),TRN
                         STRING('DELIVERY DETAILS'),AT(156,8177),USE(?String73),FONT(,9,,FONT:bold),TRN
                         STRING('CHARGE DETAILS'),AT(4792,8177),USE(?charge_details),FONT(,9,,FONT:bold),TRN
                         STRING('Model'),AT(156,3750),USE(?String40),FONT(,8,,FONT:bold),TRN
                         STRING('Account Number'),AT(156,3073),USE(?String40:2),FONT(,8,,FONT:bold),TRN
                         STRING('Order Number'),AT(1604,3073),USE(?String40:3),FONT(,8,,FONT:bold),TRN
                         STRING('Authority Number'),AT(3083,3073),USE(?String40:4),FONT(,8,,FONT:bold),TRN
                         STRING('Chargeable Type'),AT(4531,3073),USE(?Chargeable_Type),FONT(,8,,FONT:bold),TRN
                         STRING('Chargeable Repair Type'),AT(6000,3073),USE(?Repair_Type),FONT(,8,,FONT:bold),TRN
                         STRING('Make'),AT(1250,3750),USE(?String41),FONT(,8,,FONT:bold),TRN
                         STRING('Exch I.M.E.I. No'),AT(5052,3750),USE(?IMEINo:2),FONT(,8,,FONT:bold),HIDE,TRN
                         STRING('Unit Type'),AT(2604,3750),USE(?String42),FONT(,8,,FONT:bold),TRN
                         STRING('I.M.E.I. Number'),AT(3906,3750),USE(?IMEINo),FONT(,8,,FONT:bold),TRN
                         STRING('M.S.N.'),AT(6198,3750),USE(?String44),FONT(,8,,FONT:bold),TRN
                         STRING('REPAIR DETAILS'),AT(156,3490),USE(?String50),FONT(,9,,FONT:bold),TRN
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR1               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('LoanCollectionNote')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
    locJobNumber = p_web.GSV('locJobNumber')  
  Relate:DEFAULT2.Open                                     ! File DEFAULT2 used by this procedure, so make sure it's RelationManager is open
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:EXCHANGE.Open                                     ! File EXCHANGE used by this procedure, so make sure it's RelationManager is open
  Relate:INVOICE.SetOpenRelated()
  Relate:INVOICE.Open                                      ! File INVOICE used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:TRADEACC_ALIAS.Open                               ! File TRADEACC_ALIAS used by this procedure, so make sure it's RelationManager is open
  Relate:VATCODE.Open                                      ! File VATCODE used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:JOBSE2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBNOTES.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBTHIRD.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOAN.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:WARPARTS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:STOCK.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBOUTFL.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:SUBTRACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PARTS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBACC.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('LoanCollectionNote',ProgressWindow)        ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisReport.AddSortOrder(job:Ref_Number_Key)
  ThisReport.AddRange(job:Ref_Number,locJobNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:JOBS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  Previewer.Maximize = True
  SELF.SetAlerts()
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = p_web.GSV('locJobNumber')
        IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        END ! IF
        
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = wob:HeadAccountNumber
            IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                tmp:Ref_number = job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
            END ! IF
        END ! IF
        
        IF (p_web.GSV('BookingSite') = 'ARC')
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = p_web.GSV('ARC:AccountNumber')
            IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
  
            END ! IF
        END ! IF 
        
        address:SiteName        = tra:Company_Name
        address:Name            = tra:coTradingName
        address:Name2           = tra:coTradingName2
        address:Location        = tra:coLocation
        address:RegistrationNo  = tra:coRegistrationNo
        address:AddressLine1    = tra:coAddressLine1
        address:AddressLine2    = tra:coAddressLine2
        address:AddressLine3    = tra:coAddressLine3
        address:AddressLine4    = tra:coAddressLine4
        address:Telephone       = tra:coTelephoneNumber
        address:Fax             = tra:coFaxNumber
        address:EmailAddress    = tra:coEmailAddress
        address:VatNumber       = tra:coVATNumber  
       
          !Barcode bit
          code_temp = 3
          option_temp = 0
          bar_code_string_temp = Clip(tmp:Ref_number)
          job_number_temp = 'Job No: ' & Clip(tmp:Ref_number)
          SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
          bar_code_string_temp = Clip(job:esn)
          esn_temp = 'I.M.E.I. No.: ' & Clip(job:esn)
          SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:INVOICE.Close
    Relate:STANTEXT.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('LoanCollectionNote',ProgressWindow)     ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'LoanCollectionNote',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.Init(Report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,Report{PROPPRINT:Paper},PAPER:USER), CHOOSE(Report{PROP:Thous}=True,PROP:Thous,CHOOSE(Report{PROP:MM}=True,PROP:MM,CHOOSE(Report{PROP:Points}=True,PROP:Points,0))), Report{PROPPRINT:PaperWidth}, Report{PROPPRINT:PaperHeight}, Report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetTitle('LoanCollectionNote')       !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR1.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetViewerPrefs(PDFXTR1:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetFontEmbedding(True, True, False, False, False)
    PDFXTR1.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text} = FORMAT(TODAY(),@d06)
  END
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'LoanCollectionNote',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR1.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
        Set(DEFAULT2)
        Access:DEFAULT2.Next()
  
        Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
        jobe2:RefNumber = job:Ref_Number
        If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
            ! Found
        Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
            ! Error
        End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
  
        Settarget(Report)
        Hide(?labour_string)
        Hide(?labour_temp)
        Hide(?Parts_temp)
        Hide(?parts_string)
        Hide(?Carriage_string)
        Hide(?vat_string)
        Hide(?total_string)
        Hide(?line)
        hide(?unit_Cost)
        hide(?line_cost)
        hide(?cost_temp)
        hide(?line_cost_temp)
        hide(?labour_string)
        hide(?parts_string)
        hide(?courier_cost_temp)
        hide(?vat_temp)
        hide(?total_temp)
        hide(?charge_details)
        ?Euro{prop:Hide} = 1
        Settarget()
  
        If job:warranty_job = 'YES' And job:chargeable_job <> 'YES'
            Settarget(Report)
            ?chargeable_type{prop:text} = 'Warranty Type'
            ?repair_type{prop:text} = 'Warranty Repair Type'
            Settarget()
            charge_type_temp = job:warranty_charge_type
            repair_type_temp = job:repair_type_warranty
        Else
            Settarget(Report)
            settarget()
            charge_type_temp = job:charge_type
            repair_type_temp = job:repair_type
        End!If job:chargeable_job = 'YES' And job:warranty_job = 'YES'
  
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job:account_number
        IF access:subtracc.fetch(sub:account_number_key)
            !Error!
        END
        access:tradeacc.clearkey(tra:account_number_key)
        tra:account_number = sub:main_account_number
        IF access:tradeacc.fetch(tra:account_number_key)
            !Error!
        END
  
  !*************************set the display address to the head account if booked on as a web job***********************
        if p_web.GSV('BookingSite') = 'RRC' then
            def:User_Name           = tra:Company_Name
            def:Address_Line1       = tra:Address_Line1
            def:Address_Line2       = tra:Address_Line2
            def:Address_Line3       = tra:Address_Line3
            def:Postcode            = tra:Postcode
            tmp:DefaultTelephone    = tra:Telephone_Number
            tmp:DefaultFax          = tra:Fax_Number
            def:EmailAddress        = tra:EmailAddress
        end
  !*********************************************************************************************************************
  
        HideDespAdd# = 0
        if tra:invoice_sub_accounts = 'YES'
            If sub:HideDespAdd = 1
                HideDespAdd# = 1
            End!If sub:HideDespAdd = 1
  
            If SUB:UseCustDespAdd = 'YES'
                invoice_address1_temp     = job:address_line1
                invoice_address2_temp     = job:address_line2
                If job:address_line3_delivery   = ''
                    invoice_address3_temp = job:postcode
                    invoice_address4_temp = ''
                Else
                    invoice_address3_temp  = job:address_line3
                    invoice_address4_temp  = job:postcode
                End
                invoice_company_name_temp   = job:company_name
                invoice_telephone_number_temp   = job:telephone_number
                invoice_fax_number_temp = job:fax_number
            Else!If sub:invoice_customer_address = 'YES'
                invoice_address1_temp     = sub:address_line1
                invoice_address2_temp     = sub:address_line2
                If job:address_line3_delivery   = ''
                    invoice_address3_temp = sub:postcode
                    invoice_address4_temp = ''
                Else
                    invoice_address3_temp  = sub:address_line3
                    invoice_address4_temp  = sub:postcode
                End
                invoice_company_name_temp   = sub:company_name
                invoice_telephone_number_temp   = sub:telephone_number
                invoice_fax_number_temp = sub:fax_number
  
  
            End!If sub:invoice_customer_address = 'YES'
  
        else!if tra:use_sub_accounts = 'YES'
            If tra:HideDespAdd = 1
                HideDespAdd# = 1
            End!If tra:HideDespAdd = 'YES'
  
            If TRA:UseCustDespAdd = 'YES'
                invoice_address1_temp     = job:address_line1
                invoice_address2_temp     = job:address_line2
                If job:address_line3_delivery   = ''
                    invoice_address3_temp = job:postcode
                    invoice_address4_temp = ''
                Else
                    invoice_address3_temp  = job:address_line3
                    invoice_address4_temp  = job:postcode
                End
                invoice_company_name_temp   = job:company_name
                invoice_telephone_number_temp   = job:telephone_number
                invoice_fax_number_temp = job:fax_number
  
            Else!If tra:invoice_customer_address = 'YES'
                invoice_address1_temp     = tra:address_line1
                invoice_address2_temp     = tra:address_line2
                If job:address_line3_delivery   = ''
                    invoice_address3_temp = tra:postcode
                    invoice_address4_temp = ''
                Else
                    invoice_address3_temp  = tra:address_line3
                    invoice_address4_temp  = tra:postcode
                End
                invoice_company_name_temp   = tra:company_name
                invoice_telephone_number_temp   = tra:telephone_number
                invoice_fax_number_temp = tra:fax_number
  
            End!If tra:invoice_customer_address = 'YES'
        End!!if tra:use_sub_accounts = 'YES'
  
  
        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = job:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Found
  
        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Error
        End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
        !------------------------------------------------------------------
        IF CLIP(job:title) = ''
            customer_name_temp = job:initial
        ELSIF CLIP(job:initial) = ''
            customer_name_temp = job:title
        ELSE
            customer_name_temp = CLIP(job:title) & ' ' & CLIP(job:initial)
        END ! IF
        !------------------------------------------------------------------
        IF CLIP(customer_name_temp) = ''
            customer_name_temp = job:surname
        ELSIF CLIP(job:surname) = ''
            !customer_name_temp = customer_name_temp ! tautology
        ELSE
            customer_name_temp = CLIP(customer_name_temp) & ' ' & CLIP(job:surname)
        END ! IF
        !------------------------------------------------------------------
  
        endUserTelNo = ''
  
        if jobe:EndUserTelNo <> ''
            endUserTelNo = clip(jobe:EndUserTelNo)
        end
  
        if customer_name_temp <> ''
            clientName = 'Client: ' & clip(customer_name_temp) & '  Tel: ' & clip(endUserTelNo)
        else
            if endUserTelNo <> ''
                clientName = 'Tel: ' & clip(endUserTelNo)
            end
        end
  
        delivery:TelephoneNumber    = job:Telephone_Delivery
        delivery:CompanyName        = job:Company_Name_Delivery
        delivery:AddressLine1     = job:address_line1_delivery
        delivery:AddressLine2     = job:address_line2_delivery
        If job:address_line3_delivery   = ''
            delivery:AddressLine3 = job:postcode_delivery
            delivery:Postcode = ''
        Else
            delivery:AddressLine3  = job:address_line3_delivery
            delivery:Postcode  = job:postcode_delivery
        End
  
        if job:title = '' and job:initial = ''
            delivery:CustomerName = clip(job:surname)
        elsif job:title = '' and job:initial <> ''
            delivery:CustomerName = clip(job:initial) & ' ' & clip(job:surname)
        elsif job:title <> '' and job:initial = ''
            delivery:CustomerName = clip(job:title) & ' ' & clip(job:surname)
        elsif job:title <> '' and job:initial <> ''
            delivery:CustomerName = clip(job:title) & ' ' & clip(job:initial) & ' ' & clip(job:surname)
        else
            delivery:CustomerName = ''
        end
  
  
      !Change the delivery address to the RRC, if it's at the ARC
      !going back to the RRC, otherwise its the delivery address on the job
        If p_web.GSV('BookingSite') = 'ARC' And jobe:WebJob
            Access:WEBJOB.Clearkey(wob:RefNumberKey)
            wob:RefNumber   = job:Ref_Number
            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          !Found
                Access:TRADEACC_ALIAS.Clearkey(tra_ali:Account_Number_Key)
                tra_ali:Account_Number  = wob:HeadAccountNumber
                If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
              !Found
                    delivery:CustomerName   = tra_ali:Account_Number
                    delivery:CompanyName    = tra_ali:Address_Line1
                    delivery:AddressLine1   = tra_ali:Address_Line2
                    delivery:AddressLine2   = tra_ali:Address_Line3
                    delivery:AddressLine3   = tra_ali:Postcode
                    delivery:Postcode       = ''
                    delivery:TelephoneNumber= tra_ali:Telephone_Number
  
                Else ! If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
              !Error
                End !If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          !Error
            End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        Else !glo:WebJob And jobe:WebJob
  
        End !glo:WebJob And jobe:WebJob
  
  
        labour_temp = ''
        parts_temp = ''
        courier_cost_temp = ''
        vat_temp = ''
        total_temp = ''
        If job:chargeable_job = 'YES'
  
            If job:invoice_number <> ''
                access:invoice.clearkey(inv:invoice_number_key)
                inv:invoice_number = job:invoice_number
                access:invoice.fetch(inv:invoice_number_key)
                If glo:WebJob
                    If inv:ExportedRRCOracle
                        Labour_Temp = jobe:InvRRCCLabourCost
                        Parts_Temp  = jobe:InvRRCCPartsCost
                        Courier_Cost_Temp = job:Invoice_Courier_Cost
                        Vat_Temp    = (jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour/100) + |
                            (jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts/100) + |
                            (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour/100)
                    Else !If inv:ExportedRRCOracle
                        Labour_Temp = jobe:RRCCLabourCost
                        Parts_Temp  = jobe:RRCCPartsCost
                        Courier_Cost_Temp = job:Courier_Cost
                        Vat_Temp    = (jobe:RRCCLabourCost * GetVatRate(job:Account_Number,'L')/100) + |
                            (jobe:RRCCPartsCost * GetVatRate(job:Account_Number,'P')/100) + |
                            (job:Courier_Cost * GetVatRate(job:Account_Number,'L')/100)
                    End !If inv:ExportedRRCOracle
                Else !If glo:WebJob
                    labour_temp       = job:invoice_labour_cost
                    parts_temp        = job:invoice_parts_cost
                    courier_cost_temp = job:invoice_courier_cost
                    Vat_Temp    = (job:Invoice_Labour_Cost * inv:Vat_Rate_Labour/100) + |
                        (job:Invoice_Parts_Cost * inv:Vat_Rate_Parts/100) + |
                        (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour/100)
                End !If glo:WebJob
  
            Else!If job:invoice_number <> ''
                If glo:WebJob
                    Labour_Temp = jobe:RRCCLabourCost
                    Parts_Temp  = jobe:RRCCPartsCost
                    Courier_Cost_Temp = job:Courier_Cost
                    Vat_Temp    = (jobe:RRCCLabourCost * GetVatRate(job:Account_Number,'L')/100) + |
                        (jobe:RRCCPartsCost * GetVatRate(job:Account_Number,'P')/100) + |
                        (job:Courier_Cost * GetVatRate(job:Account_Number,'L')/100)
  
                Else !If glo:WebJob
                    Labour_Temp = job:Labour_Cost
                    Parts_temp  = job:Parts_Cost
                    Courier_Cost_temp    = job:Courier_Cost
                    Vat_Temp    = (job:Labour_Cost * GetVatRate(job:Account_Number,'L')/100) + |
                        (job:Parts_Cost * GetVatRate(job:Account_Number,'P')/100) + |
                        (job:Courier_Cost * GetVatRate(job:Account_Number,'L')/100)
                End !If glo:WebJob
  
            End!If job:invoice_number <> ''
            total_temp = Labour_Temp + Parts_Temp + Courier_Cost_Temp + Vat_Temp
  
        End!If job:chargeable_job <> 'YES'
  
        If job:Warranty_Job = 'YES'
  
            If job:invoice_number_warranty <> ''
                access:invoice.clearkey(inv:invoice_number_key)
                inv:invoice_number = job:invoice_number_Warranty
                access:invoice.fetch(inv:invoice_number_key)
                If glo:WebJob
                    Labour_Temp = jobe:InvRRCWLabourCost
                    Parts_Temp  = jobe:InvRRCWPartsCost
                    Courier_Cost_Temp = job:WInvoice_Courier_Cost
                    Vat_Temp    = (jobe:InvRRCWLabourCost * inv:Vat_Rate_Labour/100) + |
                        (jobe:InvRRCWPartsCost * inv:Vat_Rate_Parts/100) + |
                        (job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour/100)
                Else !If glo:WebJob
                    labour_temp       = jobe:InvoiceClaimValue
                    parts_temp        = job:Winvoice_parts_cost
                    courier_cost_temp = job:Winvoice_courier_cost
                    Vat_Temp    = (jobe:InvoiceClaimValue * inv:Vat_Rate_Labour/100) + |
                        (job:WInvoice_Parts_Cost * inv:Vat_Rate_Parts/100) + |
                        (job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour/100)
                End !If glo:WebJob
  
            Else!If job:invoice_number <> ''
                If p_web.GSV('BookingSite') = 'RRC'
                    Labour_Temp = jobe:RRCWLabourCost
                    Parts_Temp  = jobe:RRCWPartsCost
                    Courier_Cost_Temp = job:Courier_Cost_Warranty
                    Vat_Temp    = (jobe:RRCWLabourCost * GetVatRate(job:Account_Number,'L')/100) + |
                        (jobe:RRCWPartsCost * GetVatRate(job:Account_Number,'P')/100) + |
                        (job:Courier_Cost_Warranty * GetVatRate(job:Account_Number,'L')/100)
  
                Else !If glo:WebJob
                    Labour_Temp = jobe:ClaimValue
                    Parts_temp  = job:Parts_Cost_Warranty
                    Courier_Cost_temp    = job:Courier_Cost_Warranty
                    Vat_Temp    = (jobe:ClaimValue * GetVatRate(job:Account_Number,'L')/100) + |
                        (job:Parts_Cost_Warranty * GetVatRate(job:Account_Number,'P')/100) + |
                        (job:Courier_Cost_Warranty * GetVatRate(job:Account_Number,'L')/100)
                End !If glo:WebJob
  
            End!If job:invoice_number <> ''
            total_temp = Labour_Temp + Parts_Temp + Courier_Cost_Temp + Vat_Temp
  
        End !job:Warranty_Job = 'YES'
  
        access:users.clearkey(use:user_code_key)
        use:user_code = job:despatch_user
        If access:users.fetch(use:user_code_key) = Level:Benign
            despatched_user_temp = Clip(use:forename) & ' ' & Clip(use:surname)
        End!If access:users.fetch(use:user_code_key) = Level:Benign
  
        tmp:accessories= ''
        access:jobacc.clearkey(jac:ref_number_key)
        jac:ref_number = job:ref_number
        set(jac:ref_number_key,jac:ref_number_key)
        loop
            if access:jobacc.next()
                break
            end !if
            if jac:ref_number <> job:ref_number      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
                yield() ; yldcnt# = 0
            end !if
            If tmp:accessories = ''
                tmp:accessories = jac:accessory
            Else!If tmp:accessories = ''
                tmp:accessories = Clip(tmp:accessories) & ',  ' & Clip(jac:accessory)
            End!If tmp:accessories = ''
        end !loop
  
  
        Access:JOBOUTFL.ClearKey(joo:LevelKey) ! Added by Gary (28th August 2002)
        joo:JobNumber = job:ref_number
        set(joo:LevelKey,joo:LevelKey)
        loop until Access:JOBOUTFL.Next()
            if joo:JobNumber <> job:ref_number then break.
            OutFaults_Q.ofq:OutFaultsDescription = joo:Description
            get(OutFaults_Q,OutFaults_Q.ofq:OutFaultsDescription) ! Queue is used to prevent duplicates
            if error()
                tmp:EngineerReport = clip(tmp:EngineerReport) & clip(joo:Description) & '<13,10>'
                OutFaults_Q.ofq:OutFaultsDescription = joo:Description
                add(OutFaults_Q,OutFaults_Q.ofq:OutFaultsDescription)
            end
        end
  
        exchange_note# = 0
  
        !Exchange Loan
        If job:exchange_unit_number <> ''
            exchange_note# = 1
        End!If job:exchange_unit_number <> ''
  
        !Check Chargeable Parts for an "Exchange" part
  
        access:parts.clearkey(par:part_number_key)
        par:ref_number  = job:ref_number
        set(par:part_number_key,par:part_number_key)
        loop
            if access:parts.next()
                break
            end !if
            if par:ref_number  <> job:ref_number      |
                then break.  ! end if
            access:stock.clearkey(sto:ref_number_key)
            sto:ref_number = par:part_ref_number
            if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                If sto:ExchangeUnit = 'YES'
                    exchange_note# = 1
                    Break
                End!If sto:ExchangeUnit = 'YES'
            end!if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
        end !loop
  
        If exchange_note# = 0
            !Check Warranty Parts for an "Exchange" part
            access:warparts.clearkey(wpr:part_number_key)
            wpr:ref_number  = job:ref_number
            set(wpr:part_number_key,wpr:part_number_key)
            loop
                if access:warparts.next()
                    break
                end !if
                if wpr:ref_number  <> job:ref_number      |
                    then break.  ! end if
                access:stock.clearkey(sto:ref_number_key)
                sto:ref_number = wpr:part_ref_number
                if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                    If sto:ExchangeUnit = 'YES'
                        exchange_note# = 1
                        Break
                    End!If sto:ExchangeUnit = 'YES'
                end!if access:stock.tryfetch(sto:ref_number_key) = Level:Benign
            end !loop
        End!If exchange_note# = 0
  
        If job:loan_unit_number <> ''
            Settarget(report)
            ?exchange_unit{prop:text} = 'LOAN UNIT'
            Unhide(?exchange_unit)
            Unhide(?tmp:LoanExchangeUnit)
            access:loan.clearkey(loa:ref_number_key)
            loa:ref_number = job:loan_unit_number
            if access:loan.tryfetch(loa:ref_number_key) = Level:Benign
                tmp:LoanExchangeUnit = '(' & Clip(loa:Ref_number) & ') ' & CLip(loa:manufacturer) & ' ' & CLip(loa:model_number) &|
                    ' - ' & CLip(loa:esn)
            end
            Settarget()
  
        End!If job:exchange_unit_number <> ''
  
        tmp:OriginalIMEI = job:ESN
  
        !Was the unit exchanged at Third Party?
        If job:Third_Party_Site <> ''
            IMEIError# = 0
            If job:Third_Party_Site <> ''
                Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
                jot:RefNumber = job:Ref_Number
                Set(jot:RefNumberKey,jot:RefNumberKey)
                If Access:JOBTHIRD.NEXT()
                    IMEIError# = 1
                Else !If Access:JOBTHIRD.NEXT()
                    If jot:RefNumber <> job:Ref_Number
                        IMEIError# = 1
                    Else !If jot:RefNumber <> job:Ref_Number
                        If jot:OriginalIMEI <> job:esn
                            IMEIError# = 0
                        Else
                            IMEIError# = 1
                        End !If jot:OriginalIMEI <> job:esn
  
                    End !If jot:RefNumber <> job:Ref_Number
                End !If Access:JOBTHIRD.NEXT()
            Else !job:Third_Party_Site <> ''
                IMEIError# = 1
            End !job:Third_Party_Site <> ''
  
            If IMEIError# = 1
                tmp:OriginalIMEI     = job:esn
            Else !IMEIError# = 1
                Settarget(Report)
                tmp:OriginalIMEI     = jot:OriginalIMEI
                tmp:FinalIMEI        = job:esn
                ?IMEINo{prop:text}   = 'Orig I.M.E.I. No'
                ?IMEINo:2{prop:hide} = 0
                ?tmp:FinalIMEI{prop:hide} = 0
                Settarget()
            End !IMEIError# = 1
        End !job:Third_Party_Site <> ''
  
        If exchange_note# = 1
            Settarget(report)
            Unhide(?exchange_unit)
            Unhide(?tmp:LoanExchangeUnit)
            Hide(?parts_used)
            Hide(?qty)
            Hide(?part_number)
            Hide(?Description)
            access:exchange.clearkey(xch:ref_number_key)
            xch:ref_number = job:exchange_unit_number
            if access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                tmp:LoanExchangeUnit = '(' & Clip(xch:Ref_number) & ') ' & CLip(xch:manufacturer) & ' ' & CLip(xch:model_number) &|
                    ' - ' & CLip(xch:esn)
                tmp:OriginalIMEI    = job:ESN
                tmp:FinalIMEI       = xch:ESN
                ?tmp:FinalIMEI{prop:Hide} = 0
                ?IMEINo:2{prop:Hide} = 0
            end
            Settarget()
        End!If exchange_note# = 1
  
        !Get Job Notes!
        Access:JobNotes.ClearKey(jbn:RefNumberKey)
        jbn:RefNumber = job:Ref_Number
        IF Access:JobNotes.Fetch(jbn:RefNumberKey)
            !Error!
        END  
        
        Access:STANTEXT.Clearkey(stt:Description_Key)
        stt:Description = 'LOAN COLLECTION NOTE'
        IF (Access:STANTEXT.TryFetch(stt:Description_Key))
        END
  
        locTermsText = GETINI('PRINTING','TermsText',,PATH() & '\SB2KDEF.INI')   ! #12265 Set default for "terms" text (Bryan: 30/08/2011)
  ReturnValue = PARENT.TakeRecord()
        If job:exchange_unit_number <> ''
            part_number_temp = ''
            Print(rpt:detail)
        Else!If job:exchange_unit_number <> ''
            count# = 0
  
            If job:warranty_job = 'YES' And job:chargeable_job <> 'YES'
                access:warparts.clearkey(wpr:part_number_key)
                wpr:ref_number  = job:ref_number
                set(wpr:part_number_key,wpr:part_number_key)
                loop
                    if access:warparts.next()
                        break
                    end !if
                    if wpr:ref_number  <> job:ref_number      |
                        then break.  ! end if
                    yldcnt# += 1
                    if yldcnt# > 25
                        yield() ; yldcnt# = 0
                    end !if
                    count# += 1
                    part_number_temp = wpr:part_number
                    description_temp = wpr:description
                    quantity_temp = wpr:quantity
                    cost_temp = wpr:purchase_cost
                    line_cost_temp = wpr:quantity * wpr:purchase_cost
                    Print(rpt:detail)
                end !loop
  
            Else!If job:warranty_job = 'YES' And job:chargeable_job <> 'YES'
                access:parts.clearkey(par:part_number_key)
                par:ref_number  = job:ref_number
                set(par:part_number_key,par:part_number_key)
                loop
                    if access:parts.next()
                        break
                    end !if
                    if par:ref_number  <> job:ref_number      |
                        then break.  ! end if
                    yldcnt# += 1
                    if yldcnt# > 25
                        yield() ; yldcnt# = 0
                    end !if
                    count# += 1
                    description_temp = par:description
                    quantity_temp = par:quantity
                    cost_temp = par:sale_cost
                    part_number_temp = par:part_number
                    line_cost_temp = par:quantity * par:sale_cost
                    Print(rpt:detail)
                end !loop
            End!If job:warranty_job = 'YES' And job:chargeable_job <> 'YES'
            If count# = 0
                part_number_temp = ''
                Print(rpt:detail)
            End!If count# = 0
        End!If job:exchange_unit_number <> ''
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR1:rtn = PDFXTR1.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR1:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

