

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER410.INC'),ONCE        !Local module procedure declarations
                     END


WarrantyBatchCostsPage PROCEDURE  (NetWebServerWorker p_web)
! Use this procedure to "embed" html in other pages.
! on the web page use <!-- Net:WarrantyBatchCostsPage -->
!
! In this procedure set the packet string variable, and call the SendPacket routine.
!
! EXAMPLE:
! packet = '<strong>Hello World!</strong>'&CRLF
! do SendPacket
loc:divname       string(255)
loc:parent        string(255)
FilesOpened     Long
WEBJOB::State  USHORT
JOBSE::State  USHORT
JOBSWARR::State  USHORT
                    MAP
RefreshBatchValue       PROCEDURE()
                    END ! MAP
CRLF                    string('<13,10>')
NBSP                    string('&#160;')
packet                  string(NET:MaxBinData)
packetlen               long
timer                   long
  CODE
  GlobalErrors.SetProcedureName('WarrantyBatchCostsPage')
  loc:parent = p_web.GetValue('_ParentProc')
  If loc:parent <> ''
    loc:divname = 'WarrantyBatchCostsPage' & '_' & loc:parent
  Else
    loc:divname = 'WarrantyBatchCostsPage'
  End
  p_web._DivHeader(loc:divname,'adiv')
!----------- put your html code here -----------------------------------
        IF (p_web.GSV('locBatchProcessingType') = 0)
            RefreshBatchValue()
            IF (p_web.GSV('locClaims') > 0)
                DO thePage
            END ! IF
        END ! IF
        
!----------- end of custom code ----------------------------------------
  do SendPacket
  p_web._DivFooter()
  if loc:parent
    p_web._RegisterDivEx(loc:divname,timer,'''_parentProc='&clip(loc:parent)&'''')
  else
    p_web._RegisterDivEx(loc:divname,timer)
  End
  GlobalErrors.SetProcedureName()
  Return

!--------------------------------------
SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet,1,packetlen,NET:NoHeader)
    packet = ''
  end
OpenFiles  ROUTINE
  p_web._OpenFile(WEBJOB)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBSWARR)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(WEBJOB)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBSWARR)
     FilesOpened = False
  END
thePage  Routine
  packet = clip(packet) & |
    '<<div style="width:145px;"><13,10>'&|
    '    <<span class="SubHeading">Value Of Jobs<</span><13,10>'&|
    '    <<br/><13,10>'&|
    '    <<table><13,10>'&|
    '        <<tr><13,10>'&|
    ' {12}<<td><<b>Claims:<</b><</td><13,10>'&|
    ' {12}<<td><<!-- Net:s:locClaims --><</td><13,10>'&|
    '        <</tr><13,10>'&|
    '        <<tr><<td><</td><</tr><13,10>'&|
    '        <<tr><13,10>'&|
    ' {12}<<td><<b>Labour:<</b><</td><13,10>'&|
    ' {12}<<td><<!-- Net:s:locLabour --><</td><13,10>'&|
    '        <</tr>  <13,10>'&|
    '        <<tr><13,10>'&|
    ' {12}<<td><<b>Parts:<</b><</td><13,10>'&|
    ' {12}<<td><<!-- Net:s:locParts --><</td><13,10>'&|
    '        <</tr> {10}<13,10>'&|
    '        <<tr><13,10>'&|
    ' {12}<<td><<b>Total:<</b><</td><13,10>'&|
    ' {12}<<td><<!-- Net:s:locTotal --><</td><13,10>'&|
    '        <</tr> <13,10>'&|
    '        <<tr><<td><</td><</tr>  <13,10>'&|
    '        <<tr><13,10>'&|
    ' {12}<<td><<b>Marked Blue:<</b><</td><13,10>'&|
    ' {12}<<td><<!-- Net:s:locMarkedBlue --><</td><13,10>'&|
    '        <</tr>  <13,10>'&|
    '        <<tr><13,10>'&|
    ' {12}<<td><<b>Marked Green:<</b><</td><13,10>'&|
    ' {12}<<td><<!-- Net:s:locMarkedGreen --><</td><13,10>'&|
    '        <</tr> {10}<13,10>'&|
    '        <<tr><13,10>'&|
    ' {12}<<td><<b>Marked Red:<</b><</td><13,10>'&|
    ' {12}<<td><<!-- Net:s:locMarkedRed --><</td><13,10>'&|
    '        <</tr><13,10>'&|
    '        <<tr><<td><</td><</tr>  <13,10>'&|
    '        <<tr><<td><</td><</tr>  <13,10>'&|
    '        <<tr><13,10>'&|
    ' {12}<<td><<b>Underpaid:<</b><</td><13,10>'&|
    ' {12}<<td><<!-- Net:s:locUnderPaid --><</td><13,10>'&|
    '        <</tr><13,10>'&|
    '    <</table><13,10>'&|
    '<</div><13,10>'&|
    ''
RefreshBatchValue   PROCEDURE()
locClaims               LONG
locLabour               DECIMAL(7,2)
locParts                DECIMAL(7,2)
locTotal                DECIMAL(7,2)
locMarkedGreen          DECIMAL(7,2)
locMarkedBlue           DECIMAL(7,2)
locMarkedRed            DECIMAL(7,2)
locUnderPaid            DECIMAL(7,2)
    CODE
        DO OpenFiles
        locClaims = 0
        locLabour = 0
        locParts = 0
        locTotal = 0 
        locMarkedGreen = 0
        locMarkedRed = 0
        locMarkedBlue = 0
        
        STREAM(JOBSWARR)
        Access:JOBSWARR.ClearKey(jow:RepairedRRCAccManKey)
        jow:BranchID = p_web.GSV('locBranchID')
        jow:RepairedAt = 'RRC'
        jow:RRCStatus = 'APP'
        jow:Manufacturer = p_web.GSV('locWarrantyManufacturer')
        jow:DateAccepted = p_web.GSV('locPaidStartDate')
        SET(jow:RepairedRRCAccManKey,jow:RepairedRRCAccManKey)
        LOOP UNTIL Access:JOBSWARR.Next() <> Level:Benign
            IF (jow:BranchID <> p_web.GSV('locBranchID') OR |
                jow:RepairedAt <> 'RRC' OR |
                jow:RRCStatus <> 'APP' OR |
                jow:Manufacturer <> p_web.GSV('locWarrantyManufacturer') OR |
                jow:DateAccepted > p_web.GSV('locPaidEndDate'))
                BREAK
            END ! IF
            
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = jow:RefNumber
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
            END ! IF
            
            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = jow:RefNumber
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
            END ! IF
            
            locClaims += 1
            
            locLabour += jobe:RRCWLabourCost
            locParts += jobe:RRCWPartsCost
            locTotal += jobe:RRCWLabourCost + jobe:RRCWPartsCost
            
            CASE wob:ReconciledMarker
            OF 1
                locMarkedGreen += jobe:RRCWLabourCost + jobe:RRCWPartsCost
            OF 2
                locMarkedBlue += jobe:RRCWLabourCost + jobe:RRCWPartsCost
            OF 3
                locMarkedRed += jobe:RRCWLabourCost + jobe:RRCWPartsCost
            END ! CASE
        END ! LOOP
        FLUSH(JOBSWARR)
        
        locUnderPaid = locTotal - locMarkedBlue - locMarkedGreen - locMarkedRed
        
        p_web.SSV('locClaims',locClaims)
        p_web.SSV('locLabour',FORMAT(locLabour,@n_14.2))
        p_web.SSV('locParts',FORMAT(locParts,@n_14.2))
        p_web.SSV('locTotal',FORMAT(locTotal,@n_14.2))
        p_web.SSV('locMarkedGreen',FORMAT(locMarkedGreen,@n_14.2))
        p_web.SSV('locMarkedBlue',FORMAT(locMarkedBlue,@n_14.2))
        p_web.SSV('locMarkedRed',FORMAT(locMarkedRed,@n_14.2))
        p_web.SSV('locUnderPaid',FORMAT(locUnderPaid,@n_14.2))    
        
        DO CloseFiles
