

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER121.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER120.INC'),ONCE        !Req'd for module callout resolution
                     END


ViewBouncerJob       PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
tmp:CurrentTradeAccount STRING(30)                         !Current Trade Account
FilesOpened     Long
JOBS_ALIAS::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  GlobalErrors.SetProcedureName('ViewBouncerJob')
  loc:formname = 'ViewBouncerJob_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'ViewBouncerJob',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('ViewBouncerJob','')
    p_web._DivHeader('ViewBouncerJob',clip('fdiv') & ' ' & clip('FormContent'))
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferViewBouncerJob',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of InsertRecord + NET:WEB:StagePost
    do RestoreMem
    do PostInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewBouncerJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy
  of Net:CopyRecord + NET:WEB:StagePost
    do RestoreMem
    do PostCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewBouncerJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_ViewBouncerJob',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    If loc:act = InsertRecord
      do PostInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do PostUpdate
    End

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferViewBouncerJob',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of DeleteRecord + NET:WEB:StagePost
    do RestoreMem
    do PostDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_ViewBouncerJob',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'ViewBouncerJob',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(JOBS_ALIAS)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(SUBTRACC)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(JOBS_ALIAS)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('ViewBouncerJob_form:inited_',1)
  p_web.SetValue('UpdateFile','JOBS_ALIAS')
  p_web.SetValue('UpdateKey','job_ali:Ref_Number_Key')
  p_web.SetValue('IDField','job_ali:Ref_Number')
  do RestoreMem

CancelForm  Routine
  IF p_web.GetSessionValue('ViewBouncerJob:Primed') = 1
    p_web._deleteFile(JOBS_ALIAS)
    p_web.SetSessionValue('ViewBouncerJob:Primed',0)
  End

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  p_web.SetValue('UpdateFile','JOBS_ALIAS')
  p_web.SetValue('UpdateKey','job_ali:Ref_Number_Key')
AfterLookup Routine
  loc:TabNumber = -1
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('tra:Account_Number',tra:Account_Number)
  p_web.SetSessionValue('tra:Company_Name',tra:Company_Name)
  p_web.SetSessionValue('tra:Address_Line1',tra:Address_Line1)
  p_web.SetSessionValue('tra:Address_Line2',tra:Address_Line2)
  p_web.SetSessionValue('tra:Address_Line3',tra:Address_Line3)
  p_web.SetSessionValue('tra:Postcode',tra:Postcode)
  p_web.SetSessionValue('tra:Telephone_Number',tra:Telephone_Number)
  p_web.SetSessionValue('tra:Fax_Number',tra:Fax_Number)
  p_web.SetSessionValue('tra:EmailAddress',tra:EmailAddress)

RestoreMem       Routine
  !FormSource=File
  if p_web.IfExistsValue('tra:Account_Number')
    tra:Account_Number = p_web.GetValue('tra:Account_Number')
    p_web.SetSessionValue('tra:Account_Number',tra:Account_Number)
  End
  if p_web.IfExistsValue('tra:Company_Name')
    tra:Company_Name = p_web.GetValue('tra:Company_Name')
    p_web.SetSessionValue('tra:Company_Name',tra:Company_Name)
  End
  if p_web.IfExistsValue('tra:Address_Line1')
    tra:Address_Line1 = p_web.GetValue('tra:Address_Line1')
    p_web.SetSessionValue('tra:Address_Line1',tra:Address_Line1)
  End
  if p_web.IfExistsValue('tra:Address_Line2')
    tra:Address_Line2 = p_web.GetValue('tra:Address_Line2')
    p_web.SetSessionValue('tra:Address_Line2',tra:Address_Line2)
  End
  if p_web.IfExistsValue('tra:Address_Line3')
    tra:Address_Line3 = p_web.GetValue('tra:Address_Line3')
    p_web.SetSessionValue('tra:Address_Line3',tra:Address_Line3)
  End
  if p_web.IfExistsValue('tra:Postcode')
    tra:Postcode = p_web.GetValue('tra:Postcode')
    p_web.SetSessionValue('tra:Postcode',tra:Postcode)
  End
  if p_web.IfExistsValue('tra:Telephone_Number')
    tra:Telephone_Number = p_web.GetValue('tra:Telephone_Number')
    p_web.SetSessionValue('tra:Telephone_Number',tra:Telephone_Number)
  End
  if p_web.IfExistsValue('tra:Fax_Number')
    tra:Fax_Number = p_web.GetValue('tra:Fax_Number')
    p_web.SetSessionValue('tra:Fax_Number',tra:Fax_Number)
  End
  if p_web.IfExistsValue('tra:EmailAddress')
    tra:EmailAddress = p_web.GetValue('tra:EmailAddress')
    p_web.SetSessionValue('tra:EmailAddress',tra:EmailAddress)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('ViewBouncerJob_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  If p_web.GSV('job:Account_Number') <> ''
      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
      sub:Account_Number = p_web.GSV('job:Account_Number')
      If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
          tmp:CurrentTradeAccount = sub:Main_Account_Number
      End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
  End ! If p_web.GSV('tmp:AccountNumber') <> ''
  
  
  Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
  sub:Account_Number = p_web.GSV('job_ali:Account_Number')
  If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
      p_web.FileToSessionQueue(SUBTRACC)
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number = sub:Main_Account_Number
      If ACcess:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          p_web.FileToSessionQueue(TRADEACC)
      End ! If ACcess:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
  End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
  
  If tmp:CurrentTradeAccount = tra:Account_Number
      p_web.SSV('Local:NoAuthorise',1)
  Else ! If tmp:CurrentTradeAccount = p_web.GSV('tra:Account_Number')
      p_web.SSV('Local:NoAuthorise',0)
  End ! If tmp:CurrentTradeAccount = p_web.GSV('tra:Account_Number')
  
  If NOT (DoesUserHaveAccess(p_web,'BOUNCER - AUTHORISE BILLING'))
      p_web.SSV('Local:NoAuthorise',1)
  End ! If SecurityCheckFailed('RAPID ENG - CHANGE JOB STATUS')
  
    p_web.SSV('NewAccountAuthorised',0)
    
    IF (p_web.GSV('FromURL') = 'NewJobBooking')
        ! Viewed from New Job Booking, show/hide the Authorise Button
        p_web.SSV('ViewBouncerJobNextURL','NewJobBooking')
        
        p_web.site.SaveButton.TextValue = 'Authorise'
        IF (p_web.GSV('Local:NoAuthorise') = 1)
            ! Hide the "Save" Button
            p_web.site.SaveButton.Class = 'hidden'
        END ! IF
    ELSIF (p_web.GSV('FromURL') = 'CustomerServicesForm')
        ! View from customer services screen, show the view job button
        p_web.site.SaveButton.TextValue = 'View Job'
        
        p_web.SSV('ViewBouncerJobNextURL','ViewJob')
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = p_web.GSV('job_ali:Ref_Number')
        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
            p_web.FileToSessionQUEUE(WEBJOB)
        ELSE ! IF
        END ! IF
    ELSE
        ! Viewed from anywhere else, don't show the view button
        p_web.site.SaveButton.Class = 'hidden'
    END ! IF
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.GSV('ViewBouncerJobNextURL')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('ViewBouncerJob_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('ViewBouncerJob_ChainTo')
    loc:formaction = p_web.GetSessionValue('ViewBouncerJob_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'BrowseIMEIHistory'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  packet = clip(packet) & '<input type="hidden" name="JOBS_ALIAS__FileAction" value="'&p_web.getSessionValue('JOBS_ALIAS:FileAction')&'" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="file" value="JOBS_ALIAS" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateFile" value="JOBS_ALIAS" ></input><13,10>'
  packet = clip(packet) & '<input type="hidden" name="UpdateKey" value="job_ali:Ref_Number_Key" ></input><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="ViewBouncerJob" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="ViewBouncerJob" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="ViewBouncerJob" ></input><13,10>'
  end

  do SendPacket
  packet = clip(packet) & p_web.CreateInput('hidden','job_ali:Ref_Number',p_web._jsok(p_web.getSessionValue('job_ali:Ref_Number'))) & '<13,10>'
  If p_web.Translate('Contact Details') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Contact Details',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_ViewBouncerJob">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_ViewBouncerJob">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_ViewBouncerJob')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Contact Details') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_ViewBouncerJob')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_ViewBouncerJob'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_ViewBouncerJob')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Contact Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewBouncerJob_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Contact Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Contact Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Contact Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Contact Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tra:Account_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tra:Account_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tra:Company_Name
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tra:Company_Name
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tra:Address_Line1
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tra:Address_Line1
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tra:Address_Line2
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tra:Address_Line2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tra:Address_Line3
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tra:Address_Line3
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tra:Postcode
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tra:Postcode
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tra:Telephone_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tra:Telephone_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tra:Fax_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tra:Fax_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&150&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::tra:EmailAddress
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::tra:EmailAddress
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_ViewBouncerJob_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
    If p_web.GSV('Local:NoAuthorise') = 0
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::TextDisplay
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::tra:Account_Number  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Account_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tra:Account_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tra:Account_Number',p_web.GetValue('NewValue'))
    tra:Account_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = tra:Account_Number
    do Value::tra:Account_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tra:Account_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    tra:Account_Number = p_web.GetValue('Value')
  End
  do Value::tra:Account_Number
  do SendAlert

Value::tra:Account_Number  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Account_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tra:Account_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tra:Account_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Account_Number'',''viewbouncerjob_tra:account_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Account_Number',p_web.GetSessionValueFormat('tra:Account_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewBouncerJob_' & p_web._nocolon('tra:Account_Number') & '_value')


Prompt::tra:Company_Name  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Company_Name') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Company Name')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tra:Company_Name  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tra:Company_Name',p_web.GetValue('NewValue'))
    tra:Company_Name = p_web.GetValue('NewValue') !FieldType= STRING Field = tra:Company_Name
    do Value::tra:Company_Name
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tra:Company_Name',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    tra:Company_Name = p_web.GetValue('Value')
  End
  do Value::tra:Company_Name
  do SendAlert

Value::tra:Company_Name  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Company_Name') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tra:Company_Name
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tra:Company_Name')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Company_Name'',''viewbouncerjob_tra:company_name_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Company_Name',p_web.GetSessionValueFormat('tra:Company_Name'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewBouncerJob_' & p_web._nocolon('tra:Company_Name') & '_value')


Prompt::tra:Address_Line1  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line1') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Address')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tra:Address_Line1  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tra:Address_Line1',p_web.GetValue('NewValue'))
    tra:Address_Line1 = p_web.GetValue('NewValue') !FieldType= STRING Field = tra:Address_Line1
    do Value::tra:Address_Line1
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tra:Address_Line1',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    tra:Address_Line1 = p_web.GetValue('Value')
  End
  do Value::tra:Address_Line1
  do SendAlert

Value::tra:Address_Line1  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line1') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tra:Address_Line1
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tra:Address_Line1')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Address_Line1'',''viewbouncerjob_tra:address_line1_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Address_Line1',p_web.GetSessionValueFormat('tra:Address_Line1'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line1') & '_value')


Prompt::tra:Address_Line2  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line2') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tra:Address_Line2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tra:Address_Line2',p_web.GetValue('NewValue'))
    tra:Address_Line2 = p_web.GetValue('NewValue') !FieldType= STRING Field = tra:Address_Line2
    do Value::tra:Address_Line2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tra:Address_Line2',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    tra:Address_Line2 = p_web.GetValue('Value')
  End
  do Value::tra:Address_Line2
  do SendAlert

Value::tra:Address_Line2  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line2') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tra:Address_Line2
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tra:Address_Line2')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Address_Line2'',''viewbouncerjob_tra:address_line2_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Address_Line2',p_web.GetSessionValueFormat('tra:Address_Line2'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line2') & '_value')


Prompt::tra:Address_Line3  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line3') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Suburb')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tra:Address_Line3  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tra:Address_Line3',p_web.GetValue('NewValue'))
    tra:Address_Line3 = p_web.GetValue('NewValue') !FieldType= STRING Field = tra:Address_Line3
    do Value::tra:Address_Line3
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tra:Address_Line3',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    tra:Address_Line3 = p_web.GetValue('Value')
  End
  do Value::tra:Address_Line3
  do SendAlert

Value::tra:Address_Line3  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line3') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tra:Address_Line3
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tra:Address_Line3')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Address_Line3'',''viewbouncerjob_tra:address_line3_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Address_Line3',p_web.GetSessionValueFormat('tra:Address_Line3'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s30'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewBouncerJob_' & p_web._nocolon('tra:Address_Line3') & '_value')


Prompt::tra:Postcode  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Postcode') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Postcode')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tra:Postcode  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tra:Postcode',p_web.GetValue('NewValue'))
    tra:Postcode = p_web.GetValue('NewValue') !FieldType= STRING Field = tra:Postcode
    do Value::tra:Postcode
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tra:Postcode',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    tra:Postcode = p_web.GetValue('Value')
  End
  do Value::tra:Postcode
  do SendAlert

Value::tra:Postcode  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Postcode') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tra:Postcode
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tra:Postcode')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Postcode'',''viewbouncerjob_tra:postcode_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Postcode',p_web.GetSessionValueFormat('tra:Postcode'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewBouncerJob_' & p_web._nocolon('tra:Postcode') & '_value')


Prompt::tra:Telephone_Number  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Telephone_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Telephone Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tra:Telephone_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tra:Telephone_Number',p_web.GetValue('NewValue'))
    tra:Telephone_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = tra:Telephone_Number
    do Value::tra:Telephone_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tra:Telephone_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    tra:Telephone_Number = p_web.GetValue('Value')
  End
  do Value::tra:Telephone_Number
  do SendAlert

Value::tra:Telephone_Number  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Telephone_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tra:Telephone_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tra:Telephone_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Telephone_Number'',''viewbouncerjob_tra:telephone_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Telephone_Number',p_web.GetSessionValueFormat('tra:Telephone_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewBouncerJob_' & p_web._nocolon('tra:Telephone_Number') & '_value')


Prompt::tra:Fax_Number  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Fax_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Fax Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tra:Fax_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tra:Fax_Number',p_web.GetValue('NewValue'))
    tra:Fax_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = tra:Fax_Number
    do Value::tra:Fax_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tra:Fax_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    tra:Fax_Number = p_web.GetValue('Value')
  End
  do Value::tra:Fax_Number
  do SendAlert

Value::tra:Fax_Number  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:Fax_Number') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tra:Fax_Number
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tra:Fax_Number')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:Fax_Number'',''viewbouncerjob_tra:fax_number_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:Fax_Number',p_web.GetSessionValueFormat('tra:Fax_Number'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s15'),) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewBouncerJob_' & p_web._nocolon('tra:Fax_Number') & '_value')


Prompt::tra:EmailAddress  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:EmailAddress') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Email Address')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::tra:EmailAddress  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('tra:EmailAddress',p_web.GetValue('NewValue'))
    tra:EmailAddress = p_web.GetValue('NewValue') !FieldType= STRING Field = tra:EmailAddress
    do Value::tra:EmailAddress
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('tra:EmailAddress',p_web.dFormat(p_web.GetValue('Value'),'@s255'))
    tra:EmailAddress = p_web.GetValue('Value')
  End
  do Value::tra:EmailAddress
  do SendAlert

Value::tra:EmailAddress  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('tra:EmailAddress') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- tra:EmailAddress
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('tra:EmailAddress')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(60) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''tra:EmailAddress'',''viewbouncerjob_tra:emailaddress_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','tra:EmailAddress',p_web.GetSessionValueFormat('tra:EmailAddress'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,p_web.PicLength('@s255'),'Email Address') & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('ViewBouncerJob_' & p_web._nocolon('tra:EmailAddress') & '_value')


Validate::TextDisplay  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('TextDisplay',p_web.GetValue('NewValue'))
    do Value::TextDisplay
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::TextDisplay  Routine
  p_web._DivHeader('ViewBouncerJob_' & p_web._nocolon('TextDisplay') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Select "Authorise" to use the above details on the job',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('ViewBouncerJob_tra:Account_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Account_Number
      else
        do Value::tra:Account_Number
      end
  of lower('ViewBouncerJob_tra:Company_Name_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Company_Name
      else
        do Value::tra:Company_Name
      end
  of lower('ViewBouncerJob_tra:Address_Line1_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Address_Line1
      else
        do Value::tra:Address_Line1
      end
  of lower('ViewBouncerJob_tra:Address_Line2_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Address_Line2
      else
        do Value::tra:Address_Line2
      end
  of lower('ViewBouncerJob_tra:Address_Line3_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Address_Line3
      else
        do Value::tra:Address_Line3
      end
  of lower('ViewBouncerJob_tra:Postcode_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Postcode
      else
        do Value::tra:Postcode
      end
  of lower('ViewBouncerJob_tra:Telephone_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Telephone_Number
      else
        do Value::tra:Telephone_Number
      end
  of lower('ViewBouncerJob_tra:Fax_Number_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:Fax_Number
      else
        do Value::tra:Fax_Number
      end
  of lower('ViewBouncerJob_tra:EmailAddress_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::tra:EmailAddress
      else
        do Value::tra:EmailAddress
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('ViewBouncerJob_form:ready_',1)
  p_web.SetSessionValue('ViewBouncerJob_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_ViewBouncerJob',0)

PreCopy  Routine
  p_web.SetValue('ViewBouncerJob_form:ready_',1)
  p_web.SetSessionValue('ViewBouncerJob_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_ViewBouncerJob',0)
  p_web._PreCopyRecord(JOBS_ALIAS,job_ali:Ref_Number_Key)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('ViewBouncerJob_form:ready_',1)
  p_web.SetSessionValue('ViewBouncerJob_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('ViewBouncerJob:Primed',0)

PreDelete       Routine
  p_web.SetValue('ViewBouncerJob_form:ready_',1)
  p_web.SetSessionValue('ViewBouncerJob_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('ViewBouncerJob:Primed',0)
  p_web.setsessionvalue('showtab_ViewBouncerJob',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord
    IF (p_web.GSV('FromURL') = 'NewJobBooking')
        If p_web.Failed = 0
            p_web.SSV('job:Account_Number',p_web.GSV('sub:Account_Number'))
            If p_web.GSV('sub:Generic_Account') = 1
                p_web.SSV('FranchiseAccount',2)
            Else ! If p_web.GSV('sub:GenericAcount') = 1
                p_web.SSV('FranchiseAccount',1)
            End ! If p_web.GSV('sub:GenericAcount') = 1
            p_web.SSV('Hide:PreviousAddress',1)
            p_web.SSV('NewAccountAuthorised',1)
        End ! If p_web.Failed = 0
    END ! IF

ValidateDelete  Routine
  p_web.DeleteSessionValue('ViewBouncerJob_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('ViewBouncerJob_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
  ! Automatic Dictionary Validation
  If p_web.GetSessionValue('ViewBouncerJob:Primed') = 1 ! Autonumbered fields are not validated unless the record has been pre-primed
    If job_ali:Ref_Number = 0
      loc:Invalid = 'job_ali:Ref_Number'
      loc:Alert = clip('job_ali:Ref_Number') & ' ' & p_web.site.NotZeroText
      !exit
    End
  End
  If Loc:Invalid <> '' then exit.
! NET:WEB:StagePOST
PostInsert      Routine

PostCopy        Routine
  p_web.SetSessionValue('ViewBouncerJob:Primed',0)

PostUpdate      Routine
  p_web.SetSessionValue('ViewBouncerJob:Primed',0)
  p_web.StoreValue('tra:Account_Number')
  p_web.StoreValue('tra:Company_Name')
  p_web.StoreValue('tra:Address_Line1')
  p_web.StoreValue('tra:Address_Line2')
  p_web.StoreValue('tra:Address_Line3')
  p_web.StoreValue('tra:Postcode')
  p_web.StoreValue('tra:Telephone_Number')
  p_web.StoreValue('tra:Fax_Number')
  p_web.StoreValue('tra:EmailAddress')
  p_web.StoreValue('')

PostDelete      Routine
