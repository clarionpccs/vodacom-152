

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER373.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER134.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER356.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER372.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER375.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER377.INC'),ONCE        !Req'd for module callout resolution
                     END


FormRapidLoanExchangeInsert PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locStockType         STRING(30)                            !
locManufacturer      STRING(30)                            !
locModelNumber       STRING(30)                            !
locShelfLocation     STRING(30)                            !
locAllocateAccessories LONG                                !
locIMEINumber        STRING(30)                            !
locMSN               STRING(30)                            !
locIMEIsInserted     STRING(2000000)                       !
                    MAP
ProcessIMEI             PROCEDURE()
ResetIMEINo PROCEDURE()
                    END ! MAP
FilesOpened     Long
Tagging::State  USHORT
MANUFACT::State  USHORT
ACCESSOR::State  USHORT
EXCHHIST::State  USHORT
LOANACC::State  USHORT
JOBS::State  USHORT
LOAN::State  USHORT
EXCHANGE::State  USHORT
LOANHIST::State  USHORT
LOCSHELF::State  USHORT
STOCKTYP::State  USHORT
MODELNUM::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('FormRapidLoanExchangeInsert')
  loc:formname = 'FormRapidLoanExchangeInsert_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'FormRapidLoanExchangeInsert',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('FormRapidLoanExchangeInsert','')
      do SendPacket
      Do imeivalidation
      do SendPacket
    p_web._DivHeader('FormRapidLoanExchangeInsert',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferFormRapidLoanExchangeInsert',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormRapidLoanExchangeInsert',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormRapidLoanExchangeInsert',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_FormRapidLoanExchangeInsert',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferFormRapidLoanExchangeInsert',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_FormRapidLoanExchangeInsert',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'FormRapidLoanExchangeInsert',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(Tagging)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(ACCESSOR)
  p_web._OpenFile(EXCHHIST)
  p_web._OpenFile(LOANACC)
  p_web._OpenFile(JOBS)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(LOANHIST)
  p_web._OpenFile(LOCSHELF)
  p_web._OpenFile(STOCKTYP)
  p_web._OpenFile(MODELNUM)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(Tagging)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(ACCESSOR)
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(LOANACC)
  p_Web._CloseFile(JOBS)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(LOANHIST)
  p_Web._CloseFile(LOCSHELF)
  p_Web._CloseFile(STOCKTYP)
  p_Web._CloseFile(MODELNUM)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('FormRapidLoanExchangeInsert_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('tab') = 1
    loc:TabNumber += 1
  End
  Case p_Web.GetValue('lookupfield')
  Of 'locStockType'
    p_web.setsessionvalue('showtab_FormRapidLoanExchangeInsert',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(STOCKTYP)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locModelNumber')
  Of 'locModelNumber'
    p_web.setsessionvalue('showtab_FormRapidLoanExchangeInsert',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(MODELNUM)
        p_web.setsessionvalue('locManufacturer',mod:Manufacturer)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locShelfLocation')
  Of 'locShelfLocation'
    p_web.setsessionvalue('showtab_FormRapidLoanExchangeInsert',Loc:TabNumber)
    if loc:LookupDone
      p_web.FileToSessionQueue(LOCSHELF)
    End
    p_web.SetValue('SelectField',clip(loc:formname) & '.locAllocateAccessories')
  End
  If p_web.GSV('tab') = 2
    loc:TabNumber += 1
  End
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locStockType',locStockType)
  p_web.SetSessionValue('locModelNumber',locModelNumber)
  p_web.SetSessionValue('locShelfLocation',locShelfLocation)
  p_web.SetSessionValue('locAllocateAccessories',locAllocateAccessories)
  p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  p_web.SetSessionValue('locMSN',locMSN)
  p_web.SetSessionValue('locIMEIsInserted',locIMEIsInserted)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locStockType')
    locStockType = p_web.GetValue('locStockType')
    p_web.SetSessionValue('locStockType',locStockType)
  End
  if p_web.IfExistsValue('locModelNumber')
    locModelNumber = p_web.GetValue('locModelNumber')
    p_web.SetSessionValue('locModelNumber',locModelNumber)
  End
  if p_web.IfExistsValue('locShelfLocation')
    locShelfLocation = p_web.GetValue('locShelfLocation')
    p_web.SetSessionValue('locShelfLocation',locShelfLocation)
  End
  if p_web.IfExistsValue('locAllocateAccessories')
    locAllocateAccessories = p_web.GetValue('locAllocateAccessories')
    p_web.SetSessionValue('locAllocateAccessories',locAllocateAccessories)
  End
  if p_web.IfExistsValue('locIMEINumber')
    locIMEINumber = p_web.GetValue('locIMEINumber')
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
  End
  if p_web.IfExistsValue('locMSN')
    locMSN = p_web.GetValue('locMSN')
    p_web.SetSessionValue('locMSN',locMSN)
  End
  if p_web.IfExistsValue('locIMEIsInserted')
    locIMEIsInserted = p_web.GetValue('locIMEIsInserted')
    p_web.SetSessionValue('locIMEIsInserted',locIMEIsInserted)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('FormRapidLoanExchangeInsert_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    ! Initialise Form
    IF (p_web.IfExistsValue('type'))
        p_web.StoreValue('type')
    END !I F
    IF (p_web.IfExistsValue('tab'))
        p_web.StoreValue('tab')
    END ! IF
  
    IF (p_web.GSV('type') = 'EXC')
        p_web.SSV('FormTitle','Rapid Exchange Unit Insert Wizard')
        p_web.SSV('ReturnURL','FormAllExchangeUnits')
        p_web.SSV('SetStockType','E')
    ELSE
        p_web.SSV('FormTitle','Rapid Loan Unit Insert Wizard')
        p_web.SSV('ReturnURL','FormAllLoanUnits')
        p_web.SSV('SetStockType','L')
    END ! IF
    
    p_web.SSV('Hide:MSN',1)
    
    ! First Time  
    IF (p_web.IfExistsValue('firsttime'))
          ! Clear Fields
        p_web.SSV('locStockType','')
        p_web.SSV('locManufacturer','')
        p_web.SSV('locModelNumber','')
        p_web.SSV('locShelfLocation','')
        p_web.SSV('locAllocateAccessories','')
        p_web.SSV('locIMEINumber','')
        p_web.SSV('locMSN','')        
        p_web.SSV('locIMEIsInserted','')
        p_web.SSV('imeiOK',0)
    END ! IF     
    ! Tab 1
    IF (p_web.GSV('tab') = 1)
        p_web.SSV('locIMEINumber','')
        p_web.SSV('locMSN','')
        p_web.SSV('imeiOK',0)
    END ! IF
    ! Tab 2
    IF (p_web.GSV('tab') = 2)
          ! Validate that all fields have been filled in
        IF (p_web.GSV('locStockType') = '' OR | 
            p_web.GSV('locModelNumber') = '' OR | 
            p_web.GSV('locShelfLocation') = '')
              
            loc:alert = 'You must complete all the fields to continue.'
            p_web.SSV('tab',1)
        END ! IF
    END ! IF    
    
    IF (p_web.GSV('tab') = 2) 
        IF (MSNRequired(p_web.GSV('locManufacturer')))
            p_web.SSV('Hide:MSN',0)
        END ! IF   
        
        IF (p_web.IfExistsValue('imeiconf'))
            IF (VodacomClass.ScrambleUn(p_web.GSV('locIMEINumber')) <> p_web.GetValue('imeiconf'))
                loc:alert = 'An IMEI Number mismatch has occurred. The IMEI Number entered does not match the original one. Please try again.'
                loc:invalid = 'locIMEINumber'
                ResetIMEINo()
            ELSE
                p_web.SSV('imeiOK',1)
                IF (p_web.GSV('Hide:MSN') = 1)
                    IF (p_web.GSV('locAllocateAccessories') <> 1)
                        ProcessIMEI()
                    END ! IF
                ELSE
                END ! IF
            END ! IF
        END ! IF
        
        p_web.SSV('tmp:LoanModelNumber',p_web.GSV('locModelNumber')) ! Field needed for Accessory Browse
  
    END ! IF
      p_web.site.CancelButton.TextValue = 'Close'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locStockType = p_web.RestoreValue('locStockType')
 locModelNumber = p_web.RestoreValue('locModelNumber')
 locShelfLocation = p_web.RestoreValue('locShelfLocation')
 locAllocateAccessories = p_web.RestoreValue('locAllocateAccessories')
 locIMEINumber = p_web.RestoreValue('locIMEINumber')
 locMSN = p_web.RestoreValue('locMSN')
 locIMEIsInserted = p_web.RestoreValue('locIMEIsInserted')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferFormRapidLoanExchangeInsert')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('FormRapidLoanExchangeInsert_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('FormRapidLoanExchangeInsert_ChainTo')
    loc:formaction = p_web.GetSessionValue('FormRapidLoanExchangeInsert_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = p_web.GSV('ReturnURL')
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="FormRapidLoanExchangeInsert" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="FormRapidLoanExchangeInsert" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="FormRapidLoanExchangeInsert" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate(p_web.GSV('FormTitle')) <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate(p_web.GSV('FormTitle'),0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_FormRapidLoanExchangeInsert">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_FormRapidLoanExchangeInsert" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_FormRapidLoanExchangeInsert')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GSV('tab') = 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Common Fields') & ''''
        End
        If p_web.GSV('tab') = 2
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Create New Units') & ''''
        End
        Loc:Tabnumber = p_web.getSessionValue('showtab_FormRapidLoanExchangeInsert')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_FormRapidLoanExchangeInsert'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('FormRapidLoanExchangeInsert_TagValidateLoanAccessories_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
    If upper(p_web.getvalue('LookupFile'))='STOCKTYP'
      If p_web.GSV('tab') = 1
            p_web.SetValue('SelectField',clip(loc:formname) & '.locModelNumber')
      End
    End
    If upper(p_web.getvalue('LookupFile'))='MODELNUM'
      If p_web.GSV('tab') = 1
            p_web.SetValue('SelectField',clip(loc:formname) & '.locShelfLocation')
      End
    End
    If upper(p_web.getvalue('LookupFile'))='LOCSHELF'
      If p_web.GSV('tab') = 2
            p_web.SetValue('SelectField',clip(loc:formname) & '.locIMEINumber')
      End
    End
  Else
    If False
    ElsIf p_web.GSV('tab') = 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.')
    ElsIf p_web.GSV('tab') = 2
        p_web.SetValue('SelectField',clip(loc:formname) & '.locIMEINumber')
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GSV('tab') = 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
          End
          If p_web.GSV('tab') = 2
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          End
          Loc:Tabnumber = p_web.getSessionValue('showtab_FormRapidLoanExchangeInsert')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GSV('tab') = 1
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
    end
    if p_web.GSV('tab') = 2
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    end
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GSV('tab') = 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Common Fields') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormRapidLoanExchangeInsert_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Common Fields')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Common Fields')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Common Fields')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Common Fields')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::txtLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hiddenField
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locStockType
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locStockType
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locModelNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locModelNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locShelfLocation
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locShelfLocation
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('type') = 'LOA'
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locAllocateAccessories
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locAllocateAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnNext
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
  If p_web.GSV('tab') = 2
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Create New Units') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_FormRapidLoanExchangeInsert_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Create New Units')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Create New Units')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Create New Units')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Create New Units')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIMEINumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIMEINumber
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnClear
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::hidden2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    If p_web.GSV('Hide:MSN') <> 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locMSN
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locMSN
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('type') = 'LOA' AND p_web.GSV('locAllocateAccessories') = 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::brwTagAccessories
      do Value::brwTagAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
    If p_web.GSV('type') = 'LOA' AND p_web.GSV('locAllocateAccessories') = 1
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnAddUnit
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    end
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__line
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::txtINserted
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locIMEIsInserted
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&300&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locIMEIsInserted
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&200&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::__line2
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 2.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::btnBack
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end


Validate::txtLocation  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtLocation',p_web.GetValue('NewValue'))
    do Value::txtLocation
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtLocation  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('txtLocation') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('bold')&'">' & p_web.Translate(p_web.GSV('BookingSiteLocation'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::hiddenField  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hiddenField',p_web.GetValue('NewValue'))
    do Value::hiddenField
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hiddenField  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('hiddenField') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locStockType  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('locStockType') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Stock Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locStockType  Routine
    If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('Value')
      p_web.GetDescription(STOCKTYP,stp:Stock_Type_Key,,stp:Stock_Type,stp:Stock_Type,p_web.GetValue('Value'))
      loc:lookupdone = 1
    Else
      p_web.GetDescription(STOCKTYP,stp:Stock_Type_Key,,stp:Stock_Type,stp:Stock_Type,p_web.GetSessionValue('locStockType'))
    End
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locStockType',p_web.GetValue('NewValue'))
    locStockType = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locStockType
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locStockType',p_web.GetValue('Value'))
    locStockType = p_web.GetValue('Value')
  End
  If locStockType = ''
    loc:Invalid = 'locStockType'
    loc:alert = p_web.translate('Stock Type') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locStockType = Upper(locStockType)
    p_web.SetSessionValue('locStockType',locStockType)
    IF (p_web.GSV('locStockType') <> '')
        IF (p_web.GSV('type') = 'EXC')
            Access:STOCKTYP.ClearKey(stp:Use_Exchange_Key)
            stp:Use_Exchange = 'YES'
            stp:Stock_Type = p_web.GSV('locStockType')
            IF (Access:STOCKTYP.TryFetch(stp:Use_Exchange_Key))
                loc:alert = 'Invalid Stock Type'
                loc:invalid = 'locStockType'
                p_web.SSV('locStockType','')
            END ! IF
            
        ELSE
            Access:STOCKTYP.ClearKey(stp:Use_Loan_Key)
            stp:Use_Loan = 'YES'  
            stp:Stock_Type = p_web.GSV('locStockType')
            IF (Access:STOCKTYP.TryFetch(stp:Use_Exchange_Key))
                loc:alert = 'Invalid Stock Type'
                loc:invalid = 'locStockType'
                p_web.SSV('locStockType','')
            END ! IF
        END ! IF
    END ! IF
    
  p_Web.SetValue('lookupfield','locStockType')
  do AfterLookup
  do Value::locStockType
  do SendAlert

Value::locStockType  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('locStockType') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locStockType
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locStockType')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locStockType = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locStockType'',''formrapidloanexchangeinsert_locstocktype_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locStockType')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','locStockType',p_web.GetSessionValueFormat('locStockType'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('SelectStockTypes?LookupField=locStockType&Tab=1&ForeignField=stp:Stock_Type&_sort=stp:Stock_Type&Refresh=sort&LookupFrom=FormRapidLoanExchangeInsert&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRapidLoanExchangeInsert_' & p_web._nocolon('locStockType') & '_value')


Prompt::locModelNumber  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('locModelNumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Model Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locModelNumber  Routine
    If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('Value')
      p_web.GetDescription(MODELNUM,mod:Model_Number_Key,,mod:Model_Number,mod:Model_Number,p_web.GetValue('Value'))
      loc:lookupdone = 1
    Else
      p_web.GetDescription(MODELNUM,mod:Model_Number_Key,,mod:Model_Number,mod:Model_Number,p_web.GetSessionValue('locModelNumber'))
    End
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locModelNumber',p_web.GetValue('NewValue'))
    locModelNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locModelNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locModelNumber',p_web.GetValue('Value'))
    locModelNumber = p_web.GetValue('Value')
  End
  If locModelNumber = ''
    loc:Invalid = 'locModelNumber'
    loc:alert = p_web.translate('Model Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locModelNumber = Upper(locModelNumber)
    p_web.SetSessionValue('locModelNumber',locModelNumber)
    IF (p_web.GSV('locModelNumber') <> '')
        Access:MODELNUM.ClearKey(mod:Model_Number_Key)
        mod:Model_Number = p_web.GSV('locModelNumber')
        IF (Access:MODELNUM.TryFetch(mod:Model_Number_Key))
            loc:alert = 'Invalid Model Number'
            loc:invalid = 'locModelNumber'
            p_web.SSV('locModelNumber','')
        ELSE ! END
            p_web.SSV('locManufacturer',mod:Manufacturer)
        END ! IF
    END ! IF
    
  p_Web.SetValue('lookupfield','locModelNumber')
  do AfterLookup
  do Value::locModelNumber
  do SendAlert

Value::locModelNumber  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('locModelNumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locModelNumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locModelNumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locModelNumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locModelNumber'',''formrapidloanexchangeinsert_locmodelnumber_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locModelNumber')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','locModelNumber',p_web.GetSessionValueFormat('locModelNumber'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('FormManModelNumbers?LookupField=locModelNumber&Tab=1&ForeignField=mod:Model_Number&_sort=mod:Model_Number&Refresh=sort&LookupFrom=FormRapidLoanExchangeInsert&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRapidLoanExchangeInsert_' & p_web._nocolon('locModelNumber') & '_value')


Prompt::locShelfLocation  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('locShelfLocation') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Shelf Location')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locShelfLocation  Routine
    If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('Value')
      p_web.GetDescription(LOCSHELF,los:Shelf_Location_Key,,los:Site_Location,los:Shelf_Location,p_web.GetValue('Value'))
      loc:lookupdone = 1
    Else
      p_web.GetDescription(LOCSHELF,los:Shelf_Location_Key,,los:Site_Location,los:Shelf_Location,p_web.GetSessionValue('locShelfLocation'))
    End
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locShelfLocation',p_web.GetValue('NewValue'))
    locShelfLocation = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locShelfLocation
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locShelfLocation',p_web.GetValue('Value'))
    locShelfLocation = p_web.GetValue('Value')
  End
  If locShelfLocation = ''
    loc:Invalid = 'locShelfLocation'
    loc:alert = p_web.translate('Shelf Location') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locShelfLocation = Upper(locShelfLocation)
    p_web.SetSessionValue('locShelfLocation',locShelfLocation)
    IF (p_web.GSV('locShelfLocation') <> '')
        Access:LOCSHELF.ClearKey(los:Shelf_Location_Key)
        los:Site_Location = p_web.GSV('BookingSiteLocation')
        los:Shelf_Location = p_web.GSV('locShelfLocation')
        IF (Access:LOCSHELF.TryFetch(los:Shelf_Location_Key))
            loc:Alert = 'Invalid Shelf Location'
            loc:invalid = 'locShelfLocation'
            p_web.SSV('locShelfLocation','')
        END!  IF
        
    END ! IF
    
  p_Web.SetValue('lookupfield','locShelfLocation')
  do AfterLookup
  do Value::locShelfLocation
  do SendAlert

Value::locShelfLocation  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('locShelfLocation') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locShelfLocation
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locShelfLocation')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locShelfLocation = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locShelfLocation'',''formrapidloanexchangeinsert_locshelflocation_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locShelfLocation')&''',0);'  ! was 1, but need to match up netweb.js with this bit of code.
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:lookuponly = ''
    packet = clip(packet) & p_web.CreateInput('text','locShelfLocation',p_web.GetSessionValueFormat('locShelfLocation'),loc:fieldclass,loc:readonly & ' ' & loc:lookuponly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
    if not loc:viewonly and not loc:readonly
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:LookupButton,loc:formname,p_web._MakeURL('SelectShelfLocation?LookupField=locShelfLocation&Tab=1&ForeignField=los:Site_Location&_sort=los:Shelf_Location&Refresh=sort&LookupFrom=FormRapidLoanExchangeInsert&'),) !lookupextra
    End
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRapidLoanExchangeInsert_' & p_web._nocolon('locShelfLocation') & '_value')


Prompt::locAllocateAccessories  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('locAllocateAccessories') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Allocate Accessories')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locAllocateAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locAllocateAccessories',p_web.GetValue('NewValue'))
    locAllocateAccessories = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locAllocateAccessories
  ElsIf p_web.IfExistsValue('Value')
    if p_web.GetValue('Value') = ''
      p_web.SetValue('Value',0)
    end
    p_web.SetSessionValue('locAllocateAccessories',p_web.GetValue('Value'))
    locAllocateAccessories = p_web.GetValue('Value')
  End
  do Value::locAllocateAccessories
  do SendAlert

Value::locAllocateAccessories  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('locAllocateAccessories') & '_value','adiv')
  loc:extra = ''
  ! --- CHECKBOX --- locAllocateAccessories
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locAllocateAccessories'',''formrapidloanexchangeinsert_locallocateaccessories_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  If p_web.GetSessionValue('locAllocateAccessories') = 1
    loc:readonly = 'checked ' & loc:readonly
  End
  packet = clip(packet) & p_web.CreateInput('checkbox','locAllocateAccessories',clip(1),,loc:readonly,,,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRapidLoanExchangeInsert_' & p_web._nocolon('locAllocateAccessories') & '_value')


Validate::btnNext  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnNext',p_web.GetValue('NewValue'))
    do Value::btnNext
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnNext  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('btnNext') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnNext','Next','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormRapidLoanExchangeInsert?' &'tab=2')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/listnext.png',,,,)

  do SendPacket
  p_web._DivFooter()


Prompt::locIMEINumber  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('locIMEINumber') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('I.M.E.I. Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRapidLoanExchangeInsert_' & p_web._nocolon('locIMEINumber') & '_prompt')

Validate::locIMEINumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('NewValue'))
    locIMEINumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIMEINumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locIMEINumber',p_web.GetValue('Value'))
    locIMEINumber = p_web.GetValue('Value')
  End
  If locIMEINumber = ''
    loc:Invalid = 'locIMEINumber'
    loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locIMEINumber = Upper(locIMEINumber)
    p_web.SetSessionValue('locIMEINumber',locIMEINumber)
    p_web.SSV('ConfirmIMEINo',1)  
  do Value::locIMEINumber
  do SendAlert
  do Prompt::locIMEINumber

Value::locIMEINumber  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('locIMEINumber') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locIMEINumber
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(p_web.GSV('imeiOK') = 1,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If p_web.GSV('imeiOK') = 1
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  End
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locIMEINumber')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locIMEINumber = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&clip('showIMEIValidation()')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('imeiNumber')&''',2);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locIMEINumber',p_web.GetSessionValueFormat('locIMEINumber'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRapidLoanExchangeInsert_' & p_web._nocolon('locIMEINumber') & '_value')


Validate::btnClear  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnClear',p_web.GetValue('NewValue'))
    do Value::btnClear
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    ResetIMEINo()  
  do SendAlert
  do Prompt::locIMEINumber
  do Value::locIMEINumber  !1
  do Prompt::locMSN
  do Value::locMSN  !1
  do Value::btnClear  !1

Value::btnClear  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('btnClear') & '_value',Choose(p_web.GSV('imeiOK') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('imeiOK') <> 1)
  ! --- DISPLAY --- 
  loc:javascript = ' onfocus="'&clip('document.getElementById(''locIMEINumber'').focus()')&'"'
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnClear'',''formrapidloanexchangeinsert_btnclear_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnClear','Clear','SmallButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRapidLoanExchangeInsert_' & p_web._nocolon('btnClear') & '_value')


Validate::hidden2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('hidden2',p_web.GetValue('NewValue'))
    do Value::hidden2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::hidden2  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('hidden2') & '_value','adiv')
  loc:extra = ''
  ! --- HIDDEN --- 
    packet = clip(packet) & p_web.CreateInput('hidden','',p_web.GetSessionValue('')) & '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locMSN  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('locMSN') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('MSN')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRapidLoanExchangeInsert_' & p_web._nocolon('locMSN') & '_prompt')

Validate::locMSN  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locMSN',p_web.GetValue('NewValue'))
    locMSN = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locMSN
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locMSN',p_web.GetValue('Value'))
    locMSN = p_web.GetValue('Value')
  End
  If locMSN = ''
    loc:Invalid = 'locMSN'
    loc:alert = p_web.translate('MSN') & ' ' & p_web.translate(p_web.site.RequiredText)
  End
    locMSN = Upper(locMSN)
    p_web.SetSessionValue('locMSN',locMSN)
    IF (p_web.GSV('locMSN') <> '' AND p_web.GSV('locIMEINumber') <> '')
        IF (p_web.GSV('locAllocateAccessories') <> 1)
            ProcessIMEI()
        END ! IF
    END ! IF
  do Value::locMSN
  do SendAlert
  do Value::locIMEIsInserted  !1
  do Prompt::locIMEINumber
  do Value::locIMEINumber  !1
  do Prompt::locMSN
  do Value::btnClear  !1

Value::locMSN  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('locMSN') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locMSN
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = Choose(loc:viewonly,'readonly','')
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formrqd'
  If lower(loc:invalid) = lower('locMSN')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  ElsIf loc:retrying ! any field on the form is invalid
    If locMSN = ''
      loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
    End
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(30) &'"'
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locMSN'',''formrapidloanexchangeinsert_locmsn_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locIMEINumber')&''',2);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locMSN',p_web.GetSessionValueFormat('locMSN'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),,loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRapidLoanExchangeInsert_' & p_web._nocolon('locMSN') & '_value')


Prompt::brwTagAccessories  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('brwTagAccessories') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Select Accessories')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::brwTagAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwTagAccessories',p_web.GetValue('NewValue'))
    do Value::brwTagAccessories
  Else
    p_web.StoreValue('acr:Accessory')
  End

Value::brwTagAccessories  Routine
  loc:extra = ''
  ! --- BROWSE ---  TagValidateLoanAccessories --
  p_web.SetValue('TagValidateLoanAccessories:NoForm',1)
  p_web.SetValue('TagValidateLoanAccessories:FormName',loc:formname)
  p_web.SetValue('TagValidateLoanAccessories:parentIs','Form')
  p_web.SetValue('_parentProc','FormRapidLoanExchangeInsert')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('FormRapidLoanExchangeInsert_TagValidateLoanAccessories_embedded_div')&'"><!-- Net:TagValidateLoanAccessories --></div><13,10>'
    p_web._DivHeader('FormRapidLoanExchangeInsert_' & lower('TagValidateLoanAccessories') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('FormRapidLoanExchangeInsert_' & lower('TagValidateLoanAccessories') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagValidateLoanAccessories --><13,10>'
  end
  do SendPacket


Validate::btnAddUnit  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnAddUnit',p_web.GetValue('NewValue'))
    do Value::btnAddUnit
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    IF ((p_web.GSV('locMSN') <> '' OR p_web.GSV('Hide:MSN') = 1) AND p_web.GSV('locIMEINumber') <> '')
        ProcessIMEI()
    END ! IF  
  do SendAlert
  do Value::locIMEINumber  !1
  do Value::locMSN  !1
  do Value::brwTagAccessories  !1
  do Value::locIMEIsInserted  !1
  do Value::btnClear  !1

Value::btnAddUnit  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('btnAddUnit') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnAddUnit'',''formrapidloanexchangeinsert_btnaddunit_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnAddunit','Insert Loan Unit','button-entryfield',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRapidLoanExchangeInsert_' & p_web._nocolon('btnAddUnit') & '_value')


Validate::__line  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__line',p_web.GetValue('NewValue'))
    do Value::__line
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__line  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('__line') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::txtINserted  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtINserted',p_web.GetValue('NewValue'))
    do Value::txtINserted
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtINserted  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('txtINserted') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('bold')&'">' & p_web.Translate('Units Successfully Inserted:',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()


Prompt::locIMEIsInserted  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('locIMEIsInserted') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locIMEIsInserted  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locIMEIsInserted',p_web.GetValue('NewValue'))
    locIMEIsInserted = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locIMEIsInserted
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locIMEIsInserted',p_web.GetValue('Value'))
    locIMEIsInserted = p_web.GetValue('Value')
  End

Value::locIMEIsInserted  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('locIMEIsInserted') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locIMEIsInserted
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('green bold LargeText')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locIMEIsInserted'),1) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('FormRapidLoanExchangeInsert_' & p_web._nocolon('locIMEIsInserted') & '_value')


Validate::__line2  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('__line2',p_web.GetValue('NewValue'))
    do Value::__line2
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::__line2  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('__line2') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()


Validate::btnBack  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnBack',p_web.GetValue('NewValue'))
    do Value::btnBack
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::btnBack  Routine
  p_web._DivHeader('FormRapidLoanExchangeInsert_' & p_web._nocolon('btnBack') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnBack','Back','MainButton',loc:formname,,,'window.open('''& p_web._MakeURL(clip('FormRapidLoanExchangeInsert?' &'tab=1')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/listback.png',,,,)

  do SendPacket
  p_web._DivFooter()


CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('FormRapidLoanExchangeInsert_locStockType_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locStockType
      else
        do Value::locStockType
      end
  of lower('FormRapidLoanExchangeInsert_locModelNumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locModelNumber
      else
        do Value::locModelNumber
      end
  of lower('FormRapidLoanExchangeInsert_locShelfLocation_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locShelfLocation
      else
        do Value::locShelfLocation
      end
  of lower('FormRapidLoanExchangeInsert_locAllocateAccessories_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locAllocateAccessories
      else
        do Value::locAllocateAccessories
      end
  of lower('FormRapidLoanExchangeInsert_locIMEINumber_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locIMEINumber
      else
        do Value::locIMEINumber
      end
  of lower('FormRapidLoanExchangeInsert_btnClear_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnClear
      else
        do Value::btnClear
      end
  of lower('FormRapidLoanExchangeInsert_locMSN_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locMSN
      else
        do Value::locMSN
      end
  of lower('FormRapidLoanExchangeInsert_btnAddUnit_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnAddUnit
      else
        do Value::btnAddUnit
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('FormRapidLoanExchangeInsert_form:ready_',1)
  p_web.SetSessionValue('FormRapidLoanExchangeInsert_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_FormRapidLoanExchangeInsert',0)

PreCopy  Routine
  p_web.SetValue('FormRapidLoanExchangeInsert_form:ready_',1)
  p_web.SetSessionValue('FormRapidLoanExchangeInsert_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_FormRapidLoanExchangeInsert',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('FormRapidLoanExchangeInsert_form:ready_',1)
  p_web.SetSessionValue('FormRapidLoanExchangeInsert_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('FormRapidLoanExchangeInsert:Primed',0)

PreDelete       Routine
  p_web.SetValue('FormRapidLoanExchangeInsert_form:ready_',1)
  p_web.SetSessionValue('FormRapidLoanExchangeInsert_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('FormRapidLoanExchangeInsert:Primed',0)
  p_web.setsessionvalue('showtab_FormRapidLoanExchangeInsert',0)

LoadRelatedRecords  Routine
    p_web.GetDescription(STOCKTYP,stp:Stock_Type_Key,,stp:Stock_Type,,p_web.GetSessionValue('locStockType'))
    p_web.FileToSessionQueue(STOCKTYP)
    p_web.GetDescription(MODELNUM,mod:Model_Number_Key,,mod:Model_Number,,p_web.GetSessionValue('locModelNumber'))
    p_web.FileToSessionQueue(MODELNUM)
    p_web.GetDescription(LOCSHELF,los:Shelf_Location_Key,,los:Site_Location,,p_web.GetSessionValue('locShelfLocation'))
    p_web.FileToSessionQueue(LOCSHELF)
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('tab') = 1
    If (p_web.GSV('type') = 'LOA')
          If p_web.IfExistsValue('locAllocateAccessories') = 0
            p_web.SetValue('locAllocateAccessories',0)
            locAllocateAccessories = 0
          End
    End
  End
  If p_web.GSV('tab') = 2
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('FormRapidLoanExchangeInsert_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('FormRapidLoanExchangeInsert_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 1
  If p_web.GSV('tab') = 1
    loc:InvalidTab += 1
        If locStockType = ''
          loc:Invalid = 'locStockType'
          loc:alert = p_web.translate('Stock Type') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locStockType = Upper(locStockType)
          p_web.SetSessionValue('locStockType',locStockType)
        If loc:Invalid <> '' then exit.
        If locModelNumber = ''
          loc:Invalid = 'locModelNumber'
          loc:alert = p_web.translate('Model Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locModelNumber = Upper(locModelNumber)
          p_web.SetSessionValue('locModelNumber',locModelNumber)
        If loc:Invalid <> '' then exit.
        If locShelfLocation = ''
          loc:Invalid = 'locShelfLocation'
          loc:alert = p_web.translate('Shelf Location') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locShelfLocation = Upper(locShelfLocation)
          p_web.SetSessionValue('locShelfLocation',locShelfLocation)
        If loc:Invalid <> '' then exit.
  End
  ! tab = 2
  If p_web.GSV('tab') = 2
    loc:InvalidTab += 1
        If locIMEINumber = ''
          loc:Invalid = 'locIMEINumber'
          loc:alert = p_web.translate('I.M.E.I. Number') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locIMEINumber = Upper(locIMEINumber)
          p_web.SetSessionValue('locIMEINumber',locIMEINumber)
        If loc:Invalid <> '' then exit.
    If p_web.GSV('Hide:MSN') <> 1
        If locMSN = ''
          loc:Invalid = 'locMSN'
          loc:alert = p_web.translate('MSN') & ' ' & p_web.translate(p_web.site.RequiredText)
        End
          locMSN = Upper(locMSN)
          p_web.SetSessionValue('locMSN',locMSN)
        If loc:Invalid <> '' then exit.
    End
  End
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('FormRapidLoanExchangeInsert:Primed',0)
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locStockType')
  p_web.StoreValue('locModelNumber')
  p_web.StoreValue('locShelfLocation')
  p_web.StoreValue('locAllocateAccessories')
  p_web.StoreValue('')
  p_web.StoreValue('locIMEINumber')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locMSN')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locIMEIsInserted')
  p_web.StoreValue('')
  p_web.StoreValue('')
imeivalidation  Routine
  packet = clip(packet) & |
    '<<div id="div-background-imeivalidation" style="display:none;"><13,10>'&|
    '<<div id="div-back-imeivalidation"><13,10>'&|
    '<</div><13,10>'&|
    '  <<div id="div-front-imeivalidation"><13,10>'&|
    '    <<p class="SubHeading">Confirm I.M.E.I. Number<</p><13,10>'&|
    '    <<input id="imeiNumber" name="imeiNumber" type="text" action="validateIMEINumber();" size="30" class="Upper FormEntry formrqd"/><13,10>'&|
    '    <<br/><13,10>'&|
    '    <<br/><13,10>'&|
    '    <13,10>'&|
    '    <<button id="okButton" onclick="validateIMEINumber();" class="MessageBoxButton">Confirm IMEI<</button><13,10>'&|
    '    <<button id="cancelButton" onclick="window.open(''FormRapidLoanExchangeInsert?tab=2'',''_self'');" class="MessageBoxButton">Cancel<</button><13,10>'&|
    '  <</div><13,10>'&|
    '<</div><13,10>'&|
    ''
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locStockType',locStockType) ! STRING(30)
     p_web.SSV('locManufacturer',locManufacturer) ! STRING(30)
     p_web.SSV('locModelNumber',locModelNumber) ! STRING(30)
     p_web.SSV('locShelfLocation',locShelfLocation) ! STRING(30)
     p_web.SSV('locAllocateAccessories',locAllocateAccessories) ! LONG
     p_web.SSV('locIMEINumber',locIMEINumber) ! STRING(30)
     p_web.SSV('locMSN',locMSN) ! STRING(30)
     p_web.SSV('locIMEIsInserted',locIMEIsInserted) ! STRING(2000000)
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locStockType = p_web.GSV('locStockType') ! STRING(30)
     locManufacturer = p_web.GSV('locManufacturer') ! STRING(30)
     locModelNumber = p_web.GSV('locModelNumber') ! STRING(30)
     locShelfLocation = p_web.GSV('locShelfLocation') ! STRING(30)
     locAllocateAccessories = p_web.GSV('locAllocateAccessories') ! LONG
     locIMEINumber = p_web.GSV('locIMEINumber') ! STRING(30)
     locMSN = p_web.GSV('locMSN') ! STRING(30)
     locIMEIsInserted = p_web.GSV('locIMEIsInserted') ! STRING(2000000)
ProcessIMEI         PROCEDURE()
FoundExchangeNo         LONG()
RetValue                LONG(Level:Benign)
errorFound              LONG()
    CODE
        LOOP 1 TIMES
            IF (CheckLength('IMEI',p_web.GSV('locModelNumber'),p_web.GSV('locIMEINumber')))
                loc:alert = 'I.M.E.I. Number is invalid.'
                loc:invalid = 'locIMEINumber'
                ResetIMEINo()
                BREAK
            END ! IF
        
            IF (IsIMEIValid(p_web.GSV('locIMEINumber'),p_web.GSV('locModelNumber')) = FALSE)
                loc:alert = 'I.M.E.I. Number is invalid.'
                loc:invalid = 'locIMEINumber'
                ResetIMEINo()
                BREAK
            END ! IF
            
            IF (p_web.GSV('Hide:MSN') = 0)
                IF (CheckLength('MSN',p_web.GSV('locModelNumber'),p_web.GSV('locMSN')))
                    loc:alert = 'MSN is invalid.'
                    loc:invalid = 'locMSN'
                    p_web.SSV('locMSN','')
                    BREAK
                END ! IF
                
                ! #13554 Check MSN Format (DBH: 24/08/2015)
                Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                man:Manufacturer = p_web.GSV('locManufacturer')
                IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
                    IF (man:ApplyMSNFormat)
                        IF (CheckFaultFormat(p_web.GSV('locMSN'),man:MSNFormat))
                            loc:alert = 'MSN Format is invalid.'
                            loc:invalid = 'locMSN'
                            p_web.SSV('locMSN','')
                            BREAK
                        END ! IF                       
                    END ! IF
                END ! IF
            END ! IF
            
            ! Exchange Or Loan?
            CASE p_web.GSV('type')
            OF 'EXC'
                errorFound = FALSE
                Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
                xch:ESN = p_web.GSV('locIMEINumber')
                SET(xch:ESN_Only_Key,xch:ESN_Only_Key)
                LOOP UNTIL Access:EXCHANGE.Next() <> Level:Benign
                    IF (xch:ESN <> p_web.GSV('locIMEINumber'))
                        BREAK
                    END ! IF
                    IF (xch:Location = 'RETURN TO MANUFACTURER' AND xch:Available = 'NOA')
                        FoundExchangeNo = xch:Ref_Number
                    ELSE
                        loc:alert = 'This I.M.E.I. Number already exists in Exchange Stock.'
                        loc:invalid = 'locIMEINumber'
                        ResetIMEINo()
                        errorFound = TRUE
                        BREAK
                    
                    END ! IF
                END ! LOOP
            
                IF (errorFound = TRUE)
                    BREAK
                END ! IF
        
                Access:LOAN.ClearKey(loa:ESN_Only_Key)
                loa:ESN = p_web.GSV('locIMEINumber')
                IF (Access:LOAN.TryFetch(loa:ESN_Only_Key) = Level:Benign)
                    loc:alert = 'This I.M.E.I. No has already been entered as a Loan Stock Item.'
                    loc:invalid = 'locIMEINumber'
                    ResetIMEINo()
                    BREAK
                END ! IF    
            
                Access:JOBS.ClearKey(job:ESN_Key)
                job:ESN = p_web.GSV('locIMEINumber')
                IF (Access:JOBS.TryFetch(job:ESN_Key) = Level:Benign)
                    IF (job:Date_Completed = '')
                        loc:alert = 'You are attempting to insert an Exchange Unit with the same I.M.E.I. Number as job ' & job:Ref_Number & |
                            '.<13,10,13,10>You must use Rapid Job Update if you wish to return this unit to Exchange Stock.'
                        loc:invalid = 'locIMEINumber'
                        ResetIMEINo()
                        BREAK
                    END ! IF
                END ! IF
        
                IF (p_web.GSV('Hide:MSN') = 0)
                    Access:EXCHANGE.ClearKey(xch:MSN_Only_Key)
                    xch:MSN = p_web.GSV('locMSN')
                    IF (Access:EXCHANGE.TryFetch(xch:MSN_Only_Key) = Level:Benign)
                        loc:alert = 'This MSN already exists in Exchange Stock.'
                        loc:invalid = 'locMSN'
                        p_web.SSV('locMSN','')
                        BREAK
                    END ! IF
                END
        
                IF (FoundExchangeNo > 0)
                    Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                    xch:Ref_Number = FoundExchangeNo
                    IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                
                    END ! IF
                ELSE ! IF
                    IF (Access:EXCHANGE.PrimeRecord())
                        BREAK
                    END ! IF
                END ! IF
        
                xch:Model_Number = p_web.GSV('locModelNumber')
                xch:Manufacturer = p_web.GSV('locManufacturer')
                xch:ESN  = p_web.GSV('locIMEINumber')
                IF (p_web.GSV('Hide:MSN') <> 1)
                    xch:MSN = p_web.GSV('locMSN')
                END ! IF
                xch:Location = p_web.GSV('BookingSiteLocation')
                xch:Shelf_Location = p_web.GSV('locShelfLocation')
                xch:Date_Booked = TODAY()
                xch:Available = 'AVL'
                xch:StatusChangeDate = TODAY()
                xch:Stock_Type = p_web.GSV('locStockType')
        
                IF (FoundExchangeNo > 0)
                    IF (Access:EXCHANGE.TryUpdate())
                        loc:alert = 'Unable to update Exchange Unit.'
                        loc:invalid = 'locIMEINumber'
                        BREAK
                    END ! F
                    Access:EXCHHIST.ClearKey(exh:Ref_Number_Key)
                    exh:Ref_Number = xch:Ref_Number
                    SET(exh:Ref_Number_Key,exh:Ref_Number_Key)
                    LOOP UNTIL Access:EXCHHIST.Next() <> Level:Benign
                        IF (exh:Ref_Number <> xch:Ref_Number)
                            BREAK
                        END ! IF
                        exh:Notes = 'HIDDEN FROM FRANCHISE: ' & CLIP(exh:Notes)
                        Access:EXCHHIST.TryUpdate()
                    END ! LOOP
                ELSE ! IF
                    IF (Access:EXCHANGE.TryINsert())
                        loc:alert = 'Unable to insert Exchange Unit.'
                        loc:invalid = 'locIMEINumber'
                        Access:EXCHANGE.CancelAutoInc()
                        BREAK
                    END ! IF
                END ! IF
        
                IF (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                    exh:Ref_Number = xch:Ref_Number
                    exh:Date = TODAY()
                    exh:Time = CLOCK()
                    exh:User = p_web.GSV('BookingUserCode')
                    exh:Status = 'INITIAL ENTRY'
                    IF (Access:EXCHHIST.TryInsert())
                        Access:EXCHHIST.CancelAutoInc()
                    END ! IF
                END ! IF
        

            OF 'LOA'
                errorFound = FALSE

                Access:LOAN.ClearKey(loa:ESN_Only_Key)
                loa:ESN = p_web.GSV('locIMEINumber')
                SET(loa:ESN_Only_Key,loa:ESN_Only_Key)
                LOOP UNTIL Access:LOAN.Next() <> Level:Benign
                    IF (loa:ESN <> p_web.GSV('locIMEINumber'))
                        BREAK
                    END ! IF
                    loc:alert = 'This I.M.E.I. Number already exists in Loan Stock.'
                    loc:invalid = 'locIMEINumber'
                    errorFound = TRUE
                    ResetIMEINo()
                    BREAK
                END ! LOOP

                IF (errorFound = TRUE)
                    BREAK
                END ! IF

                Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
                xch:ESN = p_web.GSV('locIMEINumber')
                IF (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)
                    loc:alert = 'This I.M.E.I. Number has already been entered as an Exchange Stock Item.'
                    loc:invalid = 'locIMEINumber'
                    ResetIMEINo()
                    BREAK
                END ! IF

                Access:JOBS.ClearKey(job:ESN_Key)
                job:ESN = p_web.GSV('locIMEINumber')
                IF (Access:JOBS.TryFetch(job:ESN_Key) = Level:Benign)
                    IF (job:Date_Completed = '')
                        loc:alert = 'You are attempting to insert an Loan Unit with the same I.M.E.I. Number as job ' & job:Ref_Number & |
                            '.<13,10,13,10>You must use Rapid Job Update if you wish to return this unit to Loan Stock.'
                        loc:invalid = 'locIMEINumber'
                        ResetIMEINo()
                        BREAK        
                    END ! IF
                END ! IF

                IF (p_web.GSV('Hide:MSN') = 0)
                    Access:LOAN.ClearKey(loa:MSN_Only_Key)
                    loa:MSN = p_web.GSV('locMSN')
                    IF (Access:LOAN.TryFetch(loa:MSN_Only_Key) = Level:Benign)
                        loc:alert = 'This MSN already exists in Loan Stock.'
                        loc:invalid = 'locMSN'
                        p_web.SSV('locMSN','')
                        BREAK
                    END ! IF

                END ! IF

                IF (Access:LOAN.PrimeRecord() = Level:Benign)
                    loa:Model_Number     = p_web.GSV('locModelNumber')
                    loa:Manufacturer    = p_web.GSV('locManufacturer')
                    loa:ESN = p_web.GSV('locIMEINumber')
                    IF (p_web.GSV('Hide:MSN') = 0)
                        loa:MSN = p_web.GSV('locMSN')
                    END ! IF
                    loa:Location     = p_web.GSV('BookingSiteLocation')
                    loa:Shelf_Location = p_web.GSV('locShelfLocation')
                    loa:Date_Booked = TODAY()
                    loa:Available = 'AVL'
                    loa:Stock_Type = p_web.GSV('locStockType')
                    loa:StatusChangeDate = TODAY()
                    IF (Access:LOAN.TryInsert())
                        loc:alert = 'Unable to insert Loan Unit.'
                        loc:invalid = 'locIMEINumber'
                        Access:LOAN.CancelAutoInc()
                        BREAK
                    END ! IF
                    IF (Access:LOANHIST.PrimeRecord() = Level:Benign)
                        loh:Ref_Number = loa:Ref_Number
                        loh:Date = TODAY()
                        loh:Time = CLOCK()
                        loh:User = p_web.GSV('BookingUserCode')
                        loh:Status = 'INITIAL ENTRY'
                        IF (Access:LOANHIST.TryInsert())
                            Access:LOANHIST.CancelAutoInc()
                        END ! IF
                    END ! IF
                    IF (p_web.GSV('locAllocateAccessories'))
                        Access:Tagging.ClearKey(tgg:SessionIDKey)
                        tgg:SessionID = p_web.SessionID
                        SET(tgg:SessionIDKey,tgg:SessionIDKey)
                        LOOP UNTIL Access:Tagging.Next() <> Level:Benign
                            IF (tgg:SessionID <> p_web.SessionID)
                                BREAK
                            END ! IF
                            IF (tgg:Tagged = 0)
                                CYCLE
                            END ! IF
                            IF (Access:LOANACC.PrimeRecord() = Level:Benign)
                                lac:Ref_Number = loa:Ref_Number
                                lac:Accessory = tgg:TaggedValue
                                lac:Accessory_Status = ''
                                IF (Access:LOANACC.TryInsert())
                                    Access:LOANACC.CancelAutoInc()
                                END ! IF
                            END ! IF    
                        END ! LOOP
                        
!                        Access:TagFile.ClearKey(tag:KeyTagged)
!                        tag:SessionID = p_web.SessionID
!                        SET(tag:KeyTagged,tag:KeyTagged)
!                        LOOP UNTIL Access:TagFile.Next() <> Level:Benign
!                            IF (tag:SessionID <> p_web.SessionID)
!                                BREAK
!                            END ! IF
!                            IF (Access:LOANACC.PrimeRecord() = Level:Benign)
!                                lac:Ref_Number = loa:Ref_Number
!                                lac:Accessory = tag:TaggedValue
!                                lac:Accessory_Status = ''
!                                IF (Access:LOANACC.TryInsert())
!                                    Access:LOANACC.CancelAutoInc()
!                                END ! IF
!                            END ! IF
!                        END ! LOOP
                    END ! IF
                END ! IF
            END ! CASE    
            p_web.SSV('locIMEIsInserted',p_web.GSV('locIMEINumber') & '<br/>' & p_web.GSV('locIMEIsInserted'))
            ResetIMEINo()              
        END ! LOOP
ResetIMEINo         PROCEDURE()
    CODE
        p_web.SSV('locIMEINumber','')
        p_web.SSV('locMSN','')
        p_web.SSV('imeiOK',0)
        p_web.SetValue('SelectField',clip(loc:formname) & '.locIMEINumber')
        IF (p_web.GSV('locAllocateAccessories') = 1)
            ClearTaggingFile(p_web)
        END ! IF
