

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER438.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ClearSBOPartsList    PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
    DO OpenFiles
    Access:SBO_OutParts.Clearkey(sout:PartNumberKey)
    sout:SessionID = p_web.SessionID
    SET(sout:PartNumberKey,sout:PartNumberKey)
    LOOP UNTIL Access:SBO_OutParts.Next()
        IF (sout:SessionID <> p_web.SessionID)
            BREAK
        END

        Access:SBO_OutParts.DeleteRecord(0)
    END

    DO CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:SBO_OutParts.Open                                 ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:SBO_OutParts.UseFile                              ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:SBO_OutParts.Close
     FilesOpened = False
  END
