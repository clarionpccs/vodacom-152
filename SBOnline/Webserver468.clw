

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER468.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER076.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER077.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER185.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER201.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER269.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER270.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER271.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER280.INC'),ONCE        !Req'd for module callout resolution
                     END


DespatchConfirmation PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
locCompanyName       STRING(30)                            !
locCourierCost       REAL                                  !
locLabourCost        REAL                                  !
locPartsCost         REAL                                  !
locSubTotal          REAL                                  !
locVAT               REAL                                  !
locTotal             REAL                                  !
locInvoiceNumber     REAL                                  !
locInvoiceDate       DATE                                  !
locLabourVatRate     REAL                                  !
locPartsVatRate      REAL                                  !
locCourier           STRING(30)                            !
                    MAP
DespatchProcess         PROCEDURE(),LONG
                    END ! MAP
errorMessage                STRING(255)
FilesOpened     Long
EXCHHIST::State  USHORT
LOANHIST::State  USHORT
EXCHANGE::State  USHORT
WAYBCONF::State  USHORT
LOAN::State  USHORT
JOBSCONS::State  USHORT
COURIER::State  USHORT
VATCODE::State  USHORT
INVOICE::State  USHORT
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
JOBSE::State  USHORT
JOBS::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
job:Courier_OptionView   View(COURIER)
                          Project(cou:Courier)
                        End
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('DespatchConfirmation')
  loc:formname = 'DespatchConfirmation_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'DespatchConfirmation',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('DespatchConfirmation','')
    p_web._DivHeader('DespatchConfirmation',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferDespatchConfirmation',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDespatchConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDespatchConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_DespatchConfirmation',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferDespatchConfirmation',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_DespatchConfirmation',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'DespatchConfirmation',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
DeleteSessionValues ROUTINE ! Auto generated by Bryan's Template
    ! Local Variables
    p_web.DeleteSessionValue('Ans')
    p_web.DeleteSessionValue('locCompanyName')
    p_web.DeleteSessionValue('locCourierCost')
    p_web.DeleteSessionValue('locLabourCost')
    p_web.DeleteSessionValue('locPartsCost')
    p_web.DeleteSessionValue('locSubTotal')
    p_web.DeleteSessionValue('locVAT')
    p_web.DeleteSessionValue('locTotal')
    p_web.DeleteSessionValue('locInvoiceNumber')
    p_web.DeleteSessionValue('locInvoiceDate')
    p_web.DeleteSessionValue('locLabourVatRate')
    p_web.DeleteSessionValue('locPartsVatRate')
    p_web.DeleteSessionValue('locCourier')

    ! Other Variables
!DespatchProcess     Routine
!DATA
!locWaybillNumber    STRING(30)
!CODE
!    p_web.SSV('Hide:PrintDespatchNote',1)
!    p_web.SSV('Hide:PrintWaybill',1)
!    
!    p_web._Trace('DespatchConfirmation: cou:PrintWaybill = ' & p_web.GSV('cou:PrintWaybill'))
!    p_web._Trace('DespatchConfirmation: DespatchType = ' & p_web.GSV('DespatchType'))
!    
!    IF (p_web.GSV('cou:PrintWaybill') = 1)
!            
!        p_web.SSV('Hide:PrintWaybill',0)
!            
!        locWaybillNumber = NextWaybillNumber()
!        IF (locWaybillNumber = 0)
!            EXIT
!        END
!        
!        p_web._Trace('DespatchConfirmation: locWaybillNumber = ' & locWaybillNumber)
!        
!        Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
!        way:WayBillNumber = locWaybillNumber
!        IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey))
!            EXIT
!        END
!        
!        IF (p_web.GSV('BookingSite') = 'RRC')
!
!            way:AccountNumber = p_web.GSV('BookingAccount')
!            CASE p_web.GSV('DespatchType')
!            OF 'JOB'
!                way:WaybillID = 2
!                p_web.SSV('wob:JobWaybillNumber',locWaybillNumber)
!            OF 'EXC'
!                way:WaybillID = 3
!                p_web.SSV('wob:ExcWaybillNumber',way:WayBillNumber)
!            OF 'LOA'
!                way:WaybillID = 4
!                p_web.SSV('wob:LoaWaybillNumber',way:WayBillNumber)
!            END
!            
!            IF (p_web.GSV('job:Who_Booked') = 'WEB')
!                ! PUP Job
!                way:WayBillType = 9
!                way:WaybillID = 21
!            ELSE
!                way:WayBillType = 2
!            END
!            
!            way:FromAccount = p_web.GSV('BookingAccount')
!            way:ToAccount = p_web.GSV('job:Account_Number')
!            
!        ELSE
!            way:WayBillType = 1 ! Created From RRC
!            way:AccountNumber = p_web.GSV('wob:HeadAccountNumber')
!            way:FromAccount = p_web.GSV('ARC:AccountNumber')
!            IF (p_web.GSV('jobe:WebJob') = 1)
!                way:ToAccount = p_web.GSV('wob:HeadAccountNumber')
!                Case p_web.GSV('DespatchType')
!                of 'JOB'
!                    way:WaybillID = 5 ! ARC to RRC (JOB)
!                of 'EXC'
!                    way:WaybillID = 6 ! ARC to RRC (EXC)
!                END
!                
!            ELSE
!                way:ToAccount = job:Account_Number
!                Case p_web.GSV('DespatchType')
!                OF 'JOB'
!                    way:WaybillID = 7 ! ARC To Customer (JOB)
!                of 'EXC'
!                    way:WaybillID = 8 ! ARC To Customer (EXC)
!                of 'LOA'
!                    way:WaybillID = 9 ! ARC To Customer (LOA)
!                END
!                
!            END
!            
!        END
!        
!            
!        IF (Access:WAYBILLS.TryUpdate())
!            EXIT
!        END
!        
!        IF (Access:WAYBCONF.PrimeRecord() = Level:Benign)
!            ! Add to waybill staging table. (Add all records, 
!            ! the wrong ones will be deleted later) (DBH: 21/04/2015)
!            wac:AccountNumber = way:AccountNumber
!            wac:WaybillNo = way:WayBillNumber
!            wac:GenerateDate = way:TheDate
!            wac:GenerateTime = way:TheTime
!            wac:ConfirmationSent = 0
!            IF (Access:WAYBCONF.TryInsert())
!                Access:WAYBCONF.CancelAutoInc()
!            END!  IF
!        END ! IF
!        
!            
!        IF (Access:WAYBILLJ.PrimeRecord() = Level:Benign)
!            waj:WayBillNumber = way:WayBillNumber
!            waj:JobNumber = p_web.GSV('job:Ref_Number')
!            
!            
!            CASE p_web.GSV('DespatchType')
!            OF 'JOB'
!                waj:IMEINumber = p_web.GSV('job:ESN')
!            OF 'EXC'
!                Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
!                xch:Ref_Number = p_web.GSV('job:Exchange_Unit_Number')
!                IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)  ! #13625 Using the wrong key (DBH: 30/10/2015)
!                    waj:IMEINumber = xch:ESN
!                END
!                p_web._Trace('DespatchConfirmation: waj:IMEINumber ' & waj:IMEINumber)        
!            OF 'LOA'
!                Access:LOAN.ClearKey(loa:Ref_Number_Key)
!                loa:Ref_Number = p_web.GSV('job:Loan_Unit_Number')
!                IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
!                    waj:IMEINumber = loa:ESN
!                END
!                    
!            END
!            waj:OrderNumber = p_web.GSV('job:Order_Number')
!            waj:SecurityPackNumber = p_web.GSV('locSecurityPackID')
!            waj:JobType = p_web.GSV('DespatchType')
!            IF (Access:WAYBILLJ.TryUpdate() = Level:Benign)
!            ELSE
!                Access:WAYBILLJ.CancelAutoInc()
!            END
!        END
!            
!    ELSE ! IF (p_web.GSV('cou:PrintWaybill') = 1)
!        if (p_web.GSV('cou:AutoConsignmentNo') = 1)
!            Access:COURIER.Clearkey(cou:Courier_Key)
!            cou:Courier = p_web.GSV('cou:Courier')
!            if (Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign)
!                cou:LastConsignmentNo += 1
!                Access:COURIER.TryUpdate()
!                locWaybillNumber = cou:LastConsignmentNo
!            end
!        ELSE
!            locWaybillNumber = p_web.GSV('locConsignmentNumber')
!        end
!        p_web.SSV('Hide:PrintDespatchNote',0)
!            
!    END ! IF (p_web.GSV('cou:PrintWaybill') = 1)
!            
!    p_web.SSV('locWaybillNumber',locWaybillNumber)
!    
!    CASE p_web.GSV('DespatchType')
!    OF 'JOB'
!        !Despatch:Job(locWayBillNumber)
!        Despatch:Job(p_web)
!        
!    OF 'EXC'
!        Despatch:Exc(p_web)                
!                
!    OF 'LOA'
!        Despatch:Loa(p_web)
!        
!    END
!
!    ! Save Files
!    Access:JOBS.ClearKey(job:Ref_Number_Key)
!    job:Ref_Number = p_web.GSV('job:Ref_Number')
!    IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
!        p_web.SessionQueueToFile(JOBS)
!        IF (Access:JOBS.TryUpdate() = Level:Benign)
!            
!            Access:WEBJOB.ClearKey(wob:RefNumberKey)
!            wob:RefNumber = job:Ref_Number
!            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
!                p_web.SessionQueueToFile(WEBJOB)
!                IF (Access:WEBJOB.TryUpdate() = Level:Benign)
!                    
!                    Access:JOBSE.ClearKey(jobe:RefNumberKey)
!                    jobe:RefNumber = job:Ref_Number
!                    IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
!                        p_web.SessionQueueToFile(JOBSE)
!                        IF (Access:JOBSE.TryUpdate() = Level:Benign)
!                            
!                        END
!                    END
!                END
!            END
!        END
!    END
    
    
    
    
ShowHidePaymentDetails      ROUTINE
DATA
locPaymentStringRRC EQUATE('PAYMENT TYPES')
locPaymentStringARC EQUATE('PAYMENT DETAILS')
locPaymentString    CSTRING(30)
CODE
    
    paymentFail# = 0
        
    IF (p_web.GSV('BookingSite') = 'RRC')
        locPaymentString = locPaymentStringRRC
    ELSE
        locPaymentString = locPaymentStringARC
    END
    
    IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),locPaymentString) = 0)
        IF (p_web.GSV('job:Loan_Unit_Number') = 0)
            IF (p_web.GSV('job:Warranty_Job') = 'YES' AND p_web.GSV('job:Chargeable_Job') <> 'YES')
                paymentFail# = 1
            END
        END
    ELSE
        paymentFail# = 1
    END
    IF (paymentFail# = 1)
        p_web.SSV('Hide:PaymentDetails',1)
    ELSE
        p_web.SSV('Hide:PaymentDetails',0)
    END
    
OpenFiles  ROUTINE
  p_web._OpenFile(EXCHHIST)
  p_web._OpenFile(LOANHIST)
  p_web._OpenFile(EXCHANGE)
  p_web._OpenFile(WAYBCONF)
  p_web._OpenFile(LOAN)
  p_web._OpenFile(JOBSCONS)
  p_web._OpenFile(COURIER)
  p_web._OpenFile(VATCODE)
  p_web._OpenFile(INVOICE)
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(JOBSE)
  p_web._OpenFile(JOBS)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(LOANHIST)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(WAYBCONF)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(JOBSCONS)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(VATCODE)
  p_Web._CloseFile(INVOICE)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('DespatchConfirmation_form:inited_',1)
  do RestoreMem

CancelForm  Routine
  DO deletesessionvalues

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
  If p_web.IfExistsValue('job:Account_Number')
    p_web.SetPicture('job:Account_Number','@s15')
  End
  p_web.SetSessionPicture('job:Account_Number','@s15')
  If p_web.IfExistsValue('job:Charge_Type')
    p_web.SetPicture('job:Charge_Type','@s30')
  End
  p_web.SetSessionPicture('job:Charge_Type','@s30')
  If p_web.IfExistsValue('job:Repair_Type')
    p_web.SetPicture('job:Repair_Type','@s30')
  End
  p_web.SetSessionPicture('job:Repair_Type','@s30')
  If p_web.IfExistsValue('locCourierCost')
    p_web.SetPicture('locCourierCost','@n14.2')
  End
  p_web.SetSessionPicture('locCourierCost','@n14.2')
  If p_web.IfExistsValue('locLabourCost')
    p_web.SetPicture('locLabourCost','@n14.2')
  End
  p_web.SetSessionPicture('locLabourCost','@n14.2')
  If p_web.IfExistsValue('locPartsCost')
    p_web.SetPicture('locPartsCost','@n14.2')
  End
  p_web.SetSessionPicture('locPartsCost','@n14.2')
  If p_web.IfExistsValue('locSubTotal')
    p_web.SetPicture('locSubTotal','@n14.2')
  End
  p_web.SetSessionPicture('locSubTotal','@n14.2')
  If p_web.IfExistsValue('locInvoiceDate')
    p_web.SetPicture('locInvoiceDate','@d06')
  End
  p_web.SetSessionPicture('locInvoiceDate','@d06')
  If p_web.IfExistsValue('locVAT')
    p_web.SetPicture('locVAT','@n14.2')
  End
  p_web.SetSessionPicture('locVAT','@n14.2')
  If p_web.IfExistsValue('locTotal')
    p_web.SetPicture('locTotal','@n14.2')
  End
  p_web.SetSessionPicture('locTotal','@n14.2')
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('Show:DespatchInvoiceDetails') = 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('job:Account_Number',job:Account_Number)
  p_web.SetSessionValue('locCompanyName',locCompanyName)
  p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  p_web.SetSessionValue('job:Repair_Type',job:Repair_Type)
  p_web.SetSessionValue('locCourierCost',locCourierCost)
  p_web.SetSessionValue('locLabourCost',locLabourCost)
  p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber)
  p_web.SetSessionValue('locPartsCost',locPartsCost)
  p_web.SetSessionValue('locSubTotal',locSubTotal)
  p_web.SetSessionValue('locInvoiceDate',locInvoiceDate)
  p_web.SetSessionValue('locVAT',locVAT)
  p_web.SetSessionValue('locTotal',locTotal)
  p_web.SetSessionValue('job:Courier',job:Courier)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('job:Account_Number')
    job:Account_Number = p_web.GetValue('job:Account_Number')
    p_web.SetSessionValue('job:Account_Number',job:Account_Number)
  End
  if p_web.IfExistsValue('locCompanyName')
    locCompanyName = p_web.GetValue('locCompanyName')
    p_web.SetSessionValue('locCompanyName',locCompanyName)
  End
  if p_web.IfExistsValue('job:Charge_Type')
    job:Charge_Type = p_web.GetValue('job:Charge_Type')
    p_web.SetSessionValue('job:Charge_Type',job:Charge_Type)
  End
  if p_web.IfExistsValue('job:Repair_Type')
    job:Repair_Type = p_web.GetValue('job:Repair_Type')
    p_web.SetSessionValue('job:Repair_Type',job:Repair_Type)
  End
  if p_web.IfExistsValue('locCourierCost')
    locCourierCost = p_web.dformat(clip(p_web.GetValue('locCourierCost')),'@n14.2')
    p_web.SetSessionValue('locCourierCost',locCourierCost)
  End
  if p_web.IfExistsValue('locLabourCost')
    locLabourCost = p_web.dformat(clip(p_web.GetValue('locLabourCost')),'@n14.2')
    p_web.SetSessionValue('locLabourCost',locLabourCost)
  End
  if p_web.IfExistsValue('locInvoiceNumber')
    locInvoiceNumber = p_web.GetValue('locInvoiceNumber')
    p_web.SetSessionValue('locInvoiceNumber',locInvoiceNumber)
  End
  if p_web.IfExistsValue('locPartsCost')
    locPartsCost = p_web.dformat(clip(p_web.GetValue('locPartsCost')),'@n14.2')
    p_web.SetSessionValue('locPartsCost',locPartsCost)
  End
  if p_web.IfExistsValue('locSubTotal')
    locSubTotal = p_web.dformat(clip(p_web.GetValue('locSubTotal')),'@n14.2')
    p_web.SetSessionValue('locSubTotal',locSubTotal)
  End
  if p_web.IfExistsValue('locInvoiceDate')
    locInvoiceDate = p_web.dformat(clip(p_web.GetValue('locInvoiceDate')),'@d06')
    p_web.SetSessionValue('locInvoiceDate',locInvoiceDate)
  End
  if p_web.IfExistsValue('locVAT')
    locVAT = p_web.dformat(clip(p_web.GetValue('locVAT')),'@n14.2')
    p_web.SetSessionValue('locVAT',locVAT)
  End
  if p_web.IfExistsValue('locTotal')
    locTotal = p_web.dformat(clip(p_web.GetValue('locTotal')),'@n14.2')
    p_web.SetSessionValue('locTotal',locTotal)
  End
  if p_web.IfExistsValue('job:Courier')
    job:Courier = p_web.GetValue('job:Courier')
    p_web.SetSessionValue('job:Courier',job:Courier)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('DespatchConfirmation_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
  !region Start
    IF (p_web.IfExistsValue('firsttime') AND p_web.GetValue('firsttime') = 1)
      ! Call this code once
        IF (p_web.GSV('Show:DespatchInvoiceDetails') = 1)
         
            IF (p_web.GSV('CreateDespatchInvoice') = 1 )
                IF (CreateTheInvoice(p_web,errorMessage))
                    !todo: Need to sort out if errors, but I have a feeling
                    !they don't auto invoice from the despatch screen ofte
                END ! IF
            END
        END
        IF (DespatchProcess())
            ! Failed
            CreateScript(p_web,packet,'window.open("IndividualDespatch","_self")')
            DO SendPacket
            EXIT
        END ! IF
    END
  ! Hideous Calculations to work out invoice/costs
    p_web.SSV('URL:CreateInvoice','CreateInvoice?returnURL=DespatchConfirmation')
    p_web.SSV('URL:CreateInvoiceTarget','_self')
    p_web.SSV('URL:CreateInvoiceText','Create Invoice')
  
    IF (IsJobInvoiced(p_web.GSV('job:Invoice_Number'),p_web) = TRUE)
        p_web.SSV('URL:CreateInvoice',p_web.GSV('Document:Invoice') & '?var=' & RANDOM(1,9999999))
        p_web.SSV('URL:CreateInvoiceTarget','_blank')
        p_web.SSV('URL:CreateInvoiceText','Print Invoice')
              
        IF (p_web.GSV('BookingSite') = 'RRC')
            p_web.SSV('locCourierCost',p_web.GSV('job:Invoice_Courier_Cost'))
            p_web.SSV('locLabourCost',p_web.GSV('jobe:InvRRCCLabourCost'))
            p_web.SSV('locPartsCost',p_web.GSV('jobe:InvRRCCPartsCost'))
            Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
            inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
            IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key))
            END
            p_web.SSV('locVAT',(p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100) + |
                (p_web.GSV('jobe:InvRRCCLabourCost') * inv:Vat_Rate_Labour/100) + |
                (p_web.GSV('jobe:InvRRCCPartsCost') * inv:Vat_Rate_Parts/100))
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = p_web.GSV('BookingAccount')
            IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
            END
                  
            p_web.SSV('locInvoiceNumber',inv:Invoice_Number & '-' & tra:BranchIdentification)
            p_web.SSV('locInvoiceDate',inv:RRCInvoiceDate)
        else 
            p_web.SSV('locCourierCost',p_web.GSV('job:Invoice_Courier_Cost'))
            p_web.SSV('locLabourCost',p_web.GSV('job:Invoice_Labour_Cost'))
            p_web.SSV('locPartsCost',p_web.GSV('job:Invoice_Parts_Cost'))
  
            Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
            inv:Invoice_Number = p_web.GSV('job:Invoice_Number')
            IF (Access:INVOICE.TryFetch(inv:Invoice_Number_Key))
            END
            p_web.SSV('locVAT',(p_web.GSV('job:Invoice_Courier_Cost') * inv:Vat_Rate_Labour/100) + |
                (p_web.GSV('job:Invoice_Labour_Cost') * inv:Vat_Rate_Labour/100) + |
                (p_web.GSV('job:Invoice_Parts_Cost') * inv:Vat_Rate_Parts/100))
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = p_web.GSV('Default:AccountNumber')
            IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
            END
                  
            p_web.SSV('locInvoiceNumber',inv:Invoice_Number & '-' & tra:BranchIdentification)
            p_web.SSV('locInvoiceDate',inv:ARCInvoiceDate)        
        end
  
    ELSE
        IF (p_web.GSV('BookingSite') = 'RRC')
            p_web.SSV('locCourierCost',p_web.GSV('job:Courier_Cost'))
            p_web.SSV('locLabourCost',p_web.GSV('jobe:RRCCLabourCost'))
            p_web.SSV('locPartsCost',p_web.GSV('jobe:RRCCPartsCost'))
        else 
            p_web.SSV('locCourierCost',p_web.GSV('job:Courier_Cost'))
            p_web.SSV('locLabourCost',p_web.GSV('job:Labour_Cost'))
            p_web.SSV('locPartsCost',p_web.GSV('job:Parts_Cost'))
  
        end
        IF (InvoiceSubAccounts(p_web.GSV('job:Account_Number')))
            Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
            sub:Account_Number = p_web.GSV('job:Account_Number')
            IF (Access:subtracc.TryFetch(sub:Account_Number_Key))
            END
                  
            Access:VATCODE.ClearKey(vat:Vat_code_Key)
            vat:VAT_Code = sub:Labour_VAT_Code
            IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
                locLabourVatRate = vat:VAT_Rate
            END
            Access:VATCODE.ClearKey(vat:Vat_code_Key)
            vat:VAT_Code = sub:Parts_VAT_Code
            IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
                locPartsVatRate = vat:VAT_Rate
            END
        ELSE
            Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
            sub:Account_Number = p_web.GSV('job:Account_Number')
            IF (Access:subtracc.TryFetch(sub:Account_Number_Key))
            END
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = sub:Main_Account_Number
            IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
            END
                  
                  
            Access:VATCODE.ClearKey(vat:Vat_code_Key)
            vat:VAT_Code = tra:Labour_VAT_Code
            IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
                locLabourVatRate = vat:VAT_Rate
            END
            Access:VATCODE.ClearKey(vat:Vat_code_Key)
            vat:VAT_Code = tra:Parts_VAT_Code
            IF (Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign)
                locPartsVatRate = vat:VAT_Rate
            END
                  
        END
              
        IF (p_web.GSV('BookingSite') = 'RRC')
            p_web.SSV('locVAT',(p_web.GSV('job:Courier_Cost') * locLabourVatRate/100) + |
                (p_web.GSV('jobe:RRCCLabourCost') * locLabourVatRate/100) + |
                (p_web.GSV('jobe:RRCCPartsCost') * locPartsVatRate/100))                
        ELSE
                  
            p_web.SSV('locVAT',(p_web.GSV('job:Courier_Cost') * locLabourVatRate/100) + |
                (p_web.GSV('job:Labour_Cost') * locLabourVatRate/100) + |
                (p_web.GSV('job:Parts_Cost') * locPartsVatRate/100))
        END
    END
    p_web.SSV('locSubTotal',p_web.GSV('locCourierCost') + | 
        p_web.GSV('locLabourCost') + | 
        p_web.GSV('locPartsCost'))        
    p_web.SSV('locTotal',p_web.GSV('locSubTotal') + | 
        p_web.GSV('locVAT'))  
    
    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = p_web.GSV('job:Account_Number')
    IF (Access:subtracc.TryFetch(sub:Account_Number_Key))
    END    
    p_web.SSV('locCompanyName',sub:Company_Name)
  
    IF (SecurityCheckFailed(p_web.GSV('BookingUserPassword'),'PAYMENT TYPES'))
        p_web.SSV('Hide:PaymentDetails',1)
    ELSE
        p_web.SSV('Hide:PaymentDetails',0)
    END
  
  !endregion  
      p_web.site.SaveButton.TextValue = 'OK'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locCompanyName = p_web.RestoreValue('locCompanyName')
 locCourierCost = p_web.RestoreValue('locCourierCost')
 locLabourCost = p_web.RestoreValue('locLabourCost')
 locInvoiceNumber = p_web.RestoreValue('locInvoiceNumber')
 locPartsCost = p_web.RestoreValue('locPartsCost')
 locSubTotal = p_web.RestoreValue('locSubTotal')
 locInvoiceDate = p_web.RestoreValue('locInvoiceDate')
 locVAT = p_web.RestoreValue('locVAT')
 locTotal = p_web.RestoreValue('locTotal')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = 'IndividualDespatch'
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('DespatchConfirmation_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('DespatchConfirmation_ChainTo')
    loc:formaction = p_web.GetSessionValue('DespatchConfirmation_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'IndividualDespatch'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="DespatchConfirmation" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="DespatchConfirmation" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="DespatchConfirmation" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Despatch Confirmation') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Despatch Confirmation',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_DespatchConfirmation">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_DespatchConfirmation" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_DespatchConfirmation')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GSV('Show:DespatchInvoiceDetails') = 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Invoice Details') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Despatch') & ''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_DespatchConfirmation')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_DespatchConfirmation'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:SaveButton,loc:formname,loc:formaction,loc:formactiontarget,loc:javascript)
    Else
      loc:javascript = ''
      packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CloseButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget)
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('Show:DespatchInvoiceDetails') = 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.job:Courier')
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GSV('Show:DespatchInvoiceDetails') = 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_DespatchConfirmation')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  packet = clip(packet) & 'setDefaultButton(''save_btn'',1);<13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GSV('Show:DespatchInvoiceDetails') = 1
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GSV('Show:DespatchInvoiceDetails') = 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Invoice Details') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_DespatchConfirmation_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Invoice Details')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Invoice Details')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Invoice Details')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Invoice Details')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Account_Number
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Account_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Account_Number
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCompanyName
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCompanyName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCompanyName
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Charge_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Charge_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Repair_Type
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Repair_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Repair_Type
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::aLine
      do Comment::aLine
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::textBillingDetails
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::textBillingDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::textBillingDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::textInvoiceDetails
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::textInvoiceDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::textInvoiceDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locCourierCost
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locCourierCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locCourierCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locLabourCost
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locLabourCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locLabourCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locInvoiceNumber
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locInvoiceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locInvoiceNumber
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locPartsCost
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locPartsCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locPartsCost
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locSubTotal
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locSubTotal
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locSubTotal
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locInvoiceDate
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locInvoiceDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locInvoiceDate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locVAT
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locVAT
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locVAT
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locTotal
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locTotal
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locTotal
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::job:Courier
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::job:Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::job:Courier
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::buttonCreateInvoice
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonCreateInvoice
      do Comment::buttonCreateInvoice
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::buttonPaymentDetails
      do Comment::buttonPaymentDetails
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Despatch') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_DespatchConfirmation_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Despatch')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::DespatchMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::DespatchMessage
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPrintWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonPrintWaybill
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&'20%'&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&'28%'&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::buttonPrintDespatchNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ' width="'&'1%'&'"'
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::buttonPrintDespatchNote
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::job:Account_Number  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Account_Number') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account Number')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Account_Number  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Account_Number',p_web.GetValue('NewValue'))
    job:Account_Number = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Account_Number
    do Value::job:Account_Number
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Account_Number',p_web.dFormat(p_web.GetValue('Value'),'@s15'))
    job:Account_Number = p_web.GetValue('Value')
  End

Value::job:Account_Number  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Account_Number') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Account_Number
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Account_Number'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:Account_Number  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Account_Number') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locCompanyName  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locCompanyName') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Account Name')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locCompanyName  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCompanyName',p_web.GetValue('NewValue'))
    locCompanyName = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCompanyName
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCompanyName',p_web.GetValue('Value'))
    locCompanyName = p_web.GetValue('Value')
  End

Value::locCompanyName  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locCompanyName') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- locCompanyName
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locCompanyName'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locCompanyName  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locCompanyName') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Charge_Type  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Charge_Type') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Charge Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Charge_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Charge_Type',p_web.GetValue('NewValue'))
    job:Charge_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Charge_Type
    do Value::job:Charge_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Charge_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Charge_Type = p_web.GetValue('Value')
  End

Value::job:Charge_Type  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Charge_Type') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Charge_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Charge_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:Charge_Type  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Charge_Type') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Repair_Type  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Repair_Type') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Repair Type')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Repair_Type  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Repair_Type',p_web.GetValue('NewValue'))
    job:Repair_Type = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Repair_Type
    do Value::job:Repair_Type
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Repair_Type',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Repair_Type = p_web.GetValue('Value')
  End

Value::job:Repair_Type  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Repair_Type') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- job:Repair_Type
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('job:Repair_Type'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::job:Repair_Type  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Repair_Type') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::aLine  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('aLine',p_web.GetValue('NewValue'))
    do Value::aLine
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::aLine  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('aLine') & '_value','')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & '<hr></hr><13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::aLine  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('aLine') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::textBillingDetails  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('textBillingDetails') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Billing Details:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::textBillingDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textBillingDetails',p_web.GetValue('NewValue'))
    do Value::textBillingDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textBillingDetails  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('textBillingDetails') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::textBillingDetails  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('textBillingDetails') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::textInvoiceDetails  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('textInvoiceDetails') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Invoice Details:')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::textInvoiceDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('textInvoiceDetails',p_web.GetValue('NewValue'))
    do Value::textInvoiceDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::textInvoiceDetails  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('textInvoiceDetails') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::textInvoiceDetails  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('textInvoiceDetails') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locCourierCost  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locCourierCost') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Courier Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locCourierCost  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locCourierCost',p_web.GetValue('NewValue'))
    locCourierCost = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locCourierCost
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locCourierCost',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    locCourierCost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End

Value::locCourierCost  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locCourierCost') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locCourierCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locCourierCost')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locCourierCost',p_web.GetSessionValue('locCourierCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locCourierCost  Routine
      loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locCourierCost') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locLabourCost  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locLabourCost') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Labour Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locLabourCost  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locLabourCost',p_web.GetValue('NewValue'))
    locLabourCost = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locLabourCost
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locLabourCost',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    locLabourCost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End

Value::locLabourCost  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locLabourCost') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locLabourCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locLabourCost')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locLabourCost',p_web.GetSessionValue('locLabourCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locLabourCost  Routine
      loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locLabourCost') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locInvoiceNumber  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceNumber') & '_prompt',Choose(p_web.GSV('IsJobInvoiced') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Invoice Number')
  If p_web.GSV('IsJobInvoiced') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locInvoiceNumber  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locInvoiceNumber',p_web.GetValue('NewValue'))
    locInvoiceNumber = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locInvoiceNumber
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locInvoiceNumber',p_web.GetValue('Value'))
    locInvoiceNumber = p_web.GetValue('Value')
  End

Value::locInvoiceNumber  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceNumber') & '_value',Choose(p_web.GSV('IsJobInvoiced') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('IsJobInvoiced') <> 1)
  ! --- DISPLAY --- locInvoiceNumber
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(p_web.GetSessionValueFormat('locInvoiceNumber'),) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locInvoiceNumber  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceNumber') & '_comment',Choose(p_web.GSV('IsJobInvoiced') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('IsJobInvoiced') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locPartsCost  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locPartsCost') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Parts Cost')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locPartsCost  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locPartsCost',p_web.GetValue('NewValue'))
    locPartsCost = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locPartsCost
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locPartsCost',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    locPartsCost = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End

Value::locPartsCost  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locPartsCost') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locPartsCost
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locPartsCost')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locPartsCost',p_web.GetSessionValue('locPartsCost'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locPartsCost  Routine
      loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locPartsCost') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locSubTotal  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locSubTotal') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Sub Total')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locSubTotal  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locSubTotal',p_web.GetValue('NewValue'))
    locSubTotal = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locSubTotal
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locSubTotal',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    locSubTotal = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End

Value::locSubTotal  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locSubTotal') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locSubTotal
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locSubTotal')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locSubTotal',p_web.GetSessionValue('locSubTotal'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locSubTotal  Routine
      loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locSubTotal') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locInvoiceDate  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceDate') & '_prompt',Choose(p_web.GSV('IsJobInvoiced') <> 1,'hdiv','' & clip(p_web.site.PromptClass) &''))
  loc:prompt = p_web.Translate('Invoice Date')
  If p_web.GSV('IsJobInvoiced') <> 1
    loc:prompt = ''
  End
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locInvoiceDate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locInvoiceDate',p_web.GetValue('NewValue'))
    locInvoiceDate = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locInvoiceDate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locInvoiceDate',p_web.dFormat(p_web.GetValue('Value'),'@d06'))
    locInvoiceDate = p_web.Dformat(p_web.GetValue('Value'),'@d06') !
  End

Value::locInvoiceDate  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceDate') & '_value',Choose(p_web.GSV('IsJobInvoiced') <> 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('IsJobInvoiced') <> 1)
  ! --- DISPLAY --- locInvoiceDate
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web._jsok(format(p_web.GetSessionValue('locInvoiceDate'),'@d06')) & '</span>' & |
    '<13,10>'
  do SendPacket
  End
  p_web._DivFooter()

Comment::locInvoiceDate  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locInvoiceDate') & '_comment',Choose(p_web.GSV('IsJobInvoiced') <> 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('IsJobInvoiced') <> 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locVAT  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locVAT') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('VAT')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locVAT  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locVAT',p_web.GetValue('NewValue'))
    locVAT = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locVAT
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locVAT',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    locVAT = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End

Value::locVAT  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locVAT') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locVAT
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locVAT')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locVAT',p_web.GetSessionValue('locVAT'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locVAT  Routine
      loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locVAT') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locTotal  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locTotal') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Total')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locTotal  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locTotal',p_web.GetValue('NewValue'))
    locTotal = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locTotal
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locTotal',p_web.dFormat(p_web.GetValue('Value'),'@n14.2'))
    locTotal = p_web.Dformat(p_web.GetValue('Value'),'@n14.2') !
  End

Value::locTotal  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locTotal') & '_value','adiv')
  loc:extra = ''
  ! --- STRING --- locTotal
    loc:AutoComplete = 'autocomplete="off"'
  loc:readonly = 'readonly'
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formreadonly'
  If lower(loc:invalid) = lower('locTotal')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
    loc:extra = clip(loc:extra) & ' size="' & clip(15) &'"'
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & p_web.CreateInput('text','locTotal',p_web.GetSessionValue('locTotal'),loc:fieldclass,loc:readonly,clip(loc:extra) & ' ' & clip(loc:autocomplete),'@n14.2',loc:javascript,,) & '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::locTotal  Routine
      loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('locTotal') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::job:Courier  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Courier') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Courier')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::job:Courier  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('job:Courier',p_web.GetValue('NewValue'))
    job:Courier = p_web.GetValue('NewValue') !FieldType= STRING Field = job:Courier
    do Value::job:Courier
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('job:Courier',p_web.dFormat(p_web.GetValue('Value'),'@s30'))
    job:Courier = p_web.GetValue('Value')
  End
    job:Courier = Upper(job:Courier)
    p_web.SetSessionValue('job:Courier',job:Courier)
  do Value::job:Courier
  do SendAlert

Value::job:Courier  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Courier') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  loc:fieldclass = clip(loc:fieldclass) & ' Upper'
  If lower(loc:invalid) = lower('job:Courier')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''job:Courier'',''despatchconfirmation_job:courier_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('job:Courier',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(EXCHHIST)
  bind(exh:Record)
  p_web._OpenFile(LOANHIST)
  bind(loh:Record)
  p_web._OpenFile(EXCHANGE)
  bind(xch:Record)
  p_web._OpenFile(WAYBCONF)
  bind(wac:Record)
  p_web._OpenFile(LOAN)
  bind(loa:Record)
  p_web._OpenFile(JOBSCONS)
  bind(joc:Record)
  p_web._OpenFile(COURIER)
  bind(cou:Record)
  p_web._OpenFile(VATCODE)
  bind(vat:Record)
  p_web._OpenFile(INVOICE)
  bind(inv:Record)
  p_web._OpenFile(TRADEACC)
  bind(tra:Record)
  p_web._OpenFile(SUBTRACC)
  bind(sub:Record)
  p_web._OpenFile(JOBSE)
  bind(jobe:Record)
  p_web._OpenFile(JOBS)
  bind(job:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(job:Courier_OptionView)
  job:Courier_OptionView{prop:order} = 'UPPER(cou:Courier)'
  Set(job:Courier_OptionView)
  Loop
    Next(job:Courier_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('job:Courier') = 0
      p_web.SetSessionValue('job:Courier',cou:Courier)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,cou:Courier,choose(cou:Courier = p_web.getsessionvalue('job:Courier')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(job:Courier_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(EXCHHIST)
  p_Web._CloseFile(LOANHIST)
  p_Web._CloseFile(EXCHANGE)
  p_Web._CloseFile(WAYBCONF)
  p_Web._CloseFile(LOAN)
  p_Web._CloseFile(JOBSCONS)
  p_Web._CloseFile(COURIER)
  p_Web._CloseFile(VATCODE)
  p_Web._CloseFile(INVOICE)
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(JOBSE)
  p_Web._CloseFile(JOBS)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('DespatchConfirmation_' & p_web._nocolon('job:Courier') & '_value')

Comment::job:Courier  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('job:Courier') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::buttonCreateInvoice  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonCreateInvoice') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::buttonCreateInvoice  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonCreateInvoice',p_web.GetValue('NewValue'))
    do Value::buttonCreateInvoice
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::buttonCreateInvoice
  do SendAlert

Value::buttonCreateInvoice  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonCreateInvoice') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonCreateInvoice'',''despatchconfirmation_buttoncreateinvoice_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','CreateInvoice',p_web.GSV('URL:CreateInvoiceText'),'button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip(p_web.GSV('URL:CreateInvoice'))) & ''','''&clip(p_web.GSV('URL:CreateInvoiceTarget'))&''')',loc:javascript,0,'images/money.png',,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('DespatchConfirmation_' & p_web._nocolon('buttonCreateInvoice') & '_value')

Comment::buttonCreateInvoice  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonCreateInvoice') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPaymentDetails  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPaymentDetails',p_web.GetValue('NewValue'))
    do Value::buttonPaymentDetails
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::buttonPaymentDetails  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPaymentDetails') & '_value',Choose(p_web.GSV('Hide:PaymentDetails') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PaymentDetails') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PaymentDetails','PaymentDetails','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('DisplayBrowsePayments?DisplayBrowsePaymentsReturnURL=DespatchConfirmation')) & ''','''&clip('_self')&''')',loc:javascript,0,'images/money.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()

Comment::buttonPaymentDetails  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPaymentDetails') & '_comment',Choose(p_web.GSV('Hide:PaymentDetails') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:PaymentDetails') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::DespatchMessage  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('DespatchMessage',p_web.GetValue('NewValue'))
    do Value::DespatchMessage
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::DespatchMessage  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('DespatchMessage') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('Despatched Sucessfully',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::DespatchMessage  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('DespatchMessage') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPrintWaybill  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintWaybill',p_web.GetValue('NewValue'))
    do Value::buttonPrintWaybill
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::buttonPrintWaybill
  do SendAlert

Value::buttonPrintWaybill  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPrintWaybill') & '_value',Choose(p_web.GSV('Hide:PrintWaybill') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintWaybill') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPrintWaybill'',''despatchconfirmation_buttonprintwaybill_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintWaybill','Print Waybill','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('Waybill?var=' & RANDOM(1,9999999))) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('DespatchConfirmation_' & p_web._nocolon('buttonPrintWaybill') & '_value')

Comment::buttonPrintWaybill  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPrintWaybill') & '_comment',Choose(p_web.GSV('Hide:PrintWaybill') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:PrintWaybill') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::buttonPrintDespatchNote  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('buttonPrintDespatchNote',p_web.GetValue('NewValue'))
    do Value::buttonPrintDespatchNote
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  do Value::buttonPrintDespatchNote
  do SendAlert

Value::buttonPrintDespatchNote  Routine
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPrintDespatchNote') & '_value',Choose(p_web.GSV('Hide:PrintDespatchNote') = 1,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.GSV('Hide:PrintDespatchNote') = 1)
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''buttonPrintDespatchNote'',''despatchconfirmation_buttonprintdespatchnote_value'',1,FieldValue(this,1))')&';'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','PrintDespatchNote','Print Despatch Note','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('DespatchNote?var=' & RANDOM(1,9999999))) & ''','''&clip('_blank')&''')',loc:javascript,0,'images/printer.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('DespatchConfirmation_' & p_web._nocolon('buttonPrintDespatchNote') & '_value')

Comment::buttonPrintDespatchNote  Routine
    loc:comment = ''
  p_web._DivHeader('DespatchConfirmation_' & p_web._nocolon('buttonPrintDespatchNote') & '_comment',Choose(p_web.GSV('Hide:PrintDespatchNote') = 1,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.GSV('Hide:PrintDespatchNote') = 1
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('DespatchConfirmation_job:Courier_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::job:Courier
      else
        do Value::job:Courier
      end
  of lower('DespatchConfirmation_buttonCreateInvoice_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonCreateInvoice
      else
        do Value::buttonCreateInvoice
      end
  of lower('DespatchConfirmation_buttonPrintWaybill_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPrintWaybill
      else
        do Value::buttonPrintWaybill
      end
  of lower('DespatchConfirmation_buttonPrintDespatchNote_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::buttonPrintDespatchNote
      else
        do Value::buttonPrintDespatchNote
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('DespatchConfirmation_form:ready_',1)
  p_web.SetSessionValue('DespatchConfirmation_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_DespatchConfirmation',0)

PreCopy  Routine
  p_web.SetValue('DespatchConfirmation_form:ready_',1)
  p_web.SetSessionValue('DespatchConfirmation_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_DespatchConfirmation',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('DespatchConfirmation_form:ready_',1)
  p_web.SetSessionValue('DespatchConfirmation_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('DespatchConfirmation:Primed',0)

PreDelete       Routine
  p_web.SetValue('DespatchConfirmation_form:ready_',1)
  p_web.SetSessionValue('DespatchConfirmation_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('DespatchConfirmation:Primed',0)
  p_web.setsessionvalue('showtab_DespatchConfirmation',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('Show:DespatchInvoiceDetails') = 1
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('DespatchConfirmation_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  DO deletesessionvalues
  p_web.DeleteSessionValue('DespatchConfirmation_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 2
  If p_web.GSV('Show:DespatchInvoiceDetails') = 1
    loc:InvalidTab += 1
          job:Courier = Upper(job:Courier)
          p_web.SetSessionValue('job:Courier',job:Courier)
        If loc:Invalid <> '' then exit.
  End
  ! tab = 3
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('DespatchConfirmation:Primed',0)
  p_web.StoreValue('job:Account_Number')
  p_web.StoreValue('locCompanyName')
  p_web.StoreValue('job:Charge_Type')
  p_web.StoreValue('job:Repair_Type')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('locCourierCost')
  p_web.StoreValue('locLabourCost')
  p_web.StoreValue('locInvoiceNumber')
  p_web.StoreValue('locPartsCost')
  p_web.StoreValue('locSubTotal')
  p_web.StoreValue('locInvoiceDate')
  p_web.StoreValue('locVAT')
  p_web.StoreValue('locTotal')
  p_web.StoreValue('job:Courier')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('locCompanyName',locCompanyName) ! STRING(30)
     p_web.SSV('locCourierCost',locCourierCost) ! REAL
     p_web.SSV('locLabourCost',locLabourCost) ! REAL
     p_web.SSV('locPartsCost',locPartsCost) ! REAL
     p_web.SSV('locSubTotal',locSubTotal) ! REAL
     p_web.SSV('locVAT',locVAT) ! REAL
     p_web.SSV('locTotal',locTotal) ! REAL
     p_web.SSV('locInvoiceNumber',locInvoiceNumber) ! REAL
     p_web.SSV('locInvoiceDate',locInvoiceDate) ! DATE
     p_web.SSV('locLabourVatRate',locLabourVatRate) ! REAL
     p_web.SSV('locPartsVatRate',locPartsVatRate) ! REAL
     p_web.SSV('locCourier',locCourier) ! STRING(30)
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     locCompanyName = p_web.GSV('locCompanyName') ! STRING(30)
     locCourierCost = p_web.GSV('locCourierCost') ! REAL
     locLabourCost = p_web.GSV('locLabourCost') ! REAL
     locPartsCost = p_web.GSV('locPartsCost') ! REAL
     locSubTotal = p_web.GSV('locSubTotal') ! REAL
     locVAT = p_web.GSV('locVAT') ! REAL
     locTotal = p_web.GSV('locTotal') ! REAL
     locInvoiceNumber = p_web.GSV('locInvoiceNumber') ! REAL
     locInvoiceDate = p_web.GSV('locInvoiceDate') ! DATE
     locLabourVatRate = p_web.GSV('locLabourVatRate') ! REAL
     locPartsVatRate = p_web.GSV('locPartsVatRate') ! REAL
     locCourier = p_web.GSV('locCourier') ! STRING(30)
DespatchProcess     PROCEDURE()!,LONG
locWaybillNumber    STRING(30)
CODE
    p_web.SSV('Hide:PrintDespatchNote',1)
    p_web.SSV('Hide:PrintWaybill',1)
    
    Access:JOBS.ClearKey(job:Ref_Number_Key)
    job:Ref_Number = p_web.GSV('job:Ref_Number')
    IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        IF (JobInUse(job:Ref_Number))
            CreateScript(p_web,packet,'alert("Error. The selected job is in use. Please try again later.")')
            RETURN Level:Fatal
        END ! IF
    END !I F
    
    IF (p_web.GSV('cou:PrintWaybill') = 1)
            
        p_web.SSV('Hide:PrintWaybill',0)
            
        locWaybillNumber = NextWaybillNumber()
        IF (locWaybillNumber = 0)
            CreateScript(p_web,packet,'alert("Unable to create waybill. Please try again.")')
            RETURN Level:Fatal
        END
        
        Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
        way:WayBillNumber = locWaybillNumber
        IF (Access:WAYBILLS.TryFetch(way:WayBillNumberKey))
            CreateScript(p_web,packet,'alert("Unable to create waybill. Please try again.")')
            RETURN Level:Fatal
        END
        
        IF (p_web.GSV('BookingSite') = 'RRC')

            way:AccountNumber = p_web.GSV('BookingAccount')
            CASE p_web.GSV('DespatchType')
            OF 'JOB'
                way:WaybillID = 2
                p_web.SSV('wob:JobWaybillNumber',locWaybillNumber)
            OF 'EXC'
                way:WaybillID = 3
                p_web.SSV('wob:ExcWaybillNumber',way:WayBillNumber)
            OF 'LOA'
                way:WaybillID = 4
                p_web.SSV('wob:LoaWaybillNumber',way:WayBillNumber)
            END
            
            IF (p_web.GSV('job:Who_Booked') = 'WEB')
                ! PUP Job
                way:WayBillType = 9
                way:WaybillID = 21
            ELSE
                way:WayBillType = 2
            END
            
            way:FromAccount = p_web.GSV('BookingAccount')
            way:ToAccount = p_web.GSV('job:Account_Number')
            
        ELSE
            way:WayBillType = 1 ! Created From RRC
            way:AccountNumber = p_web.GSV('wob:HeadAccountNumber')
            way:FromAccount = p_web.GSV('ARC:AccountNumber')
            IF (p_web.GSV('jobe:WebJob') = 1)
                way:ToAccount = p_web.GSV('wob:HeadAccountNumber')
                Case p_web.GSV('DespatchType')
                of 'JOB'
                    way:WaybillID = 5 ! ARC to RRC (JOB)
                of 'EXC'
                    way:WaybillID = 6 ! ARC to RRC (EXC)
                END
                
            ELSE
                way:ToAccount = job:Account_Number
                Case p_web.GSV('DespatchType')
                OF 'JOB'
                    way:WaybillID = 7 ! ARC To Customer (JOB)
                of 'EXC'
                    way:WaybillID = 8 ! ARC To Customer (EXC)
                of 'LOA'
                    way:WaybillID = 9 ! ARC To Customer (LOA)
                END
                
            END
            
        END
        
            
        IF (Access:WAYBILLS.TryUpdate())
            CreateScript(p_web,packet,'alert("Unable to save waybill. Please try again.")')
            RETURN Level:Fatal
        END
        
        IF (Access:WAYBCONF.PrimeRecord() = Level:Benign)
            ! Add to waybill staging table. (Add all records, 
            ! the wrong ones will be deleted later) (DBH: 21/04/2015)
            wac:AccountNumber = way:AccountNumber
            wac:WaybillNo = way:WayBillNumber
            wac:GenerateDate = way:TheDate
            wac:GenerateTime = way:TheTime
            wac:ConfirmationSent = 0
            IF (Access:WAYBCONF.TryInsert())
                Access:WAYBCONF.CancelAutoInc()
            END!  IF
        END ! IF
        
            
        IF (Access:WAYBILLJ.PrimeRecord() = Level:Benign)
            waj:WayBillNumber = way:WayBillNumber
            waj:JobNumber = p_web.GSV('job:Ref_Number')
            CASE p_web.GSV('DespatchType')
            OF 'JOB'
                waj:IMEINumber = p_web.GSV('job:ESN')
            OF 'EXC'
                Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                xch:Ref_Number = p_web.GSV('job:Exchange_Unit_Number')
                IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign) ! #13625 Using the wrong key (DBH: 30/10/2015)
                    waj:IMEINumber = xch:ESN
                END
                    
            OF 'LOA'
                Access:LOAN.ClearKey(loa:Ref_Number_Key)
                loa:Ref_Number = p_web.GSV('job:Loan_Unit_Number')
                IF (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
                    waj:IMEINumber = loa:ESN
                END
                    
            END
            waj:OrderNumber = p_web.GSV('job:Order_Number')
            waj:SecurityPackNumber = p_web.GSV('locSecurityPackID')
            waj:JobType = p_web.GSV('DespatchType')
            IF (Access:WAYBILLJ.TryUpdate() = Level:Benign)
            ELSE
                Access:WAYBILLJ.CancelAutoInc()
            END
        END
            
    ELSE ! IF (p_web.GSV('cou:PrintWaybill') = 1)
        if (p_web.GSV('cou:AutoConsignmentNo') = 1)
            Access:COURIER.Clearkey(cou:Courier_Key)
            cou:Courier = p_web.GSV('cou:Courier')
            if (Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign)
                cou:LastConsignmentNo += 1
                Access:COURIER.TryUpdate()
                locWaybillNumber = cou:LastConsignmentNo
            end
        ELSE
            locWaybillNumber = p_web.GSV('locConsignmentNumber')
        end
        p_web.SSV('Hide:PrintDespatchNote',0)
            
    END ! IF (p_web.GSV('cou:PrintWaybill') = 1)
            
    p_web.SSV('locWaybillNumber',locWaybillNumber)
    
    CASE p_web.GSV('DespatchType')
    OF 'JOB'
        !Despatch:Job(locWayBillNumber)
        Despatch:Job(p_web)
        
    OF 'EXC'
        Despatch:Exc(p_web)                
                
    OF 'LOA'
        Despatch:Loa(p_web)
        
    END

    ! Save Files
    Access:JOBS.ClearKey(job:Ref_Number_Key)
    job:Ref_Number = p_web.GSV('job:Ref_Number')
    IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        p_web.SessionQueueToFile(JOBS)
        IF (Access:JOBS.TryUpdate() = Level:Benign)
            
            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
                p_web.SessionQueueToFile(WEBJOB)
                IF (Access:WEBJOB.TryUpdate() = Level:Benign)
                    
                    Access:JOBSE.ClearKey(jobe:RefNumberKey)
                    jobe:RefNumber = job:Ref_Number
                    IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                        p_web.SessionQueueToFile(JOBSE)
                        IF (Access:JOBSE.TryUpdate() = Level:Benign)
                            
                        END
                    END
                END
            END
        END
    END
    
    RETURN Level:Benign
    
    
