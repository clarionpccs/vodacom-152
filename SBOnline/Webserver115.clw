

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('WEBSERVER115.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER006.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Report
!!! </summary>
BouncerHistory PROCEDURE (<NetWebServerWorker p_web>)

  ! The NetTalk Extension to ABC report procedure has been added to this procedure.
  ! This means that p_web must be passed to this procedure. So the prototype should
  ! look like this:
  ! <(NetWebServerWorker p_web)>
loc:PDFName   String(256)
loc:NoRecords Long
Progress:Thermometer BYTE                                  !
tmp:Parts            STRING(25),DIM(15)                    !
tmp:IMEINumber       STRING(30)                            !IMEI Number
code_temp            BYTE                                  !
option_temp          BYTE                                  !
bar_code_string_temp CSTRING(21)                           !
bar_code_temp        CSTRING(21)                           !
barcodeJobNumber     STRING(20)                            !
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Date_Completed)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:MSN)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Mobile_Number)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Ref_Number)
                       PROJECT(job_ali:Unit_Type)
                       PROJECT(job_ali:date_booked)
                       JOIN(jbn_ali:RefNumberKey,job_ali:Ref_Number)
                         PROJECT(jbn_ali:Fault_Description)
                         PROJECT(jbn_ali:Invoice_Text)
                       END
                     END
ReportPageNumber     LONG,AUTO
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),DOUBLE,CENTER,GRAY,TIMER(1)
                       PROGRESS,AT(15,15,111,12),USE(Progress:Thermometer),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

report               REPORT,AT(438,1135,7521,9740),PRE(rpt),PAPER(PAPER:A4),FONT('Tahoma',8,,FONT:regular),THOUS
                       HEADER,AT(396,469,7521,1000),USE(?unnamed)
                         STRING('Page:'),AT(6406,208),USE(?String35),FONT(,8,,FONT:bold),TRN
                         STRING(@N3),AT(6823,208),USE(ReportPageNumber),FONT(,8,,FONT:bold),TRN
                       END
detail                 DETAIL,AT(0,0,,3042),USE(?detailband)
                         STRING('Job Number:'),AT(208,52,1094,313),USE(?String2),FONT(,10,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING(@s8),AT(1823,260),USE(job_ali:Ref_Number),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Unit Details:'),AT(208,521,1094,313),USE(?String2:2),FONT(,10,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Model Number:'),AT(1823,677),USE(?String6:3),FONT(,8,,FONT:bold),TRN
                         STRING('Booked:'),AT(5156,52),USE(?STRING1),FONT(,8,,FONT:bold),TRN
                         STRING(@d6b),AT(6302,52),USE(job_ali:date_booked),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Date:'),AT(4531,52,1094,313),USE(?String2:4),FONT(,10,,FONT:bold,CHARSET:ANSI),TRN
                         STRING(@D6b),AT(6302,208),USE(job_ali:Date_Completed),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING(@s20),AT(1823,52,2604,208),USE(barcodeJobNumber),FONT('C39 High 12pt LJ3',12,,,CHARSET:ANSI), |
  LEFT,COLOR(COLOR:White)
                         STRING(@D6b),AT(6302,365),USE(wob:DateJobDespatched),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Despatched:'),AT(5156,365),USE(?String6:8),FONT(,8,,FONT:bold),TRN
                         STRING('Completed:'),AT(5156,208),USE(?String6:7),FONT(,8,,FONT:bold),TRN
                         STRING('Reported Fault:'),AT(208,1042,1094,313),USE(?String2:5),FONT(,10,,FONT:bold,CHARSET:ANSI), |
  TRN
                         TEXT,AT(1823,1042,5521,417),USE(jbn_ali:Fault_Description),FONT(,8,,,CHARSET:ANSI),TRN
                         TEXT,AT(1823,1510,5521,417),USE(jbn_ali:Invoice_Text),FONT(,8,,,CHARSET:ANSI),TRN
                         STRING(@s16),AT(6250,521),USE(job_ali:ESN),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Mobile Number:'),AT(5156,833),USE(?String6:6),FONT(,8,,FONT:bold),TRN
                         STRING('Repair Details:'),AT(208,1510,1094,313),USE(?String2:6),FONT(,10,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING(@s16),AT(6250,677),USE(job_ali:MSN),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING('Parts Used:'),AT(208,1979,1094,313),USE(?String2:3),FONT(,10,,FONT:bold,CHARSET:ANSI), |
  TRN
                         STRING('Chargeable Parts'),AT(208,2240),USE(?String37),FONT(,8,,FONT:underline),TRN
                         STRING('Warranty Parts'),AT(3594,2240),USE(?String37:2),FONT(,8,,FONT:underline),TRN
                         STRING(@s25),AT(1927,2448),USE(tmp:Parts[2]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(208,2604),USE(tmp:Parts[3]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(1927,2604),USE(tmp:Parts[4]),FONT(,8)
                         STRING(@s25),AT(1927,2760),USE(tmp:Parts[6]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(3594,2448),USE(tmp:Parts[7]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(5260,2448),USE(tmp:Parts[8]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(208,2760),USE(tmp:Parts[5]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(5260,2604),USE(tmp:Parts[10]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(3594,2604),USE(tmp:Parts[9]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(5260,2760),USE(tmp:Parts[12]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(3594,2760),USE(tmp:Parts[11]),FONT(,8,,,CHARSET:ANSI)
                         STRING(@s25),AT(208,2448),USE(tmp:Parts[1]),FONT(,8,,,CHARSET:ANSI)
                         LINE,AT(208,2969,7031,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Unit Type:'),AT(1823,833),USE(?String6:4),FONT(,8,,FONT:bold),TRN
                         STRING(@s30),AT(2917,677),USE(job_ali:Model_Number),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING('I.M.E.I. Number:'),AT(5156,521),USE(?String6:5),FONT(,8,,FONT:bold),TRN
                         STRING('Manufacturer:'),AT(1823,521),USE(?String6:2),FONT(,8,,FONT:bold),TRN
                         STRING(@s30),AT(2917,521),USE(job_ali:Manufacturer),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING('M.S.N.:'),AT(5156,677),USE(?STRING3),FONT(,8,,FONT:bold),TRN
                         STRING(@s30),AT(2917,833),USE(job_ali:Unit_Type),FONT(,8,,,CHARSET:ANSI),LEFT,TRN
                         STRING(@s16),AT(6250,833),USE(job_ali:Mobile_Number),FONT(,8),TRN
                       END
                       FORM,AT(396,479,7521,10552),USE(?unnamed:3)
                         STRING('BOUNCER HISTORY REPORT'),AT(1260,52,5000,417),USE(?string20),FONT(,24,,FONT:bold), |
  CENTER,TRN
                         BOX,AT(104,521,7292,9844),USE(?Box1),COLOR(COLOR:Black),LINEWIDTH(1)
                       END
                     END
!*** PDF-Tools v4.1 Start (All)
!--- ClarioNET NOT Enabled
!--- PDFXTABCTemplate: True  Level: 4
!--- PDFXTABCTemplate: RPM: False
!--- PDFXTABCTemplate: Icetips: False
PDFXTR1               PDFXToolsReportClass                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:rtn           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXTR1:vpf           LONG                                 !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Orientation      BYTE(0)                      !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:TopMargin        REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:BottomMargin     REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:LeftMargin       REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:RightMargin      REAL(18)                     !  Added by PDF-Tools Templates v4.1
PDFXT4:Paper:Selection        STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PDFXT4:Papers:Queue           QUEUE,PRE()                  !  Added by PDF-Tools Templates v4.1
PLQ:Description                 STRING(SIZE(PDFXT4:Papers:Data.Description)) !  Added by PDF-Tools Templates v4.1
PLQ:PaperCode                   BYTE                       !  Added by PDF-Tools Templates v4.1
                              END                          !  Added by PDF-Tools Templates v4.1
!*** PDF-Tools v4.1 End
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNoRecords          PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepStringClass                       ! Progress Manager
Previewer            CLASS(PrintPreviewClass)              ! Print Previewer
Display                PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1),BYTE,DERIVED
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BouncerHistory')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
        IF (p_web.IfExistsValue('imei'))
            tmp:IMEINumber = p_web.GetValue('imei')
        ELSE
            tmp:IMEINumber = p_web.GSV('job:ESN') 
        END ! IF        
  Relate:JOBS_ALIAS.SetOpenRelated()
  Relate:JOBS_ALIAS.Open                                   ! File JOBS_ALIAS used by this procedure, so make sure it's RelationManager is open
  Relate:TagFile.Open                                      ! File TagFile used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:PARTS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:WARPARTS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('BouncerHistory',ProgressWindow)            ! Restore window settings from non-volatile store
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    loc:pdfname = '.\$$$' & format(random(1,99999),@n05) &'.pdf'
    ProgressWindow{prop:hide} = 1
  End
  ProgressMgr.Init(ScrollSort:AllowAlpha+ScrollSort:AllowNumeric,ScrollBy:RunTime)
  ThisReport.Init(Process:View, Relate:JOBS_ALIAS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job_ali:ESN)
  ThisReport.CaseSensitiveValue = FALSE
  ThisReport.AddSortOrder(job_ali:ESN_Key)
  ThisReport.AddRange(job_ali:ESN,tmp:IMEINumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,report,Previewer)
  ?Progress:UserString{PROP:Text} = ''
  Relate:JOBS_ALIAS.SetQuickScan(1,Propagate:OneMany)
  ProgressWindow{PROP:Timer} = 10                          ! Assign timer interval
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom = True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

Loc:Html  String(1024)
  CODE
  ! Report procedure should have prototype of (<NetWebServerWorker p_web>)
  If Not p_Web &= NULL
    If Loc:NoRecords
      ReportNoRecords(p_web)
    Else
      p_web.ReplyContentType = p_web._GetContentType('.pdf')
      p_web._Sendfile(loc:PDFName)
    End
  End
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS_ALIAS.Close
    Relate:TagFile.Close
    Relate:WEBJOB.Close
  END
  IF SELF.Opened
    INIMgr.Update('BouncerHistory',ProgressWindow)         ! Save window data to non-volatile store
  END
  ! Save Window Name
  AddToLog('Report End',p_web.RequestData.DataString,'BouncerHistory',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
  !*** PDF-Tools v4.1 Start (ABC)
  IF ~ReturnValue                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.Init(report, loc:PDFName,,PDFA_OFF)            !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPaper(CHOOSE(SYSTEM{PROP:Autopaper}=True,report{PROPPRINT:Paper},PAPER:USER), CHOOSE(report{PROP:Thous}=True,PROP:Thous,CHOOSE(report{PROP:MM}=True,PROP:MM,CHOOSE(report{PROP:Points}=True,PROP:Points,0))), report{PROPPRINT:PaperWidth}, report{PROPPRINT:PaperHeight}, report{PROP:Landscape}) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetCompression(TRUE,TRUE,FALSE,COMPRTYPE_C_AUTO,75,COMPRTYPE_I_AUTO,COMPRTYPE_M_AUTO) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetAuthor('ServiceBase 3g')          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetTitle('Bouncer History')          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSubject('ServiceBase 3g')         !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPdfCreator('ServiceBase 3g')      !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetKeywords()                        !  Added by PDF-Tools Templates v4.1
    PDFXTR1.EnableLinkAnalyzer(True)             !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetSpecVersion(SPECVERSION_15,TRUE)  !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageLayout(PAGELAYOUT_SINGLEPAGE) !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetPageMode(PAGEMODE_NONE)           !  Added by PDF-Tools Templates v4.1
    IF PAGEMODE_NONE = PAGEMODE_FULLSCREEN       !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = VP_FSPM_NONE                 !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
      PDFXTR1:vpf = 0                            !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDETOOLBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEMENUBAR). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_HIDEWINDOWUI). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_FITWINDOW). !  Added by PDF-Tools Templates v4.1
      IF FALSE                                    THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_CENTERWINDOW). !  Added by PDF-Tools Templates v4.1
      IF TRUE                                     THEN PDFXTR1:vpf = BOR(PDFXTR1:vpf,VP_DISPLAYDOCTITLE). !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetViewerPrefs(PDFXTR1:vpf)          !  Added by PDF-Tools Templates v4.1
    PDFXTR1.SetFontEmbedding(True, True, False, False, False)
    PDFXTR1.SetPreview(False)                    !  Added by PDF-Tools Templates v4.1
  END
  !*** PDF-Tools v4.1 End
  IF ReturnValue = Level:Benign
    report$?ReportPageNumber{PROP:PageNo} = True
  END
  ! Save Window Name
  AddToLog('Report Start',p_web.RequestData.DataString,'BouncerHistory',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TakeCloseEvent()
  !*** PDF-Tools v4.1 Start (ABC)
  PDFXTR1.Kill()
  !*** PDF-Tools v4.1 End
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
    If Not p_Web &= NULL
      loc:NoRecords = 1
      Return
    End
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
        Clear(tmp:Parts)
        PartNumber# = 0
        Access:PARTS.Clearkey(par:Part_Number_Key)
        par:Ref_Number = job_ali:Ref_Number
        Set(par:Part_Number_Key,par:Part_Number_Key)
        Loop
            If Access:PARTS.Next()
                Break
            End ! If Access:PARTS.Next()
            If par:Ref_Number <> job_ali:Ref_Number
                Break
            End ! If par:Ref_Number <> job_ali:Ref_Number
            PartNumber# += 1
            If PartNumber# > 6
                Break
            End ! If PartNumber# > 6
            tmp:Parts[PartNumber#] = par:Description
        End ! Loop
  
        PartNumber# = 6
        Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
        wpr:Ref_Number = job_ali:Ref_Number
        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
        Loop
            If Access:WARPARTS.Next()
                Break
            End ! If Access:WARPARTS.Next()
            If wpr:Ref_Number <> job_ali:Ref_Number
                Break
            End ! If wpr:Ref_Number <> job_ali:Ref_Number
            PartNumber# += 1
            If PartNumber# > 12
                Break
            End ! If PartNumber# > 12
            tmp:Parts[PartNumber#] = wpr:Description
        End ! Loop
  
        barcodeJobNumber = '*' & clip(job_ali:Ref_Number) & '*'
  
        
        IF (p_web.GetValue('showall') <> 1)
        
            Access:TagFile.ClearKey(tag:keyTagged)
            tag:taggedValue = job_ali:Ref_Number
            tag:sessionID = p_web.SessionID
            IF (Access:TagFile.TryFetch(tag:keyTagged))
                RETURN Level:USer
            ELSE
                IF (tag:tagged <> 1)
                    RETURN Level:USer
                END ! IF
            END ! IF
        END ! IF
        
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job_ali:Ref_Number
        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
        END ! IF
  ReturnValue = PARENT.TakeRecord()
  PRINT(rpt:detail)
  RETURN ReturnValue


Previewer.Display PROCEDURE(SHORT InitZoomFactor=0,LONG InitCurrentPage=1,USHORT InitPagesAcross=1,USHORT InitPagesDown=1)

ReturnValue          BYTE,AUTO

  CODE
  !*** PDF-Tools v4.1 Start (ABC)
  ReturnValue = False                            !  Added by PDF-Tools Templates v4.1
    PDFXTR1:rtn = PDFXTR1.Generate(SELF.ImageQueue,False, , ) !  Added by PDF-Tools Templates v4.1
    IF PDFXTR1:rtn THEN                          !  Added by PDF-Tools Templates v4.1
    ELSE                                         !  Added by PDF-Tools Templates v4.1
    END                                          !  Added by PDF-Tools Templates v4.1
  RETURN ReturnValue
  !*** PDF-Tools v4.1 End
  ReturnValue = PARENT.Display(InitZoomFactor,InitCurrentPage,InitPagesAcross,InitPagesDown)
  RETURN ReturnValue

