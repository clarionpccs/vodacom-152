

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER432.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WEBSERVER134.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER430.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WEBSERVER431.INC'),ONCE        !Req'd for module callout resolution
                     END


QAPass               PROCEDURE  (NetWebServerWorker p_web,long p_stage=0)
! the 'pre' functions are called when the form _opens_
! the 'post' functions are called when the 'save' or 'cancel' or 'delete' button is pressed
! remember this will happen on 2 separate threads. So use static, unthreaded variables here
! if you want to carry information from the pre, to the post, stage.
! or better yet, save the items in the sessionqueue

! there are 3 stages in the form
!   NET:WEB:StagePre which is called when the form _opens_
!   NET:WEB:StageValidate which is called when the form _closes_, before the record is written
!   NET:WEB:StagePost which is called _after_ the record is written
Ans                  LONG                                  !
ListAccessory        STRING(30)                            !
locNetwork           STRING(30)                            !
locRefurb            LONG                                  !
                    MAP
ValidateAccessories     PROCEDURE()
                    END ! MAP
FilesOpened     Long
TRADEACC::State  USHORT
SUBTRACC::State  USHORT
TRANTYPE::State  USHORT
MANUFACT::State  USHORT
NETWORKS::State  USHORT
Tagging::State  USHORT
LOANACC::State  USHORT
JOBACC::State  USHORT
ACCESSOR::State  USHORT
WebStyle                   Long
CRLF                       string('<13,10>')
NBSP                       string('&#160;')
loc:viewonly               Long
loc:formname               string(256)
loc:formaction             string(256)
loc:formactioncancel       string(256)
loc:formactioncanceltarget string(256)
loc:formactiontarget       string(256)
loc:extra                  string(256)
loc:autocomplete           String(20)
loc:enctype                string(256)
loc:javascript             string(2048)
loc:tabs                   string(256)
loc:readonly               String(32)
loc:lookuponly             String(32)
loc:invalid                String(100)
loc:alert                  String(256)
loc:comment                String(256)
loc:prompt                 String(256)
loc:invalidtab             Long
loc:tabnumber              Long
loc:retrying               Long
loc:loadedrelated          Long,Static,Thread
loc:lookupdone             Long
loc:tabheight              Long
loc:fieldclass             string(256)
loc:action                 string(40)
loc:act                    Long
loc:width                  String(40)
loc:rowstyle               String(256)
loc:even                   Long
loc:columncounter          Long
loc:maxcolumns             Long
loc:rowstarted             Long
loc:cellstarted            Long
loc:FirstInCell            Long
packet                     string(16384)
packetlen                  long
locNetwork_OptionView   View(NETWORKS)
                          Project(net:Network)
                        End
    MAP
trace	PROCEDURE(STRING pText)	
    END
  CODE
  If p_web.GetSessionLoggedIn() = 0
    Return -1
  End
  GlobalErrors.SetProcedureName('QAPass')
  loc:formname = 'QAPass_frm'
  WebStyle = Net:Web:None
  do SetAction
  ans = band(p_stage,255)
  case p_stage
  of 0
    ! Save Window Name
    AddToLog('NetWebForm',p_web.RequestData.DataString,'QAPass',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    p_web.FormReady('QAPass','')
    p_web._DivHeader('QAPass',clip('fdiv') & ' ' & clip('FormContent'))
    do PreUpdate
    do SetPics
    do GenerateForm
    p_web._DivFooter()

  of Net:Web:SetPics
  orof Net:Web:SetPics + NET:WEB:StageValidate
    do SetPics

  of Net:Web:Init
    do InitForm

  of Net:Web:AfterLookup + Net:Web:Cancel
    loc:LookupDone = 0
    do AfterLookup

  of Net:Web:AfterLookup
    loc:LookupDone = 1
    do AfterLookup

  of Net:Web:Cancel
    do CancelForm
  of InsertRecord + NET:WEB:StagePre
    if p_web._InsertAfterSave = 0
      p_web.setsessionvalue('SaveReferQAPass',p_web.getPageName(p_web.RequestReferer))
    end
    do StoreMem
    do PreInsert
  of InsertRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateInsert
  of Net:CopyRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferQAPass',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreCopy
  of Net:CopyRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateCopy

  of ChangeRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferQAPass',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreUpdate
    p_web.setsessionvalue('showtab_QAPass',0)
  of ChangeRecord + NET:WEB:StageValidate
    do RestoreMem
    If loc:act = InsertRecord
      do ValidateInsert
    ElsIf loc:act = Net:CopyRecord
      do ValidateCopy
    Else
      do ValidateUpdate
    End
  of ChangeRecord + NET:WEB:StagePost
    do RestoreMem
    do PostUpdate

  of DeleteRecord + NET:WEB:StagePre
    p_web.setsessionvalue('SaveReferQAPass',p_web.getPageName(p_web.RequestReferer))
    do StoreMem
    do PreDelete
  of DeleteRecord + NET:WEB:StageValidate
    do RestoreMem
    do ValidateDelete
  of Net:Web:Div
    do CallDiv

  End ! Case
  If Loc:Invalid
    Ans = Net:Web:InvalidRecord
    If p_web.GetValue('retry') <> ''
      p_web.requestfilename = p_web.GetValue('retry')
      if p_web.IfExistsValue('_parentPage') = 0
        p_web.SetValue('_parentPage',p_web.requestfilename)
      End
    End
    p_web.SetValue('retryfield',Loc:Invalid)
    p_web.setsessionvalue('showtab_QAPass',Loc:InvalidTab)
  End
  if loc:alert <> '' then p_web.SetValue('alert',loc:Alert).
  GlobalErrors.SetProcedureName()
    ! Save Window Name
    IF (loc:alert <> '')
        AddToLog('Alert',loc:alert,'QAPass',p_web.SessionID,p_web.PageName,p_web.GSV('BookingUserCode'))
    END ! IF
  return Ans
OpenFiles  ROUTINE
  p_web._OpenFile(TRADEACC)
  p_web._OpenFile(SUBTRACC)
  p_web._OpenFile(TRANTYPE)
  p_web._OpenFile(MANUFACT)
  p_web._OpenFile(NETWORKS)
  p_web._OpenFile(Tagging)
  p_web._OpenFile(LOANACC)
  p_web._OpenFile(JOBACC)
  p_web._OpenFile(ACCESSOR)
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(TRANTYPE)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(NETWORKS)
  p_Web._CloseFile(Tagging)
  p_Web._CloseFile(LOANACC)
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(ACCESSOR)
     FilesOpened = False
  END

InitForm       Routine
  DATA
LF  &FILE
  CODE
  p_web.SetValue('QAPass_form:inited_',1)
  do RestoreMem

CancelForm  Routine

SendAlert Routine
    p_web.Message('Alert',loc:alert,p_web._MessageClass,Net:Send)

SetPics        Routine
AfterLookup Routine
  loc:TabNumber = -1
  If p_web.GSV('ValidateNetwork') = 1
    loc:TabNumber += 1
  End
  If p_web.GSV('ValidateRefurb') = 1
    loc:TabNumber += 1
  End
  loc:TabNumber += 1
  loc:TabNumber += 1
  p_web.DeleteValue('LookupField')

StoreMem       Routine
  p_web.SetSessionValue('locNetwork',locNetwork)
  p_web.SetSessionValue('locRefurb',locRefurb)

RestoreMem       Routine
  !FormSource=Memory
  if p_web.IfExistsValue('locNetwork')
    locNetwork = p_web.GetValue('locNetwork')
    p_web.SetSessionValue('locNetwork',locNetwork)
  End
  if p_web.IfExistsValue('locRefurb')
    locRefurb = p_web.GetValue('locRefurb')
    p_web.SetSessionValue('locRefurb',locRefurb)
  End

SetAction  routine
  If loc:ViewOnly = 0
    Case p_web.GetSessionValue('QAPass_CurrentAction')
    of InsertRecord
      loc:action = p_web.site.InsertPromptText
      loc:act = InsertRecord
    of Net:CopyRecord
      loc:action = p_web.site.CopyPromptText
      loc:act = Net:CopyRecord
    of ChangeRecord
      loc:action = p_web.site.ChangePromptText
      loc:act = ChangeRecord
    of DeleteRecord
      loc:action = p_web.site.DeletePromptText
      loc:act = DeleteRecord
    End
  End
GenerateForm   Routine
  do LoadRelatedRecords
    ! Clear Vars
    IF (p_web.IfExistsValue('firsttime'))
        p_web.SSV('HideMySave',1)
        p_web.SSV('HideMyCancel',1)
        p_web.SSV('FailReason','')          
        ClearTagFile(p_web)
        
        p_web.SSV('tmp:LoanModelNumber',p_web.GSV('job:Model_Number')) ! Set filter for tag browse
  
    ! Do we need to validate the network on the job? (DBH: 02/03/2015)
        p_web.SSV('ValidateNetwork','')
        Access:MANUFACT.ClearKey(man:Manufacturer_Key)
        man:Manufacturer = p_web.GSV('job:Manufacturer')
        IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
            IF (man:QANetwork)
                p_web.SSV('ValidateNetwork',1)
            END ! I F
        END ! IF
    
        p_web.SSV('locNetwork','')    
    
    
    ! Check Refurb
        p_web.SSV('ValidateRefurb','')
        p_web.SSV('locRefurb','')
        IF (p_web.GSV('locHandsetType') = 'Repair' OR | 
            p_web.GSV('locHandsetType') = 'Third Party Repair')
        
            Access:TRANTYPE.ClearKey(trt:Transit_Type_Key)
            trt:Transit_Type = p_web.GSV('job:Transit_Type')
            IF (Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign)
                IF (trt:Exchange_Unit = 'YES' OR | 
                    p_web.GSV('job:Exchange_Unit_Number') > 0 OR | 
                    SUB(p_web.GSV('job:Exchange_Status'),1,3) = '108')
            
                    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
                    sub:Account_Number = p_web.GSV('job:Account_Number')	
                    IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
                        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                        tra:Account_Number = sub:Main_Account_Number
                        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                            IF (tra:RefurbCharge = 'YES')
                                p_web.SSV('ValidateRefurb',1)
                            END ! IF
                        ELSE ! IF
                        END ! IF
                    ELSE ! IF
                    END ! IF
                END ! IF
            END ! IF
        END ! IF
    END ! IF
    
    
      p_web.site.CancelButton.TextValue = 'Cancel'
  packet = clip(packet) & '<script type="text/javascript">popuperror=1</script><13,10>'
 locNetwork = p_web.RestoreValue('locNetwork')
 locRefurb = p_web.RestoreValue('locRefurb')
  loc:FormAction = p_web.GetValue('onsave')
  If loc:formaction = 'stay'
    loc:FormAction = p_web.Requestfilename
  Else
    loc:formaction = p_web.getsessionvalue('SaveReferQAPass')
  End
  if p_web.IfExistsValue('ChainTo')
    loc:formaction = p_web.GetValue('ChainTo')
    p_web.SetSessionValue('QAPass_ChainTo',loc:FormAction)
    loc:formactiontarget = '_self'
  ElsIf p_web.IfExistsSessionValue('QAPass_ChainTo')
    loc:formaction = p_web.GetSessionValue('QAPass_ChainTo')
    loc:formactiontarget = '_self'
  End
  If loc:FormActionTarget = ''
    loc:FormActionTarget = '_self'
  End
  If loc:formaction = ''
    loc:formaction = lower(p_web.getPageName(p_web.RequestReferer))
  End
  loc:FormActionCancel = 'QAProcedure?clear=1'
  If p_web.IfExistsValue('retryField')
    loc:retrying = 1
  End
  loc:viewonly = Choose(p_web.IfExistsValue('View_btn'),1,loc:viewonly)
  do SetAction
  packet = clip(packet) & '<form action="'&clip(loc:formaction)&'" '&clip(loc:enctype)&' method="post" name="'&clip(loc:formname)&'" id="'&clip(loc:formname)&'" target="'&clip(loc:FormActionTarget)&'" onsubmit="osf(this);"><13,10>'
  if loc:viewonly and p_web.IfExistsValue('LookupField')
    packet = clip(packet) & '<input type="hidden" name="LookupField" value="'&p_web._jsok(p_web.GetValue('LookupField'))&'" ></input><13,10>'
    packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
  else
    If p_web.IfExistsValue('_parentPage')
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="'&p_web.GetValue('_parentPage')&'" ></input><13,10>'
    Else
      packet = clip(packet) & '<input type="hidden" name="FromParent" value="QAPass" ></input><13,10>'
      packet = clip(packet) & '<input type="hidden" name="Retry" value="QAPass" ></input><13,10>'
    End
    packet = clip(packet) & '<input type="hidden" name="FromForm" value="QAPass" ></input><13,10>'
  end

  do SendPacket
  If p_web.Translate('Validate Accessories') <> ''
    packet = clip(packet) & '<span class="'&clip('SubHeading')&'">'&p_web.Translate('Validate Accessories',0)&'</span>'&CRLF
  End
  !packet = clip(packet) & p_web.br !Bryan Why is there a blank here?
  do SendPacket

  Case WebStyle
  of Net:Web:Outlook
    Packet = clip(Packet) & '<div id="MainMenu" class="'&clip('accordionOverall')&'"><13,10>'
    
  of Net:Web:TabXP
  orof Net:Web:Tab
        Packet = clip(Packet) & '<div id="Tab_QAPass">'&CRLF
  else
        Packet = clip(Packet) & '<div id="Tab_QAPass" class="'&clip('MyTab')&'">'&CRLF
    
  End
  do GenerateTab0
  do GenerateTab1
  do GenerateTab2
  do GenerateTab3
  Case WebStyle
  of Net:Web:Outlook
    loc:TabNumber# = p_web.GetSessionValue('showtab_QAPass')
    Packet = clip(Packet) &'</div>'&CRLF &|
                               '<script type="text/javascript">onloads.push( accord ); function accord() {{ new Rico.Accordion( ''MainMenu'', {{panelHeight:350, onLoadShowTab:'& loc:tabNumber# &'} ); } </script>'
    do SendPacket
  of Net:Web:TabXP
  orof Net:Web:Tab
        loc:tabs = ''
        If p_web.GSV('ValidateNetwork') = 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Validate Network') & ''''
        End
        If p_web.GSV('ValidateRefurb') = 1
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Validate Refurb') & ''''
        End
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & '''' & p_web.Translate('Validate Accessories for '& p_web.GSV('job:Model_Number')&' Handset') & ''''
          if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
          loc:tabs = clip(loc:tabs) & ''''&p_web.Translate('General')&''''
        Loc:Tabnumber = p_web.getSessionValue('showtab_QAPass')
        If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
        Packet = clip(Packet) & '</div>'&CRLF &|
        '<script type="text/javascript">initTabs(''Tab_QAPass'',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',"100%","");</script>' ! ie can't deal with a width of ""....
    
  else
      Packet = clip(Packet) & '</div>'&CRLF
    
  End
  Case WebStyle
  of Net:Web:Wizard
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizPreviousButton,loc:formname,,,'wizPrev();')
    packet = clip(packet) & p_web.CreateStdButton('button',Net:Web:WizNextButton,loc:formname,,,'wizNext();')
    do SendPacket
  End
    if loc:ViewOnly = 0
        loc:javascript = ''
        loc:javascript = clip(loc:javascript) & 'removeElement('''&clip(loc:formname)&''','''&lower('QAPass_TagValidateLoanAccessories_embedded_div')&''');'
        packet = clip(packet) & p_web.CreateStdButton('submit',Net:Web:CancelButton,loc:formname,loc:formactioncancel,loc:formactioncanceltarget,loc:javascript)
    Else
    End
  
  if loc:retrying
    p_web.SetValue('SelectField',clip(loc:formname) & '.' & p_web.GetValue('retryfield'))
  Elsif p_web.IfExistsValue('Select_btn')
  Else
    If False
    ElsIf p_web.GSV('ValidateNetwork') = 1
        p_web.SetValue('SelectField',clip(loc:formname) & '.locNetwork')
    ElsIf p_web.GSV('ValidateRefurb') = 1
    Else
    End
  End
    Case WebStyle
    of Net:Web:Wizard
          loc:tabs = ''
          If p_web.GSV('ValidateNetwork') = 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab3'''
          End
          If p_web.GSV('ValidateRefurb') = 1
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab4'''
          End
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab1'''
            if loc:tabs <> '' then loc:tabs = clip(loc:tabs) & ','.
            loc:tabs = clip(loc:tabs) & '''Tab2'''
          Loc:Tabnumber = p_web.getSessionValue('showtab_QAPass')
          If Loc:Tabnumber < 0 then Loc:TabNumber = 0.
          Packet = clip(Packet) & '<script type="text/javascript">initWizard('''&clip(loc:formname)&''',Array('&clip(loc:tabs)&'),'&loc:tabnumber&',' & loc:TabHeight & ');</script>'
    End
  packet = clip(packet) & '</form>'&CRLF
  do SendPacket
  packet = clip(packet) & '<script type="text/javascript"><13,10>'
  Case WebStyle
  of Net:Web:Rounded
    packet = clip(packet) & 'var roundCorners = Rico.Corner.round.bind(Rico.Corner);'&CRLF
    if p_web.GSV('ValidateNetwork') = 1
      packet = clip(packet) & 'roundCorners(''tab3'');'&CRLF
    end
    if p_web.GSV('ValidateRefurb') = 1
      packet = clip(packet) & 'roundCorners(''tab4'');'&CRLF
    end
      packet = clip(packet) & 'roundCorners(''tab1'');'&CRLF
      packet = clip(packet) & 'roundCorners(''tab2'');'&CRLF
  of Net:Web:Plain
  End
  packet = clip(packet) & '</script><13,10>'
  do SendPacket
  do SendPacket
GenerateTab0  Routine
  If p_web.GSV('ValidateNetwork') = 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel3">'&CRLF &|
                                    '  <div id="panel3Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Validate Network') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel3Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_QAPass_3">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Validate Network')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab3" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab3">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Validate Network')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Validate Network')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab3">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Validate Network')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locNetwork
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locNetwork
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locNetwork
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab1  Routine
  If p_web.GSV('ValidateRefurb') = 1
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel4">'&CRLF &|
                                    '  <div id="panel4Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Validate Refurb') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel4Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_QAPass_4">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Validate Refurb')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab4" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab4">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Validate Refurb')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Validate Refurb')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab4">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Validate Refurb')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ''
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::txtRefurb
      do Comment::txtRefurb
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td valign="top"'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Prompt::locRefurb
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::locRefurb
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::locRefurb
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
  end
GenerateTab2  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel1">'&CRLF &|
                                    '  <div id="panel1Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                          p_web.Translate('Validate Accessories for '& p_web.GSV('job:Model_Number')&' Handset') &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel1Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_QAPass_1">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Validate Accessories for '& p_web.GSV('job:Model_Number')&' Handset')&'</span>' &CRLF
      packet = clip(packet) & '<div id="tab1" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab1">'
        packet = clip(packet) & '<fieldset><legend class="'&clip('MainHeading')&'">'&p_web.Translate('Validate Accessories for '& p_web.GSV('job:Model_Number')&' Handset')&'</legend>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Validate Accessories for '& p_web.GSV('job:Model_Number')&' Handset')&'</span>' &CRLF

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab1">'
      packet = clip(packet) & '<span class="'&clip('MainHeading')&'">'&p_web.Translate('Validate Accessories for '& p_web.GSV('job:Model_Number')&' Handset')&'</span>' &CRLF
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
      
        if loc:maxcolumns = 0 then loc:maxcolumns = 3.
        packet = clip(packet) & '<td colspan="'&loc:maxcolumns&'">'&CRLF
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      do Value::brwAccessories
      do Comment::brwAccessories
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnTagAll
      do Comment::btnTagAll
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::btnUnTagAll
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::btnUnTagAll
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket
GenerateTab3  Routine
    Case WebStyle
    of Net:Web:Outlook
            Packet = clip(Packet) & '<div id="panel2">'&CRLF &|
                                    '  <div id="panel2Header" class="'&clip('accordionTabTitleBar')&'">'&CRLF &|
                                    '  </div>'&CRLF &|
                                    '  <div id="panel2Content"  class="'&clip('accordionTabContentBox')&'">'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
          Packet = clip(Packet) & '<div class="dhtmlgoodies_aTab" id="Tab_QAPass_2">'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '<div id="tab2" class="'&clip('formcorner')&'">' &CRLF

    of Net:Web:Plain
      packet = clip(packet) & '<div id="Tab2">'
        packet = clip(packet) & '<fieldset>' &CRLF

    of Net:Web:None
      packet = clip(packet) & '<div id="Tab2">'

    of Net:Web:Wizard
      packet = clip(packet) & '<div id="Tab2">'
    End
    loc:rowstarted = 0
    packet = clip(packet) & '<table class="'&clip('FormCentre')&'">'&CRLF
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ButtonValidate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::ButtonValidate
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ButtonMySave
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::ButtonMySave
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
      if loc:rowstarted = 0
        packet = clip(packet) & '<tr>'&CRLF
        if loc:columncounter > loc:maxcolumns then loc:maxcolumns = loc:columncounter.
        loc:columncounter = 0
        loc:rowstarted = 1
      end
      do SendPacket
      loc:width = ' width="'&50&'"'
      If loc:cellstarted = 0
      
        packet = clip(packet) & '<td'&clip(loc:width)& '>'
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
        loc:FirstInCell = 1
      Else
        loc:FirstInCell = 0
      End
      If loc:FirstInCell = 1
        packet = clip(packet) & '</td>'&CRLF ! Field Heading
        loc:cellstarted = 0
        do SendPacket
      End
      if loc:cellstarted = 0
        loc:width = ' width="'&200&'"'
                packet = clip(packet) & '<td'&clip(loc:width)&'>'&CRLF
        loc:columncounter += 1
        do SendPacket
        loc:cellstarted = 1
      end
      do Value::ButtonMyCancel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      loc:width = ''
      packet = clip(packet) & '<td'&clip(loc:width)&'>'
      loc:columncounter += 1
      do SendPacket
      do Comment::ButtonMyCancel
      packet = clip(packet) & '</td>'&CRLF
      loc:cellstarted = 0
      do SendPacket
      if loc:cellstarted
        packet = clip(packet) & '</td></tr>'&CRLF
        loc:cellstarted = 0
      Else
        packet = clip(packet) & '</tr>'&CRLF
      End
      loc:rowstarted = 0
    do SendPacket
    if loc:rowstarted and loc:cellstarted
      packet = clip(packet) & '</td></tr></table>'&CRLF
      loc:cellstarted = 0
      loc:rowstarted = 0
    elsif loc:rowstarted
      packet = clip(packet) & '</tr></table>'&CRLF
      loc:rowstarted = 0
    else
      packet = clip(packet) & '</table>'&CRLF
    end
    Case WebStyle
    of Net:Web:Outlook
          Packet = clip(Packet) & '</div>'&CRLF&'</div>'&CRLF
    of Net:Web:TabXP
    orof Net:Web:Tab
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Rounded
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Plain
      packet = clip(packet) & '</fieldset></div>'&CRLF
    of Net:Web:None
      packet = clip(packet) & '</div>'&CRLF
    of Net:Web:Wizard
      packet = clip(packet) & '</div>'&CRLF
    End
    do SendPacket


Prompt::locNetwork  Routine
  p_web._DivHeader('QAPass_' & p_web._nocolon('locNetwork') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Network')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locNetwork  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locNetwork',p_web.GetValue('NewValue'))
    locNetwork = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locNetwork
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locNetwork',p_web.GetValue('Value'))
    locNetwork = p_web.GetValue('Value')
  End
  do Value::locNetwork
  do SendAlert

Value::locNetwork  Routine
  p_web._DivHeader('QAPass_' & p_web._nocolon('locNetwork') & '_value','adiv')
  loc:extra = ''
  ! --- DROPLIST ---
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locNetwork')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:even = 1
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onchange="'&p_web._nocolon('sv(''locNetwork'',''qapass_locnetwork_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('locNetwork')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  loc:readonly = Choose(loc:viewonly,'disabled','')
  packet = clip(packet) & p_web.CreateSelect('locNetwork',loc:fieldclass,loc:readonly,,174,,loc:javascript,)
              loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
  if p_web.IfExistsSessionValue('locNetwork') = 0
    p_web.SetSessionValue('locNetwork','')
  end
    packet = clip(packet) & p_web.CreateOption('','',choose('' = p_web.getsessionvalue('locNetwork')),clip(loc:rowstyle),,)&CRLF
  loc:even = Choose(loc:even=1,2,1)
  p_web.translateoff += 1
  pushbind()
  p_web._OpenFile(TRADEACC)
  bind(tra:Record)
  p_web._OpenFile(SUBTRACC)
  bind(sub:Record)
  p_web._OpenFile(TRANTYPE)
  bind(trt:Record)
  p_web._OpenFile(MANUFACT)
  bind(man:Record)
  p_web._OpenFile(NETWORKS)
  bind(net:Record)
  p_web._OpenFile(Tagging)
  bind(tgg:Record)
  p_web._OpenFile(LOANACC)
  bind(lac:Record)
  p_web._OpenFile(JOBACC)
  bind(jac:Record)
  p_web._OpenFile(ACCESSOR)
  bind(acr:Record)
  if p_web.sqlsync then p_web.RequestData.WebServer._Wait().
  open(locNetwork_OptionView)
  locNetwork_OptionView{prop:order} = 'UPPER(net:Network)'
  Set(locNetwork_OptionView)
  Loop
    Next(locNetwork_OptionView)
    If ErrorCode() then Break.
    if p_web.IfExistsSessionValue('locNetwork') = 0
      p_web.SetSessionValue('locNetwork',net:Network)
    end
        loc:rowstyle = choose(sub(' SelectList',1,1)=' ',clip(loc:fieldclass) & clip(' SelectList') & loc:even,clip(' SelectList') & loc:even)
    packet = clip(packet) & p_web.CreateOption(,net:Network,choose(net:Network = p_web.getsessionvalue('locNetwork')),clip(loc:rowstyle),,) &CRLF
    loc:even = Choose(loc:even=1,2,1)
    do SendPacket
  End
  Close(locNetwork_OptionView)
  p_web.translateoff -= 1
  if p_web.sqlsync then p_web.RequestData.WebServer._Release().
  p_Web._CloseFile(TRADEACC)
  p_Web._CloseFile(SUBTRACC)
  p_Web._CloseFile(TRANTYPE)
  p_Web._CloseFile(MANUFACT)
  p_Web._CloseFile(NETWORKS)
  p_Web._CloseFile(Tagging)
  p_Web._CloseFile(LOANACC)
  p_Web._CloseFile(JOBACC)
  p_Web._CloseFile(ACCESSOR)
  PopBind()
  packet = clip(packet) & '</select>'&CRLF
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('QAPass_' & p_web._nocolon('locNetwork') & '_value')

Comment::locNetwork  Routine
    loc:comment = ''
  p_web._DivHeader('QAPass_' & p_web._nocolon('locNetwork') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::txtRefurb  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('txtRefurb',p_web.GetValue('NewValue'))
    do Value::txtRefurb
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::txtRefurb  Routine
  p_web._DivHeader('QAPass_' & p_web._nocolon('txtRefurb') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.

  packet = clip(packet) & |
    '<span class="'&clip('GreenBold')&'">' & p_web.Translate('This unit is authorised for refurbishment',) & '</span>' & |
    '<13,10>'
  do SendPacket
  p_web._DivFooter()

Comment::txtRefurb  Routine
    loc:comment = ''
  p_web._DivHeader('QAPass_' & p_web._nocolon('txtRefurb') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Prompt::locRefurb  Routine
  p_web._DivHeader('QAPass_' & p_web._nocolon('locRefurb') & '_prompt',clip(p_web.site.PromptClass) &'')
  loc:prompt = p_web.Translate('Refurbishment Complete?')
  packet = clip(packet) & loc:prompt
  do SendPacket
  p_web._DivFooter()

Validate::locRefurb  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('locRefurb',p_web.GetValue('NewValue'))
    locRefurb = p_web.GetValue('NewValue') !FieldType=  Field = 
    do Value::locRefurb
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('locRefurb',p_web.GetValue('Value'))
    locRefurb = p_web.GetValue('Value')
  End
  do Value::locRefurb
  do SendAlert

Value::locRefurb  Routine
  p_web._DivHeader('QAPass_' & p_web._nocolon('locRefurb') & '_value','adiv')
  loc:extra = ''
  ! --- RADIO --- locRefurb
  loc:fieldclass = 'FormEntry'
  If lower(loc:invalid) = lower('locRefurb')
    loc:fieldclass = clip(loc:fieldclass) & ' ' & 'formerror'
  End
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locRefurb') = 'Yes'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locRefurb'',''qapass_locrefurb_value'',1,'''&clip('Yes')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locRefurb',clip('Yes'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locRefurb_1') & '<13,10>'
    packet = clip(packet) & p_web.Translate('Yes') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  loc:readonly = Choose(loc:viewonly,'disabled','')
    if p_web.GetSessionValue('locRefurb') = 'No'
      loc:readonly = clip(loc:readonly) & ' checked' & Choose(loc:viewonly,' disabled','')
    else
      loc:readonly = clip(loc:readonly) & Choose(loc:viewonly,' disabled','')
    end
    loc:javascript = ''
    loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''locRefurb'',''qapass_locrefurb_value'',1,'''&clip('No')&''')')&';'
    if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
    packet = clip(packet) & p_web.CreateInput('radio','locRefurb',clip('No'),loc:fieldclass,loc:readonly,loc:extra,,loc:javascript,,,'locRefurb_2') & '<13,10>'
    packet = clip(packet) & p_web.Translate('No') & '<13,10>'
    packet = clip(packet) & '&#160;&#160;&#160;&#160;<13,10>'
  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('QAPass_' & p_web._nocolon('locRefurb') & '_value')

Comment::locRefurb  Routine
    loc:comment = ''
  p_web._DivHeader('QAPass_' & p_web._nocolon('locRefurb') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::brwAccessories  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('brwAccessories',p_web.GetValue('NewValue'))
    do Value::brwAccessories
  Else
    p_web.StoreValue('acr:Accessory')
  End

Value::brwAccessories  Routine
  loc:extra = ''
  ! --- BROWSE ---  TagValidateLoanAccessories --
  p_web.SetValue('TagValidateLoanAccessories:NoForm',1)
  p_web.SetValue('TagValidateLoanAccessories:FormName',loc:formname)
  p_web.SetValue('TagValidateLoanAccessories:parentIs','Form')
  p_web.SetValue('_parentProc','QAPass')
  if p_web.RequestAjax = 0
    packet = clip(packet) & '<div id="'&lower('QAPass_TagValidateLoanAccessories_embedded_div')&'"><!-- Net:TagValidateLoanAccessories --></div><13,10>'
    p_web._DivHeader('QAPass_' & lower('TagValidateLoanAccessories') & '_value')
    p_web._DivFooter()
    p_web._RegisterDivEx('QAPass_' & lower('TagValidateLoanAccessories') & '_value')
  else
    packet = clip(packet) & '<!-- Net:TagValidateLoanAccessories --><13,10>'
  end
  do SendPacket

Comment::brwAccessories  Routine
    loc:comment = ''
  p_web._DivHeader('QAPass_' & p_web._nocolon('brwAccessories') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnTagAll  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnTagAll',p_web.GetValue('NewValue'))
    do Value::btnTagAll
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
    Access:ACCESSOR.ClearKey(acr:Accesory_Key)
    acr:Model_Number = p_web.GSV('job:Model_Number')
    SET(acr:Accesory_Key,acr:Accesory_Key)
    LOOP UNTIL Access:ACCESSOR.Next() <> Level:Benign
        IF (acr:Model_Number <> p_web.GSV('job:Model_Number'))
            BREAK
        END ! IF
  
  	    ! Add Tag
        Access:Tagging.ClearKey(tgg:SessionIDKey)
        tgg:SessionID = p_web.SessionID
        tgg:TaggedValue = acr:Accessory
        IF (Access:Tagging.TryFetch(tgg:SessionIDKey) = Level:Benign)
            IF (tgg:Tagged <> 1)
                tgg:Tagged = 1
                Access:Tagging.TryUpdate()
            END ! IF
        ELSE ! IF
            IF (Access:Tagging.PrimeRecord() = Level:Benign)
                tgg:SessionID = p_web:SessionID
                tgg:TaggedValue = acr:Accessory
                tgg:Tagged = 1
                IF (Access:Tagging.TryInsert())
                    Access:Tagging.CancelAutoInc()
                END ! IF
            END ! IF            
        END ! IF        
  
    END ! LOOP
  do SendAlert
  do Value::brwAccessories  !1

Value::btnTagAll  Routine
  p_web._DivHeader('QAPass_' & p_web._nocolon('btnTagAll') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnTagAll'',''qapass_btntagall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnTagAll','Tag All','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('QAPass_' & p_web._nocolon('btnTagAll') & '_value')

Comment::btnTagAll  Routine
    loc:comment = ''
  p_web._DivHeader('QAPass_' & p_web._nocolon('btnTagAll') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::btnUnTagAll  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('btnUnTagAll',p_web.GetValue('NewValue'))
    do Value::btnUnTagAll
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
  ClearTaggingFile(p_web)
  do SendAlert
  do Value::brwAccessories  !1

Value::btnUnTagAll  Routine
  p_web._DivHeader('QAPass_' & p_web._nocolon('btnUnTagAll') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''btnUnTagAll'',''qapass_btnuntagall_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','btnUnTagAll','Untag Tall','MainButton',loc:formname,,,,loc:javascript,0,,,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('QAPass_' & p_web._nocolon('btnUnTagAll') & '_value')

Comment::btnUnTagAll  Routine
    loc:comment = ''
  p_web._DivHeader('QAPass_' & p_web._nocolon('btnUnTagAll') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::ButtonValidate  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ButtonValidate',p_web.GetValue('NewValue'))
    do Value::ButtonValidate
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End
      ValidateAccessories()     
  do SendAlert
  do Value::ButtonMyCancel  !1
  do Value::ButtonMySave  !1

Value::ButtonValidate  Routine
  p_web._DivHeader('QAPass_' & p_web._nocolon('ButtonValidate') & '_value','adiv')
  loc:extra = ''
  ! --- DISPLAY --- 
  loc:javascript = ''
  loc:javascript = clip(loc:javascript) & ' onclick="'&p_web._nocolon('sv(''ButtonValidate'',''qapass_buttonvalidate_value'',1,FieldValue(this,1))')&';'
  loc:javascript = clip(loc:javascript) & 'nextFocus('&clip(loc:formname)&','''&p_web._nocolon('')&''',0);'
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','Validate','Attempt Validation','button-entryfield',loc:formname,,,,loc:javascript,0,'/images/lookup.png',,,,)

  do SendPacket
  p_web._DivFooter()
  p_web._RegisterDivEx('QAPass_' & p_web._nocolon('ButtonValidate') & '_value')

Comment::ButtonValidate  Routine
    loc:comment = ''
  p_web._DivHeader('QAPass_' & p_web._nocolon('ButtonValidate') & '_comment',clip(p_web.site.CommentClass) &' adiv')
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::ButtonMySave  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ButtonMySave',p_web.GetValue('NewValue'))
    do Value::ButtonMySave
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::ButtonMySave  Routine
  p_web._DivHeader('QAPass_' & p_web._nocolon('ButtonMySave') & '_value',Choose(p_web.gsv('HideMySave')=true,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.gsv('HideMySave')=true)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ButtonMySave','Pass Validation','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('QAProcedure?' &'upd=1')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/psave.png',,,,'This will pass the current QA')

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('QAPass_' & p_web._nocolon('ButtonMySave') & '_value')

Comment::ButtonMySave  Routine
    loc:comment = ''
  p_web._DivHeader('QAPass_' & p_web._nocolon('ButtonMySave') & '_comment',Choose(p_web.gsv('HideMySave')=true,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.gsv('HideMySave')=true
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

Validate::ButtonMyCancel  Routine
  If p_web._RequestAjaxNow = 1 and p_web.ifExistsValue('NewValue')
    p_web.SetSessionValue('ButtonMyCancel',p_web.GetValue('NewValue'))
    do Value::ButtonMyCancel
  ElsIf p_web.IfExistsValue('Value')
    p_web.SetSessionValue('',p_web.GetValue('Value'))
  End

Value::ButtonMyCancel  Routine
  p_web._DivHeader('QAPass_' & p_web._nocolon('ButtonMyCancel') & '_value',Choose(p_web.gsv('HideMyCancel') = true,'hdiv','adiv'))
  loc:extra = ''
  If Not (p_web.gsv('HideMyCancel') = true)
  ! --- DISPLAY --- 
  loc:javascript = ''
  if loc:javascript <> '' then loc:javascript = clip(loc:javascript) & '"'.
  packet = clip(packet) & |
  p_web.CreateButton('button','ButtonMyCancel','Fail Validation','button-entryfield',loc:formname,,,'window.open('''& p_web._MakeURL(clip('QA_FailReason')) & ''','''&clip('_self')&''')',loc:javascript,0,'/images/pcancel.png',,,,)

  do SendPacket
  End
  p_web._DivFooter()
  p_web._RegisterDivEx('QAPass_' & p_web._nocolon('ButtonMyCancel') & '_value')

Comment::ButtonMyCancel  Routine
    loc:comment = ''
  p_web._DivHeader('QAPass_' & p_web._nocolon('ButtonMyCancel') & '_comment',Choose(p_web.gsv('HideMyCancel') = true,'hdiv',clip(p_web.site.CommentClass) &' adiv'))
  If p_web.gsv('HideMyCancel') = true
    loc:comment = ''
  End
  packet = clip(packet) & loc:comment
  do SendPacket
  p_web._DivFooter()

CallDiv    routine
  p_web._RequestAjaxNow = 1
  p_web.PageName = p_web._unEscape(p_web.PageName)
  case lower(p_web.PageName)
  of lower('QAPass_locNetwork_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locNetwork
      else
        do Value::locNetwork
      end
  of lower('QAPass_locRefurb_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::locRefurb
      else
        do Value::locRefurb
      end
  of lower('QAPass_btnTagAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnTagAll
      else
        do Value::btnTagAll
      end
  of lower('QAPass_btnUnTagAll_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::btnUnTagAll
      else
        do Value::btnUnTagAll
      end
  of lower('QAPass_ButtonValidate_value')
      case p_web.GetValue('event')
      of event:selected !257
      of event:accepted !1
        do Validate::ButtonValidate
      else
        do Value::ButtonValidate
      end
  End

SendPacket routine
  packetlen = len(clip(packet))
  if packetlen > 0
    p_web.ParseHTML(packet, 1, packetlen, NET:NoHeader)
    packet = ''
    packetlen = 0
  end

! NET:WEB:StagePRE
PreInsert       Routine
  p_web.SetValue('QAPass_form:ready_',1)
  p_web.SetSessionValue('QAPass_CurrentAction',InsertRecord)
  p_web.setsessionvalue('showtab_QAPass',0)

PreCopy  Routine
  p_web.SetValue('QAPass_form:ready_',1)
  p_web.SetSessionValue('QAPass_CurrentAction',Net:CopyRecord)
  p_web.setsessionvalue('showtab_QAPass',0)
  ! here we need to copy the non-unique fields across

PreUpdate       Routine
  p_web.SetValue('QAPass_form:ready_',1)
  p_web.SetSessionValue('QAPass_CurrentAction',ChangeRecord)
  p_web.SetSessionValue('QAPass:Primed',0)

PreDelete       Routine
  p_web.SetValue('QAPass_form:ready_',1)
  p_web.SetSessionValue('QAPass_CurrentAction',DeleteRecord)
  p_web.SetSessionValue('QAPass:Primed',0)
  p_web.setsessionvalue('showtab_QAPass',0)

LoadRelatedRecords  Routine
  loc:loadedrelated = 1

CompleteCheckBoxes  Routine
  If p_web.GSV('ValidateNetwork') = 1
  End
  If p_web.GSV('ValidateRefurb') = 1
  End

! NET:WEB:StageVALIDATE
ValidateInsert  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateCopy  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateUpdate  Routine
  do CompleteCheckBoxes
  do ValidateRecord

ValidateDelete  Routine
  p_web.DeleteSessionValue('QAPass_ChainTo')
  ! Check for restricted child records

ValidateRecord  Routine
  p_web.DeleteSessionValue('QAPass_ChainTo')

  ! Then add additional constraints set on the template
  loc:InvalidTab = -1
  ! tab = 3
  If p_web.GSV('ValidateNetwork') = 1
    loc:InvalidTab += 1
  End
  ! tab = 4
  If p_web.GSV('ValidateRefurb') = 1
    loc:InvalidTab += 1
  End
  ! tab = 1
    loc:InvalidTab += 1
  ! tab = 2
    loc:InvalidTab += 1
  ! The following fields are not on the form, but need to be checked anyway.
! NET:WEB:StagePOST

PostUpdate      Routine
  p_web.SetSessionValue('QAPass:Primed',0)
  p_web.StoreValue('locNetwork')
  p_web.StoreValue('')
  p_web.StoreValue('locRefurb')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
  p_web.StoreValue('')
SaveSessionVars  ROUTINE
     p_web.SSV('Ans',Ans) ! LONG
     p_web.SSV('ListAccessory',ListAccessory) ! STRING(30)
     p_web.SSV('locNetwork',locNetwork) ! STRING(30)
     p_web.SSV('locRefurb',locRefurb) ! LONG
RestoreSessionVars  ROUTINE
     Ans = p_web.GSV('Ans') ! LONG
     ListAccessory = p_web.GSV('ListAccessory') ! STRING(30)
     locNetwork = p_web.GSV('locNetwork') ! STRING(30)
     locRefurb = p_web.GSV('locRefurb') ! LONG
ValidateAccessories PROCEDURE()
tagError                LONG
accessoryCount          LONG
accessorySelected       LONG
noneSelected            LONG
    CODE
        LOOP 1 TIMES
            p_web.SSV('FailReason','ACCESSORIES MISSING')
            
            IF (p_web.GSV('locHandsetType') = 'Repair' OR p_web.GSV('locHandSetType') = 'Third Party Repair')
                IF (p_web.GSV('ValidateNetwork') = 1)
                    IF (p_web.GSV('locNetwork') = '')
                        loc:alert = 'Please select a network.'
                        BREAK
                    END !I F
                    IF (p_web.GSV('locNetwork') <> p_web.GSV('jobe:Network')) 
                        loc:alert = 'ERROR: The selected network does not match the network assigned to this job. Please select another and attempt valdation again, or Fail Validation.'
                        p_web.SSV('FailReason','')
                        BREAK
                    END ! IF
                END ! IF
                
                IF (p_web.GSV('ValidateRefurb'))
                    IF (p_web.GSV('locRefurb') = '')
                        loc:alert = 'You must select whether the refurb has been completed.'
                        BREAK
                    ELSIF (p_web.GSV('locRefurb') = 'No')
                        loc:alert = 'ERROR: IF the refurb has not been completed then selected Fail Validation'
                        p_web.SSV('FailReason','')
                        BREAK                        
                    END ! IF
                END ! IF
            END ! IF
        
        
            IF (p_web.GSV('locHandsetType') = 'Loan')
                tagError = 0

                Access:LOANACC.ClearKey(lac:Ref_Number_Key)
                lac:Ref_Number = p_web.GSV('job:Loan_Unit_Number')
                SET(lac:Ref_Number_Key,lac:Ref_Number_Key)
                LOOP UNTIL Access:LOANACC.Next() <> Level:Benign
                    IF (lac:Ref_Number <> p_web.GSV('job:Loan_Unit_Number'))
                        BREAK
                    END ! IF	
				
				! Has this accessory been tagged?
                    Access:Tagging.ClearKey(tgg:SessionIDKey)
                    tgg:SessionID = p_web.SessionID
                    tgg:TaggedValue = lac:Accessory
                    IF (Access:Tagging.TryFetch(tgg:SessionIDKey) = Level:Benign)
                        IF (tgg:Tagged <> 1)
                            tagError = 1
                            BREAK
                        END ! IF
                    ELSE ! IF
                        tagError = 1
                        BREAK
                    END ! IF


                    accessoryCount += 1

                END ! LOOP

                IF (tagError = 1)
                    loc:alert = 'ERROR: There is an accessory associated with this loan that has not been selected. Re-select accessories and attempt validation again or Fail Validation.'
                END ! IF (tagError = 1)
            ELSE ! IF (p_web.GSV('locHandsetType'))	
                tagError = 0

                Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                jac:Ref_Number = p_web.GSV('job:Ref_Number')
                SET(jac:Ref_Number_Key,jac:Ref_Number_Key)
                LOOP UNTIL Access:JOBACC.Next() <> Level:Benign
                    IF (jac:Ref_Number <> p_web.GSV('job:Ref_Number'))
                        BREAK
                    END ! IF
				
				! Has this accessory been tagged?
                    Access:Tagging.ClearKey(tgg:SessionIDKey)
                    tgg:SessionID = p_web.SessionID
                    tgg:TaggedValue = jac:Accessory
                    IF (Access:Tagging.TryFetch(tgg:SessionIDKey) = Level:Benign)
                        IF (tgg:Tagged <> 1)
                            tagError = 1
                            BREAK
                        END ! IF
                    ELSE ! IF
                        tagError = 1
                        BREAK
                    END ! IF                    

                    accessoryCount += 1		
                END ! LOOP

                IF (tagError = 1)
                    loc:alert = 'ERROR: There is an accessory associated with this job that has not been selected. Re-select accessories and attempt validation again or Fail Validation.'
                    BREAK
                END ! IF (tagError = 1)

            END ! IF (p_web.GSV('locHandsetType'))

			! No error. Lets see if all the tagged items are correct
            noneSelected = 0
            Access:Tagging.ClearKey(tgg:SessionIDKey)
            tgg:SessionID = p_web.SessionID
            SET(tgg:SessionIDKey,tgg:SessionIDKey)
            LOOP UNTIL Access:Tagging.Next() <> Level:Benign
                IF (tgg:SessionID <> p_web.SessionID)
                    BREAK
                END ! IF
                IF (tgg:Tagged = 0)
                    CYCLE
                END ! IF
                accessorySelected += 1
                IF (tgg:TaggedValue = 'NONE')
                    noneSelected = 1
                END ! IF 
            END ! LOOP            


                tagError = 0
                IF (accessorySelected < accessoryCount)
                    tagError = 1
                ELSIF (accessorySelected > accessoryCount)
                    IF (accessoryCount = 0 AND accessorySelected = 1) 
					! There are no accessories to selected, but only one was tagged
					! Was that "NONE"?
                        IF (noneSelected = 0)
                            tagError = 1
                        END ! IF
                    ELSE ! IF
                        tagError = 1
                    END ! IF
                END ! IF

                IF (tagError = 1)
                    loc:alert = 'ERROR: More accessories have been selected than are associated with this ' & CHOOSE(p_web.GSV('locHandsetType') = 'Loan','loan','job') & '. Re-select accessories and attempt validation again or Fail Validation.'
                END ! IF
        END ! LOOP

        IF (loc:alert <> '')
            p_web.SSV('HideMySave',1)
            p_web.SSV('HideMyCancel',0)
        ELSE
            p_web.SSV('HideMySave',0)
            p_web.SSV('HideMyCancel',1)
            p_web.SSV('FailReason','PASSED')
            
            ! QA Passed, just redirect back to QA Procedure. 
            trace('QA Passed')
        END ! IF		
		
trace	PROCEDURE(STRING pText)
outString	CSTRING(1000)
    CODE
        RETURN ! Logging Disabled
