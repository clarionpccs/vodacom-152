

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER135.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
IsOneYearWarranty    PROCEDURE  (STRING manufacturer,STRING modelNumber) ! Declare Procedure

  CODE
    RETURN vod.OneYearWarranty(manufacturer,modelNumber)
