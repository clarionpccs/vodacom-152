

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER372.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
IsIMEIValid          PROCEDURE  (STRING pIMEINumber,STRING pModelNumber) ! Declare Procedure
i                           LONG()
foundAlpha                  LONG()
retValue                    LONG(TRUE)
FilesOpened     BYTE(0)

  CODE
        IF (pIMEINumber = '' OR pModelNumber = '')
            RETURN TRUE
        END ! IF
        
        LOOP i = 1 TO LEN(CLIP(pIMEINumber))
            IF (ISALPHA(pIMEINumber[i]))
                foundAlpha = TRUE
                BREAK
            END ! IF
        END!  LOOP
    DO OpenFiles        
        IF (foundAlpha)
            Access:MODELNUM.ClearKey(mod:Model_Number_Key)
            mod:Model_Number = pModelNumber
            IF (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
                IF (mod:AllowIMEICharacters)
                    retValue = TRUE
                ELSE
                    retValue = FALSE
                END ! IF
            ELSE ! IF
            END ! IF
        END ! IF

        DO CloseFiles
        
        RETURN retValue
!--------------------------------------
OpenFiles  ROUTINE
  Access:MODELNUM.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MODELNUM.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:MODELNUM.Close
     FilesOpened = False
  END
