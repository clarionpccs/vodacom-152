

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER269.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Despatch:Loa         PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
    Do OpenFiles
    if (p_web.GSV('BookingSite') = 'RRC')    
    
        ! Add To COnsignment History
        IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
            joc:RefNumber = p_web.GSV('job:Ref_Number')
            joc:TheDate = TODAY()
            joc:TheTime = CLOCK()
            joc:UserCode = p_web.GSV('BookingUserCode')
            joc:DespatchFrom = 'RRC'
            joc:DespatchTo = 'CUSTOMER'
            joc:Courier = p_web.GSV('job:Loan_Courier')
            joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
            joc:DespatchType = 'LOA'
            IF (Access:JOBSCONS.TryInsert())
                Access:JOBSCONS.CancelAutoInc()
            END
        END    
    
        p_web.SSV('wob:ReadyToDespatch',0)
    
        p_web.SSV('AddToAudit:Action','DESPATCH FROM RRC')
        p_web.SSV('AddToAudit:Notes','UNIT NO: ' & p_web.GSV('job:Loan_Unit_Number') & |
            '<13,10>COURIER: ' & p_web.GSV('job:Loan_Courier') & |
            '<13,10>WAYBILL NO: ' & p_web.GSV('locWaybillNumber'))
    
    
        p_web.SSV('jobe:Despatched','YES')
    ELSE ! if (p_web.GSV('BookingSite') = 'RRC')
        p_web.SSV('job:Loan_Consignment_Number',p_web.GSV('locWaybillNumber'))
        p_web.SSV('job:Loan_Despatched',Today())
        p_web.SSV('job:Despatched','')
    
        ! Add To COnsignment History
        IF (Access:JOBSCONS.PrimeRecord() = Level:Benign)
            joc:RefNumber = p_web.GSV('job:Ref_Number')
            joc:TheDate = TODAY()
            joc:TheTime = CLOCK()
            joc:UserCode = p_web.GSV('BookingUserCode')
            joc:DespatchFrom = 'ARC'
            joc:DespatchTo = 'CUSTOMER'
            joc:Courier = p_web.GSV('job:Loan_Courier')
            joc:ConsignmentNumber = p_web.GSV('locWaybillNumber')
            joc:DespatchType = 'LOA'
            IF (Access:JOBSCONS.TryInsert())
                Access:JOBSCONS.CancelAutoInc()
            END
        END
    
        p_web.SSV('AddToAudit:Action','LOAN UNIT DESPATCHED VIA ' & p_web.GSV('job:Loan_Courier'))
        p_web.SSV('AddToAudit:Notes','CONSIGNMENT NOTE NUMBER: ' & p_web.GSV('locWaybillNumber'))
    
        Access:LOAN.Clearkey(loa:Ref_Number_Key)
        loa:Ref_Number = p_web.GSV('job:Loan_Unit_Number')
        if (Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign)
            loa:Available = 'DES'
            if (Access:LOAN.TryUpdate() = Level:Benign)
                If (Access:LOANHIST.PrimeRecord() = Level:Benign)
                    loh:Ref_Number = loa:Ref_Number
                    loh:Date = Today()
                    loh:Time = Clock()
                    loh:User = p_web.GSV('BookingUserCode')
                    loh:Status = 'UNIT DESPATCHED ON JOB: ' & p_web.GSV('job:Ref_Number')
                    Access:LOANHIST.TryInsert()
                END
            END
        END
    
    
    END ! if (p_web.GSV('BookingSite') = 'RRC')
    
        GetStatus(901,0,'LOA',p_web)
    
    p_web.SSV('jobe:LoaSecurityPackNo',p_web.GSV('locSecurityPackID'))
    
    
    ! Add To Audit
    p_web.SSV('AddToAudit:Type','LOA')
    IF (p_web.GSV('locSecurityPackID') <> '')
        p_web.SSV('AddToAudit:Notes',p_web.GSV('AddToAudit:Notes') & |
            '<13,10>SECURITY PACK NO: ' & p_web.GSV('locSecurityPackID'))
    END
    AddToAudit(p_web,p_web.GSV('job:Ref_Number'),'LOA',p_web.GSV('AddToAudit:Action'),p_web.GSV('AddToAudit:Notes'))
    Do CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:LOANHIST.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOANHIST.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:LOAN.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:LOAN.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBSCONS.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:LOANHIST.Close
     Access:LOAN.Close
     Access:JOBSCONS.Close
     FilesOpened = False
  END
