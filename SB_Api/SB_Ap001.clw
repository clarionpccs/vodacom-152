

   MEMBER('sb_api.clw')                               ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SB_AP001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

LocalDir             QUEUE(File:Queue),PRE(LDR)
                     END
X                    LONG
Y                    LONG
FileDirectory        STRING(255)
FirstPartName        STRING(255)
ImportFileName       STRING(255),STATIC
OutputFileName       STRING(255),STATIC
TempFullLine         STRING(1000)
LocalToken           STRING(20)
EndToken             STRING(4)
LocalArchiveFlag     BYTE
SaveDirectory        STRING(255)
LastLetter           STRING(1)
LocalRegion          STRING(30)
window               WINDOW('SB3g'),AT(0,0,161,54),FONT('Tahoma',6,,FONT:regular,CHARSET:ANSI),ICON('Cellular3g.ico'),TILED,TIMER(25),GRAY,ICONIZE,DOUBLE,IMM
                       BUTTON,AT(28,4),USE(?Button1),FLAT,ICON('closep.jpg')
                       STRING('API Interface.'),AT(44,41),USE(?String1),TRN,FONT(,8,,,CHARSET:ANSI)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ImportFile    File,Driver('BASIC','/COMMA=124'),Pre(impfile),Name(ImportFileName),Create,Bindable,Thread
Record       Record
Request        String(30)
UserName       String(30)
Password       String(30)
Token          String(20)
Detail1        String(30)           !always needed job reference or IMEI or their reference
Detail2        String(30)           !option fields from here on - need  16 for JOBBOOK
Detail3        String(30)
Detail4        String(30)
Detail5        String(30)
Detail6        String(30)
Detail7        String(30)
Detail8        String(30)
Detail9        String(30)
Detail10       String(30)
Detail11       String(30)
Detail12       String(30)
Detail13       String(30)
Detail14       String(30)
Detail15       String(30)         
Detail16       String(200)         !has to be longer to hold the fault text
Detail17       String(30)          !these are not currenly in use and will remain blank untill later
Detail18       String(30)
Detail19       String(30)
Detail20       String(30)

             End
         End


!" 1   VodacomPortalRef
!" 2   Title
!" 3   Initial
!" 4   Surname
!" 5   Address line 1
!" 6   Address line 2
!" 7   Suburb
!" 8   Work telephone number
!" 9   Mobile number
!" 10   ID number
!" 11   Mobile Device IMEI number
!" 12   Manufacturer
!" 13   Model
!" 14   Repair Outlet
!" 15   Description of problem



OutputFile    File,Driver('ASCII'),Pre(outfile),Name(OutputFileName),Create,Bindable,Thread
Record       Record
FullLine          String(1000)
             End
             End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

API_Interface       Routine

    !read the ini file incase of any changes
    FileDirectory = getini('API','DIRECTORY','',clip(path())&'\SB_API.INI')
    if clip(FileDirectory) = '' then exit.

    if GetIni('API','ACTIVE','',clip(path())&'\SB_API.INI') <> 'Y' then
        Post(Event:closewindow)
    END

    LocalArchiveFlag = false
    if GetIni('API','ARCHIVE','',clip(path())&'\SB_API.INI') = 'Y' then
        SaveDirectory =  GetIni('API','ARCHIVEDIR','',clip(path())&'\SB_API.INI') 
        If clip(SaveDirectory) <> ''
            LocalArchiveFlag = true
        END
    END

    Free(localdir)
    Directory(localdir,clip(FileDirectory)&'\*.in',ff_:NORMAL)
    if records(LocalDir) = 0 then exit.

    !Scan the directory for any .in files
    Loop X = 1 to records(LocalDir)
        Get(LocalDir,X)
        Do API_INterpret
    END

    EXIT


API_Interpret       Routine

    !get the start of the name - use for other files
    Y = instring('.',LDR:Name,1,1)
    FirstPartName = LDR:Name[ 1 : Y ]

    !rename the import file so it is not picked up next time
    Rename(clip(FileDirectory)&'\'&clip(LDR:Name), Clip(FileDirectory)&'\'&clip(FirstPartName)&'inc')

    !create name for output file and remove if existing
    OutputFileName = Clip(FileDirectory)&'\'&clip(FirstPartName)&'ret'
    if exists(OutputFileName) then remove(OutputFileName).

    !back to the import file - it should be settled by now
    ImportFileName = Clip(FileDirectory)&'\'&clip(FirstPartName)&'inc'

    !wait till the renamed import file appears
    Loop
        if exists(ImportFilename) then break.
        yield()
    END

    Open(ImportFile)
    if error() then
        Glo:ErrorText = 'ERROR|Unable to open the file sent'
    ELSE

        Set(ImportFile)
        Next(ImportFile)     !there should be exactly one entry in the file

        if error() then
            Glo:ErrorText = 'ERROR|Unable to read the content of the file'
        ELSE

            !at this point we have
            !impfile:Request        String(30)
            !impfile:UserName       String(30)
            !impfile:Password       String(30)
            !impfile:Token          String(20)
            !impfile:Detail1        String(1000)
            Glo:ErrorText = ''

            Do CheckToken           !Generates a new one if this is a login
            Do ValidatePassword

            if Glo:ErrorText = ''

                Case impfile:Request
                    of 'STATUS'
                        !try for exchange status first
                        GenerateSMSText('E',deformat(impfile:Detail1),'N')  !returns in glo:ErrorText
                        if Glo:ErrorText[1:5] = 'ERROR' then
                            !try for job
                            GenerateSMSText('J',deformat(impfile:Detail1),'N')  !returns in glo:ErrorText
                        END !if Error reported on exchange

                    of 'JOBSTATUS'
                        GenerateSMSText('J',deformat(impfile:Detail1),'N')  !returns in glo:ErrorText

                    of 'LOANSTATUS'
                        GenerateSMSText('L',deformat(impfile:Detail1),'N')  !returns in glo:ErrorText

                    of 'EXCHANGESTATUS'
                        GenerateSMSText('E',deformat(impfile:Detail1),'N')  !returns in glo:ErrorText

                    of 'JOBDETAILS'     !gives details of current job
                        GenerateJobDetails(deformat(impfile:detail1))  !returns in glo:ErrorText

                    of 'JOBCHECK'       !does this imie exist on a current/previous job
                        JobCheck(clip(impfile:Detail1))

                    of 'PREJOBCHECK'    !does this imei exist on a prejob that has not been converted
                        PreJobCheck(clip(impfile:Detail1))

                    of 'PREBOOK'    !this creates a prejob booking
                        DO PreBook

                    of 'MAKECHECK'  !needs to return make and model from IMEI - misspecced was originally CHECKMAKE now MAKECHECK
                        CheckMake(clip(impfile:Detail1))

                    !TB13370 - adding waybil courier integration
                    of 'WAYBILLCONFIRM'    !15 char
                        WaybillConfirm(clip(impfile:Detail1),'CONFIRMED',clip(impfile:Detail2),clip(impfile:Detail3),clip(impfile:Detail4),clip(impfile:Detail5))
                        !(SentWaybill,SentAction,SentDateTime,SentSender,SentReceiver,SentContact)

                    of 'WAYBILLCOLLECT'    !15 char
                        WaybillConfirm(clip(impfile:Detail1),'COLLECTED',clip(impfile:Detail2),clip(impfile:Detail3),clip(impfile:Detail4),clip(impfile:Detail5))

                    of 'WAYBILLDELIVER'
                        WaybillConfirm(clip(impfile:Detail1),'DELIVERED',clip(impfile:Detail2),clip(impfile:Detail3),clip(impfile:Detail4),clip(impfile:Detail5))

                    !TB13370 - adding waybil courier integration
                    ELSE
                        Glo:ErrorText = 'ERROR|Call of '&clip(impfile:Request)&' is not supported by the system'
                END !cass status
            END !if Glo:ErrorText showed there was no error on password
        END !error on reading file
    END !Error on opening file

    !return the sucess or error here
    Create(OutputFile)
    Open(outputFile)
    Outfile:FullLine = LocalToken & '|' & clip(Glo:ErrorText)
    Add(outputFile)
    Close(outputFile)

    if LocalArchiveFlag then
        copy(Outputfilename,SaveDirectory)
    END

    !this bit of code needs to be done asap
    Rename(Outputfilename, Clip(FileDirectory)&'\'&clip(FirstPartName)&'out')

    Close(ImportFile)

    if LocalArchiveFlag then
        copy(ImportFileName,SaveDirectory)
    END

    remove(ImportFileName)

    EXIT

CheckToken   routine

    if impfile:Token = '' then

        ENDToken = 'AAAA'  !Currently only the last couple of letter are changed giving 676 combinations
                           !may need to rethink this if they are getting more than 676 calls a second

        Loop
            !create a new one
            LocalToken = 'D'&format(today(),@d12) & 'H' & format(clock(),@t5 )&EndToken

            Access:APIToken.clearkey(API:KeyToken)
            API:Token = LocalToken
            if access:APIToken.fetch(API:KeyToken) then Break.  !not found this one in the file

            !Found the existing token so need to create a new one
            LastLetter = ENDToken[4]
            if LastLetter = 'Z' then
                ENDToken[4] = 'A'
                LastLetter = EndToken[3]
                ENDToken[3]  = CHR(VAL(LastLetter) +1)
            ELSE
                ENDToken[4]  = CHR(VAL(LastLetter) +1)
            END
        END !loop with End token

        Access:APITOKEN.primerecord()
        API:Token           = LocalToken
        API:UserName        = impfile:UserName
        API:JobRef_number   = deformat(impfile:Detail1)
        API:PreJobRefNo     = 0
        API:LastCall        = impfile:Request
        API:EntryDate       = today()
        API:EntryTime       = clock()
        Access:APIToken.update()

    ELSE
        LocalToken = Impfile:Token      !'CF9879Hjj999877569pp'
        Access:APIToken.clearkey(API:KeyToken)
        API:Token = LocalToken
        if access:APIToken.fetch(API:KeyToken)
            Glo:ErrorText = 'ERROR|Invalid token sent as part of the call'
        ELSE
            if API:EntryDate < today() or API:EntryTime < clock() - 60000     !10 minute from this reasoning  100 = 1 sec *60 = 6000 = imin *10 = 60000
                Glo:ErrorText = 'ERROR|Token sent as part of the call has expired'
            ELSE
                API:LastCall = impfile:Request
                Access:APIToken.update
            END
        END
    END


    EXIT


ValidatePassword            Routine

    if clip(impfile:Token)  = '' then

        Access:users.clearkey(use:password_key)
        use:Password = impfile:Password
        if access:Users.fetch(use:password_key)
            Glo:ErrorText = 'ERROR|Password sent is invalid - no user found with this password'
        ELSe
            if impfile:UserName = clip(use:Forename)&' '&clip(use:Surname) then
                !this is OK
            ELSE
                Glo:ErrorText = 'ERROR|User name and password do not match'
            END !if name matches
        END !if password found

    END !if running by login

    EXIT
PreBook     Routine

!" 1   VodacomPortalRef
!" 2   Title
!" 3   Initial
!" 4   Surname
!" 5   Address line 1
!" 6   Address line 2
!" 7   Suburb
!" 8   Work telephone number
!" 9   Mobile number
!" 10   ID number
!" 11   Mobile Device IMEI number
!" 12   Manufacturer
!" 13   Model
!" 14   Repair Outlet
!" 15   ChargeType
!" 16   Description of problem (200 chars)


    Do PreBookSub   !so I can use exit

    EXIT


PreBookSub          Routine

    !Standard error used if primerecord() or update() fail
    Glo:ErrorText = 'ERROR|Data update failure - please try again'

    Access:Chartype.clearkey(cha:Charge_Type_Key)
    cha:Charge_Type = upper(impfile:Detail15)
    if access:CHARTYPE.fetch(cha:Charge_Type_Key)
        Glo:ErrorText = 'ERROR|Charge type not recognised'
        EXIT
    END

    LocalRegion = GetRegion(impfile:Detail7)
    IF LocalRegion = 'ERROR' then
        glo:ErrorText = 'ERROR|Unable to track region from suburb'
        EXIT
    END

    if Access:PreJob.primerecord() = level:Benign

        PRE:APIRecordNumber     = API:RecordNumber
        PRE:JobRef_number       = 0
        PRE:VodacomPortalRef    = Upper(impfile:Detail1)
        PRE:Title               = Upper(impfile:Detail2)
        PRE:Initial             = Upper(impfile:Detail3)
        PRE:Surname             = Upper(impfile:Detail4)
        PRE:Address_Line1       = Upper(impfile:Detail5)
        PRE:Address_Line2       = Upper(impfile:Detail6)
        PRE:Suburb              = Upper(impfile:Detail7)
        PRE:Region              = Upper(LocalRegion)        !
        PRE:Telephone_number    = Upper(impfile:Detail8)
        PRE:Mobile_Number       = Upper(impfile:Detail9)
        PRE:ID_number           = Upper(impfile:Detail10)
        PRE:ESN                 = Upper(impfile:Detail11)
        PRE:Manufacturer        = Upper(impfile:Detail12)
        PRE:Model_number        = Upper(impfile:Detail13)
        PRE:DOP                 = deformat(impfile:Detail14,@d06)       !mm/dd/yy 01/03/12
        PRE:ChargeType          = upper(impfile:Detail15)
        PRE:FaultText           = Upper(impfile:Detail16)
        PRE:RepairOutletRef     = 0                                     !this will be updated when booked in
        PRE:Date_booked         = today()
        PRE:Time_Booked         = clock()

        IF Access:PreJob.update() = LEVEL:Benign

            GetAddressesFromRegion(PRE:Region)

            Glo:ErrorText = 'SUCESS|'&clip(LocalRegion)&'|'&clip(glo:ErrorText)

        ELSE
            !error can remain as above
            Access:preJob.cancelautoinc()

        END

    END

    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Button1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:APITOKEN.Open
  Relate:CHARTYPE.Open
  Relate:PREJOB.Open
  Access:USERS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:APITOKEN.Close
    Relate:CHARTYPE.Close
    Relate:PREJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button1
      ThisWindow.Update
      Post(Event:Closewindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:Timer
      do API_Interface
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

GenerateSMSText      PROCEDURE  (SentJob,SentRefNumber,JobOpen) ! Declare Procedure
LocalStatus          STRING(30)
LocalLocation        STRING(1)
localSMSType         STRING(1)
LocalKeyWord         STRING(20)
LocalKeyWordLength   LONG
LocalTextLength      LONG
LocalReplaceText     STRING(50)
LocalPosition        LONG
LocalSendingString   STRING(200)
Save_Job_ID          USHORT
Save_Wob_ID          USHORT
StartPlace           LONG
EndPlace             LONG
Count                LONG
ReturnJobType        STRING(10)
ReturnLocation       STRING(20)
SentSpecial          STRING(1)
SystemDefaultSMS     STRING(500)
  CODE
   Relate:JOBS.Open
   Relate:WEBJOB.Open
   Relate:TRADEACC.Open
   Relate:TRADEAC2.Open
   Relate:JOBSE.Open
   Relate:SMSText.Open
   Relate:SMSMAIL.Open
!(SentJob, SentRefNumber,JobOpen(Y/N))
!SentJob  J = Job:CurrentStatus, E = ExachangeStatus  L = LoanStatus  could be '2' for second loan?
!SentRefNumber is the job:Ref_number wanted
!JobOpen = Y if the job record is already open,


    SentSpecial = 'N'   !"normal" - may be changed later

    !now back to sorting the text for this job
    Glo:ErrorText = 'ERROR|Undefined, untrapped, error occurred'

    if JobOpen = 'N' then
        Access:jobs.clearkey(job:Ref_Number_Key)
        job:Ref_Number = SentRefNumber
        if access:jobs.fetch(job:Ref_Number_Key)
            !error
            Glo:ErrorText = 'ERROR|Unable to trace job number '&clip(SentRefNumber)
        ELSE
            Do PreparationRoutine   !used so I can use the exit command

        END !if jobs.fetch worked
    Else
        !jobs was open on entry
        Do PreparationRoutine
    END !if jobs was already open



PreparationRoutine      Routine

    !fetch and interpret default text - done here when we know jobs is open - jobs needed for replacments
    Access:SMSText.clearkey(SMT:Key_Description)
    SMT:Description = 'SYSTEM DEFAULT'
    if access:SMSText.fetch(SMT:Key_Description)
        !error
        SystemDefaultSMS = 'Kindly contact the Vodacom Repairs call centre, on 0821944, for more information on your device repair'
    ELSE
        glo:ErrorText = SMT:SMSText
        do CheckForKeywords
        SystemDefaultSMS = glo:ErrorText
    END

    Case(SentJob)
        of 'J'
            LocalStatus = Job:Current_Status
            ReturnJobType = 'Job'
        of 'E'
            LocalStatus = job:Exchange_Status
            ReturnJobType = 'Exchange'
        of 'L'
            LocalStatus = job:Loan_Status
            ReturnJobType = 'Loan'
        ELSE
            Glo:ErrorText = 'ERROR|Unable to identify job status from '&clip(SentJob)
            EXIT
    END !Case sent job

    if clip(LocalStatus) = '' or Clip(localStatus) = '101 NOT ISSUED' then       !this will happen everytime they ask for an exchange status with no exhange handset no exchange status
        Glo:ErrorText = 'ERROR|This handset has no traceable status or Exchange/loan has not been issued'   !&clip(LocalStatus)&' '&clip(SentJob) &' '& clip(job:Ref_Number)
        EXIT
    END

    !general setup and checking
    localSMSType = SentSpecial  !normal='N' - may be changed immeditately below

    !save the status code in global variable
    GlobalStatusCode = LocalStatus[1:3]         !new field TB12844

!TB13018 - 901, 2, 5, 10  and 16 now have individual messages to send, not the CSI from here
!!    !special cases of BER or Liquid Damage - job may be despatched but these still go through
!!    if sentSpecial = 'B' or SentSpecial = 'L' then
!!        !let it go through
!!    ELSE
!        !check for status compatability
!        !message('have '&LocalStatus[1:3])
!        if LocalStatus[1:3] = '901' or LocalStatus[1:3] = '902' or LocalStatus[1:3] = '905' or LocalStatus[1:3] = '910' then
!            !or job:Location = DespatchLocation then
!            !this job has been despatched
!            do CSITracking         !sets SentSpecial to 'C' or X if already sent
!            if SentSpecial = 'X' then
!                glo:ErrorText = 'ERROR|This handset has already received the CSI SMS, this cannot be repeated'
!                EXIT
!            END
!        END !if one of the dispatched statuses
!
!
!        !note the double if in the middle to let the code look easy
!        if SentSpecial = 'C' then
!            !it must be one of these
!            if LocalStatus[1:3] = '901' or LocalStatus[1:3] = '902' or LocalStatus[1:3] = '905' or LocalStatus[1:3] = '910' then
!                !or job:Location = DespatchLocation then
!                !this is OK
!                localSMSType = 'C'
!            ELSE
!                glo:ErrorText = 'ERROR|Attempt to send a CSI text to a handset not in a Despatched status'
!                EXIT
!            END !
!        ELSE
!            !It must not be one of these
!            if LocalStatus[1:3] = '901' or LocalStatus[1:3] = '902' or LocalStatus[1:3] = '905' or LocalStatus[1:3] = '910' then
!                !or job:Location = DespatchLocation then
!                !this job is despatched
!                Glo:ErrorText = 'ERROR|Attempt to send a non CSI text to a handset in a Despatched status'
!                EXIT
!            ELSE
!                !this is ok - LocalSMStype stays as it was
!            END !
!        END
!!TB13018

    if LocalStatus[1:3] = '520' then
        !this is an estimate
        localSMSType = 'E'
    END

    !set up glo:webjob to show where it is ... using job.location and transit_type
    if instring('FRANCHISE',job:location,1,1) then
        glo:Webjob = 1
    ELSE
        if instring('ARC',Job:Location,1,1) then
            glo:WEbjob = 0
        ELSE
            if instring('PUP',Job:location,1,1) then
                glo:webjob = 2
            ELSE
                !something like despatched
                if instring('FRANCHISE', job:Transit_type,1,1) then
                    glo:Webjob = 1
                ELSE
                    If instring('ARC',job:transit_type,1,1) then
                        glo:Webjob = 0
                    ELSE
                        if instring('PUP',job:transit_Type,1,1) then
                            glo:Webjob = 2
                        ELSE
                            !I don't know
                            glo:webjob = 99
                       END !if pup
                    END !if arc
                END !if at franchise
            END !if location PUP
        END ! at ARC
    END !if at franchise

    CASE glo:WebJob
        of 0
            LocalLocation = 'A'
            ReturnLocation = 'ARC'
        of 1
            LocalLocation = 'F'
            ReturnLocation = 'Franchise'
        of 2
            LocalLocation = 'V'
            ReturnLocation = 'PUP'
        ELSE
            Glo:ErrorText = 'ERROR|Unable to identify job location from Location, transit type: ' &clip(Job:Location)&' and '&clip(job:transit_type)
            EXIT
    END !Selecting




!sentSpecial used to define if this is :
!'C' =the "CSI - Customer Service Index" email - only set to if sts:Status[1:3] = '901' or sts:Status[1:3] = '902' or sts:Status[1:3] = '905'
!13018 - CSI not sent from this service
!'R' = resend estimate - this may change to final if all resends are used up - (Status must be  "520 Estimate sent")
!'D' = the duplicate estimate reply sms

    

    Case SentSpecial
        !13018 - CSI not sent from this service - JC 26/03/13
!        of 'C'   !Customer service index
!
!            Access:SMSText.clearkey(SMT:Key_Location_CSI)
!            SMT:Location = LocalLocation
!            SMT:CSI = 'YES'
!            if access:SMSText.fetch(SMT:Key_Location_CSI)
!                !error
!                !Glo:ErrorText = 'ERROR|CSI system has not been set up for this location'
!                !change this to a sucess and a standatd text
!                glo:ErrorText = 'SUCCESS|Kindly contact the Vodacom Repairs call centre, on 0821944, for more information on your device repair'
!                EXIT
!            END
!            glo:ErrorText = clip(SMT:SMSText)
!            

        of 'F' !final estimate sms
            Access:SMSText.clearkey(SMT:Key_StatusType_Location_Trigger)
            SMT:Status_Type = SentJob
            SMT:Location = LocalLocation
            SMT:Trigger_Status = '520 ESTIMATE SENT'
            if access:SMSText.fetch(SMT:Key_StatusType_Location_Trigger)
                !Glo:ErrorText = 'ERROR|Resent estimate system has not been set up for this location'
                !change this to a sucess and a standatd text
                glo:ErrorText = 'SUCCESS|'&clip(SystemDefaultSMS)
                EXIT
            END
            !This should only be called when we have reached the final one - before this should have been called on standard not special
            glo:ErrorText = clip(SMT:Final_Estimate_Text)
            

        of 'D' !duplicate estiimate replies received
            Access:SMSText.clearkey(SMT:Key_Location_Duplicate)
            SMT:Location = LocalLocation
            SMT:Duplicate_Estimate = 'YES'
            if access:SMSText.fetch(SMT:Key_Location_Duplicate)
                !Glo:ErrorText = 'ERROR|Duplicate estimate reply system has not been set up for this location'
                !change this to a sucess and a standatd text
                glo:ErrorText = 'SUCCESS|'&clip(SystemDefaultSMS)
                EXIT
            END
            glo:ErrorText = clip(SMT:SMSText)
            

        of 'B' !beyond economic repair
            Access:SMSText.clearkey(SMT:Key_Location_BER)
            SMT:Location = LocalLocation
            SMT:BER = 'YES'
            if access:SMSText.fetch(SMT:Key_Location_BER)
                !Glo:ErrorText = 'ERROR|BER reply system has not been set up for this location'
                !change this to a sucess and a standard text
                glo:ErrorText = 'SUCCESS|'&clip(SystemDefaultSMS)
                EXIT
            END
            glo:ErrorText = clip(SMT:SMSText)
            

        of 'L' !liquid damage
            Access:SMSText.clearkey(SMT:Key_Location_Liquid)
            SMT:Location = LocalLocation
            SMT:LiquidDamage = 'YES'
            if access:SMSText.fetch(SMT:Key_Location_Liquid)
                !Glo:ErrorText = 'ERROR|Liquid Damage SMS has not been set up for this location'
                !change this to a sucess and a standard text
                glo:ErrorText = 'SUCCESS|'&clip(SystemDefaultSMS)
                EXIT
            END
            glo:ErrorText = clip(SMT:SMSText)


        ELSE

            !normal status system call
            Access:SMSText.clearkey(SMT:Key_StatusType_Location_Trigger)
            SMT:Status_Type    = SentJob
            SMT:Location       = LocalLocation
            SMT:Trigger_Status = LocalStatus
            if access:SMSText.fetch(SMT:Key_StatusType_Location_Trigger)
                !error
                !Glo:ErrorText = 'ERROR|No SMS text is set for the combination: '&clip(SentJob)&', '&clip(LocalLocation)&', '&clip(LocalStatus)
                !change this to a sucess and a standard text
                glo:ErrorText = 'SUCCESS|'&clip(SystemDefaultSMS)
                EXIT
            END

            glo:ErrorText = clip(SMT:SMSText)

    END !case SentSpecial

    !special case of the  Job:CurrentStatus = '520 ESTIMATE SENT'
    if sentJob = 'J' and LocalSMSType = 'E' 
        do EstimateTracking
        if LocalSMSType = 'X' then exit.
    END !if estimate sent

    Do CheckForKeywords        !replaces keywords in Glo:errorText

    !if we get here it has worked
    Glo:ErrorText = 'SUCCESS|'&clip(Glo:ErrorText)

    exit


CheckForKeywords        Routine

!Ready to send now or interpreting the system default
    !interpret the keyword bits
    !There may be more to come so I want to make this easy to expand - just repeat the code in this bit
    !#JobNo
    !#SiteName
    !#Cost
    !#SitePhone
    !#Date+30
    !Each bit identifies the key word to replace, and its length (LocalKeyWord, LocalKeyWordLength)
    !and the word(s) that will replace it (LocalReplaceText)

!will need the trade account open for this bit

    access:webjob.clearkey(wob:refnumberkey)
    wob:refnumber = job:ref_number
    if access:webjob.fetch(wob:refnumberkey)
        !error
    ELSE
        access:tradeacc.clearkey(tra:Account_Number_Key)
        tra:Account_Number = wob:HeadAccountNumber
        if access:tradeacc.fetch(tra:Account_Number_Key)
            !error
        END
        Access:TradeAc2.clearkey(tra:Account_Number_Key)
        tra2:Account_Number = wob:HeadAccountNumber
        If access:TradeAc2.fetch(tra:Account_Number_Key)
            !error
            tra2:coSMSname = ''
        END
    END


!Job number ====================================
    LocalKeyWord       = '#JobNo'
    if instring(clip(LocalKeyword),clip(Glo:ErrorText),1,1) then
        LocalKeyWordLength = 6
        LocalReplaceText = job:ref_Number
        Do ReplaceKeyWord
    END !if #jobno in text

!Job number ====================================

!Site name =====================================
    LocalKeyWord       = '#SiteName'
    if instring(clip(LocalKeyword),clip(Glo:ErrorText),1,1) then
        LocalKeyWordLength = 9
        If clip(tra2:coSMSname) = '' then
            LocalReplaceText = clip(tra:Company_Name)
        ELSE
            LocalReplaceText = clip(tra2:coSMSname)
        END !if Company SMS name existed
        Do ReplaceKeyWord
    END !if instring(clip(LocalKeyword),clip(Glo:ErrorText),1,1) then
!Site name =====================================

!Site phone =====================================
    LocalKeyWord       = '#SitePhone'
    if instring(clip(LocalKeyword),clip(Glo:ErrorText),1,1) then
        LocalKeyWordLength = 10
        LocalReplaceText = clip(tra:Telephone_Number)
        Do ReplaceKeyWord
    END !if instring(clip(LocalKeyword),clip(Glo:ErrorText),1,1) then
!Site phone =====================================

!Date =====================================
    LocalKeyWord       = '#Date+30'
    if instring(clip(LocalKeyword),clip(Glo:ErrorText),1,1) then
        LocalKeyWordLength = 8
        LocalReplaceText = format(today()+30,@d06)
        Do ReplaceKeyWord
    END !if instring(clip(LocalKeyword),clip(Glo:ErrorText),1,1) then
!Date =====================================


!Estimate cost==================================
    LocalKeyWord       = '#Cost'
    if instring(clip(LocalKeyword),clip(Glo:ErrorText),1,1) then

        LocalKeyWordLength = 5

        if LocalLocation = 'A'
            !ARC
            LocalReplaceText = left(format(job:Sub_Total_Estimate + |
                               (job:Courier_Cost_Estimate * (LocalVatRate(job:Account_Number,'L') /100)) + |
                               (job:Parts_Cost_Estimate * (LocalVatRate(job:Account_Number,'P') /100)) +  |
                               (job:Labour_Cost_Estimate * (LocalVatRate(job:Account_Number,'L')/100)),@n9.2))

            
        ELSE
            !For RRC
            LocalReplaceText  = left(format(jobe:RRCESubTotal + |
                                (job:Courier_Cost_Estimate * (LocalVatRate(job:Account_Number,'L') /100)) + |
                                (jobe:RRCEPartsCost * (LocalVatRate(job:Account_Number,'P') /100)) +  |
                                (jobe:RRCELabourCost * (LocalVatRate(job:Account_Number,'L')/100)),@n9.2))

        END !if at ARC

        
        Do ReplaceKeyWord
    END !if instring(clip(LocalKeyword),clip(Glo:ErrorText),1,1) then

!Estimate cost==================================


    EXIT




ReplaceKeyWord      Routine

    Loop

        LocalPosition = instring(clip(LocalKeyWord),clip(Glo:ErrorText),1,1)
        if LocalPosition = 0 then break. !not found in the text

        LocalTextLength = len(clip(glo:ErrorText))
        glo:ErrorText = glo:ErrorText[ 1 : LocalPosition - 1 ] & clip(LocalReplaceText) & glo:ErrorText[ LocalPosition + LocalKeyWordLength : LocalTextLength]

    END !loop to find the LocalKeyWord in the Glo:ErrorText

    Exit


CSITracking         Routine

    count = 0
    Access:SMSMail.clearkey(sms:DateTimeInsertedKey)
    sms:RefNumber = Job:Ref_number
    sms:DateInserted = 0
    sms:TimeInserted = 0
    Set(sms:DateTimeInsertedKey,sms:DateTimeInsertedKey)
    loop
        if access:SMSMail.next() then break.
        if sms:RefNumber <> Job:Ref_number then break.
        if sms:SMSType = 'C' then
            count =1
            break
        END
    END !loop

    if Count = 1 then
        SentSpecial  = 'X'
    ELSE
        SentSpecial = 'C'
    END



EstimateTracking    Routine

    localSMSType = 'E'

    !if the customer has already received the maximum number or emails then send the final
    count = 0
    Access:SMSMail.clearkey(sms:DateTimeInsertedKey)
    sms:RefNumber = Job:Ref_number
    sms:DateInserted = 0
    sms:TimeInserted = 0
    Set(sms:DateTimeInsertedKey,sms:DateTimeInsertedKey)
    loop
        if access:SMSMail.next() then break.
        if sms:RefNumber <> Job:Ref_number then break.
        if sms:SMSType = 'E' then   count += 1.

    END !loop

    if count = SMT:Resend_Estimate_Days +1 then
        !it is time to send the last one
        glo:ErrorText = clip(SMT:Final_Estimate_Text)
    END
    if Count > SMT:Resend_Estimate_Days+1 then
        !don't print any
        Glo:ErrorText = 'ERROR|This handset has already received the final estimiate text'
        LocalSMSType = 'X'
    END

    !otherwise leave it as the selected text
    EXIT
   Relate:JOBS.Close
   Relate:WEBJOB.Close
   Relate:TRADEACC.Close
   Relate:TRADEAC2.Close
   Relate:JOBSE.Close
   Relate:SMSText.Close
   Relate:SMSMAIL.Close
LocalVatRate         PROCEDURE  (STRING fAccountNumber,STRING fType) ! Declare Procedure
ReturnValue          REAL
  CODE
   Relate:VATCODE.Open
   Relate:SUBTRACC.Open
   Relate:TRADEACC.Open
        ReturnValue = 0

        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = fAccountNumber
        If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Found
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = sub:Main_Account_Number
            If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Found
                If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
                    Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                    Case fType
                    Of 'L'
                        vat:Vat_Code    = sub:Labour_Vat_Code
                    Of 'P'
                        vat:Vat_Code    = sub:Parts_Vat_Code
                    End !Case func:Type
                    If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                        ReturnValue = vat:Vat_Rate
                    Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                    End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign

                Else ! If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
                    Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                    Case fType
                    Of 'L'
                        vat:Vat_Code    = tra:Labour_Vat_Code
                    Of 'P'
                        vat:Vat_Code    = tra:Parts_Vat_Code
                    End !Case func:Type
                    If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                        ReturnValue = vat:Vat_Rate
                    Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                    End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign

                End ! If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
            Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Error
            End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Error
        End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign


        Return ReturnValue
   Relate:VATCODE.Close
   Relate:SUBTRACC.Close
   Relate:TRADEACC.Close
GenerateJobDetails   PROCEDURE  (SentJobNo)           ! Declare Procedure
ExchangeString       STRING(40)
EstimateString       STRING(40)
TotalCosts           LONG
SMSString            STRING(1000)
LocalFault           STRING(100)
LocalFaultNo         LONG
LocalFaultCode       STRING(30)
  CODE
   Relate:JOBS.Open
   Relate:EXCHANGE.Open
   Relate:JOBSE.Open
   Relate:JOBNOTES.Open
   Relate:WEBJOB.Open
   Relate:TRADEACC.Open
   Relate:MANFAULO.Open
   Relate:MANFAULT.Open
   Relate:JOBOUTFL.Open
!(SentJobNo)

!Various parts to build and return
!to maintain compatability glo:ErrorText will be used as the return string

    glo:ErrorText = ''

    Access:jobs.clearkey(job:Ref_Number_Key)
    job:Ref_Number = SentJobNo
    if access:jobs.fetch(job:Ref_Number_Key)
        glo:ErrorText = 'ERROR|Unable to trace job number '&CLIP(SentJobNo)
    ELSE
        !other files needed
        Access:jobse.clearkey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        if access:jobse.fetch(jobe:RefNumberKey)
            !error
        END

        Access:jobnotes.clearkey(jbn:RefNumberKey)
        jbn:RefNumber = Job:Ref_number
        if access:jobnotes.fetch(jbn:RefNumberKey)
            !error
        END

        Access:Webjob.clearkey(wob:RefNumberKey)
        wob:RefNumber = Job:Ref_number
        if access:Webjob.fetch(wob:RefNumberKey)
            !error
        END

        !try and find the generic fault and copy to LocalFault
        !a three part fetch ...
        LocalFault = ''     !just to be sure
        LocalFaultNo = 0
        LocalFaultCode = ''

        !find the Genericfault field
        Access:MANFAULT.Clearkey(maf:MainFaultKey)
        maf:Manufacturer = job:Manufacturer
        Set(maf:MainFaultKey,maf:MainFaultKey)
        Loop
            If Access:manfault.next() then break.
            if maf:Manufacturer <> job:Manufacturer then break.
            if maf:GenericFault = 1 then
                LocalFaultNo = maf:Field_Number
                break
            END
        END !loop to find generic fault no

        !find the code we have stored against that fault number
        Case LocalFaultNo
            Of 1
                LocalFaultCode = job:Fault_Code1
            of 2
                LocalFaultCode = job:Fault_Code2
            of 3
                LocalFaultCode = job:Fault_Code3
            of 4
                LocalFaultCode = job:Fault_Code4
            of 5
                LocalFaultCode = job:Fault_Code5
            of 6
                LocalFaultCode = job:Fault_Code6
            of 7
                LocalFaultCode = job:Fault_Code7
            of 8
                LocalFaultCode = job:Fault_Code8
            of 9
                LocalFaultCode = job:Fault_Code9
            of 10
                LocalFaultCode = job:Fault_Code10
            of 11
                LocalFaultCode = job:Fault_Code11
            of 12
                LocalFaultCode = job:Fault_Code12
            of 13
                LocalFaultCode = wob:FaultCode13
            of 14
                LocalFaultCode = wob:FaultCode14
            of 15
                LocalFaultCode = wob:FaultCode15
            of 16
                LocalFaultCode = wob:FaultCode16
            of 17
                LocalFaultCode = wob:FaultCode17
            of 18
                LocalFaultCode = wob:FaultCode18
            of 19
                LocalFaultCode = wob:FaultCode19
            of 20
                LocalFaultCode = wob:FaultCode20
        END !case LocalFaultNO


        If LocalFaultNo > 0
            !now look up the desctiption for this combination
            Access:ManFaulo.clearkey(mfo:Field_Key)
            mfo:Manufacturer   = job:Manufacturer
            mfo:Field_Number   = LocalFaultNo
            mfo:Field          = LocalFaultCode
            If Access:Manfaulo.fetch(mfo:Field_Key)
                !error - leave this blank
            ELSE
                LocalFault = mfo:Description
            END
        END !if Fault No > 0

        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = wob:HeadAccountNumber
        If Access:TRADEACC.fetch(tra:Account_Number_Key)
            !error
        END

        if job:Exchange_Unit_Number = 0 then
            !set the exhange string to   NO||
            ExchangeString = 'NO|'
            !message('Exchange unit no is zero')
        ELSE
            !set exhcnage string to YES|exchange model type|
            Access:Exchange.clearkey(xch:Ref_Number_Key)
            xch:Ref_Number = job:Exchange_Unit_Number
            if access:Exchange.fetch(xch:Ref_Number_Key)
                !error
            END
            ExchangeString = 'YES|'& clip(xch:Model_Number)

        END

        !estimates
        if job:Estimate_Ready = 'YES' 
            !find the location
            if instring('FRANCHISE',job:location,1,1) then
                glo:Webjob = 1
            ELSE
                if instring('ARC',Job:Location,1,1) then
                    glo:WEbjob = 0
                ELSE
                    !something like despatched
                    if instring('FRANCHISE', job:Transit_type,1,1) then
                        glo:Webjob = 1
                    ELSE
                        If instring('ARC',job:transit_type,1,1) then
                            glo:Webjob = 0
                        ELSE
                            !I don't know
                            glo:webjob = 99
                        END
                    END !if at franchise
                END ! at ARC
            END !if at franchise

            if glo:Webjob =1 then
                !at franchise
                TotalCosts = jobe:RRCESubTotal + |
                             (job:Courier_Cost_Estimate * (LocalVatRate(job:Account_Number,'L') /100)) + |
                             (jobe:RRCEPartsCost * (LocalVatRate(job:Account_Number,'P') /100)) +  |
                             (jobe:RRCELabourCost * (LocalVatRate(job:Account_Number,'L')/100))

            ELSE
                !at ARC
                TotalCosts = job:Sub_Total_Estimate + |
                             (job:Courier_Cost_Estimate * (LocalVatRate(job:Account_Number,'L') /100)) + |
                             (job:Parts_Cost_Estimate * (LocalVatRate(job:Account_Number,'P') /100)) +  |
                             (job:Labour_Cost_Estimate * (LocalVatRate(job:Account_Number,'L')/100))
            END
            EstimateString = 'YES|'&left(format(TotalCosts,@n9.2))
        ELSE
            EstimateString = 'NO|0'
        END

        !try for exchange status first - note this uses the glo:ErrorText to return Status
        GenerateSMSText('E',job:Ref_Number,'Y')
        if Glo:ErrorText[1:5] = 'ERROR' then
            !try for job
            GenerateSMSText('J',job:Ref_Number,'Y')
        END

        if glo:ErrorText[1:5] = 'ERROR'
            !return the error that was generated
        ELSE
            !copy relevant part to the tempory holder
            SMSString = glo:ErrorText[ 9 : len(clip(glo:ErrorText)) ]
            !now generate the return string
            glo:ErrorText = 'SUCCESS|'&|
                            clip(left(format(SentJobNo,@n_12))) &'|'&|
                            clip(job:Company_Name)              &'|'&|
                            clip(job:Manufacturer)              &'|'&|
                            clip(job:Model_Number)              &'|'&|
                            clip(ExchangeString)                &'|'&|
                            left(clip(job:Charge_Type) &' '&clip(job:Warranty_Charge_type))   &'|'&|
                            clip(tra:Company_Name)              &'|'&|
                            clip(tra:Telephone_Number)          &'|'&|
                            clip(LocalFault)                    &'|'&|
                            clip(SMSString)                     &'|'&|
                            clip(job:location)                  &'|'&|
                            clip(EstimateString)                &'|'&|
                            GlobalStatusCode                              !new field added TB12844
        END !if error

    END

   Relate:JOBS.Close
   Relate:EXCHANGE.Close
   Relate:JOBSE.Close
   Relate:JOBNOTES.Close
   Relate:WEBJOB.Close
   Relate:TRADEACC.Close
   Relate:MANFAULO.Close
   Relate:MANFAULT.Close
   Relate:JOBOUTFL.Close
JobCheck             PROCEDURE  (SentIMEI)            ! Declare Procedure
LatestDate           DATE
LatestRefNumber      LONG
JobsCount            LONG
CompleteString       STRING(20)
  CODE
   Relate:JOBS.Open
!(SentIMEI)

    glo:ErrorText = 'ERROR|0|No jobs found for this ESN'
    LatestDate      = ''
    LatestRefNumber = 0
    JobsCount       = 0


    Access:jobs.clearkey(job:ESN_Key)
    job:ESN = SentIMEI
    set(job:ESN_Key,job:ESN_Key)
    Loop
        if access:jobs.next() then break.
        if job:ESN <> SentIMEI then break.

        if job:date_booked > LatestDate then
            LatestDate      = job:date_booked
            LatestRefNumber = job:Ref_Number
            JobsCount += 1
        END

    END !loop through jobs file

    if JobsCount > 0 then

        Access:jobs.clearkey(job:Ref_Number_Key)
        job:Ref_Number = LatestRefNumber
        if Access:Jobs.fetch(job:Ref_Number_Key)
            !error?
        ELSE
            if job:Completed = 'YES' then
                CompleteString = 'YES|'&format(job:Date_Completed,@d06)
            ELSE
                CompleteString = 'NO|NA'
            END

            if clip(job:Title & job:Initial & job:Surname) = '' then
                !show the company name in the surname field
                job:surname = job:Company_Name  !this will not be saved
            END
            

            glo:ErrorText = 'SUCCESS|'&|
                            clip(Left(Format(JobsCount,@n_12))) &'|'&|
                            clip(left(format(job:Ref_Number,@n_12)))    &'|'&|
                            clip(job:Title)                     &'|'&|
                            clip(job:Initial)                   &'|'&|
                            clip(job:Surname)                   &'|'&|
                            clip(job:Address_Line1)             &'|'&|
                            clip(job:Address_Line2)             &'|'&|
                            clip(job:Address_Line3)             &'|'&|
                            clip(job:Telephone_Number)          &'|'&|
                            clip(job:Mobile_Number)             &'|'&|
                            clip(job:ESN)                       &'|'&|
                            clip(job:Manufacturer)              &'|'&|
                            clip(job:Model_Number)              &'|'&|
                            format(job:DOP,@d06)                &'|'&|
                            format(job:date_booked,@d06)        &'|'&|
                            format(job:time_booked,@T04)        &'|'&|
                            clip(job:Current_Status)            &'|'&|
                            clip(CompleteString)

        END
    END
   Relate:JOBS.Close
PreJobCheck          PROCEDURE  (SentIMEI)            ! Declare Procedure
LatestDate           DATE
LatestRefNumber      LONG
JobsCount            LONG
  CODE
   Relate:PREJOB.Open
!(SentIMEI)

    glo:ErrorText = 'ERROR|0|No bookings found for this ESN'
    LatestDate      = ''
    LatestRefNumber = 0
    JobsCount       = 0

    Access:prejob.clearkey(PRE:KeyESNDateTime)
    PRE:ESN         = SentIMEI
    PRE:Date_booked = 0
    PRE:Time_Booked = 0
    set(PRE:KeyESNDateTime,PRE:KeyESNDateTime)
    Loop
        if access:prejob.next() then break.
        if PRE:ESN <> SentIMEI then break.

        if PRE:JobRef_number > 0 then cycle.   !this one has been converted to a JOB

        if PRE:Date_booked > LatestDate then
            LatestDate      = PRE:Date_booked
            LatestRefNumber = PRE:RefNumber
            JobsCount += 1
        END

    END !loop through jobs file

    if JobsCount > 0 then

        Access:Prejob.clearkey(PRE:KeyRefNumber)
        PRE:RefNumber = LatestRefNumber
        if Access:PreJob.fetch(PRE:KeyRefNumber)
            !error?
        ELSE
            glo:ErrorText = 'SUCCESS|'&|
                            clip(Left(Format(JobsCount,@n_12))) &'|'&|
                            clip(PRE:ESN)                       &'|'&|
                            clip(PRE:Manufacturer)              &'|'&|
                            clip(PRE:Model_number)              &'|'&|
                            format(PRE:Date_booked,@d06)        &'|'&|
                            format(PRE:Time_Booked,@t04)
        END
    END
   Relate:PREJOB.Close
GetRegion            PROCEDURE  (SentSuburb)          ! Declare Procedure
ReturnRegion         STRING(30)
  CODE
   Relate:SUBURB.Open
!(SentSuburb)

    Access:Suburb.clearkey(sur:SuburbKey)
    sur:Suburb = SentSuburb
    if access:Suburb.fetch(sur:SuburbKey) then
        !error
        ReturnRegion = 'ERROR'
    ELSE
        ReturnRegion = sur:Region
    END
    Return(ReturnRegion)
   Relate:SUBURB.Close
CheckMake            PROCEDURE  (SentIMEI)            ! Declare Procedure
X                    LONG
CheckString          STRING(8)
  CODE
   Relate:ESNMODEL.Open
   Relate:MODELNUM.Open
   Relate:MANUFACT.Open
!(SentIMEI)
    !DEFAULT ERROR
    glo:ErrorText = 'ERROR|0|Cannot find active IMEI in the files'

    Free(glo:Q_ModelNumber)
    Clear(glo:Q_ModelNumber)

    !build up the queue
    CheckString = Sub(SentIMEI,1,6)
    Do CheckLoop

    CheckString = Sub(SentIMEI,1,8)
    Do CheckLoop

    !now interpret what we've got
    Case Records(glo:Q_ModelNumber)
        Of 0
            ! No TAC Codes have been found. So cannot return a Model Number
            !error has already been SETUP
        Of 1
            ! Only 1 TAC Code was found. Return that Model Number
            Get(glo:Q_ModelNumber,1)
            Access:ModelNum.clearkey(mod:Model_Number_Key)
            mod:Model_Number = GLO:Model_Number_Pointer
            Access:ModelNum.fetch(mod:Model_Number_Key)

            glo:ErrorText = 'SUCCESS|1|'&clip(mod:Manufacturer)&'|'&clip(mod:Model_Number)
        Else
            !return a list of models
            glo:ErrorText = 'SUCCESS|'& clip(left(format(records(Glo:Q_ModelNumber),@n_12)))

            Loop x = 1 to records(glo:Q_ModelNumber)

                Get(glo:Q_ModelNumber,X)

                Access:ModelNum.clearkey(mod:Model_Number_Key)
                mod:Model_Number = GLO:Model_Number_Pointer
                if Access:ModelNum.fetch(mod:Model_Number_Key)
                    !Error??
                END
                Glo:ErrorText = clip(Glo:ErrorText) &'|'& clip(mod:Manufacturer)&'|'&clip(mod:Model_Number)

            END !loop to add the manufacturers and models
    End ! Case Records(ModelQUeue)


CheckLoop   Routine

    !check for six or eight digit matching
    Access:ESNMODEL.Clearkey(esn:ESN_Only_Key)
    esn:ESN    =  clip(CheckString)    
    Set(esn:ESN_Only_Key,esn:ESN_Only_Key)
    Loop ! Begin Loop
        If Access:ESNMODEL.Next() then break.
        If esn:ESN <> clip(CheckString) then break.

        !added by Paul 08/06/2010 - Log no 11466
        If esn:Active <> 'Y' then
            glo:ErrorText = 'ERROR|0|Cannot find active IMEI in the files'
            cycle
        End

        !check to see if the model number selected is active
        Access:ModelNum.clearkey(mod:Model_Number_Key)
        mod:Model_Number = esn:Model_Number
        If Access:ModelNum.fetch(mod:Model_Number_Key) = level:benign then
            If mod:Active <> 'Y' then
                glo:ErrorText = 'ERROR|0|Model foud but '&clip(ESN:MODEL_Number)&' is marked inactive'
                cycle
            End
        ELSE
            glo:ErrorText = 'ERROR|0|ESN found but model '&clip(ESN:MODEL_Number)&' is not found'
            cycle
        End

        !tb12488 Disable Manufacturers - is this manufacturer inactive
        Access:Manufact.clearkey(man:Manufacturer_Key)
        man:Manufacturer = mod:Manufacturer
        if access:manufact.fetch(man:Manufacturer_Key) = level:Benign then
            !if man:Notes[1:8] = 'INACTIVE' then
            !TB13214 - change to using field for inactive  - J 05/02/14
            if man:Inactive = 1 then 
                glo:ErrorText = 'ERROR|0|Model found '&clip(ESN:MODEL_Number)&' but manufacturer '&CLIP(MOD:Manufacturer)&' is marked inactive'
                cycle
            END !if inactive
        ELSE
            glo:ErrorText = 'ERROR|0|Model found '&clip(ESN:MODEL_Number)&' but manufacturer '&CLIP(MOD:Manufacturer)&' is not found'
            cycle
        END !if manufact.fetch
        
        GLO:Model_Number_Pointer = mod:Model_Number
        Add(glo:Q_ModelNumber)
    End ! Loop

    EXIT



   Relate:ESNMODEL.Close
   Relate:MODELNUM.Close
   Relate:MANUFACT.Close
GetAddressesFromRegion PROCEDURE  (SentRegion)        ! Declare Procedure
Count                LONG
  CODE
   Relate:TRADEAC2.Open
   Relate:TRADEACC.Open
!(SentRegion) to match TRA2:Pre_Book_Region
!there are not going to be many of these do I need a key?

    Glo:ErrorText = ''
    Count = 0

    Access:TradeAc2.clearkey(TRA2:KeyPreBookRegion)  !hm this has to be as fast as possible
    TRA2:Pre_Book_Region = SentRegion
    set(TRA2:KeyPreBookRegion,TRA2:KeyPreBookRegion)
    loop
        if access:Tradeac2.next() then break.
        if TRA2:Pre_Book_Region <> SentRegion then break.

        Access:TradeAcc.clearkey(tra:Account_Number_Key)
        tra:Account_Number = TRA2:Account_Number
        if access:TradeAcc.fetch(tra:Account_Number_Key)
            !eh
            cycle
        END

        Count += 1

        Glo:ErrorText = CLIP(Glo:ErrorText)&|
                        '|'&clip(tra:Company_Name)         &|
                        '|'&clip(tra:Address_Line1)        &|
                        '|'&clip(tra:Address_Line2)        &|
                        '|'&clip(tra:Address_Line3)        &|
                        '|'&clip(tra:Telephone_Number)

    END !loop through

    glo:ErrorText = clip(left(clip(format(count,@n_10))))  &clip(Glo:ErrorText)
   Relate:TRADEAC2.Close
   Relate:TRADEACC.Close
WaybillConfirm       PROCEDURE  (SentWaybill,SentAction,SentDateTime,SentSender,SentReceiver,SentContact) ! Declare Procedure
LocalSpace           LONG
LocalDate            DATE
LocalTime            TIME
  CODE
   Relate:WAYBCONF.Open
   Relate:WAYBILLJ.Open
   Relate:AUDIT.Open
   Relate:AUDIT2.Open
   Relate:AUDITE.Open
!(SentWaybill,SentAction,SentDateTime,SentSender,SentReceiver,SentContact)
!TB13370 - J - 15/04/15


    glo:ErrorText = 'ERROR|No waybill found with this reference'


    !deformat the date time stamp
    LocalSpace = instring(SentDateTime,' ',1,1)
    LocalDate  = deformat(SentDateTime[ 1 : LocalSpace - 1],@d06)
    LocalTime  = deformat(SentDateTime[ LocalSpace + 1 : Len(clip(SentDateTime)) ],@T1)



    Access:Waybconf.clearkey(WAC:KeyWaybillNo)
    WAC:WaybillNo = deformat(SentWaybill)
    if access:Waybconf.fetch(WAC:KeyWaybillNo)
        !error
    ELSE

        If SentAction = 'CONFIRMED' then
            WAC:ConfirmationReceived   = true
            WAC:ConfirmDate = today()
            WAC:ConfirmTime = clock()
            Access:WayBConf.update()
            glo:ErrorText = 'SUCCESS'
        END

        !or ...
        If SentAction = 'DELIVERED'
            WAC:Delivered = true
            WAC:DeliverDate = today()
            WAC:DeliverTime = clock()
            Access:WayBConf.update()
            glo:ErrorText = 'SUCCESS'
        END


        !Add to audit trail for all jobs
        Access:Waybillj.clearkey(waj:JobNumberKey)  !has waybillnumber as first component
        waj:WayBillNumber = WAC:WaybillNo           !BUG was way:WayBillNumber
        Set(waj:JobNumberKey,waj:JobNumberKey)
        Loop
            If Access:WAYBILLJ.NEXT() then break.
            If waj:WayBillNumber <> WAC:WaybillNo then break.

            Access:Audit.primerecord()
            aud:Ref_Number = waj:JobNumber
            aud:Date       = today()
            aud:Time       = clock()
            aud:User       = 'API'
            aud:Action     = 'COURIER NOTIFICATION RECEIVED'
            aud:Type       = waj:JobType
            Access:Audit.update()

            Access:Audit2.primerecord()
            aud2:AUDRecordNumber = aud:record_number
            aud2:Notes = 'WAYBILL '&clip(SentAction)&' AT '&clip(SentDateTime)&' BY '&clip(SentContact)&' FROM ' &clip(SentSender)&' TO '&cliP(SentReceiver)
            Access:Audit2.update()

            Access:Audite.primerecord()
            aude:RefNumber = aud:record_number
            aude:HostName  = 'SB API'
            Access:Audite.update()


        END !loop through jobs


    END !if waybill found


   Relate:WAYBCONF.Close
   Relate:WAYBILLJ.Close
   Relate:AUDIT.Close
   Relate:AUDIT2.Close
   Relate:AUDITE.Close
