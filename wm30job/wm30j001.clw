

   MEMBER('wm30job.clw')                              ! This is a MEMBER module

                     MAP
                       INCLUDE('WM30J001.INC'),ONCE        !Local module procedure declarations
                     END


Check_Web_Password   PROCEDURE                        ! Declare Procedure
  CODE
!Check_Password procedure
! message('about to check for password')

 relate:subtracc.open
 relate:tradeacc.open
 relate:users.open

 Global_Ordernumber = 'N'
 password = Upper(password)
 account_number_global = Upper(account_number_global)
 Select_Global1 = 'Y'

 access:tradeacc.clearkey(tra:account_number_key)
 tra:account_number = account_number_global !SUB:Main_Account_Number
 access:tradeacc.fetch(tra:account_number_key)
 If password <> Clip(tra:password)
    Select_global1='Y'
 Else
     Select_Global1 = 'N'
 End!If password <> sub:password

 glo:location = tra:SiteLocation
 glo:Default_Site_Location = tra:SiteLocation

 if Select_Global1 = 'N'
    !we now have to check to see if the users is valid
    access:users.clearkey(use:password_key)
    use:Password = LogonUserPassword
    if access:users.fetch(use:password_Key) then
        !error
        Select_global1 = 'X'
    Else
       LogonUserCode = use:User_Code
       LogonUserName = clip(use:Forename)&' '&clip(use:Surname)
       !Area reserved to kick people out for any reason I like
       if use:Active = 'NO' then select_global1 = 'X'.
       If use:PasswordLastChanged + use:RenewPassword < Today() Or use:PasswordLastChanged > Today() then Select_Global1 = 'X'.
       !check for end of password date
    END !if users.fetch
 END !If Select_global = 'N'

 relate:users.close
 relate:subtracc.close
 relate:tradeacc.close
