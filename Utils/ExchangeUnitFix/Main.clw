  MEMBER('ExchangeUnitFix.clw')
Progress:Thermometer        LONG
Skip                LONG
locSavePath         CSTRING(255)
locImportFile       CSTRING(255),STATIC
ImportFile          File,DRIVER('BASIC'),PRE(impfil),Name(locImportFile),CREATE,BINDABLE,THREAD
RECORD                  RECORD
ExchangeRefNumber           LONG
EntryDate                   LONG
EntryTime                   LONG
UserCode                    STRING(3)
Status                      STRING(30)
Notes                       STRING(30)
Location                    STRING(30)
                        END
                    END
ProgressWindow      WINDOW('Progress...'),AT(,,237,47),CENTER,GRAY,ICON('Cellular3g.ico'), |
                        FONT('Tahoma',8),TIMER(1),DOUBLE
                        PROGRESS,AT(9,15,219,12),USE(Progress:Thermometer),RANGE(0,100)
                        STRING(''),AT(0,3,237,10),USE(?Progress:UserString),CENTER,FONT(,8)
                        STRING(''),AT(0,30,237,10),USE(?Progress:PctText),CENTER
                    END
Window              WINDOW('Correct Exchange Units'),AT(,,301,72),CENTER,GRAY,FONT('Tahoma',8), |
                        COLOR(0F5F5F5H),RESIZE
                        BUTTON('Begin Process'),AT(3,49,69,17),USE(?OkButton),DEFAULT
                        BUTTON('&Cancel'),AT(251,49,48,17),USE(?CancelButton)
                        PROMPT('Select CSV File containing the exchange units that you wish to b' & |
                            'e made available.'),AT(3,5),USE(?PROMPT1)
                        ENTRY(@s255),AT(41,23,241,14),USE(locImportFile),COLOR(COLOR:White)
                        PROMPT('CSV File:'),AT(3,25),USE(?PROMPT2)
                        BUTTON('...'),AT(285,22,13),USE(?FileLookup)
                    END
                    MAP
Processing  PROCEDURE()
    END

RecordsFound        LONG

Main                PROCEDURE()
    CODE
        OPEN(Window)
        ACCEPT
            CASE EVENT()
            OF EVENT:Accepted
                CASE FIELD()
                OF ?FileLookup
                    locSavePath = PATH()
                    locImportFile = ''
                    IF (FILEDIALOG('Choose File',locImportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName))
                    END ! IF (FILEDIALOG)
                    SETPATH(locSavePath)
                    DISPLAY()
                OF ?OkButton
                    IF (locImportFile <> '')
                        BEEP(BEEP:SystemQuestion)  ;  YIELD()
                        CASE MESSAGE('Are you sure you want to begin?','ServiceBase',|
                            ICON:Question,'&Yes|&No',2) 
                        OF 1 ! &Yes Button
                            Processing()
                            BREAK
                        OF 2 ! &No Button
                        END!CASE MESSAGE                        
                    END ! IF
                OF ?CancelButton
                    BREAK
                END ! CASE
            END ! CASE
        END ! ACCEPT
        CLOSE(Window)
        
        
Processing          PROCEDURE()
recs                    LONG
upd                     LONG
fail                    LONG
    CODE
        
        Relate:EXCHANGE.Open()
        Relate:EXCHHIST.Open()
        
        OPEN(ImportFile)
        IF (ERRORCODE())
            STOP(ERROR())
            RETURN
        END ! IF
        
        SET(ImportFile,0)
        LOOP
            NEXT(ImportFile)
            IF (ERRORCODE())
                BREAK
            END!  IF
            recs += 1
        END ! LOOP
        
        OPEN(ProgressWindow)
        ?Progress:Thermometer{PROP:RangeHigh} = recs
        ?Progress:Thermometer{PROP:Progress} = 0
        DISPLAY()
        
        SET(ImportFile,0)
        LOOP
            NEXT(ImportFile)
            IF (ERRORCODE())
                BREAK
            END!  IF
            ?Progress:Thermometer{PROP:Progress} = ?Progress:Thermometer{PROP:Progress} + 1
            ?Progress:PctText{PROP:Text} = INT((?Progress:Thermometer{PROP:Progress} / ?Progress:Thermometer{PROP:RangeHigh}) * 100) & ' %'
            DISPLAY()
            
            Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
            xch:Ref_Number = impfil:ExchangeRefNumber
            IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
                CYCLE
            END ! IF

            ! Criteria
            ! Unit must have changed status on the 20th
            ! Must be not available
            ! Must be in the location "RETURN TO MANUFACTURER"
            
            IF (xch:Available <> 'NOA')
                CYCLE
            END ! IF
            !IF (xch:StatusChangeDate <> DEFORMAT('22/07/2015',@d06))
            !J 27/07/15 - above, and in documentation it says the change has to take place
            !on the 20th not the 22nd - so I have updated this
            IF (xch:StatusChangeDate <> DEFORMAT('20/07/2015',@d06))    
                CYCLE
            END ! IF
            IF (xch:Location <> 'RETURN TO MANUFACTURER')
                CYCLE
            END ! IF
            
            xch:Location = CLIP(impfil:Location)
            xch:Available = 'AVL'
            xch:StatusChangeDate = TODAY()
            IF (Access:EXCHANGE.TryUpdate())
                fail += 1
                CYCLE
            END ! IF
            
            IF (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                exh:Ref_Number = xch:Ref_Number
                exh:Date = TODAY()
                exh:Time = CLOCK()
                exh:Status = 'UNIT AVAILABLE'
                exh:Notes = 'AUTOMATED FIX'
                IF (Access:EXCHHIST.TryInsert())
                    Access:EXCHHIST.CancelAutoInc()
                END ! IF
                
            END ! IF
            
            upd += 1
            
        END ! LOOP
        
        CLOSE(ProgressWindow)
        
        CLOSE(ImportFile)
        
        Relate:EXCHANGE.Close()
        Relate:EXCHHIST.Close()
        
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()
        CASE MESSAGE('Process Completed.'&|
            '|'&|
            '|Records updated: ' & CLIP(upd) & ''&|
            '|'&|
            '|Records failed to update: ' & CLIP(fail) & '','ServiceBase',|
            ICON:Asterisk,'&OK',1) 
        OF 1 ! &OK Button
        END!CASE MESSAGE