   PROGRAM



   INCLUDE('ABERROR.INC'),ONCE
   INCLUDE('ABFILE.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ERRORS.CLW'),ONCE
   INCLUDE('KEYCODES.CLW'),ONCE
   INCLUDE('ABFUZZY.INC'),ONCE

   MAP
     MODULE('EXCHANGEUNITFIX_BC.CLW')
DctInit     PROCEDURE                                      ! Initializes the dictionary definition module
DctKill     PROCEDURE                                      ! Kills the dictionary definition module
     END
!--- Application Global and Exported Procedure Definitions --------------------------------------------
     MODULE('MAIN.CLW')
Main                   PROCEDURE   !
     END
   END

glo:ErrorText        STRING(1000)
glo:owner            STRING(20)
glo:TimeModify       TIME
glo:Notes_Global     STRING(1000)
glo:Q_Invoice        QUEUE,PRE(glo)
Invoice_Number         REAL
Account_Number         STRING(15)
                     END
glo:afe_path         STRING(255)
glo:DateModify       DATE
glo:Password         STRING(20)
glo:PassAccount      STRING(30)
glo:Preview          STRING('True')
glo:q_JobNumber      QUEUE,PRE(GLO)
Job_Number_Pointer     REAL
                     END
glo:Q_PartsOrder     QUEUE,PRE(GLO)
Q_Order_Number         REAL
Q_Part_Number          STRING(30)
Q_Description          STRING(30)
Q_Supplier             STRING(30)
Q_Quantity             LONG
Q_Purchase_Cost        REAL
Q_Sale_Cost            REAL
                     END
glo:G_Select1        GROUP,PRE(GLO)
Select1                STRING(30)
Select2                STRING(40)
Select3                STRING(40)
Select4                STRING(40)
Select5                STRING(40)
Select6                STRING(40)
Select7                STRING(40)
Select8                STRING(40)
Select9                STRING(40)
Select10               STRING(40)
Select11               STRING(40)
Select12               STRING(40)
Select13               STRING(40)
Select14               STRING(40)
Select15               STRING(40)
Select16               STRING(40)
Select17               STRING(40)
Select18               STRING(40)
Select19               STRING(40)
Select20               STRING(40)
Select21               STRING(40)
Select22               STRING(40)
Select23               STRING(40)
Select24               STRING(40)
Select25               STRING(40)
Select26               STRING(40)
Select27               STRING(40)
Select28               STRING(40)
Select29               STRING(40)
Select30               STRING(40)
Select31               STRING(40)
Select32               STRING(40)
Select33               STRING(40)
Select34               STRING(40)
Select35               STRING(40)
Select36               STRING(40)
Select37               STRING(40)
Select38               STRING(40)
Select39               STRING(40)
Select40               STRING(40)
Select41               STRING(40)
Select42               STRING(40)
Select43               STRING(40)
Select44               STRING(40)
Select45               STRING(40)
Select46               STRING(40)
Select47               STRING(40)
Select48               STRING(40)
Select49               STRING(40)
Select50               STRING(40)
                     END
glo:q_RepairType     QUEUE,PRE(GLO)
Repair_Type_Pointer    STRING(30)
                     END
glo:Q_UnitType       QUEUE,PRE(GLO)
Unit_Type_Pointer      STRING(30)
                     END
glo:Q_ChargeType     QUEUE,PRE(GLO)
Charge_Type_Pointer    STRING(30)
                     END
glo:Q_ModelNumber    QUEUE,PRE(GLO)
Model_Number_Pointer   STRING(30)
                     END
glo:Q_Accessory      QUEUE,PRE(GLO)
Accessory_Pointer      STRING(30)
                     END
glo:Q_AccessLevel    QUEUE,PRE(GLO)
Access_Level_Pointer   STRING(30)
                     END
glo:Q_AccessAreas    QUEUE,PRE(GLO)
Access_Areas_Pointer   STRING(30)
                     END
glo:Q_Notes          QUEUE,PRE(GLO)
notes_pointer          STRING(80)
                     END
glo:EnableFlag       STRING(3)
glo:TimeLogged       TIME
glo:GLO:ApplicationName STRING(8)
glo:GLO:ApplicationCreationDate LONG
glo:GLO:ApplicationCreationTime LONG
glo:GLO:ApplicationChangeDate LONG
glo:GLO:ApplicationChangeTime LONG
glo:GLO:Compiled32   BYTE
glo:GLO:AppINIFile   STRING(255)
glo:Queue            QUEUE,PRE(GLO)
Pointer                STRING(40)
                     END
glo:Queue2           QUEUE,PRE(GLO)
Pointer2               STRING(40)
                     END
glo:Queue3           QUEUE,PRE(GLO)
Pointer3               STRING(40)
                     END
glo:Queue4           QUEUE,PRE(GLO)
Pointer4               STRING(40)
                     END
glo:Queue5           QUEUE,PRE(GLO)
Pointer5               STRING(40)
                     END
glo:Queue6           QUEUE,PRE(GLO)
Pointer6               STRING(40)
                     END
glo:Queue7           QUEUE,PRE(GLO)
Pointer7               STRING(40)
                     END
glo:Queue8           QUEUE,PRE(GLO)
Pointer8               STRING(40)
                     END
glo:Queue9           QUEUE,PRE(GLO)
Pointer9               STRING(40)
                     END
glo:Queue10          QUEUE,PRE(GLO)
Pointer10              STRING(40)
                     END
glo:Queue11          QUEUE,PRE(GLO)
Pointer11              STRING(40)
                     END
glo:Queue12          QUEUE,PRE(GLO)
Pointer12              STRING(40)
                     END
glo:Queue13          QUEUE,PRE(GLO)
Pointer13              STRING(40)
                     END
glo:Queue14          QUEUE,PRE(GLO)
Pointer14              STRING(40)
                     END
glo:Queue15          QUEUE,PRE(GLO)
Pointer15              STRING(40)
                     END
glo:Queue16          QUEUE,PRE(GLO)
Pointer16              STRING(40)
                     END
glo:Queue17          QUEUE,PRE(GLO)
Pointer17              STRING(40)
                     END
glo:Queue18          QUEUE,PRE(GLO)
Pointer18              STRING(40)
                     END
glo:Queue19          QUEUE,PRE(GLO)
Pointer19              STRING(40)
                     END
glo:Queue20          QUEUE,PRE(GLO)
Pointer20              STRING(40)
                     END
glo:WebJob           BYTE(0)
glo:ARCAccountNumber STRING(30)
glo:ARCSiteLocation  STRING(30)
glo:FaultCodeGroup   GROUP,PRE(glo)
FaultCode1             STRING(30)
FaultCode2             STRING(30)
FaultCode3             STRING(30)
FaultCode4             STRING(30)
FaultCode5             STRING(30)
FaultCode6             STRING(30)
FaultCode7             STRING(30)
FaultCode8             STRING(30)
FaultCode9             STRING(30)
FaultCode10            STRING(255)
FaultCode11            STRING(255)
FaultCode12            STRING(255)
FaultCode13            STRING(30)
FaultCode14            STRING(30)
FaultCode15            STRING(30)
FaultCode16            STRING(30)
FaultCode17            STRING(30)
FaultCode18            STRING(30)
FaultCode19            STRING(30)
FaultCode20            STRING(30)
                     END
glo:IPAddress        STRING(30)
glo:HostName         STRING(60)
glo:ReportName       STRING(255)
glo:ExportReport     BYTE(0)
glo:EstimateReportName STRING(255)
glo:ReportCopies     BYTE
glo:Vetting          BYTE(0)
glo:TagFile          STRING(255)
glo:sbo_outfaultparts STRING(255)
glo:UserAccountNumber STRING(30)
glo:UserCode         STRING(3)
glo:Location         STRING(30)
glo:EDI_Reason       STRING(60)
glo:Insert_Global    STRING(3)
glo:sbo_outparts     STRING(255)
glo:StockReceiveTmp  STRING(255)
glo:SBO_GenericFile  STRING(255)
glo:RelocateStore    BYTE
glo:SBO_GenericTagFile STRING(255)
glo:SBO_DupCheck     STRING(255)
glo:EnableProcedureLogging LONG
SilentRunning        BYTE(0)                               ! Set true when application is running in 'silent mode'

!region File Declaration
EXCHAMF              FILE,DRIVER('Btrieve'),OEM,NAME('EXCHAMF.DAT'),PRE(emf),CREATE,BINDABLE,THREAD !                    
Audit_Number_Key         KEY(emf:Audit_Number),NOCASE,PRIMARY !                    
Secondary_Key            KEY(emf:Complete_Flag,emf:Audit_Number),DUP,NOCASE !                    
Record                   RECORD,PRE()
Audit_Number                LONG                           !                    
Date                        DATE                           !                    
Time                        LONG                           !                    
User                        STRING(3)                      !                    
Status                      STRING(30)                     !                    
Site_location               STRING(30)                     !                    
Complete_Flag               BYTE                           !                    
                         END
                     END                       

EXCHAUI              FILE,DRIVER('Btrieve'),OEM,NAME('EXCHAUI.DAT'),PRE(eau),CREATE,BINDABLE,THREAD !                    
Internal_No_Key          KEY(eau:Internal_No),NOCASE,PRIMARY !                    
Audit_Number_Key         KEY(eau:Audit_Number),DUP,NOCASE  !                    
Ref_Number_Key           KEY(eau:Ref_Number),DUP,NOCASE    !                    
Exch_Browse_Key          KEY(eau:Audit_Number,eau:Exists),DUP,NOCASE !                    
AuditIMEIKey             KEY(eau:Audit_Number,eau:New_IMEI),DUP,NOCASE !By I.M.E.I. Number  
Locate_IMEI_Key          KEY(eau:Audit_Number,eau:Site_Location,eau:IMEI_Number),DUP,NOCASE,OPT !                    
Main_Browse_Key          KEY(eau:Audit_Number,eau:Confirmed,eau:Site_Location,eau:Stock_Type,eau:Shelf_Location,eau:Internal_No),DUP,NOCASE !                    
Record                   RECORD,PRE()
Internal_No                 LONG                           !                    
Site_Location               STRING(30)                     !                    
Shelf_Location              STRING(30)                     !                    
Location                    STRING(30)                     !                    
Audit_Number                LONG                           !                    
Ref_Number                  LONG                           !Unit Number         
Exists                      BYTE                           !                    
New_IMEI                    BYTE                           !                    
IMEI_Number                 STRING(20)                     !                    
Confirmed                   BYTE                           !Confirmed           
Stock_Type                  STRING(30)                     !                    
                         END
                     END                       

LOANAMF              FILE,DRIVER('Btrieve'),OEM,NAME('LOANAMF.DAT'),PRE(lmf),CREATE,BINDABLE,THREAD !                    
Audit_Number_Key         KEY(lmf:Audit_Number),NOCASE,PRIMARY !                    
Secondary_Key            KEY(lmf:Complete_Flag,lmf:Audit_Number),DUP,NOCASE !                    
Record                   RECORD,PRE()
Audit_Number                LONG                           !                    
Date                        DATE                           !                    
Time                        LONG                           !                    
User                        STRING(3)                      !                    
Status                      STRING(30)                     !                    
Site_location               STRING(30)                     !                    
Complete_Flag               BYTE                           !                    
                         END
                     END                       

LOANAUI              FILE,DRIVER('Btrieve'),OEM,NAME('LOANAUI.DAT'),PRE(lau),CREATE,BINDABLE,THREAD !                    
Internal_No_Key          KEY(lau:Internal_No),NOCASE,PRIMARY !                    
Audit_Number_Key         KEY(lau:Audit_Number),DUP,NOCASE  !                    
Ref_Number_Key           KEY(lau:Ref_Number),DUP,NOCASE    !                    
Exch_Browse_Key          KEY(lau:Audit_Number,lau:Exists),DUP,NOCASE !                    
AuditIMEIKey             KEY(lau:Audit_Number,lau:New_IMEI),DUP,NOCASE !By I.M.E.I. Number  
Locate_IMEI_Key          KEY(lau:Audit_Number,lau:Site_Location,lau:IMEI_Number),DUP,NOCASE,OPT !                    
Main_Browse_Key          KEY(lau:Audit_Number,lau:Confirmed,lau:Site_Location,lau:Stock_Type,lau:Shelf_Location,lau:Internal_No),DUP,NOCASE !                    
Record                   RECORD,PRE()
Internal_No                 LONG                           !                    
Site_Location               STRING(30)                     !                    
Shelf_Location              STRING(30)                     !                    
Location                    STRING(30)                     !                    
Audit_Number                LONG                           !                    
Ref_Number                  LONG                           !Unit Number         
Exists                      BYTE                           !                    
New_IMEI                    BYTE                           !                    
IMEI_Number                 STRING(20)                     !                    
Confirmed                   BYTE                           !Confirmed           
Stock_Type                  STRING(30)                     !                    
                         END
                     END                       

EXCAUDIT             FILE,DRIVER('Btrieve'),OEM,NAME('EXCAUDIT.DAT'),PRE(exa),CREATE,BINDABLE,THREAD !Exchange Unit Audit Number File
RecordNumberKey          KEY(exa:Record_Number),NOCASE,PRIMARY !By Record Number    
Audit_Number_Key         KEY(exa:Stock_Type,exa:Audit_Number),NOCASE !By Audit Number     
StockUnitNoKey           KEY(exa:Stock_Unit_Number),DUP,NOCASE !                    
ReplaceUnitNoKey         KEY(exa:Replacement_Unit_Number),DUP,NOCASE !                    
TypeStockNumber          KEY(exa:Stock_Type,exa:Stock_Unit_Number,exa:Audit_Number),DUP,NOCASE !By Audit Number     
Record                   RECORD,PRE()
Record_Number               REAL                           !                    
Stock_Type                  STRING(30)                     !                    
Audit_Number                REAL                           !                    
Stock_Unit_Number           REAL                           !                    
Replacement_Unit_Number     REAL                           !                    
                         END
                     END                       

STOCKTYP             FILE,DRIVER('Btrieve'),NAME('STOCKTYP.DAT'),PRE(stp),CREATE,BINDABLE,THREAD !Stock Type For Loan/Exhange Units
Stock_Type_Key           KEY(stp:Stock_Type),NOCASE,PRIMARY !By Stock_Type       
Use_Loan_Key             KEY(stp:Use_Loan,stp:Stock_Type),DUP,NOCASE !By Stock Type       
Use_Exchange_Key         KEY(stp:Use_Exchange,stp:Stock_Type),DUP,NOCASE !By Stock Type       
Record                   RECORD,PRE()
Use_Loan                    STRING(3)                      !                    
Use_Exchange                STRING(3)                      !                    
Stock_Type                  STRING(30)                     !                    
Available                   BYTE                           !Available           
FranchiseViewOnly           BYTE                           !Franchise View Only Stock Type
                         END
                     END                       

LOAN                 FILE,DRIVER('Btrieve'),NAME('LOAN.DAT'),PRE(loa),CREATE,BINDABLE,THREAD !Loan Units          
Ref_Number_Key           KEY(loa:Ref_Number),NOCASE,PRIMARY !By Loan Unit Number 
ESN_Only_Key             KEY(loa:ESN),DUP,NOCASE           !                    
MSN_Only_Key             KEY(loa:MSN),DUP,NOCASE           !                    
Ref_Number_Stock_Key     KEY(loa:Stock_Type,loa:Ref_Number),DUP,NOCASE !By Ref Number       
Model_Number_Key         KEY(loa:Stock_Type,loa:Model_Number,loa:Ref_Number),DUP,NOCASE !By Model Number     
ESN_Key                  KEY(loa:Stock_Type,loa:ESN),DUP,NOCASE !By E.S.N. / I.M.E.I. Number
MSN_Key                  KEY(loa:Stock_Type,loa:MSN),DUP,NOCASE !By M.S.N. Number    
ESN_Available_Key        KEY(loa:Available,loa:Stock_Type,loa:ESN),DUP,NOCASE !By E.S.N. / I.M.E.I.
MSN_Available_Key        KEY(loa:Available,loa:Stock_Type,loa:MSN),DUP,NOCASE !By M.S.N.           
Ref_Available_Key        KEY(loa:Available,loa:Stock_Type,loa:Ref_Number),DUP,NOCASE !By Loan Unit Number 
Model_Available_Key      KEY(loa:Available,loa:Stock_Type,loa:Model_Number,loa:Ref_Number),DUP,NOCASE !By Model Number     
Stock_Type_Key           KEY(loa:Stock_Type),DUP,NOCASE    !By Stock Type       
ModelRefNoKey            KEY(loa:Model_Number,loa:Ref_Number),DUP,NOCASE !By Unit Number      
AvailIMEIOnlyKey         KEY(loa:Available,loa:ESN),DUP,NOCASE !By I.M.E.I. Number  
AvailMSNOnlyKey          KEY(loa:Available,loa:MSN),DUP,NOCASE !By M.S.N            
AvailRefOnlyKey          KEY(loa:Available,loa:Ref_Number),DUP,NOCASE !By Unit Number      
AvailModOnlyKey          KEY(loa:Available,loa:Model_Number,loa:Ref_Number),DUP,NOCASE !By Unit Number      
DateBookedKey            KEY(loa:Date_Booked),DUP,NOCASE   !By Date Booked      
LocStockAvailIMEIKey     KEY(loa:Location,loa:Stock_Type,loa:Available,loa:ESN),DUP,NOCASE !By I.M.E.I. Number  
LocStockIMEIKey          KEY(loa:Location,loa:Stock_Type,loa:ESN),DUP,NOCASE !By I.M.E.I. Number  
LocIMEIKey               KEY(loa:Location,loa:ESN),DUP,NOCASE !By I.M.E.I. Number  
LocStockAvailRefKey      KEY(loa:Location,loa:Stock_Type,loa:Available,loa:Ref_Number),DUP,NOCASE !By Loan Unit Number 
LocStockRefKey           KEY(loa:Location,loa:Stock_Type,loa:Ref_Number),DUP,NOCASE !By Loan Unit Number 
LocRefKey                KEY(loa:Location,loa:Ref_Number),DUP,NOCASE !By Loan Unit Number 
LocStockAvailModelKey    KEY(loa:Location,loa:Stock_Type,loa:Available,loa:Model_Number,loa:Ref_Number),DUP,NOCASE !By Loan Unit Number 
LocStockModelKey         KEY(loa:Location,loa:Stock_Type,loa:Model_Number,loa:Ref_Number),DUP,NOCASE !By Loan Unit Number 
LocModelKey              KEY(loa:Location,loa:Model_Number,loa:Ref_Number),DUP,NOCASE !By Loan Unit Number 
LocStockAvailMSNKey      KEY(loa:Location,loa:Stock_Type,loa:Available,loa:MSN),DUP,NOCASE !By M.S.N.           
LocStockMSNKey           KEY(loa:Location,loa:Stock_Type,loa:MSN),DUP,NOCASE !By Loan Unit Number 
LocMSNKey                KEY(loa:Location,loa:MSN),DUP,NOCASE !By M.S.N.           
AvailLocIMEI             KEY(loa:Available,loa:Location,loa:ESN),DUP,NOCASE !                    
AvailLocMSN              KEY(loa:Available,loa:Location,loa:MSN),DUP,NOCASE !                    
AvailLocRef              KEY(loa:Available,loa:Location,loa:Ref_Number),DUP,NOCASE !                    
AvailLocModel            KEY(loa:Available,loa:Location,loa:Model_Number),DUP,NOCASE !                    
InTransitLocationKey     KEY(loa:InTransit,loa:Location,loa:ESN),DUP,NOCASE !By IMEI Number      
InTransitKey             KEY(loa:InTransit,loa:ESN),DUP,NOCASE !                    
StatusChangeDateKey      KEY(loa:StatusChangeDate),DUP,NOCASE !By Status Change Date
LocStatusChangeDateKey   KEY(loa:Location,loa:StatusChangeDate),DUP,NOCASE !By Status Change Date
Record                   RECORD,PRE()
Ref_Number                  LONG                           !                    
Model_Number                STRING(30)                     !                    
Manufacturer                STRING(30)                     !                    
ESN                         STRING(30)                     !                    
MSN                         STRING(30)                     !                    
Colour                      STRING(30)                     !                    
Location                    STRING(30)                     !                    
Shelf_Location              STRING(30)                     !                    
Date_Booked                 DATE                           !                    
Times_Issued                REAL                           !                    
Available                   STRING(3)                      !                    
Job_Number                  LONG                           !                    
Stock_Type                  STRING(30)                     !                    
DummyField                  STRING(1)                      !For File Manager    
StatusChangeDate            DATE                           !Status Change Date  
InTransit                   BYTE                           !                    
                         END
                     END                       

LOANHIST             FILE,DRIVER('Btrieve'),NAME('LOANHIST.DAT'),PRE(loh),CREATE,BINDABLE,THREAD !Loan History        
Record_Number_Key        KEY(loh:record_number),NOCASE,PRIMARY !By Record Number    
Ref_Number_Key           KEY(loh:Ref_Number,-loh:Date,-loh:Time),DUP,NOCASE !By Ref Number       
Record                   RECORD,PRE()
Ref_Number                  REAL                           !                    
record_number               REAL                           !                    
Date                        DATE                           !                    
Time                        TIME                           !                    
User                        STRING(3)                      !                    
Status                      STRING(60)                     !                    
Notes                       STRING(255)                    !                    
                         END
                     END                       

EXCHHIST             FILE,DRIVER('Btrieve'),NAME('EXCHHIST.DAT'),PRE(exh),CREATE,BINDABLE,THREAD !Exchange History    
Record_Number_Key        KEY(exh:record_number),NOCASE,PRIMARY !By Record Number    
Ref_Number_Key           KEY(exh:Ref_Number,-exh:Date,-exh:Time),DUP,NOCASE !By Ref Number       
DateStatusKey            KEY(exh:Date,exh:Status,exh:Ref_Number),DUP,NOCASE !By Ref Number       
Record                   RECORD,PRE()
Ref_Number                  LONG                           !                    
record_number               LONG                           !                    
Date                        DATE                           !                    
Time                        TIME                           !                    
User                        STRING(3)                      !                    
Status                      STRING(60)                     !                    
Notes                       STRING(255)                    !                    
                         END
                     END                       

EXCHANGE             FILE,DRIVER('Btrieve'),NAME('EXCHANGE.DAT'),PRE(xch),CREATE,BINDABLE,THREAD !Exchange units      
Ref_Number_Key           KEY(xch:Ref_Number),NOCASE,PRIMARY !By Loan Unit Number 
AvailLocIMEI             KEY(xch:Available,xch:Location,xch:ESN),DUP,NOCASE !                    
AvailLocMSN              KEY(xch:Available,xch:Location,xch:MSN),DUP,NOCASE !                    
AvailLocRef              KEY(xch:Available,xch:Location,xch:Ref_Number),DUP,NOCASE !                    
AvailLocModel            KEY(xch:Available,xch:Location,xch:Model_Number),DUP,NOCASE !                    
ESN_Only_Key             KEY(xch:ESN),DUP,NOCASE           !                    
MSN_Only_Key             KEY(xch:MSN),DUP,NOCASE           !                    
Ref_Number_Stock_Key     KEY(xch:Stock_Type,xch:Ref_Number),DUP,NOCASE !By Ref Number       
Model_Number_Key         KEY(xch:Stock_Type,xch:Model_Number,xch:Ref_Number),DUP,NOCASE !By Model Number     
ESN_Key                  KEY(xch:Stock_Type,xch:ESN),DUP,NOCASE !By E.S.N. / I.M.E.I. Number
MSN_Key                  KEY(xch:Stock_Type,xch:MSN),DUP,NOCASE !By M.S.N. Number    
ESN_Available_Key        KEY(xch:Available,xch:Stock_Type,xch:ESN),DUP,NOCASE !By E.S.N. / I.M.E.I.
MSN_Available_Key        KEY(xch:Available,xch:Stock_Type,xch:MSN),DUP,NOCASE !By M.S.N.           
Ref_Available_Key        KEY(xch:Available,xch:Stock_Type,xch:Ref_Number),DUP,NOCASE !By Exchange Unit Number
Model_Available_Key      KEY(xch:Available,xch:Stock_Type,xch:Model_Number,xch:Ref_Number),DUP,NOCASE !By Model Number     
Stock_Type_Key           KEY(xch:Stock_Type),DUP,NOCASE    !By Stock Type       
ModelRefNoKey            KEY(xch:Model_Number,xch:Ref_Number),DUP,NOCASE !By Unit Number      
AvailIMEIOnlyKey         KEY(xch:Available,xch:ESN),DUP,NOCASE !By I.M.E.I. Number  
AvailMSNOnlyKey          KEY(xch:Available,xch:MSN),DUP,NOCASE !By M.S.N.           
AvailRefOnlyKey          KEY(xch:Available,xch:Ref_Number),DUP,NOCASE !By Unit Number      
AvailModOnlyKey          KEY(xch:Available,xch:Model_Number,xch:Ref_Number),DUP,NOCASE !By Unit Number      
StockBookedKey           KEY(xch:Stock_Type,xch:Model_Number,xch:Date_Booked,xch:Ref_Number),DUP,NOCASE !By Unit Number      
AvailBookedKey           KEY(xch:Available,xch:Stock_Type,xch:Model_Number,xch:Date_Booked,xch:Ref_Number),DUP,NOCASE !By Unit Number      
DateBookedKey            KEY(xch:Date_Booked),DUP,NOCASE   !By Date Booked      
LocStockAvailIMEIKey     KEY(xch:Location,xch:Stock_Type,xch:Available,xch:ESN),DUP,NOCASE !By I.M.E.I. Number  
LocStockIMEIKey          KEY(xch:Location,xch:Stock_Type,xch:ESN),DUP,NOCASE !By I.M.E.I. Number  
LocIMEIKey               KEY(xch:Location,xch:ESN),DUP,NOCASE !By I.M.E.I. Number  
LocStockAvailRefKey      KEY(xch:Location,xch:Stock_Type,xch:Available,xch:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocStockRefKey           KEY(xch:Location,xch:Stock_Type,xch:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocRefKey                KEY(xch:Location,xch:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocStockAvailModelKey    KEY(xch:Location,xch:Stock_Type,xch:Available,xch:Model_Number,xch:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocStockModelKey         KEY(xch:Location,xch:Stock_Type,xch:Model_Number,xch:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocModelKey              KEY(xch:Location,xch:Model_Number,xch:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocStockAvailMSNKey      KEY(xch:Location,xch:Stock_Type,xch:Available,xch:MSN),DUP,NOCASE !By M.S.N.           
LocStockMSNKey           KEY(xch:Location,xch:Stock_Type,xch:MSN),DUP,NOCASE !By Exchange Unit Number
LocMSNKey                KEY(xch:Location,xch:MSN),DUP,NOCASE !By M.S.N.           
InTransitLocationKey     KEY(xch:InTransit,xch:Location,xch:ESN),DUP,NOCASE !By I.M.E.I. Number  
InTransitKey             KEY(xch:InTransit,xch:ESN),DUP,NOCASE !By I.M.E.I. Number  
StatusChangeDateKey      KEY(xch:StatusChangeDate),DUP,NOCASE !                    
LocStatusChangeDateKey   KEY(xch:Location,xch:StatusChangeDate),DUP,NOCASE !                    
Record                   RECORD,PRE()
Ref_Number                  LONG                           !Unit Number         
Model_Number                STRING(30)                     !                    
Manufacturer                STRING(30)                     !                    
ESN                         STRING(30)                     !                    
MSN                         STRING(30)                     !                    
Colour                      STRING(30)                     !                    
Location                    STRING(30)                     !                    
Shelf_Location              STRING(30)                     !                    
Date_Booked                 DATE                           !                    
Times_Issued                REAL                           !                    
Available                   STRING(3)                      !                    
Job_Number                  LONG                           !Job Number          
Stock_Type                  STRING(30)                     !                    
Audit_Number                REAL                           !                    
InTransit                   BYTE                           !In Transit Flag     
FreeStockPurchased          STRING(1)                      !                    
StatusChangeDate            DATE                           !                    
                         END
                     END                       

LOANACC              FILE,DRIVER('Btrieve'),NAME('LOANACC.DAT'),PRE(lac),CREATE,BINDABLE,THREAD !Loan Accesories     
Ref_Number_Key           KEY(lac:Ref_Number,lac:Accessory),NOCASE,PRIMARY !By Accessory        
Record                   RECORD,PRE()
Ref_Number                  REAL                           !                    
Accessory                   STRING(30)                     !                    
Accessory_Status            STRING(6)                      !                    
                         END
                     END                       

EXCHACC              FILE,DRIVER('Btrieve'),NAME('EXCHACC.DAT'),PRE(xca),CREATE,BINDABLE,THREAD !Exchange Accesories 
Ref_Number_Key           KEY(xca:Ref_Number,xca:Accessory),NOCASE,PRIMARY !By Accessory        
Record                   RECORD,PRE()
Ref_Number                  REAL                           !                    
Accessory                   STRING(30)                     !                    
                         END
                     END                       

EXCHANGE_ALIAS       FILE,DRIVER('Btrieve'),NAME('EXCHANGE.DAT'),PRE(xch_ali),CREATE,BINDABLE,THREAD !                    
Ref_Number_Key           KEY(xch_ali:Ref_Number),NOCASE,PRIMARY !By Loan Unit Number 
AvailLocIMEI             KEY(xch_ali:Available,xch_ali:Location,xch_ali:ESN),DUP,NOCASE !                    
AvailLocMSN              KEY(xch_ali:Available,xch_ali:Location,xch_ali:MSN),DUP,NOCASE !                    
AvailLocRef              KEY(xch_ali:Available,xch_ali:Location,xch_ali:Ref_Number),DUP,NOCASE !                    
AvailLocModel            KEY(xch_ali:Available,xch_ali:Location,xch_ali:Model_Number),DUP,NOCASE !                    
ESN_Only_Key             KEY(xch_ali:ESN),DUP,NOCASE       !                    
MSN_Only_Key             KEY(xch_ali:MSN),DUP,NOCASE       !                    
Ref_Number_Stock_Key     KEY(xch_ali:Stock_Type,xch_ali:Ref_Number),DUP,NOCASE !By Ref Number       
Model_Number_Key         KEY(xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE !By Model Number     
ESN_Key                  KEY(xch_ali:Stock_Type,xch_ali:ESN),DUP,NOCASE !By E.S.N. / I.M.E.I. Number
MSN_Key                  KEY(xch_ali:Stock_Type,xch_ali:MSN),DUP,NOCASE !By M.S.N. Number    
ESN_Available_Key        KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:ESN),DUP,NOCASE !By E.S.N. / I.M.E.I.
MSN_Available_Key        KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:MSN),DUP,NOCASE !By M.S.N.           
Ref_Available_Key        KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:Ref_Number),DUP,NOCASE !By Exchange Unit Number
Model_Available_Key      KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE !By Model Number     
Stock_Type_Key           KEY(xch_ali:Stock_Type),DUP,NOCASE !By Stock Type       
ModelRefNoKey            KEY(xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE !By Unit Number      
AvailIMEIOnlyKey         KEY(xch_ali:Available,xch_ali:ESN),DUP,NOCASE !By I.M.E.I. Number  
AvailMSNOnlyKey          KEY(xch_ali:Available,xch_ali:MSN),DUP,NOCASE !By M.S.N.           
AvailRefOnlyKey          KEY(xch_ali:Available,xch_ali:Ref_Number),DUP,NOCASE !By Unit Number      
AvailModOnlyKey          KEY(xch_ali:Available,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE !By Unit Number      
StockBookedKey           KEY(xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Date_Booked,xch_ali:Ref_Number),DUP,NOCASE !By Unit Number      
AvailBookedKey           KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Date_Booked,xch_ali:Ref_Number),DUP,NOCASE !By Unit Number      
DateBookedKey            KEY(xch_ali:Date_Booked),DUP,NOCASE !By Date Booked      
LocStockAvailIMEIKey     KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:Available,xch_ali:ESN),DUP,NOCASE !By I.M.E.I. Number  
LocStockIMEIKey          KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:ESN),DUP,NOCASE !By I.M.E.I. Number  
LocIMEIKey               KEY(xch_ali:Location,xch_ali:ESN),DUP,NOCASE !By I.M.E.I. Number  
LocStockAvailRefKey      KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:Available,xch_ali:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocStockRefKey           KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocRefKey                KEY(xch_ali:Location,xch_ali:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocStockAvailModelKey    KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:Available,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocStockModelKey         KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocModelKey              KEY(xch_ali:Location,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE !By Exchange Unit Number
LocStockAvailMSNKey      KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:Available,xch_ali:MSN),DUP,NOCASE !By M.S.N.           
LocStockMSNKey           KEY(xch_ali:Location,xch_ali:Stock_Type,xch_ali:MSN),DUP,NOCASE !By Exchange Unit Number
LocMSNKey                KEY(xch_ali:Location,xch_ali:MSN),DUP,NOCASE !By M.S.N.           
InTransitLocationKey     KEY(xch_ali:InTransit,xch_ali:Location,xch_ali:ESN),DUP,NOCASE !By I.M.E.I. Number  
InTransitKey             KEY(xch_ali:InTransit,xch_ali:ESN),DUP,NOCASE !By I.M.E.I. Number  
StatusChangeDateKey      KEY(xch_ali:StatusChangeDate),DUP,NOCASE !                    
LocStatusChangeDateKey   KEY(xch_ali:Location,xch_ali:StatusChangeDate),DUP,NOCASE !                    
Record                   RECORD,PRE()
Ref_Number                  LONG                           !Unit Number         
Model_Number                STRING(30)                     !                    
Manufacturer                STRING(30)                     !                    
ESN                         STRING(30)                     !                    
MSN                         STRING(30)                     !                    
Colour                      STRING(30)                     !                    
Location                    STRING(30)                     !                    
Shelf_Location              STRING(30)                     !                    
Date_Booked                 DATE                           !                    
Times_Issued                REAL                           !                    
Available                   STRING(3)                      !                    
Job_Number                  LONG                           !Job Number          
Stock_Type                  STRING(30)                     !                    
Audit_Number                REAL                           !                    
InTransit                   BYTE                           !In Transit Flag     
FreeStockPurchased          STRING(1)                      !                    
StatusChangeDate            DATE                           !                    
                         END
                     END                       

!endregion

Access:EXCHAMF       &FileManager,THREAD                   ! FileManager for EXCHAMF
Relate:EXCHAMF       &RelationManager,THREAD               ! RelationManager for EXCHAMF
Access:EXCHAUI       &FileManager,THREAD                   ! FileManager for EXCHAUI
Relate:EXCHAUI       &RelationManager,THREAD               ! RelationManager for EXCHAUI
Access:LOANAMF       &FileManager,THREAD                   ! FileManager for LOANAMF
Relate:LOANAMF       &RelationManager,THREAD               ! RelationManager for LOANAMF
Access:LOANAUI       &FileManager,THREAD                   ! FileManager for LOANAUI
Relate:LOANAUI       &RelationManager,THREAD               ! RelationManager for LOANAUI
Access:EXCAUDIT      &FileManager,THREAD                   ! FileManager for EXCAUDIT
Relate:EXCAUDIT      &RelationManager,THREAD               ! RelationManager for EXCAUDIT
Access:STOCKTYP      &FileManager,THREAD                   ! FileManager for STOCKTYP
Relate:STOCKTYP      &RelationManager,THREAD               ! RelationManager for STOCKTYP
Access:LOAN          &FileManager,THREAD                   ! FileManager for LOAN
Relate:LOAN          &RelationManager,THREAD               ! RelationManager for LOAN
Access:LOANHIST      &FileManager,THREAD                   ! FileManager for LOANHIST
Relate:LOANHIST      &RelationManager,THREAD               ! RelationManager for LOANHIST
Access:EXCHHIST      &FileManager,THREAD                   ! FileManager for EXCHHIST
Relate:EXCHHIST      &RelationManager,THREAD               ! RelationManager for EXCHHIST
Access:EXCHANGE      &FileManager,THREAD                   ! FileManager for EXCHANGE
Relate:EXCHANGE      &RelationManager,THREAD               ! RelationManager for EXCHANGE
Access:LOANACC       &FileManager,THREAD                   ! FileManager for LOANACC
Relate:LOANACC       &RelationManager,THREAD               ! RelationManager for LOANACC
Access:EXCHACC       &FileManager,THREAD                   ! FileManager for EXCHACC
Relate:EXCHACC       &RelationManager,THREAD               ! RelationManager for EXCHACC
Access:EXCHANGE_ALIAS &FileManager,THREAD                  ! FileManager for EXCHANGE_ALIAS
Relate:EXCHANGE_ALIAS &RelationManager,THREAD              ! RelationManager for EXCHANGE_ALIAS

FuzzyMatcher         FuzzyClass                            ! Global fuzzy matcher
GlobalErrorStatus    ErrorStatusClass,THREAD
GlobalErrors         ErrorClass                            ! Global error manager
INIMgr               INIClass                              ! Global non-volatile storage manager
GlobalRequest        BYTE(0),THREAD                        ! Set when a browse calls a form, to let it know action to perform
GlobalResponse       BYTE(0),THREAD                        ! Set to the response from the form
VCRRequest           LONG(0),THREAD                        ! Set to the request from the VCR buttons

Dictionary           CLASS,THREAD
Construct              PROCEDURE
Destruct               PROCEDURE
                     END


  CODE
  GlobalErrors.Init(GlobalErrorStatus)
  FuzzyMatcher.Init                                        ! Initilaize the browse 'fuzzy matcher'
  FuzzyMatcher.SetOption(MatchOption:NoCase, 1)            ! Configure case matching
  FuzzyMatcher.SetOption(MatchOption:WordOnly, 0)          ! Configure 'word only' matching
  INIMgr.Init('.\ExchangeUnitFix.INI', NVD_INI)            ! Configure INIManager to use INI file
  DctInit
  SYSTEM{PROP:Icon} = 'clarion.ico'
  Main
  INIMgr.Update
  INIMgr.Kill                                              ! Destroy INI manager
  FuzzyMatcher.Kill                                        ! Destroy fuzzy matcher


Dictionary.Construct PROCEDURE

  CODE
  IF THREAD()<>1
     DctInit()
  END


Dictionary.Destruct PROCEDURE

  CODE
  DctKill()

