

  MEMBER('RemoveRRCJobFromClaims.clw')
Progress:Thermometer        LONG
Skip                LONG
ProgressWindow      WINDOW('Progress...'),AT(,,237,47),CENTER,GRAY,ICON('Cellular3g.ico'), |
                        FONT('Tahoma',8),TIMER(1),DOUBLE
                        PROGRESS,AT(9,15,219,12),USE(Progress:Thermometer),RANGE(0,100)
                        STRING(''),AT(0,3,237,10),USE(?Progress:UserString),CENTER,FONT(,8)
                        STRING(''),AT(0,30,237,10),USE(?Progress:PctText),CENTER
                    END

  MAP
    END

RecordsFound        LONG

Main                PROCEDURE()
    CODE
        BEEP(BEEP:SystemExclamation)  ;  YIELD()
        CASE MESSAGE('Process to remove ARC Repaired jobs from the RRC Warranty Claim Table.'&|
            '|'&|
            '|Click OK To Begin.','Vodacom',|
            ICON:Exclamation,'&OK|&Cancel',2) 
        OF 1 ! &OK Button
        OF 2 ! &Cancel Button
            RETURN
        END!CASE MESSAGE
        
        Relate:JOBSWARR.Open()
        Relate:JOBSE.Open()
        Relate:LOCATLOG.Open()
        
        STREAM(JOBSWARR)
        
        OPEN(ProgressWindow)
        DISPLAY()
        ?Progress:Thermometer{PROP:RangeHigh} = RECORDS(JOBSWARR)
        
        Access:JOBSWARR.ClearKey(jow:RecordNumberKey)
        jow:RecordNumber = 1
        SET(jow:RecordNumberKey,jow:RecordNumberKey)
        LOOP UNTIL Access:JOBSWARR.Next() <> Level:Benign
            Skip += 1
            IF (Skip > 1000)
                YIELD()
                Skip = 0
            END ! IF
            ?Progress:Thermometer{prop:Progress} = ?Progress:Thermometer{prop:Progress} + 1
            ?Progress:PctText{PROP:Text} = INT((?Progress:Thermometer{prop:Progress} / ?Progress:Thermometer{PROP:RangeHigh}) * 100) & ' %'
            DISPLAY()
            
            IF (jow:RepairedAt <> 'RRC')
                CYCLE
            END ! IF
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = jow:RefNumber
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                IF (jobe:HubRepairDate > 0 OR jobe:HubRepair = 1)
                    RecordsFound += 1
                    ?Progress:UserString{PROP:Text} = RecordsFound
                    jow:RepairedAt = 'ARC'
                    Access:JOBSWARR.TryUpdate()
                    CYCLE
                END ! IF
            END ! IF

            Access:LOCATLOG.ClearKey(lot:NewLocationKey)
            lot:RefNumber = jow:RefNumber
            lot:NewLocation  = GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
            IF (Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign)
                RecordsFound += 1
                ?Progress:UserString{PROP:Text} = RecordsFound
                jow:RepairedAt = 'ARC'
                Access:JOBSWARR.TryUpdate()
            END ! IF
        END ! LOOP
        
        CLOSE(ProgressWindow)
        
        FLUSH(JOBSWARR)
        
        Relate:JOBSE.Close()
        Relate:JOBSWARR.Close()
        Relate:LOCATLOG.Close()
        
        BEEP(BEEP:SystemExclamation)  ;  YIELD()
        CASE MESSAGE('Finished','Vodacom',|
            ICON:Exclamation,'&OK',1) 
        OF 1 ! &OK Button
        END!CASE MESSAGE