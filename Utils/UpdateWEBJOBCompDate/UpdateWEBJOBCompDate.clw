
                    PROGRAM


                    MAP
                    END

locExportFile       CSTRING(255),STATIC
ExportFile          FILE,DRIVER('ASCII'),PRE(expfil),NAME(locExportFile),CREATE,BINDABLE,THREAD
RECORD                  RECORD
Line                        STRING(1000)
                        END
                    END

ProgressWindow      WINDOW('Progress...'),AT(,,231,59),CENTER,GRAY,FONT('Tahoma',8),TIMER(1),DOUBLE
                        PROGRESS,AT(17,15,196,12),USE(?Progress:Therm),RANGE(0,100)
                        STRING(''),AT(0,3,,10),FULL,USE(?Progress:UserString),CENTER
                        STRING(''),AT(0,30,,10),FULL,USE(?Progress:PctText),CENTER
                        BUTTON('Cancel'),AT(91,42,50,15),USE(?Progress:Cancel)
                    END
recs                    LONG
skipRecs                LONG
updated             LONG     
total               LONG
failed              LONG
oldDate             LONG
oldTime             LONG
oldComp             STRING(3)

! region DataFile Declarations
WEBJOB              FILE,DRIVER('Btrieve'),OEM,NAME('WEBJOB.DAT'),PRE(wob),CREATE,BINDABLE,THREAD
RecordNumberKey         KEY(wob:RecordNumber),NOCASE,PRIMARY
RefNumberKey            KEY(wob:RefNumber),DUP,NOCASE
HeadJobNumberKey        KEY(wob:HeadAccountNumber,wob:JobNumber),DUP,NOCASE
HeadOrderNumberKey      KEY(wob:HeadAccountNumber,wob:OrderNumber,wob:RefNumber),DUP,NOCASE
HeadMobileNumberKey     KEY(wob:HeadAccountNumber,wob:MobileNumber,wob:RefNumber),DUP,NOCASE
HeadModelNumberKey      KEY(wob:HeadAccountNumber,wob:ModelNumber,wob:RefNumber),DUP,NOCASE
HeadIMEINumberKey       KEY(wob:HeadAccountNumber,wob:IMEINumber,wob:RefNumber),DUP,NOCASE
HeadMSNKey              KEY(wob:HeadAccountNumber,wob:MSN),DUP,NOCASE
HeadEDIKey              KEY(wob:HeadAccountNumber,wob:EDI,wob:Manufacturer,wob:JobNumber),DUP,NOCASE
ValidationIMEIKey       KEY(wob:HeadAccountNumber,wob:Validation,wob:IMEINumber),DUP,NOCASE
HeadSubKey              KEY(wob:HeadAccountNumber,wob:SubAcountNumber,wob:RefNumber),DUP,NOCASE
HeadRefNumberKey        KEY(wob:HeadAccountNumber,wob:RefNumber),DUP,NOCASE
OracleNumberKey         KEY(wob:OracleExportNumber),DUP,NOCASE
HeadCurrentStatusKey    KEY(wob:HeadAccountNumber,wob:Current_Status,wob:RefNumber),DUP,NOCASE
HeadExchangeStatus      KEY(wob:HeadAccountNumber,wob:Exchange_Status,wob:RefNumber),DUP,NOCASE
HeadLoanStatusKey       KEY(wob:HeadAccountNumber,wob:Loan_Status,wob:RefNumber),DUP,NOCASE
ExcWayBillNoKey         KEY(wob:ExcWayBillNumber),DUP,NOCASE
LoaWayBillNoKey         KEY(wob:LoaWayBillNumber),DUP,NOCASE
JobWayBillNoKey         KEY(wob:JobWayBillNumber),DUP,NOCASE
RRCWInvoiceNumberKey    KEY(wob:RRCWInvoiceNumber,wob:RefNumber),DUP,NOCASE
DateBookedKey           KEY(wob:HeadAccountNumber,wob:DateBooked),DUP,NOCASE
DateCompletedKey        KEY(wob:HeadAccountNumber,wob:DateCompleted),DUP,NOCASE
CompletedKey            KEY(wob:HeadAccountNumber,wob:Completed),DUP,NOCASE
DespatchKey             KEY(wob:HeadAccountNumber,wob:ReadyToDespatch,wob:RefNumber),DUP,NOCASE
DespatchSubKey          KEY(wob:HeadAccountNumber,wob:ReadyToDespatch,wob:SubAcountNumber,wob:RefNumber),DUP,NOCASE
DespatchCourierKey      KEY(wob:HeadAccountNumber,wob:ReadyToDespatch,wob:DespatchCourier,wob:RefNumber),DUP,NOCASE
DespatchSubCourierKey   KEY(wob:HeadAccountNumber,wob:ReadyToDespatch,wob:SubAcountNumber,wob:DespatchCourier,wob:RefNumber),DUP,NOCASE
EDIKey                  KEY(wob:HeadAccountNumber,wob:EDI,wob:Manufacturer,wob:RefNumber),DUP,NOCASE
EDIRefNumberKey         KEY(wob:HeadAccountNumber,wob:EDI,wob:RefNumber),DUP,NOCASE
Record                  RECORD,PRE()
RecordNumber                LONG
JobNumber                   LONG
RefNumber                   LONG
HeadAccountNumber           STRING(30)
SubAcountNumber             STRING(30)
OrderNumber                 STRING(30)
IMEINumber                  STRING(30)
MSN                         STRING(30)
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
MobileNumber                STRING(30)
EDI                         STRING(3)
Validation                  STRING(3)
OracleExportNumber          LONG
Current_Status              STRING(30)
Exchange_Status             STRING(30)
Loan_Status                 STRING(30)
Current_Status_Date         DATE
Exchange_Status_Date        DATE
Loan_Status_Date            DATE
ExcWayBillNumber            LONG
LoaWayBillNumber            LONG
JobWayBillNumber            LONG
DateJobDespatched           DATE
RRCEngineer                 STRING(3)
ReconciledMarker            BYTE
RRCWInvoiceNumber           LONG
DateBooked                  DATE
TimeBooked                  TIME
DateCompleted               DATE
TimeCompleted               TIME
Completed                   STRING(3)
ReadyToDespatch             BYTE
DespatchCourier             STRING(30)
FaultCode13                 STRING(30)
FaultCode14                 STRING(30)
FaultCode15                 STRING(30)
FaultCode16                 STRING(30)
FaultCode17                 STRING(30)
FaultCode18                 STRING(30)
FaultCode19                 STRING(30)
FaultCode20                 STRING(30)
                        END
                    END   
JOBS                FILE,DRIVER('Btrieve'),NAME('JOBS.DAT'),PRE(job),BINDABLE,CREATE,THREAD
Ref_Number_Key          KEY(job:Ref_Number),NOCASE,PRIMARY
Model_Unit_Key          KEY(job:Model_Number,job:Unit_Type),DUP,NOCASE
EngCompKey              KEY(job:Engineer,job:Completed,job:Ref_Number),DUP,NOCASE
EngWorkKey              KEY(job:Engineer,job:Workshop,job:Ref_Number),DUP,NOCASE
Surname_Key             KEY(job:Surname),DUP,NOCASE
MobileNumberKey         KEY(job:Mobile_Number),DUP,NOCASE
ESN_Key                 KEY(job:ESN),DUP,NOCASE
MSN_Key                 KEY(job:MSN),DUP,NOCASE
AccountNumberKey        KEY(job:Account_Number,job:Ref_Number),DUP,NOCASE
AccOrdNoKey             KEY(job:Account_Number,job:Order_Number),DUP,NOCASE
Model_Number_Key        KEY(job:Model_Number),DUP,NOCASE
Engineer_Key            KEY(job:Engineer,job:Model_Number),DUP,NOCASE
Date_Booked_Key         KEY(job:date_booked),DUP,NOCASE
DateCompletedKey        KEY(job:Date_Completed),DUP,NOCASE
ModelCompKey            KEY(job:Model_Number,job:Date_Completed),DUP,NOCASE
By_Status               KEY(job:Current_Status,job:Ref_Number),DUP,NOCASE
StatusLocKey            KEY(job:Current_Status,job:Location,job:Ref_Number),DUP,NOCASE
Location_Key            KEY(job:Location,job:Ref_Number),DUP,NOCASE
Third_Party_Key         KEY(job:Third_Party_Site,job:Third_Party_Printed,job:Ref_Number),DUP,NOCASE
ThirdEsnKey             KEY(job:Third_Party_Site,job:Third_Party_Printed,job:ESN),DUP,NOCASE
ThirdMsnKey             KEY(job:Third_Party_Site,job:Third_Party_Printed,job:MSN),DUP,NOCASE
PriorityTypeKey         KEY(job:Job_Priority,job:Ref_Number),DUP,NOCASE
Unit_Type_Key           KEY(job:Unit_Type),DUP,NOCASE
EDI_Key                 KEY(job:Manufacturer,job:EDI,job:EDI_Batch_Number,job:Ref_Number),DUP,NOCASE
InvoiceNumberKey        KEY(job:Invoice_Number),DUP,NOCASE
WarInvoiceNoKey         KEY(job:Invoice_Number_Warranty),DUP,NOCASE
Batch_Number_Key        KEY(job:Batch_Number,job:Ref_Number),DUP,NOCASE
Batch_Status_Key        KEY(job:Batch_Number,job:Current_Status,job:Ref_Number),DUP,NOCASE
BatchModelNoKey         KEY(job:Batch_Number,job:Model_Number,job:Ref_Number),DUP,NOCASE
BatchInvoicedKey        KEY(job:Batch_Number,job:Invoice_Number,job:Ref_Number),DUP,NOCASE
BatchCompKey            KEY(job:Batch_Number,job:Completed,job:Ref_Number),DUP,NOCASE
ChaInvoiceKey           KEY(job:Chargeable_Job,job:Account_Number,job:Invoice_Number,job:Ref_Number),DUP,NOCASE
InvoiceExceptKey        KEY(job:Invoice_Exception,job:Ref_Number),DUP,NOCASE
ConsignmentNoKey        KEY(job:Consignment_Number),DUP,NOCASE
InConsignKey            KEY(job:Incoming_Consignment_Number),DUP,NOCASE
ReadyToDespKey          KEY(job:Despatched,job:Ref_Number),DUP,NOCASE
ReadyToTradeKey         KEY(job:Despatched,job:Account_Number,job:Ref_Number),DUP,NOCASE
ReadyToCouKey           KEY(job:Despatched,job:Current_Courier,job:Ref_Number),DUP,NOCASE
ReadyToAllKey           KEY(job:Despatched,job:Account_Number,job:Current_Courier,job:Ref_Number),DUP,NOCASE
DespJobNumberKey        KEY(job:Despatch_Number,job:Ref_Number),DUP,NOCASE
DateDespatchKey         KEY(job:Courier,job:Date_Despatched,job:Ref_Number),DUP,NOCASE
DateDespLoaKey          KEY(job:Loan_Courier,job:Loan_Despatched,job:Ref_Number),DUP,NOCASE
DateDespExcKey          KEY(job:Exchange_Courier,job:Exchange_Despatched,job:Ref_Number),DUP,NOCASE
ChaRepTypeKey           KEY(job:Repair_Type),DUP,NOCASE
WarRepTypeKey           KEY(job:Repair_Type_Warranty),DUP,NOCASE
ChaTypeKey              KEY(job:Charge_Type),DUP,NOCASE
WarChaTypeKey           KEY(job:Warranty_Charge_Type),DUP,NOCASE
Bouncer_Key             KEY(job:Bouncer,job:Ref_Number),DUP,NOCASE
EngDateCompKey          KEY(job:Engineer,job:Date_Completed),DUP,NOCASE
ExcStatusKey            KEY(job:Exchange_Status,job:Ref_Number),DUP,NOCASE
LoanStatusKey           KEY(job:Loan_Status,job:Ref_Number),DUP,NOCASE
ExchangeLocKey          KEY(job:Exchange_Status,job:Location,job:Ref_Number),DUP,NOCASE
LoanLocKey              KEY(job:Loan_Status,job:Location,job:Ref_Number),DUP,NOCASE
BatchJobKey             KEY(job:InvoiceAccount,job:InvoiceBatch,job:Ref_Number),DUP,NOCASE
BatchStatusKey          KEY(job:InvoiceAccount,job:InvoiceBatch,job:InvoiceStatus,job:Ref_Number),DUP,NOCASE
Record                  RECORD,PRE()
Ref_Number                  LONG
Batch_Number                LONG
Internal_Status             STRING(10)
Auto_Search                 STRING(30)
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Cancelled                   STRING(3)
Bouncer                     STRING(8)
Bouncer_Type                STRING(3)
Web_Type                    STRING(3)
Warranty_Job                STRING(3)
Chargeable_Job              STRING(3)
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(20)
MSN                         STRING(20)
ProductCode                 STRING(30)
Unit_Type                   STRING(30)
Colour                      STRING(30)
Location_Type               STRING(10)
Phone_Lock                  STRING(30)
Workshop                    STRING(3)
Location                    STRING(30)
Authority_Number            STRING(30)
Insurance_Reference_Number  STRING(30)
DOP                         DATE
Insurance                   STRING(3)
Insurance_Type              STRING(30)
Transit_Type                STRING(30)
Physical_Damage             STRING(3)
Intermittent_Fault          STRING(3)
Loan_Status                 STRING(30)
Exchange_Status             STRING(30)
Job_Priority                STRING(30)
Charge_Type                 STRING(30)
Warranty_Charge_Type        STRING(30)
Current_Status              STRING(30)
Account_Number              STRING(15)
Trade_Account_Name          STRING(30)
Department_Name             STRING(30)
Order_Number                STRING(30)
POP                         STRING(3)
In_Repair                   STRING(3)
Date_In_Repair              DATE
Time_In_Repair              TIME
On_Test                     STRING(3)
Date_On_Test                DATE
Time_On_Test                TIME
Estimate_Ready              STRING(3)
QA_Passed                   STRING(3)
Date_QA_Passed              DATE
Time_QA_Passed              TIME
QA_Rejected                 STRING(3)
Date_QA_Rejected            DATE
Time_QA_Rejected            TIME
QA_Second_Passed            STRING(3)
Date_QA_Second_Passed       DATE
Time_QA_Second_Passed       TIME
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Mobile_Number               STRING(15)
Postcode_Collection         STRING(10)
Company_Name_Collection     STRING(30)
Address_Line1_Collection    STRING(30)
Address_Line2_Collection    STRING(30)
Address_Line3_Collection    STRING(30)
Telephone_Collection        STRING(15)
Postcode_Delivery           STRING(10)
Address_Line1_Delivery      STRING(30)
Company_Name_Delivery       STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Date_Completed              DATE
Time_Completed              TIME
Completed                   STRING(3)
Paid                        STRING(3)
Paid_Warranty               STRING(3)
Date_Paid                   DATE
Repair_Type                 STRING(30)
Repair_Type_Warranty        STRING(30)
Engineer                    STRING(3)
Ignore_Chargeable_Charges   STRING(3)
Ignore_Warranty_Charges     STRING(3)
Ignore_Estimate_Charges     STRING(3)
Courier_Cost                REAL
Advance_Payment             REAL
Labour_Cost                 REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier_Cost_Estimate       REAL
Labour_Cost_Estimate        REAL
Parts_Cost_Estimate         REAL
Sub_Total_Estimate          REAL
Courier_Cost_Warranty       REAL
Labour_Cost_Warranty        REAL
Parts_Cost_Warranty         REAL
Sub_Total_Warranty          REAL
Loan_Issued_Date            DATE
Loan_Unit_Number            LONG
Loan_accessory              STRING(3)
Loan_User                   STRING(3)
Loan_Courier                STRING(30)
Loan_Consignment_Number     STRING(30)
Loan_Despatched             DATE
Loan_Despatched_User        STRING(3)
Loan_Despatch_Number        LONG
LoaService                  STRING(1)
Exchange_Unit_Number        LONG
Exchange_Authorised         STRING(3)
Loan_Authorised             STRING(3)
Exchange_Accessory          STRING(3)
Exchange_Issued_Date        DATE
Exchange_User               STRING(3)
Exchange_Courier            STRING(30)
Exchange_Consignment_Number STRING(30)
Exchange_Despatched         DATE
Exchange_Despatched_User    STRING(3)
Exchange_Despatch_Number    LONG
ExcService                  STRING(1)
Date_Despatched             DATE
Despatch_Number             LONG
Despatch_User               STRING(3)
Courier                     STRING(30)
Consignment_Number          STRING(30)
Incoming_Courier            STRING(30)
Incoming_Consignment_Number STRING(30)
Incoming_Date               DATE
Despatched                  STRING(3)
Despatch_Type               STRING(3)
Current_Courier             STRING(30)
JobService                  STRING(1)
Last_Repair_Days            REAL
Third_Party_Site            STRING(30)
Estimate                    STRING(3)
Estimate_If_Over            REAL
Estimate_Accepted           STRING(3)
Estimate_Rejected           STRING(3)
Third_Party_Printed         STRING(3)
ThirdPartyDateDesp          DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(255)
Fault_Code11                STRING(255)
Fault_Code12                STRING(255)
PreviousStatus              STRING(30)
StatusUser                  STRING(3)
Status_End_Date             DATE
Status_End_Time             TIME
Turnaround_End_Date         DATE
Turnaround_End_Time         TIME
Turnaround_Time             STRING(30)
Special_Instructions        STRING(30)
InvoiceAccount              STRING(15)
InvoiceStatus               STRING(3)
InvoiceBatch                LONG
InvoiceQuery                STRING(255)
EDI                         STRING(3)
EDI_Batch_Number            REAL
Invoice_Exception           STRING(3)
Invoice_Failure_Reason      STRING(80)
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Date_Warranty       DATE
Invoice_Courier_Cost        REAL
Invoice_Labour_Cost         REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
Invoice_Number_Warranty     LONG
WInvoice_Courier_Cost       REAL
WInvoice_Labour_Cost        REAL
WInvoice_Parts_Cost         REAL
WInvoice_Sub_Total          REAL
                        END
                    END  
!endregion


    CODE
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()
        CASE MESSAGE('Process to update the WEBJOB Date/Time Completed'&|
            '|values to match the JOBS values.','ServiceBase',|
            ICON:Asterisk,'&Begin|&Cancel',2) 
        OF 1 ! &Begin Button
        OF 2 ! &Cancel Button
            RETURN
        END!CASE MESSAGE
        
        SHARE(WEBJOB)
        SHARE(JOBS)
        STREAM(JOBS)
        
        locExportFile = 'UpdatedWEBJOBS.csv'
        REMOVE(ExportFile)
        CREATE(ExportFile)
        OPEN(ExportFile)
        
        OPEN(ProgressWindow)
        SETCURSOR(CURSOR:Wait)
        total = 0
        CLEAR(job:Record)
        job:Date_Completed = DEFORMAT('01/01/2014',@d06)
        SET(job:DateCompletedKey,job:DateCompletedKey)
        LOOP
            NEXT(JOBS)
            IF (ERRORCODE())
                BREAK
            END ! IF
            total += 1
        END ! LOOP
        SETCURSOR()
        
        ?Progress:Therm{PROP:RangeHigh} = total
        ?Progress:Therm{PROP:Progress} = 0
        DISPLAY()
        recs = 0
        skipRecs = 0
        failed = 0
        
        CLEAR(job:Record)
        job:Date_Completed = DEFORMAT('01/01/2014',@d06)
        SET(job:DateCompletedKey,job:DateCompletedKey)
        LOOP
            NEXT(JOBS)
            IF (ERRORCODE())
                BREAK
            END ! IF
            recs += 1
            skipRecs += 1
            IF (skipRecs > 500)
                ?Progress:Therm{PROP:Progress} = recs
                DISPLAY()
                skipRecs = 0
            END ! IF
            
            CLEAR(wob:Record)
            wob:RefNumber = job:Ref_Number
            GET(WEBJOB,wob:RefNumberKey)
            IF (~ERRORCODE())
                IF (wob:DateCompleted <> job:Date_Completed)
                    oldDate = wob:DateCompleted
                    oldTime = wob:TimeCompleted
                    oldComp = wob:Completed
                    wob:DateCompleted = job:Date_Completed
                    wob:TimeCompleted = job:Time_Completed
                    wob:Completed = 'YES'
                    updated += 1
                    PUT(WEBJOB)
                    IF (ERRORCODE())
                        failed += 1
                    ELSE
                        expfil:Line = job:Ref_Number & ',' & |
                            oldComp & ',' & |
                            FORMAT(oldDate,@d06b) & ',' & |
                            FORMAT(oldTime,@t04b)  & ',' & | 
                            wob:Completed & ',' & |
                            FORMAT(wob:DateCompleted,@d06b) & ',' & |
                            FORMAT(wob:TimeCompleted,@t04b)    
                        ADD(ExportFile)
                    END ! IF
                    
                END ! IF
            END ! IF
        END ! LOOP
        
        FLUSH(JOBS)
        CLOSE(JOBS)
        CLOSE(WEBJOB)
        CLOSE(ExportFile)
        
        CLOSE(ProgressWindow)
        
        IF (updated = 0)
            BEEP(BEEP:SystemAsterisk)  ;  YIELD()
            CASE MESSAGE('No records updated.','ServiceBase',|
                ICON:Asterisk,'&OK',1) 
            OF 1 ! &OK Button
            END!CASE MESSAGE            
        ELSE
        
            BEEP(BEEP:SystemAsterisk)  ;  YIELD()
            CASE MESSAGE('Process Completed.'&|
                '|'&|
                '|Number of failures: ' & CLIP(failed) & ''&|
                '|'&|
                '|A csv file called "UpdatedWEBJOBS.csv" has been created.'&|
                '|Please send this to PCCS.','ServiceBase',|
                ICON:Asterisk,'&OK',1) 
            OF 1 ! &OK Button
            END!CASE MESSAGE
        
        END !I F
        
        RETURN
