

   MEMBER('imeidupes.clw')                            ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('IMEID001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

TotalRecords         LONG
RecordsRead          LONG
RecordsRemoved       LONG
IMEINoQueue          QUEUE,PRE(INQ)
IMEINumber           STRING(8)
Counter              LONG
                     END
ProcessString        STRING(100)
RecordNoQueue        QUEUE,PRE(RNQ)
RecordNo             LONG
                     END
SetActive            STRING(1)
ESNModalQueue        QUEUE,PRE(EMQ)
Refnumber            LONG
Counter              LONG
                     END
AlternateRecordsTotal LONG
AlternateRecordsRemoved LONG
ModelActive          STRING(1)
window               WINDOW('IMEI Number Correlation Duplicate Removal Tool'),AT(,,260,154),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),CENTER,GRAY,DOUBLE
                       STRING('Correlation Records Total'),AT(42,8),USE(?String1)
                       STRING(@n-14),AT(166,8),USE(TotalRecords),RIGHT
                       STRING('Duplicates Removed'),AT(42,23),USE(?String3)
                       STRING(@n-14),AT(166,23),USE(RecordsRemoved),RIGHT
                       STRING(@n-14),AT(166,38),USE(AlternateRecordsTotal),RIGHT
                       STRING('Alternate Tac Code Record Total'),AT(42,38),USE(?String6)
                       STRING('Duplicates removed'),AT(42,53),USE(?String7)
                       STRING(@n-14),AT(166,53),USE(AlternateRecordsRemoved),RIGHT
                       CHECK('  Set TAC Codes ACTIVE'),AT(82,72),USE(SetActive),VALUE('Y','N')
                       CHECK('  Set All Models Active'),AT(82,90),USE(ModelActive),VALUE('Y','N')
                       STRING(@s100),AT(4,112,252,10),USE(ProcessString),CENTER
                       BUTTON('Start'),AT(102,130,56,16),USE(?StartButton)
                       BUTTON('Close'),AT(102,130,56,16),USE(?CloseButton),HIDE,STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?String1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ESNMODAL.Open
  Relate:ESNMODAL_ALIAS.Open
  Relate:ESNMODEL_ALIAS.Open
  Relate:MODELNUM.Open
  Access:ESNMODEL.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ESNMODAL.Close
    Relate:ESNMODAL_ALIAS.Close
    Relate:ESNMODEL_ALIAS.Close
    Relate:MODELNUM.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?StartButton
      !Process the records here
      TotalRecords = records(ESNModel)
      
      ProcessString = 'Reading Records - Please Wait'
      ?StartButton{prop:disable} = True
      
      display()
      
      Free(IMEINoQueue)
      Set(ESNModel,0)
      loop
          If access:ESNModel.next() then
              break
          End
          INQ:IMEINumber = esn:ESN
          get(IMEINoQueue,INQ:IMEINumber)
          If error() then
              !does not exist - so add it
              INQ:IMEINumber = esn:ESN
              INQ:Counter = 1
              Add(IMEINoQueue)
          Else
              INQ:Counter += 1
              Put(IMEINoQueue)
          End
      End
      
      
      RecordsRead = 0
      RecordsRemoved = 0
      
      ProcessString = 'Removing Depulicate TAC codes - Please Wait'
      
      !now loop through all the unique IMEI numbers and find the dupes
      Sort(IMEINoQueue,INQ:IMEINumber)
      loop x# = 1 to records(IMEINoQueue)
          Get(IMEINoQueue,x#)
          If error() then
              break
          End
          Free(RecordNoQueue)
          RecordsRead += 1
          Loop y# = 1 to INQ:Counter
              !we need to loop through the IMEIModel file this many time to make sure we have dealt with every record
              Access:ESNModel.clearkey(esn:ESN_Only_Key)
              esn:ESN = INQ:IMEINumber
              Set(esn:ESN_Only_Key,esn:ESN_Only_Key)
              Loop
                  If access:ESNModel.next() then
                      break
                  End
                  If esn:ESN <> INQ:IMEINumber then
                      break
                  End
                  !ok - have i checked this record number before
                  RNQ:RecordNo = esn:Record_Number
                  Get(RecordNoQueue,RNQ:RecordNo)
                  If error() then
                      RNQ:RecordNo = esn:Record_Number
                      Add(RecordNoQueue)
                  Else
                      !dont need to use this record
                      cycle
                  End
                  !ok - we now need to compare this record to all the same IMEI
                  access:ESNModel_Alias.clearkey(esn_ali:ESN_Only_Key)
                  esn_ali:ESN = esn:ESN
                  Set(esn_ali:ESN_Only_Key,esn_ali:ESN_Only_Key)
                  loop
                      if Access:ESNModel_Alias.next() then
                          break
                      End
                      If esn_ali:ESN <> esn:ESN then
                          break
                      End
                      If esn_ali:Record_Number = esn:Record_Number then
                          cycle
                      End
                      !now we need to check the model number and the manufacturer
                      If clip(esn_ali:Model_Number) = clip(esn:Model_Number) then
                          If clip(esn_ali:Manufacturer) = clip(esn:Manufacturer) then
                              !ok - both the model and manufacturer are the same - so this record needs to be removed
                              !before we remove the entry - check to see if it has any TAC Code Alias records
                              Access:ESNModal.clearkey(esa:TACCodeKey)
                              esa:RefNumber = esn_ali:Record_Number
                              Set(esa:TACCodeKey,esa:TACCodeKey)
                              loop
                                  If Access:ESNModal.next() then
                                      break
                                  End
                                  If esa:RefNumber <> esn_ali:Record_Number then
                                      break
                                  End
                                  !ok - if we are here there must be some records - so change them to the new parent record
                                  esa:RefNumber = esn:Record_Number
                                  access:ESNModal.Update()
                              End
                              delete(ESNModel_Alias)
                              RecordsRemoved += 1
                              Display()
                          End
                      End
                  End !loop
              End !Loop
          End !Loop
      End !loop
      
      
      ProcessString = 'Removing Depulicate Alternative TAC codes - Please Wait'
      !now we need to see if we have any duplicate alternate TAC codes
      !so get one of each ref numbers first - and put them in a queue
      Free(ESNModalQueue)
      Set(ESNModal,0)
      loop
          If Access:ESNModal.next() then
              break
          End
          EMQ:Refnumber = esa:RefNumber
          Get(ESNModalQueue,EMQ:Refnumber)
          If error() then
              !add this record to the queue
              EMQ:Refnumber = esa:RefNumber
              EMQ:Counter = 1
              Add(ESNModalQueue)
          Else
              EMQ:Counter += 1
              Put(ESNModalQueue)
          End
      End
      
      AlternateRecordsTotal = records(ESNModal)
      AlternateRecordsRemoved = 0
      
      !now we should have a list of unique ref numbers - and how many entries for each one
      !now we can remove the duplicates
      loop x# = 1 to records(ESNModalQueue)
          get(ESNModalQueue,x#)
          If error() then
              break
          End
          loop y# = 1 to EMQ:Counter
              Access:ESNModal.clearkey(esa:TACCodeKey)
              esa:RefNumber = EMQ:Refnumber
              Set(esa:TACCodeKey,esa:TACCodeKey)
              Loop
                  if access:ESNModal.next() then
                      break
                  End
                  If esa:RefNumber <> EMQ:Refnumber then
                      break
                  End
                  !now open the alias file to find the duplicates
                  Access:ESNModal_Alias.clearkey(esa_ali:TACCodeKey)
                  esa_ali:RefNumber = esa:RefNumber
                  Set(esa_ali:TACCodeKey,esa_ali:TACCodeKey)
                  loop
                      if Access:ESNModal_Alias.next() then
                          break
                      End
                      If esa_ali:RefNumber <>  esa:RefNumber then
                          break
                      End
                      if esa:RecordNumber = esa_ali:RecordNumber then
                          cycle
                      End
                      !ok - now check to see if the Tac Codes and Model Numbers Match
                      If clip(esa:TacCode) = Clip(esa_ali:TacCode) then
                          If clip(esa:ModelNumber) = clip(esa_ali:ModelNumber) then
                              !ok - everything matches - so remove this entry
                              Delete(ESNModal_Alias)
                              AlternateRecordsRemoved += 1
                              Display()
                          End
                      End
                  End
              End
          End
      End
      
      
      
      
      If SetActive = 'Y' then
          !loop through all the records and set the TAC code to active
          ProcessString = 'Setting TAC Code ACTIVE Flag - Please Wait'
          Set(ESNModel,0)
          Loop
              If Access:ESNModel.next() then
                  break
              End
              esn:Active = 'Y'
              Access:ESNModel.update()
          End
      End
      
      If ModelActive = 'Y' then
          !set all the active flags on the models to 'Y'
          ProcessString = 'Setting Model ACTIVE Flag - Please Wait'
          set(ModelNum,0)
          loop
              if access:ModelNum.next() then
                  break
              End
              mod:Active = 'Y'
              Access:ModelNum.update()
          End
      
      End
      
      
      
      ProcessString = 'Complete'
      
      ?CloseButton{Prop:Hide} = False
      
      Display()
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

