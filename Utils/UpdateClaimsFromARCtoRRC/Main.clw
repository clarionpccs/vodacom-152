  MEMBER('UpdateClaimsFromARCToRRC.clw')
                    MAP
                    END

Main                PROCEDURE()
exp                     SimpleExportClass

Progress:Thermometer    LONG
Skip                    LONG
ProgressWindow          WINDOW('Progress...'),AT(,,237,47),CENTER,GRAY,ICON('Cellular3g.ico'), |
                            FONT('Tahoma',8),TIMER(1),DOUBLE
                            PROGRESS,AT(9,15,219,12),USE(Progress:Thermometer),RANGE(0,100)
                            STRING(''),AT(0,3,237,10),USE(?Progress:UserString),CENTER,FONT(,8)
                            STRING(''),AT(0,30,237,10),USE(?Progress:PctText),CENTER
                        END
TotalRecords            LONG
RecordsFound            LONG
RecordsUpdated          LONG
exportFileName          STRING(255)
startDate               LONG

testing                 EQUATE(0)
    CODE
        BEEP(BEEP:SystemExclamation)  ;  YIELD()
        CASE MESSAGE('Process To Update "RepairAt" Column From "ARC" to "RRC".'&|
            '|'&|
            '|Click OK To Begin.','Vodacom',|
            ICON:Exclamation,'&OK|&Cancel',2) 
        OF 1 ! &OK Button
        OF 2 ! &Cancel Button
            RETURN
        END!CASE MESSAGE
        
        Relate:JOBSWARR.Open()
        Relate:JOBSE.Open()
        Relate:LOCATLOG.Open()
        
        exportFileName = 'UpdateClaimsFromARCtoRRC.csv'
        
        IF (exp.Init(exportFilename,,1))
        END ! IF
        
        exp.AddField('Job Number',1)
        exp.AddField('Action')
        exp.AddField('Error Reason',,1)
        
        STREAM(JOBSWARR)
        
        OPEN(ProgressWindow)
        DISPLAY()
        
        startDate = DEFORMAT('02/07/2015',@d06)

        IF (testing = 1)
            startDate = 0 ! Debug Only        
            TotalRecords = RECORDS(JOBSWARR) ! Debug Only       
            STOP('Running In Debug Mode')
        END ! IF
        
        IF (testing = 0)
            SETCURSOR(CURSOR:Wait)
            TotalRecords = 0
            Access:JOBSWARR.ClearKey(jow:ClaimSubmittedKey)
            jow:ClaimSubmitted = startDate
            SET(jow:ClaimSubmittedKey,jow:ClaimSubmittedKey)
            LOOP UNTIL Access:JOBSWARR.Next() <> Level:Benign
                IF (jow:ClaimSubmitted < startDate)
                    CYCLE
                END ! IF

                IF (jow:RepairedAt <> 'ARC')
                    CYCLE
                END ! IF
            
                TotalRecords += 1
            END ! LOOP
        
            SETCURSOR()
        END ! IF
        
        ?Progress:Thermometer{PROP:RangeHigh} = TotalRecords
        
        Access:JOBSWARR.ClearKey(jow:ClaimSubmittedKey)
        jow:ClaimSubmitted = startDate
        SET(jow:ClaimSubmittedKey,jow:ClaimSubmittedKey)
        LOOP UNTIL Access:JOBSWARR.Next() <> Level:Benign
            IF (jow:ClaimSubmitted < startDate)
                CYCLE
            END ! IF
            Skip += 1
            IF (Skip > 1000)
                YIELD()
                Skip = 0
            END ! IF
            ?Progress:Thermometer{prop:Progress} = ?Progress:Thermometer{prop:Progress} + 1
            ?Progress:PctText{PROP:Text} = INT((?Progress:Thermometer{prop:Progress} / ?Progress:Thermometer{PROP:RangeHigh}) * 100) & ' %'
            DISPLAY()
            
            IF (jow:RepairedAt <> 'ARC')
                CYCLE
            END ! IF
            
            !exp.AddField(jow:RefNumber,1,1)
            
            ! Job has been marked as Hub Repair. Ignore
            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = jow:RefNumber
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                IF (jobe:HubRepairDate > 0 OR jobe:HubRepair = 1)
                    CYCLE
                END ! IF
            END ! IF

            Access:LOCATLOG.ClearKey(lot:NewLocationKey)
            lot:RefNumber = jow:RefNumber
            lot:NewLocation  = GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
            IF (Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign)
                ! Job has been to the ARC Location
                CYCLE
            ELSE
                RecordsFound += 1
                ?Progress:UserString{PROP:Text} = RecordsFound
                jow:RepairedAt = 'RRC'
                IF (Access:JOBSWARR.TryUpdate() = Level:Benign)
                    RecordsUpdated += 1
                    exp.AddField(jow:RefNumber,1)
                    exp.AddField('Updated',,1)
                ELSE
                    exp.AddField(jow:RefNumber,1)
                    exp.AddField('Failed')
                    exp.AddField('Job In Use',,1)
                END ! IF
                
            END ! IF
        END ! LOOP
        
        CLOSE(ProgressWindow)
        
        exp.Kill()
        
        FLUSH(JOBSWARR)
        
        Relate:JOBSE.Close()
        Relate:JOBSWARR.Close()
        Relate:LOCATLOG.Close()
        
        BEEP(BEEP:SystemExclamation)  ;  YIELD()
        CASE MESSAGE('Finished||Updated: ' & RecordsUpdated,'Vodacom',|
            ICON:Exclamation,'&OK',1) 
        OF 1 ! &OK Button
        END!CASE MESSAGE