

   MEMBER('sbj02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ02007.INC'),ONCE        !Local module procedure declarations
                     END


BrowseStockFaultCodeLookup PROCEDURE (f:RefNumber,f:FieldNumber) !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
tmp:Manufacturer     STRING(30)
tmp:FieldNumber      LONG
tmp:StockRefNumber   LONG
tmp:CCT              BYTE(0)
BRW1::View:Browse    VIEW(STOMPFAU)
                       PROJECT(stu:Field)
                       PROJECT(stu:Description)
                       PROJECT(stu:RecordNumber)
                       PROJECT(stu:RefNumber)
                       PROJECT(stu:FieldNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
stu:Field              LIKE(stu:Field)                !List box control field - type derived from field
stu:Description        LIKE(stu:Description)          !List box control field - type derived from field
stu:RecordNumber       LIKE(stu:RecordNumber)         !Primary key field - type derived from field
stu:RefNumber          LIKE(stu:RefNumber)            !Browse key field - type derived from field
stu:FieldNumber        LIKE(stu:FieldNumber)          !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK10::stu:FieldNumber     LIKE(stu:FieldNumber)
HK10::stu:RefNumber       LIKE(stu:RefNumber)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Browse the Fault Code Lookup File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Fault Code Lookup File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(448,300),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                       LIST,AT(168,112,344,186),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M~Field~@s30@240L(2)|M~Description~@s60@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Field'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,98,124,10),USE(stu:Field),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,MSG('Field')
                         END
                         TAB('By Description'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s60),AT(168,100,124,10),USE(stu:Description),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020437'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseStockFaultCodeLookup')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANFAUPA.Open
  Access:MODELCCT.UseFile
  SELF.FilesOpened = True
  tmp:StockRefNumber = f:RefNumber
  tmp:FieldNumber = f:FieldNumber
  
  ! Inserting (DBH 30/04/2008) # 9723 - Limit by Model Number if it's a CCT Reference
  Access:MANFAUPA.Clearkey(map:Field_Number_Key)
  map:Manufacturer = job:Manufacturer
  map:Field_Number = tmp:FieldNumber
  If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
      ! Found
      ! Is this fault a CCT? (DBH: 30/04/2008)
      If map:CCTReferenceFaultCode = 1
          tmp:CCT = 1
      End ! If map:CCTReferenceFaultCode = 1
  End ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
  ! End (DBH 30/04/2008) #9723
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOMPFAU,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','BrowseStockFaultCodeLookup')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,stu:DescriptionKey)
  BRW1.AddRange(stu:FieldNumber)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?stu:Description,stu:Description,1,BRW1)
  BRW1.AddSortOrder(,stu:FieldKey)
  BRW1.AddRange(stu:FieldNumber)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?stu:Field,stu:Field,1,BRW1)
  BIND('GLO:Select1',GLO:Select1)
  BIND('GLO:Select2',GLO:Select2)
  BRW1.AddField(stu:Field,BRW1.Q.stu:Field)
  BRW1.AddField(stu:Description,BRW1.Q.stu:Description)
  BRW1.AddField(stu:RecordNumber,BRW1.Q.stu:RecordNumber)
  BRW1.AddField(stu:RefNumber,BRW1.Q.stu:RefNumber)
  BRW1.AddField(stu:FieldNumber,BRW1.Q.stu:FieldNumber)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Field'
    ?Tab2{PROP:TEXT} = 'By Description'
    ?Browse:1{PROP:FORMAT} ='80L(2)|M~Field~@s30@#1#240L(2)|M~Description~@s60@#2#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANFAUPA.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseStockFaultCodeLookup')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020437'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020437'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020437'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='80L(2)|M~Field~@s30@#1#240L(2)|M~Description~@s60@#2#'
          ?Tab:2{PROP:TEXT} = 'By Field'
        OF 2
          ?Browse:1{PROP:FORMAT} ='240L(2)|M~Description~@s60@#2#80L(2)|M~Field~@s30@#1#'
          ?Tab2{PROP:TEXT} = 'By Description'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?stu:Field
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      If GETINI('BROWSEORDER','FaultCode',,CLIP(PATH())&'\SB2KDEF.INI') = 1
          Select(?CurrentTab,2)
          Post(Event:NewSelection,?CurrentTab)
      End !GETINI('BROWSEORDER','FaultCode',,CLIP(PATH())&'\SB2KDEF.INI') = 1
    OF EVENT:GainFocus
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:StockRefNumber
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:FieldNumber
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:StockRefNumber
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:FieldNumber
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  ! Inserting (DBH 30/04/2008) # 9723 - Limit by Model Number if it's a CCT Reference
  If tmp:CCT = 1
      Access:MODELCCT.Clearkey(mcc:CCTRefKey)
      mcc:ModelNumber = job:Model_Number
      mcc:CCTReferenceNumber = stu:Field
      If Access:MODELCCT.TryFetch(mcc:CCTRefKey) = Level:Benign
          ! Found
  
      Else ! If Access:MODELCCT.TryFetch(mcc:CCTRefKey) = Level:Benign
          ! This is no reference in the CCT List. Don't show (DBH: 30/04/2008)
          Return Record:Filtered
      End ! If Access:MODELCCT.TryFetch(mcc:CCTRefKey) = Level:Benign
  End ! If map:CCTReferenceFaultCode
  ! End (DBH 30/04/2008) #9723
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

PUPValidation PROCEDURE (fIMEINumber,fMSN,fNetwork,fInFault) !Generated from procedure template - Window

locIMEINumber        STRING(30)
locMSN               STRING(30)
locNetwork           STRING(30)
locInFault           STRING(30)
locReturn            BYTE
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,82,352,248),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Validate Unit'),AT(316,134),USE(?Prompt7),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('I.M.E.I. Number'),AT(228,164),USE(?locIMEINumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold)
                       ENTRY(@s30),AT(328,164,124,10),USE(locIMEINumber),COLOR(COLOR:White),MSG('I.M.E.I. Number'),TIP('I.M.E.I. Number'),REQ,UPR
                       PROMPT('M.S.N.'),AT(228,188),USE(?locMSN:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold)
                       ENTRY(@s30),AT(328,188,124,10),USE(locMSN),HIDE,COLOR(COLOR:White),MSG('M.S.N.'),TIP('M.S.N.'),REQ,UPR
                       PROMPT('Network'),AT(228,212),USE(?locNetwork:Prompt),TRN,FONT(,,COLOR:White,FONT:bold)
                       ENTRY(@s30),AT(328,212,124,10),USE(locNetwork),COLOR(COLOR:White),MSG('Network'),TIP('Network'),REQ,UPR
                       BUTTON,AT(456,208),USE(?CallLookup),TRN,FLAT,ICON('lookupp.jpg')
                       PROMPT('In Fault'),AT(228,236),USE(?locInFault:Prompt),TRN,FONT(,,COLOR:White,FONT:bold)
                       ENTRY(@s30),AT(328,236,124,10),USE(locInFault),SKIP,COLOR(COLOR:White),MSG('In Faul'),TIP('In Faul'),REQ,UPR,READONLY
                       BUTTON,AT(456,232),USE(?Button:LookupInFault),TRN,FLAT,ICON('lookupp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('PUP Unit Validation'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(376,332),USE(?OK),TRN,FLAT,ICON('okp.jpg')
                       BUTTON,AT(444,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
errorStr    STRING(255)
                    MAP
BouncerCheck            PROCEDURE(STRING pIMEI, *STRING pError),LONG   
                    END ! MAP
PassLinkedFaultCodeQueue    Queue(DefLinkedFaultCodeQueue)
                            End
!Save Entry Fields Incase Of Lookup
look:locNetwork                Like(locNetwork)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(locReturn)

LookupButtonCode        Routine
Data
local:JobType                 Byte(0)
local:LinkedRecordNumber      Long(0)
local:FaultCode               String(255)
Code
    Access:MANFAULT.Clearkey(maf:InFaultKey)
    maf:Manufacturer    = job:Manufacturer
    maf:InFault    = 1
    if (Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign)
        ! Found
        If maf:Field_Type = 'DATE'
            locInFault =  TINCALENDARStyle1(locInFault)
            Exit
        Else ! If malocInFault_Type = 'DATE'
            SaveRequest# = GlobalRequest
            GlobalRequest = SelectRecord

            ! Used to restrict the lookup to char/warr (DBH: 23/10/2007)
            If job:Chargeable_Job = 'YES'
                local:JobType = 1
                If job:Warranty_Job = 'YES'
                    local:JobType = 0
                End ! If job:Warranty_Job = 'YES'
            Else ! If job:Chargeable_Job = 'YES'
                local:JobType = 2
            End ! If job:Chargeable_Job = 'YES'

            ! Go through all the fault codes and see if THIS fault code is a secondary linked code (DBH: 05/11/2007)
            Free(PassLinkedFaultCodeQueue)
            Loop num# = 1 To 20
                If num# = maf:Field_Number
                    Cycle
                End ! If num# = maf:Field_Number
                found# = 0
                case num#
                Of 1
                    if (job:Fault_Code1 <> '')
                        local:FaultCode = job:Fault_Code1
                        found# = 1
                    end !if (job:Fault_Code1 <> '')
                Of 2
                    if (job:Fault_Code2 <> '')
                        local:FaultCode = job:Fault_Code2
                        found# = 1
                    end !if (job:Fault_Code1 <> '')
                Of 3
                    if (job:Fault_Code3 <> '')
                        local:FaultCode = job:Fault_Code3
                        found# = 1
                    end !if (job:Fault_Code1 <> '')
                Of 4
                    if (job:Fault_Code4 <> '')
                        local:FaultCode = job:Fault_Code4
                        found# = 1
                    end !if (job:Fault_Code1 <> '')
                Of 5
                    if (job:Fault_Code5 <> '')
                        local:FaultCode = job:Fault_Code5
                        found# = 1
                    end !if (job:Fault_Code1 <> '')
                Of 6
                    if (job:Fault_Code6 <> '')
                        local:FaultCode = job:Fault_Code6
                        found# = 1
                    end !if (job:Fault_Code1 <> '')
                Of 7
                    if (job:Fault_Code7 <> '')
                        local:FaultCode = job:Fault_Code7
                        found# = 1
                    end !if (job:Fault_Code1 <> '')
                Of 8
                    if (job:Fault_Code8 <> '')
                        local:FaultCode = job:Fault_Code8
                        found# = 1
                    end !if (job:Fault_Code1 <> '')
                Of 9
                    if (job:Fault_Code9 <> '')
                        local:FaultCode = job:Fault_Code9
                        found# = 1
                    end !if (job:Fault_Code1 <> '')
                Of 10
                    if (job:Fault_Code10 <> '')
                        local:FaultCode = job:Fault_Code10
                        found# = 1
                    end !if (job:Fault_Code1 <> '')
                Of 11
                    if (job:Fault_Code11 <> '')
                        local:FaultCode = job:Fault_Code11
                        found# = 1
                    end !if (job:Fault_Code1 <> '')
                Of 12
                    if (job:Fault_Code12 <> '')
                        local:FaultCode = job:Fault_Code12
                        found# = 1
                    end !if (job:Fault_Code1 <> '')
                Of 13
                    if (wob:FaultCode13 <> '')
                        local:FaultCode = wob:FaultCode13
                        found# = 1
                    end !if (job:Fault_Code1 <> '')
                Of 14
                    if (wob:FaultCode14 <> '')
                        local:FaultCode = wob:FaultCode14
                        found# = 1
                    end !if (job:Fault_Code1 <> '')
                Of 15
                    if (wob:FaultCode15 <> '')
                        local:FaultCode = wob:FaultCode15
                        found# = 1
                    end !if (job:Fault_Code1 <> '')
                Of 16
                    if (wob:FaultCode16 <> '')
                        local:FaultCode = wob:FaultCode16
                        found# = 1
                    end !if (job:Fault_Code1 <> '')
                Of 17
                    if (wob:FaultCode17 <> '')
                        local:FaultCode = wob:FaultCode17
                        found# = 1
                    end !if (job:Fault_Code1 <> '')
                Of 18
                    if (wob:FaultCode18 <> '')
                        local:FaultCode = wob:FaultCode18
                        found# = 1
                    end !if (job:Fault_Code1 <> '')
                Of 19
                    if (wob:FaultCode19 <> '')
                        local:FaultCode = wob:FaultCode19
                        found# = 1
                    end !if (job:Fault_Code1 <> '')
                Of 20
                    if (wob:FaultCode20 <> '')
                        local:FaultCode = wob:FaultCode20
                        found# = 1
                    end !if (job:Fault_Code1 <> '')
                end ! case num#


                If found# = 1
                    ! Get the record number of the fault code value (DBH: 05/11/2007)
                    Access:MANFAULT_ALIAS.Clearkey(maf_ali:ScreenOrderKey)
                    maf_ali:Manufacturer = job:Manufacturer
                    maf_ali:ScreenOrder = num#
                    If Access:MANFAULT_ALIAS.TryFetch(maf_ali:ScreenOrderKey) = Level:Benign
                        Access:MANFAULO_ALIAS.Clearkey(mfo_ali:Field_Key)
                        mfo_ali:Manufacturer = job:Manufacturer
                        mfo_ali:Field_Number = maf_ali:Field_Number
                        mfo_ali:Field = local:FaultCode
                        If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:Field_Key) = Level:Benign
                            Found# = 0
                            Access:MANFAURL.Clearkey(mnr:FieldKey)
                            mnr:MANFAULORecordNumber = mfo_ali:RecordNumber
                            mnr:FieldNumber = maf:Field_Number
                            Set(mnr:FieldKey,mnr:FieldKey)
                            Loop
                                If Access:MANFAURL.Next()
                                    Break
                                End ! If Access:MANFAURL.Next()
                                If mnr:MANFAULORecordNumber <> mfo_ali:RecordNumber
                                    Break
                                End ! If mnr:MANFAULORecordNumber <> mfo_ali:RecordNumber
                                If mnr:FieldNumber <> maf:Field_Number
                                    Break
                                End ! If mnr:FieldNumber <> malocInFault_Number
                                Found# = 1
                                Break
                            End ! Loop
                            If Found# = 1
                                PassLinkedFaultCodeQueue.RecordNumber = mfo_ali:RecordNumber
                                PassLinkedFaultCodeQueue.FaultType = 'J'
                                Add(PassLinkedFaultCodeQueue)
                            End ! If Found# = 1
                        End ! If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:Field_Key) = Level:Benign
                    End ! If Access:MANFAULT_ALIAS.TryFetch(maf_ali:ScreenOrderKey) = Level:Benign
                End ! If tmp:FaultCode[num#] <> ''
            End ! Loop num# = 1 To 20

            Do LookupRelatedPartFaultCodes

            BrowseJobFaultCodeLookup(job:Manufacturer,maf:Field_Number,local:JobType,'',0,PassLinkedFaultCodeQueue)

            GlobalRequest = SaveRequest#

            If GlobalResponse = RequestCompleted
                Access:MANFAUEX.ClearKey(max:ModelNumberKey)
                max:MANFAULORecordNumber = mfo:RecordNumber
                max:ModelNumber          = job:Model_Number
                If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
                    !Found
                    Case Missive('The Model Number on this job has been "Excluded" from using the selected Fault Code.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                    Exit
                Else !If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
                    !Error
                End !If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign

                If locInFault <> mfo:Field
                    ! A new fault code value has been picked. Check the secondary fault codes (DBH: 05/11/2007)
                    Loop num# = 1 To 20
                        If num# = maf:Field_Number
                            Cycle
                        End ! If num# = malocInFault


                        Access:MANFAULT_ALIAS.Clearkey(maf_ali:ScreenOrderKey)
                        maf_ali:Manufacturer = job:Manufacturer
                        maf_ali:ScreenOrder = num#
                        If Access:MANFAULT_ALIAS.TryFetch(maf_ali:ScreenOrderKey) = Level:Benign

                        Else ! If Access:MANFAULT_ALIAS.TryFetch(maf_ali:ScreenOrderKey) = Level:Benign
                            Cycle
                        End ! If Access:MANFAULT_ALIAS.TryFetch(maf_ali:ScreenOrderKey) = Level:Benign

                        ! How many secondary values are there? (DBH: 05/11/2007)
                        Count# = 0
                        Access:MANFAURL.Clearkey(mnr:FieldKey)
                        mnr:MANFAULORecordNumber = mfo:RecordNumber
                        mnr:FieldNumber = maf_ali:Field_Number
                        Set(mnr:FieldKey,mnr:FieldKey)
                        Loop
                            If Access:MANFAURL.Next()
                                Break
                            End ! If Access:MANFAURL.Next()
                            If mnr:MANFAULORecordNumber <> mfo:RecordNumber
                                Break
                            End ! If mnr:MANFAULORecordNumber <> mfo:RecordNumber
                            If mnr:FieldNumber <> maf_ali:Field_Number
                                Break
                            End ! If mnr:FieldNmber <> maf_ali:Field_Number

                            local:LinkedRecordNumber = mnr:LinkedRecordNumber
                            Count# += 1
                            If Count# > 1
                                Break
                            End ! If Count# > 1
                        End ! Loop

!                        If Count# = 1
!                            ! Autofil (DBH: 05/11/2007)
!                            Access:MANFAULO_ALIAS.Clearkey(mfo_ali:RecordNumberKey)
!                            mfo_ali:RecordNumber = local:LinkedRecordNumber
!                            If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RecordNumberKey) = Level:Benign
!                                local:FaultCode = mfo_ali:Field
!                            End ! If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RecordNumberKey) = Level:Benign
!                        ELsif Count# > 1
!                            ! Blank, the user must select a new fault code (DBH: 05/11/2007)
!                            tmp:FaultCode[num#] = ''
!                            Display()
!                        End ! If Count# > 1
                    End ! Loop num# = 1 To 20

                End ! If locInFault <> mfo:Field

                locInFault = mfo:Field

                Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
                jbn:RefNumber   = job:Ref_Number
                If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                    ! Found
                    If maf:ReplicateFault = 'YES'
                        If jbn:Fault_Description = ''
                            jbn:Fault_Description = Clip(mfo:Description)
                        Else ! If jbn:Fault_Description = ''
                            If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                                jbn:Fault_Description = Clip(jbn:Fault_Description) & '<13,10>' & Clip(mfo:Description)
                            End ! If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                        End ! If jbn:Fault_Description = ''
                    End ! If maf:ReplicateFault = 'YES'

                    If maf:ReplicateInvoice = 'YES'
                        If jbn:Invoice_Text = ''
                            jbn:Invoice_Text = Clip(mfo:Description)
                        Else ! If jbn:Invoice_Text = ''
                            If ~Instring(Clip(mfo:Description),jbn:Invoice_Text,1,1)
                                jbn:Invoice_Text = Clip(jbn:Invoice_Text) & '<13,10>' & Clip(mfo:Description)
                            End ! If ~Instring(Clip(mfo:Description),jbn:Invoice_Text,1,1)
                        End ! If jbn:Invoice_Text = ''
                    End ! If maf:ReplicateInvoice = 'YES'
                    Access:JOBNOTES.Update()
                Else ! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                    ! Error
                End ! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
            End ! If GlobalResponse = RequestCompleted
            Exit
        End ! If malocInFault_Type = 'DATE'
    Else ! If Access:MANFAULT.Tryfetch(malocInFault_Number_Key) = Level:Benign
        ! Error
    End ! If Access:MANFAULT.Tryfetch(malocInFault_Number_Key) = Level:Benign


LookupRelatedPartFaultCodes       Routine
Data
local:FaultCodeValue            String(255)
Code
    Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
    wpr:Ref_Number = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.Next()
            Break
        End ! If Access:WARPARTS.Next()
        If wpr:Ref_Number <> job:Ref_Number
            Break
        End ! If wpr:Ref_Number <> job:Ref_Number
        Loop f# = 1 To 12
            Case f#
            Of 1
                If wpr:Fault_Code1 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code1
            Of 2
                If wpr:Fault_Code2 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code2
            Of 3
                If wpr:Fault_Code3 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code3
            Of 4
                If wpr:Fault_Code4 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code4
            Of 5
                If wpr:Fault_Code5 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code5
            Of 6
                If wpr:Fault_Code6 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code6
            Of 7
                If wpr:Fault_Code7 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code7
            Of 8
                If wpr:Fault_Code8 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code8
            Of 9
                If wpr:Fault_Code9 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code9
            Of 10
                If wpr:Fault_Code10 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code10
            Of 11
                If wpr:Fault_Code11 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code11
            Of 12
                If wpr:Fault_Code12 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code12
            End ! Case

            Access:MANFAUPA.Clearkey(map:Field_Number_Key)
            map:Manufacturer = job:Manufacturer
            map:Field_Number = f#
            If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
                ! Found
                If map:UseRelatedJobCode
                    Access:MANFAULT_ALIAS.Clearkey(maf_ali:MainFaultKey)
                    maf_ali:Manufacturer = job:Manufacturer
                    maf_ali:MainFault = 1
                    If Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign
                        ! Found
                        Access:MANFAULO_ALIAS.Clearkey(mfo_ali:RelatedFieldKey)
                        mfo_ali:Manufacturer = job:Manufacturer
                        mfo_ali:RelatedPartCode = map:Field_Number
                        mfo_ali:Field_Number = maf_ali:Field_Number
                        mfo_ali:Field = local:FaultCodeValue
                        If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RelatedFieldKey) = Level:Benign
                            ! Found
                            Found# = 0
                            Access:MANFAURL.Clearkey(mnr:FieldKey)
                            mnr:MANFAULORecordNumber = mfo_ali:RecordNumber
                            mnr:PartFaultCode = 0
                            mnr:FieldNumber = maf:Field_Number
                            Set(mnr:FieldKey,mnr:FieldKey)
                            Loop
                                If Access:MANFAURL.Next()
                                    Break
                                End ! If Access:MANFAURL.Next()
                                If mnr:MANFAULORecordNumber <> mfo_ali:RecordNumber
                                    Break
                                End ! If mnr:MANFAULORecordNumber <> mfo_ali:RecordNumber
                                If mnr:PartFaultCode <> 0
                                    Break
                                End ! If mnr:PartFaultCOde <> 1
                                If mnr:FieldNUmber <> maf:Field_Number
                                    Break
                                End ! If mnr:FIeldNUmber <> malocInFault_Number
                                If mnr:RelatedPartFaultCode > 0
                                    If mnr:RelatedPartFaultCode <> map:Field_Number
                                        Cycle
                                    End ! If mnr:RelatedPartFaultCode <> map_ali:Field_Number
                                End ! If mnr:RelaterdPartFaultCode > 0
                                Found# = 1
                                Break
                            End ! Loop (MANFAURL)
                            If Found# = 1
                                PassLinkedFaultCodeQueue.RecordNumber = mfo_ali:RecordNumber
                                PassLinkedFaultCodeQueue.FaultType = 'J'
                                Add(PassLinkedFaultCodeQueue)
                            End ! If Found# = 1

                        Else ! If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RelatedFieldKey) = Level:Benign
                        End ! If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RelatedFieldKey) = Level:Benign
                    Else ! If Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign
                    End ! If Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign
                Else ! If map:UseRelatedJobCode
                    Access:MANFPALO.Clearkey(mfp:Field_Key)
                    mfp:Manufacturer = job:Manufacturer
                    mfp:Field_Number = map:Field_Number
                    mfp:Field = local:FaultCodeValue
                    If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                        ! Found
                        Access:MANFPARL.Clearkey(mpr:FieldKey)
                        mpr:MANFPALORecordNumber = mfp:RecordNumber
                        mpr:JobFaultCode = 1
                        mpr:FieldNumber = maf:Field_Number
                        Set(mpr:FieldKey,mpr:FieldKey)
                        Loop
                            If Access:MANFPARL.Next()
                                Break
                            End ! If Access:MANFPARL.Next()
                            If mpr:MANFPALORecordNumber <> mfp:RecordNumber
                                Break
                            End ! If mpr:MANFPALORecordNumber <> mfp:RecordNumber
                            If mpr:JobFaultCode <> 1
                                Break
                            End ! If mpr:JobFaultCode <> 1
                            If mpr:FieldNumber <> maf:Field_Number
                                Break
                            End ! If mpr:FieldNumber <> malocInFault_Number

                            Found# = 1
                            Break
                        End ! Loop (MANFPARL)
                        If Found# = 1
                            PassLinkedFaultCodeQueue.RecordNumber = mfp:RecordNumber
                            PassLinkedFaultCodeQueue.FaultType = 'P'
                            Add(PassLinkedFaultCodeQueue)
                        End ! If Found# = 1
                    Else ! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                    End ! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                End ! If map:UseRelatedJobCode
            Else ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
            End ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        End ! Loop f# = 1 To 20

    End ! Loop (WARPARTS)

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020732'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('PUPValidation')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?OK,RequestCancelled)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:JOBS.Open
  Relate:MANFAULO_ALIAS.Open
  Relate:MANFAULT_ALIAS.Open
  Relate:MANFAUPA_ALIAS.Open
  Relate:MANFPALO_ALIAS.Open
  Relate:NETWORKS.Open
  Relate:USERS_ALIAS.Open
  Relate:WEBJOB.Open
  Access:MANFAULT.UseFile
  Access:MANFAUEX.UseFile
  Access:MANFAULO.UseFile
  Access:MANFAUPA.UseFile
  Access:MANFAURL.UseFile
  Access:MANFPALO.UseFile
  Access:MANFPARL.UseFile
  Access:MANUFACT.UseFile
  Access:JOBSE.UseFile
  Access:JOBS_ALIAS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Access:MANUFACT.Clearkey(man:Manufacturer_Key)
  man:Manufacturer    = job:Manufacturer
  if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
      ! Found
      if (man:Use_MSN = 'YES')
          ?locMSN{prop:Hide} = 0
          ?locMSN:Prompt{prop:Hide} = 0
      end ! if (man:Use_MSN = 'YES')
  else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
      ! Error
  end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
      ! Save Window Name
   AddToLog('Window','Open','PUPValidation')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?locNetwork{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?locNetwork{Prop:Tip}
  END
  IF ?locNetwork{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?locNetwork{Prop:Msg}
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:JOBS.Close
    Relate:MANFAULO_ALIAS.Close
    Relate:MANFAULT_ALIAS.Close
    Relate:MANFAUPA_ALIAS.Close
    Relate:MANFPALO_ALIAS.Close
    Relate:NETWORKS.Close
    Relate:USERS_ALIAS.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','PUPValidation')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickNetworks
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OK
      if (locIMEINumber = '')
          select(?locIMEINumber)
          cycle
      end !if (locIMEINumber = '')
      
      if (?locMSN{prop:hide} = 0 and locMSN = '')
          select(?locMSN)
          cycle
      end !if (?locMSN{prop:hide} = 0 and locMSN = '')
      
      if (locNetwork = '')
          select(?locNetwork)
          cycle
      end !if (locNetwork = '')
      
      if (locInFault = '')
          select(?locInFault)
          cycle
      end !if (locInFault = '')
      
      if (locIMEINumber <> fIMEINumber)
          IF (BouncerCheck(locIMEINumber,errorStr))
              BEEP(BEEP:SystemHand)  ;  YIELD()
              CASE Missive(CLIP(errorStr),'ServiceBase',|
                             'mstop.jpg','/&OK') 
              OF 1 ! &OK Button
              END!CASE MESSAGE
              CYCLE
          END ! IF
          Beep(Beep:SystemQuestion)  ;  Yield()
          Case Missive('IMEI number entered does not match IMEI from booking. Do you want to continue?','ServiceBase',|
                         'mquest.jpg','\&No|/&Yes')
              Of 2 ! &Yes Button
                  Access:USERS_ALIAS.Clearkey(use_ali:User_Code_Key)
                  use_ali:User_Code    = InsertEngineerPassword()
                  if (Access:USERS_ALIAS.TryFetch(use_ali:User_Code_Key) = Level:Benign)
                      ! Found
                      Access:ACCAREAS_ALIAS.Clearkey(acc_ali:access_level_key)
                      acc_ali:user_level    = use_ali:User_Level
                      acc_ali:access_area    = 'PUP RECEIPT - CHANGE IMEI'
                      if (Access:ACCAREAS_ALIAS.TryFetch(acc_ali:access_level_key) = Level:Benign)
                          ! Found
                      else ! if (Access:ACCAREAS_ALIAS.TryFetch(acc_ali:access_level_key) = Level:Benign)
                          ! Error
                          Beep(Beep:SystemHand)  ;  Yield()
                          Case Missive('You do not have access to change the IMEI Number.','ServiceBase',|
                                         'mstop.jpg','/&OK')
                              Of 1 ! &OK Button
                          End!Case Message
                          cycle
                      end ! if (Access:ACCAREAS_ALIAS.TryFetch(acc_ali:access_level_key) = Level:Benign)
                  else ! if (Access:USERS.TryFetch(use:Users_Code_Key) = Level:Benign)
                      ! Error
                  end ! if (Access:USERS.TryFetch(use:Users_Code_Key) = Level:Benign)
              Of 1 ! &No Button
                  cycle
          End!Case Message
      end ! if (locIMEINumber <> fIMEINumber)
      
      
      fIMEINumber = locIMEINumber
      fMSN = locMSN
      fNetwork = locNetwork
      fInFault = locInFault
      
      locReturn = 1
    OF ?Cancel
      Beep(Beep:SystemQuestion)  ;  Yield()
      Case Missive('Are you sure you want to cancel validation?','ServiceBase',|
                     'mquest.jpg','\&No|/&Yes')
          Of 2 ! &Yes Button
              locReturn = 0
          Of 1 ! &No Button
          cycle
      End!Case Message
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?locIMEINumber
      if (~0{prop:acceptall})
          If CheckLength('IMEI',job:Model_Number,locIMEINUmber)
              locIMEINumber = ''
              Display()
              Cycle
          End !job:ESN <> '*INVALID IMEI*'
      
          if (IsIMEIExchangeLoan(locIMEINumber,1))
              locIMEINumber = ''
              Display()
              select(?locIMEINumber)
              Cycle
          end !if (IsIMEIExchangeLoan(locIMEINumber,1))
      
          if (IsIMEIValid(locIMEINumber,job:Model_Number,1) = False)
              locIMEINumber = ''
              Display()
              Cycle
          end ! If IsIMEIValid(locIMEINumber,job:Model_Number,1) = False
      end !if (~0{prop:acceptall})
      
    OF ?locMSN
      if (~0{prop:acceptall})
          Error# = 0
          If CheckLength('MSN',job:model_number,locMSN)
              Select(?locMSN)
          Else!If error# = 1
              Error# = 0
              Access:MANUFACT.ClearKey(man:Manufacturer_Key)
              man:Manufacturer = job:Manufacturer
              If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                  !Found
                  If man:ApplyMSNFormat
                      If CheckFaultFormat(locMSN,man:MSNFormat)
                          Case Missive('M.S.N. failed format validation.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          Select(?locMSN)
      
                      End !If CheckFaultFormat(job:Msn,man:MSNFormat)
                  End !If man:ApplyMSNFormat
              Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
          End!If error# = 1
      end !if (~0{prop:acceptall})
    OF ?locNetwork
      IF locNetwork OR ?locNetwork{Prop:Req}
        net:Network = locNetwork
        !Save Lookup Field Incase Of error
        look:locNetwork        = locNetwork
        IF Access:NETWORKS.TryFetch(net:NetworkKey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            locNetwork = net:Network
          ELSE
            !Restore Lookup On Error
            locNetwork = look:locNetwork
            SELECT(?locNetwork)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      net:Network = locNetwork
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          locNetwork = net:Network
          Select(?+1)
      ELSE
          Select(?locNetwork)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?locNetwork)
    OF ?Button:LookupInFault
      ThisWindow.Update
      do LookupButtonCode
      display()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020732'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020732'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020732'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

BouncerCheck        PROCEDURE(STRING pIMEI, *STRING pError)!,LONG 
liveBouncer             LONG
bouncers                 LONG
RetValue                LONG(Level:Benign)
    CODE
        IF (pIMEI = '')
            RETURN Level:Benign
        END ! IF
        
        Access:JOBS_ALIAS.ClearKey(job_ali:ESN_Key)
        job_ali:ESN = pIMEI
        SET(job_ali:ESN_Key,job_ali:ESN_Key)
        LOOP UNTIL Access:JOBS_ALIAS.Next() <> Level:Benign
            IF (job_ali:ESN <> pIMEI)
                BREAK
            END ! IF
            IF (job_ali:Cancelled = 'YES')
                CYCLE
            END ! IF
            IF (job_ali:Date_Completed = '')
                liveBouncer += 1
            END ! IF
            bouncers += 1
        END ! LOOP

        SET(DEFAULTS,0)
        Access:DEFAULTS.Next()

        IF (def:Allow_Bouncer <> 'YES')
            IF (bouncers > 0)
                pError = 'IMEI Number has been previously entered within the default period.' 
                RetValue =  Level:Fatal
            END ! IF
        ELSE ! IF (def:Allow_Bouncer <> 'YES')
            IF (GETINI('BOUNCER','StopLive',,Clip(Path()) & '\SB2KDEF.INI') = 1)
                IF (liveBouncer > 0)
                    pError = 'IMEI Number already exists on a Live Job.'
                    RetValue =  Level:Fatal
                END ! IF (liveBouncer > 0)
            END ! IF (GETINI('BOUNCER','StopLive',,Clip(Path()) & '\SB2KDEF.INI') = 1)
        END ! IF (def:Allow_Bouncer <> 'YES')

        RETURN RetValue
        
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Browse_Contact_Hist2 PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
tmp:User             STRING(50)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(CONTHIST)
                       PROJECT(cht:Date)
                       PROJECT(cht:Time)
                       PROJECT(cht:Action)
                       PROJECT(cht:User)
                       PROJECT(cht:SN_StickyNote)
                       PROJECT(cht:Notes)
                       PROJECT(cht:Record_Number)
                       PROJECT(cht:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
cht:Date               LIKE(cht:Date)                 !List box control field - type derived from field
cht:Date_NormalFG      LONG                           !Normal forground color
cht:Date_NormalBG      LONG                           !Normal background color
cht:Date_SelectedFG    LONG                           !Selected forground color
cht:Date_SelectedBG    LONG                           !Selected background color
cht:Time               LIKE(cht:Time)                 !List box control field - type derived from field
cht:Time_NormalFG      LONG                           !Normal forground color
cht:Time_NormalBG      LONG                           !Normal background color
cht:Time_SelectedFG    LONG                           !Selected forground color
cht:Time_SelectedBG    LONG                           !Selected background color
tmp:User               LIKE(tmp:User)                 !List box control field - type derived from local data
tmp:User_NormalFG      LONG                           !Normal forground color
tmp:User_NormalBG      LONG                           !Normal background color
tmp:User_SelectedFG    LONG                           !Selected forground color
tmp:User_SelectedBG    LONG                           !Selected background color
cht:Action             LIKE(cht:Action)               !List box control field - type derived from field
cht:Action_NormalFG    LONG                           !Normal forground color
cht:Action_NormalBG    LONG                           !Normal background color
cht:Action_SelectedFG  LONG                           !Selected forground color
cht:Action_SelectedBG  LONG                           !Selected background color
cht:User               LIKE(cht:User)                 !List box control field - type derived from field
cht:User_NormalFG      LONG                           !Normal forground color
cht:User_NormalBG      LONG                           !Normal background color
cht:User_SelectedFG    LONG                           !Selected forground color
cht:User_SelectedBG    LONG                           !Selected background color
cht:SN_StickyNote      LIKE(cht:SN_StickyNote)        !List box control field - type derived from field
cht:Notes              LIKE(cht:Notes)                !Browse hot field - type derived from field
cht:Record_Number      LIKE(cht:Record_Number)        !Primary key field - type derived from field
cht:Ref_Number         LIKE(cht:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Contact History File'),AT(60,32,560,364),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(4,8,520,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(528,4),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(456,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Contact History File'),AT(8,10),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,332,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(84,50,344,190),USE(?Browse:1),IMM,HVSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('50R(2)|M*~Date~L@d6b@26R(2)|M*~Time~@t1@130L(2)|M*~User~@s50@155L(2)|M*~Action~@' &|
   's80@37L(2)|M*~Usercode~@s3@0L(2)|M@s1@'),FROM(Queue:Browse:1)
                       PROMPT('Notes'),AT(168,266),USE(?AUD:Notes:Prompt),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(476,68),USE(?ButtonSticky),FLAT,ICON('StickyNp.jpg')
                       BUTTON,AT(480,134),USE(?Insert),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       TEXT,AT(84,246,344,76),USE(cht:Notes),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                       BUTTON,AT(480,162),USE(?Change),TRN,FLAT,LEFT,ICON('editp.jpg')
                       BUTTON,AT(480,192),USE(?Delete),TRN,FLAT,HIDE,LEFT
                       SHEET,AT(4,28,552,302),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Date'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('By Action'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('By User'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(480,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020786'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Contact_Hist2')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:CONTHIST.Open
  Access:JOBSE2.UseFile
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:CONTHIST,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
!  If glo:WebJob = 1 Or (glo:WebJob = 0 And jobe:WebJob = 0)
!      If Sub(job:Current_Status,1,3) > '704' And Sub(job:Current_Status,1,3) < '901'
!          Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
!          jobe2:RefNumber = job:Ref_Number
!          If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
!              ! Found
!              If jobe2:SMSNotification
!                  ?Button:ResendSMS{prop:Hide} = False
!              End ! If jobe2:SMSNotification
!              If jobe2:EmailNotification
!                  ?Button:ResendEmail{prop:Hide} = False
!              End ! If jobe2:EmailNotification
!          Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
!              ! Error
!          End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
!      End ! If Sub(job:Current_Status,1,3) > '704' And Sub(job:Current_Status,1,3) < '901'
!  End ! If glo:WebJob = 1 Or (glo:WebJob = 0 And jobe:WebJob = 0)
  
  
  If SecurityCheck('STICKY NOTES') = level:benign then
      Unhide(?ButtonSticky)
  ELSE
      hide(?ButtonSticky)
  End ! If SecurityCheck('AMEND RRC PART')
      ! Save Window Name
   AddToLog('Window','Open','Browse_Contact_Hist2')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,cht:Action_Key)
  BRW1.AddRange(cht:Ref_Number,GLO:Select12)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,cht:Action,1,BRW1)
  BRW1.AddSortOrder(,cht:User_Key)
  BRW1.AddRange(cht:Ref_Number,GLO:Select12)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(,cht:User,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha+ScrollSort:Descending)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,cht:Ref_Number_Key)
  BRW1.AddRange(cht:Ref_Number,GLO:Select12)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,cht:Date,1,BRW1)
  BIND('tmp:User',tmp:User)
  BRW1.AddField(cht:Date,BRW1.Q.cht:Date)
  BRW1.AddField(cht:Time,BRW1.Q.cht:Time)
  BRW1.AddField(tmp:User,BRW1.Q.tmp:User)
  BRW1.AddField(cht:Action,BRW1.Q.cht:Action)
  BRW1.AddField(cht:User,BRW1.Q.cht:User)
  BRW1.AddField(cht:SN_StickyNote,BRW1.Q.cht:SN_StickyNote)
  BRW1.AddField(cht:Notes,BRW1.Q.cht:Notes)
  BRW1.AddField(cht:Record_Number,BRW1.Q.cht:Record_Number)
  BRW1.AddField(cht:Ref_Number,BRW1.Q.cht:Ref_Number)
  QuickWindow{PROP:MinWidth}=529
  QuickWindow{PROP:MinHeight}=272
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Date'
    ?Tab2{PROP:TEXT} = 'By Action'
    ?Tab3{PROP:TEXT} = 'By User'
    ?Browse:1{PROP:FORMAT} ='50R(2)|M*~Date~L@d6b@#1#26R(2)|M*~Time~@t1@#6#130L(2)|M*~User~@s50@#11#155L(2)|M*~Action~@s80@#16#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CONTHIST.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Contact_Hist2')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
    case request
        of insertrecord
            check_access('CONTACT HISTORY - INSERT',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                do_update# = false
            end
        of changerecord
            check_access('CONTACT HISTORY - CHANGE',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                do_update# = false
            end
        of deleterecord
            check_access('CONTACT HISTORY - DELETE',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                do_update# = false
            end
    end !case request
  
    if do_update# = true
  
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Contact_History
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonSticky
      GLO:Select1 = 'STICKY'
      Post(Event:Accepted,?Insert)
      
      
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020786'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020786'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020786'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='50R(2)|M*~Date~L@d6b@#1#26R(2)|M*~Time~@t1@#6#130L(2)|M*~User~@s50@#11#155L(2)|M*~Action~@s80@#16#'
          ?Tab:2{PROP:TEXT} = 'By Date'
        OF 2
          ?Browse:1{PROP:FORMAT} ='155L(2)|M*~Action~@s80@#16#50R(2)|M*~Date~L@d6b@#1#26R(2)|M*~Time~@t1@#6#130L(2)|M*~User~@s50@#11#'
          ?Tab2{PROP:TEXT} = 'By Action'
        OF 3
          ?Browse:1{PROP:FORMAT} ='37L(2)|M*~Usercode~@s3@#21#50R(2)|M*~Date~L@d6b@#1#26R(2)|M*~Time~@t1@#6#130L(2)|M*~User~@s50@#11#155L(2)|M*~Action~@s80@#16#'
          ?Tab3{PROP:TEXT} = 'By User'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  ! Inserting (DBH 18/05/2006) #7597 - Display system or full user name
  If cht:User = '&SB'
      tmp:User = '* SYSTEM *'
  Else ! If cht:User = '&SB'
      Access:USERS.Clearkey(use:User_Code_Key)
      use:User_Code   = cht:User
      If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          ! Found
          tmp:User = CLip(use:Forename) & ' ' & Clip(use:Surname)
      Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          ! Error
          tmp:USer = '* ERROR - USER CODE: ' * Clip(cht:User) & ' *'
      End ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
  End ! If cht:User = '&SB'
  ! End (DBH 18/05/2006) #7597
  PARENT.SetQueueRecord
  IF (cht:SN_StickyNote = 'Y')
    SELF.Q.cht:Date_NormalFG = 255
    SELF.Q.cht:Date_NormalBG = 16777215
    SELF.Q.cht:Date_SelectedFG = 16777215
    SELF.Q.cht:Date_SelectedBG = 255
  ELSE
    SELF.Q.cht:Date_NormalFG = -1
    SELF.Q.cht:Date_NormalBG = -1
    SELF.Q.cht:Date_SelectedFG = -1
    SELF.Q.cht:Date_SelectedBG = -1
  END
  IF (cht:SN_StickyNote = 'Y')
    SELF.Q.cht:Time_NormalFG = 255
    SELF.Q.cht:Time_NormalBG = 16777215
    SELF.Q.cht:Time_SelectedFG = 16777215
    SELF.Q.cht:Time_SelectedBG = 255
  ELSE
    SELF.Q.cht:Time_NormalFG = -1
    SELF.Q.cht:Time_NormalBG = -1
    SELF.Q.cht:Time_SelectedFG = -1
    SELF.Q.cht:Time_SelectedBG = -1
  END
  IF (cht:SN_StickyNote = 'Y')
    SELF.Q.tmp:User_NormalFG = 255
    SELF.Q.tmp:User_NormalBG = 16777215
    SELF.Q.tmp:User_SelectedFG = 16777215
    SELF.Q.tmp:User_SelectedBG = 255
  ELSE
    SELF.Q.tmp:User_NormalFG = -1
    SELF.Q.tmp:User_NormalBG = -1
    SELF.Q.tmp:User_SelectedFG = -1
    SELF.Q.tmp:User_SelectedBG = -1
  END
  IF (cht:SN_StickyNote = 'Y')
    SELF.Q.cht:Action_NormalFG = 255
    SELF.Q.cht:Action_NormalBG = 16777215
    SELF.Q.cht:Action_SelectedFG = 16777215
    SELF.Q.cht:Action_SelectedBG = 255
  ELSE
    SELF.Q.cht:Action_NormalFG = -1
    SELF.Q.cht:Action_NormalBG = -1
    SELF.Q.cht:Action_SelectedFG = -1
    SELF.Q.cht:Action_SelectedBG = -1
  END
  IF (cht:SN_StickyNote = 'Y')
    SELF.Q.cht:User_NormalFG = 255
    SELF.Q.cht:User_NormalBG = 16777215
    SELF.Q.cht:User_SelectedFG = 16777215
    SELF.Q.cht:User_SelectedBG = 255
  ELSE
    SELF.Q.cht:User_NormalFG = -1
    SELF.Q.cht:User_NormalBG = -1
    SELF.Q.cht:User_SelectedFG = -1
    SELF.Q.cht:User_SelectedBG = -1
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?AUD:Notes:Prompt, Resize:FixRight+Resize:FixTop, Resize:LockSize)

EstimatePartsInUse   PROCEDURE  ()                    ! Declare Procedure
save_epr_id          USHORT,AUTO
StockError           BYTE
  CODE
    !TB13024 - before allocating any stock check to ensure stock is clear to handle this   JC 12/03/13

    save_epr_id = access:estparts.savefile()
    StockError = false

    access:estparts.clearkey(epr:part_number_key)
    epr:ref_number  = job:ref_number
    set(epr:part_number_key,epr:part_number_key)
    loop

        if access:estparts.next() then break.
        if epr:ref_number  <> job:ref_number then break.

        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if

        Access:STOCK.ClearKey(sto:ref_number_key)
        sto:ref_number = epr:Part_Ref_Number
        if Access:STOCK.Fetch(sto:ref_number_key)
            !error 1 - stock not found
                StockError = 1
                Break

        ELSE
            !Record in Use?
            Pointer# = Pointer(Stock)
            Hold(Stock,1)
            Get(Stock,Pointer#)
            If Errorcode() = 43
                !error 2
                StockError = 2
                Break
            End !If Errorcode() = 43
            Release(Stock)
        END !if stock fetched OK
    END !loop through estparts to check if stock is OK

    access:estparts.restorefile(save_epr_id)
    Return(StockError)
SelectSNCode PROCEDURE (SentString)                   !Generated from procedure template - Window

ReturnString         STRING(30)
SelectionType        STRING(30)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(649,4,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(165,68,352,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Select Specifice Needs Type'),AT(169,70),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(465,70),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(165,84,352,244),USE(?Panel3),FILL(09A6A7CH)
                       PROMPT('Please select one of the following:'),AT(180,162),USE(?Prompt3),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       OPTION,AT(284,184,152,82),USE(SelectionType)
                         RADIO(' HEARING IMPAIRED'),AT(316,196),USE(?SelectionType:Radio1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('HEARING IMPAIRED')
                         RADIO('VISUAL IMPAIRMENT'),AT(316,216),USE(?SelectionType:Radio2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('VISUAL IMPAIRMENT')
                         RADIO('ELDERLY'),AT(316,236),USE(?SelectionType:Radio3),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('ELDERLY')
                       END
                       PANEL,AT(165,332,352,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                       BUTTON,AT(376,332),USE(?ButtonOK),FLAT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?ButtonCancel),FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(ReturnString)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020802'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SelectSNCode')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ReturnString = SentString
  SelectionType = sentString
      ! Save Window Name
   AddToLog('Window','Open','SelectSNCode')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','SelectSNCode')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonOK
      IF CLIP(SelectionType) = '' THEN CYCLE.
      ReturnString = SelectionType
    OF ?ButtonCancel
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020802'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020802'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020802'&'0')
      ***
    OF ?ButtonOK
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
