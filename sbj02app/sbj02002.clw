

   MEMBER('sbj02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ02002.INC'),ONCE        !Local module procedure declarations
                     END


UpdateJobAccessoryNumber PROCEDURE                    !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::joa:Record  LIKE(joa:RECORD),STATIC
QuickWindow          WINDOW('Update the JOBACCNO File'),AT(,,208,84),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateJobAccessoryNumber'),SYSTEM,GRAY,RESIZE,MDI
                       SHEET,AT(4,4,200,58),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number'),AT(8,20),USE(?joa:RecordNumber:Prompt),TRN
                           ENTRY(@s8),AT(76,20,40,10),USE(joa:RecordNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Record Number'),TIP('Record Number'),UPR
                           PROMPT('Ref Number'),AT(8,34),USE(?joa:RefNumber:Prompt),TRN
                           ENTRY(@s8),AT(76,34,40,10),USE(joa:RefNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Ref Number'),TIP('Ref Number'),UPR
                           PROMPT('Accessory Number'),AT(8,48),USE(?joa:AccessoryNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(76,48,124,10),USE(joa:AccessoryNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Accessory Number'),TIP('Accessory Number'),UPR
                         END
                       END
                       BUTTON('OK'),AT(110,66,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(159,66,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(159,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateJobAccessoryNumber')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?joa:RecordNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(joa:Record,History::joa:Record)
  SELF.AddHistoryField(?joa:RecordNumber,1)
  SELF.AddHistoryField(?joa:RefNumber,2)
  SELF.AddHistoryField(?joa:AccessoryNumber,3)
  SELF.AddUpdateFile(Access:JOBACCNO)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBACCNO.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBACCNO
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','UpdateJobAccessoryNumber')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBACCNO.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateJobAccessoryNumber')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Job_Accessories PROCEDURE                      !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::8:TAGDISPSTATUS    BYTE(0)
DASBRW::8:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::10:TAGDISPSTATUS   BYTE(0)
DASBRW::10:QUEUE          QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
accessory_tag_temp   STRING(1)
accessory_tag2_temp  STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:blank            STRING(20)
tmp:AccessoryNumber  STRING(30)
BRW1::View:Browse    VIEW(JOBACC)
                       PROJECT(jac:Accessory)
                       PROJECT(jac:Damaged)
                       PROJECT(jac:Pirate)
                       PROJECT(jac:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
accessory_tag_temp     LIKE(accessory_tag_temp)       !List box control field - type derived from local data
accessory_tag_temp_Icon LONG                          !Entry's icon ID
jac:Accessory          LIKE(jac:Accessory)            !List box control field - type derived from field
jac:Damaged            LIKE(jac:Damaged)              !List box control field - type derived from field
jac:Damaged_Icon       LONG                           !Entry's icon ID
jac:Pirate             LIKE(jac:Pirate)               !List box control field - type derived from field
jac:Pirate_Icon        LONG                           !Entry's icon ID
tmp:blank              LIKE(tmp:blank)                !List box control field - type derived from local data
jac:Ref_Number         LIKE(jac:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW9::View:Browse    VIEW(ACCESSOR)
                       PROJECT(acr:Accessory)
                       PROJECT(acr:Model_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
accessory_tag2_temp    LIKE(accessory_tag2_temp)      !List box control field - type derived from local data
accessory_tag2_temp_Icon LONG                         !Entry's icon ID
acr:Accessory          LIKE(acr:Accessory)            !List box control field - type derived from field
acr:Model_Number       LIKE(acr:Model_Number)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(JOBACCNO)
                       PROJECT(joa:AccessoryNumber)
                       PROJECT(joa:RecordNumber)
                       PROJECT(joa:RefNumber)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:2
joa:AccessoryNumber    LIKE(joa:AccessoryNumber)      !List box control field - type derived from field
joa:RecordNumber       LIKE(joa:RecordNumber)         !Primary key field - type derived from field
joa:RefNumber          LIKE(joa:RefNumber)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse The Loan Accessory File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse / Amend Job Accessories'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,204,232),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('All Available Accessories'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(68,70,124,10),USE(acr:Accessory),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           LIST,AT(68,86,124,196),USE(?List),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('10L(2)I@s1@120L(2)~Accessory~@s30@'),FROM(Queue:Browse)
                           BUTTON('sho&W tags'),AT(132,118,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON('&Rev tags'),AT(140,150,50,13),USE(?DASREVTAG),HIDE
                           BUTTON,AT(200,140),USE(?DASTAGAll:2),SKIP,TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(200,176),USE(?DASUNTAGALL:2),SKIP,TRN,FLAT,LEFT,ICON('untagalp.jpg')
                           BUTTON,AT(200,106),USE(?DASTAG:2),SKIP,TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                         END
                       END
                       PANEL,AT(272,54,72,232),USE(?Panel1),FILL(09A6A7CH)
                       GROUP('Key'),AT(276,186,64,38),USE(?Group1),SKIP,BOXED,FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         PROMPT('D- Damaged'),AT(284,196),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         PROMPT('P - Pirate'),AT(288,210),USE(?Prompt1:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       END
                       BUTTON,AT(276,138),USE(?AddAccessory),SKIP,TRN,FLAT,RIGHT,ICON('addtagp.jpg')
                       BUTTON,AT(276,254),USE(?RemoveAccessory),TRN,FLAT,LEFT,ICON('removeap.jpg')
                       SHEET,AT(348,54,268,232),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('Accessory On Job'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(352,70,124,10),USE(jac:Accessory),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           LIST,AT(352,86,168,196),USE(?Browse:1),IMM,FONT('Tahoma',8,,FONT:regular),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('10L(2)I@s1@133L(2)|~Accessory~@s30@11L(2)|I~D~@n1@11L(2)|I~P~@n1@80L(2)@s20@E(0F' &|
   'FFFFFH,0FFFFFFH,0FFFFFFH,0FFFFFFH)'),FROM(Queue:Browse:1)
                           BUTTON('&Rev tags'),AT(424,98,50,13),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(412,138,70,13),USE(?DASSHOWTAG:2),HIDE
                           BUTTON,AT(524,86),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(524,116),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(524,146),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                           BUTTON,AT(524,204),USE(?AccessoryDamaged),TRN,FLAT,LEFT,ICON('accdamp.jpg')
                           BUTTON,AT(524,176),USE(?AccessoryPirated),TRN,FLAT,LEFT,ICON('accpirp.jpg')
                         END
                       END
                       SHEET,AT(64,288,552,74),USE(?Sheet3),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Additonal Notes'),USE(?Tab3)
                           PROMPT('Additional Notes'),AT(68,293),USE(?Prompt4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(68,304,168,54),USE(jobe:AccessoryNotes),VSCROLL,LEFT,FONT(,8,010101H,FONT:bold),COLOR(COLOR:White),MSG('Accessory Notes'),TIP('Accessory Notes'),UPR
                           LIST,AT(452,292,160,66),USE(?List:2),IMM,VSCROLL,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('120L(2)|M~Accessory Number~@s30@'),FROM(Queue:Browse:2)
                           PROMPT('Accessory Number'),AT(352,295),USE(?tmp:AccessoryNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(352,306,96,10),USE(tmp:AccessoryNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Accessory Number'),TIP('Accessory Number'),UPR
                           BUTTON,AT(384,330),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                         END
                       END
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
BRW9                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW9::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW4                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::8:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   GLO:Queue.Pointer = jac:Accessory
   GET(GLO:Queue,GLO:Queue.Pointer)
  IF ERRORCODE()
     GLO:Queue.Pointer = jac:Accessory
     ADD(GLO:Queue,GLO:Queue.Pointer)
    accessory_tag_temp = '*'
  ELSE
    DELETE(GLO:Queue)
    accessory_tag_temp = ''
  END
    Queue:Browse:1.accessory_tag_temp = accessory_tag_temp
  IF (accessory_tag_temp = '*')
    Queue:Browse:1.accessory_tag_temp_Icon = 4
  ELSE
    Queue:Browse:1.accessory_tag_temp_Icon = 3
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::8:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(GLO:Queue)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue.Pointer = jac:Accessory
     ADD(GLO:Queue,GLO:Queue.Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::8:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::8:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::8:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue)
    GET(GLO:Queue,QR#)
    DASBRW::8:QUEUE = GLO:Queue
    ADD(DASBRW::8:QUEUE)
  END
  FREE(GLO:Queue)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::8:QUEUE.Pointer = jac:Accessory
     GET(DASBRW::8:QUEUE,DASBRW::8:QUEUE.Pointer)
    IF ERRORCODE()
       GLO:Queue.Pointer = jac:Accessory
       ADD(GLO:Queue,GLO:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::8:DASSHOWTAG Routine
   CASE DASBRW::8:TAGDISPSTATUS
   OF 0
      DASBRW::8:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::8:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::8:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::10:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW9.UpdateBuffer
   GLO:Queue2.Pointer2 = acr:Accessory
   GET(GLO:Queue2,GLO:Queue2.Pointer2)
  IF ERRORCODE()
     GLO:Queue2.Pointer2 = acr:Accessory
     ADD(GLO:Queue2,GLO:Queue2.Pointer2)
    accessory_tag2_temp = '*'
  ELSE
    DELETE(GLO:Queue2)
    accessory_tag2_temp = ''
  END
    Queue:Browse.accessory_tag2_temp = accessory_tag2_temp
  IF (accessory_tag2_temp = '*')
    Queue:Browse.accessory_tag2_temp_Icon = 2
  ELSE
    Queue:Browse.accessory_tag2_temp_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::10:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW9.Reset
  FREE(GLO:Queue2)
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue2.Pointer2 = acr:Accessory
     ADD(GLO:Queue2,GLO:Queue2.Pointer2)
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::10:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue2)
  BRW9.Reset
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::10:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::10:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue2)
    GET(GLO:Queue2,QR#)
    DASBRW::10:QUEUE = GLO:Queue2
    ADD(DASBRW::10:QUEUE)
  END
  FREE(GLO:Queue2)
  BRW9.Reset
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::10:QUEUE.Pointer2 = acr:Accessory
     GET(DASBRW::10:QUEUE,DASBRW::10:QUEUE.Pointer2)
    IF ERRORCODE()
       GLO:Queue2.Pointer2 = acr:Accessory
       ADD(GLO:Queue2,GLO:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::10:DASSHOWTAG Routine
   CASE DASBRW::10:TAGDISPSTATUS
   OF 0
      DASBRW::10:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::10:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::10:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW9.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020432'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Job_Accessories')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCAREAS_ALIAS.Open
  Relate:ACCESSOR.Open
  Relate:LOANACC.Open
  Relate:USERS_ALIAS.Open
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:JOBACC,SELF)
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:ACCESSOR,SELF)
  BRW4.Init(?List:2,Queue:Browse:2.ViewPosition,BRW4::View:Browse,Queue:Browse:2,Relate:JOBACCNO,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !View Only?
  If glo:Preview = 'V'
      ?AddAccessory{prop:Disable} = 1
      ?RemoveAccessory{prop:Disable} = 1
      ?AccessoryDamaged{prop:Disable} = 1
      ?AccessoryPirated{prop:Disable} = 1
  End !glo:Preview = 'V'
  
  Access:JOBSE.Clearkey(jobe:RefNumberKey)
  jobe:RefNumber  = glo:Select2
  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Found
  
  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Error
  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      ! Save Window Name
   AddToLog('Window','Open','Browse_Job_Accessories')
  ?List{prop:vcr} = TRUE
  ?Browse:1{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,jac:Ref_Number_Key)
  BRW1.AddRange(jac:Ref_Number,GLO:Select2)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?jac:Accessory,jac:Accessory,1,BRW1)
  BIND('accessory_tag_temp',accessory_tag_temp)
  BIND('tmp:blank',tmp:blank)
  BIND('GLO:Select2',GLO:Select2)
  ?Browse:1{PROP:IconList,1} = '~bluetick.ico'
  ?Browse:1{PROP:IconList,2} = '~grentick.ico'
  ?Browse:1{PROP:IconList,3} = '~notick1.ico'
  ?Browse:1{PROP:IconList,4} = '~tick1.ico'
  BRW1.AddField(accessory_tag_temp,BRW1.Q.accessory_tag_temp)
  BRW1.AddField(jac:Accessory,BRW1.Q.jac:Accessory)
  BRW1.AddField(jac:Damaged,BRW1.Q.jac:Damaged)
  BRW1.AddField(jac:Pirate,BRW1.Q.jac:Pirate)
  BRW1.AddField(tmp:blank,BRW1.Q.tmp:blank)
  BRW1.AddField(jac:Ref_Number,BRW1.Q.jac:Ref_Number)
  BRW9.Q &= Queue:Browse
  BRW9.AddSortOrder(,acr:Accesory_Key)
  BRW9.AddRange(acr:Model_Number,GLO:Select1)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(?acr:Accessory,acr:Accessory,1,BRW9)
  BIND('accessory_tag2_temp',accessory_tag2_temp)
  BIND('GLO:Select1',GLO:Select1)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW9.AddField(accessory_tag2_temp,BRW9.Q.accessory_tag2_temp)
  BRW9.AddField(acr:Accessory,BRW9.Q.acr:Accessory)
  BRW9.AddField(acr:Model_Number,BRW9.Q.acr:Model_Number)
  BRW4.Q &= Queue:Browse:2
  BRW4.AddSortOrder(,joa:AccessoryNumberKey)
  BRW4.AddRange(joa:RefNumber,job:Ref_Number)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,joa:AccessoryNumber,1,BRW4)
  BRW4.AddField(joa:AccessoryNumber,BRW4.Q.joa:AccessoryNumber)
  BRW4.AddField(joa:RecordNumber,BRW4.Q.joa:RecordNumber)
  BRW4.AddField(joa:RefNumber,BRW4.Q.joa:RefNumber)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW4.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  BRW4.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW4.AskProcedure = 0
      CLEAR(BRW4.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:ACCAREAS_ALIAS.Close
    Relate:ACCESSOR.Close
    Relate:LOANACC.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Job_Accessories')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = True

    case request
        of insertrecord
            If SecurityCheck('ACCESSORIES - INSERT')
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                do_update# = false
                Access:ACCESSOR.Cancelautoinc()
            end!If SecurityCheck('ACCESSORIES - INSERT',x")
        of changerecord
            If SecurityCheck('ACCESSORIES - CHANGE')
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive

                do_update# = false
            end!If SecurityCheck('ACCESSORIES - CHANGE',x")
        of deleterecord
            If SecurityCheck('ACCESSORIES - DELETE')
                BEEP(BEEP:SystemHand)  ;  YIELD()
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive

                do_update# = false
            end!If SecurityCheck('ACCESSORIES - DELETE',x")
    end !case request

    If do_update# = True
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateJobAccessoryNumber
    ReturnValue = GlobalResponse
  END
  End!If do_update# = True
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020432'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020432'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020432'&'0')
      ***
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?AddAccessory
      ThisWindow.Update
      If SecurityCheck('JOBS - AMEND ACCESSORIES')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!If SecurityCheck('JOBS - AMEND ACCESSORIES')
          If Records(glo:Queue2) <> ''
              Loop x# = 1 To Records(glo:Queue2)
                  Get(glo:Queue2,x#)
                  If Access:JOBACC.PrimeRecord() = Level:Benign
                      jac:ref_number  = glo:select2
                      jac:accessory   = glo:pointer2
                      !If the accessories are being attached at the ARC
                      !then mark them as "sent to ARC" - 4285 (DBH: 26-05-2004)
                      If glo:WebJob = True
                          jac:Attached = False
                      Else !If glo:WebJob = True
                          jac:Attached = True
                      End !If glo:WebJob = True
                      If Access:JOBACC.TryInsert() = Level:Benign
                          !Insert Successful
      
                      Else !If Access:JOBACC.TryInsert() = Level:Benign
                          !Insert Failed
                      End !If Access:JOBACC.TryInsert() = Level:Benign
                  End !If Access:JOBACC.PrimeRecord() = Level:Benign
              End
              BRW1.ResetSort(1)
          End
      End!If SecurityCheck('JOBS - AMEND ACCESSORIES')
    OF ?RemoveAccessory
      ThisWindow.Update
      If SecurityCheck('JOBS - AMEND ACCESSORIES')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!If SecurityCheck('JOBS - AMEND ACCESSORIES')
          If Records(glo:Queue) <> ''
              Loop x# = 1 To Records(glo:Queue)
                  Get(glo:Queue,x#)
                  access:jobacc.clearkey(jac:ref_number_key)
                  jac:ref_number  = glo:select2
                  jac:accessory   = glo:pointer
                  if access:jobacc.fetch(jac:ref_number_key) = Level:Benign
                      Delete(jobacc)
                  end
              End
              BRW1.ResetSort(1)
          End
      End!!If SecurityCheck('JOBS - AMEND ACCESSORIES')
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?AccessoryDamaged
      ThisWindow.Update
      Access:JOBACC.ClearKey(jac:Ref_Number_Key)
      jac:Ref_Number = glo:Select2
      jac:Accessory  = brw1.q.jac:Accessory
      If Access:JOBACC.TryFetch(jac:Ref_Number_Key) = Level:Benign
          !Found
          If jac:Damaged
              Case Missive('The selected accessory is already marked as damaged. Do you wish to un-mark it?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      jac:Damaged = 0
                  Of 1 ! No Button
              End ! Case Missive
          Else !jac:Damaged
              Case Missive('Do you wish to mark the selected accessory as Damaged?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      jac:Damaged = 1
                  Of 1 ! No Button
              End ! Case Missive
          End !jac:Damaged
          Access:JOBACC.Update()
      Else!If Access:JOBACC.TryFetch(jac:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:JOBACC.TryFetch(jac:Ref_Number_Key) = Level:Benign
      BRW1.ResetSort(1)
    OF ?AccessoryPirated
      ThisWindow.Update
      Access:JOBACC.ClearKey(jac:Ref_Number_Key)
      jac:Ref_Number = glo:Select2
      jac:Accessory  = brw1.q.jac:Accessory
      If Access:JOBACC.TryFetch(jac:Ref_Number_Key) = Level:Benign
          !Found
          If jac:Pirate
              Case Missive('The selected accessory is already marked as Pirated. Do you wish to un-mark it?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      jac:Pirate = 0
                  Of 1 ! No Button
              End ! Case Missive
          Else !jac:Damaged
              Case Missive('Do you wish to mark the selected accessory as Pirated?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      jac:Pirate = 1
                  Of 1 ! No Button
              End ! Case Missive
          End !jac:Damaged
          Access:JOBACC.Update()
      Else!If Access:JOBACC.TryFetch(jac:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:JOBACC.TryFetch(jac:Ref_Number_Key) = Level:Benign
      BRW1.ResetSort(1)
    OF ?tmp:AccessoryNumber
      If Access:JOBACCNO.PrimeRecord() = Level:Benign
          joa:RefNumber   = job:Ref_Number
          joa:AccessoryNumber = tmp:AccessoryNumber
          If Access:JOBACCNO.Insert() = Level:Benign
              !Insert Successful
      
          Else !If Access:JOBACCNO.TryInsert() = Level:Benign
              !Insert Failed
              Access:JOBACCNO.CancelAutoInc()
          End !If Access:JOBACCNO.TryInsert() = Level:Benign
      End !If Access:JOBACCNO.PrimeRecord() = Level:Benign
      BRW4.ResetSort(1)
      tmp:AccessoryNumber = ''
      Select(?tmp:AccessoryNumber)
    OF ?Close
      ThisWindow.Update
      jobe:AccessoryNotes = Upper(jobe:AccessoryNotes)
      Access:JOBSE.TryUpdate()
      
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  jobe:AccessoryNotes = Upper(jobe:AccessoryNotes)
  Access:JOBSE.TryUpdate()
  ReturnValue = PARENT.TakeCompleted()
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      !To prevent tick boxes being ticked when
      !scrolling - 3356 (DBH: 06-10-2003)
      If KeyCode() = MouseLeft2
          Post(Event:Accepted,?DasTag:2)
      End !KeyCode() = MouseLeft2
    END
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      !To prevent tick boxes being ticked when
      !scrolling - 3356 (DBH: 06-10-2003)
      If KeyCode() = MouseLeft2
          Post(Event:Accepted,?DasTag)
      End !KeyCode() = MouseLeft2
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?CurrentTab
        SELECT(?Browse:1)                   ! Reselect list box after tab change
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?acr:Accessory
      Select(?Browse:1)
    OF ?jac:Accessory
      Select(?List)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      Case KeyCode()
          Of F10Key
              Post(Event:Accepted,?AccessoryPirated)
          Of F5Key
              Post(Event:Accepted,?Dastag:2)
          Of F6Key
              Post(Event:Accepted,?DastagAll:2)
          Of F7Key
              Post(Event:Accepted,?Dasuntagall:2)
          Of F8Key
              Post(Event:Accepted,?AddAccessory)
          Of F9Key
              Post(Event:Accepted,?AccessoryDamaged)
      !    Of F10Key
      !        Post(Event:Accepted,?Close)
      
      End !KeyCode()
    OF EVENT:OpenWindow
      ?WindowTitle{Prop:text} = 'Browse /Amend Accessories For Job: ' & glo:select2
      DO DASBRW::8:DASUNTAGALL
      DO DASBRW::10:DASUNTAGALL
    OF EVENT:GainFocus
      BRW1.ResetSort(1)
      BRW9.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = jac:Accessory
     GET(GLO:Queue,GLO:Queue.Pointer)
    IF ERRORCODE()
      accessory_tag_temp = ''
    ELSE
      accessory_tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (accessory_tag_temp = '*')
    SELF.Q.accessory_tag_temp_Icon = 4
  ELSE
    SELF.Q.accessory_tag_temp_Icon = 3
  END
  IF (jac:Damaged)
    SELF.Q.jac:Damaged_Icon = 2
  ELSE
    SELF.Q.jac:Damaged_Icon = 0
  END
  IF (jac:Pirate)
    SELF.Q.jac:Pirate_Icon = 1
  ELSE
    SELF.Q.jac:Pirate_Icon = 0
  END
   
   


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = jac:Accessory
     GET(GLO:Queue,GLO:Queue.Pointer)
    EXECUTE DASBRW::8:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue


BRW9.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue2.Pointer2 = acr:Accessory
     GET(GLO:Queue2,GLO:Queue2.Pointer2)
    IF ERRORCODE()
      accessory_tag2_temp = ''
    ELSE
      accessory_tag2_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (accessory_tag2_temp = '*')
    SELF.Q.accessory_tag2_temp_Icon = 2
  ELSE
    SELF.Q.accessory_tag2_temp_Icon = 1
  END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW9.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW9::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW9::RecordStatus=ReturnValue
  IF BRW9::RecordStatus NOT=Record:OK THEN RETURN BRW9::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue2.Pointer2 = acr:Accessory
     GET(GLO:Queue2,GLO:Queue2.Pointer2)
    EXECUTE DASBRW::10:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW9::RecordStatus
  RETURN ReturnValue


BRW4.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.DeleteControl=?Delete
  END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

GenericFaultCodes PROCEDURE (Long f:Job,String f:type,String f:Req,String f:Acc,String f:Man,String f:WCrg,String f:WRep,Date f:Comp) !Generated from procedure template - Window

FilesOpened          BYTE
tmp:Required         BYTE(0)
save_taf_id          USHORT,AUTO
save_maf_id          USHORT,AUTO
save_tfo_id          USHORT,AUTO
save_mfo_id          USHORT,AUTO
ActionMessage        CSTRING(40)
JobFaultCodes        GROUP,PRE(jfc)
JF1                  STRING(30)
JF2                  STRING(30)
JF3                  STRING(30)
JF4                  STRING(30)
JF5                  STRING(30)
JF6                  STRING(30)
JF7                  STRING(30)
JF8                  STRING(30)
JF9                  STRING(30)
JF10                 STRING(255)
JF11                 STRING(255)
JF12                 STRING(255)
                     END
TradeFaultCodes      GROUP,PRE(tfc)
TF1                  STRING(30)
TF2                  STRING(30)
TF3                  STRING(30)
TF4                  STRING(30)
TF5                  STRING(30)
TF6                  STRING(30)
TF7                  STRING(30)
TF8                  STRING(30)
TF9                  STRING(30)
TF10                 STRING(30)
TF11                 STRING(30)
TF12                 STRING(30)
                     END
FaultCodesDescription GROUP,PRE(jfd)
Description1         STRING(255)
Description2         STRING(255)
Description3         STRING(255)
Description4         STRING(255)
Description5         STRING(255)
Description6         STRING(255)
Description7         STRING(255)
Description8         STRING(255)
Description9         STRING(255)
Description10        STRING(255)
Description11        STRING(255)
Description12        STRING(255)
                     END
tmp:LookupDescription STRING(30),DIM(20)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Fault Codes'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Fault Codes'),AT(8,8),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(4,28,672,354),USE(?Sheet1),FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Manufacturers Fault Codes'),USE(?Tab1),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP,AT(4,70,656,264),USE(?FieldsGroup)
                             PROMPT('Fault Code 1:'),AT(12,76),USE(?glo:FaultCode1:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(124,76,64,10),USE(glo:FaultCode1),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(192,73),USE(?LookupFaultCode:1),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             STRING(@s30),AT(220,76,108,10),USE(tmp:LookupDescription[1]),HIDE,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Fault Code 2:'),AT(12,96),USE(?glo:FaultCode2:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(124,96,64,10),USE(glo:FaultCode2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(192,92),USE(?LookupFaultCode:2),SKIP,FLAT,HIDE,ICON('lookupp.jpg')
                             STRING(@s30),AT(220,96,108,10),USE(tmp:LookupDescription[2]),HIDE,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Fault Code 3:'),AT(12,116),USE(?glo:FaultCode3:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(124,116,64,10),USE(glo:FaultCode3),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(192,112),USE(?LookupFaultCode:3),SKIP,FLAT,HIDE,ICON('lookupp.jpg')
                             STRING(@s30),AT(220,116,108,10),USE(tmp:LookupDescription[3]),HIDE,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Fault Code 4:'),AT(12,136),USE(?glo:FaultCode4:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(124,136,64,10),USE(glo:FaultCode4),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(192,132),USE(?LookupFaultCode:4),SKIP,FLAT,HIDE,ICON('lookupp.jpg')
                             STRING(@s30),AT(220,136,108,10),USE(tmp:LookupDescription[4]),HIDE,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Fault Code 5:'),AT(12,156),USE(?glo:FaultCode5:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(124,156,64,10),USE(glo:FaultCode5),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(192,152),USE(?LookupFaultCode:5),SKIP,FLAT,HIDE,ICON('lookupp.jpg')
                             STRING(@s30),AT(220,156,108,10),USE(tmp:LookupDescription[5]),HIDE,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Fault Code 6:'),AT(12,176),USE(?glo:FaultCode6:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(124,176,64,10),USE(glo:FaultCode6),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(192,172),USE(?LookupFaultCode:6),SKIP,FLAT,HIDE,ICON('lookupp.jpg')
                             STRING(@s30),AT(220,176,108,10),USE(tmp:LookupDescription[6]),HIDE,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Fault Code 7:'),AT(12,196),USE(?glo:FaultCode7:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(124,196,64,10),USE(glo:FaultCode7),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(192,192),USE(?LookupFaultCode:7),SKIP,FLAT,HIDE,ICON('lookupp.jpg')
                             STRING(@s30),AT(220,196,108,10),USE(tmp:LookupDescription[7]),HIDE,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Fault Code 8:'),AT(12,216),USE(?glo:FaultCode8:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(124,216,64,10),USE(glo:FaultCode8),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(192,212),USE(?LookupFaultCode:8),SKIP,FLAT,HIDE,ICON('lookupp.jpg')
                             STRING(@s30),AT(220,216,108,10),USE(tmp:LookupDescription[8]),HIDE,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Fault Code 9:'),AT(12,236),USE(?glo:FaultCode9:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(124,236,64,10),USE(glo:FaultCode9),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(192,232),USE(?LookupFaultCode:9),SKIP,FLAT,HIDE,ICON('lookupp.jpg')
                             STRING(@s30),AT(220,236,108,10),USE(tmp:LookupDescription[9]),HIDE,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Fault Code 10:'),AT(12,256),USE(?glo:FaultCode10:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s255),AT(124,256,64,10),USE(glo:FaultCode10),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(192,252),USE(?LookupFaultCode:10),SKIP,FLAT,HIDE,ICON('lookupp.jpg')
                             STRING(@s30),AT(220,256,108,10),USE(tmp:LookupDescription[10]),HIDE,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Fault Code 11:'),AT(352,76),USE(?glo:FaultCode11:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s255),AT(464,76,64,10),USE(glo:FaultCode11),HIDE,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(532,72),USE(?LookupFaultCode:11),SKIP,FLAT,HIDE,ICON('lookupp.jpg')
                             STRING(@s30),AT(560,76,108,10),USE(tmp:LookupDescription[11]),HIDE,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Fault Code 12:'),AT(352,96),USE(?glo:FaultCode12:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s255),AT(464,96,64,10),USE(glo:FaultCode12),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(532,92),USE(?LookupFaultCode:12),SKIP,FLAT,HIDE,ICON('lookupp.jpg')
                             STRING(@s30),AT(560,96,108,10),USE(tmp:LookupDescription[12]),HIDE,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Fault Code 13:'),AT(352,116),USE(?glo:FaultCode13:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(464,116,64,10),USE(glo:FaultCode13),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(532,112),USE(?LookupFaultCode:13),SKIP,FLAT,HIDE,ICON('lookupp.jpg')
                             STRING(@s30),AT(560,116,108,10),USE(tmp:LookupDescription[13]),HIDE,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Fault Code 14:'),AT(352,136),USE(?glo:FaultCode14:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(464,136,64,10),USE(glo:FaultCode14),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(532,132),USE(?LookupFaultCode:14),SKIP,FLAT,HIDE,ICON('lookupp.jpg')
                             STRING(@s30),AT(560,136,108,10),USE(tmp:LookupDescription[14]),HIDE,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Fault Code 15:'),AT(352,156),USE(?glo:FaultCode15:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(464,156,64,10),USE(glo:FaultCode15),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(532,153),USE(?LookupFaultCode:15),SKIP,FLAT,HIDE,ICON('lookupp.jpg')
                             STRING(@s30),AT(560,156,108,10),USE(tmp:LookupDescription[15]),HIDE,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Fault Code 16:'),AT(352,176),USE(?glo:FaultCode16:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(464,176,64,10),USE(glo:FaultCode16),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(532,174),USE(?LookupFaultCode:16),SKIP,FLAT,HIDE,ICON('lookupp.jpg')
                             STRING(@s30),AT(560,176,108,10),USE(tmp:LookupDescription[16]),HIDE,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Fault Code 17:'),AT(352,196),USE(?glo:FaultCode17:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(464,196,64,10),USE(glo:FaultCode17),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(532,193),USE(?LookupFaultCode:17),SKIP,FLAT,HIDE,ICON('lookupp.jpg')
                             STRING(@s30),AT(560,196,108,10),USE(tmp:LookupDescription[17]),HIDE,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Fault Code 18:'),AT(352,216),USE(?glo:FaultCode18:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(464,216,64,10),USE(glo:FaultCode18),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(532,212),USE(?LookupFaultCode:18),SKIP,FLAT,HIDE,ICON('lookupp.jpg')
                             STRING(@s30),AT(560,216,108,10),USE(tmp:LookupDescription[18]),HIDE,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Fault Code 19:'),AT(352,236),USE(?glo:FaultCode19:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(464,236,64,10),USE(glo:FaultCode19),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(532,232),USE(?LookupFaultCode:19),SKIP,FLAT,HIDE,ICON('lookupp.jpg')
                             STRING(@s30),AT(560,236,108,10),USE(tmp:LookupDescription[19]),HIDE,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Fault Code 20:'),AT(352,256),USE(?glo:FaultCode20:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s255),AT(464,256,64,10),USE(glo:FaultCode20),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                             BUTTON,AT(532,252),USE(?LookupFaultCode:20),SKIP,FLAT,HIDE,ICON('lookupp.jpg')
                             STRING(@s30),AT(560,256,108,10),USE(tmp:LookupDescription[20]),HIDE,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Specific Needs Type:'),AT(12,278),USE(?jobe3:FaultCode21:Prompt),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s30),AT(124,278,64,10),USE(jobe3:FaultCode21),DISABLE,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Specific Needs Code'),TIP('Specific Needs Code')
                             BUTTON,AT(192,272),USE(?ButtonSelectSNType),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             PROMPT('SN Software Installed:'),AT(13,294),USE(?Prompt26),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             CHECK,AT(124,294),USE(jobe3:SN_Software_Installed),HIDE,VALUE('1','0'),MSG('Specific Needs Completed')
                           END
                         END
                         TAB('Fault Codes'),USE(?Tab3),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('NO Fault Codes are required for this job.'),AT(143,137),USE(?Prompt25),CENTER,FONT(,22,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(608,384),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
local       Class
FormatField         Procedure(Long f:FieldNumber,Byte f:Hide,Long f:Prompt,Long f:Field,Byte f:Req,Long f:LookupField,Long f:LookupDescription)
LookupButtonCode    Procedure(Long f:FieldNumber,String f:Field),String
LookupFieldCode     Procedure(Long f:FieldNumber,String f:Field,Long f:Lookup),String
            End ! local       Class
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

FaultCoding        Routine   ! Manufacturers Fault Codes

    tmp:Required = 0
! Changing (DBH 23/05/2006) #6733 - Change for booking so that chargeable forced codes aren't hidden
!     access:chartype.clearkey(cha:charge_type_key)
!     cha:charge_type = f:WCrg
!     If access:chartype.tryfetch(cha:charge_type_key) = Level:Benign
!         If cha:force_warranty = 'YES'
!             If f:WRep <> ''
!                 Case f:Type
!                     Of 'C'
!                         Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
!                         rtd:Manufacturer = f:Man
!                         rtd:Chargeable   = 'YES'
!                         rtd:Repair_Type  = f:WRep
!                         If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
!                             !Found
!                             If rtd:CompFaultCoding
!                                 tmp:Required = 1
!                             End !If rtd:CompFaultCoding
!
!                         Else!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
!                             !Error
!                             !Assert(0,'<13,10>Fetch Error<13,10>')
!                         End!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
!                     Of 'W'
!                         Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
!                         rtd:Manufacturer = f:Man
!                         rtd:Warranty     = 'YES'
!                         rtd:Repair_Type  = f:WRep
!                         If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
!                             !Found
!                             If rtd:CompFaultCoding
!                                 tmp:Required = 1
!                             End !If rtd:CompFaultCoding
!
!                         Else!If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
!                             !Error
!                             !Assert(0,'<13,10>Fetch Error<13,10>')
!                         End!If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
!                 End !Case f:Type
!
!             End !If f:WRep <> ''
!         End
!    end !if
! to (DBH 23/05/2006) #6733
    If f:Req = InsertRecord
        If f:Type = 'C' Or f:Type = 'S'
            Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
            cha:Charge_Type = f:WCrg
            If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                ! Found
                If cha:Force_Warranty = 'YES'
                    tmp:Required = 1
                End ! If cha:Force_Warranty = 'YES'
            Else ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                ! Error
            End ! If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        End ! If f:Type = 'C' Or f:Type = 'S'
        If (f:Type = 'W' Or f:Type = 'S') And tmp:Required = 0
            Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
            cha:Charge_Type = f:WRep
            If Access:CHARTYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                ! Found
                If cha:Force_Warranty = 'YES'
                    tmp:Required = 1
                End ! If cha:Force_Warranty = 'YES'
            Else ! If Access:CHATYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
                ! Error
            End ! If Access:CHATYPE.Tryfetch(cha:Charge_Type_Key) = Level:Benign
        End ! If (f:Type = 'W' Or f:Type = 'S') And tmp:Required = 0
    Else ! If f:Req = InsertRecord
        access:chartype.clearkey(cha:charge_type_key)
        cha:charge_type = f:WCrg
        If access:chartype.tryfetch(cha:charge_type_key) = Level:Benign
            If cha:force_warranty = 'YES'
                If f:WRep <> ''
                    Case f:Type
                        Of 'C'
                            Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
                            rtd:Manufacturer = f:Man
                            rtd:Chargeable   = 'YES'
                            rtd:Repair_Type  = f:WRep
                            If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                                !Found
                                If rtd:CompFaultCoding
                                    tmp:Required = 1
                                End !If rtd:CompFaultCoding

                            Else!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                            End!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                        Of 'W'
                            Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
                            rtd:Manufacturer = f:Man
                            rtd:Warranty     = 'YES'
                            rtd:Repair_Type  = f:WRep
                            If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                                !Found
                                If rtd:CompFaultCoding
                                    tmp:Required = 1
                                End !If rtd:CompFaultCoding

                            Else!If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                            End!If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                    End !Case f:Type

                End !If f:WRep <> ''
            End
        end !if
    End ! If f:Req = InsertRecord
! End (DBH 23/05/2006) #6733

    found# = 0
    setcursor(cursor:wait)
    save_maf_id = access:manfault.savefile()
    access:manfault.clearkey(maf:field_number_key)
    maf:manufacturer = f:Man
    set(maf:field_number_key,maf:field_number_key)
    loop
        if access:manfault.next()
           break
        end !if
        if maf:manufacturer <> f:Man      |
            then break.  ! end if

        If maf:MainFault
            If man:UseInvTextForFaults
                Cycle
            End !If man:UseInvTextForFaults
        End !If maf:MainFault
        IF found# = 0
            found# = 1
            Unhide(?tab1)
        End!IF found# = 0
! Changing (DBH 23/05/2006) #6733 - Take account of seperate chargeable and warranty forced fault codes (only at booking)
!         hide# = 0
!         req#  = 0
!
!
!         If maf:Compulsory_At_Booking = 'YES'
!             If tmp:Required
!                 req# = 1
!             End!If tmp:Required
!         Else !If maf:Compulsory_At_Booking = 'YES' and tmp:Required
!             If f:req = InsertRecord
!                 Hide# = 1
!             End !If f:req = InsertRecord
!         End !If maf:Compulsory_At_Booking = 'YES' and tmp:Required
!
!         If maf:Compulsory = 'YES' or tmp:Required  ! Changed from AND to OR (27th Aug 2002)
!             If f:Req <> InsertRecord
!                 req# = 1
!             End !If f:Req <> InsertRecord
!         End !If maf:Compulsory = 'YES' and tmp:Required
! to (DBH 23/05/2006) #6733
        Hide# = 1
        Req# = 0
        If f:Type = 'W' Or f:Type = 'S'
            If maf:Compulsory_At_Booking = 'YES'
                Hide# = 0
                If tmp:Required
                    Req# = 1
                End ! If tmp:Required
            End ! If maf:Compulsory_At_Booking = 'YES'

            If f:Req <> InsertRecord
                If maf:Compulsory = 'YES' Or tmp:Required ! Don't know why this was changed to OR, but I'm not going to change it now
                    Req# = 1
                End ! If maf:Compulsory = 'YES'
            End ! If f:Req <> InsertRecord
        End ! If f:Type = 'W' Or f:Type = 'S'
        If f:Type = 'C' Or f:Type = 'S'
            If maf:CharCompulsoryBooking
                Hide# = 0
                If tmp:Required
                    Req# = 1
                End ! If tmp:Required
            End ! If maf:CharCompulsoryBooking
            If f:Req <> InsertRecord
                If maf:CharCompulsory Or tmp:Required ! Don't know why this was changed to OR, but I'm not going to change it now
                    Req# = 1
                End ! If maf:CharCompulsory
            End ! If f:Req <> InsertRecord
        End ! If f:Type = 'C' Or f:Type = 'S'

        If f:Req <> InsertRecord
            Hide# = 0
        End !If f:Req <> InsertRecord
! End (DBH 23/05/2006) #6733

        Case maf:field_number
            Of 1
                local.FormatField(maf:Field_Number,Hide#,?glo:FaultCode1:Prompt,?glo:FaultCode1,Req#,?LookupFaultCode:1,?tmp:LookupDescription_1)
            Of 2
                local.FormatField(maf:Field_Number,Hide#,?glo:FaultCode2:Prompt,?glo:FaultCode2,Req#,?LookupFaultCode:2,?tmp:LookupDescription_2)
            Of 3
                local.FormatField(maf:Field_Number,Hide#,?glo:FaultCode3:Prompt,?glo:FaultCode3,Req#,?LookupFaultCode:3,?tmp:LookupDescription_3)
            Of 4
                local.FormatField(maf:Field_Number,Hide#,?glo:FaultCode4:Prompt,?glo:FaultCode4,Req#,?LookupFaultCode:4,?tmp:LookupDescription_4)
            Of 5
                local.FormatField(maf:Field_Number,Hide#,?glo:FaultCode5:Prompt,?glo:FaultCode5,Req#,?LookupFaultCode:5,?tmp:LookupDescription_5)
            Of 6
                local.FormatField(maf:Field_Number,Hide#,?glo:FaultCode6:Prompt,?glo:FaultCode6,Req#,?LookupFaultCode:6,?tmp:LookupDescription_6)
            Of 7
                local.FormatField(maf:Field_Number,Hide#,?glo:FaultCode7:Prompt,?glo:FaultCode7,Req#,?LookupFaultCode:7,?tmp:LookupDescription_7)
            Of 8
                local.FormatField(maf:Field_Number,Hide#,?glo:FaultCode8:Prompt,?glo:FaultCode8,Req#,?LookupFaultCode:8,?tmp:LookupDescription_8)
            Of 9
                local.FormatField(maf:Field_Number,Hide#,?glo:FaultCode9:Prompt,?glo:FaultCode9,Req#,?LookupFaultCode:9,?tmp:LookupDescription_9)
            Of 10
                local.FormatField(maf:Field_Number,Hide#,?glo:FaultCode10:Prompt,?glo:FaultCode10,Req#,?LookupFaultCode:10,?tmp:LookupDescription_10)
            Of 11
                local.FormatField(maf:Field_Number,Hide#,?glo:FaultCode11:Prompt,?glo:FaultCode11,Req#,?LookupFaultCode:11,?tmp:LookupDescription_11)
            Of 12
                local.FormatField(maf:Field_Number,Hide#,?glo:FaultCode12:Prompt,?glo:FaultCode12,Req#,?LookupFaultCode:12,?tmp:LookupDescription_12)
            Of 13
                local.FormatField(maf:Field_Number,Hide#,?glo:FaultCode13:Prompt,?glo:FaultCode13,Req#,?LookupFaultCode:13,?tmp:LookupDescription_13)
            Of 14
                local.FormatField(maf:Field_Number,Hide#,?glo:FaultCode14:Prompt,?glo:FaultCode14,Req#,?LookupFaultCode:14,?tmp:LookupDescription_14)
            Of 15
                local.FormatField(maf:Field_Number,Hide#,?glo:FaultCode15:Prompt,?glo:FaultCode15,Req#,?LookupFaultCode:15,?tmp:LookupDescription_15)
            Of 16
                local.FormatField(maf:Field_Number,Hide#,?glo:FaultCode16:Prompt,?glo:FaultCode16,Req#,?LookupFaultCode:16,?tmp:LookupDescription_16)
            Of 17
                local.FormatField(maf:Field_Number,Hide#,?glo:FaultCode17:Prompt,?glo:FaultCode17,Req#,?LookupFaultCode:17,?tmp:LookupDescription_17)
            Of 18
                local.FormatField(maf:Field_Number,Hide#,?glo:FaultCode18:Prompt,?glo:FaultCode18,Req#,?LookupFaultCode:18,?tmp:LookupDescription_18)
            Of 19
                local.FormatField(maf:Field_Number,Hide#,?glo:FaultCode19:Prompt,?glo:FaultCode19,Req#,?LookupFaultCode:19,?tmp:LookupDescription_19)
            Of 20
                local.FormatField(maf:Field_Number,Hide#,?glo:FaultCode20:Prompt,?glo:FaultCode20,Req#,?LookupFaultCode:20,?tmp:LookupDescription_20)
        End

        If maf:lookup = 'YES'
            field"  = ''
            records# = 0
            save_mfo_id = access:manfaulo.savefile()
            access:manfaulo.clearkey(mfo:field_key)
            mfo:manufacturer = maf:manufacturer
            mfo:field_number = maf:field_number
            set(mfo:field_key,mfo:field_key)
            loop
                if access:manfaulo.next()
                   break
                end !if
                if mfo:manufacturer <> maf:manufacturer      |
                or mfo:field_number <> maf:field_number      |
                    then break.  ! end if
                records# += 1
                If records# > 1
                    field" = ''
                    Break
                End!If records# > 1
                field"  = mfo:field
            end !loop
            access:manfaulo.restorefile(save_mfo_id)
            Case maf:field_number
                of 1
                    If glo:FaultCode1 = '' And ?glo:FaultCode1{prop:Hide} = 0 And ?glo:FaultCode1{prop:Disable} = 0
                        glo:FaultCode1 = field"
                    End!If glo:FaultCode = ''
                of 2
                    If glo:FaultCode2 = '' And ?glo:FaultCode2{prop:Hide} = 0 And ?glo:FaultCode2{prop:Disable} = 0
                        glo:FaultCode2 = field"
                    End!If glo:FaultCode = ''
                of 3
                    If glo:FaultCode3 = '' And ?glo:FaultCode3{prop:Hide} = 0 And ?glo:FaultCode3{prop:Disable} = 0
                        glo:FaultCode3 = field"
                    End!If glo:FaultCode = ''
                of 4
                    If glo:FaultCode4 = '' And ?glo:FaultCode4{prop:Hide} = 0 And ?glo:FaultCode4{prop:Disable} = 0
                        glo:FaultCode4 = field"
                    End!If glo:FaultCode = ''
                of 5
                    If glo:FaultCode5 = '' And ?glo:FaultCode5{prop:Hide} = 0 And ?glo:FaultCode5{prop:Disable} = 0
                        glo:FaultCode5 = field"
                    End!If glo:FaultCode = ''
                of 6
                    If glo:FaultCode6 = '' And ?glo:FaultCode6{prop:Hide} = 0 And ?glo:FaultCode6{prop:Disable} = 0
                        glo:FaultCode6 = field"
                    End!If glo:FaultCode = ''
                of 7
                    If glo:FaultCode7 = '' And ?glo:FaultCode7{prop:Hide} = 0 And ?glo:FaultCode7{prop:Disable} = 0
                        glo:FaultCode7 = field"
                    End!If glo:FaultCode = ''
                of 8
                    If glo:FaultCode8 = '' And ?glo:FaultCode8{prop:Hide} = 0 And ?glo:FaultCode8{prop:Disable} = 0
                        glo:FaultCode8 = field"
                    End!If glo:FaultCode = ''
                of 9
                    If glo:FaultCode9 = '' And ?glo:FaultCode9{prop:Hide} = 0 And ?glo:FaultCode9{prop:Disable} = 0
                        glo:FaultCode9 = field"
                    End!If glo:FaultCode = ''
                of 10
                    If glo:FaultCode10 = '' And ?glo:FaultCode10{prop:Hide} = 0 And ?glo:FaultCode10{prop:Disable} = 0
                        glo:FaultCode10 = field"
                    End!If glo:FaultCode = ''
                of 11
                    If glo:FaultCode11 = '' And ?glo:FaultCode11{prop:Hide} = 0 And ?glo:FaultCode11{prop:Disable} = 0
                        glo:FaultCode11 = field"
                    End!If glo:FaultCode = ''
                of 12
                    If glo:FaultCode12 = '' And ?glo:FaultCode12{prop:Hide} = 0 And ?glo:FaultCode12{prop:Disable} = 0
                        glo:FaultCode12 = field"
                    End!If glo:FaultCode = ''
                of 13
                    If glo:FaultCode13 = '' And ?glo:FaultCode13{prop:Hide} = 0 And ?glo:FaultCode13{prop:Disable} = 0
                        glo:FaultCode13 = field"
                    End!If glo:FaultCode = ''
                of 14
                    If glo:FaultCode14 = '' And ?glo:FaultCode14{prop:Hide} = 0 And ?glo:FaultCode14{prop:Disable} = 0
                        glo:FaultCode14 = field"
                    End!If glo:FaultCode = ''
                of 15
                    If glo:FaultCode15 = '' And ?glo:FaultCode15{prop:Hide} = 0 And ?glo:FaultCode15{prop:Disable} = 0
                        glo:FaultCode15 = field"
                    End!If glo:FaultCode = ''
                of 16
                    If glo:FaultCode16 = '' And ?glo:FaultCode16{prop:Hide} = 0 And ?glo:FaultCode16{prop:Disable} = 0
                        glo:FaultCode16 = field"
                    End!If glo:FaultCode = ''
                of 17
                    If glo:FaultCode17 = '' And ?glo:FaultCode17{prop:Hide} = 0 And ?glo:FaultCode17{prop:Disable} = 0
                        glo:FaultCode17 = field"
                    End!If glo:FaultCode = ''
                of 18
                    If glo:FaultCode18 = '' And ?glo:FaultCode18{prop:Hide} = 0 And ?glo:FaultCode18{prop:Disable} = 0
                        glo:FaultCode18 = field"
                    End!If glo:FaultCode = ''
                of 19
                    If glo:FaultCode19 = '' And ?glo:FaultCode19{prop:Hide} = 0 And ?glo:FaultCode19{prop:Disable} = 0
                        glo:FaultCode19 = field"
                    End!If glo:FaultCode = ''
                of 20
                    If glo:FaultCode20 = '' And ?glo:FaultCode20{prop:Hide} = 0 And ?glo:FaultCode20{prop:Disable} = 0
                        glo:FaultCode20 = field"
                    End!If glo:FaultCode = ''
            End!Case maf:field_number
        End!If maf:lookup = 'YES'
    end !loop
    access:manfault.restorefile(save_maf_id)
    setcursor()
    Display()

    setcursor()

    Display()




ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020450'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, Window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('GenericFaultCodes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:JOBSE3.Open
  Access:JOBS.UseFile
  Access:MANUFACT.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAULO.UseFile
  Access:JOBNOTES.UseFile
  Access:TRAFAULT.UseFile
  Access:TRAFAULO.UseFile
  Access:JOBSE.UseFile
  Access:REPTYDEF.UseFile
  Access:MANFAUEX.UseFile
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','GenericFaultCodes')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  !View Only
  If glo:Preview = 'V'
      ?FieldsGroup{prop:Disable} = 1
  End !glo:FaultCode21 = 'RRCVIEWONLY'
  
  !TB13287 - J - 06/01/15 - Adding specific needs faults
  Access:Jobse3.clearkey(jobe3:KeyRefNumber)
  jobe3:RefNumber = job:ref_number
  if access:jobse3.fetch(jobe3:KeyRefNumber)
      !error - double check ...
      if clip(GLO:Select41) = 'SND' then
          !this is called from booking process and has not been saved yet
          !message('Glo:select41 is SND')
          Access:jobse3.primerecord()
          jobe3:RefNumber = job:Ref_number
          jobe3:SpecificNeeds = 1
          access:jobse3.update()
      else
          !just to be sure
          !message('Glo:Select41 is '&clip(glo:Select41))
          jobe3:SpecificNeeds = 0
      end
  ELSE
      !record fetched - may have been created by address call?
      if clip(GLO:Select41) = 'SND' then
          !this is called from booking process and has not been saved yet
          !message('Record found and Glo:select41 is SND')
          jobe3:SpecificNeeds = 1
          Access:jobse3.update()
      END
  
  END
  
  if jobe3:SpecificNeeds then
      unhide(?jobe3:FaultCode21:Prompt)
      unhide(?jobe3:FaultCode21)
      unhide(?ButtonSelectSNType)
      If f:comp <> ''
          !this is for completion - let the software installed show
          unhide(?Prompt26)
          unhide(?jobe3:SN_Software_Installed)
      END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:JOBSE3.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','GenericFaultCodes')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  Globalrequest = Request
  Browse_Manufacturer_Fault_Lookup
  ReturnValue   = Globalresponse
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonSelectSNType
      jobe3:FaultCode21 = SelectSNCode(jobe3:FaultCode21)
      display()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020450'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020450'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020450'&'0')
      ***
    OF ?glo:FaultCode1
      glo:FaultCode1 = local.LookupFieldCode(1,glo:FaultCode1,?LookupFaultCode:1)
      Do FaultCoding
    OF ?LookupFaultCode:1
      ThisWindow.Update
      glo:FaultCode1 = local.LookupButtonCode(1,glo:FaultCode1)
      Do FaultCoding
      Display()
    OF ?glo:FaultCode2
      glo:FaultCode2 = local.LookupFieldCode(2,glo:FaultCode2,?LookupFaultCode:2)
      Do FaultCoding
    OF ?LookupFaultCode:2
      ThisWindow.Update
      glo:FaultCode2 = local.LookupButtonCode(2,glo:FaultCode2)
      Do FaultCoding
      Display()
    OF ?glo:FaultCode3
      glo:FaultCode3 = local.LookupFieldCode(3,glo:FaultCode3,?LookupFaultCode:3)
      Do FaultCoding
    OF ?LookupFaultCode:3
      ThisWindow.Update
      glo:FaultCode3 = local.LookupButtonCode(3,glo:FaultCode3)
      Do FaultCoding
      Display()
    OF ?glo:FaultCode4
      glo:FaultCode4 = local.LookupFieldCode(4,glo:FaultCode4,?LookupFaultCode:4)
      Do FaultCoding
    OF ?LookupFaultCode:4
      ThisWindow.Update
      glo:FaultCode4 = local.LookupButtonCode(4,glo:FaultCode4)
      Do FaultCoding
      Display()
    OF ?glo:FaultCode5
      glo:FaultCode5 = local.LookupFieldCode(5,glo:FaultCode5,?LookupFaultCode:5)
      Do FaultCoding
    OF ?LookupFaultCode:5
      ThisWindow.Update
      glo:FaultCode5= local.LookupButtonCode(5,glo:FaultCode5)
      Do FaultCoding
      Display()
    OF ?glo:FaultCode6
      glo:FaultCode6 = local.LookupFieldCode(6,glo:FaultCode6,?LookupFaultCode:6)
      Do FaultCoding
    OF ?LookupFaultCode:6
      ThisWindow.Update
      glo:FaultCode6 = local.LookupButtonCode(6,glo:FaultCode6)
      Do FaultCoding
      Display()
    OF ?glo:FaultCode7
      glo:FaultCode7 = local.LookupFieldCode(7,glo:FaultCode7,?LookupFaultCode:7)
      Do FaultCoding
    OF ?LookupFaultCode:7
      ThisWindow.Update
      glo:FaultCode7 = local.LookupButtonCode(7,glo:FaultCode7)
      Do FaultCoding
      Display()
    OF ?glo:FaultCode8
      glo:FaultCode8 = local.LookupFieldCode(8,glo:FaultCode8,?LookupFaultCode:8)
      Do FaultCoding
    OF ?LookupFaultCode:8
      ThisWindow.Update
      glo:FaultCode8 = local.LookupButtonCode(8,glo:FaultCode8)
      Do FaultCoding
      Display()
    OF ?glo:FaultCode9
      glo:FaultCode9 = local.LookupFieldCode(9,glo:FaultCode9,?LookupFaultCode:9)
      Do FaultCoding
    OF ?LookupFaultCode:9
      ThisWindow.Update
      glo:FaultCode9= local.LookupButtonCode(9,glo:FaultCode9)
      Do FaultCoding
      Display()
    OF ?glo:FaultCode10
      glo:FaultCode10 = local.LookupFieldCode(10,glo:FaultCode10,?LookupFaultCode:10)
      Do FaultCoding
    OF ?LookupFaultCode:10
      ThisWindow.Update
      glo:FaultCode10 = local.LookupButtonCode(10,glo:FaultCode10)
      Do FaultCoding
      Display()
    OF ?glo:FaultCode11
      glo:FaultCode11 = local.LookupFieldCode(11,glo:FaultCode11,?LookupFaultCode:11)
      Do FaultCoding
      ! Inserting (DBH 06/01/2006) #6645 - Show error, for Motorola, if the accessory date does not match the POP date
      If man:EDIFileType = 'MOTOROLA'
          If job:dop <> '' And glo:FaultCode11 <> '' And glo:FaultCode11 <> 'N/A'
              If Format(job:dop,@d13) <> glo:FaultCode11
                  Case Missive('The POP Date entered for the accessory does not match the POP Date entered for the handset. '&|
                    '|Please submit the original proof of purchase for the accessory to the Warranty Vetting Department.','ServiceBase 3g',|
                                 'mexclam.jpg','/OK')
                      Of 1 ! OK Button
                      Select(?glo:FaultCode11)
                  End ! Case Missive
              End ! If Format(job:dop,@d13) <> Format(glo:FaultCode11,@d13)
          End ! If job:dop <> '' And glo:FaultCode11 <> '' And glo:FaultCode11 <> 'N/A'
      End ! If man:EDIFileType = 'MOTOROLA'
      ! End (DBH 06/01/2006) #6645
    OF ?LookupFaultCode:11
      ThisWindow.Update
      glo:FaultCode11 = local.LookupButtonCode(11,glo:FaultCode11)
      Do FaultCoding
      Display()
      ! Inserting (DBH 06/01/2006) #6645 - Show error, for Motorola, if the accessory date does not match the POP date
      If man:EDIFileType = 'MOTOROLA'
          If job:dop <> '' And glo:FaultCode11 <> '' And glo:FaultCode11 <> 'N/A'
              If Format(job:dop,@d13) <> glo:FaultCode11
                  Case Missive('The POP Date entered for the accessory does not match the POP Date entered for the handset. '&|
                    '|Please submit the original proof of purchase for the accessory to the Warranty Vetting Department.','ServiceBase 3g',|
                                 'mexclam.jpg','/OK')
                      Of 1 ! OK Button
                      Select(?glo:FaultCode11)
                  End ! Case Missive
              End ! If Format(job:dop,@d13) <> Format(glo:FaultCode11,@d13)
          End ! If job:dop <> '' And glo:FaultCode11 <> '' And glo:FaultCode11 <> 'N/A'
      End ! If man:EDIFileType = 'MOTOROLA'
      ! End (DBH 06/01/2006) #6645
    OF ?glo:FaultCode12
      glo:FaultCode12 = local.LookupFieldCode(12,glo:FaultCode12,?LookupFaultCode:12)
      Do FaultCoding
    OF ?LookupFaultCode:12
      ThisWindow.Update
      glo:FaultCode12 = local.LookupButtonCode(12,glo:FaultCode12)
      Do FaultCoding
      Display()
    OF ?glo:FaultCode13
      glo:FaultCode13 = local.LookupFieldCode(13,glo:FaultCode13,?LookupFaultCode:13)
      Do FaultCoding
    OF ?LookupFaultCode:13
      ThisWindow.Update
      glo:FaultCode13 = local.LookupButtonCode(13,glo:FaultCode13)
      Do FaultCoding
      Display()
    OF ?glo:FaultCode14
      glo:FaultCode14 = local.LookupFieldCode(14,glo:FaultCode14,?LookupFaultCode:14)
      Do FaultCoding
    OF ?LookupFaultCode:14
      ThisWindow.Update
      glo:FaultCode14 = local.LookupButtonCode(14,glo:FaultCode14)
      Do FaultCoding
      Display()
    OF ?glo:FaultCode15
      glo:FaultCode15 = local.LookupFieldCode(15,glo:FaultCode15,?LookupFaultCode:15)
      Do FaultCoding
    OF ?LookupFaultCode:15
      ThisWindow.Update
      glo:FaultCode15 = local.LookupButtonCode(15,glo:FaultCode15)
      Do FaultCoding
      Display()
    OF ?glo:FaultCode16
      glo:FaultCode16 = local.LookupFieldCode(16,glo:FaultCode16,?LookupFaultCode:16)
      Do FaultCoding
    OF ?LookupFaultCode:16
      ThisWindow.Update
      glo:FaultCode16 = local.LookupButtonCode(16,glo:FaultCode16)
      Do FaultCoding
      Display()
    OF ?glo:FaultCode17
      glo:FaultCode17 = local.LookupFieldCode(17,glo:FaultCode17,?LookupFaultCode:17)
      Do FaultCoding
    OF ?LookupFaultCode:17
      ThisWindow.Update
      glo:FaultCode17 = local.LookupButtonCode(17,glo:FaultCode17)
      Do FaultCoding
      Display()
    OF ?glo:FaultCode18
      glo:FaultCode18 = local.LookupFieldCode(18,glo:FaultCode18,?LookupFaultCode:18)
      Do FaultCoding
    OF ?LookupFaultCode:18
      ThisWindow.Update
      glo:FaultCode18 = local.LookupButtonCode(18,glo:FaultCode18)
      Do FaultCoding
      Display()
    OF ?glo:FaultCode19
      glo:FaultCode19 = local.LookupFieldCode(19,glo:FaultCode19,?LookupFaultCode:19)
      Do FaultCoding
    OF ?LookupFaultCode:19
      ThisWindow.Update
      glo:FaultCode19 = local.LookupButtonCode(19,glo:FaultCode19)
      Do FaultCoding
      Display()
    OF ?glo:FaultCode20
      glo:FaultCode20 = local.LookupFieldCode(20,glo:FaultCode20,?LookupFaultCode:20)
      Do FaultCoding
    OF ?LookupFaultCode:20
      ThisWindow.Update
      glo:FaultCode20 = local.LookupButtonCode(20,glo:FaultCode20)
      Do FaultCoding
      Display()
    OF ?jobe3:SN_Software_Installed
      !TB13287 - J - 05/01/15 always add an audit trail when ticked/unticked
      IF ~window{Prop:AcceptAll} THEN
          if jobe3:SN_Software_Installed = true then
              If AddToAudit(job:Ref_Number,'JOB','SPECIFIC NEEDS SOFTWARE INSTALLED','SPECIFIC NEEDS SOFTWARE INSTALLED')
                  !error
              END
          ELSE
              !Do not allow to untick if job completed or in QA process
              if job:Completed = 'YES' or job:Current_Status[1] = '6'
                  Missive('This is a specific needs device. '&|
                          '|The specific needs software cannot be uninstalled after completion.','ServiceBase 3g',|
                                       'mexclam.jpg','/OK')
                  jobe3:SN_Software_Installed = true
                  display()
              ELSE
                  If AddToAudit(job:Ref_Number,'JOB','SPECIFIC NEEDS SOFTWARE UNINSTALLED','SPECIFIC NEEDS SOFTWARE UNINSTALLED.')
                      !error
                  END
              END
          END
      END
    OF ?OkButton
      ThisWindow.Update
      continue# = 1
      
      
      If f:comp <> ''
          continue# = 0
          If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?glo:FaultCode1:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode2{prop:hide} = 0 and glo:FaultCode2 = '' and error# = 0
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?glo:FaultCode2:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode3{prop:hide} = 0 and glo:FaultCode3 = '' and error# = 0
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?glo:FaultCode3:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive
      
          End!If ?job:fault_ode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode4{prop:hide} = 0 and glo:FaultCode4 = '' and error# = 0
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?glo:FaultCode4:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive
      
      
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode5{prop:hide} = 0 and glo:FaultCode5 = '' and error# = 0
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?glo:FaultCode5:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive
      
      
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode6{prop:hide} = 0 and glo:FaultCode6 = '' and error# = 0
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?glo:FaultCode6:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive
      
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode7{prop:hide} = 0 and glo:FaultCode7 = '' and error# = 0
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?glo:FaultCode7:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive
      
          End!If ?job:fault_ode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode8{prop:hide} = 0 and glo:FaultCode8 = '' and error# = 0
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?glo:FaultCode8:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive
      
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode9{prop:hide} = 0 and glo:FaultCode9 = '' and error# = 0
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?glo:FaultCode9:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive
      
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode10{prop:hide} = 0 and glo:FaultCode10 = '' and error# = 0
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?glo:FaultCode10:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive
      
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode11{prop:hide} = 0 and glo:FaultCode11 = '' and error# = 0
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?glo:FaultCode11:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive
      
          End!If ?job:fault_ode1{prop:hide} = 0 and glo:FaultCode1 = ''
          If ?glo:FaultCode12{prop:hide} = 0 and glo:FaultCode12 = '' and error# = 0
              error# = 1
              Case Missive('Warning! This job has been completed and Fault Code ' & ?glo:FaultCode12:Prompt{prop:Text} & ' is blank.'&|
                '<13,10>'&|
                '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Continue# = 1
                  Of 1 ! No Button
              End ! Case Missive
      
          End!If ?glo:FaultCode1{prop:hide} = 0 and glo:FaultCode1 = ''
      
      
          if jobe3:SpecificNeeds
              !this was opened on entry and fields displayed 
              if jobe3:SN_Software_Installed = false
                  !this is compulsory
                  Error# = 1
                  Continue# = 0
                  Case Missive('Warning! The mobile device booked in is a Specific Needs Device. ' &|
                    '<13,10>Please confirm that you have installed the Specific Needs Software before the repair can be completed.','ServiceBase 3g',|
                                 'mquest.jpg','\OK')
                      Of 1 ! OK Button
                  End ! Case Missive
       
              END !if type is empty
          END !if jobe3:SpecificNeeds
      
      
          If error# = 0
              continue# = 1
          End!If error# = 0
      
      End!If job:date_completed <> ''
      
      
          !TB13287 - J - 06/01/15 - adding specific needs
          !message('SN job = '&clip(jobe3:SpecificNeeds))
          if jobe3:SpecificNeeds
              !this was opened on entry and fields displayed 
              if clip(jobe3:FaultCode21) = ''
                  !message('found a blank jobe3:FaultCode21')
                  !this is compulsory
                  Error# = 1
                  Continue# = 0
                  Case Missive('Warning! The mobile device booked in is a Specific Needs Device. ' &|
                    '<13,10>Please select the Specific Needs Type.','ServiceBase 3g',|
                                 'mquest.jpg','\OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              ELSE
                  !message('Not a blank :jobe3:FaultCode21= '&clip(jobe3:FaultCode21))
                  Access:jobse3.update()
              END !if type is empty
      
          END !if jobe3:SpecificNeeds
      
      
      
      If continue# = 1
          Post(event:closewindow)
      End
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
      sub:Account_Number  = f:Acc
      If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          ! Found
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = sub:Main_Account_Number
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              ! Found
      
          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              ! Error
          End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      
      Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          ! Error
      End ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = f:Job
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          ! Found
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          ! Error
          If Access:JOBSE.PrimeRecord() = Level:Benign
              jobe:RefNumber = f:Job
              If Access:JOBSE.TryInsert() = Level:Benign
                  ! Insert Successful
      
              Else ! If Access:JOBSE.TryInsert() = Level:Benign
                  ! Insert Failed
                  Access:JOBSE.CancelAutoInc()
              End ! If Access:JOBSE.TryInsert() = Level:Benign
          End !If Access:JOBSE.PrimeRecord() = Level:Benign
      End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
      man:Manufacturer    = job:Manufacturer
      If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          !Found
      Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          !Error
      End !If AccessMANUFACT.Tryfetch(manManufacturer_Key) = LevelBenign
      
      Do FaultCoding
      
      If ?tab1{prop:hide} = 1
          ?tab3{prop:hide} = 0
      End!If ?tab1{prop:hide} = 0 Or ?tab2{prop:hide} = 0
      
      !ThisMakeOver.SetWindow(Win:FORM)
      Display()
      
      Select(?+1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

local.FormatField       Procedure(Long f:FieldNumber,Byte f:Hide,Long f:Prompt,Long f:Field,Byte f:Required,Long f:LookupField,Long f:LookupDescription)
Code
    If f:Hide = 0
        f:Prompt{Prop:Hide} = False
        f:Field{prop:Hide} = False
        f:Prompt{prop:Text} = maf:Field_Name
        If f:Required
            f:Field{Prop:Req} = True
        End ! If f:Req

        Case maf:Field_Type
        Of 'DATE'
            f:Field{prop:Text} = Clip(maf:DateType)
            If ~glo:WebJob
                ! Only show calendar for non web job (DBH: 18-10-2005)
                f:LookupField{prop:Hide} = False
            End ! If ~glo:WebJob

        Of 'STRING'
            If maf:RestrictLength
                f:Field{prop:Text} = '@s' & maf:LengthTo
            Else ! If maf:RestrictLength
                f:Field{prop:Text} = '@s30'
            End ! If maf:RestrictLength

            If maf:Lookup = 'YES'
                f:LookupField{prop:Hide} = 0
                Access:MANFAULO.Clearkey(mfo:Field_Key)
                mfo:Manufacturer    = f:Man
                mfo:Field_Number    = f:FieldNumber
                mfo:Field           = Contents(f:Field)
                If Access:MANFAULO.Tryfetch(mfo:Field_Key) = Level:Benign
                    ! Found
                    f:LookupDescription{prop:Hide} = 0
                    tmp:LookupDescription[f:FieldNumber] = mfo:Description
                Else ! If Access:MANFAULO.Tryfetch(mfo:Field_Key) = Level:Benign
                    ! Error
                    f:LookupDescription{prop:Hide} = 1
                End ! If Access:MANFAULO.Tryfetch(mfo:Field_Key) = Level:Benign
            Else ! If maf:Lookup = 'YES'
                f:LookupDescription{prop:Hide} = 1
                f:LookupField{prop:Hide} = 1
            End ! If maf:Lookup = 'YES'
        Of 'NUMBER'
            IF maf:RestrictLength
                f:Field{prop:Text} = '@n_' & maf:LengthTo
            Else ! IF maf:RestrictLength
                f:Field{Prop:Text} = '@n_9'
            End ! IF maf:RestrictLength
            If maf:Lookup
                f:LookupField{prop:Hide} = False
            Else
                f:LookupField{prop:Hide} = 1
            End ! If maf:Lookup
        End ! Case maf:Field_Type
        ! Inserting (DBH 04/01/2006) #6645 - Disable, or hide if blank,  "not available" fault codes
        If maf:NotAvailable = True
            If Contents(f:Field) = ''
                f:Field{prop:Hide} = True
                f:Prompt{prop:Hide} = True
            Else ! If Contents(f:Field) = ''
                f:Field{prop:Disable} = True
            End ! If Contents(f:Field) = ''
            f:LookupField{prop:Hide} = True
        Else! If maf:NotAvailable = True
            f:Field{prop:Disable} = False
        End ! If maf:NotAvailable = True
        ! End (DBH 04/01/2006) #6645
    Else
        f:Prompt{prop:Hide} = True
        f:Field{prop:Hide} = True
        f:LookupField{prop:Hide} = True
        f:LookupDescription{prop:Hide} = True
    End ! If f:Hide
local.LookupButtonCode        Procedure(Long f:FieldNumber,String f:Field)
Code
    Access:MANFAULT.Clearkey(maf:Field_Number_Key)
    maf:Manufacturer    = f:Man
    maf:Field_Number    = f:FieldNumber
    If Access:MANFAULT.Tryfetch(maf:Field_Number_Key) = Level:Benign
        ! Found
        If maf:Field_Type = 'DATE'
            Return TINCALENDARStyle1(f:Field)
        Else ! If maf:Field_Type = 'DATE'
            glo:Select1 = f:Man
            glo:Select2 = f:FieldNumber
            glo:Select3 = ''
            SaveRequest# = GlobalRequest
            GlobalRequest = SelectRecord
            GlobalResponse = RequestCancelled
            Browse_Manufacturer_Fault_Lookup
            glo:Select1 = ''
            glo:Select2 = ''
            glo:Select3 = ''
            GlobalRequest = SaveRequest#

            If GlobalResponse = RequestCompleted
                Access:MANFAUEX.ClearKey(max:ModelNumberKey)
                max:MANFAULORecordNumber = mfo:RecordNumber
                max:ModelNumber          = job:Model_Number
                If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
                    !Found
                    Case Missive('The Model Number on this job has been "Excluded" from using the selected Fault Code.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                    Return f:Field
                Else !If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
                    !Error
                End !If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign

                f:Field = mfo:Field
                If f:Job
                    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
                    jbn:RefNumber   = f:Job
                    If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                        ! Found
                        If maf:ReplicateFault = 'YES'
                            If jbn:Fault_Description = ''
                                jbn:Fault_Description = Clip(mfo:Description)
                            Else ! If jbn:Fault_Description = ''
                                If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                                    jbn:Fault_Description = Clip(jbn:Fault_Description) & '<13,10>' & Clip(mfo:Description)
                                End ! If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                            End ! If jbn:Fault_Description = ''
                        End ! If maf:ReplicateFault = 'YES'

                        If maf:ReplicateInvoice = 'YES'
                            If jbn:Invoice_Text = ''
                                jbn:Invoice_Text = Clip(mfo:Description)
                            Else ! If jbn:Invoice_Text = ''
                                If ~Instring(Clip(mfo:Description),jbn:Invoice_Text,1,1)
                                    jbn:Invoice_Text = Clip(jbn:Invoice_Text) & '<13,10>' & Clip(mfo:Description)
                                End ! If ~Instring(Clip(mfo:Description),jbn:Invoice_Text,1,1)
                            End ! If jbn:Invoice_Text = ''
                        End ! If maf:ReplicateInvoice = 'YES'
                        Access:JOBNOTES.Update()
                    Else ! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                        ! Error
                    End ! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                End ! If f:Job
            End ! If GlobalResponse = RequestCompleted
            Return f:Field
        End ! If maf:Field_Type = 'DATE'
    Else ! If Access:MANFAULT.Tryfetch(maf:Field_Number_Key) = Level:Benign
        ! Error
    End ! If Access:MANFAULT.Tryfetch(maf:Field_Number_Key) = Level:Benign
local.LookupFieldCode      Procedure(Long f:FieldNumber, String f:Field, Long f:Lookup)
    Code
    If f:Field <> ''
        Access:MANFAULT.Clearkey(maf:Field_Number_Key)
        maf:Manufacturer = f:Man
        maf:Field_Number = f:FieldNumber
        If Access:MANFAULT.Tryfetch(maf:Field_Number_Key) = Level:Benign
            ! Found
            Access:MANFAULO.Clearkey(mfo:HideFieldKey)
            mfo:NotAvailable = False
            mfo:Manufacturer = f:Man
            mfo:Field_Number = f:FieldNumber
            mfo:Field        = f:Field
            If Access:MANFAULO.Tryfetch(mfo:HideFieldKey) = Level:Benign
                ! Found
                Access:MANFAUEX.ClearKey(max:ModelNumberKey)
                max:MANFAULORecordNumber = mfo:RecordNumber
                max:ModelNumber          = job:Model_Number
                If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
                    ! Found
                    Case Missive('The Model Number on this job has been "Excluded" from using the selected Fault Code.', 'ServiceBase 3g', |
                                 'mstop.jpg', '/OK')
                    Of 1 ! OK Button
                    End ! Case Missive
                    Return ''
                Else ! If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
                    ! Error
                    If maf:ReplicateFault = 'YES'
                        If jbn:Fault_Description = ''
                            jbn:Fault_Description = Clip(mfo:Description)
                        Else ! If jbn:Fault_Description = ''
                            If ~Instring(Clip(mfo:Description), jbn:Fault_Description, 1, 1)
                                jbn:Fault_Description = Clip(jbn:Fault_Description) & '<13,10>' & Clip(mfo:Description)
                            End ! If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                        End ! If jbn:Fault_Description = ''
                    End ! If maf:ReplicateFault = 'YES'

                    If maf:ReplicateInvoice = 'YES'
                        If jbn:Invoice_Text = ''
                            jbn:Invoice_Text = Clip(mfo:Description)
                        Else ! If jbn:Invoice_Text = ''
                            If ~Instring(Clip(mfo:Description), jbn:Invoice_Text, 1, 1)
                                jbn:Invoice_Text = Clip(jbn:Invoice_Text) & '<13,10>' & Clip(mfo:Description)
                            End ! If ~Instring(Clip(mfo:Description),jbn:Invoice_Text,1,1)
                        End ! If jbn:Invoice_Text = ''
                    End ! If maf:ReplicateInvoice = 'YES'
                    Access:JOBNOTES.Update()
                End ! If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
            Else ! If Access:MANFAULO.Tryfetch(mfo:HideFieldKey) = Level:Benign
                ! Error
                If maf:Force_Lookup = 'YES'
                    Post(Event:Accepted, f:Lookup)
                End ! If maf:Force_Lookup = 'YES'
            End ! If Access:MANFAULO.Tryfetch(mfo:HideFieldKey) = Level:Benign
        Else ! If Access:MANFAULT.Tryfetch(maf:Field_Number_Key) = Level:Benign
        ! Error
        End ! If Access:MANFAULT.Tryfetch(maf:Field_Number_Key) = Level:Benign
    End ! If f:Field <> ''
    Return f:Field
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Return_Exchange_To_Stock PROCEDURE                    !Generated from procedure template - Window

FilesOpened          BYTE
esn_temp             STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select1
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?GLO:Select2
los:Shelf_Location     LIKE(los:Shelf_Location)       !List box control field - type derived from field
los:RecordNumber       LIKE(los:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?GLO:Select3
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB1::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
FDCB2::View:FileDropCombo VIEW(LOCSHELF)
                       PROJECT(los:Shelf_Location)
                       PROJECT(los:RecordNumber)
                     END
FDCB4::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
window               WINDOW('Create Exchange Unit'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Create Exchange Unit'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Exchange Unit Location'),USE(?Tab1)
                           PROMPT('Amend I.M.E.I. Number if necessary'),AT(238,144),USE(?Prompt6),FONT(,8,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('I.M.E.I. Number'),AT(238,160),USE(?glo:select4:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s16),AT(314,160,124,10),USE(GLO:Select4),FONT(,,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Please select the Site Location / Shelf Location for this Exchange Unit:'),AT(238,176,188,16),USE(?Prompt1),FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Site Location'),AT(238,200),USE(?Prompt2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(314,200,124,10),USE(GLO:Select1),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M*@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           COMBO(@s40),AT(314,216,124,10),USE(GLO:Select2),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:1)
                           LINE,AT(239,232,196,0),USE(?Line1),COLOR(COLOR:Black)
                           PROMPT('This unit will now be marked as ''Despatched''. Please select the Courier.'),AT(238,240,204,16),USE(?Prompt4),FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           COMBO(@s40),AT(314,259,124,10),USE(GLO:Select3),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:2)
                           PROMPT('Courier'),AT(238,259),USE(?Prompt5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Shelf Location'),AT(238,216),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB1                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020430'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Return_Exchange_To_Stock')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:COURIER.Open
  SELF.FilesOpened = True
  esn_temp       = glo:select4
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Return_Exchange_To_Stock')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDCB1.Init(GLO:Select1,?GLO:Select1,Queue:FileDropCombo.ViewPosition,FDCB1::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB1.Q &= Queue:FileDropCombo
  FDCB1.AddSortOrder(loc:Location_Key)
  FDCB1.AddField(loc:Location,FDCB1.Q.loc:Location)
  FDCB1.AddField(loc:RecordNumber,FDCB1.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB1.WindowComponent)
  FDCB1.DefaultFill = 0
  FDCB2.Init(GLO:Select2,?GLO:Select2,Queue:FileDropCombo:1.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo:1,Relate:LOCSHELF,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo:1
  FDCB2.AddSortOrder(los:Shelf_Location_Key)
  FDCB2.AddRange(los:Site_Location,GLO:Select1)
  FDCB2.AddField(los:Shelf_Location,FDCB2.Q.los:Shelf_Location)
  FDCB2.AddField(los:RecordNumber,FDCB2.Q.los:RecordNumber)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  FDCB4.Init(GLO:Select3,?GLO:Select3,Queue:FileDropCombo:2.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo:2,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo:2
  FDCB4.AddSortOrder(cou:Courier_Key)
  FDCB4.AddField(cou:Courier,FDCB4.Q.cou:Courier)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Return_Exchange_To_Stock')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020430'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020430'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020430'&'0')
      ***
    OF ?GLO:Select1
      FDCB2.ResetQueue(1)
    OF ?OkButton
      ThisWindow.Update
      error# = 0
      If glo:select1 = ''
          error# = 1
          Select(?glo:select1)
      End!If glo:select1 = ''
      If glo:select3 = '' And error# = 0
          error# = 1
          Select(?glo:select3)
      End!If glo:select3 = '' And error# = 0
      If glo:select4 <> '' And error# = 0
          If glo:select4 <> esn_temp
              Case Missive('Are you sure you want to change the I.M.E.I. Number of this unit?','ServiceBase 3g',|
                             'mquest.jpg','Cancel|\No|/Yes')
                  Of 3 ! Yes Button
                      esn_temp = glo:select4
                  Of 2 ! No Button
                      glo:select4 = esn_temp
                  Of 1 ! Cancel Button
                      error# = 1
                      Select(?glo:select4)
              End ! Case Missive
          End!If glo:select4 <> esn_temp
      End!If glo:select4 <> '' And error# = 0
      If error# = 0
          Post(Event:CloseWindow)
      End!If error# = 0
      
    OF ?CancelButton
      ThisWindow.Update
      glo:select1 = ''
      glo:select2 = ''
      glo:select3 = ''
      glo:select4 = ''
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

FDCB1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1

Xfiles PROCEDURE                                      !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Caption'),AT(,,260,100),GRAY,DOUBLE
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Xfiles')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ADDSEARCH.Open
  Relate:EXPGEN.Open
  Relate:MULDESPJ.Open
  Relate:PARAMSS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Xfiles')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ADDSEARCH.Close
    Relate:EXPGEN.Close
    Relate:MULDESPJ.Close
    Relate:PARAMSS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Xfiles')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
QA_Failure_Reason PROCEDURE (f_reason,f_notes)        !Generated from procedure template - Window

FilesOpened          BYTE
Rejection_Reason_Temp STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Rejection_Notes_Temp STRING(255)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Rejection_Reason_Temp
qar:Reason             LIKE(qar:Reason)               !List box control field - type derived from field
qar:RecordNumber       LIKE(qar:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB3::View:FileDropCombo VIEW(QAREASON)
                       PROJECT(qar:Reason)
                       PROJECT(qar:RecordNumber)
                     END
window               WINDOW('QA Rejected Reason'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(163,70,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PROMPT('QA Rejection Reason'),AT(167,72),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(460,72),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       SHEET,AT(164,84,352,244),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Reason for REJECTING this item for QA:'),AT(267,160),USE(?Prompt1),FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           COMBO(@s255),AT(306,174,124,10),USE(Rejection_Reason_Temp),IMM,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('240L(2)|M@s60@'),DROP(10),FROM(Queue:FileDropCombo)
                           TEXT,AT(306,197,124,81),USE(Rejection_Notes_Temp),COLOR(COLOR:White),UPR
                           PROMPT('Reason'),AT(250,174),USE(?Prompt2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(448,329),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       PANEL,AT(163,329,352,26),USE(?PanelButton),FILL(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB3                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020457'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('QA_Failure_Reason')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:QAREASON.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Access:QAREASON.CLEARKEY(qar:ReasonKey)
  qar:Reason = 'ACCESSORIES MISSING'
  IF Access:QAREASON.Fetch(qar:ReasonKey) THEN
     Access:QAREASON.PrimeRecord()
     qar:Reason = 'ACCESSORIES MISSING'
     Access:QAREASON.TRYUPDATE()
  END !IF
  IF f_reason <> '' THEN
     Rejection_Reason_Temp = f_reason
  END !IF
      ! Save Window Name
   AddToLog('Window','Open','QA_Failure_Reason')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDCB3.Init(Rejection_Reason_Temp,?Rejection_Reason_Temp,Queue:FileDropCombo.ViewPosition,FDCB3::View:FileDropCombo,Queue:FileDropCombo,Relate:QAREASON,ThisWindow,GlobalErrors,0,1,0)
  FDCB3.Q &= Queue:FileDropCombo
  FDCB3.AddSortOrder(qar:ReasonKey)
  FDCB3.AddField(qar:Reason,FDCB3.Q.qar:Reason)
  FDCB3.AddField(qar:RecordNumber,FDCB3.Q.qar:RecordNumber)
  ThisWindow.AddItem(FDCB3.WindowComponent)
  FDCB3.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:QAREASON.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','QA_Failure_Reason')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020457'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020457'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020457'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      If rejection_reason_temp = ''
          Select(?rejection_reason_temp)
      Else!If rejection_reason_temp = ''
          f_reason = rejection_reason_temp
          f_notes = upper(clip(Rejection_Notes_Temp))
          post(event:closewindow)
      
      End!If rejection_reason_temp = ''
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      FDCB3.ResetQueue(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
InsertEngineerPassword PROCEDURE                      !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Password         STRING(30)
tmp:UserCode         STRING(3)
window               WINDOW('Insert Password'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(240,146,200,144),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert Password'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,164,192,90),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Insert Password'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(248,208,184,10),USE(tmp:Password),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Password'),TIP('Password'),UPR,PASSWORD
                         END
                       END
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:UserCode)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020452'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('InsertEngineerPassword')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:USERS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','InsertEngineerPassword')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:USERS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','InsertEngineerPassword')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020452'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020452'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020452'&'0')
      ***
    OF ?Cancel
      ThisWindow.Update
      tmp:UserCode = ''
      Post(Event:CloseWindow)
    OF ?OK
      ThisWindow.Update
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = tmp:Password
      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Found
          tmp:UserCode = use:User_Code
          Post(Event:CloseWindow)
      Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Error
          Case Missive('Invalid Password!','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Select(?tmp:Password)
      End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
UpdateMANFPALO PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::mfp:Record  LIKE(mfp:RECORD),STATIC
QuickWindow          WINDOW('Update the MANFAULO File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Fault Code'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Manufacturer'),AT(234,163),USE(?MFO:Manufacturer:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(322,163,124,10),USE(mfp:Manufacturer),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Field Number'),AT(234,182),USE(?glo:select3:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n2),AT(322,182,64,10),USE(mfp:Field_Number),SKIP,FONT(,,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),READONLY
                           PROMPT('Field'),AT(234,202),USE(?MFO:Field:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(322,202,124,10),USE(mfp:Field),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           PROMPT('Description'),AT(234,222),USE(?MFP:Description:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s60),AT(322,222,124,10),USE(mfp:Description),FONT(,,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           CHECK('Not Available'),AT(322,241),USE(mfp:NotAvailable),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Not Available'),TIP('Not Available'),VALUE('1','0')
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Fault Code'
  OF ChangeRecord
    ActionMessage = 'Changing A Fault Code'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020439'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateMANFPALO')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(mfp:Record,History::mfp:Record)
  SELF.AddHistoryField(?mfp:Manufacturer,2)
  SELF.AddHistoryField(?mfp:Field_Number,3)
  SELF.AddHistoryField(?mfp:Field,4)
  SELF.AddHistoryField(?mfp:Description,5)
  SELF.AddHistoryField(?mfp:NotAvailable,8)
  SELF.AddUpdateFile(Access:MANFPALO)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MANFPALO.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MANFPALO
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','UpdateMANFPALO')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANFPALO.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateMANFPALO')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    mfp:Manufacturer = glo:select1
    mfp:Field_Number = glo:select2
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020439'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020439'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020439'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Manufacturer_Part_Lookup PROCEDURE             !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(MANFPALO)
                       PROJECT(mfp:Field)
                       PROJECT(mfp:Description)
                       PROJECT(mfp:RecordNumber)
                       PROJECT(mfp:Manufacturer)
                       PROJECT(mfp:NotAvailable)
                       PROJECT(mfp:Field_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
mfp:Field              LIKE(mfp:Field)                !List box control field - type derived from field
mfp:Description        LIKE(mfp:Description)          !List box control field - type derived from field
mfp:RecordNumber       LIKE(mfp:RecordNumber)         !Primary key field - type derived from field
mfp:Manufacturer       LIKE(mfp:Manufacturer)         !Browse key field - type derived from field
mfp:NotAvailable       LIKE(mfp:NotAvailable)         !Browse key field - type derived from field
mfp:Field_Number       LIKE(mfp:Field_Number)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK10::mfp:Field_Number    LIKE(mfp:Field_Number)
HK10::mfp:Manufacturer    LIKE(mfp:Manufacturer)
HK10::mfp:NotAvailable    LIKE(mfp:NotAvailable)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Browse the Fault Code Lookup File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Fault Code Lookup File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(449,300),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                       LIST,AT(168,112,344,186),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('81L(2)|M~Field~@s30@120L(2)|M~Description~@s60@'),FROM(Queue:Browse:1)
                       BUTTON,AT(168,300),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       BUTTON,AT(236,300),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                       BUTTON,AT(304,300),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Field'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,98,124,10),USE(mfp:Field),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('Description'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s60),AT(168,98,124,10),USE(mfp:Description),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(449,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020437'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Manufacturer_Part_Lookup')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:MANFAULT.Open
  Relate:USERS_ALIAS.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MANFPALO,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Browse_Manufacturer_Part_Lookup')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,mfp:AvailableDescriptionKey)
  BRW1.AddRange(mfp:Field_Number)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?mfp:Description,mfp:Description,1,BRW1)
  BRW1.AddSortOrder(,mfp:AvailableFieldKey)
  BRW1.AddRange(mfp:Field_Number)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?mfp:Field,mfp:Field,1,BRW1)
  BIND('GLO:Select1',GLO:Select1)
  BIND('GLO:Select2',GLO:Select2)
  BRW1.AddField(mfp:Field,BRW1.Q.mfp:Field)
  BRW1.AddField(mfp:Description,BRW1.Q.mfp:Description)
  BRW1.AddField(mfp:RecordNumber,BRW1.Q.mfp:RecordNumber)
  BRW1.AddField(mfp:Manufacturer,BRW1.Q.mfp:Manufacturer)
  BRW1.AddField(mfp:NotAvailable,BRW1.Q.mfp:NotAvailable)
  BRW1.AddField(mfp:Field_Number,BRW1.Q.mfp:Field_Number)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Field'
    ?Tab2{PROP:TEXT} = 'Description'
    ?Browse:1{PROP:FORMAT} ='81L(2)|M~Field~@s30@#1#120L(2)|M~Description~@s60@#2#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:MANFAULT.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Manufacturer_Part_Lookup')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = True
  
    case request
        of insertrecord
            If SecurityCheck('FAULT CODE LOOKUP - INSERT')
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Access:MANFPALO.CancelAutoInc()
                do_update# = false
            end!If SecurityCheck('FAULT CODE LOOKUP - INSERT')
        of changerecord
            If SecurityCheck('FAULT CODE LOOKUP - CHANGE')
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
  
                do_update# = false
            end!If SecurityCheck('FAULT CODE LOOKUP - CHANGE')
        of deleterecord
            If SecurityCheck('FAULT CODE LOOKUP - DELETE')
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
  
                do_update# = false
            end!If SecurityCheck('FAULT CODE LOOKUP - DELETE')
    end !case request
  
    If do_update# = True
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMANFPALO
    ReturnValue = GlobalResponse
  END
  End!If do_update# = True
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020437'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020437'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020437'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='81L(2)|M~Field~@s30@#1#120L(2)|M~Description~@s60@#2#'
          ?Tab:2{PROP:TEXT} = 'By Field'
        OF 2
          ?Browse:1{PROP:FORMAT} ='120L(2)|M~Description~@s60@#2#81L(2)|M~Field~@s30@#1#'
          ?Tab2{PROP:TEXT} = 'Description'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?mfp:Field
      Select(?Browse:1)
    OF ?mfp:Description
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      If GETINI('BROWSEORDER','FaultCode',,CLIP(PATH())&'\SB2KDEF.INI') = 1
          Select(?CurrentTab,2)
          Post(Event:NewSelection,?CurrentTab)
      End !GETINI('BROWSEORDER','FaultCode',,CLIP(PATH())&'\SB2KDEF.INI') = 1
    OF EVENT:GainFocus
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = GLO:Select1
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = GLO:Select2
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = GLO:Select1
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = GLO:Select2
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  if mfp:NotAvailable = true then return(record:filtered).
  
  ! Check for restrictions - TrkBs: 5365 (DBH: 27-04-2005)
  If mfp:RestrictLookup = True
      Case glo:Select4
          Of 'CORRECT PART'
              If mfp:RestrictLookupType <> 2
                  Return Record:Filtered
              End ! If mfp:RestrictLookupType <> 2
          Of 'ADJUSTMENT'
              If mfp:RestrictLookupType <> 3
                  Return Record:Filtered
              End ! If mfp:RestrictLookupType <> 3
          Else
              If mfp:RestrictLookupType <> 1
                  Return Record:Filtered
              End ! If mfp:RestrictLookupType <> 1
      End ! Case glo:Select4
  End ! mfp:RestrictLookup = True
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Model_Stock PROCEDURE (f_model_number,f_user_code) !Generated from procedure template - Window

CurrentTab           STRING(80)
model_number_temp    STRING(30)
LocalRequest         LONG
FilesOpened          BYTE
location_temp        STRING(30)
no_temp              STRING('NO')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:one              BYTE(1)
tmp:SkillLevel       LONG
RelocatedSite        STRING(30)
RelocatedMark_Up     REAL
InWarr               DECIMAL(7,2)
OutWarr              DECIMAL(7,2)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?location_temp
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(STOMODEL)
                       PROJECT(stm:Location)
                       PROJECT(stm:Description)
                       PROJECT(stm:Part_Number)
                       PROJECT(stm:Ref_Number)
                       PROJECT(stm:RecordNumber)
                       PROJECT(stm:Model_Number)
                       JOIN(sto:Ref_Part_Description_Key,stm:Location,stm:Ref_Number,stm:Part_Number,stm:Description)
                         PROJECT(sto:Purchase_Cost)
                         PROJECT(sto:Sale_Cost)
                         PROJECT(sto:Quantity_Stock)
                         PROJECT(sto:Ref_Number)
                         PROJECT(sto:E1)
                         PROJECT(sto:E2)
                         PROJECT(sto:E3)
                         PROJECT(sto:Suspend)
                         PROJECT(sto:Location)
                         PROJECT(sto:Part_Number)
                         PROJECT(sto:Description)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
stm:Location           LIKE(stm:Location)             !List box control field - type derived from field
stm:Description        LIKE(stm:Description)          !List box control field - type derived from field
stm:Part_Number        LIKE(stm:Part_Number)          !List box control field - type derived from field
InWarr                 LIKE(InWarr)                   !List box control field - type derived from local data
OutWarr                LIKE(OutWarr)                  !List box control field - type derived from local data
sto:Purchase_Cost      LIKE(sto:Purchase_Cost)        !List box control field - type derived from field
sto:Sale_Cost          LIKE(sto:Sale_Cost)            !List box control field - type derived from field
sto:Quantity_Stock     LIKE(sto:Quantity_Stock)       !List box control field - type derived from field
stm:Ref_Number         LIKE(stm:Ref_Number)           !List box control field - type derived from field
sto:Ref_Number         LIKE(sto:Ref_Number)           !List box control field - type derived from field
sto:E1                 LIKE(sto:E1)                   !List box control field - type derived from field
sto:E2                 LIKE(sto:E2)                   !List box control field - type derived from field
sto:E3                 LIKE(sto:E3)                   !List box control field - type derived from field
sto:Suspend            LIKE(sto:Suspend)              !List box control field - type derived from field
stm:RecordNumber       LIKE(stm:RecordNumber)         !Primary key field - type derived from field
stm:Model_Number       LIKE(stm:Model_Number)         !Browse key field - type derived from field
sto:Location           LIKE(sto:Location)             !Related join file key field - type derived from field
sto:Part_Number        LIKE(sto:Part_Number)          !Related join file key field - type derived from field
sto:Description        LIKE(sto:Description)          !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
QuickWindow          WINDOW('Browse the Stock File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Stock File'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,54,552,20),USE(?Panel1),FILL(09A6A7CH)
                       PROMPT('Site Location'),AT(68,58),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       COMBO(@s30),AT(152,58,124,10),USE(location_temp),SKIP,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)*@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                       LIST,AT(152,108,392,250),USE(?Browse:1),IMM,HVSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('120L(2)|M~Location~@s30@124L(2)|M~Description~@s30@117L(2)|M~Part Number~@s30@40' &|
   'R(2)|M~In Warr~L@n-10.2@40R(2)|M~Out Warr~L@n-10.2@0R(2)|M@n7.2@0R(2)|M@n7.2@47R' &|
   '(2)|M~Qty In Stock~L@N8@48L(2)|M~Ref Number~@n012@22L(2)|M~Reference Number~@s8@' &|
   '4L(2)|M~E1~@n1@4L(2)|M~E2~@n1@4L(2)|M~E3~@n1@0L(2)|M~Suspend Part~@n1@'),FROM(Queue:Browse:1)
                       BUTTON,AT(548,164),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                       SHEET,AT(64,76,552,288),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Description'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(152,92,124,10),USE(stm:Description),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Part Number'),USE(?Tab:6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(152,92,124,10),USE(stm:Part_Number),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort1:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass CLASS(StepStringClass)          !Default Step Manager
Init                   PROCEDURE(BYTE Controls,BYTE Mode)
                     END

BRW1::Sort1:StepClass CLASS(StepStringClass)          !Conditional Step Manager - Choice(?CurrentTab) = 2
Init                   PROCEDURE(BYTE Controls,BYTE Mode)
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020441'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Model_Stock')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  Relate:LOCATION.Open
  Access:MODELNUM.UseFile
  Access:STOCK.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  error# = 1
  tmp:SkillLevel = 0
  
  !will need the user open and fetched for eslewhere
  If f_user_code <> ''
      access:users.clearkey(use:user_code_key)
      use:user_code = f_user_code
      if access:users.fetch(use:user_code_key) = Level:Benign
          If use:location <> ''
              error# = 0
              location_temp = use:location
          End!If use:location <> ''
          tmp:SkillLevel = use:SkillLevel
      End!if access:users.fetch(use:user_code_key) = Level:Benign
  End!If glo:select11 <> ''
  
  if glo:RelocateStore then
      !relocated this to the main store
      Location_Temp = 'MAIN STORE'        !was 'Main Store'
      error# = 0
      !will use glo:RelocatedFrom and glo:RelocatedMark_Up
  END !if relocated to main store
  
  If error# = 1
      Set(defstock)
      Access:defstock.next()
      If dst:site_location <> ''
          location_temp = dst:site_location
      End
  End!If error# = 1
  
  Access:LOCATION.ClearKey(loc:ActiveLocationKey)
  loc:Active   = 1
  loc:Location = Location_Temp
  If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign
      !Found
  
  Else!If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
      Location_Temp = ''
  End!If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign
  
  stm:model_number = Upper(f_model_number)
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOMODEL,SELF)
  brw1.SelectWholeRecord = True
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !Have already got the user
  If use:StockFromLocationOnly
      ?Location_Temp{prop:Disable} = 1
  End !de2:PickEngStockOnly
  
      ! Save Window Name
   AddToLog('Window','Open','Browse_Model_Stock')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,stm:Location_Part_Number_Key)
  BRW1.AddRange(stm:Location,location_temp)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?stm:Part_Number,stm:Part_Number,1,BRW1)
  BRW1.AddResetField(location_temp)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,stm:Location_Description_Key)
  BRW1.AddRange(stm:Location,location_temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?stm:Description,stm:Description,1,BRW1)
  BRW1.AddResetField(location_temp)
  BIND('InWarr',InWarr)
  BIND('OutWarr',OutWarr)
  BIND('GLO:Select12',GLO:Select12)
  BIND('location_temp',location_temp)
  BIND('no_temp',no_temp)
  BIND('model_number_temp',model_number_temp)
  BRW1.AddField(stm:Location,BRW1.Q.stm:Location)
  BRW1.AddField(stm:Description,BRW1.Q.stm:Description)
  BRW1.AddField(stm:Part_Number,BRW1.Q.stm:Part_Number)
  BRW1.AddField(InWarr,BRW1.Q.InWarr)
  BRW1.AddField(OutWarr,BRW1.Q.OutWarr)
  BRW1.AddField(sto:Purchase_Cost,BRW1.Q.sto:Purchase_Cost)
  BRW1.AddField(sto:Sale_Cost,BRW1.Q.sto:Sale_Cost)
  BRW1.AddField(sto:Quantity_Stock,BRW1.Q.sto:Quantity_Stock)
  BRW1.AddField(stm:Ref_Number,BRW1.Q.stm:Ref_Number)
  BRW1.AddField(sto:Ref_Number,BRW1.Q.sto:Ref_Number)
  BRW1.AddField(sto:E1,BRW1.Q.sto:E1)
  BRW1.AddField(sto:E2,BRW1.Q.sto:E2)
  BRW1.AddField(sto:E3,BRW1.Q.sto:E3)
  BRW1.AddField(sto:Suspend,BRW1.Q.sto:Suspend)
  BRW1.AddField(stm:RecordNumber,BRW1.Q.stm:RecordNumber)
  BRW1.AddField(stm:Model_Number,BRW1.Q.stm:Model_Number)
  BRW1.AddField(sto:Location,BRW1.Q.sto:Location)
  BRW1.AddField(sto:Part_Number,BRW1.Q.sto:Part_Number)
  BRW1.AddField(sto:Description,BRW1.Q.sto:Description)
  QuickWindow{PROP:MinWidth}=584
  QuickWindow{PROP:MinHeight}=210
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  FDCB6.Init(location_temp,?location_temp,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(loc:ActiveLocationKey)
  FDCB6.AddRange(loc:Active,tmp:one)
  FDCB6.AddField(loc:Location,FDCB6.Q.loc:Location)
  FDCB6.AddField(loc:RecordNumber,FDCB6.Q.loc:RecordNumber)
  FDCB6.AddUpdateField(loc:Location,location_temp)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab2{PROP:TEXT} = 'By Description'
    ?Tab:6{PROP:TEXT} = 'By Part Number'
    ?Browse:1{PROP:FORMAT} ='124L(2)|M~Description~@s30@#2#117L(2)|M~Part Number~@s30@#3#40R(2)|M~In Warr~L@n-10.2@#4#40R(2)|M~Out Warr~L@n-10.2@#5#47R(2)|M~Qty In Stock~L@N8@#8#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
    Relate:LOCATION.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Model_Stock')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Select:2
          Pointer# = Pointer(Stock)
          Hold(Stock,1)
          Get(Stock,Pointer#)
          If Errorcode() = 43
              Case Missive('This stock item is currently in use by another station. Please try again later.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End !If Errorcode() = 43
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020441'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020441'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020441'&'0')
      ***
    OF ?location_temp
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?CurrentTab
        SELECT(?Browse:1)                   ! Reselect list box after tab change
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='124L(2)|M~Description~@s30@#2#117L(2)|M~Part Number~@s30@#3#40R(2)|M~In Warr~L@n-10.2@#4#40R(2)|M~Out Warr~L@n-10.2@#5#47R(2)|M~Qty In Stock~L@N8@#8#'
          ?Tab2{PROP:TEXT} = 'By Description'
        OF 2
          ?Browse:1{PROP:FORMAT} ='117L(2)|M~Part Number~@s30@#3#124L(2)|M~Description~@s30@#2#40R(2)|M~In Warr~L@n-10.2@#4#40R(2)|M~Out Warr~L@n-10.2@#5#47R(2)|M~Qty In Stock~L@N8@#8#'
          ?Tab:6{PROP:TEXT} = 'By Part Number'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ?WindowTitle{prop:text} = 'Available Stock For Model: ' & Clip(f_model_number)
      BRW1.ResetSort(1)
      Select(?Browse:1)
    OF EVENT:GainFocus
      BRW1.ResetSort(1)
      FDCB6.ResetQueue(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  !And engineer should only see parts that are equal or lower to his skill level
  !THIS MAY SLOW DOWN THE BROWSE TOO MUCH!
  
  If tmp:SkillLevel <> 0
      Case tmp:SkillLevel
          Of 1
              If ~sto:E1
                  Return Record:Filtered
              End !If sto:E1
          Of 2
              If ~(sto:E1 Or sto:E2)
                  Return Record:Filtered
              End !If ~sto:E1 and ~sto:E2
          Of 3
              If ~(sto:E1 Or sto:E2 Or sto:E3)
                  Return Record:Filtered
              End !If ~sto:E1 And ~sto:E2 And ~sto:E3
      End !Case tmp:SkillLevel
  End !tmp:SkillLevel <> 0
  
  !Don't show suspended parts (hopefully this won't slow things down) - L913 (DBH: 07-08-2003)
  If sto:Suspend
      Return Record:Filtered
  End !sto:Suspend
  
  
  if glo:RelocateStore then
      inWarr  = sto:Sale_Cost * (1 + InWarrantyMarkup(sto:Manufacturer,glo:RelocatedFrom)/100)
      outWarr = sto:Sale_Cost * (1 + glo:RelocatedMark_Up/100)
  ELSE
      InWarr  = sto:purchase_cost
      OutWarr = sto:Sale_Cost
  END
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue


BRW1::Sort0:StepClass.Init PROCEDURE(BYTE Controls,BYTE Mode)

  CODE
  stm:model_number = Upper(f_model_number)
  
  
  PARENT.Init(Controls,Mode)


BRW1::Sort1:StepClass.Init PROCEDURE(BYTE Controls,BYTE Mode)

  CODE
  stm:model_number = Upper(f_model_number)
  
  
  PARENT.Init(Controls,Mode)


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Panel1, Resize:FixLeft+Resize:FixTop, Resize:LockHeight)
  SELF.SetStrategy(?location_temp, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?STM:Part_Number, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?STM:Description, Resize:FixLeft+Resize:FixTop, Resize:LockSize)


FDCB6.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1

