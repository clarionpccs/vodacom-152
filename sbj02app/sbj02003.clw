

   MEMBER('sbj02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ02003.INC'),ONCE        !Local module procedure declarations
                     END



FaultCodeExcludedModels PROCEDURE                     !Generated from procedure template - Browse

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::11:TAGDISPSTATUS   BYTE(0)
DASBRW::11:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::12:TAGDISPSTATUS   BYTE(0)
DASBRW::12:QUEUE          QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Tag1             STRING(1)
tmp:Tag2             STRING(1)
BRW9::View:Browse    VIEW(MANFAUEX)
                       PROJECT(max:ModelNumber)
                       PROJECT(max:RecordNumber)
                       PROJECT(max:MANFAULORecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:Tag1               LIKE(tmp:Tag1)                 !List box control field - type derived from local data
tmp:Tag1_Icon          LONG                           !Entry's icon ID
max:ModelNumber        LIKE(max:ModelNumber)          !List box control field - type derived from field
max:RecordNumber       LIKE(max:RecordNumber)         !Primary key field - type derived from field
max:MANFAULORecordNumber LIKE(max:MANFAULORecordNumber) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                       PROJECT(mod:Manufacturer)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:Tag2               LIKE(tmp:Tag2)                 !List box control field - type derived from local data
tmp:Tag2_Icon          LONG                           !Entry's icon ID
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
mod:Manufacturer       LIKE(mod:Manufacturer)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,54,552,308),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Excluded Model Numbers'),AT(68,62),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('All Available Model Numbers'),AT(400,62),USE(?Prompt4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(68,78,124,10),USE(max:ModelNumber),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Model Number')
                       ENTRY(@s30),AT(400,78,124,10),USE(mod:Model_Number),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       LIST,AT(68,92,212,206),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11L(2)J@s1@120L(2)|M~Model Number~@s30@'),FROM(Queue:Browse)
                       LIST,AT(400,92,212,206),USE(?List:2),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11L(2)J@s1@120L(2)|M~Model Number~@s30@'),FROM(Queue:Browse:1)
                       BUTTON('&Rev tags'),AT(480,202,50,13),USE(?DASREVTAG:2),HIDE
                       BUTTON('&Rev tags'),AT(153,206,50,13),USE(?DASREVTAG),HIDE
                       BUTTON('sho&W tags'),AT(488,234,70,13),USE(?DASSHOWTAG:2),HIDE
                       BUTTON('sho&W tags'),AT(145,238,70,13),USE(?DASSHOWTAG),HIDE
                       BUTTON,AT(68,304),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                       BUTTON,AT(140,304),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                       BUTTON,AT(212,304),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                       BUTTON,AT(400,304),USE(?DASTAG:2),TRN,FLAT,ICON('tagitemp.jpg')
                       BUTTON,AT(472,304),USE(?DASTAGAll:2),TRN,FLAT,ICON('tagallp.jpg')
                       BUTTON,AT(544,304),USE(?DASUNTAGALL:2),TRN,FLAT,ICON('untagalp.jpg')
                       BUTTON,AT(212,333),USE(?Button:RemoveTaggedModels),TRN,FLAT,ICON('remmodp.jpg')
                       BUTTON,AT(400,332),USE(?Button:AddTaggedModels),TRN,FLAT,ICON('addmodp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Excluded Model Numbers'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(544,366),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW9                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW9::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW10                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW10::Sort0:Locator EntryLocatorClass                !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::11:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW9.UpdateBuffer
   glo:Queue.Pointer = max:RecordNumber
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = max:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:Tag1 = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:Tag1 = ''
  END
    Queue:Browse.tmp:Tag1 = tmp:Tag1
  IF (tmp:tag1 = '*')
    Queue:Browse.tmp:Tag1_Icon = 2
  ELSE
    Queue:Browse.tmp:Tag1_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW9.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = max:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW9.Reset
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::11:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::11:QUEUE = glo:Queue
    ADD(DASBRW::11:QUEUE)
  END
  FREE(glo:Queue)
  BRW9.Reset
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::11:QUEUE.Pointer = max:RecordNumber
     GET(DASBRW::11:QUEUE,DASBRW::11:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = max:RecordNumber
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASSHOWTAG Routine
   CASE DASBRW::11:TAGDISPSTATUS
   OF 0
      DASBRW::11:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::11:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::11:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW9.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::12:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW10.UpdateBuffer
   glo:Queue2.Pointer2 = mod:Model_Number
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = mod:Model_Number
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tmp:Tag2 = '*'
  ELSE
    DELETE(glo:Queue2)
    tmp:Tag2 = ''
  END
    Queue:Browse:1.tmp:Tag2 = tmp:Tag2
  IF (tmp:Tag2 = '*')
    Queue:Browse:1.tmp:Tag2_Icon = 2
  ELSE
    Queue:Browse:1.tmp:Tag2_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::12:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW10.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW10::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = mod:Model_Number
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::12:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW10.Reset
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::12:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::12:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::12:QUEUE = glo:Queue2
    ADD(DASBRW::12:QUEUE)
  END
  FREE(glo:Queue2)
  BRW10.Reset
  LOOP
    NEXT(BRW10::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::12:QUEUE.Pointer2 = mod:Model_Number
     GET(DASBRW::12:QUEUE,DASBRW::12:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = mod:Model_Number
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::12:DASSHOWTAG Routine
   CASE DASBRW::12:TAGDISPSTATUS
   OF 0
      DASBRW::12:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::12:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::12:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW10.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020636'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('FaultCodeExcludedModels')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANFAUEX.Open
  Access:MANFAULO.UseFile
  SELF.FilesOpened = True
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:MANFAUEX,SELF)
  BRW10.Init(?List:2,Queue:Browse:1.ViewPosition,BRW10::View:Browse,Queue:Browse:1,Relate:MODELNUM,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','FaultCodeExcludedModels')
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW9.Q &= Queue:Browse
  BRW9.AddSortOrder(,max:ModelNumberKey)
  BRW9.AddRange(max:MANFAULORecordNumber,mfo:RecordNumber)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(?max:ModelNumber,max:ModelNumber,1,BRW9)
  BIND('tmp:Tag1',tmp:Tag1)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW9.AddField(tmp:Tag1,BRW9.Q.tmp:Tag1)
  BRW9.AddField(max:ModelNumber,BRW9.Q.max:ModelNumber)
  BRW9.AddField(max:RecordNumber,BRW9.Q.max:RecordNumber)
  BRW9.AddField(max:MANFAULORecordNumber,BRW9.Q.max:MANFAULORecordNumber)
  BRW10.Q &= Queue:Browse:1
  BRW10.AddSortOrder(,mod:Manufacturer_Key)
  BRW10.AddRange(mod:Manufacturer,GLO:Select1)
  BRW10.AddLocator(BRW10::Sort0:Locator)
  BRW10::Sort0:Locator.Init(?mod:Model_Number,mod:Model_Number,1,BRW10)
  BIND('tmp:Tag2',tmp:Tag2)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW10.AddField(tmp:Tag2,BRW10.Q.tmp:Tag2)
  BRW10.AddField(mod:Model_Number,BRW10.Q.mod:Model_Number)
  BRW10.AddField(mod:Manufacturer,BRW10.Q.mod:Manufacturer)
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  BRW10.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW10.AskProcedure = 0
      CLEAR(BRW10.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:MANFAUEX.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','FaultCodeExcludedModels')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Button:RemoveTaggedModels
      ThisWindow.Update
      Case Missive('Are you sure you want to remove the Tagged Model Numbers from the "Excluded Model Number" list?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              Loop x# = 1 To Records(glo:Queue)
                  Get(glo:Queue,x#)
                  Access:MANFAUEX.ClearKey(max:RecordNumberKey)
                  max:RecordNumber = glo:Pointer
                  If Access:MANFAUEX.TryFetch(max:RecordNumberKey) = Level:Benign
                      !Found
                      Relate:MANFAUEX.Delete(0)
                  Else !If Access:MANFAUEX.TryFetch(max:RecordNumberKey) = Level:Benign
                      !Error
                  End !If Access:MANFAUEX.TryFetch(max:RecordNumberKey) = Level:Benign
              End ! Loop x# = 1 To Records(glo:Queue)
              Post(Event:Accepted,?DASUNTAGALL)
          Of 1 ! No Button
      End ! Case Missive
      BRW10.ResetSort(1)
      BRW9.ResetSort(1)
    OF ?Button:AddTaggedModels
      ThisWindow.Update
      Case Missive('Are you sure you want to add the tagged Model Numbers to the "Excluded Model" list?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              Loop x# = 1 To Records(glo:Queue2)
                  Get(glo:Queue2,x#)
                  If Access:MANFAUEX.PrimeRecord() = Level:Benign
                      max:MANFAULORecordNumber = mfo:RecordNumber
                      max:ModelNumber = glo:Pointer2
                      If Access:MANFAUEX.TryInsert() = Level:Benign
                          ! Insert Successful
      
                      Else ! If Access:MANFAUEX.TryInsert() = Level:Benign
                          ! Insert Failed
                          Access:MANFAUEX.CancelAutoInc()
                      End ! If Access:MANFAUEX.TryInsert() = Level:Benign
                  End !If Access:MANFAUEX.PrimeRecord() = Level:Benign
              End ! Loop x# = 1 To Records(glo:Queue2)
              Post(Event:Accepted,?DASUNTAGALL:2)
          Of 1 ! No Button
      End ! Case Missive
      BRW10.ResetSort(1)
      BRW9.ResetSort(1)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020636'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020636'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020636'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      If KeyCode() = MouseLeft2
          Post(Event:Accepted,?DasTag)
      End ! If KeyCode() = MouseLeft2
    END
  OF ?List:2
    CASE EVENT()
    OF EVENT:AlertKey
      If KeyCode() = MouseLeft2
          Post(Event:Accepted,?DasTag:2)
      End ! If KeyCode() = MouseLeft2
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW9.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = max:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:Tag1 = ''
    ELSE
      tmp:Tag1 = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag1 = '*')
    SELF.Q.tmp:Tag1_Icon = 2
  ELSE
    SELF.Q.tmp:Tag1_Icon = 1
  END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW9.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW9::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW9::RecordStatus=ReturnValue
  IF BRW9::RecordStatus NOT=Record:OK THEN RETURN BRW9::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = max:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::11:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW9::RecordStatus
  RETURN ReturnValue


BRW10.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = mod:Model_Number
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tmp:Tag2 = ''
    ELSE
      tmp:Tag2 = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Tag2 = '*')
    SELF.Q.tmp:Tag2_Icon = 2
  ELSE
    SELF.Q.tmp:Tag2_Icon = 1
  END


BRW10.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW10.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW10::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  ! Do not display model that has already been exlcuded - TrkBs: 5904 (DBH: 23-09-2005)
  Access:MANFAUEX.ClearKey(max:ModelNumberKey)
  max:MANFAULORecordNumber = mfo:RecordNumber
  max:ModelNumber          = mod:Model_Number
  If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
      !Found
      Return Record:Filtered
  Else !If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
      !Error
  End !If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
  BRW10::RecordStatus=ReturnValue
  IF BRW10::RecordStatus NOT=Record:OK THEN RETURN BRW10::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = mod:Model_Number
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::12:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW10::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW10::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW10::RecordStatus
  RETURN ReturnValue

UpdateMANFAULO PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
No                   STRING('NO {1}')
YES                  STRING('YES')
History::mfo:Record  LIKE(mfo:RECORD),STATIC
QuickWindow          WINDOW('Update the MANFAULO File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Job Fault Code Lookup'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,552,310),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           GROUP,AT(68,238,435,70),USE(?Group:OutFault),HIDE
                             CHECK('Set Job Fault Code'),AT(168,238),USE(mfo:SetJobFaultCode),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Set Job Fault Code'),TIP('Set Job Fault Code'),VALUE('1','0')
                             GROUP,AT(68,250,435,20),USE(?Group:JobFaultCode)
                               BUTTON,AT(476,250),USE(?Lookup:JobFaultCode),TRN,FLAT,ICON('lookupp.jpg')
                               PROMPT('Select Job Fault Code'),AT(68,255),USE(?mfo:SelectJobFaultCode:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               SPIN(@s8),AT(168,255,64,10),USE(mfo:SelectJobFaultCode),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Select Job Fault Code'),TIP('Select Job Fault Code'),REQ,UPR,RANGE(1,12),STEP(1)
                               PROMPT('Job Fault Code Value'),AT(256,255),USE(?mfo:JobFaultCodeValue:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s30),AT(348,255,124,10),USE(mfo:JobFaultCodeValue),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Job Fault Code Value'),TIP('Job Fault Code Value'),REQ,UPR
                             END
                             PROMPT('Repair Index'),AT(68,270),USE(?mfo:ImportanceLevel:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             SPIN(@n8),AT(168,270,64,10),USE(mfo:ImportanceLevel),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Importance Level'),TIP('Importance Level'),UPR,RANGE(0,10),STEP(1)
                             CHECK('Hide From Engineer'),AT(168,284),USE(mfo:HideFromEngineer),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Hide From Engineer'),TIP('Hide From Engineer'),VALUE('1','0')
                             CHECK('Return To RRC'),AT(168,298),USE(mfo:ReturnToRRC),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Return To RRC'),TIP('Return To RRC'),VALUE('1','0')
                           END
                           PROMPT('Manufacturer'),AT(68,90),USE(?MFO:Manufacturer:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,90,124,10),USE(mfo:Manufacturer),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           CHECK('Not Available'),AT(168,60),USE(mfo:NotAvailable),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Not Available'),TIP('Not Available'),VALUE('1','0')
                           PROMPT('Field'),AT(68,104),USE(?MFO:Field:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,104,124,10),USE(mfo:Field),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           CHECK('Primary Lookup'),AT(168,74),USE(mfo:PrimaryLookup),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Primary Lookup'),TIP('Primary Lookup'),VALUE('1','0')
                           PROMPT('Description'),AT(68,118),USE(?MFO:Description:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s60),AT(168,118,124,10),USE(mfo:Description),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Skill Level'),AT(68,132),USE(?mfo:SkillLevel:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n8),AT(168,132,64,10),USE(mfo:SkillLevel),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Skill Level'),TIP('Skill Level'),UPR,RANGE(0,10),STEP(1)
                           STRING('Chargeable Repair Type'),AT(68,148),USE(?String1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,148,124,10),USE(mfo:RepairType),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Repair Type'),TIP('Repair Type'),UPR
                           BUTTON,AT(296,144),USE(?LookupCRepairTypes),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           STRING('Warranty Repair Type'),AT(68,166),USE(?String1:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,166,124,10),USE(mfo:RepairTypeWarranty),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Repair Type'),TIP('Repair Type'),UPR
                           BUTTON,AT(296,164),USE(?LookupWRepairTypes),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Force Part Fault Code'),AT(68,182),USE(?Prompt7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@s8),AT(168,182,64,10),USE(mfo:ForcePartCode),FONT(,,010101H,FONT:bold),COLOR(COLOR:White),RANGE(0,12),STEP(1),MSG('Force Related Part Code')
                           PROMPT('Related Part Fault Code'),AT(68,196),USE(?Prompt7:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@s8),AT(168,196,64,10),USE(mfo:RelatedPartCode),FONT(,,010101H,FONT:bold),COLOR(COLOR:White),RANGE(0,12),STEP(1),MSG('Related Part Fault Code')
                           CHECK('Prompt For Exchange'),AT(168,210),USE(mfo:PromptForExchange),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           CHECK('Exclude From Bouncer Table'),AT(168,222),USE(mfo:ExcludeFromBouncer),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Exclude From Bouncer Table'),TIP('Exclude From Bouncer Table'),VALUE('1','0')
                           OPTION,AT(476,66,132,56),USE(mfo:RestrictLookupType),BOXED,MSG('Restrict Lookup Type')
                             RADIO('Part Used'),AT(484,76),USE(?mfo:RestrictLookupType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Part Correction'),AT(484,90),USE(?mfo:RestrictLookupType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                             RADIO('Adjustment'),AT(484,103),USE(?mfo:RestrictLookupType:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('3')
                           END
                           OPTION('Job Type Availability'),AT(476,126,132,56),USE(mfo:JobTypeAvailability),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Job Type Availability')
                             RADIO('Chargeable Jobs'),AT(484,138),USE(?mfo:JobTypeAvailability:Radio4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Warranty Jobs'),AT(484,152),USE(?mfo:JobTypeAvailability:Radio5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                             RADIO('Both'),AT(484,166),USE(?mfo:JobTypeAvailability:Radio6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                           END
                           CHECK('Restrict Lookup To'),AT(476,58),USE(mfo:RestrictLookup),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0'),MSG('Restrict Lookup To')
                         END
                       END
                       BUTTON,AT(480,366),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(548,366),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(68,366),USE(?Button:ExcludedModelNumbers),TRN,FLAT,ICON('excmodp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:mfo:RepairType                Like(mfo:RepairType)
look:mfo:RepairTypeWarranty                Like(mfo:RepairTypeWarranty)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Fault Code'
  OF ChangeRecord
    ActionMessage = 'Changing A Fault Code'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020438'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateMANFAULO')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(mfo:Record,History::mfo:Record)
  SELF.AddHistoryField(?mfo:SetJobFaultCode,20)
  SELF.AddHistoryField(?mfo:SelectJobFaultCode,21)
  SELF.AddHistoryField(?mfo:JobFaultCodeValue,22)
  SELF.AddHistoryField(?mfo:ImportanceLevel,6)
  SELF.AddHistoryField(?mfo:HideFromEngineer,10)
  SELF.AddHistoryField(?mfo:ReturnToRRC,15)
  SELF.AddHistoryField(?mfo:Manufacturer,2)
  SELF.AddHistoryField(?mfo:NotAvailable,18)
  SELF.AddHistoryField(?mfo:Field,4)
  SELF.AddHistoryField(?mfo:PrimaryLookup,19)
  SELF.AddHistoryField(?mfo:Description,5)
  SELF.AddHistoryField(?mfo:SkillLevel,7)
  SELF.AddHistoryField(?mfo:RepairType,8)
  SELF.AddHistoryField(?mfo:RepairTypeWarranty,9)
  SELF.AddHistoryField(?mfo:ForcePartCode,11)
  SELF.AddHistoryField(?mfo:RelatedPartCode,12)
  SELF.AddHistoryField(?mfo:PromptForExchange,13)
  SELF.AddHistoryField(?mfo:ExcludeFromBouncer,14)
  SELF.AddHistoryField(?mfo:RestrictLookupType,17)
  SELF.AddHistoryField(?mfo:JobTypeAvailability,23)
  SELF.AddHistoryField(?mfo:RestrictLookup,16)
  SELF.AddUpdateFile(Access:MANFAULO)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MANFAULO.Open
  Relate:MANFAULO_ALIAS.Open
  Access:REPTYDEF.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MANFAULO
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If maf:MainFault
      ?Group:OutFault{prop:Hide} = 0
  End !maf:MainFault
      ! Save Window Name
   AddToLog('Window','Open','UpdateMANFAULO')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?mfo:RepairType{Prop:Tip} AND ~?LookupCRepairTypes{Prop:Tip}
     ?LookupCRepairTypes{Prop:Tip} = 'Select ' & ?mfo:RepairType{Prop:Tip}
  END
  IF ?mfo:RepairType{Prop:Msg} AND ~?LookupCRepairTypes{Prop:Msg}
     ?LookupCRepairTypes{Prop:Msg} = 'Select ' & ?mfo:RepairType{Prop:Msg}
  END
  IF ?mfo:RepairTypeWarranty{Prop:Tip} AND ~?LookupWRepairTypes{Prop:Tip}
     ?LookupWRepairTypes{Prop:Tip} = 'Select ' & ?mfo:RepairTypeWarranty{Prop:Tip}
  END
  IF ?mfo:RepairTypeWarranty{Prop:Msg} AND ~?LookupWRepairTypes{Prop:Msg}
     ?LookupWRepairTypes{Prop:Msg} = 'Select ' & ?mfo:RepairTypeWarranty{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?mfo:SetJobFaultCode{Prop:Checked} = True
    ENABLE(?Group:JobFaultCode)
  END
  IF ?mfo:SetJobFaultCode{Prop:Checked} = False
    DISABLE(?Group:JobFaultCode)
  END
  IF ?mfo:RestrictLookup{Prop:Checked} = True
    ENABLE(?mfo:RestrictLookupType)
  END
  IF ?mfo:RestrictLookup{Prop:Checked} = False
    DISABLE(?mfo:RestrictLookupType)
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANFAULO.Close
    Relate:MANFAULO_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateMANFAULO')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickChargeableRepairTypes
      PickWarrantyRepairTypes
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020438'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020438'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020438'&'0')
      ***
    OF ?mfo:SetJobFaultCode
      IF ?mfo:SetJobFaultCode{Prop:Checked} = True
        ENABLE(?Group:JobFaultCode)
      END
      IF ?mfo:SetJobFaultCode{Prop:Checked} = False
        DISABLE(?Group:JobFaultCode)
      END
      ThisWindow.Reset
    OF ?Lookup:JobFaultCode
      ThisWindow.Update
      Access:MANFAULO_ALIAS.Clearkey(mfo_ali:FieldNumberKey)
      mfo_ali:Manufacturer = mfo:Manufacturer
      mfo_ali:Field_Number = mfo:SelectJobFaultCode
      If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:FieldNumberKey) = Level:Benign
      
      Else ! If Access:MANFAULT_ALIAS.TryFetch(mfo_ali:Field_Number_Key) = Level:Benign
          Case Missive('Cannot find any lookups for the selected fault code number.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End ! If Access:MANFAULT_ALIAS.TryFetch(mfo_ali:Field_Number_Key) = Level:Benign
      Request# = GlobalRequest
      GlobalRequest = SelectRecord
      BrowseJobFaultCodeLookup_Alias(mfo:Manufacturer,mfo:SelectJobFaultCode,'')
      GlobalRequest = Request#
      Case GlobalResponse
          Of Requestcompleted
              mfo:JobFaultCodeValue = mfo_ali:Field
          Of Requestcancelled
      End!Case Globalreponse
      Display()
    OF ?mfo:RepairType
      IF mfo:RepairType OR ?mfo:RepairType{Prop:Req}
        rtd:Repair_Type = mfo:RepairType
        rtd:Chargeable = 'YES'
        rtd:Manufacturer = mfo:Manufacturer
        !Save Lookup Field Incase Of error
        look:mfo:RepairType        = mfo:RepairType
        IF Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            mfo:RepairType = rtd:Repair_Type
          ELSE
            CLEAR(rtd:Chargeable)
            CLEAR(rtd:Manufacturer)
            !Restore Lookup On Error
            mfo:RepairType = look:mfo:RepairType
            SELECT(?mfo:RepairType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupCRepairTypes
      ThisWindow.Update
      rtd:Repair_Type = mfo:RepairType
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          mfo:RepairType = rtd:Repair_Type
          Select(?+1)
      ELSE
          Select(?mfo:RepairType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?mfo:RepairType)
    OF ?mfo:RepairTypeWarranty
      IF mfo:RepairTypeWarranty OR ?mfo:RepairTypeWarranty{Prop:Req}
        rtd:Repair_Type = mfo:RepairTypeWarranty
        rtd:Manufacturer = mfo:Manufacturer
        rtd:Warranty = 'YES'
        !Save Lookup Field Incase Of error
        look:mfo:RepairTypeWarranty        = mfo:RepairTypeWarranty
        IF Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            mfo:RepairTypeWarranty = rtd:Repair_Type
          ELSE
            CLEAR(rtd:Manufacturer)
            CLEAR(rtd:Warranty)
            !Restore Lookup On Error
            mfo:RepairTypeWarranty = look:mfo:RepairTypeWarranty
            SELECT(?mfo:RepairTypeWarranty)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupWRepairTypes
      ThisWindow.Update
      rtd:Repair_Type = mfo:RepairTypeWarranty
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          mfo:RepairTypeWarranty = rtd:Repair_Type
          Select(?+1)
      ELSE
          Select(?mfo:RepairTypeWarranty)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?mfo:RepairTypeWarranty)
    OF ?mfo:RestrictLookup
      IF ?mfo:RestrictLookup{Prop:Checked} = True
        ENABLE(?mfo:RestrictLookupType)
      END
      IF ?mfo:RestrictLookup{Prop:Checked} = False
        DISABLE(?mfo:RestrictLookupType)
      END
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?Button:ExcludedModelNumbers
      ThisWindow.Update
      FaultCodeExcludedModels
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If mfo:PrimaryLookup
      Found# = 0
      Access:MANFAULO_ALIAS.Clearkey(mfo_ali:Field_Key)
      mfo_ali:Manufacturer = mfo:Manufacturer
      mfo_ali:Field_Number = mfo:Field_Number
      Set(mfo_ali:Field_Key,mfo_ali:Field_Key)
      Loop
          If Access:MANFAULO_ALIAS.Next()
              Break
          End ! If Access:MANFAULO_ALIAS.Next()
          If mfo_ali:Manufacturer <> mfo:Manufacturer
              Break
          End ! If mfo_ali:Manufacturer <> mfo:Manufacturer
          If mfo_ali:Field_Number <> mfo:Field_Number
              Break
          End ! If mfo_ali:Field_Number <> mfo:Field_Number
          If mfo_ali:RecordNumber = mfo:RecordNumber
              Cycle
          End ! If mfo_ali:RecordNumber = mfo:RecordNumber
          If mfo_ali:PrimaryLookup
              Found# = 1
              Break
          End ! If mfo_ali:PrimaryLookup
      End ! Loop
  
      If Found#
          Case Missive('Error! The lookup value "' & Clip(mfo_ali:Field) & '" has already been set as the "Primary Lookup".','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          mfo:PrimaryLookup = 0
          Display()
          Cycle
      End ! If Found#
  End ! If mfo:PrimaryLookup
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Manufacturer_Fault_Lookup PROCEDURE            !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
BRW1::View:Browse    VIEW(MANFAULO)
                       PROJECT(mfo:Field)
                       PROJECT(mfo:Description)
                       PROJECT(mfo:RecordNumber)
                       PROJECT(mfo:NotAvailable)
                       PROJECT(mfo:Manufacturer)
                       PROJECT(mfo:Field_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
mfo:Field              LIKE(mfo:Field)                !List box control field - type derived from field
mfo:Description        LIKE(mfo:Description)          !List box control field - type derived from field
mfo:RecordNumber       LIKE(mfo:RecordNumber)         !Primary key field - type derived from field
mfo:NotAvailable       LIKE(mfo:NotAvailable)         !Browse key field - type derived from field
mfo:Manufacturer       LIKE(mfo:Manufacturer)         !Browse key field - type derived from field
mfo:Field_Number       LIKE(mfo:Field_Number)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK12::mfo:Field_Number    LIKE(mfo:Field_Number)
HK12::mfo:Manufacturer    LIKE(mfo:Manufacturer)
HK12::mfo:NotAvailable    LIKE(mfo:NotAvailable)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Browse the Fault Code Lookup File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Fault Code Lookup File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,112,344,186),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('125L(2)|M~Field~@s30@240L(2)|M~Description~@s60@'),FROM(Queue:Browse:1)
                       BUTTON,AT(448,300),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                       BUTTON,AT(168,300),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       BUTTON,AT(236,300),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                       BUTTON,AT(304,300),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Field'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,98,,10),USE(mfo:Field),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Description'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s60),AT(168,98,124,10),USE(mfo:Description),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020436'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Manufacturer_Fault_Lookup')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:MANFAULO.Open
  Relate:USERS_ALIAS.Open
  Access:MANFAULT.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MANFAULO,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Browse_Manufacturer_Fault_Lookup')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,mfo:HideDescriptionKey)
  BRW1.AddRange(mfo:Field_Number)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?mfo:Description,mfo:Description,1,BRW1)
  BRW1.AddSortOrder(,mfo:HideFieldKey)
  BRW1.AddRange(mfo:Field_Number)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?mfo:Field,mfo:Field,1,BRW1)
  BIND('GLO:Select1',GLO:Select1)
  BIND('GLO:Select2',GLO:Select2)
  BRW1.AddField(mfo:Field,BRW1.Q.mfo:Field)
  BRW1.AddField(mfo:Description,BRW1.Q.mfo:Description)
  BRW1.AddField(mfo:RecordNumber,BRW1.Q.mfo:RecordNumber)
  BRW1.AddField(mfo:NotAvailable,BRW1.Q.mfo:NotAvailable)
  BRW1.AddField(mfo:Manufacturer,BRW1.Q.mfo:Manufacturer)
  BRW1.AddField(mfo:Field_Number,BRW1.Q.mfo:Field_Number)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Field'
    ?Tab2{PROP:TEXT} = 'By Description'
    ?Browse:1{PROP:FORMAT} ='125L(2)|M~Field~@s30@#1#240L(2)|M~Description~@s60@#2#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:MANFAULO.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Manufacturer_Fault_Lookup')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = True
  
  case request
      of insertrecord
          If SecurityCheck('FAULT CODE LOOKUP - INSERT')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Access:MANFAULO.CancelAutoInc()
              do_update# = false
          end!If SecurityCheck('FAULT CODE LOOKUP - INSERT')
      of changerecord
          If SecurityCheck('FAULT CODE LOOKUP - CHANGE')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
  
              do_update# = false
          end!If SecurityCheck('FAULT CODE LOOKUP - CHANGE')
      of deleterecord
          If SecurityCheck('FAULT CODE LOOKUP - DELETE')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
  
              do_update# = false
          end!If SecurityCheck('FAULT CODE LOOKUP - DELETE')
  end !case request
  
  If do_update# = True
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMANFAULO
    ReturnValue = GlobalResponse
  END
  End!If do_update# = True
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020436'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020436'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020436'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='125L(2)|M~Field~@s30@#1#240L(2)|M~Description~@s60@#2#'
          ?Tab:2{PROP:TEXT} = 'By Field'
        OF 2
          ?Browse:1{PROP:FORMAT} ='240L(2)|M~Description~@s60@#2#125L(2)|M~Field~@s30@#1#'
          ?Tab2{PROP:TEXT} = 'By Description'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?mfo:Field
      Select(?Browse:1)
    OF ?mfo:Description
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      If GETINI('BROWSEORDER','FaultCode',,CLIP(PATH())&'\SB2KDEF.INI') = 1
          Select(?CurrentTab,2)
          Post(Event:NewSelection,?CurrentTab)
      End !GETINI('BROWSEORDER','FaultCode',,CLIP(PATH())&'\SB2KDEF.INI') = 1
    OF EVENT:GainFocus
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = GLO:Select1
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = GLO:Select2
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = GLO:Select1
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = GLO:Select2
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  ! Check for restrictions - TrkBs: 5365 (DBH: 27-04-2005)
  If mfo:RestrictLookup = True
      Case glo:Select4
          Of 'CORRECT PART'
              If mfo:RestrictLookupType <> 2
                  Return Record:Filtered
              End ! If mfo:RestrictLookupType <> 2
          Of 'ADJUSTMENT'
              If mfo:RestrictLookupType <> 3
                  Return Record:Filtered
              End ! If mfo:RestrictLookupType <> 3
          Else
              If mfo:RestrictLookupType <> 1
                  Return Record:Filtered
              End ! If mfo:RestrictLookupType <> 1
      End ! Case glo:Select4
  End ! mfo:RestrictLookup = True
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Update_Estimate_Part PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
sav:ExcludeFromOrder BYTE(0)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?epr:Supplier
sup:Company_Name       LIKE(sup:Company_Name)         !List box control field - type derived from field
sup:RecordNumber       LIKE(sup:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(SUPPLIER)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:RecordNumber)
                     END
History::epr:Record  LIKE(epr:RECORD),STATIC
QuickWindow          WINDOW('Update the WARPARTS File'),AT(,,288,180),FONT('Tahoma',8,,),CENTER,IMM,HLP('Update_Warranty_Part'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,280,144),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Estimate Part'),AT(8,20),USE(?Prompt8),FONT('Tahoma',10,COLOR:Navy,FONT:bold)
                           PROMPT('Part Number'),AT(8,36),USE(?WPR:Part_Number:Prompt),TRN
                           ENTRY(@s30),AT(84,36,124,10),USE(epr:Part_Number),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           BUTTON('Browse Stock'),AT(212,36,68,10),USE(?browse_stock_button),SKIP,LEFT,ICON('book.ico')
                           PROMPT('Description'),AT(8,52),USE(?WPR:Description:Prompt),TRN
                           ENTRY(@s30),AT(84,48,124,10),USE(epr:Description),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('Purchase Cost'),AT(8,64),USE(?WPR:Purchase_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,64,64,10),USE(epr:Purchase_Cost),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Sale Cost'),AT(8,76),USE(?WPR:Sale_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,76,64,10),USE(epr:Sale_Cost),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Supplier'),AT(8,92),USE(?Prompt9)
                           COMBO(@s30),AT(84,92,124,10),USE(epr:Supplier),VSCROLL,FONT('Tahoma',8,,FONT:bold),UPR,FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Quantity'),AT(8,108),USE(?Prompt7)
                           SPIN(@p<<<<<<<#p),AT(84,108,64,10),USE(epr:Quantity),RIGHT,FONT('Tahoma',8,,FONT:bold),RANGE(1,99999999),STEP(1),MSG('Quantity')
                           CHECK('Exclude From Ordering / Decrementing'),AT(84,128,136,8),USE(epr:Exclude_From_Order),VALUE('YES','NO')
                         END
                       END
                       PANEL,AT(4,152,280,24),USE(?Panel1),FILL(0D6E7EFH)
                       BUTTON('&OK'),AT(168,156,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(224,156,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting An Estimate Part'
  OF ChangeRecord
    ActionMessage = 'Changing An Estimate Part'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Estimate_Part')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt8
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(epr:Record,History::epr:Record)
  SELF.AddHistoryField(?epr:Part_Number,5)
  SELF.AddHistoryField(?epr:Description,6)
  SELF.AddHistoryField(?epr:Purchase_Cost,8)
  SELF.AddHistoryField(?epr:Sale_Cost,9)
  SELF.AddHistoryField(?epr:Supplier,7)
  SELF.AddHistoryField(?epr:Quantity,11)
  SELF.AddHistoryField(?epr:Exclude_From_Order,13)
  SELF.AddUpdateFile(Access:ESTPARTS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  Relate:ESTPARTS.Open
  Relate:USERS_ALIAS.Open
  Access:WARPARTS.UseFile
  Access:JOBS.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ESTPARTS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !Save Fields
  sav:ExcludeFromORder  = epr:Exclude_From_Order
      ! Save Window Name
   AddToLog('Window','Open','Update_Estimate_Part')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB6.Init(epr:Supplier,?epr:Supplier,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:SUPPLIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(sup:Company_Name_Key)
  FDCB6.AddField(sup:Company_Name,FDCB6.Q.sup:Company_Name)
  FDCB6.AddField(sup:RecordNumber,FDCB6.Q.sup:RecordNumber)
  FDCB6.AddUpdateField(sup:Company_Name,epr:Supplier)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
    Relate:ESTPARTS.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Update_Estimate_Part')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    epr:Adjustment = 'NO'
    epr:Warranty_Part = 'NO'
    epr:Exclude_From_Order = 'NO'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?epr:Exclude_From_Order
      If ~0{prop:acceptall}
          If sav:ExcludeFromOrder <> epr:Exclude_From_Order
              If SecurityCheck('PARTS - EXCLUDE FROM ORDER')
                  BEEP(BEEP:SystemHand)  ;  YIELD()
                  CASE MESSAGE('You do not have access to this option.', |
                          'ServiceBase 2000', ICON:Hand, |
                           BUTTON:OK, BUTTON:OK, 0)
                  Of BUTTON:OK
                  End !CASE
                  epr:Exclude_From_Order = sav:ExcludeFromOrder
              Else!If SecurityCheck('EXCLUDE FROM ORDER')
      !            If thiswindow.request = Insertrecord
                      If epr:Exclude_From_Order = 'YES' And epr:part_ref_number <> ''
                          BEEP(BEEP:SystemQuestion)  ;  YIELD()
                          CASE MESSAGE('You have selected to "Exclude From Order".'&|
                                  '||Warning! This stock part will NOT be decremented, and an '&|
                                  'order will NOT be raised if there are insufficient items in '&|
                                  'stock.'&|
                                  '||Are you sure you want to continue?', |
                                  'ServiceBase 2000', ICON:Question, |
                                   BUTTON:Yes+BUTTON:No, BUTTON:No, 0)
                          Of BUTTON:Yes
                              sav:ExcludeFromOrder    = epr:Exclude_From_Order
                          Of BUTTON:No
                              epr:Exclude_From_Order  = sav:ExcludeFromOrder
                          End !CASE
                      End!If epr:Exclude_From_Order = 'YES'
      !            End!If thiswindow.request = Insertrecord
              End!If SecurityCheck('EXCLUDE FROM ORDER')
          End!If sav:ExcludeFromOrder <> epr:Exclude_From_Order
      End!If ~0{prop:acceptall}
      Display()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?epr:Part_Number
      If ~0{prop:acceptall}
      error# = 0
      access:users.clearkey(use:user_code_key)
      use:user_code = job:engineer
      if access:users.tryfetch(use:user_code_key)
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
              Access:LOCATION.ClearKey(loc:Location_Key)
              loc:Location = use:Location
              If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
                  !Found
                  If loc:Active = 0
                      Error# = 3
                  End !If loc:Active = 0
              Else!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
              If Error# = 0
                  access:stock.clearkey(sto:location_manufacturer_key)
                  sto:location     = use:location
                  sto:manufacturer = job:manufacturer
                  sto:part_number  = epr:part_number
                  if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                      Access:STOMODEL.ClearKey(stm:Mode_Number_Only_Key)
                      stm:Ref_Number   = sto:Ref_Number
                      stm:Model_Number = job:Model_Number
                      If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                          !Found
                          If sto:Suspend
                              error# = 2
                          Else !If sto:Suspended
                              If use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel
                                  error# = 4
                              Else !If use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel
                                  error# = 0
                              End !If use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel
      
                          End !If sto:Suspended
                      Else!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                          !Error
                          error# = 1
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                  Else!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                      error# = 1
                  End!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
              End !If Error# = 0
          End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      
      Else!if access:users.tryfetch(use:user_code_key) = Level:Benign
          Access:LOCATION.ClearKey(loc:Location_Key)
          loc:Location = use:Location
          If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
              !Found
              If loc:Active = 0
                  Error# = 3
              End !If loc:Active = 0
          Else!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
          If Error# = 0
              access:stock.clearkey(sto:location_manufacturer_key)
              sto:location     = use:location
              sto:manufacturer = job:manufacturer
              sto:part_number  = epr:part_number
              if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                  Access:STOMODEL.ClearKey(stm:Mode_Number_Only_Key)
                  stm:Ref_Number   = sto:Ref_Number
                  stm:Model_Number = job:Model_Number
                  If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                      !Found
                      If sto:Suspend
                          error# = 2
                      Else !If sto:Suspended
                          error# = 0
                      End !If sto:Suspended
                  Else!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                      !Error
                      error# = 1
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
      
              Else!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                  error# = 1
              End!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
          End !If Error# = 0
      End!if access:users.tryfetch(use:user_code_key) = Level:Benign
      
      Case error#
          Of 0
              epr:part_number     = sto:part_number
              epr:description     = sto:description
              epr:supplier        = sto:supplier
              epr:purchase_cost   = sto:purchase_cost
              epr:sale_cost       = sto:sale_cost
              epr:part_ref_number = sto:ref_number
              If sto:assign_fault_codes = 'YES'
                  Access:STOMODEL.ClearKey(stm:Model_Number_Key)
                  stm:Ref_Number   = sto:Ref_Number
                  stm:Manufacturer = sto:Manufacturer
                  stm:Model_Number = job:Model_Number
                  If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                      epr:fault_code1    = stm:Faultcode1
                      epr:fault_code2    = stm:Faultcode2
                      epr:fault_code3    = stm:Faultcode3
                      epr:fault_code4    = stm:Faultcode4
                      epr:fault_code5    = stm:Faultcode5
                      epr:fault_code6    = stm:Faultcode6
                      epr:fault_code7    = stm:Faultcode7
                      epr:fault_code8    = stm:Faultcode8
                      epr:fault_code9    = stm:Faultcode9
                      epr:fault_code10   = stm:Faultcode10
                      epr:fault_code11   = stm:Faultcode11
                      epr:fault_code12   = stm:Faultcode12
      
                  End!If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
              End!If sto:assign_fault_codes = 'YES'
              Select(?epr:quantity)
              display()
      
          Of 1
      
              BEEP(BEEP:SystemQuestion)  ;  YIELD()
              CASE MESSAGE('Cannot find the selected Part!'&|
                      '||It does either not exist in the Engineer''s Location, or '&|
                      'is not in stock.'&|
                      '||Do you wish SELECT another part, or CONTINUE using the entered '&|
                      'part  number?', |
                      'ServiceBase 2000', ICON:Question, |
                       'Select|Continue', 2, 0)
              Of 1  ! Name: Select
                      Post(event:accepted,?browse_stock_button)
              Of 2  ! Name: Continue  (Default)
                      Select(?epr:description)
              End !CASE
          Of 2
              BEEP(BEEP:SystemHand)  ;  YIELD()
              CASE MESSAGE('This part cannot be used. It has been suspended.', |
                      'ServiceBase 2000', ICON:Hand, |
                       '&OK', 1, 0)
              Of 1  ! Name: &OK  (Default)
              End !CASE
              epr:Part_Number = ''
              Select(?epr:Part_Number)
          Of 3
              BEEP(BEEP:SystemHand)  ;  YIELD()
              CASE MESSAGE('This part cannot be used. It has been suspended.', |
                      'ServiceBase 2000', ICON:Hand, |
                       '&OK', 1, 0)
              Of 1  ! Name: &OK  (Default)
              End !CASE
              epr:Part_Number = ''
              Select(?epr:Part_Number)
          Of 4
              Case Missive('Error! Your skill level is not high enough to use the selected part.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              epr:Part_Number = ''
              Select(?epr:Part_Number)
      End!If error# = 0
      Display()
      End!If ~0{prop:acceptall}
    OF ?browse_stock_button
      ThisWindow.Update
      Error# = 0
      user_code"  = ''
      If job:engineer <> ''
          Access:USERS.Clearkey(use:User_Code_Key)
          use:User_Code   = job:Engineer
          If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              !Found
              user_code"  = use:User_Code
          Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      
      
      Else!If job:engineer <> ''
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
              User_Code"  = use:User_Code
          Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      
      End!If job:engineer <> ''
      
      Access:LOCATION.ClearKey(loc:ActiveLocationKey)
      loc:Active   = 1
      loc:Location = use:Location
      If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign
          !Found
      
      Else!lIf Access:LOCATION.TryFetch(loc:ActiveLocationKey). = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
          BEEP(BEEP:SystemHand)  ;  YIELD()
          CASE MESSAGE('This part cannot be used. It has been suspended.', |
                  'ServiceBase 2000', ICON:Hand, |
                   '&OK', 1, 0)
          Of 1  ! Name: &OK  (Default)
          End !CASE
          If use:StockFromLocationOnly
              Error# = 1
          End !If use:StockFromLocationOnly
      End!If Access:LOCATION.TryFetch(loc:ActiveLocationKey). = Level:Benign
      
      If Error# = 0
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browse_model_stock(job:model_number,user_code")
          if globalresponse = requestcompleted
              access:stock.clearkey(sto:ref_number_key)
              sto:ref_number = stm:ref_number
      
              if access:stock.fetch(sto:ref_number_key)
                  beep(beep:systemhand)  ;  yield()
                  message('Error! Cannot access the Stock File.', |
                          'ServiceBase 2000', icon:hand)
              Else!if access:stock.fetch(sto:ref_number_key)
                  If sto:Suspend
                      BEEP(BEEP:SystemHand)  ;  YIELD()
                      CASE MESSAGE('This part cannot be used. It has been suspended.', |
                              'ServiceBase 2000', ICON:Hand, |
                               '&OK', 1, 0)
                      Of 1  ! Name: &OK  (Default)
                      End !CASE
                      epr:Part_Number = ''
                      Select(?epr:Part_Number)
                  Else !If sto:Suspend
      
                      If use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel
                          Case Missive('Error! Your skill level is not high enough to use the selected part.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          epr:Part_Number = ''
                          Select(?epr:Part_Number)
                      Else !If use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel
                          epr:part_number     = sto:part_number
                          epr:description     = sto:description
                          epr:supplier        = sto:supplier
                          epr:purchase_cost   = sto:purchase_cost
                          epr:sale_cost       = sto:sale_cost
                          epr:part_ref_number = sto:ref_number
                          If sto:assign_fault_codes = 'YES'
                              stm:Manufacturer = sto:Manufacturer
                              stm:Model_Number = job:Model_Number
                              If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                                  epr:fault_code1    = stm:Faultcode1
                                  epr:fault_code2    = stm:Faultcode2
                                  epr:fault_code3    = stm:Faultcode3
                                  epr:fault_code4    = stm:Faultcode4
                                  epr:fault_code5    = stm:Faultcode5
                                  epr:fault_code6    = stm:Faultcode6
                                  epr:fault_code7    = stm:Faultcode7
                                  epr:fault_code8    = stm:Faultcode8
                                  epr:fault_code9    = stm:Faultcode9
                                  epr:fault_code10   = stm:Faultcode10
                                  epr:fault_code11   = stm:Faultcode11
                                  epr:fault_code12   = stm:Faultcode12
      
                              End!If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                          End!If sto:assign_fault_codes = 'YES'
                          Select(?epr:quantity)
      
                      End !If use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel
                  End !If sto:Suspend
                  display()
              end!if access:stock.fetch(sto:ref_number_key)
          end
          globalrequest     = saverequest#
      End !Error# = 0
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

PartsKey PROCEDURE                                    !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Parts Key'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Parts Colour Key'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           GROUP('Key'),AT(278,144,124,120),USE(?Group1),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PANEL,AT(286,157,12,8),USE(?Panel1),FILL(COLOR:Green)
                             PROMPT('- Part Requested'),AT(306,157),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PANEL,AT(286,173,12,8),USE(?Panel1:2),FILL(COLOR:Red)
                             PROMPT('- Part On Order'),AT(306,173),USE(?Prompt1:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PANEL,AT(286,189,12,8),USE(?Panel1:3),FILL(COLOR:Fuschia)
                             PROMPT('- Part Awaiting Picking'),AT(306,189),USE(?Prompt1:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('- Part Picked'),AT(306,205),USE(?Prompt1:5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('- Part Allocated'),AT(306,221),USE(?Prompt1:6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PANEL,AT(286,237,12,8),USE(?Panel1:7),FILL(08080FFH)
                             PROMPT('- Part Awaiting Return'),AT(306,237),USE(?Prompt1:7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PANEL,AT(286,205,12,8),USE(?Panel1:5),FILL(COLOR:Blue)
                             PANEL,AT(286,221,12,8),USE(?Panel1:6),FILL(010101H)
                             PANEL,AT(286,253,12,8),USE(?Panel1:4),FILL(COLOR:Purple)
                             PROMPT('- Parts On Web Order'),AT(306,253),USE(?Prompt1:4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020454'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('PartsKey')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','PartsKey')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','PartsKey')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020454'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020454'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020454'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
CreateWebOrder       PROCEDURE  (func:Type,func:Quantity) ! Declare Procedure
save_orw_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:AccountNumber    STRING(30)
  CODE
!Write the part details in the "pending web order" file
!Using either the web account, if a web job, or the default account number

!Ok, try and find an existing order?
    If glo:WebJob
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = ClarioNET:Global.Param2
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:StoresAccount <> ''
                tmp:AccountNumber   = tra:StoresAccount
            Else !If tra:StoresAccount <> ''
                tmp:AccountNumber   = ClarioNET:Global.Param2
            End !If tra:StoresAccount <> ''
        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign


    Else !If glo:WebJob
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:StoresAccount <> ''
                tmp:AccountNumber   = tra:StoresAccount
            Else !If tra:StoresAccount <> ''
                tmp:AccountNumber   = Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))
            End !If tra:StoresAccount <> ''
        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    End !If glo:WebJob

    Found# = 0
    Save_orw_ID = Access:ORDWEBPR.SaveFile()
    Access:ORDWEBPR.ClearKey(orw:PartNumberKey)
    orw:AccountNumber = tmp:AccountNumber
    Case func:Type
        Of 'C' !Chargeable Part
            orw:PartNumber  = par:Part_Number
        Of 'W' !Warranty Part
            orw:PartNumber  = wpr:Part_Number
        Of 'S' !Stock Part
            orw:PartNumber  = sto:Part_Number
    End !Case func:Type

    Set(orw:PartNumberKey,orw:PartNumberKey)
    Loop
        If Access:ORDWEBPR.NEXT()
           Break
        End !If
        If orw:AccountNumber <> tmp:AccountNumber
            Break
        End !If orw:AccountNumber <> tmp:AccountNumber

        Case func:Type
            Of 'C'
                If orw:PartNumber <> par:Part_Number
                    Break
                End !If orw:PartNumber <> par:Part_Number
                If orw:Description  = par:Description
                    orw:Quantity += func:Quantity
                    Access:ORDWEBPR.TryUpdate()
                    Found# = 1
                    Break
                End !If orw:Description  = wpr:Description
            Of 'W'
                If orw:PartNumber <> wpr:Part_Number
                    Break
                End !If orw:PartNumber <> wpr:Part_Number
                If orw:Description  = wpr:Description
                    orw:Quantity += func:Quantity
                    Access:ORDWEBPR.TryUpdate()
                    Found# = 1
                    Break
                End !If orw:Description  = wpr:Description
            Of 'S'
                If orw:PartNumber <> sto:Part_Number
                    Break
                End !If orw:PartNumber <> sto:Part_Number
                If orw:Description = sto:Description
                    orw:Quantity += func:Quantity
                End !If orw:Description = sto:Description
        End !Case func:Type
    End !Loop
    Access:ORDWEBPR.RestoreFile(Save_orw_ID)

    If Found# = 0
        !Create a new "Pending" web order
        If Access:ORDWEBPR.PrimeRecord() = Level:Benign
            orw:AccountNumber = tmp:AccountNumber

            Case func:Type
                Of 'C'
                    orw:PartNumber    = par:Part_Number
                    orw:Description   = par:Description
                    orw:Quantity      = func:Quantity
                    orw:ItemCost      = par:Retail_Cost
                Of 'W'
                    orw:PartNumber    = wpr:Part_Number
                    orw:Description   = wpr:Description
                    orw:Quantity      = func:Quantity
                    orw:ItemCost      = wpr:Retail_Cost
                Of 'S'
                    orw:PartNumber    = sto:Part_Number
                    orw:Description   = sto:Description
                    orw:Quantity      = func:Quantity
                    orw:ItemCost      = sto:Retail_Cost
            End !Case func:Type
            If Access:ORDWEBPR.TryInsert() = Level:Benign
                !Insert Successful
            Else !If Access:ORDWEBPR.TryInsert() = Level:Benign
                !Insert Failed
                Access:ORDWEBPR.CancelAutoInc()
            End !If Access:ORDWEBPR.TryInsert() = Level:Benign
        End !If Access:ORDWEBPR.PrimeRecord() = Level:Benign
    End !If Found# = 0


Billing_Confirmation PROCEDURE (charge_type_s,charge_rep_type,warranty_type,warranty_rep_type,func:OverwriteC,func:OverwriteW,func:Type) !Generated from procedure template - Window

Warranty_Job         STRING('''NO''')
Chargeable_Job       STRING('''NO''')
Charge_Type          STRING(30)
Warranty_Charge_Type STRING(30)
Repair_Type          STRING(30)
Repair_Type_Warranty STRING(30)
Password             STRING(20)
tmp:OverwriteCRepairType BYTE(0)
tmp:OverwriteWRepairType BYTE(0)
sav:ChargeType       STRING(30)
sav:WarrantyChargeType STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Billing Confirmation'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Billing Confirmation'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Billing Confirmation'),USE(?Tab1)
                           STRING('Charge Type'),AT(176,152),USE(?String1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(241,152,124,10),USE(Charge_Type),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),INS,UPR,READONLY
                           BUTTON,AT(369,150),USE(?LookupCChargeType),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Repair Type'),AT(176,174),USE(?Repair_Type:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(241,174,124,10),USE(Repair_Type),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),INS,UPR,READONLY
                           BUTTON,AT(369,168),USE(?LookupChargeableRepairType),TRN,FLAT,ICON('lookupp.jpg')
                           CHECK('Confirm Repair Type'),AT(404,174),USE(tmp:OverwriteCRepairType),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Overwrite Repair Type'),TIP('Overwrite Repair Type'),VALUE('1','0')
                           PROMPT('Warranty Type'),AT(176,216),USE(?Warranty_Charge_Type:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(241,216,124,10),USE(Warranty_Charge_Type),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),INS,UPR,READONLY
                           BUTTON,AT(369,212),USE(?LookupWarrantyChargeType),SKIP,TRN,FLAT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ICON('lookupp.jpg')
                           PROMPT('Repair Type'),AT(176,236),USE(?Repair_Type_Warranty:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(241,236,124,10),USE(Repair_Type_Warranty),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),INS,UPR,READONLY
                           CHECK('Confirm Repair Type'),AT(405,236),USE(tmp:OverwriteWRepairType),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Overwrite Repair Type'),TIP('Overwrite Repair Type'),VALUE('1','0')
                           PROMPT('Password'),AT(176,264),USE(?Password:Prompt),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s20),AT(241,264,124,10),USE(Password),HIDE,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),INS,PASSWORD
                           BUTTON,AT(369,232),USE(?LookupWarrantyRepairType),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:Charge_Type                Like(Charge_Type)
look:Warranty_Charge_Type                Like(Warranty_Charge_Type)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020422'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Billing_Confirmation')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Charge_Type          = charge_type_s
  Warranty_Charge_Type = warranty_type
  Repair_Type          = charge_rep_type
  Repair_Type_Warranty = warranty_rep_type
  tmp:OverwriteCRepairType = func:OverwriteC
  tmp:OverwriteWRepairType = func:OverwriteW
  
  sav:ChargeType = Charge_type
  sav:WarrantyChargeType = Warranty_Charge_Type
  Relate:ACCAREAS_ALIAS.Open
  Relate:CHARTYPE.Open
  Relate:USERS_ALIAS.Open
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  IF job:Warranty_Job <> 'YES'
    HIDE(?Warranty_Charge_Type)
    HIDE(?Repair_Type_Warranty)
    HIDE(?Warranty_Charge_Type:Prompt)
    HIDE(?Repair_Type_Warranty:prompt)
    ?LookupWarrantyRepairType{prop:Hide} = 1
    ?LookupWarrantyChargeType{prop:Hide} = 1
    ?tmp:OverwriteWRepairType{prop:Hide} = 1
  END
  IF job:Chargeable_Job <> 'YES'
    HIDE(?Repair_Type)
    HIDE(?Charge_Type)
    HIDE(?String1)
    HIDE(?Repair_Type:Prompt)
    ?tmp:OverwriteCRepairType{prop:Hide} = 1
    ?LookupChargeableRepairType{prop:Hide} = 1
    ?LookupCChargeType{prop:Hide} = 1
  END
  
  If SecurityCheck('JOBS - AMEND REPAIR TYPES')
      ?LookupChargeableRepairType{prop:Disable} = 1
      ?LookupWarrantyRepairType{prop:Disable} = 1
  End !SecurityCheck('JOBS - AMEND REPAIR TYPE')
      ! Save Window Name
   AddToLog('Window','Open','Billing_Confirmation')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?Charge_Type{Prop:Tip} AND ~?LookupCChargeType{Prop:Tip}
     ?LookupCChargeType{Prop:Tip} = 'Select ' & ?Charge_Type{Prop:Tip}
  END
  IF ?Charge_Type{Prop:Msg} AND ~?LookupCChargeType{Prop:Msg}
     ?LookupCChargeType{Prop:Msg} = 'Select ' & ?Charge_Type{Prop:Msg}
  END
  IF ?Warranty_Charge_Type{Prop:Tip} AND ~?LookupWarrantyChargeType{Prop:Tip}
     ?LookupWarrantyChargeType{Prop:Tip} = 'Select ' & ?Warranty_Charge_Type{Prop:Tip}
  END
  IF ?Warranty_Charge_Type{Prop:Msg} AND ~?LookupWarrantyChargeType{Prop:Msg}
     ?LookupWarrantyChargeType{Prop:Msg} = 'Select ' & ?Warranty_Charge_Type{Prop:Msg}
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:CHARTYPE.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Billing_Confirmation')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickChargeableChargeTypes
      PickWarrantyChargeTypes
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      !copy local variable back to the sent addresses
      If ?tmp:OverwriteCRepairType{PROP:HIDE} = False AND tmp:OverwriteCRepairType = 0 And func:Type = 1
         SELECT(?tmp:OverwriteCRepairType)
          Case Missive('You must confirm the Repair Type before you continue.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
         CYCLE
      End !IF
      If ?tmp:OverwriteWRepairType{PROP:HIDE} = False AND tmp:OverwriteWRepairType = 0 And func:Type = 1
         SELECT(?tmp:OverwriteWRepairType)
          Case Missive('You must confirm the Repair Type before you continue.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
         CYCLE
      End !IF
      charge_rep_type = Repair_Type
      warranty_rep_type = Repair_Type_Warranty
      func:OverwriteC = tmp:OverwriteCRepairType
      func:OverwriteW = tmp:OverwriteWRepairType
      !j added - it looked like it was missing
      charge_type_s   = Charge_Type
      warranty_type   = Warranty_Charge_Type
      
      
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020422'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020422'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020422'&'0')
      ***
    OF ?Charge_Type
      IF Charge_Type OR ?Charge_Type{Prop:Req}
        cha:Charge_Type = Charge_Type
        cha:Warranty = 'NO'
        !Save Lookup Field Incase Of error
        look:Charge_Type        = Charge_Type
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            Charge_Type = cha:Charge_Type
          ELSE
            CLEAR(cha:Warranty)
            !Restore Lookup On Error
            Charge_Type = look:Charge_Type
            SELECT(?Charge_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      If sav:ChargeType <> Charge_Type
          Repair_Type = ''
          tmp:OverwriteCRepairType = 0
          UPDATE()
      !    If ?LookupChargeableRepairType{prop:Disable} = 1
          Calculate_Billing(Repair_Type,a",a",a")
      !    End !If ?LookupRepairType{prop:Disable} = 1
      
          Display()
      End !If sav:ChargeType <> Charge_Type
    OF ?LookupCChargeType
      ThisWindow.Update
      cha:Charge_Type = Charge_Type
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          Charge_Type = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?Charge_Type)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?Charge_Type)
    OF ?LookupChargeableRepairType
      ThisWindow.Update
      If job:Model_Number = '' Or Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
        Case Missive('Error! You cannot select a Repair Type until you have selected a Model Number, Charge Type Account Number and Unit Type.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
      Else !job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
          If PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,Charge_Type,job:Repair_Type)
              Case Missive('Error! A Pricing Structure has not been setup for this combination of Account, Model Number, Charge Type, Unit Type and Repair Type.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          Else !If PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,job:Charge_Type,job:Repair_Type)
              Repair_Type = PickRepairTypes(job:Model_Number,job:Account_Number,|
                                                Charge_Type,job:Unit_Type,'C',1,|
                                                EngineerSkillLevel(job:Engineer),job:Repair_Type)
              Post(Event:Accepted,?Repair_Type)
      
          End !If PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,job:Charge_Type,job:Repair_Type)
      End !job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
      Display()
    OF ?Warranty_Charge_Type
      If sav:WarrantyChargeType <> Warranty_Charge_Type
          Repair_Type_Warranty = ''
          tmp:OverwriteWRepairType = 0
      
      !    If ?LookupWarrantyRepairType{prop:Disable} = 1
           Calculate_Billing(a",Repair_Type_Warranty,a",a")
      !    End !If ?LookupRepairType{prop:Disable} = 1
      
          Display()
      
      End !If sav:ChargeType <> Charge_Type
      IF Warranty_Charge_Type OR ?Warranty_Charge_Type{Prop:Req}
        cha:Charge_Type = Warranty_Charge_Type
        cha:Warranty = 'YES'
        !Save Lookup Field Incase Of error
        look:Warranty_Charge_Type        = Warranty_Charge_Type
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            Warranty_Charge_Type = cha:Charge_Type
          ELSE
            CLEAR(cha:Warranty)
            !Restore Lookup On Error
            Warranty_Charge_Type = look:Warranty_Charge_Type
            SELECT(?Warranty_Charge_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupWarrantyChargeType
      ThisWindow.Update
      cha:Charge_Type = Warranty_Charge_Type
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          Warranty_Charge_Type = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?Warranty_Charge_Type)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?Warranty_Charge_Type)
    OF ?LookupWarrantyRepairType
      ThisWindow.Update
      If job:Model_Number = '' Or Warranty_Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
          Case Missive('Error! You cannot select a Repair Type until you have selected a Model Number, Charge Type, Account Number and Unit Type.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          do_lookup# = 0
      Else !job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
          Repair_Type_Warranty = PickRepairTypes(job:Model_Number,job:Account_Number,|
                                                Warranty_Charge_Type,job:Unit_Type,'W',0,|
                                                EngineerSkillLevel(job:Engineer),Repair_Type_Warranty)
          DISPLAY()
          Post(Event:Accepted,?Repair_Type_Warranty)
      End !job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
      
    OF ?OkButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Calculate_Billing    PROCEDURE  (Charge_T,Warranty_T,tmp:FaultCode,tmp:FaultCodeW) ! Declare Procedure
save_epr_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Charge_Queue         QUEUE,PRE()
RepairType           STRING(30)
Cost                 REAL
FaultCode            STRING(30)
                     END
Warranty_Queue       QUEUE,PRE()
RepairTypeWarranty   STRING(30)
Cost_w               REAL
WFaultCode           STRING(30)
                     END
  CODE
!Assume Certain rules - 1. Job Open
    Do GetOutFault
! Deleting (DBH 23/11/2007) # 9569 - Update the code, to account for highest index, even if same repair type
!
!    !First Up - Lets see what we got
!    !Requires 2 memory table, 1 for charge, 1 for warranty
!
!    !Check to see if any outfaults exist - if required!
!    CLEAR(Charge_Queue)
!    FREE(Charge_Queue)
!    CLEAR(Warranty_Queue)
!    FREE(Warranty_Queue)
!    Access:Manufact.ClearKey(man:Manufacturer_Key)
!    man:Manufacturer = job:Manufacturer
!    IF Access:Manufact.Fetch(man:Manufacturer_Key)
!      !Error - do not continue
!    ELSE !IF Access:Manufact.Fetch(man:Manufacturer_Key)
!        IF man:UseInvTextForFaults = TRUE  !Only continue if this is TRUE
!            IF man:AutoRepairType = TRUE !Do we wish to Calculate the Repair Type/Costing
!                !Continue!
!                Access:JobOutFl.ClearKey(joo:JobNumberKey)
!                joo:JobNumber = job:ref_number
!                SET(joo:JobNumberKey,joo:JobNumberKey)
!                LOOP
!                    IF Access:JobOutFl.Next()
!                        BREAK
!                    END
!                    IF joo:JobNumber <> job:ref_number
!                        BREAK
!                    END
!                    !Build up memory tables!
!                    !Fetch original Manfaulo to get charge types!
!                    Access:ManFault.ClearKey(maf:MainFaultKey)
!                    maf:manufacturer = job:manufacturer
!                    maf:mainFault = TRUE
!                    IF Access:ManFault.Fetch(maf:MainFaultKey)
!                        !Error!
!                    END !IF Access:ManFault.Fetch(maf:MainFaultKey)
!                    Access:ManFaulo.ClearKey(mfo:Field_Key)
!                    mfo:Manufacturer = job:manufacturer
!                    mfo:Field_Number = maf:Field_Number
!                    mfo:Field = joo:FaultCode
!                    IF Access:ManFaulo.Fetch(mfo:Field_Key)
!                        !Error!
!                    ELSE !IF Access:ManFaulo.Fetch(mfo:Field_Key)
!                        !Match until Key added!
!                        IF mfo:RepairType <> ''
!                            !Now go and get cost from stdcharge!
!                            SORT(Charge_Queue,Charge_Queue.RepairType)
!                            Charge_Queue.RepairType = mfo:RepairType
!                            GET(Charge_Queue,Charge_Queue.RepairType)
!                            IF Error()
!                                Charge_Queue.RepairType = mfo:RepairType
!                                Charge_Queue.Cost = mfo:ImportanceLevel
!                                FaultCode = mfo:Field
!                                Add(Charge_Queue)
!                            END !IF Error()
!                        END !IF mfo:RepairType <> ''
!                        IF mfo:RepairTypeWarranty <> ''
!                            !Now go and get cost from stdcharge!
!                            SORT(Warranty_Queue,Warranty_Queue.RepairTypeWarranty)
!                            Warranty_Queue.RepairTypeWarranty = mfo:RepairTypeWarranty
!                            GET(Warranty_Queue,Warranty_Queue.RepairTypeWarranty)
!                            IF Error()
!                                Warranty_Queue.RepairTypeWarranty = mfo:RepairTypeWarranty
!                                Warranty_Queue.Cost_w = mfo:ImportanceLevel
!                                WFaultCode = mfo:Field
!                                Add(Warranty_Queue)
!                           END !IF Error()
!                        END !IF mfo:RepairType <> ''
!                    END !IF Access:ManFaulo.Fetch(mfo:Field_Key)
!                END !LOOP
!
!                !Ok, that was the JOb Fault codes, now for the chargeable/warranty fault codes!
!                !Warranty First!
!                Access:WarParts.ClearKey(wpr:Part_Number_Key)
!                wpr:Ref_Number = job:Ref_Number
!                SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
!                LOOP
!                    IF Access:WarParts.Next()
!                      BREAK
!                    END
!                    IF wpr:Ref_Number <> job:Ref_Number
!                      BREAK
!                    END
!                    Access:ManFaulo.ClearKey(mfo:Field_Key)
!                    mfo:Manufacturer = job:manufacturer
!                    mfo:Field_Number = maf:Field_Number
!                    mfo:Field = wpr:Fault_Code2
!                    IF Access:ManFaulo.Fetch(mfo:Field_Key)
!                      !Error!
!                    ELSE !IF Access:ManFaulo.Fetch(mfo:Field_Key)
!                        IF mfo:RepairType <> ''
!                            SORT(Charge_Queue,Charge_Queue.RepairType)
!                            Charge_Queue.RepairType = mfo:RepairType
!                            GET(Charge_Queue,Charge_Queue.RepairType)
!                            IF Error()
!                                Charge_Queue.RepairType = mfo:RepairType
!                                Charge_Queue.Cost = mfo:ImportanceLevel
!                                FaultCode = mfo:Field
!                                ADD(Charge_Queue)
!                            END !IF Error()
!                        END !IF mfo:RepairType <> ''
!
!                        IF mfo:RepairTypeWarranty <> ''
!                            !Now go and get cost from stdcharge!
!                            SORT(Warranty_Queue,Warranty_Queue.RepairTypeWarranty)
!                            Warranty_Queue.RepairTypeWarranty = mfo:RepairTypeWarranty
!                            GET(Warranty_Queue,Warranty_Queue.RepairTypeWarranty)
!                            IF Error()
!                                Warranty_Queue.RepairTypeWarranty = mfo:RepairTypeWarranty
!                                Warranty_Queue.Cost_w = mfo:ImportanceLevel
!                                WFaultCode    = mfo:Field
!                                ADD(Warranty_Queue)
!                            END !IF Error()
!                        END !IF mfo:RepairTypeWarranty <> ''
!                    END !IF Access:ManFaulo.Fetch(mfo:Field_Key)
!                END !LOOP
!                !Chargeable Second!!
!                !Is it an estimate?
!                If job:Estimate = 'YES' And job:Chargeable_Job = 'YES' And job:Estimate_Accepted <> 'YES' and job:Estimate_Rejected <> 'YES'
!                    Save_epr_ID = Access:ESTPARTS.SaveFile()
!                    Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
!                    epr:Ref_Number  = job:Ref_Number
!                    Set(epr:Part_Number_Key,epr:Part_Number_Key)
!                    Loop
!                        If Access:ESTPARTS.NEXT()
!                           Break
!                        End !If
!                        If epr:Ref_Number  <> job:Ref_Number      |
!                            Then Break.  ! End If
!                        !Field 2 is currently a KNOWN main fault code!
!                        Access:ManFaulo.ClearKey(mfo:Field_Key)
!                        mfo:Manufacturer = job:manufacturer
!                        mfo:Field_Number = maf:Field_Number
!                        mfo:Field = epr:Fault_Code2
!                        IF Access:ManFaulo.Fetch(mfo:Field_Key)
!                            !Error!
!                        ELSE !IF Access:ManFaulo.Fetch(mfo:Field_Key)
!                            IF mfo:RepairType <> ''
!                                SORT(Charge_Queue,Charge_Queue.RepairType)
!                                Charge_Queue.RepairType = mfo:RepairType
!                                GET(Charge_Queue,Charge_Queue.RepairType)
!                                IF Error()
!                                    Charge_Queue.RepairType = mfo:RepairType
!                                    Charge_Queue.Cost = mfo:ImportanceLevel
!                                    FaultCode = mfo:Field
!                                    ADD(Charge_Queue)
!                                END
!                            END !IF mfo:RepairType <> ''
!                            IF mfo:RepairTypeWarranty <> ''
!                                !Now go and get cost from stdcharge!
!                                SORT(Warranty_Queue,Warranty_Queue.RepairTypeWarranty)
!                                Warranty_Queue.RepairTypeWarranty = mfo:RepairTypeWarranty
!                                GET(Warranty_Queue,Warranty_Queue.RepairTypeWarranty)
!                                IF Error()
!                                    Warranty_Queue.RepairTypeWarranty = mfo:RepairTypeWarranty
!                                    Warranty_Queue.Cost_w = mfo:ImportanceLevel
!                                    WFaultCode = mfo:Field
!                                    ADD(Warranty_Queue)
!                                END
!                            END !IF mfo:RepairTypeWarranty <> ''
!                        END !IF Access:ManFaulo.Fetch(mfo:Field_Key)
!                    End !Loop
!                    Access:ESTPARTS.RestoreFile(Save_epr_ID)
!                Else !If job:Estimate = 'YES' And job:Chargeable_Job = 'YES' And job:Estimate_Accepted <> 'YES' and job:Estimate_Rejected <> 'YES'
!                    Access:Parts.ClearKey(par:Part_Number_Key)
!                    par:Ref_Number = job:Ref_Number
!                    SET(par:Part_Number_Key,par:Part_Number_Key)
!                    LOOP
!                        IF Access:Parts.Next()
!                          BREAK
!                        END
!                        IF par:Ref_Number <> job:Ref_Number
!                            BREAK
!                        END
!                        !Field 2 is currently a KNOWN main fault code!
!                        Access:ManFaulo.ClearKey(mfo:Field_Key)
!                        mfo:Manufacturer = job:manufacturer
!                        mfo:Field_Number = maf:Field_Number
!                        mfo:Field = par:Fault_Code2
!                        IF Access:ManFaulo.Fetch(mfo:Field_Key)
!                            !Error!
!                        ELSE
!                          IF mfo:RepairType <> ''
!                              SORT(Charge_Queue,Charge_Queue.RepairType)
!                              Charge_Queue.RepairType = mfo:RepairType
!                              GET(Charge_Queue,Charge_Queue.RepairType)
!                              IF Error()
!                                  Charge_Queue.RepairType = mfo:RepairType
!                                  Charge_Queue.Cost = mfo:ImportanceLevel
!                                  FaultCode = mfo:Field
!                                  ADD(Charge_Queue)
!                              END
!                          END !IF mfo:RepairType <> ''
!                          IF mfo:RepairTypeWarranty <> ''
!                              !Now go and get cost from stdcharge!
!                              SORT(Warranty_Queue,Warranty_Queue.RepairTypeWarranty)
!                              Warranty_Queue.RepairTypeWarranty = mfo:RepairTypeWarranty
!                              GET(Warranty_Queue,Warranty_Queue.RepairTypeWarranty)
!                              IF Error()
!                                  Warranty_Queue.RepairTypeWarranty = mfo:RepairTypeWarranty
!                                  Warranty_Queue.Cost_w = mfo:ImportanceLevel
!                                  WFaultCode = mfo:Field
!                                  ADD(Warranty_Queue)
!                              END
!                          END !IF mfo:RepairTypeWarranty <> ''
!
!                        END !IF Access:ManFaulo.Fetch(mfo:Field_Key)
!                    END !LOOP
!                End !If job:Estimate = 'YES' And job:Chargeable_Job = 'YES' And job:Estimate_Accepted <> 'YES' and job:Estimate_Rejected <> 'YES'
!
!                !IF RECORDS(Charge_Queue) OR RECORDS(Warranty_Queue) !Continue, we have some charges!
!                SORT(Charge_Queue,-Charge_Queue.Cost)
!                SORT(Warranty_Queue,-Warranty_Queue.Cost_w)
!                GET(Charge_Queue,1)
!                GET(Warranty_Queue,1)
!                IF man:BillingConfirmation = TRUE !Only show this screen if required!
!                    !Billing_Confirmation(job:Charge_Type,Charge_Queue.RepairType,job:Warranty_Charge_Type,Warranty_Queue.RepairTypeWarranty)
!                END !IF man:BillingConfirmation = TRUE
!            END !IF man:AutoRepairType = TRUE
!        END !IF man:UseInvTextForFaults = TRUE
!    END !IF Access:Manufact.Fetch(man:Manufacturer_Key)
!
!    !If the charge types are already filled in, then assume
!    !the overwrite boxes have been ticked
!    If Charge_T = ''
!        Charge_t = Charge_Queue.RepairType
!    End !If Charge_T = ''
!
!    If Warranty_T = ''
!        Warranty_t = Warranty_Queue.RepairTypeWarranty
!    End !If Warranty_T = ''
!
!    tmp:FaultCode = FaultCode
!    tmp:FaultCodeW = WFaultCode
!
!
! End (DBH 23/11/2007) #9569
GetOutFault        Routine
Data
local:CFaultCode        String(255)
local:CIndex            Byte(0)
local:CRepairType       String(30)
local:WFaultCOde        String(255)
local:WIndex            Byte(0)
local:WRepairType       String(30)
local:KeyRepair         Long()
Code
    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer = job:Manufacturer
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        If man:UseInvTextForFaults = True
            If man:AutoRepairType = True
                ! Get the main out fault (DBH: 23/11/2007)
                Access:MANFAULT.Clearkey(maf:MainFaultKey)
                maf:Manufacturer = job:Manufacturer
                maf:MainFault = 1
                If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

                    ! Check the out faults list (DBH: 23/11/2007)
                    Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
                    joo:JobNumber = job:Ref_Number
                    Set(joo:JobNumberKey,joo:JobNumberKey)
                    Loop
                        If Access:JOBOUTFL.Next()
                            Break
                        End ! If Access:JOBOUTFL.Next()
                        If joo:JobNumber <> job:Ref_Number
                            Break
                        End ! If joo:JobNumber <> job:Ref_Number

                        ! Get the lookup details (e.g. index and repair types) (DBH: 23/11/2007)
                        Access:MANFAULO.Clearkey(mfo:Field_Key)
                        mfo:Manufacturer = job:Manufacturer
                        mfo:Field_Number = maf:Field_Number
                        mfo:Field = joo:FaultCode
                        If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            ! Is there a repair type? (DBH: 23/11/2007)
                            If mfo:RepairType <> ''
                                ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                If mfo:ImportanceLevel > local:CIndex
                                    local:CFaultCode = mfo:Field
                                    local:CIndex = mfo:ImportanceLevel
                                    local:CRepairType = mfo:RepairType
                                End ! If mfo:ImportanceLevel > local:CIndex
                            End ! If mfo:RepairType <> ''
                            If mfo:RepairTypeWarranty <> ''
                                ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                If mfo:ImportanceLevel > local:WIndex
                                    local:WFaultCode = mfo:Field
                                    local:WIndex = mfo:ImportanceLevel
                                    local:WRepairType = mfo:RepairTypeWarranty
                                End ! If mfo:ImportanceLevel > local:CIndex
                            End ! If mfo:RepairTypeWarranty <> ''
                        End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                    End ! Loop

                    ! Inserting (DBH 30/04/2008) # 9723 - Is there a "Key Repair" fault code? If so, that is counted as the OUT Fault
                    Access:MANFAUPA.Clearkey(map:KeyRepairKey)
                    map:Manufacturer = job:Manufacturer
                    map:KeyRepair = 1
                    If Access:MANFAUPA.TryFetch(map:KeyRepairKey) = Level:Benign
                        ! Found
                        local:KeyRepair = map:Field_Number
                    Else ! If Access:MANFAUPA.TryFetch(map:KeyRepairKey) = Level:Benign
                        local:KeyRepair = 0
                    End ! If Access:MANFAUPA.TryFetch(map:KeyRepairKey) = Level:Benign
                    ! End (DBH 30/04/2008) #9723

! Changing (DBH 21/05/2008) # 9723 - Can be more than one part out fault
!                    Access:MANFAUPA.Clearkey(map:MainFaultKey)
!                    map:Manufacturer = job:Manufacturer
!                    map:MainFault = 1
!                    If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
! to (DBH 21/05/2008) # 9723
                    ! Is the out fault held on the parts. If so, check for the highest repair index there too (DBH: 23/11/2007)
                    Access:MANFAUPA.Clearkey(map:MainFaultKey)
                    map:Manufacturer = job:Manufacturer
                    map:MainFault = 1
                    Set(map:MainFaultKey,map:MainFaultKey)
                    Loop ! Begin Loop
                        If Access:MANFAUPA.Next()
                            Break
                        End ! If Access:MANFAUPA.Next()
                        If map:Manufacturer <> job:Manufacturer
                            Break
                        End ! If map:Manufacturer <> job:Manufacturer
                        If map:MainFault <> 1
                            Break
                        End ! If map:MainFault <> 1

! End (DBH 21/05/2008) #9723
                        ! Check the Warranty Parts (DBH: 23/11/2007)
                        Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
                        wpr:Ref_Number = job:Ref_Number
                        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                        Loop
                            If Access:WARPARTS.Next()
                                Break
                            End ! If Access:WARPARTS.Next()
                            If wpr:Ref_Number <> job:Ref_Number
                                Break
                            End ! If wpr:Ref_Number <> job:Ref_Number

                            ! Inserting (DBH 30/04/2008) # 9723 - If there is a key repair. Only count the part that ISthe Key Repair
                            Case local:KeyRepair
                            Of 1
                                If wpr:Fault_Code1 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code1 <> 1
                            Of 2
                                If wpr:Fault_Code2 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code2 <> 1
                            Of 3
                                If wpr:Fault_Code3 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 4
                                If wpr:Fault_Code4 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 5
                                If wpr:Fault_Code5 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 6
                                If wpr:Fault_Code6 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 7
                                If wpr:Fault_Code7 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 8
                                If wpr:Fault_Code8 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 9
                                If wpr:Fault_Code9 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 10
                                If wpr:Fault_Code10 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 11
                                If wpr:Fault_Code11 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            Of 12
                                If wpr:Fault_Code12 <> 1
                                    Cycle
                                End ! If wpr:Fault_Code3 <> 1
                            End ! Case local:KeyRepair
                            ! End (DBH 30/04/2008) #9723

                            ! Lookup the fault code details using whichever is designated as the out fault (DBH: 23/11/2007)
                            Access:MANFAULO.Clearkey(mfo:Field_Key)
                            mfo:Manufacturer = job:Manufacturer
                            mfo:Field_Number = maf:Field_NUmber
                            Case map:Field_Number
                            Of 1
                                mfo:Field = wpr:Fault_Code1
                            Of 2
                                mfo:Field = wpr:Fault_Code2
                            Of 3
                                mfo:Field = wpr:Fault_Code3
                            Of 4
                                mfo:Field = wpr:Fault_Code4
                            Of 5
                                mfo:Field = wpr:Fault_Code5
                            Of 6
                                mfo:Field = wpr:Fault_Code6
                            Of 7
                                mfo:Field = wpr:Fault_Code7
                            Of 8
                                mfo:Field = wpr:Fault_Code8
                            Of 9
                                mfo:Field = wpr:Fault_Code9
                            Of 10
                                mfo:Field = wpr:Fault_Code10
                            Of 11
                                mfo:Field = wpr:Fault_Code11
                            Of 12
                                mfo:Field = wpr:Fault_Code12
                            End ! Case map:Field_Number
                            If Access:MANFAULO.Tryfetch(mfo:Field_Key) = Level:Benign
                                ! Is there a repair type? (DBH: 23/11/2007)
                                If mfo:RepairType <> ''
                                    ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                    If mfo:ImportanceLevel > local:CIndex
                                        local:CFaultCode = mfo:Field
                                        local:CIndex = mfo:ImportanceLevel
                                        local:CRepairType = mfo:RepairType
                                    End ! If mfo:ImportanceLevel > local:CIndex
                                End ! If mfo:RepairType <> ''
                                If mfo:RepairTypeWarranty <> ''
                                    ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                    If mfo:ImportanceLevel > local:WIndex
                                        local:WFaultCode = mfo:Field
                                        local:WIndex = mfo:ImportanceLevel
                                        local:WRepairType = mfo:RepairTypeWarranty
                                    End ! If mfo:ImportanceLevel > local:CIndex
                                End ! If mfo:RepairTypeWarranty <> ''
                            End ! If Access:MANFAULO.Clearkey(mfo:Field_Key) = Level:Benign
                        End ! Loop

                        ! Check the estimate parts (DBH: 23/11/2007)
                        If job:Estimate = 'YES' And job:Chargeable_Job = 'YES' And job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
                            Access:ESTPARTS.Clearkey(epr:Part_Number_Key)
                            epr:Ref_Number = job:Ref_Number
                            Set(epr:Part_Number_Key,epr:Part_Number_Key)
                            Loop
                                If Access:ESTPARTS.Next()
                                    Break
                                End ! If Access:ESTPARTS.Next()
                                If epr:Ref_Number <> job:Ref_Number
                                    Break
                                End ! If epr:Ref_Number <> job:Ref_Number

                                ! Inserting (DBH 30/04/2008) # 9723 - If there is a key repair. Only count the part that ISthe Key Repair
                                Case local:KeyRepair
                                Of 1
                                    If epr:Fault_Code1 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code1 <> 1
                                Of 2
                                    If epr:Fault_Code2 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code2 <> 1
                                Of 3
                                    If epr:Fault_Code3 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 4
                                    If epr:Fault_Code4 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 5
                                    If epr:Fault_Code5 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 6
                                    If epr:Fault_Code6 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 7
                                    If epr:Fault_Code7 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 8
                                    If epr:Fault_Code8 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 9
                                    If epr:Fault_Code9 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 10
                                    If epr:Fault_Code10 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 11
                                    If epr:Fault_Code11 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                Of 12
                                    If epr:Fault_Code12 <> 1
                                        Cycle
                                    End ! If epr:Fault_Code3 <> 1
                                End ! Case local:KeyRepair
                                ! End (DBH 30/04/2008) #9723

                                ! Lookup the fault code details using whichever is designated as the out fault (DBH: 23/11/2007)
                                Access:MANFAULO.Clearkey(mfo:Field_Key)
                                mfo:Manufacturer = job:Manufacturer
                                mfo:Field_Number = maf:Field_NUmber
                                Case map:Field_Number
                                Of 1
                                    mfo:Field = epr:Fault_Code1
                                Of 2
                                    mfo:Field = epr:Fault_Code2
                                Of 3
                                    mfo:Field = epr:Fault_Code3
                                Of 4
                                    mfo:Field = epr:Fault_Code4
                                Of 5
                                    mfo:Field = epr:Fault_Code5
                                Of 6
                                    mfo:Field = epr:Fault_Code6
                                Of 7
                                    mfo:Field = epr:Fault_Code7
                                Of 8
                                    mfo:Field = epr:Fault_Code8
                                Of 9
                                    mfo:Field = epr:Fault_Code9
                                Of 10
                                    mfo:Field = epr:Fault_Code10
                                Of 11
                                    mfo:Field = epr:Fault_Code11
                                Of 12
                                    mfo:Field = epr:Fault_Code12
                                End ! Case map:Field_Number
                                If Access:MANFAULO.Tryfetch(mfo:Field_Key) = Level:Benign
                                    ! Is there a repair type? (DBH: 23/11/2007)
                                    If mfo:RepairType <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:CIndex
                                            local:CFaultCode = mfo:Field
                                            local:CIndex = mfo:ImportanceLevel
                                            local:CRepairType = mfo:RepairType
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairType <> ''
                                    If mfo:RepairTypeWarranty <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:WIndex
                                            local:WFaultCode = mfo:Field
                                            local:WIndex = mfo:ImportanceLevel
                                            local:WRepairType = mfo:RepairTypeWarranty
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairTypeWarranty <> ''
                                End ! If Access:MANFAULO.Clearkey(mfo:Field_Key) = Level:Benign
                            End ! Loop
                        Else ! If job:Estimate = 'YES' And job:Chargeabe_Job = 'YES' And job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
                            Access:PARTS.Clearkey(par:Part_Number_Key)
                            par:Ref_Number = job:Ref_Number
                            Set(par:Part_Number_Key,par:Part_Number_Key)
                            Loop
                                If Access:PARTS.Next()
                                    Break
                                End ! If Access:PARTS.Next()
                                If par:Ref_Number <> job:Ref_Number
                                    Break
                                End ! If par:Ref_Number <> job:Ref_Number
                                ! Inserting (DBH 30/04/2008) # 9723 - If there is a key repair. Only count the part that ISthe Key Repair
                                Case local:KeyRepair
                                Of 1
                                    If par:Fault_Code1 <> 1
                                        Cycle
                                    End ! If par:Fault_Code1 <> 1
                                Of 2
                                    If par:Fault_Code2 <> 1
                                        Cycle
                                    End ! If par:Fault_Code2 <> 1
                                Of 3
                                    If par:Fault_Code3 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 4
                                    If par:Fault_Code4 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 5
                                    If par:Fault_Code5 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 6
                                    If par:Fault_Code6 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 7
                                    If par:Fault_Code7 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 8
                                    If par:Fault_Code8 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 9
                                    If par:Fault_Code9 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 10
                                    If par:Fault_Code10 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 11
                                    If par:Fault_Code11 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                Of 12
                                    If par:Fault_Code12 <> 1
                                        Cycle
                                    End ! If par:Fault_Code3 <> 1
                                End ! Case local:KeyRepair
                                ! End (DBH 30/04/2008) #9723

                                ! Lookup the fault code details using whichever is designated as the out fault (DBH: 23/11/2007)
                                Access:MANFAULO.Clearkey(mfo:Field_Key)
                                mfo:Manufacturer = job:Manufacturer
                                mfo:Field_Number = maf:Field_NUmber
                                Case map:Field_Number
                                Of 1
                                    mfo:Field = par:Fault_Code1
                                Of 2
                                    mfo:Field = par:Fault_Code2
                                Of 3
                                    mfo:Field = par:Fault_Code3
                                Of 4
                                    mfo:Field = par:Fault_Code4
                                Of 5
                                    mfo:Field = par:Fault_Code5
                                Of 6
                                    mfo:Field = par:Fault_Code6
                                Of 7
                                    mfo:Field = par:Fault_Code7
                                Of 8
                                    mfo:Field = par:Fault_Code8
                                Of 9
                                    mfo:Field = par:Fault_Code9
                                Of 10
                                    mfo:Field = par:Fault_Code10
                                Of 11
                                    mfo:Field = par:Fault_Code11
                                Of 12
                                    mfo:Field = par:Fault_Code12
                                End ! Case map:Field_Number
                                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                    ! Is there a repair type? (DBH: 23/11/2007)
                                    If mfo:RepairType <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:CIndex
                                            local:CFaultCode = mfo:Field
                                            local:CIndex = mfo:ImportanceLevel
                                            local:CRepairType = mfo:RepairType
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairType <> ''
                                    If mfo:RepairTypeWarranty <> ''
                                        ! Record the fault code and index if it's the HIGHEST index (DBH: 23/11/2007)
                                        If mfo:ImportanceLevel > local:WIndex
                                            local:WFaultCode = mfo:Field
                                            local:WIndex = mfo:ImportanceLevel
                                            local:WRepairType = mfo:RepairTypeWarranty
                                        End ! If mfo:ImportanceLevel > local:CIndex
                                    End ! If mfo:RepairTypeWarranty <> ''
                                End ! If Access:MANFAULO.Clearkey(mfo:Field_Key) = Level:Benign

                            End ! Loop
                        End ! If job:Estimate = 'YES' And job:Chargeabe_Job = 'YES' And job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
                    End ! If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                End ! If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            End ! If man:AutoRepairType = True
        End ! If man:UseInvTextForFaults = True
    End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    ! If the charge types are already filled then assume "overwrite" box has been ticked. So don't change. (DBH: 23/11/2007)
    If Charge_T = ''
        Charge_T = local:CRepairType
    End ! If Charge_T = ''

    If Warranty_T = ''
        Warranty_T = local:WRepairType
    End ! If Warranty_T = ''

    tmp:FaultCode = local:CFaultCode
    tmp:FaultCodeW = local:WFaultCode
PartsKeyNormal PROCEDURE                              !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Parts Key'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Parts Colour Key'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON,AT(368,258),USE(?Close),TRN,FLAT,LEFT,ICON('okp.jpg')
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           GROUP('Key'),AT(248,192,184,36),USE(?Group1),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PANEL,AT(252,202,12,8),USE(?Panel1),FILL(COLOR:Green)
                             PROMPT('- Part Requested'),AT(264,202),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PANEL,AT(252,216,12,8),USE(?Panel1:2),FILL(COLOR:Red)
                             PROMPT('- Part On Order'),AT(264,216),USE(?Prompt1:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('- Parts Order Recvd'),AT(352,202),USE(?Prompt1:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PANEL,AT(340,202,12,8),USE(?Panel1:3),FILL(COLOR:Gray)
                             PANEL,AT(340,216,12,8),USE(?Panel1:4),FILL(COLOR:Purple)
                             PROMPT('- Web Order Created'),AT(352,216),USE(?Prompt1:4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                         END
                       END
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020455'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('PartsKeyNormal')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','PartsKeyNormal')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','PartsKeyNormal')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020455'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020455'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020455'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Error_Text_Return PROCEDURE                           !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Return           BYTE(0)
window               WINDOW('ServiceBase 2000'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Errors'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       TEXT,AT(224,138,232,132),USE(glo:ErrorText),SKIP,VSCROLL,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),READONLY
                       PANEL,AT(164,82,352,248),USE(?Panel5),FILL(COLOR:Red)
                       PROMPT('Click ''OK'' To Continue..'),AT(244,286,192,12),USE(?Prompt1),CENTER,FONT(,,COLOR:White,FONT:bold),COLOR(COLOR:Red)
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Return)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020448'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Error_Text_Return')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Error_Text_Return')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','Error_Text_Return')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020448'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020448'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020448'&'0')
      ***
    OF ?Cancel
      ThisWindow.Update
      tmp:Return = 0
      Post(Event:CloseWindow)
    OF ?OK
      ThisWindow.Update
      tmp:Return = 1
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
