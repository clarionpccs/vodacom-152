

   MEMBER('sbj02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ02006.INC'),ONCE        !Local module procedure declarations
                     END


GlobalIMEISearchWindow PROCEDURE                      !Generated from procedure template - Window

tmp:IMEINumber       STRING(30)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Global IMEI Search'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,162,192,94),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('I.M.E.I. Number'),AT(248,206),USE(?tmp:IMEINumber:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(308,206,124,10),USE(tmp:IMEINumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Job Number'),TIP('Job Number'),UPR
                       BUTTON,AT(300,258),USE(?Button:OK),TRN,FLAT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
tmp:TempFilePath    CString(255)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020666'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('GlobalIMEISearchWindow')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:EXCHANGE.Open
  Relate:JOBS.Open
  Access:JOBTHIRD.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','GlobalIMEISearchWindow')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','GlobalIMEISearchWindow')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020666'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020666'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020666'&'0')
      ***
    OF ?Button:OK
      ThisWindow.Update
      If GetTempPathA(255,tmp:TempFilePath)
          If Sub(Clip(tmp:TempFilePath),-1,1) = '\'
              glo:File_Name   = Clip(tmp:TempFilePath) & 'GLOBIMEI' & Clock() & '.TMP'
          Else !If Sub(Clip(TempFilePath),-1,1) = '\'
              glo:File_Name   = Clip(tmp:TempFilePath) & '\GLOBIMEI' & Clock() & '.TMP'
          End !If Sub(Clip(TempFilePath),-1,1) = '\'
      End
      
      Remove(glo:File_Name)
      Access:ADDSEARCH.Open()
      Access:ADDSEARCH.UseFile()
      
      ! Inserting (DBH 22/11/2006) # 8405 - Call IMEI Search Routine
      BuildIMEISearch(tmp:IMEInumber)
      ! End (DBH 22/11/2006) #8405
      
      Access:ADDSEARCH.Close()
      
      IMEIHistorySearch(tmp:IMEINumber)
      !IMEISearch(tmp:GlobalIMEINumber)
      
      Remove(glo:File_Name)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
JobSearchWindow PROCEDURE                             !Generated from procedure template - Window

tmp:JobNumber        LONG
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,162,192,94),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Rapid Job Search'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Job Number'),AT(276,200),USE(?tmp:JobNumber:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s8),AT(340,200,64,10),USE(tmp:JobNumber),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Job Number'),TIP('Job Number'),UPR
                       BUTTON,AT(248,226),USE(?Button:CustomerService),TRN,FLAT,ICON('custserp.jpg')
                       BUTTON,AT(364,226),USE(?Button:EngineeringDetails),TRN,FLAT,ICON('engdetp.jpg'),DEFAULT
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020667'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('JobSearchWindow')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBS.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','JobSearchWindow')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','JobSearchWindow')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020667'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020667'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020667'&'0')
      ***
    OF ?Button:CustomerService
      ThisWindow.Update
      Access:JOBS.ClearKey(job:Ref_Number_Key)
      job:Ref_Number = tmp:JobNumber
      If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
          !Found
          ! Inserting (DBH 06/12/2006) # 8559 - Do not allow access to the customer service screen if the user has not booked the job
          If glo:WebJob
              Access:WEBJOB.ClearKey(wob:RefNumberKey)
              wob:RefNumber = job:Ref_Number
              If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                  !Found
                  Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                  tra:Account_Number = wob:HeadAccountNumber
                  If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                      !Found
                      Access:USERS.ClearKey(use:Password_Key)
                      use:Password = glo:Password
                      If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
                          !Found
                          If tra:SiteLocation <> use:Location
                              Case Missive('Error! You cannot view the Customer Service screen of a job booked by another RRC.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              tmp:JobNumber = 0
                              ?tmp:JobNumber{prop:Touched} = 1
                              Select(?tmp:JobNumber)
                              Cycle
                          End ! If wob:HeadACcountNumber <> use:Location
                      Else ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
                          !Error
                      End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
                  Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                      !Error
                  End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
              Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                  !Error
              End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
          End ! If glo:WebJob
          ! End (DBH 06/12/2006) #8559
      
          GlobalRequest = ChangeRecord
          UpdateJOBS
      Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
          !Error
          Case Missive('Cannot find the selected job number.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          tmp:JobNumber = 0
          ?tmp:JobNumber{prop:Touched} = 1
          Select(?tmp:JobNumber)
      End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
    OF ?Button:EngineeringDetails
      ThisWindow.Update
      Access:JOBS.ClearKey(job:Ref_Number_Key)
      job:Ref_Number = tmp:JobNumber
      If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
          !Found
          GlobalRequest = ChangeRecord
          Update_Jobs_Rapid
      Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
          !Error
          Case Missive('Cannot find the selected job number.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          tmp:JobNumber = 0
          ?tmp:JobNumber{prop:Touched} = 1
          Select(?tmp:JobNumber)
      End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BrowseRepairNotes PROCEDURE (f:RefNumber)             !Generated from procedure template - Browse

tmp:RefNumber        LONG
tmp:UserName         STRING(60)
BRW9::View:Browse    VIEW(JOBRPNOT)
                       PROJECT(jrn:TheDate)
                       PROJECT(jrn:TheTime)
                       PROJECT(jrn:Notes)
                       PROJECT(jrn:RecordNumber)
                       PROJECT(jrn:RefNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
jrn:TheDate            LIKE(jrn:TheDate)              !List box control field - type derived from field
jrn:TheTime            LIKE(jrn:TheTime)              !List box control field - type derived from field
tmp:UserName           LIKE(tmp:UserName)             !List box control field - type derived from local data
jrn:Notes              LIKE(jrn:Notes)                !List box control field - type derived from field
jrn:RecordNumber       LIKE(jrn:RecordNumber)         !Primary key field - type derived from field
jrn:RefNumber          LIKE(jrn:RefNumber)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,82,352,248),USE(?Panel5),FILL(09A6A7CH)
                       LIST,AT(208,86,196,158),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('48R(2)|M~Date~@d6@32R(2)|M~Time~@t1b@80L(2)|M~User Name~@s60@0L(2)|M~Notes~@s255' &|
   '@'),FROM(Queue:Browse)
                       BUTTON,AT(412,236),USE(?Insert),TRN,FLAT,ICON('insertp.jpg')
                       BUTTON,AT(412,268),USE(?Change),TRN,FLAT,ICON('editp.jpg')
                       BUTTON,AT(412,300),USE(?Delete),TRN,FLAT,ICON('deletep.jpg')
                       TEXT,AT(208,248,196,80),USE(jrn:Notes),VSCROLL,LEFT,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Notes'),TIP('Notes'),UPR
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse Repair Notes'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(444,332),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW9                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020697'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseRepairNotes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBRPNOT.Open
  Access:USERS.UseFile
  SELF.FilesOpened = True
  tmp:RefNumber = f:RefNumber
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:JOBRPNOT,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','BrowseRepairNotes')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW9.Q &= Queue:Browse
  BRW9.AddSortOrder(,jrn:TheDateKey)
  BRW9.AddRange(jrn:RefNumber,tmp:RefNumber)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,jrn:TheDate,1,BRW9)
  BIND('tmp:UserName',tmp:UserName)
  BRW9.AddField(jrn:TheDate,BRW9.Q.jrn:TheDate)
  BRW9.AddField(jrn:TheTime,BRW9.Q.jrn:TheTime)
  BRW9.AddField(tmp:UserName,BRW9.Q.tmp:UserName)
  BRW9.AddField(jrn:Notes,BRW9.Q.jrn:Notes)
  BRW9.AddField(jrn:RecordNumber,BRW9.Q.jrn:RecordNumber)
  BRW9.AddField(jrn:RefNumber,BRW9.Q.jrn:RefNumber)
  BRW9.AskProcedure = 1
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBRPNOT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseRepairNotes')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateRepairNotes
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020697'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020697'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020697'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW9.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW9.SetQueueRecord PROCEDURE

  CODE
  Access:USERS.Clearkey(use:User_Code_Key)
  use:User_Code = jrn:User
  If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      tmp:Username = Clip(use:Forename) & ' ' & Clip(use:Surname)
  Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      tmp:UserName = '** Unknown User: ' & Clip(jrn:User) & ' **'
  End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
  PARENT.SetQueueRecord


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

UpdateRepairNotes PROCEDURE                           !Generated from procedure template - Form

ActionMessage        CSTRING(40)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Repair Notes'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PROMPT('Notes'),AT(216,161),USE(?jrn:Notes:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       TEXT,AT(268,161,185,106),USE(jrn:Notes),VSCROLL,LEFT,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Notes'),TIP('Notes'),REQ,UPR
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,ICON('okp.jpg'),DEFAULT,REQ
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       PANEL,AT(164,82,352,248),USE(?Panel5),FILL(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020698'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateRepairNotes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddUpdateFile(Access:JOBRPNOT)
  Relate:JOBRPNOT.Open
  Access:USERS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBRPNOT
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  If ThisWindow.Request = InsertRecord
      Access:USERS.Clearkey(use:Password_Key)
      use:Password = glo:Password
      If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
          jrn:User = use:User_Code
      End ! If Access:USERS.Clearkey(use:Password_Key) = Level:Benign
  End ! If ThisWindow.Request = InsertRecord
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','UpdateRepairNotes')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBRPNOT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateRepairNotes')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020698'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020698'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020698'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
JobFaultCodes PROCEDURE (f:Req, f:Type, f:CType, f:RType, FaultCodeQueue) !Generated from procedure template - Window

save_maf_id          USHORT,AUTO
save_manfault_alias_id USHORT,AUTO
save_manfaulo_alias_id USHORT,AUTO
tmp:FaultCode        STRING(255),DIM(20)
tmp:FaultCodeDescription STRING(30),DIM(20)
tmp:FaultCodePrompt  STRING(30),DIM(20)
tmp:Required         BYTE(0)
FieldNumbersQueue    QUEUE,PRE(field)
ScreenOrder          LONG
FieldNumber          LONG
                     END
PromptNumbersQueue   QUEUE,PRE(prompt)
ScreenOrder          LONG
FieldNumber          LONG
                     END
LookupNumbersQueue   QUEUE,PRE(lookup)
ScreenOrder          LONG
FieldNumber          LONG
                     END
DescriptionNumbersQueue QUEUE,PRE(description)
ScreenOrder          LONG
FieldNumber          LONG
                     END
tmp:MSN              STRING(30)
tmp:CurrentMSN       STRING(30)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Job Fault Codes'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(4,28,672,30),USE(?Sheet2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 2'),USE(?Tab2)
                           PROMPT('Verify M.S.N.'),AT(8,39),USE(?tmp:MSN:Prompt),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(116,39,124,10),USE(tmp:MSN),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('M.S.N.'),TIP('M.S.N.'),REQ,UPR
                           BUTTON,AT(244,31),USE(?Button:VerifyMSN),TRN,FLAT,HIDE,ICON('verifyp.jpg')
                         END
                       END
                       SHEET,AT(4,60,672,322),USE(?Sheet:FaultCodes),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Product Code'),AT(8,74),USE(?job:ProductCode:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(116,74,124,10),USE(job:ProductCode),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(244,71),USE(?Lookup:ProductCode),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code Prompt'),AT(8,95,104,18),USE(?tmp:FaultCodePrompt:1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(456,95,64,10),USE(tmp:FaultCode[11]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           ENTRY(@s255),AT(116,95,64,10),USE(tmp:FaultCode[1]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Fault Code Description'),AT(212,95,120,18),USE(?tmp:FaultCodeDescription:1),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           BUTTON,AT(524,116),USE(?Button:Lookup:12),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(184,92),USE(?Button:Lookup:1),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s255),AT(456,119,64,10),USE(tmp:FaultCode[12]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           ENTRY(@s255),AT(116,119,64,10),USE(tmp:FaultCode[2]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(524,138),USE(?Button:Lookup:13),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s255),AT(116,143,64,10),USE(tmp:FaultCode[3]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           ENTRY(@s255),AT(456,143,64,10),USE(tmp:FaultCode[13]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(184,138),USE(?Button:Lookup:3),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s255),AT(116,167,64,10),USE(tmp:FaultCode[4]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Fault Code Prompt'),AT(8,143,104,18),USE(?tmp:FaultCodePrompt:3),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code Description'),AT(212,143,120,18),USE(?tmp:FaultCodeDescription:3),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           BUTTON,AT(184,162),USE(?Button:Lookup:4),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s255),AT(456,167,64,10),USE(tmp:FaultCode[14]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Fault Code Prompt'),AT(8,119,104,18),USE(?tmp:FaultCodePrompt:2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code Description'),AT(212,119,120,18),USE(?tmp:FaultCodeDescription:2),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           BUTTON,AT(184,116),USE(?Button:Lookup:2),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s255),AT(116,191,64,10),USE(tmp:FaultCode[5]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(184,188),USE(?Button:Lookup:5),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code Prompt'),AT(348,143,104,18),USE(?tmp:FaultCodePrompt:13),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code Prompt'),AT(348,119,104,18),USE(?tmp:FaultCodePrompt:12),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(524,162),USE(?Button:Lookup:14),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s255),AT(456,191,64,10),USE(tmp:FaultCode[15]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Fault Code Prompt'),AT(348,191,104,18),USE(?tmp:FaultCodePrompt:15),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code Description'),AT(552,191,120,18),USE(?tmp:FaultCodeDescription:15),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Fault Code Prompt'),AT(348,167,104,18),USE(?tmp:FaultCodePrompt:14),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code Description'),AT(552,167,120,18),USE(?tmp:FaultCodeDescription:14),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Fault Code Prompt'),AT(8,167,104,18),USE(?tmp:FaultCodePrompt:4),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code Description'),AT(212,167,120,18),USE(?tmp:FaultCodeDescription:4),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           BUTTON,AT(524,188),USE(?Button:Lookup:15),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code Description'),AT(552,143,120,18),USE(?tmp:FaultCodeDescription:13),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Fault Code Description'),AT(552,119,120,18),USE(?tmp:FaultCodeDescription:12),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           BUTTON,AT(184,215),USE(?Button:Lookup:6),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(524,215),USE(?Button:Lookup:16),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s255),AT(116,218,64,10),USE(tmp:FaultCode[6]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Fault Code Prompt'),AT(348,218,104,18),USE(?tmp:FaultCodePrompt:16),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(456,218,64,10),USE(tmp:FaultCode[16]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Fault Code Description'),AT(552,218,120,18),USE(?tmp:FaultCodeDescription:16),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Fault Code Prompt'),AT(8,191,104,18),USE(?tmp:FaultCodePrompt:5),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code Description'),AT(212,191,120,18),USE(?tmp:FaultCodeDescription:5),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(116,242,64,10),USE(tmp:FaultCode[7]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Fault Code Prompt'),AT(348,242,104,18),USE(?tmp:FaultCodePrompt:17),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(456,242,64,10),USE(tmp:FaultCode[17]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Fault Code Description'),AT(552,242,120,18),USE(?tmp:FaultCodeDescription:17),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Fault Code Prompt'),AT(8,266,104,18),USE(?tmp:FaultCodePrompt:8),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code Description'),AT(212,266,120,18),USE(?tmp:FaultCodeDescription:8),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Fault Code Prompt'),AT(8,242,104,18),USE(?tmp:FaultCodePrompt:7),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code Description'),AT(212,242,120,18),USE(?tmp:FaultCodeDescription:7),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Fault Code Prompt'),AT(8,218,104,18),USE(?tmp:FaultCodePrompt:6),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code Description'),AT(212,218,120,18),USE(?tmp:FaultCodeDescription:6),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           BUTTON,AT(524,239),USE(?Button:Lookup:17),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(184,239),USE(?Button:Lookup:7),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s255),AT(116,266,64,10),USE(tmp:FaultCode[8]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(184,263),USE(?Button:Lookup:8),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s255),AT(456,266,64,10),USE(tmp:FaultCode[18]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(524,263),USE(?Button:Lookup:18),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s255),AT(116,290,64,10),USE(tmp:FaultCode[9]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Fault Code Prompt'),AT(348,290,104,18),USE(?tmp:FaultCodePrompt:19),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code Prompt'),AT(8,290,104,18),USE(?tmp:FaultCodePrompt:9),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(184,287),USE(?Button:Lookup:9),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s255),AT(456,290,64,10),USE(tmp:FaultCode[19]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Fault Code Description'),AT(552,290,120,18),USE(?tmp:FaultCodeDescription:19),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Fault Code Prompt'),AT(8,314,104,18),USE(?tmp:FaultCodePrompt:10),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code Description'),AT(212,314,120,18),USE(?tmp:FaultCodeDescription:10),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Fault Code Description'),AT(212,290,120,18),USE(?tmp:FaultCodeDescription:9),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Fault Code Prompt'),AT(348,266,104,18),USE(?tmp:FaultCodePrompt:18),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code Description'),AT(552,266,120,18),USE(?tmp:FaultCodeDescription:18),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           BUTTON,AT(524,287),USE(?Button:Lookup:19),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(184,311),USE(?Button:Lookup:10),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s255),AT(116,314,64,10),USE(tmp:FaultCode[10]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           ENTRY(@s255),AT(456,314,64,10),USE(tmp:FaultCode[20]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Fault Code Prompt'),AT(348,314,104,18),USE(?tmp:FaultCodePrompt:20),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code Description'),AT(552,314,120,18),USE(?tmp:FaultCodeDescription:20),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Specific Needs Type'),AT(9,336),USE(?jobe3:FaultCode21:Prompt),HIDE
                           ENTRY(@s30),AT(116,336,64,10),USE(jobe3:FaultCode21),DISABLE,HIDE,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Specific Needs Code'),TIP('Specific Needs Code')
                           BUTTON,AT(184,332),USE(?ButtonSelectSNCode),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           CHECK,AT(116,356),USE(jobe3:SN_Software_Installed),HIDE,VALUE('1','0'),MSG('Specific Needs Completed')
                           PROMPT('SN Software Installed'),AT(9,356),USE(?Prompt46),HIDE
                           PROMPT('Fault Code Prompt'),AT(348,95,104,18),USE(?tmp:FaultCodePrompt:11),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code Description'),AT(552,95,120,18),USE(?tmp:FaultCodeDescription:11),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           LINE,AT(338,74,0,255),USE(?Line1),COLOR(COLOR:White)
                           BUTTON,AT(524,311),USE(?Button:Lookup:20),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(524,92),USE(?Button:Lookup:11),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,30),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(604,386),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(80,386),USE(?Button:EditOutFaultsList),TRN,FLAT,ICON('edoutp.jpg')
                       BUTTON,AT(8,386),USE(?Button:RepairNotes),TRN,FLAT,ICON('repnotp.jpg')
                       BUTTON,AT(531,386),USE(?Close),TRN,FLAT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
local       Class
LookupButtonCode    Procedure(Long f:FieldNumber,String f:Field),String
LookupFieldCode     Procedure(Long f:FieldNumber,String f:Field,Long f:Lookup),String
PartForceFaultCode    Procedure(Long f:PartFieldNumber,String f:FaultCode, Long f:JobFieldNumber),Byte
            End ! local       Class


PassLinkedFaultCodeQueue    Queue(DefLinkedFaultCodeQueue)
                            End
!Save Entry Fields Incase Of Lookup
look:job:ProductCode                Like(job:ProductCode)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

BuildNumbersQueue        Routine
    Free(FieldNumbersQueue)
    Free(PromptNumbersQueue)
    Free(LookupNumbersQueue)
    Free(DescriptionNumbersQueue)
    field:ScreenOrder = 1
    field:FieldNumber = ?tmp:FaultCode_1
    Add(FieldNumbersQueue)
    field:ScreenOrder = 2
    field:FieldNumber = ?tmp:FaultCode_2
    Add(FieldNumbersQueue)
    field:ScreenOrder = 3
    field:FieldNumber = ?tmp:FaultCode_3
    Add(FieldNumbersQueue)
    field:ScreenOrder = 4
    field:FieldNumber = ?tmp:FaultCode_4
    Add(FieldNumbersQueue)
    field:ScreenOrder = 5
    field:FieldNumber = ?tmp:FaultCode_5
    Add(FieldNumbersQueue)
    field:ScreenOrder = 6
    field:FieldNumber = ?tmp:FaultCode_6
    Add(FieldNumbersQueue)
    field:ScreenOrder = 7
    field:FieldNumber = ?tmp:FaultCode_7
    Add(FieldNumbersQueue)
    field:ScreenOrder = 8
    field:FieldNumber = ?tmp:FaultCode_8
    Add(FieldNumbersQueue)
    field:ScreenOrder = 9
    field:FieldNumber = ?tmp:FaultCode_9
    Add(FieldNumbersQueue)
    field:ScreenOrder = 10
    field:FieldNumber = ?tmp:FaultCode_10
    Add(FieldNumbersQueue)
    field:ScreenOrder = 11
    field:FieldNumber = ?tmp:FaultCode_11
    Add(FieldNumbersQueue)
    field:ScreenOrder = 12
    field:FieldNumber = ?tmp:FaultCode_12
    Add(FieldNumbersQueue)
    field:ScreenOrder = 13
    field:FieldNumber = ?tmp:FaultCode_13
    Add(FieldNumbersQueue)
    field:ScreenOrder = 14
    field:FieldNumber = ?tmp:FaultCode_14
    Add(FieldNumbersQueue)
    field:ScreenOrder = 15
    field:FieldNumber = ?tmp:FaultCode_15
    Add(FieldNumbersQueue)
    field:ScreenOrder = 16
    field:FieldNumber = ?tmp:FaultCode_16
    Add(FieldNumbersQueue)
    field:ScreenOrder = 17
    field:FieldNumber = ?tmp:FaultCode_17
    Add(FieldNumbersQueue)
    field:ScreenOrder = 18
    field:FieldNumber = ?tmp:FaultCode_18
    Add(FieldNumbersQueue)
    field:ScreenOrder = 19
    field:FieldNumber = ?tmp:FaultCode_19
    Add(FieldNumbersQueue)
    field:ScreenOrder = 20
    field:FieldNumber = ?tmp:FaultCode_20
    Add(FieldNumbersQueue)
    lookup:ScreenOrder = 1
    lookup:FieldNumber = ?Button:Lookup:1
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 2
    lookup:FieldNumber = ?Button:Lookup:2
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 3
    lookup:FieldNumber = ?Button:Lookup:3
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 4
    lookup:FieldNumber = ?Button:Lookup:4
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 5
    lookup:FieldNumber = ?Button:Lookup:5
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 6
    lookup:FieldNumber = ?Button:Lookup:6
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 7
    lookup:FieldNumber = ?Button:Lookup:7
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 8
    lookup:FieldNumber = ?Button:Lookup:8
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 9
    lookup:FieldNumber = ?Button:Lookup:9
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 10
    lookup:FieldNumber = ?Button:Lookup:10
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 11
    lookup:FieldNumber = ?Button:Lookup:11
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 12
    lookup:FieldNumber = ?Button:Lookup:12
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 13
    lookup:FieldNumber = ?Button:Lookup:13
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 14
    lookup:FieldNumber = ?Button:Lookup:14
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 15
    lookup:FieldNumber = ?Button:Lookup:15
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 16
    lookup:FieldNumber = ?Button:Lookup:16
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 17
    lookup:FieldNumber = ?Button:Lookup:17
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 18
    lookup:FieldNumber = ?Button:Lookup:18
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 19
    lookup:FieldNumber = ?Button:Lookup:19
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 20
    lookup:FieldNumber = ?Button:Lookup:20
    Add(LookupNumbersQueue)
    prompt:ScreenOrder = 1
    prompt:FieldNumber = ?tmp:FaultCodePrompt:1
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 2
    prompt:FieldNumber = ?tmp:FaultCodePrompt:2
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 3
    prompt:FieldNumber = ?tmp:FaultCodePrompt:3
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 4
    prompt:FieldNumber = ?tmp:FaultCodePrompt:4
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 5
    prompt:FieldNumber = ?tmp:FaultCodePrompt:5
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 6
    prompt:FieldNumber = ?tmp:FaultCodePrompt:6
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 7
    prompt:FieldNumber = ?tmp:FaultCodePrompt:7
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 8
    prompt:FieldNumber = ?tmp:FaultCodePrompt:8
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 9
    prompt:FieldNumber = ?tmp:FaultCodePrompt:9
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 10
    prompt:FieldNumber = ?tmp:FaultCodePrompt:10
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 11
    prompt:FieldNumber = ?tmp:FaultCodePrompt:11
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 12
    prompt:FieldNumber = ?tmp:FaultCodePrompt:12
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 13
    prompt:FieldNumber = ?tmp:FaultCodePrompt:13
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 14
    prompt:FieldNumber = ?tmp:FaultCodePrompt:14
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 15
    prompt:FieldNumber = ?tmp:FaultCodePrompt:15
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 16
    prompt:FieldNumber = ?tmp:FaultCodePrompt:16
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 17
    prompt:FieldNumber = ?tmp:FaultCodePrompt:17
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 18
    prompt:FieldNumber = ?tmp:FaultCodePrompt:18
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 19
    prompt:FieldNumber = ?tmp:FaultCodePrompt:19
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 20
    prompt:FieldNumber = ?tmp:FaultCodePrompt:20
    Add(PromptNumbersQueue)
    description:ScreenOrder = 1
    description:FieldNumber = ?tmp:FaultCodeDescription:1
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 2
    description:FieldNumber = ?tmp:FaultCodeDescription:2
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 3
    description:FieldNumber = ?tmp:FaultCodeDescription:3
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 4
    description:FieldNumber = ?tmp:FaultCodeDescription:4
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 5
    description:FieldNumber = ?tmp:FaultCodeDescription:5
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 6
    description:FieldNumber = ?tmp:FaultCodeDescription:6
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 7
    description:FieldNumber = ?tmp:FaultCodeDescription:7
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 8
    description:FieldNumber = ?tmp:FaultCodeDescription:8
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 9
    description:FieldNumber = ?tmp:FaultCodeDescription:9
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 10
    description:FieldNumber = ?tmp:FaultCodeDescription:10
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 11
    description:FieldNumber = ?tmp:FaultCodeDescription:11
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 12
    description:FieldNumber = ?tmp:FaultCodeDescription:12
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 13
    description:FieldNumber = ?tmp:FaultCodeDescription:13
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 14
    description:FieldNumber = ?tmp:FaultCodeDescription:14
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 15
    description:FieldNumber = ?tmp:FaultCodeDescription:15
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 16
    description:FieldNumber = ?tmp:FaultCodeDescription:16
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 17
    description:FieldNumber = ?tmp:FaultCodeDescription:17
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 18
    description:FieldNumber = ?tmp:FaultCodeDescription:18
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 19
    description:FieldNumber = ?tmp:FaultCodeDescription:19
    Add(DescriptionNumbersQueue)
    description:ScreenOrder = 20
    description:FieldNumber = ?tmp:FaultCodeDescription:20
    Add(DescriptionNumbersQueue)

    Loop x# = 1 To 20
        Get(PromptNumbersQueue,x#)
        prompt:FieldNumber{prop:Text} = ''
        Get(LookupNumbersQueue,x#)
        lookup:FieldNumber{prop:Hide} = 1
        lookup:FieldNumber{prop:Disable} = 0
        Get(DescriptionNumbersQueue,x#)
        description:FieldNumber{prop:Text} = ''
        Get(FieldNumbersQueue,x#)
        field:FieldNumber{prop:Hide} = 1
        field:FieldNumber{prop:ReadOnly} = 0

    End ! Loop x# = 1 To 20

CheckStockModelFaultCodes        Routine
    Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
    wpr:Ref_Number = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.Next()
            Break
        End ! If Access:WARPARTS.Next()
        If wpr:Ref_Number <> job:Ref_Number
            Break
        End ! If wpr:Ref_Number <> job:Ref_Number

        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number = wpr:Part_Ref_Number
        If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
            If sto:Assign_Fault_Codes = 'YES'
                Access:STOMODEL.Clearkey(stm:Mode_Number_Only_Key)
                stm:Ref_Number = sto:Ref_Number
                stm:Model_NUmber = job:Model_Number
                If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                    Access:STOMJFAU.Clearkey(stj:FieldKey)
                    stj:RefNumber = stm:RecordNumber
                    If Access:STOMJFAU.TryFetch(stj:FieldKey) = Level:Benign
                        Case maf:Field_Number
                        Of 1
                            If stj:FaultCode1 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 30/10/2007)
                                If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 30/10/2007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode1
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 30/10/2007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = tmp:FaultCode[maf:ScreenOrder]
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 30/10/2007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 30/10/2007)
                                    If NewIndex# > CurrentIndex#
                                        tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode1
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode1
                                End ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                            End ! If stj:FaultCode1 <> ''
                        Of 2
                            If stj:FaultCode2 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 30/20/2007)
                                If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 30/20/2007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode2
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 30/20/2007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = tmp:FaultCode[maf:ScreenOrder]
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 30/20/2007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 30/20/2007)
                                    If NewIndex# > CurrentIndex#
                                        tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode2
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode2
                                End ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                            End ! If stj:FaultCode2 <> ''

                        Of 3
                            If stj:FaultCode3 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 30/30/3007)
                                If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 30/30/3007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode3
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 30/30/3007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = tmp:FaultCode[maf:ScreenOrder]
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 30/30/3007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 30/30/3007)
                                    If NewIndex# > CurrentIndex#
                                        tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode3
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode3
                                End ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                            End ! If stj:FaultCode3 <> ''

                        Of 4
                            If stj:FaultCode4 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 40/40/4007)
                                If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 40/40/4007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode4
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 40/40/4007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = tmp:FaultCode[maf:ScreenOrder]
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 40/40/4007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 40/40/4007)
                                    If NewIndex# > CurrentIndex#
                                        tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode4
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode4
                                End ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                            End ! If stj:FaultCode4 <> ''

                        Of 5
                            If stj:FaultCode5 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 50/50/5007)
                                If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 50/50/5007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode5
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 50/50/5007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = tmp:FaultCode[maf:ScreenOrder]
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 50/50/5007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 50/50/5007)
                                    If NewIndex# > CurrentIndex#
                                        tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode5
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode5
                                End ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                            End ! If stj:FaultCode5 <> ''

                        Of 6
                            If stj:FaultCode6 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 60/60/6007)
                                If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 60/60/6007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode6
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 60/60/6007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = tmp:FaultCode[maf:ScreenOrder]
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 60/60/6007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 60/60/6007)
                                    If NewIndex# > CurrentIndex#
                                        tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode6
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode6
                                End ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                            End ! If stj:FaultCode6 <> ''

                        Of 7
                            If stj:FaultCode7 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 70/70/7007)
                                If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 70/70/7007)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode7
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 70/70/7007)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = tmp:FaultCode[maf:ScreenOrder]
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 70/70/7007)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 70/70/7007)
                                    If NewIndex# > CurrentIndex#
                                        tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode7
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode7
                                End ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                            End ! If stj:FaultCode7 <> ''

                        Of 8
                            If stj:FaultCode8 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 80/80/8008)
                                If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 80/80/8008)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode8
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 80/80/8008)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = tmp:FaultCode[maf:ScreenOrder]
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 80/80/8008)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 80/80/8008)
                                    If NewIndex# > CurrentIndex#
                                        tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode8
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode8
                                End ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                            End ! If stj:FaultCode8 <> ''

                        Of 9
                            If stj:FaultCode9 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 90/90/9009)
                                If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 90/90/9009)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode9
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 90/90/9009)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = tmp:FaultCode[maf:ScreenOrder]
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 90/90/9009)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 90/90/9009)
                                    If NewIndex# > CurrentIndex#
                                        tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode9
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode9
                                End ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                            End ! If stj:FaultCode9 <> ''

                        Of 10
                            If stj:FaultCode10 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 100/100/100010)
                                If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 100/100/100010)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode10
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 100/100/100010)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = tmp:FaultCode[maf:ScreenOrder]
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 100/100/100010)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 100/100/100010)
                                    If NewIndex# > CurrentIndex#
                                        tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode10
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode10
                                End ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                            End ! If stj:FaultCode10 <> ''

                        Of 11
                            If stj:FaultCode11 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 110/110/110011)
                                If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 110/110/110011)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode11
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 110/110/110011)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = tmp:FaultCode[maf:ScreenOrder]
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 110/110/110011)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 110/110/110011)
                                    If NewIndex# > CurrentIndex#
                                        tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode11
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode11
                                End ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                            End ! If stj:FaultCode11 <> ''

                        Of 12
                            If stj:FaultCode12 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 120/120/120012)
                                If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 120/120/120012)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode12
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 120/120/120012)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = tmp:FaultCode[maf:ScreenOrder]
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 120/120/120012)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 120/120/120012)
                                    If NewIndex# > CurrentIndex#
                                        tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode12
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode12
                                End ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                            End ! If stj:FaultCode12 <> ''

                        Of 13
                            If stj:FaultCode13 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 130/130/130013)
                                If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 130/130/130013)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode13
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 130/130/130013)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = tmp:FaultCode[maf:ScreenOrder]
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 130/130/130013)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 130/130/130013)
                                    If NewIndex# > CurrentIndex#
                                        tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode13
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode13
                                End ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                            End ! If stj:FaultCode13 <> ''

                        Of 14
                            If stj:FaultCode14 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 140/140/140014)
                                If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 140/140/140014)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode14
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 140/140/140014)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = tmp:FaultCode[maf:ScreenOrder]
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 140/140/140014)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 140/140/140014)
                                    If NewIndex# > CurrentIndex#
                                        tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode14
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode14
                                End ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                            End ! If stj:FaultCode14 <> ''

                        Of 15
                            If stj:FaultCode15 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 150/150/150015)
                                If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 150/150/150015)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode15
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 150/150/150015)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = tmp:FaultCode[maf:ScreenOrder]
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 150/150/150015)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 150/150/150015)
                                    If NewIndex# > CurrentIndex#
                                        tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode15
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode15
                                End ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                            End ! If stj:FaultCode15 <> ''

                        Of 16
                            If stj:FaultCode16 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 160/160/160016)
                                If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 160/160/160016)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode16
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 160/160/160016)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = tmp:FaultCode[maf:ScreenOrder]
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 160/160/160016)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 160/160/160016)
                                    If NewIndex# > CurrentIndex#
                                        tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode16
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode16
                                End ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                            End ! If stj:FaultCode16 <> ''

                        Of 17
                            If stj:FaultCode17 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 170/170/170017)
                                If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 170/170/170017)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode17
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 170/170/170017)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = tmp:FaultCode[maf:ScreenOrder]
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 170/170/170017)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 170/170/170017)
                                    If NewIndex# > CurrentIndex#
                                        tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode17
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode17
                                End ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                            End ! If stj:FaultCode17 <> ''

                        Of 18
                            If stj:FaultCode18 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 180/180/180018)
                                If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 180/180/180018)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode18
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 180/180/180018)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = tmp:FaultCode[maf:ScreenOrder]
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 180/180/180018)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 180/180/180018)
                                    If NewIndex# > CurrentIndex#
                                        tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode18
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode18
                                End ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                            End ! If stj:FaultCode18 <> ''

                        Of 19
                            If stj:FaultCode19 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 190/190/190019)
                                If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 190/190/190019)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode19
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 190/190/190019)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = tmp:FaultCode[maf:ScreenOrder]
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 190/190/190019)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 190/190/190019)
                                    If NewIndex# > CurrentIndex#
                                        tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode19
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode19
                                End ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                            End ! If stj:FaultCode19 <> ''

                        Of 20
                            If stj:FaultCode20 <> ''
                                ! This is a fault code against the stock part/model number (DBH: 200/200/200020)
                                If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    ! A fault code has already been filled in. Compare the Repair Indeces (DBH: 200/200/200020)
                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = stj:FaultCode20
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the new field (DBH: 200/200/200020)
                                        NewIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                                    mfo:Manufacturer = job:Manufacturer
                                    mfo:Field_Number = maf:Field_Number
                                    mfo:Field = tmp:FaultCode[maf:ScreenOrder]
                                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                                        ! The repair index of the current field (DBH: 200/200/200020)
                                        CurrentIndex# = mfo:SkillLevel
                                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

                                    ! The highest repair index goes into the field (DBH: 200/200/200020)
                                    If NewIndex# > CurrentIndex#
                                        tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode20
                                    End ! If NewIndex# > CurrentIndex#
                                Else ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                                    tmp:FaultCode[maf:ScreenOrder] = stj:FaultCode20
                                End ! If tmp:FaultCode[maf:ScreenOrder] <> ''
                            End ! If stj:FaultCode20 <> ''

                        End ! Case maf:Field_Number
                    End ! If Access:STOMJFAU.TryFetch(stj:FieldKey) = Level:Benign
                End ! If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
            End ! If sto:Assign_Fault_Codes = 'YES'
        End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
    End ! Loop
FaultCodes        Routine
Data
local:Field        String(255)
local:ReadOnly          Byte(0)
local:Hide              Byte(0)
local:Required          Byte(0)
local:ViewUnavailable   Byte(0)
local:AmendUnavailable  Byte(0)
local:RelatedField      Long()
local:RepairIndex       Long()
local:SetFaultCode      String(255)
Code

    ! Can you the user see unavailable fault codes (DBH: 19/10/2007)
    If SecurityCheck('VIEW UNAVAILABLE FAULT CODES') = Level:Benign
        local:ViewUnavailable = 1
        If SecurityCheck('AMEND UNAVAILABLE FAULT CODES') = Level:Benign
            local:AmendUnavailable = 1
        End ! If SecurityCheck('AMEND UNAVAILABLE FAULT CODES') = Level:Benign
    End ! If SecurityCheck('VIEW UNAVAILABLE FAULT CODES') = Level:Benign

    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number = wob:HeadAccountNumber
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        Access:LOCATION.Clearkey(loc:Location_Key)
        loc:Location = tra:SiteLocation
        If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign

        End ! If Access:LOCATION.TryFetch(loa:Location_Key) = Level:Benign
    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign

    If f:Req = InsertRecord
        If f:Type = 'C' Or f:Type = 'S'
            Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
            cha:Charge_Type = f:CType
            If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
                If cha:Force_Warranty = 'YES'
                    tmp:Required = 1
                End ! If cha:Force_Warranty = 'YES'
            End ! If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
        End ! If f:Type = 'C' Or f:Type = 'S'
        If (f:Type = 'W' Or f:Type = 'S') And tmp:Required = 0
            Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
            cha:Charge_Type = f:CType
            If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
                If cha:Force_Warranty = 'YES'
                    tmp:Required = 1
                End ! If cha:Force_Warranty = 'YES'
            End ! If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
        End ! If (f:Type = 'W' Or f:Type = 'S') And tmp:Required = 0
    Else ! If f:Req = InsertRecord
        Access:CHARTYPE.Clearkey(cha:Charge_Type_Key)
        cha:Charge_Type = f:Ctype
        If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
            If cha:Force_Warranty = 'YES'
                If f:RType <> ''
                    Case f:Type
                    Of 'C' ! Chargeable Job
                        Access:REPTYDEF.Clearkey(rtd:ChaManRepairTypeKey)
                        rtd:Manufacturer = job:Manufacturer
                        rtd:Chargeable = 'YES'
                        rtd:Repair_Type = f:RType
                        If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                            If rtd:CompFaultCoding
                                tmp:Required = 1
                            End ! If rtd:CompFaultCoding
                        End ! If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                    Of 'W' ! Warranty Job
                        Access:REPTYDEF.Clearkey(rtd:WarManRepairTypeKey)
                        rtd:Manufacturer = job:Manufacturer
                        rtd:Warranty = 'YES'
                        rtd:Repair_Type = f:RType
                        If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                            If rtd:CompFaultCoding
                                tmp:Required = 1
                            End ! If rtd:CompFaultCoding
                        End ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                    End ! Case f:Type
                Else ! If f:RType <> ''
                End ! If f:RType <> ''
            Else ! If cha:Force_Warranty = 'YES'
            End ! If cha:Force_Warranty = 'YES'
        End ! If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
    End ! If f:Req = InsertRecord

    Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
    maf:Manufacturer = job:Manufacturer
    maf:ScreenOrder = 0
    Set(maf:ScreenOrderKey,maf:ScreenOrderKey)
    Loop
        If Access:MANFAULT.Next()
            Break
        End ! If Access:MANFAULT.Next()
        If maf:Manufacturer <> job:Manufacturer
            Break
        End ! If maf:Manufacturer <> f:Manfacturer
        If maf:ScreenOrder = 0
            Cycle
        End ! If maf:ScreenOrder = 0

        local:Hide = 0
        local:Required = 0
        local:ReadOnly = 0
        If f:Type = 'W' Or f:Type = 'S'
            If maf:Compulsory_At_Booking = 'YES'
                local:Hide= 0
                If tmp:Required
                    local:Required = 1
                End ! If tmp:Required
            End ! If maf:Compulsory_At_Booking = 'YES'
            If f:Req <> InsertRecord
                If maf:Compulsory = 'YES' And tmp:Required
                    local:Required = 1
                End ! If maf:Compulsory = 'YES' Or tmp:Required
            End ! If f:Req <> InsertRecord
        End ! If f:Type = 'W' Or f:Type = 'S'

        If f:Type = 'C' Or f:Type = 'S'
            If maf:CharCompulsoryBooking
                local:Hide= 0
                If tmp:Required
                    local:Required = 1
                End ! If tmp:Required
            End ! If maf:CharCompulsoryBooking
            If f:Req <> InsertRecord
                If maf:CharCompulsory And tmp:Required
                    local:Required = 1
                End ! If maf:ChaCompulsory Or tmp:Required
            End ! If f:Req <> InsertRecord
        End ! If f:Type = 'C' Or f:Type = 'S'
        If f:Req <> InsertRecord
            local:Hide = 0
        End ! If f:Req <> InsertRecord

        prompt:ScreenOrder = maf:ScreenOrder
        Get(PromptNumbersQueue,prompt:ScreenOrder)
        field:ScreenOrder = maf:ScreenOrder
        Get(FieldNumbersQueue,field:ScreenOrder)
        lookup:ScreenOrder = maf:ScreenOrder
        Get(LookupNumbersQueue,lookup:ScreenOrder)
        description:ScreenOrder = maf:ScreenOrder
        Get(DescriptionNumbersQueue,lookup:ScreenOrder)

        If maf:RestrictAvailability
            If (maf:RestrictServiceCentre = 1 And ~loc:Level1) Or |
                (maf:RestrictServiceCentre = 2 And ~loc:Level2) Or |
                (maf:RestrictServiceCentre = 3 And ~loc:Level3)
                If local:ViewUnavailable = 0
                    local:Hide = 1
                Else ! If local:ViewUnavailable = 0
                    If local:AmendUnavailable = 0
                        local:ReadOnly = 1
                    End ! If local:AmendUnavailable = 0
                End ! If local:ViewUnavailable = 0
            End ! (maf:RestrictServiceCentre = 3 And ~loc:Level3)
        End ! If maf:RestrictAvailability

        If maf:NotAvailable
            If local:ViewUnavailable = 0
                local:Hide = 1
            Else ! If local:Unavailable = 0
                If local:AmendUnavailable = 0
                    If tmp:FaultCode[maf:ScreenOrder] = ''
                        local:Hide = 1
                    Else ! If tmp:FaultCode[maf:ScreenOrder] = ''
                        local:ReadOnly = 1
                    End ! If tmp:FaultCode[maf:ScreenOrder] = ''
                End ! If local:AmendUnavailable
            End ! If local:Unavailable = 0
        End ! If maf:NotAvailable

        If local:Hide = 0
            ! Hide fault code if related field is blank (DBH: 22/10/2007)
            If maf:HideRelatedCodeIfBlank
                local:RelatedField = maf:BlankRelatedCode

                save_maf_id = Access:MANFAULT.SaveFile()
                Access:MANFAULT.Clearkey(maf:Field_Number_Key)
                maf:Manufacturer = job:Manufacturer
                maf:Field_Number = local:RelatedField
                If Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign
                    ! Get screen number of related code, and see if there is any data in it (DBH: 22/10/2007)
                    If tmp:FaultCode[maf:ScreenOrder] = ''
                        local:Hide = 1
                    End ! If tmp:FaultCode[maf:ScreenOrder] = ''
                End ! If Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign
                Access:MANFAULT.RestoreFile(save_maf_id)
            End ! If maf:HideRelatedCodeIfBlank
        End ! If local:Hide = 0

        If Clip(job:Third_Party_Site) <> ''
            If local:Required = 1
                If maf:NotCompulsoryThirdParty
                    ! Fault Code not required if job gone to third party (DBH: 22/10/2007)
                    local:Required = 0
                    If maf:HideThirdParty
                        ! Hide fault code if job has gone to 3rd Party (DBH: 22/10/2007)
                        local:Hide = 1
                    End ! If maf:HideThirdParty
                End ! If maf:NotCompulsoryThirdParty
            End ! If local:Required = 1
        End ! If Clip(job:Third_Party_Site) <> ''

        If local:Hide = 0
            field:FieldNumber{prop:Hide} = 0
            prompt:FieldNumber{prop:Text} = maf:Field_Name
            tmp:FaultCodePrompt[maf:ScreenOrder] = maf:Field_Name

            If local:Required = 0
                ! Check part fault codes to see if any of them are forcing a job fault code (DBH 25/10/2007 11:44:37) - TrkBs: 9248
                Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
                wpr:Ref_Number = job:Ref_Number
                Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                Loop
                    If Access:WARPARTS.Next()
                        Break
                    End ! If Access:WARPARTS.Next()
                    If wpr:Ref_Number <> job:Ref_Number
                        Break
                    End ! If wpr:Ref_Number <> job:Ref_Number

                    If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                        local:Required = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(2,wpr:Fault_Code2,maf:Field_Number)
                        local:Required = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(3,wpr:Fault_Code3,maf:Field_Number)
                        local:Required = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(4,wpr:Fault_Code4,maf:Field_Number)
                        local:Required = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(5,wpr:Fault_Code5,maf:Field_Number)
                        local:Required = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(6,wpr:Fault_Code6,maf:Field_Number)
                        local:Required = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(7,wpr:Fault_Code7,maf:Field_Number)
                        local:Required = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(8,wpr:Fault_Code8,maf:Field_Number)
                        local:Required = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(9,wpr:Fault_Code9,maf:Field_Number)
                        local:Required = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(10,wpr:Fault_Code10,maf:Field_Number)
                        local:Required = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(11,wpr:Fault_Code11,maf:Field_Number)
                        local:Required = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                    If local.PartForceFaultCode(12,wpr:Fault_Code12,maf:Field_Number)
                        local:Required = 1
                        Break
                    End ! If local.PartForceFaultCode(1,wpr:Fault_Code1,maf:Field_Number)
                End ! Loop
            End ! If local:Required = 0

            If local:Required
                field:FieldNumber{prop:Req} = 1
            Else ! If local:Required

                field:FieldNumber{prop:Req} = 0
            End ! If local:Required

            Case maf:Field_Type
            Of 'DATE'
                field:FieldNumber{prop:Text} = Clip(maf:DateType)
                If ~glo:WebJob
                    ! Show Lookup Calendar
                    lookup:FieldNumber{prop:Hide} = 0
                End ! If ~glo:WebJob
            Of 'STRING'
                If maf:RestrictLength
                    field:FieldNumber{prop:Text} = '@s' & maf:LengthTo
                Else ! If maf:RestrictLength
                    If maf:Field_Number = 10 Or maf:Field_Number = 11 Or maf:Field_Number = 12
                        field:FieldNumber{prop:Text} = '@s255'
                    Else ! If maf:Field_Number = 10 Or maf:Field_Number = 11 Or maf:Field_Number = 12
                        field:FieldNumber{prop:Text} = '@s30'
                    End ! If maf:Field_Number = 10 Or maf:Field_Number = 11 Or maf:Field_Number = 12
                End ! If maf:RestrictLength

                If maf:Lookup = 'YES'
                    lookup:FieldNumber{prop:Hide} = 0
                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                    mfo:Manufacturer = job:Manufacturer
                    mfo:Field_Number = maf:Field_Number
                    mfo:Field = tmp:FaultCode[maf:ScreenOrder]
                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                        description:FieldNumber{prop:Text} = mfo:Description
                    Else ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                        description:FieldNumber{prop:Text} = ''
                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                    If maf:Force_Lookup = 'YES'
                        field:FieldNumber{prop:ReadOnly} = 1
                    End ! If maf:Force_Lookup = 'YES'
                Else ! If maf:Lookup = 'YES'
                    description:FieldNumber{prop:Text} = ''
                    lookup:FieldNumber{prop:Hide} = 1
                End ! If maf:Lookup = 'YES'
            Of 'NUMBER'
                If maf:RestrictLength
                    field:FieldNumber{prop:Text} = '@n_' & maf:LengthTo
                Else ! If maf:RestrictLength
                    field:FieldNumber{prop:Text} = '@n_9'
                End ! If maf:RestrictLength
                If maf:Lookup
                    lookup:FieldNumber{prop:Hide} = 0
                Else ! If maf:Lookup
                    lookup:FieldNumber{prop:Hide} = 1
                End ! If maf:Lookup
            End ! Case maf:Field_Type

            ! If this is the generic fault, disable if the access level isn't there (DBH: 17/10/2007)
            If maf:GenericFault
                If SecurityCheck('GENERIC FAULT - AMEND')
                    field:FieldNumber{prop:ReadOnly} = 1
                    lookup:FieldNumber{prop:Hide} = 1
                End ! If SecurityCheck('GENERIC FAULT - AMEND')
            End ! If maf:GenericFault

            ! Make the out fault read only
            If maf:MainFault
                field:FieldNumber{prop:ReadOnly} = 1
                lookup:FieldNumber{prop:Hide} = 1
            End ! If maf:MainFault

            If local:ReadOnly
                lookup:FieldNumber{prop:Hide} = 1
                field:FieldNumber{prop:ReadOnly} = 1
            End ! If local:ReadOnly


            If maf:Lookup = 'YES'
                ! Autofill with primary lookup field (DBH: 23/10/2007)
                If tmp:FaultCode[maf:ScreenOrder] = '' And field:FieldNumber{prop:Hide} = 0 And field:FieldNumber{prop:ReadOnly} = 0
                    Access:MANFAULO.Clearkey(mfo:PrimaryLookupKey)
                    mfo:Manufacturer = job:Manufacturer
                    mfo:Field_Number = maf:Field_Number
                    mfo:PrimaryLookup = 1
                    If Access:MANFAULO.TryFetch(mfo:PrimaryLookupKey) = Level:Benign
                        ! If the job type is set on the primary lookup, make sure that it isn't assigned to the wrong job type (DBH: 23/10/2007)
                        If mfo:JobTypeAvailability = 0
                            tmp:FaultCode[maf:ScreenOrder] = mfo:Field
                        Elsif mfo:JobTypeAvailability = 1 ! If mfo:JobTypeAvailability = 0
                            If job:Chargeable_Job = 'YES'
                                tmp:FaultCode[maf:ScreenOrder] = mfo:Field
                            End ! If job:Chargeable_Job = 'YES'
                        Else
                            If job:Warranty_job = 'YES'
                                tmp:FaultCode[maf:ScreenOrder] = mfo:Field
                            End ! If job:Warranty_job = 'YES'
                        End ! If mfo:JobTypeAvailability = 0
                    End ! If Access:MANFAULO.TryFetch(mfo:PrimaryLookupKey) = Level:Benign
                End ! If tmp:FaultCode[maf:ScreenOrder] = ''
                If tmp:FaultCode[maf:ScreenOrder] = '' And field:FieldNumber{prop:Hide} = 0 And field:FieldNumber{prop:ReadOnly} = 0
                    ! Autofill fault code if only one entry in lookup
                    local:Field = ''
                    Count# = 0
                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                    mfo:Manufacturer = job:Manufacturer
                    mfo:Field_Number = maf:Field_Number
                    Set(mfo:Field_Key,mfo:Field_Key)
                    Loop
                        If Access:MANFAULO.Next()
                            Break
                        End ! If Access:MANFAULO.Next()
                        If mfo:Manufacturer <> job:Manufacturer
                            Break
                        End ! If mfo:Manufacturer <> f:Man
                        If mfo:Field_Number <> maf:Field_Number
                            Break
                        End ! If mfo:Field_Number <> maf:Field_Number

                        If mfo:JobTypeAvailability = 1
                            If job:Chargeable_Job <> 'YES'
                                Cycle
                            End ! If job:Chargeable_Job <> 'YES'
                        End ! If mfo:JobTypeAvailability = 1
                        If mfo:JobTypeAvailability = 2
                            If job:Warranty_Job <> 'YES'
                                Cycle
                            End ! If job:Warranty_Job <> 'YES'
                        End ! If mfo:JobTypeAvailability = 2

                        Count# += 1
                        If Count# > 1
                            local:Field = ''
                            Break
                        End ! If Count# > 1
                        local:Field = mfo:Field
                    End ! Loop
                    If local:Field <> ''
                        tmp:FaultCode[maf:ScreenOrder] = local:Field
                    End ! If local:Field <> ''
                End ! If tmp:FaultCode[maf:ScreenOrder] = ''
            End ! If maf:Lookup = 'YES'

            ! Autofill the field with the date of purchase (DBH: 22/10/2007)
            If maf:FillFromDOP
                If tmp:FaultCode[maf:ScreenOrder] = 0
                    tmp:FaultCode[maf:ScreenOrder] = job:DOP
                End ! If tmp:FaultCode[maf:ScreenOrder] = ''
            End ! If maf:FillFromDOP

            If tmp:FaultCode[maf:ScreenOrder] = ''
                Do CheckStockModelFaultCodes
            End ! If tmp:FaultCode[maf:ScreenOrder] = ''

            ! Check Out faults to see if any autofill a fault code (DBH: 09/11/2007)
            local:RepairIndex = 0
            local:SetFaultCode = ''
            Access:MANFAULT_ALIAS.Clearkey(maf_ali:MainFaultKey)
            maf_ali:Manufacturer = job:Manufacturer
            maf_ali:MainFault = 1
            If Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:benign
                ! Get the main fault code (DBH: 09/11/2007)
                Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
                joo:JobNumber = job:Ref_Number
                Set(joo:JobNumberKey,joo:JobNumberKey)
                Loop
                    If Access:JOBOUTFL.Next()
                        Break
                    End ! If Access:JOBOUTFL.Next()
                    If joo:JobNumber <> job:Ref_Number
                        Break
                    End ! If joo:JobNumber <> job:Ref_Number

                    ! See if the outfault will outfill this fault code (DBH: 09/11/2007)
                    Access:MANFAULO.Clearkey(mfo:Field_Key)
                    mfo:Manufacturer = job:Manufacturer
                    mfo:Field_Number = maf_ali:Field_Number
                    mfo:Field = joo:FaultCode
                    If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                        If mfo:SetJobFaultCode
                            If mfo:SelectJobFaultCode = maf:Field_Number
                                If mfo:SkillLevel >= local:RepairIndex
                                    ! Incase of more than one, use the highest repair index (DBH: 09/11/2007)
                                    local:SetFaultCode = mfo:JobFaultCodeValue
                                End ! If mfo:SkillLevel >= local:RepairIndex
                            End ! If mfo:SelectJobFaultCode = maf:Field_Number
                        End ! If mfo:SetJobFaultCode
                    End ! If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                End ! Loop
            End ! If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:benign
            If local:SetFaultCode <> ''
                tmp:FaultCode[maf:ScreenOrder] = local:SetFaultCode
            End ! If local:SetFaultCode <> ''
        Else ! If local:Hide= 0
            prompt:FieldNumber{prop:Text} = ''
            description:FieldNumber{prop:Text} = ''
            field:FieldNumber{prop:Hide} = 1
            lookup:FieldNumber{prop:Hide} = 1
        End ! If local:Hide= 0

        ! Check the parts for any autofilling fault codes (DBH: 30/10/2007)
    End ! Loop
    Bryan.CompFieldColour()
    Display()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020450'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('JobFaultCodes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:JOBSE3.Open
  Relate:MANFAULO_ALIAS.Open
  Relate:MANFAULT_ALIAS.Open
  Relate:MANFAUPA_ALIAS.Open
  Relate:MANFPALO_ALIAS.Open
  Relate:USERS_ALIAS.Open
  Relate:WEBJOB.Open
  Access:MANFAULT.UseFile
  Access:MANFAULO.UseFile
  Access:CHARTYPE.UseFile
  Access:REPTYDEF.UseFile
  Access:JOBS.UseFile
  Access:MODPROD.UseFile
  Access:MANUFACT.UseFile
  Access:MODELNUM.UseFile
  Access:LOCATION.UseFile
  Access:TRADEACC.UseFile
  Access:JOBOUTFL.UseFile
  Access:MANFAUPA.UseFile
  Access:MANFPALO.UseFile
  Access:MANFPARL.UseFile
  Access:MANFAURL.UseFile
  Access:WARPARTS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
  maf:Manufacturer = job:Manufacturer
  maf:ScreenOrder = 0
  If Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign
      Case Missive('One or more of the Manufacturer Fault Codes have not been assigned a "Screen Order".'&|
        '|The fault codes screen will not function correctly until this is done.','ServiceBase 3g',|
                     'mstop.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
      Post(Event:CloseWindow)
  End ! If Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign
  
  ! Save Use Numbers
  Do BuildNumbersQueue
  
  !Fill Fields In
  Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
  maf:Manufacturer = job:Manufacturer
  maf:ScreenOrder = 0
  Set(maf:ScreenOrderKey,maf:ScreenOrderKey)
  Loop
      If Access:MANFAULT.Next()
          Break
      End ! If Access:MANFAULT.Next()
      If maf:Manufacturer <> job:Manufacturer
          Break
      End ! If maf:Manufacturer <> maf:Man
      If maf:ScreenOrder = 0
          Cycle
      End ! If maf:ScreenOrder = 0
      FaultCodeQueue.FaultNumber = maf:Field_Number
      Get(FaultCodeQueue,FaultCodeQueue.FaultNumber)
      tmp:FaultCode[maf:ScreenOrder] = FaultCodeQueue.FaultCode
  End ! Loop
  
!  ! Is product code required? (DBH: 17/10/2007)
!  If ProductCodeRequired(job:Manufacturer) = Level:Benign
!      ?job:ProductCode{prop:Hide} = 0
!      ?job:ProductCode:Prompt{prop:Hide} = 0
!      ?Lookup:ProductCode{prop:Hide} = 0
!  End ! If ProductCodeRequired(job:Manufacturer)

  Case ProductCodeRequired(job:Manufacturer)
      of Level:Benign
          ?job:ProductCode{prop:Hide} = 0
          ?job:ProductCode:Prompt{prop:Hide} = 0
          ?Lookup:ProductCode{prop:Hide} = 0
          If SecurityCheck('PRODUCT CODE - AMEND')
              ?job:ProductCode{prop:Disable} = 1
              ?Lookup:ProductCode{prop:Disable} = 1
          End ! If SecurityCheck('PRODUCT CODE - AMEND')
      of Level:notify
          ?job:ProductCode{prop:Hide} = 0
          ?job:ProductCode:Prompt{prop:Hide} = 0
          ?Lookup:ProductCode{prop:Hide} = 0
          ?job:ProductCode{prop:Req} = 1
          If SecurityCheck('PRODUCT CODE - AMEND')
              ?job:ProductCode{prop:Disable} = 1
              ?Lookup:ProductCode{prop:Disable} = 1
          End ! If SecurityCheck('PRODUCT CODE - AMEND')
      ELSE    !Level:Fatal
          ?job:ProductCode{prop:Hide} = 1
          ?job:ProductCode:Prompt{prop:Hide} = 1
          ?Lookup:ProductCode{prop:Hide} = 1
  End !case ProductCodeRequired(job:Manufacturer) 
  
  ! Is MSN Required? (DBH: 17/10/2007)
  If MSNRequired(job:Manufacturer) = Level:Benign
      ! Has the MSN already been verified? (DBH: 17/10/2007)
      Found# = 0
      Access:AUDIT.Clearkey(aud:TypeRefKey)
      aud:Ref_Number = job:Ref_Number
      aud:Type = 'JOB'
      Set(aud:TypeRefKey,aud:TypeRefKey)
      Loop
          If Access:AUDIT.Next()
              Break
          End ! If Access:AUDIT.Next()
          If aud:Ref_Number <> job:Ref_Number
              Break
          End ! If aud:Ref_Number <> job:Ref_Number
          If aud:Type <> 'JOB'
              Break
          End ! If aud:Type <> 'JOB'
          If Instring('MSN VERIFICATION',Upper(aud:Action),1,1)
              Found# = 1
              Break
          End ! If Instring('MSN VERIFICATION',Upper(aud:Action),1,1)
      End ! Loop
      If Found# = 0
          ?tmp:MSN{prop:Hide} = 0
          ?tmp:MSN:Prompt{prop:Hide} = 0
          ?Button:VerifyMSN{prop:Hide} = 0
          ?Sheet:FaultCodes{prop:Disable} = 1
      End ! If Found# = 0
  End ! If MSNRequired(job:Manfacturer) = Level:Benign

    !TB13287 - J - 06/01/15 - Adding specific needs faults
    Access:Jobse3.clearkey(jobe3:KeyRefNumber)
    jobe3:RefNumber = job:ref_number
    if access:jobse3.fetch(jobe3:KeyRefNumber)
        !error - just to be sure
        jobe3:SpecificNeeds = 0
    END

    if jobe3:SpecificNeeds then
        unhide(?jobe3:FaultCode21:Prompt)
        unhide(?jobe3:FaultCode21)
        unhide(?ButtonSelectSNCode)
        unhide(?Prompt46)
        unhide(?jobe3:SN_Software_Installed)
    END
      ! Save Window Name
   AddToLog('Window','Open','JobFaultCodes')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?job:ProductCode{Prop:Tip} AND ~?Lookup:ProductCode{Prop:Tip}
     ?Lookup:ProductCode{Prop:Tip} = 'Select ' & ?job:ProductCode{Prop:Tip}
  END
  IF ?job:ProductCode{Prop:Msg} AND ~?Lookup:ProductCode{Prop:Msg}
     ?Lookup:ProductCode{Prop:Msg} = 'Select ' & ?job:ProductCode{Prop:Msg}
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:JOBSE3.Close
    Relate:MANFAULO_ALIAS.Close
    Relate:MANFAULT_ALIAS.Close
    Relate:MANFAUPA_ALIAS.Close
    Relate:MANFPALO_ALIAS.Close
    Relate:USERS_ALIAS.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','JobFaultCodes')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickNewModelProductCodes
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonSelectSNCode
      jobe3:FaultCode21 = SelectSNCode(jobe3:FaultCode21)
      display()
    OF ?Close
      ! Inserting (DBH 29/11/2007) # 9585 - Double check all of the fault code lookups are correct
      Found# = 0
      Loop x# = 1 To 20
          field:ScreenOrder = x#
          Get(FieldNumbersQueue,field:ScreenOrder)
          If field:FieldNumber{prop:Hide} = 1
              Cycle
          End ! If field:FieldNumber{prop:Hide} = 1
          If Contents(field:FieldNumber) = ''
              Cycle
          End ! If Contents(field:FieldNumber) = ''
          Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
          maf:Manufacturer = job:Manufacturer
          maf:ScreenOrder = x#
          If Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign
              If maf:Lookup = 'YES' And maf:Force_Lookup = 'YES'
                  Access:MANFAULO.Clearkey(mfo:HideFieldKey)
                  mfo:NotAvailable = False
                  mfo:Manufacturer = job:Manufacturer
                  mfo:Field_Number = maf:Field_Number
                  mfo:Field = Contents(field:FieldNumber)
                  If Access:MANFAULO.TryFetch(mfo:HideFieldKey)
                      field:FieldNumber{prop:Color} = color:Red
                      lookup:ScreenOrder = x#
      !                Get(LookupNumbersQueue,lookup:ScreenOrder)
      !                Post(Event:Accepted,lookup:FieldNumber)
                      Found# = 1
                  End ! If Access:MANFAULO.TryFetch(mfo:HideFieldKey) = Level:Benign
              End ! If maf:Lookup = 'YES' And maf:Force_Lookup = 'YES'
          End ! If Access:MANFAULT.TryFetch(maf:ScreenOrderKey) = Level:Benign
      End ! Loop x# = 1 To 20
      
      
      
        !TB13287 - J - 06/01/15 - adding specific needs
        if jobe3:SpecificNeeds
      
      !      If f:Req <> InsertRecord
      !          !software must be installed before completion
      !          if jobe3:SN_Software_Installed = 0
      !          
      !              Found# = 1
      !            
      !              Case Missive('Warning! The mobile device booked in is a Specific Needs Device. ' &|
      !                  '<13,10>Please confirm that you have installed the Specific Needs Software before the repair can be completed.','ServiceBase 3g',|
      !                           'mquest.jpg','\OK')
      !              Of 1 ! OK Button
      !              End ! Case Missive
      !          END !if software installed
      !      END !If f:Req <> InsertRecord
      
            !SN TYPE must also be completed
            If clip(jobe3:FaultCode21) = '' then
                Found# = 1
                Case Missive('Warning! The mobile device booked in is a Specific Needs Device. ' &|
                    '<13,10>Please select the Specific Needs type.','ServiceBase 3g',|
                             'mquest.jpg','\OK')
                Of 1 ! OK Button
                End ! Case Missive
            END !if no SN type
      
            Access:jobse3.update()
      
        END !if jobe3:SpecificNeeds
      
      
      If Found#
          Display()
          Cycle
      End ! If Found#
      ! End (DBH 29/11/2007) #9585
      
      
      ! Write the fault codes back to the passed queue
      Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
      maf:Manufacturer = job:Manufacturer
      maf:ScreenOrder = 0
      Set(maf:ScreenOrderKey,maf:ScreenOrderKey)
      Loop
          If Access:MANFAULT.Next()
              Break
          End ! If Access:MANFAULT.Next()
          If maf:Manufacturer <> job:Manufacturer
              Break
          End ! If maf:Manufacturer <> f:Man
          If maf:ScreenOrder = 0
              Cycle
          End ! If maf:ScreenOrder = 0
          FaultCodeQueue.FaultNumber = maf:Field_Number
          Get(FaultCodeQueue,FaultCodeQueue.FaultNumber)
          FaultCodeQUeue.FaultCode = tmp:FaultCode[maf:ScreenOrder]
          Put(FaultCodeQueue)
      End ! Loop
      Post(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button:VerifyMSN
      ThisWindow.Update
      ! Verify MSN
      If tmp:MSN = ''
          Select(?tmp:MSN)
          Cycle
      End ! If tmp:MSN = ''
      
      ! Validate MSN (DBH: 17/10/2007)
      If Len(Clip(tmp:MSN)) = 11 And job:Manufacturer = 'ERICSSON'
          tmp:MSN = Sub(tmp:MSN,2,10)
          Display()
      End ! If Len(Clip(tmp:MSN)) = 11 And job:Manufacturer = 'ERICSSON'
      
      If CheckLength('MSN',job:Model_Number,tmp:MSN)
          Select(?tmp:MSN)
          Cycle
      Else ! If CheckLength('MSN',job:Model_Number,tmp:MSN)
          Access:MANUFACT.Clearkey(man:Manufacturer_Key)
          man:Manufacturer = job:Manufacturer
          If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
              If man:ApplyMSNFormat
                  If CheckFaultFormat(tmp:MSN,man:MSNFormat)
                      Case Missive('M.S.N. failed format validation.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Select(?tmp:MSN)
                      Cycle
                  End ! If CheckFaultFormat(tmp:MSN,man:MSNFormat)
              End ! If man:ApplyMSNFormat
          End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
      End ! If CheckLength('MSN',job:Model_Number,tmp:MSN)
      
      If tmp:MSN = job:MSN
          ?Sheet:FaultCodes{prop:Disable} = 0
          ?tmp:MSN{prop:ReadOnly} = 1
          ?Button:VerifyMSN{prop:Hide} = 1
          If AddToAudit(job:Ref_Number,'JOB','MSN VERIFICATION: PASSED','MSN: ' & Clip(job:MSN))
      
          End ! If AddToAudit(job:Ref_Number,'JOB','MSN VERIFICATION: PASSED','MSN: ' & Clip(job:MSN)
          Bryan.CompFieldColour()
      Else ! If tmp:MSN = job:MSN
          tmp:CurrentMSN = job:MSN
          If MismatchMSN(tmp:MSN)
              If job:MSN <> tmp:CurrentMSN
                  ! The MSN was validated but it's different from the current MSN (DBH: 17/10/2007)
                  If AddToAudit(job:Ref_Number,'JOB','MSN VERIFICATION: AMENDMENT','ORIGINAL MSN: ' & Clip(tmp:CurrentMSN) & '<13,10>NEW MSN: ' & Clip(job:MSN))
      
                  End ! If AddToAudit(job:Ref_Number,'JOB','MSN VERIFICATION: AMENDMENT','ORIGINAL MSN: ' & Clip(tmp:MSN) & '<13,10>NEW MSN: ' & Clip(job:MSN))
              Else ! If tmp:MSN <> job:MSN
                  If AddToAudit(job:Ref_Number,'JOB','MSN VERIFICATION: PASSED','MSN: ' & Clip(job:MSN))
      
                  End ! If AddToAudit(job:Ref_Number,'JOB','MSN VERIFICATION: PASSED','MSN: ' & Clip(job:MSN)
              End ! If tmp:MSN <> job:MSN
              tmp:MSN = job:MSN
              ?Sheet:FaultCodes{prop:Disable} = 0
              ?tmp:MSN{prop:ReadOnly} = 1
              ?Button:VerifyMSN{prop:Hide} = 1
              Bryan.CompFieldColour()
          Else ! If Mismatch(tmp:MSN)
              tmp:MSN = ''
              Select(?tmp:MSN)
          End ! If Mismatch(tmp:MSN)
      End ! If tmp:MSN = job:MSN
      Display()
      
    OF ?job:ProductCode
      glo:Select50 = job:Model_Number
      IF job:ProductCode OR ?job:ProductCode{Prop:Req}
        mop:ProductCode = job:ProductCode
        mop:ModelNumber = job:Model_Number
        !Save Lookup Field Incase Of error
        look:job:ProductCode        = job:ProductCode
        IF Access:MODPROD.TryFetch(mop:ProductCodeKey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job:ProductCode = mop:ProductCode
          ELSE
            CLEAR(mop:ModelNumber)
            !Restore Lookup On Error
            job:ProductCode = look:job:ProductCode
            SELECT(?job:ProductCode)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      glo:Select50 = ''
    OF ?Lookup:ProductCode
      ThisWindow.Update
      glo:Select50 = job:Model_Number
      mop:ProductCode = job:ProductCode
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job:ProductCode = mop:ProductCode
          Select(?+1)
      ELSE
          Select(?job:ProductCode)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:ProductCode)
      glo:Select50 = ''
    OF ?tmp:FaultCode_11
      num# = 11
      tmp:FaultCode[num#] = local.LookupFieldCode(num#,tmp:FaultCode[num#],?Button:Lookup:11)
      Do FaultCodes
      Display()
    OF ?tmp:FaultCode_1
      tmp:FaultCode[1] = local.LookupFieldCode(1,tmp:FaultCode[1],?Button:Lookup:1)
      Do FaultCodes
      Display()
    OF ?Button:Lookup:12
      ThisWindow.Update
      num# = 12
      tmp:FaultCode[num#] = local.LookupButtonCode(num#,tmp:FaultCode[num#])
      Do FaultCodes
      Display()
    OF ?Button:Lookup:1
      ThisWindow.Update
      tmp:FaultCode[1] = local.LookupButtonCode(1,tmp:FaultCode[1])
      Do FaultCodes
      Display()
    OF ?tmp:FaultCode_12
      num# = 12
      tmp:FaultCode[num#] = local.LookupFieldCode(num#,tmp:FaultCode[num#],?Button:Lookup:12)
      Do FaultCodes
      Display()
    OF ?tmp:FaultCode_2
      num# = 2
      tmp:FaultCode[num#] = local.LookupFieldCode(num#,tmp:FaultCode[num#],?Button:Lookup:2)
      Do FaultCodes
      Display()
    OF ?Button:Lookup:13
      ThisWindow.Update
      num# = 13
      tmp:FaultCode[num#] = local.LookupButtonCode(num#,tmp:FaultCode[num#])
      Do FaultCodes
      Display()
    OF ?tmp:FaultCode_3
      num# = 3
      tmp:FaultCode[num#] = local.LookupFieldCode(num#,tmp:FaultCode[num#],?Button:Lookup:3)
      Do FaultCodes
      Display()
    OF ?tmp:FaultCode_13
      num# = 13
      tmp:FaultCode[num#] = local.LookupFieldCode(num#,tmp:FaultCode[num#],?Button:Lookup:13)
      Do FaultCodes
      Display()
    OF ?Button:Lookup:3
      ThisWindow.Update
      num# = 3
      tmp:FaultCode[num#] = local.LookupButtonCode(num#,tmp:FaultCode[num#])
      Do FaultCodes
      Display()
    OF ?tmp:FaultCode_4
      num# = 4
      tmp:FaultCode[num#] = local.LookupFieldCode(num#,tmp:FaultCode[num#],?Button:Lookup:4)
      Do FaultCodes
      Display()
    OF ?Button:Lookup:4
      ThisWindow.Update
      num# = 4
      tmp:FaultCode[num#] = local.LookupButtonCode(num#,tmp:FaultCode[num#])
      Do FaultCodes
      Display()
    OF ?tmp:FaultCode_14
      num# = 14
      tmp:FaultCode[num#] = local.LookupFieldCode(num#,tmp:FaultCode[num#],?Button:Lookup:14)
      Do FaultCodes
      Display()
    OF ?Button:Lookup:2
      ThisWindow.Update
      num# = 2
      tmp:FaultCode[num#] = local.LookupButtonCode(num#,tmp:FaultCode[num#])
      Do FaultCodes
      Display()
    OF ?tmp:FaultCode_5
      num# = 5
      tmp:FaultCode[num#] = local.LookupFieldCode(num#,tmp:FaultCode[num#],?Button:Lookup:5)
      Do FaultCodes
      Display()
    OF ?Button:Lookup:5
      ThisWindow.Update
      num# = 5
      tmp:FaultCode[num#] = local.LookupButtonCode(num#,tmp:FaultCode[num#])
      Do FaultCodes
      Display()
    OF ?Button:Lookup:14
      ThisWindow.Update
      num# = 14
      tmp:FaultCode[num#] = local.LookupButtonCode(num#,tmp:FaultCode[num#])
      Do FaultCodes
      Display()
    OF ?tmp:FaultCode_15
      num# = 15
      tmp:FaultCode[num#] = local.LookupFieldCode(num#,tmp:FaultCode[num#],?Button:Lookup:15)
      Do FaultCodes
      Display()
    OF ?Button:Lookup:15
      ThisWindow.Update
      num# = 15
      tmp:FaultCode[num#] = local.LookupButtonCode(num#,tmp:FaultCode[num#])
      Do FaultCodes
      Display()
    OF ?Button:Lookup:6
      ThisWindow.Update
      num# = 6
      tmp:FaultCode[num#] = local.LookupButtonCode(num#,tmp:FaultCode[num#])
      Do FaultCodes
      Display()
    OF ?Button:Lookup:16
      ThisWindow.Update
      num# = 16
      tmp:FaultCode[num#] = local.LookupButtonCode(num#,tmp:FaultCode[num#])
      Do FaultCodes
      Display()
    OF ?tmp:FaultCode_6
      num# = 6
      tmp:FaultCode[num#] = local.LookupFieldCode(num#,tmp:FaultCode[num#],?Button:Lookup:6)
      Do FaultCodes
      Display()
    OF ?tmp:FaultCode_16
      num# = 16
      tmp:FaultCode[num#] = local.LookupFieldCode(num#,tmp:FaultCode[num#],?Button:Lookup:16)
      Do FaultCodes
      Display()
    OF ?tmp:FaultCode_7
      num# = 7
      tmp:FaultCode[num#] = local.LookupFieldCode(num#,tmp:FaultCode[num#],?Button:Lookup:7)
      Do FaultCodes
      Display()
    OF ?tmp:FaultCode_17
      num# = 17
      tmp:FaultCode[num#] = local.LookupFieldCode(num#,tmp:FaultCode[num#],?Button:Lookup:17)
      Do FaultCodes
      Display()
    OF ?Button:Lookup:17
      ThisWindow.Update
      num# = 17
      tmp:FaultCode[num#] = local.LookupButtonCode(num#,tmp:FaultCode[num#])
      Do FaultCodes
      Display()
    OF ?Button:Lookup:7
      ThisWindow.Update
      num# = 7
      tmp:FaultCode[num#] = local.LookupButtonCode(num#,tmp:FaultCode[num#])
      Do FaultCodes
      Display()
    OF ?tmp:FaultCode_8
      num# = 8
      tmp:FaultCode[num#] = local.LookupFieldCode(num#,tmp:FaultCode[num#],?Button:Lookup:8)
      Do FaultCodes
      Display()
    OF ?Button:Lookup:8
      ThisWindow.Update
      num# = 8
      tmp:FaultCode[num#] = local.LookupButtonCode(num#,tmp:FaultCode[num#])
      Do FaultCodes
      Display()
    OF ?tmp:FaultCode_18
      num# = 18
      tmp:FaultCode[num#] = local.LookupFieldCode(num#,tmp:FaultCode[num#],?Button:Lookup:18)
      Do FaultCodes
      Display()
    OF ?Button:Lookup:18
      ThisWindow.Update
      num# = 18
      tmp:FaultCode[num#] = local.LookupButtonCode(num#,tmp:FaultCode[num#])
      Do FaultCodes
      Display()
    OF ?tmp:FaultCode_9
      num# = 9
      tmp:FaultCode[num#] = local.LookupFieldCode(num#,tmp:FaultCode[num#],?Button:Lookup:9)
      Do FaultCodes
      Display()
    OF ?Button:Lookup:9
      ThisWindow.Update
      num# = 9
      tmp:FaultCode[num#] = local.LookupButtonCode(num#,tmp:FaultCode[num#])
      Do FaultCodes
      Display()
    OF ?tmp:FaultCode_19
      num# = 19
      tmp:FaultCode[num#] = local.LookupFieldCode(num#,tmp:FaultCode[num#],?Button:Lookup:19)
      Do FaultCodes
      Display()
    OF ?Button:Lookup:19
      ThisWindow.Update
      num# = 19
      tmp:FaultCode[num#] = local.LookupButtonCode(num#,tmp:FaultCode[num#])
      Do FaultCodes
      Display()
    OF ?Button:Lookup:10
      ThisWindow.Update
      num# = 10
      tmp:FaultCode[num#] = local.LookupButtonCode(num#,tmp:FaultCode[num#])
      Do FaultCodes
      Display()
    OF ?tmp:FaultCode_10
      num# = 10
      tmp:FaultCode[num#] = local.LookupFieldCode(num#,tmp:FaultCode[num#],?Button:Lookup:10)
      Do FaultCodes
      Display()
    OF ?tmp:FaultCode_20
      num# = 20
      tmp:FaultCode[num#] = local.LookupFieldCode(num#,tmp:FaultCode[num#],?Button:Lookup:20)
      Do FaultCodes
      Display()
    OF ?jobe3:SN_Software_Installed
      !TB13287 - J - 05/01/15 always add an audit trail when ticked/unticked
      IF ~window{Prop:AcceptAll} THEN
          if jobe3:SN_Software_Installed = true then
              If AddToAudit(job:Ref_Number,'JOB','SPECIFIC NEEDS SOFTWARE INSTALLED','SPECIFIC NEEDS SOFTWARE INSTALLED')
                  !error
              END
          ELSE
              !Do not allow to untick if job completed or in QA process
              if job:Completed = 'YES' or job:Current_Status[1] = '6'
                  Missive('This is a specific needs device. '&|
                          '|The specific needs software cannot be uninstalled after completion.','ServiceBase 3g',|
                                       'mexclam.jpg','/OK')
                  jobe3:SN_Software_Installed = true
                  display()
              ELSE
                  If AddToAudit(job:Ref_Number,'JOB','SPECIFIC NEEDS SOFTWARE UNINSTALLED','SPECIFIC NEEDS SOFTWARE UNINSTALLED.')
                      !error
                  END
              END
          END
      END
    OF ?Button:Lookup:20
      ThisWindow.Update
      num# = 20
      tmp:FaultCode[num#] = local.LookupButtonCode(num#,tmp:FaultCode[num#])
      Do FaultCodes
      Display()
    OF ?Button:Lookup:11
      ThisWindow.Update
      num# = 11
      tmp:FaultCode[num#] = local.LookupButtonCode(num#,tmp:FaultCode[num#])
      Do FaultCodes
      Display()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020450'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020450'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020450'&'0')
      ***
    OF ?Button:EditOutFaultsList
      ThisWindow.Update
          Access:MANFAULT.Clearkey(maf:MainFaultKey)
          maf:Manufacturer = job:Manufacturer
          maf:MainFault = 1
          If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
              ! Go through all the fault codes and see if THIS fault code is a secondary linked code (DBH: 05/11/2007)
              Free(PassLinkedFaultCodeQueue)
              Loop num# = 1 To 20
                  If num# = maf:ScreenOrder
                      Cycle
                  End ! If num# = f:FieldNumber
      
                  ! Only count visible fault codes (DBH: 05/11/2007)
                  field:ScreenOrder = num#
                  Get(FieldNumbersQueue,field:ScreenOrder)
                  If ~Error()
                      If field:FieldNumber{prop:Hide} = 1
                          Cycle
                      End ! If field:FieldNumber{prop:Hide} = 1
                  End ! If ~Error()
      
                  If tmp:FaultCode[num#] <> ''
                      ! Get the record number of the fault code value (DBH: 05/11/2007)
                      Access:MANFAULT_ALIAS.Clearkey(maf_ali:ScreenOrderKey)
                      maf_ali:Manufacturer = job:Manufacturer
                      maf_ali:ScreenOrder = num#
                      If Access:MANFAULT_ALIAS.TryFetch(maf_ali:ScreenOrderKey) = Level:Benign
                          Access:MANFAULO_ALIAS.Clearkey(mfo_ali:Field_Key)
                          mfo_ali:Manufacturer = job:Manufacturer
                          mfo_ali:Field_Number = maf_ali:Field_Number
                          mfo_ali:Field = tmp:FaultCode[num#]
                          If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:Field_Key) = Level:Benign
                              Found# = 0
                              Access:MANFAURL.Clearkey(mnr:FieldKey)
                              mnr:MANFAULORecordNumber = mfo_ali:RecordNumber
                              mnr:FieldNumber = maf:Field_Number
                              Set(mnr:FieldKey,mnr:FieldKey)
                              Loop
                                  If Access:MANFAURL.Next()
                                      Break
                                  End ! If Access:MANFAURL.Next()
                                  If mnr:MANFAULORecordNumber <> mfo_ali:RecordNumber
                                      Break
                                  End ! If mnr:MANFAULORecordNumber <> mfo_ali:RecordNumber
                                  If mnr:FieldNumber <> maf:Field_Number
                                      Break
                                  End ! If mnr:FieldNumber <> maf:Field_Number
                                  Found# = 1
                                  Break
                              End ! Loop
                              If Found# = 1
                                  PassLinkedFaultCodeQueue.RecordNumber = mfo_ali:RecordNumber
                                  Add(PassLinkedFaultCodeQueue)
                              End ! If Found# = 1
                          End ! If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:Field_Key) = Level:Benign
                      End ! If Access:MANFAULT_ALIAS.TryFetch(maf_ali:ScreenOrderKey) = Level:Benign
                  End ! If tmp:FaultCode[num#] <> ''
              End ! Loop num# = 1 To 20
              OutFaultList(job:Manufacturer,job:Ref_Number,PassLinkedFaultCodeQueue)
          End ! If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
      Do FaultCodes
      Display()
    OF ?Button:RepairNotes
      ThisWindow.Update
      BrowseRepairNotes(job:Ref_Number)
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      Do FaultCodes
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

local.LookupFieldCode      Procedure(Long f:FieldNumber, String f:Field, Long f:Lookup)
    Code
    If f:Field <> ''
        Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
        maf:Manufacturer = job:Manufacturer
        maf:ScreenOrder = f:FieldNumber
        If Access:MANFAULT.Tryfetch(maf:ScreenOrderKey) = Level:Benign
            ! Found
            Access:MANFAULO.Clearkey(mfo:HideFieldKey)
            mfo:NotAvailable = False
            mfo:Manufacturer = job:Manufacturer
            mfo:Field_Number = maf:Field_Number
            mfo:Field        = f:Field
            If Access:MANFAULO.Tryfetch(mfo:HideFieldKey) = Level:Benign
                ! Found
                Access:MANFAUEX.ClearKey(max:ModelNumberKey)
                max:MANFAULORecordNumber = mfo:RecordNumber
                max:ModelNumber          = job:Model_Number
                If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
                    ! Found
                    Case Missive('The Model Number on this job has been "Excluded" from using the selected Fault Code.', 'ServiceBase 3g', |
                                 'mstop.jpg', '/OK')
                    Of 1 ! OK Button
                    End ! Case Missive
                    Return ''
                Else ! If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
                    ! Error
                    If maf:ReplicateFault = 'YES'
                        If jbn:Fault_Description = ''
                            jbn:Fault_Description = Clip(mfo:Description)
                        Else ! If jbn:Fault_Description = ''
                            If ~Instring(Clip(mfo:Description), jbn:Fault_Description, 1, 1)
                                jbn:Fault_Description = Clip(jbn:Fault_Description) & '<13,10>' & Clip(mfo:Description)
                            End ! If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                        End ! If jbn:Fault_Description = ''
                    End ! If maf:ReplicateFault = 'YES'

                    If maf:ReplicateInvoice = 'YES'
                        If jbn:Invoice_Text = ''
                            jbn:Invoice_Text = Clip(mfo:Description)
                        Else ! If jbn:Invoice_Text = ''
                            If ~Instring(Clip(mfo:Description), jbn:Invoice_Text, 1, 1)
                                jbn:Invoice_Text = Clip(jbn:Invoice_Text) & '<13,10>' & Clip(mfo:Description)
                            End ! If ~Instring(Clip(mfo:Description),jbn:Invoice_Text,1,1)
                        End ! If jbn:Invoice_Text = ''
                    End ! If maf:ReplicateInvoice = 'YES'
                    Access:JOBNOTES.Update()
                End ! If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
            Else ! If Access:MANFAULO.Tryfetch(mfo:HideFieldKey) = Level:Benign
                ! Error
                If maf:Force_Lookup = 'YES'
                    f:Field = ''
                    Post(Event:Accepted, f:Lookup)
                End ! If maf:Force_Lookup = 'YES'
            End ! If Access:MANFAULO.Tryfetch(mfo:HideFieldKey) = Level:Benign
        Else ! If Access:MANFAULT.Tryfetch(maf:Field_Number_Key) = Level:Benign
        ! Error
        End ! If Access:MANFAULT.Tryfetch(maf:Field_Number_Key) = Level:Benign
    End ! If f:Field <> ''
    Return f:Field
local.LookupButtonCode        Procedure(Long f:FieldNumber,String f:Field)
local:JobType                 Byte(0)
local:LinkedRecordNumber      Long(0)
Code
    Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
    maf:Manufacturer    = job:Manufacturer
    maf:ScreenOrder    = f:FieldNumber
    If Access:MANFAULT.Tryfetch(maf:ScreenOrderKey) = Level:Benign
        ! Found
        If maf:Field_Type = 'DATE'
            Return TINCALENDARStyle1(f:Field)
        Else ! If maf:Field_Type = 'DATE'
            SaveRequest# = GlobalRequest
            GlobalRequest = SelectRecord

            ! Used to restrict the lookup to char/warr (DBH: 23/10/2007)
            If job:Chargeable_Job = 'YES'
                local:JobType = 1
                If job:Warranty_Job = 'YES'
                    local:JobType = 0
                End ! If job:Warranty_Job = 'YES'
            Else ! If job:Chargeable_Job = 'YES'
                local:JobType = 2
            End ! If job:Chargeable_Job = 'YES'

            ! Go through all the fault codes and see if THIS fault code is a secondary linked code (DBH: 05/11/2007)
            Free(PassLinkedFaultCodeQueue)
            Loop num# = 1 To 20
                If num# = f:FieldNumber
                    Cycle
                End ! If num# = f:FieldNumber

                ! Only count visible fault codes (DBH: 05/11/2007)
                field:ScreenOrder = num#
                Get(FieldNumbersQueue,field:ScreenOrder)
                If ~Error()
                    If field:FieldNumber{prop:Hide} = 1
                        Cycle
                    End ! If field:FieldNumber{prop:Hide} = 1
                End ! If ~Error()

                If tmp:FaultCode[num#] <> ''
                    ! Get the record number of the fault code value (DBH: 05/11/2007)
                    Access:MANFAULT_ALIAS.Clearkey(maf_ali:ScreenOrderKey)
                    maf_ali:Manufacturer = job:Manufacturer
                    maf_ali:ScreenOrder = num#
                    If Access:MANFAULT_ALIAS.TryFetch(maf_ali:ScreenOrderKey) = Level:Benign
                        Access:MANFAULO_ALIAS.Clearkey(mfo_ali:Field_Key)
                        mfo_ali:Manufacturer = job:Manufacturer
                        mfo_ali:Field_Number = maf_ali:Field_Number
                        mfo_ali:Field = tmp:FaultCode[num#]
                        If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:Field_Key) = Level:Benign
                            Found# = 0
                            Access:MANFAURL.Clearkey(mnr:FieldKey)
                            mnr:MANFAULORecordNumber = mfo_ali:RecordNumber
                            mnr:FieldNumber = maf:Field_Number
                            Set(mnr:FieldKey,mnr:FieldKey)
                            Loop
                                If Access:MANFAURL.Next()
                                    Break
                                End ! If Access:MANFAURL.Next()
                                If mnr:MANFAULORecordNumber <> mfo_ali:RecordNumber
                                    Break
                                End ! If mnr:MANFAULORecordNumber <> mfo_ali:RecordNumber
                                If mnr:FieldNumber <> maf:Field_Number
                                    Break
                                End ! If mnr:FieldNumber <> maf:Field_Number
                                Found# = 1
                                Break
                            End ! Loop
                            If Found# = 1
                                PassLinkedFaultCodeQueue.RecordNumber = mfo_ali:RecordNumber
                                PassLinkedFaultCodeQueue.FaultType = 'J'
                                Add(PassLinkedFaultCodeQueue)
                            End ! If Found# = 1
                        End ! If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:Field_Key) = Level:Benign
                    End ! If Access:MANFAULT_ALIAS.TryFetch(maf_ali:ScreenOrderKey) = Level:Benign
                End ! If tmp:FaultCode[num#] <> ''
            End ! Loop num# = 1 To 20

            Do LookupRelatedPartFaultCodes

            BrowseJobFaultCodeLookup(job:Manufacturer,maf:Field_Number,local:JobType,'',0,PassLinkedFaultCodeQueue)

            GlobalRequest = SaveRequest#

            If GlobalResponse = RequestCompleted
                Access:MANFAUEX.ClearKey(max:ModelNumberKey)
                max:MANFAULORecordNumber = mfo:RecordNumber
                max:ModelNumber          = job:Model_Number
                If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
                    !Found
                    Case Missive('The Model Number on this job has been "Excluded" from using the selected Fault Code.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                    Return f:Field
                Else !If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
                    !Error
                End !If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign

                If f:Field <> mfo:Field
                    ! A new fault code value has been picked. Check the secondary fault codes (DBH: 05/11/2007)
                    Loop num# = 1 To 20
                        If num# = f:FieldNumber
                            Cycle
                        End ! If num# = maf:Field

                        field:ScreenOrder = num#
                        Get(FieldNumbersQueue,field:ScreenOrder)
                        If ~Error()
                            If field:FieldNumber{prop:Hide} = 1
                                Cycle
                            End ! If field:FieldNumber{prop:Hide} = 1
                        End ! If ~Error()

                        Access:MANFAULT_ALIAS.Clearkey(maf_ali:ScreenOrderKey)
                        maf_ali:Manufacturer = job:Manufacturer
                        maf_ali:ScreenOrder = num#
                        If Access:MANFAULT_ALIAS.TryFetch(maf_ali:ScreenOrderKey) = Level:Benign

                        Else ! If Access:MANFAULT_ALIAS.TryFetch(maf_ali:ScreenOrderKey) = Level:Benign
                            Cycle
                        End ! If Access:MANFAULT_ALIAS.TryFetch(maf_ali:ScreenOrderKey) = Level:Benign

                        ! How many secondary values are there? (DBH: 05/11/2007)
                        Count# = 0
                        Access:MANFAURL.Clearkey(mnr:FieldKey)
                        mnr:MANFAULORecordNumber = mfo:RecordNumber
                        mnr:FieldNumber = maf_ali:Field_Number
                        Set(mnr:FieldKey,mnr:FieldKey)
                        Loop
                            If Access:MANFAURL.Next()
                                Break
                            End ! If Access:MANFAURL.Next()
                            If mnr:MANFAULORecordNumber <> mfo:RecordNumber
                                Break
                            End ! If mnr:MANFAULORecordNumber <> mfo:RecordNumber
                            If mnr:FieldNumber <> maf_ali:Field_Number
                                Break
                            End ! If mnr:FieldNmber <> maf_ali:Field_Number

                            local:LinkedRecordNumber = mnr:LinkedRecordNumber
                            Count# += 1
                            If Count# > 1
                                Break
                            End ! If Count# > 1
                        End ! Loop

                        If Count# = 1
                            ! Autofil (DBH: 05/11/2007)
                            Access:MANFAULO_ALIAS.Clearkey(mfo_ali:RecordNumberKey)
                            mfo_ali:RecordNumber = local:LinkedRecordNumber
                            If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RecordNumberKey) = Level:Benign
                                tmp:FaultCode[num#] = mfo_ali:Field
                            End ! If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RecordNumberKey) = Level:Benign
                        ELsif Count# > 1
                            ! Blank, the user must select a new fault code (DBH: 05/11/2007)
                            tmp:FaultCode[num#] = ''
                            Display()
                        End ! If Count# > 1
                    End ! Loop num# = 1 To 20

                End ! If f:Field <> mfo:Field

                f:Field = mfo:Field

                Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
                jbn:RefNumber   = job:Ref_Number
                If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                    ! Found
                    If maf:ReplicateFault = 'YES'
                        If jbn:Fault_Description = ''
                            jbn:Fault_Description = Clip(mfo:Description)
                        Else ! If jbn:Fault_Description = ''
                            If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                                jbn:Fault_Description = Clip(jbn:Fault_Description) & '<13,10>' & Clip(mfo:Description)
                            End ! If ~Instring(Clip(mfo:Description),jbn:Fault_Description,1,1)
                        End ! If jbn:Fault_Description = ''
                    End ! If maf:ReplicateFault = 'YES'

                    If maf:ReplicateInvoice = 'YES'
                        If jbn:Invoice_Text = ''
                            jbn:Invoice_Text = Clip(mfo:Description)
                        Else ! If jbn:Invoice_Text = ''
                            If ~Instring(Clip(mfo:Description),jbn:Invoice_Text,1,1)
                                jbn:Invoice_Text = Clip(jbn:Invoice_Text) & '<13,10>' & Clip(mfo:Description)
                            End ! If ~Instring(Clip(mfo:Description),jbn:Invoice_Text,1,1)
                        End ! If jbn:Invoice_Text = ''
                    End ! If maf:ReplicateInvoice = 'YES'
                    Access:JOBNOTES.Update()
                Else ! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                    ! Error
                End ! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
            End ! If GlobalResponse = RequestCompleted
            Return f:Field
        End ! If maf:Field_Type = 'DATE'
    Else ! If Access:MANFAULT.Tryfetch(maf:Field_Number_Key) = Level:Benign
        ! Error
    End ! If Access:MANFAULT.Tryfetch(maf:Field_Number_Key) = Level:Benign


LookupRelatedPartFaultCodes       Routine
Data
local:FaultCodeValue            String(255)
Code
    Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
    wpr:Ref_Number = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.Next()
            Break
        End ! If Access:WARPARTS.Next()
        If wpr:Ref_Number <> job:Ref_Number
            Break
        End ! If wpr:Ref_Number <> job:Ref_Number
        Loop f# = 1 To 12
            Case f#
            Of 1
                If wpr:Fault_Code1 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code1
            Of 2
                If wpr:Fault_Code2 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code2
            Of 3
                If wpr:Fault_Code3 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code3
            Of 4
                If wpr:Fault_Code4 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code4
            Of 5
                If wpr:Fault_Code5 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code5
            Of 6
                If wpr:Fault_Code6 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code6
            Of 7
                If wpr:Fault_Code7 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code7
            Of 8
                If wpr:Fault_Code8 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code8
            Of 9
                If wpr:Fault_Code9 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code9
            Of 10
                If wpr:Fault_Code10 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code10
            Of 11
                If wpr:Fault_Code11 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code11
            Of 12
                If wpr:Fault_Code12 = ''
                    Cycle
                End ! If wpr:Fault_Code1 = ''
                local:FaultCodeValue = wpr:Fault_Code12
            End ! Case

            Access:MANFAUPA.Clearkey(map:Field_Number_Key)
            map:Manufacturer = job:Manufacturer
            map:Field_Number = f#
            If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
                ! Found
                If map:UseRelatedJobCode
                    save_MANFAULT_ALIAS_id = Access:MANFAULT_ALIAS.SaveFile()
                    Access:MANFAULT_ALIAS.Clearkey(maf_ali:MainFaultKey)
                    maf_ali:Manufacturer = job:Manufacturer
                    maf_ali:MainFault = 1
                    If Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign
                        ! Found
                        save_MANFAULO_ALIAS_id = Access:MANFAULO_ALIAS.SaveFile()
                        Access:MANFAULO_ALIAS.Clearkey(mfo_ali:RelatedFieldKey)
                        mfo_ali:Manufacturer = job:Manufacturer
                        mfo_ali:RelatedPartCode = map:Field_Number
                        mfo_ali:Field_Number = maf_ali:Field_Number
                        mfo_ali:Field = local:FaultCodeValue
                        If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RelatedFieldKey) = Level:Benign
                            ! Found
                            Found# = 0
                            Access:MANFAURL.Clearkey(mnr:FieldKey)
                            mnr:MANFAULORecordNumber = mfo_ali:RecordNumber
                            mnr:PartFaultCode = 0
                            mnr:FieldNumber = maf:Field_Number
                            Set(mnr:FieldKey,mnr:FieldKey)
                            Loop
                                If Access:MANFAURL.Next()
                                    Break
                                End ! If Access:MANFAURL.Next()
                                If mnr:MANFAULORecordNumber <> mfo_ali:RecordNumber
                                    Break
                                End ! If mnr:MANFAULORecordNumber <> mfo_ali:RecordNumber
                                If mnr:PartFaultCode <> 0
                                    Break
                                End ! If mnr:PartFaultCOde <> 1
                                If mnr:FieldNUmber <> maf:Field_Number
                                    Break
                                End ! If mnr:FIeldNUmber <> maf:Field_Number
                                If mnr:RelatedPartFaultCode > 0
                                    If mnr:RelatedPartFaultCode <> map:Field_Number
                                        Cycle
                                    End ! If mnr:RelatedPartFaultCode <> map_ali:Field_Number
                                End ! If mnr:RelaterdPartFaultCode > 0
                                Found# = 1
                                Break
                            End ! Loop (MANFAURL)
                            If Found# = 1
                                PassLinkedFaultCodeQueue.RecordNumber = mfo_ali:RecordNumber
                                PassLinkedFaultCodeQueue.FaultType = 'J'
                                Add(PassLinkedFaultCodeQueue)
                            End ! If Found# = 1

                        Else ! If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RelatedFieldKey) = Level:Benign
                        End ! If Access:MANFAULO_ALIAS.TryFetch(mfo_ali:RelatedFieldKey) = Level:Benign
                        Access:MANFAULO_ALIAS.RestoreFile(save_MANFAULO_ALIAS_id)
                    Else ! If Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign
                    End ! If Access:MANFAULT_ALIAS.TryFetch(maf_ali:MainFaultKey) = Level:Benign
                    Access:MANFAULT_ALIAS.RestoreFile(save_MANFAULT_ALIAS_id)
                Else ! If map:UseRelatedJobCode
                    Access:MANFPALO.Clearkey(mfp:Field_Key)
                    mfp:Manufacturer = job:Manufacturer
                    mfp:Field_Number = map:Field_Number
                    mfp:Field = local:FaultCodeValue
                    If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                        ! Found
                        Access:MANFPARL.Clearkey(mpr:FieldKey)
                        mpr:MANFPALORecordNumber = mfp:RecordNumber
                        mpr:JobFaultCode = 1
                        mpr:FieldNumber = maf:Field_Number
                        Set(mpr:FieldKey,mpr:FieldKey)
                        Loop
                            If Access:MANFPARL.Next()
                                Break
                            End ! If Access:MANFPARL.Next()
                            If mpr:MANFPALORecordNumber <> mfp:RecordNumber
                                Break
                            End ! If mpr:MANFPALORecordNumber <> mfp:RecordNumber
                            If mpr:JobFaultCode <> 1
                                Break
                            End ! If mpr:JobFaultCode <> 1
                            If mpr:FieldNumber <> maf:Field_Number
                                Break
                            End ! If mpr:FieldNumber <> maf:Field_Number

                            Found# = 1
                            Break
                        End ! Loop (MANFPARL)
                        If Found# = 1
                            PassLinkedFaultCodeQueue.RecordNumber = mfp:RecordNumber
                            PassLinkedFaultCodeQueue.FaultType = 'P'
                            Add(PassLinkedFaultCodeQueue)
                        End ! If Found# = 1
                    Else ! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                    End ! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                End ! If map:UseRelatedJobCode
            Else ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
            End ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        End ! Loop f# = 1 To 20

    End ! Loop (WARPARTS)
local.PartForceFaultCode    Procedure(Long f:PartFieldNumber,String f:FaultCode, Long f:JobFieldNumber )
Code
    If f:FaultCode = ''
        Return 0
    End ! If f:FaultCode = ''

    Access:MANFAUPA.Clearkey(map:Field_Number_Key)
    map:Manufacturer = job:Manufacturer
    map:Field_Number = f:PartFieldNumber
    If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
        If map:NotAvailable = 0
            Access:MANFPALO.Clearkey(mfp:Field_Key)
            mfp:Manufacturer = job:Manufacturer
            mfp:Field_Number = f:PartFieldNumber
            mfp:Field = f:FaultCode
            If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                If mfp:ForceJobFaultCode
                    If mfp:ForceFaultCodeNumber = f:JobFieldNumber
                        Return 1
                    End ! If mfp:ForceFaultCodeNumber= maf:Field_Number
                End ! If mfp:ForceJobFaultCode
            Else ! Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
            End ! Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
        End ! If map:NotAvailable = 0
    Else ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
    End ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
    Return 0
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
MismatchMSN PROCEDURE (f:MSN)                         !Generated from procedure template - Window

tmp:MSN              STRING(30)
tmp:Return           BYTE(0)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PANEL,AT(244,162,192,94),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('MSN Mismatch'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('This MSN does not match the MSN inserted at Book In. Please verify the MSN again' &|
   ' in the field below'),AT(248,190,184,26),USE(?Prompt4),CENTER,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Verify M.S.N.'),AT(248,218),USE(?tmp:MSN:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(308,218,124,10),USE(tmp:MSN),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Verify M.S.N.'),TIP('Verify M.S.N.'),REQ,UPR
                       BUTTON,AT(300,258),USE(?Close),TRN,FLAT,ICON('verifyp.jpg'),DEFAULT
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Return)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020699'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('MismatchMSN')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANUFACT.Open
  Access:MODELNUM.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','MismatchMSN')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANUFACT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','MismatchMSN')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Close
      If tmp:MSN = ''
          Cycle
      End ! If tmp:MSN = ''
      
      ! Validate MSN (DBH: 17/10/2007)
      If Len(Clip(tmp:MSN)) = 11 And job:Manufacturer = 'ERICSSON'
          tmp:MSN = Sub(tmp:MSN,2,10)
          Display()
      End ! If Len(Clip(tmp:MSN)) = 11 And job:Manufacturer = 'ERICSSON'
      
      If CheckLength('MSN',job:Model_Number,tmp:MSN)
          Select(?tmp:MSN)
          Cycle
      Else ! If CheckLength('MSN',job:Model_Number,tmp:MSN)
          Access:MANUFACT.Clearkey(man:Manufacturer_Key)
          man:Manufacturer = job:Manufacturer
          If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
              If man:ApplyMSNFormat
                  If CheckFaultFormat(tmp:MSN,man:MSNFormat)
                      Case Missive('M.S.N. failed format validation.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Select(?tmp:MSN)
                      Cycle
                  End ! If CheckFaultFormat(tmp:MSN,man:MSNFormat)
              End ! If man:ApplyMSNFormat
          End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
      End ! If CheckLength('MSN',job:Model_Number,tmp:MSN)
      
      
      If tmp:MSN = job:MSN
          tmp:Return = 1
      Elsif tmp:MSN = f:MSN
          Case Missive('Are you sure you want to change the M.S.N. on the job?'&|
            '|(If you continue all changes on the job, so far, will be saved.)','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  job:MSN = tmp:MSN
                  Access:JOBS.Update()
                  tmp:Return = 1
              Of 1 ! No Button
                  Select(?tmp:MSN)
                  Cycle
          End ! Case Missive
      Else ! Elsif tmp:MSN = f:MSN
          tmp:MSN = ''
          Case Missive('The M.S.N. entered does not match the M.S.N. entered at book-in, or the M.S.N. you are trying to verify.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Select(?tmp:MSN)
          Cycle
      End ! If tmp:MSN = job:MSN
    OF ?Cancel
      tmp:Return = 0
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020699'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020699'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020699'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
BrowseJobFaultCodeLookup PROCEDURE (f:Manufacturer,f:FieldNumber,f:JobType,f:RelatedPartNumber,f:PartType,LinkedFaultCodeQueue) !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
tmp:Manufacturer     STRING(30)
tmp:FieldNumber      LONG
tmp:RelatedPartNumber LONG
BRW1::View:Browse    VIEW(MANFAULO)
                       PROJECT(mfo:Field)
                       PROJECT(mfo:Description)
                       PROJECT(mfo:RecordNumber)
                       PROJECT(mfo:NotAvailable)
                       PROJECT(mfo:Manufacturer)
                       PROJECT(mfo:RelatedPartCode)
                       PROJECT(mfo:Field_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
mfo:Field              LIKE(mfo:Field)                !List box control field - type derived from field
mfo:Description        LIKE(mfo:Description)          !List box control field - type derived from field
mfo:RecordNumber       LIKE(mfo:RecordNumber)         !Primary key field - type derived from field
mfo:NotAvailable       LIKE(mfo:NotAvailable)         !Browse key field - type derived from field
mfo:Manufacturer       LIKE(mfo:Manufacturer)         !Browse key field - type derived from field
mfo:RelatedPartCode    LIKE(mfo:RelatedPartCode)      !Browse key field - type derived from field
mfo:Field_Number       LIKE(mfo:Field_Number)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK12::mfo:Field_Number    LIKE(mfo:Field_Number)
HK12::mfo:Manufacturer    LIKE(mfo:Manufacturer)
HK12::mfo:NotAvailable    LIKE(mfo:NotAvailable)
HK12::mfo:RelatedPartCode LIKE(mfo:RelatedPartCode)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Browse the Fault Code Lookup File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Fault Code Lookup File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,112,344,186),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('125L(2)|M~Field~@s30@240L(2)|M~Description~@s60@'),FROM(Queue:Browse:1)
                       BUTTON,AT(448,300),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                       BUTTON,AT(168,300),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       BUTTON,AT(236,300),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                       BUTTON,AT(304,300),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Field'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,98,,10),USE(mfo:Field),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Description'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s60),AT(168,98,124,10),USE(mfo:Description),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort2:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And tmp:RelatedPartNumber <> ''
BRW1::Sort3:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And tmp:RelatedPartNumber <> ''
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 and tmp:RelatedPartNumber = 0
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020436'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseJobFaultCodeLookup')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:MANFAULO.Open
  Relate:USERS_ALIAS.Open
  Access:MANFAULT.UseFile
  Access:MANFAURL.UseFile
  Access:MANFPARL.UseFile
  SELF.FilesOpened = True
  tmp:Manufacturer = f:Manufacturer
  tmp:FieldNumber = f:FieldNumber
  tmp:RelatedPartNumber = f:RelatedPartNumber
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MANFAULO,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','BrowseJobFaultCodeLookup')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,mfo:HideRelatedFieldKey)
  BRW1.AddRange(mfo:Field_Number)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?mfo:Field,mfo:Field,1,BRW1)
  BRW1.AddSortOrder(,mfo:HideRelatedDescKey)
  BRW1.AddRange(mfo:Field_Number)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?mfo:Description,mfo:Description,1,BRW1)
  BRW1.AddSortOrder(,mfo:HideDescriptionKey)
  BRW1.AddRange(mfo:Field_Number)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?mfo:Description,mfo:Description,1,BRW1)
  BRW1.AddSortOrder(,mfo:HideFieldKey)
  BRW1.AddRange(mfo:Field_Number)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?mfo:Field,mfo:Field,1,BRW1)
  BIND('GLO:Select1',GLO:Select1)
  BIND('GLO:Select2',GLO:Select2)
  BRW1.AddField(mfo:Field,BRW1.Q.mfo:Field)
  BRW1.AddField(mfo:Description,BRW1.Q.mfo:Description)
  BRW1.AddField(mfo:RecordNumber,BRW1.Q.mfo:RecordNumber)
  BRW1.AddField(mfo:NotAvailable,BRW1.Q.mfo:NotAvailable)
  BRW1.AddField(mfo:Manufacturer,BRW1.Q.mfo:Manufacturer)
  BRW1.AddField(mfo:RelatedPartCode,BRW1.Q.mfo:RelatedPartCode)
  BRW1.AddField(mfo:Field_Number,BRW1.Q.mfo:Field_Number)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Field'
    ?Tab2{PROP:TEXT} = 'By Description'
    ?Browse:1{PROP:FORMAT} ='125L(2)|M~Field~@s30@#1#240L(2)|M~Description~@s60@#2#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:MANFAULO.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseJobFaultCodeLookup')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = True
  
  case request
      of insertrecord
          If SecurityCheck('FAULT CODE LOOKUP - INSERT')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Access:MANFAULO.CancelAutoInc()
              do_update# = false
          end!If SecurityCheck('FAULT CODE LOOKUP - INSERT')
      of changerecord
          If SecurityCheck('FAULT CODE LOOKUP - CHANGE')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
  
              do_update# = false
          end!If SecurityCheck('FAULT CODE LOOKUP - CHANGE')
      of deleterecord
          If SecurityCheck('FAULT CODE LOOKUP - DELETE')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
  
              do_update# = false
          end!If SecurityCheck('FAULT CODE LOOKUP - DELETE')
  end !case request
  
  If do_update# = True
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMANFAULO
    ReturnValue = GlobalResponse
  END
  End!If do_update# = True
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020436'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020436'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020436'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='125L(2)|M~Field~@s30@#1#240L(2)|M~Description~@s60@#2#'
          ?Tab:2{PROP:TEXT} = 'By Field'
        OF 2
          ?Browse:1{PROP:FORMAT} ='240L(2)|M~Description~@s60@#2#125L(2)|M~Field~@s30@#1#'
          ?Tab2{PROP:TEXT} = 'By Description'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?mfo:Field
      Select(?Browse:1)
    OF ?mfo:Description
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      If GETINI('BROWSEORDER','FaultCode',,CLIP(PATH())&'\SB2KDEF.INI') = 1
          Select(?CurrentTab,2)
          Post(Event:NewSelection,?CurrentTab)
      End !GETINI('BROWSEORDER','FaultCode',,CLIP(PATH())&'\SB2KDEF.INI') = 1
    OF EVENT:GainFocus
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 1 And tmp:RelatedPartNumber <> ''
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:Manufacturer
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:RelatedPartNumber
     GET(SELF.Order.RangeList.List,4)
     Self.Order.RangeList.List.Right = tmp:FieldNumber
  ELSIF Choice(?CurrentTab) = 2 And tmp:RelatedPartNumber <> ''
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:Manufacturer
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:RelatedPartNumber
     GET(SELF.Order.RangeList.List,4)
     Self.Order.RangeList.List.Right = tmp:FieldNumber
  ELSIF Choice(?CurrentTab) = 2 And tmp:RelatedPartNumber = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:Manufacturer
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:FieldNumber
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:Manufacturer
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:FieldNumber
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1 And tmp:RelatedPartNumber <> ''
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 2 And tmp:RelatedPartNumber <> ''
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 2 and tmp:RelatedPartNumber = 0
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  ! Check for restrictions - TrkBs: 5365 (DBH: 27-04-2005)
  If mfo:RestrictLookup = True
      Case f:PartType
          Of 2 ! Correction
              If mfo:RestrictLookupType <> 2
                  Return Record:Filtered
              End ! If mfo:RestrictLookupType <> 2
          Of 1 ! Adjustment
              If mfo:RestrictLookupType <> 3
                  Return Record:Filtered
              End ! If mfo:RestrictLookupType <> 3
          Else
              If mfo:RestrictLookupType <> 1
                  Return Record:Filtered
              End ! If mfo:RestrictLookupType <> 1
      End ! Case glo:Select4
  End ! mfo:RestrictLookup = True
  
  ! If fault code is Chargeable Only, but job is Warranty (DBH: 23/10/2007)
  If mfo:JobTypeAvailability = 1
      If f:JobType = 2
          Return Record:Filtered
      End ! If f:JobType <> 1
  End ! If mfo:JobTypeAvailability = 1
  
  ! If fault code is Warranty Only, but job is Chargeable (DBH: 23/10/2007)
  If mfo:JobTypeAvailability = 2
      If f:JobType = 1
          Return Record:Filtered
      End ! If f:JobType <> 2
  End ! If mfo:JobTypeAvailability = 2
  
  If Records(LinkedFaultCodeQueue)
      Found# = 0
      Loop x# = 1 To Records(LinkedFaultCodeQueue)
          Get(LinkedFaultCodeQueue,x#)
          Case LinkedFaultCodeQueue.FaultType
          Of 'J' ! Job Fault Code
              Access:MANFAURL.Clearkey(mnr:LinkedRecordNumberKey)
              mnr:MANFAULORecordNumber = LinkedFaultCodeQueue.RecordNumber
              mnr:PartFaultCode = 0
              mnr:LinkedRecordNumber = mfo:RecordNumber
              If Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign
                  Found# = 1
                  Break
              Else ! If Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign
              End ! If Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign
          Of 'P' ! Part Fault Code
              Access:MANFPARL.Clearkey(mpr:LinkedRecordNumberKey)
              mpr:MANFPALORecordNumber = LinkedFaultCodeQueue.RecordNumber
              mpr:JobFaultCode = 1
              mpr:LinkedRecordNumber = mfo:RecordNumber
              If Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign
                  ! Found
                  Found# = 1
                  Break
              Else ! If Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign
              End ! If Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign
          End ! Case LinkedFaultCodeQueue.FaultType
      End ! Loop x# = 1 To Records(LinkedFaultCodeQueue)
      If Found# = 0
          Return Record:Filtered
      End ! If Found# = 0
  End ! If Records(LinkedFaultCodeQueue)
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

BrowseJobFaultCodeLookup_Alias PROCEDURE (f:Manufacturer,f:FieldNumber,f:Restrict) !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
tmp:Manufacturer     STRING(30)
tmp:FieldNumber      LONG
tmp:Restrict         STRING(30)
BRW1::View:Browse    VIEW(MANFAULO_ALIAS)
                       PROJECT(mfo_ali:Field)
                       PROJECT(mfo_ali:Description)
                       PROJECT(mfo_ali:NotAvailable)
                       PROJECT(mfo_ali:Manufacturer)
                       PROJECT(mfo_ali:Field_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
mfo_ali:Field          LIKE(mfo_ali:Field)            !List box control field - type derived from field
mfo_ali:Description    LIKE(mfo_ali:Description)      !List box control field - type derived from field
mfo_ali:NotAvailable   LIKE(mfo_ali:NotAvailable)     !Browse key field - type derived from field
mfo_ali:Manufacturer   LIKE(mfo_ali:Manufacturer)     !Browse key field - type derived from field
mfo_ali:Field_Number   LIKE(mfo_ali:Field_Number)     !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK12::mfo_ali:Field_Number LIKE(mfo_ali:Field_Number)
HK12::mfo_ali:Manufacturer LIKE(mfo_ali:Manufacturer)
HK12::mfo_ali:NotAvailable LIKE(mfo_ali:NotAvailable)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Browse the Fault Code Lookup File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Fault Code Lookup File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,114,344,184),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('125L(2)|M~Field~@s30@240L(2)|M~Description~@s60@'),FROM(Queue:Browse:1)
                       BUTTON,AT(448,300),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Field'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,100,,10),USE(mfo_ali:Field),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Description'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s60),AT(168,100,124,10),USE(mfo_ali:Description),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020436'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseJobFaultCodeLookup_Alias')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:MANFAULO_ALIAS.Open
  Relate:MANFAULT.Open
  Relate:USERS_ALIAS.Open
  SELF.FilesOpened = True
  tmp:Manufacturer = f:Manufacturer
  tmp:FieldNumber = f:FieldNumber
  tmp:Restrict = f:Restrict
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MANFAULO_ALIAS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','BrowseJobFaultCodeLookup_Alias')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,mfo_ali:HideDescriptionKey)
  BRW1.AddRange(mfo_ali:Field_Number)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?mfo_ali:Description,mfo_ali:Description,1,BRW1)
  BRW1.AddSortOrder(,mfo_ali:HideFieldKey)
  BRW1.AddRange(mfo_ali:Field_Number)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?mfo_ali:Field,mfo_ali:Field,1,BRW1)
  BIND('GLO:Select1',GLO:Select1)
  BIND('GLO:Select2',GLO:Select2)
  BRW1.AddField(mfo_ali:Field,BRW1.Q.mfo_ali:Field)
  BRW1.AddField(mfo_ali:Description,BRW1.Q.mfo_ali:Description)
  BRW1.AddField(mfo_ali:NotAvailable,BRW1.Q.mfo_ali:NotAvailable)
  BRW1.AddField(mfo_ali:Manufacturer,BRW1.Q.mfo_ali:Manufacturer)
  BRW1.AddField(mfo_ali:Field_Number,BRW1.Q.mfo_ali:Field_Number)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Field'
    ?Tab2{PROP:TEXT} = 'By Description'
    ?Browse:1{PROP:FORMAT} ='125L(2)|M~Field~@s30@#1#240L(2)|M~Description~@s60@#2#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:MANFAULO_ALIAS.Close
    Relate:MANFAULT.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseJobFaultCodeLookup_Alias')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020436'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020436'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020436'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='125L(2)|M~Field~@s30@#1#240L(2)|M~Description~@s60@#2#'
          ?Tab:2{PROP:TEXT} = 'By Field'
        OF 2
          ?Browse:1{PROP:FORMAT} ='240L(2)|M~Description~@s60@#2#125L(2)|M~Field~@s30@#1#'
          ?Tab2{PROP:TEXT} = 'By Description'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?mfo_ali:Field
      Select(?Browse:1)
    OF ?mfo_ali:Description
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      If GETINI('BROWSEORDER','FaultCode',,CLIP(PATH())&'\SB2KDEF.INI') = 1
          Select(?CurrentTab,2)
          Post(Event:NewSelection,?CurrentTab)
      End !GETINI('BROWSEORDER','FaultCode',,CLIP(PATH())&'\SB2KDEF.INI') = 1
    OF EVENT:GainFocus
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:Manufacturer
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:FieldNumber
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:Manufacturer
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:FieldNumber
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  ! Check for restrictions - TrkBs: 5365 (DBH: 27-04-2005)
  If mfo:RestrictLookup = True
      Case tmp:Restrict
          Of 'CORRECT PART'
              If mfo:RestrictLookupType <> 2
                  Return Record:Filtered
              End ! If mfo:RestrictLookupType <> 2
          Of 'ADJUSTMENT'
              If mfo:RestrictLookupType <> 3
                  Return Record:Filtered
              End ! If mfo:RestrictLookupType <> 3
          Else
              If mfo:RestrictLookupType <> 1
                  Return Record:Filtered
              End ! If mfo:RestrictLookupType <> 1
      End ! Case glo:Select4
  End ! mfo:RestrictLookup = True
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

BrowsePartFaultCodeLookup PROCEDURE (f:Manufacturer,f:FieldNumber,f:PartType,f:JobType,LinkedFaultCodeQueue) !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
tmp:Manufacturer     STRING(30)
tmp:FieldNumber      LONG
tmp:CCT              BYTE(0)
BRW1::View:Browse    VIEW(MANFPALO)
                       PROJECT(mfp:Field)
                       PROJECT(mfp:Description)
                       PROJECT(mfp:RecordNumber)
                       PROJECT(mfp:Manufacturer)
                       PROJECT(mfp:NotAvailable)
                       PROJECT(mfp:Field_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
mfp:Field              LIKE(mfp:Field)                !List box control field - type derived from field
mfp:Description        LIKE(mfp:Description)          !List box control field - type derived from field
mfp:RecordNumber       LIKE(mfp:RecordNumber)         !Primary key field - type derived from field
mfp:Manufacturer       LIKE(mfp:Manufacturer)         !Browse key field - type derived from field
mfp:NotAvailable       LIKE(mfp:NotAvailable)         !Browse key field - type derived from field
mfp:Field_Number       LIKE(mfp:Field_Number)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK10::mfp:Field_Number    LIKE(mfp:Field_Number)
HK10::mfp:Manufacturer    LIKE(mfp:Manufacturer)
HK10::mfp:NotAvailable    LIKE(mfp:NotAvailable)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Browse the Fault Code Lookup File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Fault Code Lookup File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(448,300),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                       LIST,AT(168,112,344,186),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('81L(2)|M~Field~@s30@120L(2)|M~Description~@s60@'),FROM(Queue:Browse:1)
                       BUTTON,AT(168,300),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       BUTTON,AT(236,300),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                       BUTTON,AT(304,300),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Field'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,98,124,10),USE(mfp:Field),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('Description'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s60),AT(168,98,124,10),USE(mfp:Description),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020437'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowsePartFaultCodeLookup')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:MANFAULT.Open
  Relate:USERS_ALIAS.Open
  Access:MANFAUPA.UseFile
  Access:MODELCCT.UseFile
  Access:MANFAURL.UseFile
  SELF.FilesOpened = True
  tmp:Manufacturer = f:Manufacturer
  tmp:FieldNumber = f:FieldNumber
  
  ! Inserting (DBH 30/04/2008) # 9723 - Limit by Model Number if it's a CCT Reference
  Access:MANFAUPA.Clearkey(map:Field_Number_Key)
  map:Manufacturer = tmp:Manufacturer
  map:Field_Number = tmp:FieldNumber
  If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
      ! Found
      ! Is this fault a CCT? (DBH: 30/04/2008)
      If map:CCTReferenceFaultCode
          tmp:CCT = 1
      End ! If map:CCTReferenceFaultCode
  End ! If Access:MANFAUPA.TryFetch(map:Field_Number_Key) = Level:Benign
  ! End (DBH 30/04/2008) #9723
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MANFPALO,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','BrowsePartFaultCodeLookup')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,mfp:AvailableDescriptionKey)
  BRW1.AddRange(mfp:Field_Number)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?mfp:Description,mfp:Description,1,BRW1)
  BRW1.AddSortOrder(,mfp:AvailableFieldKey)
  BRW1.AddRange(mfp:Field_Number)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?mfp:Field,mfp:Field,1,BRW1)
  BIND('GLO:Select1',GLO:Select1)
  BIND('GLO:Select2',GLO:Select2)
  BRW1.AddField(mfp:Field,BRW1.Q.mfp:Field)
  BRW1.AddField(mfp:Description,BRW1.Q.mfp:Description)
  BRW1.AddField(mfp:RecordNumber,BRW1.Q.mfp:RecordNumber)
  BRW1.AddField(mfp:Manufacturer,BRW1.Q.mfp:Manufacturer)
  BRW1.AddField(mfp:NotAvailable,BRW1.Q.mfp:NotAvailable)
  BRW1.AddField(mfp:Field_Number,BRW1.Q.mfp:Field_Number)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Field'
    ?Tab2{PROP:TEXT} = 'Description'
    ?Browse:1{PROP:FORMAT} ='81L(2)|M~Field~@s30@#1#120L(2)|M~Description~@s60@#2#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:MANFAULT.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowsePartFaultCodeLookup')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = True
  
    case request
        of insertrecord
            If SecurityCheck('FAULT CODE LOOKUP - INSERT')
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Access:MANFPALO.CancelAutoInc()
                do_update# = false
            end!If SecurityCheck('FAULT CODE LOOKUP - INSERT')
        of changerecord
            If SecurityCheck('FAULT CODE LOOKUP - CHANGE')
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
  
                do_update# = false
            end!If SecurityCheck('FAULT CODE LOOKUP - CHANGE')
        of deleterecord
            If SecurityCheck('FAULT CODE LOOKUP - DELETE')
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
  
                do_update# = false
            end!If SecurityCheck('FAULT CODE LOOKUP - DELETE')
    end !case request
  
    If do_update# = True
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMANFPALO
    ReturnValue = GlobalResponse
  END
  End!If do_update# = True
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020437'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020437'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020437'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='81L(2)|M~Field~@s30@#1#120L(2)|M~Description~@s60@#2#'
          ?Tab:2{PROP:TEXT} = 'By Field'
        OF 2
          ?Browse:1{PROP:FORMAT} ='120L(2)|M~Description~@s60@#2#81L(2)|M~Field~@s30@#1#'
          ?Tab2{PROP:TEXT} = 'Description'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?mfp:Field
      Select(?Browse:1)
    OF ?mfp:Description
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      If GETINI('BROWSEORDER','FaultCode',,CLIP(PATH())&'\SB2KDEF.INI') = 1
          Select(?CurrentTab,2)
          Post(Event:NewSelection,?CurrentTab)
      End !GETINI('BROWSEORDER','FaultCode',,CLIP(PATH())&'\SB2KDEF.INI') = 1
    OF EVENT:GainFocus
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Manufacturer
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:FieldNumber
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:Manufacturer
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:FieldNumber
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  ! Check for restrictions - TrkBs: 5365 (DBH: 27-04-2005)
  If mfp:RestrictLookup = True
      Case f:PartType
          Of 2 ! Correction
              If mfp:RestrictLookupType <> 2
                  Return Record:Filtered
              End ! If mfp:RestrictLookupType <> 2
          Of 1 ! Adjustment
              If mfp:RestrictLookupType <> 3
                  Return Record:Filtered
              End ! If mfp:RestrictLookupType <> 3
          Else
              If mfp:RestrictLookupType <> 1
                  Return Record:Filtered
              End ! If mfp:RestrictLookupType <> 1
      End ! Case glo:Select4
  End ! mfp:RestrictLookup = True
  
  ! If fault code is Chargeable Only, but job is Warranty (DBH: 23/10/2007)
  If mfp:JobTypeAvailability = 1
      If f:JobType = 2
          Return Record:Filtered
      End ! If f:JobType <> 1
  End ! If mfo:JobTypeAvailability = 1
  
  ! If fault code is Warranty Only, but job is Chargeable (DBH: 23/10/2007)
  If mfp:JobTypeAvailability = 2
      If f:JobType = 1
          Return Record:Filtered
      End ! If f:JobType <> 2
  End ! If mfo:JobTypeAvailability = 2
  
  ! Inserting (DBH 24/04/2008) # 9723 - If there are linked fault codes. Also show the relevant fault codes
  If Records(LinkedFaultCodeQueue)
      Found# = 0
      Loop x# = 1 To Records(LinkedFaultCodeQueue)
          Get(LinkedFaultCodeQueue,x#)
          Case LinkedFaultCodeQueue.FaultType
          Of 'P' ! Part Fault Code
              Access:MANFPARL.Clearkey(mpr:LinkedRecordNumberKey)
              mpr:MANFPALORecordNumber = LinkedFaultCodeQueue.RecordNumber
              mpr:LinkedRecordNumber = mfp:RecordNumber
              mpr:JobFaultCode = 0
              If Access:MANFPARL.TryFetch(mpr:LinkedRecordNumberKey) = Level:Benign
                  Found# = 1
                  Break
              End ! If Access:MANFPARL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign
          Of 'J' ! Job Fault Code
              ! Are any job fault codes linked to this fault code? (DBH: 12/05/2008)
              Access:MANFAURL.Clearkey(mnr:LinkedRecordNumberKey)
              mnr:MANFAULORecordNumber = LinkedFaultCodeQueue.RecordNumber
              mnr:LinkedRecordNumber = mfp:RecordNumber
              mnr:PartFaultCode = 1
              If Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign
                  ! Found
                  Found# = 1
                  Break
              Else ! If Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign
              End ! If Access:MANFAURL.TryFetch(mnr:LinkedRecordNumberKey) = Level:Benign
          End ! Case LinkedFaultCodeQueue.Type
      End ! Loop x# = 1 To Records(LinkedFaultCodeQueue)
      If Found# = 0
          Return Record:Filtered
      End ! If Found# = 0
  End ! If Records(LinkedFaultCodeQueue)
  ! End (DBH 24/04/2008) #9723
  
  ! Inserting (DBH 30/04/2008) # 9723 - Limit by Model Number if it's a CCT Reference
  If tmp:CCT = 1
      Access:MODELCCT.Clearkey(mcc:CCTRefKey)
      mcc:ModelNumber = job:Model_Number
      mcc:CCTReferenceNumber = mfp:Field
      If Access:MODELCCT.TryFetch(mcc:CCTRefKey) = Level:Benign
          ! Found
  
      Else ! If Access:MODELCCT.TryFetch(mcc:CCTRefKey) = Level:Benign
          ! This is no reference in the CCT List. Don't show (DBH: 30/04/2008)
          Return Record:Filtered
      End ! If Access:MODELCCT.TryFetch(mcc:CCTRefKey) = Level:Benign
  End ! If map:CCTReferenceFaultCode
  ! End (DBH 30/04/2008) #9723
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

BrowsePartFaultCodeLookup_Alias PROCEDURE (f:Manufacturer,f:FieldNumber,f:PartType,f:JobType) !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
tmp:Manufacturer     STRING(30)
tmp:FieldNumber      LONG
BRW1::View:Browse    VIEW(MANFPALO_ALIAS)
                       PROJECT(mfp_ali:Field)
                       PROJECT(mfp_ali:Description)
                       PROJECT(mfp_ali:Manufacturer)
                       PROJECT(mfp_ali:NotAvailable)
                       PROJECT(mfp_ali:Field_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
mfp_ali:Field          LIKE(mfp_ali:Field)            !List box control field - type derived from field
mfp_ali:Description    LIKE(mfp_ali:Description)      !List box control field - type derived from field
mfp_ali:Manufacturer   LIKE(mfp_ali:Manufacturer)     !Browse key field - type derived from field
mfp_ali:NotAvailable   LIKE(mfp_ali:NotAvailable)     !Browse key field - type derived from field
mfp_ali:Field_Number   LIKE(mfp_ali:Field_Number)     !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK10::mfp_ali:Field_Number LIKE(mfp_ali:Field_Number)
HK10::mfp_ali:Manufacturer LIKE(mfp_ali:Manufacturer)
HK10::mfp_ali:NotAvailable LIKE(mfp_ali:NotAvailable)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Browse the Fault Code Lookup File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Fault Code Lookup File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(448,300),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                       LIST,AT(168,110,344,188),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('81L(2)|M~Field~@s30@240L(2)|M~Description~@s60@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Field'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,98,124,10),USE(mfp:Field),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('Description'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s60),AT(168,98,124,10),USE(mfp:Description),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020437'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowsePartFaultCodeLookup_Alias')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:MANFAULT.Open
  Relate:MANFPALO_ALIAS.Open
  Relate:USERS_ALIAS.Open
  SELF.FilesOpened = True
  tmp:Manufacturer = f:Manufacturer
  tmp:FieldNumber = f:FieldNumber
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MANFPALO_ALIAS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','BrowsePartFaultCodeLookup_Alias')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,mfp_ali:AvailableDescriptionKey)
  BRW1.AddRange(mfp_ali:Field_Number)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,mfp_ali:Description,1,BRW1)
  BRW1.AddSortOrder(,mfp_ali:AvailableFieldKey)
  BRW1.AddRange(mfp_ali:Field_Number)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,mfp_ali:Field,1,BRW1)
  BIND('GLO:Select1',GLO:Select1)
  BIND('GLO:Select2',GLO:Select2)
  BRW1.AddField(mfp_ali:Field,BRW1.Q.mfp_ali:Field)
  BRW1.AddField(mfp_ali:Description,BRW1.Q.mfp_ali:Description)
  BRW1.AddField(mfp_ali:Manufacturer,BRW1.Q.mfp_ali:Manufacturer)
  BRW1.AddField(mfp_ali:NotAvailable,BRW1.Q.mfp_ali:NotAvailable)
  BRW1.AddField(mfp_ali:Field_Number,BRW1.Q.mfp_ali:Field_Number)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Field'
    ?Tab2{PROP:TEXT} = 'Description'
    ?Browse:1{PROP:FORMAT} ='81L(2)|M~Field~@s30@#1#120L(2)|M~Description~@s60@#2#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:MANFAULT.Close
    Relate:MANFPALO_ALIAS.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowsePartFaultCodeLookup_Alias')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = True
  
    case request
        of insertrecord
            If SecurityCheck('FAULT CODE LOOKUP - INSERT')
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Access:MANFPALO.CancelAutoInc()
                do_update# = false
            end!If SecurityCheck('FAULT CODE LOOKUP - INSERT')
        of changerecord
            If SecurityCheck('FAULT CODE LOOKUP - CHANGE')
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
  
                do_update# = false
            end!If SecurityCheck('FAULT CODE LOOKUP - CHANGE')
        of deleterecord
            If SecurityCheck('FAULT CODE LOOKUP - DELETE')
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
  
                do_update# = false
            end!If SecurityCheck('FAULT CODE LOOKUP - DELETE')
    end !case request
  
    If do_update# = True
  End!If do_update# = True
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020437'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020437'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020437'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='81L(2)|M~Field~@s30@#1#120L(2)|M~Description~@s60@#2#'
          ?Tab:2{PROP:TEXT} = 'By Field'
        OF 2
          ?Browse:1{PROP:FORMAT} ='120L(2)|M~Description~@s60@#2#81L(2)|M~Field~@s30@#1#'
          ?Tab2{PROP:TEXT} = 'Description'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?mfp:Field
      Select(?Browse:1)
    OF ?mfp:Description
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      If GETINI('BROWSEORDER','FaultCode',,CLIP(PATH())&'\SB2KDEF.INI') = 1
          Select(?CurrentTab,2)
          Post(Event:NewSelection,?CurrentTab)
      End !GETINI('BROWSEORDER','FaultCode',,CLIP(PATH())&'\SB2KDEF.INI') = 1
    OF EVENT:GainFocus
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Manufacturer
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:FieldNumber
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:Manufacturer
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:FieldNumber
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  ! Check for restrictions - TrkBs: 5365 (DBH: 27-04-2005)
  If mfp:RestrictLookup = True
      Case f:PartType
          Of 2 ! Correction
              If mfp:RestrictLookupType <> 2
                  Return Record:Filtered
              End ! If mfp:RestrictLookupType <> 2
          Of 1 ! Adjustment
              If mfp:RestrictLookupType <> 3
                  Return Record:Filtered
              End ! If mfp:RestrictLookupType <> 3
          Else
              If mfp:RestrictLookupType <> 1
                  Return Record:Filtered
              End ! If mfp:RestrictLookupType <> 1
      End ! Case glo:Select4
  End ! mfp:RestrictLookup = True
  
  ! If fault code is Chargeable Only, but job is Warranty (DBH: 23/10/2007)
  If mfp:JobTypeAvailability = 1
      If f:JobType = 2
          Return Record:Filtered
      End ! If f:JobType <> 1
  End ! If mfo:JobTypeAvailability = 1
  
  ! If fault code is Warranty Only, but job is Chargeable (DBH: 23/10/2007)
  If mfp:JobTypeAvailability = 2
      If f:JobType = 1
          Return Record:Filtered
      End ! If f:JobType <> 2
  End ! If mfo:JobTypeAvailability = 2
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

