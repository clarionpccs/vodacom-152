

   MEMBER('sbj02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ02004.INC'),ONCE        !Local module procedure declarations
                     END


EstimateDetails PROCEDURE                             !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_epr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_aud_id          USHORT,AUTO
window               WINDOW('Estimate Details'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Estimate Details'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           CHECK('Estimate Required'),AT(248,184),USE(job:Estimate),LEFT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           PROMPT('Estimate If Over'),AT(348,176),USE(?job:Estimate_If_Over:Prompt),TRN,FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(348,184,64,10),USE(job:Estimate_If_Over),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           CHECK('Estimate Ready'),AT(348,202),USE(job:Estimate_Ready),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('Estimate Accepted'),AT(348,218),USE(job:Estimate_Accepted),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('Estimate Rejected'),AT(348,234),USE(job:Estimate_Rejected),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                         END
                       END
                       BUTTON,AT(368,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020423'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('EstimateDetails')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?OK,RequestCancelled)
  Relate:AUDIT.Open
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:AUDIT2.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  if GLO:Select1 = 'INSERT'
      job:Estimate = 'YES'
  end
  
  If job:Estimate_Ready = 'YES' And job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
      ?job:Estimate_Ready{prop:Hide} = 0
  End !job:Estiamte_Ready = 'YES'
  
  If SecurityCheck('RELEASE - ESTIMATE READY')
      If job:Estimate_Ready = 'YES'
          ?job:Estimate_Ready{prop:Disable} = 1
      End !If job:Estimate_Ready = 'YES'
  End !SecurityCheck('RELEASE - ESTIMATE READY')
  
  If SecurityCheck('RELEASE - ESTIMATE ACCEPTED')
      If job:Estimate_Accepted = 'YES'
          ?job:Estimate_Accepted{prop:Disable} = 1
          ?job:Estimate_Rejected{prop:Disable} = 1
      End !If job:Estimate_Accepted = 'YES'
  End !SecurityCheck('RELEASE - ESTIMATE ACCEPTED')
  
  If SecurityCheck('RELEASE - ESTIMATE REJECTED')
      If job:Estimate_Rejected = 'YES'
          ?job:Estimate_Rejected{prop:Disable} = 1
          ?job:Estimate_Accepted{prop:Disable} = 1
      End !If job:Estimate_Rejected = 'YES'
  
  End !SecurityCheck('RELEASE - ESTIMATE REJECTED')
  
  ! Inserting (DBH 07/12/2006) # 8559 - Don't allow to change details if the job is in view only mode
  If glo:Preview = 'V'
      ?Sheet1{prop:Disable} = True
  End ! If glo:Preview = 'V'
  ! End (DBH 07/12/2006) #8559
      ! Save Window Name
   AddToLog('Window','Open','EstimateDetails')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?job:Estimate{Prop:Checked} = True
    UNHIDE(?job:Estimate_If_Over:Prompt)
    UNHIDE(?job:Estimate_If_Over)
    UNHIDE(?job:Estimate_Accepted)
    UNHIDE(?job:Estimate_Rejected)
  END
  IF ?job:Estimate{Prop:Checked} = False
    HIDE(?job:Estimate_If_Over:Prompt)
    HIDE(?job:Estimate_If_Over)
    HIDE(?job:Estimate_Accepted)
    HIDE(?job:Estimate_Rejected)
  END
  IF ?job:Estimate_Accepted{Prop:Checked} = True
    job:Estimate_Rejected = 'NO'
  END
  IF ?job:Estimate_Rejected{Prop:Checked} = True
    job:Estimate_Accepted = 'NO'
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','EstimateDetails')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?job:Estimate
      If ~0{prop:acceptall}
          If job:estimate = 'NO'
              reset_status# = 1
              If job:estimate_accepted = 'YES'
                  Case Missive('Warning! The estimate on this job has not been accepted.'&|
                    '<13,10>'&|
                    '<13,10>Do you want to reset this estimate?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                      Of 1 ! No Button
                          job:estimate    = 'YES'
                          reset_status# = 0
                  End ! Case Missive
              End
              If reset_status# = 1
                  setcursor(cursor:wait)
                  save_aud_id = access:audit.savefile()
                  access:audit.clearkey(aud:ref_number_key)
                  aud:ref_number = job:ref_number
                  set(aud:ref_number_key,aud:ref_number_key)
                  loop
                      if access:audit.next()
                         break
                      end !if
                      if aud:ref_number <> job:ref_number  |
                          then break.  ! end if
                      If Instring('STATUS CHANGE',aud:action,1,1) And Instring('505',aud:action,1,1)
                          Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                          aud2:AUDRecordNumber = aud:Record_Number
                          IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                              GetStatus(Clip(Sub(aud2:notes,18,3)),thiswindow.request,'JOB')
      
                              Break
                          END ! IF
      
                      End!If Instring('STATUS CHANGE',aud:action,1,1) And Instring('505',aud:action,1,1)
                  end !loop
                  access:audit.restorefile(save_aud_id)
                  setcursor()
              End!If reset_status# = 1
          Else!If job:estimate = 'NO'
              GetStatus(505,1,'JOB')
          End!If job:estimate = 'NO'
      End!If ~0{prop:acceptall}
    OF ?job:Estimate_Accepted
      If ~0{prop:Acceptall}
          If job:Estimate_Accepted = 'YES'
              error# = 0
              If job:estimate_ready <> 'YES'
                  error# = 1
                  Case Missive('The estimate has not been marked as "Ready".','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  job:Estimate_Accepted = 'NO'
              Else!If job:estimate_ready <> 'YES'
                  Case Missive('Are you sure you want to mark this job as accepted?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
      
                          !TB13024 - before allocating any stock check to ensure stock is clear to handle this   JC 12/03/13
                          Case EstimatePartsInUse()
                            of 0 !no parts in use - ok to proceed
                                GetStatus(535,thiswindow.request,'JOB') !estimate accepted
            
                                glo:select1  = 'ACCEPTED'
                                glo:select2  = ''
                                glo:select3  = ''
                                glo:select4  = ''
                                Estimate_Reason
                                glo:select1  = ''
      
                                IF (AddToAudit(job:Ref_Number,'JOB','ESTIMATE ACCEPTED FROM: ' & Clip(glo:select2),'COMMUNICATION METHOD: ' & Clip(glo:select3)))
                                END ! IF
            
                                glo:select2 = ''
                                glo:select3 = ''
                                glo:select4 = ''
                                job:COurier_Cost = job:Courier_Cost_Estimate
            
                                ConvertEstimateParts()
            
                                job:Labour_Cost = job:Labour_Cost_Estimate
                                job:Parts_Cost  = job:Parts_Cost_Estimate
                                job:Ignore_Chargeable_Charges = job:Ignore_Estimate_Charges
            
                                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                                jobe:RefNumber  = job:Ref_Number
                                If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                    !Found
                                    jobe:RRCCLabourCost = jobe:RRCELabourCost
                                    jobe:RRCCPartsCost  = jobe:RRCEPartsCost
                                    jobe:IgnoreRRCChaCosts = jobe:IgnoreRRCEstCosts
                                    Access:JOBSE.Update()
                                Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                    !Error
                                End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                            of 1
                                miss# = Missive('The Estimate cannot be accepted at this time.'&|
                                'The stock record is missing.'&|
                                '|Ref No: '& Clip(epr:Part_Ref_Number),'ServiceBase 3g',|
                                'midea.jpg','/OK')
                                job:Estimate_Accepted = 'NO'
                            of 2
                                miss# = Missive('The Estimate cannot be accepted at this time.'&|
                                'The stock record is locked.'&|
                                '|Part No: '& Clip(sto:Part_Number) & |
                                '|Location: ' & Clip(sto:Location),'ServiceBase 3g',|
                                'midea.jpg','/OK')
                                job:Estimate_Accepted = 'NO'
      
                          END !Case EstimatePartsInUse
                      Of 1 ! No Button
                          job:Estimate_Accepted = 'NO'
                  End ! Case Missive
      
              End!If job:estimate_ready <> 'YES'
          Else !If job:Estimate_Accepted = 'NO'
              Case Missive('Are you sure you want to UNMARK this job as "Accepted"?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      !Check for Chargeable Parts
                      Found# = 0
                      Save_par_ID = Access:PARTS.SaveFile()
                      Access:PARTS.ClearKey(par:Part_Number_Key)
                      par:Ref_Number  = job:Ref_Number
                      Set(par:Part_Number_Key,par:Part_Number_Key)
                      Loop
                          If Access:PARTS.NEXT()
                             Break
                          End !If
                          If par:Ref_Number  <> job:Ref_Number      |
                              Then Break.  ! End If
                          Found# = 1
                          Break
                      End !Loop
                      Access:PARTS.RestoreFile(Save_par_ID)
      
                      If Found# = 1
                          Case Missive('You must remove all the Chargeable Parts attached to this job before you can mark this estimate as NOT Accepted.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          job:Estimate_Accepted = 'YES'
                      End !If Found# = 1
                  Of 1 ! No Button
                      job:Estimate_Accepted = 'YES'
              End ! Case Missive
          End !If job:Estimate_Accepted = 'NO'
      End !0{prop:Acceptall}
      Display()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020423'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020423'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020423'&'0')
      ***
    OF ?job:Estimate
      IF ?job:Estimate{Prop:Checked} = True
        UNHIDE(?job:Estimate_If_Over:Prompt)
        UNHIDE(?job:Estimate_If_Over)
        UNHIDE(?job:Estimate_Accepted)
        UNHIDE(?job:Estimate_Rejected)
      END
      IF ?job:Estimate{Prop:Checked} = False
        HIDE(?job:Estimate_If_Over:Prompt)
        HIDE(?job:Estimate_If_Over)
        HIDE(?job:Estimate_Accepted)
        HIDE(?job:Estimate_Rejected)
      END
      ThisWindow.Reset
    OF ?job:Estimate_Accepted
      IF ?job:Estimate_Accepted{Prop:Checked} = True
        job:Estimate_Rejected = 'NO'
      END
      ThisWindow.Reset
    OF ?job:Estimate_Rejected
      IF ?job:Estimate_Rejected{Prop:Checked} = True
        job:Estimate_Accepted = 'NO'
      END
      ThisWindow.Reset
      If ~0{prop:Acceptall}
          If job:Estimate_Rejected = 'YES'
      
              GetStatus(540,thiswindow.request,'JOB') !estimate refused
      
              glo:select1  = 'REJECTED'
              glo:select2  = ''
              glo:select3  = ''
              glo:select4  = ''
              Estimate_Reason
              glo:select1  = ''
      
              IF (AddToAudit(job:Ref_Number,'JOB','ESTIMATE REJECTED FROM: ' & Clip(glo:select2),'COMMUNICATION METHOD: ' & Clip(glo:select3) & |
                               '<13,10>REASON: ' & Clip(glo:select4)))
              END ! IF
      
              glo:select2 = ''
              glo:select3 = ''
              glo:select4 = ''
      
          End !If job:Estimate_Rejected = 'YES'
      End !0{prop:Acceptall}
    OF ?OK
      ThisWindow.Update
      Access:Jobs.Update()
      ! Inserting (DBH 16/09/2008) # 10253 - Update Date/Time Stamp
      UpdateDateTimeStamp(job:Ref_Number)
      ! End (DBH 16/09/2008) #10253
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
UpdateEstimatePart PROCEDURE                          !Generated from procedure template - Window

CurrentTab           STRING(80)
save_sto_id          USHORT,AUTO
fault_codes_required_temp STRING('NO {1}')
pos                  STRING(255)
Part_Details_Group   GROUP,PRE(TMP)
Part_Number          STRING(30)
Description          STRING(30)
Supplier             STRING(30)
Purchase_Cost        REAL
Sale_Cost            REAL
                     END
adjustment_temp      STRING(3)
quantity_temp        LONG
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
CPCSExecutePopUp     BYTE
tmp:location         STRING(30)
tmp:ShelfLocation    STRING(30)
tmp:SecondLocation   STRING(30)
sav:ExcludeFromOrder STRING(3)
sav:Supplier         STRING(30)
tmp:CreateNewOrder   BYTE(0)
UnHide_Tab           BYTE(0)
tmp:NewPurchasePrice REAL
tmp:PurchaseCost     REAL
tmp:OutWarrantyCost  REAL
tmp:OutWarrantyMarkup LONG
tmp:MaxMarkup        LONG
tmp:ARCPart          BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::epr:Record  LIKE(epr:RECORD),STATIC
QuickWindow          WINDOW('Estimate Part'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Estimate Part'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,284,310),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Estimate Part'),AT(68,64),USE(?Title),FONT('Tahoma',10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Part Number'),AT(68,80),USE(?epr:Part_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(160,80,124,10),USE(epr:Part_Number),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           BUTTON,AT(288,76),USE(?browse_stock_button),SKIP,TRN,FLAT,LEFT,ICON('lookupp.jpg')
                           PROMPT('Description'),AT(68,92),USE(?epr:Description:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(160,92,124,10),USE(epr:Description),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           PROMPT('Location'),AT(68,108),USE(?tmp:location:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,956,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(160,108,124,10),USE(tmp:location),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Shelf/Second Location'),AT(68,120),USE(?tmp:location:Prompt:2),TRN,LEFT,FONT('Tahoma',8,COLOR:White,956,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(160,120,60,10),USE(tmp:ShelfLocation),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           ENTRY(@s30),AT(224,120,60,10),USE(tmp:SecondLocation),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           GROUP,AT(64,136,248,136),USE(?AdjustmentGroup)
                             PROMPT('Purchase Cost'),AT(68,136),USE(?epr:Purchase_Cost:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@n14.2),AT(160,136,64,10),USE(tmp:PurchaseCost),SKIP,DISABLE,RIGHT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                             PROMPT('% Markup'),AT(228,140),USE(?Prompt29),FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Out Warranty Cost'),AT(68,148),USE(?epr:Sale_Cost:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@n14.2),AT(160,148,64,10),USE(tmp:OutWarrantyCost),SKIP,DISABLE,RIGHT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                             SPIN(@s3),AT(228,148,24,10),USE(tmp:OutWarrantyMarkup),FONT(,,010101H,FONT:bold),COLOR(COLOR:White),RANGE(0,99),STEP(5)
                             STRING('FIXED PRICE'),AT(260,148),USE(?String1),HIDE,FONT(,,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Supplier'),AT(68,164),USE(?epr:Supplier:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(160,164,124,10),USE(epr:Supplier),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),UPR
                             BUTTON,AT(288,160),USE(?LookupSupplier),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Quantity'),AT(68,180),USE(?Prompt7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             SPIN(@p<<<<<<<#p),AT(160,180,64,10),USE(epr:Quantity),RIGHT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,RANGE(1,99999999),STEP(1),MSG('Quantity')
                             PROMPT('Despatch Note No'),AT(68,196),USE(?epr:Despatch_Note_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(160,196,124,10),USE(epr:Despatch_Note_Number),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                             CHECK('Exclude From Ordering / Decrementing'),AT(160,212,144,8),USE(epr:Exclude_From_Order),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                             PROMPT('Part Status'),AT(68,224),USE(?Prompt24),FONT(,10,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                             CHECK('Part Allocated'),AT(160,224),USE(epr:PartAllocated),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Part Allocated'),TIP('Part Allocated'),VALUE('1','0')
                             PROMPT('Date Ordered'),AT(68,236),USE(?epr:Date_Ordered:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@d6b),AT(160,236,64,10),USE(epr:Date_Ordered),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                             PROMPT('Order Number'),AT(68,248),USE(?epr:Order_Number:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@n012b),AT(160,248,64,10),USE(epr:Order_Number),SKIP,FONT(,,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),READONLY,MSG('Order Number')
                             PROMPT('Date Received'),AT(68,260),USE(?epr:Date_Received:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@d6b),AT(160,260,64,10),USE(epr:Date_Received),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           END
                           PROMPT('Out Fault Code'),AT(68,298),USE(?OutFaultCode:Prompt),HIDE,FONT(,10,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                         END
                       END
                       SHEET,AT(352,54,264,310),USE(?FaultCodeSheet),COLOR(0D6E7EFH),SPREAD
                         TAB('Fault Coding'),USE(?FaultCodeTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Fault Codes Checked'),AT(444,74),USE(epr:Fault_Codes_Checked),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           PROMPT('Fault Code 1:'),AT(356,88),USE(?epr:Fault_Code1:Prompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(400,88,124,10),USE(epr:Fault_Code1,,?epr:Fault_Code1:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(432,88,124,10),USE(epr:Fault_Code1),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(560,84),USE(?Button4),FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(576,84),USE(?PopCalendar),FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 2:'),AT(356,106),USE(?epr:Fault_Code2:Prompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(400,106,124,10),USE(epr:Fault_Code2,,?epr:Fault_Code2:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(432,106,124,10),USE(epr:Fault_Code2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(560,102),USE(?Button4:2),FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(576,102),USE(?PopCalendar:2),FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 3:'),AT(356,124),USE(?epr:Fault_Code3:Prompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(400,124,124,10),USE(epr:Fault_Code3,,?epr:Fault_Code3:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(432,124,124,10),USE(epr:Fault_Code3),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(560,120),USE(?Button4:3),FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(576,120),USE(?PopCalendar:3),FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 4:'),AT(356,142),USE(?epr:Fault_Code4:Prompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(400,142,124,10),USE(epr:Fault_Code4,,?epr:Fault_Code4:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(432,142,124,10),USE(epr:Fault_Code4),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(560,138),USE(?Button4:4),FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(576,138),USE(?PopCalendar:4),FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 5:'),AT(356,160),USE(?epr:Fault_Code5:Prompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(400,160,124,10),USE(epr:Fault_Code5,,?epr:Fault_Code5:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(432,160,124,10),USE(epr:Fault_Code5),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(560,156),USE(?Button4:5),FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(576,156),USE(?PopCalendar:5),FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 6:'),AT(356,178),USE(?epr:Fault_Code6:Prompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(400,178,124,10),USE(epr:Fault_Code6,,?epr:Fault_Code6:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(432,178,124,10),USE(epr:Fault_Code6),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(560,174),USE(?Button4:6),FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(576,174),USE(?PopCalendar:6),FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 7:'),AT(356,196),USE(?epr:Fault_Code7:Prompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(400,196,124,10),USE(epr:Fault_Code7,,?epr:Fault_Code7:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(432,196,124,10),USE(epr:Fault_Code7),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(560,192),USE(?Button4:7),FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(576,192),USE(?PopCalendar:7),FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 8:'),AT(356,214),USE(?epr:Fault_Code8:Prompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(400,214,124,10),USE(epr:Fault_Code8,,?epr:Fault_Code8:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(432,214,124,10),USE(epr:Fault_Code8),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(560,210),USE(?Button4:8),FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(576,210),USE(?PopCalendar:8),FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 9:'),AT(356,232),USE(?epr:Fault_Code9:Prompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(400,232,124,10),USE(epr:Fault_Code9,,?epr:Fault_Code9:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(432,232,124,10),USE(epr:Fault_Code9),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(560,228),USE(?Button4:9),FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(576,228),USE(?PopCalendar:9),FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 10:'),AT(356,250),USE(?epr:Fault_Code10:Prompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(400,250,124,10),USE(epr:Fault_Code10,,?epr:Fault_Code10:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(432,250,124,10),USE(epr:Fault_Code10),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(560,246),USE(?Button4:10),FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(576,246),USE(?PopCalendar:10),FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 11:'),AT(356,268),USE(?epr:Fault_Code11:Prompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(400,268,124,10),USE(epr:Fault_Code11,,?epr:Fault_Code11:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(432,268,124,10),USE(epr:Fault_Code11),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(560,264),USE(?Button4:11),FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(576,264),USE(?PopCalendar:11),FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code 12:'),AT(356,286),USE(?epr:Fault_Code12:Prompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(400,286,124,10),USE(epr:Fault_Code12,,?epr:Fault_Code12:2),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(432,286,124,10),USE(epr:Fault_Code12),HIDE,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(560,282),USE(?Button4:12),FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(576,282),USE(?PopCalendar:12),FLAT,HIDE,ICON('lookupp.jpg')
                         END
                         TAB,USE(?NoFaultCodeTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('No Fault Codes Required'),AT(412,158),USE(?Prompt27),FONT(,12,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(480,366),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(548,366),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
save_map_id   ushort,auto
save_par_ali_id     ushort,auto                                  

!Save Entry Fields Incase Of Lookup
look:epr:Supplier                Like(epr:Supplier)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

EnableDisableCosts      Routine

!TB13351 - In the case of a warranty job the user must not be able to change any of the costs or percentage markup as warranty pricing for parts cannot be changed.
!J - 11/08/14 - Percentage markup is now available - but nothing else may be changed
!    If tmp:OutWarrantyMarkup = 0
!        Global.ReadOnly(?tmp:OutWarrantyCost,0)
!    Else !If tmp:OutWarrantyMarkup <> 0
!        Global.ReadOnly(?tmp:OutWarrantyCost,1)
!    End !If tmp:OutWarrantyMarkup <> 0
!TB13351 - END

    !Only allow to change the part detail up until it's accepted - L870 (DBH: 16-07-2003)
    If job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
    Else !If job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
        Global.ReadOnly(?tmp:OutWarrantyCost,1)
        Global.ReadOnly(?tmp:OutWarrantyMarkup,1)
    End !If job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'

    If tmp:OutWarrantyMarkup <> 0
        tmp:OutWarrantyCost = Markups(tmp:OutWarrantyCost,tmp:PurchaseCost,tmp:OutWarrantyMarkup)
    End !If tmp:OutWarrantyMarkup <> 0

    access:chartype.clearkey(cha:charge_type_key)
    cha:charge_type = job:charge_type
    If access:chartype.fetch(cha:charge_type_key) = Level:Benign
        !Zero Parts RRC
        If cha:Zero_Parts_ARC
            If tmp:ARCPart
                UNHIDE(?String1)
                ?tmp:OutWarrantyMarkup{prop:Disable} = 1
                tmp:OutWarrantyCost = 0
                ?tmp:OutWarrantyCost{prop:Disable} = 1
                If glo:WebJob
                    ?OK{prop:Hide} = 1
                End !If glo:WebJob
            End !If tmp:ARCPart
        End
        If cha:Zero_Parts = 'YES'
            If ~tmp:ARCPart
                UNHIDE(?String1)
                ?tmp:OutWarrantyMarkup{prop:Disable} = 1
                tmp:OutWarrantyCost = 0
                ?tmp:OutWarrantyCost{prop:Disable} = 1

                If ~glo:WebJob
                    ?OK{prop:Hide} = 1
                End !If ~glo:WebJob
            End !If ~tmp:ARCPart
        End
    end !if

    If ThisWindow.Request <> InsertRecord
        Global.ReadOnly(?tmp:PurchaseCost,1)
    End !If ThisWindow.Request <> InsertRecord

    If job:Estimate_Accepted = 'YES' or job:Estimate_Rejected = 'YES'
        ?OK{prop:Hide} = 1
    End !If job:Estimate_Accepted = 'YES' or job:Estimate_Rejected = 'YES'

    Display()
MaxMarkup_Message Routine
    Case Missive('You cannot mark-up parts by more than ' & Format(tmp:MaxMarkup,@n3) & ' %.','ServiceBase 3g',|
                   'mstop.jpg','/OK')
        Of 1 ! OK Button
    End ! Case Missive
ShowCosts       Routine
    !Do not allow ARC to edit RRC parts and vice versa - L789 (DBH: 17-07-2003)
    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number  =  GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Found
        !Get the Part's Location - L789 (DBH: 17-07-2003)
        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number  = epr:Part_Ref_Number
        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Found
            If sto:Location <> tra:SiteLocation
                !This part is NOT from ARC - L789 (DBH: 17-07-2003)
                if glo:RelocateStore = true and Sto:location = 'MAIN STORE' then
                    !this is permitted
                    !message('This should be OK')
                    tmp:ARCPart = 1
                ELSE

                    tmp:ARCPart = 0
                    If ~glo:WebJob
                        ?OK{prop:Hide} = 1
                    End !If glo:WebJob
                END
            Else ! !If sto:Location <> tra:SiteLocation
                tmp:ARCPart = 1
                If glo:WebJob
                    ?OK{prop:Hide} = 1
                End !If glo:WebJob
            End !If sto:Location <> tra:SiteLocation
        Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Error
        End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
    Else ! If Access:TRADE.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Error
    End !If Access:TRADE.Tryfetch(tra:Account_Number_Key) = Level:Benign

    If tmp:ARCPart
        tmp:PurchaseCost        = epr:AveragePurchaseCost
        tmp:OutWarrantyCost     = epr:Sale_Cost
        tmp:OutWarrantyMarkup   = epr:OutWarrantyMarkup
    Else !If tmp:ARCPart
        If epr:RRCAveragePurchaseCost > 0
            tmp:PurchaseCost    = epr:RRCAveragePurchaseCost
        Else !If epr:RRCAveragePurchaseCost > 0
            tmp:PurchaseCost    = epr:RRCPurchaseCost
        End !If epr:RRCAveragePurchaseCost > 0
        tmp:OutWarrantyCost     = epr:RRCSaleCost
        tmp:OutWarrantyMarkup   = epr:RRCOutWarrantyMarkup
    End !If tmp:ARCPart

    Display()
LookupMainFault     Routine
    ?OutFaultCode:Prompt{prop:Hide} = 1
    Access:MANFAUPA.ClearKey(map:MainFaultKey)
    map:Manufacturer = job:Manufacturer
    map:MainFault    = 1
    If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
        !Found
        Access:MANFAULT.ClearKey(maf:MainFaultKey)
        maf:Manufacturer = job:Manufacturer
        maf:MainFault    = 1
        If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            !Found
            Access:MANFAULO.ClearKey(mfo:Field_Key)
            mfo:Manufacturer = job:Manufacturer
            mfo:Field_Number = maf:Field_Number
            Case map:Field_Number
                Of 1
                    mfo:Field        = epr:Fault_Code1
                Of 2
                    mfo:Field        = epr:Fault_Code2
                Of 3
                    mfo:Field        = epr:Fault_Code3
                Of 4
                    mfo:Field        = epr:Fault_Code4
                Of 5
                    mfo:Field        = epr:Fault_Code5
                Of 6
                    mfo:Field        = epr:Fault_Code6
                Of 7
                    mfo:Field        = epr:Fault_Code7
                Of 8
                    mfo:Field        = epr:Fault_Code8
                Of 9
                    mfo:Field        = epr:Fault_Code9
                Of 10
                    mfo:Field        = epr:Fault_Code10
                Of 11
                    mfo:Field        = epr:Fault_Code11
                Of 12
                    mfo:Field        = epr:Fault_Code12
            End !Case map:Field_Number
            If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                !Found
                ?OutFaultCode:Prompt{prop:Hide} = 0
                ?OutFaultCode:Prompt{prop:Text} = 'Out Fault Code: ' & Clip(mfo:Description)
            Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

        Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

    Else!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
Adjustment          Routine

    If epr:adjustment = 'YES'
        epr:Part_Number = 'ADJUSTMENT'
        epr:Description = 'ADJUSTMENT'
        epr:Purchase_Cost = ''
        epr:Sale_Cost     = ''
        epr:quantity = 1


       ?epr:Part_Number{prop:readonly} = 0
       ?epr:Part_Number{prop:skip} = 0
       ?epr:Part_Number{prop:disable} = 1


       ?epr:Description{prop:readonly} = 0
       ?epr:Description{prop:skip} = 0
       ?epr:Description{prop:disable} = 1


       ?epr:Quantity{prop:readonly} = 0
       ?epr:Quantity{prop:skip} = 0
       ?epr:Quantity{prop:disable} = 1

       !TB13351 - J - 11/08/14 - never let the purchase cost be editable
       ?tmp:PurchaseCost{prop:readonly} = 1     !was 0
       ?tmp:PurchaseCost{prop:skip} = 1         !was 0
       ?tmp:PurchaseCost{prop:disable} = 1


       !TB13351 - J - 11/08/14 - never let the selling cost be directly editable
       ?tmp:OutWarrantyCost{prop:readonly} = 1   !all these were 0
       ?tmp:OutWarrantyCost{prop:skip} = 1
       ?tmp:OutWarrantyCost{prop:disable} = 1


       ?epr:Supplier{prop:readonly} = 0
       ?epr:Supplier{prop:skip} = 0
       ?epr:Supplier{prop:disable} = 1

       ?epr:Exclude_From_Order{prop:readonly} = 0
       ?epr:Exclude_From_Order{prop:skip} = 0
       ?epr:Exclude_From_Order{prop:disable} = 1

    Else


       ?epr:Part_Number{prop:readonly} = 0
       ?epr:Part_Number{prop:skip} = 0
       ?epr:Part_Number{prop:disable} = 0


       ?epr:Description{prop:readonly} = 0
       ?epr:Description{prop:skip} = 0
       ?epr:Description{prop:disable} = 0


       ?epr:Quantity{prop:readonly} = 0
       ?epr:Quantity{prop:skip} = 0
       ?epr:Quantity{prop:disable} = 0


       !TB13351 - J - 11/08/14 - never let the purchase cost be editable
       ?tmp:PurchaseCost{prop:readonly} = 1  !all these were 0
       ?tmp:PurchaseCost{prop:skip} = 1
       ?tmp:PurchaseCost{prop:disable} = 1

       !!TB13351 - J - 11/08/14 - never let the selling cost be directly editable
       ?tmp:OutWarrantyCost{prop:readonly} = 1  !all these were 0
       ?tmp:OutWarrantyCost{prop:skip} = 1
       ?tmp:OutWarrantyCost{prop:disable} = 1

       ?epr:Supplier{prop:readonly} = 0
       ?epr:Supplier{prop:skip} = 0
       ?epr:Supplier{prop:disable} = 0


       ?epr:Exclude_From_Order{prop:readonly} = 0
       ?epr:Exclude_From_Order{prop:skip} = 0
       ?epr:Exclude_From_Order{prop:disable} = 0

    End
    Display()

Hide_Fields     Routine
    If epr:part_ref_number = ''
        Do Show_parts
    Else
        Do Hide_parts
    End
Show_Parts      Routine

       ?epr:Part_Number{prop:readonly} = 0
       ?epr:Part_Number{prop:skip} = 0
       ?epr:Part_Number{prop:disable} = 0

       ?epr:Description{prop:readonly} = 0
       ?epr:Description{prop:skip} = 0
       ?epr:Description{prop:disable} = 0

!       ?tmp:PurchaseCost{prop:readonly} = 0
!       ?tmp:PurchaseCost{prop:skip} = 0
!       ?tmp:PurchaseCost{prop:disable} = 0
!
!
!       ?tmp:OutWarrantyCost{prop:readonly} = 0
!       ?tmp:OutWarrantyCost{prop:skip} = 0
!       ?tmp:OutWarrantyCost{prop:disable} = 0


       ?epr:Supplier{prop:readonly} = 0
       ?epr:Supplier{prop:skip} = 0
       ?epr:Supplier{prop:disable} = 0

       Display()


Hide_Parts      Routine


       ?epr:Part_Number{prop:readonly} = 1
       ?epr:Part_Number{prop:skip} = 1
       ?epr:Part_Number{prop:disable} = 0


       ?epr:Description{prop:readonly} = 1
       ?epr:Description{prop:skip} = 1
       ?epr:Description{prop:disable} = 0


!       ?tmp:PurchaseCost{prop:readonly} = 1
!       ?tmp:PurchaseCost{prop:skip} = 1
!       ?tmp:PurchaseCost{prop:disable} = 0
!
!
!       ?tmp:OutWarrantyCost{prop:readonly} = 1
!       ?tmp:OutWarrantyCost{prop:skip} = 1
!       ?tmp:OutWarrantyCost{prop:disable} = 0


       ?epr:Supplier{prop:readonly} = 1
       ?epr:Supplier{prop:skip} = 1
       ?epr:Supplier{prop:disable} = 0

       Display()

Lookup_Location     Routine
    If epr:part_ref_number <> ''
        access:stock.clearkey(sto:ref_number_key)
        sto:ref_number  = epr:part_ref_number
        If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
            tmp:location        = sto:location
            tmp:shelflocation   = sto:shelf_location
            tmp:secondlocation  = sto:second_location
        Else!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
            tmp:location    = ''
            tmp:shelflocation   = ''
            tmp:secondlocation  = ''
        End!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
    Else!If epr:part_ref_number <> ''
        tmp:location    = ''
        tmp:shelflocation   = ''
        tmp:secondlocation  = ''
    End!If epr:part_ref_number <> ''
    Display()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting An Estimate Part'
  OF ChangeRecord
    ActionMessage = 'Changing An Estimate Part'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020424'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateEstimatePart')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(epr:Record,History::epr:Record)
  SELF.AddHistoryField(?epr:Part_Number,5)
  SELF.AddHistoryField(?epr:Description,6)
  SELF.AddHistoryField(?epr:Supplier,7)
  SELF.AddHistoryField(?epr:Quantity,11)
  SELF.AddHistoryField(?epr:Despatch_Note_Number,14)
  SELF.AddHistoryField(?epr:Exclude_From_Order,13)
  SELF.AddHistoryField(?epr:PartAllocated,38)
  SELF.AddHistoryField(?epr:Date_Ordered,15)
  SELF.AddHistoryField(?epr:Order_Number,17)
  SELF.AddHistoryField(?epr:Date_Received,18)
  SELF.AddHistoryField(?epr:Fault_Codes_Checked,33)
  SELF.AddHistoryField(?epr:Fault_Code1:2,21)
  SELF.AddHistoryField(?epr:Fault_Code1,21)
  SELF.AddHistoryField(?epr:Fault_Code2:2,22)
  SELF.AddHistoryField(?epr:Fault_Code2,22)
  SELF.AddHistoryField(?epr:Fault_Code3:2,23)
  SELF.AddHistoryField(?epr:Fault_Code3,23)
  SELF.AddHistoryField(?epr:Fault_Code4:2,24)
  SELF.AddHistoryField(?epr:Fault_Code4,24)
  SELF.AddHistoryField(?epr:Fault_Code5:2,25)
  SELF.AddHistoryField(?epr:Fault_Code5,25)
  SELF.AddHistoryField(?epr:Fault_Code6:2,26)
  SELF.AddHistoryField(?epr:Fault_Code6,26)
  SELF.AddHistoryField(?epr:Fault_Code7:2,27)
  SELF.AddHistoryField(?epr:Fault_Code7,27)
  SELF.AddHistoryField(?epr:Fault_Code8:2,28)
  SELF.AddHistoryField(?epr:Fault_Code8,28)
  SELF.AddHistoryField(?epr:Fault_Code9:2,29)
  SELF.AddHistoryField(?epr:Fault_Code9,29)
  SELF.AddHistoryField(?epr:Fault_Code10:2,30)
  SELF.AddHistoryField(?epr:Fault_Code10,30)
  SELF.AddHistoryField(?epr:Fault_Code11:2,31)
  SELF.AddHistoryField(?epr:Fault_Code11,31)
  SELF.AddHistoryField(?epr:Fault_Code12:2,32)
  SELF.AddHistoryField(?epr:Fault_Code12,32)
  SELF.AddUpdateFile(Access:ESTPARTS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  Relate:LOCATION_ALIAS.Open
  Relate:PARTS_ALIAS.Open
  Relate:STOCKALX.Open
  Relate:USERS_ALIAS.Open
  Access:SUPPLIER.UseFile
  Access:MANFAUPA.UseFile
  Access:JOBS.UseFile
  Access:MANUFACT.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:PARTS.UseFile
  Access:USERS.UseFile
  Access:LOCATION.UseFile
  Access:REPTYDEF.UseFile
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ESTPARTS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','UpdateEstimatePart')
  Bryan.CompFieldColour()
  ?epr:Fault_Code7:2{Prop:Alrt,255} = MouseLeft2
  ?epr:Fault_Code8:2{Prop:Alrt,255} = MouseLeft2
  ?epr:Fault_Code9:2{Prop:Alrt,255} = MouseLeft2
  ?epr:Fault_Code10:2{Prop:Alrt,255} = MouseLeft2
  ?epr:Fault_Code11:2{Prop:Alrt,255} = MouseLeft2
  ?epr:Fault_Code12:2{Prop:Alrt,255} = MouseLeft2
  ?epr:Fault_Code1:2{Prop:Alrt,255} = MouseLeft2
  ?epr:Fault_Code2:2{Prop:Alrt,255} = MouseLeft2
  ?epr:Fault_Code3:2{Prop:Alrt,255} = MouseLeft2
  ?epr:Fault_Code4:2{Prop:Alrt,255} = MouseLeft2
  ?epr:Fault_Code5:2{Prop:Alrt,255} = MouseLeft2
  ?epr:Fault_Code6:2{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?epr:Supplier{Prop:Tip} AND ~?LookupSupplier{Prop:Tip}
     ?LookupSupplier{Prop:Tip} = 'Select ' & ?epr:Supplier{Prop:Tip}
  END
  IF ?epr:Supplier{Prop:Msg} AND ~?LookupSupplier{Prop:Msg}
     ?LookupSupplier{Prop:Msg} = 'Select ' & ?epr:Supplier{Prop:Msg}
  END
  tmp:MaxMarkup = GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI')
  Do Lookup_Location
  Do ShowCosts
  
  sav:ExcludeFromOrder    = epr:Exclude_From_Order
  sav:Supplier = epr:Supplier
  
  Do EnableDisableCosts
  
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
    Relate:LOCATION_ALIAS.Close
    Relate:PARTS_ALIAS.Close
    Relate:STOCKALX.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateEstimatePart')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    par:Adjustment = 'NO'
    par:Warranty_Part = 'NO'
    par:Exclude_From_Order = 'NO'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Browse_Suppliers
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?browse_stock_button
      Error# = 0
      user_code"  = ''
      If job:engineer <> ''
          Access:USERS.Clearkey(use:User_Code_Key)
          use:User_Code   = job:Engineer
          If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              !Found
              user_code"  = use:User_Code
          Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      
      
      Else!If job:engineer <> ''
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
              User_Code"  = use:User_Code
          Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      
      End!If job:engineer <> ''
      
      Access:LOCATION.ClearKey(loc:ActiveLocationKey)
      loc:Active   = 1
      loc:Location = use:Location
      If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign
          !Found
      
      Else!lIf Access:LOCATION.TryFetch(loc:ActiveLocationKey). = Level:Benign
          !Error
          Case Missive('The selected user is not allocated to an Active Site Location.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          If use:StockFromLocationOnly
              Error# = 1
          End !If use:StockFromLocationOnly
      End!If Access:LOCATION.TryFetch(loc:ActiveLocationKey). = Level:Benign
      
      If Error# = 0
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browse_model_stock(job:model_number,user_code")
          if globalresponse = requestcompleted
              access:stock.clearkey(sto:ref_number_key)
              sto:ref_number = stm:ref_number
      
              if access:stock.fetch(sto:ref_number_key)
                  Case Missive('Error! Cannot access the Stock File.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              Else!if access:stock.fetch(sto:ref_number_key)
                  If sto:Suspend
                      Case Missive('This part cannot be used. It has been suspended.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      epr:Part_Number = ''
                      Select(?epr:Part_Number)
                  Else !If sto:Suspend
                      If use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel
                          Case Missive('Error! Your skill level is not high enough to use the selected part.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          epr:Part_Number = ''
                          Select(?epr:Part_Number)
                      Else !If use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel
                          Access:JOBSE.Clearkey(jobe:RefNumberKey)
                          jobe:RefNumber  = job:Ref_Number
                          If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                              !Found
      
                          Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                              !Error
                          End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                          epr:part_number     = sto:part_number
                          If sto:Quantity_Stock = 0
                              !There is none is stock, so update the stock part with the
                              !costs from the Main Store part. And then apply these to this part
      
                              !Save the current record
                              Save_sto_ID = Access:STOCK.SaveFile()
      
                              !Get the MAIN STORE part
                              Access:STOCK.ClearKey(sto:Location_Key)
                              sto:Location    = MainStoreLocation()
                              sto:Part_Number = epr:Part_Number
                              If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                                  !Found
                                  tmp:NewPurchasePrice = sto:Sale_Cost
                              Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                              End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
      
                              !Restore the original Record
                              !And update the costs
                              Access:STOCK.RestoreFile(Save_sto_ID)
      
                              sto:AveragePurchaseCost = tmp:NewPurchasePrice
                              If sto:Percentage_Mark_Up
                                  sto:Sale_Cost = Markups(sto:Sale_Cost,sto:AveragePurchaseCost,sto:Percentage_Mark_Up)
                              End !If sto:PurchaseMarkUp
                              If sto:PurchaseMarkUp
                                  sto:Purchase_Cost = Markups(sto:Purchase_Cost,sto:AveragePurchaseCost,sto:PurchaseMarkup)
                              End !If sto:PurchaseMarkUp
                              Access:STOCK.Update()
                          End !If sto:Quantity_Stock = 0
                          epr:part_number     = sto:part_number
                          epr:description     = sto:description
                          epr:supplier        = sto:supplier
      
      
                          if glo:RelocateStore then
                              epr:AveragePurchaseCost = sto:Sale_Cost
                              epr:Purchase_Cost       = sto:Sale_Cost
                              epr:Sale_Cost           = sto:Sale_Cost*(1+glo:RelocatedMark_Up/100)
                              epr:InWarrantyMarkup    = InWarrantyMarkup(job:Manufacturer,glo:RelocatedFrom)
                              epr:OutWarrantyMarkUp   = glo:RelocatedMark_Up
                          ELSE
                              epr:AveragePurchaseCost     = sto:AveragePurchaseCost
                              epr:purchase_cost           = sto:purchase_cost
                              epr:sale_cost               = sto:sale_cost
                              epr:InWarrantyMarkup        = sto:PurchaseMarkUp
                              epr:OutWarrantyMarkup       = sto:Percentage_Mark_Up
                          END
      
                          epr:part_ref_number = sto:ref_number
      
                          epr:RRCPurchaseCost         = sto:purchase_Cost
                          epr:RRCSaleCost             = sto:sale_Cost
                          epr:RRCInWarrantyMarkup     = sto:PurchaseMarkUp
                          epr:RRCOutWarrantyMarkup    = sto:Percentage_Mark_Up
                          epr:RRCAveragePurchaseCost  = sto:AveragePurchaseCost
                          !If it's an RRC job and the parts are being added by the ARC
                          If ~glo:WebJob and jobe:WebJob
                              epr:RRCAveragePurchaseCost  = sto:sale_Cost
                              epr:RRCPurchaseCost           = Markups(epr:Purchase_Cost,epr:RRCAveragePurchaseCost,InWarrantyMarkup(job:Manufacturer,sto:Location))
                              epr:RRCSaleCost               = Markups(epr:RRCSaleCost,epr:RRCAveragePurchaseCost,loc:OutWarrantyMarkup)
                              epr:RRCInWarrantyMarkup       = sto:PurchaseMarkup
                              epr:RRCOutWarrantyMarkup      = sto:Percentage_Mark_Up
                          End !If ~glo:WebJob and jobe:WebJob
      
                           If sto:assign_fault_codes = 'YES'
                              stm:Manufacturer = sto:Manufacturer
                              stm:Model_Number = job:Model_Number
                              If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                                  epr:fault_code1    = stm:Faultcode1
                                  epr:fault_code2    = stm:Faultcode2
                                  epr:fault_code3    = stm:Faultcode3
                                  epr:fault_code4    = stm:Faultcode4
                                  epr:fault_code5    = stm:Faultcode5
                                  epr:fault_code6    = stm:Faultcode6
                                  epr:fault_code7    = stm:Faultcode7
                                  epr:fault_code8    = stm:Faultcode8
                                  epr:fault_code9    = stm:Faultcode9
                                  epr:fault_code10   = stm:Faultcode10
                                  epr:fault_code11   = stm:Faultcode11
                                  epr:fault_code12   = stm:Faultcode12
                              End!If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                          End!If sto:assign_fault_codes = 'YES'
                          access:chartype.clearkey(cha:charge_type_key)
                          cha:charge_type = job:charge_type
                          If access:chartype.fetch(cha:charge_type_key) = Level:Benign
                              If ~glo:WebJob
                                  If cha:Zero_Parts_ARC
                                      epr:Sale_Cost    = 0
                                  End
                              Else !If glo:WebJob
                                  If cha:Zero_Parts = 'YES'
                                      epr:RRCSaleCost  = 0
                                  End
                              End !If glo:WebJob
                          end !if
                          Select(?epr:quantity)
      
                      End !If use:RestrictParts And use:RestrictChargeable And sto:SkillLevel > use:SkillLevel
      
      
                  End !If sto:Suspend
                  display()
              end!if access:stock.fetch(sto:ref_number_key)
          end
          globalrequest     = saverequest#
          do lookup_location
      End !Error# = 0
      do LookupMainFault
      Do ShowCosts
      Do EnableDisableCosts
    OF ?tmp:OutWarrantyMarkup
      if ~0{prop:AcceptAll}
          if tmp:MaxMarkup <> 0
              if tmp:OutWarrantyMarkup > tmp:MaxMarkup
                  tmp:OutWarrantyMarkup = tmp:MaxMarkup
                  do MaxMarkup_Message
                  select(?tmp:OutWarrantyMarkup)
                  cycle
              end
          end
      end
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020424'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020424'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020424'&'0')
      ***
    OF ?epr:Part_Number
      If ~0{prop:acceptall}
      error# = 0
      access:users.clearkey(use:user_code_key)
      use:user_code = job:engineer
      if access:users.tryfetch(use:user_code_key)
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
              Access:LOCATION.ClearKey(loc:Location_Key)
              loc:Location = use:Location
              If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
                  !Found
                  If loc:Active = 0
                      Error# = 3
                  End !If loc:Active = 0
              Else!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
              If Error# = 0
                  access:stock.clearkey(sto:location_manufacturer_key)
                  sto:location     = use:location
                  sto:manufacturer = job:manufacturer
                  sto:part_number  = epr:part_number
                  if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                      Access:STOMODEL.ClearKey(stm:Mode_Number_Only_Key)
                      stm:Ref_Number   = sto:Ref_Number
                      stm:Model_Number = job:Model_Number
                      If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                          !Found
                          If sto:Suspend
                              error# = 2
                          Else !If sto:Suspended
                              error# = 0
                          End !If sto:Suspended
                      Else!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                          !Error
                          error# = 1
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                  Else!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                      error# = 1
                  End!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
      
              End !If Error# = 0
      
          End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      
      Else!if access:users.tryfetch(use:user_code_key) = Level:Benign
          Access:LOCATION.ClearKey(loc:Location_Key)
          loc:Location = use:Location
          If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
              !Found
              If loc:Active = 0
                  Error# = 3
              End !If loc:Active = 0
          Else!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
          If Error# = 0
              access:stock.clearkey(sto:location_manufacturer_key)
              sto:location     = use:location
              sto:manufacturer = job:manufacturer
              sto:part_number  = epr:part_number
              if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                  Access:STOMODEL.ClearKey(stm:Mode_Number_Only_Key)
                  stm:Ref_Number   = sto:Ref_Number
                  stm:Model_Number = job:Model_Number
                  If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                      !Found
                      If sto:Suspend
                          error# = 2
                      Else !If sto:Suspended
                          error# = 0
                      End !If sto:Suspended
                  Else!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                      !Error
                      error# = 1
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
      
              Else!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                  error# = 1
              End!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
          End !If Error# = 0
      End!if access:users.tryfetch(use:user_code_key) = Level:Benign
      
      Case error#
          Of 0
              epr:part_number     = sto:part_number
              epr:description     = sto:description
              epr:supplier        = sto:supplier
              epr:purchase_cost   = sto:purchase_cost
              epr:sale_cost       = sto:sale_cost
              epr:part_ref_number = sto:ref_number
              If sto:assign_fault_codes = 'YES'
                  Access:STOMODEL.ClearKey(stm:Model_Number_Key)
                  stm:Ref_Number   = sto:Ref_Number
                  stm:Manufacturer = sto:Manufacturer
                  stm:Model_Number = job:Model_Number
                  If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                      epr:fault_code1    = stm:Faultcode1
                      epr:fault_code2    = stm:Faultcode2
                      epr:fault_code3    = stm:Faultcode3
                      epr:fault_code4    = stm:Faultcode4
                      epr:fault_code5    = stm:Faultcode5
                      epr:fault_code6    = stm:Faultcode6
                      epr:fault_code7    = stm:Faultcode7
                      epr:fault_code8    = stm:Faultcode8
                      epr:fault_code9    = stm:Faultcode9
                      epr:fault_code10   = stm:Faultcode10
                      epr:fault_code11   = stm:Faultcode11
                      epr:fault_code12   = stm:Faultcode12
      
                  End!If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
              End!If sto:assign_fault_codes = 'YES'
              Select(?epr:quantity)
              display()
      
          Of 1
              Case Missive('Cannot find the selected part!'&|
                '<13,10>It either does not exist in the engineer''s location, or is not in stock.'&|
                '<13,10>Do you wish to SELECT another part, or CONTINUE using the entered part?','ServiceBase 3g',|
                             'mquest.jpg','Continue|Select')
                  Of 2 ! Select Button
                      Post(event:accepted,?browse_stock_button)
                  Of 1 ! Continue Button
                      Select(?epr:description)
              End ! Case Missive
          Of 2
              Case Missive('This part cannot be used. It has been suspended.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              epr:Part_Number = ''
              Select(?epr:Part_Number)
          Of 3
              Case Missive('Error! Stock for this job will be picked from location ' & Clip(use:Location) & ' by default.'&|
                '<13,10>'&|
                '<13,10>This location is not active and cannot be used.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              epr:Part_Number = ''
              Select(?epr:Part_Number)
      End!If error# = 0
      Do Lookup_Location
      Display()
      End!If ~0{prop:acceptall}
    OF ?tmp:OutWarrantyMarkup
      Do EnableDisableCosts
    OF ?epr:Supplier
      If ~0{prop:acceptall}
          If sav:Supplier <> epr:Supplier
              If SecurityCheck('PARTS - AMEND SUPPLIER')
                  Case Missive('You do not have sufficient access to change the supplier.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  epr:Supplier    = sav:Supplier
              Else!If SecurityCheck('PARTS - AMEND SUPPLIER')
                  sav:Supplier    = epr:Supplier
              End!If SecurityCheck('PARTS - AMEND SUPPLIER')
          End!If sav:Supplier <> epr:Supplier
      End!If ~0{prop:acceptall}
      Display(?epr:Supplier)
      IF epr:Supplier OR ?epr:Supplier{Prop:Req}
        sup:Company_Name = epr:Supplier
        !Save Lookup Field Incase Of error
        look:epr:Supplier        = epr:Supplier
        IF Access:SUPPLIER.TryFetch(sup:Company_Name_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            epr:Supplier = sup:Company_Name
          ELSE
            !Restore Lookup On Error
            epr:Supplier = look:epr:Supplier
            SELECT(?epr:Supplier)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupSupplier
      ThisWindow.Update
      sup:Company_Name = epr:Supplier
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          epr:Supplier = sup:Company_Name
          Select(?+1)
      ELSE
          Select(?epr:Supplier)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?epr:Supplier)
    OF ?epr:Exclude_From_Order
      If ~0{prop:acceptall}
          If sav:ExcludeFromOrder <> epr:Exclude_From_Order
              If SecurityCheck('PARTS - EXCLUDE FROM ORDER')
                  Case Missive('You do not have access to this option.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  epr:Exclude_From_Order = sav:ExcludeFromOrder
              Else!If SecurityCheck('EXCLUDE FROM ORDER')
                  If thiswindow.request = Insertrecord
                      If epr:Exclude_From_Order = 'YES' And epr:part_ref_number <> ''
                          Case Missive('You have selected to "Exclude From Order".'&|
                            '<13,10>Warning! This stock part will NOT be decremented and an order will NOT be raised if there are insufficient items in stock.'&|
                            '<13,10>Are you sure?','ServiceBase 3g',|
                                         'mquest.jpg','\No|/Yes')
                              Of 2 ! Yes Button
                                  sav:ExcludeFromOrder    = epr:Exclude_From_Order
                              Of 1 ! No Button
                                  epr:Exclude_From_Order  = sav:ExcludeFromOrder
                          End ! Case Missive
                      End!If epr:Exclude_From_Order = 'YES'
                  End!If thiswindow.request = Insertrecord
              End!If SecurityCheck('EXCLUDE FROM ORDER')
          End!If sav:ExcludeFromOrder <> epr:Exclude_From_Order
      End!If ~0{prop:acceptall}
      Display()
    OF ?epr:Fault_Code1
      If epr:fault_code1 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 1
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 1
                  mfp:field        = epr:fault_code1
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          epr:fault_code1 = mfp:field
                      else
                          epr:fault_code1 = ''
                          select(?-1)
                      end
                      display(?epr:fault_code1)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If epr:fault_code1 <> ''
    OF ?Button4
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 1
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          epr:fault_code1 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          epr:Fault_Code1 = TINCALENDARStyle1(epr:Fault_Code1)
          Display(?epr:Fault_Code1:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?epr:Fault_Code2
      If epr:fault_Code2 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 2
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES' AND map:MainFault = 0 !workaround
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 2
                  mfp:field        = epr:fault_code2
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          epr:fault_code2 = mfp:field
                      else
                          epr:fault_code2 = ''
                          select(?-1)
                      end
                      display(?epr:fault_code2)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If epr:fault_Code2 <> ''
      Do LookupMainFault
    OF ?Button4:2
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 2
      glo:select3  = ''
      
      !Check to se if we need to use main file!
      Access:ManFauPa.ClearKey(map:Field_Number_Key)
      map:Manufacturer = job:manufacturer
      map:Field_Number = 2
      IF Access:ManFauPa.Fetch(map:Field_Number_Key)
        !Can'thappen :)p
      ELSE
        IF map:MainFault = TRUE
          !This is where the fun begins!
          Access:ManFault.ClearKey(maf:MainFaultKey)
          maf:Manufacturer = job:manufacturer
          maf:MainFault = 1
          IF Access:ManFault.Fetch(maf:MainFaultKey)
            !No main fault!
          ELSE
            glo:select2  = maf:Field_Number
          END
          Browse_Manufacturer_Fault_Lookup
          if globalresponse = requestcompleted
              epr:fault_code2 = mfo:field
          Else
              !epr:fault_code2 = ''
          end
        ELSE
          Browse_Manufacturer_Part_Lookup
          if globalresponse = requestcompleted
              epr:fault_code2 = mfp:field
          Else
              !epr:fault_code2 = ''
          end
        END
      END
      display()
      
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      
      Do LookupMainFault
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          epr:Fault_Code2 = TINCALENDARStyle1(epr:Fault_Code2)
          Display(?epr:Fault_Code2:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?epr:Fault_Code3
      If epr:fault_code3 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 3
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 3
                  mfp:field        = epr:fault_code3
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          epr:fault_code3 = mfp:field
                      else
                          epr:fault_code3 = ''
                          select(?-1)
                      end
                      display(?epr:fault_code3)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If epr:fault_code3 <> ''
    OF ?Button4:3
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 3
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          epr:fault_code3 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          epr:Fault_Code3 = TINCALENDARStyle1(epr:Fault_Code3)
          Display(?epr:Fault_Code3:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?epr:Fault_Code4
      If epr:fault_code4 <> ''
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 4
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 4
                  mfp:field        = epr:fault_code4
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          epr:fault_code4 = mfp:field
                      else
                          epr:fault_code4 = ''
                          select(?-1)
                      end
                      display(?epr:fault_code4)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If epr:fault_code4 <> ''
    OF ?Button4:4
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 4
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          epr:fault_code4 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          epr:Fault_Code4 = TINCALENDARStyle1(epr:Fault_Code4)
          Display(?epr:Fault_Code4:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?epr:Fault_Code5
      If epr:fault_code5 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 5
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 5
                  mfp:field        = epr:fault_code5
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          epr:fault_code5 = mfp:field
                      else
                          epr:fault_code5 = ''
                          select(?-1)
                      end
                      display(?epr:fault_code5)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If epr:fault_code5 <> ''
    OF ?Button4:5
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 5
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          epr:fault_code5 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          epr:Fault_Code5 = TINCALENDARStyle1(epr:Fault_Code5)
          Display(?epr:Fault_Code5:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?epr:Fault_Code6
      IF epr:fault_code6 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 6
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 6
                  mfp:field        = epr:fault_code6
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          epr:fault_code6 = mfp:field
                      else
                          epr:fault_code6 = ''
                          select(?-1)
                      end
                      display(?epr:fault_code6)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!IF epr:fault_code6 <> ''
    OF ?Button4:6
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 6
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          epr:fault_code6 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          epr:Fault_Code6 = TINCALENDARStyle1(epr:Fault_Code6)
          Display(?epr:Fault_Code6:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?epr:Fault_Code7
      If epr:fault_code7 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 7
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 7
                  mfp:field        = epr:fault_code7
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          epr:fault_code7 = mfp:field
                      else
                          epr:fault_code7 = ''
                          select(?-1)
                      end
                      display(?epr:fault_code7)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If epr:fault_code7 <> ''
    OF ?Button4:7
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 7
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          epr:fault_code7 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          epr:Fault_Code7 = TINCALENDARStyle1(epr:Fault_Code7)
          Display(?epr:Fault_Code7:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?epr:Fault_Code8
      If epr:fault_Code8 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 8
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 8
                  mfp:field        = epr:fault_code8
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          epr:fault_code8 = mfp:field
                      else
                          epr:fault_code8 = ''
                          select(?-1)
                      end
                      display(?epr:fault_code8)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If epr:fault_Code8 <> ''
    OF ?Button4:8
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 8
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          epr:fault_code8 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          epr:Fault_Code8 = TINCALENDARStyle1(epr:Fault_Code8)
          Display(?epr:Fault_Code8:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?epr:Fault_Code9
      If epr:fault_code9 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 9
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 9
                  mfp:field        = epr:fault_code9
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          epr:fault_code9 = mfp:field
                      else
                          epr:fault_code9 = ''
                          select(?-1)
                      end
                      display(?epr:fault_code9)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If epr:fault_code9 <> ''
    OF ?Button4:9
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 9
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          epr:fault_code9 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          epr:Fault_Code9 = TINCALENDARStyle1(epr:Fault_Code9)
          Display(?epr:Fault_Code9:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?epr:Fault_Code10
      If epr:fault_code10 <> ''
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 10
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 10
                  mfp:field        = epr:fault_code10
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          epr:fault_code10 = mfp:field
                      else
                          epr:fault_code10 = ''
                          select(?-1)
                      end
                      display(?epr:fault_code10)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If epr:fault_code10 <> ''
    OF ?Button4:10
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 10
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          epr:fault_code10 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          epr:Fault_Code10 = TINCALENDARStyle1(epr:Fault_Code10)
          Display(?epr:Fault_Code10:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?epr:Fault_Code11
      If epr:fault_code11 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 11
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 11
                  mfp:field        = epr:fault_code11
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          epr:fault_code11 = mfp:field
                      else
                          epr:fault_code11 = ''
                          select(?-1)
                      end
                      display(?epr:fault_code11)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If epr:fault_code11 <> ''
    OF ?Button4:11
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 11
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          epr:fault_code11 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          epr:Fault_Code11 = TINCALENDARStyle1(epr:Fault_Code11)
          Display(?epr:Fault_Code11:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?epr:Fault_Code12
      If epr:fault_code12 <> ''
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 12
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 12
                  mfp:field        = epr:fault_code12
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          epr:fault_code12 = mfp:field
                      else
                          epr:fault_code12 = ''
                          select(?-1)
                      end
                      display(?epr:fault_code12)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If epr:fault_code12 <> ''
    OF ?Button4:12
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 12
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          epr:fault_code12 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          epr:Fault_Code12 = TINCALENDARStyle1(epr:Fault_Code12)
          Display(?epr:Fault_Code12:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Fault Codes Checked
  If epr:fault_codes_checked <> 'YES'
      If fault_codes_required_temp = 'YES'
          Case Missive('You must tick the "Fault Codes Checked" box before you can continue.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End!If fault_codes_required_temp = 'YES'
  End!If wpr:fault_codes_checked <> 'YES'
  !Check max markup
  if tmp:MaxMarkup <> 0
      if tmp:OutWarrantyMarkup > tmp:MaxMarkup
          do MaxMarkup_Message
          select(?tmp:OutWarrantyMarkup)
          cycle
      end
  end
    If ThisWindow.Request = insertrecord
        epr:PartAllocated = 1
        Set(DEFAULTS)
        Access:DEFAULTS.Next()
        found# = 0
    !Is this part number already on the job?
        If epr:part_number <> 'ADJUSTMENT'

        End!If epr:part_number <> 'ADJUSTMENT'
        If epr:exclude_from_order <> 'YES'
            If epr:part_ref_number <> ''                                                        !Using A STock Item
                Case Missive('Is this part being fitted to the repair NOW or will it be fitted only when the estimate is ACCEPTED?','ServiceBase 3g',|
                               'mquest.jpg','Accepted|Now')
                    Of 2 ! Now Button
                        epr:UsedOnRepair = 1
                        access:stock.clearkey(sto:ref_number_key)
                        sto:ref_number = epr:part_ref_number
                        if access:stock.fetch(sto:ref_number_key)
                            Case Missive('An error has occurred retrieving the details of this stock part.','ServiceBase 3g',|
                                           'mstop.jpg','/OK')
                                Of 1 ! OK Button
                            End ! Case Missive
                        Else!if access:stock.fetch(sto:ref_number_key)
                            If sto:sundry_item <> 'YES'

                                If epr:quantity > sto:quantity_stock
                                    Case Missive('There are insufficient items in stock. '&'for stock ref no ='&clip(sto:Ref_Number),'ServiceBase 3g',|
                                                   'mstop.jpg','/OK')
                                        Of 1 ! OK Button
                                    End ! Case Missive
                                    Select(?epr:Quantity)
                                    Cycle
                                Else !If epr:quantity > sto:quantity_stock                              !This is sufficient in stock
                                    !If its not a Rapid site, turn part allocated off
                                    If RapidLocation(sto:Location)
                                        epr:PartAllocated = 0
                                    End !If RapidLocation(sto:Location) = Level:Benign
                                    Access:ESTPARTS.Update()

                                    !Add To Stock Allocation Browse - L873 (DBH: 11-08-2003)
                                    AddToStockAllocation(epr:Record_Number,'EST',epr:Quantity,'',job:Engineer)

                                    epr:date_ordered = Today()
                                    sto:quantity_stock -= epr:quantity
                                    If sto:quantity_stock < 0
                                        sto:quantity_stock = 0
                                    End
                                    Access:stock.update()
                                    set(defaults)
                                    access:defaults.next()
                                    If def:add_stock_label = 'YES'
                                        glo:select1  = sto:ref_number
                                        case def:label_printer_type
                                            of 'TEC B-440 / B-442'
                                                stock_request_label(Today(),Clock(),epr:quantity,job:ref_number,job:engineer,epr:sale_cost)
                                            of 'TEC B-452'
                                                stock_request_label_B452(Today(),Clock(),epr:quantity,job:ref_number,job:engineer,epr:sale_cost)
                                        end!case def:label_printer_type
                                        glo:select1 = ''
                                    End!If def:add_stock_label = 'YES'
                                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                         'DEC', | ! Transaction_Type
                                                         epr:Despatch_Note_Number, | ! Depatch_Note_Number
                                                         job:Ref_Number, | ! Job_Number
                                                         0, | ! Sales_Number
                                                         epr:Quantity, | ! Quantity
                                                         epr:Purchase_Cost, | ! Purchase_Cost
                                                         epr:Sale_Cost, | ! Sale_Cost
                                                         epr:Retail_Cost, | ! Retail_Cost
                                                         'STOCK DECREMENTED', | ! Notes
                                                         '') ! Information
                                        ! Added OK
                                    Else ! AddToStockHistory
                                        ! Error
                                    End ! AddToStockHistory
                                    ! #10396 Suspend part if none in stock and suspended at main store. (DBH: 16/07/2010)
                                    SuspendedPartCheck()
                                End !If epr:quantity > sto:quantity_stock
                            Else!!!If sto:sundry_item = 'YES'                                           !Using A Sundry Stock Item
                                epr:date_ordered = Today()                                              !Not need to order or decrement !
                            End!If sto:sundry_item = 'YES'
                        end!if access:stock.fetch(sto:ref_number_key)
                    Of 1 ! Accepted Button
                End ! Case Missive

            Else !If epr:part_ref_number <> ''                                                  !A Non Stock Item
            End !If epr:part_ref_number <> ''
        Else!End !If epr:exclude_from_order <> 'YES'
            epr:date_ordered = Today()
        End !If epr:exclude_from_order <> 'YES'
        !remmed JAC - 090902
        !SolaceViewVars(epr:Part_Ref_Number,,,6)

        Access:STOCK.ClearKey(sto:ref_number_key)
        sto:ref_number = epr:part_ref_number
        if not Access:STOCK.Fetch(sto:ref_number_key)
            if sto:ExchangeUnit = 'YES'
                ! Change repair type to 'exchange estimate'
                Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
                rtd:Manufacturer = job:Manufacturer
                rtd:Chargeable   = 'YES'
                set(rtd:ChaManRepairTypeKey, rtd:ChaManRepairTypeKey)
                loop until Access:REPTYDEF.Next()
                    if rtd:Manufacturer <> job:Manufacturer then break.
                    if rtd:Chargeable <> 'YES' then break.
                    if rtd:BER = 9 ! Exchange Estimate
                        job:Repair_Type = rtd:Repair_Type
                        break
                    end
                end

            end
        end

    End !If localrequest = insertrecord

  !Write Costs Back
      If tmp:ARCPart
          epr:AveragePurchaseCost     = tmp:PurchaseCost
          epr:Sale_Cost               = tmp:OutWarrantyCost
          epr:OutWarrantyMarkup       = tmp:OutWarrantyMarkup
      Else !If tmp:ARCPart
          epr:RRCAveragePurchaseCost  = tmp:PurchaseCost
          epr:RRCSaleCost             = tmp:OutWarrantyCost
          epr:RRCOutWarrantyMarkup    = tmp:OutWarrantyMarkup
          epr:Purchase_Cost           = epr:RRCPurchaseCost
          epr:Sale_Cost               = epr:RRCSaleCost
      End !If tmp:ARCPart
  If Thiswindow.request <> Insertrecord
      check_parts# = 0
      access:stock.clearkey(sto:ref_number_key)
      sto:ref_number = epr:part_ref_number
      if access:stock.fetch(sto:ref_number_key) = Level:benign
          If sto:sundry_item <> 'YES'
              check_parts# = 1
          End!If sto:sundry_item <> 'YES'
  
          If check_parts# = 1
              If TMP:Part_Number <> epr:part_number Or |
                TMP:Description <> epr:description Or |
                TMP:Supplier <> epr:supplier Or |
                TMP:Purchase_Cost <> epr:purchase_cost Or |
                TMP:Sale_Cost <> epr:sale_cost Or |
                quantity_temp <> epr:quantity
  
                  !Does a pending part exist. If so, you can't change this part.
                  If epr:pending_ref_number = ''
                      setcursor(cursor:wait)
                      save_par_ali_id = access:parts_alias.savefile()
                      access:parts_alias.clearkey(par_ali:part_number_key)
                      par_ali:ref_number  = job:ref_number
                      par_ali:part_number = tmp:part_number
                      set(par_ali:part_number_key,par_ali:part_number_key)
                      loop
                          if access:parts_alias.next()
                             break
                          end !if
                          if par_ali:ref_number  <> job:ref_number      |
                          or par_ali:part_number <> tmp:part_number      |
                              then break.  ! end if
                          If par_ali:record_number <> epr:record_number
                              If par_ali:date_received = ''
                                  found# = 1
                                  Break
                              End!If par_ali:date_received = ''
                          End!If par_ali:record_number <> epr:record_number
                      end !loop
                      access:parts_alias.restorefile(save_par_ali_id)
                      setcursor()
  
                      If found# = 1
                          Case Missive('The part is awaiting Order Generation.'&|
                            '<13,10>'&|
                            '<13,10>While a "Pending" Part exists, you cannot amend this part.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          epr:part_number     = tmp:part_number
                          epr:description     = tmp:description
                          epr:supplier        = tmp:supplier
                          epr:purchase_cost   = tmp:purchase_cost
                          epr:sale_cost       = tmp:sale_cost
                          epr:quantity    = quantity_temp
                          Display()
                          Select(?epr:part_number)
                          Cycle
                      Else!If found# = 1
                          If epr:quantity > quantity_temp
                              If epr:quantity - quantity_temp > sto:quantity_stock
                                  Case Missive('There are insufficient items in stock.'&'for stock ref no ='&clip(sto:Ref_Number)&|
                                    '<13,10>Do you wish to ORDER the remaining items, or RE-ENTER the quantity required?','ServiceBase 3g',|
                                                 'mquest.jpg','\Cancel|Re-Enter|Order')
                                      Of 3 ! Order Button
                                          If SecurityCheck('JOBS - ORDER PARTS')
                                              Case Missive('You do not have access to Order Parts.','ServiceBase 3g',|
                                                             'mstop.jpg','/OK')
                                                  Of 1 ! OK Button
                                              End ! Case Missive
                                              Select(?epr:Quantity)
                                              Cycle
                                          Else !If SecurityCheck('JOBS - ORDER PARTS')
                                              get(ordpend,0)
                                              if access:ordpend.primerecord() = level:benign
                                                  ope:part_ref_number = sto:ref_number
                                                  ope:job_number      = job:ref_number
                                                  ope:part_type       = 'JOB'
                                                  ope:supplier        = epr:supplier
                                                  ope:part_number     = epr:part_number
                                                  ope:description     = epr:description
                                                  ope:quantity        = epr:quantity - quantity_temp
                                                  if access:ordpend.insert()
                                                      access:ordpend.cancelautoinc()
                                                  end
                                              end!if access:ordpend.primerecord() = level:benign
                                              glo:select1      = 'NEW PENDING'
                                              glo:select2      = epr:part_ref_number
                                              glo:select3      = (epr:quantity - quantity_temp) - sto:quantity_stock
                                              glo:select4      = ope:ref_number
                                              epr:quantity     = quantity_temp + sto:quantity_stock
  
!                                              !If the stock is relocated then we jump the gun a bit at this point
!                                              !Log Number 012031 ( ARC To Request Spares directly) - JC - 25/05/12
!                                              if glo:RelocateStore then
!                                                  !epr:WebOrder    = 1
!                                                  epr:PartAllocated = 0
!                                                  Access:estPARTS.Update()
!                                                  AddToStockAllocation(epr:Record_Number,'EST',epr:Quantity,'WEB',job:Engineer)
!                                              END !if glo:RelocateStore
  
                                          End !If SecurityCheck('JOBS - ORDER PARTS')
                                      Of 2 ! Re-Enter Button
                                          Select(?epr:quantity)
                                          Cycle
                                      Of 1 ! Cancel Button
                                          Select(?epr:part_number)
                                          Cycle
                                  End ! Case Missive
                              Else !If epr:quantity - quantity_temp > sto:quantity_stock
                                  sto:quantity_stock -= (epr:quantity - quantity_temp)
                                  If sto:quantity_stock < 0
                                      sto:quantity_stock = 0
                                  End
                                  Access:stock.Update()
                                  If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                     'DEC', | ! Transaction_Type
                                                     epr:Despatch_Note_NUmber, | ! Depatch_Note_Number
                                                     job:Ref_Number, | ! Job_Number
                                                     0, | ! Sales_Number
                                                     epr:Quantity - Quantity_Temp, | ! Quantity
                                                     epr:Purchase_Cost, | ! Purchase_Cost
                                                     epr:Sale_Cost, | ! Sale_Cost
                                                     epr:Retail_Cost, | ! Retail_Cost
                                                     'STOCK DECREMENTED', | ! Notes
                                                     '') ! Information
                                    ! Added OK
                                  Else ! AddToStockHistory
                                    ! Error
                                  End ! AddToStockHistory
                              End !If epr:quantity - quantity_temp > sto:quantity_stock
                          Else !If epr:quantity > quantity_temp
                              !If quantity is lowered
                              sto:quantity_stock += quantity_temp - epr:quantity
                              Access:stock.Update()
                              If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                 'REC', | ! Transaction_Type
                                                 epr:Despatch_Note_Number, | ! Depatch_Note_Number
                                                 job:Ref_Number, | ! Job_Number
                                                 0, | ! Sales_Number
                                                 Quantity_Temp - epr:Quantity, | ! Quantity
                                                 epr:Purchase_Cost, | ! Purchase_Cost
                                                 epr:Sale_Cost, | ! Sale_Cost
                                                 epr:Retail_Cost, | ! Retail_Cost
                                                 'STOCK RECREDITED', | ! Notes
                                                 '') ! Information
                                ! Added OK
                              Else ! AddToStockHistory
                                ! Error
                              End ! AddToStockHistory
                          End !If epr:quantity > quantity_temp
                      End!If found# = 1
                  Else!If epr:pending_ref_number = ''
                      If TMP:Part_Number <> epr:part_number Or |
                        TMP:Description <> epr:description Or |
                        TMP:Supplier <> epr:supplier Or |
                        TMP:Purchase_Cost <> epr:purchase_cost Or |
                        TMP:Sale_Cost <> epr:sale_cost
  
                          Case Missive('Cannot amend. This part is awaiting Order Generation.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          epr:part_number     = tmp:part_number
                          epr:description     = tmp:description
                          epr:supplier        = tmp:supplier
                          epr:purchase_cost   = tmp:purchase_cost
                          epr:sale_cost       = tmp:sale_cost
                          epr:quantity    = quantity_temp
                          Display()
                          Select(?epr:part_number)
                          Cycle
                      Else !!If part_details_group <> epr:part_details_group
                          !As long as the part details haven't changed
                          !Amend the pending order
  
                          access:ordpend.clearkey(ope:ref_number_key)
                          ope:ref_number = job:ref_number
                          if access:ordpend.fetch(ope:ref_number_key) = Level:Benign
                              If epr:Quantity < Quantity_Temp
                                  ope:Quantity -= (Quantity_temp - epr:Quantity)
                              End!If epr:Quantity < Quantity_Temp
                              If epr:Quantity > Quantity_Temp
                                  ope:Quantity += (epr:Quantity - Quantity_Temp)
                              End!If epr:Quantity > Quantity_Temp
  
                              Access:ordpend.Update()
                          end !if
  
                      End!If part_details_group <> epr:part_details_group
                  End!If epr:pending_ref_number = ''
              End!If part_details_group <> epr:part_details_group
          End!If check_parts# = 1
      End!if access:stock.fetch(sto:ref_number_key) = Level:benign
  End!If Thiswindow.request <> Insertrecord
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?epr:Fault_Code1:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?epr:Fault_Code2:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  OF ?epr:Fault_Code3:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:3)
      CYCLE
    END
  OF ?epr:Fault_Code4:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:4)
      CYCLE
    END
  OF ?epr:Fault_Code5:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:5)
      CYCLE
    END
  OF ?epr:Fault_Code6:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:6)
      CYCLE
    END
  OF ?epr:Fault_Code7:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:7)
      CYCLE
    END
  OF ?epr:Fault_Code8:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:8)
      CYCLE
    END
  OF ?epr:Fault_Code9:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:9)
      CYCLE
    END
  OF ?epr:Fault_Code10:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:10)
      CYCLE
    END
  OF ?epr:Fault_Code11:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:11)
      CYCLE
    END
  OF ?epr:Fault_Code12:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:12)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?tmp:OutWarrantyMarkup
    Do EnableDisableCosts
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
      !Is this An Adjustment?
      If glo:select1 = 'ADJUSTMENT' And thiswindow.request = Insertrecord
          epr:adjustment = 'YES'
          epr:part_number = 'ADJUSTMENT'
          epr:description = 'ADJUSTMENT'
      
      End!If glo:select1 = 'ADJUSTMENT' And thiswindow.request = Insertrecord
      adjustment_temp = epr:Adjustment
      If ThisWindow.Request = Insertrecord
          epr:quantity = 1
          Do show_parts
      Else
          Disable(?browse_stock_button)
      End
      Do Adjustment
      If epr:order_number <> ''
          ?title{prop:text} = 'Estimate Part - On Order'
      End
      If epr:pending_ref_number <> ''
          ?title{prop:text} = 'Estimate Part - Pending Order'
      End
      If epr:date_received <> ''
          ?title{prop:text} = 'Estimate Part - Received'
      End
      If epr:Exclude_From_Order = 'YES'
          ?title{prop:text} = 'Estimate Part - Excluded From Order'
      End!If epr:Exclude_From_Order = 'YES'
      
      If Thiswindow.request <> Insertrecord
          Disable(?epr:exclude_from_order)
      End!If THiswindow.request <> Insertrecord
      !Save Fields
      quantity_temp = epr:quantity
      tmp:part_number     = epr:part_number
      tmp:description     = epr:description
      tmp:supplier        = epr:supplier
      tmp:purchase_cost   = epr:purchase_cost
      tmp:sale_cost       = epr:sale_cost
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      !Security Check
      Check_access('JOB PART COSTS - EDIT',x")
      If x" = False
          ?tmp:PurchaseCost{prop:readonly} = 1
          ?tmp:OutWarrantyCost{prop:readonly} = 1
          ?tmp:PurchaseCost{prop:color} = color:silver
          ?tmp:OutWarrantyCost{prop:color} = color:silver
      
          !TB13351 - make these apply to the spinner as well
          ?tmp:OutWarrantyMarkup{prop:Disable} = true
          
      End!"If x" = False
      
      If SecurityCheck('PART ALLOCATED')
          ?epr:PartAllocated{prop:Disable} = 1
      Else !SecurityAccess('PART ALLOCATED')
          ?epr:PartAllocated{prop:Disable} = 0
      End !SecurityAccess('PART ALLOCATED')
      ! Fault Coding (Hopefully)
      fault_codes_required_temp = 'NO'
      required# = 0
      access:chartype.clearkey(cha:charge_type_key)
      cha:charge_type = job:charge_type
      If access:chartype.fetch(cha:charge_type_key) = Level:Benign
          If cha:force_warranty = 'YES'
              required# = 1
          End
      end !if
      ! Inserting (DBH 19/06/2006) #6733 - Quick fix to stop forcing fault codes
      Required# = 0
      ! End (DBH 19/06/2006) #6733
      !Ok, workaround!
      access:manfaupa.clearkey(map:MainFaultKey)
      map:manufacturer = job:manufacturer
      map:MainFault = TRUE
      IF Access:MANFAUPA.Fetch(map:MainFaultKey)
        !No main code, carry on a s normal!
      ELSE
        If sto:Accessory <> 'YES' and man:ForceAccessoryCode
          !Only unhide the main fault!
          unhide_tab = TRUE
        END
      END
      
      found# = 0
      setcursor(cursor:wait)
      save_map_id = access:manfaupa.savefile()
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = job:manufacturer
      set(map:field_number_key,map:field_number_key)
      loop
      
          if access:manfaupa.next()
             break
          end !if
      
          if map:manufacturer <> job:manufacturer      |
              then break.  ! end if
      
          IF unhide_tab = TRUE
            IF map:MainFault = FALSE
              CYCLE
            END
          END
      
          Case map:field_number
              Of 1
                  found# = 1
                  Unhide(?epr:fault_code1:prompt)
                  ?epr:fault_code1:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar)
                      ?popcalendar{prop:xpos} = 212
                      Unhide(?epr:fault_code1:2)
                      ?epr:fault_code1:2{prop:xpos} = 84
                  Else
                      Unhide(?epr:fault_code1)
                      If map:lookup = 'YES'
                          Unhide(?button4)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?epr:fault_code1{prop:req} = 1
                      ?epr:fault_code1:2{prop:req} = 1
                  else
                      ?epr:fault_code1{prop:req} = 0
                      ?epr:fault_code1:2{prop:req} = 0
                  End
              Of 2
                  found# = 1
                  Unhide(?epr:fault_code2:prompt)
                  ?epr:fault_code2:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:2)
                      ?popcalendar:2{prop:xpos} = 212
                      Unhide(?epr:fault_code2:2)
                      ?epr:fault_code2:2{prop:xpos} = 84
                  Else
                      Unhide(?epr:fault_code2)
                      If map:lookup = 'YES'
                          Unhide(?button4:2)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?epr:fault_code2{prop:req} = 1
                      ?epr:fault_code2:2{prop:req} = 1
                  else
                      ?epr:fault_code2{prop:req} = 0
                      ?epr:fault_code2:2{prop:req} = 0
                  End
              Of 3
                  found# = 1
                  Unhide(?epr:fault_code3:prompt)
                  ?epr:fault_code3:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:3)
                      ?popcalendar:3{prop:xpos} = 212
                      Unhide(?epr:fault_code3:2)
                      ?epr:fault_code3:2{prop:xpos} = 84
                  Else
                      Unhide(?epr:fault_code3)
                      If map:lookup = 'YES'
                          Unhide(?button4:3)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?epr:fault_code3{prop:req} = 1
                      ?epr:fault_code3:2{prop:req} = 1
                  else
                      ?epr:fault_code3{prop:req} = 0
                      ?epr:fault_code3:2{prop:req} = 0
                  End
              Of 4
                  found# = 1
                  Unhide(?epr:fault_code4:prompt)
                  ?epr:fault_code4:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:4)
                      ?popcalendar:4{prop:xpos} = 212
                      Unhide(?epr:fault_code4:2)
                      ?epr:fault_code4:2{prop:xpos} = 84
                  Else
                      Unhide(?epr:fault_code4)
                      If map:lookup = 'YES'
                          Unhide(?button4:4)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?epr:fault_code4{prop:req} = 1
                      ?epr:fault_code4:2{prop:req} = 1
                  else
                      ?epr:fault_code4{prop:req} = 0
                      ?epr:fault_code4:2{prop:req} = 0
                  End
              Of 5
                  found# = 1
                  Unhide(?epr:fault_code5:prompt)
                  ?epr:fault_code5:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:5)
                      ?popcalendar:5{prop:xpos} = 212
                      Unhide(?epr:fault_code5:2)
                      ?epr:fault_code5:2{prop:xpos} = 84
                  Else
                      Unhide(?epr:fault_code5)
                      If map:lookup = 'YES'
                          Unhide(?button4:5)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?epr:fault_code5{prop:req} = 1
                      ?epr:fault_code5:2{prop:req} = 1
                  else
                      ?epr:fault_code5{prop:req} = 0
                      ?epr:fault_code5:2{prop:req} = 0
                  End
              Of 6
                  found# = 1
                  Unhide(?epr:fault_code6:prompt)
                  ?epr:fault_code6:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:6)
                      ?popcalendar:6{prop:xpos} = 212
                      Unhide(?epr:fault_code6:2)
                      ?epr:fault_code6:2{prop:xpos} = 84
                  Else
                      Unhide(?epr:fault_code6)
                      If map:lookup = 'YES'
                          Unhide(?button4:6)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?epr:fault_code6{prop:req} = 1
                      ?epr:fault_code6:2{prop:req} = 1
                  else
                      ?epr:fault_code6{prop:req} = 0
                      ?epr:fault_code6:2{prop:req} = 0
                  End
              Of 7
                  found# = 1
                  Unhide(?epr:fault_code7:prompt)
                  ?epr:fault_code7:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:7)
                      ?popcalendar:7{prop:xpos} = 212
                      Unhide(?epr:fault_code7:2)
                      ?epr:fault_code7:2{prop:xpos} = 84
                  Else
                      Unhide(?epr:fault_code7)
                      If map:lookup = 'YES'
                          Unhide(?button4:7)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?epr:fault_code7{prop:req} = 1
                      ?epr:fault_code7:2{prop:req} = 1
                  else
                      ?epr:fault_code7{prop:req} = 0
                      ?epr:fault_code7:2{prop:req} = 0
                  End
      
              Of 8
                  found# = 1
                  Unhide(?epr:fault_code8:prompt)
                  ?epr:fault_code8:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:8)
                      ?popcalendar:8{prop:xpos} = 212
                      Unhide(?epr:fault_code8:2)
                      ?epr:fault_code8:2{prop:xpos} = 84
                  Else
                      Unhide(?epr:fault_code8)
                      If map:lookup = 'YES'
                          Unhide(?button4:8)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?epr:fault_code8{prop:req} = 1
                      ?epr:fault_code8:2{prop:req} = 1
                  else
                      ?epr:fault_code8{prop:req} = 0
                      ?epr:fault_code8:2{prop:req} = 0
                  End
      
              Of 9
                  found# = 1
                  Unhide(?epr:fault_code9:prompt)
                  ?epr:fault_code9:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:9)
                      ?popcalendar:9{prop:xpos} = 212
                      Unhide(?epr:fault_code9:2)
                      ?epr:fault_code9:2{prop:xpos} = 84
                  Else
                      Unhide(?epr:fault_code9)
                      If map:lookup = 'YES'
                          Unhide(?button4:9)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?epr:fault_code9{prop:req} = 1
                      ?epr:fault_code9:2{prop:req} = 1
                  else
                      ?epr:fault_code9{prop:req} = 0
                      ?epr:fault_code9:2{prop:req} = 0
                  End
      
              Of 10
                  found# = 1
                  Unhide(?epr:fault_code10:prompt)
                  ?epr:fault_code10:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:10)
                      ?popcalendar:10{prop:xpos} = 212
                      Unhide(?epr:fault_code10:2)
                      ?epr:fault_code10:2{prop:xpos} = 84
                  Else
                      Unhide(?epr:fault_code10)
                      If map:lookup = 'YES'
                          Unhide(?button4:10)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?epr:fault_code10{prop:req} = 1
                      ?epr:fault_code10:2{prop:req} = 1
                  else
                      ?epr:fault_code10{prop:req} = 0
                      ?epr:fault_code10:2{prop:req} = 0
                  End
      
              Of 11
                  found# = 1
                  Unhide(?epr:fault_code11:prompt)
                  ?epr:fault_code11:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:11)
                      ?popcalendar:11{prop:xpos} = 212
                      Unhide(?epr:fault_code11:2)
                      ?epr:fault_code11:2{prop:xpos} = 84
                  Else
                      Unhide(?epr:fault_code11)
                      If map:lookup = 'YES'
                          Unhide(?button4:11)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?epr:fault_code11{prop:req} = 1
                      ?epr:fault_code11:2{prop:req} = 1
                  else
                      ?epr:fault_code11{prop:req} = 0
                      ?epr:fault_code11:2{prop:req} = 0
                  End
      
              Of 12
                  found# = 1
                  Unhide(?epr:fault_code12:prompt)
                  ?epr:fault_code12:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:12)
                      ?popcalendar:12{prop:xpos} = 212
                      Unhide(?epr:fault_code12:2)
                      ?epr:fault_code12:2{prop:xpos} = 84
                  Else
                      Unhide(?epr:fault_code12)
                      If map:lookup = 'YES'
                          Unhide(?button4:12)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?epr:fault_code12{prop:req} = 1
                      ?epr:fault_code12:2{prop:req} = 1
                  else
                      ?epr:fault_code12{prop:req} = 0
                      ?epr:fault_code12:2{prop:req} = 0
                  End
      
          End !Case map:field_number
      end !loop
      access:manfaupa.restorefile(save_map_id)
      setcursor()
      If found# = 1
          ?FaultCodeTab{prop:Hide} = 0
          ?NoFaultCodeTab{prop:Hide} = 1
      Else !If found# = 1
          ?FaultCodeTab{prop:Hide} = 1
          ?NoFaultCodeTab{prop:Hide} = 0
      End !If found# = 1
      !ThisMakeOver.SetWindow(Win:Form)
      !Popup Stock Button
      Do LookupMainFault
      If epr:Part_Number = ''
          Post(Event:Accepted,?Browse_Stock_Button)
      End !wpr:Part_Number = ''
      Do ShowCosts
      Do EnableDisableCosts
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Rapid_Fault_Description PROCEDURE                     !Generated from procedure template - Window

FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Fault Description'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Fault Description'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Fault Description'),AT(240,151),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(208,162),USE(?Lookup_Engineers_Notes),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           TEXT,AT(240,162,200,84),USE(jbn:Fault_Description),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT,REQ
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting'
  OF ChangeRecord
    ActionMessage = 'Changing Fault Description'
  END
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020458'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Rapid_Fault_Description')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddUpdateFile(Access:JOBNOTES)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBNOTES.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBNOTES
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Rapid_Fault_Description')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBNOTES.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Rapid_Fault_Description')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Lookup_Engineers_Notes
      glo:select1 = job:manufacturer
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020458'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020458'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020458'&'0')
      ***
    OF ?Lookup_Engineers_Notes
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Fault_Description_Text
      ThisWindow.Reset
      If Records(glo:Q_Notes) <> ''
          Loop x$ = 1 To Records(glo:Q_Notes)
              Get(glo:Q_Notes,x$)
              If jbn:fault_description = ''
                  jbn:fault_description = Clip(glo:notes_pointer)
              Else !If job:engineers_notes = ''
                  jbn:fault_description = Clip(jbn:fault_description) & '. ' & Clip(glo:notes_pointer)
              End !If job:engineers_notes = ''
          End
          Display()
      End
      Clear(glo:Q_Notes)
      Free(glo:Q_Notes)
      glo:select1 = ''
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
AllocateEngineerToJob PROCEDURE                       !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Engineer_Temp        STRING(3)
Engineer_Name_Temp   STRING(30)
engineer_code_temp   STRING(3)
window               WINDOW('Allocate Engineer'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Allocate Engineer'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Select Engineer'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(248,208,148,10),USE(Engineer_Name_Temp),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(404,204),USE(?Engineer_Lookup),TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(300,258),USE(?Ok),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(368,258),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

AddEngineer     Routine
    Do_Status# = 0

    If job:Engineer <> Engineer_Temp

        IF job:Date_Completed <> ''
            Error# = 1
            Case Missive('Cannot change! This job has been completed.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End !IF job:Date_Completed <> ''
        If job:Invoice_Number <> ''
            Error# = 1
            Case Missive('Cannot change! This job has been invoiced.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End !If job:Invoice_Number <> ''
        If Error# = 0
            If Engineer_Temp = ''
                IF (AddToAudit(job:Ref_Number,'JOB','ENGINEER ALLOCATED: ' & CLip(job:engineer),Clip(use:Forename) & ' ' & Clip(use:Surname) & ' - ' & Clip(use:Location) & ' - LEVEL ' & Clip(use:SkillLevel)))
                END ! IF

                do_status# = 1
                engineer_temp = job:engineer

                If Access:JOBSENG.PrimeRecord() = Level:Benign
                    joe:JobNumber     = job:Ref_Number
                    joe:UserCode      = job:Engineer
                    joe:DateAllocated = Today()
                    joe:TimeAllocated = Clock()
                    joe:AllocatedBy   = use:User_Code
                    access:users.clearkey(use:User_Code_Key)
                    use:User_Code   = job:Engineer
                    access:users.fetch(use:User_Code_Key)

                    joe:EngSkillLevel = use:SkillLevel
                    joe:JobSkillLevel = use:SkillLevel

                    If Access:JOBSENG.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:JOBSENG.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:JOBSENG.TryInsert() = Level:Benign
                End !If Access:JOBSENG.PrimeRecord() = Level:Benign

            Else !If Engineer_Temp = ''
                Case Missive('Are you sure you want to change the Engineer?','ServiceBase 3g',|
                               'mquest.jpg','\No|/Yes')
                    Of 2 ! Yes Button

                        IF (AddToAudit(job:Ref_Number,'JOB','ENGINEER CHANGED TO ' & CLip(job:engineer),'NEW ENGINEER: ' & CLip(job:Engineer) & |
                                                '<13,10>SKILL LEVEL: ' & use:SkillLevel & |
                                                '<13,10,13,10>PREVIOUS ENGINEER: ' & CLip(engineer_temp)))
                        END ! IF

                        Engineer_Temp   = job:Engineer

                        If Access:JOBSENG.PrimeRecord() = Level:Benign
                            joe:JobNumber     = job:Ref_Number
                            joe:UserCode      = job:Engineer
                            joe:DateAllocated = Today()
                            joe:TimeAllocated = Clock()
                            joe:AllocatedBy   = use:User_Code
                            access:users.clearkey(use:User_Code_Key)
                            use:User_Code   = job:Engineer
                            access:users.fetch(use:User_Code_Key)

                            joe:EngSkillLevel = use:SkillLevel
                            joe:JobSkillLevel = jobe:SkillLevel

                            If Access:JOBSENG.TryInsert() = Level:Benign
                                !Insert Successful
                            Else !If Access:JOBSENG.TryInsert() = Level:Benign
                                !Insert Failed
                            End !If Access:JOBSENG.TryInsert() = Level:Benign
                        End !If Access:JOBSENG.PrimeRecord() = Level:Benign

                    Of 1 ! No Button
                        job:Engineer    = Engineer_Temp
                End ! Case Missive
            End !If Engineer_Temp = ''
        Else !If Error# = 0
            job:Engineer    = engineer_temp
        End !If Error# = 0

    End !If job:Engineer <> Engineer_Temp

    If do_status# = 1
        GetStatus(310,thiswindow.request,'JOB') !allocated to engineer
        !Do time_remaining                      ! Turnaround time skipped
    End!If do_status# = 1

    Access:Jobs.TryUpdate()                     ! Added because of Joe's requirement...
show_engineer       Routine
    access:users.clearkey(use:user_code_key)
    use:user_code = engineer_code_temp
    if access:users.fetch(use:user_code_key) = Level:Benign
        engineer_name_temp = Clip(use:forename) & ' ' & Clip(use:surname)
    Else!if access:users.fetch(use:user_code_key) = Level:Benign
        engineer_name_temp = ''
    end!!if access:users.fetch(use:user_code_key) = Level:Benign
    Display(?engineer_name_temp)

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020445'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('AllocateEngineerToJob')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBS.Open
  Access:USERS.UseFile
  Access:TRADEACC.UseFile
  Access:LOCATLOG.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Engineer_Temp = job:Engineer
  
  engineer_code_temp = job:Engineer
  
  do show_engineer
      ! Save Window Name
   AddToLog('Window','Open','AllocateEngineerToJob')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','AllocateEngineerToJob')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Engineer_Lookup
      Access:Users.ClearKey(use:password_key)
      use:password = glo:password
      if not Access:Users.Fetch(use:password_key)
          GLO:Select1 = use:Location          ! Only show engineers with same site location as the current user
      end
      
      GlobalRequest   = SelectRecord
      PickLocationEngineers
      glo:Select1 = ''
      
      case globalresponse
          of requestcompleted
              engineer_code_temp = use:user_code
          of requestcancelled
              engineer_code_temp = ''
      end!case globalreponse
      Do show_engineer
    OF ?Ok
      ! Allocate Engineer
      
      job:Engineer = engineer_code_temp
      
      if job:Engineer = ''            ! Field validation
          select(?Engineer_Name_Temp)
          cycle
      else
          Access:Users.Clearkey(use:User_Code_Key)
          use:User_Code = job:Engineer
          if Access:Users.Fetch(use:User_Code_Key)
              select(?Engineer_Name_Temp)
              cycle
          end
      end
      
      !If the job is at the ARC, check that it has been
      !through Waybill confirmation - L921 (DBH: 13-08-2003)
      Error# = 0
      
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
      
      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      
      
      If use:Location = tra:SiteLocation
          Access:LOCATLOG.ClearKey(lot:NewLocationKey)
          lot:RefNumber   = job:Ref_Number
          lot:NewLocation = Clip(GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI'))
          If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
              !Found
          Else !If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
              !Error
              Case Missive('Error! This job is not at the ARC Location. Please ensure that the Waybill Confirmation procedure has been completed.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Error# = 1
          End !If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
      End !If use:Location = tmp:ARCSiteLocation
      If Error# = 0
          Do AddEngineer
      End !If Error# = 0
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020445'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020445'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020445'&'0')
      ***
    OF ?Ok
      ThisWindow.Update
       POST(Event:CloseWindow)
    OF ?Close
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Print_Routines PROCEDURE                              !Generated from procedure template - Window

tmp:ReportType       BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Print Routines'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Select Report'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB,USE(?Tab1)
                           OPTION,AT(266,192,148,36),USE(tmp:ReportType),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Job Receipt'),AT(314,192),USE(?tmp:ReportType:Radio3),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Job Label'),AT(314,218),USE(?tmp:ReportType:Radio1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('3')
                             RADIO('Job Card'),AT(314,205),USE(?tmp:ReportType:Radio2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                           END
                         END
                       END
                       BUTTON,AT(300,258),USE(?Print),TRN,FLAT,LEFT,ICON('printp.jpg')
                       BUTTON,AT(368,258),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020456'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Routines')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:JOBS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Print_Routines')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Print_Routines')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020456'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020456'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020456'&'0')
      ***
    OF ?Print
      ThisWindow.Update
      Case tmp:ReportType
          Of 1
              Job_Receipt
          Of 2
              Job_Card
          Of 3
              Set(defaults)
              access:defaults.next()
              access:jobs.clearkey(job:Ref_number_key)
              job:ref_number  = glo:Select1
              access:jobs.tryfetch(job:Ref_number_key)
              Case def:label_printer_type
                  Of 'TEC B-440 / B-442'
                      If job:bouncer = 'B'
                          Thermal_Labels('SE')
                      Else!If job:bouncer = 'B'
                          Thermal_Labels('')
                      End!If job:bouncer = 'B'
      
                  Of 'TEC B-452'
                      Thermal_Labels_B452
              End!Case def:label_printer_type
      End!Case tmp:ReportType
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Browse_Warranty_Charge_Types PROCEDURE                ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
!temp
Nominate_Engineer PROCEDURE                           !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Engineer_Temp        STRING(3)
Engineer_Name_Temp   STRING(30)
engineer_code_temp   STRING(3)
window               WINDOW('Allocate Engineer'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Allocate QA Engineer'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Please select an Engineer to QA this unit'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Engineer'),AT(248,206),USE(?Engineer_Name_Temp:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(288,206,112,10),USE(Engineer_Name_Temp),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(404,202),USE(?Engineer_Lookup),TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(300,258),USE(?Ok),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(368,258),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

AddEngineer     Routine
    Do_Status# = 0

    If job:Engineer <> Engineer_Temp
        IF job:Date_Completed <> ''
            Error# = 1
            Case Missive('Cannot change! This job has been completed.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End !IF job:Date_Completed <> ''
        If job:Invoice_Number <> ''
            Error# = 1
            Case Missive('Cannot change! This job has been invoiceed.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End !If job:Invoice_Number <> ''
        If Error# = 0
            If Engineer_Temp = ''

                IF (AddToAudit(job:Ref_Number,'JOB','ENGINEER ALLOCATED: ' & CLip(job:engineer),Clip(use:Forename) & ' ' & Clip(use:Surname) & ' - ' & Clip(use:Location) & ' - LEVEL ' & Clip(use:SkillLevel)))
                END ! IF

                do_status# = 1
                engineer_temp = job:engineer

                If Access:JOBSENG.PrimeRecord() = Level:Benign
                    joe:JobNumber     = job:Ref_Number
                    joe:UserCode      = job:Engineer
                    joe:DateAllocated = Today()
                    joe:TimeAllocated = Clock()
                    joe:AllocatedBy   = use:User_Code
                    access:users.clearkey(use:User_Code_Key)
                    use:User_Code   = job:Engineer
                    access:users.fetch(use:User_Code_Key)

                    joe:EngSkillLevel = use:SkillLevel
                    joe:JobSkillLevel = use:SkillLevel

                    If Access:JOBSENG.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:JOBSENG.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:JOBSENG.TryInsert() = Level:Benign
                End !If Access:JOBSENG.PrimeRecord() = Level:Benign

            Else !If Engineer_Temp = ''
                !Case MessageEx('Are you sure you want to change the Engineer?','ServiceBase 2000',|
                   !            'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
                   ! Of 1 ! &Yes Button
                        IF (AddToAudit(job:Ref_Number,'JOB','ENGINEER CHANGED TO ' & CLip(job:engineer),'NEW ENGINEER: ' & CLip(job:Engineer) & |
                                                '<13,10>SKILL LEVEL: ' & use:SkillLevel & |
                                                '<13,10,13,10>PREVIOUS ENGINEER: ' & CLip(engineer_temp)))
                        END ! IF
                        Engineer_Temp   = job:Engineer

                        If Access:JOBSENG.PrimeRecord() = Level:Benign
                            joe:JobNumber     = job:Ref_Number
                            joe:UserCode      = job:Engineer
                            joe:DateAllocated = Today()
                            joe:TimeAllocated = Clock()
                            joe:AllocatedBy   = use:User_Code
                            access:users.clearkey(use:User_Code_Key)
                            use:User_Code   = job:Engineer
                            access:users.fetch(use:User_Code_Key)

                            joe:EngSkillLevel = use:SkillLevel
                            joe:JobSkillLevel = jobe:SkillLevel

                            If Access:JOBSENG.TryInsert() = Level:Benign
                                !Insert Successful
                            Else !If Access:JOBSENG.TryInsert() = Level:Benign
                                !Insert Failed
                            End !If Access:JOBSENG.TryInsert() = Level:Benign
                        End !If Access:JOBSENG.PrimeRecord() = Level:Benign

                   ! Of 2 ! &No Button
                    !    job:Engineer    = Engineer_Temp
                !End!Case MessageEx
            End !If Engineer_Temp = ''
        Else !If Error# = 0
            job:Engineer    = engineer_temp
        End !If Error# = 0

    End !If job:Engineer <> Engineer_Temp

    If do_status# = 1
        GetStatus(310,thiswindow.request,'JOB') !allocated to engineer
        !Do time_remaining                      ! Turnaround time skipped
    End!If do_status# = 1

    Access:Jobs.TryUpdate()                     ! Added because of Joe's requirement...
show_engineer       Routine
    access:users.clearkey(use:user_code_key)
    use:user_code = engineer_code_temp
    if access:users.fetch(use:user_code_key) = Level:Benign
        engineer_name_temp = Clip(use:forename) & ' ' & Clip(use:surname)
    Else!if access:users.fetch(use:user_code_key) = Level:Benign
        engineer_name_temp = ''
    end!!if access:users.fetch(use:user_code_key) = Level:Benign
    Display(?engineer_name_temp)

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020453'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Nominate_Engineer')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBS.Open
  Access:USERS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Engineer_Temp = job:Engineer
  
  engineer_code_temp = job:Engineer
  
  do show_engineer
      ! Save Window Name
   AddToLog('Window','Open','Nominate_Engineer')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Nominate_Engineer')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Engineer_Lookup
      Access:Users.ClearKey(use:password_key)
      use:password = glo:password
      if not Access:Users.Fetch(use:password_key)
          GLO:Select1 = use:Location          ! Only show engineers with same site location as the current user
      end
      
      GlobalRequest   = SelectRecord
      PickLocationEngineers
      glo:Select1 = ''
      
      case globalresponse
          of requestcompleted
              engineer_code_temp = use:user_code
          of requestcancelled
              engineer_code_temp = ''
      end!case globalreponse
      Do show_engineer
    OF ?Ok
      ! Allocate Engineer
      
      job:Engineer = engineer_code_temp
      
      if job:Engineer = ''            ! Field validation
          select(?Engineer_Name_Temp)
          cycle
      else
          Access:Users.Clearkey(use:User_Code_Key)
          use:User_Code = job:Engineer
          if Access:Users.Fetch(use:User_Code_Key)
              select(?Engineer_Name_Temp)
              cycle
          end
      end
      
      Do AddEngineer
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020453'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020453'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020453'&'0')
      ***
    OF ?Ok
      ThisWindow.Update
       POST(Event:CloseWindow)
    OF ?Close
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ViewCosts PROCEDURE                                   !Generated from procedure template - Window

save_aud_id          USHORT,AUTO
save_jobs_id         USHORT,AUTO
save_jobse_id        USHORT,AUTO
save_jobse2_id       USHORT,AUTO
PricingGroup         GROUP,PRE(tmppri)
LabourCost           STRING(30)
PartsCost            STRING(30)
Pass                 STRING(30)
ClaimCost            STRING(30)
HandlingFee          STRING(30)
ExchangeCost         STRING(30)
RRCLabourCost        STRING(30)
RRCPartsCost         STRING(30)
                     END
TotalGroup           GROUP,PRE(tmptot)
VAT                  STRING(30)
Total                STRING(30)
Balance              STRING(30)
                     END
save_wpr_id          USHORT,AUTO
save_epr_id          USHORT,AUTO
save_jpt_id          USHORT,AUTO
save_par_id          USHORT,AUTO
RRCChargeable        GROUP,PRE(tmprc)
Vat                  REAL
Total                REAL
Paid                 REAL
Outstanding          REAL
                     END
ARCChargeable        GROUP,PRE(tmpac)
Vat                  REAL
Total                REAL
Paid                 REAL
Outstanding          REAL
                     END
ARCWarranty          GROUP,PRE(tmpaw)
Vat                  REAL
Total                REAL
                     END
ARCClaim             GROUP,PRE(tmpcl)
SubTotal             REAL
Vat                  REAL
Total                REAL
                     END
Estimate             GROUP,PRE(tmpes)
VAT                  REAL
Total                REAL
                     END
RRCWarranty          GROUP,PRE(tmprw)
VAT                  REAL
Total                REAL
                     END
tmp:RRCHandlingFee   REAL
tmp:RRCExchangeFee   REAL
tmp:3rdPartyCost     REAL
tmp:RenameCourierCost BYTE(0)
tmp:CourierCostName  STRING(30)
ARCChargeableInvoice GROUP,PRE(tmpiac)
VAT                  REAL
Total                REAL
Paid                 REAL
Outstanding          REAL
                     END
ARCWarrantyInvoice   GROUP,PRE(tmpiaw)
VAT                  REAL
Total                REAL
                     END
ARCClaimInvoice      GROUP,PRE(tmpial)
SubTotal             REAL
VAT                  REAL
Total                REAL
                     END
RRCChargeableInvoice GROUP,PRE(tmpirc)
Vat                  REAL
Total                REAL
Paid                 REAL
Outstanding          REAL
                     END
RRCWarrantyInvoice   GROUP,PRE(tmpirw)
Vat                  REAL
Total                REAL
                     END
RRCEstimate          GROUP,PRE(tmpres)
vat                  REAL
total                REAL
                     END
ClaimAdjustment      GROUP,PRE(tmpcla)
SubTotal             REAL
Vat                  REAL
Total                REAL
                     END
ShowFields           GROUP,PRE(show)
ARCChargeableTab     BYTE(0)
RRCChargeableTab     BYTE(0)
ARCWarrantyTab       BYTE(0)
ARCClaimTab          BYTE(0)
RRCHandlingTab       BYTE(0)
RRCExchangedTab      BYTE(0)
RRCWarrantyTab       BYTE(0)
                     END
tmp:3rdPartyTotal    REAL
tmp:3rdPartyCharge   REAL
tmp:3rdPartyVAT      REAL
tmp:TotalPaid        REAL
tmp:HandlingFee      REAL
tmp:ExchangeFee      REAL
save:IgnoreARCC      STRING(3)
save:IgnoreRRCC      STRING(3)
save:IgnoreARCW      STRING(3)
save:IgnoreRRCW      STRING(3)
save:IgnoreARCE      STRING(3)
save:IgnoreRRCE      STRING(3)
save:IgnoreClaim     STRING(3)
save:Ignore3rdParty  STRING(3)
tmp:ARCLocation      STRING(30)
tmp:Close            BYTE(0)
TempQueue            QUEUE,PRE(tmpque)
Field                STRING(255)
Reason               STRING(255)
SaveCost             BYTE(0)
Cost                 REAL
                     END
tmp:CreditType       STRING(7)
tmp:CreditNumber     STRING(30)
tmp:CreditAmount     REAL
tmp:CreditTotal      REAL
LocalDefaultLabour   REAL
LocalDiscountAmount  REAL
BRW9::View:Browse    VIEW(JOBSINV)
                       PROJECT(jov:DateCreated)
                       PROJECT(jov:RecordNumber)
                       PROJECT(jov:RefNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:CreditNumber       LIKE(tmp:CreditNumber)         !List box control field - type derived from local data
tmp:CreditNumber_NormalFG LONG                        !Normal forground color
tmp:CreditNumber_NormalBG LONG                        !Normal background color
tmp:CreditNumber_SelectedFG LONG                      !Selected forground color
tmp:CreditNumber_SelectedBG LONG                      !Selected background color
tmp:CreditType         LIKE(tmp:CreditType)           !List box control field - type derived from local data
tmp:CreditType_NormalFG LONG                          !Normal forground color
tmp:CreditType_NormalBG LONG                          !Normal background color
tmp:CreditType_SelectedFG LONG                        !Selected forground color
tmp:CreditType_SelectedBG LONG                        !Selected background color
jov:DateCreated        LIKE(jov:DateCreated)          !List box control field - type derived from field
jov:DateCreated_NormalFG LONG                         !Normal forground color
jov:DateCreated_NormalBG LONG                         !Normal background color
jov:DateCreated_SelectedFG LONG                       !Selected forground color
jov:DateCreated_SelectedBG LONG                       !Selected background color
tmp:CreditAmount       LIKE(tmp:CreditAmount)         !List box control field - type derived from local data
tmp:CreditAmount_NormalFG LONG                        !Normal forground color
tmp:CreditAmount_NormalBG LONG                        !Normal background color
tmp:CreditAmount_SelectedFG LONG                      !Selected forground color
tmp:CreditAmount_SelectedBG LONG                      !Selected background color
jov:RecordNumber       LIKE(jov:RecordNumber)         !Primary key field - type derived from field
jov:RefNumber          LIKE(jov:RefNumber)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('View Costs'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('View Costs'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,82,352,16),USE(?Panel1),FILL(09A6A7CH)
                       PROMPT('A.R.C. Costs'),AT(168,86),USE(?ARCTitle),TRN,FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                       PROMPT('R.R.C. Costs'),AT(340,86),USE(?RRCTitle),TRN,FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                       SHEET,AT(164,100,172,230),USE(?Sheet1),ABOVE(10),FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(0D6E7EFH),SPREAD
                         TAB('C'),USE(?ARCChargeableTab),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Chargeable'),AT(168,116),USE(?Prompt27),FONT(,9,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           CHECK('Ignore Default Charges'),AT(232,118),USE(job:Ignore_Chargeable_Charges),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Ignore Default Charges'),TIP('Ignore Default Charges'),VALUE('YES','NO')
                           PROMPT('Courier Cost'),AT(168,132),USE(?job:Courier_Cost:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(264,132,48,10),USE(job:Courier_Cost),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Courier Cost'),TIP('Courier Cost'),UPR
                           PROMPT('Labour Cost'),AT(168,148),USE(?Prompt7),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LINE,AT(248,180,67,0),USE(?Line1),COLOR(COLOR:Black)
                           PROMPT('Parts Cost'),AT(168,164),USE(?Prompt7:2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(264,163,48,10),USE(job:Parts_Cost),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           ENTRY(@n14.2),AT(264,185,48,10),USE(job:Sub_Total),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           LINE,AT(247,228,67,0),USE(?Line1:14),COLOR(COLOR:Black)
                           ENTRY(@n14.2),AT(264,196,48,10),USE(tmpac:Vat),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           ENTRY(@n14.2),AT(264,212,48,10),USE(tmpac:Total),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           PROMPT('Paid'),AT(168,233),USE(?tmpac:Paid:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(264,233,48,10),USE(tmpac:Paid),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Balance'),TIP('Balance'),UPR,READONLY
                           PROMPT('Outstanding'),AT(168,244),USE(?Outstanding:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14.2),AT(264,244,48,10),USE(tmpac:Outstanding),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Outstanding'),TIP('Outstanding'),UPR,READONLY
                           PROMPT('Sub Total'),AT(168,185),USE(?Prompt7:3),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('V.A.T.'),AT(168,196),USE(?Prompt7:4),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Total Cost'),AT(168,212),USE(?Prompt7:5),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(264,148,48,10),USE(job:Labour_Cost),SKIP,TRN,RIGHT,FONT(,8,,FONT:bold),COLOR(09A6A7CH),READONLY
                         END
                         TAB('W'),USE(?ARCWarrantyTab),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Warranty'),AT(168,116,43,10),USE(?Prompt27:3),FONT(,9,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           CHECK('Ignore Default Charges'),AT(232,119),USE(job:Ignore_Warranty_Charges),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Ignore Default Charges'),TIP('Ignore Default Charges'),VALUE('YES','NO')
                           PROMPT('Exchange Fee'),AT(168,132),USE(?job:Courier_Cost_Warranty:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(264,132,48,10),USE(job:Courier_Cost_Warranty),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Courier Cost'),TIP('Courier Cost'),UPR,READONLY
                           PROMPT('Labour Cost'),AT(168,148),USE(?Prompt7:11),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Parts Cost'),AT(168,164),USE(?Prompt7:12),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LINE,AT(243,180,67,0),USE(?Line1:3),COLOR(COLOR:Black)
                           PROMPT('Sub Total'),AT(168,185),USE(?Prompt7:13),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('V.A.T.'),AT(168,196),USE(?Prompt7:14),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Total Cost'),AT(168,212),USE(?Prompt7:15),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(264,212,48,10),USE(tmpaw:Total),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           ENTRY(@n14.2),AT(264,196,48,10),USE(tmpaw:Vat),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           ENTRY(@n14.2),AT(264,185,48,10),USE(job:Sub_Total_Warranty),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           ENTRY(@n14.2),AT(264,164,48,10),USE(job:Parts_Cost_Warranty),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           ENTRY(@n14.2),AT(264,148,48,10),USE(job:Labour_Cost_Warranty),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                         END
                         TAB('CL'),USE(?ARCClaimTab),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Claim'),AT(168,116),USE(?Prompt27:4),FONT(,9,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           CHECK('Ignore Default Charges'),AT(232,119),USE(jobe:IgnoreClaimCosts),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Ignore Default Charges'),TIP('Ignore Default Charges'),VALUE('1','0')
                           PROMPT('Exchange Fee'),AT(168,132),USE(?job:Courier_Cost_Warranty:Prompt:2),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(264,132,48,10),USE(job:Courier_Cost_Warranty,,?job:Courier_Cost_Warranty:2),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Courier Cost'),TIP('Courier Cost'),UPR,READONLY
                           PROMPT('Labour Cost'),AT(168,148),USE(?Prompt7:16),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Parts Cost'),AT(168,164),USE(?Prompt7:17),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(264,148,48,10),USE(jobe:ClaimValue),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY,MSG('ClaimValue')
                           LINE,AT(244,177,67,0),USE(?Line1:4),COLOR(COLOR:Black)
                           PROMPT('Sub Total'),AT(168,180),USE(?Prompt7:18),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('V.A.T.'),AT(168,193),USE(?Prompt7:19),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Total Cost'),AT(168,233),USE(?Prompt7:20),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(264,233,48,10),USE(tmpcl:Total),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           ENTRY(@n14.2),AT(264,193,48,10),USE(tmpcl:Vat),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           PROMPT('Third Party Handling Fee'),AT(168,210),USE(?jobe2:ThirdPartyHandlingFee:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n10.2),AT(264,210,48,10),USE(jobe2:ThirdPartyHandlingFee),SKIP,TRN,HIDE,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           ENTRY(@n14.2),AT(264,180,48,10),USE(tmpcl:SubTotal),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           ENTRY(@n14.2),AT(264,164,48,10),USE(jobe:ClaimPartsCost),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY,MSG('Claim Parts Cost')
                         END
                         TAB('3'),USE(?ARC3rdPartyTab),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('3rd Party'),AT(168,116),USE(?Prompt27:5),FONT(,9,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           CHECK('Ignore Default Charges'),AT(228,118),USE(jobe:Ignore3rdPartyCosts),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Ignore 3rd Party Costs'),TIP('Ignore 3rd Party Costs'),VALUE('1','0')
                           PROMPT('Cost'),AT(168,132),USE(?tmprc:CourierCost:Prompt:2),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(264,132,48,10),USE(jobe:ARC3rdPartyCost),SKIP,TRN,RIGHT,FONT(,8,,FONT:bold),COLOR(09A6A7CH),READONLY,MSG('3rd Party Cost')
                           PROMPT('Markup'),AT(168,228),USE(?jobe:ARC3rdPartyMarkup:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(264,228,48,10),USE(jobe:ARC3rdPartyMarkup),TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('3rd Party Markup'),TIP('3rd Party Markup'),UPR
                           PROMPT('V.A.T.'),AT(168,185),USE(?jobe:ARC3rdPartyVAT:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(264,185,48,10),USE(tmp:3rdPartyVAT),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('3rd Party Vat'),TIP('3rd Party Vat'),UPR,READONLY
                           PROMPT('Total'),AT(168,201),USE(?tmp:3rdPartyTotal:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(264,201,48,10),USE(tmp:3rdPartyTotal),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('3rd Party Total'),TIP('3rd Party Total'),UPR,READONLY
                           STRING(''),AT(168,257,168,12),USE(?3rdPartyInvoiceNumber),FONT(,,,FONT:bold)
                           LINE,AT(245,180,67,0),USE(?Line1:13),COLOR(COLOR:Black)
                         END
                         TAB,USE(?ARCBlankTab),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('E'),USE(?EstimateTab),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Ignore Default Charges'),AT(232,118),USE(job:Ignore_Estimate_Charges),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Ignore Default Charges'),TIP('Ignore Default Charges'),VALUE('YES','NO')
                           PROMPT('Courier Cost'),AT(168,132),USE(?job:Courier_Cost:Prompt:2),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(264,132,48,10),USE(job:Courier_Cost_Estimate),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Courier Cost'),TIP('Courier Cost'),UPR
                           PROMPT('Estimate'),AT(168,116),USE(?Prompt27:2),FONT(,9,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Labour Cost'),AT(168,148),USE(?Prompt7:6),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(264,148,48,10),USE(job:Labour_Cost_Estimate),SKIP,TRN,RIGHT,FONT(,8,,FONT:bold),COLOR(09A6A7CH),READONLY
                           PROMPT('Parts Cost'),AT(168,164),USE(?Prompt7:7),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LINE,AT(249,179,67,0),USE(?Line1:2),COLOR(COLOR:Black)
                           PROMPT('Sub Total'),AT(168,185),USE(?Prompt7:8),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('V.A.T.'),AT(168,196),USE(?Prompt7:9),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Total Cost'),AT(168,212),USE(?Prompt7:10),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(264,212,48,10),USE(tmpes:Total),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           ENTRY(@n14.2),AT(264,196,48,10),USE(tmpes:VAT),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           ENTRY(@n14.2),AT(264,185,48,10),USE(job:Sub_Total_Estimate),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           ENTRY(@n14.2),AT(264,164,48,10),USE(job:Parts_Cost_Estimate),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                         END
                         TAB('IC'),USE(?ARCCInvoiceTab),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Chargeable - Invoiced'),AT(168,116),USE(?ARCInvoiced),FONT(,9,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           CHECK('Ignore Default Charges'),AT(232,129),USE(job:Ignore_Chargeable_Charges,,?job:Ignore_Chargeable_Charges:2),DISABLE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Ignore Default Charges'),TIP('Ignore Default Charges'),VALUE('YES','NO')
                           PROMPT('Inv No'),AT(168,257,164,12),USE(?ARCInvoiceNumber),LEFT,FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Courier Cost'),AT(168,140),USE(?tmprc:CourierCost:Prompt:3),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(268,140,48,10),USE(job:Invoice_Courier_Cost),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Courier Cost'),TIP('Courier Cost'),UPR,READONLY
                           PROMPT('Labour Cost'),AT(168,153),USE(?Prompt7:36),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(268,153,48,10),USE(job:Invoice_Labour_Cost),SKIP,TRN,RIGHT,FONT(,8,,FONT:bold),COLOR(09A6A7CH),READONLY
                           PROMPT('Parts Cost'),AT(168,164),USE(?Prompt7:37),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(268,164,48,10),USE(job:Invoice_Parts_Cost),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           LINE,AT(252,180,67,0),USE(?Line1:8),COLOR(COLOR:Black)
                           PROMPT('Sub Total'),AT(168,185),USE(?Prompt7:38),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LINE,AT(253,227,67,0),USE(?Line1:16),COLOR(COLOR:Black)
                           ENTRY(@n14.2),AT(268,185,48,10),USE(job:Invoice_Sub_Total),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           PROMPT('V.A.T.'),AT(168,196),USE(?Prompt7:39),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(268,196,48,10),USE(tmpiac:VAT),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           PROMPT('Total Cost'),AT(168,212),USE(?Prompt7:40),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(268,212,48,10),USE(tmpiac:Total),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           PROMPT('Paid'),AT(168,233),USE(?Paid:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(268,233,48,10),USE(tmpiac:Paid),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Paid'),TIP('Paid'),UPR,READONLY
                           PROMPT('Outstanding'),AT(168,244),USE(?Outstanding:Prompt:3),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14.2),AT(268,244,48,10),USE(tmpiac:Outstanding),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Outstanding'),TIP('Outstanding'),UPR,READONLY
                         END
                         TAB('IW'),USE(?ARCWInvoiceTab),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Warranty - Invoiced'),AT(168,116,124,12),USE(?Prompt27:12),FONT(,9,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           CHECK('Ignore Default Charges'),AT(232,129),USE(job:Ignore_Warranty_Charges,,?job:Ignore_Warranty_Charges:2),DISABLE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Ignore Default Charges'),TIP('Ignore Default Charges'),VALUE('YES','NO')
                           PROMPT('Inv No'),AT(168,257,160,12),USE(?ARCWarrantyInvoiceNumber),LEFT,FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Exchange Fee'),AT(168,140),USE(?job:Courier_Cost_Warranty:Prompt:4),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(264,140,48,10),USE(job:WInvoice_Courier_Cost),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Courier Cost'),TIP('Courier Cost'),UPR,READONLY
                           PROMPT('Labour Cost'),AT(168,153),USE(?Prompt7:41),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(264,153,48,10),USE(job:WInvoice_Labour_Cost),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           PROMPT('Parts Cost'),AT(168,164),USE(?Prompt7:42),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(264,164,48,10),USE(job:WInvoice_Parts_Cost),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           LINE,AT(243,180,67,0),USE(?Line1:9),COLOR(COLOR:Black)
                           PROMPT('Sub Total'),AT(168,185),USE(?Prompt7:43),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(264,185,48,10),USE(job:WInvoice_Sub_Total),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           PROMPT('V.A.T.'),AT(168,196),USE(?Prompt7:44),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(264,196,48,10),USE(tmpaw:Vat,,?tmpaw:Vat:2),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           PROMPT('Total Cost'),AT(168,212),USE(?Prompt7:45),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(264,212,48,10),USE(tmpaw:Total,,?tmpaw:Total:2),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                         END
                         TAB('IL'),USE(?ARCCLInvoiceTab),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Claim - Invoiced'),AT(168,116,68,12),USE(?Prompt27:13),FONT(,9,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Adjustment'),AT(284,116),USE(?Prompt96),FONT(,,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           CHECK('Ignore Default Charges'),AT(232,129),USE(jobe:IgnoreClaimCosts,,?jobe:IgnoreClaimCosts:2),DISABLE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Ignore Default Charges'),TIP('Ignore Default Charges'),VALUE('1','0')
                           PROMPT('Exchange Fee'),AT(168,140),USE(?job:Courier_Cost_Warranty:Prompt:5),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(220,140,48,10),USE(job:WInvoice_Courier_Cost,,?job:WInvoice_Courier_Cost:2),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Courier Cost'),TIP('Courier Cost'),UPR,READONLY
                           PROMPT('Labour Cost'),AT(168,153),USE(?Prompt7:46),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(220,153,48,10),USE(jobe:InvoiceClaimValue),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY,MSG('Invoice Claim Value')
                           ENTRY(@n14.2),AT(280,153,48,10),USE(jobe:LabourAdjustment),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Labour Adjustment'),TIP('Labour Adjustment'),UPR,READONLY
                           PROMPT('Parts Cost'),AT(168,164),USE(?Prompt7:47),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(220,164,48,10),USE(jobe:InvClaimPartsCost),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY,MSG('Invoice Parts Claim Cost')
                           ENTRY(@n14.2),AT(280,140,48,10),USE(jobe:ExchangeAdjustment),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY,MSG('Exchange Adjustment')
                           LINE,AT(260,177,67,0),USE(?Line1:10),COLOR(COLOR:Black)
                           ENTRY(@n14.2),AT(280,164,48,10),USE(jobe:PartsAdjustment),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY,MSG('Parts Adjustment')
                           PROMPT('Sub Total'),AT(168,180),USE(?Prompt7:48),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(220,180,48,10),USE(tmpial:SubTotal),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           PROMPT('V.A.T.'),AT(168,193),USE(?Prompt7:49),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(220,193,48,10),USE(tmpial:VAT),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           ENTRY(@n14.2),AT(280,180,48,10),USE(tmpcla:SubTotal),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           ENTRY(@n14.2),AT(280,193,48,10),USE(tmpcla:Vat),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           PROMPT('Third Party Handling Fee'),AT(168,214,52,16),USE(?jobe2:InvThirdPartyHandlingFee:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n10.2),AT(220,214,48,10),USE(jobe2:InvThirdPartyHandlingFee),SKIP,TRN,HIDE,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           PROMPT('Total Cost'),AT(168,234),USE(?Prompt7:50),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(220,234,48,10),USE(tmpial:Total),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           ENTRY(@n14.2),AT(280,234,48,10),USE(tmpcla:Total),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                         END
                         TAB('MP'),USE(?ManufacturerPaidTab),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Manuacturer Payment'),AT(168,118),USE(?Prompt123),FONT(,9,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Labour Paid'),AT(168,132),USE(?jobe2:WLabourPaid:Prompt),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14.2),AT(268,132,48,10),USE(jobe2:WLabourPaid),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Warranty Labour Paid'),TIP('Warranty Labour Paid'),UPR,READONLY
                           PROMPT('Parts Paid'),AT(168,148),USE(?jobe2:WPartsPaid:Prompt),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14.2),AT(268,148,48,10),USE(jobe2:WPartsPaid),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Warranty Parts Paid'),TIP('Warranty Parts Paid'),UPR,READONLY
                           PROMPT('Other Costs'),AT(168,164),USE(?jobe2:WOtherCosts:Prompt),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14.2),AT(268,164,48,10),USE(jobe2:WOtherCosts),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Warranty Other Costs'),TIP('Warranty Other Costs'),UPR,READONLY
                           LINE,AT(248,179,67,0),USE(?Line1:18),COLOR(COLOR:Black)
                           PROMPT('Sub Total'),AT(168,185),USE(?jobe2:WSubTotal:Prompt),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14.2),AT(268,185,48,10),USE(jobe2:WSubTotal),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Warranty Sub Total'),TIP('Warranty Sub Total'),UPR,READONLY
                           PROMPT('V.A.T.'),AT(168,196),USE(?jobe2:WVAT:Prompt),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14.2),AT(268,196,48,10),USE(jobe2:WVAT),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Warranty VAT'),TIP('Warranty VAT'),UPR,READONLY
                           PROMPT('Total Paid'),AT(168,212),USE(?jobe2:WTotal:Prompt),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14.2),AT(268,212,48,10),USE(jobe2:WTotal),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Warranty Total'),TIP('Warranty Total'),UPR,READONLY
                           PROMPT('Warranty Invoice Ref Number'),AT(168,249),USE(?jobe2:WInvoiceRefNo:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(168,260,124,10),USE(jobe2:WInvoiceRefNo),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Warranty Invoice Ref Number'),TIP('Warranty Invoice Ref Number'),UPR,READONLY
                         END
                       END
                       SHEET,AT(340,100,176,230),USE(?Sheet2),ABOVE(10),FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(0D6E7EFH),SPREAD
                         TAB('C'),USE(?RRCChargeableTab),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Chargeable'),AT(344,116),USE(?Prompt27:8),FONT(,9,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           CHECK('Ignore Default Charges'),AT(412,119),USE(jobe:IgnoreRRCChaCosts),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Ignore Default Charges'),TIP('Ignore Default Charges'),VALUE('1','0')
                           PROMPT('Courier Cost'),AT(344,146),USE(?tmprc:CourierCost:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,146,48,10),USE(job:Courier_Cost,,?job:Courier_Cost:2),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Courier Cost'),TIP('Courier Cost'),UPR
                           PROMPT('Discount'),AT(344,132),USE(?jobe2:JobDiscountAmnt:Prompt),FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,132,48,10),USE(jobe2:JobDiscountAmnt),SKIP,TRN,RIGHT,FONT(,,,,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Chargeable Discount Labour Cost'),TIP('Chargeable Discount Labour Cost'),READONLY
                           PROMPT('Labour Cost'),AT(344,161),USE(?Prompt7:21),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,161,48,10),USE(jobe:RRCCLabourCost),SKIP,TRN,RIGHT,FONT(,8,,FONT:bold),COLOR(09A6A7CH),READONLY,MSG('Labour Cost')
                           PROMPT('Parts Cost'),AT(344,175),USE(?Prompt7:22),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,175,48,10),USE(jobe:RRCCPartsCost),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY,MSG('Parts Cost')
                           PROMPT('Sub Total'),AT(344,191),USE(?Prompt7:23),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LINE,AT(421,186,67,0),USE(?Line1:5),COLOR(COLOR:Black)
                           ENTRY(@n14.2),AT(440,191,48,10),USE(jobe:RRCCSubTotal),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY,MSG('RRC Sub Total')
                           PROMPT('V.A.T.'),AT(344,202),USE(?Prompt7:24),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,202,48,10),USE(tmprc:Vat),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           PROMPT('Total Cost'),AT(344,217),USE(?Prompt7:25),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,217,48,10),USE(tmprc:Total),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           LINE,AT(423,228,67,0),USE(?Line1:15),COLOR(COLOR:Black)
                           PROMPT('Paid'),AT(344,233),USE(?tmprc:Paid:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14.2),AT(440,233,48,10),USE(tmprc:Paid),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Balance'),TIP('Balance'),UPR,READONLY
                           PROMPT('Outstanding'),AT(344,244),USE(?Outstanding:Prompt:2),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14.2),AT(440,244,48,10),USE(tmprc:Outstanding),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Outstanding'),TIP('Outstanding'),UPR,READONLY
                         END
                         TAB('W'),USE(?RRCWarrantyTab),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Warranty'),AT(344,116,43,10),USE(?Prompt27:10),FONT(,9,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           CHECK('Ignore Default Charges'),AT(412,116),USE(jobe:IgnoreRRCWarCosts),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Ignore Default Charges'),TIP('Ignore Default Charges'),VALUE('1','0')
                           PROMPT('Courier Cost'),AT(344,132),USE(?job:Courier_Cost_Warranty:Prompt:3),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Labour Cost'),AT(344,148),USE(?Prompt7:31),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,148,48,10),USE(jobe:RRCWLabourCost),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY,MSG('Labour Cost')
                           PROMPT('Parts Cost'),AT(344,164),USE(?Prompt7:32),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,164,48,10),USE(jobe:RRCWPartsCost),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY,MSG('Parts Cost')
                           LINE,AT(420,180,67,0),USE(?Line1:7),COLOR(COLOR:Black)
                           PROMPT('Sub Total'),AT(344,185),USE(?Prompt7:33),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,185,48,10),USE(jobe:RRCWSubTotal),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY,MSG('RRC Sub Total')
                           PROMPT('V.A.T.'),AT(344,196),USE(?Prompt7:34),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,196,48,10),USE(tmprw:VAT),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           PROMPT('Total Cost'),AT(344,212),USE(?Prompt7:35),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,212,48,10),USE(tmprw:Total),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           ENTRY(@n14.2),AT(440,132,48,10),USE(job:Courier_Cost_Warranty,,?job:Courier_Cost_Warranty:3),DISABLE,HIDE,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Courier Cost'),TIP('Courier Cost'),UPR
                         END
                         TAB('H'),USE(?RRCHandlingTab),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Handling'),AT(344,116),USE(?Prompt27:6),FONT(,9,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Handling Fee'),AT(344,132),USE(?tmpac:CourieCost:Prompt:5),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,132,48,10),USE(tmp:HandlingFee),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                         END
                         TAB('EX'),USE(?RRCExchangeTab),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Exchange'),AT(344,116),USE(?Prompt27:7),FONT(,9,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Exchange Fee'),AT(344,132),USE(?tmpac:CourieCost:Prompt:6),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,132,48,10),USE(tmp:ExchangeFee),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                         END
                         TAB,USE(?RRCBlankTab),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('E'),USE(?RRCEstimateTab),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Estimate'),AT(344,116),USE(?Prompt27:9),FONT(,9,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           CHECK('Ignore Default Charges'),AT(412,119),USE(jobe:IgnoreRRCEstCosts),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Ignore Default Charges'),TIP('Ignore Default Charges'),VALUE('1','0')
                           PROMPT('Courier Cost'),AT(344,132),USE(?job:Courier_Cost:Prompt:3),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,132,48,10),USE(job:Courier_Cost_Estimate,,?job:Courier_Cost_Estimate:2),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Courier Cost'),TIP('Courier Cost'),UPR
                           PROMPT('Labour Cost'),AT(344,148),USE(?Prompt7:26),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,148,48,10),USE(jobe:RRCELabourCost),SKIP,TRN,RIGHT,FONT(,8,,FONT:bold),COLOR(09A6A7CH),READONLY,MSG('RRC Estimate Labour Cost')
                           PROMPT('Parts Cost'),AT(344,164),USE(?Prompt7:27),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,164,48,10),USE(jobe:RRCEPartsCost),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY,MSG('RRC Estimate Parts Cost')
                           LINE,AT(422,180,67,0),USE(?Line1:6),COLOR(COLOR:Black)
                           PROMPT('Sub Total'),AT(344,185),USE(?Prompt7:28),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,185,48,10),USE(jobe:RRCESubTotal),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY,MSG('RRC Estimate Sub Total')
                           PROMPT('V.A.T.'),AT(344,196),USE(?Prompt7:29),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,196,48,10),USE(tmpres:vat),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           PROMPT('Total Cost'),AT(344,212),USE(?Prompt7:30),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,212,48,10),USE(tmpres:total),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                         END
                         TAB('IC'),USE(?RRCCInvoiceTab),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Courier Cost'),AT(344,153),USE(?tmprc:CourierCost:Prompt:4),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,153,48,10),USE(job:Invoice_Courier_Cost,,?job:Invoice_Courier_Cost:2),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Courier Cost'),TIP('Courier Cost'),UPR,READONLY
                           PROMPT('Discount'),AT(344,138),USE(?jobe2:InvDiscountAmnt:Prompt),FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,138,48,10),USE(jobe2:InvDiscountAmnt),SKIP,TRN,RIGHT,FONT(,,,,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Chargeable Discount LaboutCost'),TIP('Chargeable Discount LaboutCost'),READONLY
                           PROMPT('Labour Cost'),AT(344,164),USE(?Prompt7:51),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,164,48,10),USE(jobe:InvRRCCLabourCost),SKIP,TRN,RIGHT,FONT(,8,,FONT:bold),COLOR(09A6A7CH),READONLY,MSG('Invoice Labour Cost')
                           PROMPT('Parts Cost'),AT(344,177),USE(?Prompt7:52),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,177,48,10),USE(jobe:InvRRCCPartsCost),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY,MSG('Invoice Parts Cost')
                           LINE,AT(419,188,67,0),USE(?Line1:11),COLOR(COLOR:Black)
                           PROMPT('Sub Total'),AT(344,193),USE(?Prompt7:53),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LINE,AT(422,231,67,0),USE(?Line1:17),COLOR(COLOR:Black)
                           ENTRY(@n14.2),AT(440,193,48,10),USE(jobe:InvRRCCSubTotal),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY,MSG('Invoice Sub Total')
                           PROMPT('V.A.T.'),AT(344,204),USE(?Prompt7:54),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,204,48,10),USE(tmpirc:Vat),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           PROMPT('Total Cost'),AT(344,217),USE(?Prompt7:55),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,217,48,10),USE(tmpirc:Total),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           PROMPT('Credit Exists'),AT(392,217),USE(?Prompt:CreditExists),HIDE,FONT(,,COLOR:Red,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Yellow)
                           PROMPT('Paid'),AT(344,233),USE(?Paid:Prompt:2),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14.2),AT(440,233,48,10),USE(tmpirc:Paid),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Paid'),TIP('Paid'),UPR,READONLY
                           PROMPT('Outstanding'),AT(344,244),USE(?Outstanding:Prompt:4),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14.2),AT(440,244,48,10),USE(tmpirc:Outstanding),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Outstanding'),TIP('Outstanding'),UPR,READONLY
                           PROMPT('Chargeable - Invoiced'),AT(344,116),USE(?Prompt27:14),FONT(,9,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           CHECK('Ignore Default Charges'),AT(412,127),USE(jobe:IgnoreRRCChaCosts,,?jobe:IgnoreRRCChaCosts:2),DISABLE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Ignore Default Charges'),TIP('Ignore Default Charges'),VALUE('1','0')
                           PROMPT('Inv No'),AT(344,255,168,20),USE(?RRCInvoiceNumber),LEFT,FONT(,,,FONT:bold),COLOR(09A6A7CH)
                         END
                         TAB('IW'),USE(?RRCWInvoiceTab),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Warranty - Invoiced'),AT(344,116,144,12),USE(?Prompt27:15),FONT(,9,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           CHECK('Ignore Default Charges'),AT(412,129),USE(jobe:IgnoreRRCWarCosts,,?jobe:IgnoreRRCWarCosts:2),DISABLE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Ignore Default Charges'),TIP('Ignore Default Charges'),VALUE('1','0')
                           PROMPT('Courier Cost'),AT(344,140),USE(?job:Courier_Cost_Warranty:Prompt:6),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,140,48,10),USE(job:WInvoice_Courier_Cost,,?job:WInvoice_Courier_Cost:3),SKIP,TRN,HIDE,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Courier Cost'),TIP('Courier Cost'),UPR,READONLY
                           PROMPT('Labour Cost'),AT(344,153),USE(?Prompt7:56),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,153,48,10),USE(jobe:InvRRCWLabourCost),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY,MSG('Invoice Labour Cost')
                           PROMPT('Parts Cost'),AT(344,164),USE(?Prompt7:57),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,164,48,10),USE(jobe:InvRRCWPartsCost),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY,MSG('Invoice Parts Cost')
                           LINE,AT(419,180,67,0),USE(?Line1:12),COLOR(COLOR:Black)
                           PROMPT('Sub Total'),AT(344,185),USE(?Prompt7:58),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,185,48,10),USE(jobe:InvRRCWSubTotal),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY,MSG('Invoice Sub Total')
                           PROMPT('V.A.T.'),AT(344,196),USE(?Prompt7:59),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,196,48,10),USE(tmpirw:Vat),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                           PROMPT('Total Cost'),AT(344,212),USE(?Prompt7:60),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(440,212,48,10),USE(tmpirw:Total),SKIP,TRN,RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH),UPR,READONLY
                         END
                         TAB('CR'),USE(?Tab:Credit),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Original Total'),AT(344,116),USE(?Prompt110),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n-14.2),AT(412,257),USE(tmpirc:Total,,?tmpirc:Total:2),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Original Invoice'),AT(344,130),USE(?Prompt110:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Prompt 115'),AT(412,130),USE(?Prompt:CreditInvoice),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Credits/Invoices'),AT(344,146),USE(?Prompt111),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(344,156,168,74),USE(?List),IMM,FONT(,8,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('60L(2)|M*~Number~@s30@22L(2)|M*~Type~@s4@46R(2)|M*~Date~@d6@56R(2)|M*~Amount~D@n' &|
   '-14.2@'),FROM(Queue:Browse)
                           PROMPT('Current Total'),AT(344,257),USE(?CurrentTotal),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n-14.2),AT(412,116),USE(tmp:CreditTotal),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       CHECK('Exchange Replacement Charge'),AT(168,340),USE(jobe:ExcReplcamentCharge),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0'),MSG('Exchange Replacement Charge')
                       BUTTON,AT(444,332),USE(?Button:Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(380,332),USE(?Button:SaveChanges),TRN,FLAT,LEFT,ICON('savep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW9                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
local       Class
ShowField       Procedure(String func:Field)
HideField       Procedure(String func:Field)
Pricing         Procedure(Byte func:Force)
AddToTempQueue  Procedure(String f:Field,String f:SaveField,String f:Type,String f:True,String f:False,Byte f:SaveCost,Real f:Cost),String
            End ! local       Class
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

SaveChanges     Routine
Data
local:Notes         String(255)
Code
    Loop x# = 1 To Records(TempQueue)
        Get(TempQueue,x#)
        local:Notes = 'REASON: ' & Clip(tmpque:Reason)
        If tmpque:SaveCost
            local:Notes = Clip(local:Notes) & '<13,10>PREVIOUS CHARGE: ' & Format(tmpque:Cost,@n14.2)
        End ! If tmpque:SaveCost
        If AddToAudit(job:Ref_Number,'JOB',Clip(tmpque:Field),Clip(local:Notes))
        End ! If AddToAudit(job:Ref_Number,'JOB',Clip(tmpque:Field),Clip(local:Notes)
    End ! Loop x# = 1 To Records(TempQueue)
    Access:JOBS.Update()
    Access:JOBSE.Update()
    tmp:Close = 1
    Access:JOBSE2.RestoreFile(Save_JOBSE2_ID)

    !added by paul 05/10/2010 - Log no 11691
    !need to update the discount amount if it needs it

    jobe2:JobDiscountAmnt = LocalDiscountAmount
    jobe2:InvDiscountAmnt = jobe2:JobDiscountAmnt
    access:JOBSE2.Update()

    Post(Event:CloseWindow)
ShowHideTabs            Routine
    !Hide All Tabs -  (DBH: 05-12-2003)
    ?ARCChargeableTab{prop:Hide} = 1
    ?ARCWarrantyTab{prop:Hide} = 1
    ?ARCClaimTab{prop:Hide} = 1
    ?ARC3rdPartyTab{prop:Hide} = 1
    ?EstimateTab{prop:Hide} = 1
    ?ARCCInvoiceTab{prop:Hide} = 1
    ?ARCWInvoiceTab{prop:Hide} = 1
    ?ARCCLInvoiceTab{prop:Hide} = 1
    ?ARCBlankTab{prop:Hide} = 1
    ?RRCChargeableTab{prop:Hide} = 1
    ?RRCWarrantyTab{prop:Hide} = 1
    ?RRCHandlingTab{prop:Hide} = 1
    ?RRCExchangeTab{prop:Hide} = 1
    ?RRCBlankTab{prop:Hide} = 1
    ?RRCEstimateTab{prop:Hide} = 1
    ?RRCCInvoiceTab{prop:Hide} = 1
    ?RRCWInvoiceTab{prop:Hide} = 1

    !What tabs shall we see today?
    ?ARCChargeableTab{prop:Text} = 'Chargeable'
    ?ARCWarrantyTab{prop:Text} = 'Warranty'
    ?EstimateTab{prop:Text} = 'Estimate'
    ?RRCChargeableTab{prop:Text} = 'Chargeable'
    ?RRCWarrantyTab{prop:Text} = 'Warranty'
    ?RRCHandlingTab{prop:Text} = 'Handling'
    ?RRCExchangeTab{prop:Text} = 'Exchange'
    ?RRCEstimateTab{prop:Text} = 'Estimate'
    ?ARCCInvoiceTab{prop:Text} = 'Chargeable'
    ?ARCWInvoiceTab{prop:Text} = 'Warranty'
    ?ARCCLInvoiceTab{prop:Text} = 'Claim'
    ?RRCCInvoiceTab{prop:Text} = 'Chargeable'
    ?RRCWInvoiceTab{prop:Text} = 'Warranty'
    ?ARCClaimTab{prop:Text} = 'Claim'
    ?ARC3rdPartyTab{prop:Text} = '3rd Party'
    ?ManufacturerPaidTab{prop:Text} = 'Man. Paid'

    Clear(ShowFields)

    If jobe:WebJob
        If job:Chargeable_Job = 'YES'
            If glo:WebJob
                show:RRCChargeableTab = 1
                If job:Exchange_Unit_Number <> 0
                    If jobe:ExchangedAtRRC
                        show:RRCExchangedTab = 1
                    Else !If jobe:ExchangedAtRRC
                        show:RRCHandlingTab = 1
                        show:ARCChargeableTab = 1
                    End !If jobe:ExchangedAtRRC
                Else
                    If SentToHub(job:Ref_Number)
                        show:ARCChargeableTab = 1
                        show:RRCHandlingTab = 1
                    End !If SentToHub(job:Ref_Number)
                End !If job:Exchange_Unit_NUmber <> 0 And jobe:ExchangedAtRRC
            Else !If glo:WebJob
                show:RRCChargeableTab = 1
                If job:Exchange_Unit_Number <> 0
                    If jobe:ExchangedATRRC
                        show:RRCExchangedTab = 1
                    Else !If jobe:ExchangedATRRC
                        show:RRCHandlingTab = 1
                    End !If jobe:ExchangedATRRC
                Else !If job:Exchange_Unit_Number <> 0
                    If SentToHub(job:Ref_Number)
                        show:RRCHandlingTab = 1
                    End !If SentToHub(job:Ref_Number)
                End !If job:Exchange_Unit_Number <> 0

                If SentToHub(job:Ref_number)
                    show:ARCChargeableTab = 1
                End !If SentToHub(job:Ref_number)
            End !If glo:WebJob
        End !If job:Chargeable_Job = 'YES'

        If job:Warranty_Job = 'YES'
            If glo:WebJob
                If job:Exchange_Unit_Number <> 0
                    If jobe:ExchangedATRRC
                        show:RRCExchangedTab = 1
                    Else !If jobe:ExchangedATRRC
                        show:RRCHandlingTab = 1
                    End !If jobe:ExchangedATRRC
                Else !If job:Exchange_Unit_Number <> 0
                    If SentToHub(job:Ref_Number)
                        show:RRCHandlingTab = 1
                    Else !If SentToHub(job:Ref_Number)
                        show:RRCWarrantyTab = 1
                    End !If SentToHub(job:Ref_Number)
                End !If job:Exchange_Unit_Number <> 0

            Else !If glo:Web_Job
                show:ARCClaimTab = 1
                If job:Exchange_Unit_Number <> 0
                    If jobe:ExchangedAtRRC
                        show:RRCExchangedTab = 1
                        show:ARCWarrantyTab = 1
                    Else !If jobe:ExchangedAtRRC
                        show:RRCHandlingTab = 1
                        show:ARCWarrantyTab = 1
                    End !If jobe:ExchangedAtRRC
                Else !If job:Exchange_Unit_Number <> 0
                    If SentToHub(job:Ref_Number)
                        show:ARCWarrantyTab = 1
                        show:RRCHandlingTab = 1
                    Else !If SentToHub(job:Ref_Number)
                        show:RRCWarrantyTab = 1
                    End !If SentToHub(job:Ref_Number)
                End !If job:Exchange_Unit_Number <> 0

            End !If glo:Web_Job
        End !If job:Warranty_Job = 'YES'
    Else !jobe:HubRepair
        If ~glo:WebJob
            If job:Chargeable_Job = 'YES'
                show:ARCChargeableTab = 1
            End !If job:Chargeable_Job = 'YES'
            If job:Warranty_job = 'YES'
                show:ARCWarrantyTab = 1
                show:ARCClaimTab = 1
            End !If job:Warranty_job = 'YES'
        End !If ~glo:WebJob
    End !jobe:HubRepair

    Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
    inv:Invoice_Number  = job:Invoice_Number
    If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
        !Found

    Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
        !Error
    End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign

    If show:ARCChargeableTab
        If job:Invoice_Number = 0
            !ARC Chargeable tab
            ?ARCChargeableTab{prop:Hide} = 0
        Else !If job:Invoice_Number = 0
            If inv:ARCInvoiceDate <> 0
                !ARC Invoice Chargeable tab
                ?ARCCInvoiceTab{prop:Hide} = 0
            Else !If inv:ARCInvoiceDate <> 0
                !ARC Chargeable tab
                ?ARCChargeableTab{prop:Hide} = 0
            End !If inv:ARCInvoiceDate <> 0
        End !If job:Invoice_Number = 0
    End !show:ARCChargeableTab

    If show:RRCChargeableTab
        If job:Invoice_Number = 0
            !RRC Chargeable Tab
            ?RRCChargeableTab{prop:Hide} = 0
        Else !If job:Invoice_Number = 0

            If inv:ExportedRRCOracle
                !RRC Invoice Chargeable Tab
                ?RRCCInvoiceTab{prop:Hide} = 0
            Else !If inv:ExportedRRCOracle
                !RRC Chargeable Tab
                ?RRCChargeableTab{prop:Hide} = 0
            End !If inv:ExportedRRCOracle
        End !If job:Invoice_Number = 0
    End !show:RRCChargeableTab

    If show:RRCWarrantyTab
        If wob:RRCWInvoiceNumber = 0
            !RRC Warranty Tab
            ?RRCWarrantyTab{prop:Hide} = 0
        Else !If job:Invoice_Number_Warranty = 0
            !RRC Invoiced Warranty Tab
            ?RRCWInvoiceTab{prop:Hide} = 0
        End !If job:Invoice_Number_Warranty = 0
    End !show:RRCWarrantyTab

    If show:ARCWarrantyTab
        If job:Invoice_Number_Warranty = 0
            !ARC Warranty Tab
            ?ARCWarrantyTab{prop:Hide} = 0
        Else !If job:Invoice_Number_Warranty = 0
            !ARC Invoiced Warranty Tab
            ?ARCWInvoiceTab{prop:Hide} = 0
        End !If job:Invoice_Number_Warranty = 0
    End !show:ARCWarrantyTab

    If show:ARCClaimTab
        ?jobe2:ThirdPartyHandlingFee{prop:Hide} = 1
        ?jobe2:ThirdPartyHandlingFee:Prompt{prop:Hide} = 1
        ?jobe2:InvThirdPartyHandlingFee{prop:Hide} = 1
        ?jobe2:InvThirdPartyHandlingFee:Prompt{prop:Hide} = 1
        If job:Invoice_Number_Warranty = 0
            !ARC Claim Tab
            ?ARCClaimTab{prop:Hide} = 0
            IF (_PreClaimThirdParty(job:Third_Party_Site,job:Manufacturer))  ! #12363 Show 3rd party handling fee (DBH: 16/02/2012)
                ?jobe2:ThirdPartyHandlingFee{prop:Hide} = 0
                ?jobe2:ThirdPartyHandlingFee:Prompt{prop:Hide} = 0
            END ! IF (_PreThirdPartyClaim(job:Third_Party_Site,job:Manufacturer))
        Else !If job:Invoice_Number_Warranty = 0
            !ARC Invoiced Claim Tab
            ?ARCCLInvoiceTab{prop:Hide} = 0
            ! Inserting (DBH 20/06/2008) # 9792 - Show what the manufacturer paid
            ?ManufacturerPaidTab{prop:Hide} = 0
            ! End (DBH 20/06/2008) #9792
            IF (_PreClaimThirdParty(job:Third_Party_Site,job:Manufacturer))  ! #12363 Show 3rd party handling fee (DBH: 16/02/2012)
                ?jobe2:InvThirdPartyHandlingFee{prop:Hide} = 0
                ?jobe2:InvThirdPartyHandlingFee:Prompt{prop:Hide} = 0
            END ! IF (_PreThirdPartyClaim(job:Third_Party_Site,job:Manufacturer))

        End !If job:Invoice_Number_Warranty = 0
    End !show:ARCClaimTab

    If (job:Chargeable_Job = 'YES' And ~ExcludeHandlingFee('C',job:Manufacturer,job:Repair_Type))Or |
        (job:Warranty_job = 'YES' And ~EXcludeHandlingFee('W',job:Manufacturer,job:Repair_Type_Warranty))
        If show:RRCHandlingTab
            !RRC Handing Tab
            ?RRCHandlingTab{prop:Hide} = 0
        End !show:RRCHandlingFee
    End !job:Warranty_job = 'YES' And ~EXcludeHandlingFee('W',job:Manufacturer,job:Repair_Type_Warranty)

    If show:RRCExchangedTab
        !RRC Exchanged Tab
        ?RRCExchangeTab{prop:Hide} = 0
    End !show:RRCExchangedTab

    If job:Chargeable_Job = 'YES'
        If job:Estimate = 'YES'
            !if job:Estimate_Accepted = 'NO' and job:Estimate_Rejected = 'NO'
            If job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
                ?RRCChargeableTab{prop:Hide} = 1
                ?ARCChargeableTab{prop:Hide} = 1
            Else
! Changing (DBH 04/12/2007) # 8218 - The ARC Tab is disabled, so why not the RRC?
!                If jobe:WebJob
!                    !?RRCEstimateTab{prop:Disable} = 1
! to (DBH 04/12/2007) # 8218
! Deleting (DBH 13/02/2008) # 9761 - Don't disable the estimate tabs (mmmm)
!                    ?RRCEstimateTab{prop:Disable} = 1
!                    ?EstimateTab{prop:Disable} = 1
! End (DBH 13/02/2008) #9761
! End (DBH 04/12/2007) #8218
            end
            If jobe:WebJob
                ?RRCEstimateTab{prop:Hide} = 0
                ! Inserting (DBH 03/12/2007) # 8218 - Show the ARC and RRC estimate tabs for RRC booked estimates
                If SentToHub(job:Ref_Number)
                    ?EstimateTab{prop:Hide} = 0
                End ! If SentToHub(job:Ref_Number)
                ! End (DBH 03/12/2007) #8218
            Else
                ?EstimateTab{prop:Hide} = 0
            End
        End !If job:Estiamte = 'YES'
    End !job:Chargeable_Job = 'YES'

    If ~glo:WebJob
        !Has this job been sent to Third Party?
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        If Access:JOBTHIRD.TryFetch(jot:RefNumberKey) = Level:Benign
            !Found
            ?ARC3rdPartyTab{prop:Hide} = 0
        Else!If Access:JOBTHIRD.TryFetch(jot:RefNumberKey) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:JOBTHIRD.TryFetch(jot:RefNumberKey) = Level:Benign
    End !glo:WebJob

    !----------------------------------------------------------------------!
    if ?RRCHandlingTab{prop:Hide} = 0   ! Don't show handling fee if repair type = liquid damage
        if job:Chargeable_Job = 'YES'
            Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
            rtd:Manufacturer = job:Manufacturer
            rtd:Chargeable   = 'YES'
            rtd:Repair_Type  = job:Repair_Type
            if Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                if rtd:BER = 4 ! Liquid Damage
                    ?RRCHandlingTab{prop:Hide} = 1
                end
            end
        end

        if job:Warranty_Job = 'YES'
            Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
            rtd:Manufacturer = job:Manufacturer
            rtd:Warranty     = 'YES'
            rtd:Repair_Type  = job:Repair_Type_Warranty
            if Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                if rtd:BER = 4 ! Liquid Damage
                    ?RRCHandlingTab{prop:Hide} = 1
                end
            end
        end

    end
    !----------------------------------------------------------------------!

    Do CheckIfTabsAreShowing


    tmp:RenameCourierCost = Clip(GETINI('RENAME','RenameCourierCost',,CLIP(PATH())&'\SB2KDEF.INI'))

    if tmp:RenameCourierCost
        tmp:CourierCostName = Clip(GETINI('RENAME','CourierCostName',,CLIP(PATH())&'\SB2KDEF.INI'))
    end

    if tmp:CourierCostName <> ''
        ?JOB:Courier_Cost:Prompt:2{prop:Text} = clip(tmp:CourierCostName)                       ! Rename courier costs prompt
        ?JOB:Courier_Cost:Prompt{prop:Text} = clip(tmp:CourierCostName)
        ?tmprc:CourierCost:Prompt:3{prop:Text} = clip(tmp:CourierCostName)
        ?tmprc:CourierCost:Prompt{prop:Text} = clip(tmp:CourierCostName)
        ?job:Courier_Cost:Prompt:3{prop:Text} = clip(tmp:CourierCostName)
        ?tmprc:CourierCost:Prompt:4{prop:Text} = clip(tmp:CourierCostName)

! Changing (DBH 15/05/2007) # 8999 - Show courier cost if job is RTM
!        If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangedATRRC
! to (DBH 15/05/2007) # 8999
        If job:Exchange_Unit_Number <> 0 And (~jobe:ExchangedATRRC Or (jobe:ExchangedATRRC And job:Repair_Type_Warranty = 'R.T.M.'))
! End (DBH 15/05/2007) #8999
            UnHide(?JOB:Courier_Cost_Warranty:Prompt)                                                 ! Exclude courier costs from
            UnHide(?job:Courier_Cost_Warranty)                                                        ! warranty claim
            UnHide(?job:Courier_Cost_Warranty:Prompt:2)
            UnHide(?job:Courier_Cost_Warranty:2)
        Else
            Hide(?JOB:Courier_Cost_Warranty:Prompt)                                                 ! Exclude courier costs from
            Hide(?job:Courier_Cost_Warranty)                                                        ! warranty claim
            Hide(?job:Courier_Cost_Warranty:Prompt:2)
            Hide(?job:Courier_Cost_Warranty:2)
        End !If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangedATRRC
    end


    !Has a loan unit been attached?
    If LoanAttachedToJob(job:Ref_Number) = 1
        If ~jobe:HubRepair
            ?job:Courier_Cost{prop:Hide} = 0
            ?job:Courier_Cost:Prompt{prop:Hide} = 0
            ?tmprc:CourierCost:Prompt:3{prop:Hide} = 0
            ?job:Invoice_Courier_Cost{prop:Hide} = 0
            ?job:Courier_Cost:Prompt:2{prop:Hide} = 0
            ?job:Courier_Cost_Estimate{prop:Hide} = 0
        Else !If ~jobe:HubRepair
            ?tmprc:CourierCost:Prompt{prop:Hide} = 0
            ?job:Courier_Cost:2{prop:Hide} = 0
            ?tmprc:CourierCost:Prompt:4{prop:Hide} = 0
            ?job:Invoice_Courier_Cost:2{prop:Hide} = 0
            ?job:Courier_Cost:Prompt:3{prop:Hide} = 0
            ?job:Courier_Cost_Estimate:2{prop:Hide} = 0
        End !If ~jobe:HubRepair
    Else !LoanAttachedToJob(job:Ref_Number)
        !Is this a 48 Hour Exchange? -  (DBH: 10-11-2003)
        ?job:Courier_Cost{prop:Hide} = 1
        ?job:Courier_Cost:Prompt{prop:Hide} = 1
        ?tmprc:CourierCost:Prompt{prop:Hide} = 1
        ?job:Courier_Cost:2{prop:Hide} = 1
        ?tmprc:CourierCost:Prompt:3{prop:Hide} = 1
        ?job:Invoice_Courier_Cost{prop:Hide} = 1
        ?tmprc:CourierCost:Prompt:4{prop:Hide} = 1
        ?job:Invoice_Courier_Cost:2{prop:Hide} = 1
        ?job:Courier_Cost:Prompt:2{prop:Hide} = 1
        ?job:Courier_Cost_Estimate{prop:Hide} = 1
        ?job:Courier_Cost:Prompt:3{prop:Hide} = 1
        ?job:Courier_Cost_Estimate:2{prop:Hide} = 1

        !Do not show the Exchange Replacement
        !tick for Warranty Jobs -  (DBH: 05-12-2003)
        If jobe:Engineer48HourOption = 1 and job:Chargeable_Job = 'YES' !Amended by NB 9/12/2003 Added = 1 (always true)
            If jobe:WebJob = 1
                ?jobe:ExcReplcamentCharge{prop:Hide} = 0
                If jobe:ExcReplcamentCharge
                    !Do not show the Chargeable Tab if the job has been invoiced - 3865 (DBH: 11-12-2003)
                    If ?RRCCInvoiceTab{prop:Hide} = 0
                        ?jobe:ExcReplcamentCharge{prop:Disable} = 1
                        ?tmprc:CourierCost:Prompt:4{prop:Text} = 'Exchange Repl'
                        ?tmprc:CourierCost:Prompt:4{prop:Hide} = 0
                        ?job:Invoice_Courier_Cost:2{prop:Hide} = 0
                    Else !If ?RRCCInvoiceTab{prop:Hide} = 0
                        ?RRCChargeableTab{prop:Hide} = 0
                        ?job:Courier_Cost:2{prop:Hide} = 0
                        ?tmprc:CourierCost:Prompt{prop:Hide} = 0
                        ?tmprc:CourierCost:Prompt{prop:Text} = 'Exchange Repl'
                    End !Else !If ?RRCCInvoiceTab{prop:Hide} = 0

                    !Replacement Charge should also show on the ARC side too
                    !But disable field, because there's no need to have two fields to input the same thing -  (DBH: 15-01-2004)
                    If ~glo:WebJob
                        If ?ARCCInvoiceTab{prop:Hide} = 0
                            ?tmprc:CourierCost:Prompt:3{prop:Text} = 'Exchange Repl'
                            ?tmprc:CourierCost:Prompt:3{prop:Hide} = 0
                            ?job:Invoice_Courier_Cost{prop:Hide} = 0
                        Else !If ?ARCCInvoiceTab{prop:Hide} = 0
                            ?ARCChargeableTab{prop:Hide} = 0
                            ?job:Courier_Cost{prop:Hide} = 0
                            ?job:Courier_Cost:Prompt{prop:Hide} = 0
                            ?job:Courier_Cost:Prompt{prop:Text} = 'Exchange Repl'
                        End !If ?ARCCInvoiceTab{prop:Hide} = 0
                    End !If ~glo:WebJob
                End !If tmp:ExchangeCharge
            End !If jobe:WebJob = 1
        End !If jobe:Engineer48HourOption
    End !LoanAttachedToJob(job:Ref_Number)


    !j commented this
    !Access:JOBSE.Clearkey(jobe:RefNumberKey)
    !jobe:RefNumber  = job:Ref_Number
    !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
    !
    !End !Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

    If jobe:ARC3rdPartyInvoiceNumber <> ''
        ?3rdPartyInvoiceNumber{prop:Text} = 'Inv: ' & jobe:ARC3rdPartyInvoiceNumber
    End !jobe:ARC3rdPartyInvoiceNumber <> ''

    !Do not allow RRC to change ARC Tab - L954 (DBH: 16-09-2003)
    If glo:WebJob
        ?ARCChargeableTab{prop:Disable} = 1
    End !If glo:WebJob

    !Does the user have access to the exchange replacement
    !tick box? -  (DBH: 27-11-2003)
    If SecurityCheck('EXCHANGE REPLACEMENT CHARGE')
        ?jobe:ExcReplcamentCharge{prop:Disable} = True
    End !SecurityCheck('EXCHANGE REPLACMENT CHARGE')

    ! Inserting (DBH 24/07/2007) # 9141 - Show the credit tab and adjust totals
    If ?RRCCInvoiceTab{prop:Hide} = 0
        Found# = 0
        tmp:CreditTotal = tmpirc:Total
        Access:JOBSINV.Clearkey(jov:TypeRecordKey)
        jov:RefNumber = job:Ref_Number
        jov:Type = 'C'
        Set(jov:TypeRecordKey,jov:TypeRecordKey)
        Loop ! Begin Loop
            If Access:JOBSINV.Next()
                Break
            End ! If Access:JOBSINV.Next()
            If jov:RefNumber <> job:Ref_Number
                Break
            End ! If jov:RefNumber <> job:Ref_Number
            If jov:Type <> 'C'
                Break
            End ! If jov:Type <> 'C'
            Found# = 1
!            tmp:CreditTotal -= jov:CreditAmount
            tmpirc:Total -= jov:CreditAmount
            tmpirc:Outstanding -= jov:CreditAmount
        End ! Loop

        If Found#
            ?Tab:Credit{prop:Hide} = 0
            ?Tab:Credit{prop:Text} = 'Credit Details'
            ?Prompt:CreditExists{prop:Hide} = 0
            ?Prompt:CreditInvoice{prop:Text} = Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification)
        End ! If Found#
    End ! If ?RRCCInvoiceTab{prop:Hide} = 0
    ! End (DBH 24/07/2007) #9141

CheckIfTabsAreShowing       Routine
    If ?ARCChargeableTab{prop:Hide} = 1 And ?ARCWarrantyTab{prop:Hide} = 1 And |
        ?EstimateTab{prop:Hide} = 1 And ?ARCClaimtab{prop:Hide} = 1 And ?ARC3rdPartyTab{prop:Hide} = 1 And |
        ?ARCCInvoiceTab{prop:Hide} = 1 And ?ARCWInvoiceTab{prop:Hide} = 1 And ?ARCCLInvoiceTab{prop:Hide} = 1
        ?ARCBlankTab{prop:Hide} = 0
        ?ARCTitle{prop:Hide} = 1
    End !?EstimateTab{prop:Hide} = 1 And ?ARCClaimtab{prop:Hide} = 1 And ?ARC3rdPartyTab{prop:Hide} = 1

    If ?RRCChargeableTab{prop:Hide} = 1 And ?RRCWarrantyTab{prop:Hide} = 1 And |
        ?RRCHandlingTab{prop:Hide} = 1 And ?RRCExchangeTab{prop:Hide} = 1 And |
        ?RRCEstimateTab{prop:Hide} = 1 And ?RRCCInvoiceTab{prop:Hide} = 1 And ?RRCWInvoiceTab{prop:Hide} = 1
        ?RRCBlankTab{prop:Hide} = 0
        ?RRCTitle{prop:Hide} = 1
    End !?RRCEstimateTab{prop:Hide} = 1 And ?RRCCInvoiceTab{prop:Hide} = 1 And ?RRCWInvoiceTab{prop:Hide} = 1
TickBoxUpdate       Routine
    Case job:Ignore_Chargeable_Charges
        Of 'NO'
            Local.HideField(?job:Labour_Cost)
        Of 'YES'
            Local.ShowField(?job:Labour_Cost)
    End !job:Ignore_Chargeable_Charges


    Case job:Ignore_Warranty_Charges
        Of 'NO'
            Local.HideField(?job:Labour_Cost_Warranty)
            Local.HideField(?job:Courier_Cost_Warranty)
        Of 'YES'
            Local.ShowField(?job:Labour_Cost_Warranty)
            Local.ShowField(?job:Courier_Cost_Warranty)

    End !job:Ignore_Chargeable_Charges

    Case jobe:IgnoreClaimCosts
        Of 0
            Local.HideField(?jobe:ClaimValue)
            Local.HideField(?job:Courier_Cost_Warranty)
        Of 1
            Local.ShowField(?jobe:ClaimValue)
            Local.ShowField(?job:Courier_Cost_Warranty)
    End !job:Ignore_Chargeable_Charges

    Case jobe:Ignore3rdPartyCosts
        Of 0
            Local.HideField(?jobe:ARC3rdPartyCost)
            Local.HideField(?jobe:ARC3rdPartyMarkup)
        Of 1
            Local.ShowField(?jobe:ARC3rdPartyCost)
            Local.ShowField(?jobe:ARC3rdPartyMarkup)
    End !job:Ignore_Chargeable_Charges

    Case job:Ignore_Estimate_Charges
        Of 'NO'
            Local.HideField(?job:Labour_Cost_Estimate)
        Of 'YES'
            Local.ShowField(?job:Labour_Cost_Estimate)
    End !job:Ignore_Chargeable_Charges

    Case jobe:IgnoreRRCChaCosts
        Of 0
            Local.HideField(?jobe:RRCCLabourCost)
        Of 1
            Local.ShowField(?jobe:RRCCLabourCost)
    End !job:Ignore_Chargeable_Charges

    Case jobe:IgnoreRRCWarCosts
        Of 0
            Local.HideField(?jobe:RRCWLabourCost)
        Of 1
            Local.ShowField(?jobe:RRCWLabourCost)
    End !job:Ignore_Chargeable_Charges

    Case jobe:IgnoreRRCEstCosts
        Of 0
            Local.HideField(?jobe:RRCELabourCost)
        Of 1
            Local.ShowField(?jobe:RRCELabourCost)
    End !job:Ignore_Chargeable_Charges

DefaultLabourCost            ROUTINE

    If InvoiceSubAccounts(job:Account_Number)
        access:subchrge.clearkey(suc:model_repair_type_key)
        suc:account_number = job:account_number
        suc:model_number   = job:model_number
        suc:charge_type    = job:charge_type
        suc:unit_type      = job:unit_type
        suc:repair_type    = job:repair_type
        if access:subchrge.fetch(suc:model_repair_type_key)
            access:trachrge.clearkey(trc:account_charge_key)
            trc:account_number = sub:main_account_number
            trc:model_number   = job:model_number
            trc:charge_type    = job:charge_type
            trc:unit_type      = job:unit_type
            trc:repair_type    = job:repair_type
            if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                LocalDefaultLabour      = trc:RRCRate
            End!if access:trachrge.fetch(trc:account_charge_key)
        Else
            LocalDefaultLabour      = suc:RRCRate
        End!if access:subchrge.fetch(suc:model_repair_type_key)
    Else !If InvoiceSubAccounts(job:Account_Number)
        access:trachrge.clearkey(trc:account_charge_key)
        trc:account_number = sub:main_account_number
        trc:model_number   = job:model_number
        trc:charge_type    = job:charge_type
        trc:unit_type      = job:unit_type
        trc:repair_type    = job:repair_type
        if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
            LocalDefaultLabour      = trc:RRCRate
        End!if access:trachrge.fetch(trc:account_charge_key)

    End !If InvoiceSubAccounts(job:Account_Number)

    If LocalDefaultLabour = 0 then
        access:stdchrge.clearkey(sta:model_number_charge_key)
        sta:model_number = job:model_number
        sta:charge_type  = job:charge_type
        sta:unit_type    = job:unit_type
        sta:repair_type  = job:repair_type
        if access:stdchrge.fetch(sta:model_number_charge_key)
        Else !if access:stdchrge.fetch(sta:model_number_charge_key)
            LocalDefaultLabour      = sta:RRCRate
        end !if access:stdchrge.fetch(sta:model_number_charge_key)
    End !LocalDefaultLabour = 0 then


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020428'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ViewCosts')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:JOBPAYMT.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:TRACHRGE.UseFile
  Access:SUBCHRGE.UseFile
  Access:STDCHRGE.UseFile
  Access:CHARTYPE.UseFile
  Access:ESTPARTS.UseFile
  Access:INVOICE.UseFile
  Access:JOBTHIRD.UseFile
  Access:REPTYDEF.UseFile
  Access:LOCATLOG.UseFile
  Access:TRDPARTY.UseFile
  Access:MODELNUM.UseFile
  Access:JOBSINV.UseFile
  Access:JOBSE2.UseFile
  Access:MANUFACT.UseFile
  Access:AUDIT2.UseFile
  SELF.FilesOpened = True
  !Save Variables
      save:IgnoreARCC = job:Ignore_Chargeable_Charges
      save:IgnoreRRCC = jobe:IgnoreRRCChaCosts
      save:IgnoreARCW = job:Ignore_Warranty_Charges
      save:IgnoreRRCW = jobe:IgnoreRRCWarCosts
      save:IgnoreARCE = job:Ignore_Estimate_Charges
      save:IgnoreRRCE = jobe:IgnoreRRCEstCosts
      save:IgnoreClaim    = jobe:IgnoreClaimCosts
      save:Ignore3rdParty   = jobe:Ignore3rdPartyCosts
  
      ! #11838 Get the trade site location to get the user's site location. (Bryan: 08/12/2010)
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          tmp:ARCLocation = tra:SiteLocation
      END
  
  
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:JOBSINV,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Save_JOBSE_ID = Access:JOBSE.SaveFile()
  Save_JOBS_ID = Access:JOBS.SaveFile()
  Access:JOBSE.Clearkey(jobe:RefNumberKey)
  jobe:RefNumber  = job:Ref_Number
  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Found
  
  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Error
  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
  Save_JOBSE2_ID = Access:JOBSE2.SaveFile()
  Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
  jobe2:RefNumber = job:Ref_Number
  If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
  
  !added by Paul 04/10/2010 - Log no 11691
  
  If jobe2:JobDiscountAmnt > 0 then
      !there is a value for this - so show it
      ?jobe2:JobDiscountAmnt:Prompt{prop:Hide} = False
      ?jobe2:JobDiscountAmnt{prop:Hide} = False
  Else
      !no value - hide it
      ?jobe2:JobDiscountAmnt:Prompt{prop:Hide} = True
      ?jobe2:JobDiscountAmnt{prop:Hide} = True
  End
  
  if jobe2:InvDiscountAmnt > 0 then
      !there is a value for this - so show it
      ?jobe2:InvDiscountAmnt:Prompt{prop:Hide} = False
      ?jobe2:InvDiscountAmnt{prop:Hide} = False
  Else
      !no value - hide it
      ?jobe2:InvDiscountAmnt:Prompt{prop:Hide} = True
      ?jobe2:InvDiscountAmnt{prop:Hide} = True
  End
  
  !find out the default labour cost
  
  DO DefaultLabourCost
  
  
      ! Save Window Name
   AddToLog('Window','Open','ViewCosts')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW9.Q &= Queue:Browse
  BRW9.AddSortOrder(,jov:RefNumberKey)
  BRW9.AddRange(jov:RefNumber,job:Ref_Number)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,jov:RecordNumber,1,BRW9)
  BIND('tmp:CreditNumber',tmp:CreditNumber)
  BIND('tmp:CreditType',tmp:CreditType)
  BIND('tmp:CreditAmount',tmp:CreditAmount)
  BRW9.AddField(tmp:CreditNumber,BRW9.Q.tmp:CreditNumber)
  BRW9.AddField(tmp:CreditType,BRW9.Q.tmp:CreditType)
  BRW9.AddField(jov:DateCreated,BRW9.Q.jov:DateCreated)
  BRW9.AddField(tmp:CreditAmount,BRW9.Q.tmp:CreditAmount)
  BRW9.AddField(jov:RecordNumber,BRW9.Q.jov:RecordNumber)
  BRW9.AddField(jov:RefNumber,BRW9.Q.jov:RefNumber)
  IF ?jobe:IgnoreRRCChaCosts{Prop:Checked} = True
    UNHIDE(?jobe2:JobDiscountAmnt:Prompt)
    UNHIDE(?jobe2:JobDiscountAmnt)
  END
  IF ?jobe:IgnoreRRCChaCosts{Prop:Checked} = False
    HIDE(?jobe2:JobDiscountAmnt:Prompt)
    HIDE(?jobe2:JobDiscountAmnt)
  END
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:JOBPAYMT.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ViewCosts')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button:SaveChanges
          !added by Paul 05/10/2010 - Log no 11691
          If (?RRCChargeableTab{prop:Hide} = 0 And jobe:IgnoreRRCChaCosts = 1)
              ! #11773 Only check if the Char Tab is actually visible. (Bryan: 28/10/2010)
              If job:Chargeable_Job = 'YES' then
                  If jobe:RRCCLabourCost > LocalDefaultLabour  then
                      miss# = missive('Error! Override Labour Cost is Higher than Default Cost.','Servicebase 3g Error','Mstop.jpg','/OK')
                      cycle
                  End
              End
          End ! If (?RRCChargeableTab{prop:Hide} = 0)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020428'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020428'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020428'&'0')
      ***
    OF ?job:Ignore_Chargeable_Charges
      ! Changing (DBH 31/08/2006) # 8084 - Record change if "Save Changes" button is pressed.
      !    Case job:Ignore_Chargeable_Charges
      !        Of 'NO'
      !            save:IgnoreARCC = 'NO'
      !        Of 'YES'
      !            If job:Ignore_Chargeable_Charges <> save:IgnoreARCC
      !                glo:EDI_Reason = ''
      !                Get_EDI_Reason
      !                If glo:EDI_Reason <> ''
      !                    save:IgnoreARCC = 'YES'
      !                    If Access:AUDIT.PrimeRecord() = Level:Benign
      !                        aud:Notes         = 'REASON: ' & Clip(Upper(glo:EDI_Reason))
      !                        aud:Ref_Number    = job:ref_number
      !                        aud:Date          = Today()
      !                        aud:Time          = Clock()
      !                        aud:Type          = 'JOB'
      !                        Access:USERS.ClearKey(use:Password_Key)
      !                        use:Password      = glo:Password
      !                        Access:USERS.Fetch(use:Password_Key)
      !                        aud:User          = use:User_Code
      !                        aud:Action        = 'IGNORE DEFAULT ARC CHARGEABLE COSTS'
      !                        Access:AUDIT.Insert()
      !                    End!If Access:AUDIT.PrimeRecord() = Level:Benign
      !                Else !If glo:EDI_Reason <> ''
      !                    job:Ignore_Chargeable_Charges = 'NO'
      !                End !If glo:EDI_Reason <> ''
      !            End !If job:Ignore_Chargeable_Charges <> save:Ignore
      !    End !job:Ignore_Chargeable_Charges
      ! to (DBH 31/08/2006) # 8084
          job:Ignore_Chargeable_Charges = local.AddToTempQueue(job:Ignore_Chargeable_Charges,save:IgnoreARCC,'IGNORE DEFAULT ARC CHARGEABLE COSTS','YES','NO',1,job:Labour_Cost)
      ! End (DBH 31/08/2006) #8084
      Local.Pricing(0)
    OF ?job:Courier_Cost
      Local.Pricing(0)
    OF ?job:Labour_Cost
      Local.Pricing(0)
    OF ?job:Ignore_Warranty_Charges
      Case job:Ignore_Warranty_Charges
          Of 'NO'
              save:IgnoreARCW = 'NO'
              !If job completed, then try and get the previous price - L945 (DBH: 08-09-2003)
              If job:Date_Completed <> ''
                  Save_aud_ID = Access:AUDIT.SaveFile()
                  Access:AUDIT.ClearKey(aud:TypeActionKey)
                  aud:Ref_Number = job:Ref_Number
                  aud:Type       = 'JOB'
                  aud:Action     = 'IGNORE DEFAULT ARC WARRANTY COSTS'
                  aud:Date       = Today()
                  Set(aud:TypeActionKey,aud:TypeActionKey)
                  Loop
                      If Access:AUDIT.NEXT()
                         Break
                      End !If
                      If aud:Ref_Number <> job:Ref_Number      |
                      Or aud:Type       <> 'JOB'      |
                      Or aud:Action     <> 'IGNORE DEFAULT ARC WARRANTY COSTS'      |
                          Then Break.  ! End If
      
                      Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                      aud2:AUDRecordNumber = aud:Record_Number
                      IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                          pos# = 0
                          pos# = Instring('PREVIOUS CHARGE:',aud2:Notes,1,1)
                          If pos#
                              job:Labour_Cost_Warranty = Sub(aud2:Notes,pos# + 17,30)
                          End !If pos#
                          Break
                      END ! IF
      
                  End !Loop
                  Access:AUDIT.RestoreFile(Save_aud_ID)
              End !If job:Date_Completed <> ''
          Of 'YES'
      ! Changing (DBH 31/08/2006) # 8084 - Save the changes later
      !            If job:Ignore_Warranty_Charges <> save:IgnoreARCW
      !                glo:EDI_Reason = ''
      !                Get_EDI_Reason
      !                If glo:EDI_Reason <> ''
      !                    save:IgnoreARCW = 'YES'
      !                    If Access:AUDIT.PrimeRecord() = Level:Benign
      !                        aud:Notes         = 'REASON: ' & Clip(Upper(glo:EDI_Reason))
      !                        !Save the previous cost if ignore ticked
      !                        !when the job is competed. This is so the cost
      !                        !can be reverted if the box is unticked - L945 (DBH: 08-09-2003)
      !                        If job:Date_Completed <> ''
      !                            aud:Notes = Clip(aud:Notes) & '<13,10>PREVIOUS CHARGE: ' & Format(job:Labour_Cost_Warranty,@n14.2)
      !                        End !If job:Date_Compelted <> ''
      !                        aud:Ref_Number    = job:ref_number
      !                        aud:Date          = Today()
      !                        aud:Time          = Clock()
      !                        aud:Type          = 'JOB'
      !                        Access:USERS.ClearKey(use:Password_Key)
      !                        use:Password      = glo:Password
      !                        Access:USERS.Fetch(use:Password_Key)
      !                        aud:User          = use:User_Code
      !                        aud:Action        = 'IGNORE DEFAULT ARC WARRANTY COSTS'
      !                        Access:AUDIT.Insert()
      !                    End!If Access:AUDIT.PrimeRecord() = Level:Benign
      !                Else !If glo:EDI_Reason <> ''
      !                    job:Ignore_Warranty_Charges = 'NO'
      !                End !If glo:EDI_Reason <> ''
      !            End !If job:Ignore_Chargeable_Charges <> save:Ignore
      ! to (DBH 31/08/2006) # 8084
              job:Ignore_Warranty_Charges = local.AddToTempQueue(job:Ignore_Warranty_Charges,save:IgnoreARCW,'IGNORE DEFAULT ARC WARRANTY COSTS','YES','NO',1,job:Labour_Cost_Warranty)
      ! End (DBH 31/08/2006) #8084
      
      End !job:Ignore_Chargeable_Charges
      Local.Pricing(0)
    OF ?job:Courier_Cost_Warranty
      Local.Pricing(0)
    OF ?job:Labour_Cost_Warranty
      Local.Pricing(0)
    OF ?jobe:IgnoreClaimCosts
      Case jobe:IgnoreClaimCosts
          Of 0
              save:IgnoreCLaim = 0
              !If job completed, then try and get the previous price - L945 (DBH: 08-09-2003)
              If job:Date_Completed <> ''
                  Save_aud_ID = Access:AUDIT.SaveFile()
                  Access:AUDIT.ClearKey(aud:TypeActionKey)
                  aud:Ref_Number = job:Ref_Number
                  aud:Type       = 'JOB'
                  aud:Action     = 'IGNORE DEFAULT CLAIM COSTS'
                  aud:Date       = Today()
                  Set(aud:TypeActionKey,aud:TypeActionKey)
                  Loop
                      If Access:AUDIT.NEXT()
                         Break
                      End !If
                      If aud:Ref_Number <> job:Ref_Number      |
                      Or aud:Type       <> 'JOB'      |
                      Or aud:Action     <> 'IGNORE DEFAULT CLAIM COSTS'      |
                          Then Break.  ! End If
                      Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                      aud2:AUDRecordNumber = aud:Record_Number
                      IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                          pos# = 0
                          pos# = Instring('PREVIOUS CHARGE:',aud2:Notes,1,1)
                          If pos#
                              jobe:ClaimValue = Sub(aud2:Notes,pos# + 17,30)
                          End !If pos#
                          Break
      
                      END ! IF
                  End !Loop
                  Access:AUDIT.RestoreFile(Save_aud_ID)
              End !If job:Date_Completed <> ''
          Of 1
      ! Changing (DBH 31/08/2006) # 8084 - Save the costs at the end
      !        If jobe:IgnoreClaimCosts <> save:IgnoreClaim
      !            glo:EDI_Reason = ''
      !            Get_EDI_Reason
      !            If glo:EDI_Reason <> ''
      !                save:IgnoreClaim = 1
      !                If Access:AUDIT.PrimeRecord() = Level:Benign
      !                    aud:Notes         = 'REASON: ' & Clip(Upper(glo:EDI_Reason))
      !                    !Save the previous cost if ignore ticked
      !                    !when the job is competed. This is so the cost
      !                    !can be reverted if the box is unticked - L945 (DBH: 08-09-2003)
      !                    If job:Date_Completed <> ''
      !                        aud:Notes = Clip(aud:Notes) & '<13,10>PREVIOUS CHARGE: ' & Format(jobe:ClaimValue,@n14.2)
      !                    End !If job:Date_Compelted <> ''
      !                    aud:Ref_Number    = job:ref_number
      !                    aud:Date          = Today()
      !                    aud:Time          = Clock()
      !                    aud:Type          = 'JOB'
      !                    Access:USERS.ClearKey(use:Password_Key)
      !                    use:Password      = glo:Password
      !                    Access:USERS.Fetch(use:Password_Key)
      !                    aud:User          = use:User_Code
      !                    aud:Action        = 'IGNORE DEFAULT CLAIM COSTS'
      !                    Access:AUDIT.Insert()
      !                End!If Access:AUDIT.PrimeRecord() = Level:Benign
      !            Else !If glo:EDI_Reason <> ''
      !                jobe:IgnoreClaimCosts = 0
      !            End !If glo:EDI_Reason <> ''
      !        End !If job:Ignore_Chargeable_Charges <> save:Ignore
      ! to (DBH 31/08/2006) # 8084
          jobe:IgnoreClaimCosts = local.AddToTempQueue(jobe:IgnoreClaimCosts,save:IgnoreClaim,'IGNORE DEFAULT CLAIM COSTS',1,0,1,jobe:ClaimValue)
      ! End (DBH 31/08/2006) #8084
      End !job:Ignore_Chargeable_Charges
      Local.Pricing(0)
    OF ?job:Courier_Cost_Warranty:2
      Local.Pricing(0)
    OF ?jobe:ClaimValue
      Local.Pricing(0)
    OF ?jobe:Ignore3rdPartyCosts
      ! Changing (DBH 31/08/2006) # 8084 - Save the changes to the end
      !Case jobe:Ignore3rdPartyCosts
      !    Of 0
      !        save:Ignore3rdParty = 0
      !    Of 1
      !        If jobe:Ignore3rdPartyCosts <> save:Ignore3rdParty
      !            glo:EDI_Reason = ''
      !            Get_EDI_Reason
      !            If glo:EDI_Reason <> ''
      !                save:Ignore3rdParty = 1
      !                If Access:AUDIT.PrimeRecord() = Level:Benign
      !                    aud:Notes         = 'REASON: ' & Clip(Upper(glo:EDI_Reason))
      !                    aud:Ref_Number    = job:ref_number
      !                    aud:Date          = Today()
      !                    aud:Time          = Clock()
      !                    aud:Type          = 'JOB'
      !                    Access:USERS.ClearKey(use:Password_Key)
      !                    use:Password      = glo:Password
      !                    Access:USERS.Fetch(use:Password_Key)
      !                    aud:User          = use:User_Code
      !                    aud:Action        = 'IGNORE DEFAULT 3RD PARTY COSTS'
      !                    Access:AUDIT.Insert()
      !                End!If Access:AUDIT.PrimeRecord() = Level:Benign
      !            Else !If glo:EDI_Reason <> ''
      !                jobe:Ignore3rdPartyCosts = 0
      !            End !If glo:EDI_Reason <> ''
      !        End !If job:Ignore_Chargeable_Charges <> save:Ignore
      !
      !End !job:Ignore_Chargeable_Charges
      ! to (DBH 31/08/2006) # 8084
      jobe:Ignore3rdPartyCosts = local.AddToTempQueue(jobe:Ignore3rdPartyCosts,save:Ignore3rdParty,'IGNORE DEFAULT 3RD PARTY COSTS',1,0,1,jobe:ARC3rdPartyCost)
      ! End (DBH 31/08/2006) #8084
      Local.Pricing(0)
    OF ?jobe:ARC3rdPartyCost
      Local.Pricing(0)
    OF ?jobe:ARC3rdPartyMarkup
      Local.Pricing(0)
    OF ?job:Ignore_Estimate_Charges
      ! Changing (DBH 31/08/2006) # 8084 - Save the changes until the end
      !Case job:Ignore_Estimate_Charges
      !    Of 'NO'
      !        save:IgnoreARCE = 'NO'
      !
      !    Of 'YES'
      !        If job:Ignore_Estimate_Charges <> save:IgnoreARCE
      !            glo:EDI_Reason = ''
      !            Get_EDI_Reason
      !            If glo:EDI_Reason <> ''
      !                save:IgnoreARCE = 'YES'
      !                If Access:AUDIT.PrimeRecord() = Level:Benign
      !                    aud:Notes         = 'REASON: ' & Clip(Upper(glo:EDI_Reason))
      !                    aud:Ref_Number    = job:ref_number
      !                    aud:Date          = Today()
      !                    aud:Time          = Clock()
      !                    aud:Type          = 'JOB'
      !                    Access:USERS.ClearKey(use:Password_Key)
      !                    use:Password      = glo:Password
      !                    Access:USERS.Fetch(use:Password_Key)
      !                    aud:User          = use:User_Code
      !                    aud:Action        = 'IGNORE DEFAULT ARC ESTIMATE COSTS'
      !                    Access:AUDIT.Insert()
      !                End!If Access:AUDIT.PrimeRecord() = Level:Benign
      !            Else !If glo:EDI_Reason <> ''
      !                job:Ignore_Estimate_Charges = 'NO'
      !            End !If glo:EDI_Reason <> ''
      !        End !If job:Ignore_Chargeable_Charges <> save:Ignore
      !
      !
      !End !job:Ignore_Chargeable_Charges
      ! to (DBH 31/08/2006) # 8084
      job:Ignore_Estimate_Charges = local.AddToTempQueue(job:Ignore_Estimate_Charges,save:IgnoreARCE,'IGNORE DEFAULT ARC ESTIMATE COSTS','YES','NO',1,job:Labour_Cost_Estimate)
      ! End (DBH 31/08/2006) #8084
      Local.Pricing(0)
    OF ?job:Courier_Cost_Estimate
      Local.Pricing(0)
    OF ?job:Labour_Cost_Estimate
      Local.Pricing(0)
    OF ?jobe:IgnoreRRCChaCosts
      IF ?jobe:IgnoreRRCChaCosts{Prop:Checked} = True
        UNHIDE(?jobe2:JobDiscountAmnt:Prompt)
        UNHIDE(?jobe2:JobDiscountAmnt)
      END
      IF ?jobe:IgnoreRRCChaCosts{Prop:Checked} = False
        HIDE(?jobe2:JobDiscountAmnt:Prompt)
        HIDE(?jobe2:JobDiscountAmnt)
      END
      ThisWindow.Reset
      ! Changing (DBH 31/08/2006) # 8084 - Save the changed until later
      !Case jobe:IgnoreRRCChaCosts
      !    Of 0
      !        save:IgnoreRRCC = 0
      !    Of 1
      !        If jobe:IgnoreRRCChaCosts <> save:IgnoreRRCC
      !            glo:EDI_Reason = ''
      !            Get_EDI_Reason
      !            If glo:EDI_Reason <> ''
      !                save:IgnoreRRCC = 1
      !                If Access:AUDIT.PrimeRecord() = Level:Benign
      !                    aud:Notes         = 'REASON: ' & Clip(Upper(glo:EDI_Reason))
      !                    aud:Ref_Number    = job:ref_number
      !                    aud:Date          = Today()
      !                    aud:Time          = Clock()
      !                    aud:Type          = 'JOB'
      !                    Access:USERS.ClearKey(use:Password_Key)
      !                    use:Password      = glo:Password
      !                    Access:USERS.Fetch(use:Password_Key)
      !                    aud:User          = use:User_Code
      !                    aud:Action        = 'IGNORE DEFAULT RRC CHARGEABLE COSTS'
      !                    Access:AUDIT.Insert()
      !                End!If Access:AUDIT.PrimeRecord() = Level:Benign
      !            Else !If glo:EDI_Reason <> ''
      !                jobe:IgnoreRRCChaCosts = 0
      !            End !If glo:EDI_Reason <> ''
      !        End !If job:Ignore_Chargeable_Charges <> save:Ignore
      !
      !End !job:Ignore_Chargeable_Charges
      ! to (DBH 31/08/2006) # 8084
      jobe:IgnoreRRCChaCosts = local.AddToTempQueue(jobe:IgnoreRRCChaCosts,save:IgnoreRRCC,'IGNORE DEFAULT RRC CHARGEABLE COSTS',1,0,1,jobe:RRCCLabourCost)
      ! End (DBH 31/08/2006) #8084
      Local.Pricing(0)
    OF ?job:Courier_Cost:2
      Local.Pricing(0)
    OF ?jobe:RRCCLabourCost
      Local.Pricing(0)
      
      !added by Paul 04/10/2010 - Log no 11691
      If (?RRCChargeableTab{prop:Hide} = 0 And jobe:IgnoreRRCChaCosts = 1)
          ! #11875 Only check if the Char Tab is actually visible. (Bryan: 05/01/2011)
          If jobe:RRCCLabourCost > LocalDefaultLabour then
              miss# = missive('Error! Override Labour Cost is Higher than Default Cost.','Servicebase 3g Error','Mstop.jpg','/OK')
          else
              !work out what the discount should be
              jobe2:JobDiscountAmnt = LocalDefaultLabour - jobe:RRCCLabourCost
              LocalDiscountAmount = jobe2:JobDiscountAmnt
              display()
          End
      END
      
    OF ?jobe:IgnoreRRCWarCosts
      Case jobe:IgnoreRRCWarCosts
          Of 0
              save:IgnoreRRCW = 0
              !If job completed, then try and get the previous price - L945 (DBH: 08-09-2003)
              If job:Date_Completed <> ''
                  Save_aud_ID = Access:AUDIT.SaveFile()
                  Access:AUDIT.ClearKey(aud:TypeActionKey)
                  aud:Ref_Number = job:Ref_Number
                  aud:Type       = 'JOB'
                  aud:Action     = 'IGNORE DEFAULT RRC WARRANTY COSTS'
                  aud:Date       = Today()
                  Set(aud:TypeActionKey,aud:TypeActionKey)
                  Loop
                      If Access:AUDIT.NEXT()
                         Break
                      End !If
                      If aud:Ref_Number <> job:Ref_Number      |
                      Or aud:Type       <> 'JOB'      |
                      Or aud:Action     <> 'IGNORE DEFAULT RRC WARRANTY COSTS'      |
                          Then Break.  ! End If
                      Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                      aud2:AUDRecordNumber = aud:Record_Number
                      IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                          pos# = 0
                          pos# = Instring('PREVIOUS CHARGE:',aud2:Notes,1,1)
                          If pos#
                              jobe:RRCWLabourCost = Sub(aud2:Notes,pos# + 17,30)
                          End !If pos#
                          Break
      
                      END ! IF
                  End !Loop
                  Access:AUDIT.RestoreFile(Save_aud_ID)
              End !If job:Date_Completed <> ''
          Of 1
      ! Changing (DBH 31/08/2006) # 8084 - Save the changes until the end
      !        If jobe:IgnoreRRCWarCosts <> save:IgnoreRRCW
      !            glo:EDI_Reason = ''
      !            Get_EDI_Reason
      !            If glo:EDI_Reason <> ''
      !                save:IgnoreRRCW = 'YES'
      !                If Access:AUDIT.PrimeRecord() = Level:Benign
      !                    aud:Notes         = 'REASON: ' & Clip(Upper(glo:EDI_Reason))
      !                    !Save the previous cost if ignore ticked
      !                    !when the job is competed. This is so the cost
      !                    !can be reverted if the box is unticked - L945 (DBH: 08-09-2003)
      !                    If job:Date_Completed <> ''
      !                        aud:Notes = Clip(aud:Notes) & '<13,10>PREVIOUS CHARGE: ' & Format(jobe:RRCWLabourCost,@n14.2)
      !                    End !If job:Date_Compelted <> ''
      !                    aud:Ref_Number    = job:ref_number
      !                    aud:Date          = Today()
      !                    aud:Time          = Clock()
      !                    aud:Type          = 'JOB'
      !                    Access:USERS.ClearKey(use:Password_Key)
      !                    use:Password      = glo:Password
      !                    Access:USERS.Fetch(use:Password_Key)
      !                    aud:User          = use:User_Code
      !                    aud:Action        = 'IGNORE DEFAULT RRC WARRANTY COSTS'
      !                    Access:AUDIT.Insert()
      !                End!If Access:AUDIT.PrimeRecord() = Level:Benign
      !            Else !If glo:EDI_Reason <> ''
      !                jobe:IgnoreRRCWarCosts = 0
      !            End !If glo:EDI_Reason <> ''
      !        End !If job:Ignore_Chargeable_Charges <> save:Ignore
      ! to (DBH 31/08/2006) # 8084
              jobe:IgnoreRRCWarCosts = local.AddToTempQueue(jobe:IgnoreRRCWarCosts,save:IgnoreRRCW,'IGNORE DEFAULT RRC WARRANTY COSTS',1,0,1,jobe:RRCWLabourCost)
      ! End (DBH 31/08/2006) #8084
      
      End !job:Ignore_Chargeable_Charges
      Local.Pricing(0)
    OF ?jobe:RRCWLabourCost
      Local.Pricing(0)
    OF ?jobe:IgnoreRRCEstCosts
      ! Changing (DBH 31/08/2006) # 8084 - Save the changes at the end
      !Case jobe:IgnoreRRCEstCosts
      !    Of 0
      !        save:IgnoreRRCE = 0
      !
      !    Of 1
      !        If jobe:IgnoreRRCEstCosts <> save:IgnoreRRCE
      !            glo:EDI_Reason = ''
      !            Get_EDI_Reason
      !            If glo:EDI_Reason <> ''
      !                save:IgnoreRRCE = 1
      !                If Access:AUDIT.PrimeRecord() = Level:Benign
      !                    aud:Notes         = 'REASON: ' & Clip(Upper(glo:EDI_Reason))
      !                    aud:Ref_Number    = job:ref_number
      !                    aud:Date          = Today()
      !                    aud:Time          = Clock()
      !                    aud:Type          = 'JOB'
      !                    Access:USERS.ClearKey(use:Password_Key)
      !                    use:Password      = glo:Password
      !                    Access:USERS.Fetch(use:Password_Key)
      !                    aud:User          = use:User_Code
      !                    aud:Action        = 'IGNORE DEFAULT RRC ESTIMATE COSTS'
      !                    Access:AUDIT.Insert()
      !                End!If Access:AUDIT.PrimeRecord() = Level:Benign
      !            Else !If glo:EDI_Reason <> ''
      !                jobe:IgnoreRRCEstCosts = 0
      !            End !If glo:EDI_Reason <> ''
      !        End !If job:Ignore_Chargeable_Charges <> save:Ignore
      !
      !
      !
      !End !job:Ignore_Chargeable_Charges
      ! to (DBH 31/08/2006) # 8084
      jobe:IgnoreRRCEstCosts = local.AddToTempQueue(jobe:IgnoreRRCEstCosts,save:IgnoreRRCE,'IGNORE DEFAULT RRC ESTIMATE COSTS',1,0,1,jobe:RRCELabourCost)
      ! End (DBH 31/08/2006) #8084
      Local.Pricing(0)
    OF ?job:Courier_Cost_Estimate:2
      Local.Pricing(0)
    OF ?jobe:RRCELabourCost
      Local.Pricing(0)
    OF ?jobe:ExcReplcamentCharge
      If ~0{prop:AcceptAll}
          If jobe:ExcReplcamentCharge
              !Do not show the Chargeable Tab if the job has been invoiced - 3865 (DBH: 11-12-2003)
              If ?RRCCInvoiceTab{prop:Hide} = 0
                  ?tmprc:CourierCost:Prompt:4{prop:Text} = 'Exchange Repl'
                  ?tmprc:CourierCost:Prompt:4{prop:Hide} = 0
                  ?job:Invoice_Courier_Cost:2{prop:Hide} = 0
              Else !If ?RRCCInvoiceTab{prop:Hide} = 0
                  ?RRCChargeableTab{prop:Hide} = 0
                  ?job:Courier_Cost:2{prop:Hide} = 0
                  ?tmprc:CourierCost:Prompt{prop:Hide} = 0
                  ?tmprc:CourierCost:Prompt{prop:Text} = 'Exchange Repl'
      
                  Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                  xch:Ref_Number  = job:Exchange_Unit_Number
                  If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                      !Found
                      Access:MODELNUM.ClearKey(mod:Model_Number_Key)
                      mod:Model_Number = xch:Model_Number
                      If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                          !Found
                          job:Courier_Cost = mod:ReplacementValue
      
                      Else !If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                          !Error
                      End !If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
      
                  Else ! If Access:EXCHANGE.Tryfetch(xch:RefNumberKey) = Level:Benign
                      !Error
                  End !If Access:EXCHANGE.Tryfetch(xch:RefNumberKey) = Level:Benign
              End !If ?RRCCInvoiceTab{prop:Hide} = 0
          Else !If jobe:ExcReplcementCharge
      !        ?RRCChargeableTab{prop:Hide} = 1
              job:Courier_Cost = 0
          End !If jobe:ExcReplcementCharge
          UPDATE()
          DISPLAY()
          Local.Pricing(0)
      End !0{prop:AcceptAll}
      Do ShowHideTabs
    OF ?Button:Cancel
      ThisWindow.Update
      tmp:Close = 1
      Access:JOBSE.RestoreFile(Save_JOBSE_ID)
      Access:JOBS.RestoreFile(Save_JOBS_ID)
      Access:JOBSE2.RestoreFile(Save_JOBSE2_ID)
      Post(Event:CloseWindow)
    OF ?Button:SaveChanges
      ThisWindow.Update
      Do SaveChanges
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
      If tmp:Close = 0
          Cycle
      End !tmp:Close = 0
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      Local.Pricing(0)
      Do ShowHideTabs
      
      ! Inserting (DBH 01/09/2006) # 8084 - Do not allow to alter the costs if the job is "view only"
      If glo:Preview = 'V'
          ?Button:SaveChanges{prop:Disable} = True
          ?job:Ignore_Chargeable_Charges{prop:Disable} = True
          ?job:Ignore_Warranty_charges{prop:Disable} = True
          ?jobe:IgnoreClaimCosts{prop:Disable} = True
          ?jobe:Ignore3rdPartyCosts{prop:Disable} = True
          ?job:Ignore_Estimate_Charges{prop:Disable} = True
          ?jobe:IgnoreRRCChaCosts{prop:Disable} = True
          ?jobe:IgnoreRRCWarCosts{prop:Disable} = True
          ?jobe:IgnoreRRCEstCosts{prop:Disable} = True
      End ! If glo:Preview = 'V'
      ! End (DBH 01/09/2006) #8084
      ! #116725 Don't allow access if access level not applied. (Bryan: 31/08/2010)
      If SecurityCheck('IGNORE DEFAULT CHARGE')
          ?job:Ignore_Chargeable_Charges{prop:Disable} = True
          ?job:Ignore_Warranty_charges{prop:Disable} = True
          ?jobe:IgnoreClaimCosts{prop:Disable} = True
          ?jobe:Ignore3rdPartyCosts{prop:Disable} = True
          ?job:Ignore_Estimate_Charges{prop:Disable} = True
          ?jobe:IgnoreRRCChaCosts{prop:Disable} = True
          ?jobe:IgnoreRRCWarCosts{prop:Disable} = True
          ?jobe:IgnoreRRCEstCosts{prop:Disable} = True
      end
      
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

local.AddToTempQueue        Procedure(String f:Field,String f:SaveField,String f:Type,String f:True,String f:False,Byte f:SaveCost,Real f:Cost)
Code
    If f:Field <> f:SaveField
        If f:Field = f:True
            glo:EDI_Reason = ''
            Get_EDI_Reason
            If Clip(glo:EDI_Reason) <> ''
                tmpque:Field = f:Type
                Get(TempQueue,tmpque:Field)
                If Error()
                    Clear(TempQueue)
                    tmpque:Field = f:Type
                    tmpque:Reason = glo:EDI_Reason
                    If f:SaveCost
                        tmpque:SaveCost = 1
                        tmpque:Cost = f:Cost
                    End ! If f:SaveCost
                    Add(TempQueue)
                Else ! If Error
                    tmpque:Reason = glo:EDI_Reason
                    Put(TempQueue)
                End ! If Error
                Return f:True
            Else ! If Clip(glo:EDI_Reason) <> ''
                Return f:False
            End ! If Clip(glo:EDI_Reason) <> ''
        Else ! If f:Field = f:True
            tmpque:Field = f:Type
            Get(TempQueue,tmpque:Field)
            If ~Error()
                Delete(TempQueue)
            End ! If ~Error()
        End ! If f:Field = f:True
    Else ! If f:Field <> f:SaveField
        tmpque:Field = f:Type
        Get(TempQueue,tmpque:Field)
        If ~Error()
            Delete(TempQueue)
        End ! If ~Error()
    End ! If f:Field <> f:SaveField

    Return f:Field
Local.Pricing     Procedure(Byte func:Force)
Code
    !This code is working out the parts cost and showing,
    !eve
    ?RRCInvoiceNumber{prop:Text} = ''
    ?ARCInvoiceNumber{prop:Text} = ''
    ?ARCWarrantyInvoiceNumber{prop:Text} = ''

    !Add new prototype - L945 (DBH: 04-09-2003)
    JobPricingRoutine(func:Force)

    If job:warranty_job = 'YES'
        If job:Invoice_Number_Warranty <> 0
            tmp:HandlingFee = jobe:InvoiceHandlingFee
            tmp:ExchangeFee = jobe:InvoiceExchangeRate

            ?ARCWarrantyInvoiceNumber{prop:Text} = 'Inv No: ' & Clip(inv:Invoice_Number)

            Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
            inv:Invoice_Number = job:Invoice_Number_Warranty
            If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
                !Found

                !Warranty Invoice Charges ARC
                !Invoice Courier Cost = Exchange Fee (if applicable)
                !Invoice Parts Cost
                !Invoice Labour Cost

                tmpiaw:vat  = job:WInvoice_Courier_Cost * (inv:Vat_Rate_Labour/100) + |
                                job:WInvoice_Parts_Cost * (inv:Vat_Rate_Parts/100) + |
                                job:WInvoice_Labour_Cost * (inv:Vat_Rate_Labour/100)

                tmpiaw:Total = tmpiaw:Vat + job:WInvoice_Sub_total

                !Warranty Invoice Charges RRC
                !Invoice Parts Cost
                !Invoice Labour Cost

                !Sub Total was not always filled in when the job were invoiced...
                !This should fix that problem, eventually.
                !If jobe:InvRRCWSubTotal = 0


                !Warranty Claim Charges ARC
                !Invoice Courier Cost = Exchange Fee (If applicable)
                !Invoice Parts Cost
                !Invoice Claim Cost (Claimed Labour Cost)

                tmpial:VAT     = job:WInvoice_Courier_Cost * (inv:Vat_Rate_Labour/100) + |
                                jobe:InvClaimPartsCost * (inv:Vat_Rate_Parts/100) + |
                                jobe:InvoiceClaimValue * (inv:Vat_Rate_Labour/100)

                tmpial:SubTotal = job:WInvoice_Courier_Cost + jobe:InvClaimPartsCost + |
                                    jobe:InvoiceClaimValue

                tmpial:Total    = tmpial:SubTotal + tmpial:VAT

                IF (_PreClaimThirdParty(job:Third_party_site,job:Manufacturer))
                    tmpial:Total += jobe2:InvThirdPartyHandlingFee   ! #12363 Add third party value IF used (DBH: 16/02/2012)
                END

                !Warranty Claim Adjustment ARC
                !Labour ADjustment
                !Parts Adjustment
                !Exchange ADjustment (Which will be the adjusted courier cost)

                tmpcla:SubTotal     = jobe:LabourAdjustment + jobe:ExchangeAdjustment + |
                                        jobe:PartsAdjustment

                tmpcla:VAT          = jobe:PartsAdjustment * (inv:Vat_Rate_Parts/100) + |
                                    jobe:LabourAdjustment * (inv:Vat_Rate_Labour/100) +|
                                    jobe:ExchangeAdjustment * (inv:Vat_Rate_Labour/100)

                tmpcla:Total        = tmpcla:SubTotal + tmpcla:VAT
            Else!If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
        Else
            tmp:HandlingFee = jobe:HandlingFee
            tmp:ExchangeFee = jobe:ExchangeRate
        End !If job:Invoice_Number_Warranty = 0


    End!If job:warranty_job = 'YES'

    If job:chargeable_job = 'YES'
        Clear(PricingGroup)
        If job:Invoice_Number <> 0
            tmp:HandlingFee = jobe:InvoiceHandlingFee
            tmp:ExchangeFee = jobe:InvoiceExchangeRate

            Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
            inv:Invoice_Number = job:Invoice_Number
            If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
                !Found
                !If RRC invoice not been printed, then show the costs that can be changed.
                Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                tra:Account_Number  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
                If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Found
                    ?ARCInvoiceNumber{prop:Text} = 'Inv No: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & ' (' &CLIP(Format(inv:Date_Created,@d6))& ')'
                Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    !Error
                End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

                If inv:ExportedRRCOracle
                    jobe:InvRRCCSubTotal = job:Invoice_Courier_Cost + |
                                            jobe:InvRRCCLabourCost + |
                                            jobe:InvRRCCPartsCost

                    tmpirc:vat  = job:Invoice_Courier_Cost * (inv:Vat_Rate_Labour/100) + |
                                    jobe:InvRRCCLabourCost * (inv:Vat_Rate_Labour/100) + |
                                    jobe:InvRRCCPartsCost * (inv:Vat_Rate_Parts/100)

                    tmpirc:Total    = tmpirc:Vat + jobe:InvRRCCSubTotal
                    Access:WEBJOB.Clearkey(wob:RefNumberKey)
                    wob:RefNumber   = job:Ref_Number
                    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                        !Found
                        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                        tra:Account_Number  = wob:HeadAccountNumber
                        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                            !Found
                            ?RRCInvoiceNumber{prop:Text} = 'Inv No: ' & Clip(inv:Invoice_Number) & '-' & Clip(tra:BranchIdentification) & ' (' &CLIP(Format(inv:RRCInvoiceDate,@d6))& ')'
                            If job:Exchange_Unit_Number <> 0
                                ?RRCInvoiceNumber{prop:Text} = Clip(?RRCInvoiceNumber{prop:Text}) & ' Exchange'
                            End !If job:Exchange_Unit_Number <> 0
                        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                            !Error
                        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                        !Error
                    End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

                End !If inv:ExportedRRCOracle

                tmpiac:vat  = job:Invoice_Courier_Cost * (inv:Vat_Rate_Labour/100) + |
                                job:Invoice_Parts_Cost * (inv:Vat_Rate_Parts/100) + |
                                job:Invoice_Labour_Cost * (inv:Vat_Rate_Labour/100)

                job:Invoice_Sub_Total = job:Invoice_Courier_Cost + |
                                        job:Invoice_Labour_Cost + |
                                        job:Invoice_Parts_Cost

                tmpiac:Total = tmpiac:Vat + job:Invoice_Sub_total

            Else!If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
        Else
            tmp:HandlingFee = jobe:HandlingFee
            tmp:ExchangeFee = jobe:ExchangeRate
        End !If job:Invoice_Number = 0

        !Paid?
        tmprc:Paid          = 0
        tmprc:Outstanding   = 0
        tmpac:Paid          = 0
        tmpac:Outstanding   = 0
        tmpiac:Paid         = 0
        tmpiac:Outstanding  = 0
        tmpirc:Paid         = 0
        tmpirc:Outstanding  = 0
        tmp:TotalPaid = 0
        Save_jpt_ID = Access:JOBPAYMT.SaveFile()
        Access:JOBPAYMT.ClearKey(jpt:All_Date_Key)
        jpt:Ref_Number = job:Ref_Number
        Set(jpt:All_Date_Key,jpt:All_Date_Key)
        Loop
            If Access:JOBPAYMT.NEXT()
               Break
            End !If
            If jpt:Ref_Number <> job:Ref_Number      |
                Then Break.  ! End If
            Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code   = jpt:User_code
            If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                !Found
                If use:Location = tmp:ARCLocation
                    tmpac:Paid += jpt:Amount
                    tmpiac:Paid += jpt:Amount
                Else !If use:Location = tmp:ARCLocation
                    tmprc:Paid += jpt:Amount
                    tmpirc:Paid += jpt:Amount
                End !If use:Location = tmp:ARCLocation
            Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                !Error
            End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
            tmp:TotalPaid += jpt:Amount
        End !Loop
        Access:JOBPAYMT.RestoreFile(Save_jpt_ID)
    End!If job:chargeable_job = 'YES'


    tmp:3rdPartyCharge  = jobe:ARC3rdPartyCost + jobe:ARC3rdPartyMarkup

    tmp:3rdPartyVAT     = (jobe:ARC3rdPartyCost * trd:VatRate/100)

    tmp:3rdPartyTotal   = jobe:ARC3rdPartyCost + tmp:3rdPartyVAT



    tmpac:Vat       = (job:Courier_Cost * (VatRate(job:Account_Number,'L') /100)) + |
                               (job:Parts_Cost * (VatRate(job:Account_Number,'P') /100)) +  |
                                (job:Labour_Cost * (VatRate(job:Account_Number,'L')/100))


    tmpac:Total     = job:Sub_Total + tmpac:VAT


    tmprc:Vat       = (job:Courier_Cost * (VatRate(job:Account_Number,'L') /100)) + |
                           (jobe:RRCCPartsCost * (VatRate(job:Account_Number,'P') /100)) +  |
                            (jobe:RRCCLabourCost * (VatRate(job:Account_Number,'L')/100))

    tmprc:Total     = tmprc:Vat + jobe:RRCCSubTotal


    tmpaw:Vat   = (job:Parts_Cost_Warranty * (VatRate(job:Account_Number,'P') /100)) +  |
                    (job:Labour_Cost_Warranty * (VatRate(job:Account_Number,'L')/100)) + |
                    (job:Courier_Cost_Warranty * (VatRate(job:Account_Number,'L') /100))

    tmprw:Vat   = (jobe:RRCWPartsCost * (VatRate(job:Account_Number,'P') /100)) +  |
                    (jobe:RRCWLabourCost * (VatRate(job:Account_Number,'L')/100))

    tmpaw:Total = tmpaw:Vat + job:Sub_Total_Warranty

    tmprw:Total = tmprw:Vat + jobe:RRCWSubTotal


    tmpes:Vat    = (job:Courier_Cost_Estimate * (VatRate(job:Account_Number,'L') /100)) + |
                           (job:Parts_Cost_Estimate * (VatRate(job:Account_Number,'P') /100)) +  |
                            (job:Labour_Cost_Estimate * (VatRate(job:Account_Number,'L')/100))
    tmpes:Total  = tmpes:VAT + job:Sub_Total_Estimate


    tmpcl:SubTotal      = job:Courier_Cost_Warranty + jobe:ClaimPartsCost + jobe:ClaimValue

    tmpcl:Vat           = (job:Courier_Cost_Warranty * (VatRate(job:Account_Number,'L') /100)) + |
                           (jobe:ClaimPartsCost * (VatRate(job:Account_Number,'P') /100)) +  |
                            (jobe:ClaimValue * (VatRate(job:Account_Number,'L')/100))

    tmpcl:Total         = tmpcl:SubTotal + tmpcl:Vat

    IF (_PreClaimThirdParty(job:Third_Party_Site,job:Manufacturer))
        tmpcl:Total += jobe2:ThirdPartyHandlingFee  ! #12363 Add third party value IF used (DBH: 16/02/2012)
    END


    tmpres:Vat  = (job:Courier_Cost_Estimate * (VatRate(job:Account_Number,'L') /100)) + |
                           (jobe:RRCEPartsCost * (VatRate(job:Account_Number,'P') /100)) +  |
                            (jobe:RRCELabourCost * (VatRate(job:Account_Number,'L')/100))
    tmpres:Total        = jobe:RRCESubTotal + tmpres:Vat

    jobe:InvRRCWSubTotal    = jobe:InvRRCWPartsCost + jobe:InvRRCWLabourCost
    !End !If jobe:InvRRCWSubTotal = 0

    tmpirw:vat  = jobe:InvRRCWPartsCost * (inv:Vat_Rate_Parts/100) + |
                    jobe:InvRRCWLabourCost * (inv:Vat_Rate_Labour/100)

    tmpirw:Total    = tmpirw:Vat + jobe:InvRRCWSubTotal



    tmprc:Outstanding   = tmprc:Total - tmprc:Paid
    tmpac:Outstanding   = tmpac:Total - tmpac:Paid
    tmpiac:Outstanding  = tmpiac:Total - tmpiac:Paid
    tmpirc:Outstanding  = tmpirc:Total - tmpirc:Paid

    !access:jobs.update()
    Do TickBoxUpdate
    Display()
Local.ShowField     Procedure(String func:Field)
Code
    func:Field{prop:Trn} = 0
    func:Field{prop:Skip} = 0
    func:Field{prop:ReadOnly} = 0
    func:Field{prop:Color} = color:White
    func:Field{Prop:FontColor} = 010101H

Local.HideField     Procedure(String func:Field)
Code
    func:Field{prop:Trn} = 1
    func:Field{prop:Skip} = 1
    func:Field{prop:ReadOnly} = 1
    func:Field{prop:color} = 09A6A7CH
    func:Field{prop:fontcolor} = color:White
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW9.SetQueueRecord PROCEDURE

  CODE
  IF (jov:Type = 'C')
    tmp:CreditType = 'Cred'
  ELSE
    tmp:CreditType = 'Inv'
  END
  IF (jov:Type = 'C')
    tmp:CreditNumber = 'CN' & CLIP(jov:InvoiceNumber) & '-' & CLIP(tra:BranchIdentification) & jov:Suffix
  ELSE
    tmp:CreditNumber = CLIP(jov:InvoiceNumber) & '-' & CLIP(tra:BranchIdentification) & jov:Suffix
  END
  IF (jov:Type = 'C')
    tmp:CreditAmount = jov:CreditAmount * -1
  ELSE
    tmp:CreditAmount = jov:NewTotalCost
  END
  PARENT.SetQueueRecord
  SELF.Q.tmp:CreditNumber_NormalFG = -1
  SELF.Q.tmp:CreditNumber_NormalBG = -1
  SELF.Q.tmp:CreditNumber_SelectedFG = -1
  SELF.Q.tmp:CreditNumber_SelectedBG = -1
  SELF.Q.tmp:CreditType_NormalFG = -1
  SELF.Q.tmp:CreditType_NormalBG = -1
  SELF.Q.tmp:CreditType_SelectedFG = -1
  SELF.Q.tmp:CreditType_SelectedBG = -1
  SELF.Q.jov:DateCreated_NormalFG = -1
  SELF.Q.jov:DateCreated_NormalBG = -1
  SELF.Q.jov:DateCreated_SelectedFG = -1
  SELF.Q.jov:DateCreated_SelectedBG = -1
  SELF.Q.tmp:CreditAmount_NormalFG = -1
  SELF.Q.tmp:CreditAmount_NormalBG = -1
  SELF.Q.tmp:CreditAmount_SelectedFG = -1
  SELF.Q.tmp:CreditAmount_SelectedBG = -1
  SELF.Q.tmp:CreditNumber = tmp:CreditNumber          !Assign formula result to display queue
  SELF.Q.tmp:CreditType = tmp:CreditType              !Assign formula result to display queue
  SELF.Q.tmp:CreditAmount = tmp:CreditAmount          !Assign formula result to display queue
   
   
   IF (jov:Type = 'C')
     SELF.Q.tmp:CreditNumber_NormalFG = 255
     SELF.Q.tmp:CreditNumber_NormalBG = 16777215
     SELF.Q.tmp:CreditNumber_SelectedFG = 16777215
     SELF.Q.tmp:CreditNumber_SelectedBG = 255
   ELSE
     SELF.Q.tmp:CreditNumber_NormalFG = 0
     SELF.Q.tmp:CreditNumber_NormalBG = 16777215
     SELF.Q.tmp:CreditNumber_SelectedFG = 16777215
     SELF.Q.tmp:CreditNumber_SelectedBG = 0
   END
   IF (jov:Type = 'C')
     SELF.Q.tmp:CreditType_NormalFG = 255
     SELF.Q.tmp:CreditType_NormalBG = 16777215
     SELF.Q.tmp:CreditType_SelectedFG = 16777215
     SELF.Q.tmp:CreditType_SelectedBG = 255
   ELSE
     SELF.Q.tmp:CreditType_NormalFG = 0
     SELF.Q.tmp:CreditType_NormalBG = 16777215
     SELF.Q.tmp:CreditType_SelectedFG = 16777215
     SELF.Q.tmp:CreditType_SelectedBG = 0
   END
   IF (jov:Type = 'C')
     SELF.Q.jov:DateCreated_NormalFG = 255
     SELF.Q.jov:DateCreated_NormalBG = 16777215
     SELF.Q.jov:DateCreated_SelectedFG = 16777215
     SELF.Q.jov:DateCreated_SelectedBG = 255
   ELSE
     SELF.Q.jov:DateCreated_NormalFG = 0
     SELF.Q.jov:DateCreated_NormalBG = 16777215
     SELF.Q.jov:DateCreated_SelectedFG = 16777215
     SELF.Q.jov:DateCreated_SelectedBG = 0
   END
   IF (jov:Type = 'C')
     SELF.Q.tmp:CreditAmount_NormalFG = 255
     SELF.Q.tmp:CreditAmount_NormalBG = 16777215
     SELF.Q.tmp:CreditAmount_SelectedFG = 16777215
     SELF.Q.tmp:CreditAmount_SelectedBG = 255
   ELSE
     SELF.Q.tmp:CreditAmount_NormalFG = 0
     SELF.Q.tmp:CreditAmount_NormalBG = 16777215
     SELF.Q.tmp:CreditAmount_SelectedFG = 16777215
     SELF.Q.tmp:CreditAmount_SelectedBG = 0
   END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

ConvertEstimateParts PROCEDURE                        ! Declare Procedure
tmp:AddToStockAllocation BYTE(0)
tmpLocation          STRING(30)
tmp:CreateNewOrder   BYTE
save_epr_id          USHORT,AUTO
StockError           BYTE
Local                CLASS
AllocateExchangePart Procedure(String func:Type,Byte func:Allocated)
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
    setcursor(cursor:wait)
    tmp:AddToStockAllocation = 0

    !JC took this out of the loop - you don't need to look this up for every new part
    Access:USERS.Clearkey(use:User_Code_Key)
    use:User_Code   = job:Engineer
    If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        if glo:RelocateStore then
            tmpLocation  = 'MAIN STORE'     !glo:RelocatedFrom
        ELSE
            tmpLocation  = use:Location
        END
        !Found
    END !if users.fetch

    save_epr_id = access:estparts.savefile()

    !TB13024 - before allocating any stock check to ensure stock is clear to handle this   JC 12/03/13
    StockError = false
    access:estparts.clearkey(epr:part_number_key)
    epr:ref_number  = job:ref_number
    set(epr:part_number_key,epr:part_number_key)
    loop

        if access:estparts.next() then break.
        if epr:ref_number  <> job:ref_number then break.

        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if

        Access:STOCK.ClearKey(sto:ref_number_key)
        sto:ref_number = epr:Part_Ref_Number
        if Access:STOCK.Fetch(sto:ref_number_key)
            !error 1 - stock not found
            miss# = Missive('The Estimate cannot be accepted at this time.'&|
                            'The stock record is missing.'&|
                            '|Ref No: '& Clip(epr:Part_Ref_Number),'ServiceBase 3g',|
                            'midea.jpg','/OK')
            StockError = true
            Break
        ELSE
            !Record in Use?
            Pointer# = Pointer(Stock)
            Hold(Stock,1)
            Get(Stock,Pointer#)
            If Errorcode() = 43
                !error 2
                miss# = Missive('The Estimate cannot be accepted at this time.'&|
                                'The stock record is locked.'&|
                                '|Part No: '& Clip(sto:Part_Number) & |
                                '|Location: ' & Clip(sto:Location),'ServiceBase 3g',|
                                'midea.jpg','/OK')
                StockError = true
                Break
            End !If Errorcode() = 43
            Release(Stock)

        END !if stock fetched OK

    END !loop through estparts to check if stock is OK

    !TB13024 - ends here - providing there were no stock errors we can proceed...
    If StockError = false then

        !older code (pre13024) continues from here
        access:estparts.clearkey(epr:part_number_key)
        epr:ref_number  = job:ref_number
        set(epr:part_number_key,epr:part_number_key)
        loop
            if access:estparts.next()
               break
            end !if
            if epr:ref_number  <> job:ref_number      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if

            Access:STOCK.ClearKey(sto:ref_number_key)
            sto:ref_number = epr:Part_Ref_Number
            if not Access:STOCK.Fetch(sto:ref_number_key)
                if sto:ExchangeUnit = 'YES'
                    !Check for existing unit - then exchange
                    !chargeable parts and partnumber = EXCH?
                    access:parts.clearkey(par:Part_Number_Key)
                    par:Ref_Number = job:ref_number
                    par:Part_Number = 'EXCH'
                    if access:parts.fetch(par:Part_NUmber_Key) = level:benign
                        !found an existing Exchange unit
                        Case Missive('This job already has an exchange unit.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    ELSE

                        Access:STOCK.Clearkey(sto:Location_Key)
                        sto:Location    = tmpLocation
                        sto:Part_Number = 'EXCH'
                        If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                            access:location.clearkey(loc:Location_Key)
                            loc:Location = tmpLocation
                            access:location.fetch(loc:Location_Key)
                            !changed 19/11 alway allocate at once
                            !change back 20/11
                            if loc:UseRapidStock then
                                !Found
                                Local.AllocateExchangePart('CHA',0)
                            ELSE
                                ExchangeUnitNumber# = job:Exchange_Unit_Number
                                ViewExchangeUnit()
                                Access:JOBS.TryUpdate()
                                ! Inserting (DBH 16/09/2008) # 10253 - Update Date/Time Stamp
                                UpdateDateTimeStamp(job:Ref_Number)
                                ! End (DBH 16/09/2008) #10253
                                If job:Exchange_Unit_Number <> ExchangeUnitNumber#
                                    Local.AllocateExchangePart('CHA',1)
                                End !If job:Exchange_Unit_Number <> ExchangeUnitNumber#
                            END !if loc:useRapidStock
                        Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                            !Error
                            Case Missive('You must have a Part setup in Stock Control with a Part Number of "EXCH" under the location ' & Clip(tmpLocation) & '.','ServiceBase 3g',|
                                           'mstop.jpg','/OK')
                                Of 1 ! OK Button
                            End ! Case Missive
                        End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign

                        GetStatus(108,0,'EXC') ! 108 - Exchange unit required
                    End !If exchang unit already existed

                    !Remove the tick so that if the job is made "Unaccpeted"
                    !then you won't have to remove these parts to stock twice
                    If epr:UsedOnRepair
                        epr:UsedOnRepair = 0
                        Access:ESTPARTS.Update()
                    End !If epr:UsedOnRepair
                    cycle
                end
            end

            get(parts,0)
            glo:select1 = ''
            if access:parts.primerecord() = level:benign
                !message('New part created')
                par:ref_number           = job:ref_number
                par:adjustment           = epr:adjustment
                par:part_number     = epr:part_number
                par:description     = epr:description
                par:supplier        = epr:supplier
                par:purchase_cost   = epr:purchase_cost
                par:sale_cost       = epr:sale_cost
                par:retail_cost     = epr:retail_cost
                par:quantity             = epr:quantity
                par:exclude_from_order   = epr:exclude_from_order
                par:part_ref_number      = epr:part_ref_number
                !If not used, then mark to be allocated
                If epr:UsedOnRepair
                    !message('part used on repair')
                    par:PartAllocated       = epr:PartAllocated
                Else !If epr:UsedOnRepair
                    par:PartAllocated       = 0
                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                    sto:Ref_Number  = par:Part_Ref_Number
                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Found
                        IF (sto:Sundry_Item <> 'YES')    ! #11837 Don't decrement a sundry item. (Bryan: 08/12/2010)
                            !message('Not a sundry item')
                            If par:Quantity <= sto:Quantity_Stock
                                !If its not a Rapid site, turn part allocated off
                                If RapidLocation(sto:Location)
                                    par:PartAllocated = 0
                                End !If RapidLocation(sto:Location) = Level:Benign
                                sto:Quantity_Stock  -= par:Quantity
                                If sto:Quantity_Stock < 0
                                    sto:Quantity_Stock = 0
                                End !If sto:Quantity_Stock < 0

                                tmp:AddToStockAllocation = 1

                                If Access:STOCK.Update() = Level:Benign
                                    If def:Add_Stock_Label = 'YES'
                                        glo:Select1 = sto:Ref_Number
                                        Case def:Label_Printer_Type
                                            of 'TEC B-440 / B-442'
                                                stock_request_label(Today(),Clock(),par:quantity,job:ref_number,job:engineer,par:sale_cost)
                                            of 'TEC B-452'
                                                stock_request_label_B452(Today(),Clock(),par:quantity,job:ref_number,job:engineer,par:sale_cost)
                                        End !Case def:Label_Printer_Type
                                    End !If def:Add_Stock_Label = 'YES'
                                    If AddToStockHistory(sto:Ref_Number, |              ! Ref_Number
                                                         'DEC', |                       ! Transaction_Type
                                                         par:Despatch_Note_Number, |    ! Depatch_Note_Number
                                                         job:Ref_Number, |              ! Job_Number
                                                         0, |                           ! Sales_Number
                                                         par:Quantity, |                ! Quantity
                                                         sto:Purchase_Cost, |           ! Purchase_Cost
                                                         sto:Sale_Cost, |               ! Sale_Cost
                                                         sto:Retail_Cost, |             ! Retail_Cost
                                                         'STOCK DECREMENTED', |         ! Notes
                                                         '')                            ! Information
                                        ! Added OK
                                    Else ! AddToStockHistory
                                        ! Error
                                    End ! AddToStockHistory
                                End! If Access:STOCK.Update() = Level:Benign
                            Else !If par:Quantity < sto:Quantity
                                !message('More wanted than available')
                                If VirtualSite(sto:Location)
                                    !message('This is a virtual site')
                                    CreateWebOrder('C',par:Quantity - sto:Quantity_Stock)
                                    If sto:Quantity_Stock > 0
                                        !message('set select1 to new pending web')
                                        glo:Select1 = 'NEW PENDING WEB'
                                        glo:Select2 = par:Part_Ref_Number
                                        glo:Select3 = par:Quantity - sto:Quantity_Stock
                                        !glo:Select4 =
                                        par:Quantity    = sto:Quantity_Stock
                                        par:Date_Ordered    = Today()
                                        tmp:AddToStockAllocation = 0
                                    Else !If sto:Quantity_Stock > 0
                                        par:WebOrder    = 1
                                        par:PartAllocated = 0
                                        ! Inserting (DBH 19/06/2006) #6733 - Add to Stock Allocation
                                        ! Inserting (DBH 12/09/2006) # 8188 - Save record
                                         tmp:AddToStockAllocation = 2
                                        !Access:PARTS.Update()
                                        ! End (DBH 12/09/2006) #8188
                                        !AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'WEB',job:Engineer)
                                        ! End (DBH 19/06/006) #6733
                                    End !If sto:Quantity_Stock > 0
                                Else !If job:WebJob
                                    !message('this is NOT a virtual site def:summaryOrders is '&def:SummaryOrders)
                                    tmp:CreateNewOrder = 0
                                    Case def:SummaryOrders
                                        Of 0 !Old Way
                                            tmp:CreateNewOrder = 1
                                            !message('Old way')
                                        Of 1 !New Way
                                            !Check to see if a pending order already exists.
                                            !If so, add to that.
                                            !If not, create a new pending order
                                            !message('New way')
                                            Access:ORDPEND.Clearkey(ope:Supplier_Key)
                                            ope:Supplier    = par:Supplier
                                            ope:Part_Number = par:Part_Number
                                            If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                                !Found
                                                ope:Quantity += par:Quantity
                                                Access:ORDPEND.Update()

                                                tmp:CreateNewOrder = 0  !doing it here
                                                !If there is some in stock, then use that for this part
                                                !then pass the variables, to create another part line
                                                !for the ordered part.
                                                If sto:Quantity_Stock > 0
                                                    !message('Setting varialbes to order '&clip(par:Quantity-sto:quantity_stock))
                                                    glo:Select1 = 'NEW PENDING'
                                                    glo:Select2 = par:Part_Ref_Number
                                                    glo:Select3 = par:Quantity-sto:quantity_stock
                                                    glo:Select4 = ope:Ref_Number
                                                    par:Quantity    = sto:Quantity_Stock
                                                    par:Date_Ordered    = Today()
                                                    if glo:RelocateStore
                                                        !message('Not doing here')
                                                    END
                                                Else !If sto:Quantity_Stock > 0
                                                    par:Pending_Ref_Number  =  ope:Ref_Number
                                                    par:Requested   = True


                                                End !If sto:Quantity_Stock > 0
                                            Else! If Access:ORDPEND.Tryfetch(orp:Supplier_Key) = Level:Benign
                                                tmp:CreateNewOrder  = 1
                                                par:Requested   = True
                                                !Error
                                                !Assert(0,'<13,10>Fetch Error<13,10>')
                                            End! If Access:ORDPEND.Tryfetch(orp:Supplier_Key) = Level:Benign

                                    End !Case def:SummaryOrders

                                    If tmp:CreateNewOrder = 1
                                        !message('Creating a new order')
                                        If Access:ORDPEND.PrimeRecord() = Level:Benign
                                            ope:Part_Ref_Number = sto:Ref_Number
                                            ope:Job_Number      = job:Ref_Number
                                            ope:Part_Type       = 'JOB'
                                            ope:Supplier        = par:Supplier
                                            ope:Part_Number     = par:Part_Number
                                            ope:Description     = par:Description

                                            If sto:Quantity_Stock <=0
                                                par:Pending_Ref_Number  = ope:Ref_Number
                                                ope:Quantity    = par:Quantity
                                                ope:PartRecordNumber    = par:Record_Number
                                            Else !If sto:Quantity_Stock <=0
                                                !message('Setting variables to order '&clip(par:Quantity - sto:Quantity_Stock))
                                                glo:Select1 = 'NEW PENDING'
                                                glo:Select2 = par:Part_Ref_Number
                                                glo:Select3 = par:Quantity - sto:Quantity_Stock
                                                glo:Select4 = ope:Ref_Number
                                                ope:Quantity    = par:Quantity - sto:Quantity_Stock
                                                par:Quantity    = sto:Quantity_Stock
                                                par:Date_Ordered = Today()
                                            End !If sto:Quantity_Stock <=0
                                            If Access:ORDPEND.TryInsert() = Level:Benign
                                                !Insert Successful
                                            Else !If Access:ORDPEND.TryInsert() = Level:Benign
                                                !Insert Failed
                                            End !If Access:ORDPEND.TryInsert() = Level:Benign
                                        End !If Access:ORDPEND.PrimeRecord() = Level:Benign
                                    End !If tmp:CreateNewOrder = 1

                                    !If the stock is relocated then we jump the gun a bit at this point
                                    !Log Number 012031 ( ARC To Request Spares directly) - JC - 25/05/12
                                    if glo:RelocateStore
                                        if sto:Quantity_Stock <= 0 then
                                            !this could not be met - it becomes the order
                                            !message('Ordering')
                                            par:WebOrder    = 1
                                            par:PartAllocated = 0
                                            AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'WEB',job:Engineer)
                                        ELSE
                                            !this could be met from stock - it becomes allocated
                                            !message('Allocating')
                                            par:WebOrder    = 0
                                            par:PartAllocated = 1
                                        END
                                        Access:PARTS.Update()

                                    END !if glo:RelocateStore

                                END !if virtual store

    !Beyond here it reverts to the original code

                            End !If par:Quantity > sto:Quantity
                        ELSE !If (sto:Sundry_Item <> 'YES')
                            !message('This is a sundry item')
                            par:Date_Ordered = Today()
                            par:PartAllocated = 1
                        END ! If (sto:Sundry_Item <> 'YES')
                    Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        !Error
                    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                End !If epr:UsedOnRepair

                par:Fault_Code1         = epr:Fault_Code1
                par:Fault_Code2         = epr:Fault_Code2
                par:Fault_Code3         = epr:Fault_Code3
                par:Fault_Code4         = epr:Fault_Code4
                par:Fault_Code5         = epr:Fault_Code5
                par:Fault_Code6         = epr:Fault_Code6
                par:Fault_Code7         = epr:Fault_Code7
                par:Fault_Code8         = epr:Fault_Code8
                par:Fault_Code9         = epr:Fault_Code9
                par:Fault_Code10        = epr:Fault_Code10
                par:Fault_Code11        = epr:Fault_Code11
                par:Fault_Code12        = epr:Fault_Code12
                par:Fault_Codes_Checked = epr:Fault_Codes_Checked
                !Neil
                par:RRCPurchaseCost        = epr:RRCPurchaseCost
                par:RRCSaleCost            = epr:RRCSaleCost
                par:InWarrantyMarkUp       = epr:InWarrantyMarkUp
                par:OutWarrantyMarkUp      = epr:OutWarrantyMarkUp
                par:RRCAveragePurchaseCost = epr:RRCAveragePurchaseCost
                par:AveragePurchaseCost    = epr:AveragePurchaseCost

                access:parts.insert()

                !Add part to stock allocation list - L873 (DBH: 11-08-2003)
                Case tmp:AddToStockAllocation
                    Of 1
                        AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'',job:Engineer)
                    Of 2
                        AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'WEB',job:Engineer)
                End !If tmp:AddToStockAllocation

                If glo:Select1 = 'NEW PENDING WEB' or glo:Select1 = 'NEW PENDING'
                    access:stock.clearkey(sto:ref_number_key)
                    sto:ref_number = par:part_ref_number
                    If access:stock.fetch(sto:ref_number_key)
                        Case Missive('An error has occurred retrieving the details of this Stock Part.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    Else!If access:stock.fetch(sto:ref_number_key)
                        sto:quantity_stock     = 0
                        access:stock.update()

                        If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                             'DEC', | ! Transaction_Type
                                             par:Despatch_Note_Number, | ! Depatch_Note_Number
                                             job:Ref_Number, | ! Job_Number
                                             0, | ! Sales_Number
                                             sto:Quantity_stock, | ! Quantity
                                             par:Purchase_Cost, | ! Purchase_Cost
                                             par:Sale_Cost, | ! Sale_Cost
                                             par:Retail_Cost, | ! Retail_Cost
                                             'STOCK DECREMENTED', | ! Notes
                                             '') ! Information
                            ! Added OK
                        Else ! AddToStockHistory
                            ! Error
                        End ! AddToStockHistory

                        access:parts_alias.clearkey(par_ali:RefPartRefNoKey)
                        par_ali:ref_number      = job:ref_number
                        par_ali:part_ref_number = glo:select2
                        If access:parts_alias.fetch(par_ali:RefPartRefNoKey) = Level:Benign
                            par:ref_number           = par_ali:ref_number
                            par:adjustment           = par_ali:adjustment
                            par:part_ref_number      = par_ali:part_ref_number
                            par:part_number          = par_ali:part_number
                            par:description          = par_ali:description
                            par:supplier             = par_ali:supplier
                            par:purchase_cost        = par_ali:purchase_cost
                            par:sale_cost            = par_ali:sale_cost
                            par:retail_cost          = par_ali:retail_cost
                            par:quantity             = glo:select3
                            par:warranty_part        = 'NO'
                            par:exclude_from_order   = 'NO'
                            par:despatch_note_number = ''
                            par:date_ordered         = ''
                            par:date_received        = ''
                            par:pending_ref_number   = glo:select4
                            par:order_number         = ''
                            par:fault_code1          = par_ali:fault_code1
                            par:fault_code2          = par_ali:fault_code2
                            par:fault_code3          = par_ali:fault_code3
                            par:fault_code4          = par_ali:fault_code4
                            par:fault_code5          = par_ali:fault_code5
                            par:fault_code6          = par_ali:fault_code6
                            par:fault_code7          = par_ali:fault_code7
                            par:fault_code8          = par_ali:fault_code8
                            par:fault_code9          = par_ali:fault_code9
                            par:fault_code10         = par_ali:fault_code10
                            par:fault_code11         = par_ali:fault_code11
                            par:fault_code12         = par_ali:fault_code12
                            If glo:Select1 = 'NEW PENDING WEB' or glo:RelocateStore
                                par:WebOrder = 1
                            End !If glo:Select1 = 'NEW PENDING WEB'
                            access:parts.insert()
                            AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'WEB',job:Engineer)
                        end !If access:parts_alias.clearkey(par_ali:part_ref_number_key) = Level:Benign
                    end !if access:stock.fetch(sto:ref_number_key = Level:Benign
                End !If glo:Select1 = 'NEW PENDING WEB'
            end!if access:parts.primerecord() = level:benign

            !Remove the tick so that if the job is made "Unaccpeted"
            !then you won't have to remove these parts to stock twice
            If epr:UsedOnRepair
                epr:UsedOnRepair = 0
                Access:ESTPARTS.Update()
            End !If epr:UsedOnRepair
        end !loop
    END !if stock error = false

    access:estparts.restorefile(save_epr_id)
    setcursor()
Local.AllocateExchangePart      Procedure(String    func:Type,Byte  func:Allocated)

Code

        Case func:Type
            Of 'WAR'
                get(warparts,0)
                if access:warparts.primerecord() = level:benign
                    wpr:PArt_Ref_Number      = sto:Ref_Number
                    wpr:ref_number            = job:ref_number
                    wpr:adjustment            = 'YES'
                    wpr:part_number           = 'EXCH'
                    wpr:description           = Clip(job:Manufacturer) & ' EXCHANGE UNIT'
                    wpr:quantity              = 1
                    wpr:warranty_part         = 'NO'
                    wpr:exclude_from_order    = 'YES'
                    wpr:PartAllocated         = func:Allocated !was func:Allocated but see VP116 - must always be allocated straight off!
                                                  !changed back from '1' 20/11 - original system reinstalled
                    if func:Allocated = 0 then
                        wpr:Status            = 'REQ'
                    ELSE
                        WPR:Status            = 'PIK'
                    END

                    If sto:Assign_Fault_Codes = 'YES'
                        !Try and get the fault codes. This key should get the only record
                        Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                        stm:Ref_Number  = sto:Ref_Number
                        stm:Part_Number = sto:Part_Number
                        stm:Location    = sto:Location
                        stm:Description = sto:Description
                        If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                            !Found
                            wpr:Fault_Code1  = stm:FaultCode1
                            wpr:Fault_Code2  = stm:FaultCode2
                            wpr:Fault_Code3  = stm:FaultCode3
                            wpr:Fault_Code4  = stm:FaultCode4
                            wpr:Fault_Code5  = stm:FaultCode5
                            wpr:Fault_Code6  = stm:FaultCode6
                            wpr:Fault_Code7  = stm:FaultCode7
                            wpr:Fault_Code8  = stm:FaultCode8
                            wpr:Fault_Code9  = stm:FaultCode9
                            wpr:Fault_Code10 = stm:FaultCode10
                            wpr:Fault_Code11 = stm:FaultCode11
                            wpr:Fault_Code12 = stm:FaultCode12
                        Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                            !Error
                        End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                    end !If sto:Assign_Fault_Codes = 'YES'

                    if access:warparts.insert()
                        access:warparts.cancelautoinc()
                    end
                End

            Of 'CHA'
                get(parts,0)
                if access:parts.primerecord() = level:benign
                    par:PArt_Ref_Number      = sto:Ref_Number
                    par:ref_number            = job:ref_number
                    par:adjustment            = 'YES'
                    par:part_number           = 'EXCH'
                    par:description           = Clip(job:Manufacturer) & ' EXCHANGE UNIT'
                    par:quantity              = 1
                    par:warranty_part         = 'NO'
                    par:exclude_from_order    = 'YES'
                    par:PartAllocated         = func:Allocated
                    !message('Called with func:Allocated ='&clip(func:Allocated))
                    !see above for confusion
                    if func:allocated = 0 then
                        par:Status            = 'REQ'
                    ELSE
                        par:status            = 'PIK'
                    END
                    If sto:Assign_Fault_Codes = 'YES'
                       !Try and get the fault codes. This key should get the only record
                       Access:STOMODEL.ClearKey(stm:Ref_Part_Description)
                       stm:Ref_Number  = sto:Ref_Number
                       stm:Part_Number = sto:Part_Number
                       stm:Location    = sto:Location
                       stm:Description = sto:Description
                       If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Found
                           par:Fault_Code1  = stm:FaultCode1
                           par:Fault_Code2  = stm:FaultCode2
                           par:Fault_Code3  = stm:FaultCode3
                           par:Fault_Code4  = stm:FaultCode4
                           par:Fault_Code5  = stm:FaultCode5
                           par:Fault_Code6  = stm:FaultCode6
                           par:Fault_Code7  = stm:FaultCode7
                           par:Fault_Code8  = stm:FaultCode8
                           par:Fault_Code9  = stm:FaultCode9
                           par:Fault_Code10 = stm:FaultCode10
                           par:Fault_Code11 = stm:FaultCode11
                           par:Fault_Code12 = stm:FaultCode12
                       Else!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                           !Error
                       End!If Access:STOMODEL.TryFetch(stm:Ref_Part_Description) = Level:Benign
                    End !If sto:Assign_Fault_Codes = 'YES'
                    if access:parts.insert()
                        access:parts.cancelautoinc()
                    end
                End !If access:Prime
        End !Case func:Type
BrowseRestrictedManFaultLookup PROCEDURE              !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(MANFAULO)
                       PROJECT(mfo:Field)
                       PROJECT(mfo:Description)
                       PROJECT(mfo:RecordNumber)
                       PROJECT(mfo:NotAvailable)
                       PROJECT(mfo:Manufacturer)
                       PROJECT(mfo:RelatedPartCode)
                       PROJECT(mfo:Field_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
mfo:Field              LIKE(mfo:Field)                !List box control field - type derived from field
mfo:Description        LIKE(mfo:Description)          !List box control field - type derived from field
mfo:RecordNumber       LIKE(mfo:RecordNumber)         !Primary key field - type derived from field
mfo:NotAvailable       LIKE(mfo:NotAvailable)         !Browse key field - type derived from field
mfo:Manufacturer       LIKE(mfo:Manufacturer)         !Browse key field - type derived from field
mfo:RelatedPartCode    LIKE(mfo:RelatedPartCode)      !Browse key field - type derived from field
mfo:Field_Number       LIKE(mfo:Field_Number)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK12::mfo:Field_Number    LIKE(mfo:Field_Number)
HK12::mfo:Manufacturer    LIKE(mfo:Manufacturer)
HK12::mfo:NotAvailable    LIKE(mfo:NotAvailable)
HK12::mfo:RelatedPartCode LIKE(mfo:RelatedPartCode)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Browse the Fault Code Lookup File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Fault Code Lookup File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,112,344,182),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('125L(2)|M~Field~@s30@240L(2)|M~Description~@s60@'),FROM(Queue:Browse:1)
                       BUTTON,AT(448,300),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                       BUTTON,AT(168,300),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       BUTTON,AT(236,300),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                       BUTTON,AT(304,300),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Field'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,98,,10),USE(mfo:Field),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Description'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s60),AT(168,98,124,10),USE(mfo:Description),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020435'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseRestrictedManFaultLookup')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:MANFAULO.Open
  Relate:USERS_ALIAS.Open
  Access:MANFAULT.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MANFAULO,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','BrowseRestrictedManFaultLookup')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,mfo:HideRelatedDescKey)
  BRW1.AddRange(mfo:Field_Number)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?mfo:Description,mfo:Description,1,BRW1)
  BRW1.AddSortOrder(,mfo:HideRelatedFieldKey)
  BRW1.AddRange(mfo:Field_Number)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?mfo:Field,mfo:Field,1,BRW1)
  BIND('GLO:Select1',GLO:Select1)
  BIND('GLO:Select2',GLO:Select2)
  BRW1.AddField(mfo:Field,BRW1.Q.mfo:Field)
  BRW1.AddField(mfo:Description,BRW1.Q.mfo:Description)
  BRW1.AddField(mfo:RecordNumber,BRW1.Q.mfo:RecordNumber)
  BRW1.AddField(mfo:NotAvailable,BRW1.Q.mfo:NotAvailable)
  BRW1.AddField(mfo:Manufacturer,BRW1.Q.mfo:Manufacturer)
  BRW1.AddField(mfo:RelatedPartCode,BRW1.Q.mfo:RelatedPartCode)
  BRW1.AddField(mfo:Field_Number,BRW1.Q.mfo:Field_Number)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Field'
    ?Tab2{PROP:TEXT} = 'By Description'
    ?Browse:1{PROP:FORMAT} ='125L(2)|M~Field~@s30@#1#240L(2)|M~Description~@s60@#2#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:MANFAULO.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseRestrictedManFaultLookup')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = True
  
  case request
      of insertrecord
          If SecurityCheck('FAULT CODE LOOKUP - INSERT')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
  
  
              do_update# = false
          end!If SecurityCheck('FAULT CODE LOOKUP - INSERT')
      of changerecord
          If SecurityCheck('FAULT CODE LOOKUP - CHANGE')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
  
              do_update# = false
          end!If SecurityCheck('FAULT CODE LOOKUP - CHANGE')
      of deleterecord
          If SecurityCheck('FAULT CODE LOOKUP - DELETE')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
  
              do_update# = false
          end!If SecurityCheck('FAULT CODE LOOKUP - DELETE')
  end !case request
  
  If do_update# = True
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMANFAULO
    ReturnValue = GlobalResponse
  END
  End!If do_update# = True
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020435'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020435'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020435'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='125L(2)|M~Field~@s30@#1#240L(2)|M~Description~@s60@#2#'
          ?Tab:2{PROP:TEXT} = 'By Field'
        OF 2
          ?Browse:1{PROP:FORMAT} ='240L(2)|M~Description~@s60@#2#125L(2)|M~Field~@s30@#1#'
          ?Tab2{PROP:TEXT} = 'By Description'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?mfo:Field
      Select(?Browse:1)
    OF ?mfo:Description
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      If GETINI('BROWSEORDER','FaultCode',,CLIP(PATH())&'\SB2KDEF.INI') = 1
          Select(?CurrentTab,2)
          Post(Event:NewSelection,?CurrentTab)
      End !GETINI('BROWSEORDER','FaultCode',,CLIP(PATH())&'\SB2KDEF.INI') = 1
    OF EVENT:GainFocus
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = GLO:Select1
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = GLO:Select3
     GET(SELF.Order.RangeList.List,4)
     Self.Order.RangeList.List.Right = GLO:Select2
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = glo:Select1
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = GLO:Select3
     GET(SELF.Order.RangeList.List,4)
     Self.Order.RangeList.List.Right = GLO:Select2
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  ! Check for restrictions - TrkBs: 5365 (DBH: 27-04-2005)
  If mfo:RestrictLookup = True
      Case glo:Select4
          Of 'CORRECT PART'
              If mfo:RestrictLookupType <> 2
                  Return Record:Filtered
              End ! If mfo:RestrictLookupType <> 2
          Of 'ADJUSTMENT'
              If mfo:RestrictLookupType <> 3
                  Return Record:Filtered
              End ! If mfo:RestrictLookupType <> 3
          Else
              If mfo:RestrictLookupType <> 1
                  Return Record:Filtered
              End ! If mfo:RestrictLookupType <> 1
      End ! Case glo:Select4
  End ! mfo:RestrictLookup = True
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

