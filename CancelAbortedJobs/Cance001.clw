

   MEMBER('CancelAbortedJobs.clw')                    ! This is a MEMBER module

                     MAP
                       INCLUDE('CANCE001.INC'),ONCE        !Local module procedure declarations
                     END


Main                 PROCEDURE                        ! Declare Procedure
pos                  STRING(255)
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

  CODE
   Relate:WEBJOB.Open
   Relate:JOBS.Open
   Relate:AUDIT.Open
   Relate:STATUS.Open
   Relate:STAHEAD.Open
   Relate:DEFAULTS.Open
   Relate:TRADEACC.Open
   Relate:SUBTRACC.Open
   Relate:SUBEMAIL.Open
   Relate:TRAEMAIL.Open
   Relate:STARECIP.Open
   Relate:JOBSTAGE.Open
   Relate:JOBSWARR.Open
    ! Found the job number of a job that is 6 months old (DBH: 10-11-2005)

    Access:JOBS.ClearKey(job:Date_Booked_Key)
    job:date_booked = Today() - 180
    Set(job:Date_Booked_Key,job:Date_Booked_Key)
    Loop
        If Access:JOBS.NEXT()
           Break
        End !If
        If job:date_booked > Today()       |
            Then Break.  ! End If
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
            !Found
            JobNumber# = wob:RecordNumber
            Break
        Else !If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
            !Error
        End !If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
    End !Loop

    Access:WEBJOB.Clearkey(wob:RecordNumberKey)
    Set(wob:RecordNumberKey)
    Loop
        if access:WEBJOB.previous()
            keyfield# = 1
        Else
            keyfield# = wob:RecordNumber + 1
        End
        Break
    End

    Prog.ProgressSetup((KeyField# - JobNumber#)*2)

    LinePrint('Routine Started: ' & Format(Today(),@d6) & ' ' & Format(Clock(),@t1),'CancelAbortedJobs.log')

    Access:JOBS.Clearkey(job:Date_Booked_Key)
    job:Date_Booked = Today() - 180
    Set(job:Date_Booked_Key,job:Date_Booked_Key)
    Loop ! Begin Loop
        If Access:JOBS.Next()
            Break
        End ! If Access:JOBS.Next()
        If job:Date_Booked > TOday()
            Break
        End ! If job:Date_Booked > TOday()
        If Prog.InsideLoop()
            Break
        End ! If Prog.InsideLoop()

        If job:Cancelled = 'YES'
            Cycle
        End ! If wob:Current_Status = '799 JOB CANCELLED'

        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
            !Found
        Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
            !Error
            GetStatus(799,1,'JOB')
            job:Cancelled = 'YES'
            job:Bouncer = ''
            ! Inserting (DBH 11/23/2005) # - Show the location as "Despatched"
            job:Location = 'DESPATCHED'
            ! End (DBH 11/23/2005) #

            ! #VMS 665 After 8 years, have realised this doesn't complete the job. (DBH: 29/07/2013)
            job:date_completed  = Today()
            job:time_completed  = Clock()
            job:completed       = 'YES'
            job:edi             = 'XXX'

            If Access:JOBS.TryUpdate() = Level:Benign
                Count# += 1
                LinePrint(Clip(job:Ref_Number) & ',Booking Aborted (Missing File)','CancelAbortedJobs.log')
                If Access:AUDIT.PrimeRecord() = Level:Benign
                    aud:Ref_Number = job:Ref_Number
                    aud:Date        = Today()
                    aud:Time        = Clock()
                    aud:Type        = 'JOB'
                    aud:User        = ''
                    aud:Action      = 'JOB CANCELLED'
                    !aud:Notes       = 'AUTOMATIC ROUTINE: BOOKING ABORTED'
                    If Access:AUDIT.TryInsert() = Level:Benign
                        ! Insert Successful
                    Else ! If Access:AUDIT.TryInsert() = Level:Benign
                        ! Insert Failed
                        Access:AUDIT.CancelAutoInc()
                    End ! If Access:AUDIT.TryInsert() = Level:Benign
                End !If Access:AUDIT.PrimeRecord() = Level:Benign

                ! Inserting (DBH 22/07/2008) # 10259 - Remove jobs from warranty screens
                Access:JOBSWARR.ClearKey(jow:RefNumberKey)
                jow:RefNumber = job:Ref_Number
                If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                    !Found
                    Delete(JOBSWARR)
                Else ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                    !Error
                End ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                ! End (DBH 22/07/2008) #10259

            End ! If Access:JOBS.Update() = Level:Benign
            ! End (DBH 02/11/2006) #8441\
        End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
    End ! Loop

    Access:WEBJOB.ClearKey(wob:RecordNumberKey)
    wob:RecordNumber = JobNumber#
    Set(wob:RecordNumberKey,wob:RecordNumberKey)
    Loop
        If Access:WEBJOB.NEXT()
           Break
        End !If
        If Prog.InsideLoop()
            Break
        End ! If Prog.InsideLoop()
        If wob:Current_Status = '799 JOB CANCELLED'
            Cycle
        End ! If wob:Current_Status = '799 JOB CANCELLED'

        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = wob:RefNumber
        If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
            !Found
            If Sub(job:Current_Status,1,3) = '799'
                Cycle
            End ! If job:Current_Status = '799 JOB CANCELLED'

            If job:Cancelled = 'YES'
                Cycle
            End ! If job:Cancelled = 'YES'


            If wob:IMEINumber = '* IN PROGRESS *' or (wob:IMEINumber = '' AND job:ESN = '')
                GetStatus(799,1,'JOB')
                
                job:Cancelled = 'YES'
                job:Bouncer = ''
                ! Inserting (DBH 11/23/2005) # - Show the location as "Despatched"
                job:Location = 'DESPATCHED'
                ! End (DBH 11/23/2005) #

                ! #VMS 665 After 8 years, have realised this doesn't complete the job. (DBH: 29/07/2013)
                job:date_completed  = Today()
                job:time_completed  = Clock()
                job:completed       = 'YES'
                job:edi             = 'XXX'
                wob:DateCompleted = TODAY()
                wob:TimeCompleted = CLOCK()
                wob:Completed = 'YES'
                Access:WEBJOB.tryUpdate()
                If Access:JOBS.tryUpdate() = Level:Benign
                    Count# += 1
                    LinePrint(Clip(wob:HeadAccountNumber) & ',' & Clip(wob:RefNumber) & ',Booking Aborted','CancelAbortedJobs.log')
                    If Access:AUDIT.PrimeRecord() = Level:Benign
                        aud:Ref_Number = job:Ref_Number
                        aud:Date        = Today()
                        aud:Time        = Clock()
                        aud:Type        = 'JOB'
                        aud:User        = ''
                        aud:Action      = 'JOB CANCELLED'
                        !aud:Notes       = 'AUTOMATIC ROUTINE: BOOKING ABORTED'
                        If Access:AUDIT.TryInsert() = Level:Benign
                            ! Insert Successful
                        Else ! If Access:AUDIT.TryInsert() = Level:Benign
                            ! Insert Failed
                            Access:AUDIT.CancelAutoInc()
                        End ! If Access:AUDIT.TryInsert() = Level:Benign
                    End !If Access:AUDIT.PrimeRecord() = Level:Benign

                    ! Inserting (DBH 22/07/2008) # 10259 - Remove jobs from warranty screens
                    Access:JOBSWARR.ClearKey(jow:RefNumberKey)
                    jow:RefNumber = job:Ref_Number
                    If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                        !Found
                        Delete(JOBSWARR)
                    Else ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                        !Error
                    End ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                    ! End (DBH 22/07/2008) #10259
                End ! If Access:JOBS.Update() = Level:Benign
            End ! If wob:ESN = '* IN PROGRESS *'
        Else !If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
            !Error
            ! Found WEBJOB. But not JOB record. Cancel it. - TrkBs: 6599 (DBH: 10-11-2005)
            wob:Current_Status = '799 JOB CANCELLED'
            ! #VMS 665 After 8 years, have realised this doesn't complete the job. (DBH: 29/07/2013)
            wob:DateCompleted = TODAY()
            wob:TimeCompleted = CLOCK()
            wob:Completed = 'YES'

            Access:WEBJOB.tryUpdate()
            LinePrint(Clip(wob:HeadAccountNumber) & ',' & Clip(wob:RefNumber) & ',Missing Job Record','CancelAbortedJobs.log')
            Count# += 1
            ! Inserting (DBH 22/07/2008) # 10259 - Remove jobs from warranty screens
            Access:JOBSWARR.ClearKey(jow:RefNumberKey)
            jow:RefNumber = wob:RefNumber
            If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                !Found
                Delete(JOBSWARR)
            Else ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                !Error
            End ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
            ! End (DBH 22/07/2008) #10259
        End !If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
    End !Loop
    LinePrint('Jobs Updated: ' & Count#,'CancelAbortedJobs.log')
    LinePrint('Routine Finished: ' & Format(Today(),@d6) & ' ' & Format(Clock(),@t1),'CancelAbortedJobs.log')
    Prog.ProgressFinish()
   Relate:WEBJOB.Close
   Relate:JOBS.Close
   Relate:AUDIT.Close
   Relate:STATUS.Close
   Relate:STAHEAD.Close
   Relate:DEFAULTS.Close
   Relate:TRADEACC.Close
   Relate:SUBTRACC.Close
   Relate:SUBEMAIL.Close
   Relate:TRAEMAIL.Close
   Relate:STARECIP.Close
   Relate:JOBSTAGE.Close
   Relate:JOBSWARR.Close
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Source
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Open(Prog:ProgressWindow)
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Beep(Beep:SystemQuestion)  ;  Yield()
        Case Message('Are you sure you want to cancel?', |
                'Cancel Pressed', Icon:Question, |
                 Button:Yes+Button:No, Button:No, 0)
        Of Button:Yes
            return 1
        Of Button:No
        End !CASE
    End!If cancel# = 1

    return 0
